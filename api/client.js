import { ApolloClient } from 'apollo-client'
import { HttpLink } from "apollo-link-http"
import { InMemoryCache } from 'apollo-cache-inmemory'
import fetch from 'node-fetch'
import { ApolloLink } from 'apollo-link'
import { RetryLink } from 'apollo-link-retry' 

const link = ApolloLink.from([
  new RetryLink({
    delay: {
      initial: 300,
      max: 1000,
      jitter: true
    },
    attempts: {
      max: 0,
      retryIf: (error, _operation) => !!error
    }
  }),
  new HttpLink({
    uri:'https://apiV2.cricket.com/cricket',
    fetch,
  }),

]);
const defaultOptions = {
	watchQuery: {
		fetchPolicy: 'no-cache',
	},
	query: {
		fetchPolicy: 'no-cache',
	}
}

const client = new ApolloClient({
  link,
  cache: new InMemoryCache({
    addTypename: false,
  }),
  defaultOptions: defaultOptions
})

export default client