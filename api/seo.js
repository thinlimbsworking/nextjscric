export const SCHEDULE = {
	title: {
		all: {
			title: "Cricket Schedule | Current, Upcoming ODI, Test & T20 Matches | Cricket.com",
			description:
				"Get the list of current & upcoming cricket schedules for all international & domestic ODI, Tests & T20 series along with complete match info & more on Cricket.com",
			h1: "Cricket Schedule",
		},
		international: {
			title: "Cricket Schedule, International ODI, Test & T20 Matches | Cricket.com",
			description:
				"Get the list of current and upcoming cricket schedule for all international ODI, Test and T20 series, match schedule and match results at Cricket.com",
			h1: "Cricket Schedule - International Matches",
		},
		"international-live": {
			title: "International Cricket Schedule, Current ODI, Test & T20 | Cricket.com",
			description:
				"Get the list of international cricket schedule for current ODI, Test, and T20 matches at Cricket.com",
			h1: "Cricket Schedule - Current International Matches",
		},
		"international-upcoming": {
			title: "International Cricket Schedule, Upcoming ODI, Test & T20 | Cricket.com",
			description:
				"Get the list of international cricket schedule for upcoming ODI, Test, and T20 matches at Cricket.com",
			h1: "Cricket Schedule - Upcoming International Matches",
		},
		"international-completed": {
			title: "Cricket Results, International ODI, Test & T20 Matches | Cricket.com",
			description:
				"Check match results, full scorecard, stats, match summary and complete match details of all international ODI, Test & T20 series and matches on Cricket.com",
			h1: "Cricket Results - International Matches",
		},
		"international-completed": {
			title: "Cricket Results, International ODI, Test & T20 Matches | Cricket.com",
			description:
				"Check match results, full scorecard, stats, match summary and complete match details of all international ODI, Test & T20 series and matches on Cricket.com",
			h1: "Cricket Results - International Matches",
		},
	},
};
