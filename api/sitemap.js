import {
  TOTAL,
  getArticleIDs,
  getAllplayersForSitemap,
  getAllTeamsForSitemap,
  getAllStadiumsForSitemap,
  getAllSeriesSiteMap,
  getAllScheduleSiteMap,
  getAllFantasySiteMap
} from './queries';
import { CRYCLYTICS_COMPLETED, CRYCLYTICS_UPCOMING, CRYCLYTICS_LIVE, SERIES_TABS } from '../constant/constants';
import {
  scheduleMatchView,
  getNewsUrl,
  getPlayerUrl,
  getTeamUrl,
  getStadiumUrl,
  getSeries,
  getCriclyticsUrl,
  getFrcSeriesTeamUrl,
  getFrcSeriesHomeUrl,
  getFrcSeriesTabUrl
} from './services';
import axios from 'axios';

/**
 * @description to access data from api and genrate data for sitemap
 * @param { String } type sitemap type ex live or completed  match
 * @param { String || null} condition  sitemap switch want to genrate a sitemap for
 * @param { String || null} arg sitemap sub tab  ex - create-team, export-team, matchinfo
 */
const Sitemap = async (type, condition, arg) => {
  let { data: count } = await API(TOTAL);
  let limit = 5000;
  let rawData;
  let reciveData;
  switch (arg && condition ? condition : type) {
    case 'match':
      let scheCount = count.data.getCountForStaticApis.schedulecount;
      rawData = await batchApi(getAllScheduleSiteMap, { skip: 0, limit: 0, status: type }, scheCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllScheduleSiteMap).flat();
      let paths = reciveData.map((match) => {
        return {
          slug: scheduleMatchView(match, arg).as,
          lastMod: new Date(Number(match.updatedAt)).toISOString()
        };
      });

      return paths;

    case 'articles':
      let articleCount = count.data.getCountForStaticApis.articlescount;
      rawData = await batchApi(getArticleIDs, { skip: 0, limit: 0 }, articleCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllArticleIDs).flat();

      return reciveData.map((article) => {
        return {
          slug: getNewsUrl(article).as,
          lastMod: new Date(Number(article.updatedAt)).toISOString()
        };
      });

    case 'players':
      let playersCount = count.data.getCountForStaticApis.playerscount;
      rawData = await batchApi(getAllplayersForSitemap, { skip: 0, limit: 0 }, playersCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllplayersForSitemap).flat();

      return reciveData.map((player) => {
        return {
          slug: getPlayerUrl({ playerName: player.name, ...player }).as
        };
      });
    case 'teams':
      let teamCount = count.data.getCountForStaticApis.teamscount;
      rawData = await batchApi(getAllTeamsForSitemap, { skip: 0, limit: 0 }, teamCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllTeamsForSitemap).flat();

      return reciveData.map((team) => {
        return {
          slug: getTeamUrl(team).as
        };
      });
    case 'stadiums':
      let stadiumCount = count.data.getCountForStaticApis.stadiumscount;
      rawData = await batchApi(getAllStadiumsForSitemap, { skip: 0, limit: 0 }, stadiumCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllStadiumsForSitemap).flat();

      return reciveData.map((stadium) => {
        return {
          slug: getStadiumUrl({ id: stadium.venue_id, name: stadium.name }).as
        };
      });

    case 'series':
      let seriesCount = count.data.getCountForStaticApis.seriescount;
      rawData = await batchApi(getAllSeriesSiteMap, { skip: 0, limit: 0 }, seriesCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllSeriesSiteMap).flat();
      let slugs = [];
      reciveData.forEach((series) => {
        SERIES_TABS.map((tab) => {
          let obj = {
            slug: getSeries(series, tab).as
          };
          // might both condition not pass so thats why added else if for a reason
          if (tab === 'points-table' && series.hasPoints) {
            slugs.push(obj);
          } else if (tab === 'stats' && series.hasStatistics) {
            slugs.push(obj);
          } else if (tab !== 'points-table' && tab !== 'stats') {
            slugs.push(obj);
          }
        });
      });
      return slugs;
    case 'fantasy':
      let fantasyCount = count.data.getCountForStaticApis.fantasyCount;
      rawData = await batchApi(getAllFantasySiteMap, { skip: 0, limit: 0 }, fantasyCount, limit);
      reciveData = rawData.map((data) => data.data.data.getAllFantasySiteMap).flat();

      let allSlugs = [];

      reciveData.forEach((series) => {
        series.match.MatchName = series.match.matchName;
        allSlugs.push({
          slug: getFrcSeriesHomeUrl(series.match).as
        });
        series.teams.forEach((team) => {
          allSlugs.push({
            slug: getFrcSeriesTeamUrl(
              series.match,
              'expert-teams',
              team.fantasy_teamName.split(' ').join('-').toLowerCase()
            ).as
          });
        });
        allSlugs.push({
          slug: getFrcSeriesTabUrl(series.match, 'create-team').as
        });
        allSlugs.push({
          slug: getFrcSeriesTabUrl(series.match, 'expert-teams').as
        });
      });

      return allSlugs;

    case 'criclytics':
      let cryclyticsCount = count.data.getCountForStaticApis.cryclyticsCount;
      rawData = await batchApi(
        getAllScheduleSiteMap,
        { skip: 0, limit: 0, isCricklyticsAvailable: true },
        cryclyticsCount,
        limit
      );
      reciveData = rawData.map((data) => data.data.data.getAllScheduleSiteMap).flat();
      return reciveData
        .map((series) => {
          let tabs = getTabsForCryclytics(series.matchStatus);
          series.homeTeamName = series.homeTeamShortName;
          series.awayTeamName = series.awayTeamShortName;

          return tabs.map((tab) => {
            return {
              slug: getCriclyticsUrl(series, tab).as
            };
          });
        })
        .flat();
  }
};

/**
 *
 * @param {String} matchStatus based on match status return graphql query
 */
function getTabsForCryclytics(matchStatus) {
  switch (matchStatus) {
    case 'completed':
      return CRYCLYTICS_COMPLETED;
    case 'live':
      return CRYCLYTICS_LIVE;
    case 'upcoming':
      return CRYCLYTICS_UPCOMING;
    default:
      return CRYCLYTICS_UPCOMING;
  }
}

/**
 *
 * @param {*} query Graphql Query
 * @param {*} variables Graphql Variable
 */
function API(query, variables) {
  return axios.post(
   'https://apiV2.cricket.com/cricket',
    {
      query: query,
      variables: variables
    },
    { timeout: 10000 }
  );
}

/**
 * @description Because of db limit we process api in batch
 * @param {*} query Graphql Query
 * @param {*} variables Graphql variable
 * @param {*} totalCount Total document we want
 * @param {*} batchCount Per request limit
 */
async function batchApi(query, variables, totalCount, batchCount) {
  let flag = true;
  let allCalls = [];
  let skip = 0;
  while (flag) {
    let varData = { ...variables };
    varData.skip = skip;
    varData.limit = batchCount;
    allCalls.push(API(query, varData));
    skip += batchCount;
    if (skip >= totalCount) {
      flag = false;
    }
  }
  return Promise.all(allCalls);
}

export default Sitemap;
