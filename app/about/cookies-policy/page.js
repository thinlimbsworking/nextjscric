'use client'
import React from 'react'
import { useState, useReducer, useEffect } from 'react'

const backIconWhite = '/svgs/backIconWhite.svg'

export default function Cookies() {
  return (
    <div className="max-w-5xl center md:text-black text-white">
      <div className="py-3 px-2 w-full fixed md:hidden lg:hidden xl:hidden top-0 flex bg-basebg z-10 items-center">
        {/* <img className='mr-2 lg:hidden w-4 h-4' onClick={() => window.history.back()} src={backIconWhite} alt='back icon' /> */}
        <div
          onClick={() => window.history.back()}
          className="outline-0 md:cursor-pointer mr-2 lg:hidden bg-gray rounded"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
        <span className="text-lg font-semibold"> Cookies Policy</span>
      </div>
      <div className="pt-14 md:pt-0 text-justify">
        <div className="md:bg-white bg-gray pb-2">
          <h4 className="pt-1 md:text-lg text-base font-bold px-2">
            {' '}
            COOKIES STATEMENT
          </h4>
          <p className="pt-2 md:text-base text-sm font-semibold px-2 ">
            Cookies
          </p>
          <p className="text-sm pt-3 px-2">
            Cricket.com (the “Site”) and its related mobile applications (the
            “Apps) use a number of different cookies on the Site and the Apps to
            help us remember details of your visit to the Site and Apps. The
            Site and its Apps are owned and operated by Crictec Media Limited, a
            company registered in Ireland with company number 630962 and having
            its registered address at 2 nd Floor, Palmerston House, Fenian
            Street, Dublin 2, Ireland.
          </p>
          <p className="text-sm pt-3 px-2">
            A cookie is a small text file placed on your computer or other
            internet enabled device in order to add functionality. It allows a
            website to remember your actions or preferences over a period of
            time. Some of the cookies we use are essential for the Site and Apps
            to work. Other cookies that we used are not essential for the Site
            and the Apps to work but are useful to assist us to collect
            information about the number of visitors to our Site and Apps for
            example. For further information about cookies, or how to control or
            delete them, then we recommend you visit http://www.aboutcookies.org
            for detailed guidance.
          </p>
          <p className="text-sm pt-3 px-2">
            The list below describes the cookies we use on the Site and Apps and
            what we use them for. To the extent that we may process personal
            data in connection with our use of cookies, our Privacy Statement
            [Insert link to Privacy Statement, once finalised] will apply to our
            use of such personal data and should be read in conjunction with
            this Cookies Statement. If you are not happy with our use of
            cookies, then we recommend that you either not use the Site and
            Apps, or manage your cookie settings in accordance with the
            ‘Managing and disabling cookies section’ below.
          </p>
          <p className="pt-2 md:text-base text-sm font-semibold px-2">
            Log Files
          </p>
          <p className="text-sm pt-3 px-2">
            Like many other websites and mobile applications, the Site and Apps
            make use of log files. The information inside the log files includes
            internet protocol ( IP ) addresses, type of browser, Internet
            Service Provider ( ISP ), date/time stamp, referring/exit pages, and
            number of clicks to analyze trends, administer the site, track
            user’s movement around the site, and gather demographic information.
            IP addresses, and other such information generally are not linked to
            any information that might identify you.
          </p>
          <p className="pt-2 md:text-base text-sm font-semibold px-2">
            First Party Cookies and Web Beacons
          </p>
          <p className="text-sm pt-3 px-2">
            The Site and Apps use cookies to store information about visitors’
            preferences, records user-specific information on which pages the
            user accesses or visits, customises web page content based on
            visitors browser type or other information that the visitor sends
            via his/her browser.
          </p>
          <p className="text-sm pt-3 px-2">
            Our Sites and Apps use different types of cookies to improve your
            experience and to provide services and advertising. We use:
          </p>
          <p className="text-sm pt-3 px-2">
             Essential cookies to enable services you specifically asked for.
          </p>
          <p className="text-sm pt-3 px-2">
             Functionality cookies to recognize when you return to our Site
            and/or Apps so that we can remember your preferences.
          </p>
          <p className="text-sm pt-3 px-2">
             Analytics and performance cookies to count visitors and how our
            Site and Apps are used. These help us understand our audience on an
            aggregate basis. For example number of visitors to a Site or an App,
            where visitors are visiting from, how many pages they look at and
            how long they stay on a page or visit. This information is not used
            to track individual users.
          </p>
          <p className="text-sm pt-3 px-2">
             Advertising cookies to measure interactions with ads and prevent
            showing the same ads to you too often. Also to make advertising more
            relevant to you and more valuable to us and advertisers. These
            cookies are anonymous and we cannot identify you from the cookies we
            place.
          </p>
          <p className="text-sm pt-3 px-2">
            The table below identifies the specific cookies we use, why and
            their duration
          </p>

          <table className="border border-solid border-collapse border-gray-500  m-2">
            <tr className="border border-solid border-collapse  border-gray-500 text-xs">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                Cookie name
              </td>
              <td className="border border-solid border-collapse  border-gray-500">
                Purpose
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Expiry
              </td>
            </tr>
            <tr className="border border-solid border-collapse border-gray-500 text-xs">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                resultid{' '}
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Fan polls tracking
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                30 days
              </td>
            </tr>
            <tr className="border border-solid border-collapse border-gray-500 text-xs">
              <td className="border border-collapse border-gray-500 py-1">
                selected Option
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                User selected option for specific fab poll
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                30 days
              </td>
            </tr>
          </table>
          <p className="pt-2 md:text-base text-sm font-semibold px-2">
            Third Party Cookies
          </p>
          <p className="text-sm pt-3 px-2 ">
            These are cookies set on your machine and device by external
            websites whose services are used on the Site and Apps. Examples of
            these types of cookies are the sharing buttons across the Site and
            Apps that allow visitors to share content onto social networks.
            Cookies are currently set by **LinkedIn, Twitter, Facebook, Google+
            and Pinterest. In order to implement these buttons, and connect them
            to the relevant social networks and external sites, there are
            scripts from domains outside of our Site and Apps. You should be
            aware that these websites may collect personal data about you and
            what you are doing all around the internet, including on this
            website, and you should familiarize yourself with their respective
            privacy statements and cookie statements to understand how they may
            use such information.
          </p>
          <p className="text-sm pt-3 px-2">
            For analytics, we use Clevertap analytics to help us understand how
            visitors engage with our Site and Apps. Clevertap Analytics uses
            cookies to store non personal data. You can find out more in their
            privacy information.
          </p>
          <p className="text-sm pt-3 px-2">
            Advertisers sometimes use their own cookies or similar technologies
            like web beacons for example to better target their ads, to measure
            their ad campaign or to stop showing you an ad.
          </p>
          <p className="text-sm pt-3 px-2">
            The table below identifies the specific third party cookies we use,
            why and their duration:
          </p>
          <table className="border border-solid border-collapse border-gray-500 text-xs mx-2">
            <tr className="border border-solid border-collapse  border-gray-500 text-xs ">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                Provider
              </td>
              <td className="border border-solid border-collapse  border-gray-500">
                Cookie name
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Purpose
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Expiry
              </td>
            </tr>
            <tr className="border border-solid border-collapse border-gray-500 text-xs">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                Google{' '}
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                [ _ga ]
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Used to distinguish users
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                2 years
              </td>
            </tr>
            <tr className="border border-solid border-collapse border-gray-500 text-xs">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                Google
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                [ _gat ]
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Used to throttle request rate
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                1 min
              </td>
            </tr>
            <tr className="border border-solid border-collapse border-gray-500 text-xs">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                Google
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                [ _gid ]
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Used to distinguish users
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                1 day
              </td>
            </tr>
            <tr className="border border-solid border-collapse border-gray-500 text-xs">
              <td className="border border-solid border-collapse border-gray-500 py-1">
                Clevertap
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                [WZRK_S_ TEST-Z88 -4KR-845Z ]
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                Used to distinguish users
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                1 day
              </td>
            </tr>
            <tr className="ba border-collapse border-gray-500 text-xs">
              <td className="ba border-collapse border-gray-500 py-1">
                Clevertap
              </td>
              <td className="ba border-collapse border-gray-500">[WZRK_G]</td>
              <td className="ba border-collapse border-gray-500">
                Duplicate tracking
              </td>
              <td className="border border-solid border-collapse border-gray-500">
                10 years
              </td>
            </tr>
          </table>
          <p className="pt-2 md:text-base text-sm font-semibold px-2">
            Managing and Disabling Cookies
          </p>
          <p className="text-sm pt-3 px-2">
            If you wish to disable cookies, you may do so through your
            individual browser options. More detailed information about cookie
            management with specific web browsers can be found at the browsers'
            respective websites. You can control and/or delete cookies as you
            wish. You can delete all cookies that are already on your device and
            you can set most browsers to prevent them from being placed. If you
            do this, however, you may have to manually adjust some preferences
            every time you visit a site and some services and functionalities
            may not work as intended or at all.
          </p>
        </div>
      </div>
    </div>
  )
}
