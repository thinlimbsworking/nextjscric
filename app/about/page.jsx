'use client'
import React from 'react'
import Link from 'next/link'

// import TermOfUse from '../../components/about/terms-and-conditions'
// import Privacy from '../../components/about/privacy-policy'
// import Cookies from '../../components/about/cookies-policy'

export default function Page({ params }) {
  //   return (
  //     <div className="bg-gray">
  //       <div className="pt-4 ">
  //         <div className="bg-gray pl-3 pr-3 pb-2 ">
  //           <h4 className="pt-3 text-xl font-semibold"> COOKIES STATEMENT2</h4>
  //           <p className="text-base pt-1 font-semibold ">Cookies</p>
  //           <p className="text-sm pt-1">
  //             Cricket.com (the “Site”) and its related mobile applications (the
  //             “Apps) use a number of different cookies on the Site and the Apps to
  //             help us remember details of your visit to the Site and Apps. The
  //             Site and its Apps are owned and operated by Crictec Media Limited, a
  //             company registered in Ireland with company number 630962 and having
  //             its registered address at 2 nd Floor, Palmerston House, Fenian
  //             Street, Dublin 2, Ireland.
  //           </p>
  //           <p className="text-base py-2">
  //             A cookie is a small text file placed on your computer or other
  //             internet enabled device in order to add functionality. It allows a
  //             website to remember your actions or preferences over a period of
  //             time. Some of the cookies we use are essential for the Site and Apps
  //             to work. Other cookies that we used are not essential for the Site
  //             and the Apps to work but are useful to assist us to collect
  //             information about the number of visitors to our Site and Apps for
  //             example. For further information about cookies, or how to control or
  //             delete them, then we recommend you visit http://www.aboutcookies.org
  //             for detailed guidance.
  //           </p>
  //           <p className="text-base py-2">
  //             The list below describes the cookies we use on the Site and Apps and
  //             what we use them for. To the extent that we may process personal
  //             data in connection with our use of cookies, our Privacy Statement
  //             [Insert link to Privacy Statement, once finalised] will apply to our
  //             use of such personal data and should be read in conjunction with
  //             this Cookies Statement. If you are not happy with our use of
  //             cookies, then we recommend that you either not use the Site and
  //             Apps, or manage your cookie settings in accordance with the
  //             ‘Managing and disabling cookies section’ below.
  //           </p>
  //           <p className="f5 pt-1 fw5">Log Files</p>
  //           <p className="text-base py-2">
  //             Like many other websites and mobile applications, the Site and Apps
  //             make use of log files. The information inside the log files includes
  //             internet protocol ( IP ) addresses, type of browser, Internet
  //             Service Provider ( ISP ), date/time stamp, referring/exit pages, and
  //             number of clicks to analyze trends, administer the site, track
  //             user’s movement around the site, and gather demographic information.
  //             IP addresses, and other such information generally are not linked to
  //             any information that might identify you.
  //           </p>
  //           <p className="f5 pt-1 fw5">First Party Cookies and Web Beacons</p>
  //           <p className="text-base py-2">
  //             The Site and Apps use cookies to store information about visitors’
  //             preferences, records user-specific information on which pages the
  //             user accesses or visits, customises web page content based on
  //             visitors browser type or other information that the visitor sends
  //             via his/her browser.
  //           </p>
  //           <p className="text-base py-2">
  //             Our Sites and Apps use different types of cookies to improve your
  //             experience and to provide services and advertising. We use:
  //           </p>
  //           <p className="text-base py-2">
  //              Essential cookies to enable services you specifically asked for.
  //           </p>
  //           <p className="text-base py-2">
  //              Functionality cookies to recognize when you return to our Site
  //             and/or Apps so that we can remember your preferences.
  //           </p>
  //           <p className="text-base py-2">
  //              Analytics and performance cookies to count visitors and how our
  //             Site and Apps are used. These help us understand our audience on an
  //             aggregate basis. For example number of visitors to a Site or an App,
  //             where visitors are visiting from, how many pages they look at and
  //             how long they stay on a page or visit. This information is not used
  //             to track individual users.
  //           </p>
  //           <p className="text-base py-2">
  //              Advertising cookies to measure interactions with ads and prevent
  //             showing the same ads to you too often. Also to make advertising more
  //             relevant to you and more valuable to us and advertisers. These
  //             cookies are anonymous and we cannot identify you from the cookies we
  //             place.
  //           </p>
  //           <p className="text-base py-2">
  //             The table below identifies the specific cookies we use, why and
  //             their duration
  //           </p>

  //           <table className="border-2 border-gray-2 collapse b--mid-gray">
  //             <tr className="border-2 border-gray-2 collapse  b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Cookie name
  //               </td>
  //               <td className="border-2 border-gray-2 collapse  b--mid-gray">
  //                 Purpose
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Expiry
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 resultid{' '}
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Fan polls tracking
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 30 days
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 selected Option
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 User selected option for specific fab poll
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 30 days
  //               </td>
  //             </tr>
  //           </table>
  //           <p className="f5 pt-1 fw5">Third Party Cookies</p>
  //           <p className="text-base pt-1 ">
  //             These are cookies set on your machine and device by external
  //             websites whose services are used on the Site and Apps. Examples of
  //             these types of cookies are the sharing buttons across the Site and
  //             Apps that allow visitors to share content onto social networks.
  //             Cookies are currently set by **LinkedIn, Twitter, Facebook, Google+
  //             and Pinterest. In order to implement these buttons, and connect them
  //             to the relevant social networks and external sites, there are
  //             scripts from domains outside of our Site and Apps. You should be
  //             aware that these websites may collect personal data about you and
  //             what you are doing all around the internet, including on this
  //             website, and you should familiarize yourself with their respective
  //             privacy statements and cookie statements to understand how they may
  //             use such information.
  //           </p>
  //           <p className="text-base pt-1">
  //             For analytics, we use Clevertap analytics to help us understand how
  //             visitors engage with our Site and Apps. Clevertap Analytics uses
  //             cookies to store non personal data. You can find out more in their
  //             privacy information.
  //           </p>
  //           <p className="text-base pt-1">
  //             Advertisers sometimes use their own cookies or similar technologies
  //             like web beacons for example to better target their ads, to measure
  //             their ad campaign or to stop showing you an ad.
  //           </p>
  //           <p className="text-base pt-1">
  //             The table below identifies the specific third party cookies we use,
  //             why and their duration:
  //           </p>
  //           <table className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //             <tr className="border-2 border-gray-2 collapse  b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Provider
  //               </td>
  //               <td className="border-2 border-gray-2 collapse  b--mid-gray">
  //                 Cookie name
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Purpose
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Expiry
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Google{' '}
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 [ _ga ]
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Used to distinguish users
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 2 years
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Google
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 [ _gat ]
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Used to throttle request rate
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 1 min
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Google
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 [ _gid ]
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Used to distinguish users
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 1 day
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Clevertap
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 [WZRK_S_ TEST-Z88 -4KR-845Z ]
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Used to distinguish users
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 1 day
  //               </td>
  //             </tr>
  //             <tr className="border-2 border-gray-2 collapse b--mid-gray text-base">
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Clevertap
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 [WZRK_G]
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 Duplicate tracking
  //               </td>
  //               <td className="border-2 border-gray-2 collapse b--mid-gray">
  //                 10 years
  //               </td>
  //             </tr>
  //           </table>
  //           <p className="f5 pt-1 fw5">Managing and Disabling Cookies</p>
  //           <p className="text-base pt-1">
  //             If you wish to disable cookies, you may do so through your
  //             individual browser options. More detailed information about cookie
  //             management with specific web browsers can be found at the browsers'
  //             respective websites. You can control and/or delete cookies as you
  //             wish. You can delete all cookies that are already on your device and
  //             you can set most browsers to prevent them from being placed. If you
  //             do this, however, you may have to manually adjust some preferences
  //             every time you visit a site and some services and functionalities
  //             may not work as intended or at all.
  //           </p>
  //         </div>
  //       </div>
  //     </div>
  //   )
  const terms = [
    {
      name: 'Term of Use',
      link: 'terms-and-conditions',
      img: '/pngsV2/terms-of-use.png',
    },
    {
      name: 'Privacy Policy',
      link: 'privacy-policy',
      img: '/pngsV2/privacy-policy.png',
    },
    {
      name: 'Cookies Policy',
      link: 'cookies-policy',
      img: '/pngsV2/copy-rights.png',
    },
  ]
  return (
    <div className="max-w-5xl center text-white">
      <div className="px-2 py-3 flex items-center">
        {/* <img className='mr-2 lg:hidden w-4 h-4' onClick={() => window.history.back()} src={backIconWhite} alt='back icon' /> */}
        <div
          onClick={() => window.history.back()}
          className=" outline-0 md:cursor-pointer mr-2 lg:hidden  bg-gray rounded"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
        <span className="text-lg font-semibold">About cricket.com</span>
      </div>

      <div className="lg:mt-3 bg-gray">
        {terms.map((term, i) => (
          <>
            <Link
              href={`/about/${term.link}`}
              passHref
              // onClick={() => {
              //   handleNavigation(term.link);
              // }}
              className=""
              key={i}
            >
              <div className="p-3 flex justify-between items-center">
                <div className="flex items-center">
                  <img src={term.img} alt="" className="w-4 h-4"></img>
                  <p className="text-sm font-medium pl-3">{term.name}</p>
                </div>
                {/* <img src={RightSchevronBlack} alt='' className='w-4 h-4'></img> */}
                <svg width="30" viewBox="0 0 24 24">
                  <path
                    fill="#38d925"
                    d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
              <div className="bg-black/80 h-[1px] w-full" />
            </Link>
          </>
        ))}
        {/* {params?.slug[0] === 'terms-and-conditions' ? (
          <TermOfUse />
        ) : params?.slug[0] === 'cookies-policy' ? (
          <Cookies />
        ) : (
          <Privacy />
        )} */}
      </div>
    </div>
  )
}

// import React from 'react'
// import CleverTap from 'clevertap-react'
// import Link from 'next/link'
// const backIconWhite = '/svgs/backIconWhite.svg'
// const crictecIcon = '/moreSvgs/crictecIcon.svg'
// const RightSchevronBlack = '/svgs/RightSchevronBlack.svg'

// export default function about() {
//   const terms = [
//     {
//       name: 'Term of Use',
//       link: 'terms-and-conditions',
//       img: '/pngsV2/terms-of-use.png',
//     },
//     {
//       name: 'Privacy Policy',
//       link: 'privacy-policy',
//       img: '/pngsV2/privacy-policy.png',
//     },
//     {
//       name: 'Cookies Policy',
//       link: 'cookies-policy',
//       img: '/pngsV2/copy-rights.png',
//     },
//   ]

//   // const handleNavigation = (link) => {
//   //   CleverTap.event('About', {
//   //     Source: 'AboutHome',
//   //     Platform: localStorage.Platform
//   //   });
//   // };

//   return (
//     <div className="max-w-5xl center  text-white">
//       <div className="px-2 py-3 flex items-center">
//         {/* <img className='mr-2 lg:hidden w-4 h-4' onClick={() => window.history.back()} src={backIconWhite} alt='back icon' /> */}
//         <div
//           onClick={() => window.history.back()}
//           className=" outline-0 cursor-pointer mr-2 lg:hidden  bg-gray rounded"
//         >
//           <svg width="30" focusable="false" viewBox="0 0 24 24">
//             <path
//               fill="#ffff"
//               d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
//             ></path>
//             <path fill="none" d="M0 0h24v24H0z"></path>
//           </svg>
//         </div>
//         <span className="text-lg font-semibold">About cricket.com</span>
//       </div>

//       <div className="lg:mt-3 bg-gray">
//         {terms.map((term, i) => (
//           <Link
//             href={`/about/${term.link}`}
//             passHref
//             // onClick={() => {
//             //   handleNavigation(term.link);
//             // }}
//             className=""
//             key={i}
//           >
//             <div className="p-3 flex justify-between items-center">
//               <div className="flex items-center">
//                 <img src={term.img} alt="" className="w-4 h-4"></img>
//                 <p className="text-sm font-medium pl-3">{term.name}</p>
//               </div>
//               {/* <img src={RightSchevronBlack} alt='' className='w-4 h-4'></img> */}
//               <svg width="30" viewBox="0 0 24 24">
//                 <path
//                   fill="#38d925"
//                   d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
//                 ></path>
//                 <path fill="none" d="M0 0h24v24H0z"></path>
//               </svg>
//             </div>
//             <div className="bg-black/80 h-[1px] w-full" />
//           </Link>
//         ))}
//       </div>
//     </div>
//   )
// }
