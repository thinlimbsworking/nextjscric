'use client'
import React from 'react'
const backIconWhite = '/svgs/backIconWhite.svg'

export default function Privacy() {
  return (
    <div className="max-w-5xl center md:text-black text-white">
      <div className="py-3 px-2 w-full fixed md:hidden lg:hidden xl:hidden top-0 flex bg-basebg z-10 items-center">
        {/* <img className='mr-2 lg:hidden w-4 h-4' onClick={() => window.history.back()} src={backIconWhite} alt='back icon' /> */}
        <div
          onClick={() => window.history.back()}
          className=" outline-0 md:cursor-pointer mr-2 lg:hidden  bg-gray rounded"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
        <span className="text-lg font-semibold">Privacy Policy</span>
      </div>
      <div className="pt-14 text-justify md:pt-0">
        <div className="md:bg-white bg-gray py-2">
          <h4 className="pt-1 md:text-lg text-base font-bold px-2">
            PRIVACY STATEMENT
          </h4>

          <div className="flex-col">
            <p className="text-sm pt-3 px-2">
              This website, www.cricket.com (the “Site”), and any related mobile
              applications (“Apps”) are owned and operated by Crictec Media
              Limited (“Crictec”, “we”, “us” or “our”), a company registered in
              Ireland with company number 630962 and having its registered
              address at 2 nd Floor, Palmerston House, Fenian Street, Dublin 2,
              Ireland.
            </p>
            <p className="text-sm pt-3 px-2">
              At Crictec, the privacy of visitors to our Site and Apps is
              important to us. This privacy statement outlines the types of
              personal data that is received and collected in connection with
              the Site and the Apps and how it may be used and processed by us
              in accordance with the General Data Protection Regulation and the
              Data Protection Act 2018 (the “Data Protection Law”). Under Data
              Protection Law, personal data is information that identifies you
              as an individual or is capable of doing so such as your name,
              mobile phone number or email address. For the purposes of Data
              Protection Law, Crictec is considered the controller of your
              personal data. Please read this privacy statement carefully to
              understand how your personal data may be processed.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2">
              Categories of personal data we may gather
            </p>
            <p className="text-sm pt-3 px-2 ">
              Crictec may collect, or have transferred to us, information from
              which you can be identified including:
            </p>
            <p className="text-sm pt-3 px-2">
               When you create an account on the Site or through our Apps
              including to access certain sections of the Site and/or Apps to
              play fantasy cricket or to receive other products and services,
              you may be asked to provide us with your name, mobile number,
              email address and any other information required to create your
              user profile (e.g. password);
            </p>
            <p className="text-sm pt-3 px-2">
               Where you seek to participate in a competition or other game
              such as fantasy cricket, you may be asked to provide certain
              personal data to confirm that you are eligible to participate
              (e.g. confirmation that you are eighteen years or over) and any
              other details required for the operation of the competition or
              game (e.g. your user profile);
            </p>
            <p className="text-sm pt-3 px-2">
               If you comment on articles or other content displayed on our
              Site and/or Apps, we may process any personal data contained in
              those posts;
            </p>
            <p className="text-sm pt-3 px-2">
               When you subscribe to mailing lists, you may be asked to provide
              your email address and details of your marketing preferences;
            </p>
            <p className="text-sm pt-3 px-2">
               When you order products or services on the Site or through the
              Apps you may be asked to, or of your own volition, provide your
              personal data, including your name, mobile number, email address,
              home or business address, etc.;
            </p>
            <p className="text-sm pt-3 px-2">
               When you visit the Site and the Apps we may collect information,
              via cookies and related technology, from the devices you use to
              access the Site and/or Apps. Such information may include details
              about your use of the Site and Apps, third party content, products
              and services. This may also include information about your device,
              machine or browser so that we can make sure the Site and/or Apps
              are displayed in the most effective manner for you and such
              information may include your IP address, identification number,
              online identifier, browser information, location data and other
              similar identifying information required for your devices to
              communicate with websites and applications on the internet. For
              more information on cookies and how to manage them, please see our
              cookies notice.
            </p>
            <p className="text-sm pt-3 px-2">
               If you contact Crictec for any reason, you may provide us with
              your personal data in connection with that communication and we
              may keep a record of this;
            </p>
            <p className="text-sm pt-3 px-2">
               Information provided by other organisations who are legally
              permitted to share information about you with us; and
            </p>
            <p className="text-sm pt-3 px-2">
               Any other personal data that you provide to us or is generated
              in connection with your use of the Site and Apps.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2">
              Purposes for which we may use your personal data
            </p>
            <p className="text-sm pt-3 px-2">
               Registration purposes, we may process your personal data for the
              purpose of registering you as an account holder on our Site and
              Apps and to provide you with log-in details in connection
              therewith;
            </p>
            <p className="text-sm pt-3 px-2">
               Identification purposes, including to ensure that we can
              identify you as a Crictec account holder when you log-in to
              restricted areas of the Site and/or Apps;
            </p>
            <p className="text-sm pt-3 px-2">
               Providing services on the Site and our Apps, including enabling
              you to participate in fantasy cricket or any other games or
              competitions that Crictec may provide from time to time or to
              enable you to comment on articles or other content posted on the
              Site and/or Apps from time to time;
            </p>
            <p className="text-sm pt-3 px-2">
               Verification purposes, including to ensure that you are eligible
              to participate in certain competitions and games that Crictec may
              provide from time to time;
            </p>
            <p className="text-sm pt-3 px-2">
               Direct marketing purposes, your email address may be used by us
              or by other entities within our group to send you direct marketing
              materials where you have provided your consent to such and have
              not opted-out of receiving such (See section entitled ‘Disclosure
              of your Information’);
            </p>
            <p className="text-sm pt-3 px-2">
               Processing any orders or requests for products and services made
              to us;
            </p>
            <p className="text-sm pt-3 px-2">
               Operating the Site and the Apps, including to ensure that the
              content on the Site and Apps are presented in the most effective,
              responsive and compatible ways for your device;
            </p>
            <p className="text-sm pt-3 px-2">
               Measuring the activity of the Site and Apps, including assessing
              and analysing the level of traffic to the Site and the Apps and
              other statistics and behavior;
            </p>
            <p className="text-sm pt-3 px-2">
               Quality and improvement monitoring, including enabling Crictec
              to update the Site and Apps with a view to improving performance
              and/or to reflect changes in content to be displayed on the Site
              and Apps;
            </p>
            <p className="text-sm pt-3 px-2">
               Communications, we may process your personal data in order to
              facilitate communication with you in particular to respond to or
              address any communication you have submitted to us or to otherwise
              inform you of any changes impacting the Site and/or the Apps such
              as for example that the Site may be unavailable due to
              maintenance; and
            </p>
            <p className="text-sm pt-3 px-2">
               Legal obligations, we may process your personal data where we
              are required or authorised to do so to comply with a legal
              obligation.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2">
              Legal bases
            </p>
            <p className="text-sm pt-3 px-2">
              The legal bases on which Crictec collect, process and transfer
              your personal data in the manner described in this privacy
              statement are as follows:
            </p>
            <p className="text-sm pt-3 px-2">
               Your consent (where we have sought it and you have provided it),
              and in which case, you can withdraw your consent at any time;
            </p>
            <p className="text-sm pt-3 px-2">
               the processing is necessary for the performance of a contract
              with you or in order to take steps at your request prior to
              entering into a contract with you;
            </p>
            <p className="text-sm pt-3 px-2">
               the processing is necessary where we, or a third party, has a
              legitimate interest in processing your personal data such as
              managing and improving the performance of the Site and our Apps,
              operating and promoting our business or to protect against and
              prevent fraud; and
            </p>
            <p className="text-sm pt-3 px-2">
               the processing is necessary for compliance with a legal
              obligation to which we are subject.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2 ">
              Disclosure of your information
            </p>
            <p className="text-sm pt-3 px-2">
              The personal data you provide to us or that we gather in
              connection with the above purposes may be ccessed by or given to
              third parties including:
            </p>
            <p className="text-sm pt-3 px-2">
               our parent company, subsidiaries or any other entity within the
              Crictec group in order to enable them to directly market you where
              you have consented to receiving such marketing material and you
              have not subsequently opted-out of such. Your personal data
              processed by such entities is limited primarily to your email
              address and they only receive your personal data for marketing
              purposes where you have consented to this and have not opted-out.
              We have contractual arrangements in place with such entities where
              they confirm that they will abide by applicable law;
            </p>
            <p className="text-sm pt-3 px-2">
               Third parties including service providers, who on our behalf
              process information, fulfill and deliver orders, provide IT
              services, process payments and provide support services on our
              behalf;
            </p>
            <p className="text-sm pt-3 px-2">
               Service providers to whom we may pass aggregate information on
              the usage of our Site and Apps to assess the performance of such
              but this will not include information that can be used to identify
              you;
            </p>
            <p className="text-sm pt-3 px-2">
               Potential new business partners or owners and their advisors in
              the event of a proposed joint venture, sale or merger with another
              business entity;
            </p>
            <p className="text-sm pt-3 px-2">
               Service providers who provide professional services to us,
              including legal advisors and accountants; and
            </p>
            <p className="text-sm pt-3 px-2">
               Other entities as required or permitted by law, such as law
              enforcement authorities.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2">
              Consequences of not providing your personal data
            </p>
            <p className="text-sm pt-3 px-2">
              Unless you are informed otherwise at the time your personal data
              is collected, the provision of certain of your personal data is
              required for the performance of your relationship with us. If you
              do not provide us with the personal data that we need, we may not
              be able to provide you with certain services such as access to
              fantasy cricket.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2">
              Security
            </p>
            <p className="text-sm pt-3 px-2">
              We are committed to ensuring that your personal data is secure. In
              order to prevent unauthorised access, use or disclosure of your
              personal data, we have put in place suitable physical, electronic
              and managerial procedures to safeguard and secure the information
              we process in connection with the Site and Apps.
            </p>
            <p className="text-sm pt-3 px-2">
              Unfortunately, the transmission of information via the internet is
              not completely secure. Although we will do our best to protect
              your personal data, we cannot guarantee the security of your data
              transmitted to our Site or Apps; any transmission is at your own
              risk.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-2 md:text-base text-sm font-semibold px-2">
              Data transfers
            </p>
            <p className="text-sm pt-3 px-2">
              We may transfer your personal data outside of the European
              Economic Area (EEA), including to a jurisdiction which is not
              recognised by the European Commission as providing for an
              equivalent level of protection for personal data as is provided
              for in the European Union. If and to the extent that we do so, we
              will ensure that appropriate safeguards are in place to comply
              with our obligations under Data Protection Law governing such
              transfers, which may include entering into a contract governing
              the transfer which contains the ‘standard contractual clauses’
              approved for this purpose by the European Commission or, in
              respect of transfers to the United States of America ensuring that
              the transfer is covered by the EU-US Privacy Shield framework.
              Further details of the measures that we have taken in this regard
              are available by contacting us at info@cricket.com.
            </p>
            <p className="f5 pt-1 px-2 font-medium">
              Retention of your personal data
            </p>
            <p className="text-sm pt-3 px-2">
              Crictec will only retain your personal data for as long as is
              necessary for the purposes for which it is processed. Crictec’s
              retention periods differ depending upon the nature of the personal
              data and the purposes for which it is processed.
            </p>
            <p className="text-sm pt-3 px-2">
              Where you register an account with Crictec, your personal data
              will be retained for the duration of our relationship with you and
              for a period of time after as necessary to comply with our legal
              and regulatory obligations under applicable law and, if relevant
              to deal with any complaint or dispute that might arise between us.
              Complaints concerning the retention of your personal data can be
              submitted via email to info@cricket.com.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-1 text-sm font-medium px-2"></p>
            <p className="text-sm pt-3 px-2">
              You have the following rights, in certain circumstances and
              subject to certain restrictions, in relation to your personal
              data:
            </p>
            <p className="text-sm pt-3 px-2">
               Right to access the data – You have the right to request a copy
              of the personal data that we hold about you, together with other
              information about our processing of that personal data;
            </p>
            <p className="text-sm pt-3 px-2">
               Right to rectification – You have the right to request that any
              inaccurate data that is held about you is corrected, or if we have
              incomplete information you may request that we update the
              information such that it is complete;
            </p>
            <p className="text-sm pt-3 px-2">
               Right to erasure – You have the right to request us to delete
              personal data that we hold about you. This is sometimes referred
              to as the right to be forgotten;
            </p>
            <p className="text-sm pt-3 px-2">
               Right to restriction of processing or to object to processing –
              You have the right to request that we no longer process your
              personal data for particular purposes, or to object to our
              processing of your personal data for particular purpose; and
            </p>
            <p className="text-sm pt-3 px-2">
               Right to data portability – You have the right to request us to
              provide you, or a third party, with a copy of your personal data
              in a structured, commonly used machine readable format.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-1 text-sm font-medium px-2">
              Consequences of not providing your personal data
            </p>
            <p className="text-sm pt-3 px-2">
              To exercise any of the rights set out above, you can email us at
              info@cricket.com. If you are not happy with the way in which we
              have used your personal data or addressed your rights, you also
              have the right to lodge a complaint with your supervisory
              authority which in Ireland is the Data Protection Commission. The
              Data Protection Commission’s contact details can be found at
              www.dataprotection.ie.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-1 text-sm font-medium px-2">
              Changes to this privacy statement
            </p>
            <p className="text-sm pt-3 px-2">
              This privacy statement may be reviewed and updated from time to
              time to consider changes in the law and changes in how we may
              process your personal data. Where material changes are made, we
              will post a prominent notice on the Site and Apps indicating that
              we have updated the privacy statement and will indicate at the top
              of the privacy statement when it was last updated. Your continued
              use of the Site and Apps after such changes will be subject to the
              then-current privacy statement. We encourage you to periodically
              review this privacy statement to stay informed about how we
              collect, use, and disclose personal data.
            </p>
            <div className="bg-black/80 mt-2 h-[1px] w-full"></div>

            <p className="pt-1 text-sm font-medium px-2">Contact information</p>
            <p className="text-sm pt-3 px-2">
              If you have questions about this privacy statement or our
              treatment of the information provided to us, please contact us at
              info@cricket.com.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
