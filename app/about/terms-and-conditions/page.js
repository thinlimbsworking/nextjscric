'use client'
import React from 'react'
const backIconWhite = '/svgs/backIconWhite.svg'
// import {Head} from 'react-static'
//import { Link, Router } from "components/Router";
export default function TermOfUse() {
  return (
    <div className="max-w-5xl center md:text-black text-white">
      <div className="py-3 px-2 w-full fixed md:hidden lg:hidden xl:hidden top-0 flex bg-basebg z-10 items-center">
        {/* <img className='mr2 lg:hidden w-4 h-4' onClick={() => window.history.back()} src={backIconWhite} alt='back icon' /> */}
        <div
          onClick={() => window.history.back()}
          className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
        <span className="text-lg font-semibold">Terms Of Use</span>
      </div>

      <div className="pt-14 text-justify md:pt-0">
        <div className="pb-2 md:bg-white bg-gray md:-mt-1">
          <h4 className="pt-1 md:text-lg text-base font-bold px-2">
            TERMS AND CONDITIONS OF USE
          </h4>
          <div className="bg-black/80 h-[1px] w-full"></div>
          <div className="pt-2 md:text-base text-sm font-semibold px-2">
            INTRODUCTION
          </div>
          <p className="text-sm pt-3 px-2">
            This website, www.cricket.com ("Site", “Website”), and its related
            mobile applications (“Apps”) are owned and operated by Crictec Media
            Limited (hereafter referred to as "Crictec", "the Company", “we”,
            “us” and “our”). These terms and conditions of use govern your
            access to and use of the Site and Apps. If you do not agree to be
            bound by these terms and conditions, you should stop using the Site
            and Apps immediately.
          </p>
          <p className="text-sm pt-3 px-2">
            {' '}
            For the purpose of these terms and conditions of use, wherever the
            context so requires "you" or "your" or "user" shall mean any natural
            or legal person who (a) has agreed to become a member of the Site
            and Apps by registering for an account on the Site or through the
            Apps; or (b) browses the Site and/or Apps.{' '}
          </p>
          <div className="bg-black/80 h-[1px] w-full mt-2"></div>

          <div className="pt-2 md:text-base text-sm font-semibold px-2">
            REGISTRATION
          </div>
          <p className="text-sm px-2">
            In order to access certain restricted areas and services on the Site
            and Apps, you may apply to register for an account. By applying to
            register for an account, you represent and warrant that all
            information you provide, now or in the future, is accurate and that
            you will maintain the accuracy of such information. The Company
            reserves the right, in its sole discretion, to refuse registration
            of an account for any reason.{' '}
          </p>
          <p className="text-sm px-2 pt-2">
            If you choose, or are provided with, a password or any other log-in
            details to access your account as part of our security procedures,
            you must treat such information as confidential and you must not
            disclose it or make it available to any third party. You agree to
            immediately notify us of any unauthorised use of your log-in details
            or of any other breach of security that might impact on your use of
            the Site and/or Apps and/or ours. The Company shall not be liable
            for any loss or damage arising from your failure to comply with this
            provision.{' '}
          </p>
          <p className="text-sm px-2 pt-2">
            Your registration is valid from the date you first log-in to the
            Site or Apps and is automatically renewed every time you log-in
            thereafter. If you do not log-in to the Site or Apps for a
            continuous period of 90 days your registration could be
            automatically cancelled.{' '}
          </p>
          <div className="bg-black/80 h-[1px] w-full mt-2"></div>
          <div className="pt-2 md:text-base text-sm font-semibold px-2">
            LICENCE TO ACCESS AND USE THE SITE AND APPS
          </div>
          <p className="text-sm px-2">
            Subject to these terms and conditions of use, the Company hereby
            grants you the right to access and use the Site and Apps only for
            your personal and non-commercial use and in a way that these terms
            and conditions of use permit and for no other purpose.{' '}
          </p>
          <p className="text-sm px-2 pt-2">
            You may post and share links, images, text and other content from
            the Site and the Apps on social media platforms like Facebook,
            Twitter, Google+, Pinterest, with appropriate link-back to the
            original source.{' '}
          </p>
          <div className="bg-black/80 h-[1px] w-full mt-2"></div>
          <div className="pt-2 md:text-base text-sm font-semibold px-2">
            RESTRICTED USES
          </div>
          <p className="text-sm px-2 pt-2">
            In relation to the Site and the Apps, you agree that you shall:
          </p>
          <div>
            <ol type="a">
              <li className="text-sm px-2 pt-2 ">
                <p>
                  not modify, copy, make derivative works, reproduce, republish,
                  upload, download, post, transmit or distribute in any way any
                  material or content from the Site or the Apps including code
                  and software, except where specifically authorised by us;
                </p>{' '}
              </li>
              <li className="text-sm px-2 pt-2">
                <p>
                  not licence, sell, rent, lease, transfer, assign, distribute,
                  host or otherwise commercially exploit the Site or the Apps,
                  whether in whole or in part, or any content displayed on such;
                </p>
              </li>
              <li className="text-sm px-2 pt-2">
                <p>
                  {' '}
                  not access, view or use the Site or the Apps in order to build
                  a competitor website, app, platform, service, competition or
                  game;
                </p>
              </li>
              <li className="text-sm px-2 pt-2">
                <p>
                  not access, screen scrape, retrieve or index any portion of
                  the Site or the Apps or reframe or reformat any of the content
                  thereon;
                </p>
              </li>
              <li className="text-sm px-2 pt-2">
                <p>
                  not use the Site or the Apps to harvest or otherwise collect
                  by any means data, program material or any other information
                  whatsoever (including without limitation, email addresses and
                  personal data of other users);
                </p>
              </li>
              <li className="text-sm px-2 pt-2">
                <p>
                  not download any file posted by another user of a Forum that
                  you know, or reasonably should know, cannot be legally
                  distributed in such manner; and
                </p>
              </li>
              <li className="text-sm px-2 pt-2">
                <p>
                  comply with any content rules that we may issue from time to
                  time.
                </p>
                <p>
                  You represent and warrant that that you have the right to
                  submit and make publicly available any content posted to the
                  Forums. User content posted by you shall be subject to
                  relevant laws and may be removed, or subject to investigation
                  under applicable laws. You agree that the Company may disclose
                  or preserve user content if required to do so by law or in the
                  good faith belief that such preservation or disclosure is
                  reasonably necessary to: (a) comply with law or legal process;
                  (b) respond to claims that any user content violates the
                  rights of third parties; or (c) protect the rights, property,
                  or personal safety of the Company, other users and the public.
                  Furthermore, if you are found to be in non-compliance with the
                  laws and regulations of your territory or these terms and
                  conditions of use, we may in accordance with the section
                  entitled ‘Termination and Suspension’ terminate your
                  account/block your access to the Site and Apps and we reserve
                  the right to remove any user content that is not compliant
                  with these terms and conditions of use.
                </p>
                <p>
                  All Forums are public and not private communications. Chats,
                  postings, conferences, and other communications by other users
                  are not endorsed by the Company, and such communications shall
                  not be considered reviewed, screened, or approved by the
                  Company. The Company reserves the right for any reason to
                  remove without notice any content posted to the Forums
                  received from users, including without limitation message
                  board postings or comments posted under articles or blog
                  posts.
                </p>
              </li>
            </ol>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>
            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              INTELLECTUAL PROPERTY
            </div>
            <p className="text-sm px-2">
              All proprietary rights (including Intellectual Property) in the
              Site and the Apps, including the design of such and any
              information and content contained therein, are the valuable and
              exclusive property of the Company (or its licensors, where
              applicable), and nothing in these terms and conditions of use
              shall be construed as transferring or assigning any such ownership
              rights or any other interest in such rights to you or any other
              person or entity, except where otherwise stated.
            </p>
            <p className="text-sm px-2 pt-2">
              For the purposes of these terms and conditions of use,
              "Intellectual Property" shall mean all intellectual and industrial
              property rights including without limitation, logos, brand names,
              images, designs, photographs, video clips, other materials that
              appear as part of a website or mobile application, copyright,
              database rights and rights in computer software, domain names,
              business names, trade marks, service marks, trade dress, rights in
              get-up and goodwill, the right to sue for passing off and any
              other intellectual property rights whether registered or
              unregistered and existing now or in the future
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              PRIVACY
            </div>
            <p className="text-sm px-2 ">
              Your privacy is important to us. Please read our Privacy Statement
              to understand how we collect, use and process your personal data.
              We recommend that you do this before using our Site or any of our
              Apps.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              ERRORS, CHANGES AND UNAVAILABILITY OF THE SITE AND APPS
            </div>
            <p className="text-sm px-2">
              The contents and information published on the Site and the Apps
              may include inaccuracies or typographical errors from time to
              time. The Company may, but shall not be obliged, from time to time
              update the contents of the Site and the Apps. The Company does not
              guarantee the accuracy, completeness, reliability or currency of
              the contents and information published on the Site and the Apps
              and does not accept any responsibility for keeping such up to date
              and complete or any liability for any failure to do so.
            </p>
            <p className="text-sm px-2 pt-2">
              The Company and/or its respective suppliers may, without notice to
              you, make improvements and/or changes in the Site and the Apps or
              any part thereof. This means that we may add or remove temporarily
              or permanently any content, feature, component or other
              functionality of the Site and the Apps. The Company shall not be
              liable to you or any third party for any changes to the Site and
              the Apps or any part thereof. Any changes to the Site and the Apps
              shall also be subject to these terms and conditions of use. If you
              are not happy with such changes, you can cease using the Site and
              the Apps.
            </p>
            <p className="text-sm px-2 pt-2">
              The Site and the Apps may be temporarily unavailable from time to
              time for various reasons including, without limitation, due to
              required maintenance, telecommunications interruptions, or other
              disruptions. As such, access to the Site and Apps are provided on
              an “as is” and “as available” basis as set out below in the
              section entitled ‘Liability and Indemnification’. The Company will
              not be liable if for any reason the Site, the Apps or any part
              thereof is unavailable at any time.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2 ">
              SECURITY OF COMMUNICATIONS
            </div>
            <p className="text-sm px-2 ">
              Although we have taken all reasonable security precautions, the
              nature of communication via the Internet and other electronic
              means is such that we cannot guarantee the privacy or
              confidentiality of any information relating to you passing by such
              methods. In accessing the Site and the Apps, you accept that
              communications may not be free from interference by third parties
              and may not remain confidential.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2 ">
              LINKS TO THIRD PARTY WEBSITES
            </div>
            <p className="text-sm px-2 ">
              The Site and Apps may contain links to websites owned or operated
              by parties other than Crictec. Such links are provided for your
              convenience only. Crictec does not monitor or control outside
              websites and is not responsible for their content. Crictec’s
              inclusion of links to an outside website does not imply any
              endorsement of the material on our Site or Apps or, unless
              expressly disclosed otherwise, any sponsorship, affiliation or
              association with its owner, operator or sponsor.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2 ">
              SWEEPSTAKES, CONTESTS AND PROMOTIONS
            </div>
            <p className="text-sm px-2 ">
              Any sweepstakes, contest or similar promotion made available
              through the Company’s Site and/or Apps or for which the Company
              may, from time to time, send e-mail messages to you, will be
              governed by official rules that are separate from these terms and
              conditions of use. By participating in any such sweepstakes,
              contest or similar promotion, you will become subject to its
              specific official rules. Note, however, that you remain subject to
              these terms and conditions of use to the extent that they do not
              conflict with the applicable official rules.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              LIABILITY AND INDEMNIFICATION
            </div>
            <p className="text-sm px-2 font-semibold">
              Important – Please read this section carefully as it addresses the
              Company’s liability to you.
            </p>
            <p className="text-sm px-2 pt-2">
              You expressly agree that your use of the Site and the Apps is at
              your sole risk.
            </p>
            <p className="text-sm px-2 pt-2">
              You hereby acknowledge and agree that the Site and the Apps are
              available for use ‘as is’ and ‘as available’, with no warranties
              of any kind whatsoever and that, without prejudice to the
              generality of the foregoing, we make no warranty regarding, and
              shall have no responsibility for, the accuracy, availability,
              reliability, security, fitness for purpose or performance of the
              Site or the Apps or the contents thereof. To the fullest extent
              permitted by applicable law, we hereby expressly exclude all
              conditions, warranties, guarantees and other terms which might
              otherwise be implied by statute, common law or otherwise.
            </p>
            <p className="text-sm px-2 pt-2">
              Nothing in these terms and conditions of use excludes or limits
              the Company’s liability for death or personal injury resulting
              from its negligence, fraud or fraudulent misrepresentation.
              Notwithstanding the foregoing, in no event shall the Company
              and/or its associated entities be liable to you in connection with
              the Site and the Apps for any direct, indirect, punitive,
              incidental, special or consequential damages or losses howsoever
              arising whether based on contract, tort (including negligence or
              breach of statutory duty), misrepresentation (whether innocent or
              negligent) strict liability or otherwise and in no circumstances
              shall they be liable for:
            </p>
            <p className="text-sm px-2 pt-2">
               your inability to use the Site, the Apps or any products or
              services made available through such;
            </p>
            <p className="text-sm px-2 pt-2">
               any decision made or action taken in reliance on any content
              displayed on the Site and/or the Apps;
            </p>
            <p className="text-sm px-2 pt-2">
               any delay or service interruption to the Site or the Apps;
            </p>
            <p className="text-sm px-2 pt-2">
               the loss of use, data, profits or opportunity or arising out of
              or in connection with the access, use, performance or
              functionality of the Site, Apps or any of their contents;
            </p>
            <p className="text-sm px-2 pt-2">
               the conduct of any third party in relation to the Site and Apps;
              and
            </p>
            <p className="text-sm px-2 pt-2">
               loss of or damage to any device or other property.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              TERMINATION AND SUSPENSION
            </div>
            <p className="text-sm px-2">
              The Company reserves the right, in its sole discretion and for any
              reason, to suspend or terminate your access to all or part of the
              Site and/or Apps at any time without notice, and without issuing
              any refunds if the Company suspects or knows that the user is in
              breach of these terms and conditions of use or for any actual or
              improper use of the Site and/or the Apps. If you use multiple
              accounts, you may have action taken against all of your accounts.
            </p>
            <p className="text-sm px-2 pt-2">
              The Company reserves the right to withdraw the Site and/or the
              Apps at any time and without notice to you. We shall not be liable
              to the user or any third party for any loss or damage howsoever
              arising from the termination or withdrawal of the Site and/or Apps
              or any part thereof.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2 ">
              SEVERABILITY
            </div>
            <p className="text-sm px-2 ">
              To the extent that any provisions of these terms and conditions of
              use are held by any court or any competent authority to be
              invalid, unlawful or unenforceable, then such provision shall be
              severed, modified or deleted from the remaining terms, conditions
              and provisions, which will continue to be valid to the fullest
              extent permitted by applicable law.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2 ">
              PRECEDENCE
            </div>
            <p className="text-sm px-2">
              In the event of any conflict and/or inconsistency as between the
              provisions contained in these terms and conditions of use and
              those in any official rules for sweepstakes, contest or similar
              promotion, the official rules shall take precedence.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2 ">
              WAIVER
            </div>
            <p className="text-sm px-2">
              The failure by the Company to perform any of its obligations under
              these terms and conditions of use, or to exercise any of its
              rights or remedies under these terms and conditions of use, shall
              not constitute a waiver of such rights or remedies and shall not
              release you from compliance with such obligations. A waiver by the
              Company of any default by you shall not constitute a waiver of any
              subsequent or future default. No waiver by the Company of any of
              your obligations under these terms and conditions of use shall be
              effective unless it is expressly stated to be a waiver and is
              communicated to you in writing.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              GOVERNING LAW AND JURISDICTION
            </div>
            <p className="text-sm px-2">
              These terms and conditions of use and any disputes or claims
              arising out of or in connection with it or its subject matter or
              formation (including non-contractual disputes or claims) shall be
              governed by and interpreted in accordance with the laws of
              Ireland. For the avoidance of doubt, mandatory consumer laws in
              the country in which you live shall continue to apply to these
              terms and conditions of use.
            </p>
            <p className="text-sm px-2 pt-2">
              The user and Crictec agree that the courts of Ireland shall have
              non-exclusive jurisdiction to settle any dispute or claim arising
              out of or in connection with these terms and conditions of use or
              its subject matter or formation (including non-contractual
              disputes or claims). You can bring a claim to enforce your
              consumer protection rights in connection with these terms and
              conditions of use in the country in which you live.
            </p>
            <div className="bg-black/80 h-[1px] w-full mt-2"></div>

            <div className="pt-2 md:text-base text-sm font-semibold px-2">
              CONTACT US
            </div>
            <p className="text-sm px-2">
              If you wish to contact us, please email info@cricket.com.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
