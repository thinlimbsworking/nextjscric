import gql from 'graphql-tag'
export const HOME_PAGE_POINTS_TABLE_QUEY = gql`
  query topFourTeams {
    topFourTeams {
      teams {
        teamID
        teamName
        matchesPlayed
        points
        qp
        nrr
        wins
        lost
        teamID
        isQualified
      }
      seriesName

      seriesID
      tourID
    }
  }
`

export const GET_NEWS_SOCIAL_TRACKER = gql`
query getSocialTracker {
  getSocialTracker {
    data {
     
      type
      twiterID
      feedID
      position
     
      isHomeSocial
    }
  }
}
`

export const GET_NEWS_SOCIAL_TRACKER_SERVER = `
query getSocialTracker {
  getSocialTracker {
    data {
     
      type
      twiterID
      feedID
      position
     
      isHomeSocial
    }
  }
}
`

export const POSTSERIES_PLAYER_IMPACT = gql`
  query postseries_player_impact($tourID: String) {
    getPostSeriesImpact(tourID: $tourID) {
      tourID
      topTotalImpact {
        playerTeam
        playerID
        playerName
        role
        impactScore
        batting_impact
        bowling_impact
      }
      topBattingImpact {
        playerTeam
        playerID
        playerName
        role
        impactScore
      }
      topBowlingImpact {
        playerTeam
        playerID
        playerName
        role
        impactScore
      }
    }
  }
`

export const GET_PLAYERS_RANKINGS = gql`
  query getPlayerRankings {
    getPlayerRankings {
      ODI {
        men {
          allrounder {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bat {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bowl {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
        }
        women {
          allrounder {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bat {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bowl {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
        }
      }
      Test {
        men {
          allrounder {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bat {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bowl {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
        }
        women {
          allrounder {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bat {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bowl {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
        }
      }
      T20 {
        men {
          allrounder {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bat {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bowl {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
        }
        women {
          allrounder {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bat {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
          bowl {
            matchType
            gender
            rank {
              playerName
              position
              Points
              teamID
              teamName
            }
          }
        }
      }
    }
  }
`

export const MATCH_DATA_TAB_WISE_AXIOS = `
  query getMatchCardTabWiseByMatchID($matchID: String!) {
    getMatchCardTabWiseByMatchID(matchID: $matchID) {
      type
      tourName
      tourID
      matchDetails{
        matchNumber
        date
        toss
      }
      venueID
      venue
      matchOfficials{
        umpire
        thirdrdumpire
        referee
      }
      venueDetails{
        avgFirstInningScore
        paceWicketPercent
        spinWicketPercent
        highestScoreChased
        overall
        bestSuitedFor
      }
      squad{
        teamID
        teamName
        teamShortName
        playingxi{
          playerID
          playerRole
          playerClubName
          playerName
          playerImage
          captain
          keeper
        }
        benchplayers{
          playerID
          playerRole
          playerClubName
          playerName
          playerImage
          captain
          keeper
        }
  
      }
      
      matchSummary {
        innings{
           score{
            overs
            runsScored
            wickets
            runRate
            battingTeamName
            battingTeamID
          }
          battingList{
            playerID
            playerName
            playerTeam
            playerMatchBalls
            playerMatchRuns
            playerBattingNumber
            playerMatchSixes
            playerMatchFours
          }
          bowlingList{
            playerID 
            playerName 
            playerTeam 
            playerDotBalls 
            playerWicketsTaken 
            playerMaidensBowled 
            playerRunsConceeded
            playerOversBowled
            playerEconomyRate
          } 
        }
        topPerformer{
          batsman{
            playerID
            playerName
            playerTeam
            playerMatchBalls
            playerMatchRuns
            playerBattingNumber
            playerMatchSixes
            playerMatchFours
          }
          bowler{
            playerID 
            playerName 
            playerTeam 
            playerDotBalls 
            playerWicketsTaken 
            playerMaidensBowled 
            playerRunsConceeded
            playerOversBowled
            playerEconomyRate
          }
        }
      }
        
    }
  }
`

export const MATCH_DATA_TAB_WISE = gql`
  query getMatchCardTabWiseByMatchID($matchID: String!) {
    getMatchCardTabWiseByMatchID(matchID: $matchID) {
      type
      tourName
      tourID
      matchDetails {
        matchNumber
        date
        toss
      }
      venueID
      venue
      matchOfficials {
        umpire
        thirdrdumpire
        referee
      }
      venueDetails {
        avgFirstInningScore
        paceWicketPercent
        spinWicketPercent
        highestScoreChased
        overall
        bestSuitedFor
      }
      squad {
        teamID
        teamName
        teamShortName
        playingxi {
          playerID
          playerRole
          playerClubName
          playerName
          playerImage
          captain
          keeper
        }
        benchplayers {
          playerID
          playerRole
          playerClubName
          playerName
          playerImage
          captain
          keeper
        }
      }

      matchSummary {
        innings {
          score {
            overs
            runsScored
            wickets
            runRate
            battingTeamName
            battingTeamID
          }
          battingList {
            playerID
            playerName
            playerTeam
            playerMatchBalls
            playerMatchRuns
            playerBattingNumber
            playerMatchSixes
            playerMatchFours
          }
          bowlingList {
            playerID
            playerName
            playerTeam
            playerDotBalls
            playerWicketsTaken
            playerMaidensBowled
            playerRunsConceeded
            playerOversBowled
            playerEconomyRate
          }
        }
        topPerformer {
          batsman {
            playerID
            playerName
            playerTeam
            playerMatchBalls
            playerMatchRuns
            playerBattingNumber
            playerMatchSixes
            playerMatchFours
          }
          bowler {
            playerID
            playerName
            playerTeam
            playerDotBalls
            playerWicketsTaken
            playerMaidensBowled
            playerRunsConceeded
            playerOversBowled
            playerEconomyRate
          }
        }
      }
    }
  }
`

export const SERIES_HOME_CARD_QUERY = gql`
  query SeriesHomeCard {
    SeriesHomeCard {
      topPerformances {
        playerID
        playerName
        teamName
        score
        ScoreType
        tourID
        matchType
        tourName
        teamId
      }
      matchesData {
        awayTeamID
        seriesName
        awayTeamShortName
        homeTeamID
        homeTeamShortName
        matchDateTimeIST
        matchStatus
        venue
        seriesName
        matchName
        seriesID
        tourID
        matchID
        seriesType
      }
      seriesType
    }
  }
`
export const IPL_DASHBOARD_QUERY = gql`
  query iplDashboard {
    iplDashboard {
      tab
      tourID
      seriesName
      seriesShortname
      live {
        inning
        matchNumber
        teamID
        teamShortName
        overs
        runsScored
        wickets
      }
      completed {
        inning
        matchNumber
        teamID
        teamShortName
        overs
        runsScored
        wickets
      }
    }
  }
`
export const IPL_DASHBOARD = ` 
          query iplDashboard{
            iplDashboard{
              tab
              seriesName
              seriesShortname
              tourID
              live{
                  inning
                  matchNumber
                  teamID
                  teamShortName
                  overs
                  runsScored
                  wickets
              }
              completed{
                 inning
                  matchNumber
                  teamID
                  teamShortName
                  overs
                  runsScored
                  wickets
              }
            }
          }`
export const CRIC_UPDATES = gql`
  query articlesHomeScreenCricUpdates($matchID: String!) {
    articlesHomeScreenCricUpdates(matchID: $matchID) {
      articleID
      sm_image_safari
      thumbnail
      title
      type
      description
      authors
      videoID
      videoUrl
      videoYTId
      createdAt
    }
  }
`

export const RUN_COMPARISON = gql`
  query runsComparsion($matchID: String!) {
    runsComparsion(matchID: $matchID) {
      inningsPhase {
        _id
        teamID
        teamShortName
        totalRuns
        totalWickets
        data {
          overNumber
          runs
          wickets
          innings
          teamID
          teamShortName
          score
        }
      }
      format
    }
  }
`

export const GET_RUN_RATE = gql`
  query getRunrate($matchID: String!) {
    getRunrate(matchID: $matchID) {
      format
      inningsPhase {
        innings
        highestRunrate
        teamID
        totalRuns
        totalWickets
        teamShortName
        data {
          r
          o
        }
      }
    }
  }
`
export const HOME_SCREEN_FAN_POLL = gql`
  query homeScreenFanPoll($matchID: String!) {
    homeScreenFanPoll(matchID: $matchID) {
      matchID
      pollID
      totalVotes
      options {
        teamId
        teamName
        teamShortName
        votes
        percentage
      }
    }
  }
`

export const HOME_SCREEN_FAN_POLL_MUTAION = gql`
  mutation homeScreenFanPollUpdate($matchID: String!, $teamId: String!) {
    homeScreenFanPollUpdate(matchID: $matchID, teamId: $teamId) {
      matchID
      pollID
      totalVotes
      options {
        teamId
        teamName
        teamShortName
        votes
        percentage
      }
    }
  }
`
export const HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING = gql`
  query HomeScreenPlayerMatchupsUpcomming($matchID: String!) {
    HomeScreenPlayerMatchupsUpcomming(matchID: $matchID) {
      matchID
      matchStatus
      preMatch {
        homeTeamID
        homeTeamShortName
        awayTeamID
        awayTeamShortName
        mappings {
          batsmanId
          name
          nameHindi
          role
          prePopulated
          label
          shortlist
          bowlers {
            bowlerId
            name
            wicket
            nameHindi
            role
            label
            shortlist
            ballsFaced
            runsScored
            battingSR
          }
        }
      }
    }
  }
`
export const HOME_SCREEN_PLAYER_MATCHUPS_LIVE = gql`
  query HomeScreenPlayerMatchupsLive($matchID: String!) {
    HomeScreenPlayerMatchupsLive(matchID: $matchID) {
      matchID
      matchStatus
      liveMatch {
        batsman {
          playerID
          playerName
          playerTeam
          teamID
          ballfaced
          runsscored
          ballfacedPre
          runsscoredPre
          dismissalsPre
          bowlerlabel
        }
        bowler {
          playerID
          playerName
          teamID
          playerTeam
        }
      }
    }
  }
`
export const HOME_OVER_SIMULATOR = gql`
  query homePageOverSimulator($matchID: String!) {
    homePageOverSimulator(matchID: $matchID) {
      overSimulatorData {
        matchID
        format
        nextThreeScore
        nextThreeOvers
        nextThreeWickets
        currentTeamShortName
        predictedOversArray {
          overNumber
          runs
          wickets
        }
      }
      phaseOfInningsData {
        overNumber
        runs
        wickets
        isCurrentOver
      }
    }
  }
`
export const GET_HOME_PAGE_LIVE_COMMENTS = gql`
  query getHomePageLiveComments($matchID: String!) {
    getHomePageLiveComments(matchID: $matchID) {
      published
      createdAt
      title
      author
      matchName
      matchNumber
      matchSlug
      articleID
      blogType

      comments {
        title
        currentScore
        content
      }
    }
  }
`

export const LIVE_WAGON_DATA = gql`
  query getWagonwheelLive($matchID: String!) {
    getWagonwheelLive(matchID: $matchID) {
      matchBatsman {
        playerID
        playerName
        playerMatchRuns
        playerMatchBalls
        playerMatchSixes
        playerMatchFours
        playerBattingNumber
        playerTeam
        battingStyle
        playerMatchStrikeRate
        playerMatchAvg
        isNotOut
        zad {
          zadval
          runs
        }
      }
    }
  }
`
export const MINI_SCORE_CARD_HOME = gql`
  query miniScoreCard($matchID: String!) {
    miniScoreCard(matchID: $matchID) {
      batting {
        playerFeedID
        playerName
        runs
        battingStyle
        zad {
          zadval
          runs
        }
        playerOnStrike
        playerMatchBalls
        playerMatchStrikeRate
        playerDismissalInfo
        impact
        arrow
        fours
        sixes
      }
      bowling {
        playerFeedID
        playerName
        wickets
        RunsConceeded
        overs
        impact
        arrow
        economy
        maiden
        economy
      }
      partnership
      runRate
      rRunRate
      liveScoreUrl
      data {
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        homeTeamName
        awayTeamName
        matchType
        matchScore {
          teamShortName
          teamID
          teamScore {
            inning
            runsScored
            wickets
            overs
            teamID
            battingTeamShortName
          }
        }
      }
    }
  }
`
export const LAST_12_BALLS = gql`
  query last12Balls($matchID: String!, $innings: String) {
    last12Balls(matchID: $matchID, innings: $innings) {
      over {
        isWicket
        isBall
        isLastBall
        overNumber
        runs
        type
      }
      overNumber
    }
  }
`
export const TEAM_SEARCH = `
query teamSearch($name: String!){teamSearch(name:$name){
  name
  teamID
  shortName
}}
`
export const GET_PAYMENT_SESSION = `
  query getpaySession($token: String!, $planId: String!) {
    creatCheckOutSession(planId: $planId, token: $token) {
      sessionId
    }
  }
`

export const RetrivePaymentStatus = gql`
  query checkOutSuccess($sessionId: String!, $token: String!) {
    checkOutSuccess(sessionId: $sessionId, token: $token) {
      code
      payStatus
      amount
    }
  }
`

export const CDC_EXCLUSIVE_AXIOS = `
 query getCdcExclusive($type:Int) {
getCdcExclusive (type : $type)
{
   title
    data 
    {
        type
        authors
        videoUrl
        videoID
        title
        description
        videoYTId
        articleID
        publishedAt
        createdAt
    }

}
}`
export const CDC_EXCLUSIVE = gql`
  query getCdcExclusive($type: Int) {
    getCdcExclusive(type: $type) {
      title
      data {
        type
        authors
        videoUrl
        videoID
        title
        description
        videoYTId
        articleID
        publishedAt
        createdAt
      }
    }
  }
`

export const ARCHIVE_AXIOS = `
query getArchives($year: String,$format:String, $country:String, $page:Int) {
    getArchives(year:$year, format:$format, country:$country, page:$page)
    {
      tourName
      seriesSearchNameForES
      tourID
      league
      matchLevel
      seriesStartDate
      seriesEndDate
      T20count
      Testcount
      Odicount
      listA
      firstClass
      seriesStatus
      priorityFlag
      compType
    }
}
`
export const Archive = gql`
  query getArchives(
    $year: String
    $format: String
    $country: String
    $page: Int
  ) {
    getArchives(year: $year, format: $format, country: $country, page: $page) {
      tourName
      seriesSearchNameForES
      tourID
      league
      matchLevel
      seriesStartDate
      seriesEndDate
      T20count
      Testcount
      Odicount
      listA
      firstClass
      seriesStatus
      priorityFlag
      compType
    }
  }
`
export const TeamDiscovery = `
query {
  teamDiscoveryV2{
    international{
      mens{teamID
      teamName
        teamShortName
        odiRanking
        testRanking
        t20Ranking
      }
       womens{teamID
      teamName
        teamShortName
        odiRanking
        testRanking
        t20Ranking
      }
    }
    otherLeagues{
      ipl{
        teamID
        teamName
        trophy_details
        teamShortName
      }
      bbl{
        teamID
        teamShortName
        teamName
        trophy_details
      }
      cpl{
        teamID
        teamName
        teamShortName
        trophy_details
      }
      psl{
        teamID
        teamName
        teamShortName
        trophy_details
      }
    }
  }
}
`
export const PAYMENT_CONFIG = gql`
  query getPaymentConfig($token: String!) {
    getPaymentConfig(token: $token) {
      YEAR_PLAN_ID
      MONTHLY_PLAN_ID
    }
  }
`
export const PAYMENT = `
mutation getPayment($paymentPayload: paymentPayload!,$token:String!) {
  getPayment(paymentPayload: $paymentPayload,token: $token) {
      code
      message
    }
  }
`
export const TDL_SCHEDULE_AXIOS = `
query {
  tdlschedule{
		matches{
	 homeTeam
    awayTeam
    homeScore
    homeOver
    awayScore
    awayOver
    manOfTheMatch
    venue
    matchNo
    matchDate
    result
    status
    time
		},
		url1
		url2
    match1Name,
match2Name
	}
}`

export const FEATURED_MATCHES = gql`
  query featuredMatches {
    featurematch {
      criclyticsButtonFlags {
        featuredSeriesName
        isFinalFour
        isPlayerIndex
        tourID
        seriesName
        frcStartTime
      }
      IPLpolling {
        name
        isPolling
        display
        isAuctionStarted
        isCompleted
      }
      displayFeatureMatchScoreCard
      seriesID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      homeTeamName
      awayTeamName
      toss
      startEndDate
      matchStatus
      matchID
      matchType
      statusMessage
      phaseOfInningFlag
      matchNumber
      venue
      matchResult
      startDate
      playerOfTheMatch
      playerofTheMatchTeamShortName
      playing11Status
      probable11Status
      playerID
      winningTeamID
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      fourthInningsTeamID
      matchScore {
        teamShortName
        teamID
        teamFullName
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      isCricklyticsAvailable
      isLiveCriclyticsAvailable
      isAbandoned
      currentDay
      currentSession
    }
  }
`

export const FEATURED_MATCHES_AXIOS = `
	query featuredMatches {
    featurematch {
      IPLpolling {
        name
        isPolling
        display
        isAuctionStarted
      }
      criclyticsButtonFlags{
        featuredSeriesName
        isFinalFour
        isPlayerIndex
        tourID
        seriesName
      }
      displayFeatureMatchScoreCard
      seriesID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      homeTeamName
      awayTeamName
      toss
      startEndDate
      matchStatus
      matchID
      matchType
      statusMessage
      phaseOfInningFlag
      matchNumber
      venue
      matchResult
      startDate
      playerOfTheMatch
      playing11Status
      probable11Status
      playerID
      winningTeamID
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      fourthInningsTeamID
      matchScore {
        teamShortName
        teamID
        teamFullName
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      isCricklyticsAvailable
      isLiveCriclyticsAvailable
      isAbandoned
      currentDay
      currentSession
      isHomeMatchUpade
      content
    }
  }
`

export const FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE_AXIOS = `
query featuredMatches {
	featurematch {
		IPLpolling {
			name
			isPolling
			display
			isAuctionStarted
      isCompleted
      
		}

		displayFeatureMatchScoreCard
		seriesID
		currentinningsNo
		currentInningteamID
		currentInningsTeamName
		seriesName
		homeTeamName
		awayTeamName
		toss
		startEndDate
		matchStatus
		matchID
		matchType
		statusMessage
		matchNumber
		venue
		matchResult
		startDate
		playerOfTheMatch
    playerofTheMatchTeamShortName
		playing11Status
		probable11Status
		playerID
		firstInningsTeamID
		secondInningsTeamID
		thirdInningsTeamID
		teamsWinProbability {
			homeTeamShortName
			homeTeamPercentage
			awayTeamShortName
			awayTeamPercentage
			tiePercentage
		}
		fourthInningsTeamID
		matchScore {
			teamShortName
			teamID
			teamFullName
			teamScore {
				inning
				inningNumber
				battingTeam
				runsScored
				wickets
				overs
				runRate
				battingSide
				teamID
				battingTeamShortName
				declared
				folowOn
			}
		}
		teamsWinProbability {
			homeTeamShortName
			homeTeamPercentage
			awayTeamShortName
			awayTeamPercentage
			tiePercentage
		}
		isCricklyticsAvailable
		isLiveCriclyticsAvailable
		isAbandoned
		currentDay
		currentSession
		isHomeMatchUpade
		content
	}
	getArticleByPostitions{
		articleID
    
		matchIDs{
		  matchID
		  matchSlug
		}
		seriesIDs{
		  name
		  id
		}
		teamIDs{
		  name
		  id
		}
		playerIDs{
		  name
		  id
		}
		approvalStatus
		status
		title
		slugTitle
		bg_image
		sm_image
		bg_image_safari
		sm_image_safari
		thumbnail
		featureThumbnail
		description
		tags{
		  name
		  type
		  tag
		  id
		}
		type
		filters
		createdAt
		updatedAt
		author
		publishedAt
		seoTags
	  }
}
`

export const FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE = gql`
  query featuredMatches {
    featurematch {
      IPLpolling {
        name
        isPolling
        display
        isAuctionStarted
        isCompleted
      }
      seriesList {
        tourID
        name
        count
      }

      displayFeatureMatchScoreCard
      seriesID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      homeTeamName
      awayTeamName
      toss
      startEndDate
      matchStatus
      matchID
      matchType
      statusMessage
      matchNumber
      venue
      matchResult
      startDate
      playerOfTheMatch
      playerofTheMatchTeamShortName
      playing11Status
      probable11Status
      playerID
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      fourthInningsTeamID
      matchScore {
        teamShortName
        teamID
        teamFullName
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      isCricklyticsAvailable
      isLiveCriclyticsAvailable
      isAbandoned
      currentDay
      currentSession
      isHomeMatchUpade
      content
    }
    getArticleByPostitions {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`
export const SERIES_OLD = gql`
  query seriesListing {
    seriesListing {
      type
      list {
        month
        series {
          seriesName
          seriesID
          seriesSlug
          matchType
          matchFormat
          matches
          startDate
          endDate
        }
      }
    }
  }
`

export const SERIES = gql`
  query listseries($type: String!) {
    listseries(type: $type) {
      month
      series {
        seriesID
        seriesName
        Odicount
        T20count
        status
        Testcount
        startEndDate
        league
        startDate
      }
    }
  }
`

export const SERIES_AXIOS = `
  query listseries($type: String!){
  listseries(type: $type){
    month
    series {
      seriesID
      seriesName
      Odicount
      T20count
      status
      Testcount
      startEndDate
      league
	  startDate
      type
    }
  }
}
`
export const SERIES_PRIORITY_AXIOS = `
query  seriesPriority($tab:String,$filter:String,$country:String,$format:String,$page:Int){
  seriesPriority(tab:$tab,filter:$filter,country:$country,format:$format,page:$page){
   
      tourName
      seriesSearchNameForES
      tourID
      league
      seriesStartDate
      seriesEndDate
      T20count
      Odicount
      Testcount
    
  }
  
} `

export const SERIES_PRIORITY = gql`
  query seriesPriority(
    $tab: String
    $filter: String
    $country: String
    $format: String
    $page: Int
  ) {
    seriesPriority(
      tab: $tab
      filter: $filter
      country: $country
      format: $format
      page: $page
    ) {
      tourName
      seriesSearchNameForES
      tourID
      league
      seriesStartDate
      seriesEndDate
      T20count
      matchLevel
      Odicount
      Testcount
    }
  }
`

// seriesPriority(tab:String,filter:String,country:String,format:String,page:Int){
//   seriesPriority(tab:$tab,filter:$filter,country:$country,format:$format,page:$page){
//     tourName
//     seriesSearchNameForES
//     tourID
//     league
//     seriesStartDate
//     seriesEndDate
//     T20count
//     Odicount
//     Testcount
//   }

export const SERIES_DETAILS = gql`
  query matcheslist($seriesID: String!) {
    matcheslist(seriesID: $seriesID) {
      matchName
      isPoints
      isStatistics
      homeTeamShortName
      awayTeamShortName
      matchResult
      venue
      winningTeamID
      matchNumber
      matchdate
      matchID
      matchStatus
      startDate
      matchType
      statusMessage
      playing11Status
      probable11Status
      playerOfTheMatch
      playerID
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      fourthInningsTeamID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      seriesID
      toss
      matchResult
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      matchScore {
        teamShortName
        teamID
        teamFullName
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
    }
  }
`

export const SERIES_DETAILS_AXIOS = `
  query matcheslist($seriesID: String!){
    matcheslist(seriesID: $seriesID) {
      matchName
      isPoints
      isStatistics
      homeTeamShortName
      awayTeamShortName
      matchResult
      venue
      winningTeamID
      matchNumber
      matchdate
      matchID
      matchStatus
      startDate
      matchType
      statusMessage
      playing11Status
      probable11Status
      playerOfTheMatch
      playerID
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      fourthInningsTeamID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      seriesID
      toss
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      matchScore{
        teamShortName
        teamID
        teamFullName
        teamScore{
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }

      }

    }
  }
`

export const SERIES_SQUADS = gql`
  query squads($seriesID: String!) {
    squads(seriesID: $seriesID) {
      teamID
      name
      playerData {
        name
        playerRole
        iswk
        iscap
      }
    }
  }
`

export const SERIES_VENUES = gql`
  query venuedetails($seriesID: String!) {
    venuedetails(seriesID: $seriesID) {
      stadiumName
      capacity
      city
      venueId
    }
  }
`
export const SERIES_VENUES_AXIOS = `
	query venuedetails($seriesID: String!) {
		venuedetails(seriesID: $seriesID) {
			stadiumName
			capacity
			city
			venueId
		}
	}
`

export const SCHEDULE_Axios = `
  query schedule($type: String!, $status: String!,$page:Int!) {
    schedule(type: $type, status: $status,page:$page){
     
    }
  }
`

export const SCHEDULE = gql`
  query schedule($type: String!, $status: String!, $page: Int!) {
    schedule(type: $type, status: $status, page: $page) {
      seriesID

      league
      currentinningsNo
      currentInningsTeamName
      seriesName
      homeTeamName
      awayTeamName
      toss
      startEndDate
      matchStatus
      matchID
      matchType
      statusMessage
      matchNumber
      venue
      matchResult
      startDate
      playerID
      playing11Status
      probable11Status
      playerOfTheMatch
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      fourthInningsTeamID
      matchScore {
        teamShortName
        teamID
        teamFullName
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
    }
  }
`

export const SCHEDULE_NEW_AXIOS = `
query newSchedule($type: String!, $status: String!, $page: Int!)
{
newSchedule(type: $type, status: $status, page: $page){
  seriesView
  seriesAvailable
  seriesID
  matchType
  type
  seriesName  
    matches{
     seriesID

    league
    currentinningsNo
    currentInningsTeamName
    seriesName
    homeTeamName
    awayTeamName
    toss
    startEndDate
    matchStatus
    matchID
    matchType
    statusMessage
    matchNumber
    venue
    matchResult
      teamsWinProbability {
      homeTeamShortName
      homeTeamPercentage
      awayTeamShortName
      awayTeamPercentage
      tiePercentage
    }
      
       matchScore {
      teamShortName
      teamID
      teamFullName
      teamScore {
        inning
        inningNumber
        battingTeam
        runsScored
        wickets
        overs
        runRate
        battingSide
        teamID
        battingTeamShortName
        declared
        folowOn
      }
      }
      
    startDate
      
    playerID
    playing11Status
    probable11Status
    playerOfTheMatch
      
    firstInningsTeamID
    secondInningsTeamID
    thirdInningsTeamID
      
    fourthInningsTeamID
      league
      
    }
   
}
}
`

export const SCHEDULE_NEW = gql`
  query newSchedule($type: String!, $status: String!, $page: Int!) {
    newSchedule(type: $type, status: $status, page: $page) {
      seriesView
      seriesAvailable
      seriesID
      matchType
      seriesName
      type
      matches {
        seriesID

        league
        currentinningsNo
        currentInningsTeamName
        isLiveCriclyticsAvailable
        seriesName
        homeTeamName
        awayTeamName
        toss
        startEndDate
        matchStatus
        matchID
        matchType
        statusMessage
        matchNumber
        venue
        matchResult
        teamsWinProbability {
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
        }

        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }

        startDate

        playerID
        winningTeamID
        playing11Status
        probable11Status
        playerOfTheMatch
        playerofTheMatchTeamShortName
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID

        fourthInningsTeamID
        league
      }
    }
  }
`

export const RANKINGS = gql`
  query getrankings {
    getrankings {
      odi {
        teamID
        ranking
        teamName
        ratings
      }
      test {
        teamID
        ranking
        teamName
        ratings
      }
      t20 {
        teamID
        ranking
        teamName
        ratings
      }
    }
  }
`

export const RANKINGS_AXIOS = `
  query getrankings{
    getrankings{
      odi {
        teamID
        ranking
        teamName
        ratings
      }
      test {
        teamID
        ranking
        teamName
        ratings
      }
      t20 {
        teamID
        ranking
        teamName
        ratings
      }
    }
  }
`

export const RECORDS = gql`
  query getRecords($format: String!, $playertype: String!, $sortType: String!) {
    getRecords(format: $format, playertype: $playertype, sortType: $sortType) {
      batting {
        name
        runs
        inningsPlayed
        strikeRate
        centuries
        fifties
        fours
        sixes
        avg
        matchesPlayed
      }
      bowling {
        name
        inningsPlayed
        matchesPlayed
        wickets
        bestBallInInnings
        economy
        avg
        strikeRate
        fiveWickets
        runs
      }
    }
  }
`
export const RECORDS_AXIOS = `
  query getRecords($format: String!, $playertype: String!,$sortType: String!){
    getRecords(format: $format, playertype: $playertype,sortType: $sortType){
      batting {
        name
        playerID
        runs
        inningsPlayed
        strikeRate
        centuries
        fifties
        fours
        sixes
        avg
        matchesPlayed
        teamName
      }
      bowling {
        name
        playerID
        inningsPlayed
        matchesPlayed
        wickets
        bestBallInInnings
        economy
        avg
        strikeRate
        fiveWickets
        runs
        overs
        teamName
      }
    }
  }
`

export const STADIUM_LIST = gql`
  query stadiumdicovery {
    getStadiumsDiscovery {
      popularStadiums {
        id
        name
        image
      }
      stadiumByCategory {
        type
        stadiumList {
          id
          name
          image
        }
      }
    }
  }
`

export const STADIUM_LIST_AXIOS = `
query stadiumdicovery{
  getStadiumsDiscovery{
    popularStadiums{
      id
      name
      image
    }
    stadiumByCategory{
      type
      stadiumList{
         id
          name
          image
      }
    }
  }
  }
`

export const STADIUM_DETAILS = `
  query stadium($venueID: String!){
    stadium(venueID: $venueID){
      venueID
      fullName
      capacity
      image
      city
      country
      bowlingEnds
      floodlight
      venueStats {
        odiDetails {
          avgIstInningScore
          firstBattingWin
          firstMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lastMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          highestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lowestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
        }
        testDetails {
          avgIstInningScore
          firstBattingWin
          firstMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lastMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          highestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lowestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
        }
        t20Details {
          avgIstInningScore
          firstBattingWin
          firstMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lastMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          highestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lowestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
        }
      }
    }
  }
`
export const TEAM_DETAILS = gql`
  query team($teamID: String!) {
    team(teamID: $teamID) {
      teamID
      name
      description
      shortName
      squadsArray {
        playerID
        playerName
        playerRole
      }
      odiFormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
      testFormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
      t20FormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
    }
  }
`

export const TEAM_DETAILS_AXIOS = `
  query team($teamID: String!) {
    team(teamID: $teamID) {
      teamID
      name
      description
      shortName
      squadsArray {
        playerID
        playerName
        playerRole
      }
      odiFormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
      testFormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
      t20FormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
    }
  }
`

export const TEAM_SCHEDULE = gql`
  query teamSchedule($teamID: String!, $startDate: String!) {
    teamSchedule(teamID: $teamID, startDate: $startDate) {
      teamId
      teamName
      matches {
        format
        matchID
        otherTeamId
        seriesId
        seriesName
        startDate
        statusStr
        teamName
      }
    }
  }
`

export const TEAM_LIST = gql`
  query teamsdiscovery {
    getTeamsDiscovery {
      popularTeams {
        id
        name
      }
      teamByCategory {
        type
        teamList {
          id
          name
        }
      }
    }
  }
`
export const TEAM_LIST_AXIOS = `
  query teamsdiscovery{
    getTeamsDiscovery{
      popularTeams{
        id
        name
      }
      teamByCategory{
        type
        teamList{
          id
            name
        }
      }
    }
    }
`

export const MATCH_DATA_FOR_SCORECARD_AXIOS = `
  query miniScoreCard($matchID: String!) {
    miniScoreCard(matchID: $matchID) {
      isDisplayDugout
      batting {
        matchID
        playerFeedID
        playerName
        sixes
        fours
        runs
        battingStyle
        zad {
          zadval
          runs
        }
        playerOnStrike
        playerDismissalInfo
      }
      bowling {
        matchID
        playerFeedID
        playerName
        playerTeam
        wickets
        maiden
        RunsConceeded
        overs
        economy
      }
      partnership
      oversRemaining
      reviewDetails {
        teamName
        review
      }
      runRate
      rRunRate
      liveScoreUrl
      data {
        winningTeamID
        loginEnable
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        seriesName
        seriesID
        homeTeamName
        awayTeamName
        toss
        startEndDate
        matchStatus
        matchID
        matchType
        statusMessage
        matchNumber
        venue
        matchResult
        startDate
        playerID
        playerOfTheMatch
        playerofTheMatchTeamShortName
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID
        fourthInningsTeamID
        isCricklyticsAvailable
        isFantasyAvailable
        isLiveCriclyticsAvailable
        isAbandoned
        playing11Status
        probable11Status
        currentDay
        currentSession
        teamsWinProbability {
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
        }
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
      }
    }
  }
`

export const MATCH_DATA_FOR_SCORECARD = gql`
  query miniScoreCard($matchID: String!) {
    miniScoreCard(matchID: $matchID) {
      isDisplayDugout
      batting {
        matchID
        playerFeedID
        playerName
        sixes
        fours
        battingStyle
        zad {
          zadval
          runs
        }
        runs
        playerOnStrike
        playerDismissalInfo
      }
      bowling {
        matchID
        playerFeedID
        playerName
        playerTeam
        wickets
        maiden
        RunsConceeded
        overs
        economy
      }
      partnership
      oversRemaining
      reviewDetails {
        teamName
        review
      }
      runRate
      rRunRate
      liveScoreUrl
      data {
        homeTeamID
        awayTeamID
        league
        loginEnable
        winningTeamID
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        seriesName
        seriesID
        homeTeamName
        awayTeamName
        toss
        startEndDate
        matchStatus
        matchID
        matchType
        statusMessage
        matchNumber
        venue
        matchResult
        startDate
        playerID
        playerOfTheMatch
        playerofTheMatchTeamShortName
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID
        fourthInningsTeamID
        isCricklyticsAvailable
        isFantasyAvailable
        isLiveCriclyticsAvailable
        isAbandoned
        playing11Status
        probable11Status
        currentDay
        currentSession
        teamsWinProbability {
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
        }
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
      }
    }
  }
`

export const FULLSCORECARD = gql`
  query fullScoreCard($matchID: String!) {
    fullScoreCard(matchID: $matchID) {
      matchID
      innings
      inningsScore
      inningsExtras {
        byes
        byes
        no_balls
        penalties
        total_extras
        wides
      }
      inningsTotal
      fow {
        playerName
        order
        over_ball
        runs
      }
      batsmen {
        playerName
        playerDismissalInfo
        playerMatchRuns
        playerMatchBalls
        playerMatchFours
        playerMatchSixes
        playerMatchStrikeRate
      }
      bowlers {
        playerName
        playerOversBowled
        playerMaidensBowled
        playerRunsConceeded
        playerWicketsTaken
        playerEco
      }
    }
  }
`

export const GET_SCORE_CARD = gql`
  query getScoreCard($matchID: String!) {
    getScoreCard(matchID: $matchID) {
      fullScoreCard {
        battingTeamName
        battingTeamShortName
        battingTeamID
        runsScored
        wickets
        overs
        runRate
        batting {
          matchID
          playerID
          playerName
          playerTeam
          playerMatchSixes
          playerMatchFours
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerDismissalInfo
          playerHowOut
          playerMatchStrikeRate
        }
        bowling {
          matchID
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerWides
          playerNoBall
          playerOversBowled
          playerEconomyRate
        }
        extras {
          byes
          legByes
          noBalls
          penalties
          wides
          totalExtras
        }
        fow {
          playerName
          playerID
          order
          over_ball
          runs
        }
        total {
          overs
          runsScored
          wickets
          runRate
        }
      }
    }
  }
`

export const GET_SCORE_CARD_AXIOS = `
  query getScoreCard($matchID: String!){
    getScoreCard(matchID: $matchID){
      fullScoreCard{
        battingTeamName
        battingTeamShortName
        battingTeamID
        runsScored
        wickets
        overs
        runRate
        batting{
          matchID
          playerID
          playerName
          playerTeam
          playerMatchSixes
          playerMatchFours
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerDismissalInfo
          playerHowOut
          playerMatchStrikeRate
        }
        bowling{
          matchID
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerWides
          playerNoBall
          playerOversBowled
          playerEconomyRate
        }
        extras{
          byes
          legByes
          noBalls
          penalties
          wides
          totalExtras
        }
        fow{
          playerName
          playerID
          order
          over_ball
          runs
        }
        total{
          overs
          runsScored
          wickets
          runRate
        }
      }

    }
  }
`

export const MINI_SCORE_CARD = gql`
  query miniScoreCard($matchID: String!) {
    miniScoreCard(matchID: $matchID) {
      batting {
        matchID
        playerFeedID
        playerName
        playerTeam
        sixes
        battingStyle
        zad {
          zadval
          runs
        }
        fours
        runs
        playerOnStrike
        playerMatchBalls
        playerMatchStrikeRate
        playerDismissalInfo
      }
      bowling {
        matchID
        playerFeedID
        playerName
        playerTeam
        wickets
        maiden
        RunsConceeded
        overs
        economy
      }
      partnership
      runRate
      rRunRate
      liveScoreUrl
      reviewDetails {
        teamName
        review
      }
      oversRemaining
      data {
        seriesID
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        seriesName
        homeTeamName
        awayTeamName
        toss
        startEndDate
        matchStatus
        matchID
        matchType
        statusMessage
        matchNumber
        playing11Status
        probable11Status
        venue
        matchResult
        probable11Status
        startDate
        playerOfTheMatch
        playerID
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID
        fourthInningsTeamID
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
        teamsWinProbability {
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
        }
        isCricklyticsAvailable
        isFantasyAvailable
      }
    }
  }
`

export const LOGIN_OTP_MUTATION = gql`
  mutation login($type: String!, $number: String, $email: String) {
    login(type: $type, number: $number, email: $email) {
      code
      token
      message
      username
    }
  }
`

export const UPDATE_PROFILE = gql`
  mutation updateProfileInfo($username: String!, $token: String) {
    updateProfileInfo(username: $username, token: $token) {
      code
      message
    }
  }
`

export const MERGE_PROFILE = gql`
  mutation mergeProfiles($token: String) {
    mergeProfiles(token: $token) {
      code
      message
    }
  }
`

export const MERGE_PROFILE_AXIOS = `
  mutation mergeProfiles( $token: String) {
    mergeProfiles( token: $token) {
      code
      message
    }
  }
`

export const VERIFY_OTP_MUTATION = gql`
  mutation verifyOtp($number: String, $otp: Int!, $email: String) {
    verifyOtp(number: $number, otp: $otp, email: $email) {
      code
      token
      message
      username
    }
  }
`

export const GET_MATCH_INFO_AXIOS = `
  query getMatchInfo( $matchID:String! ) {
    getMatchInfo(matchID: $matchID) {
      seriesName
      homeTeamName
      awayTeamName
      homeTeamID
      awayTeamID
      homeTeamShortName
      awayTeamShortName
      date
      matchOrder
      toss
      venue
      venueID
      umpires
      thirdUmpire
      matchReferee
      homePlayingXI{
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
      }
      awayPlayingXI{
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
      }
    }
  }
`

export const GET_MATCH_INFO = gql`
  query getMatchInfo($matchID: String!) {
    getMatchInfo(matchID: $matchID) {
      seriesName
      homeTeamName
      awayTeamName
      homeTeamID
      awayTeamID
      homeTeamShortName
      awayTeamShortName
      date
      matchOrder
      toss
      venue
      venueID
      umpires
      thirdUmpire
      matchReferee
      homePlayingXI {
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
      }
      awayPlayingXI {
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
      }
    }
  }
`

export const SERIES_DETAILS_OLD = gql`
  query singleSeriesListing($seriesID: String!) {
    singleSeriesListing(seriesID: $seriesID) {
      matches {
        matchOrder
        matchID
        matchType
        tour_name
        venueCity
        homeShortName
        awayShortName
        homeFlag
        matchStatus
        awayFlag
        matchDateTime
        result
        toss
        description
        matchScore {
          inning
          battingTeam
          overs
          runs_scored
          wickets
        }
        playerOfThematch {
          playerName
          playerImage
        }
        prediction {
          teamA
          teamB
          teamAPercentage
          teamBpercentage
          tiePercent
          summary
        }
      }
    }
  }
`

export const BALL_BY_BALL = gql`
  query getBallByBall(
    $matchID: String
    $day: Int
    $session: Int
    $matchType: String
    $innings: String
  ) {
    getBallByBall(
      matchID: $matchID
      day: $day
      session: $session
      matchType: $matchType
      innings: $innings
    ) {
      overs {
        over
        innings
        balls {
          type
          over
          commentary
          runs
          wicket
          zad
          isLastBall
          isOverLastBall
          batsmanName
          bowlerName
          innings
          overNumber
          teamID
          teamName
          ballNumber
          summary {
            score
            over
            runs
            wickets
            teamShortName
            batsmen {
              batsmanName
              onStrike
              runs
              balls
              fours
              sixes
            }
            bowler {
              bowlerName
              overs
              maidens
              runs
              wickets
            }
          }
          inningsdata {
            score {
              overs
              runsScored
              wickets
              runRate
              battingTeamName
              battingTeamID
            }
            battingList {
              playerID
              playerName
              playerTeam
              playerMatchBalls
              playerMatchRuns
              playerBattingNumber
            }
            bowlingList {
              playerID
              playerName
              playerTeam
              playerDotBalls
              playerWicketsTaken
              playerMaidensBowled
              playerRunsConceeded
            }
          }
        }
        onStrikeBatsmanName
        onStrikeBatsmanStats
        nonStrikeBatsmanName
        nonStrikeBatsmanStats
        bowlerName
        bowlerStats
        teamName
        summary {
          score
          over
          runs
          wickets
          teamShortName
          batsmen {
            batsmanName
            onStrike
            runs
            balls
            fours
            sixes
          }
          bowler {
            bowlerName
            overs
            maidens
            runs
            wickets
          }
        }
        teamShortName
        isLastBall
      }
      inningsSummary {
        score {
          overs
          runsScored
          wickets
          runRate
          battingTeamName
          battingTeamID
        }
        battingList {
          playerID
          playerName
          playerTeam
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
        }
        bowlingList {
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
        }
      }
    }
  }
`

export const OVER_BY_OVER = gql`
  query overByOver(
    $matchID: String
    $day: Int
    $session: Int
    $matchType: String
    $innings: String
  ) {
    overByOver(
      matchID: $matchID
      day: $day
      session: $session
      matchType: $matchType
      innings: $innings
    ) {
      inningsSummary {
        score {
          overs
          runsScored
          wickets
          runRate
          battingTeamName
          battingTeamID
        }
        battingList {
          playerID
          playerName
          playerTeam
          playerMatchBalls
          playerMatchRuns
          playerMatchRuns
          playerBattingNumber
        }
        bowlingList {
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
        }
      }
      oversdata {
        overNumber
        balls {
          type
          over
          commentary
          runs
          wicket
          zad
          isLastBall
          isOverLastBall
          batsmanName
          bowlerName
          innings
          overNumber
          teamID
          teamName
          ballNumber
          summary {
            score
            over
            runs
            wickets
            teamShortName
            batsmen {
              batsmanName
              onStrike
              runs
              balls
              fours
              sixes
            }
            bowler {
              bowlerName
              overs
              maidens
              runs
              wickets
            }
          }
          inningsdata {
            score {
              overs
              runsScored
              wickets
              runRate
              battingTeamName
              battingTeamID
            }
            battingList {
              playerID
              playerName
              playerTeam
              playerMatchBalls
              playerMatchRuns
              playerBattingNumber
            }
            bowlingList {
              playerID
              playerName
              playerTeam
              playerDotBalls
              playerWicketsTaken
              playerMaidensBowled
              playerRunsConceeded
            }
          }
        }
      }
    }
  }
`

export const PLAYER_DETAILS = gql`
  query getPlayerProfile($playerID: String!) {
    playersDetails(playerID: $playerID) {
      name
      birthPlace
      dob
      playerIPLBid
      playerIPLTeam
      description
      battingStyle
      bowlingStyle
      international {
        Test {
          basicdetails {
            matches
            innings
          }
          debutMatch {
            matchID
            date
            against
            againstID
          }
          lastMatch {
            matchID
            date
            against
            againstID
          }
          battingRecord {
            notOuts
            runs
            ballsFaced
            hundreds
            fifties
            fours
            sixes
            innings
            average
            strikeRate
          }
          bowlingRecord {
            overs
            wickets
            ballsBowled
            innings
            average
            strikeRate
            economyRate
            fiveWicketHauls
            strikeRate
          }
        }
        Odi {
          basicdetails {
            matches
            innings
          }
          debutMatch {
            matchID
            date
            against
            againstID
          }
          lastMatch {
            matchID
            date
            against
            againstID
          }
          battingRecord {
            notOuts
            runs
            ballsFaced
            hundreds
            fifties
            fours
            sixes
            average
            innings
            strikeRate
          }
          bowlingRecord {
            overs
            wickets
            ballsBowled
            average
            strikeRate
            economyRate
            fiveWicketHauls
            strikeRate
            innings
          }
        }
        T20 {
          basicdetails {
            matches
            innings
          }
          debutMatch {
            matchID
            date
            against
            againstID
          }
          lastMatch {
            matchID
            date
            against
            againstID
          }
          battingRecord {
            notOuts
            runs
            ballsFaced
            hundreds
            fifties
            fours
            sixes
            average
            innings
            strikeRate
          }
          bowlingRecord {
            overs
            wickets
            ballsBowled
            average
            strikeRate
            economyRate
            fiveWicketHauls
            innings
            strikeRate
          }
        }
      }
      Ipl {
        basicdetails {
          matches
          innings
        }
        debutMatch {
          matchID
          date
          against
          againstID
        }
        lastMatch {
          matchID
          date
          against
          againstID
        }
        battingRecord {
          notOuts
          runs
          ballsFaced
          hundreds
          fifties
          fours
          sixes
          average
          strikeRate
        }
        bowlingRecord {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          fiveWicketHauls
          economyRate
        }
      }
    }
  }
`

export const PLAYER_DETAILS_AXIOS = `
query getPlayerProfile($playerID: String!) {
  playersDetails(playerID: $playerID){
    name
    birthPlace
    dob
    playerIPLBid
    playerIPLTeam
    fullName
    description
    battingStyle
    bowlingStyle
    international{
      Test{
        basicdetails{
          matches
          innings
        }
        debutMatch{
          matchID
          date
          against
          againstID
        }
        lastMatch{
          matchID
          date
          against
          againstID
        }
        battingRecord{
          notOuts
          runs
          ballsFaced
          hundreds
          fifties
          fours
          sixes
          innings
          average
          strikeRate
        }
        bowlingRecord{
          overs
          wickets
          ballsBowled
          innings
          average
          strikeRate
          economyRate
          fiveWicketHauls
          strikeRate
        }
      }
      Odi{
        basicdetails{
          matches
          innings
        }
        debutMatch{
          matchID
          date
          against
          againstID
        }
        lastMatch{
          matchID
          date
          against
          againstID
        }
        battingRecord{
          notOuts
          runs
          ballsFaced
          hundreds
          fifties
          fours
          sixes
          average
          innings
          strikeRate
        }
        bowlingRecord{
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          strikeRate
          innings
        }
      }
      T20{
        basicdetails{
          matches
          innings
        }
        debutMatch{
          matchID
          date
          against
          againstID
        }
        lastMatch{
          matchID
          date
          against
          againstID
        }
        battingRecord{
          notOuts
          runs
          ballsFaced
          hundreds
          fifties
          fours
          sixes
          average
          innings
          strikeRate
        }
        bowlingRecord{
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          strikeRate
        }
      }
    }
    Ipl{
        basicdetails{
          matches
          innings
        }
        debutMatch{
          matchID
          date
          against
          againstID
        }
        lastMatch{
          matchID
          date
          against
          againstID
        }
        battingRecord{
          notOuts
          runs
          ballsFaced
          hundreds
          fifties
          fours
          sixes
          average
          strikeRate
        }
        bowlingRecord{
          overs
          wickets
          ballsBowled
          average
          strikeRate
          fiveWicketHauls
          economyRate
        }
      }
  }

}
`

export const LIVE_CRICLYTICS = gql`
  query cricliticsScoreCard($matchID: String!) {
    cricliticsScoreCard(matchID: $matchID) {
      lasttenball {
        isBall
        isWicket
        runs
      }
      scoreCard {
        batting {
          playerID
          playerName
          playerTeam
          playerMatchSixes
          playerMatchFours
          playerMatchBalls
          playerMatchRuns
          playerOnStrike
          playerBattingNumber
          playerDismissalInfo
          playerHowOut
          playerMatchStrikeRate
        }
        bowling {
          playerID
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerName
          playerRunsConceeded
          playerWides
          playerNoBall
          playerOversBowled
          playerEconomyRate
        }
      }
    }
  }
`

export const AUTHOR_DATA = gql`
  query getAuthorsArticles(
    $authorID: String!
    $page: Int
    $type: String!
    $fromDate: String
  ) {
    getAuthorsArticles(
      authorID: $authorID
      page: $page
      type: $type
      fromDate: $fromDate
    ) {
      author {
        userName

        userID
        twtID
        bio
        pic
      }
      articles {
        articleID
        matchIDs {
          matchID
          matchSlug
        }
        title
        description
        author
        type
        sm_image_safari
        sm_image
        bg_image
        featureThumbnail
        bg_image_safari
        content
        updatedAt
        createdAt
      }
    }
  }
`

export const AUTHOR_DATA2 = ` query getAuthorsArticles($authorID:String!,$page:Int){
  getAuthorsArticles(authorID:$authorID,page:$page){
  author{
    userName

    userID
    twtID
    bio
    pic
  }
  articles{
    articleID
    title
    author
    type
    sm_image_safari
    sm_image
    bg_image
    featureThumbnail
    bg_image_safari
    content
    updatedAt
  }
}
}`

export const SERIES_NAME_FOR_ARTICLES = gql`
  query {
    getSeriesListForArticles {
      seriesListWithIds {
        tourID
        tourName
        articlesFlag
        videosFlag
      }
    }
  }
`

export const SERIES_NAME_FOR_ARTICLES_AXIOS = `
  query {
    getSeriesListForArticles {
      seriesListWithIds {
        tourID
        tourName
        articlesFlag
        videosFlag
      }
    }
  }
`

export const ARTICLE_LIST = gql`
  query getArticles($type: String!, $page: Int!, $seriesID: String) {
    getArticles(type: $type, page: $page, seriesID: $seriesID) {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`

export const ARTICLE_LIST_AXIOS = `
  query getArticles($type: String!, $page: Int!){
    getArticles(type: $type, page: $page){
      articleID
   matchIDs{
     matchID
     matchSlug
   }
   seriesIDs{
     name
     id
   }
   teamIDs{
     name
     id
   }
   playerIDs{
     name
     id
   }
   approvalStatus
   status
   title
   slugTitle
   bg_image
   sm_image
   bg_image_safari
   sm_image_safari
   thumbnail
   featureThumbnail
   description
   tags{
     name
     type
     tag
     id
   }
   type
   filters
   createdAt
   updatedAt
   author
   publishedAt
   seoTags
    }
  }
`
// export const ARTICLE_LIST = gql`
//   query getArticles($type: String!, $page: Int!) {
//     getArticles(type: $type, page: $page) {
//       articleID
//       matchIDs {
//         matchID
//         matchSlug
//       }
//       seriesIDs {
//         name
//         id
//       }
//       teamIDs {
//         name
//         id
//       }
//       playerIDs {
//         name
//         id
//       }
//       approvalStatus
//       status
//       title
//       slugTitle
//       bg_image
//       sm_image
//       bg_image_safari
//       sm_image_safari
//       thumbnail
//       featureThumbnail
//       description
//       tags {
//         name
//         type
//         tag
//         id
//       }
//       type
//       filters
//       createdAt
//       updatedAt
//       author
//       publishedAt
//       seoTags
//     }
//   }
// `

export const GET_VIDEOS_BY_CATEGORIES_AXIOS = `
query getVideosByCategories($type : String!, $page: Int, $seriesID : String){
  getVideosByCategories(type : $type, page : $page, seriesID: $seriesID)
  {

    videoID
    title
    description
    videoUrl
    videoYTId
    createdAt
    authors

  }
}`
export const GET_VIDEOS_BY_CATEGORIES = gql`
  query getVideosByCategories($type: String!, $page: Int, $seriesID: String) {
    getVideosByCategories(type: $type, page: $page, seriesID: $seriesID) {
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
      authors
    }
  }
`

export const GET_VIDEOS_AXIOS = `
query videos{
  getVideos{
    Fantasy{
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
      authors
    }
      Latest{
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
      authors
    }
    MatchRelated{
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
      authors
    }
    News{
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
      authors
    }
  }
 }
`
export const GET_VIDEOS = gql`
  query videos {
    getVideos {
      Fantasy {
        videoID
        title
        description
        videoUrl
        videoYTId
        createdAt
      }
      Latest {
        videoID
        title
        description
        videoUrl
        videoYTId
        createdAt
      }
      MatchRelated {
        videoID
        title
        description
        videoUrl
        videoYTId
        createdAt
      }
      News {
        videoID
        title
        description
        videoUrl
        videoYTId
        createdAt
      }
    }
  }
`

export const GET_VIDEO_BY_ID = gql`
  query getvideobyid($videoID: String!) {
    getVideoByvideoID(videoID: $videoID) {
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
      relatedVideos {
        videoID
        title
        description
        videoUrl
        videoYTId
        createdAt
      }
    }
  }
`

export const GET_VIDEO_BY_ID_AXIOS = `
query getvideobyid($videoID:String!){
  getVideoByvideoID(videoID:$videoID){
    videoID
    title
    description
    videoUrl
    videoYTId
    createdAt
    relatedVideos{
      videoID
      title
      description
      videoUrl
      videoYTId
      createdAt
    }
  }
 }
 `

export const GET_ARTICLE_DETAILS = `
  query articleDetail($articleID: String!){
    getArticlesDetails(articleID: $articleID){
      articleID
       type
       matchIDs{
         matchID
         matchSlug
       }
       seriesIDs{
         name
         id
       }
       teamIDs{
         name
         id
       }
       playerIDs{
         name
         id
       }
       title
       content
       bg_image
       sm_image
       bg_image_safari
       sm_image_safari
       thumbnail
       featureThumbnail
       seoTags
       description
       tags{
         name
         type
         tag
         leagueType
         id
       }
       authors
       newAuthors{
        id
        name
      }
       
       publishedAt
       updatedAt
       createdAt
       relatedArticles{
        thumbnail
        sm_image
        bg_image
        sm_image_safari
        title
        articleID
        description
        author
        
        
        createdAt
        type
        slugTitle
      }
    }
  }
`

export const DATA_DIGEST = gql`
  query getTopTenDataDigest {
    getTopTenDataDigest {
      img
      _id
    }
  }
`

export const LIVE_SCORE_PREDICTOR = gql`
  query liveScorePredictor($matchId: String!, $matchType: String) {
    liveScorePredictor(matchId: $matchId, matchType: $matchType) {
      matchId
      matchNumber
      liveScores {
        inningNo
        overNo
        currentOvers
        currentScore
        currentWickets
        predictedScore
        predictedOver
        predictedWicket
        predictvizMarginView {
          innings
          result
          runs
          winnerTeamId
          wickets
        }
        winvizView {
          battingTeamPercent
          bowlingTeamPercent
          drawPercent
          tiePercent
        }
        currentView {
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
        }
        secondPredictedScore
        secondPredictedOVer
        secondPredictedWicket
        thirdPredictedScore
        thirdPredictedOver
        thirdPredictedWicket
        fourthPredictedScore
        fourthPredictedOver
        fourthPredictedWicket
        inningIds
        team1Id
        team2Id
        team1ShortName
        team2ShortName
        homeTeamPercentage
        homeTeamShortName
        awayTeamPercentage
        awayTeamShortName
        tiePercentage
        projected_result
      }
    }
  }
`

export const LIVE_SCORE_PREDICTOR_AXIOS = `
  query liveScorePredictor($matchId: String!, $matchType: String){
    liveScorePredictor(matchId: $matchId,matchType:$matchType){
      matchId
      liveScores{
        inningNo
        overNo
        currentOvers
        currentScore
        currentWickets
        predictedScore
        predictedOver
        predictedWicket
        predictvizMarginView{
          innings
          result
          runs
          winnerTeamId
          wickets
        }
        winvizView{
          battingTeamPercent
          bowlingTeamPercent
          drawPercent
          tiePercent
        }
        secondPredictedScore
        secondPredictedOVer
        secondPredictedWicket
        thirdPredictedScore
        thirdPredictedOver
        thirdPredictedWicket
        fourthPredictedScore
        fourthPredictedOver
        fourthPredictedWicket
        inningIds
        team1Id
        team2Id
        team1ShortName
        team2ShortName
        homeTeamPercentage
        homeTeamShortName
        awayTeamPercentage
        awayTeamShortName
        tiePercentage
        projected_result

      }
    }
  }
`

export const GET_PLAYER_ONE_DATA = gql`
  query getPlayerOneData($crictecMatchId: String) {
    getPlayerOneData(crictecMatchId: $crictecMatchId) {
      crictecMatchId
      matchUpData {
        player1
        player2
        player1Team
        player2Team
        player1Role
        player2Role
        ballsFaced
        runsScored
        battingSR
        bowlingSR
        status
      }
    }
  }
`

export const GET_PLAYER_MATCHUPS = gql`
  query getplayermatchups($crictecMatchId: String, $playerId: String) {
    getPlayermatchups(crictecMatchId: $crictecMatchId, playerId: $playerId) {
      crictecMatchId
      matchUpData {
        player1
        player2
        player1Team
        player2Team
        player1Role
        player2Role
        ballsFaced
        runsScored
        battingSR
        bowlingSR
        status
        Dismissals
      }
    }
  }
`

export const PLAYER_LIST_OBJECT = gql`
  query getplayerdiscovery {
    getPlayersDiscovery {
      popularPlayer {
        id
        name
      }
      playerByCategory {
        type
        playerList {
          id
          name
        }
      }
    }
  }
`

export const PLAYER_LIST_OBJECT_AXIOS = `
query{
  getPlayersDiscovery{
    popularPlayer{
      id
      name
      shortname
    }
    playerByCategory{
      type
      playerList{
        id
        name
        shortname
      }
    }
  }
}`
export const GET_FAN_POLL = gql`
  query getFanPolls {
    getFanPolls {
      pollID
      question
      matchID
      totalVotes
      options {
        percentage
        name
      }
    }
  }
`

export const UPDATE_FAN_POLL = gql`
  mutation updateFanPoll($id: String!, $option: String!) {
    updateFanPoll(id: $id, option: $option) {
      pollID
      question
      matchID
      totalVotes
      options {
        percentage
        name
      }
    }
  }
`

const addFantasyTeamPlayers = gql`
  input addFantasyTeamPlayers {
    credits: Float
    playerCredits: String
    playerId: String
    playerFeedID: String
    playerName: String
    selectionPercent: String
    teamName: String
    playerRole: String
    captain: Boolean
    vice_captain: Boolean
    projectedPoints: String
    playerClubName: String
    ActualPoint: String
    playerCaptain: Boolean
    playerViceCaptain: Boolean
  }
`

const fanFightTeams = gql`
  input fanFightTeams {
    players: [addFantasyTeamPlayers]
  }
`

const fantasyTeamsForFanfight = gql`
  input fantasyTeamsForFanfight {
    matchID: String
    uniqueCode: String
    teams: [fanFightTeams]
  }
`

export const EXPORT_TEAM_FANFIGHT = gql`
  mutation addFantasyMyTeams(
    $matchID: String!
    $fantasyTeamsForFanfight: fantasyTeamsForFanfight
  ) {
    addFantasyMyTeams(
      matchID: $matchID
      fantasyTeamsForFanfight: $fantasyTeamsForFanfight
    )
  }
`

export const MATCH_SUMMARY_AXIOS = `
	query matchSummary($matchID: String!, $status: String!) {
		matchSummary(matchID: $matchID, status: $status) {
			bestBatsman {
				playerID
				playerName
				playerTeam
				playerTeamID
				totalBattingPoints
				battingStatsList {
					runs
					fours
					sixes
					balls
          strikeRate
          isNotOut
				}
				totalBowlingPoints
				totalFieldingPoints
				bowlingStatsList {
					wickets
					runsConceded
					economyRate
					overs
				}
				fieldingStatsList {
					runsOuts
					catches
					stumps
				}
			}

			bestBowler {
				playerID
				playerName
				playerTeam
				playerTeamID
				totalBattingPoints
				battingStatsList {
					runs
					fours
					sixes
					balls
          strikeRate
          isNotOut
				}
				totalBowlingPoints
				totalFieldingPoints
				bowlingStatsList {
					wickets
					runsConceded
					economyRate
					overs
				}
				fieldingStatsList {
					runsOuts
					catches
					stumps
				}
			}

			homeTeamData {
				overs
				runs
				wickets
				teamName
				teamShortName
				teamID
				batsmanSummary1 {
					topBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}
				batsmanSummary2 {
					topBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}

				bowlerSummary1 {
					topBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}
				bowlerSummary2 {
					topBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}
			}
			awayTeamData {
				overs
				runs
				wickets
				teamName
				teamShortName
				teamID
				batsmanSummary1 {
					topBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}
				batsmanSummary2 {
					topBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBatsman {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
              strikeRate
              isNotOut
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}

				bowlerSummary1 {
					topBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}
				bowlerSummary2 {
					topBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
					runnerBowler {
						playerID
						playerName
						playerTeam
						totalBattingPoints
						totalPoints
						battingStatsList {
							runs
							fours
							sixes
							balls
							strikeRate
						}
						totalBowlingPoints
						totalFieldingPoints
						bowlingStatsList {
							wickets
							runsConceded
							economyRate
							overs
						}
						fieldingStatsList {
							runsOuts
							catches
							stumps
						}
					}
				}
			}
			inningOrder
		}
	}
`
export const MATCH_SUMMARY = gql`
  query matchSummary($matchID: String!, $status: String!) {
    matchSummary(matchID: $matchID, status: $status) {
      bestBatsman {
        playerID
        playerName
        playerTeam
        playerTeamID
        totalBattingPoints
        battingStatsList {
          runs
          fours
          sixes
          balls
          strikeRate
          isNotOut
        }
        totalBowlingPoints
        totalFieldingPoints
        bowlingStatsList {
          wickets
          runsConceded
          economyRate
          overs
        }
        fieldingStatsList {
          runsOuts
          catches
          stumps
        }
      }

      bestBowler {
        playerID
        playerName
        playerTeam
        playerTeamID
        totalBattingPoints
        battingStatsList {
          runs
          fours
          sixes
          balls
          strikeRate
          isNotOut
        }
        totalBowlingPoints
        totalFieldingPoints
        bowlingStatsList {
          wickets
          runsConceded
          economyRate
          overs
        }
        fieldingStatsList {
          runsOuts
          catches
          stumps
        }
      }

      homeTeamData {
        overs
        runs
        wickets
        teamName
        teamShortName
        teamID
        batsmanSummary1 {
          topBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }
        batsmanSummary2 {
          topBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }

        bowlerSummary1 {
          topBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }
        bowlerSummary2 {
          topBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }
      }
      awayTeamData {
        overs
        runs
        wickets
        teamName
        teamShortName
        teamID
        batsmanSummary1 {
          topBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }
        batsmanSummary2 {
          topBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBatsman {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
              isNotOut
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }

        bowlerSummary1 {
          topBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }
        bowlerSummary2 {
          topBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
          runnerBowler {
            playerID
            playerName
            playerTeam
            totalBattingPoints
            totalPoints
            battingStatsList {
              runs
              fours
              sixes
              balls
              strikeRate
            }
            totalBowlingPoints
            totalFieldingPoints
            bowlingStatsList {
              wickets
              runsConceded
              economyRate
              overs
            }
            fieldingStatsList {
              runsOuts
              catches
              stumps
            }
          }
        }
      }
      inningOrder
    }
  }
`

export const LIVE_PLAYER_PROJECTION = gql`
  query getLivePlayerProjectionDetails($matchId: String!) {
    getLivePlayerProjectionDetails(matchId: $matchId) {
      superOver
      bowler {
        matchID
        strikerId
        nonStrikerId
        bowlerId
        bowlerRuns
        wicketsTakenTillNow
        maxOvers
        oversBowledSoFar
        strikerPosition
        nonStrikerPosition
        inningNo
        format
        projected
        bounds
        bowlerName
        teamName
      }
      batsman {
        playerRuns
        playerTeamName
        playerId
        playerName
        playerBalls
        overNo
        inningNo
        playerBattingProbabilities {
          playerId
          playerData {
            probabilities
            bound
            red
          }
          teamId
        }
      }
    }
  }
`
export const PHASES_OF_INNIGNS = gql`
  query phaseOfInnings($matchID: String) {
    phaseOfInnings(matchID: $matchID) {
      inningsPhase {
        overNumber
        runs
        wickets
      }
      scoreCard {
        inning
        inningNumber
        battingTeam
        runsScored
        wickets
        overs
        runRate
        battingSide
        teamID
        battingTeamShortName
        declared
        folowOn
      }
      liveScores {
        inningNo
        projected_result
        overNo
        team2Id
        team2ShortName
        currentScore
        predictedScore
        predictedOver
        predictvizMarginView {
          innings
          result
          runs
          winnerTeamId
          wickets
        }
        winvizView {
          battingTeamPercent
          bowlingTeamPercent
          drawPercent
          tiePercent
        }
        secondPredictedScore
        secondPredictedOVer
        thirdPredictedScore
        thirdPredictedOver
        fourthPredictedScore
        fourthPredictedOver
        predictedWicket
        secondPredictedWicket
        thirdPredictedWicket
        fourthPredictedWicket
      }
    }
  }
`

export const GET_PARTNERSHIP_BY_MATCH_ID = gql`
  query getpartnerShipPrediction($matchID: String!) {
    getpartnerShipPrediction(matchID: $matchID) {
      partnerShipData {
        player1Name
        player2Name
        player1Runs
        player2Runs
        player2Percentage
        player1Percentage
        totalBalls
        totalRuns
      }
      matchID
      prediction
      currentRuns
      currentWickets
    }
  }
`

export const GET_PARTNERSHIP_BY_MATCH_ID_AXIOS = `
query getpartnerShipPrediction($matchID:String!){
  getpartnerShipPrediction(matchID:$matchID) { 
   partnerShipData{
      player1Name
      player2Name
      player1Runs
      player2Runs
      player1Percentage
      player2Percentage
      totalBalls
      totalRuns
    }
    matchID
    prediction
    currentRuns
    currentWickets
    currentOver
    teamID
    teamShortName
    

    }}
`

export const GET_KEY_MATCH_UPS_PHASE_STATS = gql`
  query getKeyStats($matchID: String!) {
    getPhaseOfInningsResolver(matchID: $matchID) {
      inningsPhase {
        _id
        teamID
        teamShortName
        totalRuns
        totalWickets
        data {
          runs
          wickets
          innings
          teamID
          teamShortName
          overNumber
        }
      }
      format
    }
    gameChangingOvers(matchID: $matchID) {
      matchID
      overs {
        inningNo
        overNo
        runsInOver
        score
        wicketsInOver
        teamShortName
        teamID
        commentary {
          over
          overNumber
          runs
          wicket
          type
          comment
          batsmanName
          bowlerName
        }
        commentaryOver {
          over
          overNumber
          runs
          wicket
          type
        }
        lastBallPredictionData {
          homeTeamShortName
          homeTeamPercent
          awayTeamShortName
          awayTeamPercent
          tiePercent
          ballNo
          currentWickets
          currentScore
          score
        }
      }
    }
    matchupsById(crictecMatchId: $matchID) {
      crictecMatchId
      matchUpData {
        player1
        player2
        player1Team
        player2Team
        player1Role
        player2Role
        ballsFaced
        runsScored
        player1Name
        player2Name
        battingSR
        bowlingSR
        status
        Dismissals
      }
    }
    getKeyStats(matchID: $matchID) {
      head2headStats {
        venueStatsData {
          firstBattingWinPercent
          avgFirstInningScore
          venueName
          overall
          bestSuited
          paceWicketPercent
          spinWicketPercent
          highestScoreChased
        }
        head2Head {
          totalMatches
          noResult
          teamA
          teamB
        }
        teamA {
          key
          lastFStatus
          teamName
          teamShortName
          color
        }
        teamB {
          key
          lastFStatus
          teamName
          teamShortName
          color
        }
      }
      topRunScorer1 {
        teamID
        fullName
        shortName
        playerID
        playerName
        playerRuns
        playerWicket
        average
      }
      topRunScorer2 {
        teamID
        fullName
        shortName
        playerID
        playerName
        playerRuns
        playerWicket
        average
      }
      topWicketTaker1 {
        teamID
        fullName
        shortName
        playerID
        playerName
        totalWickets
        average
      }
      topWicketTaker2 {
        teamID
        fullName
        shortName
        playerID
        playerName
        totalWickets
        average
      }
    }
    getmatchStats(matchID: $matchID) {
      matchID
      awayTeamID
      homeTeamID
      format
      matchStatsArray {
        teamId
        shortName
        fullName
        highestBattingScore {
          value
          percent
        }
        totalFours {
          value
          percent
        }
        totalSix {
          value
          percent
        }
        runsScoredInBoundaries {
          value
          percent
        }
        highestPartnership {
          value
          percent
        }
        highestPartnership {
          value
          percent
        }
        totalDotBalls {
          value
          percent
        }
        runRate {
          value
          percent
        }
        runRateInPowerplay1_6 {
          value
          percent
        }
        runRateInPowerplay1_10 {
          value
          percent
        }
        runRateInPowerplay11_40 {
          value
          percent
        }
        runRateInPowerplay41_50 {
          value
          percent
        }
        runRateDeathOver {
          value
          percent
        }

        highestWickets {
          value
          percent
        }
        extras {
          value
          percent
        }
      }
    }
  }
`

export const GET_KEY_STATS = gql`
  query getKeyStats($matchID: String!) {
    getKeyStats(matchID: $matchID) {
      head2headStats {
        venueStatsData {
          firstBattingWinPercent
          avgFirstInningScore
          venueID
          venueName
          paceWicketPercent
          spinWicketPercent
          highestScoreChased
          overall
          bestSuited
        }
        head2Head {
          totalMatches
          noResult
          teamA
          teamB
        }
        teamA {
          key
          lastFStatus
          teamName
          teamShortName
          color
        }
        teamB {
          key
          lastFStatus
          teamName
          teamShortName
          color
        }
      }
      topRunScorer1 {
        teamID
        fullName
        shortName
        playerID
        playerName
        playerRuns
        playerWicket
        average
      }
      topRunScorer2 {
        teamID
        fullName
        shortName
        playerID
        playerName
        playerRuns
        playerWicket
        average
      }
      topWicketTaker1 {
        teamID
        fullName
        shortName
        playerID
        playerName
        totalWickets
        average
      }
      topWicketTaker2 {
        teamID
        fullName
        shortName
        playerID
        playerName
        totalWickets
        average
      }
    }
  }
`

export const OVER_SAPERATOR = gql`
  query overSaperator(
    $matchID: String!
    $innings: String!
    $overNumber: String!
  ) {
    overSaperator(
      matchID: $matchID
      innings: $innings
      overNumber: $overNumber
    ) {
      score
      over
      runs
      wickets
      teamShortName
      teamShortName
      batsmen {
        batsmanName
        onStrike
        runs
        balls
        fours
        sixes
      }
      bowler {
        bowlerName
        overs
        maidens
        runs
        wickets
      }
    }
  }
`

export const PHASES_OF_SESSIONS = gql`
  query phaseOfSessions($matchID: String!, $day: Int!) {
    phaseOfSessions(matchID: $matchID, day: $day) {
      phaseOfSession {
        session
        totalRuns
        totalOvers
        totalWickets
        overs {
          over
          runs
          wicket
          teamId
        }
      }
      cricPrediction {
        currentScore
        currentOverNo
        currentWicket
        teamId
        teamShortName
        predictedOver
        predectedScore
        predictedWicket
      }
    }
  }
`
export const GET_MATCHUP_BY_ID = gql`
  query matchupsById($crictecMatchId: String) {
    matchupsById(crictecMatchId: $crictecMatchId) {
      crictecMatchId
      matchUpData {
        label
        player1
        player2
        player1Team
        player2Team
        player1Role
        player2Role
        ballsFaced
        runsScored
        player1Name
        player2Name
        player1HindiName
        player2HindiName
        battingSR
        bowlingSR
        status
        Dismissals
      }
    }
  }
`
export const PRE_MATCH_PREDICTION = gql`
  query preMatchPredection($matchId: String) {
    preMatchPredection(matchId: $matchId) {
      maxBatScore
      maxBallScore
      matchId
      status
      team1Id
      team2Id
      playerList {
        playerId
        teamId
        playerRole
        playerName
        shortName
        fullname
        listProjections {
          role
          values {
            probabilities
            bound
          }
        }
      }
      teams_Runs_Projection {
        teamScore
        teamId
        teamName
      }
    }
  }
`

export const UPDATE_ARTICLE_LIKE = gql`
  mutation updateLikes($articleID: String!, $like: Boolean!) {
    updateLikes(articleID: $articleID, like: $like)
  }
`

export const GET_ARTICLE_LIKES = gql`
  query getArticlesLikes($articleID: String!) {
    getArticlesLikes(articleID: $articleID) {
      articlesCount
      articleID
    }
  }
`

export const GET_CRICLYTICS_COMMON_API = gql`
  query getcriclyticsCommonApi($matchId: String, $innings: String) {
    getcriclyticsCommonApi(matchId: $matchId, innings: $innings) {
      matchId
      lasttenball {
        isBall
        isWicket
        runs
        type
        isLastBall
      }
      scoreCard {
        batting {
          matchID
          playerID
          playerName
          playerTeam
          playerMatchSixes
          playerMatchFours
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerDismissalInfo
          playerHowOut
          playerMatchStrikeRate
          playerOnStrike
        }
        bowling {
          matchID
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerWides
          playerNoBall
          playerOversBowled
          playerEconomyRate
        }
      }

      TeamProjections {
        projected_result
        inningNo
        overNo
        currentScore
        currentWickets
        currentOvers
        predictedScore
        predictedOver
        predictedWicket
        predictvizMarginView {
          innings
          result
          runs
          winnerTeamId
        }
        winvizView {
          battingTeamPercent
          bowlingTeamPercent
          drawPercent
          tiePercent
        }
        secondPredictedScore
        secondPredictedOVer
        secondPredictedWicket
        thirdPredictedScore
        thirdPredictedOver
        thirdPredictedWicket
        fourthPredictedScore
        fourthPredictedOver
        fourthPredictedWicket
        inningIds
        team1Id
        team2Id
        team1ShortName
        team2ShortName
      }
      bowler {
        strikerId
        nonStrikerId
        bowlerId
        bowlerId
        bowlerRuns
        bowlerName
        teamName
        wicketsTakenTillNow
        maxOvers
        oversBowledSoFar
        strikerPosition
        nonStrikerPosition
        inningNo
        format
        projected
        bounds
      }
      batsman {
        playerId
        playerRuns
        playerBalls
        playerTeamName
        inningNo
        playerName
        overNo
        format
        playerBattingProbabilities {
          playerId
          playerData {
            probabilities
            bound
          }
          playerBound
        }
      }
    }
  }
`

export const GET_MOMENTUM_SHIFT = gql`
  query getMomentumShift($_id: String!) {
    getMomentumShift(_id: $_id) {
      overHistoryDetails {
        overNo
        inningNo
        diff
        isNeg
        perc
      }
      topFourDiffPoints {
        overNo
        inningNo
        diff
        isNeg
        perc
        summarydetails {
          over
          innings
          balls {
            runs
            type
            wicket
            over
            commentary
          }
          summary {
            score
            wickets
            runs
            over
            teamShortName
            batsmen {
              batsmanName
              onStrike
              fours
              sixes
              balls
              runs
            }
            bowler {
              bowlerName
              overs
              maidens
              runs
              wickets
            }
          }
        }
      }
      matchobj
    }
  }
`

export const GET_ARTICLES_BY_ID_AND_TYPE = gql`
  query getArticlesByIdAndType($type: String!, $Id: String!) {
    getArticlesByIdAndType(type: $type, Id: $Id) {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`

export const GET_ARTICLES_BY_ID_AND_TYPE_AXIOS = `
	query getArticlesByIdAndType($type: String!, $Id: String!) {
		getArticlesByIdAndType(type: $type, Id: $Id) {
			articleID
			matchIDs {
				matchID
				matchSlug
			}
			seriesIDs {
				name
				id
			}
			teamIDs {
				name
				id
			}
			playerIDs {
				name
				id
			}
			approvalStatus
			status
			title
			slugTitle
			content
			bg_image
			sm_image
			bg_image_safari
			sm_image_safari
			thumbnail
			featureThumbnail
			description
			tags {
				name
				type
				tag
				id
			}
			type
			filters
			createdAt
			updatedAt
			author
			publishedAt
			seoTags
		}
	}
`

export const GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS = `
  query getArticlesByIdAndType($type: String!, $Id: String!){
    getScoreCard(matchID: $Id){
      fullScoreCard{
        battingTeamName
        battingTeamShortName
        battingTeamID
        runsScored
        wickets
        overs
		runRate
        batting{
          isCaptain 
          isKeeper
          matchID
          playerID
          playerName
          playerTeam
          playerMatchSixes
          playerMatchFours
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerDismissalInfo
          playerHowOut
          playerMatchStrikeRate
          battingStyle
          zad{
            runs
            zadval
          }
        }
        bowling{
          isCaptain 
          matchID
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerWides
          playerNoBall
          playerOversBowled
          playerEconomyRate
          
        }
        extras{
          byes
          legByes
          noBalls
          penalties
          wides
          totalExtras
        }
        fow{
          isCaptain 
          isKeeper
          playerName
          playerID
          order
          over_ball
          runs
        }
        total{
          overs
          runsScored
          wickets
          runRate
        }
      }

    }
    getMatchInfo(matchID: $Id) {
	  seriesName
	  seriesID
      homeTeamName
      awayTeamName
      homeTeamID
      awayTeamID
      homeTeamShortName
      awayTeamShortName
      date
      matchOrder
      toss
      venue
	  venueID
	  type
      umpires
      thirdUmpire
      matchReferee
      homePlayingXI{
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
        captain
      }
      awayPlayingXI{
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
        captain
      }
    }
    getArticleByPostitions {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
    getArticlesByIdAndType(type: $type, Id: $Id){
      articleID
      matchIDs{
        matchID
        matchSlug
      }
      seriesIDs{
        name
        id
      }
      teamIDs{
        name
        id
      }
      playerIDs{
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags{
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`
export const GET_ARTICLES_MATCH_INFO_SCORECARD = gql`
  query getArticlesByIdAndType($type: String!, $Id: String!) {
    getScoreCard(matchID: $Id) {
      fullScoreCard {
        battingTeamName
        battingTeamShortName
        battingTeamID
        runsScored
        wickets
        overs
        runRate
        batting {
          isCaptain
          isKeeper
          isSubstitutePlayer
          matchID
          playerID
          playerName
          playerTeam
          playerMatchSixes
          playerMatchFours
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerDismissalInfo
          playerHowOut
          playerMatchStrikeRate
          battingStyle
          zad {
            runs
            zadval
          }
        }
        bowling {
          isCaptain
          matchID
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerWides
          playerNoBall
          playerOversBowled
          playerEconomyRate
        }
        extras {
          byes
          legByes
          noBalls
          penalties
          wides
          totalExtras
        }
        fow {
          isCaptain
          isKeeper
          playerName
          playerID
          order
          over_ball
          runs
        }
        total {
          overs
          runsScored
          wickets
          runRate
        }
        impactPlayer
        Substitutes {
          playerInID
          playerInName
          playerOutID
          playerOutName
          teamID
          playerRole
        }
      }
    }
    getMatchInfo(matchID: $Id) {
      seriesName
      seriesID
      homeTeamName
      awayTeamName
      homeTeamID
      awayTeamID
      homeTeamShortName
      awayTeamShortName
      date
      matchOrder
      toss
      venue
      venueID
      type
      umpires
      thirdUmpire
      matchReferee
      homePlayingXI {
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
        captain
      }
      awayPlayingXI {
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
        captain
      }
    }
    getArticleByPostitions {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
    getArticlesByIdAndType(type: $type, Id: $Id) {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`

export const PROBABLE_PLAYING_XI = gql`
  query probablePlaying11($matchID: String!) {
    probablePlaying11(matchID: $matchID) {
      homeTeamPP11 {
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
        captain
        keeper
      }
      awayTeamPP11 {
        playerID
        playerRole
        playerClubName
        playerName
        playerImage
        captain
        keeper
      }
      homeTeamName
      awayTeamName
      homeTeamShortName
      awayTeamShortName
    }
  }
`

export const GET_HIGHLIGHTS_AXIOS = `
  query getHighlights($matchID: String, $innings: String, $type: String, $page: Int){
    getHighlights(matchID: $matchID, innings: $innings, type: $type, page: $page){
      HighlightBall{
        matchID
        ballID
        ballNumber
        innings
        over
        overNumber
        commentary
        teamID
        teamName
        runs
        xCoordinate
        yCoordinate
        zad
        wicket
      }
      totalCount{
        totalFours
        totalSix
        totalWickets
      }
    }
  }
`

export const GET_HIGHLIGHTS = gql`
  query getHighlights(
    $matchID: String
    $innings: String
    $type: String
    $page: Int
  ) {
    getHighlights(
      matchID: $matchID
      innings: $innings
      type: $type
      page: $page
    ) {
      HighlightBall {
        matchID
        ballID
        ballNumber
        innings
        over
        overNumber
        commentary
        teamID
        teamName
        runs
        xCoordinate
        yCoordinate
        zad
        wicket
      }
      totalCount {
        totalFours
        totalSix
        totalWickets
      }
    }
  }
`

export const GET_QUICKBYTES = gql`
  query getQuickbytes($matchID: String!) {
    getQuickbytes(matchID: $matchID)
  }
`

export const PRE_MATCH_CRICLYTICS = gql`
  query preMatchHomePageCriclytics($matchID: String!) {
    preMatchHomePageCriclytics(matchID: $matchID) {
      teamADetails {
        topBatsmen {
          player {
            playerId
            teamId
            playerName
            teamName
            playerRole
            listProjections {
              role
              values {
                probabilities
                bound
              }
            }
          }
        }
        topBowler {
          player {
            playerId
            teamId
            playerName
            teamName
            playerRole
            listProjections {
              role
              values {
                probabilities
                bound
              }
            }
          }
        }
      }
      teamBDetails {
        topBatsmen {
          player {
            playerId
            teamId
            playerName
            teamName
            playerRole
            listProjections {
              role
              values {
                probabilities
                bound
              }
            }
          }
          middleBound
        }
        topBowler {
          player {
            playerId
            teamId
            playerName
            teamName
            listProjections {
              role
              values {
                probabilities
                bound
              }
            }
          }
        }
      }
      head2Head {
        venueStatsData {
          firstBattingWinPercent
          avgFirstInningScore
          venueName
          venueID
          paceWicketPercent
          spinWicketPercent
          highestScoreChased
        }
        head2Head {
          totalMatches
          noResult
          teamA
          teamB
        }
        teamA {
          key
          lastFStatus
          teamName
          teamShortName
          color
        }
        teamB {
          key
          lastFStatus
          teamName
          teamShortName
          color
        }
      }
    }
  }
`

export const GET_VIDEO_POSITIONS = gql`
  query getVideosPostitions {
    getVideosPostitions {
      videoID
      title
      description
      videoUrl
      videoYTId
    }
  }
`
export const GET_VIDEO_POSITIONS_AXIOS = `
  query getVideosPostitions{
    getVideosPostitions{
    videoID
    title
    description
    videoUrl
    videoYTId
  }
  }
`

export const GET_ARTICLES_BY_POSITION = gql`
  query getArticleByPostitions {
    getArticleByPostitions {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`
export const GET_ARTICLES_BY_POSITION_AXIOS = `
  query getArticleByPostitions {
    getArticleByPostitions{
      articleID
      matchIDs{
        matchID
        matchSlug
      }
      seriesIDs{
        name
        id
      }
      teamIDs{
        name
        id
      }
      playerIDs{
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags{
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`

export const POPULAR_PLAYER = gql`
  query popularPlayers {
    PopularPlayer {
      name
      id
    }
  }
`
export const POPULAR_PLAYER_AXIOS = `
  query popularPlayers{
    PopularPlayer{
      name
      id
    }
  }
`

export const GET_FANTASY_RESEARCH = gql`
  query fantasy($matchID: String!) {
    getPlayerSelectionComposition {
      gameType
      totalPlayersCount
      credits
      players {
        role
        roleName
        roleShortName
        min
        max
      }
    }
    getfantasyResearch(matchID: $matchID) {
      matchFeedID
      matchTeamHome
      matchTeamAway
      homeTeamId
      awayTeamId
      matchTeamHomeFlag
      matchTeamAwayFlag
      matchTeamHomeShort
      matchTeamAwayShort
      matchStarted
      matchDetails
      matchSquad {
        clubName
        matchFeedID
        playersArray {
          playerFeedID
          playerId
          cricketPlayerID
          playerName
          playerClubName
          playerRole
          playerImage
          playerInStarting11
          playerCredits
          playerPoints
          playerMatchPoints
          playerSelectionPercentage
          fantasy_flag
          projectedPoints
          captain
          vice_captain
          strength
          weakness
          inningsHistory {
            score
            wickets
            opposition
          }
        }
      }
    }
  }
`

export const GET_FANTASY_RESEARCH_AXIOS = `
	query fantasy($matchID: String!) {
    getPlayerSelectionComposition{
      gameType
      totalPlayersCount
      credits
      players{
        role
        roleName
        roleShortName
        min
        max
      }
    }
		fantasyResearchTopPlayers {
			matchID
			startDate
			top2Players {
				playerName
				playerRole
				playerCredits
				playerSelectionPercentage
				projectedPoints
				weakness
				strength
				playerId
				playerFeedID
			}
			MatchName
			homeTeam
			awayTeam
			homeTeamId
			awayTeamId
			startDate
			stadiumsDetails {
				firstBattingAvgScore
				HeigestScoreChased
				FirstBattingWinPercentage
				pace
				spin
				city
			}
			captainAndViseCaptain {
				teamType
				projectedPoints
				teamPlayers {
					captain
					vice_captain
					playerName
					playerId
				}
				fantasy_teamName
			}
		}
		getfantasyResearch(matchID: $matchID) {
			matchFeedID
			matchTeamHome
			matchTeamAway
			homeTeamId
			awayTeamId
			matchTeamHomeFlag
			matchTeamAwayFlag
			matchTeamHomeShort
			matchTeamAwayShort
			matchStarted
			matchDetails
			matchSquad {
				clubName
				matchFeedID
				playersArray {
					playerFeedID
					playerId
					cricketPlayerID
					playerName
					playerClubName
					playerRole
					playerImage
					playerInStarting11
					playerCredits
					playerPoints
					playerMatchPoints
					playerSelectionPercentage
					fantasy_flag
					projectedPoints
					captain
					vice_captain
					strength
					weakness
					inningsHistory {
						score
						wickets
						opposition
					}
				}
			}
		}
	}
`

export const GET_ARTICLES_BY_CATEGORIES = gql`
  query getArticlesByCategories($type: String!, $page: Int) {
    getArticlesByCategories(type: $type, page: $page) {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
      relatedArticles {
        articleID
        matchIDs {
          matchID
          matchSlug
        }
        seriesIDs {
          name
          id
        }
        teamIDs {
          name
          id
        }
        playerIDs {
          name
          id
        }
        approvalStatus
        status
        title
        slugTitle
        content
        thumbnail
        featureThumbnail
        description
        tags {
          name
          type
          tag
          id
        }
        type
        filters
        createdAt
        updatedAt
        author
        publishedAt
        seoTags
        relatedArticles {
          articleID
        }
      }
    }
  }
`

export const VIDEO_BY_CATEGORY = gql`
  query getVideosByType($type: String!) {
    getVideosByType(type: $type) {
      videoID
      title
      description
      videoUrl
      videoYTId
    }
  }
`

export const GET_FANTASY_RESEARCH_TOP_PLAYERS = gql`
  query fantasyResearchTopPlayers {
    fantasyResearchTopPlayers {
      matchID
      startDate
      top2Players {
        playerName
        playerRole
        playerCredits
        playerSelectionPercentage
        projectedPoints
        weakness
        strength
        playerId
        playerFeedID
      }
      MatchName
      homeTeam
      awayTeam
      homeTeamId
      awayTeamId
      startDate
      stadiumsDetails {
        firstBattingAvgScore
        HeigestScoreChased
        FirstBattingWinPercentage
        pace
        spin
        city
      }
      captainAndViseCaptain {
        teamType
        projectedPoints
        teamPlayers {
          captain
          vice_captain
          playerName
          playerId
        }
        fantasy_teamName
      }
    }
  }
`

export const SOCIAL_TRACKER_AXIOS = `
query getSocialTracker {
  getSocialTracker {
      data {
            data
              type
              twitterID
              feedID
              position
              state
              isHomeSocial
      }
  }
}
`
export const SOCIAL_TRACKER = gql`
  query getSocialTracker {
    getSocialTracker {
      data {
        data
        type
        twitterID
        feedID
        position
        state
        isHomeSocial
      }
    }
  }
`
export const GET_FANTASY_RESEARCH_TOP_PLAYERS_AXIOS = gql`
  query fantasyResearchTopPlayers {
    fantasyResearchTopPlayers {
      matchID
      startDate
      top2Players {
        playerName
        playerRole
        playerCredits
        playerSelectionPercentage
        projectedPoints
        weakness
        strength
        playerId
        playerFeedID
      }
      MatchName
      homeTeam
      awayTeam
      homeTeamId
      awayTeamId
      startDate
      stadiumsDetails {
        firstBattingAvgScore
        HeigestScoreChased
        FirstBattingWinPercentage
        pace
        spin
        city
      }
      captainAndViseCaptain {
        teamType
        projectedPoints
        teamPlayers {
          captain
          vice_captain
          playerName
          playerId
        }
        fantasy_teamName
      }
    }
  }
`
// loginEnable

export const GET_FANTASY_RESEARCH_TOP_PLAYERS_COMPLETE_LIVE_MATCH_AND_VIDEOS_AXIOS = `
	query fantasyResearchTopPlayers($matchStatus: String!) {
    
		fantasyCompletedAndliveMatches(matchStatus: $matchStatus) {
      
      matchId
      
			MatchName
			matchType
			homeTeamId
			statusMessage
			homeTeamShortName
			awayTeamShortName
			awayTeamId
			matchStatus
			currentinningsNo
      currentInningsTeamID
      
			matchScore {
				teamShortName
				teamFullName
				teamID
				teamScore {
					runsScored
					inning
					inningNumber
					battingTeam
					runsScored
					wickets
					overs
					runRate
					battingSide
					teamID
					battingTeamShortName
					declared
					folowOn
				}
			}
			teams {
				fantasy_teamName
				totalFPoints
			}
			captainAndViseCaptain {
				live
				teamType
				fantasy_teamName
				projectedPoints
				teamPlayers {
					captain
					vice_captain
					playerId
					playerName
				}
			}
		}
		getVideos {
			Fantasy {
				videoID
				title
				description
				videoUrl
				videoYTId
			}
		}
		fantasyResearchTopPlayers {
      loginEnable
			matchID
			startDate
			top2Players {
				playerName
				playerRole
				playerCredits
				playerSelectionPercentage
				projectedPoints
				weakness
				strength
				playerId
				playerFeedID
			}
			MatchName
			homeTeamShortName
			awayTeamShortName
			homeTeam
			awayTeam
			homeTeamId
			awayTeamId
			startDate
			stadiumsDetails {
				firstBattingAvgScore
				HeigestScoreChased
				FirstBattingWinPercentage
				pace
				spin
				city
			}
			captainAndViseCaptain {
				teamType
				projectedPoints
				teamPlayers {
					captain
					vice_captain
					playerName
					playerId
				}
        fantasy_teamName
        
			}
    }
    
	}
`
export const GET_FANTASY_RESEARCH_TEAMS = gql`
  query FantasyResearchTeams($matchID: String!) {
    FantasyResearchTeams(matchID: $matchID) {
      matchId
      playing11Status
      teams {
        fantasy_teamName
        totalCredits
        homeTeamCount
        awayTeamCount
        homeTeamShortName
        awayTeamShortName
        homeTeamId
        awayTeamId
        MatchName
        totalProjectedPoints
        players {
          credits
          playerCredits
          playerId
          playerFeedID
          playerName
          selectionPercent
          projectedPoints
          playerClubName
          teamName
          playerRole
          captain
          vice_captain
        }
      }
    }
  }
`

export const GET_FANTASY_RESEARCH_TEAMS_AXIOS = `
	query FantasyResearchTeams($matchID: String!) {
		fantasyResearchTopPlayers {
			matchID
			startDate
			top2Players {
				playerName
				playerRole
				playerCredits
				playerSelectionPercentage
				projectedPoints
				weakness
				strength
				playerId
				playerFeedID
			}
			MatchName
			homeTeam
			awayTeam
			homeTeamId
			awayTeamId
			startDate
			stadiumsDetails {
				firstBattingAvgScore
				HeigestScoreChased
				FirstBattingWinPercentage
				pace
				spin
				city
			}
			captainAndViseCaptain {
				teamType
				projectedPoints
				teamPlayers {
					captain
					vice_captain
					playerName
					playerId
				}
				fantasy_teamName
			}
		}
		FantasyResearchTeams(matchID: $matchID) {
			matchId
			playing11Status
			teams {
				fantasy_teamName
				totalCredits
				homeTeamCount
				awayTeamCount
				homeTeamShortName
				awayTeamShortName
				homeTeamId
				awayTeamId
				MatchName
				totalProjectedPoints
				players {
					credits
					playerCredits
					playerId
					playerFeedID
					playerName
					selectionPercent
					projectedPoints
					playerClubName
					teamName
					playerRole
					captain
					vice_captain
				}
			}
		}
	}
`

export const TEAM_AUCTION_AXIOS = `query
 getTeamAuctionDetails($name: String!) {
  getTeamAuctionDetails(name: $name) {
    name
    budget
    remainingBudget
    indianCount
    overseasCount
    players {
      cdcID
      firstName
      surname
      overseas
      price
      retained
      currentIplTeam
      DOB
      unCapped
      bids {
        price
        team
        isSoldOut
      }
      finalPrice
      isLive
      isSoldOut
    }
  }
}
`

export const TEAM_AUCTION = gql`
  query getTeamAuctionDetails($name: String!) {
    getTeamAuctionDetails(name: $name) {
      name
      budget
      remainingBudget
      indianCount
      overseasCount
      players {
        cdcID
        firstName
        surname
        overseas
        price
        retained
        currentIplTeam
        DOB
        unCapped
        bids {
          price
          team
          isSoldOut
        }
        finalPrice
        isLive
        isSoldOut
      }
    }
  }
`
export const LIVE_AUCTION = gql`
  query getLivePlayerAuction {
    getLivePlayerAuction {
      cdcID
      firstName
      surname
      overseas
      price
      retained
      currentIplTeam
      DOB
      unCapped
      bids {
        price
        team
        isSoldOut
      }
      finalPrice
      isLive

      auctionDetails {
        name
        isPolling
        display
        isCompleted
      }
    }
  }
`

export const GET_RECENT_PLAYERS_AXIOS = `
  query getRecentPlayers {
    getRecentPlayers {
      livePlayerObject {
        cdcID
        firstName
        surname
        overseas
        price
        retained
        currentIplTeam
        DOB
        
        unCapped
        bids {
          price
          team
          isSoldOut
        }
        finalPrice
        isLive
        isSoldOut
      }
      players {
        cdcID
        firstName
        surname
        overseas
        price
        retained
        currentIplTeam
        DOB
        unCapped
        bids {
          price
          team
          isSoldOut
        }
        finalPrice
        isLive
        isUnsold
        isSoldOut
      }
    }
  }
`

export const GET_RECENT_PLAYERS = gql`
  query getRecentPlayers {
    getRecentPlayers {
      livePlayerObject {
        cdcID
        firstName
        surname
        overseas
        price
        retained
        currentIplTeam
        DOB

        unCapped
        bids {
          price
          team
          isSoldOut
        }
        finalPrice
        isLive
        isSoldOut
      }
      players {
        cdcID
        firstName
        surname
        overseas
        price
        retained
        currentIplTeam
        DOB
        unCapped
        bids {
          price
          team
          isSoldOut
        }
        finalPrice
        isLive
        isUnsold
        isSoldOut
      }
    }
  }
`

export const GET_ALL_PLAYERS = gql`
  query getAllPlayerAuction($skip: Int) {
    getAllPlayerAuction(skip: $skip) {
      players {
        cdcID
        firstName
        surname
        overseas
        price
        retained
        currentIplTeam
        DOB
        unCapped
      }
    }
  }
`
// loginEnable
export const GET_FANTASY_COMPLETED_AND_LIVE_MATCHES = gql`
  query fantasyCompletedAndliveMatches($matchStatus: String!) {
    fantasyCompletedAndliveMatches(matchStatus: $matchStatus) {
      matchId
      MatchName
      matchType
      homeTeamId
      statusMessage
      homeTeamShortName
      awayTeamShortName
      awayTeamId
      matchStatus
      currentinningsNo
      currentInningsTeamID
      matchScore {
        teamShortName
        teamFullName
        teamID
        teamScore {
          runsScored
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
      teams {
        fantasy_teamName
        totalFPoints
      }
      captainAndViseCaptain {
        live
        teamType
        fantasy_teamName
        projectedPoints
        teamPlayers {
          captain
          vice_captain
          playerId
          playerName
        }
      }
    }
  }
`

export const GET_FANTASY_COMPLETED_AND_LIVE_MATCHES_AXIOS = `
	query fantasyCompletedAndliveMatches($matchStatus: String!) {
		fantasyCompletedAndliveMatches(matchStatus: $matchStatus) {
			matchId
			MatchName
			matchType
			homeTeamId
			statusMessage
			awayTeamId
			matchStatus
			currentinningsNo
			currentInningsTeamID
			matchScore {
				teamShortName
				teamFullName
				teamID
				teamScore {
					runsScored
					inning
					inningNumber
					battingTeam
					runsScored
					wickets
					overs
					runRate
					battingSide
					teamID
					battingTeamShortName
					declared
					folowOn
				}
			}
			teams {
				fantasy_teamName
				totalFPoints
			}
			captainAndViseCaptain {
				live
				teamType
				fantasy_teamName
				projectedPoints
				teamPlayers {
					captain
					vice_captain
					playerId
					playerName
				}
			}
		}
	}
`

export const GET_FANTASY_TEAMS_FOR_COMPLETED_AND_LIVE_MATCHES_AXIOS = `
 query fantasyTeamsForCompletedAndliveMatches($matchID:String!,$matchStatus:String){
   fantasyTeamsForCompletedAndliveMatches(matchID:$matchID,matchStatus:$matchStatus){
     matchScore{
       teamFullName
       teamShortName
       teamID
       teamScore{
         inning
         inningNumber
         battingTeam
         runsScored
         wickets
         overs
         runRate
         battingSide
         teamID
         battingTeamShortName
         declared
         folowOn
       }
     }
     teams{
       fantasy_teamName
       totalFPoints
       totalCredits
       totalProjectedPoints
       homeTeamCount
       awayTeamCount
       homeTeamShortName
       awayTeamShortName
       homeTeamId
       awayTeamId
       MatchName
       players {
         credits
         playerCredits
         playerId
         playerFeedID
         playerName
         selectionPercent
         teamName
         playerRole
         captain
         vice_captain
         projectedPoints
         playerClubName
         ActualPoint
       }
     }
     matchId
     MatchName
     homeTeamId
     awayTeamId
     matchStatus
     matchType
     statusMessage
     currentinningsNo
     currentInningsTeamID
   }
 }
 `

export const GET_FANTASY_TEAMS_FOR_COMPLETED_AND_LIVE_MATCHES = gql`
  query fantasyTeamsForCompletedAndliveMatches(
    $matchID: String!
    $matchStatus: String
  ) {
    fantasyTeamsForCompletedAndliveMatches(
      matchID: $matchID
      matchStatus: $matchStatus
    ) {
      matchScore {
        teamFullName
        teamShortName
        teamID
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
      teams {
        fantasy_teamName
        totalFPoints
        totalCredits
        totalProjectedPoints
        homeTeamCount
        awayTeamCount
        homeTeamShortName
        awayTeamShortName
        homeTeamId
        awayTeamId
        MatchName
        players {
          credits
          playerCredits
          playerId
          playerFeedID
          playerName
          selectionPercent
          teamName
          playerRole
          captain
          vice_captain
          projectedPoints
          playerClubName
          ActualPoint
        }
      }
      matchId
      MatchName
      homeTeamId
      awayTeamId
      matchStatus
      matchType
      statusMessage
      currentinningsNo
      currentInningsTeamID
    }
  }
`

export const FANTASY_PLAYER_RECORDS = gql`
  query fantasyPlayersRecords($playerID: String!) {
    fantasyPlayersRecords(playerID: $playerID) {
      playerCredits
      matchesData {
        againstTeam
        matchType
        playerRun
        playerwickets
        TotalFantasyPoint
      }
      playerName
      playerID
      playerCredits
      SelectionPercentage
      strength
      weakness
      bowlingStyle
      battingStyle
    }
  }
`

export const TOP_GUNS = gql`
  query getStatsResolver($tourID: String!) {
    getStatsResolver(tourID: $tourID) {
      odi {
        topGuns {
          Most_Runs {
            player_id
            team_id
            player_name
            runs_scored
            team_name
            team_short_name
          }
          Most_Wickets {
            team_id
            player_id
            player_name
            wickets
            team_short_name
          }
          Highest_Score {
            team_id
            player_id
            player_name
            team_name
            team_short_name
            team_id
            highest_score
            balls_faced
          }
          Best_figures {
            team_id
            player_id
            player_name
            team_short_name
            best_bowling_figures
          }
        }
        Batting {
          Most_Runs {
            player_id
            player_name
            matches_played
            innings_played
            runs_scored
            highest_score
            team_id
            team_short_name
            innings_played
          }
          Highest_Score {
            player_id
            player_name
            team_id
            team_short_name
            vs_team_short_name
            highest_score
            balls_faced
          }

          Most_Fifties {
            player_id
            team_id
            player_name
            matches_played
            innings_played
            fifties
            team_short_name
            runs_scored
          }
          Most_Hundreds {
            player_id
            team_id
            player_name
            hundred
            runs_scored
            matches_played
            innings_played
            team_short_name
          }
          Most_4s {
            player_id
            team_id
            player_name
            matches_played
            team_short_name
            runs_scored
            fours
            innings_played
          }
          Most_6s {
            player_id
            team_id
            player_name
            matches_played
            sixes
            runs_scored
            team_short_name
          }
          Best_Batting_Average {
            player_id
            team_id
            player_name
            matches_played
            innings_played
            team_short_name
            runs_scored
            average
          }
          Best_Strike_Rate {
            player_id
            team_id
            player_name
            matches_played
            team_short_name
            batting_strike_rate
            runs_scored
          }
        }
        Bowling {
          Most_Wickets {
            player_id
            team_id
            player_name
            wickets
            team_short_name
            overs
            matches_played
          }
          Best_figures {
            player_id
            team_id
            player_name
            best_bowling_figures
            team_short_name
            overs
            vs_team_short_name
          }
          Five_Wicket_Haul {
            player_id
            team_id
            player_name
            team_short_name
            five_wickets_haul
            matches_played
            wickets
          }
          Best_Bowling_Average {
            player_id
            team_id
            player_name
            team_short_name
            best_bowling_average
            wickets
            matches_played
          }
          Best_Bowling_Strike_Rate {
            player_id
            team_id
            player_name
            team_short_name
            bowling_strike_rate
            matches_played
            wickets
          }

          Best_Economy_Rates {
            player_id
            team_id
            player_name
            team_short_name
            economy
            matches_played
            wickets
          }
        }
        fielding {
          Most_Catches {
            player_id
            team_id
            player_name
            team_short_name
            most_catches
            matches_played
          }
          Most_Run_Outs {
            player_id
            team_id
            player_name
            team_short_name
            most_run_outs
            matches_played
          }
          Most_Dismissals {
            player_id
            team_id
            player_name
            team_short_name
            most_dismissals
            matches_played
          }
        }
      }
      test {
        topGuns {
          Most_Runs {
            player_id
            team_id
            player_name
            runs_scored
            team_short_name
          }
          Most_Wickets {
            player_id
            team_id
            player_name
            matches_played
            wickets
            team_short_name
          }
          Highest_Score {
            player_id
            team_id
            player_name
            team_short_name
            highest_score
            balls_faced
          }
          Best_figures {
            player_id
            team_id
            player_name
            team_short_name
            best_bowling_figures
          }
        }
        Batting {
          Most_Runs {
            player_id
            player_name
            matches_played
            innings_played
            runs_scored
            highest_score
            team_id
            team_short_name
          }
          Highest_Score {
            player_id
            player_name
            team_id
            team_short_name
            vs_team_short_name
            highest_score
            balls_faced
          }

          Most_Fifties {
            player_id
            team_id
            player_name
            matches_played
            innings_played
            fifties
            team_short_name
            runs_scored
          }
          Most_Hundreds {
            player_id
            team_id
            player_name
            hundred
            runs_scored
            matches_played
            innings_played
            team_short_name
          }
          Most_4s {
            player_id
            team_id
            player_name
            matches_played
            team_short_name
            runs_scored
            fours
            innings_played
          }
          Most_6s {
            player_id
            team_id
            player_name
            matches_played
            sixes
            runs_scored
            team_short_name
          }
          Best_Batting_Average {
            player_id
            team_id
            player_name
            matches_played
            innings_played
            team_short_name
            runs_scored
            average
          }
          Best_Strike_Rate {
            player_id
            team_id
            player_name
            matches_played
            team_short_name
            batting_strike_rate
            runs_scored
          }
        }
        Bowling {
          Most_Wickets {
            player_id
            team_id
            player_name
            wickets
            team_short_name
            overs
            matches_played
          }
          Best_figures {
            player_id
            team_id
            player_name
            best_bowling_figures
            team_short_name
            overs
            vs_team_short_name
          }
          Five_Wicket_Haul {
            player_id
            team_id
            player_name
            team_short_name
            five_wickets_haul
            matches_played
            wickets
          }
          Best_Bowling_Average {
            player_id
            team_id
            player_name
            team_short_name
            best_bowling_average
            wickets
            matches_played
          }
          Best_Bowling_Strike_Rate {
            player_id
            team_id
            player_name
            team_short_name
            bowling_strike_rate
            matches_played
            wickets
          }
          Best_Economy_Rates {
            player_id
            team_id
            player_name
            team_short_name
            economy
            matches_played
            wickets
          }
        }
        fielding {
          Most_Catches {
            player_id
            team_id
            player_name
            team_short_name
            most_catches
            matches_played
          }
          Most_Run_Outs {
            player_id
            team_id
            player_name
            team_short_name
            most_run_outs
            matches_played
          }
          Most_Dismissals {
            player_id
            team_id
            player_name
            team_short_name
            most_dismissals
            matches_played
          }
        }
      }
      t20 {
        topGuns {
          Most_Runs {
            player_id
            team_id
            player_name
            runs_scored
            team_short_name
          }
          Most_Wickets {
            player_id
            team_id
            player_name
            wickets
            team_short_name
          }
          Highest_Score {
            player_id
            team_id
            player_name
            team_short_name
            highest_score
            balls_faced
          }
          Best_figures {
            player_id
            team_id
            player_name
            team_short_name
            best_bowling_figures
          }
        }
        Batting {
          Most_Runs {
            player_id
            player_name
            matches_played
            innings_played
            runs_scored
            highest_score
            team_id
            team_short_name
          }
          Highest_Score {
            player_id
            player_name
            team_id
            team_short_name
            vs_team_short_name
            highest_score
            balls_faced
          }

          Most_Fifties {
            player_id
            team_id
            player_name
            matches_played
            innings_played
            fifties
            team_short_name
            runs_scored
          }
          Most_Hundreds {
            player_id
            team_id
            player_name
            hundred
            runs_scored
            matches_played
            innings_played
            team_short_name
          }
          Most_4s {
            player_id
            team_id
            player_name
            matches_played
            team_short_name
            runs_scored
            fours
            innings_played
          }
          Most_6s {
            player_id
            team_id
            player_name
            matches_played
            sixes
            runs_scored
            team_short_name
          }

          Best_Batting_Average {
            player_id
            team_id
            player_name
            matches_played
            innings_played
            team_short_name
            runs_scored
            average
          }
          Best_Strike_Rate {
            player_id
            team_id
            player_name
            matches_played
            team_short_name
            batting_strike_rate
            runs_scored
          }
        }
        Bowling {
          Most_Wickets {
            player_id
            team_id
            player_name
            wickets
            team_short_name
            overs
            matches_played
          }
          Best_figures {
            player_id
            team_id
            player_name
            best_bowling_figures
            team_short_name
            overs
            vs_team_short_name
          }
          Five_Wicket_Haul {
            player_id
            team_id
            player_name
            team_short_name
            five_wickets_haul
            matches_played
            wickets
          }
          Three_Wicket_Haul {
            player_id
            team_id
            player_name
            team_short_name
            three_wickets_haul
            matches_played
            wickets
          }

          Best_Bowling_Average {
            player_id
            team_id
            player_name
            team_short_name
            best_bowling_average
            wickets
            matches_played
          }
          Best_Bowling_Strike_Rate {
            player_id
            team_id
            player_name
            team_short_name
            bowling_strike_rate
            matches_played
            wickets
          }
          Best_Economy_Rates {
            player_id
            team_id
            player_name
            team_short_name
            economy
            matches_played
            wickets
          }
        }
        fielding {
          Most_Catches {
            player_id
            team_id
            player_name
            team_short_name
            most_catches
            matches_played
          }
          Most_Run_Outs {
            player_id
            team_id
            player_name
            team_short_name
            most_run_outs
            matches_played
          }
          Most_Dismissals {
            player_id
            team_id
            player_name
            team_short_name
            most_dismissals
            matches_played
          }
        }
      }
      tourType
      formatArray
    }
  }
`

export const TOP_GUNS_AXIOS = `
	query getStatsResolver($tourID: String!) {
		getStatsResolver(tourID: $tourID) {
			odi {
				topGuns {
					Most_Runs {
						player_id
            team_id
						player_name
						runs_scored
						team_name
						team_short_name
					}
					Most_Wickets {
						player_id
            team_id
						player_name
						wickets
						team_short_name
					}
					Highest_Score {
						player_id
						player_name
						team_name
						team_short_name
						team_id
						highest_score
						balls_faced
					}
					Best_figures {
						player_id
            team_id
						player_name
						team_short_name
						best_bowling_figures
					}
				}
				Batting {
					Most_Runs {
						player_id
						player_name
						matches_played
						innings_played
						runs_scored
						highest_score
						team_id
						team_short_name
						innings_played
					}
					Highest_Score {
						player_id
						player_name
						team_id
						team_short_name
						vs_team_short_name
						highest_score
						balls_faced
					}

					Most_Fifties {
						player_id
						player_name
						matches_played
						innings_played
						fifties
						team_short_name
						runs_scored
					}
					Most_Hundreds {
						player_id
						player_name
						hundred
						runs_scored
						matches_played
						innings_played
						team_short_name
					}
					Most_4s {
						player_id
						player_name
						matches_played
						team_short_name
						runs_scored
						fours
						innings_played
					}
					Most_6s {
						player_id
						player_name
						matches_played
						sixes
						runs_scored
						team_short_name
					}
					Best_Batting_Average {
						player_id
						player_name
						matches_played
						innings_played
						team_short_name
						runs_scored
						average
					}
					Best_Strike_Rate {
						player_id
						player_name
						matches_played
						team_short_name
						batting_strike_rate
						runs_scored
					}
				}
				Bowling {
					Most_Wickets {
						player_id
						player_name
						wickets
						team_short_name
						overs
						matches_played
					}
					Best_figures {
						player_id
						player_name
						best_bowling_figures
						team_short_name
						overs
						vs_team_short_name
					}
					Five_Wicket_Haul {
						player_id
						player_name
						team_short_name
						five_wickets_haul
						matches_played
						wickets
					}
					Best_Bowling_Average {
						player_id
						player_name
						team_short_name
						best_bowling_average
						wickets
						matches_played
					}
					Best_Bowling_Strike_Rate {
						player_id
						player_name
						team_short_name
						bowling_strike_rate
						matches_played
						wickets
					}

					Best_Economy_Rates {
						player_id
						player_name
						team_short_name
						economy
						matches_played
						wickets
					}
				}
				fielding {
					Most_Catches {
						player_id
						player_name
						team_short_name
						most_catches
						matches_played
					}
					Most_Run_Outs {
						player_id
						player_name
						team_short_name
						most_run_outs
						matches_played
					}
					Most_Dismissals {
						player_id
						player_name
						team_short_name
						most_dismissals
						matches_played
					}
				}
			}
			test {
				topGuns {
					Most_Runs {
						player_id
            team_id
						player_name
						runs_scored
						team_short_name
					}
					Most_Wickets {
						player_id
            team_id
						player_name
						matches_played
						wickets
						team_short_name
					}
					Highest_Score {
						player_id
            team_id
						player_name
						team_short_name
						highest_score
						balls_faced
					}
					Best_figures {
						player_id
            team_id
						player_name
						team_short_name
						best_bowling_figures
					}
				}
				Batting {
					Most_Runs {
						player_id
						player_name
						matches_played
						innings_played
						runs_scored
						highest_score
						team_id
						team_short_name
					}
					Highest_Score {
						player_id
						player_name
						team_id
						team_short_name
						vs_team_short_name
						highest_score
						balls_faced
					}

					Most_Fifties {
						player_id
						player_name
						matches_played
						innings_played
						fifties
						team_short_name
						runs_scored
					}
					Most_Hundreds {
						player_id
						player_name
						hundred
						runs_scored
						matches_played
						innings_played
						team_short_name
					}
					Most_4s {
						player_id
						player_name
						matches_played
						team_short_name
						runs_scored
						fours
						innings_played
					}
					Most_6s {
						player_id
						player_name
						matches_played
						sixes
						runs_scored
						team_short_name
					}
					Best_Batting_Average {
						player_id
						player_name
						matches_played
						innings_played
						team_short_name
						runs_scored
						average
					}
					Best_Strike_Rate {
						player_id
						player_name
						matches_played
						team_short_name
						batting_strike_rate
						runs_scored
					}
				}
				Bowling {
					Most_Wickets {
						player_id
						player_name
						wickets
						team_short_name
						overs
						matches_played
					}
					Best_figures {
						player_id
						player_name
						best_bowling_figures
						team_short_name
						overs
						vs_team_short_name
					}
					Five_Wicket_Haul {
						player_id
						player_name
						team_short_name
						five_wickets_haul
						matches_played
						wickets
					}
					Best_Bowling_Average {
						player_id
						player_name
						team_short_name
						best_bowling_average
						wickets
						matches_played
					}
					Best_Bowling_Strike_Rate {
						player_id
						player_name
						team_short_name
						bowling_strike_rate
						matches_played
						wickets
					}
					Best_Economy_Rates {
						player_id
						player_name
						team_short_name
						economy
						matches_played
						wickets
					}
				}
				fielding {
					Most_Catches {
						player_id
						player_name
						team_short_name
						most_catches
						matches_played
					}
					Most_Run_Outs {
						player_id
						player_name
						team_short_name
						most_run_outs
						matches_played
					}
					Most_Dismissals {
						player_id
						player_name
						team_short_name
						most_dismissals
						matches_played
					}
				}
			}
			t20 {
				topGuns {
					Most_Runs {
						player_id
            team_id
						player_name
						runs_scored
						team_short_name
					}
					Most_Wickets {
						player_id
            team_id
						player_name
						wickets
						team_short_name
					}
					Highest_Score {
						player_id
            team_id
						player_name
						team_short_name
						highest_score
						balls_faced
					}
					Best_figures {
						player_id
            team_id
						player_name
						team_short_name
						best_bowling_figures
					}
				}
				Batting {
					Most_Runs {
						player_id
						player_name
						matches_played
						innings_played
						runs_scored
						highest_score
						team_id
						team_short_name
					}
					Highest_Score {
						player_id
						player_name
						team_id
						team_short_name
						vs_team_short_name
						highest_score
						balls_faced
					}

					Most_Fifties {
						player_id
						player_name
						matches_played
						innings_played
						fifties
						team_short_name
						runs_scored
					}
					Most_Hundreds {
						player_id
						player_name
						hundred
						runs_scored
						matches_played
						innings_played
						team_short_name
					}
					Most_4s {
						player_id
						player_name
						matches_played
						team_short_name
						runs_scored
						fours
						innings_played
					}
					Most_6s {
						player_id
						player_name
						matches_played
						sixes
						runs_scored
						team_short_name
					}

					Best_Batting_Average {
						player_id
						player_name
						matches_played
						innings_played
						team_short_name
						runs_scored
						average
					}
					Best_Strike_Rate {
						player_id
						player_name
						matches_played
						team_short_name
						batting_strike_rate
						runs_scored
					}
				}
				Bowling {
					Most_Wickets {
						player_id
						player_name
						wickets
						team_short_name
						overs
						matches_played
					}
					Best_figures {
						player_id
						player_name
						best_bowling_figures
						team_short_name
						overs
						vs_team_short_name
					}
					Five_Wicket_Haul {
						player_id
						player_name
						team_short_name
						five_wickets_haul
						matches_played
						wickets
					}
					Three_Wicket_Haul {
						player_id
						player_name
						team_short_name
						three_wickets_haul
						matches_played
						wickets
					}

					Best_Bowling_Average {
						player_id
						player_name
						team_short_name
						best_bowling_average
						wickets
						matches_played
					}
					Best_Bowling_Strike_Rate {
						player_id
						player_name
						team_short_name
						bowling_strike_rate
						matches_played
						wickets
					}
					Best_Economy_Rates {
						player_id
						player_name
						team_short_name
						economy
						matches_played
						wickets
					}
				}
				fielding {
					Most_Catches {
						player_id
						player_name
						team_short_name
						most_catches
						matches_played
					}
					Most_Run_Outs {
						player_id
						player_name
						team_short_name
						most_run_outs
						matches_played
					}
					Most_Dismissals {
						player_id
						player_name
						team_short_name
						most_dismissals
						matches_played
					}
				}
			}
			tourType
			formatArray
		}
	}
`

export const GET_POINTS_TABLE = gql`
  query getPointsTable($tourID: String!) {
    getPointsTable(tourID: $tourID) {
      standings {
        name
        teams {
          pos
          teamID
          teamName
          teamShortName
          all
          wins
          lost
          points
          nrr
          isQualified
          noResult
        }
      }
    }
  }
`
export const GET_POINTS_TABLE_AXIOS = `
	query getPointsTable($tourID: String!) {
		getPointsTable(tourID: $tourID) {
			standings {
				name
				teams {
					pos
					teamID
					teamName
					teamShortName
					all
					wins
					lost
					points
					nrr
					isQualified
				}
			}
		}
	}
`

export const GET_SERIES_SQUADS = gql`
  query getSeriesSquads($seriesID: String!) {
    getSeriesSquads(seriesID: $seriesID) {
      isNewSeries
      displayFormats
      disableFormats
      squadData {
        teamID
        name
        squad {
          matchType
          playersArray {
            name
            playerID
            iswk
            iscap
            playerRole
            playerName
          }
        }
      }
    }
  }
`
export const GET_SERIES_SQUADS_AXIOS = `
	query getSeriesSquads($seriesID: String!) {
		getSeriesSquads(seriesID: $seriesID) {
			isNewSeries
			displayFormats
			disableFormats
			squadData {
				teamID
				name
				squad {
					matchType
					playersArray {
						name
						iswk
						iscap
            playerID
						playerRole
						playerName
					}
				}
			}
		}
	}
`
export const GET_PRIMARY_VIDEOS = gql`
  query getPrimaryVideo {
    getPrimaryVideo {
      videoID
      type
      title
      description
      videoUrl
      videoYTId
      createdAt
    }
  }
`
export const GET_PRIMARY_VIDEOS_AXIOS = `
  query getPrimaryVideo{
    getPrimaryVideo{
      videoID
      type
      title
      description
      videoUrl
      videoYTId
      createdAt
    }
  }
`

export const GET_VIDEO_BY_MATCH_AND_TYPE = gql`
  query getVideosByMatchIDandType($matchID: String, $type: String) {
    getVideosByMatchIDandType(matchID: $matchID, type: $type) {
      publishedDate
      videoID
      title
      description
      type
      videoUrl
      videoYTId
      timeDuration
    }
  }
`
export const GET_TOP_PLAYERS_AND_VIDEO_BY_MATCH_AND_TYPE_AXIOS = `
	query getVideosByMatchIDandType($matchID: String, $type: String) {
		fantasyResearchTopPlayers {
			matchID
			startDate
			top2Players {
				playerName
				playerRole
				playerCredits
				playerSelectionPercentage
				projectedPoints
				weakness
				strength
				playerId
				playerFeedID
			}
			MatchName
			homeTeam
			awayTeam
			homeTeamId
			awayTeamId
			startDate
			stadiumsDetails {
				firstBattingAvgScore
				HeigestScoreChased
				FirstBattingWinPercentage
				pace
				spin
				city
			}
			captainAndViseCaptain {
				teamType
				projectedPoints
				teamPlayers {
					captain
					vice_captain
					playerName
					playerId
				}
				fantasy_teamName
			}
		}
		getVideosByMatchIDandType(matchID: $matchID, type: $type) {
			publishedDate
			videoID
			title
			description
			type
			videoUrl
			videoYTId
			timeDuration
		}
	}
`

export const GET_VIDEOS_FANTASY = gql`
  query videos {
    getVideos {
      Fantasy {
        videoID
        title
        description
        videoUrl
        videoYTId
      }
    }
  }
`

export const getArticleIDs = `
  query  getAllArticleIDs($skip: Int, $limit: Int){
    getAllArticleIDs(skip: $skip, limit: $limit){
      articleID
    updatedAt

    }
  }
`

export const getAllScheduleSiteMap = `
  query  getAllScheduleSiteMap($skip: Int, $limit: Int, $status: String , $isCricklyticsAvailable: Boolean){
    getAllScheduleSiteMap(skip: $skip, limit: $limit, isCricklyticsAvailable: $isCricklyticsAvailable, status: $status){
      matchID
      awayTeamShortName
      homeTeamShortName
      matchName
      matchNumber
      matchStatus
      seriesName
      updatedAt
    }
  }
`

export const getAllFantasySiteMap = `
  query  getAllFantasySiteMap($skip: Int, $limit: Int){
    getAllFantasySiteMap(skip: $skip, limit: $limit){
      teams{
        fantasy_teamName
      }
      match{
        matchID
        awayTeamShortName
        homeTeamShortName
        matchName
        matchNumber
        seriesName
        matchStatus
      }
    }
  }`

export const getAllSeriesSiteMap = `
  query  getAllSeriesSiteMap($skip: Int, $limit: Int){
    getAllSeriesSiteMap(skip: $skip, limit:  $limit) {
      seriesID
      seriesName
      type
      updatedAt 
      hasPoints
      hasStatistics
    }
  }
`

export const getAllStadiumsForSitemap = `
  query  getAllStadiumsForSitemap($skip: Int, $limit: Int){
    getAllStadiumsForSitemap(skip: $skip, limit:  $limit) {
      venue_id
      name
    }
  }
`

export const getAllTeamsForSitemap = `
  query getAllTeamsForSitemap($skip: Int, $limit: Int){
    getAllTeamsForSitemap(skip: $skip, limit: $limit) {
        teamID
        name
    }
  }
`

export const getAllplayersForSitemap = `
  query getAllplayersForSitemap($skip: Int, $limit: Int){
    getAllplayersForSitemap(skip: $skip, limit: $limit) {
        playerID
        name
        fullName
    }
  }
`
export const GET_ALL_ARTICLES = `
  query getAllArticles($skip: Int, $limit: Int){
    getAllArticles(skip: $skip, limit: $limit){
      articleID
      matchIDs{
        matchID
        matchSlug
      }
      seriesIDs{
        name
        id
      }
      teamIDs{
        name
        id
      }
      playerIDs{
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      featureThumbnail
      thumbnail
      description
      tags{
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
      relatedArticles{
        articleID
      matchIDs{
        matchID
        matchSlug
      }
      seriesIDs{
        name
        id
      }
      teamIDs{
        name
        id
      }
      playerIDs{
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      featureThumbnail
      thumbnail
      description
      tags{
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags

      }
    }
  }
`

export const TOTAL = `
query getCountForStaticApis {
  getCountForStaticApis{
    articlescount
    playerscount
    teamscount
    stadiumscount
    seriescount
    schedulecount
    cryclyticsCount,
    fantasyCount
  }
}
`

export const GET_TRUMP_CARDS = gql`
  query getTrumpCards {
    getTrumpCards {
      playerID
      fullName
      name
      matches
      runs
      average
      fifties
      hundreds
      wickets
      bowlingAverage
      economyRate
    }
  }
`

export const GET_ALL_STADIUM_DETAILS = `
  query getAllstadiumDetails($skip: Int, $limit: Int ){
    getAllstadiumDetails(skip: $skip, limit: $limit){
      venueID
      fullName
      capacity
      city
      country
      bowlingEnds
      floodlight
      venueStats {
        odiDetails {
          avgIstInningScore
          firstBattingWin
          firstMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lastMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          highestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lowestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
        }
        testDetails {
          avgIstInningScore
          firstBattingWin
          firstMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lastMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          highestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lowestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
        }
        t20Details {
          avgIstInningScore
          firstBattingWin
          firstMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lastMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          highestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
          lowestScoreMatch {
            matchString
            matchName
            matchStatusStr
            scoreStr
            startDate
          }
        }
      }
    }
  }
`

export const GET_ALL_PLAYER_DETAILS = `
query  getAllplayersDetails($skip: Int, $limit: Int) {
  getAllplayersDetails(skip: $skip, limit: $limit){
    name
    playerID
    birthPlace
    dob
    description
    battingStyle
    bowlingStyle
  }
}
`

export const GET_ALL_TEAM_DETAILS = `
  query getAllteamDetails($skip: Int, $limit: Int) {
    getAllteamDetails(skip: $skip, limit: $limit) {
      teamID
      name
      description
      shortName

      odiFormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
      testFormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
      t20FormatDetails {
        basicdetails {
          seriesId
          format
          competitionType
        }
        firstMatch {
          matchId
          date
          result
          against {
            teamID
            teamName
            teamShortName
          }
          venue {
            venueID
            venueName
          }
        }
      }
    }
  }
`

export const DEATH_OVER_SIMULATOR = gql`
  query getOverSimulator($matchID: String!) {
    getOverSimulator(matchID: $matchID) {
      overSimulatorData {
        matchID
        predictedOversArray {
          overNumber
          runs
          wickets
        }
        nextThreeScore
        nextThreeOvers
        nextFiveScore
        nextFiveOvers
        nextFiveWickets
        nextThreeWickets
        currentOvers
        currentScore
        currentTeamId
        currentTeamShortName
        currentWickets
        inningNo
        format
        fiveFlag
        threeFlag
      }
      phaseOfInningsData {
        overNumber
        runs
        wickets
      }
    }
  }
`

export const NEW_COMMENTARY = gql`
  query getCommentary($matchId: String!, $timeStamp: Float!) {
    getCommentary(matchId: $matchId, timeStamp: $timeStamp) {
      commentary
    }
  }
`

export const POLL_COMMENTARY = gql`
  query pollCommentary($matchId: String!, $timeStamp: Float!) {
    pollCommentary(matchId: $matchId, timeStamp: $timeStamp)
  }
`

export const GET_OVER_BY_OVER = gql`
  query getOverbyOver($matchId: String!, $innings: Int!) {
    getOverbyOver(matchId: $matchId, innings: $innings) {
      overbyover
    }
  }
`

export const GAME_CHANGING_OVERS = gql`
  query gameChangingOvers($matchID: String!) {
    gameChangingOvers(matchID: $matchID) {
      matchID
      overs {
        inningNo
        overNo
        runsInOver
        score
        wicketsInOver
        teamShortName
        teamID
        commentary {
          over
          overNumber
          runs
          wicket
          type
          comment
        }
        commentaryOver {
          over
          overNumber
          runs
          wicket
          type
        }
        lastBallPredictionData {
          homeTeamShortName
          homeTeamPercent
          awayTeamShortName
          awayTeamPercent
          tiePercent
          ballNo
          currentWickets
          currentScore
          score
        }
      }
    }
  }
`

export const GET_MATCH_STATS = gql`
  query getmatchStats($matchID: String!) {
    getmatchStats(matchID: $matchID) {
      matchID
      awayTeamID
      homeTeamID
      format
      matchStatsArray {
        teamId
        shortName
        fullName
        highestBattingScore {
          value
          percent
        }
        totalFours {
          value
          percent
        }
        totalSix {
          value
          percent
        }
        runsScoredInBoundaries {
          value
          percent
        }
        highestPartnership {
          value
          percent
        }
        highestPartnership {
          value
          percent
        }
        totalDotBalls {
          value
          percent
        }
        runRate {
          value
          percent
        }
        runRateInPowerplay1_6 {
          value
          percent
        }
        runRateInPowerplay1_10 {
          value
          percent
        }
        runRateInPowerplay11_40 {
          value
          percent
        }
        runRateInPowerplay41_50 {
          value
          percent
        }
        runRateDeathOver {
          value
          percent
        }

        highestWickets {
          value
          percent
        }
        extras {
          value
          percent
        }
      }
    }
  }
`

export const GET_MATCH_STATS_AXIOS = `
query getmatchStats($matchID: String!){
  getmatchStats(matchID: $matchID){
   matchID
  	awayTeamID
    homeTeamID
    format
    matchStatsArray{
      teamId
      shortName
      fullName
      highestBattingScore{
        value
        percent
      }
      totalFours{
        value
        percent
      }
      totalSix{
        value
        percent
      }
      runsScoredInBoundaries{
        value
        percent
      }
      highestPartnership{
        value
        percent
      }
      highestPartnership{
        value
        percent
      }
      totalDotBalls{
        value
        percent
      }
      runRate{
        value
        percent
      }
      runRateInPowerplay1_6{
        value
        percent
      }
     runRateInPowerplay1_10{
      value
      percent
    }
      runRateInPowerplay11_40{
        value
        percent
      }
      runRateInPowerplay41_50{
        value
        percent
      }
      runRateDeathOver{
        value
        percent
      }

      highestWickets{
        value
        percent
      }
      extras{
        value
        percent
      }
    }
  }
}`

export const PHASES_OF_PLAY = gql`
  query getPhaseOfInningsResolver($matchID: String!) {
    getPhaseOfInningsResolver(matchID: $matchID) {
      inningsPhase {
        _id
        teamID
        teamShortName
        totalRuns
        totalWickets
        data {
          runs
          wickets
          innings
          teamID
          teamShortName
          overNumber
        }
      }
      format
    }
  }
`

export const QUALIFICATION_PROBABILITY = gql`
  query getQualificationProbability {
    getQualificationProbability {
      probabilityArray
      text
      qpTeamList {
        teamName
        teamColor
      }
      pointsTable {
        teamID
        teamName
        matchesPlayed
        points
        lost
        qp
        wins
        lost
        nrr
        isQualified
      }
    }
  }
`

export const PLAYER_INDEX = gql`
  query playerIndex {
    playerIndex {
      playerObject {
        playerID
        playerName
        playerTeamID
        playerTeamName
        totalPoints
        teamColor
      }
    }
  }
`

export const MATCH_RATINGS = gql`
  query matchRatings($matchID: String!) {
    matchRatings(matchID: $matchID) {
      finalArray {
        playerID
        playerName
        playerTeamID
        playerTeamName
        battingPoints
        battingPoints
        bowlingPoints
        totalPoints
      }
    }
  }
`

export const PLAYER_IMPACT = gql`
  query postmatch($matchID: String) {
    getPostMatchImpact(matchID: $matchID) {
      team1 {
        bowling_impact
        batting_impact
        playerName
        total_impact
        playerID
        playerTeam
      }
      team2 {
        bowling_impact
        batting_impact
        playerName
        total_impact
        playerID
        playerTeam
      }
      team1Name
      team2Name
    }
  }
`

export const SERIES_HOME_CARD = gql`
  query SeriesHomeCard {
    SeriesHomeCard {
      topPerformances {
        playerID
        playerName
        teamName
        score
        ScoreType
        tourID
        matchType
        tourName
      }
      matchesData {
        awayTeamID
        awayTeamShortName
        homeTeamID
        homeTeamShortName
        matchDateTimeIST
        matchStatus
        venue
        seriesName
        matchName
        seriesID
        tourID
        matchID
      }
    }
  }
`
export const TOP_FOUR_TEAMS = gql`
  query topFourTeams {
    topFourTeams {
      teams {
        teamName
        matchesPlayed
        points
        qp
        teamID
        isQualified
      }
      seriesName
      tourID
    }
  }
`
export const GET_MATCH_BASED_VIDEOS = gql`
  query getVideoByMatchID($matchID: String!) {
    getVideoByMatchID(matchID: $matchID) {
      videoType
      videoID
      title
      description
      type
      videoUrl
      videoYTId
      startDate
    }
  }
`

export const GET_REALTED_ARTICLE_DETAILS = gql`
  query articleDetail($articleID: String!) {
    getArticlesDetails(articleID: $articleID) {
      articleID
      type

      relatedArticles {
        thumbnail
        sm_image
        bg_image
        sm_image_safari
        title
        articleID
        description
        author
        createdAt
        type
        slugTitle
      }
    }
  }
`

export const CRICLYTICS_IPL = gql`
  query featuredMatches {
    featurematch {
      criclyticsButtonFlags {
        featuredSeriesName
        isFinalFour
        isPlayerIndex
        seriesName
        tourID
      }
    }
  }
`

export const NEWS_BY_ID_TYPE_AXIOS = `
  query getArticlesByIdAndType($Id: String!, $type: String!) {
    getArticlesByIdAndType(Id: $Id, type: $type) {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      thumbnail
      featureThumbnail
      bg_image
      bg_image_safari
      sm_image
      sm_image_safari
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }
  }
`

export const SERIES_AXIOS_SHORT = `
  query seriesResolverApp($type: String!){
    seriesResolverApp(type: $type){
    month
    series {
      seriesID
      seriesName
      Odicount
      T20count
      status
      Testcount
      startEndDate
      league
	  startDate
    }
  }
}
`

export const ACCOUNT_CHECK_AXIOS = `
query  accountCheck($account:String!){
  accountCheck(account:$account){
    code
    isAccountExist
    isMerged
  }
}
`

export const GET_SESSION_WINNER = gql`
  query getSessionWinner($matchID: String!) {
    getSessionWinner(matchID: $matchID) {
      teamA
      teamB
      teamAId
      teamBId
      teamAWon
      teamBWon
      shared
      data {
        sessions {
          day
          session
          teamWon
          teamId
          scores {
            startScore
            endScore
            teamId
            teamName
            inningNo
          }
        }
      }
    }
  }
`
export const SEASON_FANTASY_LOGIN = gql`
  mutation sessionFantasyToken($token: String) {
    sessionFantasyToken(token: $token) {
      code
      message
    }
  }
`

export const LIVE_BLOGGING_AXIOS = `
query getLiveBlockComments($articleID: String!){
getLiveBlockComments(articleID:$articleID){
  published
  createdAt
  blogType
  title
  author
  tags{
    id
    name
    type
    tag
    league
  }
  approvalStatus
  thumbnail
  bg_image
  sm_image
  bg_image_safari
  sm_image_safari
  featureThumbnail
  comments{
      createdAt
      currentScore
      title
      content
      publishedAt
      image
      image_desk
  }
  matchStatus
  type
  matchID
  matchName
  matchNumber
  matchSlug
  articleID
  newAuthors{
    id
    name
  }
}
}
`

export const LIVE_BLOGGING = gql`
  query getLiveBlockComments($articleID: String!) {
    getLiveBlockComments(articleID: $articleID) {
      published
      blogType
      createdAt
      title
      author
      tags {
        id
        name
        type
        tag
        league
      }
      approvalStatus
      thumbnail
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      featureThumbnail
      comments {
        createdAt
        currentScore
        title
        content
        publishedAt
        image
        image_desk
      }
      matchStatus
      type
      matchID
      matchName
      matchNumber
      matchSlug
      articleID
      newAuthors {
        id
        name
      }
    }
  }
`

export const SEASON_FANTASY_LOGIN_AXIOS = `
  mutation sessionFantasyToken($token: String) {
    sessionFantasyToken(token: $token) {
      code
      message
      
    }
  }
`

export const GET_SQUAD_FOR_BUILD_TEAM = gql`
  query getSquadForBuildTeam($matchID: String!, $token: String!) {
    getSquadForBuildTeam(matchID: $matchID, token: $token) {
      batsman {
        matchID
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        teamID
        projectedPoints
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
      bowler {
        matchID
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        teamID
        projectedPoints
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
      keeper {
        matchID
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        teamID
        projectedPoints
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
      all_rounder {
        matchID
        playerName
        player_role
        playerNameHindi
        playerId
        selectionPercent
        credits
        teamID
        projectedPoints
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
    }
  }
`

export const GET_FRC_TEAM = gql`
  query getfrcTeam(
    $matchID: String!
    $playerIds: [String]
    $leagueType: String!
    $selectCriteria: String!
    $token: String!
  ) {
    getFrcTeam(
      matchID: $matchID
      playerIds: $playerIds
      leagueType: $leagueType
      selectCriteria: $selectCriteria
      token: $token
    ) {
      totalPoints
      batsman {
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        teamID
        isMyPick
        projectedPoints
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
      bowler {
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        teamID
        projectedPoints
        isMyPick
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
      keeper {
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        teamID
        projectedPoints
        playing_xi
        isMyPick
        captain
        teamName
        vice_captain
        playerCredits
      }
      all_rounder {
        playerName
        playerNameHindi
        player_role
        playerId
        selectionPercent
        credits
        isMyPick
        teamID
        projectedPoints
        playing_xi
        captain
        teamName
        vice_captain
        playerCredits
      }
    }
  }
`

export const GET_FRC_PLAYER_REPLACEMENT = gql`
  query getFrcPlayerReplacement(
    $matchID: String!
    $replacementPlayerID: String!
    $playerIds: [String]
    $leagueType: String!
    $selectCriteria: String!
    $token: String!
  ) {
    getFrcPlayerReplacement(
      matchID: $matchID
      replacementPlayerID: $replacementPlayerID
      playerIds: $playerIds
      leagueType: $leagueType
      selectCriteria: $selectCriteria
      token: $token
    ) {
      matchID
      best_replacements {
        playerName
        playerNameHindi
        player_role
        playerId
        projectedPoints
        selectionPercent
        mean_projected_points
        playerCredits
        credits
        matchID
        playing_xi
        teamID
        teamName
        captain
        vice_captain
        isMyPick
      }
      other_replacements {
        playerName
        playerNameHindi
        player_role
        playerId
        projectedPoints
        selectionPercent
        mean_projected_points
        playerCredits
        credits
        matchID
        playing_xi
        teamID
        teamName
        captain
        vice_captain
        isMyPick
      }
      locked_players {
        playerName
        playerNameHindi
        player_role
        playerId
        projectedPoints
        selectionPercent
        mean_projected_points
        playerCredits
        credits
        matchID
        playing_xi
        teamID
        teamName
        captain
        vice_captain
        isMyPick
      }
    }
  }
`

export const CRICLYTICS_FORM_INDEX = gql`
  query getFormIndex($matchID: String!) {
    getFormIndex(matchID: $matchID) {
      teamOne {
        name
        bowlers {
          playerID
          playerName
          BowlER
          OverallForm
          BowlAvg
          BowlSR
        }
        batsmen {
          playerID
          playerName
          BatSR
          BatAvg
          OverallForm
        }
        keepers {
          playerID
          playerName
          BatSR
          BatAvg
          OverallForm
        }
        allRounders {
          playerID
          playerName
          BatSR
          BatAvg
          OverallForm
          BowlAvg
        }
      }
      teamTwo {
        name
        bowlers {
          playerID
          playerName
          BowlER
          OverallForm
          BowlAvg
          BowlSR
        }
        batsmen {
          playerID
          playerName
          BatSR
          BatAvg
          OverallForm
        }
        keepers {
          playerID
          playerName
          BatSR
          BatAvg
          OverallForm
        }
        allRounders {
          playerID
          playerName
          BatSR
          BatAvg
          OverallForm
          BowlAvg
        }
      }
    }
  }
`

export const GET_ALL_USER_TEAM = gql`
  query getAllUserTeam($matchID: String!, $token: String) {
    getUserAllFrcTeams(matchID: $matchID, token: $token) {
      ffCode
      matchID
      teams {
        teamName
        leagueType
        selectCriteria
        ffCode
        team {
          isMyPick
          captain
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          playerId
          playerName
          playerNameHindi
          projectedPoints
          playing_xi
        }
      }
    }
  }
`

export const GET_ALL_USER_TEAM_AXIOS = `
query getAllUserTeam($matchID:String!,$token:String) {
  getUserAllFrcTeams(matchID:$matchID,token:$token) {
   ffCode
    matchID
    teams{
      teamName
      leagueType
      selectCriteria
      ffCode
      team{
         	isMyPick
					captain
					mean_projected_points
					vice_captain
					credits
					playerCredits
					matchID
					teamID
					teamName
					player_role
					selectionPercent
					playerId
					playerName
          playerNameHindi
					projectedPoints
					playing_xi
      }
    }
  }
}`

export const CREATE_AND_UPDATE_USER_TEAM = gql`
  mutation createAndUpdateUserTeam(
    $team: frcTeams
    $matchID: String
    $token: String
  ) {
    createAndUpdateUserTeam(team: $team, matchID: $matchID, token: $token) {
      ffCode
      code
      message
    }
  }
`

export const EDIT_MY_FANTASY_TEAM_NAME = gql`
  mutation editMyFantasyTeamName(
    $matchID: String
    $newTeamName: String
    $oldTeamName: String
    $token: String
  ) {
    editMyFantasyTeamName(
      matchID: $matchID
      newTeamName: $newTeamName
      oldTeamName: $oldTeamName
      token: $token
    )
  }
`

export const STAT_HUB_STADIUM = gql`
  query stadiumHub($matchID: String!) {
    stadiumHub(matchID: $matchID) {
      matchID
      compType
      venueName
      venueNameInHindi
      cityInHindi
      venueID
      venueNameInHindi
      cityInHindi
      characteristics {
        pitchType
        wicketSplit
        inningsWinSplit
        weather
      }
      characteristicsInHindi {
        pitchType
        wicketSplit
        inningsWinSplit
        weather
      }
      recentMatches {
        homeTeam
        awayTeam
        homeTeamID
        awayTeamID
        winnerTeam
        matchResult
        matchDate
        venue
        matchID
        matchResultHindi
        venueHindi
      }
      groundStats {
        firstBattingWinPercent
        avgFirstInningScore
        pacerWickets
        paceWicketPercent
        spinnerWickets
        spinWicketPercent
        totalMatches
      }
      playerRecordBatting {
        battingAverage {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          battingAverage
          playerBallsFaced
          totalMatches
        }
        battingStrikeRate {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          battingStrikeRate
          playerBallsFaced
          totalMatches
        }
        totalFours {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          totalFours
          playerBallsFaced
          totalMatches
        }
        totalSixes {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          totalSixes
          playerBallsFaced
          totalMatches
        }
        totalRuns {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          playerBallsFaced
          totalMatches
        }
        fifties {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          fifties
          playerBallsFaced
          totalMatches
        }
        hundreds {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          hundreds
          playerBallsFaced
          totalMatches
        }
      }
      playerRecordBowling {
        totalWickets {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
        }
        bowlingAverage {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          bowlingAverage
        }
        bowlingStrikeRate {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          bowlingStrikeRate
        }
        economy {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          economy
        }
        threeWickets {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          threeWickets
        }
        fiveWickets {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          fiveWickets
        }
      }
    }
  }
`

// export const STAT_HUB_PLAYER = gql`
// query playerHub($matchID: String!){
//   playerHub(matchID:$matchID){
//     matchID
//     compType
//     faceOffs{
//       playerID
//       playerSkill
//       teamID
//       fullName
//       name
//       statsHubPlayerbatting{
//         commonBatPosition
//         battingAverage
//         battingStrikeRate
//         battingFoursSixes
//         battingRunsInnings
// 				battingHighestScore
//       }
// 			statsHubPlayerbowling{
//         bowlingWickets
//         bowlingStrikeRate
//         bowlingEconomyRate
//         bowlingBestBowlingTotalSpell
//       }
//       lastFiveMatches{
//         oppTeamName
//         oppTeamID
//         batting_stats
//         bowling_stats
//         points
//       }
//     }
//     recentForm{
//       playerID
//       playerSkill
//       teamID
//       fullName
//       name
//       statsHubPlayerbatting{
//         commonBatPosition
//         battingAverage
//         battingStrikeRate
//         battingFoursSixes
//         battingRunsInnings
// 				battingHighestScore
//       }
// 			statsHubPlayerbowling{
//         bowlingWickets
//         bowlingStrikeRate
//         bowlingEconomyRate
//         bowlingBestBowlingTotalSpell
//       }
//       lastFiveMatches{
//         oppTeamName
//         oppTeamID
//         batting_stats
//         bowling_stats
//         points
//       }
//     }
// }
// }
// `;

export const STAT_HUB_PLAYER_V2 = gql`
  query playerHub($matchID: String!) {
    playerHub(matchID: $matchID) {
      matchID
      compType
      awayTeamID
      awayTeamName
      awayTeamShortName
      homeTeamID
      homeTeamName
      homeTeamShortName
      recentForm {
        playerID
        playerSkill
        teamID
        teamShortName
        teamName
        awayTeamID
        awayTeamShortName
        awayTeamName
        fullName
        name
        playerNameHindi
        avgFantasyPoints
        playerCredits
        threatOppPlayerID
        highLowFlag
        threat
        strength {
          stmt1
          stmt2
          stmt3
        }
        weakness {
          stmt1
          stmt2
          stmt3
        }
        strengthInHindi {
          stmt1
          stmt2
          stmt3
        }
        weaknessInHindi {
          stmt1
          stmt2
          stmt3
        }
        threatInHindi
        statsHubPlayerbatting {
          commonBatPosition
          battingAverage
          battingStrikeRate
          battingFoursSixes
          battingRunsInnings
          battingHighestScore
        }
        statsHubPlayerbowling {
          bowlingWickets
          bowlingStrikeRate
          bowlingEconomyRate
          bowlingBestBowlingTotalSpell
        }
        lastFiveMatches {
          oppTeamName
          oppTeamID
          batting_stats
          bowling_stats
          points
          dta_flag
        }
        battingDetails {
          maxAvg
          maxWickets
          maxEconomy
          battingType {
            types
            economyRate
            battingAvg
            wickets
          }
        }
        bowlingDetails {
          maxAvg
          maxDismissals
          maxSR
          bowlingType {
            Dismissals
            bowlingAvg
            bowlingSR
            types
          }
        }
        performanceIndicator {
          overallRPI
          overallWPI
          teamRPI
          teamWPI
          stadiumRPI
          stadiumWPI
          atStadium
          atStadiumHindi
          vsTeam
        }
      }
    }
  }
`

export const STAT_HUB_TEAM = gql`
  query teamHub($matchID: String!) {
    teamHub(matchID: $matchID) {
      matchID
      compType
      teamRecords {
        matchType
        homeTeamID
        awayTeamID
        homeTeamShortName
        awayTeamShortName

        overAllRecord {
          homeData {
            teamRuns
            avgTeamRuns
            avgWickets
            wins
            highestScore
            lowestScore
            totalMatches
          }
          awayData {
            teamRuns
            avgTeamRuns
            avgWickets
            wins
            highestScore
            lowestScore
            totalMatches
          }
        }
        battingFirst {
          homeData {
            teamRuns
            avgTeamRuns
            avgWickets
            wins
            highestScore
            lowestScore
            totalMatches
          }
          awayData {
            teamRuns
            avgTeamRuns
            avgWickets
            wins
            highestScore
            lowestScore
            totalMatches
          }
        }
        battingSecond {
          homeData {
            teamRuns
            avgTeamRuns
            avgWickets
            wins
            highestScore
            lowestScore
            totalMatches
          }
          awayData {
            teamRuns
            avgTeamRuns
            avgWickets
            wins
            highestScore
            lowestScore
            totalMatches
          }
        }
      }
      lastFiveH2H {
        homeTeam
        awayTeam
        homeTeamID
        awayTeamID
        winnerTeam
        matchResult
        matchResultHindi
        matchDate
        venue
        venueHindi
        matchID
        winnerTeamID
        matchStatus
        matchType
      }
      lastFiveHome {
        homeTeam
        awayTeam
        homeTeamID
        awayTeamID
        winnerTeam
        matchResultHindi
        venueHindi
        matchResult
        matchResultHindi
        venueHindi
        matchDate
        venue
        matchID
        winnerTeamID
        matchStatus
        matchType
      }
      lastFiveAway {
        homeTeam
        awayTeam
        homeTeamID
        awayTeamID
        winnerTeam
        matchResultHindi
        matchResult
        venueHindi
        matchDate
        venue
        matchID
        winnerTeamID
        matchStatus
        matchType
      }
      playerRecordBatting {
        battingAverage {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          battingAverage
          playerBallsFaced
          totalMatches
        }
        battingStrikeRate {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          battingStrikeRate
          playerBallsFaced
          totalMatches
        }
        totalFours {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          totalFours
          playerBallsFaced
          totalMatches
        }
        totalSixes {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          totalSixes
          playerBallsFaced
          totalMatches
        }
        totalRuns {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          playerBallsFaced
          totalMatches
        }
        fifties {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          fifties
          playerBallsFaced
          totalMatches
        }
        hundreds {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          playerRuns
          hundreds
          playerBallsFaced
          totalMatches
        }
      }
      playerRecordBowling {
        totalWickets {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
        }
        bowlingAverage {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          bowlingAverage
        }
        bowlingStrikeRate {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          bowlingStrikeRate
        }
        economy {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          economy
        }
        threeWickets {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          threeWickets
        }
        fiveWickets {
          teamID
          shortName
          playerID
          playerName
          playerNameHindi
          totalWickets
          playerRunsConceeded
          totalMatches
          ballsBowled
          fiveWickets
        }
      }
    }
  }
`

export const GET_FRC_HOME_PAGE = gql`
  query getFRCHomePage {
    getFRCHomePage {
      upcomingmatches {
        matchID
        matchName
        matchNameHindi
        matchStatus
        statusMessage
        isLiveCriclyticsAvailable
        homeTeamID
        awayTeamID
        homeTeamShortName
        awayTeamShortName
        matchNumber
        toss
        matchDateTimeGMT
        tourName
        currentInningsTeamID
        currentInnings
        matchType
        winningTeamID
        city
        cityHindi
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
      }
      livematches {
        matchID
        matchName
        matchNameHindi
        matchStatus
        statusMessage
        statusMessageHindi
        isLiveCriclyticsAvailable
        homeTeamID
        awayTeamID
        homeTeamShortName
        awayTeamShortName
        matchNumber
        toss
        matchDateTimeGMT
        tourName
        currentInningsTeamID
        currentInnings
        matchType
        winningTeamID
        city
        cityHindi
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
      }
      completedmatches {
        matchID
        matchName
        matchNameHindi
        matchStatus
        statusMessage
        statusMessageHindi
        homeTeamID
        isLiveCriclyticsAvailable
        awayTeamID
        currentInningsTeamID
        currentInnings
        matchType
        homeTeamShortName
        awayTeamShortName
        matchNumber
        toss
        matchDateTimeGMT
        tourName
        winningTeamID
        city
        cityHindi
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
      }
    }
  }
`

export const ADVANCED_MATCHUPS = gql`
  query advanceMatchupsById($matchId: String!) {
    advanceMatchupsById(matchId: $matchId) {
      matchId
      advanceMatchUpData {
        player1
        player2
        player1Team
        player2Team
        player1Name
        player2Name
        player1Role
        player2Role
        ballsFaced
        runsScored
        battingSR
        bowlingSR
        status
        Dismissals
      }
    }
  }
`

export const GET_FRC_HOME_PAGE_AXIOS = ` query getFRCHomePage{
  getFRCHomePage{
    upcomingmatches {
      matchID
      matchName
      matchNameHindi
      matchStatus
      statusMessage
      isLiveCriclyticsAvailable
      homeTeamID
      awayTeamID
      homeTeamShortName
      awayTeamShortName
      matchNumber
      toss
      matchDateTimeGMT
      tourName
      currentInningsTeamID
      currentInnings
      matchType
      winningTeamID
      city
      cityHindi
      matchScore{
        teamShortName
        teamID
        teamFullName
        teamScore{
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
    }
    livematches{
      matchID
      matchName
      matchNameHindi
      matchStatus
      statusMessage
      statusMessageHindi
      isLiveCriclyticsAvailable
      homeTeamID
      awayTeamID
      homeTeamShortName
      awayTeamShortName
      matchNumber
      toss
      matchDateTimeGMT
      tourName
      currentInningsTeamID
      currentInnings
      matchType
      winningTeamID
      city
      cityHindi
      matchScore{
        teamShortName
        teamID
        teamFullName
        teamScore{
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
    }
    completedmatches{
      matchID
      matchName
      matchNameHindi
      matchStatus
      statusMessage
      statusMessageHindi
      homeTeamID
      isLiveCriclyticsAvailable
      awayTeamID
      currentInningsTeamID
      currentInnings
      matchType
      homeTeamShortName
      awayTeamShortName
      matchNumber
      toss
      matchDateTimeGMT
      tourName
      winningTeamID
      city
      cityHindi
      matchScore{
        teamShortName
        teamID
        teamFullName
        teamScore{
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
          declared
          folowOn
        }
      }
    }
  }
}
`

export const GET_USER_FRC_TEAM_LIVE_POINTS = gql`
  query getUserFrcTeamLivePoints($matchID: String!, $token: String) {
    getUserFrcTeamLivePoints(matchID: $matchID, token: $token) {
      teamName
      leagueType
      selectCriteria
      ffCode
      teamtotalpoints
      team {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
    }
  }
`

export const MATCH_DATA_INSIDE_FRC_AXIOS = `
  query miniScoreCard($matchID: String!) {
      miniScoreCard(matchID: $matchID){
        data{
          loginEnable
          currentinningsNo
          currentInningteamID
          currentInningsTeamName
          seriesName
          seriesID
          homeTeamName
          awayTeamName
          toss
          homeTeamID
          awayTeamID
          startEndDate
          matchStatus
          matchID
          matchType
          statusMessage
          matchNumber
          venue
          matchResult
          startDate
          playerID
          matchDateTimeGMT
          playerOfTheMatch
          firstInningsTeamID
          secondInningsTeamID
          thirdInningsTeamID
          fourthInningsTeamID
          isCricklyticsAvailable
          isFantasyAvailable
          isLiveCriclyticsAvailable
          isAbandoned
          playing11Status
          probable11Status
          currentDay
          currentSession
          matchScore{
            teamShortName
            teamID
            teamFullName
            teamScore{
              inning
              inningNumber
              battingTeam
              runsScored
              wickets
              overs
              runRate
              battingSide
              teamID
              battingTeamShortName
              declared
              folowOn
            }
          }
        }
      }
  }
`

export const MATCH_DATA_INSIDE_FRC = gql`
  query miniScoreCard($matchID: String!) {
    miniScoreCard(matchID: $matchID) {
      data {
        loginEnable
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        seriesName
        seriesID
        homeTeamName
        awayTeamName
        toss
        homeTeamID
        awayTeamID
        startEndDate
        matchStatus
        matchID
        matchType
        statusMessage
        matchNumber
        venue
        matchResult
        startDate
        playerID
        matchDateTimeGMT
        playerOfTheMatch
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID
        fourthInningsTeamID
        isCricklyticsAvailable
        isFantasyAvailable
        isLiveCriclyticsAvailable
        isAbandoned
        playing11Status
        probable11Status
        currentDay
        currentSession
        matchScore {
          teamShortName
          teamID
          teamFullName
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
            declared
            folowOn
          }
        }
      }
    }
  }
`

export const GET_ALGO_11 = gql`
  query getAlgo11($matchID: String!, $token: String!) {
    getAlgo11(matchID: $matchID, token: $token) {
      saveTeams
      saveTeamCount
      teamtotalpoints
      totalProjectedPoints
      timestamp
      batsman {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
      bowler {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
      keeper {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
      all_rounder {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
    }
  }
`

export const GET_FANTASY_SIX_TEAMS = gql`
  query getFantasySixTeams($matchID: String!, $token: String) {
    getFantasySixTeams(matchID: $matchID, token: $token) {
      teamtotalpoints
      fantasy_teamName
      teamTagline
      timestamp
      totalProjectedPoints
      all_rounder {
        playerName
        playerNameHindi
        credits
        playerId
        captain
        vice_captain
        player_role
        teamID
        totalPoints
        projectedPoints
      }
      bowler {
        playerName
        playerNameHindi
        credits
        playerId
        captain
        vice_captain
        teamID
        totalPoints
        player_role
        projectedPoints
      }
      batsman {
        playerName
        playerNameHindi
        credits
        playerId
        totalPoints
        captain
        vice_captain
        teamID
        player_role
        projectedPoints
      }

      keeper {
        playerName
        playerNameHindi
        credits
        totalPoints
        playerId
        captain
        vice_captain
        player_role
        teamID
        projectedPoints
      }
    }
  }
`

export const DUGOUT = gql`
  query dugoutDetails($matchID: String!) {
    dugoutDetails(matchID: $matchID) {
      isDisplayDugout
      status
      midChart
      bowlerColor
      batsmanColor
      matchBatsman {
        playerID
        playerName
        playerMatchRuns
        playerMatchBalls
        playerMatchSixes
        playerMatchFours
        battingStyle
        playerMatchStrikeRate
        isNotOut
        zad {
          runs
          zadval
        }
      }
      completed {
        matchBatsman {
          playerID
          playerName
          playerMatchRuns
          playerMatchBalls
          playerMatchSixes
          playerMatchFours
          battingStyle
          playerMatchStrikeRate
          isNotOut
          zad {
            runs
            zadval
          }
        }
        scores {
          ones
          twos
          threes
          fours
          sixs
        }
        total
        battingTeamID
        battingTeamShortName
        teamColor
      }

      StatNuggets {
        isDisplay
        batsman {
          playerID
          playerName
          powerplay
          middle_overs
          death_overs
          against_spinner
          against_pacer
        }
        bowler {
          playerID
          playerName
          powerplay
          middle_overs
          death_overs
          against_left_handed_batsman
          against_right_handed_batsman
        }
      }

      matchBowler {
        playerID
        playerName
        playerRunsConceeded
        playerWicketsTaken
        playerOversBowled
        playerEconomyRate
        over {
          isBall
          runs
          type
          wicket
        }
      }
      matchUps {
        playerID
        playerName
        ballfaced
        runsscored
        boundaries
      }
      nextBatsmans {
        playerID
        playerName
        playerMatchRuns
        playerMatchBalls
        playerMatchSixes
        playerMatchFours
        playerMatchStrikeRate
        playerMatchAvg
      }
      last18balls {
        wickets
        runs
        teamID
        teamShortName
      }
      runRateProjection {
        rr
        oversRemaining
        runs
      }
    }
  }
`

export const WWTW = gql`
  query WWTW {
    getWWTW {
      WWTWData {
        matchID
        matchName
        venue
        firstInningsTeamID
        firstInningsScore
        firstInningsOver
        firstInningsShortName
        firstInningsName
        secondInningsTeamID
        secondInningsScore
        secondInningsOver
        secondInningsShortName
        secondInningsName
        result
        SuperOverFlag
        winningTeamID
      }
    }
  }
`

export const PLAYER_CAREER_STATS_DATA = gql`
  query getPlayersProfileV2($playerID: String!) {
    getPlayersProfileV2(playerID: $playerID) {
      name
      playerID
      birthPlace
      dob
      description
      battingStyle
      bowlingStyle
      headShotImage
      actionShotImage
      similarplayers {
        playerID
        headShotImage
        playerName
        similarity
      }
      international {
        Test {
          basicdetails {
            matches
            innings
          }
          debutMatch {
            matchID
            date
            against
            againstID
            format
          }
          lastMatch {
            matchID
            date
            against
            againstID
            format
          }
          battingRecord {
            notOuts
            runs
            ballsFaced
            hundreds
            fifties
            fours
            sixes
            average
            strikeRate
            innings
            hundredsfifties
            foursix
            format
            matches
          }
          bowlingRecord {
            overs
            wickets
            ballsBowled
            average
            strikeRate
            economyRate
            fiveWicketHauls
            innings
            bestBowling
            fivetenWicketHauls
          }
        }
        Odi {
          basicdetails {
            matches
            innings
          }
          debutMatch {
            matchID
            date
            against
            againstID
            format
          }
          lastMatch {
            matchID
            date
            against
            againstID
            format
          }
          battingRecord {
            notOuts
            runs
            ballsFaced
            hundreds
            fifties
            fours
            sixes
            average
            strikeRate
            innings
            hundredsfifties
            foursix
            format
            matches
          }
          bowlingRecord {
            overs
            wickets
            ballsBowled
            average
            strikeRate
            economyRate
            fiveWicketHauls
            innings
            bestBowling
            fivetenWicketHauls
          }
        }
        T20 {
          basicdetails {
            matches
            innings
          }
          debutMatch {
            matchID
            date
            against
            againstID
            format
          }
          lastMatch {
            matchID
            date
            against
            againstID
            format
          }
          battingRecord {
            notOuts
            runs
            ballsFaced
            hundreds
            fifties
            fours
            sixes
            average
            strikeRate
            innings
            hundredsfifties
            foursix
            format
            matches
          }
          bowlingRecord {
            overs
            wickets
            ballsBowled
            average
            strikeRate
            economyRate
            fiveWicketHauls
            innings
            bestBowling
            fivetenWicketHauls
          }
        }
      }
      Ipl {
        basicdetails {
          matches
          innings
        }
        debutMatch {
          matchID
          date
          against
          againstID
          format
        }
        lastMatch {
          matchID
          date
          against
          againstID
          format
        }
        battingRecord {
          notOuts
          runs
          ballsFaced
          hundreds
          fifties
          fours
          sixes
          average
          strikeRate
          innings
          hundredsfifties
          foursix
          format
          matches
        }
        bowlingRecord {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          fivetenWicketHauls
        }
      }
      role
      fullName
      playerIPLTeam
      playerIPLBid
      battingStats {
        notOuts
        runs
        ballsFaced
        hundreds
        fifties
        fours
        sixes
        average
        recForm {
          a
          b
        }
        color
        strikeRate
        innings
        hundredsfifties
        foursix
        format
        matches
      }
      bowlingStats {
        format
        matches
        overs
        wickets
        ballsBowled
        average
        recForm {
          a
          b
        }
        color
        strikeRate
        economyRate
        fiveWicketHauls
        innings
        bestBowling
        fivetenWicketHauls
      }
      fieldingStats {
        format
        catches
        stupms
        runOuts
      }
    }
  }
`

export const PLAYER_FANTASY_MATCH = gql`
  query getPlayerFantasyMatches($playerID: String!) {
    getPlayerFantasyMatches(playerID: $playerID) {
      t20 {
        opposition
        battingStats
        bowlingStats
        points
        isDreamTeam
        matchType
      }
      test {
        opposition
        battingStats
        bowlingStats
        points
        isDreamTeam
        matchType
      }
      odi {
        opposition
        battingStats
        bowlingStats
        points
        isDreamTeam
        matchType
      }
      t20Domestic {
        opposition
        battingStats
        bowlingStats
        points
        isDreamTeam
        matchType
      }
      all {
        opposition
        battingStats
        bowlingStats
        points
        isDreamTeam
        matchType
      }
    }
  }
`

export const PLAYER_MATCH = gql`
  query getPlayerMatches($playerID: String!) {
    getPlayerMatches(playerID: $playerID) {
      all {
        homeTeamShortName
        awayTeamShortName
        homeTeamID
        awayTeamID
        matchDate
        matchID
        matchType
        opposition
        battingStats
        bowlingStats
      }
      t20 {
        homeTeamShortName
        awayTeamShortName
        homeTeamID
        awayTeamID
        matchDate
        matchID
        matchType
        opposition
        battingStats
        bowlingStats
      }
      odi {
        homeTeamShortName
        awayTeamShortName
        homeTeamID
        awayTeamID
        matchDate
        matchID
        matchType
        opposition
        battingStats
        bowlingStats
      }
      test {
        homeTeamShortName
        awayTeamShortName
        homeTeamID
        awayTeamID
        matchDate
        matchID
        matchType
        opposition
        battingStats
        bowlingStats
      }
      t20Domestic {
        homeTeamShortName
        awayTeamShortName
        homeTeamID
        awayTeamID
        matchDate
        matchID
        matchType
        opposition
        battingStats
        bowlingStats
      }
    }
  }
`

export const PLAYER_CAREER_DETAILS_AXIOS = `
query getPlayersProfileV2($playerID:String!) {
  getPlayersProfileV2(playerID: $playerID) {
    name
    playerID
    birthPlace
    dob
    headShotImage
    actionShotImage
    description
    battingStyle
    bowlingStyle
    role
    teamID
  }
  }`

//   export const PLAYER_CAREERSTATS_DETAILS_GQL = gql`
// query getPlayersProfileV2($playerID:String!) {
//   getPlayersProfileV2(playerID: $playerID) {
//     name
//     playerID
//     birthPlace
//     dob
//     description
//     battingStyle
//     bowlingStyle
//     role
//     teamID
//   }
//   }`

export const PLAYER_DISCOVERY_V2_AXIOS = `
  query playerDiscoveryV2($teamShortName:String){
    playerDiscoveryV2(teamShortName:$teamShortName){
      ID
      BP
      V
      ON
      PN
      TI
      PS
      ASI
      HSI
    }
   }`

export const PLAYER_DISCOVERY_V2 = gql`
  query playerDiscoveryV2($teamShortName: String) {
    playerDiscoveryV2(teamShortName: $teamShortName) {
      ID
      BP
      V
      ON
      PN
      TI
      PS
      ASI
      HSI
    }
  }
`

export const PLAYER_DISCOVERY_SEARCH = `
  query playerSearch($name:String){
    playerSearch(name:$name){
      playerID
      name
      fullName
      birthPlace
      playerRank
      
      
    }
   }`

export const PLAYER_COMPARISION = gql`
  query getPlayersComarisionV2($playerID1: String, $playerID2: String) {
    getPlayersComarisionV2(playerID1: $playerID1, playerID2: $playerID2) {
      player1
      role1
      player2
      role2
      headShotImagePlayer1
      headShotImagePlayer2
      otherSimilarPlayers {
        playerID
        playerName
        similarity
        headShotImage
      }
      battingStyle {
        T20I {
          runs
          playerName
          recForm {
            a
            b
          }
          color
          strikeRate
          innings
          hundredsfifties
          foursix
          average
          format
          matches
        }
        ODI {
          runs
          playerName
          recForm {
            a
            b
          }
          color
          strikeRate
          innings
          hundredsfifties
          average
          foursix
          format
          matches
        }
        Test {
          runs
          playerName
          recForm {
            a
            b
          }
          color
          strikeRate
          innings
          average
          hundredsfifties
          foursix
          format
          matches
        }
        T20s {
          runs
          playerName
          recForm {
            a
            b
          }
          color
          strikeRate
          average
          innings
          hundredsfifties
          foursix
          format
          matches
        }
        FC {
          runs
          playerName
          recForm {
            a
            b
          }
          color
          strikeRate
          innings
          hundredsfifties
          average
          foursix
          format
          matches
        }
        listA {
          runs
          playerName
          recForm {
            a
            b
          }
          color
          strikeRate
          innings
          hundredsfifties
          foursix
          format
          matches
        }
      }
      bowlingStyle {
        T20I {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          recForm {
            a
            b
          }
          color
          fivetenWicketHauls
          format
          matches
          playerID
          playerName
        }
        ODI {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          recForm {
            a
            b
          }
          color
          fivetenWicketHauls
          format
          matches
          playerID
          playerName
        }
        Test {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          recForm {
            a
            b
          }
          color
          fivetenWicketHauls
          format
          matches
          playerID
          playerName
        }
        T20s {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          recForm {
            a
            b
          }
          color
          fivetenWicketHauls
          format
          matches
          playerID
          playerName
        }
        FC {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          recForm {
            a
            b
          }
          color
          fivetenWicketHauls
          format
          matches
          playerID
          playerName
        }
        listA {
          overs
          wickets
          ballsBowled
          average
          strikeRate
          economyRate
          fiveWicketHauls
          innings
          bestBowling
          recForm {
            a
            b
          }
          color
          fivetenWicketHauls
          format
          matches
          playerID
          playerName
        }
      }
    }
  }
`

export const TEAM_STATS_V2 = gql`
  query teamStats($teamID: String) {
    teamStats(teamID: $teamID) {
      hideTabs
      odi {
        matchesWon
        matchesLost
        matchesTied
        matchesPlayed
        winPercentage
        mostRuns {
          playerName
          playerID
          matchesPlayed
          runs
          average
        }
        mostWickets {
          playerName
          matchesPlayed
          wicket
          economy
          playerID
        }
        bestScore {
          playerName
          matchesPlayed
          runs
          average
          playerID
        }
        bestFigures {
          playerName
          matchesPlayed
          runs
          average
          playerID
        }
      }
      test {
        matchesWon
        matchesTied
        matchesLost
        matchesPlayed
        winPercentage
        mostRuns {
          playerName
          playerID
          matchesPlayed
          runs
          average
        }
        mostWickets {
          playerName
          playerID
          matchesPlayed
          playerID
          wicket
          economy
        }
        bestScore {
          playerName
          playerID
          matchesPlayed
          runs
          average
        }
        bestFigures {
          playerName
          playerID
          matchesPlayed
          runs
          average
        }
      }
      t20 {
        matchesWon
        matchesLost
        matchesPlayed
        winPercentage
        matchesTied
        mostRuns {
          playerName
          matchesPlayed
          playerID
          runs
          average
        }
        mostWickets {
          playerName
          matchesPlayed
          wicket
          economy
          playerID
        }
        bestScore {
          playerName
          matchesPlayed
          runs
          average
          playerID
        }
        bestFigures {
          playerName
          matchesPlayed
          runs
          average
          playerID
        }
      }
    }
  }
`
export const RECENT_FORM = gql`
  query homepageRecentForm($homeTeamID: String!, $awayTeamID: String!) {
    homepageRecentForm(homeTeamID: $homeTeamID, awayTeamID: $awayTeamID) {
      homeTeamID
      awayTeamID
      homeTeamName
      awayTeamName
      homeTeamData {
        matchResult
        type
        winningTeamID
        team1
        team2
      }
      awayTeamData {
        matchResult
        type
        winningTeamID
        team1
        team2
      }
    }
  }
`
export const HOME_RECENT_FORM = gql`
  query homepageRecentForm(
    $homeTeamID: String
    $awayTeamID: String
    $format: String
  ) {
    homepageRecentForm(
      homeTeamID: $homeTeamID
      awayTeamID: $awayTeamID
      format: $format
    ) {
      homeTeamID
      awayTeamID
      homeTeamName
      awayTeamName
      homeTeamData {
        matchResult
        type
        winningTeamID
        team1
        team2
      }
      awayTeamData {
        matchResult
        type
        winningTeamID
        team1
        team2
      }
    }
  }
`

export const TEAM_RECENT_FORMAT_V2 = gql`
  query teamRecentForm($teamID: String) {
    teamRecentForm(teamID: $teamID) {
      hideTabs
      odi {
        winLost
        scoreCard {
          playerID
          playerOfTheMatch
          seriesName
          win
          venue
          matchResult
          matchType
          matchNumber
          matchStatus
          winningTeamID
          startDate
          matchID
          matchScore {
            teamShortName
            teamID
            teamFullName
            teamScore {
              inning
              inningNumber
              battingTeam
              runsScored
              wickets
              overs
              runRate
              battingSide
              teamID
              battingTeamShortName
              declared
              folowOn
            }
          }
        }
      }
      t20 {
        winLost
        scoreCard {
          playerID
          playerOfTheMatch
          seriesName
          win
          venue
          matchResult
          matchType
          matchNumber
          matchStatus
          winningTeamID
          startDate
          matchID
          matchScore {
            teamShortName
            teamID
            teamFullName
            teamScore {
              inning
              inningNumber
              battingTeam
              runsScored
              wickets
              overs
              runRate
              battingSide
              teamID
              battingTeamShortName
              declared
              folowOn
            }
          }
        }
      }
      test {
        winLost
        scoreCard {
          playerID
          playerOfTheMatch
          seriesName
          win
          venue
          matchResult
          matchType
          matchNumber
          matchStatus
          winningTeamID
          startDate
          matchID
          matchScore {
            teamShortName
            teamID
            teamFullName
            teamScore {
              inning
              inningNumber
              battingTeam
              runsScored
              wickets
              overs
              runRate
              battingSide
              teamID
              battingTeamShortName
              declared
              folowOn
            }
          }
        }
      }
    }
  }
`

export const TEAM_PLAYER = gql`
  query teamPlayer($teamID: String!) {
    teamPlayersV2(teamID: $teamID) {
      hideTabs
      odiSquad {
        playerName
        playerID
        playerRole
      }
      testSquad {
        playerID
        playerName
        playerRole
      }
      t20Squad {
        playerID
        playerName
        playerRole
      }
    }
  }
`

export const TEAM_SCHEDULE_V2 = gql`
  query teamsScheduleV2($teamID: String!) {
    teamsScheduleV2(teamID: $teamID) {
      hideTabs
      upcomingMatches {
        matchID
        awayTeamID
        awayTeamShortName
        homeTeamID
        homeTeamShortName
        matchDateTimeIST
        matchNumber
        matchType
        venue
        matchName
      }
      scheduleMatches {
        matchID
        matchDateTimeIST
        matchStatus
        compType
      }
    }
  }
`

export const GET_FRC_SELECTION_COMPOSITION = gql`
  query getPlayerSelectionComposition {
    getPlayerSelectionComposition {
      gameType
      totalPlayersCount
      credits
      players {
        role
        roleName
        roleShortName
        min
        max
      }
    }
  }
`
export const IPL_POLLING_AXIOS = `
query featuredMatches {
	featurematch {
		IPLpolling {
			name
			isPolling
			display
			isAuctionStarted
      isCompleted
      
		}
}
}
`

export const SHORTS_API_AXIOS = `
query getcricShortssWithPagination{
  getcricShortssWithPagination(cricShortsId:"cricShorts7-test7-1639572646934", page:0){
  cricShortsId
  title
  image
  url
  content
  author
  publishedAt
  createdAt
}
}
`

export const SHORTS_API = gql`
  query getcricShortssWithPagination {
    getcricShortssWithPagination(
      cricShortsId: "cricShorts7-test7-1639572646934"
      page: 0
    ) {
      cricShortsId
      title
      image
      url
      content
      author
      publishedAt
      createdAt
    }
  }
`

export const A23playerStatsByIDAPI = gql`
  query A23playerStatsByID($matchID: String, $playerID: String) {
    A23playerStatsByID(matchID: $matchID, playerID: $playerID) {
      playerID
      name
      fullName
      highLowFlag
      awayTeamID
      awayTeamName
      avgFantasyPoints
      awayTeamShortName
      playerCredits
      playerNameHindi
      playerSkill
      teamID
      teamName
      teamShortName
      threat
      threatInHindi
      threatOppPlayerID
      battingDetails {
        maxAvg
        maxEconomy
        maxWickets
        battingType {
          battingAvg
          economyRate
          types
          wickets
        }
      }

      bowlingDetails {
        maxAvg
        maxDismissals
        maxSR
        bowlingType {
          Dismissals
          bowlingAvg
          bowlingSR
          types
        }
      }

      lastFiveMatches {
        batting_stats
        bowling_stats
        dta_flag
        oppTeamID
        oppTeamName
        points
      }

      performanceIndicator {
        atStadium
        atStadiumHindi
        overallRPI
        overallWPI
        stadiumRPI
        stadiumWPI
        teamRPI
        teamWPI
        vsTeam
      }

      statsHubPlayerbatting {
        battingAverage
        battingFoursSixes
        battingHighestScore
        battingRunsInnings
        battingStrikeRate
        commonBatPosition
      }
      statsHubPlayerbowling {
        bowlingWickets
        bowlingStrikeRate
        bowlingEconomyRate
        bowlingBestBowlingTotalSpell
      }
      strength {
        stmt1
        stmt2
        stmt3
      }

      weakness {
        stmt1
        stmt2
        stmt3
      }

      weaknessInHindi {
        stmt1
        stmt2
        stmt3
      }
    }
  }
`

export const HOME_PAGE_FORM_INDEX = gql`
  query homepageFormIndex($matchID: String!) {
    homepageFormIndex(matchID: $matchID) {
      team1Name
      team2Name
      team1Data {
        playerName
        playerID
        playerRole
        Form
        avg
        sr
      }
      team2Data {
        playerName
        playerID
        playerRole
        Form
        avg
        sr
      }
    }
  }
`

export const PREDICTION_LANDING = gql`
  query predictionHome {
    predictionHome {
      superOver
      userNotification
      matchenddate
      phaseOfInningFlag
      isHomeMatchUpade
      content
      displayFeatureMatchScoreCard
      seriesID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      homeTeamName
      awayTeamName
      awayTeamID
      homeTeamID
      toss
      startDate
      matchStatus
      matchResult
      matchID
      matchType
      statusMessage
      matchNumber
      venue
      currentDay
      currentSession
      playerOfTheMatch
      playerID
      winningTeamID
      playerOfTheMatchdDetails {
        playerID
        playerTeamID
        playerName
        batsman {
          batsmanBalls
          batsmanRuns
          isNotOut
        }
        bowler {
          bowlerWickets
          bowlerConceeded
          bowlerOvers
        }
      }
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      fourthInningsTeamID
      isCricklyticsAvailable
      isLiveCriclyticsAvailable
      playing11Status
      probable11Status
      isAbandoned
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      matchScore {
        teamShortName
        teamID
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          declared
          folowOn
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
        }
      }
      totalParticipants
    }
  }
`

export const PREDICTION_LANDING_AXIOS = `
  query predictionHome {
    predictionHome {
      superOver
      userNotification
      matchenddate
      phaseOfInningFlag
      isHomeMatchUpade
      content
      displayFeatureMatchScoreCard
      seriesID
      currentinningsNo
      currentInningteamID
      currentInningsTeamName
      seriesName
      homeTeamName
      awayTeamName
      awayTeamID
      homeTeamID
      toss
      startDate
      matchStatus
      matchResult
      matchID
      matchType
      statusMessage
      matchNumber
      venue
      currentDay
      currentSession
      playerOfTheMatch
      playerID
      winningTeamID
      playerOfTheMatchdDetails {
        playerID
        playerTeamID
        playerName
        batsman {
          batsmanBalls
          batsmanRuns
          isNotOut
        }
        bowler {
          bowlerWickets
          bowlerConceeded
          bowlerOvers
        }
      }
      firstInningsTeamID
      secondInningsTeamID
      thirdInningsTeamID
      fourthInningsTeamID
      isCricklyticsAvailable
      isLiveCriclyticsAvailable
      playing11Status
      probable11Status
      isAbandoned
      teamsWinProbability {
        homeTeamShortName
        homeTeamPercentage
        awayTeamShortName
        awayTeamPercentage
        tiePercentage
      }
      matchScore {
        teamShortName
        teamID
        teamScore {
          inning
          inningNumber
          battingTeam
          runsScored
          wickets
          declared
          folowOn
          overs
          runRate
          battingSide
          teamID
          battingTeamShortName
        }
      }
      totalParticipants
    }
  }
`

export const PREDICTION_CONTEST = gql`
  query predictionContest($matchID: String!, $token: String) {
    predictionContest(matchID: $matchID, token: $token) {
      matchDetails {
        coverageLevel
        superOver
        phaseOfInningFlag
        isHomeMatchUpade
        content
        displayFeatureMatchScoreCard
        seriesID
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        seriesName
        homeTeamName
        awayTeamName
        awayTeamID
        homeTeamID
        toss
        startDate
        matchStatus
        matchResult
        matchID
        matchType
        statusMessage
        matchNumber
        venue
        currentDay
        currentSession
        playerOfTheMatch
        playerID
        winningTeamID
        playerOfTheMatchdDetails {
          playerID
          playerTeamID
          playerName
          batsman {
            batsmanBalls
            batsmanRuns
            isNotOut
          }
          bowler {
            bowlerWickets
            bowlerConceeded
            bowlerOvers
          }
        }
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID
        fourthInningsTeamID
        isCricklyticsAvailable
        isLiveCriclyticsAvailable
        playing11Status
        probable11Status
        isAbandoned
        matchScore {
          teamShortName
          teamID
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            declared
            folowOn
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
          }
        }
        totalParticipants
      }
      userPredictions {
        remainingCoins
        matchID
        matchDateTimeGMT
        totalWinning
        rank
        totalPredictions
        PrematchWinOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        TotalRunsOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        PowerPlayWinOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        OpeningPartnershipWinOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        BattingMilestonesOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        OpeningPartnershipWinOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        WicketTypeOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        EconomicalBowlerOddsHomeTeam {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        EconomicalBowlerOddsAwayTeam {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        TopScorerOddsHomeTeam {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        TopScorerOddsAwayTeam {
          betCoins
          id
          winCoins
          status
          selectedName
        }
        TossOdds {
          betCoins
          id
          winCoins
          status
          selectedName
        }
      }
      predictionOdds {
        PrematchWinOdds {
          question
          homeTeam
          awayTeam
        }
        TossOdds {
          question
          homeTeam
          awayTeam
        }
        TotalRunsOdds {
          homeTeam
          awayTeam
          question
          runs
          over
          under
        }
        PowerPlayWinOdds {
          question
          homeTeam
          awayTeam
        }
        OpeningPartnershipWinOdds {
          question
          homeTeam
          awayTeam
        }
        BattingMilestonesOdds {
          question
          homeTeam
          awayTeam
          Yes
          No
        }
        WicketTypeOdds {
          question
          caught
          runout
          stumped
          bowled
          lbw
          others
        }
        EconomicalBowlerOddsHomeTeam {
          question
          team {
            playerID
            playerName
            Odds
          }
        }
        EconomicalBowlerOddsAwayTeam {
          question
          team {
            playerID
            playerName
            Odds
          }
        }
        TopScorerOddsHomeTeam {
          question
          team {
            playerID
            playerName
            Odds
          }
        }
        TopScorerOddsAwayTeam {
          question
          team {
            playerID
            playerName
            Odds
          }
        }
      }
    }
  }
`

export const PREDICTION_CONTEST_MUTATION = gql`
  mutation postUserPredictions(
    $matchID: String!
    $type: String!
    $betCoins: Int!
    $id: String
    $token: String
  ) {
    postUserPredictions(
      matchID: $matchID
      type: $type
      betCoins: $betCoins
      id: $id
      token: $token
    ) {
      statusCode
      message
      error
      remainingCoins
      matchID
      matchDateTimeGMT
      totalWinning
      rank
      PrematchWinOdds {
        betCoins
        id
        winCoins
      }
      TotalRunsOdds {
        betCoins
        id
        winCoins
      }
      PowerPlayWinOdds {
        betCoins
        id
        winCoins
      }
      OpeningPartnershipWinOdds {
        betCoins
        id
        winCoins
      }
      BattingMilestonesOdds {
        betCoins
        id
        winCoins
      }
      WicketTypeOdds {
        betCoins
        id
        winCoins
      }
      EconomicalBowlerOddsHomeTeam {
        betCoins
        id
        winCoins
      }
      EconomicalBowlerOddsAwayTeam {
        betCoins
        id
        winCoins
      }
      TopScorerOddsHomeTeam {
        betCoins
        id
        winCoins
      }
      TopScorerOddsAwayTeam {
        betCoins
        id
        winCoins
      }
    }
  }
`

export const PREDICTION_PROFILE = gql`
  query predictionProfile($year: String, $month: String, $token: String) {
    predictionProfile(year: $year, month: $month, token: $token) {
      name
      level
      totalEarnings
      contestEntered
      photo
      matches {
        earnings
        rightPrediction
        totalPredictions
        matchDetails {
          superOver
          matchenddate
          phaseOfInningFlag
          isHomeMatchUpade
          content
          displayFeatureMatchScoreCard
          seriesID
          currentinningsNo
          currentInningteamID
          currentInningsTeamName
          seriesName
          homeTeamName
          awayTeamName
          awayTeamID
          homeTeamID
          toss
          startDate
          matchStatus
          matchResult
          matchID
          matchType
          statusMessage
          matchNumber
          venue
          currentDay
          currentSession
          playerOfTheMatch
          playerID
          winningTeamID
          playerOfTheMatchdDetails {
            playerID
            playerTeamID
            playerName
            batsman {
              batsmanBalls
              batsmanRuns
              isNotOut
            }
            bowler {
              bowlerWickets
              bowlerConceeded
              bowlerOvers
            }
          }
          firstInningsTeamID
          secondInningsTeamID
          thirdInningsTeamID
          fourthInningsTeamID
          isCricklyticsAvailable
          isLiveCriclyticsAvailable
          playing11Status
          probable11Status
          isAbandoned
          teamsWinProbability {
            homeTeamShortName
            homeTeamPercentage
            awayTeamShortName
            awayTeamPercentage
            tiePercentage
          }
          matchScore {
            teamShortName
            teamID
            teamScore {
              inning
              inningNumber
              battingTeam
              runsScored
              wickets
              declared
              folowOn
              overs
              runRate
              battingSide
              teamID
              battingTeamShortName
            }
          }
          totalParticipants
        }
      }
    }
  }
`
export const PREDICTION_PROFILE_AXIOS = ` 
  query predictionProfile($year: String, $month: String, $token: String) {
    predictionProfile(year: $year, month: $month, token: $token) {
      name
      level
      totalEarnings
      contestEntered
      photo
      matches {
        earnings
        rightPrediction
        totalPredictions
        matchDetails {
        superOver
        matchenddate
        phaseOfInningFlag
        isHomeMatchUpade
        content
        displayFeatureMatchScoreCard
        seriesID
        currentinningsNo
        currentInningteamID
        currentInningsTeamName
        seriesName
        homeTeamName
        awayTeamName
        awayTeamID
        homeTeamID
        toss
        startDate
        matchStatus
        matchResult
        matchID
        matchType
        statusMessage
        matchNumber
        venue
        currentDay
        currentSession
        playerOfTheMatch
        playerID
        winningTeamID
        playerOfTheMatchdDetails {
          playerID
          playerTeamID
          playerName
          batsman {
            batsmanBalls
            batsmanRuns
            isNotOut
          }
          bowler {
            bowlerWickets
            bowlerConceeded
            bowlerOvers
          }
        }
        firstInningsTeamID
        secondInningsTeamID
        thirdInningsTeamID
        fourthInningsTeamID
        isCricklyticsAvailable
        isLiveCriclyticsAvailable
        playing11Status
        probable11Status
        isAbandoned
        teamsWinProbability {
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
        }
        matchScore {
          teamShortName
          teamID
          teamScore {
            inning
            inningNumber
            battingTeam
            runsScored
            wickets
            declared
            folowOn
            overs
            runRate
            battingSide
            teamID
            battingTeamShortName
          }
        }
    totalParticipants
        }
      }
    }
  }
`

export const LEADERBOARD_PROFILE = gql`
  query leaderBoardByKeys($id: String, $key: String, $token: String) {
    leaderBoardByKeys(id: $id, key: $key, token: $token) {
      data {
        userID
        username
        totalpoints
        rank
        photo
        s_photo
      }
      count
    }
  }
`

export const PREDICTION_PROFILE_MATCH = gql`
  query predictionProfileMatch($matchID: String!, $token: String) {
    predictionProfileMatch(matchID: $matchID, token: $token) {
      rank
      totalEarnings
      rightPrediction
      totalPredictions
      PrematchWinOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TossOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TotalRunsOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TopScorerOddsHomeTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TopScorerOddsAwayTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      EconomicalBowlerOddsHomeTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      EconomicalBowlerOddsAwayTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      PowerPlayWinOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      OpeningPartnershipWinOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      BattingMilestonesOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      WicketTypeOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TotalBet
      isCancelled
    }
  }
`

export const PREDICTION_PROFILE_MATCH_AXIOS = `
  query predictionProfileMatch($matchID: String!, $token: String) {
    predictionProfileMatch(matchID: $matchID, token: $token) {
      rank
      totalEarnings
      rightPrediction
      totalPredictions
      PrematchWinOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TossOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TotalRunsOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TopScorerOddsHomeTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TopScorerOddsAwayTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      EconomicalBowlerOddsHomeTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      EconomicalBowlerOddsAwayTeam {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      PowerPlayWinOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      OpeningPartnershipWinOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      BattingMilestonesOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      WicketTypeOdds {
        question
        selectedID
        selectedName
        correctID
        correctName
        winCoins
        betCoins
        status
      }
      TotalBet
      isCancelled
    }
  }
`

export const PREDICTION_PROFILE_STREAK = gql`
  query contestStreak($token: String) {
    contestStreak(token: $token) {
      continousStreak
      heighestStreak
      streak
      matchesData {
        matchDate
        matchesList {
          matchID
          homeTeamName
          awayTeamName
        }
      }
    }
  }
`

export const PREDICTION_PROFILE_STREAK_AXIOS = gql`
  query contestStreak($token: String) {
    contestStreak(token: $token) {
      continousStreak
      heighestStreak
      streak
      matchesData {
        matchDate
        matchesList {
          matchID
          homeTeamName
          awayTeamName
        }
      }
    }
  }
`

export const CHECK_UNIQUE_USERNAME = gql`
  mutation checkUniqueUSer($username: String!, $token: String) {
    checkUniqueUSer(username: $username, token: $token) {
      code
      message
      userExist
    }
  }
`

export const DELETE_ACCOUNT = gql`
  mutation deleteUserProfile(
    $token: String!
    $reason: String!
    $feedback: String!
  ) {
    deleteUserProfile(token: $token, reason: $reason, feedback: $feedback) {
      code
    }
  }
`

export const CHECK_ACCOUNT_WITH_USERNAME = gql`
  query checkPhoneandEmail(
    $token: String!
    $phnumber: String!
    $email: String!
  ) {
    checkPhoneandEmail(token: $token, phnumber: $phnumber, email: $email) {
      code
      token
      username
      email
      phnumber
      photp
      emailotp
      userID
      photo
    }
  }
`

export const GET_PREURL = gql`
  mutation getPresignUrl($token: String!) {
    getPresignUrl(token: $token) {
      code
      message
      preurl
      path
      photoUrl
    }
  }
`

export const GET_PREURL_AXIOS = gql`
  mutation getPresignUrl($token: String!) {
    getPresignUrl(token: $token) {
      code
      message
      preurl
      path
      photoUrl
    }
  }
`

export const DELETE_PROFILE_IMAGE = gql`
  mutation deletePicture($token: String!) {
    deletePicture(token: $token) {
      code
      message
    }
  }
`

export const GET_MATCH_BY_CLEVERTAP = gql`
  query matchByClevertapID($userID: String!, $tourID: String!) {
    matchByClevertapID(userID: $userID, tourID: $tourID) {
      seriesFlag
      matchlist
    }
  }
`

export const CHANGE_TEAM_NAME = gql`
  mutation editMyFantasyTeamName(
    $matchID: String
    $newTeamName: String
    $oldTeamName: String
    $token: String
  ) {
    editMyFantasyTeamName(
      matchID: $matchID
      newTeamName: $newTeamName
      oldTeamName: $oldTeamName
      token: $token
    )
  }
`

export const SERIES_NAME_FOR_LEADERBOARD = gql`
  query predictionHomeSeries {
    predictionHomeSeries {
      tourID
      tourName
    }
  }
`

export const SERIES_NAME_FOR_LEADERBOARD_AXIOS = gql`
  query predictionHomeSeries {
    predictionHomeSeries {
      tourID
      tourName
    }
  }
`

export const UPDATE_WALLET_COINS = gql`
  mutation addWalletCoins($token: String) {
    addWalletCoins(token: $token)
  }
`
export const UPDATE_WALLET_COINS_AXIOS = gql`
  mutation addWalletCoins($token: String) {
    addWalletCoins(token: $token)
  }
`

export const HOME_PAGE_GET_ALGO11 = gql`
  query homePageGetAlgo11($matchID: String!) {
    homePageGetAlgo11(matchID: $matchID) {
      totalPoints
      teamtotalpoints
      timestamp
      totalProjectedPoints
      batsman {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
      bowler {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
      keeper {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
      all_rounder {
        isMyPick
        captain
        mean_projected_points
        vice_captain
        credits
        playerCredits
        matchID
        teamID
        teamName
        player_role
        selectionPercent
        playerId
        playerName
        playerNameHindi
        projectedPoints
        playing_xi
        totalPoints
      }
    }
  }
`

export const TEAM_ANALYSIS_HOMEPAGE = gql`
  query teamAnalysis($matchID: String) {
    getTeamAnalysis(matchID: $matchID) {
      matchID
      compTypeID
      compType
      teamA {
        teamID
        teamShortName
        battingVSPace
        battingVSSpin
        paceBowling
        spinBowling
        battingDepth
        overallRating
      }
      teamB {
        teamID
        teamShortName
        battingVSPace
        battingVSSpin
        paceBowling
        spinBowling
        battingDepth
        overallRating
      }
    }
  }
`

export const HOME_PRE_MATCH_CRICLYTICS = gql`
  query upcomingMatchHomepageData($matchID: String) {
    upcomingMatchHomepageData(matchID: $matchID) {
      teamAnalysis {
        matchID
        compTypeID
        compType
        teamA {
          teamID
          teamShortName
          battingVSPace
          battingVSSpin
          paceBowling
          spinBowling
          battingDepth
          overallRating
        }
        teamB {
          teamID
          teamShortName
          battingVSPace
          battingVSSpin
          paceBowling
          spinBowling
          battingDepth
          overallRating
        }
      }
      topPicks {
        team1Name
        team2Name
        team1Data {
          playerID
          Form
          avg
          playerName
          sr
          playerRole
          matches {
            homeTeamShortName
            awayTeamShortName
            homeTeamID
            awayTeamID
            matchDate
            matchID
            matchType
            opposition
            battingStats
            bowlingStats
          }
        }
        team2Data {
          playerID
          Form
          avg
          playerName
          sr
          playerRole
          matches {
            homeTeamShortName
            awayTeamShortName
            homeTeamID
            awayTeamID
            matchDate
            matchID
            matchType
            opposition
            battingStats
            bowlingStats
          }
        }
      }
      stadiumDetails {
        firstBattingWinPercent
        avgFirstInningScore
        highestScoreChased
        venueName
        venueID
        paceWicket
        spinWicket
        StadiumCharacteristics
        text
      }
      cricketDotComTeam {
        saveTeams
        timestamp
        fantasy_teamName
        totalProjectedPoints
        batsman {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        all_rounder {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        bowler {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        keeper {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        teamID
        teamTagline
      }
    }
  }
`
export const HOME_POST_MATCH_CRICLYTICS = gql`
  query completedMatchHomepageData($matchID: String) {
    completedMatchHomepageData(matchID: $matchID) {
      livePlayerImpact {
        teamName
        topThree {
          playerName
          batting_impact
          bowling_impact
          total_impact
          playerID
          playerTeam
        }
      }
      runRate {
        format
        inningsPhase {
          highestRunrate
          teamID
          totalRuns
          innings
          totalWickets
          teamShortName
          highestRunrate
          innings
          highestRunrate
          data {
            r
            o
          }
        }
      }
      scoreChart {
        inningsPhase {
          _id
          teamID
          teamShortName
          totalRuns
          totalWickets
          data {
            runs
            wickets
            innings
            teamID
            teamShortName
            overNumber
          }
        }
        format
      }
      wormChart {
        format
        inningsPhase {
          totalRuns
          _id
          teamID
          teamShortName
          totalWickets
          data {
            runs
            overNumber
            wickets
            innings
            teamID
            teamShortName
            score
          }
        }
      }
      matchReel {
        matchId
        liveScores {
          inningNo
          overNo
          currentScore
          predictedScore
          predictedOver
          predictvizMarginView {
            innings
            result
            runs
            winnerTeamId
            wickets
          }
          winvizView {
            battingTeamPercent
            bowlingTeamPercent
            drawPercent
            tiePercent
          }
          secondPredictedScore
          secondPredictedOVer
          thirdPredictedScore
          thirdPredictedOver
          fourthPredictedScore
          fourthPredictedOver
          predictedWicket
          secondPredictedWicket
          thirdPredictedWicket
          fourthPredictedWicket
          team2Id
          team1Id
          inningIds
          team1Name
          team2Name
          team1ShortName
          team2ShortName
          currentWickets
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
          currentOvers
          projected_result
        }
      }
      cricketDotComTeam {
        saveTeams
        saveTeamCount
        totalPoints
        teamtotalpoints
        timestamp
        fantasy_teamName
        totalProjectedPoints
        batsman {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        all_rounder {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        bowler {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        keeper {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        teamID
        teamTagline
      }
    }
  }
`

export const HOME_LIVE_MATCH_CRICLYTICS = gql`
  query liveMatchHomepageData($matchID: String) {
    liveMatchHomepageData(matchID: $matchID) {
      matchCardDetails {
        miniScore {
          data {
            seriesName
            homeTeamName
            awayTeamName
          }
        }
        last12Balls {
          over {
            type
            runs
            overNumber
          }
          overNumber
        }
      }
      runRate {
        format
        inningsPhase {
          highestRunrate
          teamID
          totalRuns
          innings
          totalWickets
          teamShortName
          highestRunrate
          innings
          highestRunrate
          data {
            r
            o
          }
        }
      }
      inningsSummaryData {
        score {
          overs
          runsScored
          wickets
          runRate
          battingTeamName
          battingTeamID
        }
        battingList {
          playerID
          playerName
          playerTeam
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerMatchSixes
          playerMatchFours
        }
        bowlingList {
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerOversBowled
          playerEconomyRate
        }
      }
      scoreChart {
        inningsPhase {
          _id
          teamID
          teamShortName
          totalRuns
          totalWickets
          data {
            runs
            wickets
            innings
            teamID
            teamShortName
            overNumber
          }
        }
        format
      }
      wormChart {
        format
        inningsPhase {
          totalRuns
          _id
          teamID
          teamShortName
          totalWickets
          data {
            runs
            overNumber
            wickets
            innings
            teamID
            teamShortName
            score
          }
        }
      }
      scoreProjection {
        matchId
        lasttenball {
          runs
          isBall
          isWicket
          type
          isLastBall
        }
        scoreCard {
          batting {
            matchID
            playerID
            playerName
            playerTeam
            playerMatchSixes
            playerMatchFours
            playerMatchBalls
            playerMatchRuns
            playerBattingNumber
            playerDismissalInfo
            playerHowOut
            playerMatchStrikeRate
            playerOnStrike
          }
          bowling {
            matchID
            playerID
            playerName
            playerTeam
            playerDotBalls
            playerWicketsTaken
            playerMaidensBowled
            playerRunsConceeded
            playerWides
            playerNoBall
            playerOversBowled
            playerEconomyRate
          }
        }
        TeamProjections {
          inningNo
          overNo
          currentScore
          predictedScore
          predictedOver
          predictvizMarginView {
            innings
            result
            runs
            winnerTeamId
            wickets
          }
          winvizView {
            battingTeamPercent
            bowlingTeamPercent
            drawPercent
            tiePercent
          }
          secondPredictedScore
          secondPredictedOVer
          thirdPredictedScore
          thirdPredictedOver
          fourthPredictedScore
          fourthPredictedOver
          predictedWicket
          secondPredictedWicket
          thirdPredictedWicket
          fourthPredictedWicket
          team2Id
          team1Id
          inningIds
          team1Name
          team2Name
          team1ShortName
          team2ShortName
          currentWickets
          homeTeamShortName
          homeTeamPercentage
          awayTeamShortName
          awayTeamPercentage
          tiePercentage
          currentOvers
          projected_result
        }
      }
      cricketDotComTeam {
        saveTeams
        saveTeamCount
        totalPoints
        teamtotalpoints
        timestamp
        fantasy_teamName
        totalProjectedPoints
        batsman {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        all_rounder {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        bowler {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
          totalPoints
          dream_team_appearances
          playerNameHindi
        }
        keeper {
          isMyPick
          captain
          projected_points
          mean_projected_points
          vice_captain
          credits
          playerCredits
          matchID
          teamID
          teamName
          player_role
          selectionPercent
          dream_team_appearance
          fantasy_points
          playerId
          playerName
          projectedPoints
          playing_xi
        }
        teamID
        teamTagline
      }
    }
  }
`

export const HOME_UPCOMNG_SERIES = gql`
  query getseriesforHomepage {
    getseriesforHomepage {
      teams
      teamid
      tourName
      type
      seriesType
      seriesStartDate
      tourID
      match
      seriesEndDate
    }
  }
`

// export const DATA_DIGEST = gql`
//   query getTopTenDataDigest {
//     getTopTenDataDigest {
//       img
//       _id
//     }
//   }
// `;

export const ARTICLES_VIDEOS_BY_SERIES = gql`
  query getArticlesVideosBySeries {
    getArticlesVideosBySeries {
      tourID
      tourName
      seriesType
      data {
        title
        id
        type
        description
        authors
        videoID
        videoUrl
        videoYTId
        thumbnail
        featureThumbnail
        sm_image_safari
        bg_image_safari
        publishedAt
        contentType
      }
    }
  }
`
export const HOME_VIDEOS = gql`
  query getVideosPostitions {
    getVideosPostitions {
      videoID
      title
      description
      videoUrl
      videoYTId
      authors
      createdAt
    }
  }
`

// query liveMatchHomepageData($matchID: String) {
//   liveMatchHomepageData(matchID: $matchID) {

export const GET_WEB_HOMW_PAGE_DATA_AXIOS = gql`
  query getwebHomePageDataSideBar {
    getwebHomePageDataSideBar {
      pointsTable {
        data {
          tourName
          tourID
          pointsData {
            standings {
              name
              teams {
                pos
                teamID
                teamName
                teamShortName
                all
                wins
                lost
                points
                nrr
                qp
                isQualified
                noResult
              }
            }
          }
        }
        listOfTours
      }

      womensCricUpdate {
        type
        cricShortsId
        title
        imageUrl
        description
        redirectUrl
        videoUrl
        videoId
        author
        publishedAt
        likes
      }
      pictureOfTheDay {
        type
        cricShortsId
        title
        imageUrl
        description
        redirectUrl
        videoUrl
        videoId
        author
        publishedAt
        likes
      }
    }
  }
`

export const GET_WEB_HOMW_PAGE_DATA = gql`
  query getwebHomePageDataSideBar {
    getwebHomePageDataSideBar {
      playerRanking {
        ODI {
          men {
            team {
              matchType
              rank {
                position
                Rating
                teamName
              }
            }
            allrounder {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
            bat {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
            bowl {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
          }
        }
        Test {
          men {
            team {
              matchType
              rank {
                position
                Rating
                teamName
              }
            }
            allrounder {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
            bat {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
            bowl {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
          }
        }
        T20 {
          men {
            team {
              matchType
              rank {
                position
                Rating
                teamName
              }
            }
            allrounder {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
            bat {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
            bowl {
              matchType
              gender
              rank {
                playerName
                position
                Points
                teamID
                teamName
              }
            }
          }
        }
      }
      fantasycentre {
        homeTeamID
        awayTeamID
        matchid
        seriesName
        matchName
        matchNumber
        awayTeamShortName
        homeTeamShortName

        maxProjection {
          fantasy_teamName
          fantasy_teamType
          maxProjections
          minProjections

          players {
            captain
            teamID
            playerName
            playerId
            vice_captain
            projected_points
            playerRole
            credits
          }
        }
      }
      pointsTable {
        data {
          tourName
          tourID
          pointsData {
            standings {
              name
              teams {
                pos
                teamID
                teamName
                teamShortName
                all
                wins
                lost
                points
                nrr
                qp
                isQualified
                noResult
              }
            }
          }
        }
        listOfTours
      }

      womensCricUpdate {
        type
        cricShortsId
        title
        imageUrl
        description
        redirectUrl
        videoUrl
        videoId
        author
        publishedAt
        likes
      }
      pictureOfTheDay {
        type
        cricShortsId
        title
        imageUrl
        description
        redirectUrl
        videoUrl
        videoId
        author
        publishedAt
        likes
      }
    }
  }
`

export const HOME_SERIES_TO_LOOK_OUT_AXIOS = `

  query getWebHomePageData
{
    getWebHomePageData 
    {
        
        seriesToLookoutFor
        {
            seriesStartDate
           seriesEndDate
           seriesType
           tourName
           teams
           teamid
           tourID
           match
        }

        cricStats
        {
            img
        }
 

        socialTracker
        {
            
            data
            {
                
                data
                type
                twitterID
                feedID
            }
        }
    }
    
}`

export const HOME_SERIES_TO_LOOK_OUT = gql`
  query getWebHomePageData {
    getWebHomePageData {
      seriesToLookoutFor {
        seriesStartDate
        seriesEndDate
        seriesType
        tourName
        teams
        teamid
        type
        tourID
        match
      }

      cricStats {
        img
      }

      socialTracker {
        data {
          data
          type
          twitterID
          feedID
        }
      }
    }
  }
`

export const CDC_EXCLUSIVE_DATA_AXIOS = `
query getCdcExclusive ($type : Int, $page : Int)
{
getCdcExclusive (type : $type, page:$page)
{
   title
    data 
    {
        type
        authors
        videoUrl
        videoID
        title
        description
        videoYTId
        articleID
        publishedAt
        createdAt
        bg_image_safari
    }

}
}
`

export const GET_ARTICLES_BY_CATEGORIES_AXIOS = `
  query getArticlesByCategories($type: String!, $page: Int) {
    getArticlesByCategories(type: $type, page: $page) {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      content
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
      relatedArticles {
        articleID
        matchIDs {
          matchID
          matchSlug
        }
        seriesIDs {
          name
          id
        }
        teamIDs {
          name
          id
        }
        playerIDs {
          name
          id
        }
        approvalStatus
        status
        title
        slugTitle
        content
        thumbnail
        featureThumbnail
        description
        tags {
          name
          type
          tag
          id
        }
        type
        filters
        createdAt
        updatedAt
        author
        publishedAt
        seoTags
        relatedArticles {
          articleID
        }
      }
    }
  }
`

export const VIDEO_BY_CATEGORY_AXIOS = `
  query getVideosByType($type: String!) {
    getVideosByType(type: $type) {
      videoID
      title
      description
      videoUrl
      videoYTId
    }
  }
`

export const HOME_MATCH_SUMMARY_LIVE = gql`
  query matchSummaryHomeScreen($matchID: String!) {
    matchSummaryHomeScreen(matchID: $matchID) {
      innings {
        score {
          overs
          runsScored
          wickets
          runRate
          battingTeamName
          battingTeamID
        }
        battingList {
          playerID
          playerName
          playerTeam
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerMatchSixes
          playerMatchFours
        }
        bowlingList {
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerOversBowled
          playerEconomyRate
        }
      }
      topPerformer {
        batsman {
          playerID
          playerName
          playerTeam
          playerMatchBalls
          playerMatchRuns
          playerBattingNumber
          playerMatchSixes
          playerMatchFours
        }
        bowler {
          playerID
          playerName
          playerTeam
          playerDotBalls
          playerWicketsTaken
          playerMaidensBowled
          playerRunsConceeded
          playerOversBowled
          playerEconomyRate
        }
      }
    }
  }
`

// export const HOME_GET_ARTICLE_VIDEO_BY_SERIES= gql`

// getArticlesVideosBySeries{
//   tourID
//   tourName
//   data{
//    title
//           id
//           type
//           description
//           authors
//           videoID
//           videoUrl
//           videoYTId
//           thumbnail
//           featureThumbnail
//           sm_image_safari
//           bg_image_safari
//           publishedAt
//           contentType
//   }
//   }`

export const HomePageData = gql`
  query homePageData {
    getArticleByPostitions {
      articleID
      matchIDs {
        matchID
        matchSlug
      }
      seriesIDs {
        name
        id
      }
      teamIDs {
        name
        id
      }
      playerIDs {
        name
        id
      }
      approvalStatus
      status
      title
      slugTitle
      bg_image
      sm_image
      bg_image_safari
      sm_image_safari
      thumbnail
      featureThumbnail
      description
      tags {
        name
        type
        tag
        id
      }
      type
      filters
      createdAt
      updatedAt
      author
      publishedAt
      seoTags
    }

    getVideosPostitions {
      videoID
      title
      description
      createdAt

      videoUrl
      videoYTId
    }
    getArticlesVideosBySeries {
      tourID

      tourName
      data {
        title
        id

        type
        description
        authors
        videoID

        videoUrl
        videoYTId
        thumbnail
        featureThumbnail
        sm_image_safari
        bg_image_safari
        publishedAt
        contentType
      }
    }

    getWebHomePageData {
      seriesToLookoutFor {
        seriesStartDate
        seriesEndDate
        seriesType
        tourName
        teams
        teamid
        type
        tourID
        match
      }

      cricStats {
        img
      }
    }
  }
`
export const UPDATE_FEED_LIKES = gql`
  mutation updateFeedLikes($id: String!, $like: Boolean) {
    updateFeedLikes(id: $id, like: $like) {
      feedID
      type
      title
      featuredImage
      embeddedCode
      likes
      lasteditedBy
      publishedAt
      createdAt
    }
  }
`
export const GET_FEEDS = gql`
  query primaryFeeds($id: String!, $activeId: String!, $page: Int!) {
    primaryFeeds(id: $id, activeId: $activeId, page: $page) {
      feedID
      type
      status
      title
      description
      heading
      embeddedCode
      featuredImage
      image
      youtubeurl
      youtubeurlid
      video
      likes
      rawDataRedirect
      redirectionType
      redirectID
      background
      redirectURL
      lasteditedBy
      publishedAt
      createdAt
      appID
      question
      answer
      twiterID
    }
  }
`

export const ASK_AN_EXPERT = gql`
  mutation addExpertQuestion($question: String!, $feedID: String!) {
    addExpertQuestion(question: $question, feedID: $feedID)
  }
`
