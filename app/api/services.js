import {
  CRYCLYTICS_VIEW,
  MATCH_DETAIL_LAYOUT,
  SERIES_DETAILS,
  SCHEDULE_TAB_STATUS_SWITCH,
  VIDEOS_VIEW,
  ARTICLE_VIEW,
  ARTICLE_TAB_SWITCH,
  PLAYER_VIEW,
  TEAM_VIEW,
  STADIUM_VIEW,
  SERIES_TAB_SWITCH,
  RECORD_VIEW,
  FRC_SERIES_VIEW,
  CRYCLYTICS_VIEW_SCORE,
  FRC_SERIES_CATEGORY_VIEW,
  FRC_SERIES_TEAM_VIEW,
  SERIES_DETAILS_View,
  SERIES_DETAILS_View_BEST,
  PLAYER_VIEW_BIO,
  News_Live_Tab_URL,
  LIVE_SCORE_ARTICLE,
  SERIES_TAB_BASE_URL,
  SERIES_HOME,
  PLAYER_TABS,
  AUTHOR_NEWS_VIEW,
  SERIES_FILTER_TAB_BASE_URL,
  TEAM_TAB_V2,
  GET_LIEV_STREAM,
  PLAY_THE_ODDS_CONTEST,
} from '../constant/Links'
export const getCriclyticsUrlHome = (match, tab) => {
  let matchName = `${match.homeTeamName}-vs-${
    match.awayTeamName
  }-${match.matchNumber.split(' ').join('-')}-`
  let criclyticName =
    matchName.toLowerCase() +
    match.seriesName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()
  tab = tab ? tab : ''
  let matchID = match.matchID
  return {
    as: eval(CRYCLYTICS_VIEW.as),
    href: CRYCLYTICS_VIEW.href,
  }
}

export const getCriclyticsUrlHomeDesktop = (match, tab) => {
  let matchName = `${match.homeTeamName}-vs-${
    match.awayTeamName
  }-${match.matchNumber.split(' ').join('-')}-`
  let criclyticName =
    matchName.toLowerCase() +
    match.seriesName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()
  tab = tab ? '' : ''
  let matchID = match.matchID
  return {
    as: eval(CRYCLYTICS_VIEW.as),
    href: CRYCLYTICS_VIEW.href,
  }
}

export const getLangText = (lang, keys, key) => {
  //
  //
  //
  let [english, hindi] = keys[key]
  switch (lang) {
    case 'HIN':
      return hindi
    case 'ENG':
      return english
    default:
      return english
  }
}
/**
 *
 * @param { Object } team to genrate team url
 */
export const getTeamUrl = (team, tab) => {
  let teamName = team.name
  let teamId = team.teamID
  teamName = teamName.split(' ').join('-').toLowerCase()
  return {
    as: eval(TEAM_VIEW.as),
    href: TEAM_VIEW.href,
  }
}

/**
 *
 * @param { Object } match match details for url process
 * @param { String } tabName tab name for perticular schedule switch ex - commentry,summary
 */
export const scheduleMatchView = (match, tabName) => {
  // alert(8)
  let matchStatus =
    match?.matchStatus === 'completed' ? 'match-score' : 'live-score'
  // alert(matchStatus)
  let matchID = match?.matchID
  let matchName = `${
    match?.homeTeamName ? match?.homeTeamName : match?.homeTeamShortName
  }-vs-${
    match?.awayTeamName ? match?.awayTeamName : match?.awayTeamShortName
  }-${match?.matchNumber ? match?.matchNumber.split(' ').join('-') : ''}-`

  let seriesName =
    matchName.toLowerCase() +
    (match?.seriesName
      ? match.seriesName
          .replace(/[^a-zA-Z0-9]+/g, ' ')
          .split(' ')
          .join('-')
          .toLowerCase()
      : '')

  let currentTab = tabName
    ? tabName.toLowerCase()
    : match.isAbandoned
    ? 'commentary'
    : match.matchResult === 'Match Cancelled'
    ? 'commentary'
    : match.matchStatus == 'upcoming'
    ? 'matchinfo'
    : match.matchStatus === 'completed'
    ? 'summary'
    : 'commentary'

  return {
    as: eval(MATCH_DETAIL_LAYOUT.as),
    href: eval(MATCH_DETAIL_LAYOUT.href),
  }
}

export const newScheduleMatchView = (match, tabName) => {
  let matchStatus =
    match.matchStatus === 'completed' ? 'match-score' : 'live-score'
  let matchID = match.matchID
  let matchName = `${
    match.homeTeamName ? match.homeTeamName : match.homeTeamShortName
  }-vs-${match.awayTeamName ? match.awayTeamName : match.awayTeamShortName}-${
    match.matchNumber ? match.matchNumber.split(' ').join('-') : ''
  }-`

  let seriesName =
    matchName.toLowerCase() +
    (match.seriesName
      ? match.seriesName
          .replace(/[^a-zA-Z0-9]+/g, ' ')
          .split(' ')
          .join('-')
          .toLowerCase()
      : '')

  let currentTab = tabName
    ? tabName.toLowerCase()
    : match.isAbandoned
    ? 'commentary'
    : match.matchResult === 'Match Cancelled'
    ? 'commentary'
    : match.matchStatus == 'upcoming'
    ? 'matchinfo'
    : match.matchStatus === 'completed'
    ? 'summary'
    : 'commentary'

  return {
    as: eval(MATCH_DETAIL_LAYOUT.as),
    href: eval(MATCH_DETAIL_LAYOUT.href),
  }
}
/**
 * 
 * @param { Object } match match details
  @param { String } tab cryclytics tabs ex- live-player-projection, game-chaging-over, matchups
 */
export const getCriclyticsUrl = (match, tab) => {
  let matchName = `${match.homeTeamName}-vs-${
    match.awayTeamName
  }-${match.matchNumber.split(' ').join('-')}-`
  let criclyticName =
    matchName.toLowerCase() +
    match.seriesName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()
  tab = tab ? tab : 'live-player-projection'
  let matchID = match.matchID
  return {
    as: eval(CRYCLYTICS_VIEW.as),
    href: CRYCLYTICS_VIEW.href,
  }
}

/**
 *
 * @param { Object } series series details
 * @param { String } currentTab
 */
export const getSeries = (series, currentTab = 'matches') => {
  let seriesName = series.seriesSlug
    ? series.seriesSlug
    : series.seriesName
        .replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
  let type =
    series.league == 'ICC' || series.league == "Women's International"
      ? 'international'
      : 'domestic'
  let seriesId = series.seriesID
  // seriesName = `${seriesName}-${currentTab}`;

  return { as: eval(SERIES_DETAILS.as), href: SERIES_DETAILS.href }
}

export const getSeriesNew = (series, currentTab = 'matches') => {
  let seriesName = series.tourName
    .replace(/[^a-zA-Z0-9]+/g, ' ')
    .split(' ')
    .join('-')
    .toLowerCase()
  let type =
    series.league == 'ICC'
      ? 'international'
      : series.league == "Women's International"
      ? 'womens-international'
      : 'domestic'
  let seriesId = series.tourID

  return { as: eval(SERIES_DETAILS.as), href: eval(SERIES_DETAILS.as) }
}

/**
 * @description series multiple tab view
 * @param { Object } data
 */
export const seriesViewStat = (data) => {
  let seriesType = data.seriesType
  let seriesId = data.seriesID
  let type2 = data.type
  let types = data.types
  let seriesName = data.slug

  return { as: eval(SERIES_DETAILS_View.as), href: SERIES_DETAILS_View.href }
}

export const seriesViewStatBest = (data) => {
  let seriesType = data.seriesType
  let seriesId = data.seriesID
  let type2 = data.type
  let types = data.types.toLowerCase()
  let seriesName = data.slug
  let format = data.format

  return {
    as: eval(SERIES_DETAILS_View_BEST.as),
    href: SERIES_DETAILS_View_BEST.href,
  }
}

/**
 * @description To switch between tabs in schedule section
 * @param { String } tabName
 * @param { String } status
 */
export const getScheduleTabUrl = (tabName, status) => {
  let pathWillBe
  tabName = tabName.toLowerCase()
  status = status.toLowerCase()

  if (status == 'completed') {
    status = 'results'
  }
  if (tabName === 'all') {
    pathWillBe = `${status}`
  } else {
    pathWillBe = `${tabName}/${status}`
  }
  return {
    as: eval(SCHEDULE_TAB_STATUS_SWITCH.as),
    href: SCHEDULE_TAB_STATUS_SWITCH.href,
  }
}

export const getLiveStreamUrl = (tabName) => {
  let pathWillBe
  tabName = tabName.toLowerCase()

  // if (tabName === 'all') {
  //   pathWillBe = `${status}${status == 'results' ? '' : '-matches'}`;
  // } else {
  //   pathWillBe = `${tabName}/${status}${status == 'results' ? '' : '-matches'}`;
  // }

  pathWillBe = tabName
  return {
    as: eval(GET_LIEV_STREAM.as),
    href: GET_LIEV_STREAM.href,
  }
}

export const getContestUrl = (matchID) => {
  return {
    as: eval(PLAY_THE_ODDS_CONTEST.as),
    href: PLAY_THE_ODDS_CONTEST.href,
  }
}

export const getTabUrlSeries = (tab) => {
  if (tab == 'all') {
    return {
      as: SERIES_HOME.as,
      href: SERIES_HOME.href,
    }
  }
  return {
    as: eval(SERIES_TAB_BASE_URL.as),
    href: SERIES_TAB_BASE_URL.href,
  }
}

export const getFilterTabUrlSeries = (tab, filterTab) => {
  return {
    as: eval(SERIES_FILTER_TAB_BASE_URL.as),
    href: SERIES_FILTER_TAB_BASE_URL.href,
  }
}

export const getFilterTabUrlSeriesHome = (tab, filterTab) => {
  return {
    as: eval(SERIES_HOME.as),
    href: SERIES_HOME.href,
  }
}

export const getVideoUrl = (video, type = 'latest') => {
  type = type.toLowerCase()
  let videoId = video.videoID
  return {
    as: eval(VIDEOS_VIEW.as),
    href: VIDEOS_VIEW.href,
  }
}

export const getPlayerUrl = (player, slugLevel) => {
  //
  //player.playerName = player.shortname ? player.shortname : player.playerName ? player.playerName : player.fullName;
  if (player.id) player.playerID = player.id
  let playerId = player.playerID
  let playerSlug = `${
    player.playerName && player.playerName.split(' ').join('-').toLowerCase()
  }`
  //
  //
  return {
    as: eval(PLAYER_VIEW.as),
    href: PLAYER_VIEW.href,
  }
}
export const getCriclyticsScore = (match, tab) => {
  let matchName = `${match.homeTeamName}-vs-${
    match.awayTeamName
  }-${match.matchNumber.split(' ').join('-')}-`
  let criclyticName =
    matchName.toLowerCase() +
    match.seriesName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()

  let matchID = match.matchID
  return {
    as: eval(CRYCLYTICS_VIEW_SCORE.as),
    href: CRYCLYTICS_VIEW_SCORE.href,
  }
}
export const getPlayerUrlNewSearch = (player) => {
  //

  let playerId = player.playerID
  let playerSlug = `${
    player.name && player.name.split(' ').join('-').toLowerCase()
  }`
  //
  //
  return {
    as: eval(PLAYER_VIEW.as),
    href: PLAYER_VIEW.href,
  }
}

export const getPlayerUrlBio = (player) => {
  let dataString = player

  return {
    as: eval(PLAYER_VIEW_BIO.as),
    href: PLAYER_VIEW_BIO.href,
  }
}

export const PlayerMatchView = (name, playerID, tab) => {
  // let data='/players/3676/ms-dhoni'
  let tabName = tab
  let PlayerId = playerID
  let PlayerName = name
  //
  //

  return {
    as: eval(PLAYER_TABS.as),
    href: eval(PLAYER_TABS.href),
  }
}

export const TeamViewV2 = (name, teamID, tab, teamTab) => {
  // let data='/players/3676/ms-dhoni'

  let tabName = tab
  let teamId = teamID
  let teamName = name
  //
  //

  return {
    as: eval(TEAM_TAB_V2.as),
    href: TEAM_TAB_V2.href,
  }
}

export const LiveScoreArticle = (
  matchStatus,
  matchID,
  currentTab,
  seriesName,
) => {
  // let dataString = player;

  return {
    as: eval(LIVE_SCORE_ARTICLE.as),
    href: LIVE_SCORE_ARTICLE.href,
  }
}

export const getLiveArticleUrl = (article) => {
  let matchId = article.matchIDs[0].matchID
  let matchName = article.matchIDs[0].matchSlug.toLowerCase()
  let articleID = article.articleID

  return {
    as: eval(News_Live_Tab_URL.as),
    href: News_Live_Tab_URL.href,
  }
}

export const navigateMatchDetailBlog = (matchId, matchName, articleID) => {
  // let matchId=article.matchIDs[0].matchID
  // let matchName=article.matchIDs[0].matchSlug.toLowerCase()
  // let articleID=article.articleID

  return {
    as: eval(News_Live_Tab_URL.as),
    href: News_Live_Tab_URL.href,
  }
}

/**
 *
 * @param { String } switchName  article tab switch
 */
export const getArticleTabUrl = (switchName) => {
  let href, as
  if (!switchName) {
    // href = `/news/[type]`
    as = '/news/latest'
  } else {
    as = `/news/${switchName}`
    // href = '/news/[type]'
  }
  return {
    as: as,
    href: href,
  }
}

// export const getArticleTabUrlLive = (switchName) => {
//   let href, as;
//   if (!switchName) {
//     href = `/news/[type]`;
//     as = '/news/latest/live-blog/articleID/articleName';
//   }
//   return {
//     as: as,
//     href: href
//   };
// };

/**
 * @description news view
 * @param { Object } article
 * @param { String } tab
 */
export const getNewsUrl = (article, tab) => {
  let articleId = article?.articleID
  tab = tab ? tab : article.type
  return {
    as: eval(ARTICLE_VIEW.as),
    href: ARTICLE_VIEW.href,
  }
}

export const getNewsUrlHome = (article) => {
  let articleId = article.articleID
  tab = tab ? tab : article.type
  return {
    as: eval(ARTICLE_VIEW.as),
    href: ARTICLE_VIEW.href,
  }
}

/**
 *
 * @param { Object } stadium
 */
export const getStadiumUrl = (stadium) => {
  let stadiumId = stadium.id
  let stadiumSlug = stadium.name
    .replace(/[^a-zA-Z0-9]+/g, ' ')
    .split(' ')
    .join('-')
    .toLowerCase()
  return {
    as: eval(STADIUM_VIEW.as),
    href: STADIUM_VIEW.href,
  }
}

export const getSeriesTabUrl = (tabName) => {
  if (tabName === 'All') {
    return { href: '/series', as: '/series' }
  }
  let tab = tabName.toLowerCase()
  return { href: SERIES_TAB_SWITCH.href, as: eval(SERIES_TAB_SWITCH.as) }
}

export const getRecordUrl = (format, type, item) => {
  return {
    as: eval(RECORD_VIEW.as),
    href: RECORD_VIEW.href,
  }
}

export const getAUthorUrl = (authorName, authorID) => {
  return {
    as: eval(AUTHOR_NEWS_VIEW.as),
    href: AUTHOR_NEWS_VIEW.href,
  }
}

// export const getAUthorUrl2 = (authorName) => {

//
//   let href, as;

//     as = `/news/authors/${authorName}`;
//     href = '/news/[...slugs]';

//   return {
//     as: as,
//     href: href
//   };
// };

export const getFrcSeriesHomeUrl = (match) => {
  let matchId = match.matchID ? match.matchID : match.matchId
  let matchName = `${match.homeTeamShortName}-vs-${match.awayTeamShortName}-`.toLowerCase()
  let seriesSlug =
    matchName +
    match.matchName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()

  return {
    as: eval(FRC_SERIES_VIEW.as),
    href: FRC_SERIES_VIEW.href,
  }
}

// ex - create-team, expert-team
export const getFrcSeriesTabUrl = (match, tab) => {
  let matchId = match.matchId ? match.matchId : match.matchID
  // let matchId ="194161"
  let seriesSlug = match.seriesSlug
    ? match.seriesSlug
    : match.MatchName
    ? match.MatchName.replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
    : match.matchName
        .replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
  // let seriesSlug =  "sl-vs-wi-2nd-odi-west-indies-tour-of-sri-lanka-2020"
  return {
    // as: eval(FRC_SERIES_CATEGORY_VIEW.as),
    href: eval(FRC_SERIES_CATEGORY_VIEW.as),
  }
}

// ex - safeTeam 1 , agrresive team
export const getFrcSeriesTeamUrl = (match, tab, fantasyTeam) => {
  let matchId = match.matchId ? match.matchId : match.matchID

  let seriesSlug = match.seriesSlug
    ? match.seriesSlug
    : match.MatchName.replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
  return {
    as: eval(FRC_SERIES_TEAM_VIEW.as),
    href: FRC_SERIES_TEAM_VIEW.href,
  }
}

export const getStatshubFrcUrl = (matchId, seriesSlug, tab) => {
  // let seriesSlug =  "sl-vs-wi-2nd-odi-west-indies-tour-of-sri-lanka-2020"
  return {
    as: eval(FRC_SERIES_CATEGORY_VIEW.as),
    href: FRC_SERIES_CATEGORY_VIEW.href,
  }
}
