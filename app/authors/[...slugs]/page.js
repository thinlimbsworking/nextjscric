'use client'
import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { ARTICLE_LIST_AXIOS } from '../../api/queries'
import MetaDescriptor from '../../components/MetaDescriptor'
const backIconWhite = '/svgs/backIconWhite.svg'

// import { ARTICLE_LIST_AXIOS, GET_ARTICLE_DETAILS, LIVE_BLOGGING_AXIOS, } from '../../api/queries';
import Head from 'next/head'
import { useRouter } from 'next/navigation'
import AuthorBlog from '../../components/news/authorBlog'
const arrowLeft = '/pngs/arrow-left.png'

let scrollTop = null
import {
  getArticleTabUrl,
  getLiveArticleUrl,
  getNewsUrl,
} from '../../api/services'

async function getServerData() {
  const res = await axios.post(process.env.API, {
    query: ARTICLE_LIST_AXIOS,
    variables: { type: 'news', page: 0 },
  })
  return {
    type: 'authors',
    data: res?.data,
  }
}

export default function Live({ params }) {
  const router = useRouter()
  const [props, setProps] = useState()

  const type = params.slug

  useEffect(() => {
    getServerData().then((res) => setProps(res.data))
  }, [])
  let data = props?.data

  if (router.isFallback) {
    return <Loading />
  } else {
    return (
      <>
        <MetaDescriptor />
        <Head>
          <title>
            Latest Cricket News & Articles | Cricket Updates | Cricket.com
          </title>
          <meta
            name="description"
            content="IPL Stats - Find the strengths and weakness of all your favourite IPL players and teams"
          />
          <meta
            property="og:title"
            content="Latest Cricket News & Articles | Cricket Updates | Cricket.com"
          />
          <meta
            property="og:description"
            content=" Find indepth stats, strengths and weakness of all your favourite IPL players and teams"
          />
        </Head>{' '}
        <AuthorBlog
          props={props}
          params={params}
          data={data}
          News_Live_Tab_URL={getLiveArticleUrl}
        />
      </>
    )
  }
}
