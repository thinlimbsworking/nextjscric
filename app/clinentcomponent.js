'use client'
import { useState, useEffect, useRef, useLayoutEffect, useContext } from 'react'
import axios from 'axios'
import { Navigation } from 'swiper'
import { LocalStorageContext } from './components/layout'

import Tab from './components/shared/Tab'
import {
  FEATURED_MATCHES_AXIOS,
  FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE,
  FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE_AXIOS,
  FEATURED_MATCHES,
  HOME_PRE_MATCH_CRICLYTICS,
} from '../api/queries'

import Scores from './components/commom/score'
import Heading from './components/shared/heading'
// import { useEffect, useState } from 'react';
import { useQuery } from '@apollo/react-hooks'
import Loading from './components/loading'
import dynamic from 'next/dynamic'
// import Slider from  './components/slider'
import MatchReel from './components/crcltics/swipeCrclytics/matchReel'
import BuildYourTeam from './components/buildYourTeam'
import Ipldashbord from './components/ipldashbord'
import Iplstanding from './components/iplstanding'
import MatchRating from './components/crcltics/swipeCrclytics/matchRating'
// import BuildYourTeam from  '../com'
import UpcomingMatches from './components/upcoming'
// import UpcomingMatches from "./components/upcomingMatches";

import Datadigest from './components/datadigest'
import ImageWithFallback from './components/commom/Image'

import { Swiper, SwiperSlide } from 'swiper/react'
import PlayerIndex from './components/clinet/homePAge/playerIndex'
import TeamAnalysis from './components/clinet/homePAge/teamAnalysis'
import RunComparisonCompleted from './components/crcltics/swipeCrclytics/runComparisonCompleted'
import StatHubStadium from './components/clinet/homePAge/stathubStadium'
import Slider from './components/slider'
import Slider2 from './components/slider'

import UpcomigMatch from './components/clinet/homePAge/upcomingCards'
import CompltedMatch from './components/clinet/homePAge/compeletedMatchCard'
import ClientComponent from './components/clinet/main'
import UpcomingMatchHome from './components/clinet/homePAge/upcoming'
import LivematchHome from './components/clinet/homePAge/liveMatchHome'
import SocialTracker from './components/servercomponent/hompage/scoialTracker'
import TopPerformer from './components/clinet/homePAge/topPerformerHome'
import 'swiper/css'
import Link from 'next/link'
// import ptom from '/pngsv2/ptom.png'
import HomePageCLinet from './components/clinet/homePAge/homePageClinet'
import ContentPage from './components/clinet/homePAge/contentPage'
export default function ClientComponent2({ children, ...props }) {
  const handleNavigation = (match) => {
    // alert(8)

    const event =
      match.matchStatus === 'upcoming' || match.matchStatus === 'live'
        ? 'CommentaryTab'
        : 'SummaryTab'

    // const event = match.matchStatus === 'upcoming' || match.matchStatus === 'live' ? 'CommentaryTab' : 'SummaryTab';
    let matchStatus =
      match?.matchStatus === 'completed' ? 'match-score' : 'live-score'

    let currentTab = match.isAbandoned
      ? 'commentary'
      : match.matchStatus === 'upcoming' || match.matchStatus === 'live'
      ? 'Commentary'
      : 'Summary'

    let matchName = `${
      match?.homeTeamName ? match?.homeTeamName : match?.homeTeamShortName
    }-vs-${
      match?.awayTeamName ? match?.awayTeamName : match?.awayTeamShortName
    }-${match?.matchNumber ? match?.matchNumber.split(' ').join('-') : ''}-`

    let seriesName =
      matchName.toLowerCase() +
      (match?.seriesName
        ? match.seriesName
            .replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()
        : '')

    return `/${matchStatus.toLowerCase()}/${
      match.matchID
    }/${currentTab.toLowerCase()}/${seriesName}`
  }

  const { getTabContent, setTabContent } = useContext(LocalStorageContext)

  const [pegIndex, setPag] = useState(0)
  const [pegIndex2, setPag2] = useState(0)
  const [showPlayerIndex, setShowPlayerIndex] = useState(false)
  const [showTeamInfo, setShowTeamInfo] = useState(false)

  const [showFormIndex, setShowFormIndex] = useState(false)

  let scrl = useRef(null)
  let scrl1 = useRef(null)
  const [scrollWidth, setScrollWidth] = useState(0)

  const [scrollX, setscrollX] = useState(0)
  const [scrollX1, setscrollX1] = useState(0)
  const [tabData, setTabData] = useState([])
  const [selectedTabData, setSelectedTabData] = useState('all')

  const [contenTheme, setContentTheme] = useState(
    getTabContent() || 'selectMatch',
  )
  const [scrolEnd, setscrolEnd] = useState(false)
  const [scrolEnd1, setscrolEnd1] = useState(false)
  const [use, setUse] = useState(0)
  const [value, setValue] = useState(0)
  // const [data,setData]=useState([])

  let { loading, error, data } = useQuery(FEATURED_MATCHES, {
    pollInterval: 10000,
    onCompleted: (data) => {
      let dumTab = []

      data.featurematch[0].seriesList.map((item, index) => {
        dumTab.push(item)
      })
      setTabData(dumTab)
    },
  })

  useEffect(() => {
    if (scrl && scrl.current && scrl.current.offsetWidth) {
      setScrollWidth(scrl && scrl.current && scrl.current.offsetWidth)
    }
  }, [showTeamInfo, showFormIndex, showPlayerIndex])

  const slide = (shift) => {
    // scrl.current.scrollLeft += shift;

    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: 'smooth',
    })

    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  const slideZero = () => {
    scrl.current?.scrollTo({
      left: 0,
      behavior: 'smooth',
    })
    // setscrollX(0);
  }

  const slide1 = (shift) => {
    scrl1.current.scrollLeft += shift
    setscrollX1(scrollX1 + shift)

    if (
      Math.floor(scrl1.current.scrollWidth - scrl1.current.scrollLeft) <=
      scrl1.current.offsetWidth
    ) {
      setscrolEnd1(true)
    } else {
      setscrolEnd1(false)
    }
  }
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  if (loading) {
    return <Loading />
  }

  if (data) {
    return (
      <>
        {!props.desktopMode && (
          <div className=" flex flex-col  md:hidden  relative  ">
            {showPlayerIndex && (
              <div
                onClick={() => setShowPlayerIndex(false)}
                className=" w-full h-screen  z-50  absolute   flex items-start justify-center   "
                style={{ height: '300vh', opacity: 100 }}
              >
                <div className="flex relative h-full w-full">
                  <div className="w-full  absolute z-100  mx-4 h-28   "> </div>
                </div>
              </div>
            )}

            {showTeamInfo && (
              <div
                onClick={() => setShowTeamInfo(false)}
                className=" w-full h-screen  z-50  absolute   flex items-start justify-center   "
                style={{ height: '300vh', opacity: 100 }}
              >
                <div className="flex relative h-full w-full">
                  <div className="w-full  absolute z-100  mx-4 h-28   "> </div>
                </div>
              </div>
            )}

            {showFormIndex && (
              <div
                onClick={() => setShowFormIndex(false)}
                className=" w-full h-screen  z-50  absolute   flex items-start justify-center  "
                style={{ height: '300vh', opacity: 100 }}
              >
                <div className="flex relative h-full w-full">
                  <div className="w-full  absolute z-100  mx-4 h-28   ">s </div>
                </div>
              </div>
            )}

            <div className=" w-full h-10 flex items-center justify-center   overflow-hidden px-3  mt-5 relative">
              <div
                className={`
              w-full  h-10 rounded-md relative overflow-hidden flex items-center justify-center border ${
                contenTheme == 'selectMatch'
                  ? 'border-red-5  bg-red-5'
                  : 'border-[#D8993B]'
              }
              `}
                onClick={() => (
                  setContentTheme('selectMatch'), setTabContent('selectMatch')
                )}
              >
                {contenTheme !== 'selectMatch' ? (
                  <div className="flex absolute left-0 o-50 tab-bg-animation">
                    <div
                      className="mr-1 bg-white/50"
                      style={{ height: 100, width: 10 }}
                    />
                    <div
                      className="bg-white/50"
                      style={{ height: 100, width: 5 }}
                    />
                  </div>
                ) : (
                  <></>
                )}
                <p>Match Center</p>
              </div>
              <div
                className={`  overflow-hidden absolute  bg-basebg ${
                  contenTheme == 'selectMatch'
                    ? 'border border-red-5  '
                    : 'border  border-[#D8993B]'
                }   z-50  h-12   w-12     rounded-full `}
              >
                <img
                  className="  flex items-center justify-center  realtive    overflow-hidden p-2 z-50  "
                  src="/pngsV2/cricket.png"
                />
              </div>
              <div
                className={`
             w-full  h-10 rounded-md relative overflow-hidden  flex items-center justify-center border ${
               contenTheme == 'selectContent'
                 ? 'border-[#D8993B]  bg-[#D8993B] text-black'
                 : 'border-red-5'
             }
             `}
                onClick={() => (
                  setContentTheme('selectContent'),
                  setTabContent('selectContent')
                )}
              >
                {contenTheme !== 'selectContent' ? (
                  <div className="flex absolute left-0 o-50 tab-bg-animation">
                    <div
                      className="mr-1 bg-white/50"
                      style={{ height: 100, width: 10 }}
                    />
                    <div
                      className="bg-white/50"
                      style={{ height: 100, width: 5 }}
                    />
                  </div>
                ) : (
                  <></>
                )}
                <p>Newsroom</p>
              </div>
            </div>

            <div className=" w-full mt-3 px-4 ">
              <Slider
                data={data && data.featurematch}
                setPag={setPag}
                pegIndex={pegIndex}
              />
            </div>
            {/* {console.log("data.featurematch[pegIndex].matchStatus", data.featurematch[pegIndex].matchStatus)}
         { (data.featurematch[pegIndex].matchStatus == "completed" || 
              data.featurematch[pegIndex].matchStatus == "upcoming" ||
              data.featurematch[pegIndex].matchStatus =="live") && */}
            <div className="flex items-center justify-center flex-col w-full">
              <div className="flex items-center justify-start w-full  ">
                {data.featurematch[pegIndex].matchStatus == 'completed' && (
                  <CompltedMatch
                    matchStatus={data.featurematch[pegIndex].matchStatus}
                    matchType={data && data.featurematch[pegIndex].matchType}
                    mainData={data && data.featurematch[pegIndex]}
                    tabContent={contenTheme}
                    showPlayerIndex={showPlayerIndex}
                    setShowPlayerIndex={setShowPlayerIndex}
                    seriesName={data.featurematch[pegIndex].seriesName}
                    matchID={data.featurematch[pegIndex].matchID}
                  />
                )}

                {data.featurematch[pegIndex].matchStatus == 'upcoming' && (
                  <UpcomingMatchHome
                    matchStatus={data.featurematch[pegIndex].matchStatus}
                    showTeamInfo={showTeamInfo}
                    setShowFormIndex={setShowFormIndex}
                    showPlayerIndex={showPlayerIndex}
                    setShowTeamInfo={setShowTeamInfo}
                    setShowPlayerIndex={setShowPlayerIndex}
                    showFormIndex={showFormIndex}
                    seriesName={data.featurematch[pegIndex].seriesName}
                    matchType={data && data.featurematch[pegIndex].matchType}
                    mainData={data && data.featurematch[pegIndex]}
                    tabContent={contenTheme}
                    matchID={data.featurematch[pegIndex].matchID}
                  />
                )}
                {data.featurematch[pegIndex].matchStatus == 'live' &&
                  data.featurematch[pegIndex].isLiveCriclyticsAvailable && (
                    <LivematchHome
                      data2={data}
                      matchStatus={data.featurematch[pegIndex].matchStatus}
                      seriesName={data.featurematch[pegIndex].seriesName}
                      matchType={data && data.featurematch[pegIndex].matchType}
                      mainData={data && data.featurematch[pegIndex]}
                      setShowPlayerIndex={setShowPlayerIndex}
                      tabContent={contenTheme}
                      showPlayerIndex={showPlayerIndex}
                      matchID={data.featurematch[pegIndex].matchID}
                    />
                  )}
              </div>
            </div>

            {contenTheme == 'selectMatch' && (
              <div className="lp:flex justify-between w-full items-start">
                {/* <div className='lg:w-1/2 md:w-1/2'>
             

              <Ipldashbord data={data.featurematch[pegIndex].iplDashData} />
             
            </div> */}
                <div className="lg:w-1/2 md:w-1/2">
                  <Iplstanding data={data.featurematch[pegIndex].pointsTable} />
                </div>
                {/* <RunComparisonCompleted matchType={data.featurematch[pegIndex].matchType}  matchID={data.featurematch[pegIndex].matchID} /> */}
              </div>
            )}
            {contenTheme == 'selectContent' && (
              <div className="m-2">
                <div className="flex flex-col">
                  {' '}
                  <ContentPage matchID={props.matchID} />
                </div>
              </div>
            )}
            {/* <div className="flex overflow-scroll">
            <UpcomingMatches />
          </div> */}
          </div>
        )}

        {/* DESKTOP */}

        {props.desktopMode && (
          <div className="hidden lg:block md:block  relative     ">
            <div className="py-2  relative  flex  mx-auto max-w-7xl lg:px-24 md:px-24  align-center justify-between  ">
              <div
                className={`w-full flex items-center justify-start   dark:border-none dark:bg-gray rounded-full`}
              >
                {tabData.map((item, index) => (
                  <>
                    {index == 0 && (
                      <div
                        onClick={() => {
                          setSelectedTabData('all')
                          setTimeout(() => {
                            slideZero()
                          }, 1)

                          // slideZero();
                        }}
                        className={`flex rounded-full  p-1 justify-center items-center cursor-pointer truncate  w-28 m-1 ${
                          selectedTabData === 'all'
                            ? 'text-white  bg-black dark:border  dark:border-green dark:text-green dark:bg-transparent'
                            : 'hover:bg-light_gray  dark:bg-transparent '
                        }`}
                      >
                        <span className="font-mnr text-xs font-semibold ">
                          {' '}
                          All Matches{' '}
                        </span>
                      </div>
                    )}
                    <div
                      onClick={() => {
                        setSelectedTabData(item)

                        //  scrl1.current.scrollLeft
                      }}
                      className={`flex rounded-full p-1 justify-center items-center cursor-pointer truncate w-28 m-1 ${
                        item.name === selectedTabData.name
                          ? 'text-white bg-black dark:border dark:border-green dark:text-green dark:bg-transparent'
                          : 'hover:bg-light_gray dark:bg-transparent '
                      }`}
                    >
                      <span className="font-mnr  text-xs font-semibold">
                        {' '}
                        {item.name}{' '}
                      </span>
                    </div>
                  </>
                ))}
              </div>
            </div>

            <div className="flex flex-col bg-basebg py-4 relative ">
              {false && (
                <div className=" relative flex    mx-auto max-w-7xl lg:px-24 md:px-24    item-center justify-center">
                  <div
                    className="white pointer dn db-ns z-999 outline-0 flex  item-center justify-center "
                    onClick={() => slide(-300)}
                    id="swiper-button-prev"
                  >
                    <svg width="30" focusable="false" viewBox="0 0 24 24">
                      <path
                        fill="#000"
                        d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                      ></path>
                      <path fill="none" d="M0 0h24v24H0z"></path>
                    </svg>
                  </div>

                  <div className="flex  w-full   ">
                    <div
                      className={` relative  flex  overflow-x-scroll items-center  w-full} `}
                      style={{
                        width:
                          scrl && scrl.current && scrl.current.scrollWidth + 20,
                      }}
                      ref={scrl}
                      onScroll={scrollCheck}
                    >
                      <div className="flex  item-center justify-center">
                        {scrl1 &&
                        scrl.current &&
                        data.featurematch.length > 0 ? (
                          data.featurematch.map(
                            (card) =>
                              (card.seriesID == selectedTabData.tourID ||
                                selectedTabData == '') && (
                                <div
                                  style={{
                                    width:
                                      (scrl &&
                                        scrl.current &&
                                        scrl.current.offsetWidth) / 3,
                                  }}
                                  key={card.matchID}
                                  className=" mx-1  flex justify-center  w-full items-center cursor-pointer "
                                >
                                  {/* <Link className='w-full' {...getContest(card.matchID)} passHref > */}
                                  <div className=" w-full  cursor-pointer flex flex-col ">
                                    <div className=" w-full  cursor-pointer flex flex-col">
                                      {card && card.matchID ? (
                                        <Scores
                                          homePage={true}
                                          from={'schedule'}
                                          featured={true}
                                          data={card}
                                          playTheOdds={false}
                                        />
                                      ) : (
                                        <></>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              ),
                          )
                        ) : (
                          <></>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="flex relative items-center justify-center ">
                    <div
                      onClick={() =>
                        slide(
                          +(
                            (scrl && scrl.current && scrl.current.offsetWidth) /
                            3
                          ) + 30,
                        )
                      }
                      id="swiper-button-prev"
                      className="white pointer dn db-ns z-999 outline-0 flex  item-center justify-center  absolute left-10"
                    >
                      <svg width="44" viewBox="0 0 24 24">
                        <path
                          fill="#456"
                          d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                        ></path>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                      </svg>
                    </div>
                  </div>
                </div>
              )}
              <div className=" relative  flex   mx-auto max-w-7xl  lg:px-24 md:px-24  align-center ">
                <div className="flex relative items-center justify-center ">
                  {(selectedTabData == 'all' ||
                    (selectedTabData !== '' &&
                      selectedTabData &&
                      selectedTabData.count > 2)) && (
                    <div
                      className={`${
                        scrollX < 280 ? 'opacity-40' : 'opacity-100'
                      }  white pointer dn db-ns z-999 outline-0 flex  item-center justify-center absolute -left-10  cursor-pointer `}
                      onClick={() =>
                        scrollX !== 0 && scrollX > -1 && slide(-300)
                      }
                      id="swiper-button-prev cursor-pointer"
                    >
                      <svg width="30" focusable="false" viewBox="0 0 24 24">
                        <path
                          fill="#fff"
                          d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                        ></path>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                      </svg>
                    </div>
                  )}
                </div>

                <div
                  class=" w-full flex gap-3 snap-x snap-mandatory pt-4  overflow-scroll touch-auto  "
                  ref={scrl}
                  onScroll={scrollCheck}
                >
                  {true &&
                    data &&
                    data.featurematch.map(
                      (card, index) =>
                        (selectedTabData == 'all' ||
                          card.seriesID == selectedTabData.tourID ||
                          selectedTabData == '') && (
                          <Link href={handleNavigation(card)}>
                            {' '}
                            <div class="snap-start snap-x" key={index}>
                              <div
                                class="snap-x w-96 rounded-xl bg-white  duration-700  cursor-pointer"
                                style={{ width: '22rem' }}
                              >
                                {card && card.matchID ? (
                                  <Scores
                                    from={'schedule'}
                                    homePage={true}
                                    featured={true}
                                    data={card}
                                    playTheOdds={false}
                                  />
                                ) : (
                                  <></>
                                )}
                              </div>
                            </div>
                          </Link>
                        ),
                    )}
                </div>

                <div className="flex relative items-center justify-center ">
                  {(selectedTabData == 'all' ||
                    (selectedTabData !== '' &&
                      selectedTabData &&
                      selectedTabData.count > 2)) && (
                    <div
                      onClick={() =>
                        !scrolEnd &&
                        slide(
                          +(
                            (scrl && scrl.current && scrl.current.offsetWidth) /
                            3
                          ) + 30,
                        )
                      }
                      id="swiper-button-prev"
                      className={`${
                        scrolEnd ? 'opacity-40' : 'opacity-100'
                      } white pointer dn db-ns z-999 outline-0 flex  item-center justify-center  -right-8 absolute z-10  cursor-pointer`}
                    >
                      <svg width="30" viewBox="0 0 24 24">
                        <path
                          fill="#fff"
                          d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                        ></path>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                      </svg>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        )}

        {children}

        {!props.desktopMode && (
          <>
            {contenTheme == 'selectMatch' && (
              <div className="  md:hidden ">
                <TopPerformer />
              </div>
            )}
            {contenTheme == 'selectMatch' && (
              <div className=" md:hidden  ">
                <UpcomingMatches />
              </div>
            )}
            {/* <div className=' lg:hidden md:hidden'>
                <UpcomingMatches data={data} />
              </div>  */}

            {contenTheme == 'selectMatch' && (
              <div className=" md:hidden">
                <Datadigest />
              </div>
            )}
          </>
        )}
      </>
    )
  }
}
