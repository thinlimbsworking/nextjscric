import React from 'react'
import CleverTap from 'clevertap-react'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/router'
import { MATCH_DETAIL_LAYOUT } from '../../constant/Links'
import { MATCH_SUMMARY } from '../../api/queries'
import { format } from 'date-fns'
import Link from 'next/link'
const empty = '/svgs/empty.svg'
const RightSchevronWhite = '/svgs/RightSchevronWhite.svg'
const cricket = '/pngs/cricket.png'
const bowlerOverlay = '/pngs/bowlerOverlay.png'
const flagPlaceHolder = '/pngsV2/flag_dark.png'
const playerAvatar = '/pngsV2/playerph.png'

export default function ScorecardSummary({ match, browser, noURL, ...props }) {
  const router = useRouter()

  const { loading, error, data } = useQuery(MATCH_SUMMARY, {
    variables: {
      matchID: props.matchID,
      status: props.a23 ? 'completed' : props.status,
    },
  })

  const getScoreUrl = (match) => {
    let matchStatus =
      match.matchStatus === 'completed' ? 'match-score' : 'live-score'
    let matchID = match.matchID.toString()
    let matchName = `${match.homeTeamName}-vs-${
      match.awayTeamName
    }-${match.matchNumber.split(' ').join('-')}-`
    let seriesName =
      matchName.toLowerCase() +
      match.seriesName
        .replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
    let currentTab = match.isAbandoned
      ? 'commentary'
      : match.matchStatus === 'completed'
      ? 'summary'
      : 'scorecard'

    return {
      as: eval(MATCH_DETAIL_LAYOUT.as),
      href: eval(MATCH_DETAIL_LAYOUT.href),
    }
  }
  const handleCriclyticsNav = () => {
    if (match.isCricklyticsAvailable) {
      CleverTap.initialize('SummaryTab', {
        Source: 'Summary',
        MatchID: match.matchID,
        SeriesID: match.seriesID,
        TeamAID: match.matchScore[0].teamID,
        TeamBID: match.matchScore[1].teamID,
        MatchFormat: match.matchType,
        MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
        MatchStatus: match.matchStatus,
        // CriclyticsEngine: "CompletedMS",
        Platform: localStorage.Platform,
      })
    }
  }

  if (error) return <div></div>
  if (loading) {
    return <div></div>
  }
  // scorecard-up-arrow-triangle
  else
    return (
      <>
        {data && data.matchSummary ? (
          <div
            className={`bg-basebg  ${!props.isDetails ? '' : ''} shadow-4 ${
              global && global.window && global.window.location.pathname === '/'
                ? ''
                : ''
            }`}
          >
            {/* title */}
            {}
            {!props.isDetails && noURL === undefined && !props.a23 && (
              <div className="flex scorecard-up-arrow-triangle p-3 items-center justify-between border-b-white">
                <h2 className="text-white text-sm f6-l font-semibold">
                  MATCH SUMMARY
                </h2>
                <Link {...getScoreUrl(match)} passHref>
                  <img
                    className="pointer"
                    alt={RightSchevronWhite}
                    src={RightSchevronWhite}
                    onClick={handleCriclyticsNav}
                  />
                </Link>
              </div>
            )}

            {/* content */}

            {/* topBatsman */}
            <div className="md:hidden lg:hidden xl:hidden">
              <div className="">
                <div className="flex justify-start items-center relative">
                  <img
                    className="absolute contain z-0 top-0 left-0"
                    width="120"
                    height="120"
                    alt={cricket}
                    src={cricket}
                  />

                  <img
                    className="h-36 z-0 px-4 mt-2 "
                    alt=""
                    src={`https://images.cricket.com/players/${data.matchSummary.bestBatsman.playerID}_headshot_safari.png`}
                    onError={(e) => {
                      e.target.src = playerAvatar
                    }}
                  />

                  <div className="">
                    <div className="text-white flex items-center">
                      <img
                        alt=""
                        src={`https://images.cricket.com/teams/${data.matchSummary.bestBatsman.playerTeamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = flagPlaceHolder
                        }}
                        className="w-8 h-5 shadow-4 mr-2"
                      />
                      <span className="text-md font-semibold">
                        {' '}
                        {data.matchSummary.bestBatsman.playerName}
                      </span>
                    </div>

                    {data.matchSummary.bestBatsman.battingStatsList &&
                    props.matchType !== 'Test' ? (
                      <div className="rounded text-white mt-2 bg-white/20 mr-3">
                        <div className="flex justify-between items-center">
                          <div className="flex justify-center items-center px-2 ">
                            <span className="f5 font-medium mr-1">
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].runs
                              }
                              {data.matchSummary.bestBatsman.battingStatsList[0]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="text-xs font-medium">
                              (
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].balls
                              }
                              )
                            </span>
                          </div>

                          <div className="flex m-1 bg-basebg rounded items-center">
                            <span className="text-xs near-white p-1 text-center">
                              4s{' '}
                              <span className="text-white text-sm font-semibold">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].fours
                                }
                              </span>{' '}
                            </span>
                            <span className="w-[1px] h-5 bg-gray-2" />

                            <span className="text-xs near-white p-1">
                              6s{' '}
                              <span className="white text-sm font-semibold">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].sixes
                                }
                              </span>{' '}
                            </span>
                          </div>
                        </div>

                        {data.matchSummary.bestBatsman.bowlingStatsList && (
                          <div>
                            <div className="flex justify-between items-center">
                              <div className="flex w-40 justify-center items-center p-2">
                                <span className="f5 font-medium mr-1 nowrap">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-xs">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              <div className="flex w-52 p-1 justify-end">
                                <span className="f6 near-white w-full bg-navy py-2 border-2 text-center nowrap">
                                  Eco:{' '}
                                  <span className="text-white text-sm font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].economyRate
                                    }
                                  </span>{' '}
                                </span>
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    ) : (
                      <div className="border-2 text-white mt-2 bg-basebg mr-3">
                        <div className="flex justify-between items-center">
                          <div className="flex justify-center items-center p-2">
                            <span className="f5 font-medium">
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].runs
                              }
                              {data.matchSummary.bestBatsman.battingStatsList[0]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="text-xs pl-2">
                              (
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].balls
                              }
                              )
                            </span>
                          </div>
                          <div className="flex justify-center items-center p-2">
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <span className="f5 font-medium">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[1].runs
                                }
                              </span>
                            )}
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <span className="text-xs">{`(${data.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                            )}
                          </div>
                        </div>

                        {data.matchSummary.bestBatsman.bowlingStatsList && (
                          <div>
                            <div className="flex justify-between items-center">
                              <div className="flex justify-center items-center p-2">
                                <span className="f5 font-medium">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-xs pl-2">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="">&</div>
                              )}
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="flex justify-center items-center p-2">
                                  <span className="f5 font-medium">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].runsConceded
                                    }
                                  </span>
                                  <span className="text-xs pl-2">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].overs
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                            </div>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>

              {/* score details */}
              <div className="md:flex lg:flex xl:flex flex-wrap">
                {data.matchSummary.inningOrder
                  .slice(
                    0,
                    props.matchType !== 'Test'
                      ? 2
                      : data.matchSummary.inningOrder.length,
                  )
                  .map((inning, i) => (
                    <div key={i} className="text-white md:w-52 xl:w-52 lg:w-52">
                      <div>
                        {/* Team Details */}

                        <div className="flex bg-gray justify-between items-center p-2 px-3">
                          <div className="flex items-center">
                            <img
                              alt=""
                              src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                              onError={(e) => {
                                e.target.src = flagPlaceHolder
                              }}
                              className="w-7 h-4 mr-2"
                            />
                            <div className="text-xs font-semibold">
                              {data.matchSummary[inning].teamName}
                            </div>
                          </div>
                          <div>
                            <span className="text-sm font-semibold">
                              {data.matchSummary[inning].runs[i <= 1 ? 0 : 1]}/
                              {
                                data.matchSummary[inning].wickets[
                                  i <= 1 ? 0 : 1
                                ]
                              }
                            </span>
                            <span className="text-xs">
                              {' '}
                              ({data.matchSummary[inning].overs[i <= 1 ? 0 : 1]}
                              )
                            </span>
                          </div>
                        </div>

                        <div className="flex justify-between items-center">
                          {data.matchSummary[inning][
                            i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                          ].topBatsman &&
                            data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].topBatsman.battingStatsList && (
                              <div className="flex w-full justify-between p-2 py-3">
                                <span className="text-xs font-medium truncate w-6/12">
                                  {
                                    data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.playerName
                                  }
                                </span>
                                <span className="text-xs font-medium">
                                  {
                                    data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.battingStatsList[0].runs
                                  }
                                  {data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].topBatsman.battingStatsList[0].isNotOut ===
                                  true
                                    ? '*'
                                    : ''}
                                </span>
                              </div>
                            )}

                          <div className="h-8 w-[1px] bg-black/80" />

                          {data.matchSummary[
                            inning === 'homeTeamData'
                              ? 'awayTeamData'
                              : 'homeTeamData'
                          ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                            .topBowler &&
                            data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .topBowler.bowlingStatsList && (
                              <div className="flex w-full justify-between p-2 py-3">
                                <span className="text-xs font-medium truncate w-6/12">
                                  {
                                    data.matchSummary[
                                      inning === 'homeTeamData'
                                        ? 'awayTeamData'
                                        : 'homeTeamData'
                                    ][
                                      i <= 1
                                        ? 'bowlerSummary1'
                                        : 'bowlerSummary2'
                                    ].topBowler.playerName
                                  }
                                </span>
                                <span className="text-xs font-medium">
                                  {
                                    data.matchSummary[
                                      inning === 'homeTeamData'
                                        ? 'awayTeamData'
                                        : 'homeTeamData'
                                    ][
                                      i <= 1
                                        ? 'bowlerSummary1'
                                        : 'bowlerSummary2'
                                    ].topBowler.bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary[
                                      inning === 'homeTeamData'
                                        ? 'awayTeamData'
                                        : 'homeTeamData'
                                    ][
                                      i <= 1
                                        ? 'bowlerSummary1'
                                        : 'bowlerSummary2'
                                    ].topBowler.bowlingStatsList[0].runsConceded
                                  }
                                </span>
                              </div>
                            )}
                        </div>

                        <div className="bg-black/80 h-[1px] mx-2" />

                        {props.matchType !== 'Test' && (
                          <div className="flex  justify-between items-center">
                            {data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].runnerBatsman &&
                              data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].runnerBatsman.battingStatsList && (
                                <div className="flex w-full justify-between p-2 py-3">
                                  <span className="text-xs font-medium truncate w-6/12">
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].runnerBatsman.playerName
                                    }
                                  </span>
                                  <span className="text-xs font-medium">
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].runnerBatsman.battingStatsList[0].runs
                                    }
                                    {data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].runnerBatsman.battingStatsList[0]
                                      .isNotOut === true
                                      ? '*'
                                      : ''}
                                  </span>
                                </div>
                              )}

                            <div className="h-8 w-[2px] bg-black/80 " />

                            {data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .runnerBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler.bowlingStatsList && (
                                <div className="flex w-full justify-between p-2  py-3">
                                  <span className="text-xs font-medium truncate w-6/12">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.playerName
                                    }
                                  </span>
                                  <span className="text-xs font-medium">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0]
                                        .runsConceded
                                    }
                                  </span>
                                </div>
                              )}
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
              </div>

              {/* topBowler */}

              {data.matchSummary.bestBowler.bowlingStatsList &&
              props.matchType !== 'Test' ? (
                <div className="flex justify-between text-white items-center mx-2 mt-2 relative overflow-hidden ">
                  {/* player NAME */}
                  <div className="flex items-center w-1/4">
                    <img
                      alt=""
                      src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                      onError={(e) => {
                        e.target.src = flagPlaceHolder
                      }}
                      className="ib w-16 h-1 shadow-4"
                    />
                    <div className="text-md font-semibold px-2">
                      {data.matchSummary.bestBowler.playerName}
                    </div>
                  </div>

                  {/* player Image */}
                  <div className=" w-1/2 flex justify-center h-24 overflow-hidden">
                    <img
                      className="object-cover object-top h-full "
                      alt=""
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                      onError={(e) => {
                        e.target.src = playerAvatar
                      }}
                    />
                    <img
                      width="100"
                      height="100"
                      className="absolute contain z-0 top-0"
                      alt={cricket}
                      src={cricket}
                    />
                  </div>

                  {/* player stats */}
                  <div className="w-1/4">
                    {data.matchSummary.bestBowler.battingStatsList &&
                      (data.matchSummary.bestBowler.battingStatsList[0].runs !==
                        0 ||
                        data.matchSummary.bestBowler.battingStatsList[0]
                          .balls !== 0) && (
                        <div className="flex justify-center items-center p-1">
                          <span className="f5 font-medium mr-1 nowrap">
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .runs
                            }
                            {data.matchSummary.bestBowler.battingStatsList[0]
                              .isNotOut === true
                              ? '*'
                              : ''}
                          </span>
                          <span className="text-xs ">
                            (
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .balls
                            }
                            )
                          </span>
                        </div>
                      )}
                    {data.matchSummary.bestBowler.bowlingStatsList && (
                      <div className="flex justify-center items-center p-1">
                        <span className="f5 font-medium mr-1 nowrap">
                          {
                            data.matchSummary.bestBowler.bowlingStatsList[0]
                              .wickets
                          }
                          /
                          {
                            data.matchSummary.bestBowler.bowlingStatsList[0]
                              .runsConceded
                          }
                        </span>
                        <span className="text-xs ">
                          (
                          {
                            data.matchSummary.bestBowler.bowlingStatsList[0]
                              .overs
                          }
                          )
                        </span>
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <div className="flex justify-between white items-center relative mx-3">
                  {/* player Image */}
                  <div>
                    <img
                      className="absolute contain z-0 top-0 left-0 bottom-0 "
                      width="80"
                      height="70"
                      alt={bowlerOverlay}
                      src={bowlerOverlay}
                    />
                    <img
                      className="-mt-3 h-36"
                      alt=""
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                      onError={(e) => {
                        e.target.src = playerAvatar
                      }}
                    />
                  </div>

                  {/* player NAME */}
                  <div className="flex justify-center items-center center">
                    <img
                      alt=""
                      src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                      onError={(e) => {
                        e.target.src = flagPlaceHolder
                      }}
                      className="w-16 h-1 shadow-4 mr-2"
                    />
                    <div className="f6 font-light">
                      {data.matchSummary.bestBowler.playerName}
                    </div>
                  </div>

                  {/* player stats */}
                  <div className="flex justify-between items-center py-3 w-44">
                    <div className="">
                      {data.matchSummary.bestBowler.battingStatsList && (
                        <div className="flex justify-start items-center py-1">
                          <span className="f6 font-medium">
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .runs
                            }
                            {data.matchSummary.bestBowler.battingStatsList[0]
                              .isNotOut === true
                              ? '*'
                              : ''}
                          </span>
                          <span className="text-xs ">
                            &nbsp;(
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .balls
                            }
                            )
                          </span>
                        </div>
                      )}
                      {data.matchSummary.bestBowler.bowlingStatsList && (
                        <div className="flex justify-start items-center py-1">
                          <span className="f6 font-medium">
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .wickets
                            }
                            /
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .runsConceded
                            }
                          </span>
                          {/* <span>&nbsp;</span> */}
                          <span className="text-xs ">
                            &nbsp;(
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                    <div className="">
                      {data.matchSummary.bestBowler.battingStatsList[1] && (
                        <div className="p-1 text-xs">&</div>
                      )}
                      {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                        <div className="p-1 text-xs">&</div>
                      )}
                    </div>
                    <div className="">
                      {data.matchSummary.bestBowler.battingStatsList && (
                        <div className="flex justify-start items-center py-1">
                          {data.matchSummary.bestBowler.battingStatsList[1] && (
                            <span className="f6 font-medium">
                              {
                                data.matchSummary.bestBowler.battingStatsList[1]
                                  .runs
                              }
                              {data.matchSummary.bestBowler.battingStatsList[1]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                          )}
                          {data.matchSummary.bestBowler.battingStatsList[1] && (
                            <span className="text-xs ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.battingStatsList[1]
                                  .balls
                              }
                              )
                            </span>
                          )}
                        </div>
                      )}
                      {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                        <div className="flex justify-start items-center py-1">
                          <span className="f6 font-medium">
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[1]
                                .wickets
                            }
                            /
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[1]
                                .runsConceded
                            }
                          </span>
                          <span className="text-xs ">
                            &nbsp;(
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[1]
                                .overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}
            </div>

            {/* -------------------------------------------------------desktop only----------------------------------------------------------------------- */}
            <div className="hidden md:block lg:block xl:block">
              <div className="bg-gray-4 flex items-starts">
                <div className="w-52 relative ">
                  {/* ---------------top batman---------------------------- */}
                  <div className="flex justify-start items-center  h-4 ">
                    <img
                      className="h-full z-0 px-3 bw0"
                      alt=""
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBatsman.playerID}_headshot_safari.png`}
                      onError={(e) => {
                        e.target.src = playerAvatar
                      }}
                    />

                    <div className="">
                      <div className="text-white flex items-center p-2">
                        <img
                          alt=""
                          src={`https://images.cricket.com/teams/${data.matchSummary.bestBatsman.playerTeamID}_flag_safari.png`}
                          onError={(e) => {
                            e.target.src = flagPlaceHolder
                          }}
                          className="ib w-16 h-1 shadow-4 mr-2"
                        />
                        <span className="f5 font-medium">
                          {' '}
                          {data.matchSummary.bestBatsman.playerName}
                        </span>
                      </div>

                      {data.matchSummary.bestBatsman.battingStatsList &&
                      props.matchType !== 'Test' ? (
                        // -----------------------------------ODI && T20----------------------------------------------

                        <div className="border-2 text-white mt-2 mr-3">
                          <div className="flex justify-between items-center">
                            <div className="flex justify-center items-center py-2 px-3 bg-white-10 rounded-full">
                              <span className="f5 font-medium mr-1">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="f6 font-medium">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>

                            <div className="flex justify-end p-1">
                              <span className="f5 p-2 font-medium">
                                6s{' '}
                                <span className="font-semibold">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].sixes
                                  }
                                </span>{' '}
                              </span>
                              <span className="f5 p-2 font-medium">
                                4s{' '}
                                <span className="font-semibold">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].fours
                                  }
                                </span>{' '}
                              </span>
                            </div>
                          </div>

                          {data.matchSummary.bestBatsman.bowlingStatsList && (
                            <div className="flex justify-between items-center">
                              <div className="flex justify-center items-center py-2 px-3 bg-white-10 rounded-full">
                                <span className="f5 font-medium mr-1 nowrap">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-xs">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              <div className="flex px-3 py-2 justify-center items-center bg-white-10 rounded-full">
                                <span className="f6 near-white w-full border-2 text-center ">
                                  Eco:{' '}
                                  <span className="font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].economyRate
                                    }
                                  </span>{' '}
                                </span>
                              </div>
                            </div>
                          )}
                        </div>
                      ) : (
                        // ***************************************************  Test Match Top Batman *****************************************************
                        <div className="border-2 text-white mt-2 mr-3">
                          <div className="flex justify-between items-center">
                            <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-3">
                              <span className="f5 font-medium mr-1">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="f6 font-medium">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <div className="px-2">&</div>
                            )}
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-3">
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[1] && (
                                  <span className="f5 font-medium">
                                    {
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[1].runs
                                    }
                                  </span>
                                )}
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[1] && (
                                  <span className="f6 font-medium">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[1].balls
                                    }
                                    )
                                  </span>
                                )}
                              </div>
                            )}
                          </div>

                          {data.matchSummary.bestBatsman.bowlingStatsList && (
                            <div className="flex justify-between items-center mt-2">
                              <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-3">
                                <span className="f5 font-medium">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="f6 font-medium">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="px-2">&</div>
                              )}
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-3">
                                  <span className="f5 font-medium">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].runsConceded
                                    }
                                  </span>
                                  <span className="f6 font-medium">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].overs
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="h-[1px] bg-black/80" />

                  {/* *---------------------------- TOP Bowler desktop -----------------------------* */}
                  <div>
                    {data.matchSummary.bestBowler.bowlingStatsList &&
                    props.matchType !== 'Test' ? (
                      /* ************************************* TOP Bowler ODI &&  T20************************************************************* */

                      <div className="flex justify-start text-white items-center h-4">
                        {/* player Image */}
                        <img
                          className="h-full z-0 px-3 bw0"
                          alt=""
                          src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                          onError={(e) => {
                            e.target.src = playerAvatar
                          }}
                        />

                        <div className="pl-4">
                          {/* player NAME */}
                          <div className="flex items-center text-white py-2">
                            <img
                              alt=""
                              src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                              onError={(e) => {
                                e.target.src = flagPlaceHolder
                              }}
                              className="ib w-16 h-1 shadow-4 mr-2"
                            />
                            <div className="f5 font-medium">
                              {data.matchSummary.bestBowler.playerName}
                            </div>
                          </div>

                          {/* player stats */}
                          <div className="">
                            {data.matchSummary.bestBowler.battingStatsList &&
                              (data.matchSummary.bestBowler.battingStatsList[0]
                                .runs !== 0 ||
                                data.matchSummary.bestBowler.battingStatsList[0]
                                  .balls !== 0) && (
                                <div className="flex justify-center items-center">
                                  <div className="py-2 px-3 rounded-full bg-white">
                                    <span className="f5 font-medium mr-1">
                                      {
                                        data.matchSummary.bestBowler
                                          .battingStatsList[0].runs
                                      }
                                      {data.matchSummary.bestBowler
                                        .battingStatsList[0].isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                    <span className="f6 font-medium  ">
                                      (
                                      {
                                        data.matchSummary.bestBowler
                                          .battingStatsList[0].balls
                                      }
                                      )
                                    </span>
                                  </div>
                                </div>
                              )}
                            {data.matchSummary.bestBowler.bowlingStatsList && (
                              <div className="flex justify-center items-center pt-2">
                                <div className=" bg-white rounded-full py-2 px-3 ">
                                  <span className="f5 font-medium mr-1">
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="f6 font-medium  ">
                                    (
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    ) : (
                      /* ************************************* TOP Bowler Test ************************************************************* */

                      <div className="flex justify-start text-white items-center mt-2 h-4">
                        {/* player Image */}

                        <img
                          className="h-full px-3 bw0 "
                          alt=""
                          src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                          onError={(e) => {
                            e.target.src = playerAvatar
                          }}
                        />

                        {/* player NAME */}
                        <div className="pl-4">
                          <div className="flex justify-start items-center py-2">
                            <img
                              alt=""
                              src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                              onError={(e) => {
                                e.target.src = flagPlaceHolder
                              }}
                              className="w-16 h-1 shadow-4 mr-2"
                            />
                            <div className="f5 font-medium">
                              {data.matchSummary.bestBowler.playerName}
                            </div>
                          </div>

                          {/* player stats */}
                          <div className="mb-2">
                            <div className="flex items-center">
                              {data.matchSummary.bestBowler
                                .battingStatsList && (
                                <div className="flex justify-center items-center bg-white-10 rounded-full py-2 px-3">
                                  <span className="f5 font-medium">
                                    {
                                      data.matchSummary.bestBowler
                                        .battingStatsList[0].runs
                                    }
                                    {data.matchSummary.bestBowler
                                      .battingStatsList[0].isNotOut === true
                                      ? '*'
                                      : ''}
                                  </span>
                                  <span className="f6 font-medium ">
                                    &nbsp;(
                                    {
                                      data.matchSummary.bestBowler
                                        .battingStatsList[0].balls
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                              {data.matchSummary.bestBowler
                                .battingStatsList[1] && (
                                <div className="ph-1 f5 font-medium ">&</div>
                              )}
                              {data.matchSummary.bestBowler.battingStatsList &&
                                data.matchSummary.bestBowler
                                  .battingStatsList[1] && (
                                  <div className="flex justify-center items-center bg-white-10 rounded-full m-1 py-2 px-3">
                                    {
                                      <span className="f5 font-medium">
                                        {
                                          data.matchSummary.bestBowler
                                            .battingStatsList[1].runs
                                        }
                                        {data.matchSummary.bestBowler
                                          .battingStatsList[1].isNotOut === true
                                          ? '*'
                                          : ''}
                                      </span>
                                    }
                                    {
                                      <span className="f6 font-medium">
                                        &nbsp;(
                                        {
                                          data.matchSummary.bestBowler
                                            .battingStatsList[1].balls
                                        }
                                        )
                                      </span>
                                    }
                                  </div>
                                )}
                            </div>

                            <div className="flex items-center mt-2">
                              {data.matchSummary.bestBowler
                                .bowlingStatsList && (
                                <div className="flex justify-center items-center bg-white-10 rounded-full py-2 px-3">
                                  <span className="f5 font-medium">
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="f6 font-medium  ">
                                    &nbsp;(
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                              {data.matchSummary.bestBowler
                                .bowlingStatsList[1] && (
                                <div className="ph-1 f5 font-medium ">&</div>
                              )}
                              {data.matchSummary.bestBowler
                                .bowlingStatsList[1] && (
                                <div>
                                  <div className="flex justify-start items-center py-1 bg-white-10 rounded-full m-1 px-3">
                                    <span className="f5 font-medium">
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[1].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[1].runsConceded
                                      }
                                    </span>
                                    <span className="f6 font-medium">
                                      &nbsp;(
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[1].overs
                                      }
                                      )
                                    </span>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                {/* ************************************************** Team score details ****************************************************************** */}

                <div className="w-52 text-white">
                  <div className=" ">
                    {data.matchSummary.inningOrder
                      .slice(
                        0,
                        props.matchType !== 'Test'
                          ? 2
                          : data.matchSummary.inningOrder.length,
                      )
                      .map((inning, i) => (
                        <div key={i} className="">
                          {/* Team Details */}

                          <div className="flex justify-between items-center p-2 bg-gray">
                            <div className="flex  items-center">
                              <img
                                alt=""
                                src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                                onError={(e) => {
                                  e.target.src = flagPlaceHolder
                                }}
                                className="ib w-16 h-1 shadow-4 mr-3"
                              />
                              <div className="f6 font-semibold">
                                {data.matchSummary[
                                  inning
                                ].teamName.toUpperCase()}
                              </div>
                            </div>
                            <div>
                              <span className="f6 font-bold darkRed">
                                {data.matchSummary[inning].runs[i <= 1 ? 0 : 1]}
                                /
                                {
                                  data.matchSummary[inning].wickets[
                                    i <= 1 ? 0 : 1
                                  ]
                                }
                              </span>
                              <span className="text-xs font-semibold pl-2">
                                {' '}
                                (
                                {
                                  data.matchSummary[inning].overs[
                                    i <= 1 ? 0 : 1
                                  ]
                                }
                                )
                              </span>
                            </div>
                          </div>

                          <div className="flex justify-between">
                            {data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].topBatsman &&
                              data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].topBatsman.battingStatsList && (
                                <div className="flex w-full justify-between p-2 py-2">
                                  <span className="f6 font-medium truncate w-80 ">
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.playerName
                                    }
                                  </span>
                                  <span className="f6 font-semibold">
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.battingStatsList[0].runs
                                    }
                                    {data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.battingStatsList[0]
                                      .isNotOut === true
                                      ? '*'
                                      : ''}
                                  </span>
                                </div>
                              )}
                            <div className="br b--moon-gray mt-1" />
                            {data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .topBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .topBowler.bowlingStatsList && (
                                <div className="flex w-full justify-between p-2 py-2">
                                  <span className="f6 font-medium truncate w-80">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.playerName
                                    }
                                  </span>
                                  <span className="f6 font-semibold ">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.bowlingStatsList[0]
                                        .runsConceded
                                    }
                                  </span>
                                </div>
                              )}
                          </div>

                          {props.matchType !== 'Test' && (
                            <div className="flex  justify-between ">
                              {data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].runnerBatsman &&
                                data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].runnerBatsman.battingStatsList && (
                                  <div className="flex w-full justify-between p-2 py-3">
                                    <span className="f6 font-medium  truncate w-80">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.playerName
                                      }
                                    </span>
                                    <span className="f6 font-semibold">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.battingStatsList[0].runs
                                      }
                                      {data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].runnerBatsman.battingStatsList[0]
                                        .isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                  </div>
                                )}

                              <div className="br b--moon-gray mb-2" />

                              {data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler &&
                                data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .runnerBowler.bowlingStatsList && (
                                  <div className="flex w-full justify-between p-2 py-3">
                                    <span className="f6 font-medium truncate w-80">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.playerName
                                      }
                                    </span>
                                    <span className="f6 font-semibold ">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.bowlingStatsList[0]
                                          .wickets
                                      }
                                      /
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.bowlingStatsList[0]
                                          .runsConceded
                                      }
                                    </span>
                                  </div>
                                )}
                            </div>
                          )}
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <></>
        )}
      </>
    )
}
