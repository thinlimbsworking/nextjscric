import React, { useEffect, useState } from 'react'
import parse from 'html-react-parser'
import CleverTap from 'clevertap-react'
import { UPDATE_ARTICLE_LIKE, GET_ARTICLE_LIKES } from '../api/queries'
import Head from 'next/head'
import { format } from 'date-fns'
import { useQuery, useMutation } from '@apollo/react-hooks'
import RelatedArticle from './relatedArticle'
import Router from 'next/router'
import MetaDescriptor from './MetaDescriptor'
import { useRouter } from 'next/navigation'
import { ArticleJsonLd } from 'next-seo'
// import LiveBlogging from './LiveBlogging';
import { getAUthorUrl } from '../api/services'

import { Link } from 'next/link'
import Heading from './commom/heading'
const backIconWhite = '/svgs/backIconWhite.svg'
const arrowLeft = '/pngs/arrow-left.png'

let scrollTop = null

const ArticleDetails = ({ params, ...props }) => {
  const [fontSize, setFontSize] = useState('f5')
  const [temp, setTemp] = useState(10)

  const fontHeading = {
    f55: '1.4rem',
    f5: '1.3rem',
    f6: '1.1rem',
  }

  const fontSubHeading = {
    f55: '1.3rem',
    f5: '1.2rem',
    f6: '1rem',
  }

  const router = useRouter()
  const article = props?.data?.getArticlesDetails

  const realtedArticles =
    props?.data?.getArticlesDetails &&
    props?.data?.getArticlesDetails?.relatedArticles

  const [showHeader, setShowHeader] = useState(false)
  const [apiCall, setApicall] = useState(true)
  const [path, setPath] = useState('')
  const [loadFirstTime, setloadFirstTime] = useState(false)
  const [articleCount, setArticleCount] = useState('')
  const [flagCount, setFlagCount] = useState(true)
  const [flagCount2, setFlagCount2] = useState(false)

  const [FlagArray, setFlagArray] = useState([])
  const [rArticleEvent, setArticleEvent] = useState(true)

  const [showFont, setShowFont] = useState(false)
  const [screenWidth, setscreenWidth] = useState()

  const { loading, error, data } = useQuery(GET_ARTICLE_LIKES, {
    variables: { articleID: props?.data?.getArticlesDetails?.articleID },

    onCompleted: (data) => {
      setArticleCount(data?.getArticlesLikes?.articlesCount)
    },
  })

  // ;

  const goToAUthor = (item) => {
    const name = item.name
    const id = item.id
    let authorName = `${name && name.split(' ').join('-').toLowerCase()}`
    let authorID = item.id
    router.push(
      // getAUthorUrl(authorName, authorID).href,
      getAUthorUrl(authorName, authorID).as,
    )
  }

  //For Infogram rendering
  useEffect(() => {
    if (
      typeof window !== 'undefined' &&
      window &&
      window.screen &&
      window.screen.width
    ) {
      setscreenWidth(window.screen.width > 975 ? 976 : window.screen.width)
    }
  }, [])

  useEffect(() => {
    const script = document.createElement('script')
    script.src = !(function (e, i, n, s) {
      var t = 'InfogramEmbeds',
        d = e.getElementsByTagName('script')[0]
      if (window[t] && window[t].initialized)
        window[t].process && window[t].process()
      else if (!e.getElementById(n)) {
        var o = e.createElement('script')
        ;(o.async = 1),
          (o.id = n),
          (o.src = 'https://e.infogram.com/js/dist/embed-loader-min.js'),
          d.parentNode.insertBefore(o, d)
      }
    })(document, 0, 'infogram-async')
    script.async = true
  }, [article?.content && article?.content?.length, data])

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  useEffect(() => {
    const s = document.createElement('script')
    s.setAttribute('src', 'https://platform.twitter.com/widgets.js')
    s.setAttribute('async', 'true')
    document.head.appendChild(s)
  }, [])

  const handleScroll = (event) => {
    ;({ scrollTop } = event.target.scrollingElement)
    if (scrollTop > 208 && !showHeader) {
      setShowHeader(true)
    }
    if (scrollTop < 159) {
      setShowHeader(false)
    }
  }

  const debounce = (fn, delay) => {
    let timeoutID
    return function (...args) {
      if (timeoutID) {
        clearTimeout(timeoutID)
      }
      timeoutID = setTimeout(() => {
        fn(...args)
      }, delay)
    }
  }

  const handleTagUrl = (item) => {
    if (item.type == 'series' || item.type == 'matches') {
      item.leagueType !== null &&
        router.push(
          `/series/${item.leagueType.toLowerCase()}/${item.id}/${item.name
            .replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()}/matches`,
        )
    }

    if (item.type == 'players') {
      router.push(
        `/players/${item.id}/${item.name
          .replace(/[^a-zA-Z0-9]+/g, ' ')
          .split(' ')
          .join('-')
          .toLowerCase()}/career-stats`,
      )
    }
    if (item.type == 'teams') {
      router.push(
        `/teams/${item.id}/${item.name
          .replace(/[^a-zA-Z0-9]+/g, ' ')
          .split(' ')
          .join('-')
          .toLowerCase()}/form`,
      )
    }
  }

  useEffect(() => {
    setFlagArray(
      localStorage.getItem('FlagArray') == null
        ? []
        : JSON.parse(localStorage.getItem('FlagArray')),
    )
    setPath(params?.slug)

    setFlagCount2(!flagCount2)
    localStorage.getItem('FlagArray') !== null
      ? JSON.parse(localStorage.getItem('FlagArray')).indexOf(path) >= 0
        ? setFlagCount(false)
        : setFlagCount(true)
      : false
  }, [articleCount])

  useEffect(() => {
    document.documentElement.scrollHeight < 900 ? setScrolledValue(100) : null
    setTemp(document.documentElement.scrollHeight)

    setloadFirstTime(true)
  }, [])

  const [UPDATE_ARTICLE_LIKE2, response2] = useMutation(UPDATE_ARTICLE_LIKE, {
    onCompleted: (data) => {
      let arr = FlagArray
      const index = FlagArray.indexOf(path)
      if (index > -1) {
        arr.splice(index, 1)
      } else {
        arr.push(path)
      }
      setloadFirstTime(false)

      setFlagArray(arr)

      localStorage.setItem('FlagArray', JSON.stringify(FlagArray))
      setArticleCount(data.updateLikes)
      setFlagCount(!flagCount)

      setApicall(true)
    },
  })
  const likeHandle = () => {
    if (typeof window !== 'undefined') {
      setApicall(false)
      const flag = FlagArray.indexOf(path) >= 0 ? false : true

      UPDATE_ARTICLE_LIKE2({
        variables: {
          articleID: path,
          like: loadFirstTime ? flag : flagCount,
        },
      })
      CleverTap.initialize('Like', {
        Source: 'Article',
        ArticleHeading: article.title || 'TYPE',
        Platform: localStorage ? localStorage.Platform : '',
      })
    }
  }
  const handleShare = () => {
    if (window.navigator.share !== undefined) {
      window.navigator
        .share({
          title: article.title || 'TITLE',
          url: window.location.href,
        })
        .then((res) => {
          CleverTap.initialize('Share', {
            Source: 'Article',
            ArticleType: article.type || 'TYPE',
            Platform: localStorage ? localStorage.Platform : '',
          })
        })
    }
  }

  const [scrolledValue, setScrolledValue] = useState(10)

  const progressContainerStyle = {
    border: '2px solid',
    borderColor: '#f8bbd0',
    boxShadow: '0 2px 4px rgba(0, 0, 0, 0.3)',
    height: '100px',
    position: 'sticky',
    width: '100px',
    borderRadius: '100%',

    top: 0,
    left: 0,

    zIndex: 99,
  }

  const progressBarStyle = {
    height: '100px',
    borderRadius: '100%',
    border: '2px solid',
    borderColor: '#e91e63',
    width: scrolledValue,
  }

  const scrollProgress = () => {
    const scrollPx = document.documentElement.scrollTop
    const winHeightPx =
      document.documentElement.scrollHeight -
      (document.documentElement.clientHeight + 50)
    const scrolled = `${(scrollPx / winHeightPx) * 100}`

    if (scrolled > 10) {
      setScrolledValue(scrolled)
    }
  }

  useEffect(() => {
    // ;

    window.addEventListener('scroll', scrollProgress)
    // setScrolledValue(document.documentElement.scrollHeight>document.documentElement.clientHeight?10:100)
    return () => {
      window.removeEventListener('scroll', scrollProgress)
    }
  }, [])

  return (
    <>
      <>
        <title>{`${article?.title} | ${article?.type}`}</title>
        {/* <h1 className='dn'>{article.title ? article.title : 'Cricket News'}</h1> */}
        <meta name="description" content={article?.description} />
        <meta name="keywords" content={article?.seoTags} />
        <meta itemprop="width" content="595" />
        <meta itemprop="height" content="450" />
        <meta itemprop="url" content={article?.thumbnail} />
        <meta property="og:image" content={article?.thumbnail} />
        <meta property="og:width" content="595" />
        <meta property="og:height" content="450" />
        <meta property="og:image:width" content="595" />
        <meta property="og:image:height" content="450" />
        <meta property="og:image:secure_url" content={article?.thumbnail} />
        <meta
          property="og:title"
          content={article?.title || 'cricket.com article'}
        />
        <meta
          property="og:url"
          content={`https://www.cricket.com/news/${article?.articleID}`}
        />
        <meta property="og:type" content="Article" />
        <meta property="fb:app_id" content="631 889693979290" />
        <meta property="og:site_name" content="cricket.com" />
        <link
          rel="canonical"
          href={`https://www.cricket.com/news/${article?.articleID}`}
        />
        <link
          rel="apple-touch-icon"
          href="http://mysite.com/img/apple-touch-icon-57x57.png"
        />
        <script async src="https://platform.twitter.com/widgets.js"></script>
        <script
          async
          src="https://platform.instagram.com/en_US/embeds.js"
        ></script>
      </>
      <ArticleJsonLd
        url={`https://www.cricket.com/news/${article?.articleID}`}
        title={
          article?.title ? article.title.replace(/\"/g, '') : 'Cricket News'
        }
        images={[article?.bg_image_safari]}
        datePublished={
          article?.createdAt
            ? new Date(parseInt(article?.createdAt)).toISOString()
            : ''
        }
        dateModified={
          article?.createdAt
            ? new Date(parseInt(article?.createdAt)).toISOString()
            : ''
        }
        authorName={[(article?.author && article?.author) || '']}
        publisherName="Cricket.com"
        publisherLogo="https://images.cricket.com/icons/mainlogoico.ico"
        description={
          article?.description ? article?.description?.replace(/\"/g, '') : ''
        }
      />
      <div className="md:pb-28 xl:pb-28 lg:pb-28">
        <div className="absolute left-2 top-3 md:hidden">
          {/* <Link {}> */}
          {/* <img src={backIconWhite} alt='back icon' onClick={() => Router.back()} /> */}
          <div
            onClick={() => window.history.back()}
            className=" outline-0 md:cursor-pointer mr-2 lg:hidden  bg-gray rounded"
          >
            <svg width="30" focusable="false" viewBox="0 0 24 24">
              <path
                fill="#ffff"
                d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
              ></path>
              <path fill="none" d="M0 0h24v24H0z"></path>
            </svg>
          </div>
          {/* </Link> */}
        </div>
        {showHeader && (
          <div
            className=" fixed top-0 left-0 right-0  px-2 flex items-center flex-auto justify-between z-10 bg-basebg text-white md:hidden"
            style={{ height: 46 }}
          >
            <div className="flex items-center ">
              {/* <Link {...getArticleTabUrl()}> */}
              <img
                src={backIconWhite}
                alt="back icon"
                onClick={() => window?.history?.back()}
              />
              {/* </Link> */}
              <div className="text-sm font-medium px-2  ">
                {(article && article.title) || ''}
              </div>
            </div>
          </div>
        )}

        <div className="">
          <div className="rounded-md bg-white md:h-[500px]">
            <img
              className="hidden lg:block md:block h-[500px] p-4 rounded-md mt-2 w-full object-cover object-top"
              src={`${article?.bg_image_safari}?auto=compress&dpr=2&fit=clip&w=976&h=500`}
              alt="safari"
            />
            <img
              className="md:hidden lg:hidden h-56 w-full lg:mt-2 md:mt-2 xl:mt-2 object-cover object-top"
              src={`${article?.bg_image_safari}?auto=compress&dpr=1&fit=clip&w=${screenWidth}&h=224`}
              alt="safari"
            />
            {/* <h1
              className=" -mt-14 dark:ml-2 ml-4 text-lg font-semibold helvetica tracking-wide md:pl-1 text-white"
              style={{ fontSize: fontHeading[fontSize] }}
            >
              {(article?.title && article.title) || ''}
            </h1> */}
          </div>
          {/* ${data.getArticlesDetails.bg_image_safari}?auto=compress&dpr=2&w=${screenWidth}&h=230&fit=clip */}
          {
            <div className="md:flex flex-wrap md:px-0 lg:px-0 lg:mt-6 md:mt-6 xl:mt-6 dark:text-white text-black dark:bg-gray-1 dark:border-gray-1 bg-white dark:border-2 border-solid">
              <div className=" md:w-3/5 first-line:mb-4 relative">
                {/* {article && article.matchName && (
                <div>
                  <div className='pa3 f7 fw6 dn-ns '>{article.matchName}</div>
                </div>
              )}
              <div className='divider dn-ns' /> */}

                {
                  <div className="px-2 py-3 lg:border-r lg:border-r-solid lg:border-r-gray-11 md:border-r md:border-r-solid md:border-r-gray-11">
                    <div className="flex flex-wrap md:flex-row-reverse justify-between items-center text-sm font-medium md:mt-3 py-2 text-gray-2">
                      <div className="flex mt-1">
                        {/* <span className="text-gray-2 text-xs font-medium ">
                          By:
                        </span> */}
                        <img
                          src="/pngsV2/authoriconnew.png"
                          alt=""
                          className="hidden md:block"
                          height={'20px'}
                          width={'20px'}
                        />
                        <img
                          src="/pngsV2/user.png"
                          alt=""
                          className="md:hidden"
                          height={'20px'}
                          width={'20px'}
                        />
                        {article?.newAuthors &&
                          article?.newAuthors?.map((item, index) => {
                            return (
                              <div
                                onClick={() => {
                                  item.id && item.name && goToAUthor(item)
                                }}
                              >
                                <div
                                  className={`md:cursor-pointer ${
                                    index != 0 ? 'mt-1' : ''
                                  }  flex justify-start text-xs text-gray-2 `}
                                >
                                  {/* {index == 0 && <img alt='author' src={'/svgs/author-icon.svg'} />} */}

                                  <span
                                    className={`${
                                      index == 0
                                        ? 'ml-1 helvetica'
                                        : 'ml-4 helvetica'
                                    } `}
                                  >
                                    {item.name}{' '}
                                  </span>
                                </div>
                              </div>
                            )
                          })}
                      </div>
                      <div className="helvetica flex justify-start items-start text-xs">
                        {(article &&
                          article?.createdAt &&
                          format(
                            +article?.createdAt,
                            'dd MMM yyyy | hh:mm a',
                          )) ||
                          ''}
                      </div>
                    </div>
                    <div className="bg-[#DDE0E7] lg:hidden md:hidden h-[1px]"></div>
                    {/* mt-1 helvetica font-semibold text-sm text-gray-2 leading-tight italic tracking-wide */}
                    <h1
                      className="dark:ml-2 text-lg font-semibold helvetica tracking-wide md:pl-2 dark:text-white text-black"
                      style={{ fontSize: fontHeading[fontSize] }}
                    >
                      {(article?.title && article.title) || ''}
                    </h1>
                    <h2 className="ml-2.5 dark:border-l-green-6 border-l-basered border-l-2 border-l-solid dark:border-r-gray-1 dark:border-y-gray-1 my-4 pt-1 helvetica font-bold dark:text-white leading-5">
                      <div
                        className="noto italic pl-2 dark:font-bold font-semibold text-gray-2"
                        style={{ fontSize: fontSubHeading[fontSize] }}
                      >
                        {(article?.description && article?.description) || ''}
                      </div>
                    </h2>

                    <div
                      className={`articleContent ${
                        fontSize === 'f6'
                          ? 'text-xs'
                          : fontSize === 'f5'
                          ? 'text-sm'
                          : fontSize === 'f55'
                          ? 'text-lg'
                          : ''
                      } leading-6 serif-noto p-2 [&>p]:pt-4`}
                    >
                      {article && article?.content && parse(article?.content)}
                    </div>

                    <div>
                      <div className="pl-4 md:pt-4 hidden md:block">
                        <div className="flex flex-wrap">
                          <div className="flex md:cursor-pointer p-1 px-4 flex-wrap justify-center items-center font-bold text-2xl w-1/5">
                            <img
                              alt="heart"
                              className="mr-2"
                              onClick={debounce(likeHandle, 1000)}
                              // onClick={() => (apiCall ? likeHandle() : null)}
                              src={
                                FlagArray.indexOf(path) >= 0
                                  ? '/svgs/redHeart.svg'
                                  : '/svgsV2/heartWhite.svg'
                              }
                              style={{ height: 45, width: 40 }}
                            />
                            {articleCount !== null &&
                            FlagArray.indexOf(path) >= 0
                              ? articleCount
                              : ''}
                          </div>

                          {/* <div
                          onClick={handleShare}
                          className=' flex cursor-pointer  p-1 px-3  flex-wrap justify-center items-center font-semibold  w-1/5 shadow-5 '>
                          <img className=' cursor-pointer' src={'/svgs/shareGray.svg'} alt='shareGray' /> Share
                        </div> */}
                        </div>
                      </div>

                      <div className="tex-sm font-semibold py-2">Tags</div>
                      <div className="flex flex-wrap mb-6">
                        {article &&
                          article?.tags &&
                          article?.tags.map(
                            (tag, key) =>
                              tag.name && (
                                <span
                                  key={key}
                                  onClick={() => handleTagUrl(tag)}
                                  className="border-2 dark:bg-gray-4 bg-basered dark:border-green-6 border-[#E2E2E2] px-2 py-1 text-white rounded text-sm font-semibold mr-2 mb-2 md:cursor-pointer"
                                >
                                  {tag.name}
                                </span>
                              ),
                          )}
                      </div>
                    </div>
                  </div>
                }
              </div>
              {/* <span className="border-l-2 border-solid border-red"></span> */}
              <div className="md:w-2/5 lg:w-2/5 relative dark:bg-basebg">
                {/* <div className='divider dn-ns' /> */}

                <div className="p-2 dark:-mt-4 mt-5">
                  <div className="text-left text-white">
                    {/* <h3 className="text-lg text-black font-semibold">
                      Related Articles
                    </h3> */}
                    <h3 className="ml-2 my-2">
                      <Heading heading={'Related Articles'} />
                    </h3>
                    {/* <div className="bg-gray-2 h-[0.2px] lg:hidden md:hidden" /> */}
                    <div className="flex flex-wrap hidescroll h-full overflow-scroll">
                      {article &&
                      realtedArticles &&
                      realtedArticles.length !== 0 ? (
                        realtedArticles.map((article, key) => (
                          <div className="w-45 md:cursor-pointer" key={key}>
                            <RelatedArticle
                              article={article}
                              rArticleEvent={rArticleEvent}
                              setArticleEvent={setArticleEvent}
                            />
                            <div className="dark:bg-gray-4 bg-[#DDE0E7] h-[1px]" />
                          </div>
                        ))
                      ) : (
                        <div className="pt-4  ">
                          <div className="text-gray-2 text-center font-semibold text-md pb-4">
                            No Articles found
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
      </div>

      {showFont && (
        <div
          className="w-full z-20 items-center justify-center fixed  left-0 right-0 flex  "
          style={{ bottom: '2.9rem' }}
        >
          <div className="w-1/4 flex items-center justify-center"></div>

          <div className="w-1/4 flex items-center justify-center "></div>
          <div className="w-1/4 flex-col block  justify-around items-center  shadow-triangleMain  ">
            <div
              className={` flex items-center justify-center border-b p-1 border-gray-2 font-semibold  bg-gray-1 shadow-triangle1 ${
                fontSize == 'f55' ? 'text-green-6' : 'text-gray-2'
              }  `}
              style={{ fontSize: fontHeading.f55 }}
              onClick={() => (setFontSize('f55'), setShowFont(false))}
            >
              Aa
            </div>
            <div
              className={` flex items-center justify-center  p-1 border-b   border-gray-2 font-medium  bg-gray-1  shadow-triangle2 ${
                fontSize == 'f5' ? 'text-green-6' : 'text-gray-2'
              }  `}
              style={{ fontSize: fontHeading.f5 }}
              onClick={() => (setFontSize('f5'), setShowFont(false))}
            >
              Aa
            </div>

            <div
              className={` flex items-center justify-center  p-1  font-normal  bg-gray-1  shadow-triangle ${
                fontSize == 'f6' ? 'text-green-6' : 'text-gray-2'
              } `}
              style={{ fontSize: fontHeading.f6 }}
              onClick={() => (setFontSize('f6'), setShowFont(false))}
            >
              Aa
            </div>

            <div className="flex items-center justify-center fw4 red_10 p1">
              <div className="flex items-center justify-center">
                <div className="flex items-center justify-center arrow-down1"></div>
              </div>
            </div>
          </div>
          <div className="w-1/4 flex items-center justify-center"></div>
        </div>
      )}

      <div className="w-full grid-rows-2 lg:hidden xl:hidden md:hidden  items-center justify-center bottom-0 left-0 right-0 flex">
        <div
          className="w-full py-2 flex justify-around items-center z-10 bg-gray-1 fixed bottom-0
       shadow-5  "
        >
          <div className="w-1/4 flex items-center justify-center">
            <img
              src={
                FlagArray.indexOf(path) >= 0
                  ? '/svgs/redHeart.svg'
                  : '/svgsV2/heartWhite.svg'
              }
              // src={ global.window && global.window.localStorage.getItem('FlagArray') !== null && global.window.localStorage.getItem('FlagArray') !== null && JSON.parse(global.window.localStorage.getItem('FlagArray')).indexOf(router.query.type)>0?`/svgs/redHeart.svg`:'/svgs/newsheart.svg'}
              onClick={debounce(likeHandle, 1000)}
              alt=""
            />
            <span className="ml-2 red text-sm font-semibold">
              {articleCount !== null && FlagArray.indexOf(path) >= 0
                ? articleCount
                : ''}
            </span>
          </div>
          <div
            className="w-1/4 flex items-center justify-center"
            onClick={handleShare}
          >
            <img src="/svgs/shareWhite.svg" alt="" />
          </div>

          <div className="w-1/4 flex items-center justify-center ">
            <img
              src="/svgsV2/font_news.svg"
              onClick={() => setShowFont(true)}
              alt=""
            />
          </div>

          <div className="w-1/4  flex items-center justify-center">
            <CircularProgressBar
              strokeWidth="4"
              sqSize="33"
              percentage={
                0 >= 100 - (parseInt(scrolledValue) / 100) * 100
                  ? 0
                  : 100 - (parseInt(scrolledValue) / 100) * 100
              }
              value={scrolledValue}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default ArticleDetails

const CircularProgressBar = (props) => {
  //  ;
  const sqSize = props.sqSize
  // SVG centers the stroke width on the radius, subtract out so circle fits in square
  const radius = (props.sqSize - props.strokeWidth) / 2
  // Enclose cicle in a circumscribing square
  const viewBox = `0 0 ${sqSize} ${sqSize}`
  // Arc length at 100% coverage is the circle circumference
  const dashArray = radius * Math.PI * 2
  // Scale 100% coverage overlay with the actual percent
  const dashOffset = dashArray - (dashArray * (100 - props.percentage)) / 100

  return (
    <svg width={props.sqSize} height={props.sqSize} viewBox={viewBox}>
      <circle
        style={{
          fill: '#2B323F',
          stroke: '#0000001F',
          strokeLinecap: 'round',
          strokeLinejoin: 'round',
        }}
        className="circle-background"
        cx={props.sqSize / 2}
        cy={props.sqSize / 2}
        r={radius}
        strokeWidth={`${props.strokeWidth}px`}
      />

      <circle
        className="circle-progress"
        cx={props.sqSize / 2}
        cy={props.sqSize / 2}
        r={radius}
        strokeWidth={`${props.strokeWidth}px`}
        // Start progress marker at 12 O'Clock
        transform={`rotate(-90 ${props.sqSize / 2} ${props.sqSize / 2})`}
        style={{
          strokeDasharray: dashArray,
          strokeDashoffset: dashOffset,
          fill: 'none',
          stroke: '#38D926',
          transition: 'stroke-dashoffset 0s linear 0s',
          strokeLinecap: 'round',
          strokeLinejoin: 'round',
        }}
      />
      {props.percentage < 3 && (
        <text
          style={{
            fontSize: 12,
            fontWeight: '900',
            fill: '#38D926',
            stroke: '#38D926',
          }}
          x="50%"
          y="50%"
          dy=".3em"
          textAnchor="middle"
        >
          ✓
        </text>
      )}
    </svg>
  )
}
