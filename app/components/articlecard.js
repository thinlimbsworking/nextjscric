import React from 'react'

import { trackScroll } from '../components/commom/lazyimageloader'
import LazyImageLoader from '../components/commom/lazyimageloader'
import { format } from 'date-fns'
import CardComponent from './news/socialTracker'
import ImageWithFallback from './commom/Image'
import NewsSocialTracker from './news/newsSocialTracker'
import Heading from './shared/heading'

{
  /* <div className="dn db-l"></div> */
}

const ArticleCard = ({ isFeatured = false, isDetails, article, ...props }) => {
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    let sec = Math.floor((distance % (1000 * 60)) / 1000)

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago'

    return result
  }

  return (
    <div className="w-full">
      <div className="hidden lg:block md:block">
        {isFeatured && (
          <div className="flex w-full">
            <div className="flex flex-col md:p-4 lg:bg-white md:mt-2 xl:bg-white md:bg-white md:rounded-md">
              <img
                className="object-cover object-top md:rounded-md "
                style={{ width: '100rem', height: '25rem' }}
                src={`${article?.bg_image_safari}?auto=compress&dpr=3&w=400&h=224`}
              />

              {/* <ImageWithFallback
              className="w-full h-96"
              width={200}
              height={600}
              src={`${article.bg_image_safari}?auto=compress&dpr=2&w=1&h=200`}
            /> */}
              <div className="w-full flex relative bg-top">
                <div className="px-1 absolute bottom-2 dark:bg-green-6 bg-basered text-white dark:text-black text-xs font-medium">
                  {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
                </div>
              </div>

              <div className="px-2 py-2 w-full">
                <h2 className="text-md font-bold tracking-wider text-black dark:text-white line-clamp">
                  {article && (article.title || '')}
                </h2>
                <div
                  className="text-sm font-normal font-mnr text-gray-9 mt-1 line-clamp-2 dark:text-white"
                  style={{ WebkitLineClamp: 3 }}
                >
                  {article && (article.description || '')}
                </div>
                <div className="mt-2 flex justify-between items-center">
                  <div className="flex items-center w-2/3">
                    <img
                      className={`${
                        props.authorPic ? ' h-1 w-1 br-100 ' : 'h-5 w-5'
                      }`}
                      src={`${
                        props.authorPic
                          ? props.authorPic
                          : '/pngsV2/UserProfile.png'
                      }`}
                      alt="user"
                    />
                    <div className="text-gray-2 text-xs italic pl-1 font-medium">
                      {article &&
                        article.author &&
                        (article.author.split(',').join(', ') || '')}
                    </div>
                  </div>
                  <div className="w-1/3 text-right text-sm font-medium text-gray-2">
                    {Date.now() - parseInt(article?.createdAt) > 8640000
                      ? article && format(+article?.createdAt, 'dd MMM yyyy')
                      : getUpdatedTime(article?.createdAt)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {!isFeatured && (
          <div className="flex md:bg-white md:rounded-md mb-4 p-4">
            <div className=" w-1/2 br-2 flex relative bg-top h-48">
              <img
                className=" object-cover object-top h-48 xl:rounded-md lg:rounded-md md:rounded-md"
                style={{ width: '100%' }}
                src={`${article?.bg_image_safari}?auto=compress&dpr=2&w=400&h=224`}
              />

              <div className="px-1 absolute bottom-2 dark:bg-green-6 dark:text-black text-white bg-basered text-xs font-medium">
                {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
              </div>
            </div>

            <div className="pl-4 py-2 w-1/2">
              <h2 className="text-md font-semibold tracking-wider dark:text-white text-black line-clamp">
                {article && (article.title || '')}
              </h2>
              <div
                className="text-sm font-normal mt-1 text-gray-9 leading-5 font-mnr dark:text-white line-clamp-2"
                style={{ WebkitLineClamp: 3 }}
              >
                {article && (article.description || '')}
              </div>
              <div className="mt-2 flex justify-between items-center">
                <div className="flex items-center w-2/3">
                  <img
                    className={`${
                      props.authorPic ? ' h-1 w-1 br-100 ' : 'h-5 w-5'
                    }`}
                    src={`${
                      props.authorPic
                        ? props.authorPic
                        : '/pngsV2/UserProfile.png'
                    }`}
                    alt="user"
                  />
                  <div className="text-gray-2 text-xs italic pl-1 font-medium">
                    {article &&
                      article.author &&
                      (article.author.split(',').join(', ') || '')}
                  </div>
                </div>
                <div className="w-1/3 text-right text-sm font-medium text-gray-2">
                  {Date.now() - parseInt(article?.createdAt) > 8640000
                    ? article && format(+article?.createdAt, 'dd MMM yyyy')
                    : getUpdatedTime(article?.createdAt)}
                </div>
              </div>
            </div>
          </div>
        )}
        {/* <div className="bg-black h-[1px]  "></div> */}
      </div>
      {/* <div className="flex items-end justify-end bg-red w-3/4">
        <CardComponent />
      </div> */}
      {/* ---------------------------------mobile------------------------------- */}
      <div className="md:hidden lg:hidden text-white">
        {isFeatured ? (
          <div className="pb-1">
            <div className="py-1 px-2">
              <div className="rounded relative w-full">
                <LazyImageLoader
                  className="object-cover object-top h-40 hidden"
                  style={{ width: '100%' }}
                  src={`${article?.bg_image_safari}?auto=compress&dpr=2&w=1&h=160`}
                />
                <img
                  className="object-cover object-top h-44"
                  style={{ width: '100%' }}
                  src={`${article?.bg_image_safari}?auto=compress&dpr=2&w=360&h=176`}
                />

                <div className="px-1 absolute bottom-2 dark:bg-green-6 dark:text-black text-white bg-basered text-xs font-medium">
                  {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
                </div>
              </div>

              <div className="mt-1">
                <div className="font-semibold text-md line-clamp leading-snug">
                  {article && (article.title || '')}
                </div>
                <div
                  className="font-normal mt-2 text-xs leading-tight"
                  style={{ WebkitLineClamp: 3 }}
                >
                  {article && (article.description || '')}
                </div>
                <div className="dark:flex dark:justify-between">
                  <div className="mt-2 flex items-center">
                    {/* <img src='/pngsV2/UserProfile.png' alt='user' className='w-5 h-5' /> */}
                    <span className="text-gray-2 text-xs font-medium">By:</span>
                    {/* <img
                      src="/pngsV2/authoriconnew.png"
                      alt=""
                      height={'20px'}
                      width={'20px'}
                    /> */}
                    <span className="text-gray-2 text-xs font-medium pl-1">
                      {article &&
                        article.author &&
                        (article.author.split(',').join(', ') || '')}
                    </span>
                  </div>
                  <div className="mt-2 text-gray-2 text-xs font-medium">
                    {/* {Date.now() - parseInt(article.createdAt) > 8640000
                      ? article && format(+article.createdAt, 'dd MMM yyyy')
                      : getUpdatedTime(article.createdAt)} */}
                    {(article &&
                      article.createdAt &&
                      format(+article?.createdAt, 'dd MMM yyyy | hh:mm a')) ||
                      ''}
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-black w-full h-[1px]"></div>
          </div>
        ) : !isDetails ? (
          <div className="pb-1">
            <div className="p-1 flex items-start m-1">
              <div className="relative w-40">
                {article.sm_image_safari ? (
                  <img
                    className=" object-cover object-top h-20 w-36 rounded"
                    src={`${article?.sm_image_safari}?auto=compress&dpr=2&w=240&h=144`}
                  />
                ) : (
                  <div className="bg-gray-2 h-36 w-36"></div>
                )}

                <div className="px-1 absolute bottom-2 dark:bg-green-6 text-white bg-basered dark:text-black text-xs font-medium">
                  {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
                </div>
              </div>
              <div className=" w-3/5 ">
                <div className="text-xs font-semibold mt-1 line-clamp ">
                  {article && (article.title || '')}
                </div>
                <div className="text-xs mt-1 hidden lg:block md:block xl:block ">
                  {article && (article.description || '')}
                </div>

                <div className="flex justify-center mt-6 lg:hidden md:hidden xl:hidden">
                  <div className="flex items-center justify-start mt-1 w-7/12">
                    {/* <div
                      className={`${
                        props.authorPic
                          ? 'flex items-center justify-center w-10 h-10  '
                          : 'flex items-center justify-center w-5 h-5'
                      }`}>
                      {' '}
                      <img
                        className={`${props.authorPic ? ' h-1 w-1 rounded-full ' : ''}`}
                        src={`${props.authorPic ? props.authorPic : '/pngsV2/UserProfile.png'}`}
                        alt='user'
                      />
                    </div> */}

                    <div className="flex w-full">
                      {' '}
                      <span className="text-gray-2 text-xs font-medium ">
                        By:
                      </span>
                      <span className="text-gray-2 text-xs pl-1 line-clamp-1">
                        {article &&
                          article.author &&
                          article.author.split(',') &&
                          article.author.split(',')[0]}
                        {article &&
                        article.author &&
                        article.author.split(',') &&
                        article.author.split(',').length > 1
                          ? ',...'
                          : ''}
                      </span>
                    </div>
                  </div>
                  <div className="flex items-center text-xs text-gray-2 justify-end mt-1 w-5/12">
                    <div className="flex w-full">
                      <span className="text-gray-white -mt-1 text-sm pr-1 ">
                        .
                      </span>
                      {}
                      <span className='line-clamp-1'>
                        {Date.now() - parseInt(article?.createdAt) > 8640000
                        ? article && format(+article?.createdAt, 'dd MMM yyyy')
                        : getUpdatedTime(article?.createdAt)}
                      </span>
                    </div>
                  </div>{' '}
                </div>
              </div>
            </div>
            <div className="bg-black h-[1px] "></div>
          </div>
        ) : null}
      </div>
    </div>
  )
}

export default trackScroll(ArticleCard)
