import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ImageWithFallback from './commom/Image';

import CleverTap from 'clevertap-react';
const tabIcon2 ='/pngsV2/schedule.png';
const tabIcon2Active = '/pngsV2/tabicon2.png';
const tabIcon1 = '/pngsV2/home.png';
const tabIcon1Active = '/pngsV2/tabicon1.png';
const tabIcon4 = '/pngsV2/cricUpdates.png';
const tabIcon4Active = '/pngsV2/tabicon4.png';
const tabIcon5 = 'pngsv2/more.png';
const tabIcon5Active = '/pngsV2/tabicon5.png';
const criclytics_active = '/pngsV2/criclytics_active.png';
const feedIcon = '/svgsV2/feeds.svg';

const criclytics_inactive = '/pngsV2/criclytics_inactive.png';

export default function BottomNavBar(props) {

  
  
  let router = useRouter();
  const [tab, setTab] = useState(resolvePath(router.pathname));
  const handleNavigation = (source) => {
    let event = '';
    switch (source) {
      case '/schedule':
        event = 'Schedule';
        setTab('schedule');
        break;
      case '/criclytics':
        event = 'CriclyticsHome';
        setTab('criclytics');

        break;
      case '/news/latest':
        event = 'NewsHome';
        setTab('news');
        break;
      case '/more':
        event = 'More';
        setTab('more');
        break;
      case '/':
        event = 'Home';
        setTab('home');
        break;
      default:
        event = '';
    }
    CleverTap.initialize(event=="news"?'NewsHome':event, {
      Source: 'BottomNavigation',
      Platform: localStorage ? localStorage.Platform : ''
    });
  };

  return (
    <nav className='lg:hidden fixed bottom-0 left-0 right-0 flex bg-gray-8 backdrop-blur-2xl py-3  text-xs shadow-md  z-50'>
     

        <div className='w-1/5 '>
          <Link
            href='/'
            as='/'
            passHref
            className='flex justify-center items-center  flex-col'
            onClick={() => handleNavigation('/')}>

            <ImageWithFallback height={18} width={18} loading='lazy'
              fallbackSrc = {tabIcon1}
              alt='home'
              // className='h-8 py-1'
              src={tab === 'home' ? tabIcon1Active : tabIcon1}
            />
            <span className={`text-xs  ${tab === 'home' ? 'green font-semibold' : 'text-gray-2'}`}>Home</span>

          </Link>
        </div>

        <div className='w-1/5 '>
          <Link
            as='/schedule/live-matches'
            href='/schedule/[...slugs]'
            passHref
            className='flex flex-col justify-center items-center'
            onClick={() => handleNavigation('/schedule')}>

            <ImageWithFallback height={18} width={18} loading='lazy'
              fallbackSrc = {tabIcon1}
              alt='/footer/tabicon2.png'
              className='h-8 py-1'
              src={tab === 'schedule' ? tabIcon2Active : tabIcon2}
            />
            <span className={`text-xs ${tab === 'schedule' ? 'green  font-semibold' : 'text-gray-2'}`}>Schedule</span>

          </Link>
        </div>
        <div className='w-1/5 pr-2 '>
          <Link
            href='/criclytics'
            as='/criclytics'
            passHref
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/criclytics')}>

            <ImageWithFallback height={18} width={18} loading='lazy'
              fallbackSrc = {tabIcon1}
              alt='/footer/tabicon4.png'
              className='h-8 py-1'
              src={tab === 'criclytics' ? criclytics_active : criclytics_inactive}
            />
            <div className={`flex ${tab === 'criclytics' ? 'green  font-semibold' : 'text-gray-2' } relative`}>

            <span className={`text-xs  ${tab === 'criclytics' ? 'green  font-semibold' : 'text-gray-2' }`}>Criclytics</span>
            <span className='text-[8px] font-semibold absolute right-[-13px] bottom-1'>TM</span>
            </div>

          </Link>
        </div>

        <div className='w-1/5 '>
          <Link
            href='/news/[type]'
            as='/news/latest'
            passHref
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/news/latest')}>

            <ImageWithFallback height={18} width={18} loading='lazy'
              fallbackSrc = {tabIcon1}
              alt='/footer/tabicon4.png'
              className='h-8 py-1'
              src={tab === 'news' ? tabIcon4Active : tabIcon4}
            />
            <span className={`text-xs ${tab === 'news' ? 'green  font-semibold' : 'text-gray-2'}`}>cricUpdates</span>

          </Link>
        </div>

        <div className='w-1/5 '>
          <Link
            href='/more'
            as='/more'
            passHref
            title='More'
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/more')}>

            <ImageWithFallback height={18} width={18} loading='lazy'
              fallbackSrc = {tabIcon1}
              alt='/footer/tabicon5.png'
              className='h-8 py-1'
              src={tab === 'more' ? tabIcon5Active : tabIcon5}
            />
            <span className={`text-xs ${tab === 'more' ? 'green  font-semibold' : 'text-gray-2'}`}>More</span>

          </Link>
        </div>
    </nav>
  );
}

function resolvePath(path) {
  switch (path) {
    case '/more':
      return 'more';
    case '/news/[type]':
      return 'news';
    case '/schedule/[...slugs]':
      return 'schedule';
    case '/':
      return 'home';
    case '/criclytics':
      return 'criclytics';
    default:
      return false;
  }
}
