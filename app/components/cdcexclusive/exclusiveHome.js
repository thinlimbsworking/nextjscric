import React, { useRef } from 'react';
import DataNotFound from '../commom/datanotfound';
import Link from 'next/link';
import { getArticleTabUrl, getVideoUrl } from '../../api/services';
import ImageWithFallback from '../commom/Image';
import { format, formatDistanceStrict } from 'date-fns';

export default function ExclusiveHome({ data }) {


    return (
        <div className="w-full">


            <div className="w-full flex flex-wrap my-3  ">

                {!data.length ? <DataNotFound />
                    : data.map(video => <div className="w-full md:w-1/3 lg:w-1/3 p-2 ">

                        <Link href={video.videoYTId ? getVideoUrl(video).as : getArticleTabUrl(video.articleID).as}> <div className='w-full h-80 border dark:border-none rounded-lg bg-white  dark:bg-gray-4   '>
                            <div className='z-0 h-3/4 contain relative  rounded-t-lg cursor-pointer'>
                                <ImageWithFallback
                                    height={110}
                                    width={110}
                                    loading='lazy'
                                    src={video.videoYTId ? `https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg` : `${video?.bg_image_safari}?auto=compress&dpr=2&w=360&h=176`}
                                    alt=''
                                    className='lazyload object-cover object-top w-full h-full mx-auto rounded-t-lg '
                                />
                                {video.videoYTId && <img className='absolute w-16 h-16  top-1/2  left-1/2  transform -translate-x-1/2 -translate-y-1/2  ' src='/pngsV2/videos_icon.png' alt='' />}
                            </div>
                            <div className=' p-2 h-1/4 flex flex-col justify-between  dark:text-white  rounded-b-lg'>
                                <div className="w-full ">
                                    <div className='mx-2 truncate text-sm font-semibold'>{video.title}</div>

                                    {/* <div className='mx-2 text-xs  pt-2 truncate'>{video.description}</div> */}
                                </div>
                                <div className='flex justify-between'>
                                    {!!(video.author || video.authors?.length) && (
                                        <div className=' text-xs text-gray-2 '>
                                            By <span>{video.author || (video.authors && video.authors[0])}</span>
                                        </div>
                                    )}
                                    {video.createdAt && (
                                        <div className='mx-2 flex justify-end text-xs text-gray-2 pt-2 '>
                                            {video.createdAt
                                                ? new Date().getTime() - Number(video.createdAt) > 86400000
                                                    ? format(new Date(Number(video.createdAt)), 'dd MMM, yyyy')
                                                    : formatDistanceStrict(new Date(Number(video.createdAt)), new Date()) + ' ago'
                                                : ''}
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                        </Link>
                    </div>)
                }
            </div>

        </div>
    )
}
