import React from 'react';
const criclyticsIcon = '/pngsV2/CriclyticsIcon.png';
const FantasyStatsIcon = '/pngsV2/FantasyStatsIcon.png';

import { useRouter } from 'next/router';
import Heading from './commom/heading';

export default function CheckMoreStats() {
  
  const router = useRouter();
  const navigate = router.push;

  return (
    <div className='m-3 text-white mt-5'>



<Heading heading={'Check More Stats'} />
      {/* <p className='text-left font-semibold text-lg '>Check More Stats</p>
      <div className='bg-blue-8 h-1 rounded mt-2 w-20'></div> */}
      <div className='mt-5 flex justify-around'>
        <div
          className=' w-6/12  mr-2 cursor-pointer'
          onClick={() => {
            navigate('/criclytics', '/criclytics');
          }}
          >
          <div className='border-2 border-blue-8 bg-gray-4 rounded-md  p-2 '>
            <div className='flex justify-end'>
              <img className='w-8 h-8' src={criclyticsIcon} />
            </div>
            <p className='pt-2 font-semibold text-sm'>Criclytics</p>
          </div>
        </div>
        <div className='w-6/12 cursor-pointer '>
          <div
            className='border-2 border-blue-8 bg-gray-4 rounded-md  p-2'
            onClick={() => {
              navigate('/fantasy-research-center', '/fantasy-research-center');
            }}>
            <div className='flex justify-end'>
              <img className='w-8 h-8' src={FantasyStatsIcon} />
            </div>
            <p className='pt-2 font-semibold text-sm whitespace-nowrap capitalize'>Fantasy Centre</p>
          </div>
        </div>
      </div>
    </div>
  );
}
