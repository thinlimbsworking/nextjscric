import React, { useState, useRef, useEffect } from 'react'
import {
  format,
  formatDistanceToNowStrict,
  formatDistanceStrict,
} from 'date-fns'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import { Swiper, SwiperSlide } from 'swiper/react'
import {
  EffectCoverflow,
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Controller,
  FreeMode,
} from 'swiper'
import Heading from '../../commom/heading'
export default function ArticleSeries({ data, ...props }) {
  const [swiper, setSwiper] = useState()
  const router = useRouter()
  const navigate = router.push
  return (
    <div className="flex flex-col mt-2">
      {data && data.data.length > 0 && (
        <div className="flex m-3 justify-evenly items-center  ">
          <div className="text-left w-full">
            <Heading heading={data.tourName} hideBottom={true} />
            <div className={`bg-[#D8993B] h-1 rounded-md mt-1 w-10`}></div>
          </div>
          <div className="bg-gray px-2 py-1.5 rounded-md">
            {/* {item.seriesType} */}
            <Link
              href={`/series/${props.seriesType}/${
                props.tourID
              }/${props.tourName
                .replace(/[^a-zA-Z0-9]+/g, ' ')
                .split(' ')
                .join('-')
                .toLowerCase()}/news`}
            >
              <img
                className="h-6 w-6 object-cover object-top"
                src="/pngsV2/arrow.png"
                alt=""
              />
            </Link>
          </div>
        </div>
      )}
      {/* <div className=' mt-3 mx-2  py-2 border-b-2  border-black'> 
      <div className='flex relative items-center justify-center  w-full '>
        <img className='   rounded-lg object-conatin object-top  m-2' src={data.getArticleByPostitions[0].featureThumbnail} alt='' />
      </div>
      <div className='  overflow-hidden mt-2 text-base font-semibold text-ellipsis font-sans text-left tracking-wider '>
        {data.getArticleByPostitions[0].title || ''}
      </div>
      <div className='  text-xs font-thin my-2 truncate text-left tracking-wide '>
        {data.getArticleByPostitions[0].description || ''}
      </div>
     <div className='flex items-center py-2 justify-start'>
      <div className='px-2 text-gray-2 text-xs text-left truncate  border-r'>{data.getArticleByPostitions[0].author}</div>
      <div className='px-2 text-gray-2 text-xs text-left mt-1'>
        {format(new Date(Number(data.getArticleByPostitions[0].createdAt) - 19800000), 'dd MMM yyyy')}
      </div>
      </div>
    </div> */}
      <div className="flex overflow-scroll w-full items-stretch">
        {/* series/domestic/2254/icc-men-s-t20-world-cup-australia-2022/news */}
        {data &&
          data.data.map((item, i) => {
            return item.contentType == 'articles' ? (
              <div className="w-52 m-1 bg-gray rounded-md self-auto">
                <Link
                  href={
                    item.type === 'live-blog'
                      ? `/news/live-blog/${item?.matchIDs[0]?.matchID}/${item?.matchIDs[0]?.matchSlug}/${item.articleID}`
                      : `/news/${item.id}`
                  }
                  // href={`/news/${item.id}`}
                >
                  <div className="flex relative items-center justify-center w-52   ">
                    <img
                      className="h-28 w-full rounded-lg object-conatin object-center  "
                      src={
                        item.contentType == 'videos'
                          ? `https://img.youtube.com/vi/${item.videoYTId}/hqdefault.jpg`
                          : `${item.featureThumbnail}?auto=compress&dpr=2&fit=crop&w=150&h=100`
                      }
                      alt=""
                    />
                    {item.type ? (
                      <div
                        style={{ fontSize: 10, maxWidth: 120 }}
                        className="absolute left-0 bottom-1 text-sm bg-green px-1 uppercase font-medium text-black line-clamp-1"
                      >
                        {item.type.replace(/[_]/g, ' ')}
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="flex flex-col px-2">
                    <div className=" w-full overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left ">
                      {item.title || ''}
                    </div>

                    <div
                      style={{ fontSize: 10 }}
                      className="text-gray-2 text-left truncate  flex justify-between items-center py-2 "
                    >
                      <div className="flex text-left">By: {item.authors}</div>
                      <div className="w-[2px] h-3 bg-gray-2" />
                      <div className="flex text-right ">
                        {item.publishedAt && item.publishedAt
                          ? new Date().getTime() - Number(item.publishedAt) >
                            86400000
                            ? format(
                                new Date(Number(item.publishedAt)),
                                'dd MMM, yyyy',
                              )
                            : formatDistanceStrict(
                                new Date(Number(item.publishedAt)),
                                new Date(),
                              ) + ' ago'
                          : ''}
                      </div>
                    </div>
                  </div>
                </Link>
                {/* <div className="px-2 text-gray-2 text-xs text-left mt-1"></div> */}
              </div>
            ) : (
              <div className="  w-52  m-1 bg-gray-4 rounded-md pb-4">
                <Link
                  href={`/videos/${item.videoID
                    .replace(/[_]/g, ' ')
                    .toLowerCase()}`}
                >
                  <div className="flex relative items-center justify-center w-52 flex-col ">
                    <div className="  relative rounded-t-lg">
                      <img
                        src={`https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg`}
                        alt=""
                        className="h-32   w-52 rounded-lg object-conatin object-top "
                      />
                      <img
                        className="absolute w-16 h-16  mt-2 bottom-0 right-0  "
                        src="/pngsV2/videos_icon.png"
                        alt=""
                      />
                    </div>

                    <div className="p-2 h-1/2 text-black dark:text-white  rounded-b-lg">
                      <div className="text-xs  font-semibold  line-clamp-2">
                        {item.title}
                      </div>

                      {item.publishedAt && (
                        <div className="text-xs text-gray-2 pt-2 ">
                          {item.publishedAt
                            ? new Date().getTime() - Number(item.publishedAt) >
                              86400000
                              ? format(
                                  new Date(Number(item.publishedAt)),
                                  'dd MMM, yyyy',
                                )
                              : formatDistanceStrict(
                                  new Date(Number(item.publishedAt)),
                                  new Date(),
                                ) + ' ago'
                            : ''}
                        </div>
                      )}
                    </div>
                  </div>
                </Link>
              </div>
            )
          })}
      </div>
    </div>
  )
}
