import React, { useState } from 'react'

import { HOME_POST_MATCH_CRICLYTICS } from '../../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import PlayerIndex from '../homePAge/playerIndex'
import PlayerImpact from './playerImpact'
// import ScorecardTab from '.'
import HomeageFrcTeam from './homepageTeam'
import RunComparisonCompleted from '../../crcltics/swipeCrclytics/homeRunComparisonComplted'
import MatchReel from './matchReel'
import HomePageMain from '../../servercomponent/hompage/homepageMain'
import ContentPage from './contentPage'
import PhasesOfSession from './phasesofIng'

// import SlideComp from '../../commom/slideComp';
// import { useState } from 'react/cjs/react.production.min';
// import { useState } from 'react/cjs/react.production.min';

export default function CompltedMatch(props) {
  const [teamData, setMyTeam] = useState([])

  const { loading, error, data } = useQuery(HOME_POST_MATCH_CRICLYTICS, {
    variables: { matchID: props.matchID },
    onCompleted: (algo11) => {
      if (
        algo11 &&
        algo11.completedMatchHomepageData &&
        algo11.completedMatchHomepageData.cricketDotComTeam
      ) {
        setMyTeam([
          ...(algo11.completedMatchHomepageData.cricketDotComTeam.batsman ||
            []),
          ...(algo11.completedMatchHomepageData.cricketDotComTeam.all_rounder ||
            []),
          ...(algo11.completedMatchHomepageData.cricketDotComTeam.bowler || []),
          ...(algo11.completedMatchHomepageData.cricketDotComTeam.keeper || []),
        ])
      }
    },
  })

  if (loading) {
    return <div></div>
  }
  if (error) {
    return <div></div>
  }
  if (data) {
    // setalgoTeam([...(algo11.getAlgo11.batsman || []), ...(algo11.getAlgo11.all_rounder || []), ...(algo11.getAlgo11.bowler || []), ...(algo11.getAlgo11.keeper || [])]);
console.log("data?.completedMatchHomepageData?.livePlayerImapct",data?.completedMatchHomepageData?.livePlayerImapct)
    return (
      <div className="w-full    ">
        <div>
          {props.tabContent == 'selectMatch' && (
            <div className="">
              <div className=" ">
                { data?.completedMatchHomepageData&& (
                  <PlayerImpact
                  setShowPlayerIndex={props.setShowPlayerIndex}
                  matchStatus={props.matchStatus}
                    seriesName={props.seriesName}
                    data={data.completedMatchHomepageData}
                    matchID={props.matchID}
                    mainData={props.mainData}
                    showPlayerIndex={props.showPlayerIndex}
                  />
                )}
              </div>
              <div className="">
                {props.matchType == 'Test' && (
                  <PhasesOfSession
                  matchStatus={props.matchStatus}
                    seriesName={props.seriesName}
                    matchID={props.matchID}
                    days={props.mainData && props.mainData.currentDay}
                    mainData={props.mainData}
                  />
                )}
              </div>
              <div className="">
                {props.matchType !== 'Test' && (
                  <RunComparisonCompleted
                    seriesName={props.seriesName}
                    matchID={props.matchID}
                    matchStatus={props.matchStatus}
                    data={data.completedMatchHomepageData}
                  />
                )}
              </div>
              <div className="">
                {
                  <MatchReel
                  
                    seriesName={props.seriesName}
                    matchStatus={props.matchStatus}
                    matchID={props.matchID}
                    homePageHeading={true}
                    title={'Match Reel'}
                  />
                }
              </div>
             {  data&& data.completedMatchHomepageData&& data.completedMatchHomepageData.cricketDotComTeam
                      .teamtotalpoints && <div className="">
                <HomeageFrcTeam
                
                  teamtotalpoints={
                    data &&
                    data.completedMatchHomepageData.cricketDotComTeam
                      .teamtotalpoints
                  }
                  seriesName={props.seriesName}
                  mainData={props.mainData}
                  matchID={props.matchID}
                  score={
                    data &&
                    data.completedMatchHomepageData.cricketDotComTeam
                      .teamtotalpoints
                  }
                  matchStatus={'completed'}
                  teamName={
                    data &&
                    data.completedMatchHomepageData.cricketDotComTeam
                      .fantasy_teamName
                  }
                  teamData={teamData}
                  data2={data}
                  totalProjectedPoints={
                    data &&
                    data.completedMatchHomepageData.cricketDotComTeam
                      .totalProjectedPoints
                    }
                />
              </div>}
              {/* <HomeageFrcTeam  matchID={props.matchID} /> */}
            </div>
          )}

          {/* {props.tabContent == 'selectContent' && (
            <div className="m-2">
              <div className="flex flex-col">
                {' '}
                <ContentPage matchID={props.matchID} />
              </div>
            </div>
          )} */}
        </div>
      </div>
    )
  }
}
