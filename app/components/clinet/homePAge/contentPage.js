import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE,ARTICLES_VIDEOS_BY_SERIES,HOME_VIDEOS } from '../../../api/queries'
import NewsArticle from '../../newsArticle'
import VideoHome from './homeVideoContent'


import ArticleSeries from './articleSeriesContent'
// const Handle = Slider.Handle
const play = '/pngsV2/playcric.png'
const pause = '/pngsV2/pausebutton.png'
const empty = '/pngsV2/empty.png'



export default function ContentPage(props) {


  const { loading, error, data } = useQuery(FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE, {
    // variables: { matchId: props.matchID, matchType: props.matchType },
    // pollInterval: 8000,
})
const { loading:loading2, error:error2, data:data2 } = useQuery(ARTICLES_VIDEOS_BY_SERIES, {

})

const { loading:loading3, error:error3, data:data3 } = useQuery(HOME_VIDEOS, {

})
if(loading){
    <div> </div>
}
if(data)
{
 
   
  return (
    <div className="flex  w-full flex-col">
       <div className='flex flex-col '>
              <NewsArticle data={data && data} />
             </div>


             <div className='flex flex-col '>
              <VideoHome data={data3 && data3} />
             </div>


            {data2&& data2.getArticlesVideosBySeries.map((item,index)=>{return item.data.length>0 &&(  <div className='flex flex-col'>
             
        
              <ArticleSeries seriesType={item.seriesType} tourID={item.tourID} data={item && item} tourName={item.tourName} />
             </div>)})

}

    </div>
  )
}
}
