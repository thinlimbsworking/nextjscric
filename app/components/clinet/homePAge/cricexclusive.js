"use client";
import { exclusiveTabs } from "../../../constant/constants";
import { useState, useEffect, useRef } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { SOCIAL_TRACKER } from "../../../api/queries";
import Script from "next/script";
import Head from "next/head";
import parse from "html-react-parser";
// import TweetEmbed from 'react-tweet-embed'
import SliderTab from "../../shared/Slidertab";
import { TwitterTweetEmbed } from "react-twitter-embed";
import Tab from "../../shared/Tab";
import Button from "../../shared/button";
import Heading from "../../shared/heading";
import Link from "next/link";
export default function ClientComponent({ children }) {
  let scrl = useRef(null);
  let scrl1 = useRef(null);
  const [scrollWidth, setScrollWidth] = useState(0);

  const [scrollX, setscrollX] = useState(0);
  const [scrollX1, setscrollX1] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  const [scrolEnd1, setscrolEnd1] = useState(false);
  const [windows, updateWindows] = useState();
  const callSocialWidget = () => {
    global.windows &&
      global.windows.twttr.ready().then(() => {
        global.windows &&
          global.windows.twttr.widgets.load(
            document.getElementsByClassName("twitter-div")
          );
      });
  };
  const [tabName, setTabName] = useState({
    name: "SOCIAL TRACKER",
    value: "SOCIAL_TRACKER",
  });
  const [counter, setCointer] = useState(0);

  useEffect(() => {
    if (scrl1 && scrl1.current && scrl1.current.clientWidth) {
      setScrollWidth(scrl1.current.clientWidth);
    }
  }, []);

  const slide = (shift) => {
    scrl.current.scrollLeft += shift;
    setscrollX(scrollX + shift);

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  const slide1 = (shift) => {
    scrl1.current.scrollLeft += shift;
    setscrollX1(scrollX1 + shift);

    if (
      Math.floor(scrl1.current.scrollWidth - scrl1.current.scrollLeft) <=
      scrl1.current.offsetWidth
    ) {
      setscrolEnd1(true);
    } else {
      setscrolEnd1(false);
    }
  };
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  if (true) {
    return (
      <div className="flex flex-col  w-full ">
        <Heading heading={"Cricket.com Exclusive"}  noPad />
        <div className="flex flex-col relative items-center justify-center ">
          <div className="flex flex-col overflow-scroll w-full items-center justify-center ">
            <div
              onClick={() => scrollX !== 0 && scrollX > -1 && slide(+-205)}
              className={`${
                scrollX < 90 ? "opacity-30" : "opacity-100"
              }  flex  absolute -left-4 cursor-pointer  bg-black rounded-full   item-center justify-center z-10  white pointer dn db-ns z-999 outline-0 `}
              id="swiper-button-prev"
            >
              <svg width="20" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>

            <div className="w-full  relative">
              <div
                className={` relative  pt-4 flex  overflow-x-scroll items-center  w-full} `}
                ref={scrl}
                onScroll={scrollCheck}
              >
                {exclusiveTabs
                  .filter((ele) => ele.type)
                  .map((item, i) => {
                    return (
                      <div
                        className="  flex m-1  flex-col min-h-[160px] min-w-[200px] 
     "
                      >
                        <Link href={`/exclusive/${item.type}`}>
                          <div className="  flex flex-col  rounded-md w-full ">
                            <div key={i} className="    ">
                              <img
                                className=" flex w-full h-36 items-center justify-center rounded-md object-cover object-top"
                                src={item.src}
                                alt=""
                              />
                            </div>
                            <div className="w-full  flex items-start justify-start  truncate overflow-hidden mt-2 text-sm font-semibold   line-clamp-1 ">
                              {item.name}
                            </div>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
              </div>
            </div>

            <div
              onClick={() => !scrolEnd && slide(205)}
              id="swiper-button-prev"
              className={`${
                scrolEnd ? "opacity-40" : "opacity-100"
              } white pointer absolute  bg-black rounded-full -right-5 flex  item-center justify-center cursor-pointer shadow-md `}
            >
              <svg width="20" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
          </div>
          <div className="flex items-center justify-end w-full -mt-5">
            <Button url="/exclusive/1" title={"View All"} />
            {/* <div className='cursor-pointer w-24 p-1 border-2 border-red-5 text-red-5 font-semibold text-sm flex justify-center items-center rounded-md'>VIEW ALL</div> */}
          </div>
        </div>
      </div>
    );
  }
}
