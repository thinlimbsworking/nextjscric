import React, {useEffect, useState} from "react";
import {useQuery, useMutation} from "@apollo/react-hooks";
import {HOME_PAGE_FORM_INDEX} from "../../../api/queries";
const arrow = "/pngsV2/arrow.png";
import {useRouter} from "next/navigation";
import Heading from "../../shared/heading";
import Link from "next/link";

export default function FormIndex({...props}) {
  const [skill, setSkill] = useState("batsman");
  const router = useRouter();
  const navigate = router.push;
  const [toggle, setToggle] = useState(
    props.score && props.score.topPicks && props.score.topPicks.team1Name
  );
  // console.log("propsss12121", props);
  const [teamKey, setTeamKey] = useState("team1Data");
  //   /criclytics/222496/krm-vs-ncmi-match-24-kuwait-t20-challengers-cup-2023/form-index
  //   const { loading, error, data } = useQuery(HOME_PAGE_FORM_INDEX, {
  //     // variables: { matchID: props.matchID},
  //     variables: { matchID: props.matchID },
  //     onCompleted: (data) => {
  //       setToggle(data.homepageFormIndex.team1Name);
  //     }
  //   });

  let data = props.score.topPicks;

  const redirectToFile = () => {
    let matchName = `${props.matchProjection}-${props.seriesName}`;
    let seriesSlug = matchName
      .replace(/[^a-zA-Z0-9]+/g, " ")
      .split(" ")
      .join("-")
      .toLowerCase();
    navigate(`criclytics/${props.matchID}/${seriesSlug}/form-index`);
  };
  const capsuleCss = {
    verygood: "  text-green  py-1 pl-2 pr-1 text-xs  font-semibold",
    good: "  text-[#91B42D]  py-1 pl-2 pr-1 text-xs  font-semibold",
    neutral: "  text-yellow  py-1 pl-2 pr-1 text-xs font-semibold",
    bad: " text-orange-3  py-1 pl-2 pr-1 text-xs font-semibold",
    verybad: "  text-red  py-1 pl-2 pr-1 text-xs  font-semibold",
  };

  const round = {
    verygood: "bg-green   rounded-full w-2 h-2   ",
    good: "bg-green-4  rounded-full w-2 h-2  ",
    neutral: "bg-yellow  rounded-full w-2 h-2    ",
    bad: "bg-orange-3   rounded-full w-2 h-2    ",
    verybad: "bg-red   rounded-full w-2 h-2   ",
  };

  return props.score &&
    props.score.topPicks &&
    props.score.topPicks.team1Name ? (
    <div className="bg-gray rounded text-white relative p-2 mt-4 m-2">
      {props.showFormIndex && (
        <div
          className="     absolute  flex items-start justify-center "
          style={{top: "-7rem"}}>
          <div className="flex relative items-end justify-center  ">
            <div
              className="w-11/12 z-50    bg-gray-2  p-2 text-[11px] text-white rounded-md leading-5    "
              onClick={() => props.setShowFormIndex(false)}>
              It categorizes a player's form based on their last year's
              performance, comparing players with similar skills (e.g. top order
              batsman with top order batsman). Parameters considered are batting
              avg and SR for batsmen, and bowl avg and SR for bowlers.
            </div>
          </div>
        </div>
      )}
      <div className="flex justify-between items-center">
        <div className="flex items-center justify-center">
          <Heading heading={"Form Index"} />

          <svg
            xmlns="http://www.w3.org/2000/svg"
            onClick={() => props.setShowFormIndex(true)}
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-4 h-4 ml-1 -mt-2">
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"
            />
          </svg>
        </div>
        {/* <h3 className='font-semibold text-lg tracking-wider'>Form Index</h3> */}
        <Link
          href={`/criclytics/${props.matchID}/${props.mainData.seriesName
            .replace(/[^a-zA-Z0-9]+/g, " ")
            .split(" ")
            .join("-")
            .toLowerCase()}/form-index`}>
          <div className="bg-basebg px-2 py-1.5 rounded">
            <img
              onClick={() => redirectToFile()}
              className="bg-basebg w-6 rounded-lg"
              src={arrow}
            />
          </div>
        </Link>
      </div>

      <div className="bg-gray-4 rounded-3xl mt-4 flex items-center justify-between text-sm ">
        <div
          className={`w-1/2 text-center rounded-3xl py-1.5  ${
            toggle.toLowerCase() ===
            props.score.topPicks.team1Name.toLowerCase()
              ? `border-2  border-green`
              : ""
          }`}
          onClick={() => (
            setToggle(
              props.score &&
                props.score.topPicks &&
                props.score.topPicks.team1Name
            ),
            setTeamKey("team1Data")
          )}>
          {props.score &&
            props.score.topPicks &&
            props.score.topPicks.team1Name}
        </div>
        <div
          className={`w-1/2 text-center rounded-3xl py-1.5  ${
            toggle.toLowerCase() ===
            props.score.topPicks.team2Name.toLowerCase()
              ? `border-2  border-green`
              : ""
          }`}
          onClick={() => {
            if (props.score.topPicks.team2Name) {
              setToggle(props.score.topPicks.team2Name),
                setTeamKey("team2Data");
            }
          }}>
          {props.score &&
            props.score.topPicks &&
            props.score.topPicks.team2Name}
        </div>
      </div>

      {
        <div className="">
          {props.score.topPicks[teamKey].map((player, index) => (
            <div
              className="bg-gray-4 mb-3 p-3  flex-col  rounded-lg mt-4 flex justify-center items-start "
              key={index}>
              <div className="flex items-center w-full">
                <div className="flex items-center justify-start w-full ">
                  <div className="relative">
                    <div className="h-14 w-14 rounded-full overflow-hidden bg-basebg">
                      <img
                        className="h-16 w-16  object-top object-cover mt-1"
                        src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = "/pngsV2/playerph.png")
                        }
                      />
                    </div>
                    {/* {player.playerRole.toLowerCase() == "bowler"} */}
                    <img
                      height={18}
                      width={18}
                      className="w-6 h-6 drop-shadow-lg  p-1  bg-basebg  absolute  -bottom-1 -right-1 border  rounded-full "
                      src={
                        player.playerRole.toLowerCase() == "bowler"
                          ? "/pngsV2/ball-line-white.png"
                          : player.playerRole.toLowerCase() == "batsman"
                          ? "/pngsV2/bat-line-white.png"
                          : "/pngsV2/allrounder-line-white.png"
                      }
                    />
                  </div>
                  <div className="pl-2">
                    <div className="text-sm font-semibold">
                      {player.playerName}
                    </div>
                  </div>
                </div>

                <div className="flex items-center w-5/12 justify-end  text-xs">
                  <div
                    className={`${
                      capsuleCss[player.Form.split(" ").join("").toLowerCase()]
                    } uppercase text-xs`}>
                    {player.Form}
                  </div>
                </div>
              </div>
              <div className="flex items-center justify-end text-xs w-full ">
                <div className="w-full flex items-center font-regular mt-5 mb-1">
                  Recent form
                </div>
                {player.playerRole.toLowerCase() == "all_rounder" && (
                  <div className="w-full  flex-col  flex items-end justify-end ">
                    {/* <div className='w-full'>    Recent form  </div> */}

                    {skill == "batsman" && (
                      <div className="flex bg-basebg p-1 rounded-full items-center justify-evenly">
                        {/* {player.playerRole.toLowerCase()=="all_rounder" && <span onClick={()=>setSkill(skill=='batsman'?'bowler':'batsman')}>ss </span>} */}
                        <img
                          onClick={() =>
                            setSkill(skill == "batsman" ? "bowler" : "batsman")
                          }
                          className="h-6 bg-gray rounded-full p-1"
                          src="/pngsV2/batgreen.png"
                          alt=""
                          srcset=""
                        />
                        <img
                          onClick={() =>
                            setSkill(skill == "batsman" ? "bowler" : "batsman")
                          }
                          className="h-6 p-1"
                          src="/pngsV2/ballwhiteindex.png"
                          alt=""
                          srcset=""
                        />
                      </div>
                    )}
                    {skill == "bowler" && (
                      <div className="flex bg-basebg p-1 rounded-full items-center justify-evenly">
                        {/* {player.playerRole.toLowerCase()=="all_rounder" && <span onClick={()=>setSkill(skill=='batsman'?'bowler':'batsman')}>ss </span>} */}
                        <img
                          onClick={() =>
                            setSkill(skill == "batsman" ? "bowler" : "batsman")
                          }
                          className="h-6  rounded-full p-1"
                          src="/pngsV2/batwhiteindex.png"
                          alt=""
                          srcset=""
                        />
                        <img
                          onClick={() =>
                            setSkill(skill == "batsman" ? "bowler" : "batsman")
                          }
                          className="h-6 p-1 bg-gray rounded-full"
                          src="/pngsV2/ballgreen.png"
                          alt=""
                          srcset=""
                        />
                      </div>
                    )}
                  </div>
                )}
              </div>

              <div className="flex items-center justify-between w-full">
                {player.matches.map((item) => {
                  return (
                    <div className="bg-basebg p-2 rounded-md flex flex-col items-center justify-center m-1 w-full">
                      {player.playerRole.toLowerCase() !== "all_rounder" && (
                        <div className="flex text-xs  font-mnb text-center">
                          {player.playerRole.toLowerCase() == "batsman"
                            ? item.battingStats
                            : item.bowlingStats}
                        </div>
                      )}
                      {player.playerRole.toLowerCase() == "all_rounder" && (
                        <div className="flex text-xs  font-mnb text-center">
                          {skill == "batsman"
                            ? item.battingStats
                            : item.bowlingStats}
                        </div>
                      )}
                      <div className="flex text-xs font-mnr text-light py-1 text-center">
                        {item.homeTeamShortName} {" vs "}{" "}
                        {item.awayTeamShortName}
                      </div>
                    </div>
                  );
                })}
              </div>

              {/* <div className='flex items-center justify-between'>
                <img
                  className='h-10 w-10 bg-gray object-top object-cover rounded-full'
                  src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                />
                <div className='pl-1'>
                  <div className='text-xs font-semibold pb-1'>{player.playerName}</div>
                  <div className=' text-gray-2 text-[9px] font-medium'>
                    <div className=''> {player.playerRole == 'BOWLER' ? 'BOWL AVG:' : 'BAT AVG:'} {player.avg} | {player.playerRole == 'BOWLER' ? 'BOWL SR:' : 'BAT SR:'} {player.sr} </div>
                  </div>
                </div>
              </div> */}
            </div>
          ))}
        </div>
      }
      {/* <div className='flex justify-between pt-3 pb-1'>
      
          <div className='flex items-center'>
            <div className='h-2 w-2 rounded-full bg-green'></div>
            <div className='text-gray-2 text-xs font-thin pl-1'>Very Good</div>
          </div>
          <div className='flex items-center justify-center'>
            <div className='h-2 w-2 rounded-full bg-green-3'></div>
            <div className='text-gray-2 text-xs font-thin pl-1'>Good</div>
          </div>
          <div className='flex items-center justify-center'>
            <div className='h-2 w-2 rounded-full bg-yellow'></div>
            <div className='text-gray-2 text-xs font-thin pl-1'>Neutral</div>
          </div>

          <div className='flex items-center'>
            <div className='h-2 w-2 rounded-full bg-orange-3'></div>
            <div className='text-gray-2 text-xs font-thin pl-1'>Bad</div>
          </div>
          <div className='flex items-center'>
            <div className='h-2 w-2 rounded-full bg-red'></div>
            <div className='text-gray-2 text-xs font-thin pl-1'>Very Bad</div>
          </div>
      
      </div> */}
    </div>
  ) : (
    <div></div>
  );
}
