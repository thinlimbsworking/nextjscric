'use client'

import React from 'react'
import Button from '../../shared/button'
import Heading from '../../shared/heading'
// import NewsVideo from './newHomePage'
import Link from 'next/link'
import { HomePageData } from '../../../api/queries'
import dynamic from 'next/dynamic'
import { format, formatDistanceToNowStrict } from 'date-fns'

import { useQuery, useMutation } from '@apollo/react-hooks'
const HomePageCLinet = dynamic(() => import('./homePageClinet', { ssr: false }))

// import HomePageCLinet from './homePageClinet';
// const HomePageVideo = dynamic(() => import("./homePageVideo",{ssr:false}))

// import HomePageVideo from './homePageVideo';
const Excluseive = dynamic(() => import('./cricexclusive', { ssr: false }))
const SeriesHome = dynamic(() => import('./seriesHomePage', { ssr: false }))
const OnthisDay = dynamic(() => import('./onthisday', { ssr: false }))
const QuizNews = dynamic(() => import('./quizNews', { ssr: false }))

const TwitterHomePage = dynamic(() => import('./tweetHomePage', { ssr: false }))
const SeriesHomeNews = dynamic(() =>
  import('./seriesHomeVideoArticle', { ssr: false }),
)
// const HomeNewsVideo = dynamic(() => import('./homeNewsVideo',{ssr:false}))

const HomePageVideo = dynamic(() => import('./homePageVideo', { ssr: false }))

export default function HomeNewsVideo(props) {
  const { loading, error, data } = useQuery(HomePageData, {})

  if (loading) {
    return <div></div>
  }
  if (data) {
    return (
      <div className="w-full">
        {' '}
        <div className="w-full flex  py-4 px-8 rounded-md mt-5 shadow-sm   bg-white">
          {' '}
          <HomePageCLinet data={data} />
        </div>
        <div className="w-full flex bg-white py-4 px-8 rounded-md   mt-5 shadow-sm">
          {' '}
          <HomePageVideo data={data} />
        </div>
        <div className="w-full flex  bg-white pt-4 pb-5  px-8 rounded-md  mt-5 shadow-sm ">
          {' '}
          <Excluseive data={data} />
        </div>
        <div className="w-full flex  bg-white pt-4 pb-3 px-8 rounded-md  mt-5 shadow-sm">
          {' '}
          <TwitterHomePage data={data.getWebHomePageData} />
        </div>
        <div className="w-full flex  bg-white pt-4 pb-3 px-8 rounded-md  mt-5 shadow-sm ">
          {' '}
          <OnthisDay />
        </div>
        <div >
          {' '}
          <QuizNews />{' '}
        </div>
        <div className="w-full flex  bg-white pt-4 pb-3 px-8  rounded-md  mt-5 shadow-sm">
          {' '}
          <SeriesHome data={data} />
        </div>
        <div className="w-full flex shadow-sm ">
          {' '}
          <SeriesHomeNews data={data} />
        </div>
      </div>
    )
  }
}
