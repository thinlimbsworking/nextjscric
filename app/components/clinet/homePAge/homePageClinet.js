'use client'

import React from 'react'
import Button from '../../shared/button'
import Heading from '../../shared/heading'
// import NewsVideo from './newHomePage'
import { format, formatDistanceToNowStrict } from 'date-fns'

import Link from 'next/link'
import { FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE } from '../../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'

export default function HomePageCLinet({ data, ...props }) {
  //   let { loading, error, data } = useQuery(FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE, {

  // })
  // const { loading, error, data2 } = useQuery(FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE, {
  // }
  // )

  // if(loading)
  // {
  //   return (<div></div>)
  // }
  if (data.getArticleByPostitions) {
    return (
      <div className="flex  flex-col items-center justify-center w-full  ">
        <div className="flex items-start justify-start  flex-col  w-full ">
          <Heading heading={'News '} noPad />

          <div className="flex flex-col  w-full  pt-4">
            <Link
              href={
                data?.getArticleByPostitions[0]?.type !== 'live-blog'
                  ? `/news/${data?.getArticleByPostitions[0]?.articleID}`
                  : `/news/live-blog/${data?.getArticleByPostitions[0]?.matchIDs[0]?.matchID
                      .replace(/[^a-zA-Z0-9]+/g, ' ')
                      .split(' ')
                      .join('-')
                      .toLowerCase()}/${data?.getArticleByPostitions[0]?.matchIDs[0]?.matchSlug
                      .replace(/[^a-zA-Z0-9]+/g, ' ')
                      .split(' ')
                      .join('-')
                      .toLowerCase()}/${
                      data?.getArticleByPostitions[0]?.articleID
                    }`
              }
            >
              {/* <Link
              href={`/news/${data.getArticleByPostitions[0].articleID}`}
              as={`/news/${data.getArticleByPostitions[0].articleID}`}
            > */}
              <div className="flex flex-col  w-full ">
                <div className="w-full h-72  rounded-md relative">
                  <img
                    className=" w-full h-full object-top object-cover  rounded-md "
                    src={`${data.getArticleByPostitions[0].featureThumbnail}?auto=compress&fit=crop&crop=face&w=600&h=350`}
                    // src={`${data.getArticleByPostitions[1].featureThumbnail}?auto=compress&fit=crop&crop=top&h=400`}
                    alt=""
                  />
                  <div className="px-1 absolute bottom-2 dark:bg-green-6 dark:text-black text-white bg-gray-4 rounded-sm text-xs font-medium left-1">
                    {data.getArticleByPostitions &&
                      data.getArticleByPostitions[0].type
                        .replace(/[_]/g, ' ')
                        .toUpperCase()}
                  </div>
                </div>
                <div className="overflow-hidden mt-2 text-base font-medium text-ellipsis text-left font-mnm leading-6 line-clamp-1">
                  {' '}
                  {data.getArticleByPostitions[0].title || ''}{' '}
                </div>
                <div className="text-sm leading-5 text-gray-9 font-semibold text-left font-mnr line-clamp-2 ">
                  {' '}
                  {data.getArticleByPostitions[0].description || ''}
                </div>
              </div>
            </Link>
            {/* ews/live-blog/222229/mi-vs-gt-match-57-indian-premier-league-2023/test-168387247315 */}
            {console.log(data, 'data---->>>>>')}
            <div className="w-full text-mnr pt-4">
              <div className="">
                <div className="grid grid-cols-3 gap-4 ">
                  {data &&
                    data.getArticleByPostitions.map((item, i) => {
                      return (
                        0 < i &&
                        i < 4 && (
                          <div className="rounded-md">
                            {console.log('itemitemitemitemitem', data)}
                            {/* temp1.getArticleByPostitions[0].matchIDs[0].matchID */}
                            <Link
                              href={
                                item.type !== 'live-blog'
                                  ? `/news/${item?.articleID}`
                                  : `/news/live-blog/${item?.matchIDs?.[0]?.matchID
                                      .replace(/[^a-zA-Z0-9]+/g, ' ')
                                      .split(' ')
                                      .join('-')
                                      .toLowerCase()}/${item?.matchIDs?.[0]?.matchSlug
                                      .replace(/[^a-zA-Z0-9]+/g, ' ')
                                      .split(' ')
                                      .join('-')
                                      .toLowerCase()}/${item?.articleID}`
                              }
                            >
                              <div className="flex  items-center justify-center  h-28  relative">
                                <img
                                  className=" w-full  rounded-lg object-cover object-top h-full"
                                  src={`${item?.featureThumbnail}?auto=compress&fit=crop&w=260&h=150`}
                                  alt=""
                                />

                                <div className="px-1 absolute bottom-2 dark:bg-green-6 dark:text-black text-white bg-gray-4 rounded-sm text-xs font-medium left-1">
                                  {item &&
                                    item.type
                                      .replace(/[_]/g, ' ')
                                      .toUpperCase()}
                                </div>
                              </div>
                              <div className="mt-2 font-mnsb leading-4 text-sm font-semibold text-gray-9 line-clamp-2">
                                {' '}
                                {item.title || ''}
                              </div>
                              <div className="flex items-center  justify-start p-1">
                                <div className="pr-2  text-gray-2 text-xs text-left w-6/12  line-clamp-1  border-r">
                                  {item.author}
                                </div>
                                <div className="px-2 text-gray-2 text-xs text-left ">
                                  {format(
                                    new Date(Number(item.createdAt) - 19800000),
                                    'dd MMM yyyy',
                                  )}
                                </div>
                              </div>
                              {/* <div className='px-2  text-gray-2 text-xs text-left truncate '>{item.author}</div> */}
                            </Link>
                          </div>
                        )
                      )
                    })}
                </div>
              </div>

              {false &&
                data &&
                data.map((item, i) => {
                  return (
                    <div className="   pb-4 bg-gray cardUpdate  rounded-xl  m-1 ">
                      <div className="flex  items-center justify-center w-full ">
                        <img
                          className="h-32   w-full rounded-lg object-conatin object-top "
                          src={item.featureThumbnail}
                          alt=""
                        />
                      </div>
                      <div className=" px-2 overflow-hidden mt-2 text-sm font-semibold text-ellipsis text-left ">
                        {item.title || ''}
                      </div>
                      <div className=" my-1 px-2 text-xs font-thin truncate text-left tracking-wide ">
                        {item.description || ''}
                      </div>
                      <div className="px-2  text-gray-2 text-xs text-left truncate ">
                        {item.author}
                      </div>
                    </div>
                  )
                })}
            </div>
          </div>
        </div>

        <div className="flex w-full items-center justify-end mt-5 mb-3">
          <Button title={'MORE ARTICLES '} url={'/news/latest'} />
        </div>
      </div>
    )
  }
}
