'use client';

import React from 'react'
import Button from '../../shared/button'
import Heading from '../../shared/heading'
// import NewsVideo from './newHomePage'
import Link from 'next/link'
import { GET_VIDEO_POSITIONS } from '../../../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { format, formatDistanceToNowStrict } from 'date-fns';

// import {HeadingText} from '../../../components/shared/css'

export default function HomePageCLinet({data,...props}) {

//   let { loading, error, data } = useQuery(FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE, {
  
// })
// const { loading, error, data } = useQuery(GET_VIDEO_POSITIONS, {
// }
// )

// if(loading)
// {
//   return (<div></div>)
// }
if(data)



  {
   
return (
        <div className='flex  flex-col items-center justify-center  w-full  '>
    
        <div className='flex items-start justify-start  flex-col  w-full ' >
    
          <Heading heading={' Videos'} noPad />
    
    
    
    
          <div className='flex flex-col  w-full pt-5 '>
    <Link href={`videos/${data.getVideosPostitions[0].videoID}`}>
            <div className='flex flex-col  w-full ' >
    
    
              <div className='flex   items-center justify-center w-full h-72  rounded-md ' >
    
                <img
                  className=' flex  h-full w-full items-center justify-center  rounded-md object-cover'
                  src={`https://img.youtube.com/vi/${data.getVideosPostitions[0].videoYTId}/hqdefault.jpg`}
                  alt='' />
    
                <img className="absolute  h-12 w-12" src="/pngsV2/redplay.png" alt="" />
              </div>
    

           <div className='flex items-center justify-start w-full  overflow-hidden py-2 text-base font-medium  text-left   font-mnm  leading-6 line-clamp-1 '> 
              
              {data.getVideosPostitions[0].title || ''} </div> 
              {/* <div className='flex w-full my-1 ml-2  text-base  font-thin    text-left tracking-wide text-gray  font-mnr  '> {data.getVideosPostitions[0].description || ''}</div> */}
    
            </div>
            </Link>
    
    
            <div className='w-full text-mnr pt-3'>
              <div className=''>
                <div className='grid grid-cols-3 gap-4'>
    
                  {data &&
                    data.getVideosPostitions.map((item, i) => {
                      return 0 < i && i < 4 && (<div className='rounded-md  '>
                        <Link  href={`videos/${item.videoID}`}>

                        <div className='flex  items-center justify-center h-28'>
                            <img className='rounded-lg w-full object-cover h-full'  src={`https://img.youtube.com/vi/${item.videoYTId}/hqdefault.jpg?auto=compress&fit=crop&crop=faces,top&w=260&h=150`} alt='' />
                            <img className="absolute  h-10 w-10" src="/pngsV2/redplay.png" alt="" />
                        </div>                
                        <div className='mt-2 text-sm  leading-4 text-gray-9 font-semibold  line-clamp-2 '> {item.title || ''} 
                         
                        </div>
                        {/* <div className='flex items-center  justify-start p-1'>
      <div className='pr-2  text-gray-2 text-xs text-left  line-clamp-1  border-r'>{item.author}</div>
      <div className='px-2 text-gray-2 text-xs text-left '>
        {format(new Date(Number(item.createdAt) - 19800000), 'dd MMM yyyy')}
      </div>
      </div> */}
                        </Link>
                      </div>
                     
    
                      )
                    })}
                </div>
              </div>
    
    
    
            </div>
    
    
    
    
          </div>
        </div>
        <div className='flex w-full items-center justify-end  mt-5 mb-3'>
        <Button title={"More Videos"} url={"/videos/latest "}   />
         </div>
        {/* <Link className='w-full flex w-full items-center justify-end ' href="/videos/latest "> 
        <div className='flex w-full items-center justify-end '>
          <div className='flex w-2/12 items-center justify-center text-center cursor-pointer   my-2 border border-red-5 font-mnr  text-red-5 p-2 text-xs rounded-md '> More Videos</div>
        
         </div>
         </Link> */}
    
      </div>)
  }
 
}
