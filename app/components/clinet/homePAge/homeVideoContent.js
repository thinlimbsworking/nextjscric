import React, { useState, useRef, useEffect } from 'react'
import { format, formatDistanceToNowStrict } from 'date-fns'
import { useRouter } from 'next/navigation'
import Heading from '../../commom/heading'
import Link from 'next/link'
// import Heading from '../commom/heading';
export default function VideoHome({ data, ...props }) {
  const [swiper, setSwiper] = useState()
  const router = useRouter()
  const navigate = router.push
  return (
    <div className="flex flex-col bg-gray-4  mt-3  rounded-md p-2 ">
      <div className="flex items-center px-2 pb-2">
        <div className="text-left w-full">
          <Heading heading={'Featured Videos'} lineColor={'#ddd'} />
        </div>

        <Link href={`videos/latest`}>
          <div className="bg-basebg px-2 py-1.5 rounded-md">
            <img
              className="h-6 w-6 object-cover object-top cursor-pointer"
              src="/pngsV2/arrow.png"
              alt=""
            />
          </div>
        </Link>
      </div>
      <Link href={`videos/${data && data.getVideosPostitions[0].videoID}`}>
        <div className="  mx-2  pb-2  border-black">
          <div className="flex relative items-center rounded-md justify-center  w-full   ">
            <img
              className="w-full h-48 rounded-md object-cover object-center "
              src={`https://img.youtube.com/vi/${
                data && data.getVideosPostitions[0].videoYTId
              }/hqdefault.jpg`}
              alt=""
            />
            <div className="flex items-center justify-center absolute">
              <img
                className="w-16 h-16 "
                src="/pngsV2/newPlayIcon.png"
                alt=""
              />
            </div>
          </div>
          <div className="  overflow-hidden mt-2 text-sm font-semibold text-ellipsis font-sans text-left tracking-wider ">
            {(data && data.getVideosPostitions[0].title) || ''}
          </div>
          <div className="   overflow-hidden mt-1 text-xs font-light text-ellipsis font-sans text-left tracking-wider line-clamp-2">
            {(data && data.getVideosPostitions[0].description) || ''}
          </div>
          {data &&
          data.getVideosPostitions &&
          data.getVideosPostitions.length > 0 ? (
            <div className="text-gray-2 text-xs text-left mt-1">
              {format(
                new Date(
                  Number(data.getVideosPostitions[0].createdAt) - 19800000,
                ),
                'dd MMM yyyy',
              )}
            </div>
          ) : (
            <></>
          )}
        </div>
      </Link>
      <div className="flex w-full ">
        <div className="flex w-full  overflow-x-scroll">
          {true &&
            data &&
            data.getVideosPostitions.map((item, i) => {
              return (
                i > 0 && (
                  <Link href={`videos/${item.videoID}`}>
                    {' '}
                    <div className="   w-52 m-1">
                      <div className="flex relative items-center rounded-md justify-center w-52 ">
                        <img
                          className="w-full h-32 rounded-md object-cover object-center  "
                          src={`https://img.youtube.com/vi/${item.videoYTId}/hqdefault.jpg`}
                          alt=""
                        />

                        <div className="flex items-center justify-center absolute">
                          <img
                            className="w-10 h-10 "
                            src="/pngsV2/newPlayIcon.png"
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left tracking-wider line-clamp-2">
                        {item.title || ''}
                      </div>

                      <div className="text-gray-2 text-xs text-left mt-1">
                        {format(
                          new Date(Number(item.createdAt) - 19800000),
                          'dd MMM yyyy',
                        )}
                      </div>
                    </div>
                  </Link>
                )
              )
            })}
        </div>
      </div>
    </div>
  )
}
