import React from 'react'
import Heading from '../../commom/heading';
import SlideComp from '../../commom/slideComp';
import Link from 'next/link';
export default function HomeageFrcTeam(props) {
 
  <style jsx>{`
       
  ::-webkit-scrollbar {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height:4px;
      background-color:red
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px;
      background-color: red;
      
    }
`}</style>


  let algoteam=props.teamData
  const getData = teamData => {
    const captain = teamData.filter(pl => pl.captain == 1);
    const vice_captain = teamData.filter(pl => pl.vice_captain == 1);
    const filteredList = teamData.filter(
      pl => pl.captain != 1 && pl.vice_captain != 1,
    );

    // console.log(captain, 'captain');
    return [captain[0], vice_captain[0], ...filteredList];
  };
  // alert(8)
  return (algoteam &&
    algoteam?.length &&
    <div className={ `${props.matchStatus === "live" ?`mt-2`:'mt-2'}`}>
    <div className={` text-white flex flex-col    p-2  bg-gray rounded-md  ${props.matchStatus === "live" ?'mx-2': 'mx-2'} `}>
      
    <div className='flex    py  items-center justify-between'>
     <Heading heading={`Fantasy Team ${props.matchStatus === "completed" ? "Performance" : ""}`} />
      <div className='flex justify-center rounded  bg-basebg px-2 py-1.5   items-center  '
     
      >
        <Link href={`/fantasy-research-center/${props.matchID&& props.matchID}/${props.seriesName && props.seriesName.replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()} /fantasyTeam`}>
        <img className='w-6 h-6' src='/pngsV2/arrow.png' alt='' />
        </Link>
      </div>
    </div>

    <div className='flex bg-gray-4  mt-2 items-center justify-evenly p-2 rounded-md'>
     <div className='flex text-left w-5/12  text-xs'>{props.teamName||'Any contest'}</div>
     <div className='flex text-right items-center justify-end  w-7/12 text-xs '> 
     
     <div className='font-light text-xs text-[12px]'>{props.matchStatus === "completed" ? 'FANTASY': props.matchStatus === "upcoming" ? "PROJECTED" : "LIVE FANTASY"} POINTS : <span className='text-xs font-semibold text-green'>{props.score}</span> </div> 
     {/* FANTASY POINTS{" "} :  <span className='text-xs text-green text-right ml-1'>{props.teamtotalpoints}</span> */}
     </div>
      </div>
    <div className='flex overflow-scroll w-full  py-2  '>
      <div className='flex items-stretch justify-start'>
      {algoteam &&
      algoteam.length > 0 &&
      getData(algoteam).map((item, index) =>  {
          return (
        <div className={`flex  self-auto w-28 ${index === algoteam.length - 1 ? "border-r-none":"border-r"}  border-gray-4 px-2`}>
          <div className='flex flex-col items-center w-full justify-between'>
              <div className='relative mb-2  flex justify-center'>
                <div className='h-16 w-16 rounded-full dark:bg-gray-4 overflow-hidden'>
                <img height={28} width={28} loading='lazy'
                  //  fallbackSrc = '/pngsV2/playerph.png'
                  className='h-20 w-20 bg-white  rounded-full dark:bg-gray-4 object-contain object-top mt-2'
                  src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                />
                </div>
                    {(item.captain == '1' || item.vice_captain == '1') && (
                  <div className='w-7 h-5 absolute -left-1 bottom-0 rounded-md text-green border bg-gray-4 flex justify-center items-center text-xs font-medium'>
                    {item.captain == '1' ? 'C' : item.vice_captain == '1' ? 'VC' : ''}
                  </div>
                )}
                <img height={18} width={18} loading='lazy'
                  //  fallbackSrc = '/pngsV2/flag_dark.png'
                  className='w-5 h-5 drop-shadow-lg absolute border -right-2 bottom-0 object-center flex justify-center rounded-full '
                  src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                 
                />
            
              </div>
              <p
                className='dark:text-white text-xs text-center font-medium  line-clamp-2'
              >
                { item.playerName}

              </p>
              <p className='text-gray-2 text-xs uppercase text-center '>{item.player_role.split('_').join(" ")}</p>
            </div>
       
              </div>
          );
        })}
       </div>
    </div>
    <div className='flex w-full items-start justify-arround text-sm font-mnr font-thin'>

      <div className='flex w-full'>
        {
          props.matchStatus === "completed" ? 
            <p className='uppercase text-xs'>Pojected POINTS : <span className='text-blue-8 font-semibold'>{props.totalProjectedPoints}</span> </p>
            : null
        }

      </div>
<div className='w-9/12 flex font-medium text-xs  items-start justify-end'>

Powered By  <span className=' pl-1 text-xs  text-blue-8'> Criclytics</span> <sub className='mt-1 text-[6px] text-blue-8'>TM</sub>
</div>
    </div>
  </div>
  </div>
  )
}

