import React from 'react'
import Heading from '../../commom/heading'
import SlideComp from '../../commom/slideComp'
import Link from 'next/link'

export default function HomeageFrcTeam(props) {
  console.log(props)
  ;<style jsx>{`
    ::-webkit-scrollbar {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height: 4px;
      background-color: red;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px;
      background-color: red;
    }
  `}</style>
  const backImg = '/pngsV2/frcteamback.png'
  let algoteam = props.teamData
  return (
    <div
      className={
        props.item
          ? 'mt-4 rounded-md  flex  items-start justify-start flex-col w-full'
          : 'text-white mb:mt-5 md:mt-1 p-1 mx-2 bg-gray rounded-md px-3 '
      }
    >
      {!props.item && (
        <div className="flex items-center   justify-between">
          <Heading heading={'Fantasy Team'} />
          <div className="flex justify-center rounded  bg-gray-4 p-1  items-center  ">
            <img className="w-8 h-8" src="/pngsV2/arrow.png" alt="" />
          </div>
        </div>
      )}
      {props.item && (
        <div className="flex flex-col  items-center justify-between">
          <div className="flex justify-center rounded text-xs font-mnr items-center  text-left font-medium  ">
            {/* {props.item.matchNumber}. {props.item.seriesName} */}
            {props.item.seriesName}
          </div>
        </div>
      )}
      {props.item && (
        <div className="flex items-center justify-between w-full my-2">
          <div className="flex items-center justify-start text-xs font-medium">
            <img
              className="h-4 w-6 rounded-sm"
              // src={`https://images.cricket.com/teams/${
              //   props.item && props.item.homeTeamID
              // }_flag_safari.png`}
              src={`https://images.cricket.com/teams/${props?.item?.homeTeamID}_flag_safari.png`}
              onError={(evt) => (evt.target.src = '/pngsV2/flag_light.png')}
            />

            <span className="pl-2">{props.item.homeTeamShortName}</span>
          </div>
          <div className="h-5 w-5 flex justify-center items-center text-[0.6rem] bg-slate-100 rounded-full">
            vs
          </div>
          <div className="flex  items items-center justify-end text-xs  font-medium">
            <span className="pr-2">{props.item.awayTeamShortName}</span>
            <img
              className="h-4 w-6 rounded-sm"
              src={`https://images.cricket.com/teams/${
                props.item && props.item.awayTeamID
              }_flag_safari.png`}
              onError={(evt) => (evt.target.src = '/pngsV2/flag_light.png')}
            />
          </div>
        </div>
      )}

      <div className="bg-slate-100 p-2 rounded w-full">
        <div className="flex items-center w-full text-slate-700">
          <div className="flex text-left text-[0.66rem] w-full">
            PROJECTED POINTS{' '}
          </div>
          <div className="flex text-right items-center justify-end text-xs uppercase w-full  font-bold">
            <span className="text-xs text-right font-semibold">
             {props&& props.item.maxProjection.minProjections} - { props.item && props.item.maxProjection.maxProjections}
            </span>
          </div>
        </div>
        <div className=" w-full flex   items-center justify-center pt-2">
          {algoteam &&
            algoteam.length > 0 &&
            algoteam.map((item, y) => {
              return (
                <div className="w-full  flex items-center justify-center">
                  <div
                    className={`   ${
                      props.item ? '' : 'border-r border-black'
                    }  w-24`}
                  >
                    <div className="flex w-full justify-center">
                      <div className="flex flex-col items-center ">
                        <div className="relative mb-2  flex justify-center">
                          <img
                            height={28}
                            width={28}
                            loading="lazy"
                            fallbackSrc="/pngsV2/playerph.png"
                            className="h-10 w-10 bg-slate-50 dark:bg-gray-4 rounded-full  object-contain object-top"
                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/pngsV2/playerph.png')
                            }
                          />
                          {(item.captain == '1' ||
                            item.vice_captain == '1') && (
                            <div
                              className={`w-5 h-4 absolute left-0 bottom-0 rounded-md text-green border border-slate-100 bg-gray-4 flex justify-center items-center text-[0.5rem] font-medium`}
                            >
                              {item.captain == '1'
                                ? 'C'
                                : item.vice_captain == '1'
                                ? 'VC'
                                : ''}
                            </div>
                          )}
                          <img
                            height={18}
                            width={18}
                            loading="lazy"
                            fallbackSrc="/svgs/empty.svg"
                            className="w-3 h-3 drop-shadow-lg absolute border -right-2 bottom-0 object-center flex justify-center rounded-full "
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/pngsV2/flag_dark.png')
                            }
                          />
                        </div>
                        <p className="dark:text-white font-semibold text-slate-900  text-center text-[0.6rem]  w-20 line-clamp-1">
                          {item.playerName}
                        </p>
                        <p className="text-[0.6rem] text-gray-2 capitalize text-center">
                          Credits:{' '}
                          <span className="text-gray-6"> {item.credits}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      </div>

      <div className="w-full flex justify-end pt-3">
        <Link
          href={`/fantasy-research-center/${props.item.matchid}/${
            props.item &&
            props.item.matchName
              .replace(/[^a-zA-Z0-9]+/g, ' ')
              .split(' ')
              .join('-')
              .toLowerCase()
          }/fantasyTeam`}
        >
          <button className="py-1 px-2 rounded text-red-5 border border-red-5 text-[0.66rem]">
            View Team
          </button>
        </Link>
      </div>
    </div>
  )
}
