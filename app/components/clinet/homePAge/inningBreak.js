import React from 'react'
import { MATCH_SUMMARY, MATCH_DATA_TAB_WISE } from '../../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import Heading from '../../commom/heading'
const flagPlaceHolder = '/pngsV2/flag_dark.png'



export default function InngBreak({
  matchID,
 
  status,

  ...props
}) {

    var { loading, error, data:data2} = useQuery(MATCH_DATA_TAB_WISE, {
      variables: { matchID, status:"completed" },
    })
  
  if(data2)
  {

let data=data2.getMatchCardTabWiseByMatchID


  return (
    <div>
      {true && data && data.matchSummary && data.matchSummary && (
        <div className="m-2">
          <div className="white dark:bg-gray-8 bg-white">
            {/* <div className="dark:text-white text-black p-3 text-base font-semibold">
              Innings SUMMARY
              <div className="border-b-gray-3 h-[1px] my-1 w-[60px] border-b-2 border-b-solid "></div>
            </div> */}
            <div className='m-2'>
            <Heading heading={"Innings Summary"} />
            </div>


            <div className="dark:bg-gray bg-white mx-3 pb-3 mb-3">
              <div className="flex">
                <div className="w-6/12 flex flex-col p-2 items-center">
                  <div className="dark:text-gray-2 text-black font-semibold mb-1 text-xs">
                    TOP BATTER
                  </div>
                  <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center">
                    <div className="overflow-hidden w-full h-full rounded-full ">
                      <img
                        className=" object-top object-contain w-24   "
                        style={{ objectPosition: '0 0%' }}
                        src={`https://images.cricket.com/players/${data.matchSummary.topPerformer.batsman.playerID}_headshot.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngs/fallbackprojection.png')
                        }
                        alt=""
                      ></img>
                    </div>
                  </div>

                  {/* matchSummary.topPerformer.bowler */}

                  <div className="w-full -mt-10 dark:bg-gray-4 bg-[#E2E2E2] text-center rounded-md px-2">
                    <div className="text-sm font-semibold mt-10 truncate dark:text-white text-black">
                      {data.matchSummary.topPerformer.batsman.playerName}
                    </div>
                    {data.matchSummary.topPerformer &&
                    props.matchType !== 'Test' ? (
                      <div>
                        <div className="flex flex-col items-center py-1">
                          {data.matchSummary.topPerformer.bowler && (
                            <div className="text-sm font-semibold truncate dark:text-white text-black">
                              <span className="f5 fw5 mr1">
                                {
                                data.matchSummary.topPerformer.batsman
                                    .playerMatchRuns
                                }
                                {/* {data.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''} */}
                              </span>
                              <span className="f6">
                                {' '}
                                (
                                {
                                data.matchSummary.topPerformer.batsman
                                    .playerMatchBalls
                                }
                                )
                              </span>
                            </div>
                          )}
                          <div className="flex items-center justify-between mt-2">
                            <div className="mr-1">
                              <span className="text-sm font-semibold truncate dark:text-white text-black">
                                4s{' '}
                              </span>
                              <span className="text-sm font-semibold truncate dark:text-white text-black">
                                {` `}
                                {
                                  data.matchSummary.topPerformer.batsman
                                    .playerMatchFours
                                }
                              </span>
                            </div>
                            <div className="ml-1 border-l">
                              <span className="f text-sm font-semibold truncate dark:text-white text-black ml-2">
                                6s
                              </span>
                              <span className="text-sm font-semibold truncate dark:text-white text-black">
                                {` `}
                                {
                                  data.matchSummary.topPerformer.batsman
                                    .playerMatchSixes
                                }
                              </span>
                            </div>
                          </div>
                        </div>
                      
                      </div>
                    ) : (
                      <div>
                        <div className="flex flex-col items-center py-1">
                          {data.matchSummary.topPerformer.bowler && (
                            <div className="text-sm font-semibold truncate dark:text-white text-black">
                              <span className="f5 fw5 mr1 ">
                                {
                                data.matchSummary.topPerformer.batsman
                                    .playerMatchRuns
                                }
                                {/* {data.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''} */}
                              </span>
                              <span className="f6 font-thin">
                                {' '}
                                (
                                {
                                data.matchSummary.topPerformer.batsman
                                    .playerMatchBalls
                                }
                                )
                              </span>
                            </div>
                          )}
                          <div className="flex items-center justify-between mt-2">
                            <div className="mr-1">
                              <span className="text-sm font-thin truncate dark:text-white text-black">
                                4s{' '}
                              </span>
                              <span className="text-sm font-semibold truncate dark:text-white text-black">
                                {` `}
                                {
                                  data.matchSummary.topPerformer.batsman
                                    .playerMatchFours
                                }
                              </span>
                            </div>
                            <div className="ml-1 border-l">
                              <span className="f text-sm font-thin truncate dark:text-white text-black ml-2">
                                6s
                              </span>
                              <span className="text-sm font-semibold truncate dark:text-white text-black">
                                {` `}
                                {
                                  data.matchSummary.topPerformer.batsman
                                    .playerMatchSixes
                                }
                              </span>
                            </div>
                          </div>
                        </div>
                      
                      </div>
                    )}
                  </div>
                </div>

                {data.matchSummary.topPerformer && props.matchType !== 'Test' ? (
                  <div className="w-6/12 flex flex-col p-2 items-center">
                    <div className="dark:text-gray-2 text-black font-semibold mb-1 text-xs">
                      TOP BOWLER
                    </div>
                    <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                      <div className="overflow-hidden w-full h-full rounded-full ">
                        <img
                          className=" object-top object-contain w-24  rounded-full  "
                          src={`https://images.cricket.com/players/${data.matchSummary.topPerformer.bowler.playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngs/fallbackprojection.png')
                          }
                          alt=""
                        ></img>
                      </div>
                    </div>

                    <div className="w-full -mt-10 dark:bg-gray-4 text-center bg-[#E2E2E2] rounded-md px-2">
                      <div className="text-base font-semibold mt-10 dark:text-white text-black truncate">
                        {'sss' === 'HIN'
                          ? data.matchSummary.bestBowler.playerNameHindi
                          :data.matchSummary.topPerformer.bowler.playerName}
                      </div>
                      <div className="flex flex-col  items-center justify-between dark:text-white text-black">
                        {data.matchSummary.topPerformer.bowler && (
                          <div className="text-base">
                            <span className="font-bold dark:text-white text-black">
                              {
                                data.matchSummary.topPerformer.bowler
                                  .playerWicketsTaken
                              }
                              /
                              {
                                data.matchSummary.topPerformer.bowler
                                  .playerRunsConceeded
                              }
                            </span>
                            <span className="f6 text-xs font-thin">
                              (
                              {
                                data.matchSummary.topPerformer.bowler.playerOversBowled
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                      <div className="text-center pb-2">
                        <span className="dark:text-white  text-xs font-thin">
                          Eco :
                        </span>
                        <span className="dark:text-white text-black text-xs font-semibold ml-1">
                          {data &&
                          data.matchSummary.topPerformer.bowler.playerOversBowled &&
                          data.matchSummary.topPerformer.bowler.playerEconomyRate}
                        </span>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="w-6/12 flex flex-col p-2 items-center">
                  <div className="dark:text-gray-2 text-black font-semibold mb-1 text-xs">
                    TOP BOWLER
                  </div>
                  <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                    <div className="overflow-hidden w-full h-full rounded-full ">
                      <img
                        className=" object-top object-contain w-24  rounded-full  "
                        src={`https://images.cricket.com/players/${data.matchSummary.topPerformer.bowler.playerID}_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngs/fallbackprojection.png')
                        }
                        alt=""
                      ></img>
                    </div>
                  </div>

                  <div className="w-full -mt-10 dark:bg-gray-4 text-center bg-[#E2E2E2] rounded-md px-2">
                    <div className="text-sm font-semibold mt-10 dark:text-white text-black truncate">
                      {'sss' === 'HIN'
                        ? data.matchSummary.bestBowler.playerNameHindi
                        :data.matchSummary.topPerformer.bowler.playerName}
                    </div>
                    <div className="flex flex-col  items-center justify-between dark:text-white text-black">
                      {data.matchSummary.topPerformer.bowler && (
                        <div className="text-sm">
                          <span className="font-bold dark:text-white text-black">
                            {
                              data.matchSummary.topPerformer.bowler
                                .playerWicketsTaken
                            }
                            /
                            {
                              data.matchSummary.topPerformer.bowler
                                .playerRunsConceeded
                            }
                          </span>
                          <span className="f7 text-xs font-thin">
                            (
                            {
                              data.matchSummary.topPerformer.bowler.playerOversBowled
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                    <div className="text-center pb-2">
                      <span className="dark:text-white  text-xs font-thin">
                        Eco :
                      </span>
                      <span className="dark:text-white text-black text-xs font-semibold ml-1">
                        {data &&
                        data.matchSummary.topPerformer.bowler.playerOversBowled &&
                        data.matchSummary.topPerformer.bowler.playerEconomyRate}
                      </span>
                    </div>
                  </div>
                </div>
                )}
              </div>
            

              <div className="flex flex-col items-center justify-center mx-3">
                {data.matchSummary.innings.map((item, index) => {
                  return (
                    <>
                      <div className="w-full flex items-center justify-between my-2 dark:bg-gray-4 bg-[#E2E2E2] p-2">
                        <div className="flex items-center justify-center">
                          <img
                            className="w-12 h-8 rounded object-cover object-top"
                            src={`https://images.cricket.com/teams/${item?.score?.battingTeamID}_flag_safari.png`}
                            alt=""
                            onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          />
                          <span className="ml-2 dark:text-white text-black text-sm font-semibold">
                            {item.score.battingTeamName}
                          </span>
                        </div>
                        <div className="flex text-base font-bold dark:text-white text-black">
                          {' '}
                          {item.score.runsScored}/{item.score.wickets}{' '}
                          <span className="text-sm ml-1 font-normal">
                            {'('}
                            {item.score.overs} {')'}
                          </span>{' '}
                        </div>
                      </div>
                      <div className="w-full flex flex-row  items-center justify-start my-2 ">
                        <div className=" flex-col flex items-center justify-start w-6/12  text-xs text-gray-2 border-r border-black">
                          <div className="flex items-center justify-start w-full py-2 font-bold">
                            BATTER
                          </div>

                          {item.battingList.map((itemInner, indexInner) => {
                            return (
                              <>
                                <div className="flex items-center justify-start w-full text-sm   py-1 text-white font-bold">
                                  {itemInner.playerName}
                                </div>
                                <div className="flex items-center justify-start w-full text-sm  py-1 text-white font-bold ">
                                  {itemInner.playerMatchRuns} {'('}
                                  {itemInner.playerMatchBalls}
                                  {')'}{' '}
                                </div>
                              </>
                            )
                          })}
                        </div>

                        <div className="flex flex-col items-center justify-end w-6/12 text-gray-2 text-xs">
                          <div className="flex items-center justify-end py-2 font-bold w-full ">
                            BOWLER
                          </div>

                          {item.bowlingList.map((itemInner, indexInner) => {
                            return (
                              <>
                                <div className="flex items-center  justify-end w-full text-sm  truncate  py-1 text-white font-bold">
                                  {itemInner.playerName}
                                </div>
                                <div className="flex items-center justify-end w-full text-sm  py-1 text-white font-bold">
                                  {itemInner.playerWicketsTaken}/
                                  {itemInner.playerRunsConceeded}{' '}
                                </div>
                              </>
                            )
                          })}
                        </div>
                      </div>
                    </>
                  )
                })}{' '}
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}
}
