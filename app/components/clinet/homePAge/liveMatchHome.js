import React, { useState } from 'react'

import {
  HOME_LIVE_MATCH_CRICLYTICS,
  MATCH_DATA_FOR_SCORECARD,
} from '../../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import PlayerIndex from '../homePAge/playerIndex'
import PlayerImpact from './playerImpact'
// import Liveballs from '.'
// import ContentPage from './contentPage';

// import HomeageFrcTeam from './homepageTeam';
// import RunComparisonCompleted from '../../crcltics/swipeCrclytics/homeRunComparisonComplted'
import MatchReel from './matchReel'
import HomePageMain from '../../servercomponent/hompage/homepageMain'
import ContentPage from './contentPage'
import HomeageFrcTeam from './homepageTeam'
import RunComparisonCompleted from '../../crcltics/swipeCrclytics/homeRunComparisonComplted'
import Liveballs from '../../liveballs'
import ScorecardTab from '../../match-score/matchDetails/matchLiveTabHB'
import InngBreak from './inningBreak'
import PhasesOfSession from './phasesofIng'
export default function Livemtahc({ ...props }) {
  const [teamData, setMyTeam] = useState([])
  const { loading, error, data } = useQuery(HOME_LIVE_MATCH_CRICLYTICS, {
    pollInterval: 5000,
    variables: { matchID: props.matchID },

    onCompleted: (algo11) => {
      if (
        algo11 &&
        algo11.liveMatchHomepageData &&
        algo11.liveMatchHomepageData.cricketDotComTeam
      ) {
        setMyTeam([
          ...(algo11.liveMatchHomepageData.cricketDotComTeam.batsman || []),
          ...(algo11.liveMatchHomepageData.cricketDotComTeam.all_rounder || []),
          ...(algo11.liveMatchHomepageData.cricketDotComTeam.bowler || []),
          ...(algo11.liveMatchHomepageData.cricketDotComTeam.keeper || []),
        ])
      }
    },
  })

  return (
    <div className="flex flex-col w-full">
      {props.tabContent == 'selectMatch' && (
        <>
       
           {((props.mainData &&
            props.mainData.statusMessage.includes('Innings Break')) ||
            props.mainData.statusMessage.includes('Match Tied') ||
            props.mainData.statusMessage.includes('Lunch') ||
            props.mainData.statusMessage.includes('Tea') ||
            props.mainData.statusMessage.includes('Stumps') ||
            props.mainData.statusMessage.includes('Dinner Break')) ?"": <Liveballs
           statusMessage={ props.mainData.statusMessage}
            data2={props.data2}
              matchID={props.matchID}
              matchStatus={props.matchStatus}
              mainData={props.mainData}
              currentinningsNo={props.mainData.currentinningsNo}
            />

      }
           {((props.mainData &&
            props.mainData.statusMessage.includes('Innings Break')) ||
            props.mainData.statusMessage.includes('Match Tied') ||
            props.mainData.statusMessage.includes('Lunch') ||
            props.mainData.statusMessage.includes('Tea') ||
            props.mainData.statusMessage.includes('Stumps') ||
            props.mainData.statusMessage.includes('Dinner Break')) ? '':  <ScorecardTab
              seriesName
              matchStatus={props.matchStatus}
              matchData={{ matchID: props.matchID }}
              mainData={props.mainData}
              data={data && data.liveMatchHomepageData}
            />
         
      }
           

          {((props.mainData &&
            props.mainData.statusMessage.includes('Innings Break')) ||
            props.mainData.statusMessage.includes('Match Tied') ||
            props.mainData.statusMessage.includes('Lunch') ||
            props.mainData.statusMessage.includes('Tea') ||
            props.mainData.statusMessage.includes('Stumps') ||
            props.mainData.statusMessage.includes('Dinner Break')) && (
            
              <InngBreak
                seriesName={props.seriesName}
                matchID={props.matchID}
                matchType={props.matchType}
                matchStatus={props.matchStatus}
                homePageLive={true}
                homePageHeading={true}
                title={'Match Reel'}
              />
          )}
           <MatchReel
              matchStatus={props.matchStatus}
              seriesName={props.seriesName}
              matchID={props.matchID}
              homePageLive={true}
              homePageHeading={true}
              title={'Match Reel'}
            />
          {props.matchType == 'Test' && (
            
              <PhasesOfSession
                seriesName={props.seriesName}
                matchID={props.matchID}
                matchType={props.matchType}
                matchStatus={props.matchStatus}
                days={props.mainData && props.mainData.currentDay}
                mainData={props.mainData}
              />
          )}

          {props.matchType !== 'Test' && (
           
              <RunComparisonCompleted
                seriesName={props.seriesName}
                matchID={props.matchID}
                matchType={props.matchType}

                matchStatus={props.matchStatus}
                mainData={props.mainData}
                data={data && data.liveMatchHomepageData}
              />
          )}

         { data?.liveMatchHomepageData?.cricketDotComTeam?.teamtotalpoints && 
            <HomeageFrcTeam
              seriesName={props.seriesName}
              mainData={props.mainData}
              
              matchID={props.matchID}
              score={
                data &&
                data.liveMatchHomepageData.cricketDotComTeam.teamtotalpoints
              }
              teamData={teamData}
              matchStatus={'live'}
              teamName={
                data &&  data.completedMatchHomepageData&&   data.completedMatchHomepageData.cricketDotComTeam &&
                data.completedMatchHomepageData.cricketDotComTeam
                  .fantasy_teamName
              }
            />
      }

      
        </>
      )}

      {/* {props.tabContent == 'selectContent' && (
        <>
          <div className="flex flex-col">
            {' '}
            <ContentPage matchID={props.matchID} />
          </div>
        </>
      )} */}
    </div>
  )
}
