import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { LIVE_SCORE_PREDICTOR } from '../../../api/queries'
import Link from 'next/link'
import Slider from 'rc-slider'
import Tooltip from 'rc-tooltip'
import Heading from '../../commom/heading'
const Handle = Slider.Handle
const play = '/pngsV2/playcric.png'
const pause = '/pngsV2/pausebutton.png'
const empty = '/pngsV2/empty.png'

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value + 1}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  )
}

export default function teamScoreProjection(props) {
  const [maxRun, setMaxRun] = useState()
  const [index, setIndex] = useState(0)
  const [button, setbutton] = useState(false)
  const [click, setclick] = useState(false)

  const { loading, error, data } = useQuery(LIVE_SCORE_PREDICTOR, {
    variables: { matchId: props.matchID, matchType: props.matchType },
    // pollInterval: 10000,
    onCompleted: (data) => {
      if (data.liveScorePredictor.liveScores.length > 0) {
        setIndex(data.liveScorePredictor.liveScores.length - 1)
        setMaxRun(
          Math.max(
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].predictedScore,
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].secondPredictedScore,
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].thirdPredictedScore,
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].fourthPredictedScore,
          ),
        )
      }
    },
  })
  let mytime = 0
  click &&
    (mytime = setTimeout(function () {
      index === data.liveScorePredictor.liveScores.length - 1
        ? (clearTimeout(mytime), setclick(!click), setbutton(false))
        : setIndex(index + 1)
    }, 500))
  //
  if (data) {
    console.log(' alert(8) alert(8) alert(8)', data)
    return data &&
      data.liveScorePredictor &&
      data.liveScorePredictor.liveScores &&
      data.liveScorePredictor.liveScores.length ? (
      <div className={` ${props.matchStatus === 'live' ? 'm-2 ' : 'm-2 my-5'}`}>
        <div className="w-full">
          {!props.homePageHeading && (
            <>
              {props.matchStatus === 'completed' ? (
                <div className="mx-3 md:my-2">
                  <Heading
                    heading={'Match Reel'}
                    subHeading={
                      'A fast-tracked reel of how the fortunes of the teams shifted through the game.'
                    }
                  />

                  {/* <div className='text-md  text-white tracking-wide font-bold '> Match Reel </div>
            <div className='text-xs text-white pt-1 font-medium tracking-wide'>
              {' '}
              An over by over breakdown of projected scores during the match{' '}
            </div>
            <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
                </div>
              ) : (
                <div className="mx-3">
                  <Heading
                    heading={'Team Score Projection'}
                    subHeading={
                      'A prediction of final scores if the match that also shows how fortunes of the teams have swung until this moment'
                    }
                  />
                  {/* <div className='text-md  text-white tracking-wide font-bold '> Team Score Projection </div>
            <div className='text-xs text-white pt-1 font-medium tracking-wide'>
              {' '}
              2A prediction of final scores if the match that also shows how fortunes of the teams have swung until this moment{' '}
            </div>
            <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
                </div>
              )}
            </>
          )}

          <div className="flex flex-col bg-gray rounded-md  pb-4">
            <div className="flex mx-2 py-2 ">
              {!props.homePageHeading || props.homePageLive ? (
                <div className="flex    py  items-center justify-between w-full">
                  <Heading heading={'Team Score Projection'} hideBottom />
                  <div className="flex justify-center rounded  bg-gray-4 p-1  items-center  ">
                    <Link
                      href={`/criclytics/${props.matchID}/${
                        props.seriesName &&
                        props.seriesName
                          .replace(/[^a-zA-Z0-9]+/g, ' ')
                          .split(' ')
                          .join('-')
                          .toLowerCase()
                      }/score-projection`}
                    >
                      <img
                        className="w-8 h-8 p-1"
                        src="/pngsV2/arrow.png"
                        alt=""
                      />
                    </Link>
                  </div>
                </div>
              ) : (
                <div className="flex    py  items-center justify-between w-full">
                  <Heading heading={'Match Reel'} hideBottom />
                  <div className="flex justify-center rounded  bg-basebg  py-1.5 px-2  items-center  ">
                    <Link
                      href={`/criclytics/${props.matchID}/${
                        props.seriesName &&
                        props.seriesName
                          .replace(/[^a-zA-Z0-9]+/g, ' ')
                          .split(' ')
                          .join('-')
                          .toLowerCase()
                      }/match-reel`}
                    >
                      <img className="w-6 h-6" src="/pngsV2/arrow.png" alt="" />
                    </Link>
                  </div>
                </div>
              )}
            </div>

            {data &&
            data.liveScorePredictor &&
            data.liveScorePredictor.liveScores &&
            data.liveScorePredictor.liveScores.length > 0 ? (
              <div
                className={
                  !props.homePageHeading
                    ? `flex-col dark:bg-gray lg:w-9/12 bg-white lg:border-2 lg:border-solid lg:border-[#E2E2E2]   py-3  rounded-md text-white`
                    : `dark:bg-gray w-full bg-white lg:border-2 lg:border-solid lg:border-[#E2E2E2]   px-3  rounded-md text-white`
                }
              >
                <div>
                  {/* <p className="font-semibold text-md tracking-wide md:text-xl ld:text-xl ">
                {(!props.homePageHeading || props.homePageLive) ?"Team Score Projection":"Team Score Projection"}
                </p> */}

                  {props.matchStatus !== 'live' && (
                    <div className="flex justify-center dark:justify-between items-center mt-4">
                      <div className="flex items-center">
                        <img
                          className="dark:w-8 w-20 dark:h-5 h-14 rounded-sm"
                          src={`https://images.cricket.com/teams/${
                            data.liveScorePredictor.liveScores &&
                            data.liveScorePredictor.liveScores.length > 0 &&
                            data.liveScorePredictor.liveScores[index] &&
                            data.liveScorePredictor.liveScores[index].team1Id
                          }_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/flag_dark.png')
                          }
                        />
                        <p className="pl-2 text-md font-semibold">
                          {data.liveScorePredictor.liveScores &&
                            data.liveScorePredictor.liveScores.length > 0 &&
                            data.liveScorePredictor.liveScores[index] &&
                            data.liveScorePredictor.liveScores[index]
                              .team1ShortName}
                        </p>
                      </div>
                      <div className="w-20 h-0.5 bg-basebg relative object-center flex justify-center items-center">
                        <div className="dark:w-6 dark:h-6 w-9 h-9 bg-basebg  rounded-full text-xs font-semibold flex items-center justify-center">
                          vs
                        </div>
                      </div>
                      <div className="flex items-center">
                        <p className="pr-2 text-md font-semibold">
                          {data.liveScorePredictor.liveScores &&
                            data.liveScorePredictor.liveScores.length > 0 &&
                            data?.liveScorePredictor?.liveScores[index]
                              ?.team2ShortName}
                        </p>
                        <img
                          className="dark:w-8 w-20 dark:h-5 h-14 rounded-sm text-md "
                          src={`https://images.cricket.com/teams/${
                            data?.liveScorePredictor?.liveScores &&
                            data?.liveScorePredictor?.liveScores?.[index]
                              ?.team2Id &&
                            data?.liveScorePredictor?.liveScores?.[index]
                              ?.team2Id
                          }_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/flag_dark.png')
                          }
                        />
                      </div>
                    </div>
                  )}

                  {props.matchStatus !== 'live' &&
                    data.liveScorePredictor?.liveScores[index] &&
                    data.liveScorePredictor.liveScores?.[index]?.inningIds &&
                    data.liveScorePredictor.liveScores?.[index]?.inningIds
                      ?.length == 2 && (
                      <div className="mt-4 flex justify-between items-center">
                        <div className="flex items-center text-md font-medium">
                          <p className="pr-2">
                            {data.liveScorePredictor?.liveScores?.[index]
                              ?.inningNo == 1 &&
                            data?.liveScorePredictor?.liveScores?.[index]
                              ?.inningIds[0] ==
                              data.liveScorePredictor.liveScores[index].team1Id
                              ? data.liveScorePredictor.liveScores[index]
                                  .currentScore +
                                '/' +
                                data.liveScorePredictor.liveScores[index]
                                  .currentWickets
                              : data?.liveScorePredictor?.liveScores?.[index]
                                  ?.predictedScore +
                                '/' +
                                data.liveScorePredictor?.liveScores?.[index]
                                  ?.predictedWicket}
                          </p>
                          <p className="font-normal text-sm">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == 1 &&
                            data.liveScorePredictor.liveScores?.[index]
                              ?.inningIds[0] ==
                              data.liveScorePredictor?.liveScores?.[index]
                                .team1Id
                              ? '(' +
                                data.liveScorePredictor?.liveScores?.[index]
                                  .currentOvers +
                                ')'
                              : '(' +
                                data?.liveScorePredictor?.liveScores?.[index]
                                  .predictedOver +
                                ')'}
                          </p>
                        </div>
                        <div
                          className={`${
                            data.liveScorePredictor.liveScores[index]
                              .inningNo == 1 &&
                            data.liveScorePredictor.liveScores[index]
                              .inningIds[0] ==
                              data.liveScorePredictor.liveScores[index].team1Id
                              ? 'text-gray-2 text-sm'
                              : 'text-white text-md'
                          }  font-medium flex items-center`}
                        >
                          <p className="pr-2">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == 1 &&
                            data.liveScorePredictor.liveScores[index]
                              .inningIds[0] ==
                              data.liveScorePredictor.liveScores[index].team1Id
                              ? 'Yet to Bat'
                              : data.liveScorePredictor.liveScores[index]
                                  .inningNo == 2 &&
                                data.liveScorePredictor.liveScores[index]
                                  .inningIds[1] ==
                                  data.liveScorePredictor.liveScores[index]
                                    .team2Id
                              ? data.liveScorePredictor.liveScores[index]
                                  .currentScore +
                                '/' +
                                data.liveScorePredictor.liveScores[index]
                                  .currentWickets
                              : data.liveScorePredictor.liveScores[index]
                                  .secondPredictedScore +
                                '/' +
                                data.liveScorePredictor.liveScores[index]
                                  .secondPredictedWicket}
                          </p>
                          <p className="font-normal text-sm">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == 1 &&
                            data.liveScorePredictor.liveScores[index]
                              .inningIds[0] ==
                              data.liveScorePredictor.liveScores[index].team1Id
                              ? ''
                              : data.liveScorePredictor.liveScores[index]
                                  .inningNo == 2 &&
                                data.liveScorePredictor.liveScores[index]
                                  .inningIds[1] ==
                                  data.liveScorePredictor.liveScores[index]
                                    .team2Id
                              ? '(' +
                                data.liveScorePredictor.liveScores[index]
                                  .currentOvers +
                                ')'
                              : '(' +
                                data.liveScorePredictor.liveScores[index]
                                  .secondPredictedOVer +
                                ')'}
                          </p>
                        </div>
                      </div>
                    )}

                  {/* --------------------------------------------------For more then 2 innings including test matches and superover--------------------------------- */}

                  {data.liveScorePredictor.liveScores[index] &&
                    data.liveScorePredictor.liveScores[index].inningIds &&
                    data.liveScorePredictor.liveScores[index].inningIds.length >
                      2 &&
                    props.matchType == 'Test' && (
                      <div className="mt-2 flex justify-between items-center">
                        <div className="flex items-center text-md font-medium">
                          <p className="pr-2">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == '1' ||
                            data.liveScorePredictor.liveScores[index]
                              .inningNo == '3'
                              ? data.liveScorePredictor.liveScores[index]
                                  .currentScore != ''
                                ? data.liveScorePredictor.liveScores[index]
                                    .currentScore +
                                  '/' +
                                  data.liveScorePredictor.liveScores[index]
                                    .currentWickets
                                : ''
                              : data.liveScorePredictor.liveScores[index]
                                  .inningNo == '2'
                              ? data.liveScorePredictor.liveScores[index]
                                  .predictedScore != '--'
                                ? data.liveScorePredictor.liveScores[index]
                                    .predictedScore +
                                  '/' +
                                  data.liveScorePredictor.liveScores[index]
                                    .predictedWicket
                                : ''
                              : data.liveScorePredictor.liveScores[index]
                                  .thirdPredictedScore != '--'
                              ? data.liveScorePredictor.liveScores[index]
                                  .thirdPredictedScore +
                                '/' +
                                data.liveScorePredictor.liveScores[index]
                                  .thirdPredictedWicket
                              : ''}
                          </p>
                          <p className="font-normal text-sm">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == '1' ||
                            data.liveScorePredictor.liveScores[index]
                              .inningNo == '3'
                              ? data.liveScorePredictor.liveScores[index]
                                  .currentOvers != ''
                                ? '(' +
                                  data.liveScorePredictor.liveScores[index]
                                    .currentOvers +
                                  ')'
                                : ''
                              : data.liveScorePredictor.liveScores[index]
                                  .inningNo == '2'
                              ? '(' +
                                data.liveScorePredictor.liveScores[index]
                                  .predictedScore +
                                ')'
                              : '(' +
                                data.liveScorePredictor.liveScores[index]
                                  .thirdPredictedOver +
                                ')'}
                          </p>
                        </div>
                        <div className="text-gray-2 text-sm font-medium flex items-center">
                          <p className="pr-2">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == '1' ||
                            data.liveScorePredictor.liveScores[index]
                              .inningNo == '3'
                              ? 'Yet to Bat'
                              : data.liveScorePredictor.liveScores[index]
                                  .inningNo == '2' ||
                                data.liveScorePredictor.liveScores[index]
                                  .inningNo == '4'
                              ? data.liveScorePredictor.liveScores[index]
                                  .currentScore
                                ? data.liveScorePredictor.liveScores[index]
                                    .currentScore +
                                  '/' +
                                  data.liveScorePredictor.liveScores[index]
                                    .currentWickets
                                : ''
                              : ''}
                          </p>
                          <p className="font-normal text-sm">
                            {data.liveScorePredictor.liveScores[index]
                              .inningNo == '1' ||
                            data.liveScorePredictor.liveScores[index]
                              .inningNo == '3'
                              ? ''
                              : data.liveScorePredictor.liveScores[index]
                                  .inningNo == '2' ||
                                data.liveScorePredictor.liveScores[index]
                                  .inningNo == '2'
                              ? '(' +
                                data.liveScorePredictor.liveScores[index]
                                  .currentOvers +
                                ')'
                              : ''}
                          </p>
                        </div>
                      </div>
                    )}

                  <div className="lg:flex lg:justify-between lg:gap-6 lg:mx-4">
                    {data?.liveScorePredictor?.liveScores?.[index]?.inningNo <=
                      2 && (
                      <div className="dark:bg-gray-4 bg-[#EEEFF2] p-2 py-3 lg:pb-8 mt-3 rounded-md lg:w-6/12">
                        <p className="text-gray-2 text-xs font-medium">
                          PROJECTED SCORES
                        </p>
                        {data.liveScorePredictor.liveScores?.[index]
                          ?.inningNo == '1' &&
                          data?.liveScorePredictor?.liveScores[index]
                            .predictedScore != '--' && (
                            <div className="mt-3 flex items-center justify-between w-100 lg:mt-4 text-xs font-medium">
                              <div className="w-2/12">
                                {
                                  data.liveScorePredictor.liveScores[index]
                                    .team1ShortName
                                }
                              </div>
                              {/* <img
                                  className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm"
                                  src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team1Id}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = '/pngsV2/flag_dark.png')
                                  }
                                /> */}
                              <div className="w-9/12 overflow-hidden rounded-full">
                                <div
                                  className="bg-green rounded h-2"
                                  style={{
                                    width: `${
                                      (data.liveScorePredictor.liveScores[index]
                                        .predictedScore /
                                        (maxRun + 20)) *
                                      100
                                    }%`,
                                  }}
                                ></div>
                              </div>
                              <p className="text-sm font-medium tracking-wider lg:text-black">
                                {data.liveScorePredictor.liveScores[index]
                                  .predictedScore +
                                  '/' +
                                  data.liveScorePredictor.liveScores[index]
                                    .predictedWicket}
                              </p>
                            </div>
                          )}
                        {data.liveScorePredictor.liveScores[index].inningNo <=
                          2 &&
                          data.liveScorePredictor.liveScores[index]
                            .secondPredictedScore != '--' && (
                            <div className="mt-3 flex items-center justify-between w-full text-xs font-medium lg:mt-4">
                              <div className="w-2/12">
                                {
                                  data.liveScorePredictor.liveScores[index]
                                    .team2ShortName
                                }
                              </div>
                              {/* <img
                                  className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm"
                                  src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team2Id}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = '/pngsV2/flag_dark.png')
                                  }
                                /> */}
                              <div className="w-9/12 rounded-full overflow-hidden">
                                <div
                                  className={`${
                                    props.matchStatus == 'completed'
                                      ? 'bg-gray-2'
                                      : 'bg-gray-2'
                                  }  rounded h-2`}
                                  style={{
                                    width: `${
                                      (data.liveScorePredictor.liveScores[index]
                                        .secondPredictedScore /
                                        (maxRun + 20)) *
                                      100
                                    }%`,
                                  }}
                                ></div>
                              </div>
                              <p className="text-sm font-medium tracking-wider lg:text-black">
                                {data.liveScorePredictor.liveScores[index]
                                  .secondPredictedScore +
                                  '/' +
                                  data.liveScorePredictor.liveScores[index]
                                    .secondPredictedWicket}
                              </p>
                            </div>
                          )}
                      </div>
                    )}
                    {data.liveScorePredictor.liveScores[index] &&
                      data.liveScorePredictor.liveScores[index].inningIds &&
                      data.liveScorePredictor.liveScores[index].inningIds
                        .length > 2 && (
                        <div className="dark:bg-gray-4 bg-[#EEEFF2] p-2 py-3 mt-3 lg:pb-8 lg:w-6/12 rounded-md">
                          <p className="text-gray-2 text-xs font-medium">
                            PROJECTED SCORES
                          </p>
                          {data.liveScorePredictor.liveScores[index].inningNo <=
                            3 &&
                            data.liveScorePredictor.liveScores[index]
                              .thirdPredictedScore != '' && (
                              <div className="mt-3 flex items-center justify-between w-100 lg:mt-4 text-xs font-medium">
                                {data.liveScorePredictor.liveScores[index]
                                  .inningIds[2] ==
                                data.liveScorePredictor.liveScores[index]
                                  .team1Id
                                  ? data.liveScorePredictor.liveScores[index]
                                      .team1ShortName
                                  : data.liveScorePredictor.liveScores[index]
                                      .team2ShortName}
                                {/* <img
                                    className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm mr-1"
                                    src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team1Id}_flag_safari.png`}
                                    onError={(evt) =>
                                      (evt.target.src = '/pngsV2/flag_dark.png')
                                    }
                                  /> */}
                                <div className="w-9/12 overflow-hidden rounded-full">
                                  <div
                                    className="bg-green rounded h-2"
                                    style={{
                                      width: `${
                                        (data.liveScorePredictor.liveScores[
                                          index
                                        ].thirdPredictedScore /
                                          (maxRun + 20)) *
                                        100
                                      }%`,
                                    }}
                                  ></div>
                                </div>
                                <p
                                  className="text-sm font-medium tracking-wider lg:text-black text-right"
                                  style={{ width: '15%' }}
                                >
                                  {data.liveScorePredictor.liveScores[index]
                                    .thirdPredictedScore +
                                    '/' +
                                    data.liveScorePredictor.liveScores[index]
                                      .thirdPredictedWicket}
                                </p>
                              </div>
                            )}
                          {data.liveScorePredictor.liveScores[index].inningNo <=
                            4 &&
                            data.liveScorePredictor.liveScores[index]
                              .fourthPredictedScore != '' &&
                            data.liveScorePredictor.liveScores[index].inningIds
                              .length > 3 && (
                              <div className="mt-3 flex items-center justify-between w-100 lg:mt-4 text-xs font-medium">
                                {data.liveScorePredictor.liveScores[index]
                                  .inningIds[3] ==
                                data.liveScorePredictor.liveScores[index]
                                  .team1Id
                                  ? data.liveScorePredictor.liveScores[index]
                                      .team1ShortName
                                  : data.liveScorePredictor.liveScores[index]
                                      .team2ShortName}
                                {/* <img
                                    className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm mr-1"
                                    src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team2Id}_flag_safari.png`}
                                    onError={(evt) =>
                                      (evt.target.src = '/pngsV2/flag_dark.png')
                                    }
                                  /> */}
                                <div className="w-9/12">
                                  <div
                                    className="bg-gray-2 rounded h-2"
                                    style={{
                                      width: `${
                                        (data.liveScorePredictor.liveScores[
                                          index
                                        ].fourthPredictedScore /
                                          (maxRun + 20)) *
                                        100
                                      }%`,
                                    }}
                                  ></div>
                                </div>
                                <p
                                  className="text-sm font-medium tracking-wider lg:text-black text-right"
                                  style={{ width: '15%' }}
                                >
                                  {data.liveScorePredictor.liveScores[index]
                                    .fourthPredictedScore +
                                    '/' +
                                    data.liveScorePredictor.liveScores[index]
                                      .fourthPredictedWicket}
                                </p>
                              </div>
                            )}
                        </div>
                      )}
                    <div className="dark:bg-gray-4 bg-[#EEEFF2] p-2 lg:pb-12 py-3 mt-3 lg:w-6/12 rounded-md">
                      <p className="text-gray-2 text-xs font-medium">
                        PROJECTED RESULT
                      </p>
                      <p
                        className={`${
                          props.matchStatus === 'completed'
                            ? 'text-blue'
                            : 'text-green'
                        } dark:mt-1 font-semibold text-sm mt-4`}
                      >
                        {
                          data?.liveScorePredictor?.liveScores?.[index]
                            ?.projected_result
                        }
                      </p>
                    </div>
                  </div>
                </div>
                {!props.homePageLive && (
                  <div className="mt-2">
                    {data.liveScorePredictor.liveScores.length > 0 &&
                      data.liveScorePredictor.liveScores[
                        data.liveScorePredictor.liveScores.length - 1
                      ].winvizView !== null && (
                        <div>
                          <div className="flex-l lg:mx-4 lg:mt-8 justify-between items-center ">
                            <div className=" w-50-l">
                              {data?.liveScorePredictor?.liveScores?.[index]
                                ?.currentView && (
                                <>
                                  <div className="text-gray-2 uppercase text-xs font-medium my-2">
                                    Win percentage
                                  </div>

                                  <div className="flex pv1 justify-center items-center ">
                                    <div
                                      style={{
                                        width: `${
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView &&
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView.homeTeamPercentage
                                        }%`,
                                        height: 10,
                                      }}
                                      className={` dib bg-green rounded-l ${
                                        data.liveScorePredictor.liveScores[
                                          index
                                        ].currentView.homeTeamPercentage == 100
                                          ? 'rounded-r'
                                          : ''
                                      }`}
                                    ></div>
                                    {data.liveScorePredictor.liveScores[index]
                                      .currentView &&
                                    parseInt(
                                      data.liveScorePredictor.liveScores[index]
                                        .currentView.tiePercentage,
                                    ) > 0 ? (
                                      <div
                                        className=" dib bg-gray-3"
                                        style={{
                                          width: `${data.liveScorePredictor.liveScores[index].currentView.tiePercentage}%`,
                                          height: 10,
                                        }}
                                      />
                                    ) : (
                                      <div />
                                    )}
                                    <div
                                      style={{
                                        width: `${
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView &&
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView.awayTeamPercentage
                                        }%`,
                                        height: 10,
                                      }}
                                      className={`dib bg-white rounded-r ${
                                        data.liveScorePredictor.liveScores[
                                          index
                                        ].currentView.awayTeamPercentage == 100
                                          ? 'rounded-l'
                                          : ''
                                      }`}
                                    ></div>
                                  </div>
                                  <div className="flex flex-row justify-between font-semibold text-xs text-gray-2 pt-1">
                                    <div className="flex items-center">
                                      <div className="h-1.5 w-1.5 rounded-full bg-green"></div>
                                      <div className="px-1">
                                        {data.liveScorePredictor.liveScores[
                                          index
                                        ].currentView &&
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView.homeTeamShortName}
                                      </div>
                                      <div className="tracking-wider font-medium">
                                        (
                                        {
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView.homeTeamPercentage
                                        }
                                        %)
                                      </div>
                                    </div>
                                    {data.liveScorePredictor.liveScores[index]
                                      .currentView &&
                                      data.liveScorePredictor.liveScores[index]
                                        .currentView.tiePercentage &&
                                      parseInt(
                                        data.liveScorePredictor.liveScores[
                                          index
                                        ].currentView.tiePercentage,
                                      ) > 0 && (
                                        <div className="flex items-center ">
                                          <div className="h-1.5 w-1.5 rounded-full bg-gray-3"></div>

                                          {props.matchType === 'Test' ? (
                                            <div className="px-1">DRAW</div>
                                          ) : (
                                            <div className="px-1">Tie</div>
                                          )}
                                          <div className="tracking-wider font-medium">
                                            (
                                            {
                                              data.liveScorePredictor
                                                .liveScores[index].currentView
                                                .tiePercentage
                                            }
                                            %)
                                          </div>
                                        </div>
                                      )}
                                    <div className="flex items-center">
                                      <div className="h-1.5 w-1.5 rounded-full bg-white"></div>

                                      <div className="px-1">
                                        {data.liveScorePredictor.liveScores[
                                          index
                                        ].currentView &&
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView.awayTeamShortName}
                                      </div>
                                      <div className="tracking-wider font-medium">
                                        (
                                        {
                                          data.liveScorePredictor.liveScores[
                                            index
                                          ].currentView.awayTeamPercentage
                                        }
                                        %)
                                      </div>
                                    </div>
                                  </div>
                                </>
                              )}
                            </div>
                          </div>

                          <div className="dark:pa2 lg:mt-6">
                            <div className="flex items-center justify-between justify-around-l ">
                              <img
                                src={button ? pause : play}
                                alt=""
                                className="h-12 md:cursor-pointer w-12 m-2"
                                onClick={() => (
                                  index ===
                                  data.liveScorePredictor.liveScores.length - 1
                                    ? setIndex(0)
                                    : '',
                                  setbutton(!button),
                                  setclick(!click)
                                )}
                              />
                              <div className="w-full pl-2 flex h-10 dark:bg-basebg bg-[#EEEFF2] justify-center items-center p-1 rounded">
                                <Slider
                                  className="slider-main w-full"
                                  max={
                                    data.liveScorePredictor.liveScores.length -
                                    1
                                  }
                                  min={0}
                                  step={1}
                                  value={index}
                                  onChange={(val) => {
                                    if (
                                      val !==
                                        data.liveScorePredictor.liveScores
                                          .length -
                                          1 &&
                                      val <=
                                        data.liveScorePredictor.liveScores
                                          .length -
                                          1 &&
                                      !button
                                    ) {
                                      setIndex(val)
                                    }
                                  }}
                                  handle={handle}
                                  handleStyle={[
                                    {
                                      backgroundColor: 'white',
                                      border: 'white',
                                      width: '18px',
                                      height: '18px',
                                      marginTop: '-8px',
                                      boxShadow:
                                        '0 2px 7px 0 rgba(162, 167, 177, 0.51)',
                                    },
                                  ]}
                                  trackStyle={[
                                    {
                                      backgroundColor: '#38d925',
                                      height: '5px',
                                    },
                                  ]}
                                  railStyle={{
                                    backgroundColor: 'gray',
                                    height: '5px',
                                  }}
                                />
                              </div>
                            </div>

                            <div className="tc py-2 f8 flex lg:text-gray-2 lg:gap-3 dark:justify-center justify-start items-center">
                              {props.matchType === 'Test' && (
                                <div className="">
                                  INN{' '}
                                  {
                                    data.liveScorePredictor.liveScores[index]
                                      .inningNo
                                  }{' '}
                                </div>
                              )}
                              <div className="px-2 text-xs lg:text-gray-2">
                                {data?.liveScorePredictor?.liveScores?.[index]
                                  ?.inningIds[
                                  data?.liveScorePredictor.liveScores?.[index]
                                    ?.inningNo - 1
                                ] ===
                                data.liveScorePredictor.liveScore?.[index]
                                  ?.team1Id
                                  ? data.liveScorePredictor.liveScores?.[index]
                                      ?.team1ShortName
                                  : data?.liveScorePredictor?.liveScores?.[
                                      index
                                    ]?.team2ShortName}{' '}
                                BAT{' '}
                              </div>
                              {data.liveScorePredictor.liveScores?.[index]
                                ?.overNo > 0 && (
                                <div className="text-xs ">
                                  {/* <div className="w-4 h-4 rounded bg-gray-4"></div> */}
                                  OVR{' '}
                                  {
                                    data.liveScorePredictor?.liveScores?.[index]
                                      ?.overNo
                                  }
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      )}
                  </div>
                )}
              </div>
            ) : (
              <div>
                <div className="w-full flex justify-center ">
                  <img className=" w-44" src={empty} />
                </div>
                <div className="text-center text-sm font-semibold pt-2 text-white">
                  Data not Available
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    ) : (
      <div />
    )
  }
}
