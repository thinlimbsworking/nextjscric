
'use client';
import { useState,useEffect,useRef } from "react";
import { useQuery, useMutation } from '@apollo/react-hooks';
import { SOCIAL_TRACKER ,ARTICLE_LIST} from '../../../api/queries';
import Script from 'next/script'
import Head from "next/head";
import parse from 'html-react-parser'
// import TweetEmbed from 'react-tweet-embed'
import SliderTab from '../../shared/Slidertab'
import { TwitterTweetEmbed } from "react-twitter-embed";
import Tab from "../../shared/Tab";
import Button from '../../shared/button'
import Heading from "../../shared/heading";
import Link from "next/link";
import { format } from 'date-fns'

export default function ClientComponent({children}) {


  let scrl = useRef(null);


  let scrl1 = useRef(null);
  const [scrollWidth, setScrollWidth] = useState(0);

  const [scrollX, setscrollX] = useState(0);
  const [scrollX1, setscrollX1] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  const [scrolEnd1, setscrolEnd1] = useState(false);
  const [windows, updateWindows] = useState()
  useEffect(() => {
    if (scrl&&scrl.current && scrl.current.clientWidth ) {
      setScrollWidth(scrl.current.clientWidth)
    }
   
  }, [])
  const slide = (shift) => {
   

    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft +shift,
      behavior: 'smooth',
    
    
  });
  setscrollX(scrollX + shift);

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };



  const { loading, error, data } = useQuery(ARTICLE_LIST, { variables: { type: 'onthisday', page: 0 }
  }
  )


  if(data)
  {
 
  return (<div className="flex flex-col  w-full ">
 

    
 
 <Heading heading={'On This Day'} noPad />
  <div className="flex items-center justify-center relative  pt-4">
  <div
 onClick={() =>  scrollX!==0 && scrollX>-1&& slide(+((-263)))}
                        
  className={ `${scrollX<90?'opacity-30':'opacity-100'}  flex -left-4   absolute cursor-pointer  bg-black rounded-full   item-center justify-center z-10 `}

              //  onClick={() => slide(+((-263)))}
                        
                            id='swiper-button-prev'
                            // className='flex  absolute -left-4 cursor-pointer  bg-black rounded-full   item-center justify-center z-10  white pointer dn db-ns z-999 outline-0'
                            >
                            <svg width='20' focusable='false' viewBox='0 0 24 24'>
                              <path fill='#fff' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                              <path fill='none' d='M0 0h24v24H0z'></path>
                            </svg>
                          </div>
 
  <div className="flex flex-col w-full">
 
 
 
 
  <div className= {` relative   flex  overflow-x-scroll w-full } `}   ref={scrl} onScroll={scrollCheck}>
 
 { data &&
           data.getArticles.map((item, i) =>  {
 return (
   <div className='  flex m-1  flex-col min-h-[200px] min-w-[250px]  shadow-md rounded-md overflow-hidden
 '> <Link href={`/news/${item.articleID}`}>
     <div className=' flex flex-col   w-full '>
   <div key={i} className=' h-44   '  >
     
       
         
         
             <img    className=' flex h-44 w-full items-center justify-center  object-cover object-top'
     src={`${item.featureThumbnail}?auto=compress&fit=crop&crop=faces,top&w=250&h=180`}
     alt='' />
       
   
     </div>
     <div className='flex w-full items-center justify-center   text-sm  font-semibold capitalize p-1 leading-6   '> 
     
    
     { format(+item?.createdAt, 'dd MMM yyyy')}
  
</div>
     <div className=' my-1 ml-2     text-sm leading-5 line-clamp-2 text-gray-9 font-semibold   '>
         {item.title}
     </div>
 
     </div>
     </Link>
     </div>
   
 );
 })}
   
     </div>
   
 </div>
 
 
 <div
                          
                          onClick={() =>  !scrolEnd&&  slide(((263)))}
                          id='swiper-button-prev'
                          className={`${scrolEnd?'opacity-40':'opacity-100'} white pointer absolute  bg-black rounded-full -right-5 flex  item-center justify-center cursor-pointer shadow-md  `}
                        
                            >
                             <svg width='20' viewBox='0 0 24 24'>
                              <path fill='#fff' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                              <path fill='none' d='M0 0h24v24H0z'></path>
                            </svg>
                          </div>
 
 
 
                </div>
                
               
              
               
              
 </div>
  );
}
}



