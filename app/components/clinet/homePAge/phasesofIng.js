import React, {useState, useEffect} from "react";
import {PHASES_OF_SESSIONS} from "../../../api/queries";
import {scheduleMatchView, getCriclyticsUrl} from "../../../api/services";
import {useQuery} from "@apollo/react-hooks";
import CleverTap from "clevertap-react";
import {format} from "date-fns";
import Link from "next/link";
import ScoringChart from "../../../../components/crcltics/scoringchart";
import Phaseng from "./phaseIntest";
import CommonDropdown from "../../commom/commonDropdown";
import Heading from "../../commom/heading";
// import { useEffect } from 'react/cjs/react.production.min';
export default function PhasesOfSession({browser, ...props}) {
  const [articlesFilter, setArticlesFilter] = useState([
    {valueLabel: "All", valueId: "All"},
    {valueLabel: "All2", valueId: "All2"},
    {valueLabel: "All3", valueId: "All3"},
  ]);
  const [activefilter, setActivefilter] = useState([
    {valueLabel: "All", valueId: "All"},
  ]);
  const [activeTab, setActiveTab] = useState([
    {valueLabel: "All", valueId: ""},
  ]);
  const {mainData} = props;
  console.log(mainData, "match data");
  const [currentSession, setCurrentSession] = useState();
  const [daySelection, setDaysSlection] = useState(1);
  const [dayShow, setDayShow] = useState(false);
  const [selectedDay, setSelectedDay] = useState(mainData.currentDay || 1);
  const [sessionTab, setSessionTab] = useState(0);

  const [yRange, setYRange] = useState(10);
  const [maxRun, setMaxRun] = useState(10);

  const {loading, error, data} = useQuery(PHASES_OF_SESSIONS, {
    variables: {matchID: props.matchID, day: selectedDay},
    onCompleted: (data) => {
      let tabValue =
        data?.phaseOfSessions?.phaseOfSession?.[
          data.phaseOfSessions.phaseOfSession.length - 1
        ]?.session - 1 ?? 0;
      setSessionTab(tabValue);
      let a = Math.max.apply(
        Math,
        data?.phaseOfSessions?.phaseOfSession?.[tabValue]?.overs.map((o) => {
          return o.runs;
        })
      );
      // console.log("tabvalue: ", tabValue, a);
      setMaxRun(a);
      setYRange(
        a < 8
          ? 10
          : a <= 13
          ? 15
          : a <= 18
          ? 20
          : a <= 23
          ? 25
          : a <= 28
          ? 30
          : a <= 33
          ? 35
          : a <= 38
          ? 40
          : 45
      );
      setCurrentSession(data.phaseOfSessions.phaseOfSession.length);
    },
    fetchPolicy: "network-only",
  });

  const updateSessionTab = (key) => {
    setSessionTab(key);
    // let a = Math.max.apply(
    //   Math,
    //   data.phaseOfSessions.phaseOfSession[key].overs.map((o) => {
    //     return o.runs;
    //   })
    // );
    let a = Math.max.apply(
      Math,
      data?.phaseOfSessions?.phaseOfSession
        .filter((o) => o.session == key + 1)?.[0]
        .overs.map((o) => {
          return o.runs;
        })
    );
    setMaxRun(a);
    setYRange(
      a < 8
        ? 10
        : a <= 13
        ? 15
        : a <= 18
        ? 20
        : a <= 23
        ? 25
        : a <= 28
        ? 30
        : a <= 33
        ? 35
        : a <= 38
        ? 40
        : 45
    );
  };
  const handleCriclyticsNav = () => {
    if (match.isCricklyticsAvailable) {
      CleverTap.initialize("Criclytics", {
        Source: "HomepageCriclytics",
        MatchID: match.matchID,
        SeriesID: match.seriesID,
        TeamAID: match.matchScore[0].teamID,
        TeamBID: match.matchScore[1].teamID,
        MatchFormat: match.matchType,
        MatchStartTime: format(Number(match.startDate), "do MMM yyyy, h:mm a"),
        MatchStatus: match.matchStatus,
        // CriclyticsEngine: "LiveTeam",
        Platform: localStorage ? localStorage.Platform : "",
      });
    }
  };

  const handleScorecardsNav = () => {
    CleverTap.initialize("CommentaryTab", {
      Source: "Homepage",
      MatchID: match.matchID,
      SeriesID: match.seriesID,
      TeamAID: match.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), "do MMM yyyy, h:mm a"),
      MatchStatus: match.matchStatus,
      Platform: localStorage ? localStorage.Platform : "",
    });
  };
  useEffect(() => {}, [dayShow]);

  if (error) return <div></div>;

  if (loading || !data) {
    return <div className="h-40"></div>;
  } else if (data) {
    return (
      data?.phaseOfSessions?.phaseOfSession?.length > 0 && (
        <div className="m-2">
          <div className="flex  flex-col bg-gray rounded-md p-2">
            {data.phaseOfSessions.phaseOfSession.length ? (
              <div className=" w-full relative   bg-gray  rounded-md ">
                <div className="flex p-2  items-center justify-between">
                  <Heading
                    hideBottom={true}
                    heading={"Run Comparison - Scoring Chart"}
                  />
                </div>

                <div className="flex bg-basebg mx-4  mb-2  w-4/12 rounded-full relative">
                  <div
                    className="w-full text-sm p-2 flex items-center justify-evenly "
                    onClick={() => setDayShow(true)}>
                    Day {selectedDay}
                    <img
                      className="h-2 w-3"
                      src={"/pngsV2/whitearrow_down.png"}
                    />
                  </div>
                  {dayShow && (
                    <div className="absolute w-full rounded-md bg-basebg shadow-2xl p-2 flex flex-col z-50 top-8">
                      {[...Array(Number(mainData.currentDay))].map((x, i) => (
                        <div
                          className="flex text-sm p-1 font-mnr "
                          onClick={() => (
                            setSelectedDay(i + 1), setDayShow(false)
                          )}>
                          Day {i + 1}
                        </div>
                      ))}
                    </div>
                  )}
                </div>

                <div className="overflow-x-scroll scroll pt-3 tc  mx-5 bg-basebg flex justify-around ">
                  {data.phaseOfSessions.phaseOfSession.map((x, i) => (
                    <div
                      key={i}
                      className={` pointer text-center text-xs w-full ${
                        sessionTab === i
                          ? "text-gray-3 border-b border-gray-3 text-blue-3   font-mnsb font-bold text-base"
                          : "text-gray-2 border-b border-gray-2 font-mnsb font-bold text-base"
                      }`}
                      style={{letterSpacing: 0.6}}
                      onClick={() =>
                        data.phaseOfSessions.phaseOfSession.filter(
                          (o) => o.session == i + 1
                        )[0]
                          ? updateSessionTab(i)
                          : false
                      }>{`Session${i + 1}`}</div>
                  ))}
                </div>

                {/*            
              <div className='abssolute  left-0 bottom-0  h-full  items-end  justify-end   z-50    flex flex-col '>

  <div className=' py-2 text-xs font-semibold'>5</div>
  <div className='py-2 text-xs font-semibold'>4</div>
  <div className='py-2 text-xs font-semibold'>4</div>
  <div className='py-2 text-xs font-semibold'>4</div>
  <div className='py-2 text-xs font-semibold'>4</div>
  </div> */}
                <div
                  className="flex flex-col text-gray-2 text-xs font-medium absolute -left-1 bottom-8"
                  style={{width: 22}}>
                  <div className="absolute -top-8"> runs</div>
                  {[1, 2, 3, 4, 5].map((val, i) => (
                    <div key={i} className="" style={{height: 38}}>
                      <div className="flex justify-end ">
                        {(yRange / 5) * (5 - i)}
                      </div>
                      <div
                        className={`${
                          i === 0 ? "" : ""
                        } -rotate-90 flex justify-end mt-2 ml-3`}></div>
                    </div>
                  ))}
                </div>

                <div className="flex justify-start relative items-end  bg-basebg mx-5 h-40 overflow-hidden ">
                  {data.phaseOfSessions.phaseOfSession[sessionTab] &&
                    data.phaseOfSessions.phaseOfSession[
                      sessionTab
                    ].overs.map((over, i) => (
                      <div key={i} className="flex items-center flex-col">
                        {Array.apply(null, {length: Number(over.wicket)}).map(
                          (x, i) => (
                            <div
                              key={i}
                              className="rounded-full h-2 w-2 bg-red mb-1"
                              style={{height: 6, width: 6}}
                            />
                          )
                        )}
                        <div
                          style={{
                            height: Number(over.runs) * 10,
                            width: 8,
                            marginLeft: "0.7px",
                            marginRight: "0.7px",
                            backgroundColor:
                              props?.mainData?.firstInningsTeamID ===
                              over.teamId
                                ? "#496780"
                                : "#38d925",
                          }}
                        />
                      </div>
                    ))}
                </div>
                <div className="flex ml-5 text-gray-2 text-xs">overs</div>

                {data.phaseOfSessions.phaseOfSession[sessionTab] && (
                  <div className="flex justify-between items-center px-3 py-2 bg-navy py2">
                    <div className="flex items-center justify-center">
                      <div className=" h-2 w-2 bg-gray-3 rounded-full mr-1"></div>
                      <span className="white f6 fw5 ml2 text-xs">
                        {/* {data.phaseOfSessions.phaseOfSession[currentSession - 1].totalOvers} */}
                        {props?.mainData?.matchScore &&
                          props?.mainData?.matchScore[0] &&
                          props?.mainData?.matchScore[0]?.teamScore && (
                            <span>
                              {props?.mainData?.matchScore[0]?.teamShortName}{" "}
                              {props.mainData.matchScore[0].teamScore?.[0]
                                ?.runsScored &&
                                props.mainData.matchScore[0].teamScore?.[0]
                                  ?.runsScored}{" "}
                              {"/"}
                              {props.mainData.matchScore[0].teamScore?.[0]
                                ?.runsScored &&
                                props.mainData.matchScore[0].teamScore?.[0]
                                  ?.wickets}
                              {" ("}
                              {props.mainData.matchScore[0].teamScore?.[0]
                                ?.runsScored &&
                                props.mainData.matchScore[0].teamScore?.[0]
                                  ?.overs}
                              {")"}{" "}
                              {console.log(
                                "check hereeeer",
                                props.mainData.matchScore[0].teamShortName
                              )}
                            </span>
                          )}
                      </span>
                    </div>
                    <div className="flex items-center justify-center">
                      <div className=" h-2 w-2 bg-green rounded-full mr-1"></div>
                      <span className="white f6 fw5 ml2 text-xs">
                        {/* {data.phaseOfSessions.phaseOfSession[currentSession - 1].totalOvers} */}
                        {props?.mainData?.matchScore &&
                          props?.mainData?.matchScore[1] &&
                          props?.mainData?.matchScore[1]?.teamScore && (
                            <span>
                              {props?.mainData?.matchScore[1]?.teamShortName}{" "}
                              {props?.mainData?.matchScore[1]?.teamScore
                                ?.length > 0 && (
                                <>
                                  {props?.mainData?.matchScore?.[1]
                                    ?.teamScore[0]?.runsScored &&
                                    props?.mainData?.matchScore?.[1]
                                      .teamScore[0]?.runsScored}{" "}
                                  {"/"}
                                  {props?.mainData?.matchScore?.[1]
                                    ?.teamScore[0]?.runsScored &&
                                    props?.mainData?.matchScore?.[1]
                                      ?.teamScore?.[0]?.wickets}
                                  {" ("}
                                  {props?.mainData?.matchScore?.[1]
                                    ?.teamScore[0]?.runsScored &&
                                    props?.mainData?.matchScore?.[1]
                                      ?.teamScore?.[0]?.overs}
                                  {")"}{" "}
                                </>
                              )}
                            </span>
                          )}
                      </span>
                    </div>
                    <div className="flex items-center ">
                      <div className="h-2 w-2 mr-1 bg-red rounded-full "></div>
                      <div className="text-gray-2 text-xs">Wickets </div>
                    </div>
                  </div>
                )}
              </div>
            ) : null}
          </div>
        </div>
      )
    );
  }
}
