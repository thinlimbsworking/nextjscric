import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import {
  PHASES_OF_PLAY,
  PLAYER_IMPACT,
  PLAYER_MATCHUPS,
} from '../../../api/queries'
import CleverTap from 'clevertap-react'
import DataNotavaible from '../../commom/datanotfound'
import Heading from '../../commom/heading'
import Tab from '../../shared/Tab'
import DataNotFound from '../../../components/commom/datanotfound'
import BatIcon from '../../commom/baticon'
import BowlIcon from '../../commom/bowlicon'
import Link from 'next/link'
const location = '/svgs/location-icon-color.png'
const information = '/svgs/information.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const ground = '/svgs/groundImageWicket.png'
const playerAvatar = '/placeHodlers/playerAvatar.png'
const bat = '/pngsV2/battericon.png'
const ball = '/pngsV2/bowlericon.png'
export default function MatchRating({ browser, ...props }) {
  const [tabActiveIndex, setActiveIndex] = useState(0)
  const [activeTab, setActiveTab] = useState({
    name: props.data && props.data.livePlayerImpact?.[0]?.teamName,
    value: props.data && props.data.livePlayerImpact?.[0]?.teamName,
    index: 0,
  })
  // const handleTabChange = (tab,index) => {
  //
  //   setActiveTab(activeTab)
  // }
  // useEffect(()=>{
  //   setActiveTab
  // },[activeTab])
  function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals).toFixed(
      decimals,
    )
  }
  return (
    props.data &&
    props.data.livePlayerImpact?.length > 0 &&
    props.data.livePlayerImpact?.[0]?.teamName && (
      <div className="flex flex-col bg-gray rounded-md relative mx-2 mt-2">
        {props.showPlayerIndex && (
          <div
            className="-top-[10rem] flex items-start justify-center  bottom-0 absolute"
            style={{ zIndex: 1000 }}
            onClick={() => props.setShowPlayerIndex(false)}
          >
            <div className="flex relative items-end justify-center">
              <div
                className="w-11/12 bg-gray-2  p-2 text-xs text-white rounded-md leading-5 popup-container"
                onClick={() => props.setShowPlayerIndex(false)}
              >
                It measures a player's contribution per ball towards their
                team's win in a match. The impact is based on batting and
                bowling and factors such as required and overall run rates, eco
                rate and strike rate. An impact score greater than 0 signifies a
                positive contribution, while a negative score indicates the
                opposite. Impact scores vary across matches.
              </div>
            </div>
          </div>
        )}
        {/* <div className="flex m-2 py-2" >
          <Heading heading={"Player Impact"} />
           </div> */}
        <div className="flex mx-2 py-2 items-center justify-between">
          <div className="flex w-10/12 ">
            <Heading heading={'Players Impact'} />
            <svg
              xmlns="http://www.w3.org/2000/svg"
              onClick={() => props.setShowPlayerIndex(true)}
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              className="w-4 h-4 ml-1 mt-1"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"
              />
            </svg>
          </div>
          <div className="flex justify-center rounded  bg-gray-4 px-2 py-1.5 items-center  ">
            <Link
              href={`/criclytics/${props?.matchID}/${props?.mainData?.seriesName
                .replace(/[^a-zA-Z0-9]+/g, ' ')
                .split(' ')
                .join('-')
                .toLowerCase()}/report-card`}
            >
              <img className="w-6 h-6" src="/pngsV2/arrow.png" alt="" />
            </Link>
          </div>
        </div>
        <div className="h-8/12  p-2">
          <div className="w-full  flex items-center justify-center  ">
            <div
              className={`w-full   dark:border-none dark:bg-gray-4 rounded-full p-1 text-xs  font-semibold flex md:gap-2 lg:gap-2  text-black dark:text-white`}
            >
              {[
                {
                  name:
                    props.data && props.data.livePlayerImpact?.[0]?.teamName,
                  value:
                    props.data && props.data.livePlayerImpact?.[0]?.teamName,
                  index: 1,
                },
                {
                  name:
                    props.data && props.data.livePlayerImpact?.[1]?.teamName,
                  value:
                    props.data && props.data.livePlayerImpact?.[1]?.teamName,
                  index: 0,
                },
              ].map((item, index) => (
                <div
                  onClick={() => {
                    setActiveTab(item)
                    setActiveIndex(index)
                  }}
                  className={`flex gap-3 rounded-full justify-center items-center md:p-2 lg:p-2 p-1 md:cursor-pointer truncate w-full ${
                    item.value === activeTab?.value
                      ? 'text-white bg-red-6 dark:basebg dark:border  dark:border-green dark:text-green dark:bg-transparent'
                      : 'bg-light_gray dark:bg-transparent '
                  }`}
                >
                  <span> {item.name} </span>
                  {item.src && (
                    <img className="w-5 h-5 " src={item.src} alt="icon" />
                  )}
                </div>
              ))}
            </div>
            {/* <Tab
          data={[
            {
                name: props.data &&props.data.livePlayerImpact[0].teamName,
                value: props.data &&props.data.livePlayerImpact[0].teamName,
                index:1
            },
            {
                name: props.data &&props.data.livePlayerImpact[1].teamName,
                value: props.data &&props.data.livePlayerImpact[1].teamName,
                index:0
            },
          ]}
          type="block"
          selectedTab={activeTab}
          handleTabChange={handleTabChange}
        /> */}
          </div>
          <div className=" w-full ">
            <div className="flex py-2 mt-4 bg-gray-8 shadow-4 justify-between items-center rounded-t-md text-xs">
              <div className="flex md:w-8/12 lg:w-8/12 xl:w-8/12 w-4/12">
                <h2 className="text-xs font-semibold ml-2 text-white">
                  Player
                </h2>
              </div>
              {playerImpactTag?.map((list) => (
                <div className="flex items-center w-3/12 md:w-20  text-white py-2 rounded-sm justify-center xl:w-20 lg:w-20 text-center text-xs font-semibold">
                  {list?.label == 'Batting Impact' ? (
                    <BatIcon height={16} clr="#5F92AC" />
                  ) : list?.label == 'Bowling Impact' ? (
                    <BowlIcon height={16} clr="#5F92AC" />
                  ) : (
                    list?.label
                  )}
                </div>
              ))}
            </div>
            {activeTab?.value?.toLowerCase() === props?.data &&
            props?.data?.livePlayerImpact?.[0]?.teamName?.toLowerCase() ? (
              <div className="">
                {props.data &&
                  props?.data?.livePlayerImpact?.[1]?.topThree?.map(
                    (player, index) => {
                      return (
                        <div className="">
                          <div
                            className="
                  flex py-1 lg:my-4 text-xs font-medium justify-between items-center px-1 dark:bg-gray-8 dark:my-4 bg-white"
                          >
                            <div className="flex dark:w-3/12 w-9/12 break-words">
                              <div className="flex dark:text-white text-black truncate">
                                {player?.playerName}
                              </div>
                            </div>
                            <div
                              className={`flex items-center ${
                                player?.bowling_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              } text-xs justify-center w-1/12
                      `}
                            >
                              {round(player?.bowling_impact, 2)}
                            </div>
                            <div
                              className={`flex items-center justify-center text-xs w-2/12 ${
                                player?.batting_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              }`}
                            >
                              {round(player?.batting_impact, 2)}
                            </div>
                            <div
                              className={`flex items-center md:w-20 justify-center ${
                                player?.total_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              }  text-xs  w-2/12 `}
                            >
                              {round(player?.total_impact, 2)}
                            </div>
                          </div>
                        </div>
                      )
                    },
                  )}
              </div>
            ) : (
              <>
                <div className="bg-gray-4  pb-2">
                  {props?.data &&
                    props?.data?.livePlayerImpact[
                      tabActiveIndex
                    ]?.topThree?.map((player, index) => {
                      return (
                        <div className="pt-1">
                          <div
                            className="
                      flex py-1 text-xs font-medium justify-between items-center px-1   "
                          >
                            <div className="flex w-9/12 dark:w-4/12 break-words">
                              <div className="flex dark:text-white text-black truncate ml-1">
                                {player?.playerName}
                              </div>
                            </div>
                            <div
                              className={`flex items-center justify-center text-xs ${
                                player?.batting_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              }  w-3/12 `}
                            >
                              {round(player?.batting_impact, 2)}
                            </div>
                            <div
                              className={`flex items-center ${
                                player?.bowling_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              } text-xs justify-center w-3/12
                    `}
                            >
                              {round(player?.bowling_impact, 2)}
                            </div>
                            <div
                              className={`flex items-center md:w-20 justify-center ${
                                player?.total_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              }  text-xs w-3/12 `}
                            >
                              {round(player?.total_impact, 2)}
                            </div>
                          </div>
                        </div>
                      )
                    })}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    )
  )
}
const playerImpactTag = [
  {
    label: 'Batting Impact',
    key: 'battingImpact',
  },
  {
    label: 'Bowling Impact',
    key: 'bowlingImpact',
  },
  {
    label: 'Net Impact',
    key: 'netImpact',
  },
]
