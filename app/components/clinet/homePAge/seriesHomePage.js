"use client";
import React, { useRef, useState, useEffect } from "react";
import Heading from "../../shared/heading";
import { HOME_SERIES_TO_LOOK_OUT } from "../../../api/queries";
import { format, formatDistanceToNowStrict } from "date-fns";
import { useQuery, useLazyQuery } from "@apollo/client";
import Link from "next/link";

export default function SeriesLookOut({ data, ...props }) {
  let scrl = useRef(null);

  let scrl1 = useRef(null);
  const [scrollWidth, setScrollWidth] = useState(0);

  const [scrollX, setscrollX] = useState(0);
  const [scrollX1, setscrollX1] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  const [scrolEnd1, setscrolEnd1] = useState(false);
  useEffect(() => {
    if (scrl && scrl.current && scrl.current.clientWidth) {
      setScrollWidth(scrl.current.clientWidth);
    }
  }, []);
  const slide = (shift) => {
    // scrl.current.scrollLeft += shift;

    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: "smooth",
    });
    setscrollX(scrollX + shift);

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  // let { loading, error, data } = useQuery(HOME_SERIES_TO_LOOK_OUT, {

  //     })
  //     if(loading)
  //     {
  //         return (<div>Loading...</div>)
  //     }

  if (data) {
    return (
      <div className="flex  flex-col w-full">
        <div className="flex items-center justify-between">
          <Heading heading={"Series To Lookout For"} noPad />
        </div>

        <div className="flex items-center justify-center  pt-4  w-full relative">
          {data &&
            data.getWebHomePageData &&
            data.getWebHomePageData.seriesToLookoutFor &&
            data.getWebHomePageData.seriesToLookoutFor.length > 4 && (
              <div
                onClick={() => scrollX !== 0 && scrollX > -1 && slide(-190)}
                className={`${
                  scrollX < 90 ? "opacity-30" : "opacity-100"
                }  flex  absolute -left-4 cursor-pointer  bg-black rounded-full   item-center justify-center z-10  white pointer dn db-ns z-999 outline-0 `}
                id="swiper-button-prev"
              >
                <svg width="20" focusable="false" viewBox="0 0 24 24">
                  <path
                    fill="#fff"
                    d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
            )}
          <div className="flex overflow-x-scroll w-full" ref={scrl}>
            {data &&
              data.getWebHomePageData &&
              data.getWebHomePageData.seriesToLookoutFor &&
              data.getWebHomePageData.seriesToLookoutFor.map((item, i) => {
                return (
                  <div className="">
                    <div className="flex">
                      {item.seriesType == "Bilateral" && (
                        <div
                          key={i}
                          className=" w-48 shadow-md  mx-2  border rounded-xl"
                        >
                          <Link
                            href={`/series/${item.type}/${
                              item.tourID
                            }/${item.tourName
                              .replace(/[^a-zA-Z0-9]+/g, " ")
                              .split(" ")
                              .join("-")
                              .toLowerCase()}/matches`}
                          >
                            <div className="flex flex-col  h-32 items-center justify-center">
                              <div className="flex items-center justify-center  ">
                                <div className="z-10">
                                  <img
                                    className="h-6 w-9 rounded  "
                                    src={`https://images.cricket.com/teams/${item.teamid[0]}_flag_safari.png`}
                                    alt=""
                                  />
                                </div>
                                <div>
                                  <img
                                    className="h-6 w-6 mx-1"
                                    src="/pngsV2/bluecross.png"
                                    alt=""
                                  />
                                </div>
                                <div className=" z-10">
                                  <img
                                    className="h-6 w-9 rounded  "
                                    src={`https://images.cricket.com/teams/${item.teamid[1]}_flag_safari.png`}
                                    alt=""
                                  />
                                </div>
                              </div>

                              <div className="flex text-md mt-3 items-center justify-center">
                                {item.match}
                              </div>

                              <div className="font-semibold  text-sm mt-2 items-center  justify-center text-black rounded-full  px-2 py-1">
                                {format(
                                  new Date(
                                    Number(item.seriesStartDate) - 19800000
                                  ),
                                  " MMM do"
                                )}
                                -{" "}
                                {format(
                                  new Date(
                                    Number(item.seriesEndDate) - 19800000
                                  ),
                                  " MMM do"
                                )}
                              </div>
                            </div>
                          </Link>
                        </div>
                      )}
                      {item.seriesType == "Tournament" && (
                        <div
                          key={i}
                          className=" w-48 shadow-md  mx-2  border   rounded-xl"
                        >
                          <Link
                            href={`/series/${item.type.toLowerCase()}/${
                              item.tourID
                            }/${item.tourName
                              .replace(/[^a-zA-Z0-9]+/g, " ")
                              .split(" ")
                              .join("-")
                              .toLowerCase()}/matches`}
                          >
                            <div className="flex flex-col  h-32 items-center justify-center">
                              <div className="flex items-center justify-center  ">
                                <div className="z-10">
                                  <img
                                    className=" object-cover  object-top rounded-md  "
                                    src={`${item.teamid[0]}?w=40&h=40&auto=compress,format&fit=crop&dpr=2`}
                                    alt=""
                                  />
                                </div>
                              </div>

                              <div className="font-semibold text-sm mt-2 items-center  justify-center text-black  rounded-full  px-2 py-1">
                                {format(
                                  new Date(
                                    Number(item.seriesStartDate) - 19800000
                                  ),
                                  " MMM dd"
                                )}
                                -{" "}
                                {format(
                                  new Date(
                                    Number(item.seriesEndDate) - 19800000
                                  ),
                                  " MMM dd"
                                )}
                              </div>
                            </div>
                          </Link>
                        </div>
                      )}
                    </div>
                  </div>
                );
              })}
          </div>
          {data &&
            data.getWebHomePageData &&
            data.getWebHomePageData.seriesToLookoutFor &&
            data.getWebHomePageData.seriesToLookoutFor.length > 4 && (
              <div
                onClick={() => !scrolEnd && slide(190)}
                id="swiper-button-prev"
                className={`${
                  scrolEnd ? "opacity-40" : "opacity-100"
                } white pointer absolute  bg-black rounded-full -right-5 flex  item-center justify-center cursor-pointer shadow-md  `}
              >
                <svg width="20" viewBox="0 0 24 24">
                  <path
                    fill="#fff"
                    d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
            )}
        </div>
      </div>
    );
  }
}
