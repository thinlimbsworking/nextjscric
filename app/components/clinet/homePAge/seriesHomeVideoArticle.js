'use client'
import { format, formatDistanceToNowStrict } from 'date-fns'

import React from 'react'
import Button from '../../shared/button'
import Heading from '../../shared/heading'
// import NewsVideo from './newHomePage'
import Link from 'next/link'
import { ARTICLES_VIDEOS_BY_SERIES } from '../../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'

export default function HomePageCLinet({ data, ...props }) {
  //   let { loading, error, data } = useQuery(FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE, {

  // })
  // const { loading, error, data } = useQuery(ARTICLES_VIDEOS_BY_SERIES, {})

  // if (loading) {
  //   return <div></div>
  // }
  console.log("data.getArticlesVideosBySeriesdata.getArticlesVideosBySeries",data.getArticlesVideosBySeries)
  if (data.getArticlesVideosBySeries) {
    return (

<div>
      {data?.getArticlesVideosBySeries.map((item,index)=>{return(
      <div className="flex flex-col items-center justify-center    w-full bg-white py-4 px-8 rounded-md mt-5   ">




        <div className="flex items-start justify-start  flex-col  w-full ">
          <div className=" py-2 text-base font-medium  text-left   font-mnm  leading-5 line-clamp-1">
            {' '}
            {item.tourName || ''}{' '}
          </div>
          <div className="flex flex-col w-full">
            <Link
              href={`/news/${item?.data?.[0]?.id}`}
            >
              <div className="flex flex-col  w-full py-2">
                <div className="relative w-full h-72  rounded-md  ">
                  <img
                    className=" w-full h-full object-top object-cover  rounded-md "
                    src={`${item?.data?.[0]?.featureThumbnail}?auto=compress&fit=crop&crop=face&w=600&h=350`}
                    // src={`${data.getArticleByPostitions[1].featureThumbnail}?auto=compress&fit=crop&crop=top&h=400`}
                    alt=""
                  />
                  <div className="px-1 absolute bottom-2 uppercase dark:bg-green-6 dark:text-black text-white bg-gray-4 rounded-sm text-xs font-medium left-1">
                     {item?.data?.[0]?.type
                        .replace(/[^a-zA-Z0-9]+/g, ' ')
                        .split(' ')
                        .join('-')
                        .toLowerCase()} 
                  </div>
                </div>

                <div className=" ml-2  font-semibold    text-sm  py-1 leading-5 text-gray-9      text-left    font-mnr  line-clamp-2 ">
                  {' '}
                  {item?.data?.[0]
                    ?.description || ''}
                </div>
              </div>
            </Link>

            <div className="w-full text-mnr ">
              <div className="">
                <div className="flex w-full ">
                  {item?.data?.map((items, i) => {
                      return (
                        0 < i &&
                        i < 4 && (
                          <div className="  w-4/12 m-1    p-1 rounded-md">
                            <Link href={`/news/${items.id}`}>
                              <div className="flex  items-center justify-center  h-28  relative">
                                <img
                                  className=" w-full  rounded-lg object-cover object-top h-full"
                                  src={`${items.featureThumbnail}?auto=compress&fit=crop&w=260&h=150`}
                                  alt=""
                                />
                                <div className="px-1 absolute bottom-2 dark:bg-green-6 dark:text-black text-white bg-gray-4 rounded-sm text-xs font-medium left-1 uppercase">
                                  {items.type.replace(/[_]/g, ' ').toUpperCase()}
                                </div>
                              </div>
                              <div className="mt-2 font-mnsb leading-4 text-sm font-semibold text-gray-9 line-clamp-2 ">
                                {' '}
                                {items.title || ''}
                              </div>
                              <div className="flex items-center  justify-start p-1 pt-1">
                                <div className="pr-2  text-gray-2 text-xs text-left   border-r">
                                  {items.authors}
                                </div>
                                <div className="px-2 text-gray-2 text-xs text-left ">
                                  {format(
                                    new Date(
                                      Number(items.publishedAt) - 19800000,
                                    ),
                                    'dd MMM yyyy',
                                  )}
                                </div>
                              </div>
                            </Link>
                          </div>
                        )
                      )
                    })}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="flex w-full items-center justify-end  pt-3">
          <Button
            title={'View more'}
            url={`series/domestic/${item?.tourID}/${item.tourName.replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()}/news`}
          />
        </div>
      </div>)})}
      </div>
    )
  }
}
