import React from 'react'
import { useQuery, useLazyQuery } from '@apollo/react-hooks'
import { STAT_HUB_STADIUM, MATCH_SUMMARY } from '../../../api/queries'
import Heading from '../../shared/heading'
import { words } from '../../../constant/language'

export default function StatHubStadium(props) {
  let lang = props.language ? props.language : 'ENG'

  const { loading, error, data: StadiumStatHub } = useQuery(STAT_HUB_STADIUM, {
    variables: { matchID: props.matchID },
  })
  const getLangText = (lang, keys, key) => {
    let [english, hindi] = keys[key]
    switch (lang) {
      case 'HIN':
        return hindi
      case 'ENG':
        return english
      default:
        return english
    }
  }
  return StadiumStatHub ? (
    <div className="w-full mt-3 mx-2 ">
      <div className="border p-2 border-gray-2 dark:border-none bg-white text-black dark:bg-none dark:text-white dark:bg-gray  rounded-lg">
        {
          <div className="w-full p-">
            <div className=" flex w-1/2 pb-2">
              <Heading heading={'Stadium Stats'} />
            </div>

            <div className="flex w-full items-center  justify-start  text-xs font-mnr  ">
              <img
                className="mr-1 h-4 w-4"
                src="/pngsV2/location2.png"
                alt=""
              />
              {props.data.venueName}
            </div>
            <div className="flex w-full flex-wrap items-start  justify-start gap-2 md:gap-8 lg:gap-8 my-2  ">
              {lang === 'HIN'
                ? StadiumStatHub &&
                  StadiumStatHub?.stadiumHub &&
                  Object.keys(StadiumStatHub?.stadiumHub?.characteristicsInHindi)
                    .filter(
                      (val) =>
                        StadiumStatHub?.stadiumHub?.characteristicsInHindi[
                          val
                        ] !== null,
                    )
                    .map((character, i) => (
                      <div
                        className={` text-center  ${
                          character === 'weather' &&
                          StadiumStatHub?.stadiumHub?.characteristicsInHindi[
                            'weather'
                          ] === 'अनुपलब्ध'
                            ? ''
                            : ' black  bg-green p-1  capatilize rounded-lg '
                        } `}
                        key={i}
                      >
                        {character === 'weather' &&
                        StadiumStatHub.stadiumHub.characteristicsInHindi[
                          'weather'
                        ] === 'अनुपलब्ध'
                          ? ''
                          : character === 'weather' &&
                            StadiumStatHub.stadiumHub.characteristicsInHindi[
                              'weather'
                            ] !== 'अनुपलब्ध'
                          ? 'मौसम : ' +
                            StadiumStatHub.stadiumHub.characteristicsInHindi[
                              character
                            ]
                          : StadiumStatHub.stadiumHub.characteristicsInHindi[
                              character
                            ]}
                      </div>
                    ))
                : StadiumStatHub &&
                  StadiumStatHub.stadiumHub &&
                  Object.keys(StadiumStatHub.stadiumHub.characteristics)
                    .filter(
                      (val) =>
                        StadiumStatHub.stadiumHub.characteristics[val] !== null,
                    )
                    .map((character, i) => (
                      <div
                        className={`text-center ${
                          character === 'weather' &&
                          StadiumStatHub.stadiumHub.characteristics[
                            'weather'
                          ] === 'N/A'
                            ? ''
                            : ' black bg-green p-1 capatilize rounded-lg text-xs text-gray '
                        } `}
                        key={i}
                      >
                        {character === 'weather' &&
                        StadiumStatHub.stadiumHub.characteristics['weather'] ===
                          'N/A'
                          ? ''
                          : character === 'weather' &&
                            StadiumStatHub.stadiumHub.characteristics[
                              'weather'
                            ] !== 'N/A'
                          ? 'weather : ' +
                            StadiumStatHub.stadiumHub.characteristics[character]
                          : StadiumStatHub.stadiumHub.characteristics[
                              character
                            ]}
                      </div>
                    ))}
            </div>
          </div>
        }
        <div className="flex  pb-2">
          <div className="w-3/12 flex flex-col gap-2  border-r border-gray-2 items-center justify-center ">
            <div className="font-semibold   text-md  text-center ">
              {props.data.avgFirstInningScore || '-'}
            </div>
            <div
              style={{ fontSize: 10 }}
              className="dark:text-white text-light  text-center  "
            >
              1st Batting Avg
            </div>
          </div>
          <div className="bl b--white-20"></div>
          <div className="w-3/12 flex flex-col gap-2 border-r border-gray-2 items-center justify-center text-center  ">
            <div className="text-md dark:text-white font-bold text-center ">
              {props.data.firstBattingWinPercent || '-'}
            </div>
            <div
              style={{ fontSize: 10 }}
              className=" dark:text-white text-light  text-center"
            >
              1st Batting Win %
            </div>
          </div>
          <div className="bl b--white-20"></div>
          <div className="w-3/12 flex flex-col gap-2 border-r border-gray-2 items-center justify-center text-center">
            <div className="text-md dark:text-white font-bold text-center">
              {props.data.highestScoreChased || '-'}
            </div>
            {/* <div className=" text-white font-bold text-xl tracked ">18/38</div> */}
            <div
              style={{ fontSize: 10 }}
              className=" dark:text-whitetext-light text-center "
            >
              Highest Score Chased
            </div>
          </div>
          <div className="bl  b--white-20"></div>
          <div className="w-3/12 flex flex-col gap-2 items-center justify-center">
            <div className="font-semibold   text-md  text-center  ">
              {`${props.data.paceWicket} / ${props.data.spinWicket}`}
            </div>
            <div
              style={{ fontSize: 10 }}
              className="text-light  dark:text-white text-center  "
            >
              Wicket-spilt <br /> Pace-spin
            </div>
          </div>
        </div>
        {StadiumStatHub &&
          StadiumStatHub.stadiumHub &&
          StadiumStatHub.stadiumHub.characteristics && (
            <div className="text-left text-xs text-light p-2">
              {lang === 'HIN' ? (
                <span>{`*  पिछले 3 वर्षों के ${StadiumStatHub.stadiumHub.compType}  डेटा`}</span>
              ) : (
                <span>{props.data.text}</span>
              )}
            </div>
          )}
      </div>
    </div>
  ) : (
    <div className=''></div>
  )
}
