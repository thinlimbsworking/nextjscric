import React, { useState } from 'react'
import Heading from '../../shared/heading'
import { TEAM_ANALYSIS_HOMEPAGE } from '../../../api/queries'
import { useQuery } from '@apollo/react-hooks'
// import { useState } from 'react/cjs/react.production.min';

export default function TeamAnalysis(props) {
  const [showList, setSHowList] = useState(0)

  // program to check if a number is a float or integer value

  function checkNumber(x) {
    // check if the passed value is a number
    if (typeof x == 'number' && !isNaN(x)) {
      // check if it is integer
      if (Number.isInteger(x)) {
        return 'number'
      } else {
        return 'float'
      }
    } else {
    }
  }

  const lableObject = [
    {
      lable: 'Overall',
      key: 'overallRating',
    },
    {
      lable: 'Vs Pace Bowling',
      key: 'battingVSPace',
    },
    {
      lable: 'Vs Spin Bowling',
      key: 'battingVSSpin',
    },
    {
      lable: 'Batting Depth',
      key: 'battingDepth',
    },
    {
      lable: 'Pace Bowling',
      key: 'paceBowling',
    },
    {
      lable: 'Spin Bowling',
      key: 'spinBowling',
    },
  ]

  let { loading, error, data } = useQuery(TEAM_ANALYSIS_HOMEPAGE, {
    variables: { matchID: props.matchID },
  })
  if (error) return <div></div>
  if (loading) return <div></div>
  if (data && data.getTeamAnalysis.teamA && data.getTeamAnalysis.teamB) {
    return (
      <div className="flex w-full  flex-col  relative  mt-3 m-2">
        {props.showTeamInfo && (
          <div
            className="  z-50   absolute  flex items-start justify-center "
            style={{ top: '-10rem' }}
          >
            <div className="flex relative items-end justify-center  ">
              <div
                className="w-11/12    bg-gray-2  p-2 text-[11px] text-white rounded-md leading-5   popup-container "
                onClick={() => props.setShowTeamInfo(false)}
              >
                It measures team strength by analyzing the performance of
                individual team members over a period of time and dividing their
                strengths into sub-categories. This allows for a more detailed
                examination of the team's performance in specific areas, such as
                batting, bowling and team dynamics. By combining the results of
                each sub-category, an overall team strength rating is obtained.
              </div>
            </div>
          </div>
        )}

        <div className="flex bg-gray-4  w-full rounded-md items-center justify-evenly p-2">
          <div className=" font-medium   flex items-center justify-start text-sm  capitalize   font-ttb   text-left w-full ">
            Team Analysis
            <svg
              xmlns="http://www.w3.org/2000/svg"
              onClick={() => props.setShowTeamInfo(true)}
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-4 h-4 ml-1"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"
              />
            </svg>
          </div>
          <div
            className={`pr-2  flex items-end justify-end   w-full text-right  `}
            onClick={() => setSHowList(showList == 0 ? 5 : 0)}
          >
            {' '}
            <img
              className={`h-2 ${showList == 0 ? '' : 'rotate-180'} `}
              src={'/pngsV2/whitearrow_down.png'}
            />{' '}
          </div>
        </div>

        <div className="flex flex-col items-center justify-center bg-gray">
          <div className="flex items-center justify-center   w-full border-b-2 border-gray-4 p-1 ">
            <div className="w-4/12 flex items-center justify-center"></div>
            <div className="w-4/12 flex items-center justify-center border-r border-l border-gray-4 text-xs font-medium">
              {data.getTeamAnalysis.teamA.teamShortName}
            </div>

            <div className="w-4/12 flex items-center justify-center  text-xs font-medium">
              {data.getTeamAnalysis.teamB.teamShortName}
            </div>
          </div>
          <div className="flex items-center justify-center flex-col w-full">
            {lableObject.map((item, index) => {
              return (
                showList >= index && (
                  <div className="flex text-xs items-center border-b-2 border-gray-4 justify-center   w-full ">
                    <div className="w-4/12 flex items-center justify-start text-md  font-sans pl-2 font-medium ">
                      {item.lable}{' '}
                    </div>
                    <div className="w-4/12 flex items-center justify-center border-r border-l border-gray-4">
                      {/* {parseFloat(data.getTeamAnalysis.teamA[lableObject[index].key])} */}

                      <div className="flex justify-evenly py-2">
                        {[1, 2, 3, 4, 5].map((num, innerKey) =>
                          data.getTeamAnalysis.teamA[item.key] > innerKey &&
                          data.getTeamAnalysis.teamA[item.key] <
                            innerKey + 1 ? (
                            <img
                              className="mx-1 h-2 w-2 object-contain"
                              src={'/pngsV2/re_hstarwhite.png'}
                              alt="FW"
                            />
                          ) : data.getTeamAnalysis.teamA[item.key] >
                            innerKey ? (
                            <img
                              className=" mx-1 h-2 w-2 object-contain"
                              src={'/pngsV2/re_fstarwhite.png'}
                              alt="FW"
                            />
                          ) : (
                            <img
                              className="mx-1 h-2 w-2 object-contain"
                              src={'/pngsV2/re_fstarblack.png'}
                              alt="Eb"
                            />
                          ),
                        )}
                      </div>
                    </div>

                    <div className="w-4/12 flex flex-col items-center justify-center ">
                      <div key={item.key}>
                        {/* <div>
                  <div  className='flex lable-light dark:lable pl-2'>
                    {item.lable}
                  </div>
                </div> */}
                        <div className="flex justify-evenly py-2 ">
                          {[1, 2, 3, 4, 5].map((num, innerKey) =>
                            data.getTeamAnalysis.teamB[item.key] > innerKey &&
                            data.getTeamAnalysis.teamB[item.key] <
                              innerKey + 1 ? (
                              <img
                                className="mx-1 h-2 w-2 object-contain"
                                src={'/pngsV2/re_hstarwhite.png'}
                                alt="FW"
                              />
                            ) : data.getTeamAnalysis.teamB[item.key] >
                              innerKey ? (
                              <img
                                className=" mx-1 h-2 w-2 object-contain"
                                src={'/pngsV2/re_fstarwhite.png'}
                                alt="FW"
                              />
                            ) : (
                              <img
                                className="mx-1 h-2 w-2 object-contain"
                                src={'/pngsV2/re_fstarblack.png'}
                                alt="Eb"
                              />
                            ),
                          )}
                        </div>
                      </div>

                      {false &&
                        [1, 2, 3, 4, 5].map((item, index) => {
                          return parseFloat(
                            data.getTeamAnalysis.teamA[lableObject[index].key],
                          ) >= item ? (
                            <>
                              {/* {5-parseFloat(data.getTeamAnalysis.teamA[lableObject[index].key])} */}
                              <img
                                className="m-1"
                                src={'/pngsV2/re_fstarwhite.png'}
                              />
                            </>
                          ) : (
                            <>
                              <img
                                className="m-1"
                                src={'/pngsV2/re_hstarwhite.png'}
                              />
                            </>
                          )
                        })}
                    </div>
                  </div>
                )
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}
