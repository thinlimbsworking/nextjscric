import React from "react";
import { format, formatDistanceToNowStrict } from "date-fns";
import { SERIES_HOME_CARD_QUERY } from "../../../api/queries";
import { useQuery } from "@apollo/react-hooks";
import { useRouter } from "next/navigation";
import Heading from "../../shared/heading";

import Link from "next/link";

export default function UpcomingMatches(props) {



  const router = useRouter();
  const navigate = router.push;
  let {
    loading: loading,
    error: error,
    data,
  } = useQuery(SERIES_HOME_CARD_QUERY, {});

  const handleNavigation = (match) => {
    // alert(8)

    const event =
      match.matchStatus === "upcoming" || match.matchStatus === "live"
        ? "CommentaryTab"
        : "SummaryTab";

    // const event = match.matchStatus === 'upcoming' || match.matchStatus === 'live' ? 'CommentaryTab' : 'SummaryTab';
    let matchStatus =
      match?.matchStatus === "completed" ? "match-score" : "live-score";

    let currentTab = match.isAbandoned
      ? "commentary"
      : match.matchStatus === "upcoming" || match.matchStatus === "live"
      ? "Commentary"
      : "Summary";
    let matchName = `${
      match?.homeTeamName ? match?.homeTeamName : match?.homeTeamShortName
    }-vs-${
      match?.awayTeamName ? match?.awayTeamName : match?.awayTeamShortName
    }-${match?.matchNumber ? match?.matchNumber.split(" ").join("-") : ""}-`;

    let seriesName =
      matchName.toLowerCase() +
      (match?.seriesName
        ? match.seriesName
            .replace(/[^a-zA-Z0-9]+/g, " ")
            .split(" ")
            .join("-")
            .toLowerCase()
        : "");

    return `/${matchStatus.toLowerCase()}/${
      match.matchID
    }/${currentTab.toLowerCase()}/${seriesName}`;
  };


  if (loading) data = {};
  if (props.error) error = props.error;
  if (data && data.SeriesHomeCard && data.SeriesHomeCard.topPerformances && data.SeriesHomeCard.topPerformances.length > 0) {
    return (
      <>
        <div className=" flex flex-col text-white mb:mt-5 md:mt-1 p-1 ml-2  ">
          <div className="flex ">
            <Heading heading={"Top Performers"} borderlineHide={true} />
          </div>

          {/* series/domestic/2814/indian-premier-league-2023/stats */}
          <div className=" text-white mb:mt-5 md:mt-1  ">
            <div className="flex  w-full items-center justify-evenly">
              {data &&
                data.SeriesHomeCard &&
                data.SeriesHomeCard.topPerformances &&
                data.SeriesHomeCard.topPerformances &&
                data.SeriesHomeCard.topPerformances.map((item, i) => {
                  return (
                    <Link
                      className="flex w-full flex-col items-center m-1  "
                      href={`/series/${data?.SeriesHomeCard?.seriesType?.toLowerCase()}/${
                        item.tourID}/${item.tourName
                                .replace(/[^a-zA-Z0-9]+/g, " ")
                                .split(" ")
                                .join("-")
                                .toLowerCase()}/stats`}
                    >
                    
                      <div className="flex w-full flex-col items-center  bg-gray p-2  rounded-md">
                        <div className="relative   flex justify-center p-2">
                          <div className="h-18 w-18 rounded-full overflow-hidden bg-white dark:bg-gray-4">
                            <img
                              loading="lazy"
                              //  fallbackSrc = '/pngsV2/playerph.png'
                              className="h-20 w-20 object-cover object-top"
                              src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                              onError={(evt) =>
                                (evt.target.src = "/pngsV2/playerph.png")
                              }
                            />
                          </div>
                          {(item.captain == "1" ||
                            item.vice_captain == "1") && (
                            <div className="w-3 h-3 absolute left-0 bottom-0 rounded-md text-green border p-2 bg-gray-4 flex justify-center items-center text-xs font-medium">
                              {item.captain == "1"
                                ? "C"
                                : item.vice_captain == "1"
                                ? "VC"
                                : ""}
                            </div>
                          )}
                          <img
                            height={18}
                            width={18}
                            loading="lazy"
                            className="w-5 h-5 drop-shadow-lg absolute border left-2 bottom-1 object-center flex justify-center rounded-full "
                            src={`https://images.cricket.com/teams/${item.teamId}_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = "/pngsV2/flag_dark.png")
                            }
                          />
                        </div>
                        <div className="dark:text-white text-sm   text-center font-semibold  truncate  pt-1">
                          {item.playerName}
                        </div>
                        <div
                          className={`${
                            item.ScoreType &&
                            item.ScoreType.toLowerCase() == "runs"
                              ? "text-orange-3"
                              : "text-purple-500"
                          } py-1 font-regular text-xs capitalize text-center  `}
                        >
                          Most {item.ScoreType}
                        </div>
                        <div className=" text-base capitalize text-center font-semibold  text-white ">
                          {item.score}
                        </div>
                      </div>
                    </Link>
                  );
                })}
            </div>
          </div>
        </div>

        {data &&
          data.SeriesHomeCard &&
          data.SeriesHomeCard.matchesData &&
          data.SeriesHomeCard.matchesData.length > 0 && (
            <div className=" mx-2 mt-1">
              <div className="flex items-center justify-between mb-2">
                <div className="ml-2">
                <Heading heading={"Upcoming Matches"} borderlineHide={true} />
                </div>
                <div className="flex justify-center rounded  bg-gray px-2 py-1.5  items-center  ">
                  <Link href=
                  {`/series/${data?.SeriesHomeCard?.matchesData[0]?.seriesType?.toLowerCase()}/${
                    data?.SeriesHomeCard?.matchesData[0]?.tourID
                          }/${data?.SeriesHomeCard?.matchesData[0]?.seriesName
                            .replace(/[^a-zA-Z0-9]+/g, " ")
                            .split(" ")
                            .join("-")
                            .toLowerCase()}/matches`}
>                    <img className="w-6 h-6" src="/pngsV2/arrow.png" alt="" />
                  </Link>
                </div>
              </div>

            
              <div className="flex overflow-scroll -mb-2 ">
                {data &&
                  data.SeriesHomeCard &&
                  data.SeriesHomeCard.matchesData.map((item, i) => {
                    return (
                      <div className="">
                        {
                          <div
                            key={i}
                            className=" w-44  mx-2  bg-gray   rounded-xl"
                          >
                          <Link
                            
                            className="w-full"
                            // href={"/here"}
                            // href={handleNavigation(item)}
                            href={`/series/${item.seriesType?.toLowerCase()}/${
                              item.tourID
                            }/${item.seriesName
                              .replace(/[^a-zA-Z0-9]+/g, " ")
                              .split(" ")
                              .join("-")
                              .toLowerCase()}/matches`}
                            >
                            <div className="flex flex-col  h-40 items-center justify-center">
                              <div className="flex flex-col items-center justify-center  ">
                                <div className="flex items-center justify-center  ">
                                  <div className="z-10">
                                    <img
                                      className="h-6 w-9 rounded  "
                                      src={`https://images.cricket.com/teams/${item.homeTeamID}_flag_safari.png`}
                                      onError={(evt) =>
                                        (evt.target.src =
                                          "/pngsV2/flag_dark.png")
                                      }
                                      alt=""
                                    />
                                  </div>
                                  <div>
                                    <img
                                      className="h-6 w-6 mx-1"
                                      src="/pngsV2/bluecross.png"
                                      alt=""
                                    />
                                  </div>
                                  <div className=" z-10">
                                    <img
                                      className="h-6 w-9 rounded  "
                                      src={`https://images.cricket.com/teams/${item.awayTeamID}_flag_safari.png`}
                                      onError={(evt) =>
                                        (evt.target.src =
                                          "/pngsV2/flag_dark.png")
                                      }
                                      alt=""
                                    />
                                  </div>
                                </div>
                                <div className="flex items-center justify-evenly mt-2  w-full ">
                                  <div className="z-10 ">
                                    {item.homeTeamShortName}
                                  </div>
                                  <div>VS</div>
                                  <div className=" z-10">
                                    {item.awayTeamShortName}
                                  </div>
                                </div>
                              </div>

                              <div className="font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1">
                                {format(
                                  new Date(Number(item.matchDateTimeIST)),
                                  "MMM dd, iii"
                                )}
                                {format(
                                  new Date(
                                    Number(item.matchDateTimeIST - 19800000)
                                  ),
                                  " h:mm a"
                                )}
                              </div>
                            </div>
                            </Link>
                          </div>
                        }
                      </div>
                    );
                  })}
              </div>
            </div>
          )}
      </>
    );
  }
}
