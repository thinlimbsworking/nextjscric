'use client'
import { useState, useEffect, useRef } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { DATA_DIGEST } from '../../../api/queries'
import Script from 'next/script'
import Head from 'next/head'
import parse from 'html-react-parser'
// import TweetEmbed from 'react-tweet-embed'
import SliderTab from '../../shared/Slidertab'
// import { TwitterTweetEmbed } from "react-twitter-embed";
import Tab from '../../shared/Tab'
import {
  TwitterTimelineEmbed,
  TwitterShareButton,
  TwitterFollowButton,
  TwitterHashtagButton,
  TwitterMentionButton,
  TwitterTweetEmbed,
  TwitterMomentShare,
  TwitterDMButton,
  TwitterVideoEmbed,
  TwitterOnAirButton,
} from 'react-twitter-embed'
import Button from '../../shared/button'
import Heading from '../../shared/heading'
export default function ClientComponent({ data, ...props }) {
  let scrl = useRef(null)
  let scrl1 = useRef(null)
  const [scrollWidth, setScrollWidth] = useState(0)

  const [scrollX, setscrollX] = useState(0)
  const [scrollX1, setscrollX1] = useState(0)
  const [scrolEnd, setscrolEnd] = useState(false)
  const [scrolEnd1, setscrolEnd1] = useState(false)
  const [windows, updateWindows] = useState()
  const callSocialWidget = () => {
    // alert(8)
    global.windows &&
      global.windows.twttr.ready().then(() => {
        global.windows &&
          global.windows.twttr.widgets.load(
            document.getElementsByClassName('twitter-div'),
          )
      })
  }
  const [tabName, setTabName] = useState({
    name: 'SOCIAL TRACKER',
    value: 'SOCIAL_TRACKER',
  })
  const [counter, setCointer] = useState(0)

  // const { loading, error, data } = useQuery(DATA_DIGEST, {
  // }
  // )

  useEffect(() => {
    if (global.window) updateWindows(global.window)
  }, [global.window])

  useEffect(() => {
    const s = document.createElement('script')
    s.setAttribute('src', 'https://platform.twitter.com/widgets.js')
    s.setAttribute('async', 'true')
    document.head.appendChild(s)
  }, [])
  const [index, setIndex] = useState(0)

  let matchTypes = [
    {
      name: 'SOCIAL TRACKER',
      value: 'SOCIAL_TRACKER',
    },
    {
      name: 'CRICSTATS',
      value: 'CRICSTATS',
    },
  ]
  useEffect(() => {
    if (scrl1 && scrl1.current && scrl1.current.clientWidth) {
      setScrollWidth(scrl1.current.clientWidth)
    }
  }, [])

  const slide = (shift) => {
    // scrl.current.scrollLeft += shift;

    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: 'smooth',
    })
    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  const slide1 = (shift) => {
    scrl1.current.scrollLeft += shift
    setscrollX1(scrollX1 + shift)

    if (
      Math.floor(scrl1.current.scrollWidth - scrl1.current.scrollLeft) <=
      scrl1.current.offsetWidth
    ) {
      setscrolEnd1(true)
    } else {
      setscrolEnd1(false)
    }
  }
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  if (data) {
    return (
      data &&
      data.cricStats && (
        <div className="flex flex-col  w-full ">
          <Heading heading={'Cric Stats'} noPad />
          <div className="flex relative items-center justify-center   pt-4">
            <div
              onClick={() => scrollX !== 0 && scrollX > -1 && slide(+-305)}
              className={`${
                scrollX < 300 ? 'opacity-30' : 'opacity-100'
              }   -left-4 flex  absolute cursor-pointer  bg-black rounded-full   item-center justify-center z-10    `}
              id="swiper-button-prev"
            >
              <svg width="20" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <div className="flex w-full">
              {tabName.value == 'SOCIAL_TRACKER' && (
                <div className="w-full  relative">
                  <div
                    className={` relative  flex   overflow-x-auto items-start   input-reset w-full} `}
                    ref={scrl}
                    onScroll={scrollCheck}
                  >
                    {data &&
                      data.cricStats &&
                      data.cricStats.map((item, i) => {
                        return (
                          <div className=" ">
                            <div key={i} className="w-72 h-72 m-1">
                              <img
                                className="h-72  object-top object-cover rounded-md shadow-sm"
                                src={`${item.img}?w=288&h=288&auto=compress,format&fit=crop&dpr=2`}
                                alt=""
                              />
                            </div>
                          </div>
                        )
                      })}
                    {/* <img onClick={() => setIndex(l => (l === data?.length-1 ? 0 : l+1))} src='/svgs/downArrow.svg' className='z-20 cursor-pointer -rotate-90 black absolute w-6 h-6 -right-8 bottom-[240px]'/> */}
                  </div>
                </div>
              )}
            </div>

            <div
              onClick={() => !scrolEnd && slide(305)}
              id="swiper-button-prev"
              className={`${
                scrolEnd ? 'opacity-40' : 'opacity-100'
              } white pointer absolute  bg-black rounded-full -right-5 flex  item-center justify-center cursor-pointer shadow-md `}
            >
              <svg width="20" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
          </div>
        </div>
      )
    )
  }
}
