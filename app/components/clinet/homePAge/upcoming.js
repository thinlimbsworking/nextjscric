import React, { useState } from 'react'
import { HOME_PRE_MATCH_CRICLYTICS } from '../../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import StatHubStadium from './stathubStadium'
import TeamAnalysis from './teamAnalysis'
import HomeageFrcTeam from './homepageTeam'
import PlayerIndex from './playerIndex'
import ContentPage from './contentPage'
import MatchReel from './matchReel'
import PlayerIndexMweb from './formIndexMweb'
export default function UpcomingMatchHome(props) {
  const [teamData, setMyTeam] = useState([])
  const { loading, error, data } = useQuery(HOME_PRE_MATCH_CRICLYTICS, {
    variables: { matchID: props.matchID },

    onCompleted: (algo11) => {
      if (
        algo11 &&
        algo11.upcomingMatchHomepageData &&
        algo11.upcomingMatchHomepageData.cricketDotComTeam
      ) {
        setMyTeam([
          ...(algo11.upcomingMatchHomepageData.cricketDotComTeam.batsman || []),
          ...(algo11.upcomingMatchHomepageData.cricketDotComTeam.all_rounder ||
            []),
          ...(algo11.upcomingMatchHomepageData.cricketDotComTeam.bowler || []),
          ...(algo11.upcomingMatchHomepageData.cricketDotComTeam.keeper || []),
        ])
      }
    },
  })

  if (loading) {
    return <div></div>
  }
  if (data) {
    return (
      <div className="flex flex-col w-full">
        <div>
          {props.tabContent == 'selectMatch' && (
            <div className="">
             
              {data &&
                data.upcomingMatchHomepageData.teamAnalysis &&
                data.upcomingMatchHomepageData.teamAnalysis &&
                data.upcomingMatchHomepageData.teamAnalysis.teamA
                  .overallRating &&
                data.upcomingMatchHomepageData.teamAnalysis.teamB
                  .overallRating && (
                  <div className="flex w-full ">
                    <TeamAnalysis
                      setShowTeamInfo={props.setShowTeamInfo}
                      showTeamInfo={props.showTeamInfo}
                      seriesName={props.seriesName}
                      matchID={props.matchID}
                      data={data && data.upcomingMatchHomepageData.teamAnalysis}
                    />
                  </div>
                )}
              {teamData && teamData.length > 0 && (
                <div className="m">
                  <HomeageFrcTeam
                    seriesName={props.seriesName}
                    matchID={props.matchID}
                    score={
                      data &&
                      data.upcomingMatchHomepageData.cricketDotComTeam
                        .totalProjectedPoints
                    }
                    matchStatus={'upcoming'}
                    teamtotalpoints={
                      data &&
                      data.upcomingMatchHomepageData.cricketDotComTeam
                        .teamtotalpoints
                    }
                    teamData={teamData}
                    teamName={data&&data.upcomingMatchHomepageData.cricketDotComTeam.fantasy_teamName}
                  />
                </div>
              )}
              <div className="flex w-full">
                <StatHubStadium
                  seriesName={props.seriesName}
                  matchID={props.matchID}
                  data={data && data.upcomingMatchHomepageData.stadiumDetails}
                />
              </div>

              <div>
                <PlayerIndexMweb
                  setShowFormIndex={props.setShowFormIndex}
                  showFormIndex={props.showFormIndex}
                  seriesName={props.seriesName}
                  matchID={props.matchID}
                  mainData={props.mainData}
                  score={data && data.upcomingMatchHomepageData}
                  teamData={teamData}
                />
              </div>
            </div>
          )}

          {/* {props.tabContent == 'selectContent' && (
            <div className="m-2">
              <div className="flex flex-col">
                <ContentPage matchID={props.matchID} />
              </div>
            </div>
          )} */}
        </div>
      </div>
    )
  }
}