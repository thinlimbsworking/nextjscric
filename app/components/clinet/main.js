'use client'
import { useState, useEffect } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { SOCIAL_TRACKER } from '../../api/queries'
import Script from 'next/script'
import Head from 'next/head'
import parse from 'html-react-parser'
// import TweetEmbed from 'react-tweet-embed'
import { TwitterTweetEmbed } from 'react-twitter-embed'
export default function ClientComponent({ children }) {
  const [windows, updateWindows] = useState()
  const callSocialWidget = () => {
    // alert(8)
    global.windows &&
      global.windows.twttr.ready().then(() => {
        global.windows &&
          global.windows.twttr.widgets.load(
            document.getElementsByClassName('twitter-div'),
          )
      })
  }
  const [counter, setCointer] = useState(0)

  const { loading, error, data } = useQuery(SOCIAL_TRACKER, {})

  useEffect(() => {
    if (global.window) updateWindows(global.window)
  }, [global.window])

  useEffect(() => {
    const s = document.createElement('script')
    s.setAttribute('src', 'https://platform.twitter.com/widgets.js')
    s.setAttribute('async', 'true')
    document.head.appendChild(s)
  }, [])

  if (data) {
    return (
      <div className="flex flex-col h-64 overflow-scroll">
        <Head>
          <script async src="https://platform.twitter.com/widgets.js"></script>
          <script
            async
            src="https://platform.instagram.com/en_US/embeds.js"
          ></script>
        </Head>
        {data &&
          data.getSocialTracker.data.map((item, index) => {
            return (
              <div className="relative   flex flex-col">
                <div className="">
                  <TwitterTweetEmbed tweetId={item.twitterID} />
                </div>
              </div>
            )
          })}

        {counter}
        <button onClick={() => setCointer(counter + 1)}>ssHIt</button>
      </div>
    )
  }
}
