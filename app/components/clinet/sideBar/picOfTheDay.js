// import React from 'react'
import React, { useRef, useState, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import {
  EffectCoverflow,
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Controller,
} from 'swiper'
import 'swiper/css'
import { Swiper, SwiperSlide } from 'swiper/react'
import Heading from '../../shared/heading'
import Link from 'next/link'
export default function WcricUpdte(props) {
  const swiperRef = useRef()

  return (
    <div className="flex w-full flex-col relative  ">
      <Heading heading={'Picture Of The Day'} />

      <div className="w-full flex items-center justify-center  rounded-md   mt-3   ">
        <div>
          <div className="flex flex-col  w-full ">
            <div className=" w-full  pb-2">
              {
                <img
                  className=" w-full rounded-md object-cover object-top"
                  src={`${props.data.imageUrl}?w=300&h=400&auto=compress&fit=crop`}
                  alt=""
                />
              }
            </div>
            <div className="   font-mnsb font-medium    text-xs text-gray-9  leading-4  line-clamp-2    ">
              {props.data.description}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

// // import React from 'react'
// import React, { useRef, useState, useEffect } from 'react';
// import { useRouter } from 'next/navigation';
// import { EffectCoverflow, Navigation, Pagination, Scrollbar, A11y, Controller } from 'swiper';
// import "swiper/css";
// import { Swiper, SwiperSlide } from 'swiper/react';
// import Heading from '../../shared/heading';
// import Link from 'next/link';
// export default function WcricUpdte(props) {

//   const swiperRef = useRef();

//  console.log("womensCricUpdatewomensCricUpdatewomensCricUpdate",props)
//   return (
//     <div className='flex w-full flex-col relative  '>

//         <Heading heading={"Women’s Cricupdates"} />

// <div className='w-full flex items-center justify-center  rounded-md    mt-3   '>
//       <div className='flex items-center justify-center  absolute left-0    '>

//        <div
//           style={{ zIndex: 10 }}
//           onClick={() => swiperRef.current?.slidePrev()}
//           id='swiper-button-prev'
//           className='white bg-basebg rounded-full  shadow-md dn db-ns z-999 outline-0 cursor-pointer'>
//           <svg width='20' focusable='false' viewBox='0 0 24 24'>
//             <path fill='#fff' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
//             <path fill='none' d='M0 0h24v24H0z'></path>
//           </svg>
//         </div>

//         </div>

//    <Swiper

//  onBeforeInit={(swiper) => {
//   swiperRef.current = swiper;
// }}
// modules={[Navigation]} slidesPerView={1} className="mySwiper"
// // direction='verticle'

//                     updateOnWindowResize
//                     observer
//                     observeParents

//                     // onSwiper={setSwiper}
//                     // onClick={(swiper)=>HandleSwipeCLick(swiper.realIndex)}

//                     //   onSlideChange={(swiper)=>setPag(swiper.realIndex)}
//                       shouldSwiperUpdate={true}

//                         grabCursor={true}
//                         centeredSlides={true}

//                         loop={true}

//                     >

//         {true &&props.data.map((item, index) =>  {
//             return  (
//               <SwiperSlide key={index} className="">
//                    <Link href={`/news/${item.cricShortsId}`}>
//                   <div className='flex flex-col  w-full '>

//               <div  className=' w-full '>

// {<img className=' w-full rounded-md object-cover object-top' src={`${item.imageUrl}?w=300&h=400&auto=compress&fit=crop`} alt="" />}

//               </div>
//               <div className=' ml-1    font-mnsb font-medium    text-xs text-gray-9  leading-4  line-clamp-2    '>
// {item.description}
//               </div>

//               </div>
//               </Link>
//               </SwiperSlide>
//             )
//           })}
//       </Swiper>

//       <div className="flex items-center justify-center absolute right-0  "  >
//        <div
//           style={{ zIndex: 10 }}
//           onClick={() => swiperRef.current?.slideNext()}
//           id='swiper-button-prev'
//           className=' bg-basebg rounded-full white  dn db-ns z-999 outline-0 cursor-pointer'>
//          <svg width="20" viewBox="0 0 24 24"><path fill="#fff" d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"></path><path fill="none" d="M0 0h24v24H0z"></path></svg>
//         </div>

//         </div>

//     </div>
//     </div>
//   )
// }
