import React, { useState, useEffect } from 'react'
// import { useEffect } from 'react/cjs/react.production.min'
import CommonDropdown from '../../commom/commonDropdown'
import ImageWithFallback from '../../commom/Image'
import Heading from '../../shared/heading'

export default function Point(props) {
  const [articlesFilter, setArticlesFilter] = useState([
    {
      valueLable: props.data && props.data.listOfTours[0],
      valueId: props.data && props.data.listOfTours[0],
    },
  ])

  const [activefilter, setActivefilter] = useState({
    valueLable: props.data && props.data.listOfTours[0],
    valueId: props.data && props.data.listOfTours[0],
  })
  function activeIndex() {
    const idx = props?.data?.data?.findIndex(
      (element) => element.tourName == props?.data?.listOfTours?.[0],
    )
    return idx
  }
  const [activeTabIndex, setActiveTabIndex] = useState(activeIndex())
  let filterTemp = []
  useEffect(() => {
    props.data.listOfTours.map((item) => {
      filterTemp.push({ valueId: item, valueLable: item })
    })

    setArticlesFilter(filterTemp)
  }, [activefilter, activeTabIndex])
  return (
    <div className="flex flex-col w-full ">
      {' '}
      <Heading heading={'Points Table'} />
      <div className="flex w-full flex-col   rounded-md mt-1">
        <div className="flex w-full pt-2 ">
          <CommonDropdown
            options={articlesFilter || []}
            setActiveTabIndex={setActiveTabIndex}
            isFixedHeight
            onChange={(option) => setActivefilter(option)}
            defaultSelected={activefilter}
            placeHolder={activefilter?.valueLable}
            showExpandIcon
            variant="outline"
          />
        </div>
        <div className="flex flex-col  ">
          <div className="flex items-center text-sm  leading-6 justify-evenly mt-3 border-b   pb-2">
            <div className="flex items-center text-sm  leading-6   w-3/12 font-mnr font-medium">
              {' '}
              Team
            </div>
            <div className="flex items-center text-sm  leading-6 justify-center w-1/12 font-mnr font-medium">
              M
            </div>
            <div className="flex items-center text-sm  leading-6 justify-center w-1/12 font-mnr font-medium">
              W
            </div>

            <div className="flex items-center text-sm  leading-6 justify-center w-1/12 font-mnr font-medium">
              L
            </div>

            <div className="flex items-center text-sm  leading-6 justify-center w-1/12 font-mnr font-medium">
              T
            </div>

            <div className="flex items-center text-sm  leading-6 justify-center w-1/12 font-mnr font-medium">
              NR
            </div>
            <div className="flex items-center text-sm  leading-6 justify-center w-1/12 font-mnr font-medium">
              PT
            </div>
            <div className="flex items-center text-sm  leading-6 justify-center w-2/12 font-mnr font-medium">
              NRR
            </div>
          </div>

          {props.data &&
            props.data.data &&
            props.data &&
            props.data.data[0] &&
            props.data.data[parseInt(activeTabIndex)] &&
            props.data.data[parseInt(activeTabIndex)].pointsData &&
            props.data.data[parseInt(activeTabIndex)].pointsData.standings &&
            props.data.data[parseInt(activeTabIndex)].pointsData.standings[0]
              .teams &&
            props.data.data[
              parseInt(activeTabIndex)
            ].pointsData.standings[0].teams.map((item, index) => {
              return (
                <div className="flex  my-3">
                  <div className="flex justify-evenly w-full ">
                    <div className="flex items-center justify-evenly  w-3/12  text-xs">
                      <img
                        className="h-6 w-8  rounded"
                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngsV2/flag_dark.png')
                        }
                        alt=""
                        srcset=""
                      />

                      <div className="px-1 w-6/12"> {item.teamShortName}</div>
                    </div>
                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5  w-1/12  ">
                      {item.all || '--'}
                    </div>
                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5  w-1/12 ">
                      {item.wins || '--'}
                    </div>

                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5   w-1/12">
                      {item.lost || '--'}
                    </div>

                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5  w-1/12 ">
                      {item.noResult || '--'}
                    </div>

                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5  w-1/12 ">
                      {item.noResult || '--'}
                    </div>
                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5  w-1/12 ">
                      {item.points || '--'}
                    </div>
                    <div className="flex items-center text-xs justify-center font-mnr  fomt-normal  leading-5  w-2/12 ">
                      {item.nrr || '--'}
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      </div>
    </div>
  )
}
