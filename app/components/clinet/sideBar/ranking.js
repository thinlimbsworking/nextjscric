import React,{useState,useEffect} from 'react'
import { useQuery } from '@apollo/react-hooks'
import { RANKING_PLAYER_HOMEPAGE } from '../../../../api/queries'
import Heading from "../../commom/heading";
// import { useEffect } from 'react/cjs/react.production.min';
import parse from 'html-react-parser'
import Head from 'next/head';
import Button from '../../shared/button'
import Link from 'next/link'
export default function Ranking(props) {
  // https://staging.devcdc.com/players/all
  // https://staging.devcdc.com/teams/international
  // https://staging.devcdc.com/rankings
   
    const [tabData, setTabData] = useState([{name:"Test",label:"Test"},{name:"ODI",label:"ODI"},{name:"T20",label:"T20"}]);
    const [tabDataBottom, setTabDataBottom] = useState([{name:"Team",label:"Team"},{name:"Batter",label:"Bat"},{name:"Bowler",label:"bowl"},{name:"ALR",label:"allrounder"}]);
    const [selectedTabData, setSelectedTabData] = useState({name:"Test",label:"Test"});
    const [selectedTabDataBottom, setSelectedTabDataBottom] = useState({name:"Team",label:"Team"});
    const [innerHeading, setInnerHeading] = useState([{name:"Rank",label:"Rank"},{name:"Team",label:"Team"},{name:"Rating",label:"Rating"}]);
    const [innerHeading2, setInnerHeading2] = useState([{name:"Rank",label:"Rank"},{name:"Player",label:"playerName"},{name:"Team",label:"teamName"}]);
    const { loading, error, data } = useQuery(RANKING_PLAYER_HOMEPAGE, {
        
      })
if(loading)
{
    return  (<div>loading</div>)
}
if(error)
{ return  (<div>error</div>)
}
let f='{"type":"twitter","EmbeddedText":"<blockquote class=\\"twitter-tweet\\"><p lang=\\"en\\" dir=\\"ltr\\">🗣 &quot;<a href=\\"https://twitter.com/mipaltan?ref_src=twsrc%5Etfw\\">@mipaltan</a> have beaten us, but so had <a href=\\"https://twitter.com/SunRisers?ref_src=twsrc%5Etfw\\">@Sunrisers</a>&quot;<br><br>Stoinis is confident that <a href=\\"https://twitter.com/DelhiCapitals?ref_src=twsrc%5Etfw\\">@DelhiCapitals</a> can turn it around in the final <a href=\\"https://twitter.com/hashtag/IPL2020?src=hash&amp;ref_src=twsrc%5Etfw\\">#IPL2020</a> <a href=\\"https://t.co/vZlQorDSj2\\">pic.twitter.com/vZlQorDSj2</a></p>&mdash; ESPNcricinfo (@ESPNcricinfo) <a href=\\"https://twitter.com/ESPNcricinfo/status/1325729843636367362?ref_src=twsrc%5Etfw\\">November 9, 2020</a></blockquote> <script async src=\\"https://platform.twitter.com/widgets.js\\" charset=\\"utf-8\\"></script>","author":"ayushi","isHomeSocial":false,"instaUri":"","title":"Stoinis is confident that  @DelhiCapitals  can turn it around in the final #IPL2020"}'
// useEffect(()=>{
//   selectedTabDataBottom.label=="Team"? setInnerHeading([{name:"Rank",label:"position"},{name:"Team",label:"Team"},{name:"Rating",label:"Rating"}]):setInnerHeading([{name:"Rank",label:"position"},{name:"Player",label:"playerName"},{name:"Team",label:"teamName"}])
// })
const callSocialWidget = () => {
  global.windows&&global.windows.twttr.ready().then(() => {
    global.windows&& global.windows.twttr.widgets.load(document.getElementsByClassName('twitter-div'));
  });
 
};
if(data)
{
  
  return (
    <div className='flex w-full  flex-col'>
       {<div className='flex items-center justify-evenly w-full'>
      <div className='flex w-full justify-start rounded    items-start '>
      <Heading  heading={"Ranking"}  />
       
      </div>
      <div className='flex w-full justify-end items-start rounded  py-2  '>
        <Link href={'/rankings'}>
      <button className='py-1 px-3  hover:opacity-70 transition ease-in duration-150  rounded text-red-5 border border-red-5 text-[0.66rem]' >View All</button>
      </Link>
      </div>
    </div>}
        <div className='flex flex-col pt-3  w-full  rounded-md '>
        <div
          className={`w-full flex items-center justify-evenly   dark:border-none dark:bg-gray rounded-full `}>
        
        
          {tabData.map((item,index) => (
            <>
          
            <div
              onClick={() => {
                setSelectedTabData(item)
               
              }}
              className={`flex rounded-full p-1 justify-center items-center cursor-pointer truncate  w-full m-1 ${
                item.name === selectedTabData.name
                  ? 'text-white bg-red-6 dark:border dark:border-green dark:text-green dark:bg-transparent'
                  : 'bg-light_gray dark:bg-transparent '
              }`}
            >
              <span className='font-mnr  leading-5 text-sm font-medium '> {item.name} </span>
            
            </div>
            </>
          ))}
        </div>
     
      
    
            <div
          className={`w-full  p-1 text-sm font-semibold flex text-black dark:text-gray-2 `}
        >
          {tabDataBottom.map((item,index) => (
            <div
              onClick={() => {
                setSelectedTabDataBottom(item)
              }}
              className={`flex gap-2 justify-evenly items-center md:p-2 lg:p-2 p-1 cursor-pointer border-b dark:border-b-2  w-full ${
                item.name === selectedTabDataBottom.name
                  ? 'border-b-2 border-red-6 text-red-6 dark:border-gray-3 dark:text-gray-3  '
                  : 'border-gray-2'
              }`}
            >
              <span className='font-mnr text-xs  leading-5 text-gray-9'> {item.name} </span>
           
            </div>
          ))}
        </div>
        <div className='flex w-full items-center py-2'>
          { selectedTabDataBottom.label=="Team" && innerHeading.map((item,index)=>{return(
              <div className={`flex ${index==0?'w-2/12': index === 1 ? 'w-6/12':'w-4/12'} `}>
              <div className={` w-full  font-mnr text-gray-9 text-sm font-medium leading-5 ${index === 2?"text-center":"ml-2"}`}>   { item.name } </div>
             
           
            </div>
          )})}
            {selectedTabDataBottom.label!=="Team" && innerHeading2.map((item,index)=>{return(
              <div className={`flex ${index!==1?'w-2/12':'w-6/12'} `}>
              <div className={` w-full flex items-start justify-start  leading-5 text-sm text-gray-9 font-mnr ${index === 1 ? "ml-1":""} font-medium       font-mnr  line-clamp-1 `}>   { item.name } </div>
             
           
            </div>
          )})}
        </div>
        <div className='flex w-full flex-col'>
       
          {props.data?.[selectedTabData.label]?.men?.[selectedTabDataBottom.label&& selectedTabDataBottom.label.toLowerCase()]?.rank.map((item,index)=>{ return index<5&& (
            <div className='flex w-full p-1  items-center justify-evenly'>
              <div className='flex  w-2/12   text-ellipsis  truncate  font-mnr leading-6 text-xs text-gray-9 pl-3'>  { item.position}  </div>
              <div className='flex       truncate w-6/12 font-mnr px-1  text-xs leading-6  text-gray-9 '>  {selectedTabDataBottom.label=="Team" && item.teamName}  {item.playerName} {" "} </div>
              {/* {console.log("props.data?.[selectedTabData.label]?.men?.team?.rank",item.Rating)} */}
              <div className={`flex w-4/12   items-center   ${selectedTabDataBottom.label == "Team" ? " justify-center " :""}  text-xs  truncate  font-mnr 
              leading-5 text-gray-9 font-medium       font-mnr  line-clamp-1 ${selectedTabDataBottom.label == "Team" ? "text-center" :"text-start"}
              
              `}>   {selectedTabDataBottom.label=="Team" &&item.Rating ||item.teamName}
           
              </div>
           
            </div>
          )})}
        </div>
            </div>
    </div>
  )
}
}