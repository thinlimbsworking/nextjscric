'use client'
import { useState, useEffect } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { GET_WEB_HOMW_PAGE_DATA } from '../../../api/queries'
import WomenCric from './wcricUpdate'
import PicofTheDay from './picOfTheDay'
import ImageWithFallback from '../../commom/Image'
import Link from 'next/link'
// import HomeageFrcTeam from '..'
// import Heading from "../";
import HomeageFrcTeam from '../homePAge/homepageTeam'
import Ranking from './ranking'
import Point from './pointsTable'
import SocialIcon from './socialIcon'
import Tw from '../main'
import TeamSidebar from './teamSideBar'
import Heading from '../../shared/heading'
export default function ClientComponent({ children }) {
  const [windows, updateWindows] = useState()

  const [counter, setCointer] = useState(0)

  const { loading, error, data } = useQuery(GET_WEB_HOMW_PAGE_DATA)
  const frc = '/pngsV2/FantasyStatsIcon.png'
  const crycLyticsLogo = '/pngsV2/CriclyticsIcon.png'

  if (data) {
    return (
      <>
        <div className="flex justify-center  items-center   cursor-pointer bg-slate-100">
          <Link href="/criclytics">
            <div className="flex relative items-center justify-end font-semibold dark:text-white text-sm">
              <img
                src="/pngsV2/crichome2.png "
                className=" w-full h-full object-top object-cover"
              />
              <div className="text-sm absolute flex  items-center justify-end font-ttb font-semibold mr-6 tracking-wider    text-white  ">
                CRICLYTICS
              </div>
            </div>
          </Link>


          <Link href="/twitter">
          twittertwittertwittertwitter
          </Link>
        </div>
        {/* <div className="flex justify-start items-center py-2 w-full text-xl">
            <Heading heading={'Game Centre'} />
          </div> */}
        {/* <div
          className="flex justify-center  items-center py-2  mt-2 cursor-pointer"

        >
         
          <Link
      href="/play-the-odd"
    >
      
        <div
            className="flex relative items-center justify-start font-semibold dark:text-white text-sm w-full"
           
          >
            <img src="/pngsV2/ptohome.png" className="" />
            <div

              
              className="text-sm absolute flex  items-center justify-start  ml-2   font-ttb font-bold  text-white  "
            >
             PLAY THE ODDS
            </div>
          </div>
          </Link>
          
        
        </div> */}

        <div className="flex flex-col w-full">
          <div className="flex items-center justify-center  bg-white p-4 mt-5 rounded-md shadow-sm">
            <TeamSidebar
              data={data && data.getwebHomePageDataSideBar.fantasycentre}
            />
          </div>
          {data &&
            data.getwebHomePageDataSideBar &&
            data.getwebHomePageDataSideBar.womensCricUpdate &&
            data.getwebHomePageDataSideBar.womensCricUpdate.length > 0 && (
              <div className="flex items-center justify-center  bg-white p-3 mt-5 rounded-md shadow-sm">
                <WomenCric
                  data={
                    data &&
                    data.getwebHomePageDataSideBar &&
                    data.getwebHomePageDataSideBar.womensCricUpdate
                  }
                />
              </div>
            )}
          {data &&
            data.getwebHomePageDataSideBar &&
            data.getwebHomePageDataSideBar.pictureOfTheDay && (
              <div className="flex items-center justify-center   bg-white p-3 mt-5 rounded-md">
                <PicofTheDay
                  data={
                    data &&
                    data.getwebHomePageDataSideBar &&
                    data.getwebHomePageDataSideBar.pictureOfTheDay
                  }
                />
              </div>
            )}

          <div className="flex items-center justify-center  bg-white p-4 mt-5 rounded-md shadow-sm">
            <Point
              data={
                data &&
                data.getwebHomePageDataSideBar &&
                data.getwebHomePageDataSideBar.pointsTable
              }
            />
          </div>

          <div className="flex items-center justify-center  bg-white p-4 mt-5 rounded-md shadow-sm">
            <Ranking
              data={
                data &&
                data.getwebHomePageDataSideBar &&
                data.getwebHomePageDataSideBar.playerRanking
              }
            />
          </div>

          <div className="flex items-center justify-center  bg-white p-4 mt-5 rounded-md shadow-sm">
            <SocialIcon />
          </div>
          {/* <div className="flex items-center justify-center ml-4 py-2">
<Tw />    
</div>       */}
        </div>
      </>
    )
  }
}
