import React from 'react'
import Link from 'next/link'
export default function SocialIcon() {
  return (
    <div className="flex w-full ">
      <div className="flex flex-col border items-center justify-arround w-full rounded-md p-2">
        <div className="flex flex-col font-mnr text-lg font-bold">
          Follow cricket.com
        </div>

        <div className="flex w-full items-center justify-evenly mt-3">
          <div className=" flex  items-center justify-center ">
            <Link href="https://twitter.com/weRcricket">
              <img
                className="h-12 object-cover object-top"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAACCCAMAAACAT4GpAAAAY1BMVEUdm/D///8Alu8AlO8Aku8TmfD7/f8AkO8Aju7c7Pzq9P3y+P71+v7O5fvB3vqRxfa42fmCv/U0ofF7u/Sx1fhPqvKdzPdXrfJvt/TU6Pvi7/yq0viIwvVApfHH4fqXyPZksvNvjVIJAAAHKElEQVR4nMWcadeqIBCAFRz3XDC3zOr//8pX63VJQEHQ5pz75d6rPQ3MypBhniZh4Pt+YEs9YxzE8i32o6jKvO0kz8v71RdmPAGvLnLHQgDGWwAQdo00CYSePRovTHJA/2QzQUDut7Xn7Cg8Hs9uiEWz/RPizOM+6JWleTjew0Actg8gynw23As5l6Pxwgqvwb0BSUQ/V1cIQR4u8eycr+wd8kCrqvsXN/u2Yv9KnG47WIm5xHs4uZg5CUkDvE23UOC/nnqx64zg/jEAk8LrTCzXRvd0heB6FPJZND+6G87/V0LDmk94l+6fcKqJLhVZ2Ikv8J4lgfEZVA3vMb7fiO9a6ApLnK7j67Ygmu8FPBr0iGd/2PFTA12zabIU31zQZM4j3uX/C+NCma6WWFmGWLMdNuJlw1fAsSKdT8RsliPTxpvhhfn4TlX9ZUrKQ+3cEQ543mwD4EyFLlKjKyc67+kPeI/5bkZZyP5oAbFblaVF+UAXRCVuRu19GxveHz8SKZ+yECe2/9lSYlnVtLj37yVBpN5HF7T74d4Jgu17Rd7lrABtOOFVix0DxmMXnsrOy7zb9f4i1ifs4l5BA1653DHgxnJVy0eUdh7GU/DAb/UMeDkj4S7lDSQQzgQ2xPpErxW8Lq25yuIVkuFsg46/uG9BqaQFV0oBY/rcIXLwTONfcCtlIYrxbPzUMYUe8GKOxQFO2eUKU2ota4uq0SgHvCv3xYgkwnj8t0iINSs/xqC24q9wK+qjZZJkjsBXRsJKCehHUCW2wmwDk5Iu0DLwQpZnmQQ5qUiVSVThkPG9UHQ6yn0wXW2KfN6mqD2ULz5jxHs4G48CstLLBp4iHVCJ5qwU2n43gjxa3YSqyqN8xFRICuXgYJHnyiZUxaOC6HcZLvQOq208TjKjaBp4BY+ZFTAFMLwK5iILv4ItqFm+cIZ3kUjDATnGPbktlahWpK3j2ZJOFWGSV8ktnDEq5lNre29HQO/b2I5Rpk1SfxSpmBKs4+0LmR1j75MIyV9pqrj3VhyL2ZdZu1/f1Qg9pxLdFp7pCfjmAwWoTvOi9a2rVtgpVNBcdublGod6BQwqIP3j2aN3SH+nPyCUrx/wpprx+Tv9tVSsHBbXRaT4PyhsfmUfQB8MjHhdQEZV8c4Go9/QGVBx8d55bue4UBkn9UVPuSoriG67U02MLk5h9Xprl2C6Yh3wNJSAyuLQxcx2GX6eAN3QGfBEc+Uj6XK6YTfg+QpNV02CGAd6Y1DT1PpSwaNy5Rle9MNg+xFgVNFSde7BeIxe8ZSx8Dp8p9GxzronvPq3dKyY8ZXvcfq3ZwlmNRFneLffeuaWVdjPs+WfBja6O7XEC5W7hwpCV2lLPDP54fK6zK7Sdyn0/BkflCy6ZaXW/mr7MXI9Bp5Cn0BJgLAb18s611ftXu/Eo8sMJp7pk1+sr8M5uqPn94L8fD7gjREy/j6sdJ0ZCwsz3nLwOv8H5yoQDN6JDlurtxc+00LgxaHjzo4+Su7IrH5xuCee3NFW+1GyBo6PECi5Mx9rk7fes3XPiHLsbGATrxO/yQ+nA8L/fAbetYjqWxD43uUaV+XxSZa1MgzAwEssbFmW0/3BSHC6V0VWdh4Tzz436nJyFS7euVkftCt0TLzbmVkLWp3jYVruiRU54mRSa3j2eXh4fQKF7fdk56L3021McXPc8kkdA8g3Rhg5eP456rO2Jk94Qe1yhvrQ5oA5N+aesP2AbE6n8lOC+HA+tLW0qxnL0d5PZDZ/LaEq3COjh9DNn9V8LzoyutGHt7J45o0cVhI52xtvE69b4INqStYhxg4881apjn+wRPRKiMAlxLrSfoKK2N28XXimGcStobOohFZ0mFzwCqd9aV7Y1VR8MAZCFPHeEjyuz7sGOtiekd2D14mdacCTGMOXw3uouxkAmWs+Mnh2pr7zJC/5SOAlGtq6crqTwOvcs7rRIiJ5gVoQL4zXb6EL0rXiNiuB5xdaWn2I2wRVwQtjLXkLOHf5m2VbePYlc7RE3B3Xtrbw7KDINeUr0kaxiVdfX1hXNmrtvLPKxgv8KNWYpCBD/DbUAm+5XQMveWYtsjQmUM5L1p/M8BqSV+k9juN7VpUG6kVryxaRPTYx4PVHQC5GHwHtpRlYgpe0uHidEeQHdQTAKndeQ57jmWGkNVkf4BBJ9tzxpfA6F3fVXtJaRP0XGCbHEl5bjSYBqG32X89n4HUaTEpXyxoDOG2iAY5yy16mvsaASaXrd3GoqHG7EkdBhYBc0uz2wtt4Zp/elWiXDgHjMlZxc0J4nZl4TS4ZPfqB9raptey4Lbw3YhSX4ODtQNL9D+zAKxb83TBNeGb/AxCX4tUSA/eKZP4yQKcyg7Svphb/1TV9eB/xL0mRvlpw3f6M13qf9lqO61pGXt2bpNa62eTxPmLbdnCrL48oSqLocfH80D5GYV9ywg/cqcgfqqxZLKcXLJEAAAAASUVORK5CYII="
                alt=""
              />
            </Link>
          </div>
          <div className=" flex  items-center justify-center ">
            <Link href="https://www.instagram.com/cricket.com_official/">
              <img
                className="h-16 object-cover object-top "
                src="https://cdn.pixabay.com/photo/2016/08/09/17/52/instagram-1581266_1280.jpg"
                alt=""
              />
            </Link>
          </div>

          <div className="flex items-center justify-center">
            <Link href="https://www.youtube.com/@cricketcomtv">
              <img
                className="h-16 object-cover object-top "
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAw1BMVEX////+AAAoKCgAAAAjIyMdHR0QEBD/y8skJCTa2tp1dXX5+fkZGRkMDAzExMRFRUXOzs7i4uJcXFyOjo6Ghoa4uLg9PT2qqqrh4eEaGhoyMjJoaGhVVVV+fn54eHgsLCzu7u7+wsH/9fb+5ub/9vb+oqL/19f+j4/+ZWb+Vlb+Pj7+Nzf/goH+bWz9m5v+xMT+09P/sbKfn5/+4eD+SEn+LCv+HRr+eHb+ior9UVH/oaD+urr/amr+LS79JCOwsLD9EBTL31AIAAAInklEQVR4nO2cCVfiOhSAu0KhlF2U7VEKIqBPiziij9GZ//+rXm5amLoNEnIT9NxvzpkpUDr5aJabm7aGQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQXwBfH/sj6NoNFouJ3Gcz9/cDIfDNfszHN7c5PNxPFkuR6MoYnv5vu7C7sKPmEk8XJxfXzxcraazu8sf9+Z+3P+Y382mq6uHi+vzxTBm5tFRaI9H8eJhNb173tPnMzzfTVcPi3g01qe3vJ4+IZi95ml2vdTid/OowG7D441yv9FMoR8wG6kVHCr2A4YqBRcaBE1zoU5wrUXQNNeqBCeaBE1zoshQxRDxPk9qBK+1CZrmtQrBaN94TCb3kQJDPf3oBhX9qeqh/iUzfMHRb62Gv/FDm7xWQdPMoxteaDa8QDf8V7Phv9iC/lyz4Rx75h9pFjRN7BFxpFvQxO5Mb3QLmtiz/XPdguY5suGDbkHzAdlwpVvQXCEbTnULmlNkQ5Hh8OJSpuEcV3AsMjmMpfZP97g58LFImViwHElsv7iGQiENnw4s72QZ4gY1QiFNOuFZ/5BjiBvUCCUSN1O6sZwcFm5KUSho+zNpHclojrhhm1C2Ozstjw9vjriZb6FEW/7wQ2TBTbcJDWyvUiv+gbEtbugt1Fe8SR4dtviIm/cWykO9kx7LHxDI4eaiZBmy+i6cd8U1FGpC76c4oytBQ9wJolCpPkriTsSa4xWqodCI/XGaeigSyOFOgSUbCnXOuIZCGe+/LjWM9z4kbtb7p3RD1hz3DOR+ohoKpWl2Lhet97oyADdRg2OY3+ssfj3D5Z4Hxa2l8nuaaO8gArenkT5aLPYP3r7UeBiLBOC4hlKjtpFYAh03apMYeQvnpXAjb3mzp7XwpeFfY34YH3AJNa6hpCzGQUlF3CyGjEzUoRc34maiJGQTh4fem4GbTTw4IyxhgQY3I3xgVn8smpvJgpvVP2hlRtJCKe7KzCGrazeSLhjDXV07YIVU2jUOuCukoqvcY4nX4SCv44tdqbCQePU78pUKQlebXEtbwgeQrzbRfB07gH0t+/e/6uv7X7n3/a++/P5X0C51C5rYdz1//+u8x9rvRkC/PV9o9UkiuDl94PvfFfT97+zS3dUoePCAyodFvOURX1BzVIMd0QCRzptIf6u401lrb4rfkwI+xuN2PsezoscO6Rsw8IeKFF2djYpuJkXPgyOUPDJig44HKyh8PA2wVD3HmKt/GpasW2A+xQ9lD995QSz3trsPubyItfhxonj432o2f8J4osv903y2+m8YKwljdhItJ3F+fX5xtfo5nT3ezS+fnu/3ie5+3z8/Xc7vHmfTn6uri/N1Pp4sj8PsI/gTIeGZkMvlZJI8F3K4Xq8XCWxrmDwbcjKBXWDXKNL49ECCIAiCIAiCIAiCICTiA7oLgUnBBqq6i5FS6NXr9V45eVHmL87+/o0q7JShd/rmmLZlWbljMfRLruPkGsmLfs5x3M6O+lW2nRcEjdd7HJehUXQty6kk2xXHstziji+Uc9YLSsdueAoFtvlmCAXO/drxheQcJnawlTt2wxoUxy7AZhVk3dqOL1R7jHoHBAd12D7ydmj4UDWTE3dbYmel97mvQeX2Ku9/dmSGRrPNTtw/sNV13mtV75Ntvq85NsNywM5Gh234Le/zBftKhrx/cUNWMKh4nTB5d1dQ8jnD1wfRFOlA5YTynLCT6XZ5QX4Ve61KvZmehrNTAHqgMmzcwntbw5B/WM3utjGsNXqt3u1WqtysVyq9/olyQd7BlG6TBsm7nEIrcB3Pc9o2b57GiZ3L5WyIe/p8y88anvG3mtndEsNC2Sk5nhP0EsWwbrdhcHHteqjasMAGCYeduzqM92zYqFnOZjTP8eH/BEaRAAzhR7BeGtbam1F/uxs3dG9djx8j4OfcqJS2IUJdtSH0MF7LCDv8H1ZrWeGtdhB4Gy8hQ/amHfCooAL7n8J2iZ1VOEBZtSIvba3AytBuJqfUKjWNGjNO2qWYYa4ZlgfcFEL5gZfsVf9MYCgdKFpQ/cX+zpWTZmkNwsyGWC0FjUZp81aQdth8bBqoNqzBSH/bgI7GTyopj2x4EAfxnJAhHy34IaAXg888qK4hDxJD1YoscHO6bNAAMR7F8coJ4yPvXMUN+SGg6jd4QMi+6P8Jg1UC/3+nxUrFguiQNz+oYnBqednFDWs8muinu/M+FD4LlMc71XTKB79tCF48Tq156QkQNwwHaYuEDiZjqLwz9d10qIISl14YwgmQYNjbGkKXE6iPa+rOpjpJNuxkDXtVBhw+p96QT/STnxbP0PJYVMf/Iw2GZ3yMDnxcwy0aaqkBDTGZ3mMaeml6ztZguI3QUNvh2QYN08Q/hqGLO1roImNobQ1ljfj/pKHgsRi2NjHNmZMJKg+M2vqbYNfoVurdbqjT8M/gXNhMDCRE3jAcJRPFisum+TuWRpANeSYUZsI8fV+qJTOeZAxrum8Nebttvm/IJ2AQhXJVmIkZfMISajWEeaLVrqZtB373apC2Jh6QvDL0+VjOdgtbTtbQ7frJJNry2I/k59JhENJdnqVeMGsYshJYXqfBkxl8UYLP6axStxs4nTeGBhdze8W228kaWiWvx5c3kuZX5KFvs8/rdF+vodHg6ZU2L12SPq3w8RoWYYrOG8MGD8Qct12/bWcNe66TCWD48ojlgqfX1tAMjY7jee3U0CjaaYxVcpOJXNXmabOgazRLnpcYsg2XG/olfnpKnfAkYB9yQ/gstHidzqVHPbHdzUGVz52ASqvV6mwTRL8qth0EttvfrEOdDOzAbrMae9pptRww7LONVrKKU6iwD+1iaJwNWq0B+00KFjvYrREW2dt2fxO/FLo5OKjX3LW4pYawWq7WsrFVoVz4ONSqlavhe+/7hRfvs5fV49AjCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIg/gfFXd/RiureowAAAABJRU5ErkJggg=="
                alt=""
              />
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}
