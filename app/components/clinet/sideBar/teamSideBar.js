import React from 'react'
import Button from '../../shared/button'
import Heading from '../../shared/heading'
import HomeageFrcTeam from '../homePAge/homepageTeamHome'

export default function TeamSideBar(props) {
  const MatchName = (item) => {
    let matchId = item.matchid

    return `/fantasy-research-center/${matchId}/${
      item &&
      item.matchName
        .replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
    }/fantasyTeam`
  }

  return (
    <div className="flex flex-col  w-full">
      {props.data.map((item, index) => {
        return (
          <div className="w-full flex-col flex">
            {
              <div className="flex items-center justify-evenly w-full">
                <div className="flex w-full justify-start rounded    items-start ">
                  {index == 0 ? (
                    <Heading heading={'Fantasy Centre'} noPad />
                  ) : (
                    <div className="" />
                  )}
                </div>

                <div className="flex w-full justify-end items-start rounded  py-2   ">
                  {/* <Button url={MatchName(item)} title={"View All"} /> */}
                </div>
              </div>
            }
            <div className="">
              <HomeageFrcTeam
                score={'1212'}
                item={item}
                teamData={item.maxProjection.players}
              />
            </div>
          </div>
        )
      })}
    </div>
  )
}
