import React, { useEffect, useState } from 'react';
import Image from 'next/image';

const ImageWithFallback = (props) => {
    const { src, fallbackSrc, ...rest } = props;
    const [imgSrc, setImgSrc] = useState(src || fallbackSrc || '');
    useEffect(() => {
        setImgSrc(src || fallbackSrc || '')
    },[src])
    return (
        <img
            {...rest}
            src={imgSrc}
            onError={() => {
                setImgSrc(fallbackSrc || '');
            }}
        />
    );
};

export default ImageWithFallback;