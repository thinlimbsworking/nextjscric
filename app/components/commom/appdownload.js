import React from 'react'
import CleverTap from 'clevertap-react'
const RightSchevronBlack = '/svgs/RightSchevronWhite.svg'

const AppDownload = ({ isAndroid, url, isMore }) => {
  // console.log( "Sss=?>>>>>>>>>>>>>", global&& global.window&&global.window.localStorage &&global.window.localStorage.Platform&&window.navigator.Platform)
  return (
    <div className="bg-green-4 shadow-5  " style={{ padding: '14px' }}>
      <a
        href={url}
        download
        target="_blank"
        rel="noopener noreferrer"
        style={{ textDecoration: 'none' }}
      >
        <div
          className="flex justify-between items-center"
          onClick={() => {
            CleverTap.initialize('DownloadApp', {
              Source: isMore ? 'More' : 'Homepage',
              Platform: global.window.localStorage.Platform,
            })
          }}
        >
          <div className="flex items-center">
            <img
              src={isAndroid ? '/svgs/android.svg' : '/svgs/i-os-icon.svg'}
              style={{ width: 20, height: 20 }}
              alt="android"
            />

            <span className="white ml2 fw6 f6">DOWNLOAD APP</span>
          </div>

          <img
            className="h-4 w-2 "
            width="10"
            alt="RightSchevronBlack"
            src={RightSchevronBlack}
          />
        </div>
      </a>
    </div>
  )
}

export default AppDownload
