import React from 'react'

export default function ArrowIcon({ width, height, clr, className }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke-width="1.5"
      //   stroke="currentColor"
      className={className}
      width={width || 21}
      height={height || 32}
    >
      <g fill={clr || '#FFF'} fill-rule="evenodd">
        <path
          stroke-linecap="round"
          stroke-linejoin="round"
          d="M19.5 8.25l-7.5 7.5-7.5-7.5"
        />
      </g>
    </svg>
  )
}
