'use client'
// import React, { useState } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import React, { useContext, useEffect, useState } from 'react'

import CleverTap from 'clevertap-react';
import { LocalStorageContext } from '../layout'

const  FanActive = '/pngsV2/fanactive.png';
const InFanActive='/pngsV2/fantasy.png';
// import tabIcon2 from '../../public/pngsV2/schedule.png';
const  tabIcon2Active = '/pngsV2/tabicon2.png';
const  tabIcon1 = '/pngsV2/home.png';

const  tabIcon1Active = '/pngsV2/tabicon1.png';
const  tabIcon4 = '/pngsV2/cricupdates.png';
const  tabIcon4Active = '/pngsV2/tabicon4.png';
const  tabIcon5 = '/pngsV2/more.png';
const  tabIcon5Active = '/pngsV2/tabIcon5.png';
const  criclytics_active = '/pngsV2/criclytics_active.png';
const  criclytics_inactive = '/pngsV2/criclytics_inactive.png';
const tabIcon2 = '/pngsV2/schedule.png';
const feedIcon = '/svgsV2/feeds.svg';
const feedIconActive = '/svgs/activefeed.svg';



export default function BottomNavBar(props) {
  // const { path } = props;
  const pathname = usePathname()

  
//  alert(4)

   const [tab, setTab] = useState(resolvePath("/"));

  useEffect(() => {
    handleNavigation('/' + pathname.split('/')[1])
  }, )

  const handleNavigation = (source) => {
    let event = '';
    switch (source) {
      case '/schedule':
        event = 'Schedule';
        setTab('schedule');
        break;
      case '/criclytics':
        event = 'CriclyticsHome';
        setTab('criclytics');

        break;
      case '/news/latest':
        event = 'NewsHome';
        setTab('news');
        break;
        case '/fantasy-research-center':
        event = 'FanHome';
        setTab('Fan');
        break;
      case '/more':
        event = 'More';
        setTab('more');
        break;
      case '/':
        event = 'Home';
        setTab('home');
        break;
      default:
        event = '';
    }
    CleverTap.initialize(event=="news"?'NewsHome':event, {
      Source: 'BottomNavigation',
      Platform: localStorage ? localStorage.Platform : ''
    });
  };

  const  {getTabContent}  = useContext(LocalStorageContext)

  
  return <>
    <div className='h-10' />
   
    <nav className='lg:hidden md:hidden fixed bottom-0 left-0 right-0 flex bg-gray-8 backdrop-blur-2xl py-3  text-xs shadow-md  z-50'>
     

        <div className='w-1/5 '>
          <Link
            href='/'
          
            passHref
            className='flex justify-center items-center  flex-col'
            onClick={() => handleNavigation('/')}>

            <img
              alt='home'
              className='h-8 py-1'
              src={tab === 'home' ? tabIcon1Active : tabIcon1}
            />
            <span className={`text-xs  ${tab === 'home' ? 'text-green font-semibold' : 'text-gray-2'}`}>Home </span>

          </Link>
        </div>

        <div className='w-1/5 '>
          <Link
            as='/schedule/live-matches'
            href='/schedule/live-matches'
            passHref
            className='flex flex-col justify-center items-center'
            onClick={() => handleNavigation('/schedule')}>

            <img
              alt='/footer/tabicon2.png'
              className='h-8 py-1'
              src={tab === 'schedule' ? tabIcon2Active : tabIcon2}
            />
            <span className={`text-xs ${tab === 'schedule' ? 'text-green  font-semibold' : 'text-gray-2'}`}> Schedule</span>

          </Link>
        </div>
        <div className='w-1/5 pr-2 '>
        {getTabContent() =="selectMatch"? 
          <Link
            href='/criclytics'
          
            passHref
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/criclytics')}>

            <img
              alt='/footer/tabicon4.png'
              className='h-8 py-1'
              src={tab === 'criclytics' ? criclytics_active : criclytics_inactive}
            />
            <div className={`flex ${tab === 'criclytics' ? 'text-green  font-semibold' : 'text-gray-2' } relative`}>

            <span className={`text-xs  ${tab === 'criclytics' ? 'text-green  font-semibold' : 'text-gray-2' }`}>Criclytics</span>
            <span className='text-[8px] font-semibold absolute right-[-13px] bottom-1'>TM</span>
            </div>

          </Link>: <Link
            href='/feeds'
          
            passHref
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/criclytics')}>

            <img
              alt='/footer/tabicon4.png'
              className='h-8 py-1'
              src={tab === 'criclytics' ? feedIconActive  :feedIcon }
            />
            <div className={`flex ${tab === 'criclytics' ? 'text-green  font-semibold' : 'text-gray-2' } relative`}>

            <span className={`text-xs  ${tab === 'criclytics' ? 'text-green  font-semibold' : 'text-gray-2' }`}>Feed</span>
           
            </div>

          </Link>}
        </div>


        {getTabContent() !=="selectMatch"? 
        <div className='w-1/5 '>
          <Link
            href='/news/latest'
          
            passHref
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/news/latest')}>

            <img
            
              className='h-8 py-1'
              src={tab === 'news' ? tabIcon4Active : tabIcon4}
            />
            <span className={`text-xs ${tab === 'news' ? 'text-green  font-semibold' : 'text-gray-2'}`}>News</span>

          </Link>
        </div>
        :<div className='w-1/5 pr-2 '>
        <Link
          href='/fantasy-research-center'
       
          passHref
          className='flex flex-col justify-center items-center '
          onClick={() => handleNavigation('/fantasy-research-center')}>

          <img className='h-8  py-1'
             src={tab === 'Fan' ? FanActive :  InFanActive}
           />
          <div className={`flex   ${tab === 'Fan' ? 'text-green  font-semibold' : 'text-gray-2' } relative`}>

          <span className={`text-xs  ${tab === 'Fan' ? 'gtext-greenreen  font-semibold' : 'text-gray-2' }`}>Fantasy</span>
        
          </div>

        </Link>
      </div>
      
}

        <div className='w-1/5 '>
          <Link
            href='/more'
          
            passHref
            title='More'
            className='flex flex-col justify-center items-center '
            onClick={() => handleNavigation('/more')}>

            <img
              alt='/footer/tabicon5.png'
              className='h-8 py-1'
              src={tab === 'more' ? tabIcon5Active : tabIcon5}
            />
            <span className={`text-xs ${tab === 'more' ? 'text-green  font-semibold' : 'text-gray-2'}`}>More</span>

          </Link>
        </div>
    </nav>
    </>;
}

function resolvePath(path) {
  switch (path) {
    case '/more':
      return 'more';
    case '/news/[type]':
      return 'news';
    case '/schedule/[...slugs]':
      return 'schedule';
    case '/':
      return 'home';
    case '/criclytics':
      return 'criclytics';
    default:
      return false;
  }
}