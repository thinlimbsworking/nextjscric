'use client'
import React from 'react'
import { useState, useEffect } from 'react'
import ImageWithFallback from './Image'
const expandRed = '/svgs/expand-red.svg'
const iconPos = new Map([
  ['end', 'flex-row'],
  ['start', 'flex-row-reverse'],
])

const buttonVariants = new Map([
  ['contained', 'bg-basered rounded-full text-white justify-between'],
  [
    'outline',
    'bg-white border text-xs dark:border-none py-2 font-semibold dark:bg-gray-4 dark:text-white focus:border-[#E2E2E2] border-solid rounded-md text-black justify-between',
  ],
  [
    'normal',
    'border  text-basered bg-white dark:bg-gray dark:border-none rounded-md',
  ],
])

const DropDownSelector = ({
  variant = 'contained',
  setDropDown,
  placeHolder = 'no options',
  placeholderIcon,
  expandIcon,
  isOpen,
  showExpandIcon,
  btnClasses = '',
  ...props
}) => {
  return (
    <button
      // disabled
      className={`${!btnClasses ? buttonVariants.get(variant) : btnClasses}
       font-semibold w-full py-2 z-10 px-4 inline-flex items-center justify-between`}
      onClick={() => setDropDown((prev) => !prev)}
    >
      {props.isNews ? (
        <span className="mr-1 text-xs font-semibold capitalize truncate dark:text-white">
          {placeHolder}
        </span>
      ) : props.isSchedule ? (
        <span className="mr-1 text-xs font-semibold capitalize truncate text-white">
          {placeHolder}
        </span>
      ) : (
        <span className="mr-1 text-xs capitalize font-semibold dark:text-white text-black">
          {placeHolder}
        </span>
      )}
      {variant === 'normal' && (
        <ImageWithFallback
          className="w-4 h-4"
          width={10}
          height={10}
          src={expandRed}
        />
      )}
      {/* {variant === 'normal' && (
        <ImageWithFallback
          className="w-4 h-4"
          width={10}
          height={10}
          src={expandRed}
        />
      )} */}

      {placeholderIcon}

      {showExpandIcon && props?.isNews && (
        <>
          {(expandIcon && <img src={expandIcon} alt="expandIcon" />) || (
            <svg
              className={`fill-current h-6 w-6 ${
                isOpen ? 'rotate-180' : 'rotate-0'
              }`}
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <g fill={'#A52A2A'}>
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </g>
            </svg>
          )}
        </>
      )}
      {showExpandIcon && props?.isSchedule && (
        <>
          {(expandIcon && <img src={expandIcon} alt="expandIcon" />) || (
            <svg
              className={`fill-current h-4 w-4 ${
                isOpen ? 'rotate-180' : 'rotate-0'
              }`}
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <g fill={'#FFF'}>
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </g>
            </svg>
          )}
        </>
      )}
      {showExpandIcon && !props?.isSchedule && !props?.isNews && (
        <>
          {(expandIcon && <img src={expandIcon} alt="expandIcon" />) || (
            <svg
              className={`fill-current h-4 w-4 ${
                isOpen ? 'rotate-180' : 'rotate-0'
              }`}
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
            </svg>
          )}
        </>
      )}
    </button>
  )
}

const CommonDropdown = ({
  options = [],
  onChange,
  defaultSelected,
  placeHolder,
  ...props
}) => {
  const [selectedOption, setSelectedOption] = useState(defaultSelected)
  const [openDropDown, setDropDown] = useState(false)

  // useEffect(() => {
  //   if (onChange) {
  //     onChange(selectedOption)
  //   }
  // }, [selectedOption, onChange])

  return (
    <div className="inline-block relative w-full">
      <DropDownSelector
        {...props}
        isOpen={openDropDown}
        setDropDown={setDropDown}
        placeHolder={placeHolder ? placeHolder : selectedOption?.valueLable}
      />
      <ul
        className={`z-20 absolute flex text-xs flex-col dark:bg-basebg border border-solid ${
          props.isNews ? 'top-10 ' : ''
        } ${
          props.isSchedule ? 'rounded-lg rounded-tr-2xl rounded-tl-2xl' : ''
        } dark:border-gray-2 bg-white w-full ${
          props.isFixedHeight && options.length > 1
            ? 'max-h-44 overflow-scroll overflow-x-auto'
            : ''
        } right-0 ${openDropDown ? '' : 'hidden'} text-gray-700 
        `}
      >
        {options.length &&
          options?.length > 1 &&
          options
            // ?.filter((item) => item?.valueLable !== selectedOption?.valueLable)
            // .slice(0)
            .map((option, index) => {
              return option?.valueLable !== selectedOption?.valueLable ? (
                <>
                  {(option?.renderValue && option.renderValue) || (
                    <li
                      role="button"
                      className={`bg-white ${
                        index === options.length - 1 ? '' : 'border-b'
                      } dark:bg-gray-8 dark:text-gray-2 w-full text-black dark:p-2 ${iconPos.get(
                        option?.iconPosition || 'end',
                      )}`}
                      onClick={(e) => {
                        e.preventDefault()
                        setSelectedOption(option)
                        onChange(option)
                        setDropDown(false)
                        props.setActiveTabIndex &&
                          props.setActiveTabIndex(index)
                      }}
                    >
                      <p className="rounded-t text-xs z-20 text-black dark:text-gray-2 w-full p-4 lg:text-ellipsis dark:text-xs dark:py-1 dark:px-2 block whitespace-no-wrap font-semibold dark:font-normal rounded-tr-lg rounded-tl-lg capitalize">
                        {option?.valueLable}
                      </p>

                      {option?.icon && (
                        <img src={option.icon} className="" alt="uiui" />
                      )}
                    </li>
                  )}
                </>
              ) : null
            })}
      </ul>
    </div>
  )
}

export default CommonDropdown
