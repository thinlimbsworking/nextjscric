import React from 'react'
const Dnf = '/pngsV2/empty.png'
export default function DataNotFound({ displayText }) {
  return (
    <div className="flex w-full flex-col items-center justify-center">
      <div>
        {' '}
        <img className="w-44 h-44" src={Dnf} alt="" />
      </div>

      <div className="text-center dark:text-white text-black dark:text-xs text-sm">
        {displayText || 'Data not available'}
      </div>
    </div>
  )
}
