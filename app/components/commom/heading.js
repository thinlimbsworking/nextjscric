import React from 'react'

export default function Heading(props) {
  return (
    <div className="">
      <div
        className={`${
          props?.customClass
            ? props.customClass
            : 'font-semibold md:text-base md:text-black text-white text-base capitalize leading-6 font-ttb'
        }`}
      >
        {props.heading}{' '} 
      </div>
      {props.subHeading && (
        <div className={`${props.subHeadingClass ? props.subHeadingClass : 'text-black text-sm tracking-wide dark:text-white pt-1'}`}>
          {props.subHeading}{' '}
        </div>
      )}

      {!props.hideBottom && (
        <div className={`${props.lineColor ? 'bg-[#D8993B]':'bg-blue-8'} h-1 rounded-md mt-1 w-10`}></div>
      )}
    </div>
  )
}
