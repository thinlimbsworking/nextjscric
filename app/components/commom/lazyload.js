import {
    LazyLoadComponent,
    LazyLoadImage,
    trackWindowScroll
  } from 'react-lazy-load-image-component'
  const fallbackBackground = '/pngs/gray_background.png'
  
  const LazyImageLoader = ({ fallbackImage, showOnError, ...props }) => {
    const fallBack = fallbackImage ? fallbackImage : fallbackBackground
    const errorImage = showOnError ? showOnError : fallbackBackground;
    return (
      <LazyLoadImage
        placeholderSrc={fallBack}
        {...props}
        onError={(ev) => ev.currentTarget.src = errorImage}
      />
    )
  }
  
  export const LazyLoadImageComponent = ({ children, ...props }) => (
    <LazyLoadComponent>
      {children}
    </LazyLoadComponent>
  )
  export { trackWindowScroll as trackScroll }
  export default LazyImageLoader