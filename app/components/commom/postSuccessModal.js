import React from "react";
import ImageWithFallback from "./Image";
const PostSuccessModal = ({Msg}) =>{
  return (
    <div style={{height:'80vh', width:'100%'}} className="border-none outline-0 outline-none flex flex-col justify-center items-center  focus:outline-none ">
      <ImageWithFallback src="/gif/success.gif" height={300} width={300} className="h-64 w-64" loading='lazy' />
      <span className="text-black dark:text-white text-xl ">{Msg}</span>
    </div>
  )
}
export default PostSuccessModal
