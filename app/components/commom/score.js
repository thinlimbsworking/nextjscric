import { format } from 'date-fns'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import React, { useContext, useState } from 'react'
import Countdown from 'react-countdown-now'
import Login from '../../login/page'
import { GET_SCORE_CARD_AXIOS } from '../../api/queries'
import { LocalStorageContext } from '../layout'
import ImageWithFallback from './Image'
// import { useErrorHandler } from '../../components/ErrorBoundary';
import {
  getCriclyticsScore,
  getCriclyticsUrlHome,
  getFrcSeriesTabUrl,
} from '../../api/services'
import { PLAYER_VIEW } from '../../constant/Links'
import useCleverTab from '../customHooks/useCleverTab'
import Predictionbar from '../playodds/predictionDetails'
const AbandonedIcon = '/svgs/images/Abandoned_Icon.png'
const flagPlaceHolder = '/pngsV2/flag_dark.png'
const ManOfMatch = '/pngsV2/MOM2.png'
const crycLyticsLogo = '/pngsV2/CriclyticsIcon.png'
const frc = '/pngsV2/fantasy-center.png'
const location = '/pngsV2/location.png'

export default function Scores({
  data,
  hideFantasyCriTab,
  isPredictedMatch,
  homePage,
  ...props
}) {
  const { getItem } = useContext(LocalStorageContext)
  const { pushEvent } = useCleverTab()
  const { featured, showCriclytics, isDetails } = props
  const [showlogin, setShowLogin] = useState(false)
  const [loginPath, setLoginPath] = useState('')
  let router = useRouter()
  let navigate = router.push
  const handleCriclyticsNav = () => {
    pushEvent('Criclytics', {
      Source: 'MatchScorecard',
      MatchID: data.matchID,
      SeriesID: data.seriesID,
      TeamAID: data.matchScore[0].teamID,
      TeamBID: data.matchScore[1].teamID,
      MatchFormat: data.matchType,
      MatchStartTime: format(Number(data.startDate), 'd,y, h:mm a'),
      MatchStatus: data.matchStatus,
      CriclyticsEngine:
        data.matchStatus === 'live'
          ? 'LiveTeam'
          : data.matchStatus === 'completed' && data.matchType !== 'Test'
          ? 'CompletedMS'
          : data.matchStatus === 'completed' && data.matchType === 'Test'
          ? 'CompletedMR'
          : data.matchStatus === 'upcoming'
          ? 'ScheduledStats'
          : '',
    })
  }
  const getCriclyticdUrlHome = (match) => {
    let tab =
      match.matchStatus === 'completed'
        ? match.matchType !== 'Test'
          ? ''
          : ''
        : match.matchStatus === 'live' && match.isLiveCriclyticsAvailable
        ? ''
        : match.matchStatus === 'upcoming' ||
          (match.matchStatus === 'live' && !match.isLiveCriclyticsAvailable)
        ? ''
        : ''
    return getCriclyticsUrlHome(match, tab)
  }
  const criclyticsUrl = () => {
    let tab =
      data.matchStatus === 'upcoming' ||
      (data.matchStatus === 'live' && !data.isLiveCriclyticsAvailable)
        ? 'key-stat'
        : data.matchStatus === 'live' && data.isLiveCriclyticsAvailable === true
        ? 'score-projection'
        : data.matchStatus === 'completed' && data.matchType !== 'Test'
        ? 'report-card'
        : data.matchStatus === 'completed' && data.matchType === 'Test'
        ? 'match-reel'
        : 'key-stat'
    return getCriclyticsScore(data, tab)
  }
  const handleFantasyNav = () => {
    pushEvent('Fantasy', {
      Source: 'MatchScorecard',
      MatchID: data.matchID,
      SeriesID: data.seriesID,
      TeamAID: data.matchScore[0].teamID,
      TeamBID: data.matchScore[1].teamID,
      MatchFormat: data.matchType,
      MatchStatus:
        data.matchStatus === 'live'
          ? 'Live'
          : data.matchStatus === 'upcoming'
          ? 'Scheduled'
          : data.matchStatus === 'completed'
          ? 'Completed'
          : '',
    })
  }
  const getUrl = (player) => {
    let playerSlug = `${player.name.split(' ').join('-').toLowerCase()}`
    let playerId = player.id
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href,
    }
  }
  const handlePlayerNav = (playerName, playerID) => {
    pushEvent('Players', {
      Source: 'MatchScorecard',
      PlayerID: playerID,
    })
    let { as, href } = getUrl({ name: playerName, id: playerID })
    navigate(href, as)
  }
  const redirectToFile = (match, isLiveCriclyticsAvailable) => {
    let matchProjection = {}
    let tab = ''
    matchProjection.matchID = match.matchID
    let matchName = `${match.homeTeamName}-vs-${match.awayTeamName}-${match.matchNumber}-${match.seriesName}`
    matchProjection.seriesSlug = matchName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()
    return getFrcSeriesTabUrl(matchProjection, tab)
  }
  const handleNav = (data, status) => {
    const { href, as } = redirectToFile(data)

    if (data && data.loginEnable) {
      if (getItem('tokenData')) {
        navigate(href, as)
      } else {
        setLoginPath(as)
        setShowLogin(true)
      }
    } else {
      navigate(href, as)
    }
  }
  // console.log('data in score --- ',data);
  return (
    <>
      {showlogin && (
        <Login
          navTo={loginPath}
          modalText={'Login / Register to access Fantasy Research Center'}
          sucessPath={loginPath}
          name={false}
          onCLose={'/match-score'}
          setShowLogin={setShowLogin}
          TeamAID={data ? data.matchScore[0].teamID : ''}
          TeamBID={data ? data.matchScore[1].teamID : ''}
          matchID={data ? data.matchID : ''}
          ShowLoginSucess={true}
          entryRoute={'Scorecard'}
        />
      )}
      {data.matchStatus == 'completed' && (
        <div
          className="bg-white border overflow-hidden dark:bg-gray dark:border-none rounded-lg w-full p-2"

          // style={{
          //   height: props.playTheOdds || props.from === 'frc' ? 275 : 300,
          // }}
        >
          {props.hideSeriesName !== true && (
            <div
              className={`font-semibold flex items-center text-xs gap-2 tracking-wide dark:text-white ${
                props?.wonTag ? 'justify-between' : ''
              }`}
            >
              <div className="flex items-center gap-2 truncate">
                {/* <div className="text-xs text-gray-2 font-semibold  uppercase">
                {data.matchNumber}
              </div> */}

                {data.matchResult === 'Match Cancelled' ||
                (!props?.showMatchNumber && data.isAbandoned) ? (
                  <div className=" text-red-5  font-semibold   flex  uppercase  justify-center items-center">
                    {data.isAbandoned ? 'ABANDONED' : 'Cancelled'}
                  </div>
                ) : (
                  <div className="font-semibold text-[#707070] flex  uppercase  justify-center items-center ">
                    {data.matchStatus === 'completed' && !props?.showMatchNumber
                      ? 'Result'
                      : ''}{' '}
                  </div>
                )}
                {/* {props?.showMatchNumber && (
                  <div className="font-semibold text-xs flex uppercase">
                {props?.showMatchNumber && (
                  <div className="font-semibold text-xs flex uppercase ">
                    {data.matchNumber}
                  </div>
                )} */}

                {/* {!props?.hideDot && (
                  <div className="w-1 h-1 rounded-full bg-black"></div>
                )} */}
                <div className="w-1 h-1 rounded-full bg-white md:bg-black"></div>
                {props?.from === 'series' ? (
                  <div className="flex text-basered dark:text-white font-normal text-xs items-center gap-1">
                    {
                      format(
                        Number(data.matchDateTimeGMT || data.startDate) -
                          19800000,
                        'd LLLL, hh:mm a ',
                      )?.split(',')?.[0]
                    }
                  </div>
                ) : (
                  <div
                    className={`${
                      props?.from === 'schedule'
                        ? 'w-64 truncate'
                        : props?.wonTag
                        ? 'w-48 truncate'
                        : 'truncate w-64'
                    }`}
                  >
                    {data.seriesName ||
                      (props.lang === 'ENG'
                        ? data.matchName
                        : data.matchNameHindi || data.matchName)}
                  </div>
                )}
              </div>
              {props?.wonTag && (
                <div className="flex  ">
                  <div
                    className={`px-3 text-white ${
                      data.win === 'W'
                        ? 'bg-green-3'
                        : data.win === 'L'
                        ? 'bg-basered'
                        : 'dark:bg-basebg bg-gray'
                    }`}
                  >
                    {data.win === 'W'
                      ? 'Won'
                      : data.win === 'L'
                      ? 'Lost'
                      : 'Draw'}
                  </div>
                </div>
              )}
            </div>
          )}
          <div className="flex gap-2 items-center">
            {(!props?.from === 'frc' ||
              props?.from == 'schedule' ||
              props?.from == 'series') && (
              <span className="text-xs font-semibold text-black dark:text-gray-500">
                {data?.matchNumber}
              </span>
            )}

            <div className="flex items-center mt-1">
              <ImageWithFallback
                loading="lazy"
                src={location}
                height={12}
                width={10}
                // className="w-3 h-3"
              />
              <div className="text-xs text-gray-2 font-semibold pl-1">
                {data.venue ||
                  (props.lang === 'ENG'
                    ? data.city
                    : data.cityHindi || data.city)}
              </div>
            </div>
            {/* <div className=''>
                      <img src={Bell} alt='' />
                    </div> */}
          </div>
          <div className="mt-2 bg-gray-10 dark:bg-gray-4 flex flex-col justify-center gap-2 rounded p-2 ">
            <div className="flex gap-2 justify-between items-center ">
              <div className="flex items-center dark:text-white text-xs font-semibold w-8/12">
                <ImageWithFallback
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  // height={18}
                  // width={18}
                  className="w-7  rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                />{' '}
                {data.matchScore[0].teamShortName}
              </div>
              {}
              <div className="flex gap-2 justify-center">
                {data.matchScore &&
                  data.matchScore[0] &&
                  data.matchScore[0].teamScore &&
                  data.matchScore[0].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      {id === 1 && <span className="text-sm">&</span>}
                      <p
                        className={`${
                          data.winningTeamID === team.teamID
                            ? 'font-bold md:text-green-600 text-green'
                            : 'font-bold'
                        } text-xs font-semibold`}
                      >
                        {team.runsScored}
                        {team.runsScored && <span>/</span>}
                        {team.wickets}
                      </p>
                      {team.overs && (
                        <p className="text-xs font-medium dark:text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
              {/* <div
                className={`justify-center items-center flex border-2 font-semibold ${
                  data.matchResult === 'Match Cancelled' || data.isAbandoned
                    ? 'border-gray-3 text-gray-3'
                    : 'border-green text-green'
                }  rounded-full  uppercase text-xs px-3 mr-2 py-0.5 `}
              >
                {data.matchType}
              </div> */}
            </div>

            <div className="flex gap-2 justify-between items-center ">
              <div className="flex items-center dark:text-white text-xs font-semibold w-8/12">
                <ImageWithFallback
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  // height={18}
                  // width={18}
                  className="w-7 rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                />{' '}
                {data.matchScore[1].teamShortName}
              </div>
              <div className="flex gap-2 justify-center">
                {data.matchScore &&
                  data.matchScore[1] &&
                  data.matchScore[1].teamScore &&
                  data.matchScore[1].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      {id === 1 && <span className="text-sm">&</span>}
                      <p
                        className={`${
                          data.winningTeamID === team.teamID
                            ? 'font-bold md:text-green-600 text-green'
                            : 'dark:text-white'
                        } text-xs font-semibold `}
                      >
                        {team.runsScored}
                        {team.runsScored && <span>/</span>}
                        {team.wickets}
                      </p>
                      {team.overs && (
                        <p className="text-xs  font-medium  dark:text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
              {/* <div
                className={`justify-center items-center flex border-2 font-semibold ${
                  data.matchResult === 'Match Cancelled' || data.isAbandoned
                    ? 'border-gray-3 text-gray-3'
                    : 'border-green text-green'
                }  rounded-full  uppercase text-xs px-3 mr-2 py-0.5 `}
              >
                {data.matchType}
              </div> */}
            </div>
          </div>

          {data.matchResult === 'Match Cancelled' || data.isAbandoned ? (
            <div className="bg-gray-10 dark:bg-gray-4 truncate text-center my-1 rounded py-1  text-xs font-semibold ">
              {props.lang === 'ENG'
                ? data.statusMessage
                : data.statusMessageHindi || data.statusMessage}
            </div>
          ) : (
            <div className="bg-gray-10 dark:bg-gray-4 truncate text-green md:text-green-600 h-6 text-center my-1 rounded py-1  text-xs font-semibold ">
              {data.matchResult ||
                (props.lang === 'ENG'
                  ? data.statusMessage
                  : data.statusMessageHindi || data.statusMessage)}
            </div>
          )}

          {props?.from !== 'frc' &&
            (!isPredictedMatch ? (
              <div
                className={`flex justify-start items-center gap-3 ${
                  homePage && data.isAbandoned
                    ? 'py-1'
                    : data?.playerOfTheMatch?.length == 0
                    ? ''
                    : ''
                }`}
              >
                {data.matchResult !== 'Match Cancelled' && !data.isAbandoned ? (
                  <div class="flex relative  items-center ">
                    <ImageWithFallback
                      fallbackSrc="/pngsV2/MOM2.png"
                      height={18}
                      width={18}
                      className="h-12 w-12 bg-gray-10 dark:bg-gray-4 object-cover object-top rounded-full p-1"
                      loading="lazy"
                      src={`https://images.cricket.com/players/${
                        data && data.playerID
                      }_headshot_safari.png`}
                    />
                    <ImageWithFallback
                      height={18}
                      width={18}
                      className="absolute h-4 w-6 -right-1 bottom-0 object-fill rounded-sm"
                      loading="lazy"
                      src={`https://images.cricket.com/teams/${
                        data && data.winningTeamID
                      }_flag_safari.png`}
                      fallbackSrc="/pngsV2/flag_dark.png"
                    />
                  </div>
                ) : (
                  <ImageWithFallback
                    height={18}
                    width={18}
                    className="h-10 w-10 bg-gray-10 dark:bg-gray-4 rounded-full p-2"
                    src={ManOfMatch}
                    loading="lazy"
                  />
                )}
                {data.matchResult == 'Match Cancelled' ? (
                  <div className="dark:text-white text-xs py-4 font-semibold ">
                    Match Cancelled
                  </div>
                ) : data.isAbandoned ? (
                  <div className="text-gray-2 text-xs font-semibold ">
                    Match Abandoned
                  </div>
                ) : (
                  <div className="flex flex-col">
                    <div className="dark:text-white text-xs font-semibold ">
                      {data.playerOfTheMatch || 'TBA'}
                    </div>
                    <div className=" capitalize text-gray-2 text-xs font-medium">
                      player Of The Match
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <div className="flex flex-col justify-between -mt-2">
                {!props.playTheOdds && (
                  <Predictionbar matchID={data?.matchID} />
                )}
              </div>
            ))}
        </div>
      )}

      {/* ----------------------completed match block ends-------------------- */}

      {/*------------------------------ upcoming card block starts--------------------------------------------- */}

      {(data.matchStatus === 'upcoming' ||
        (data.matchStatus == 'live' && !data.isLiveCriclyticsAvailable)) && (
        <div className="border bg-white dark:bg-gray dark:border-none rounded-lg p-2 w-full">
          {props.hideSeriesName !== true && (
            <div className="font-semibold flex items-center text-xs gap-2 dark:text-white ">
              <div className="flex items-center ">
                {/* {data.matchResult === 'Match Cancelled' || data.isAbandoned ? (
                  <div className=" text-red-5  font-semibold   flex  uppercase  justify-center items-center">
                    {data.isAbandoned ? 'ABANDONED' : 'Cancelled'}
                  </div>
                ) : (
                  <div className="  text-basered font-semibold   flex  uppercase  justify-center items-center ">
                    {data.matchStatus}{' '}
                  </div>
                )} */}

                {data.matchResult === 'Match Cancelled' ||
                (!props?.showMatchNumber && data.isAbandoned) ? (
                  <div className="md:text-red-5 text-green-600 font-semibold   flex  uppercase  justify-center items-center">
                    {data.isAbandoned ? 'ABANDONED' : 'Cancelled'}
                  </div>
                ) : (
                  <div
                    className={`font-semibold uppercase text-xs ${
                      data.matchStatus.toUpperCase() === 'UPCOMING'
                        ? 'text-red-5'
                        : 'text-green-600'
                    }   text justify-center items-center`}
                  >
                    {data.matchStatus}{' '}
                  </div>
                )}
                {/* {props?.showMatchNumber && (
                  <div className="font-semibold text-xs flex uppercase">
                    {data.matchNumber}
                  </div>
                )} */}
              </div>
              {!props?.hideDot && (
                <div className="w-1 h-1 rounded-full bg-white md:bg-black"></div>
              )}
              {props?.from === 'series' ? (
                <div className="flex text-basered dark:text-white font-normal text-xs items-center gap-1">
                  {
                    format(
                      Number(data.matchDateTimeGMT || data.startDate) -
                        19800000,
                      'd LLLL, hh:mm a ',
                    )?.split(',')?.[0]
                  }
                </div>
              ) : (
                <div
                  className={`${
                    props?.from === 'schedule'
                      ? 'truncate w-64'
                      : props?.wonTag
                      ? 'w-48 truncate'
                      : 'truncate w-64'
                  }`}
                >
                  {data.seriesName ||
                    (props.lang === 'ENG'
                      ? data.matchName
                      : data.matchNameHindi || data.matchName)}
                </div>
              )}
            </div>
          )}
          <div className="flex gap-2">
            {!props?.from === 'frc' ||
              (props?.from === 'schedule' && (
                <span className="text-xs font-semibold text-black dark:text-gray-500 py-1">
                  {data?.matchNumber}
                </span>
              ))}
            <div className="flex items-center mt-1">
              <ImageWithFallback
                src={location}
                height={12}
                width={10}
                // className="w-3 h-3"
                loading="lazy"
              />
              <div className="text-xs text-gray-2 font-semibold pl-1">
                {data.venue ||
                  (props.lang === 'ENG'
                    ? data.city
                    : data.cityHindi || data.city)}
              </div>
            </div>
          </div>
          <div className="mt-2 bg-gray-10 dark:bg-gray-4 flex flex-col justify-center gap-2 rounded p-2 ">
            <div className="flex justify-between items-center ">
              <div className="flex items-center dark:text-white text-xs font-semibold w-8/12">
                <ImageWithFallback
                  // height={18}
                  // width={18}
                  className="w-7 rounded-sm mr-2"
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                />
                {data.matchScore[0].teamShortName}
              </div>

              <div className="flex gap-1 justify-center">
                {data?.matchStatus === 'live' &&
                  data.isLiveCriclyticsAvailable &&
                  data?.currentinningsNo % 2 !== 0 && (
                    <div className="flex">
                      <div className="w-2 h-2 rounded-full bg-green-600"></div>
                    </div>
                  )}
                {data.matchScore &&
                data.matchScore[0] &&
                data.matchScore[0].teamScore &&
                data.matchScore[0].teamScore.length > 0 ? (
                  data.matchScore[0].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      <p
                        className={`${
                          data.winningTeamID === team.teamID
                            ? 'text-green md:text-green-600'
                            : 'dark:text-white'
                        } text-xs font-semibold `}
                      >
                        {team.runsScored}
                        {team.runsScored && <span>/</span>}
                        {team.wickets}
                      </p>
                      {team.overs && (
                        <p className="text-xs  font-medium  dark:text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))
                ) : (
                  <div className="flex text-basered dark:text-green text-xs items-center gap-1">
                    {
                      format(
                        Number(data.matchDateTimeGMT || data.startDate) -
                          19800000,
                        'd LLLL, hh:mm a ',
                      )?.split(',')?.[0]
                    }
                  </div>
                )}
              </div>

              {/* <div className="justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3 mx-3 py-0.5 ">
                  {data.matchType}
                </div> */}
            </div>

            <div className="flex justify-between items-center ">
              <div className="flex items-center dark:text-white text-xs font-semibold gap-2 w-8/12">
                <ImageWithFallback
                  height={18}
                  width={18}
                  className="w-7 rounded-sm "
                  src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                />
                {data.matchScore[1].teamShortName}
              </div>

              <div className="flex gap-1 justify-center">
                {data?.matchStatus === 'live' &&
                  data.isLiveCriclyticsAvailable &&
                  data?.currentinningsNo % 2 !== 0 && (
                    <div className="flex">
                      <div className="w-2 h-2 rounded-full bg-green-600"></div>
                    </div>
                  )}
                {data.matchScore &&
                data.matchScore[1] &&
                data.matchScore[1].teamScore &&
                data.matchScore[0].teamScore &&
                data.matchScore[0].teamScore.length > 0
                  ? data.matchScore[1].teamScore.map((team, id) => (
                      <div key={id} className="flex items-center gap-1">
                        {team.runsScored && (
                          <p
                            className={`${
                              data.winningTeamID === team.teamID
                                ? 'md:text-green-600 text-green'
                                : 'dark:text-white'
                            } text-xs font-semibold `}
                          >
                            {team.runsScored}/{team.wickets || 0}
                          </p>
                        )}
                        {team.overs && (
                          <p className="text-xs  font-medium dark:text-white">
                            ({team.overs})
                          </p>
                        )}
                        {team.declared && (
                          <p className="text-xs text-green  pl-1 font-semibold">
                            d
                          </p>
                        )}
                        {team.folowOn && (
                          <p className="text-xs text-green  pl-1 font-semibold">
                            f
                          </p>
                        )}
                      </div>
                    ))
                  : (
                      <div className="flex  text-xs items-center gap-1">
                        {
                          format(
                            Number(data.matchDateTimeGMT || data.startDate) -
                              19800000,
                            'd LLLL, hh:mm a ',
                          )?.split(',')?.[1]
                        }
                      </div>
                    ) || (
                      <div className="text-gray-2 text-xs font-medium ">
                        Yet to bat
                      </div>
                    )}
              </div>
            </div>
          </div>
          <div className="flex bg-gray-10 dark:bg-gray-4 justify-center items-center my-1 rounded p-1 dark:text-white">
            {data.statusMessage ? (
              <p className="text-xs font-medium text-center truncate ">
                {data.statusMessage}
              </p>
            ) : (
              <div className="flex items-center">
                <Countdown
                  date={
                    new Date(
                      Number(data.startDate || data.matchDateTimeGMT) -
                        19800000 -
                        30 * 60 * 1000,
                    )
                  }
                  renderer={(props) => (
                    <span className="text-xs font-semibold">
                      {`${
                        props.days > 0
                          ? format(
                              Number(data.startDate || data.matchDateTimeGMT) -
                                19800000 -
                                30 * 60 * 1000,
                              'd LLLL, hh:mm a ',
                            )
                          : Number(props.hours || 0) +
                            ' hrs ' +
                            props.minutes +
                            ' mins to toss'
                      }`}
                    </span>
                  )}
                />
              </div>
            )}
          </div>
          {props?.from !== 'frc' &&
            !props.playTheOdds &&
            (data.teamsWinProbability && !isPredictedMatch ? (
              data?.teamsWinProbability?.homeTeamPercentage ? (
                <div>
                  <div className="text-gray-2 text-xs my-1 font-medium">
                    WIN PERCENTAGE
                  </div>
                  <div className=" flex dark:bg-gray-3 rounded-full h-2 overflow-hidden ">
                    <div
                      className={`${'bg-green'}  h-2 rounded-l-lg`}
                      style={{
                        width:
                          data.teamsWinProbability.homeTeamPercentage + '%',
                      }}
                    />
                    <div
                      className={` bg-gray-3 h-2   `}
                      style={{
                        width: data.teamsWinProbability.tiePercentage + '%',
                      }}
                    />
                    <div
                      className={`${'bg-gray-11 dark:bg-white'}  h-2 rounded-r-lg  `}
                      style={{
                        width:
                          data.teamsWinProbability.awayTeamPercentage + '%',
                      }}
                    />
                  </div>
                  <div className="flex justify-between items-center text-gray-2">
                    <div className=" flex items-center text-xs font-medium">
                      <p
                        className={`w-2 h-2 ${'bg-green'} rounded-full m-1  `}
                      ></p>
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.homeTeamShortName}{' '}
                      (
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.homeTeamPercentage}
                      %)
                    </div>
                    {data.teamsWinProbability &&
                      data.teamsWinProbability.tiePercentage && (
                        <div className="flex items-center text-xs font-medium">
                          <p className="w-2 h-2 bg-gray-3 rounded-full m-1  "></p>
                          {data.matchType != 'Test' ? 'Tie' : 'DRAW'} (
                          {(data.teamsWinProbability &&
                            data.teamsWinProbability.tiePercentage) ||
                            0}
                          %)
                        </div>
                      )}
                    <div className="flex items-center text-xs font-medium">
                      <p
                        className={`w-2 h-2 ${'bg-gray-11 dark:bg-white'} rounded-full m-1  `}
                      ></p>
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.awayTeamShortName}{' '}
                      (
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.awayTeamPercentage}
                      %)
                    </div>
                  </div>
                </div>
              ) : (
                <div className="py-3">
                  <span className="text-sm dark:text-xs text-gray-2">
                    Win projections to be updated soon...
                  </span>
                </div>
              )
            ) : (
              !props.playTheOdds && <Predictionbar matchID={data?.matchID} />
            ))}
          {props.playTheOdds && (
            <div className="text-xs dark:text-white p-2 ">
              {(data.totalParticipants || 0) + ' participants'}
            </div>
          )}
        </div>
      )}

      {/* -------------------------------------------end of upcoming match card------------------------------------ */}

      {/* --------------------------------------live match card----------------------------------- */}

      {data.matchStatus == 'live' && data.isLiveCriclyticsAvailable && (
        <div
          className="border bg-white w-full dark:bg-gray dark:border-none rounded-lg p-2"
          // style={{
          //   height: props.playTheOdds || props.from === 'frc' ? 275 : 300,
          // }}
        >
          {props.hideSeriesName !== true && (
            <div className="font-semibold flex items-center text-xs gap-2 tracking-wide dark:text-white ">
              <div className="flex items-center ">
                {/* <div className="text-xs text-gray-2 font-semibold  uppercase">
                {data.matchNumber}
              </div> */}
                {/* {data.matchResult === 'Match Cancelled' || data.isAbandoned ? (
                  <div className=" text-red-5  font-semibold   flex  uppercase  justify-center items-center">
                    {data.isAbandoned ? 'ABANDONED' : 'Cancelled'}
                  </div>
                ) : (
                  <div className="  text-green font-semibold   flex  uppercase  justify-center items-center ">
                    {data.matchStatus}{' '}
                  </div>
                )} */}
                {data.matchResult === 'Match Cancelled' ||
                (!props?.showMatchNumber && data.isAbandoned) ? (
                  <div className=" text-red-5 font-semibold flex uppercase justify-center items-center">
                    {data.isAbandoned ? 'ABANDONED' : 'Cancelled'}
                  </div>
                ) : (
                  // <div className="font-semibold text-[#229B15] flex  uppercase  justify-center items-center ">
                  //   {data.matchStatus}{' '}
                  // </div>
                  <div className="font-semibold flex md:text-green-600 text-black dark:bg-green-600 dark:px-1 text-xs uppercase justify-center items-center">
                    {data.matchStatus}{' '}
                  </div>
                )}
                {/* {props?.showMatchNumber && (
                  <div className="font-semibold text-xs flex uppercase">
                    {data.matchNumber}
                  </div>
                )} */}
              </div>
              {!props?.hideDot && (
                <div className="w-1 h-1 rounded-full bg-white md:bg-black"></div>
              )}
              {props?.from === 'series' ? (
                <div className="flex text-basered dark:text-white font-normal text-xs items-center gap-1">
                  {
                    format(
                      Number(data.matchDateTimeGMT || data.startDate) -
                        19800000,
                      'd LLLL, hh:mm a ',
                    )?.split(',')?.[0]
                  }
                </div>
              ) : (
                <div
                  className={`${
                    props?.from === 'schedule'
                      ? 'w-64 truncate'
                      : props?.wonTag
                      ? 'w-48 truncate'
                      : 'truncate w-64'
                  }`}
                >
                  {data.seriesName ||
                    (props.lang === 'ENG'
                      ? data.matchName
                      : data.matchNameHindi || data.matchName)}
                </div>
              )}
            </div>
          )}
          <div className="flex gap-2 dark:pt-1">
            {/* <div className="flex gap-1 items-center">
              <div className="text-xs text-gray-2 font-semibold  uppercase">
                {data.matchNumber}
              </div>
              <div className=" font-semibold text-xs  rounded-full bg-red-5  px-2 p-1 flex  uppercase text-white justify-center items-center">
                <div className="w-1.5 h-1.5  bg-white rounded-full mr-1 "></div>{' '}
                {data.matchStatus}{' '}
              </div>
            </div> */}
            {!props?.from === 'frc' ||
              (props?.from === 'schedule' && (
                <span className="text-xs font-semibold text-black dark:text-gray-500 py-1">
                  {data?.matchNumber}
                </span>
              ))}

            <div className="flex items-center mt-1">
              <ImageWithFallback
                src={location}
                height={12}
                width={10}
                // className="w-3 h-3"
                loading="lazy"
              />
              <div className="text-xs text-gray-2 font-semibold pl-1">
                {data.venue ||
                  (props.lang === 'ENG'
                    ? data.city
                    : data.cityHindi || data.city)}
              </div>
            </div>
          </div>
          <div className="mt-2 bg-gray-10 dark:bg-gray-4 flex flex-col justify-center gap-2 rounded p-2 ">
            <div className="flex justify-between items-center ">
              <div className="flex items-center dark:text-white text-xs font-semibold w-8/12">
                <ImageWithFallback
                  height={18}
                  width={18}
                  className="w-7 rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                  fallbackSrc="/pngsV2/flag_dark.png"
                />{' '}
                {data.matchScore[0].teamShortName}
              </div>

              {/* <div className="w-8/12 flex px-2 bg-red">
                <div className="w-2 h-2 rounded-full bg-green-600"></div>
              </div> */}

              <div className="flex gap-1 justify-center items-center">
                {data?.matchStatus === 'live' &&
                  data.isLiveCriclyticsAvailable &&
                  data?.currentinningsNo % 2 !== 0 && (
                    <div className="flex">
                      <div className="w-2 h-2 rounded-full bg-green-600"></div>
                    </div>
                  )}
                {data.matchScore &&
                  data.matchScore[0] &&
                  data.matchScore[0].teamScore &&
                  data.matchScore[0].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center">
                      {/* {data?.currentInningsTeamName === team?.teamFullName ? (
                        <div className="w-2 h-2 rounded-full bg-green-600"></div>
                      ) : (
                        ''
                      )} */}
                      {id === 1 && <span className="text-sm mx-0.5">&</span>}
                      <p
                        className={`${
                          data.winningTeamID === team.teamID
                            ? 'font-bold'
                            : 'dark:text-white'
                        } text-xs font-semibold `}
                      >
                        {team.runsScored}
                        {team.runsScored && <span>/</span>}
                        {team.wickets}
                      </p>
                      {team.overs && (
                        <p className="text-xs pl-1 font-medium dark:text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
            </div>

            <div className="flex justify-between items-center">
              <div className="flex items-center gap-2  dark:text-white text-xs font-semibold w-8/12 ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  className="w-7 rounded-xs "
                  fallbackSrc="/pngsV2/flag_dark.png"
                  src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                />
                {data.matchScore[1].teamShortName}
              </div>

              <div className="flex gap-1 items-center justify-center">
                {data?.matchStatus === 'live' &&
                  data.isLiveCriclyticsAvailable &&
                  data?.currentinningsNo % 2 == 0 && (
                    <div className="flex">
                      <div className="w-2 h-2 rounded-full bg-green-600"></div>
                    </div>
                  )}
                {data.matchScore &&
                data.matchScore[1] &&
                data.matchScore[1].teamScore.length > 0 ? (
                  data.matchScore[1].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center">
                      {id === 1 && <span className="text-sm mx-0.5">&</span>}
                      {team.runsScored && (
                        <p
                          className={`${
                            data.winningTeamID === team.teamID
                              ? 'font-bold'
                              : 'dark:text-white'
                          } text-xs font-semibold `}
                        >
                          {team.runsScored}
                          {team.runsScored && <span>/</span>}
                          {team.wickets}
                        </p>
                      )}
                      {team.overs && (
                        <p className="text-xs font-medium dark:text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-xs text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))
                ) : (
                  <div className="text-gray-2 text-xs font-medium ">
                    Yet to bat
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="bg-gray-10 dark:bg-gray-4 text-center my-1 rounded py-1  text-xs font-semibold">
            {props.lang === 'ENG'
              ? data.statusMessage
              : data.statusMessageHindi ||
                data.statusMessage ||
                data.statusMessage}
          </div>

          {(props?.from !== 'frc' || !props.playTheOdds) &&
            (data.teamsWinProbability && !isPredictedMatch ? (
              <>
                {data.teamsWinProbability &&
                  (data?.teamsWinProbability?.homeTeamPercentage ? (
                    <div className="">
                      <div className="text-gray-2 text-xs my-1 font-medium">
                        WIN PERCENTAGE
                      </div>
                      <div className=" flex dark:bg-gray-3 rounded-full  h-2 overflow-hidden ">
                        <div
                          className={`${'bg-green'}  h-2 rounded-l-lg`}
                          style={{
                            width:
                              data.teamsWinProbability.homeTeamPercentage + '%',
                          }}
                        />
                        <div
                          className={` bg-gray-3 h-2   `}
                          style={{
                            width: data.teamsWinProbability.tiePercentage + '%',
                          }}
                        />
                        <div
                          className={`${'bg-gray-11 dark:bg-white'}  h-2 rounded-r-lg  `}
                          style={{
                            width:
                              data.teamsWinProbability.awayTeamPercentage + '%',
                          }}
                        />
                      </div>
                      <div className="flex justify-between items-center text-gray-2">
                        <div className=" flex items-center text-xs font-medium">
                          <p
                            className={`w-2 h-2 ${'bg-green'} rounded-full m-1  `}
                          ></p>
                          {data.teamsWinProbability &&
                            data.teamsWinProbability.homeTeamShortName}{' '}
                          (
                          {data.teamsWinProbability &&
                            data.teamsWinProbability.homeTeamPercentage}
                          %)
                        </div>
                        {data.teamsWinProbability &&
                          data.teamsWinProbability.tiePercentage && (
                            <div className="flex items-center text-xs font-medium">
                              <p className="w-2 h-2 bg-gray-3 rounded-full m-1  "></p>
                              {data.matchType != 'Test' ? 'Tie' : 'DRAW'} (
                              {(data.teamsWinProbability &&
                                data.teamsWinProbability.tiePercentage) ||
                                0}
                              %)
                            </div>
                          )}
                        <div className="flex items-center text-xs font-medium">
                          <p
                            className={`w-2 h-2 ${'bg-gray-11 dark:bg-white'} rounded-full m-1  `}
                          ></p>
                          {data.teamsWinProbability &&
                            data.teamsWinProbability.awayTeamShortName}{' '}
                          (
                          {data.teamsWinProbability &&
                            data.teamsWinProbability.awayTeamPercentage}
                          %)
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className="py-3">
                      <span className="text-sm dark:text-xs text-gray-2">
                        Win projections to be updated soon...
                      </span>
                    </div>
                  ))}
              </>
            ) : (
              props?.from !== 'frc' &&
              !props.playTheOdds && (
                <div className="-mt-4">
                  <Predictionbar matchID={data?.matchID} />
                </div>
              )
            ))}
        </div>
      )}
      {isDetails && (
        <div className="h-[1px] lg:hidden md:hidden mt-2 w-full"></div>
      )}
      {!hideFantasyCriTab && !props.a23 && props?.from === 'schedule' && (
        <div className="w-full dark:flex dark:flex-row-reverse dark:gap-1">
          {data?.isFantasyAvailable && (
            <div
              className="flex justify-center w-full items-center dark:p-2 md:mb-1 md:cursor-pointer dark:bg-gray px-2 rounded-md"
              onClick={() => handleNav(data, data.matchStatus)}
            >
              <div
                className="flex justify-center items-center md:cursor-pointer"
                onClick={() => {
                  handleFantasyNav()
                  redirectToFile(data, data?.isLiveCriclyticsAvailable)
                }}
              >
                <div className="lg:hidden md:hidden font-semibold mr-8 dark:text-white text-xs">
                  Fantasy Centre
                </div>

                <div
                  className="lg:block md:block hidden font-semibold dark:text-white text-xs"
                  style={{ position: 'relative' }}
                >
                  <img
                    src="/pngsV2/frcLogo.png"
                    className="object-cover object-top w-full h-24"
                  />
                  <div
                    style={{
                      position: 'absolute',
                      right: '5%',
                      top: '50%',
                      color: '#FFFFFF',
                      fontSize: '16px',
                    }}
                  >
                    FANTASY CENTRE
                  </div>
                </div>
                <ImageWithFallback
                  src={frc}
                  loading="lazy"
                  height={18}
                  width={19}
                  className="w-8 h-8 lg:hidden md:hidden"
                  alt="frc"
                />
              </div>
            </div>
          )}
          {showCriclytics && data.isCricklyticsAvailable && (
            <>
              <Link
                {...criclyticsUrl()}
                passHref
                onClick={handleCriclyticsNav}
                className="hidden md:flex justify-center w-full items-center dark:gap-3 md:px-2 md:cursor-pointer dark:bg-gray dark:p-2 rounded-md"
              >
                <div
                  className="lg:block md:flex hidden font-semibold dark:text-white text-xs"
                  style={{ position: 'relative' }}
                >
                  <img
                    src="/pngsV2/cryclyticsBanner.png"
                    className="object-cover object-center h-24 w-full"
                  />
                  <div
                    style={{
                      position: 'absolute',
                      right: '10%',
                      top: '50%',
                      color: '#FFFFFF',
                      fontSize: '16px',
                    }}
                  >
                    CRICLYTICS
                  </div>
                </div>
              </Link>

              <Link {...getCriclyticdUrlHome(data)} passHref legacyBehavior>
                <div className="flex justify-between w-full items-center dark:gap-3 md:px-2 md:cursor-pointer dark:bg-gray dark:p-2 rounded-md">
                  <div className="md:hidden whitespace-nowrap font-semibold w-24 dark:text-white text-xs">
                    Criclytics Dashboard
                  </div>
                  <ImageWithFallback
                    src={crycLyticsLogo}
                    alt="Criclytics"
                    height={18}
                    width={19}
                    className="w-8 h-8 lg:hidden md:hidden"
                    loading="lazy"
                  />
                </div>
              </Link>
            </>
          )}
        </div>
      )}
    </>
  )
}
