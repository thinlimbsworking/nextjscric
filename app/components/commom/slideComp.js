import React, { useRef, useState } from 'react'
import ImageWithFallback from './Image'
import { from } from '@apollo/client'
const redRight = '/svgs/red-right.svg'
const redLeft = '/svgs/red-left.svg'
export default function SlideComp({ algoteam, lang, from, ...props }) {
  ;<style jsx>{`
    ::-webkit-scrollbar {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height: 4px;
      background-color: #2b323f;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px;
      background-color: #6a91a9;
    }
  `}</style>
  const scrl = useRef()
  const [scrolEnd, setscrolEnd] = useState(false)
  const [scrollX, setscrollX] = useState(0)
  const [scrolStart, setScrolStart] = useState(true)
  const [scrollX1, setscrollX1] = useState(0)
  const getPlayerRole = role => {
    switch (role) {
      case 'BATSMAN':
        return 'BATTER  '
      case 'ALL_ROUNDER':
        return 'ALL ROUNDER  '
      default:
        return role;
    }
  }
  const slide = (shift) => {
    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: 'smooth',
    })
    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
    if (scrl.current.scrollLeft <= 0) setScrolStart(true)
    else setScrolStart(false)
  }
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
    if (scrl.current.scrollLeft <= 0) setScrolStart(true)
    else setScrolStart(false)
  }
  return (
    <div className="bg-gray-10 dark:bg-gray rounded-b-lg p-1 overflow-x-auto w-full ">
      <img
        onClick={() =>
          slide(-((scrl && scrl.current && scrl.current.offsetWidth) / 6) - 10)
        }
        src={redLeft}
        className={`z-10 cursor-pointer hidden md:block lg:block   absolute ${
          from === 'suggestTeam' ? 'top-16' : 'top-20'
        }  -left-8 black  w-6 h-6 ${scrolStart ? 'opacity-70 blur-0' : ''} `}
      />

      <div
        className="scrollmenu flex  overflow-x-scroll cursor-pointer "
        ref={scrl}
        onScroll={scrollCheck}
        onClick={props.handleNav}
      >
        {algoteam &&
          algoteam.length > 0 &&
          algoteam.map((item, y) => (
            <div className="pt-2">
              <div className="flex w-full justify-center">
                <div className="flex flex-col items-center border-r border-slate-200">
                  <div className="relative mb-1  flex justify-center">
                    <ImageWithFallback
                      height={28}
                      width={28}
                      loading="lazy"
                      fallbackSrc="/pngsV2/playerph.png"
                      className="h-12 w-12 bg-white dark:bg-gray-4 rounded-full  object-cover object-top"
                      src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                      // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                    />
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      fallbackSrc="/svgs/empty.svg"
                      className="w-3 h-3 drop-shadow-lg absolute left-0 bottom-0 object-center flex justify-center rounded-full ba"
                      src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                      // onError={(evt) => (evt.target.src = '/svgs/empty.svg')}
                    />
                    {(item.captain == '1' || item.vice_captain == '1') && (
                      <div className="w-5 h-5 text-green bg-gray-4 absolute right-[-4px] bottom-0 rounded-md p-1  flex justify-center items-center text-[0.6rem] font-medium">
                        {item.captain == '1'
                          ? 'C'
                          : item.vice_captain == '1'
                          ? 'VC'
                          : ''}
                      </div>
                    )}
                  </div>
                  <p className="dark:text-white text-[0.66rem] font-medium text-center w-28 truncate">
                    {lang === 'HIN' ? item.playerNameHindi || item.playerName : item.playerName}
                  </p>
                  <p className="text-gray-2 text-[0.56rem] uppercase text-center">
                    {getPlayerRole(item.player_role)}
                  </p>
                </div>
              </div>
            </div>
          ))}
      </div>
      <img
        onClick={() =>
          slide(+((scrl && scrl.current && scrl.current.offsetWidth) / 6) - 10)
        }
        src={redRight}
        className={`z-10 hidden md:block lg:block cursor-pointer  absolute ${
          from === 'suggestTeam' ? 'top-16' : 'top-20'
        } -right-8 black  w-6 h-6 ${scrolEnd ? 'opacity-70 blur-0' : ''} `}
      />
    </div>
  )
}
