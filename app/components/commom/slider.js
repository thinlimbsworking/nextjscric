'use client'
import React, { useState, useRef, useEffect } from 'react'
import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'
import CleverTap from 'clevertap-react'
const Playerfallback = '/pngsV2/playerph.png'
import { getPlayerUrl } from '../../api/services'
import { getStadiumUrl } from '../../api/services'
//import { VIDEOS_VIEW } from '../constant/Links';
//import { GET_VIDEO_POSITIONS } from '../api/queries';
import { useRouter } from 'next/navigation'
import { Swiper, SwiperSlide } from 'swiper/react'
import {
  EffectCoverflow,
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Controller,
  FreeMode,
} from 'swiper'

const IMAGES = {
  backIconWhite: '/svgs/backIconWhite.svg',
  fallBackPlaceHolder: '/pngs/stadium-placeholder.jpg',
  redRight: '/svgs/red-right.svg',
  redLeft: '/svgs/red-left.svg',
}

export default function SliderCommon({ data, ...props }) {

  const [swiper, setSwiper] = useState()
  const [activeIndexPlayer, setaActiveIndexPlayer] = useState(0)
  const prevRef = useRef()
  const nextRef = useRef()
  const [deviceType, setDeviceType] = useState('Desktop')

  useEffect(() => {
    // if (swiper && swiper.params) {
    //   swiper.params.navigation.prevEl = prevRef.current
    //   swiper.params.navigation.nextEl = nextRef.current
    //   swiper.navigation.init()
    //   swiper.navigation.update()
    // }

    if (navigator) {
      const ua = navigator.userAgent
      if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        setDeviceType('tablet')
      } else if (
        /Mobile|Android|iP(hone|od)|IEMobile|BlackBerry|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
          ua,
        )
      ) {
        setDeviceType('mobile')
      } else {
        setDeviceType('desktop')
      }
    }
  }, [swiper])
  const router = useRouter()
  const navigate = router.push

  //const { loading, error, data } = useQuery(GET_VIDEO_POSITIONS);
  const [currentIndex, updateCurrentIndex] = useState(deviceType === 'mobile' ? 2 : 0)

  const getVideoView = (video) => {
    let type = 'featured-videos'
    let videoId = video.videoID
    return {
      as: eval(VIDEOS_VIEW.as),
      href: VIDEOS_VIEW.href,
    }
  }
  const handleVideoNavigation = (videoID) => {
    CleverTap.event('Videos', {
      Source: 'Homepage',
      VideoID: videoID,
      Platform: localStorage ? localStorage.Platform : '',
    })
  }

  const handleNavigation = () => {
    CleverTap.event('VideosHome', {
      Source: 'Homepage',
      Platform: localStorage ? localStorage.Platform : '',
    })
  }

  const handlePlayerCompare = (playerID1, playerSlug, playerID2) => {
    return { as: eval(PLAYER_COMPARE_VIEW.as), href: PLAYER_COMPARE_VIEW.href }
  }

  const handleCompare = (Player2ID) => {
    CleverTap.event('PlayersCompare', {
      Source: 'PlayersCompare',
      Platform: global.window.localStorage
        ? global.window.localStorage.Platform
        : '',
    })
    setDefaultImage(false)
    let playerID2 = Player2ID
    let playerID1 = playerID
    router.push(
      handlePlayerCompare(playerID1, playerSlug, playerID2).href,
      handlePlayerCompare(playerID1, playerSlug, playerID2).as,
    )
  }

  if (false)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center">
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  if (false) return <div></div>
  else
    return (
      <div className=" text-white my-3 ">
        <div className={`flex items-center ${props.event === 'playerDashboard' && 'p-2'}`}>
          <div className="prev mb-8 md:mb-0 lg:mb-0 cursor-pointer hidden md:block lg:block " ref={prevRef}>
            {props.event !== 'players' && props.event !== 'PlayersCompare' && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12.243"
                height="20.242"
                viewBox="0 0 14.243 24.242"
              >
                <path
                  id="Path_18868"
                  data-name="Path 18868"
                  d="M-442-3594l-10,10,10,10"
                  transform="translate(454.121 3596.121)"
                  fill="none"
                  stroke="#9b000d"
                  stroke-linecap="round"
                  stroke-width="3"
                />
              </svg>
            )}
            {(props.event === 'players' ||
              props.event === 'PlayersCompare') && (
                <img src={IMAGES.redLeft} className="w-10 h-10" />
              )}
          </div>

          <Swiper
            // className="external-buttons "
            
            grabCursor={true}
            centeredSlides={props.event === 'playerDashboard' && deviceType === 'desktop' ? false : true}
            // slidesPerView={"auto"}
            coverflowEffect={{
              // rotate: 0,
              // stretch: 0,
              // depth: 0,
              // modifier: 1,
              slideShadows: true,
            }}
            pagination={true}
            modules={[EffectCoverflow,Navigation]}
            className="mySwiper"



            // modules={[
            //   EffectCoverflow,
            //   Navigation,
            //   Pagination,
            //   Scrollbar,
            //   A11y,
            //   Controller,
            // ]}
            // initialSlide={playerComponent?playerIndex:0}
            navigation={{
              prevEl: '.prev',
              nextEl: '.next',
            }}
            updateOnWindowResize
            observer
            observeParents
            onSwiper={setSwiper}
            onSlideChange={(swiper) => { updateCurrentIndex(swiper.realIndex === 0 ? deviceType === 'mobile' ? 2 : 0 : swiper.realIndex); props?.updateCurrentIndex && props?.updateCurrentIndex(swiper.realIndex === 0 ? deviceType === 'mobile' ? 2 : 0 : swiper.realIndex) }}
            shouldSwiperUpdate={true}
            loop={true}
            // grabCursor={true}
            // centeredSlides={deviceType === 'mobile' && props.event === 'playerDashboard'}
            slidesPerView={
              deviceType === 'mobile'
                ? props.event === 'stadiums'
                  ? '2.5'
                  : props.event === 'playerDashboard'
                    ? '5'
                    : '3'
                : props.event === 'playerDashboard'
                  ? '8'
                  : '5'
            }
          >
            {data.map((player, i) => (
              <SwiperSlide
                // onClick={() =>  navigate(`players/compare/${player.playerID}`)}
                key={i}
              >
                {/* for players */}

                {props.event === 'playerDashboard' && (
                  <div key={i} className={`w-full `}>
                    <Link
                      className="w-full"
                      {...props.tabSwitch(player)}
                      legacyBehavior
                    >

                      <div
                        className={`w-1/8  dark:bg-none text-xs dark:sky-100 dark:border-b-2 rounded-full dark:rounded-none mx-1 dark:mx-0 border-gray-2 dark:border-sky-100 ${props.tags === player
                            ? 'dark:border-b-2 bg-basered dark:bg-transparent text-white dark:border-sky-300 dark:text-sky-300'
                            : 'bg-gray-10 dark:bg-transparent text-black dark:text-sky-100'
                          } cursor-pointer flex items-center justify-center py-2 `}
                      >
                        {player}
                      </div>
                    </Link>
                  </div>
                )}

                {/* for stadium */}

                {props.event === 'stadiums' && (
                  <div
                    key={i}
                    className="md:mx-2 lg:mx-2 h-48 p-1 md:p-2 lg:p-2 border dark:border-none text-black dark:text-white  rounded-md pr-2 "
                    onClick={() => props.handleVenueNavigation(player.id)}
                  >
                    <div className="  relative overflow-hidden ">
                      <Link {...getStadiumUrl(player)} passHref>
                        <img
                          className=" h-32 w-full object-cover dark:rounded-md "
                          onLoadStart={() => <Loading />}
                          src={
                            player.image
                              ? `${player.image}?auto=compress&dpr=2`
                              : `https://images.cricket.com/stadiums/${player.id}_actionshot_safari.jpg`
                          }
                          onError={(e) => {
                            e.target.src = IMAGES.fallBackPlaceHolder
                          }}
                          alt="stadium"
                        />
                      </Link>
                    </div>
                    <div className="mt-3">
                      <div className=" text-xs font-medium">{player.name}</div>
                    </div>
                  </div>
                )}

                {props.event === 'players' && (
                  <Link
                    {...getPlayerUrl(player)}
                    passHref
                    className="w-full h-min  "
                    legacyBehavior
                  >
                    <div className="relative flex text-black dark:text-gray-2 flex-col gap-2 items-center">
                      <div
                        className={`rounded-full bg-light_gray dark:bg-gray-8 ${currentIndex === i ? 'w-24 h-24 ' : 'w-24 h-24'
                          }   overflow-hidden flex justify-center items-top  bg-gray-2 `}
                      >
                        {player.headShotImage !== '' ? (
                          <Imgix
                            src={`${player.headShotImage}?fit=crop&crop=face`}
                            width={currentIndex === i ? 127 : 94}
                            height={currentIndex === i ? 183 : 160}
                          />
                        ) : (
                          <img
                            className="  "
                            // style={{ height: currentIndex === i ? 183 : 160, width: currentIndex === i ? 127 : 94 }}
                            src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                            alt={''}
                            onError={(evt) => (evt.target.src = Playerfallback)}
                          />
                        )}
                      </div>
                      <div className="mt-2 text-xs font-semibold w-24 text-center truncate whitespace-nowrap">
                        {player.playerName}

                      </div>
                      <div className=" text-xs text-green-3 ">
                        {/* {Math.round((player.similarity*100)/100)}% */}
                        { Number(Math.round(player.similarity + 'e' + 2) + 'e-' + 2).toFixed(
      2,
    )}%
                      </div>
                    </div>
                  </Link>
                )}
                {
                  props.event === 'PlayersCompare' && (
                    // <Link { ...getPlayerUrl(player)} passHref className='w-full h-min  '>

                    <div
                      className="relative text-black dark:text-gray-2 flex flex-col gap-2 items-center"
                      onClick={() => props.handleCompare(player.playerID)}
                    >
                      <div
                        className={` rounded-full bg-gray-10 dark:bg-gray-8 ${currentIndex === i ? 'w-24 h-24 ' : 'w-24 h-24'
                          }   overflow-hidden flex justify-center items-top   `}
                      >
                        {player.headShotImage !== '' ? (
                          <Imgix
                            src={`${player.headShotImage}?fit=crop&crop=face`}
                            width={currentIndex === i ? 127 : 94}
                            height={currentIndex === i ? 183 : 160}
                          />
                        ) : (
                          <img
                            className="  "
                            // style={{ height: currentIndex === i ? 183 : 160, width: currentIndex === i ? 127 : 94 }}
                            src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                            alt={''}
                            onError={(evt) => (evt.target.src = Playerfallback)}
                          />
                        )}
                        <div className='w-6 h-6 flex justify-center items-center absolute bottom-12 bg-gray dark:bg-white rounded-full'><img className=' w-4 h-4 ' src='/pngsV2/swap.png' /></div>
                        
                      </div>
                      <div className="mt-2 text-xs w-24 text-center truncate whitespace-nowrap overflow-hidden">
                        {player.playerName}
                      </div>
                      <div className=" text-xs text-green-3 ">
                      { Number(Math.round(player.similarity + 'e' + 2) + 'e-' + 2).toFixed(
      2,
    )}%
                      </div>
                    </div>
                  )

                  // </Link>
                }
              </SwiperSlide>
            ))}
          </Swiper>
          <div className=" next mb-8 md:mb-0 lg:mb-0  text-2xl cursor-pointer hidden md:block lg:block" ref={nextRef}>
            {props.event !== 'players' && props.event !== 'PlayersCompare' && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12.243"
                height="20.242"
                viewBox="0 0 14.243 24.242"
              >
                <path
                  id="Path_18869"
                  data-name="Path 18869"
                  d="M-452-3594l10,10-10,10"
                  transform="translate(454.121 3596.122)"
                  fill="none"
                  stroke="#9b000d"
                  stroke-linecap="round"
                  stroke-width="3"
                />
              </svg>
            )}
            {(props.event === 'players' ||
              props.event === 'PlayersCompare') && (
                <img src={IMAGES.redRight} className="w-10 h-10" />
              )}
          </div>
        </div>
      </div>
    )
}
