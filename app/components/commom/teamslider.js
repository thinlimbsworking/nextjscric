import React from 'react'
import { HOME_PAGE_GET_ALGO11 } from '../../api/queries'
import { Link } from 'next/link'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { useRef, useState } from 'react'
const arrow = '/pngsV2/fantasy_Icon.png'
import { useRouter } from 'next/router'

export default function BuildYourTeam(props) {
  //
  let scrl = useRef(null)
  const [scrollX, setscrollX] = useState(0)
  const [scrolEnd, setscrolEnd] = useState(false)

  //Slide click
  const slide = (shift) => {
    scrl.current.scrollLeft += shift
    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }
  const scrollCheck = () => {
    setscrollX(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  const firstItemRef = useRef(null)
  const router = useRouter()
  const navigate = router.push
  const { loading, error, data } = useQuery(HOME_PAGE_GET_ALGO11, {
    variables: { matchID: '197688' },
  })
  //
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    let sec = Math.floor((distance % (1000 * 60)) / 1000)

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago'
    return result
  }
  ;<style jsx>{`
    -webkit-scrollbar {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height: 4px;
      background-color: #2b323f;
    }
    ::-webkit-scrollbar:hover {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height: 4px;
      background-color: #2b323f;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px;
      background-color: #6a91a9;
    }
  `}</style>
  // {}
  return props.data ? (
    <div className="p-3  ">
      {props.data &&
        props.data.teams.map((item, index) => {
          return (
            <div className="flex justify-center">
              {}

              <div
                className="flex  items-center justify-center"
                onClick={() => slide(-50)}
              >
                <svg width="30" focusable="false" viewBox="0 0 24 24">
                  <path
                    fill="#38d925"
                    d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
              <div className=" w-full bg-gray-4   my-3 rounded-md">
                <div className="">
                  <div className="p-2 flex items-center justify-center">
                    <div className="text-xs font-medium">PROJECTED SCORE :</div>
                    <div className="text-green pl-1 text-sm font-medium">
                      <Link
                        href={{
                          pathname: '/fantasy-research-center/[...slugs]',
                        }}
                        as={`/fantasy-research-center/${props.matchID}/${
                          props.seriesSlug
                        }/${
                          team.leagueType === 'statsHub'
                            ? 'fantasy-stats/players'
                            : 'create-team'
                        }`}
                        passHref
                      >
                        <div className="ttu  flex f6 fw6 ph3 bg-dark-gray  pa2 br2 db ">
                          <div>
                            <img className="" src={editTeam} />
                          </div>
                          <div className="pl1">
                            {lang === 'HIN' ? 'टीम एडिट करें' : 'EDIT TEAM'}
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>

                  <div
                    className="flex  rounded-lg p-1 overflow-x-auto w-full   overflow-x-auto"
                    ref={scrl}
                    onScroll={scrollCheck}
                  >
                    <div className="flex bg-gray  ">
                      {true &&
                        item.team.map((item, y) => {
                          return (
                            <div className=" mr-2  border-r-2 border-gray-4   p-2 px-4  w-32">
                              <div className="flex w-full justify-center items-center">
                                <div className="flex flex-col items-center ">
                                  <div className="relative mb-2">
                                    <img
                                      className="h-16 w-16 bg-gray-4 rounded-full  object-cover object-top"
                                      src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                                      onError={(evt) =>
                                        (evt.target.src =
                                          '/pngsV2/playerph.png')
                                      }
                                    />
                                    <img
                                      className="w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba"
                                      src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                      onError={(evt) =>
                                        (evt.target.src =
                                          '/svgs/images/flag_empty.svg')
                                      }
                                    />
                                    {(item.captain == '1' ||
                                      item.vice_captain == '1') && (
                                      <div className="w-5 h-5 absolute right-[-4px] bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium">
                                        {item.captain == '1'
                                          ? 'C'
                                          : item.vice_captain == '1'
                                          ? 'VC'
                                          : ''}
                                      </div>
                                    )}
                                  </div>
                                  <p
                                    className="text-xs text-center font-medium leading-5 w-full wordWrapLine"
                                    style={{ wordWrap: 'break-word' }}
                                  >
                                    {item.playerName}
                                  </p>
                                  <p className="text-gray-2 text-xs uppercase">
                                    Batter
                                  </p>
                                </div>
                                {/* <div className='bg-black w-[1px] h-10'></div> */}
                              </div>
                            </div>
                          )
                        })}

                      {/* bowler */}
                    </div>
                  </div>
                </div>
              </div>
              <div
                className=" flex items-center justify-center"
                onClick={() => slide(+50)}
              >
                <svg width="30" viewBox="0 0 24 24">
                  <path
                    fill="#38d925"
                    d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
            </div>
          )
        })}
    </div>
  ) : (
    <></>
  )
}
