import React, { useState } from 'react'

const Tooltip = ({
  delay,
  content,
  className = '',
  direction,
  children,
  addCoinsToolTip,
}) => {
  let timeout
  const [active, setActive] = useState(false)

  const showTip = () => {
    timeout = setTimeout(() => {
      setActive(true)
    }, delay || 400)
  }

  const hideTip = () => {
    clearInterval(timeout)
    setActive(false)
  }

  return (
    <div
      className={`relative ${className}`}
      onMouseEnter={showTip}
      onMouseLeave={hideTip}
    >
      <div className={`${addCoinsToolTip ? '' : 'h-7 w-7'}`}>{children}</div>
      {active && (
        <div
          className={` ${
            addCoinsToolTip
              ? 'mr-4 left-1/2 bottom-0'
              : 'absolute bottom-4 mb-3 break-words w-20 text-center h-10 -ml-6 '
          } bg-gray-2 text-xs text-white border-solid border-gray border-2 border-r-4 border-b-4 border-r-solid border-b-solid border-b-red${
            direction || 'top-0'
          }`}
        >
          {content}
        </div>
      )}
    </div>
  )
}

export default Tooltip
