import React from 'react'
import { MATCH_SUMMARY, MATCH_DATA_TAB_WISE } from '../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import Heading from './heading'

const flagPlaceHolder = '/pngsV2/flag_dark.png'

export default function Topperform({
  matchID,
  data,
  status,
  matchSummary,
  ...props
}) {
  if (matchID) {
    const { loading, error, data: matchSummaryData } = useQuery(MATCH_SUMMARY, {
      variables: { matchID, status },
    })

    const tabwise = useQuery(MATCH_DATA_TAB_WISE, {
      variables: { matchID },
    })

    matchSummary = tabwise?.data?.getMatchCardTabWiseByMatchID?.matchSummary

    data = matchSummaryData
  }

  console.log(matchSummary, 'matchSummary', data, 'data')

  return (
    <div className="text-black dark:text-white white bg-white dark:bg-gray-8 w-full">
      {true && data && data.matchSummary && (
        <div className="w-full">
          {props.showHeading && (
            <div className="mb-2">
              {' '}
              <Heading heading="MATCH SUMMARY" />
            </div>
          )}
          {/* <div className="dark:  p-3 text-base font-semibold">
            MATCH SUMMARY
            <div className="border-b-gray-3 h-[1px] my-1 w-[60px] border-b-2 border-b-solid "></div>
          </div> */}

          <div className="dark:bg-gray bg-white  pb-3 ">
            <div className="flex">
              <div className="w-6/12 flex flex-col p-2 items-center">
                <div className="dark:text-gray-2  font-semibold mb-1 text-xs">
                  TOP BATTER
                </div>
                <div className="relative   bg-gray-10 dark:bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center">
                  {/* <div className="overflow-hidden w-full h-full rounded-full "> */}
                  <div className="overflow-hidden w-full h-full rounded-full ">
                    <img
                      className="  object-top object-contain w-24  rounded-full   "
                      style={{ objectPosition: '0 0%' }}
                      src={`https://images.cricket.com/players/${
                        matchSummary?.topPerformer.batsman.playerID ||
                        data.matchSummary.bestBatsman.playerID
                      }_headshot.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngs/fallbackprojection.png')
                      }
                      alt=""
                    ></img>
                  </div>
                  {/* </div> */}
                </div>

                {/* matchSummary.topPerformer.bowler */}

                <div className="w-full -mt-10 dark:bg-gray-4 bg-gray-10 h-32 p-2 text-center rounded-md px-2">
                  <div className="text-sm font-semibold mt-10 truncate">
                    {matchSummary?.topPerformer.batsman.playerName ||
                      data.matchSummary.bestBatsman.playerName}
                  </div>
                  {data?.matchSummary.bestBatsman.battingStatsList &&
                  props.matchType !== 'Test' ? (
                    <div>
                      <div className="flex flex-col items-center py-1">
                        {(matchSummary?.topPerformer.batsman ||
                          data.matchSummary.bestBatsman) && (
                          <div className="text-sm font-semibold truncate dark: ">
                            <span className="f5 fw5 mr1">
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0]?.runs
                              }
                              {data.matchSummary.bestBatsman.battingStatsList[0]
                                ?.isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="t font-normal">
                              {' '}
                              (
                              {matchSummary?.topPerformer.batsman
                                .playerMatchBalls ||
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0]?.balls}
                              )
                            </span>
                          </div>
                        )}
                        <div className="flex  mt-2 justify-between items-center">
                          <div className="mr-1 flex items-center gap-1">
                            <span className="text-xs truncate">4s: </span>
                            <span className="text-sm font-semibold truncate dark: ">
                              {` `}
                              {matchSummary?.topPerformer.batsman
                                .playerMatchFours ||
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0]?.fours}
                            </span>
                          </div>
                          <div className="ml-1 flex items-center gap-1 border-l">
                            <span className=" text-xs ml-2">6s:</span>
                            <div className="text-xs font-semibold  ">
                              <span className="text-sm font-semibold truncate dark: ">
                                {` `}
                                {matchSummary?.topPerformer.batsman
                                  .playerMatchSixes ||
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0]?.sixes}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className="mt-1">
                      <div className="flex p-1  items-center justify-center">
                        {(matchSummary?.topPerformer.batsman ||
                          data.matchSummary.bestBatsman) && (
                          <div className="w-full flex flex-col gap-1 items-center">
                            <div className="flex gap-1 justify-center">
                              <div className="text-xs font-semibold  ">
                                <span className="f5 fw5 mr1">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0]?.runs
                                  }
                                  {data.matchSummary.bestBatsman
                                    .battingStatsList[0]?.isNotOut === true
                                    ? '*'
                                    : ''}
                                </span>
                                <span className="t font-normal">
                                  {' '}
                                  (
                                  {matchSummary?.topPerformer.batsman
                                    .playerMatchBalls ||
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0]?.balls}
                                  )
                                </span>
                              </div>
                              <span className="text-xs">&</span>
                              <div className="text-xs font-semibold ">
                                <span className="f5 fw5 mr1">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[1]?.runs
                                  }
                                  {data.matchSummary.bestBatsman
                                    .battingStatsList[1]?.isNotOut === true
                                    ? '*'
                                    : ''}
                                </span>
                                <span className="t font-normal">
                                  {' '}
                                  (
                                  {matchSummary?.topPerformer.batsman
                                    .playerMatchBalls ||
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[1]?.balls}
                                  )
                                </span>
                              </div>
                            </div>
                            <div className="flex hidden justify-between ">
                              <div className=" border-r flex items-center gap-1 pr-1">
                                <span className="text-xs ">4s: </span>
                                <span className="text-sm font-semibold  ">
                                  {` `}
                                  {matchSummary?.topPerformer.batsman
                                    .playerMatchFours ||
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0]?.fours}
                                </span>
                                <span className="text-xs">&</span>
                                <span className="text-sm font-semibold  ">
                                  {` `}
                                  {matchSummary?.topPerformer.batsman
                                    .playerMatchFours ||
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[1]?.fours}
                                </span>
                              </div>
                              <div className="hidden border-l flex items-center gap-1 pl-1">
                                <span className=" text-xs ">6s:</span>
                                <div className="text-sm font-semibold  ">
                                  <span className="text-sm font-semibold  ">
                                    {` `}
                                    {matchSummary?.topPerformer.batsman
                                      .playerMatchSixes ||
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[0]?.sixes}
                                  </span>
                                </div>
                                <span className="text-xs">&</span>
                                <span className="text-sm font-semibold  ">
                                  {` `}
                                  {matchSummary?.topPerformer.batsman
                                    .playerMatchFours ||
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[1]?.sixes}
                                </span>
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                </div>
              </div>

              {(matchSummary?.topPerformer || data.matchSummary) &&
              props.matchType !== 'Test' ? (
                <div className="w-6/12 flex flex-col p-2 items-center">
                  <div className="dark:text-gray-2  font-semibold mb-1 text-xs">
                    TOP BOWLER
                  </div>
                  <div className="relative bg-gray-10 dark:bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                    <div className="overflow-hidden w-full h-full rounded-full ">
                      <img
                        className=" object-top object-contain w-24  rounded-full  "
                        src={`https://images.cricket.com/players/${
                          matchSummary?.topPerformer.bowler.playerID ||
                          data.matchSummary.bestBowler.playerID
                        }_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngs/fallbackprojection.png')
                        }
                        alt=""
                      ></img>
                    </div>
                  </div>

                  <div className="w-full -mt-10 dark:bg-gray-4 text-center h-32 p-2 bg-gray-10 rounded-md px-2">
                    <div className="text-sm  font-semibold mt-10   truncate">
                      {'sss' === 'HIN'
                        ? data.matchSummary.bestBowler.playerNameHindi
                        : matchSummary?.topPerformer.bowler.playerName ||
                          data.matchSummary.bestBowler.playerName}
                    </div>
                    <div className="flex flex-col  items-center justify-between pb-1 ">
                      {data.matchSummary.bestBowler && (
                        <div className="text-sm">
                          <span className="font-bold  ">
                            {matchSummary?.topPerformer.bowler
                              .playerWicketsTaken ||
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                ?.wickets}
                            /
                            {matchSummary?.topPerformer.bowler
                              .playerRunsConceeded ||
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                ?.runsConceded}
                          </span>
                          <span className="f7 text-sm">
                            (
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                ?.overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                    <div className="text-center md:py-2 dark:py-1">
                      <span className="dark:text-white text-gray text-xs">
                        Eco :
                      </span>
                      <span className="dark:  text-xs font-semibold ml-1">
                        {(matchSummary &&
                          matchSummary?.topPerformer.bowler
                            .playerOversBowled) ||
                          (data.matchSummary.bestBowler.playerOversBowled &&
                            matchSummary.topPerformer.bowler
                              .playerEconomyRate) ||
                          data.matchSummary.bestBowler.bowlingStatsList[0]
                            ?.economyRate}
                      </span>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="w-6/12 flex flex-col p-2 items-center">
                  <div className="dark:text-gray-2  font-semibold mb-1 text-xs">
                    TOP BOWLER
                  </div>
                  <div className="relative bg-gray-10 dark:bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                    <div className="overflow-hidden w-full h-full rounded-full ">
                      <img
                        className=" object-top object-contain w-24  rounded-full  "
                        src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngs/fallbackprojection.png')
                        }
                        alt=""
                      ></img>
                    </div>
                  </div>
                  <div className="w-full -mt-10 dark:bg-gray-4 text-center h-32 p-2 bg-gray-10 rounded-md px-2">
                    <div className="text-sm  font-semibold mt-10   truncate">
                      {'sss' === 'HIN'
                        ? data.matchSummary.bestBowler.playerNameHindi
                        : data.matchSummary.bestBowler.playerName ||
                          data.matchSummary.bestBowler.playerNameHindi}
                    </div>

                    <div className="flex items-center justify-center  mt-1 w-full gap-1 ">
                      <div className="text-xs font-semibold ">
                        {/* {data.matchSummary.bestBowler &&
                          data.matchSummary.bestBowler.battingStatsList &&
                          data.matchSummary.bestBowler.battingStatsList.length >
                            0 && (
                            <div className="font-semibold">
                              {' '}
                              <span className="f6 fw5">
                                {
                                  data.matchSummary.bestBowler
                                    .battingStatsList[0]?.runs
                                }
                                {data.matchSummary.bestBowler
                                  .battingStatsList[0]?.isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="f7 ">
                                &nbsp;(
                                {
                                  data.matchSummary.bestBowler
                                    .battingStatsList[0]?.balls
                                }
                                )
                              </span>
                            </div>
                          )} */}
                        {data.matchSummary.bestBowler.bowlingStatsList && (
                          <div className="ba mv2  bg-silver br2 tc pv1 b--black  ph2">
                            {' '}
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  ?.wickets
                              }
                              /
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  ?.runsConceded
                              }
                            </span>
                            {/* <span>&nbsp;</span> */}
                            <span className="text-xs ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  ?.overs
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                      <div className="text-xs ">
                        {data.matchSummary.bestBowler.battingStatsList[1] && (
                          <div className="pa1 f7">&</div>
                        )}
                      </div>
                      <div className="text-xs font-semibold ">
                        {/* {data.matchSummary.bestBowler &&
                          data.matchSummary.bestBowler.battingStatsList &&
                          data.matchSummary.bestBowler.battingStatsList.length >
                            0 &&
                          data.matchSummary.bestBowler.battingStatsList[1] && (
                            <div className="ba bg-frc-yellow  tc br2 pv1 b--black black  ph2">
                              {data.matchSummary.bestBowler
                                .battingStatsList[1] && (
                                <span className="f6 fw5">
                                  {
                                    data.matchSummary.bestBowler
                                      .battingStatsList[1].runs
                                  }
                                  {data.matchSummary.bestBowler
                                    .battingStatsList[1].isNotOut === true
                                    ? '*'
                                    : ''}
                                </span>
                              )}
                              {data.matchSummary.bestBowler
                                .battingStatsList[1] && (
                                <span className="f7 ">
                                  &nbsp;(
                                  {
                                    data.matchSummary.bestBowler
                                      .battingStatsList[1].balls
                                  }
                                  )
                                </span>
                              )}
                            </div>
                          )} */}
                        {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                          <div className="ba mv2 bg-silver br2 pv1 b--black  tc ph2">
                            {' '}
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .wickets
                              }
                              /
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .runsConceded
                              }
                            </span>
                            <span className="f7 ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .overs
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="text-center hidden w-full py-2 dark:text-white">
                      <span className="dark:text-white text-gray text-xs">
                        Eco :
                      </span>
                      <span className="dark:  text-xs font-semibold ml-1">
                        {
                          data.matchSummary.bestBowler.bowlingStatsList[0]
                            ?.economyRate
                        }
                      </span>
                      <span className="text-xs"> & </span>
                      <span className="  text-xs font-semibold ">
                        {
                          data.matchSummary.bestBowler.bowlingStatsList[1]
                            ?.economyRate
                        }
                      </span>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="flex flex-col items-center justify-center mx-3">
              {matchSummary &&
                matchSummary?.innings.map((item, index) => {
                  return (
                    <>
                      <div className="w-full flex items-center justify-between my-2 dark:bg-gray-4 md:bg-gradient-to-r md:from-[#D1D1D1] p-2">
                        <div className="flex items-center justify-center">
                          <img
                            className="w-12 h-8 rounded object-cover object-top"
                            src={`https://images.cricket.com/teams/${item?.score?.battingTeamID}_flag_safari.png`}
                            alt=""
                            onError={(evt) =>
                              (evt.target.src = flagPlaceHolder)
                            }
                          />
                          <span className="ml-2 dark:  text-xs font-semibold">
                            {item.score.battingTeamName}
                          </span>
                        </div>
                        <div className="flex text-base font-bold dark: ">
                          {' '}
                          {item.score.runsScored}/{item.score.wickets}{' '}
                          <span className="text-sm ml-1 mt-0.5 font-normal">
                            {'('}
                            {item.score.overs} {')'}
                          </span>{' '}
                        </div>
                      </div>
                      <div className="w-full flex flex-row  items-center justify-start my-2 ">
                        <div className=" flex-col flex items-center justify-start w-6/12  text-xs dark:text-gray-2 text-black border-r border-r-gray-11">
                          <div className="flex items-center justify-start w-full py-2 font-bold">
                            BATTER
                          </div>

                          {item.battingList.map((itemInner, indexInner) => {
                            return (
                              <>
                                <div className="flex items-center justify-start w-full text-sm  truncate py-1">
                                  {itemInner.playerName}
                                </div>
                                <div className="flex items-center justify-start w-full text-sm  py-1 ">
                                  {itemInner.playerMatchRuns} {'('}
                                  {itemInner.playerMatchBalls}
                                  {')'}{' '}
                                </div>
                              </>
                            )
                          })}
                        </div>

                        <div className="flex flex-col items-center justify-end w-6/12 dark:text-gray-2 text-black text-xs">
                          <div className="flex items-center justify-end py-2 font-bold w-full ">
                            BOWLER
                          </div>

                          {item.bowlingList.map((itemInner, indexInner) => {
                            return (
                              <>
                                <div className="flex items-center  justify-end w-full text-sm  truncate  py-1">
                                  {itemInner.playerName}
                                </div>
                                <div className="flex items-center justify-end w-full text-sm  py-1">
                                  {itemInner.playerWicketsTaken}/
                                  {itemInner.playerRunsConceeded}{' '}
                                </div>
                              </>
                            )
                          })}
                        </div>
                      </div>
                    </>
                  )
                })}{' '}
            </div>

            {!matchSummary && (
              <>
                {data.matchSummary.inningOrder
                  .slice(
                    0,
                    props.matchType !== 'Test'
                      ? 2
                      : data.matchSummary.inningOrder.length,
                  )
                  .map((inning, i) => (
                    <div key={i}>
                      {/* <div className='h-solid-divider-light mh3 mv2 '></div> */}
                      <div className="w-full p-2">
                        {/* {} */}
                        <div className="w-full flex items-center justify-start my-2 dark:bg-gray-4 md:bg-gradient-to-r md:from-[#D1D1D1] p-2">
                          <div className="flex w-full  items-center  gap-2">
                            <div className="w-1/2 md:w-24 lg:w-24 flex items-center gap-1 ">
                              <img
                                className=" w-12 h-8 rounded object-cover object-top"
                                src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                                alt=""
                                onError={(evt) =>
                                  (evt.target.src = flagPlaceHolder)
                                }
                              />
                              <div className="ml-2 dark:  text-sm font-semibold">
                                {data.matchSummary[
                                  inning
                                ].teamShortName.toUpperCase()}
                              </div>
                            </div>
                            <div className="w-1/2 md:w-40 lg:w-40  flex items-center justify-end  md:justify-start lg:justify-start">
                              <div className=" flex text-base font-bold dark:">
                                {data.matchSummary[inning].runs[i <= 1 ? 0 : 1]}
                                /
                                {
                                  data.matchSummary[inning].wickets[
                                    i <= 1 ? 0 : 1
                                  ]
                                }
                              </div>
                              <div className="text-sm ml-1 mt-0.5 font-normal">
                                (
                                {
                                  data.matchSummary[inning].overs[
                                    i <= 1 ? 0 : 1
                                  ]
                                }
                                )
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className=" text-xs flex justify-between">
                          {data.matchSummary[inning][
                            i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                          ].topBatsman &&
                            data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].topBatsman.battingStatsList && (
                              <div className="w-5/12  md:w-1/6 lg:w-1/6  flex flex-col gap-1">
                                <div className="text-gray-2 uppercase text-xs font-semibold">
                                  Batter
                                </div>
                                <div className="flex  items-center moon-gray w-full justify-between ">
                                  {'sss' === 'HIN' ? (
                                    <div className="  font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerNameHindi
                                      }
                                    </div>
                                  ) : (
                                    <div className="   font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerName
                                      }
                                    </div>
                                  )}
                                  <div className={` font-semibold `}>
                                    {' '}
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.battingStatsList[0]?.runs
                                    }
                                    {data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.battingStatsList[0]
                                      ?.isNotOut === true
                                      ? '*'
                                      : ''}
                                    {'('}
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].runnerBatsman.battingStatsList[0]?.balls
                                    }
                                    {')'}
                                  </div>
                                </div>
                                {true &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman.battingStatsList && (
                                    <div className="flex  items-center moon-gray w-full justify-between  ">
                                      {'sss' === 'HIN' ? (
                                        <div className="font-semibold">
                                          {' '}
                                          {
                                            data.matchSummary[inning][
                                              i <= 1
                                                ? 'batsmanSummary1'
                                                : 'batsmanSummary2'
                                            ].runnerBatsman.playerNameHindi
                                          }
                                        </div>
                                      ) : (
                                        <div className="  font-semibold">
                                          {' '}
                                          {
                                            data.matchSummary[inning][
                                              i <= 1
                                                ? 'batsmanSummary1'
                                                : 'batsmanSummary2'
                                            ].runnerBatsman.playerName
                                          }
                                        </div>
                                      )}
                                      <div className=" font-semibold">
                                        {' '}
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.battingStatsList[0]
                                            ?.runs
                                        }
                                        {data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.battingStatsList[0]
                                          ?.isNotOut === true
                                          ? '*'
                                          : ''}
                                        {'('}
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.battingStatsList[0]
                                            ?.balls
                                        }
                                        {')'}
                                      </div>
                                    </div>
                                  )}
                              </div>
                            )}

                          <div className="w-5/12   md:w-1/6 lg:w-1/6 flex flex-col items-start gap-1">
                            <div className="text-gray-2 uppercase  font-semibold">
                              Bowler
                            </div>
                            {data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .topBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .topBowler.bowlingStatsList && (
                                <div className="flex moon-gray items-center w-full justify-between text-xs">
                                  {'sss' === 'HIN' ? (
                                    <div className="font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerNameHindi
                                      }
                                    </div>
                                  ) : (
                                    <div className="font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerName
                                      }
                                    </div>
                                  )}
                                  <div className=" font-semibold">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.bowlingStatsList[0]?.wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.bowlingStatsList[0]
                                        ?.runsConceded
                                    }
                                  </div>
                                </div>
                              )}
                            {true &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler.bowlingStatsList && (
                                <div className="flex moon-gray items-center  w-full justify-between  ">
                                  {'sss' === 'HIN' ? (
                                    <div className="  font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.playerNameHindi
                                      }
                                    </div>
                                  ) : (
                                    <div className="font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.playerName
                                      }
                                    </div>
                                  )}
                                  <div className="font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0]
                                        ?.wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0]
                                        ?.runsConceded
                                    }
                                  </div>
                                </div>
                              )}
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
              </>
            )}
          </div>
        </div>
      )}
    </div>
  )
}
