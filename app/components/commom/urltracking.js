import React,{useState,useEffect} from 'react'
import CleverTap from 'clevertap-react';
export default function Urltracking(props) {

    const [cleverTapObject, updateClevertap] = useState(null);
    const [windowObject, updateWindowObject] = useState(null);

useEffect(() => {
  
    updateClevertap({ ...CleverTap })
    updateWindowObject({...global.window})
    
  }, [])
  

  useEffect(() => {
 if (cleverTapObject)
    {

     cleverTapObject.event(props.PageName?props.PageName:"", {
        Source:windowObject.document.referrer==="https://l.facebook.com/"?"Facebook": windowObject.document.referrer==="https://t.co/"?"Twitter":'Miscellaneous',
        Platform: localStorage.Platform    
      })
    }
  }, [cleverTapObject,windowObject])
    return (
        <div>
              </div>
    )
}
