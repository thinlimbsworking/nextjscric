import React, { useState, useEffect } from 'react';
const backIconWhite = '/svgs/backIconWhite.svg';
import finalfour from '../../public/svgs/final-four.svg';
import playerindex from '../../public/svgs/player-index.svg';
import Finalfour from '../Criclytics/finalFour';
import Playerindex from '../Criclytics/playerIndex';
import { Router, navigate } from '@reach/router';
import share from '../.."/svgs/share-line.png"';
export default function IplTabs(props) {
  const [tab, setTab] = useState('');
  return (
    <div className=''>
      <div className='bg-navy  flex justify-between items-center dn-l'>
        <div className='w2-5 h2-4 flex justify-center items-center cursor-pointer dn-l'>
          <img className='cursor-pointer' alt='' onClick={() => window.history.back()} src={backIconWhite}></img>
        </div>
        <p className='f5 fw5 mv0 flex-auto white pl2 pl3-l truncate'>IPL 2020</p>
        <div className='w2-5 h2-4 flex justify-center items-center relative cursor-pointer'>
          <img src={share} alt='' className='w13'></img>
        </div>
      </div>
      <div className=''>
        <div className='flex justify-start items-center pa2 bg-white shadow-4 dn-l'>
          {[
            { tabName: 'Final Four', selected: finalfour },
            { tabName: 'Most Valuable Player', selected: playerindex }
          ].map((tabs, i) => (
            <div
              key={i}
              className={`ba br2 mr2 w35 h35 ${tabs.tabName === tab || (tab === '' && i === 0) ? 'o-100' : 'o-50'} `}
              onClick={() => (
                navigate(`${tabs.tabName.split(' ').join('').toLowerCase()}`, {
                  replace: true
                }),
                setTab(tabs.tabName)
              )}>
              <div className='tc center pt2'>
                <img src={tabs.selected} alt='' className='h25 w25' />
              </div>
              <div className='f7 f8 tc'>{tabs.tabName}</div>
            </div>
          ))}
        </div>
        <div className='dn db-l mw75-l center'>
          <div className='flex justify-between items-center'>
            {[
              { tabName: 'Final Four', selected: finalfour },
              { tabName: 'Most Valuable Player', selected: playerindex }
            ].map((tabs, i) => (
              <div
                key={i}
                className={`br2 w-49 h35 flex items-center justify-center cursor-pointer ${
                  tabs.tabName === tab || (tab === '' && i === 0) ? 'cdcgr white' : 'bg-white'
                } shadow-4`}
                onClick={() => (
                  navigate(`${tabs.tabName.split(' ').join('').toLowerCase()}`, { replace: true }), setTab(tabs.tabName)
                )}>
                <img src={tabs.selected} alt='' className='h3 pr2' />
                <div className='f6 fw5'>{tabs.tabName}</div>
              </div>
            ))}
          </div>
        </div>
        <Router>
          <Finalfour path='finalfour' series={props.series} />
          <Playerindex path='playerindex' />
        </Router>
      </div>
    </div>
  );
}
