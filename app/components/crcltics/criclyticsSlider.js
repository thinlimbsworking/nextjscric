import React, { useState } from 'react'
import SwipeableViews from 'react-swipeable-views'
const backIconWhite = '/svgs/backIconWhite.svg'

import format from 'date-fns/format'
// components
import CleverTap from 'clevertap-react'
import MatchUps from '../../components/crcltics/matchUps'
import KeyStats from '../../components/crcltics/keyStats'
import LivePlayerProjection from '../../components/crcltics/livePlayerProjection'
import LiveScorePredictorODI from '../../components/crcltics/liveScorePredictorODI'
import PlayerProjection from '../../components/crcltics/playerProjection'
import DeathOverSimulator from '../../components/crcltics/deathOverSimulator'
import MatchStats from '../../components/crcltics/matchStats'
import PhasesOfPlay from '../../components/crcltics/phaseOfPlay'
import GameChangingOvers from '../../components/crcltics/gameChangingOvers'
import { useQuery } from '@apollo/react-hooks'
import { MATCH_DATA_FOR_SCORECARD } from '../../api/queries'
import Loading from '../loading'

const keystats_active = '/pngs/keystats-active.png'
const keystats_inactive = '/pngs/keystats-inactive.png'
const d_keystats_active = '/svgs/d_keystats_active.svg'

const playerprojection_active = '/pngs/playerprojection-active.png'
const playerprojection_inactive = '/pngs/playerprojection-inactive.png'
const d_playerprojection_active = '/svgs/d_playerprojection_active.svg'

const matchups_active = '/pngs/matchups-active.png'
const matchups_inactive = '/pngs/matchups-inactive.png'
const d_matchups_active = '/svgs/d_matchups_active.svg'

const team_score_prediction_active = '/pngs/team-score-prediction-active.png'
const team_score_prediction_inactive =
  '/pngs/team-score-prediction-inactive.png'
const d_team_score_prediction_active =
  '/svgs/d_team_score_prediction_active.svg'

const game_changing_overs_active = '/pngs/game-changing-overs-active.png'
const game_changing_overs_inactive = '/pngs/game-changing-overs-inactive.png'
const d_game_changing_overs_active = '/svgs/d_game_changing_overs_active.svg'

const matchstats_active = '/pngs/matchstats-active.png'
const matchstats_inactive = '/pngs/matchstats-inactive.png'
const d_matchstats_active = '/svgs/d_matchstats_active.svg'

const phases_of_play_active = '/pngs/phases-of-play-active.png'
const phases_of_play_inactive = '/pngs/phases-of-play-inactive.png'
const d_phases_of_play_active = '/svgs/d_phases_of_play_active.svg'

const over_simulator_active = '/pngs/over-simulator-active.png'
const over_simulator_inactive = '/pngs/over-simulator-inactive.png'
const d_over_simulator_active = '/svgs/d_over_simulator_active.svg'

import { Router, navigate } from '@reach/router'

export default function CriclyticsSlider({ browser, params, ...props }) {
  const { loading, error, data } = useQuery(MATCH_DATA_FOR_SCORECARD, {
    variables: { matchID: props.matchID },
  })
  const [tab, setTab] = useState(
    props.location.pathname.split('/').slice(-1)[0],
  )
  const NevSlider = (match, screen) => {
    CleverTap.initialize('Criclytics', {
      Source: 'Slider',
      MatchID: match.matchID,
      SeriesID: match.seriesID,
      TeamAID: match.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: match.matchStatus,
      CriclyticsEngine:
        screen === 'LiveScorePredictorODI' || screen === 'LiveScorePredictor'
          ? 'LiveTeam'
          : screen === 'LivePlayerProjection'
          ? 'LivePlayer'
          : data.miniScoreCard.data[0].matchStatus === 'live' &&
            screen === 'MatchUps'
          ? 'LiveMatchups'
          : data.miniScoreCard.data[0].matchStatus === 'upcoming' &&
            screen === 'KeyStats'
          ? 'ScheduledStats'
          : data.miniScoreCard.data[0].matchStatus === 'upcoming' &&
            screen === 'PlayerProjection'
          ? 'ScheduledPlayer'
          : data.miniScoreCard.data[0].matchStatus === 'upcoming' &&
            screen === 'MatchUps'
          ? 'ScheduledMatchups'
          : '',
      Platform: localStorage ? localStorage.Platform : '',
    })
  }

  if (error)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center">
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  if (loading) return <Loading />
  else
    return (
      <div className="min-vh-100 mw75-l center nt2-ns">
        <div className="">
          <div className="criclytic-bg flex justify-start items-center dn-l">
            <div className="w2-5 h2-5 flex justify-center items-center md:cursor-pointer">
              <img
                className=""
                onClick={() => window.history.back()}
                src={backIconWhite}
              ></img>
            </div>
            <span className="fw6 f5 white ">
              {data.miniScoreCard.data[0].matchNumber}.{' '}
              {data.miniScoreCard.data[0].seriesName}
            </span>
          </div>
          <h2 className="f35 black-70 b pt2 dn db-l">
            {data.miniScoreCard.data[0].matchNumber}.{' '}
            {data.miniScoreCard.data[0].seriesName}
          </h2>

          {/* mobile */}
          <div className="">
            <div className="bg-white pa2 shadow-4 dn-l">
              {data.miniScoreCard.data[0].matchStatus === 'upcoming' ||
              (data.miniScoreCard.data[0].matchStatus === 'live' &&
                !data.miniScoreCard.data[0].isLiveCriclyticsAvailable) ? (
                <div className="flex justify-start items-center ">
                  {[
                    {
                      tabName: 'Keystats',
                      selected: keystats_active,
                      notSelected: keystats_inactive,
                      key: 'keystats',
                    },
                    {
                      tabName: 'Player Projection',
                      selected: playerprojection_active,
                      notSelected: playerprojection_inactive,
                      key: 'playerprojection',
                    },
                    {
                      tabName: 'Matchups',
                      selected: matchups_active,
                      notSelected: matchups_inactive,
                      key: 'matchups',
                    },
                  ].map((tabs, i) => (
                    <div
                      key={i}
                      className={`ba br2 mr2 w35 h35 `}
                      onClick={() => (
                        setTab(tabs.tabName),
                        navigate(
                          `${props.uri}/${tabs.tabName
                            .split(' ')
                            .join('')
                            .toLowerCase()}`,
                          {
                            replace: true,
                          },
                        )
                      )}
                    >
                      <div className="tc center pt2">
                        <img
                          src={
                            tabs.key === tab ||
                            tabs.tabName === tab ||
                            (tab === '' && i === 0)
                              ? tabs.selected
                              : tabs.notSelected
                          }
                          alt=""
                          className="h2-5  w2-5"
                        />
                      </div>
                      <div className="f7 f8 tc">{tabs.tabName}</div>
                    </div>
                  ))}
                </div>
              ) : null}

              {data.miniScoreCard.data[0].matchStatus === 'live' &&
              data.miniScoreCard.data[0].isLiveCriclyticsAvailable ? (
                <div className="flex  overflow-x-scroll hidescroll">
                  {[
                    {
                      tabName: 'Team Score Prediction',
                      selected: team_score_prediction_active,
                      notSelected: team_score_prediction_inactive,
                      key: 'teamscoreprediction',
                    },
                    {
                      tabName: 'Live Player Projection',
                      selected: playerprojection_active,
                      notSelected: playerprojection_inactive,
                      key: 'liveplayerprojection',
                    },
                    {
                      tabName: 'Matchups',
                      selected: matchups_active,
                      notSelected: matchups_inactive,
                      key: 'matchups',
                    },
                    {
                      tabName: 'Over Simulator',
                      selected: over_simulator_active,
                      notSelected: over_simulator_inactive,
                      key: 'oversimulator',
                    },
                  ].map((tabs, i) => (
                    <div
                      key={i}
                      className={`ba br2 mr2 w35 h35`}
                      onClick={() => (
                        setTab(tabs.tabName),
                        navigate(
                          `${props.uri}/${tabs.tabName
                            .split(' ')
                            .join('')
                            .toLowerCase()}`,
                          {
                            replace: true,
                          },
                        )
                      )}
                    >
                      <div className="tc center pt2">
                        <img
                          src={
                            tabs.key === tab ||
                            tabs.tabName === tab ||
                            (tab === '' && i === 0)
                              ? tabs.selected
                              : tabs.notSelected
                          }
                          alt=""
                          className="h2-5  w2-5"
                        />
                      </div>
                      <div className="f7 f8 tc">{tabs.tabName}</div>
                    </div>
                  ))}
                </div>
              ) : null}

              {data.miniScoreCard.data[0].matchStatus === 'completed' ? (
                <div className="flex  justify-start items-center">
                  {[
                    {
                      tabName: 'Match Stats',
                      selected: matchstats_active,
                      notSelected: matchstats_inactive,
                      key: 'matchstats',
                    },
                    {
                      tabName: 'Phases Of Play',
                      selected: phases_of_play_active,
                      notSelected: phases_of_play_inactive,
                      key: 'phasesofplay',
                    },
                    {
                      tabName: 'Game changing Overs',
                      selected: game_changing_overs_active,
                      notSelected: game_changing_overs_inactive,
                      key: 'gamechangingovers',
                    },
                  ].map((tabs, i) => (
                    <div
                      key={i}
                      className={`ba br2 mr2 w35 h35`}
                      onClick={() => (
                        setTab(tabs.tabName),
                        navigate(
                          `${props.uri}/${tabs.tabName
                            .split(' ')
                            .join('')
                            .toLowerCase()}`,
                          {
                            replace: true,
                          },
                        )
                      )}
                    >
                      <div className="tc center pt2">
                        <img
                          src={
                            tabs.key === tab ||
                            tabs.tabName === tab ||
                            (tab === '' && i === 0)
                              ? tabs.selected
                              : tabs.notSelected
                          }
                          alt=""
                          className="h2-5  w2-5"
                        />
                      </div>
                      <div className="f7 f8 pt1 tc">{tabs.tabName}</div>
                    </div>
                  ))}
                </div>
              ) : null}
            </div>
          </div>
          {/* desktop */}

          <div className="mv3 dn db-l">
            {data.miniScoreCard.data[0].matchStatus === 'upcoming' ||
            (data.miniScoreCard.data[0].matchStatus === 'live' &&
              !data.miniScoreCard.data[0].isLiveCriclyticsAvailable) ? (
              <div className="flex justify-between items-center">
                {[
                  {
                    tabName: 'Keystats',
                    selected: d_keystats_active,
                    notSelected: keystats_inactive,
                    key: 'keystats',
                  },
                  {
                    tabName: 'Player Projection',
                    selected: d_playerprojection_active,
                    notSelected: playerprojection_inactive,
                    key: 'playerprojection',
                  },
                  {
                    tabName: 'Matchups',
                    selected: d_matchups_active,
                    notSelected: matchups_inactive,
                    key: 'matchups',
                  },
                ].map((tabs, i) => (
                  <div
                    key={i}
                    className={`br2 w-32 h35 flex items-center justify-center md:cursor-pointer grow ${
                      tabs.key === tab ||
                      tabs.tabName === tab ||
                      (i === 0 && tab === '')
                        ? 'cdcgr white'
                        : 'bg-white'
                    } shadow-4`}
                    onClick={() => (
                      navigate(
                        `${props.uri}/${tabs.tabName
                          .split(' ')
                          .join('')
                          .toLowerCase()}`,
                        {
                          replace: true,
                        },
                      ),
                      setTab(tabs.tabName)
                    )}
                  >
                    <img
                      src={
                        tabs.key === tab ||
                        tabs.tabName === tab ||
                        (tab === '' && i === 0)
                          ? tabs.selected
                          : tabs.notSelected
                      }
                      alt=""
                      className="h2-5 w2-5 pr2"
                    />
                    <div className="f6 fw5">{tabs.tabName}</div>
                  </div>
                ))}
              </div>
            ) : null}

            {data.miniScoreCard.data[0].matchStatus === 'live' &&
            data.miniScoreCard.data[0].isLiveCriclyticsAvailable ? (
              <div className="flex justify-between items-center">
                {[
                  {
                    tabName: 'Team Score Prediction',
                    selected: d_team_score_prediction_active,
                    notSelected: team_score_prediction_inactive,
                    key: 'teamscoreprediction',
                  },
                  {
                    tabName: 'Live Player Projection',
                    selected: d_playerprojection_active,
                    notSelected: playerprojection_inactive,
                    key: 'liveplayerprojection',
                  },
                  {
                    tabName: 'Matchups',
                    selected: d_matchups_active,
                    notSelected: matchups_inactive,
                    key: 'matchups',
                  },
                  {
                    tabName: 'Over Simulator',
                    selected: d_over_simulator_active,
                    notSelected: over_simulator_inactive,
                    key: 'oversimulator',
                  },
                ].map((tabs, i) => (
                  <div
                    key={i}
                    className={`br3 w-23 pv4 tc center md:cursor-pointer grow ${
                      tabs.key === tab ||
                      tabs.tabName === tab ||
                      (i === 0 && tab === '')
                        ? 'cdcgr white'
                        : 'bg-white'
                    } shadow-4`}
                    onClick={() => (
                      navigate(
                        `${props.uri}/${tabs.tabName
                          .split(' ')
                          .join('')
                          .toLowerCase()}`,
                        {
                          replace: true,
                        },
                      ),
                      setTab(tabs.tabName)
                    )}
                  >
                    <img
                      className="h2-5 w2-5"
                      src={
                        tabs.key === tab ||
                        tabs.tabName === tab ||
                        (tab === '' && i === 0)
                          ? tabs.selected
                          : tabs.notSelected
                      }
                      alt=""
                    />
                    <div className="f6 fw5">{tabs.tabName}</div>
                  </div>
                ))}
              </div>
            ) : null}

            {data.miniScoreCard.data[0].matchStatus === 'completed' ? (
              <div className="flex  justify-between items-center">
                {[
                  {
                    tabName: 'Match Stats',
                    selected: d_matchstats_active,
                    notSelected: matchstats_inactive,
                    key: 'matchstats',
                  },
                  {
                    tabName: 'Phases Of Play',
                    selected: d_phases_of_play_active,
                    notSelected: phases_of_play_inactive,
                    key: 'phasesofplay',
                  },
                  {
                    tabName: 'Game changing Overs',
                    selected: d_game_changing_overs_active,
                    notSelected: game_changing_overs_inactive,
                    key: 'gamechangingovers',
                  },
                ].map((tabs, i) => (
                  <div
                    key={i}
                    className={`br2 w-32 h35 flex items-center justify-center md:cursor-pointer grow ${
                      tabs.key === tab ||
                      tabs.tabName === tab ||
                      (i === 0 && tab === '')
                        ? 'cdcgr white'
                        : 'bg-white'
                    } shadow-4`}
                    onClick={() => (
                      navigate(
                        `${props.uri}/${tabs.tabName
                          .split(' ')
                          .join('')
                          .toLowerCase()}`,
                        {
                          replace: true,
                        },
                      ),
                      setTab(tabs.tabName)
                    )}
                  >
                    {}

                    <img
                      src={
                        tabs.key === tab ||
                        tabs.tabName === tab ||
                        (tab === '' && i === 0)
                          ? tabs.selected
                          : tabs.notSelected
                      }
                      alt=""
                      className="h2-5 w2-5 pr2"
                    />
                    <div className="f6 fw5">{tabs.tabName}</div>
                  </div>
                ))}
              </div>
            ) : null}
          </div>

          <Router>
            <KeyStats
              matchData={data.miniScoreCard.data}
              path="keystats"
              browser={browser}
            />
            <PlayerProjection
              matchData={data.miniScoreCard.data}
              path="playerprojection"
              browser={browser}
            />
            <MatchUps
              matchData={data.miniScoreCard.data}
              path="matchups"
              browser={browser}
            />
            <LiveScorePredictorODI
              matchData={data.miniScoreCard.data}
              path="teamscoreprediction"
              browser={browser}
            />
            <LivePlayerProjection
              matchData={data.miniScoreCard.data}
              path="liveplayerprojection"
              browser={browser}
            />
            <DeathOverSimulator
              matchData={data.miniScoreCard.data}
              path="oversimulator"
              browser={browser}
            />
            <MatchStats
              matchData={data.miniScoreCard.data}
              path="matchstats"
              browser={browser}
            />
            <PhasesOfPlay
              matchData={data.miniScoreCard.data}
              path="phasesofplay"
              browser={browser}
            />
            <GameChangingOvers
              matchData={data.miniScoreCard.data}
              path="gamechangingovers"
              browser={browser}
            />
          </Router>
        </div>
      </div>
    )
}
