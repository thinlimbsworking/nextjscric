import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import { QUALIFICATION_PROBABILITY } from '../../api/queries';
const information = '/svgs/information.svg';
const play = '/svgs/play-button.svg';
const pause = '/svgs/pause.svg';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const ground = '/svgs/groundImageWicket.png';
const Handle = Slider.Handle;
const handle = ({ value, dragging, index, ...restProps }) => {
  return (
    <Tooltip prefixCls='rc-slider-tooltip' overlay={value + 1} visible={dragging} placement='top' key={index}>
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};
export default function FinalFour({ series, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false);
  const [index,setindex]=useState(0)
  const [match, setMatchNo] = useState(0);
  const [click, setclick] = useState(false);
  const [button, setbutton] = useState(false);
  const { error, loading, data } = useQuery(QUALIFICATION_PROBABILITY);
  // console.log("data.getQualificationProbability",data.getQualificationProbability)
  useEffect(() => {
    if (data && data.getQualificationProbability && data.getQualificationProbability.probabilityArray) {
      setMatchNo(data.getQualificationProbability.probabilityArray[index].length - 1);
    }
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0);
    }
  }, [data]);
  let mytime = 0;
  click &&
    (mytime = setTimeout(function () {
      match === data.getQualificationProbability.probabilityArray[index].length-1 
        ? (clearTimeout(mytime), setclick(!click), setbutton(false))
        : setMatchNo(match+1 );
    }, 1000));
  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };
  if (error)
    return (
      <div className='bg-white pv3 shadow-4 mt2'>
        <div>
          <img
            className='w45-m h45-m w4 h4 w5-l h5-l'
            style={{ margin: 'auto', display: 'block' }}
            src={ground}
            alt='loading...'
          />
        </div>
        <div className='tc pv2 f5 fw5'>Data Not Available</div>
      </div>
    );
  if (loading || !data) return <div>Loading.. </div>;
  else
    return (
      <div className='mw75-l center mt2-l'>
        {data.getQualificationProbability ? (
          <div className='flex justify-between bg-white'>
            <div className='w-50-l w-100 shadow-4-l'>
              <div className='mt2 mv0-l bg-white'>
                <div className='flex justify-between items-center pa2'>
                  <div className='f7 f5-l fw4 grey_10 ttu'>Qualification Probability</div>
                  <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
                </div>
                <div className='outline light-gray' />
                {infoToggle && (
                  <div className='bg-white'>
                    <div className='f7 grey_8 pa2'>
                      The changes in qualification fortunes, across the tenure of the tournament, presented as a
                      snapshot.
                    </div>
                    <div className='outline light-gray' />
                  </div>
                )}
              </div>
              {data.getQualificationProbability.probabilityArray.length>1 &&  <div className="ma2">
          <div className="flex  bg-white   shadow-4 w-100 items-center justify-center">
            {data && data.getQualificationProbability && data.getQualificationProbability.probabilityArray.length>0 && data.getQualificationProbability.probabilityArray.map((group,i)=>
            <>
            <div className={`w-50 pv2 ${i===index?'bb b--darkRed':''} bw1`} key={i} onClick={()=>{setindex(i);setMatchNo(data.getQualificationProbability.probabilityArray[index].length-1)}}>
            <div className=" flex  items-center justify-center  w-100"><div className={` f6 ${i===index?'black fw6':'gray fw5'}`}>Group {i+1}  </div>
           
            </div>
            </div>
           {i!==data.getQualificationProbability.probabilityArray.length-1 &&  <div className="v-divider-dark mv1"></div>}
         </>
            )}
            </div>
            </div>}
              <div className='dn db-l pa2'>
                <div className='bg-white flex items-center justify-between'>
                  <img
                    alt='pause'
                    src={button ? pause : play}
                    className='h2-4 w2-4'
                    onClick={() => (
                      setclick(!click),
                      match === data.getQualificationProbability.probabilityArray[index].length - 1 ? setMatchNo(0) : '',
                      setbutton(!button)
                    )}
                  />
                  <div className='w-90 flex justify-between items-center pl2'>
                    <div className='f9 f7-l nowrap '>Match 0</div>
                    <div className='w-80 mh4'>
                      <Slider
                        className='slider-main'
                        max={data.getQualificationProbability.probabilityArray[index].length - 1}
                        min={0}
                        step={1}
                        value={match}
                        onChange={(val) => {
                          if (
                            val !== data.getQualificationProbability.probabilityArray[index].length &&
                            val <= data.getQualificationProbability.probabilityArray[index].length
                          ) {
                            setbutton(false);
                            setclick(false);
                            setMatchNo(val);
                          }
                        }}
                        handle={handle}
                        handleStyle={[
                          {
                            backgroundColor: 'white',
                            border: 'white',
                            width: '18px',
                            height: '18px',
                            marginTop: '-8px',
                            boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
                          }
                        ]}
                        trackStyle={[{ backgroundColor: '#a70e13', height: '5px' }]}
                        railStyle={{
                          backgroundColor: '#e8ebf3',
                          height: '5px'
                        }}
                      />
                    </div>
                    <div className='f9 f7-l nowrap'>
                      Match {data.getQualificationProbability.probabilityArray[index].length - 1}
                    </div>
                  </div>
                </div>
              </div>
              <div className='divider' />
              <div className='flex justify-center items-center shadow-4 pa2 mv3 w-30 center br2'>
                <div className='f5 fw4'>Match</div>
                <div className='f3 pl1 fw5 darkRed'>{match}</div>
              </div>
              <div className=''>
                {data && data.getQualificationProbability && data.getQualificationProbability.qpTeamList.length>0 &&data.getQualificationProbability.qpTeamList[index].map((tname, i) => (
                  <div key={i} className='pv3 ph4 flex justify-between items-center '>
                    <div className='f7 f6-l fw5 w-15'>{tname.teamName}</div>
                    <div className='flex w-85  w-90-l items-center bg-grey_2 br3' style={{ height: 8 }}>
                      {/* {console.log("matchmatch",match)} */}
                      <div
                        style={{
                          width: `${data && data.getQualificationProbability &&  data.getQualificationProbability.probabilityArray.length>0 && data.getQualificationProbability.probabilityArray[index] && 
                            data.getQualificationProbability.probabilityArray[index][match] && data.getQualificationProbability.probabilityArray[index][match][i] && data.getQualificationProbability.probabilityArray[index][match][i]}%`,
                          height: 8,
                          backgroundColor: tname.teamColor
                        }}
                        className={`br2 br--left poll`}></div>
                      <div className='bg-white br-100 h2 w2 z-999  ba flex items-center justify-center ' style={{}}>
                        <span className='f7 fw6' style={{ color: tname.teamColor }}>
                          {data && data.getQualificationProbability &&  data.getQualificationProbability.probabilityArray.length>0 && data.getQualificationProbability.probabilityArray[index] && 
                            data.getQualificationProbability.probabilityArray[index][match] && data.getQualificationProbability.probabilityArray[index][match][i] && data && data.getQualificationProbability.probabilityArray[index][match][i] === 100
                            ? 'Q'
                            : data && data.getQualificationProbability &&  data.getQualificationProbability.probabilityArray.length>0 && data.getQualificationProbability.probabilityArray[index] && 
                            (data.getQualificationProbability.probabilityArray[index][match] && data.getQualificationProbability.probabilityArray[index][match][i] && data.getQualificationProbability.probabilityArray[index][match][i]? data.getQualificationProbability.probabilityArray[index][match][i] :0)+ '%'}
                        </span>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
              <div className='flex justify-end pv2 ph3 f8 fw6'>{data.getQualificationProbability.text}</div>
              <div className='bg-white  mv2 mv0-l pa2 flex items-center justify-between shadow-4 dn-l'>
                <img
                  alt='pause'
                  src={button ? pause : play}
                  className='h2-4 w2-4'
                  onClick={() => (
                    setclick(!click),
                    match === data.getQualificationProbability.probabilityArray[0].length - 1 ? setMatchNo(0) : '',
                    setbutton(!button)
                  )}
                />
                {}
                <div className='w-90 flex justify-between items-center pl2'>
                  <div className='f9 f7-l nowrap '>Match 0</div>
                  <div className='w-80 mh4'>
                    <Slider
                      className='slider-main'
                      max={data.getQualificationProbability.probabilityArray[0].length - 1}
                      min={0}
                      step={1}
                      value={match}
                      onChange={(val) => {
                        if (
                          val !== data.getQualificationProbability.probabilityArray[0].length &&
                          val <= data.getQualificationProbability.probabilityArray[0].length
                        ) {
                          setbutton(false);
                          setclick(false);
                          setMatchNo(val);
                        }
                      }}
                      handle={handle}
                      handleStyle={[
                        {
                          backgroundColor: 'white',
                          border: 'white',
                          width: '18px',
                          height: '18px',
                          marginTop: '-8px',
                          boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
                        }
                      ]}
                      trackStyle={[{ backgroundColor: '#a70e13', height: '5px' }]}
                      railStyle={{
                        backgroundColor: '#e8ebf3',
                        height: '5px'
                      }}
                    />
                  </div>
                  <div className='f9 f7-l nowrap'>
                    Match {data.getQualificationProbability.probabilityArray[0].length-1 }
                  </div>
                </div>
              </div>
            </div>
            <div className='v-solid-divider' />
            <div className='w-50-l dn db-l shadow-4'>
              <div className='mv2 mv0-l pb2 '>
                <div className='flex justify-between items-center pa2'>
                  <div className='f6 fw5 grey_10 ttu'>{series && series.split('-').join(' ')} Points table</div>
                </div>
                <div className='outline light-gray' />
                <div className='flex justify-between pa2 tc f8 fw5 items-center grey_10'>
                  <span className='w-20'>TEAMS</span>
                  <span className='w-20'>MATCHES</span>
                  <span className='w-20'>POINTS</span>
                  <span className='w-30 '>QUALIFICATION PROBABILITY</span>
                </div>
                <div className='outline light-gray' />
                {data && data.getQualificationProbability && data.getQualificationProbability.qpTeamList.length>0 && data.getQualificationProbability.pointsTable[index].map((point, i) => (
                  <div key={i}>
                    <div className='flex justify-between pa2 pv3 tc f7 fw5 items-center h3'>
                      <span className='w-20 flex justify-start pl2  items-center'>
                        <img
                          alt=''
                          src={`https://images.cricket.com/teams/${point.teamID}_flag.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          className='w2 h1 ba b--light-gray mr2'
                        />
                        <span className='f7 fw5'>{point.teamName}</span>
                      </span>
                      <span className='w-20'>{point.matchesPlayed}</span>
                      <span className='w-20'>{point.points}</span>
                      {point.isQualified ? (
                        <div className='white tc w-30 flex justify-center'>
                          <span className='bg-green_10  pa1'>Qualified</span>
                        </div>
                      ) : (
                        <span className='w-30 '>{point.qp}%</span>
                      )}
                    </div>
                    {i < 7 && <div className='outline light-gray' />}
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          <div className='bg-white pv3 shadow-4'>
            <div>
              <img
                className='w45-m h45-m w4 h4 w5-l h5-l'
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt='loading...'
              />
            </div>
            <div className='tc pv2 f5 fw5'>Data Not Available</div>
          </div>
        )}
      </div>
    );
}
