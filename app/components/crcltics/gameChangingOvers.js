import React, { useState } from 'react'
import Winwiz from '../winwiz'
import DATA from '../commom/datanotfound'
import Heading from '../commom/heading'
import { useQuery } from '@apollo/react-hooks'
import { GAME_CHANGING_OVERS } from '../../api/queries'

const information = '/svgs/information.svg'
const ground = '/svgs/groundImageWicket.png'
const down_arrow = '/svgs/down-arrow.png'
const up_arrow = '/svgs/up-arrow.png'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'

export default function GameChangingOvers({ browser, ...props }) {
  const { data, loading } = useQuery(GAME_CHANGING_OVERS, {
    variables: {
      matchID: props?.matchID,
    },
  })

  const [infoToggle, setInfoToggle] = useState(false)
  const [click, setClick] = useState(true)
  const [overno, setOver] = useState(0)
  const [showCommentary, setshowCommentary] = useState(false)
  const [deskCommentary, setDeskCommentary] = useState(0)

  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }
  //
  return (
    <div>
      <div className="dark:mb-2 dark:-mt-1 mt-2 mb-6 ml-2">
        <Heading
          heading={'Game Changing Overs'}
          subHeading={
            'A collection of four overs which according to Criclytics changed the game,with a detailed breakdown of what happened in each ball of these overs.'
          }
        />

        {/* <div className='text-md  text-white tracking-wide font-bold uppercase '>Game Changing Overs</div>
        <div className='text-xs text-white pt-1 font-medium tracking-wide'>
          {' '}
          Forecast of where the match is headed, in the next few overs{' '}
        </div>
        <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
      </div>
      {data && data.gameChangingOvers.matchID ? (
        <div className="md:bg-white md:rounded-md md:ml-1 md:shadow md:p-3">
          <div className="flex justify-between items-center pa2 ph3-l bg-white mv2-l mt2 shadow-4">
            <h1 className="f7 f5-l fw5 grey_10 ttu hidden">
              Game changing overs
            </h1>
            <img
              src={information}
              onClick={() => toggleInfo()}
              alt=""
              className="h1 w1 hidden h13-l w13-l"
            />
          </div>
          <div className="light-gray" />
          {infoToggle && (
            <div className="bg-white">
              <div className="f7 grey_8 pa2">
                A collection of four overs which according to Criclytics changed
                the game, with a detailed breakdown of what happened in each
                ball of these overs
              </div>
              <div className="outline light-gray" />
            </div>
          )}
          <div className="flex-l justify-between dark:border-none dark:bg-basebg dark:mt-5 mt-4">
            <div className="w-49-l dark:mt-0 mt-4">
              {data.gameChangingOvers.overs.map((over, i) => (
                <div key={i} className="mb-2 br2 md:cursor-pointer">
                  <div className="bg-white dark:bg-gray-4 shadow-4">
                    <div
                      key={i}
                      className={`p-2 lg:p-3 ${
                        overno === i || !click ? 'opacity-full' : 'opacity-80'
                      } ${
                        overno === i
                          ? 'rounded-md dark:bg-gray-4 bg-[#F1F1F1] border border-green-6'
                          : ''
                      }`}
                      onClick={() => (
                        setClick(true),
                        setOver(i),
                        setDeskCommentary(i),
                        setshowCommentary(false)
                      )}
                    >
                      <div className="flex items-center">
                        <div
                          className="p-1 dark:bg-gray border-2 lg:hidden md:hidden dark:rounded-md dark:border-none"
                          style={{ minWidth: '50px' }}
                        >
                          <p className="text-center m-0 fw7 text-white f4 px-2">
                            {over.overNo || ''}
                          </p>
                          <div className="divider" />
                          <p className="text-center m-0 white f8 fw5 p-1">
                            over{' '}
                          </p>
                        </div>
                        <div
                          className="p-1 border-2 border-black lg:block md:block hidden"
                          style={{ background: '', minWidth: '50px' }}
                        >
                          <p className="text-center m-0 fw7 text-black f4 px-2">
                            {over.overNo || ''}
                          </p>
                          <div className="divider" />
                          <p className="text-center m-0 white f8 fw5 p-1">
                            over{' '}
                          </p>
                        </div>
                        <div className="flex hidescroll md:py-2 dark:overflow-scroll">
                          {over.commentaryOver.map((ball, i) => (
                            <div key={i} className="text-center ml-2">
                              {ball.wicket ? (
                                <div className="flex justify-center items-center w-8 h-8 text-xs rounded-full bg-red">
                                  W
                                </div>
                              ) : (
                                <div
                                  className={`rounded-full flex justify-center items-center ${
                                    ball.runs === '4'
                                      ? 'bg-green white b--green h-8 w-8 '
                                      : ball.runs === '6'
                                      ? 'bg-blue white b--blue h-8 w-8 '
                                      : 'dark:bg-white bg-[#F1F1F1] h-8 w-8 text-black border border-gray-11'
                                  } `}
                                >
                                  {ball.type === 'no ball'
                                    ? 'nb'
                                    : ball.type === 'leg bye'
                                    ? 'lb'
                                    : ball.type === 'bye'
                                    ? 'b'
                                    : ball.type === 'wide'
                                    ? 'wd'
                                    : ''}
                                  {ball.type === 'no ball' ||
                                  ball.type === 'leg bye' ||
                                  ball.type === 'bye' ||
                                  ball.type === 'wide'
                                    ? ball.runs >= 1
                                      ? ball.runs
                                      : ''
                                    : ball.runs}
                                </div>
                              )}
                              <p className="f8 black-30 pt-1 m-0 fw6">
                                {ball.over}
                              </p>
                            </div>
                          ))}
                        </div>
                      </div>

                      <Winwiz
                        key={i}
                        click={click}
                        isAnimate={i == overno}
                        pollData={over.lastBallPredictionData}
                        teamName={over.teamShortName}
                        score={over.score}
                      />
                    </div>
                    {/* {click && i === overno && (
                      <div className="flex justify-center pv1 dn-l">
                        <img
                          alt=""
                          src={showCommentary ? up_arrow : down_arrow}
                          className="pa2 shadow-4 bg-white w1"
                          onClick={() => setshowCommentary(!showCommentary)}
                        />
                      </div>
                    )} */}
                  </div>

                  {/* MWEB ONLY */}

                  <div className="hidden md:hidden">
                    {click && i === overno && (
                      <div className="dark:bg-gray-4 bg-white">
                        <div className="bg-navy p-2 flex justify-between items-center white">
                          <div className="flex justify-between items-center">
                            <img
                              className="w-2 h1-3 shadow-4"
                              src={`https://images.cricket.com/teams/${over.teamID}_flag_safari.png`}
                              onError={(evt) =>
                                (evt.target.src = flagPlaceHolder)
                              }
                              alt=""
                            />
                            <div className=" f5 fw5 ph2">
                              {over.teamShortName}
                            </div>
                          </div>
                          <div className="f6 fw4"> INN {over.inningNo} </div>
                        </div>
                        {[...(over?.commentary || [])]
                          ?.reverse()
                          .map((x, z) => (
                            <div key={z}>
                              <div className="flex justify-start items-start pa2 black">
                                <div
                                  className={`pa1 br2 ba ${
                                    x.type === 'wicket'
                                      ? 'bg-cdc white'
                                      : x.type === 'four'
                                      ? 'bg-green white b--green'
                                      : x.type === 'six'
                                      ? 'bg-blue white b--blue'
                                      : ''
                                  }`}
                                  style={{ minWidth: '50px' }}
                                >
                                  <p className="tc ma0 fw7 f4 ph2 pv1">
                                    {x.wicket ? 'W' : x.runs}
                                  </p>
                                  <div className="divider" />
                                  <p className="tc ma0 f8 fw5 pa1">{x.over}</p>
                                </div>
                                <div className="f6 pl2">
                                  <span className="fw6">{x.bowlerName}</span>
                                  <span> to </span>
                                  <span className="fw6">{x.batsmanName}</span>
                                  <span> {x.comment}</span>
                                </div>
                              </div>
                              <div className="divider" />
                            </div>
                          ))}
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
            <div className="hidden">
              {
                <div className="bg-white">
                  <div className="bg-navy pa2 flex justify-between items-center white">
                    <div className="flex justify-between items-center">
                      <img
                        className="w-2 h1-3 shadow-4"
                        src={`https://images.cricket.com/teams/${data.gameChangingOvers.overs[deskCommentary].teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        alt=""
                      />
                      <div className=" f5 fw5 ph2">
                        {
                          data.gameChangingOvers.overs[deskCommentary]
                            .teamShortName
                        }
                      </div>
                    </div>
                    <div className="f6 fw4">
                      {' '}
                      INN{' '}
                      {
                        data.gameChangingOvers.overs[deskCommentary].inningNo
                      }{' '}
                    </div>
                  </div>
                  {data.gameChangingOvers.overs[deskCommentary].commentary.map(
                    (x, z) => (
                      <div
                        key={z}
                        className="flex justify-start items-start p-2 black"
                      >
                        <div
                          className={`p-1 br2 ba ${
                            x.type === 'wicket'
                              ? 'bg-cdc white'
                              : x.type === 'four'
                              ? 'bg-green white b--green'
                              : x.type === 'six'
                              ? 'bg-blue white b--blue'
                              : ''
                          }`}
                          style={{ minWidth: '50px' }}
                        >
                          <p className="text-center m-0 fw7 f4 px-2 py-1">
                            {x.wicket ? 'W' : x.runs}
                          </p>
                          <div className="divider" />
                          <p className="text-center ma0 f8 fw5 p-1">{x.over}</p>
                        </div>
                        <div className="f6 pl-2">
                          <span className="fw6">{x.bowlerName}</span>
                          <span> to </span>
                          <span className="fw6">{x.batsmanName}</span>
                          <span> {x.comment}</span>
                        </div>
                      </div>
                    ),
                  )}
                </div>
              }
            </div>
          </div>
        </div>
      ) : (
        <div className="h-screen w-100 mt-2 shadow-4 p-4">
          <DATA />
        </div>
      )}
    </div>
  )
}
