import { useQuery } from '@apollo/react-hooks';
import React, { useState, useEffect } from 'react';
import CleverTap from 'clevertap-react';
import { format } from 'date-fns';
import { LIVE_PLAYER_PROJECTION } from '../../api/queries';
const ground = '/svgs/groundImageWicket.png';
const playerAvatar = '/placeHodlers/playerAvatar.png';
const information = '/svgs/information.svg';
export default function LivePlayerProjection({ isActive, browser, matchData, ...props }) {
  const [bowler, setBowler] = useState(0);
  const [infoToggle, setInfoToggle] = useState(false);
  const { error, loading, data, startPolling } = useQuery(LIVE_PLAYER_PROJECTION, {
    variables: { matchId: matchData[0].matchID },
    pollInterval: props.path !== 'matchreel' ? 8000 : ''
  });
  useEffect(() => {
    if (data) {
      setBowler(
        data.getLivePlayerProjectionDetails.bowler.projected.indexOf(
          Math.max.apply(null, data.getLivePlayerProjectionDetails.bowler.projected).toString()
        )
      );
    }
  }, [data]);
  useEffect(() => {
    if (isActive) {
      startPolling(8000);
    }
  }, [isActive]);
  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };
  if (error)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  if (loading || !data) return <div>Loading.. </div>;
  else
    return (
      <div className=''>
        <div className='flex justify-between items-center pa2 ph3-l bg-white mt2 shadow-4'>
          <h1 className='f7 f5-l fw5 grey_10 ttu'>Live player projection</h1>
          <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
        </div>
        <div className='outline light-gray' />
        {infoToggle && (
          <div className='bg-white'>
            <div className='f7 grey_8 pa2'>
              Every ball bowled/faced alters the chance a player has, of scoring within a particular range.
            </div>
            <div className='outline light-gray' />
          </div>
        )}
        {true ? (
          (data.getLivePlayerProjectionDetails.batsman.length > 0 || (data.getLivePlayerProjectionDetails.bowler && data.getLivePlayerProjectionDetails.bowler.bowlerId!=="")) ? (
            <div>
              <div className='bg-white  pv1'>
                {data.getLivePlayerProjectionDetails.batsman.map((player, i) => (
                  <div key={i}>
                  <div className='flex-l items-center justify-around mv2  mv0-l pv3-l' >
                    <div className='w-40-l '>
                      <div className='ph3 flex  justify-between items-center'>
                        <div className='flex items-center '>
                          <div className='br-50 center w2-6 h2-6 overflow-hidden b--light-gray ba '>
                            <img
                              className='contain w3'
                              src={`https://images.cricket.com/players/${player.playerId}_headshot_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = playerAvatar)}
                            />
                          </div>
                          <div className='pl2'>
                            <div className=' f6 fw5'>{`${player.playerName} ${i === 0 ? '*' : ''}`}</div>
                            <div className='flex justify-start items-center mt1'>
                              <div className='fw5 f7 grey_8 pr2'>{player.playerTeamName}</div>
                              <img className='w1 h1' alt='' src='/svgs/bat_icon.png' />
                            </div>
                          </div>
                        </div>
                        <div className='flex justify-start items-center'>
                          <div className='oswald f3 fw5'>{player.playerRuns}</div>
                          <div className='f6 fw4 mt2'>({player.playerBalls})</div>
                        </div>
                      </div>
                    </div>
                    <div className='flex justify-center    items-center w-60-l '>
                    <div className='bg-orange_2_1 mt3 w-80 w-70-l flex justify-center  items-center mt0-l pv1 ph2 tc br-pill'>
                        <div className='f7 '>
                        {player.playerName} is projected  to score{' '}
                          {player.playerBattingProbabilities.playerData.map((x, i) =>
                            x.red == true ? player.playerBattingProbabilities.playerData[i].bound : ''
                          )}{' '}
                          runs
                        </div>
                      </div>
                    </div>
                    
                    <div className='divider mt3 dn-l' />
                  </div>
                  <div className='divider  dn db-l' />
                  </div>
                ))}
              </div>
            
              <div className='bg-white flex-l items-center   pv3 '>
                
                  <div className='ph3 flex justify-between w-40-l  items-center'>
                    <div className='flex items-center'>
                      <div className='br-50 center w2-6 h2-6 overflow-hidden b--light-gray ba '>
                        <img
                          className='contain w3'
                          src={`https://images.cricket.com/players/${data.getLivePlayerProjectionDetails.bowler.bowlerId}_headshot_safari.png`}
                          alt=''
                          onError={(evt) => (evt.target.src = playerAvatar)}
                        />
                      </div>
                      <div className='pl2'>
                        <div className='f6 fw5'>{data.getLivePlayerProjectionDetails.bowler.bowlerName}</div>
                        <div className='flex justify-start items-center mt1'>
                          <div className='fw5 f7 grey_8 pr2'>{data.getLivePlayerProjectionDetails.bowler.teamName}</div>
                          <img className='w1 h1' src='/pngs/ball_icon.png' alt='' />
                        </div>
                      </div>
                    </div>
                    <div>
                      <span className='oswald f3 fw5'>
                        {data.getLivePlayerProjectionDetails.bowler.wicketsTakenTillNow || 0}
                      </span>
                      <span className='oswald f3 fw5'>/{data.getLivePlayerProjectionDetails.bowler.bowlerRuns}</span>
                      <span className=''>({data.getLivePlayerProjectionDetails.bowler.oversBowledSoFar})</span>
                    </div>
                  </div>
                <div className='flex justify-center  items-center w-60-l'>
                <div className='bg-orange_2_1 w-70-l w-80 flex justify-center  items-center mt0-l pv1 ph2  mt3 mt0-l pa1 tc br-pill'>
                    <span className='f7'>
                    {data.getLivePlayerProjectionDetails.bowler.bowlerName} is projected to pick{'  '}
                      {data.getLivePlayerProjectionDetails.bowler.bounds[bowler] < 0
                        ? 0
                        :  data.getLivePlayerProjectionDetails.bowler.bounds[bowler]}{' '}
                      wickets
                    </span>
                  </div>
                </div>
                
              </div>
            </div>
          ) : (
            <div className=' bg-white pv3'>
              <div>
                <img
                  className='w45-m h45-m w4 h4 w5-l h5-l'
                  style={{ margin: 'auto', display: 'block' }}
                  src={ground}
                  alt='loading...'
                />
              </div>
              <div className='tc pv2 f5 fw5'>Data Not Available</div>
            </div>
          )
        ) : (
          <div className=' bg-white pv3'>
            <div>
              <img
                className='w45-m h45-m w4 h4 w5-l h5-l'
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt='loading...'
              />
            </div>
            <div className='tc pv2 f5 fw5'>Player Projections not available for Super Over</div>
          </div>
        )
        }
      </div>
    );
}