import React, { useState } from 'react';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import { useQuery } from '@apollo/react-hooks';
import { LIVE_SCORE_PREDICTOR } from '../../api/queries';

const ground = '/svgs/groundImageWicket.png';
const information = '/svgs/information.svg';
const play = '/svgs/play-button.svg';
const pause = '/svgs/pause.svg';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const Handle = Slider.Handle;

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip prefixCls='rc-slider-tooltip' overlay={value + 1} visible={dragging} placement='top' key={index}>
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};
export default function LiveScorePredictorODI({ matchData, browser, ...props }) {
  const [button, setbutton] = useState(false);
  const [index, setIndex] = useState(0);
  const [click, setclick] = useState(false);
  const [infoToggle, setInfoToggle] = useState(false);

  const { loading, error, data } = useQuery(LIVE_SCORE_PREDICTOR, {
    variables: { matchId: matchData[0].matchID, matchType: matchData[0].matchType },
    pollInterval: props.path !== 'matchreel' ? 8000 : '',
    onCompleted: (data) => {
      if (data.liveScorePredictor.liveScores.length) {
        setIndex(data.liveScorePredictor.liveScores.length - 1);
      }
    }
  });

  let mytime = 0;
  click &&
    (mytime = setTimeout(function () {
      index === data.liveScorePredictor.liveScores.length - 1
        ? (clearTimeout(mytime), setclick(!click), setbutton(false))
        : setIndex(index + 1);
    }, 500));

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };
  if (!data || error)
    return (
      <div className=' bg-white pv3'>
        <div>
          <img
            className='w45-m h45-m w4 h4 w5-l h5-l'
            style={{ margin: 'auto', display: 'block' }}
            src={ground}
            alt='loading...'
          />
        </div>
        <div className='tc pv2 f5 fw5'>Data Not Available</div>
      </div>
    );
  else {
    return (
      <div className='mt2 overflow-y-hidden'>
        <div className='flex justify-between items-center pa2 ph3-l bg-white shadow-4 '>
          <h1 className='f7 f5-l fw5 grey_10 ttu'>{`${
            props.path !== 'matchreel' ? 'Live Team Projection' : 'Match Reel'
          }`}</h1>
          <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
        </div>
        <div className='outline light-gray' />
        {infoToggle && (
          <div className='bg-white'>
            {props.path !== 'matchreel' ? (
              <div className='f7 grey_8 pa2'>
                A prediction of final scores of the match that also shows how fortunes of the teams have swung until
                this moment.
              </div>
            ) : (
              <div className='f7 grey_8 pa2'>
                A fast-tracked reel of how the fortunes of the teams shifted through the game.
              </div>
            )}
            <div className='outline light-gray' />
          </div>
        )}

        {data.liveScorePredictor.liveScores.length > 0 &&
        data.liveScorePredictor.liveScores[data.liveScorePredictor.liveScores.length - 1].winvizView !== null ? (
          <div>
            <div className='flex-l justify-between items-center bg-white shadow-4 '>
              <div className='ph2 pv3 w-50-l'>
                {data.liveScorePredictor.liveScores[index].currentView && (
                  <>
                    <div className='flex flex-row justify-between f7 fw6'>
                      <div
                        className={`${
                          data.liveScorePredictor.liveScores[index].currentView &&
                          (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
                          parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
                            ? 'green_10'
                            : 'red_10')
                        }`}>
                        {data.liveScorePredictor.liveScores[index].currentView &&
                          `${data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage}%`}
                      </div>
                      {data.liveScorePredictor.liveScores[index].currentView &&
                      parseInt(data.liveScorePredictor.liveScores[index].currentView.tiePercentage) > 0 ? (
                        <div className='grey_7 '>
                          {data.liveScorePredictor.liveScores[index].currentView.tiePercentage &&
                            `${data.liveScorePredictor.liveScores[index].currentView.tiePercentage}%`}
                        </div>
                      ) : (
                        <div />
                      )}
                      <div
                        className={`${
                          data.liveScorePredictor.liveScores[index].currentView &&
                          (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
                          parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
                            ? 'red_10'
                            : 'green_10')
                        } flex justify-end`}>
                        {data.liveScorePredictor.liveScores[index].currentView &&
                          `${data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage}%`}
                      </div>
                    </div>
                    <div className='flex pv1 justify-center items-center '>
                      <div
                        style={{
                          width: `${
                            data.liveScorePredictor.liveScores[index].currentView &&
                            data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage
                          }%`,
                          height: 14
                        }}
                        className={` dib ${
                          data.liveScorePredictor.liveScores[index].currentView &&
                          (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
                          parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
                            ? 'bg-green_10'
                            : 'bg-red_10')
                        }`}></div>
                      {data.liveScorePredictor.liveScores[index].currentView &&
                      parseInt(data.liveScorePredictor.liveScores[index].currentView.tiePercentage) > 0 ? (
                        <div
                          className=' dib bg-grey_2'
                          style={{
                            width: `${data.liveScorePredictor.liveScores[index].currentView.tiePercentage}%`,
                            height: 14
                          }}
                        />
                      ) : (
                        <div />
                      )}
                      <div
                        style={{
                          width: `${
                            data.liveScorePredictor.liveScores[index].currentView &&
                            data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage
                          }%`,
                          height: 14
                        }}
                        className={`dib ${
                          data.liveScorePredictor.liveScores[index].currentView &&
                          (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
                          parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
                            ? 'bg-red_10'
                            : 'bg-green_10')
                        } dib`}></div>
                    </div>
                    <div className='flex flex-row justify-between f7'>
                      <div className='fw5 f7'>
                        {data.liveScorePredictor.liveScores[index].currentView &&
                          data.liveScorePredictor.liveScores[index].currentView.homeTeamShortName}
                      </div>
                      {data.liveScorePredictor.liveScores[index].currentView &&
                      data.liveScorePredictor.liveScores[index].currentView.tiePercentage &&
                      parseInt(data.liveScorePredictor.liveScores[index].currentView.tiePercentage) > 0 ? (
                        matchData[0].matchType === 'Test' ? (
                          <div className='fw5 f7'>DRAW</div>
                        ) : (
                          <div className='fw5 f7'>Tie</div>
                        )
                      ) : (
                        <div />
                      )}
                      <div className='fw5 f7 flex justify-end'>
                        {data.liveScorePredictor.liveScores[index].currentView &&
                          data.liveScorePredictor.liveScores[index].currentView.awayTeamShortName}
                      </div>
                    </div>
                  </>
                )}
              </div>

              <div className='bg-light-gray pa2 ttu f7 fw4 dn-l'>Team Score Projections</div>
              <div className='v-solid-divider db-l' />
              <div className='bg-white w-50-l'>
                <div className='flex justify-between items-center f8 f7-l fw5 gray pa2'>
                  <span className='w-25 tc'>LIVE</span>
                  <span className='w-50 tc'>TEAM</span>
                  <span className='w-25 tc'>PROJECTED</span>
                </div>

                <div className='outline light-gray' />

                {data.liveScorePredictor.liveScores[index].inningIds.length >= 3 && (
                  <div className='fw2 f7 gray lh-copy pl3 pv2'>INNINGS 1</div>
                )}
                <div className='pa2'>
                  {/* 1st innings */}

                  <div className={`flex justify-between items-center mb4 mb2`}>
                    <div className='w-25 tc'>
                      <div className='f6 fw6'>
                        {data.liveScorePredictor.liveScores[index].inningNo === 1
                          ? data.liveScorePredictor.liveScores[index].currentScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index].currentWickets
                          : data.liveScorePredictor.liveScores[index].predictedScore +
                            (data.liveScorePredictor.liveScores[index].predictedWicket !== 10
                              ? '/' + data.liveScorePredictor.liveScores[index].predictedWicket
                              : '')}
                      </div>
                      <div className='f8 fw4 gray pt1'>
                        (
                        {data.liveScorePredictor.liveScores[index].inningNo === 1
                          ? data.liveScorePredictor.liveScores[index].overNo
                          : data.liveScorePredictor.liveScores[index].predictedOver}
                        )
                      </div>
                    </div>

                    <div className='w-50 flex justify-center items-center'>
                      <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                      <div className='tc ' style={{ height: 1, width: '70%', borderBottom: '1px dashed gray' }}>
                        <img
                          className='ib w2-3 h1-3 nt3 shadow-4'
                          src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].inningIds[0]}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          alt=''
                        />
                        <div className='dark-gray f7 fw5'>
                          {data.liveScorePredictor.liveScores[index].inningIds[0] ===
                          data.liveScorePredictor.liveScores[index].team1Id
                            ? data.liveScorePredictor.liveScores[index].team1ShortName
                            : data.liveScorePredictor.liveScores[index].team2ShortName}
                        </div>
                      </div>
                      <div className='bg-darkRed br-50' style={{ height: 10, width: 10 }} />
                    </div>

                    <div className='w-25 tc'>
                      <div className='f6 fw6 darkRed'>
                        {data.liveScorePredictor.liveScores[index].inningNo === 1 &&
                        data.liveScorePredictor.liveScores[index].predictedScore
                          ? data.liveScorePredictor.liveScores[index].predictedScore +
                            (data.liveScorePredictor.liveScores[index].predictedWicket !== 10
                              ? '/' + data.liveScorePredictor.liveScores[index].predictedWicket
                              : '')
                          : '--'}
                      </div>
                      {data.liveScorePredictor.liveScores[index].inningNo === 1 ? (
                        <div className='f8 fw4 gray pt1'>
                          {data.liveScorePredictor.liveScores[index].predictedOver
                            ? `(${data.liveScorePredictor.liveScores[index].predictedOver})`
                            : ''}
                        </div>
                      ) : (
                        ''
                      )}
                    </div>
                  </div>

                  {/* 2nd innings */}

                  <div className={`flex justify-between items-center mb4 mb2`}>
                    <div className='w-25 tc'>
                      <div className='f6 fw6'>
                        {data.liveScorePredictor.liveScores[index].inningNo === 2
                          ? data.liveScorePredictor.liveScores[index].currentScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index].currentWickets
                          : data.liveScorePredictor.liveScores[index].inningNo > 2
                          ? data.liveScorePredictor.liveScores[index].secondPredictedScore +
                            (data.liveScorePredictor.liveScores[index].secondPredictedWicket !== 10 &&
                            data.liveScorePredictor.liveScores[index].secondPredictedWicket
                              ? `/${data.liveScorePredictor.liveScores[index].secondPredictedWicket}`
                              : '')
                          : '--'}
                      </div>
                      <div className='f8 fw4 gray pt1'>
                        {data.liveScorePredictor.liveScores[index].inningNo === 2
                          ? `(${data.liveScorePredictor.liveScores[index].overNo})`
                          : data.liveScorePredictor.liveScores[index].inningNo > 2
                          ? `(${data.liveScorePredictor.liveScores[index].secondPredictedOVer})`
                          : ''}
                      </div>
                    </div>

                    <div className='w-50 flex justify-center items-center'>
                      <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                      <div className='tc ' style={{ height: 1, width: '70%', borderBottom: '1px dashed gray' }}>
                        <img
                          className='ib w2-3 h1-3 nt3 shadow-4'
                          src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].inningIds[1]}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          alt=''
                        />
                        <div className='dark-gray f7 fw5'>
                          {data.liveScorePredictor.liveScores[index].inningIds[1] ===
                          data.liveScorePredictor.liveScores[index].team1Id
                            ? data.liveScorePredictor.liveScores[index].team1ShortName
                            : data.liveScorePredictor.liveScores[index].team2ShortName}
                        </div>
                      </div>
                      <div className='bg-darkRed br-50' style={{ height: 10, width: 10 }} />
                    </div>

                    <div className='w-25 tc'>
                      <div className='f6 fw6 darkRed'>
                        {data.liveScorePredictor.liveScores[index].inningNo <= 2
                          ? data.liveScorePredictor.liveScores[index].secondPredictedScore +
                            (data.liveScorePredictor.liveScores[index].secondPredictedWicket !== 10
                              ? '/' + data.liveScorePredictor.liveScores[index].secondPredictedWicket
                              : '')
                          : '--'}
                      </div>
                      {data.liveScorePredictor.liveScores[index].inningNo <= 2 ? (
                        <div className='f8 fw4 gray pt1'>
                          {data.liveScorePredictor.liveScores[index].secondPredictedOVer
                            ? `(${data.liveScorePredictor.liveScores[index].secondPredictedOVer})`
                            : ''}
                        </div>
                      ) : (
                        ''
                      )}
                    </div>
                  </div>

                  {/* 3rd inning */}
                  {/* (x.inningIds.length > 2 ? (x.inningNo % 2 == 0 ? 4 : 3) : x.inningNo) */}

                  {data.liveScorePredictor.liveScores[index].inningIds.length >= 3 && (
                    <div>
                      <div className='fw2 f7 gray lh-copy pl2 pb3'>INNINGS 2</div>
                      <div className='flex justify-between items-center mb4 mb2'>
                        <div className='w-25 tc'>
                          <div className='f6 fw6'>
                            {(matchData[0].matchType !== 'Test'
                              ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                ? 4
                                : 3
                              : data.liveScorePredictor.liveScores[index].inningNo) === 3
                              ? data.liveScorePredictor.liveScores[index].currentScore +
                                '/' +
                                data.liveScorePredictor.liveScores[index].currentWickets
                              : data.liveScorePredictor.liveScores[index].inningNo > 3
                              ? data.liveScorePredictor.liveScores[index].thirdPredictedScore +
                                (data.liveScorePredictor.liveScores[index].thirdPredictedWicket !== 10
                                  ? '/' + data.liveScorePredictor.liveScores[index].thirdPredictedWicket
                                  : '')
                              : '--'}
                          </div>
                          <div className='f8 fw4 gray pt1'>
                            {(matchData[0].matchType !== 'Test'
                              ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                ? 4
                                : 3
                              : data.liveScorePredictor.liveScores[index].inningNo) === 3
                              ? `(${data.liveScorePredictor.liveScores[index].overNo})`
                              : data.liveScorePredictor.liveScores[index].inningNo > 3
                              ? `(${data.liveScorePredictor.liveScores[index].thirdPredictedOver})`
                              : ''}
                          </div>
                        </div>

                        <div className='w-50 flex justify-center items-center'>
                          <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                          <div className='tc ' style={{ height: 1, width: '70%', borderBottom: '1px dashed gray' }}>
                            <img
                              className='ib w2-3 h1-3 nt3 shadow-4'
                              src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].inningIds[2]}_flag_safari.png`}
                              onError={(evt) => (evt.target.src = flagPlaceHolder)}
                              alt=''
                            />
                            <div className='dark-gray f7 fw5'>
                              {data.liveScorePredictor.liveScores[index].inningIds[2] ===
                              data.liveScorePredictor.liveScores[index].team1Id
                                ? data.liveScorePredictor.liveScores[index].team1ShortName
                                : data.liveScorePredictor.liveScores[index].team2ShortName}
                            </div>
                          </div>
                          <div className='bg-darkRed br-50' style={{ height: 10, width: 10 }} />
                        </div>

                        <div className='w-25 tc'>
                          <div className='f6 fw6 darkRed'>
                            {(matchData[0].matchType !== 'Test'
                              ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                ? 4
                                : 3
                              : data.liveScorePredictor.liveScores[index].inningNo) <= 3
                              ? data.liveScorePredictor.liveScores[index].thirdPredictedScore +
                                (data.liveScorePredictor.liveScores[index].thirdPredictedWicket !== 10
                                  ? '/' + data.liveScorePredictor.liveScores[index].thirdPredictedWicket
                                  : '')
                              : '--'}
                          </div>
                          {(matchData[0].matchType !== 'Test'
                            ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                              ? 4
                              : 3
                            : data.liveScorePredictor.liveScores[index].inningNo) <= 3 ? (
                            <div className='f8 fw4 gray pt1'>
                              {data.liveScorePredictor.liveScores[index].thirdPredictedOver
                                ? `(${data.liveScorePredictor.liveScores[index].thirdPredictedOver})`
                                : ''}
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                    </div>
                  )}

                  {/* 4th innings */}

                  {data.liveScorePredictor.liveScores[index].inningIds.length === 4 &&
                    data.liveScorePredictor.liveScores[index].inningIds[
                      data.liveScorePredictor.liveScores[index].inningIds.length - 1
                    ] !== null && (
                      <div>
                        <div className='flex justify-between items-center mb4 mb2'>
                          <div className='w-25 tc'>
                            <div className='f6 fw6'>
                              {(matchData[0].matchType !== 'Test'
                                ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                  ? 4
                                  : 3
                                : data.liveScorePredictor.liveScores[index].inningNo) === 4
                                ? data.liveScorePredictor.liveScores[index].currentScore +
                                  '/' +
                                  data.liveScorePredictor.liveScores[index].currentWickets
                                : '--'}
                            </div>
                            <div className='f8 fw4 gray pt1'>
                              {(matchData[0].matchType !== 'Test'
                                ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                  ? 4
                                  : 3
                                : data.liveScorePredictor.liveScores[index].inningNo) === 4
                                ? `(${data.liveScorePredictor.liveScores[index].overNo})`
                                : ''}
                            </div>
                          </div>

                          <div className='w-50 flex justify-center items-center'>
                            <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                            <div className='tc ' style={{ height: 1, width: '70%', borderBottom: '1px dashed gray' }}>
                              <img
                                className='ib w2-3 h1-3 nt3 shadow-4'
                                src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].inningIds[3]}_flag_safari.png`}
                                onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                alt=''
                              />
                              <div className='dark-gray f7 fw5'>
                                {data.liveScorePredictor.liveScores[index].inningIds[3] ===
                                data.liveScorePredictor.liveScores[index].team1Id
                                  ? data.liveScorePredictor.liveScores[index].team1ShortName
                                  : data.liveScorePredictor.liveScores[index].team2ShortName}
                              </div>
                            </div>
                            <div className='bg-darkRed br-50' style={{ height: 10, width: 10 }} />
                          </div>

                          <div className='w-25 tc'>
                            <div className='f6 fw6 darkRed'>
                              {(matchData[0].matchType !== 'Test'
                                ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                  ? 4
                                  : 3
                                : data.liveScorePredictor.liveScores[index].inningNo) <= 4
                                ? data.liveScorePredictor.liveScores[index].fourthPredictedScore +
                                  (data.liveScorePredictor.liveScores[index].fourthPredictedWicket !== 10
                                    ? '/' + data.liveScorePredictor.liveScores[index].fourthPredictedWicket
                                    : '')
                                : '--'}
                            </div>
                            {(matchData[0].matchType !== 'Test'
                              ? data.liveScorePredictor.liveScores[index].inningNo % 2 == 0
                                ? 4
                                : 3
                              : data.liveScorePredictor.liveScores[index].inningNo) <= 4 ? (
                              <div className='f8 fw4 gray pt1'>
                                {data.liveScorePredictor.liveScores[index].fourthPredictedOver
                                  ? `(${data.liveScorePredictor.liveScores[index].fourthPredictedOver})`
                                  : ''}
                              </div>
                            ) : (
                              ''
                            )}
                          </div>
                        </div>
                      </div>
                    )}
                </div>

                <div className='outline light-gray' />
                {data && data.liveScorePredictor && data.liveScorePredictor.liveScores[index] && (
                  <div className='ph2 pv3 flex justify-center items-center'>
                    <div className='f7 fw4 pr2'>Projected Result</div>
                    <div className='br4 pv1 ph3 tc f7 fw5 truncate' style={{ backgroundColor: '#F16A3633' }}>
                      {data.liveScorePredictor.liveScores[index].projected_result}
                    </div>
                  </div>
                )}
              </div>
            </div>

            <div className='bg-white pa2 '>
              <div className='flex items-center justify-between justify-around-l '>
                <img
                  src={button ? pause : play}
                  alt=''
                  className='h2-4 w2-4 mr2'
                  onClick={() => (
                    index === data.liveScorePredictor.liveScores.length - 1 ? setIndex(0) : '',
                    setbutton(!button),
                    setclick(!click)
                  )}
                />
                <div className='w-90 pl2'>
                  <Slider
                    className='slider-main'
                    max={data.liveScorePredictor.liveScores.length - 1}
                    min={0}
                    step={1}
                    value={index}
                    onChange={(val) => {
                      if (
                        val !== data.liveScorePredictor.liveScores.length - 1 &&
                        val <= data.liveScorePredictor.liveScores.length - 1 &&
                        !button
                      ) {
                        setIndex(val);
                      }
                    }}
                    handle={handle}
                    handleStyle={[
                      {
                        backgroundColor: 'white',
                        border: '1px solid #a70e13',
                        width: '22px',
                        height: '22px',
                        marginTop: '-8px',
                        boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
                      }
                    ]}
                    trackStyle={[{ backgroundColor: '#a70e13', height: '6px' }]}
                    railStyle={{
                      backgroundColor: '#e8ebf3',
                      height: '6px'
                    }}
                  />
                </div>
              </div>
              <div className='tc f8 flex justify-center items-center'>
                {matchData[0].matchType === 'Test' && (
                  <div className=''>INN {data.liveScorePredictor.liveScores[index].inningNo} </div>
                )}
                <div className='ph2'>
                  {data.liveScorePredictor.liveScores[index].inningIds[
                    data.liveScorePredictor.liveScores[index].inningNo - 1
                  ] === data.liveScorePredictor.liveScores[index].team1Id
                    ? data.liveScorePredictor.liveScores[index].team1ShortName
                    : data.liveScorePredictor.liveScores[index].team2ShortName}{' '}
                  BAT
                </div>
                {data.liveScorePredictor.liveScores[index].overNo > 0 && (
                  <div>OVR {data.liveScorePredictor.liveScores[index].overNo}</div>
                )}
              </div>
            </div>
          </div>
        ) : (
          <div className=' bg-white pv3'>
            <div>
              <img
                className='w45-m h45-m w4 h4 w5-l h5-l'
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt='loading...'
              />
            </div>
            <div className='tc pv2 f5 fw5'>Data Not Available</div>
          </div>
        )}
      </div>
    );
  }
}
