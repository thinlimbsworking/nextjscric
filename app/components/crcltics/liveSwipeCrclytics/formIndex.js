import React, { useEffect, useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { CRICLYTICS_FORM_INDEX } from '../../../api/queries'
import DataNotFound from '../../commom/datanotfound'
import Heading from '../../commom/heading'
import Loading from '../../commom/loading'

export default function formIndex(props) {
  const [toggle, setToggle] = useState('')
  const [teamKey, setTeamKey] = useState('teamOne')
  const { loading, error, data } = useQuery(CRICLYTICS_FORM_INDEX, {
    // variables: { matchID: props.matchID},
    variables: { matchID: props.matchID },
    onCompleted: (data) => {
      setToggle(
        (data &&
          data.getFormIndex &&
          data.getFormIndex.teamOne &&
          data.getFormIndex.teamOne.name) ||
          [],
      )
    },
  })

  const capsuleCss = {
    verygood: 'h-2 w-2 rounded-full bg-green',
    good: 'h-2 w-2 rounded-full bg-lime-500',
    neutral: 'h-2 w-2 rounded-full bg-yellow',
    bad: 'h-2 w-2 rounded-full bg-orange-3',
    verybad: 'h-2 w-2 rounded-full bg-red',
    none: 'h-2 w-2 rounded-full border-solid border-2 border-gray-2',
  }
  const capsuleCssColor = {
    verygood: ' text-green text-xs font-bold',
    good: ' text-xs  text-lime-500  font-bold',
    neutral: ' text-xs text-yellow  font-bold',
    bad: ' text-orange-3  text-xs  font-bold',
    verybad: 'text-red text-xs font-bold',
    none: 'text-gray-2 text-xs font-bold',
  }
  if (loading) {
    return <Loading />
  }

  return (
    <div className="w-full">
      <div className="md:ml-3 md:mt-0 mt-2 dark:px-4">
        <span className="">
          <Heading
            hideBottom
            heading={'Form Index'}
            subHeading={
              'An indicator of how a player has performed in the last one year when compared to other players of similar skill set'
            }
          />
        </span>

        {/* <div className='  m w-full'>
        <div className='text-md  text-white tracking-wide font-bold '>  Form Index</div>
        <div className='text-md  text-white tracking-wide font-bold '>   An accurate analysis of players from both teams based on their form from recent matches</div>
       
      <div className='w-12 h-1 bg-blue-8 mt-2'></div>
      </div> */}
        {/* <div className="mt-4 dark:text-white text-black text-sm font-semibold ">
          Current Team Form
        </div>
        <div className="w-12 h-1 bg-blue-8 mt-2"></div> */}
        <span className="dark:mt-4 hidden md:block my-2">
          <Heading heading={'Current Team Form'} />
        </span>
      </div>
      <div className="ml-4 md:hidden mt-3">
        <span className="">
          <Heading heading={'Current Team Form'} />
        </span>
      </div>

      <div className="hidden lg:block md:block">
        {data && data.getFormIndex && data.getFormIndex.teamOne ? (
          <div className="flex flex-col items-center justify-center text-sm py-2">
            <div className="flex w-8/12 lg:w-4/12 md:w-4/12 lg:py-1 md:py-1 items-center justify-center dark:bg-gray-4  rounded-3xl p-0.5">
              <div
                className={`w-1/2 lg:mx-1 py-1 text-center dark:text-white text-black cursor-pointer rounded-3xl text-xs ${
                  toggle === data.getFormIndex.teamOne.name
                    ? `border-2 md:border-basered md:text-white md:bg-basered border-solid`
                    : 'md:text-black md:bg-[#EEEEEE] md:border-2 md:border-solid'
                }`}
                onClick={() => (
                  setToggle(data.getFormIndex.teamOne.name),
                  setTeamKey('teamOne')
                )}
              >
                <p className="lg:text-xs lg:font-semibold">
                  {data.getFormIndex.teamOne.name}
                </p>
              </div>
              <div
                className={`w-1/2 md:mx-1 py-1 text-center rounded-3xl dark:text-white cursor-pointer text-xs  ${
                  toggle === data.getFormIndex.teamTwo.name
                    ? `border-2 md:text-white md:border-basered md:bg-basered border-solid`
                    : 'md:text-black md:bg-[#EEEEEE] border border-solid'
                }`}
                onClick={() => (
                  setToggle(data.getFormIndex.teamTwo.name),
                  setTeamKey('teamTwo')
                )}
              >
                <p className="lg:text-xs lg:font-semibold">
                  {data.getFormIndex.teamTwo.name}
                </p>
              </div>
            </div>

            <div className="flex pt-3">
              <div className="flex items-center lg:mx-2 md:mx-2">
                <p className="h-2 w-2 m-1 rounded-full bg-green lg:mx-1 md:mx-1"></p>
                <p className="text-gray-2 lg:text-sm md:text-sm text-xs font-normal">
                  Very Good
                </p>
              </div>
              <div className="flex items-center justify-center lg:mx-2 md:mx-2">
                <p className="h-2 w-2 m-1 rounded-full bg-green-3"></p>
                <p className="text-gray-2 lg:text-sm md:text-sm text-xs font-normal">
                  Good
                </p>
              </div>
              <div className="flex items-center justify-center lg:mx-2 md:mx-2">
                <p className="h-2 w-2 m-1 rounded-full bg-yellow lg:mx-1 md:mx-1"></p>
                <p className="text-gray-2 lg:text-sm md:text-sm text-xs font-normal">
                  Neutral
                </p>
              </div>

              <div className="flex items-center lg:mx-2 md:mx-2">
                <p className="h-2 w-2 m-1 rounded-full bg-orange-3 lg:mx-1 md:mx-1"></p>
                <p className="text-gray-2 text-xs lg:text-sm md:text-sm font-normal">
                  Bad
                </p>
              </div>
              <div className="flex items-center lg:mx-2 md:mx-2">
                <p className="h-2 w-2 m-1 rounded-full bg-red lg:mx-1 md:mx-1"></p>
                <p className="text-gray-2 text-xs lg:text-sm md:text-sm font-normal">
                  Very Bad
                </p>
              </div>
            </div>
          </div>
        ) : (
          <div className="flex mt-5">
            <DataNotFound />
          </div>
        )}
      </div>

      {data && data.getFormIndex && data.getFormIndex.teamOne ? (
        <div className="mx-1 md:mt-3 mt-3 rounded px-3 py-3 text-white md:bg-white md:rounded-md md:shadow">
          <div className="lg:hidden md:hidden bg-gray-4 rounded-3xl  flex items-center justify-between text-sm  ">
            <div
              className={`w-1/2 text-center rounded-3xl py-1.5 text-xs ${
                toggle === data.getFormIndex.teamOne.name
                  ? `border-2  border-green`
                  : ''
              }`}
              onClick={() => (
                setToggle(data.getFormIndex.teamOne.name), setTeamKey('teamOne')
              )}
            >
              {data.getFormIndex.teamOne.name}
            </div>
            <div
              className={`w-1/2 text-center rounded-3xl py-1.5 text-xs  ${
                toggle === data.getFormIndex.teamTwo.name
                  ? `border-2  border-green`
                  : ''
              }`}
              onClick={() => (
                setToggle(data.getFormIndex.teamTwo.name), setTeamKey('teamTwo')
              )}
            >
              {data.getFormIndex.teamTwo.name}
            </div>
          </div>
          <div className="flex justify-evenly pt-3 lg:hidden md:hidden">
            <div className="flex items-center">
              <p className="h-2 w-2 m-1 rounded-full bg-green"></p>
              <p className="text-gray-2 text-xs font-normal pl-1.5">
                Very Good
              </p>
            </div>
            <div className="flex items-center justify-center">
              <p className="h-2 w-2 rounded-full bg-lime-500"></p>
              <p className="text-gray-2 text-xs font-normal pl-1.5">Good</p>
            </div>
            <div className="flex items-center justify-center">
              <p className="h-2 w-2 rounded-full bg-yellow"></p>
              <p className="text-gray-2 text-xs font-normal pl-1.5">Neutral</p>
            </div>

            <div className="flex items-center">
              <p className="h-2 w-2 rounded-full bg-orange-3"></p>
              <p className="text-gray-2 text-xs font-normal pl-1.5">Bad</p>
            </div>
            <div className="flex items-center">
              <p className="h-2 w-2 rounded-full bg-red"></p>
              <p className="text-gray-2 text-xs font-normal pl-1.5">Very Bad</p>
            </div>
          </div>
          <div className="flex flex-col lg:justify-center lg:items-center">
            <div className="flex flex-col lg:w-8/12">
              {
                <div>
                  {data.getFormIndex.teamOne.batsmen && (
                    <div className="flex gray-2 dark:my-3 text-xs mt-3 uppercase dark:text-white text-black">
                      {' '}
                      BATTERS
                    </div>
                  )}
                  {teamKey == 'teamOne'
                    ? data.getFormIndex.teamOne.batsmen &&
                      data.getFormIndex.teamOne.batsmen.map((player, y) => {
                        return (
                          <div className="dark:bg-gray-4 bg-white rounded-lg mt-2 border dark:border-2 dark:border-gray flex justify-between items-center p-2">
                            <div className="flex items-center justify-between">
                              <img
                                className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                onError={(evt) =>
                                  (evt.target.src = '/pngsV2/playerph.png')
                                }
                              />
                              <div className="pl-3">
                                <p className="text-sm font-medium text-black dark:text-white">
                                  {player.playerName}
                                </p>
                                <p className="text-gray-2 text-[11px] font-medium">
                                  BAT SR: {player.BatSR} | BAT AVG:{' '}
                                  {player.BatAvg}
                                </p>
                              </div>
                            </div>
                            <div>
                              <div className="flex items-center">
                                <p
                                  className={`${
                                    capsuleCssColor[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs mr-1`}
                                >
                                  {/* {player.OverallForm} */}
                                  {player.OverallForm === 'None'
                                    ? 'Not Available'
                                    : player.OverallForm}
                                </p>
                                <p
                                  className={`${
                                    capsuleCss[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs`}
                                ></p>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    : data.getFormIndex.teamTwo.batsmen &&
                      data.getFormIndex.teamTwo.batsmen.map((player, y) => {
                        return (
                          <div className="dark:bg-gray-4 bg-white border-2 border-solid border-[#E2E2E2] dark:border-2 dark:border-gray rounded-lg mt-2 flex justify-between items-center p-2">
                            <div className="flex items-center justify-between">
                              <img
                                className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                onError={(evt) =>
                                  (evt.target.src = '/pngsV2/playerph.png')
                                }
                              />
                              <div className="pl-3">
                                <p className="text-sm font-medium dark:text-white text-black">
                                  {player.playerName}
                                </p>
                                <p className="text-gray-2 text-[11px] font-medium ">
                                  BAT SR: {player.BatSR} | BAT AVG:{' '}
                                  {player.BatAvg}
                                </p>
                              </div>
                            </div>
                            <div>
                              <div className="flex items-center">
                                <p
                                  className={`${
                                    capsuleCssColor[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs mr-1`}
                                >
                                  {/* {player.OverallForm} */}
                                  {player.OverallForm === 'None'
                                    ? 'Not Available'
                                    : player.OverallForm}
                                </p>
                                <p
                                  className={`${
                                    capsuleCss[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs`}
                                ></p>
                              </div>
                            </div>
                          </div>
                        )
                      })}
                </div>
              }
              {
                <div>
                  {teamKey == 'teamOne' ? (
                    <>
                      {data.getFormIndex.teamOne.keepers && (
                        <div className="flex gray-2 dark:my-3 text-xs text-black dark:text-white mt-3 uppercase">
                          WICKETS KEEPER
                        </div>
                      )}
                      {data.getFormIndex.teamOne.keepers &&
                        data.getFormIndex.teamOne.keepers.map((player, y) => {
                          return (
                            <div className="dark:bg-gray-4 bg-white border-[#E2E2E2] border-solid border-2 dark:border-2 dark:border-gray rounded-lg mt-2 flex justify-between items-center p-2">
                              <div className="flex items-center justify-between">
                                <img
                                  className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                  src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = '/pngsV2/playerph.png')
                                  }
                                />
                                <div className="pl-3">
                                  <p className="text-sm font-medium dark:text-white text-black pb-1">
                                    {player.playerName}
                                  </p>
                                  <p className="text-gray-2 text-[11px] font-medium ">
                                    BAT SR: {player.BatSR} | BAT AVG:{' '}
                                    {player.BatAvg}
                                  </p>
                                </div>
                              </div>
                              <div>
                                <div className="flex items-center">
                                  <p
                                    className={`${
                                      capsuleCssColor[
                                        player.OverallForm.split(' ')
                                          .join('')
                                          .toLowerCase()
                                      ]
                                    } uppercase text-xs mr-1`}
                                  >
                                    {/* {player.OverallForm} */}
                                    {player.OverallForm === 'None'
                                      ? 'Not Available'
                                      : player.OverallForm}
                                  </p>
                                  <p
                                    className={`${
                                      capsuleCss[
                                        player.OverallForm.split(' ')
                                          .join('')
                                          .toLowerCase()
                                      ]
                                    } uppercase text-xs`}
                                  ></p>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                    </>
                  ) : (
                    <>
                      {data.getFormIndex.teamTwo.keepers && (
                        <div className="flex gray-2 dark:my-3 text-xs mt-3 text-black dark:text-white uppercase">
                          WICKETS KEEPER
                        </div>
                      )}

                      {data.getFormIndex.teamTwo.keepers &&
                        data.getFormIndex.teamTwo.keepers.map((player, y) => {
                          return (
                            <div className="dark:bg-gray-4 bg-white rounded-lg mt-2 flex border-2 border-solid border-gray-11 dark:border-2 dark:border-gray justify-between items-center p-2">
                              <div className="flex items-center justify-between">
                                <img
                                  className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                  src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = '/pngsV2/playerph.png')
                                  }
                                />
                                <div className="pl-3">
                                  <p className="text-sm font-medium text-black dark:text-white pb-1">
                                    {player.playerName}
                                  </p>
                                  <p className="text-gray-2 text-[11px] font-medium ">
                                    BAT SR: {player.BatSR} | BAT AVG:{' '}
                                    {player.BatAvg}
                                  </p>
                                </div>
                              </div>
                              <div>
                                <div className="flex items-center">
                                  <p
                                    className={`${
                                      capsuleCssColor[
                                        player.OverallForm.split(' ')
                                          .join('')
                                          .toLowerCase()
                                      ]
                                    } uppercase text-xs mr-1`}
                                  >
                                    {/* {player.OverallForm} */}
                                    {player.OverallForm === 'None'
                                      ? 'Not Available'
                                      : player.OverallForm}
                                  </p>
                                  <p
                                    className={`${
                                      capsuleCss[
                                        player.OverallForm.split(' ')
                                          .join('')
                                          .toLowerCase()
                                      ]
                                    } uppercase text-xs`}
                                  ></p>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                    </>
                  )}
                </div>
              }
            </div>
            <div className="flex flex-col lg:w-8/12 lg:justify-center">
              {
                <div>
                  {teamKey == 'teamOne' ? (
                    <>
                      {data.getFormIndex.teamOne.allRounders && (
                        <div className="flex gray-2 dark:my-3 text-xs text-black dark:text-white mt-3">
                          ALL ROUNDER
                        </div>
                      )}
                      {data.getFormIndex.teamOne.allRounders &&
                        data.getFormIndex.teamOne.allRounders.map(
                          (player, y) => {
                            return (
                              <div className="dark:bg-gray-4 bg-white rounded-lg mt-2 flex border-2 border-solid border-[#E2E2E2] dark:border-2 dark:border-gray justify-between items-center p-2">
                                <div className="flex items-center justify-between">
                                  <img
                                    className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                    src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                    onError={(evt) =>
                                      (evt.target.src = '/pngsV2/playerph.png')
                                    }
                                  />
                                  <div className="pl-3">
                                    <p className="text-sm text-black dark:text-white font-medium pb-1">
                                      {player.playerName}
                                    </p>
                                    <p className="text-gray-2 text-[11px] font-medium ">
                                      BAT AVG: {player.BatAvg} | BOWL AVG:{' '}
                                      {player.BowlAvg}
                                    </p>
                                  </div>
                                </div>
                                <div>
                                  <div className="flex items-center">
                                    <p
                                      className={`${
                                        capsuleCssColor[
                                          player.OverallForm.split(' ')
                                            .join('')
                                            .toLowerCase()
                                        ]
                                      } uppercase text-xs mr-1`}
                                    >
                                      {/* {player.OverallForm} */}
                                      {player.OverallForm === 'None'
                                        ? 'Not Available'
                                        : player.OverallForm}
                                    </p>
                                    <p
                                      className={`${
                                        capsuleCss[
                                          player.OverallForm.split(' ')
                                            .join('')
                                            .toLowerCase()
                                        ]
                                      } uppercase text-xs`}
                                    ></p>
                                  </div>
                                </div>
                              </div>
                            )
                          },
                        )}
                    </>
                  ) : (
                    <>
                      {' '}
                      {data.getFormIndex.teamTwo.allRounders && (
                        <div className="flex dark:my-3 gray-2 dark:text-white text-black text-xs mt-3">
                          ALL ROUNDER
                        </div>
                      )}
                      {data.getFormIndex.teamTwo.allRounders &&
                        data.getFormIndex.teamTwo.allRounders.map(
                          (player, y) => {
                            return (
                              <div className="dark:bg-gray-4 bg-white border-[#E2E2E2] dark:border-2 dark:border-gray border-2 border-solid rounded-lg mt-2 flex justify-between items-center p-2">
                                <div className="flex items-center justify-between">
                                  <img
                                    className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                    src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                    onError={(evt) =>
                                      (evt.target.src = '/pngsV2/playerph.png')
                                    }
                                  />
                                  <div className="pl-3">
                                    <p className="text-sm dark:text-white text-black font-medium pb-1">
                                      {player.playerName}{' '}
                                    </p>
                                    <p className="text-gray-2 text-[11px] font-medium ">
                                      BAT AVG: {player.BatAvg} | BOWL AVG:{' '}
                                      {player.BowlAvg}
                                    </p>
                                  </div>
                                </div>
                                <div>
                                  <div className="flex items-center">
                                    <p
                                      className={`${
                                        capsuleCssColor[
                                          player.OverallForm.split(' ')
                                            .join('')
                                            .toLowerCase()
                                        ]
                                      } uppercase text-xs mr-1`}
                                    >
                                      {/* {player.OverallForm} */}
                                      {player.OverallForm === 'None'
                                        ? 'Not Available'
                                        : player.OverallForm}
                                    </p>
                                    <p
                                      className={`${
                                        capsuleCss[
                                          player.OverallForm.split(' ')
                                            .join('')
                                            .toLowerCase()
                                        ]
                                      } uppercase text-xs`}
                                    ></p>
                                  </div>
                                </div>
                              </div>
                            )
                          },
                        )}
                    </>
                  )}
                </div>
              }
              {
                <div>
                  <div className="flex gray-2 text-xs mt-3 dark:my-3 uppercase text-black dark:text-white">
                    {' '}
                    bowlers
                  </div>
                  {teamKey == 'teamOne'
                    ? data.getFormIndex.teamOne.bowlers.map((player, y) => {
                        return (
                          <div className="dark:bg-gray-4 bg-white rounded-lg mt-2 border-2 border-solid border-[#E2E2E2] dark:border-2 dark:border-gray  flex justify-between items-center p-2">
                            <div className="flex items-center justify-between">
                              <img
                                className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                onError={(evt) =>
                                  (evt.target.src = '/pngsV2/playerph.png')
                                }
                              />
                              <div className="pl-3">
                                <p className="text-sm font-medium dark:text-white text-black ">
                                  {player.playerName}
                                </p>
                                <p className="text-gray-2 text-[11px] font-medium">
                                  BOWL SR:
                                  {player.BowlSR} | BOWL AVG: {player.BowlAvg}
                                </p>
                              </div>
                            </div>
                            <div>
                              <div className="flex items-center">
                                <p
                                  className={`${
                                    capsuleCssColor[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs mr-1`}
                                >
                                  {/* {player.OverallForm} */}
                                  {player.OverallForm === 'None'
                                    ? 'Not Available'
                                    : player.OverallForm}
                                </p>
                                <p
                                  className={`${
                                    capsuleCss[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs`}
                                ></p>
                              </div>
                            </div>
                          </div>
                        )
                      })
                    : data.getFormIndex.teamTwo.bowlers.map((player, y) => {
                        return (
                          <div className="dark:bg-gray-4 bg-white border-solid border-2 border-[#E2E2E2] dark:border-2 dark:border-gray rounded-lg mt-2 flex justify-between items-center p-2">
                            <div className="flex items-center justify-between">
                              <img
                                className="h-12 w-12 bg-gray object-top object-cover rounded-full"
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                onError={(evt) =>
                                  (evt.target.src = '/pngsV2/playerph.png')
                                }
                              />
                              <div className="pl-3">
                                <p className=" dark:text-white text-black pb-1">
                                  {player.playerName}
                                </p>
                                <p className="text-gray-2 text-[11px] font-medium">
                                  BOWL SR:
                                  {player.BowlSR} | BOWL AVG: {player.BowlAvg}
                                </p>
                              </div>
                            </div>
                            <div>
                              <div className="flex items-center">
                                <p
                                  className={`${
                                    capsuleCssColor[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs mr-1`}
                                >
                                  {player.OverallForm === 'None'
                                    ? 'Not Available'
                                    : player.OverallForm}
                                </p>
                                <p
                                  className={`${
                                    capsuleCss[
                                      player.OverallForm.split(' ')
                                        .join('')
                                        .toLowerCase()
                                    ]
                                  } uppercase text-xs`}
                                ></p>
                              </div>
                              {/* <div className={`${capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]} uppercase text-xs`}>
                {player.OverallForm.toLowerCase()==='none'?'NOT AVAIBLE':player.OverallForm} 
                </div>
                <div className={`${capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]} h-2 w-2`}>
                  </div> */}
                            </div>
                          </div>
                        )
                      })}
                </div>
              }
            </div>
          </div>
        </div>
      ) : (
        <div className="flex mt-5">
          <DataNotFound />
        </div>
      )}
    </div>
  )
}
