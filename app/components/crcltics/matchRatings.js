import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { MATCH_RATINGS } from '../../api/queries'
import CleverTap from 'clevertap-react'

const location = '/svgs/location-icon-color.png'
const information = '/svgs/information.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const ground = '/svgs/groundImageWicket.png'
const playerAvatar = '/placeHodlers/playerAvatar.png'

const MatchRating = ({ browser, ...props }) => {
  const [infoToggle, setInfoToggle] = useState(false)
  const [showRatings, setShowRatings] = useState(true)
  const [indexNo, setindexNo] = useState(0)
  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }

  const { error, loading, data } = useQuery(MATCH_RATINGS, {
    variables: { matchID: props.matchData[0].matchID },
  })
  useEffect(() => {
    if (data && data.matchRatings) {
      CleverTap.initialize('Criclytics', {
        Source: 'Slider',
        MatchID: props.matchData[0].matchID,
        TeamAID: data.matchRatings.team1ID || '',
        TeamBID: data.matchRatings.team2ID || '',
        MatchFormat: props.matchData[0].matchType,
        MatchStatus: 'Completed',
        CriclyticsEngine: 'CompletedMR',
        Platform: localStorage.Platform,
      })
    }
  }, [])
  if (error) return <div></div>
  if (loading) return <div></div>
  else
    return (
      <div>
        {data && data.matchRatings && data.matchRatings.finalArray ? (
          <div className="bg-white mv2">
            <div className="flex justify-between items-center pa2">
              <h1 className="f7 f5-l fw5 grey_10 ttu">Report Card</h1>
              <img
                src={information}
                onClick={() => toggleInfo()}
                alt=""
                className="h1 w1"
              />
            </div>
            <div className="outline light-gray" />
            {infoToggle && (
              <div>
                <div className="f7 grey_8 pa2">
                  A comprehensive view of a player's impact on the game
                </div>
                <div className="outline light-gray" />
              </div>
            )}
            <div className="pv2 dn-l">
              {data.matchRatings.finalArray.map((plr, y) => (
                <div key={y}>
                  <div
                    className={`flex items-center pa2 ${
                      showRatings && indexNo === y ? 'shadow-3 pv2' : ''
                    }`}
                    onClick={() => (setShowRatings(true), setindexNo(y))}
                  >
                    <div className="w-20">
                      <div className="br-50 center w3 h3 overflow-hidden b--light-gray ba ">
                        <img
                          className="contain"
                          src={`https://images.cricket.com/players/${plr.playerID}_headshot_safari.png`}
                          alt=""
                          onError={(evt) => (evt.target.src = playerAvatar)}
                        />
                      </div>
                      <img
                        className="w15 h1 shadow-4 flex justify-center center nt2"
                        src={`https://images.cricket.com/teams/${plr.playerTeamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        alt=""
                      />
                      <div className="f6 fw4 pt1 tc ttu truncate">
                        {plr.playerTeamName}
                      </div>
                    </div>
                    <div className="w-80 ml2">
                      <div className="flex justify-between items-center nt3">
                        <div className="f6 fw5">{plr.playerName}</div>
                        <div className="darkRed f5 fw5">{plr.totalPoints}</div>
                      </div>
                      {showRatings && y == indexNo && (
                        <div className="flex justify-between f7 gray pv2">
                          <div className="">
                            <div className="silver tc">
                              Batting Contribution
                            </div>
                            <div className="tc f6 fw5 pt1 grey_8">
                              {plr.battingPoints}
                            </div>
                          </div>
                          <div className="pl2">
                            <div className="silver tc ">
                              Bowling Contribution
                            </div>
                            <div className="tc f6 fw5 pt1 grey_8">
                              {plr.bowlingPoints}
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                  {data.matchRatings.finalArray.length - 1 !== y && (
                    <div className="divider" />
                  )}
                </div>
              ))}
            </div>
            <div className="dn db-ns">
              <div className="bg-white shadow-4 pa2">
                {data.matchRatings.finalArray.map((plr, y) => (
                  <div key={y}>
                    <div
                      className={`flex ph2 items-center shadow-4 pv3 mv3 mh2`}
                    >
                      <div className="br-50 center w2-6 h2-6 overflow-hidden b--light-gray ba ">
                        <img
                          className="contain w3"
                          src={`https://images.cricket.com/players/${plr.playerID}_headshot_safari.png`}
                          alt=""
                          onError={(evt) => (evt.target.src = playerAvatar)}
                        />
                      </div>
                      <div className="w-90 flex justify-between items-center">
                        <div className="pa2 w-30">
                          <div className="f6 fw5">{plr.playerName}</div>
                          <div className="pt2 flex justify-start items-center">
                            <img
                              className="w2 h1 shadow-4"
                              src={`https://images.cricket.com/teams/${plr.playerTeamID}_flag_safari.png`}
                              onError={(evt) =>
                                (evt.target.src = flagPlaceHolder)
                              }
                              alt=""
                            />
                            <div className="f7 fw5 pl2 ttu">
                              {plr.playerTeamName}
                            </div>
                          </div>
                        </div>
                        <div className="darkRed f5 fw5 w-10 tc">
                          {plr.totalPoints}
                        </div>
                        <div className="ph2">
                          <div className="f7 fw5 silver">
                            Batting Contribution
                          </div>
                          <div className="tc f6 fw5 pt2 grey_7">
                            {plr.battingPoints}
                          </div>
                        </div>
                        <div className="ph2">
                          <div className="f7 fw5 silver">
                            Bowling Contribution
                          </div>
                          <div className="tc f6 fw5 pt2 grey_7">
                            {plr.bowlingPoints}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          <div className=" bg-white pv3 mt2 shadow-4">
            <div>
              <img
                className="w45-m h45-m w4 h4 w5-l h5-l"
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt="loading..."
              />
            </div>
            <div className="tc pv2 f5 fw5">Data Not Available</div>
          </div>
        )}
      </div>
    )
}

export default MatchRating
