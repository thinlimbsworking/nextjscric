// import React, { useState } from 'react';
// import Slider from 'rc-slider';
// import Tooltip from 'rc-tooltip';
// import { useQuery } from '@apollo/react-hooks';
// import { LIVE_SCORE_PREDICTOR } from '../../api/queries';

// const ground = '/svgs/groundImageWicket.png';
// const information = '/svgs/information.svg';
// const play = '/pngsV2/playcric.png';
// const pause = '/pngsV2/pausebutton.png';
// const flagPlaceHolder = '/svgs/images/flag_empty.svg';
// const Handle = Slider.Handle;

// const handle = (props) => {
//   const { value, dragging, index, ...restProps } = props;
//   return (
//     <Tooltip prefixCls='rc-slider-tooltip' overlay={value + 1} visible={dragging} placement='top' key={index}>
//       <Handle value={value} {...restProps} />
//     </Tooltip>
//   );
// };
// export default function MatchReel({ matchData, browser, ...props }) {
//   const [button, setbutton] = useState(false);
//   const [index, setIndex] = useState(0);
//   const [click, setclick] = useState(false);
//   const [infoToggle, setInfoToggle] = useState(false);

//   const { loading, error, data } = useQuery(LIVE_SCORE_PREDICTOR, {
//     variables: { matchId: props.matchID, matchType: props.matchType },
//     pollInterval: props.path !== 'matchreel' ? 8000 : '',
//     onCompleted: (data) => {
//       if (data.liveScorePredictor.liveScores.length) {
//         setIndex(data.liveScorePredictor.liveScores.length - 1);
//       }
//     }
//   });

//   let mytime = 0;
//   click &&
//     (mytime = setTimeout(function () {
//       index === data.liveScorePredictor.liveScores.length - 1
//         ? (clearTimeout(mytime), setclick(!click), setbutton(false))
//         : setIndex(index + 1);
//     }, 500));

//   const toggleInfo = () => {
//     setInfoToggle(true);
//     setTimeout(() => {
//       setInfoToggle(false);
//     }, 6000);
//   };
//   if (!data || error)
//     return (
//       <div className=' bg-white pv3'>
//         <div>
//           <img
//             className='w45-m h45-m w4 h4 w5-l h5-l'
//             style={{ margin: 'auto', display: 'block' }}
//             src={ground}
//             alt='loading...'
//           />
//         </div>
//         <div className='tc pv2 f5 fw5'>Data Not Available</div>
//       </div>
//     );
//   else {
//     return (
//       <div className='mt2 overflow-y-hidden '>
       
      
    

//         {data.liveScorePredictor.liveScores.length > 0 &&
//         data.liveScorePredictor.liveScores[data.liveScorePredictor.liveScores.length - 1].winvizView !== null ? (
//           <div>
//             <div className='flex-l justify-between items-center shadow-4 '>
//               <div className='ph2 pv3 w-50-l'>
//                 {data.liveScorePredictor.liveScores[index].currentView && (
//                   <>
//                     <div className='flex flex-row justify-between f7 fw6'>
//                       <div
//                         className={`${
//                           data.liveScorePredictor.liveScores[index].currentView &&
//                           (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
//                           parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
//                             ? 'green_10'
//                             : 'red_10')
//                         }`}>
//                         {data.liveScorePredictor.liveScores[index].currentView &&
//                           `${data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage}%`}
//                       </div>
//                       {data.liveScorePredictor.liveScores[index].currentView &&
//                       parseInt(data.liveScorePredictor.liveScores[index].currentView.tiePercentage) > 0 ? (
//                         <div className='grey_7 '>
//                           {data.liveScorePredictor.liveScores[index].currentView.tiePercentage &&
//                             `${data.liveScorePredictor.liveScores[index].currentView.tiePercentage}%`}
//                         </div>
//                       ) : (
//                         <div />
//                       )}
//                       <div
//                         className={`${
//                           data.liveScorePredictor.liveScores[index].currentView &&
//                           (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
//                           parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
//                             ? 'red_10'
//                             : 'green_10')
//                         } flex justify-end`}>
//                         {data.liveScorePredictor.liveScores[index].currentView &&
//                           `${data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage}%`}
//                       </div>
//                     </div>
//                     <div className='flex pv1 justify-center items-center '>
//                       <div
//                         style={{
//                           width: `${
//                             data.liveScorePredictor.liveScores[index].currentView &&
//                             data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage
//                           }%`,
//                           height: 14
//                         }}
//                         className={` dib ${
//                           data.liveScorePredictor.liveScores[index].currentView &&
//                           (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
//                           parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
//                             ? 'bg-green_10'
//                             : 'bg-red_10')
//                         }`}></div>
//                       {data.liveScorePredictor.liveScores[index].currentView &&
//                       parseInt(data.liveScorePredictor.liveScores[index].currentView.tiePercentage) > 0 ? (
//                         <div
//                           className=' dib bg-grey_2'
//                           style={{
//                             width: `${data.liveScorePredictor.liveScores[index].currentView.tiePercentage}%`,
//                             height: 14
//                           }}
//                         />
//                       ) : (
//                         <div />
//                       )}
//                       <div
//                         style={{
//                           width: `${
//                             data.liveScorePredictor.liveScores[index].currentView &&
//                             data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage
//                           }%`,
//                           height: 14
//                         }}
//                         className={`dib ${
//                           data.liveScorePredictor.liveScores[index].currentView &&
//                           (parseInt(data.liveScorePredictor.liveScores[index].currentView.homeTeamPercentage) >
//                           parseInt(data.liveScorePredictor.liveScores[index].currentView.awayTeamPercentage)
//                             ? 'bg-red_10'
//                             : 'bg-green_10')
//                         } dib`}></div>
//                     </div>
//                     <div className='flex flex-row justify-between f7'>
//                       <div className='fw5 f7'>
//                         {data.liveScorePredictor.liveScores[index].currentView &&
//                           data.liveScorePredictor.liveScores[index].currentView.homeTeamShortName}
//                       </div>
//                       {data.liveScorePredictor.liveScores[index].currentView &&
//                       data.liveScorePredictor.liveScores[index].currentView.tiePercentage &&
//                       parseInt(data.liveScorePredictor.liveScores[index].currentView.tiePercentage) > 0 ? (
//                         props.matchType === 'Test' ? (
//                           <div className='fw5 f7'>DRAW</div>
//                         ) : (
//                           <div className='fw5 f7'>Tie</div>
//                         )
//                       ) : (
//                         <div />
//                       )}
//                       <div className='fw5 f7 flex justify-end'>
//                         {data.liveScorePredictor.liveScores[index].currentView &&
//                           data.liveScorePredictor.liveScores[index].currentView.awayTeamShortName}
//                       </div>
//                     </div>
//                   </>
//                 )}
//               </div>

           
            
           
//             </div>

//             <div className='pa2 '>
//               <div className='flex items-center justify-between justify-around-l '>
//                 <img
//                   src={button ? pause : play}
//                   alt=''
//                   className='h2-4 w2-4 mr2'
//                   onClick={() => (
//                     index === data.liveScorePredictor.liveScores.length - 1 ? setIndex(0) : '',
//                     setbutton(!button),
//                     setclick(!click)
//                   )}
//                 />
//                 <div className='w-90 pl2 flex h-10 bg-basebg justify-center  items-center p-1 rounded' >
//                   <Slider
//                     className='slider-main'
//                     max={data.liveScorePredictor.liveScores.length - 1}
//                     min={0}
//                     step={1}
//                     value={index}
//                     onChange={(val) => {
//                       if (
//                         val !== data.liveScorePredictor.liveScores.length - 1 &&
//                         val <= data.liveScorePredictor.liveScores.length - 1 &&
//                         !button
//                       ) {
//                         setIndex(val);
//                       }
//                     }}
//                     handle={handle}
//                     handleStyle={[
//                       {
//                         backgroundColor: 'white',
//                         border: 'white',
//                         width: '18px',
//                         height: '18px',
//                         marginTop: '-8px',
//                         boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
//                       }
//                     ]}
//                     trackStyle={[{ backgroundColor: '#38d925', height: '5px' }]}
//                     railStyle={{
//                       backgroundColor: 'gray',
//                       height: '5px'
//                     }}
//                   />
//                 </div>
//               </div>
//               <div className='tc f8 flex justify-center items-center'>
//                 {props.matchType === 'Test' && (
//                   <div className=''>INN {data.liveScorePredictor.liveScores[index].inningNo} </div>
//                 )}
//                 <div className='ph2'>
//                   {data.liveScorePredictor.liveScores[index].inningIds[
//                     data.liveScorePredictor.liveScores[index].inningNo - 1
//                   ] === data.liveScorePredictor.liveScores[index].team1Id
//                     ? data.liveScorePredictor.liveScores[index].team1ShortName
//                     : data.liveScorePredictor.liveScores[index].team2ShortName}
                    
//                     {' '}
//                   BAT
//                 </div>
//                 {data.liveScorePredictor.liveScores[index].overNo > 0 && (
//                   <div>OVR {data.liveScorePredictor.liveScores[index].overNo}</div>
//                 )}
//               </div>
//             </div>
//           </div>
//         ) : (
//           <div className='  pv3'>
//             <div>
//               <img
//                 className='w45-m h45-m w4 h4 w5-l h5-l'
//                 style={{ margin: 'auto', display: 'block' }}
//                 src={ground}
//                 alt='loading...'
//               />
//             </div>
//             <div className='tc pv2 f5 fw5'>Data Not Available</div>
//           </div>
//         )}
//       </div>
//     );
//   }
// }
