import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { POSTSERIES_PLAYER_IMPACT } from '../../api/queries'
import Tab from '../shared/Tab'
import DataNotFound from '../../components/commom/datanotfound'
import Loading from '../loading'
import Heading from '../shared/heading'
import ImageWithFallback from '../commom/Image'
import BatIcon from '../commom/baticon'
import BowlIcon from '../commom/bowlicon'
const Playerfallback = '/pngsV2/playerph.png'

const TabData = [
  { name: 'OVERALL', value: 'overall' },
  { name: 'BATTING', value: 'batting' },
  { name: 'BOWLING', value: 'bowling' },
]

export default function PlayerIndex({ browser, ...props }) {
  const [currentTab, setCurrentTab] = useState(TabData[0])
  const { data, loading, error } = useQuery(POSTSERIES_PLAYER_IMPACT, {
    variables: {
      tourID: props?.tourID,
    },
  })

  // useEffect(() => {
  //   if (typeof window !== 'undefined') {
  //     window.scrollTo(0, 0)
  //   }
  // }, [])

  if (error)
    return (
      <div className="mt-5">
        <DataNotFound />
      </div>
    )
  if (loading)
    return (
      <div className="mt-5">
        <Loading />
      </div>
    )

  return (
    // <div className=" ">
    //   <div className="flex items-center justify-start p-3 fixed bg-basebg w-full top-0 z-10">
    //     <div
    //       className="p-3 bg-gray rounded-md "
    //       onClick={() => window.history.back()}
    //     >
    //       <img
    //         className=" flex items-center justify-center h-4 w-4 rotate-180"
    //         src="/svgsV2/RightSchevronWhite.svg"
    //         alt=""
    //       />
    //     </div>
    //     <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
    //       <div className="">Criclytics</div>
    //       <span className="text-[8px] -mt-2">TM</span>
    //     </div>{' '}
    //   </div>
    //   <div className="mt-14  mx-1 p-2 ">
    //     <div className="flex flex-column justify-start items-start ">
    //       <div className="flex text-lg white font-bold">
    //         Most Valuable Players
    //       </div>
    //       <div className="flex w-full text-sm white font-medium mt-1 leading-4 tracking-wide text-white/80">
    //         The batsmen who made short work of the hude target, the bowlers who
    //         acted as partnership-breakers - here's a list of players whose
    //         consistent performances lit up the series.
    //       </div>
    //     </div>
    //     <div className="bg-blue-8 w-12 h-1 mt-2"></div>
    //     {data && data.playerIndex && data.playerIndex.playerObject.length > 0 && (
    //       <div className="flex mt-3 mb-2 justify-between items-center font-semibold">
    //         <div className=" text-gray-2 uppercase  text-xs">Player</div>

    //         <div className="uppercase text-gray-2 text-xs">Rank</div>
    //       </div>
    //     )}

    //     {data &&
    //     data.playerIndex &&
    //     data.playerIndex.playerObject.length > 0 ? (
    //       <div className="">
    //         {data.playerIndex.playerObject.map((player, i) => (
    //           <>
    //             <div
    //               key={i}
    //               className={`flex  mb-[2px] p-2  justify-between items-center ${
    //                 i < 3 ? 'bg-gray rounded' : 'border-gray border-b'
    //               } `}
    //             >
    //               <div className="flex justify-start items-center">
    //                 <div className=" relative bg-basebg  rounded-full">
    //                   <img
    //                     className="h-12 w-12  object-top object-cover rounded-full "
    //                     src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
    //                     onError={(evt) =>
    //                       (evt.target.src = '/pngsV2/playerph.png')
    //                     }
    //                   />
    //                 </div>
    //                 <div className=" ml-2">
    //                   <div className="flex items-center  justify-start text-white  text-sm font-semibold">
    //                     <div className=" ">{player.playerName} </div>
    //                     <div className="font-medium text-xs pl-2 text-white/80">
    //                       ({player.playerTeamName})
    //                     </div>
    //                   </div>
    //                   <div className="font-medium  text-white text-xs mt-1">
    //                     {player.totalPoints} Points{' '}
    //                   </div>
    //                 </div>
    //               </div>

    //               <div
    //                 className={`  ${
    //                   i < 3 ? 'bg-basebg' : 'bg-gray'
    //                 }  rounded flex justify-center items-center text-white w-11 h-10 font-semibold`}
    //               >
    //                 {' '}
    //                 {i + 1}{' '}
    //               </div>
    //             </div>
    //           </>
    //         ))}
    //       </div>
    //     ) : (
    //       <div className=" flex  mt-5">
    //         <DataNotFound />
    //       </div>
    //     )}
    //   </div>
    // </div>
    <div className="">
      <div className="lg:hidden md:hidden xl:hidden">
        <div className="p-2 fixed items-center bg-basebg w-full justify-start flex mb-2">
          <div
            className="p-3 bg-gray rounded-md "
            // onClick={() => router.back()}
            onClick={() => window.history.back()}
          >
            <img
              className=" flex items-center justify-center h-4 w-4 rotate-180"
              src="/svgsV2/RightSchevronWhite.svg"
              alt=""
            />
          </div>
          <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
            <div className="">Criclytics</div>
            <span className="text-[8px] -mt-2">TM</span>
          </div>
          <div className="flex  text-right bg-green"></div>
        </div>

        <div className="md:hidden lg:hidden xl:hidden p-3 my-0 flex items-center -mt-1">
          <span className="font-semibold md:hidden lg:hidden xl:hidden"></span>
        </div>
      </div>
      <div className="hidden lg:block md:block">
        <Heading
          heading={'Most Valuable Player'}
          subHeading={
            'An Overview of individual player impact during the course of the tournament'
          }
        />
      </div>
      <div className="md:hidden mt-6 mx-2">
        <Heading
          heading={'Most Valuable Player'}
          subHeading={
            'An Overview of individual player impact during the course of the tournament'
          }
        />
      </div>

      <div className="justify-between lg:flex md:flex xl:flex hidden">
        <div className="lg:w-6/12 md:w-6/12 xl:w-6/12 dark:mx-6 dark:flex dark:itmes-center dark:justify-center mt-4 dark:mt-8">
          <Tab
            type="block"
            data={TabData}
            handleTabChange={(item) => setCurrentTab(item)}
            selectedTab={currentTab}
          />
        </div>
        <div className="flex gap-3 mt-8">
          <BatIcon clr={'#5F92AC'} height={20} />
          <span className="dark:text-xs text-sm dark:text-gray-3 text-black">
            Batting Impact
          </span>
          <BowlIcon clr={'#5F92AC'} height={16} />
          <span className="dark:text-xs text-sm dark:text-gray-3 text-black">
            Bowling Impact
          </span>
        </div>
      </div>
      <div className="lg:w-6/12 md:w-6/12 lg:hidden md:hidden xl:hidden xl:w-6/12 dark:mx-6 dark:flex dark:itmes-center dark:justify-center mt-4 dark:mt-2">
        <Tab
          type="block"
          data={TabData}
          handleTabChange={(item) => setCurrentTab(item)}
          selectedTab={currentTab}
        />
      </div>
      <div className="w-full dark:px-3">
        <div className="flex md:py-1 md:mt-4 mt-2 bg-gray-4 shadow-4 justify-around dark:justify-around items-center rounded-t-xl text-xs">
          <div
            className={`flex 
            dark:w-2/12 ${
              currentTab?.value === 'overall' ? 'w-4/12' : 'w-1/12'
            }`}
          >
            <h2 className="dark:text-xs text-sm mx-2 font-semibold dark:text-gray-3 text-white">
              Rank
            </h2>
          </div>

          <div
            className={`flex items-center dark:text-gray-3 text-white ${
              currentTab?.value === 'overall'
                ? 'w-4/12 dark:w-4/12'
                : 'w-2/12 -ml-4'
            } py-2 pl-3 rounded-sm justify-end text-center dark:text-xs text-sm font-semibold`}
          >
            Player
          </div>
          {currentTab?.value === 'overall' && (
            <div className="flex items-center w-11/12 dark:w-11/12 text-gray-3 py-2 rounded-sm justify-end text-center text-xs font-semibold">
              <BatIcon clr={'#5F92AC'} />
            </div>
          )}
          {currentTab?.value === 'overall' && (
            <div className="flex items-center w-10/12 dark:w-3/12 text-gray-3 py-2 md:px-2 px-1 md:ml-0 rounded-sm justify-end text-center text-xs font-semibold">
              <BowlIcon clr={'#5F92AC'} />
            </div>
          )}

          <div className="flex items-center md:w-11/12 w-6/12 dark:text-gray-3 text-white text-sm py-2 md:px-4 px-2 rounded-sm justify-end text-center dark:text-xs font-semibold">
            {currentTab?.value === 'overall'
              ? 'Net Impact'
              : currentTab?.value === 'batting'
              ? 'Batting Impact'
              : 'Bowling Impact'}
          </div>
        </div>
        {currentTab?.value === 'overall' ? (
          <div className="bg-white dark:bg-gray">
            {data?.getPostSeriesImpact?.topTotalImpact?.map((item, index) => {
              return (
                <div className="">
                  <div className="flex md:py-1 py-0.5 text-xs font-medium justify-between items-center dark:bg-gray px-1 text-gray-2">
                    <div className="flex pl-1 md:w-2/12 dark:w-1/12 items-center break-words">
                      {index + 1}
                    </div>

                    <div className="flex items-center w-full">
                      <div className="">
                        {/* <img
                          className="absolute h-5 w-8 rounded-sm border-solid ml-16 my-20"
                          // src={`https://images.cricket.com/teams/${}_flag_safari.png`}
                          src="/svgs/whitebat.svg"
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                        <img
                          className="bg-gray-8 rounded-full lg:w-32 md:w-32 w-16 h-24 pt-2 px-2"
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          alt={Playerfallback}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                        <img
                          class=" absolute h-5 w-5 right-0 bottom-0 object-fill rounded border-2"
                          src={`https://images.cricket.com/teams/${item.playerTeam}_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/flag_dark.png')
                          }
                        /> */}
                      </div>
                      <div class="flex relative items-center">
                        <img
                          class="dark:h-12 dark:w-12 h-16 w-20 bg-gray-4 object-cover object-top rounded-full p-1"
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/MOM2.png')
                          }
                        />
                        {item?.role === 'Batsman' ? (
                          <img
                            src="/svgs/whitebat.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 bg-basebg absolute md:h-5 h-4 w-4 md:w-5 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : item?.role === 'Bowler' ? (
                          <img
                            src="/svgs/bowling.svg"
                            height="10px"
                            width="13px"
                            className="ml-1.5 mt-1 absolute bg-basebg md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : item?.role === 'Keeper' ? (
                          <img
                            src="/svgs/wicketKeeper.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 absolute bg-basebg md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : (
                          <img
                            src="/svgs/allRounderWhite.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 absolute bg-basebg md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        )}
                      </div>
                      <div className="flex flex-col w-5/12 dark: mx-1 truncate dark:flex-1">
                        <span className="text-xs dark:text-[10px] dark:text-white text-black">
                          {item.playerName}
                        </span>
                        <p className="dark:text-gray-3 text-[#4F93AF]">
                          {item?.playerTeam}
                        </p>
                      </div>
                      <div className="flex text-red truncate dark:text-[10px] dark:w-2/12 w-5/12 mx-2 dark:mx-1">
                        <span
                          className={`${
                            item.batting_impact > 0
                              ? 'text-green-6'
                              : 'text-red-4'
                          }`}
                        >
                          {item.batting_impact}
                        </span>
                      </div>
                      <div className="flex truncate lg:w-5/12 xl:w-5/12 md:w-5/12 dark:w-2/12">
                        <span
                          className={`${
                            item.bowling_impact > 0
                              ? 'text-green-6'
                              : 'text-red-4'
                          }`}
                        >
                          {item.bowling_impact}
                        </span>
                      </div>
                    </div>

                    <div
                      className={`flex items-center md:w-20 justify-center text-xs w-2/12 dark:mr-1`}
                    >
                      <span
                        className={`${
                          item.impactScore > 0 ? 'text-green-6' : 'text-red-4'
                        }`}
                      >
                        {' '}
                        {item.impactScore}
                      </span>
                    </div>
                  </div>
                  <div className="dark:bg-gray-4 bg-gray-11 h-[1px]" />
                </div>
              )
            })}
            <div className="flex gap-3 md:hidden py-3">
              <BatIcon clr={'#5F92AC'} height={20} className={'ml-1'} />
              <span className="text-xs">Batting Impact</span>
              <BowlIcon clr={'#5F92AC'} height={16} />
              <span className="text-xs">Bowling Impact</span>
            </div>
          </div>
        ) : currentTab?.value === 'batting' ? (
          <div className="bg-white dark:bg-gray">
            {data?.getPostSeriesImpact?.topBattingImpact?.map((item, index) => {
              return (
                <div className="">
                  <div className="flex py-1 text-xs font-medium justify-between items-center dark:bg-gray px-1 text-gray-2">
                    <div className="flex w-2/12 pl-1 items-center break-words">
                      {index + 1}
                    </div>

                    <div className="flex items-center dark:w-6/12 w-10/12">
                      {/* <div className="w-20 h-20 rounded-full overflow-hidden m-4 object-cover object-top">
                        <img
                          className="absolute h-5 w-8 rounded-sm border-solid ml-16 my-20"
                          // src={`https://images.cricket.com/teams/${}_flag_safari.png`}
                          src="/svgs/whitebat.svg"
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                        <img
                          className="bg-gray-8 rounded-full  lg:w-32 md:w-32 pt-2 px-2 h-32"
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          alt={Playerfallback}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                      </div> */}

                      <div class="flex relative items-center">
                        <img
                          class="dark:h-12 dark:w-12 h-16 w-14 bg-gray-4 object-cover object-top rounded-full p-1"
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/MOM2.png')
                          }
                        />
                        {item?.role === 'Batsman' ? (
                          <img
                            src="/svgs/whitebat.svg"
                            height="10px"
                            width="16px"
                            className="mr-1 mt-0.5 bg-basebg absolute h-4 w-4 md:w-5 md:h-5 right-0 bottom-0 object-fill md:border rounded"
                          />
                        ) : item?.role === 'Bowler' ? (
                          <img
                            src="/svgs/bowling.svg"
                            height="10px"
                            width="13px"
                            className="mr-1 mt-0.5 bg-basebg absolute h-4 w-4 md:h-5 md:w-5 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : item?.role === 'Keeper' ? (
                          <img
                            src="/svgs/wicketKeeper.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 absolute bg-basebg md:h-5 md:w-5 h-4 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : (
                          <img
                            src="/svgs/allRounderWhite.svg"
                            height="10px"
                            width="16px"
                            className="mr-1 mt-0.5 bg-basebg absolute md:h-5 md:w-5 h-4 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        )}
                      </div>
                      <div className="flex flex-col truncate mx-1">
                        <span className="truncate dark:text-white text-xs text-black">
                          {item.playerName}
                        </span>
                        <p className="dark:text-gray-3 text-[#4F93AF]">
                          {item?.playerTeam}
                        </p>
                      </div>
                    </div>
                    <div
                      className={`flex items-center md:w-20 justify-center text-xs dark:mr-1 w-2/12 `}
                    >
                      <span
                        className={`${
                          item.impactScore > 0 ? 'text-green-6' : 'text-red-4'
                        }`}
                      >
                        {' '}
                        {item.impactScore}
                      </span>
                    </div>
                  </div>
                  <div className="dark:bg-gray-4 bg-gray-11 h-[1px]" />
                </div>
              )
            })}
          </div>
        ) : (
          <div className="bg-white dark:bg-gray">
            {data?.getPostSeriesImpact?.topBowlingImpact?.map((item, index) => {
              return (
                <div className="">
                  <div className="flex py-1 text-xs  font-medium justify-between items-center dark:bg-gray px-1 text-gray-2">
                    <div className="flex w-2/12 pl-1 items-center break-words">
                      {index + 1}
                    </div>

                    <div className="flex items-center w-10/12 dark:w-6/12">
                      {/* <div className="w-20 h-20 rounded-full overflow-hidden m-4 object-cover object-top">
                        <img
                          className="absolute h-5 w-8 rounded-sm border-solid ml-16 my-20"
                          // src={`https://images.cricket.com/teams/${}_flag_safari.png`}
                          src="/svgs/whitebat.svg"
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                        <img
                          className="bg-gray-8 rounded-full lg:w-32 md:w-32 pt-2 px-2 h-24"
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          alt={Playerfallback}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                      </div> */}
                      <div class="flex relative items-center">
                        <img
                          class="w-14 dark:h-12 dark:w-12 h-16 bg-gray-4 object-cover object-top rounded-full p-1"
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/MOM2.png')
                          }
                        />
                        {item?.role === 'Batsman' ? (
                          <img
                            src="/svgs/whitebat.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 bg-basebg absolute md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : item?.role === 'Bowler' ? (
                          <img
                            src="/svgs/bowling.svg"
                            height="10px"
                            width="13px"
                            className="ml-1.5 mt-1 absolute bg-basebg md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : item?.role === 'Keeper' ? (
                          <img
                            src="/svgs/wicketKeeper.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 absolute bg-basebg md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        ) : (
                          <img
                            src="/svgs/allRounderWhite.svg"
                            height="10px"
                            width="16px"
                            className="ml-1.5 mt-0.5 absolute bg-basebg md:h-5 h-4 md:w-5 w-4 right-0 bottom-0 object-fill rounded md:border"
                          />
                        )}
                      </div>
                      <div className="flex flex-col truncate mx-1">
                        <span className="truncate text-black text-xs dark:text-white">
                          {item.playerName}
                        </span>
                        <p className="dark:text-gray-3 text-[#4F93AF]">
                          {item?.playerTeam}
                        </p>
                      </div>
                    </div>
                    <div
                      className={`flex items-center md:w-20 justify-center text-xs w-2/12 dark:mr-1`}
                    >
                      <span
                        className={`${
                          item.impactScore > 0 ? 'text-green-6' : 'text-red-4'
                        }`}
                      >
                        {' '}
                        {item.impactScore}
                      </span>
                    </div>
                  </div>
                  <div className="dark:bg-gray-4 bg-gray-11 h-[1px]" />
                </div>
              )
            })}
          </div>
        )}
      </div>
    </div>
  )
}
