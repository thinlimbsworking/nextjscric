import React, { useState, useEffect } from 'react'
// import Svg, {Defs, G, Image, LinearGradient, Path, Stop, Text, TextPath} from "react-native-svg";
import * as scale from 'd3-scale'
import * as shape from 'd3-shape'
const ground = '/svgs/groundImageWicket.png'

const d3 = {
  scale,
  shape,
}
import { useQuery } from '@apollo/react-hooks'
const information = '/svgs/information.svg'

import { GET_SESSION_WINNER } from '../../api/queries'

export default function PieChart({ browser, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false)
  const [windowObject, updateWindowObject] = useState(null)
  const [firstSelect, SetFirstSelect] = useState(false)

  const [activePieIndex, SetactivePieIndex] = useState(0)
  const [activeSession, SetactiveSession] = useState(0)

  const [firstindex, SetfirstIndex] = useState(0)
  useEffect(() => {
    updateWindowObject({ ...global.window })
  }, [firstindex])
  const { loading, error, data } = useQuery(GET_SESSION_WINNER, {
    variables: { matchID: props.matchData[0].matchID },
    onCompleted: (data) => {
      if (
        data &&
        data.getSessionWinner &&
        data.getSessionWinner.data &&
        data.getSessionWinner.data.length
      ) {
        for (let i = 0; i < data.getSessionWinner.data.length; i++) {
          let res = data.getSessionWinner.data[i].sessions.filter(
            (o) => o.session,
          )[0]
          if (res) {
            SetactiveSession(res.session - 1)
            SetactivePieIndex(data.getSessionWinner.data.length - 1)
            SetFirstSelect(data.getSessionWinner.data.length - 1)

            break
          }
        }
      }
    },
  })

  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }

  if (
    error ||
    (!loading &&
      (!data ||
        !data.getSessionWinner ||
        !data.getSessionWinner.data ||
        data.getSessionWinner.data.length == 0))
  )
    return (
      <div className="mt2 pv2 shadow-4 bg-white">
        <img
          className="w45-m h45-m w4 h4 w5-l h5-l"
          style={{ margin: 'auto', display: 'block' }}
          src={ground}
          alt="loading..."
        />
        <div className="tc pt2 fw5">Data Not Available</div>
      </div>
    )
  else if (loading) return <div></div>
  else {
    const scr =
      data.getSessionWinner.data[activePieIndex].sessions[activeSession].scores
    return (
      <div className={`bg-white mt2 mw75-l center shadow-4`}>
        {/* <div className={`f6 fw5 pa2`}>PHASES OF PLAY</div> */}
        <div className="flex justify-between items-center pa2 bg-white">
          <h1 className="f7 f5-l fw5 grey_10 ttu">Phases of play</h1>
          <img
            src={information}
            onClick={() => toggleInfo()}
            alt=""
            className="h1 w1"
          />
        </div>
        {infoToggle && (
          <div>
            <div className="f7 f6-l grey_8 pa2 bg-white">
              Win the sessions to win the match, they say. Find out who won
              which session, at just a glance.
            </div>
            <div className="outline light-gray" />
          </div>
        )}
        <div className="flex-l">
          <div className="w-50-l">
            <div className={`pb2`}>
              <div className="divider" />
              <div className={`f6 fw5 bg-near-white`}>
                <div className="pa2">SESSION WON</div>
              </div>
              <div className="divider" />
            </div>
            {/* ---------------------------------------------TEAMS FLAG START------------------------------------------------------------------ */}
            {/* ------------------TEAMA----------------------- */}
            <div className="ph3 flex justify-between items-center ">
              <div className="flex items-center w-30">
                <div className="mr2 pt2">
                  <img
                    alt=""
                    className="w2 h13 shadow-4"
                    src={`https://images.cricket.com/teams/${data.getSessionWinner.teamAId}_flag.png`}
                  />
                  <div className={`f6 fw5 pb1 tc `}>
                    {data.getSessionWinner.teamA}
                  </div>
                </div>
                <div
                  className="shadow-4  flex items-center justify-center"
                  style={{
                    height: 42,
                    marginTop: 0,
                    width: 58,
                    borderBottomColor: '#DEB465',
                    borderBottomWidth: 7,
                  }}
                >
                  <p className="f4 fw5 darkRed">
                    {data.getSessionWinner.teamAWon}
                  </p>
                </div>
              </div>
              {/*--------------------TEAMB-------------------*/}
              <div className="flex items-center w-30">
                <div className="mr2 pt2">
                  <img
                    alt=""
                    className="w2 h13 shadow-4"
                    src={`https://images.cricket.com/teams/${data.getSessionWinner.teamBId}_flag.png`}
                  />
                  <div className={`f6 fw5 pb1 tc `}>
                    {data.getSessionWinner.teamB}
                  </div>
                </div>
                <div
                  className="shadow-4  flex items-center justify-center"
                  style={{
                    height: 42,
                    marginTop: 0,
                    width: 58,
                    borderBottomColor: '#917645',
                    borderBottomWidth: 7,
                  }}
                >
                  <p className="f4 fw5 darkRed">
                    {data.getSessionWinner.teamBWon}
                  </p>
                </div>
              </div>
              {/* SHARED */}
              <div className="flex items-center w-30">
                <div className="mr2 pt1">
                  <div className={`f6 fw6 pv1 tc `}>Shared</div>
                </div>
                <div
                  className="shadow-4  flex items-center justify-center"
                  style={{
                    height: 42,
                    marginTop: 0,
                    width: 58,
                    borderBottomColor: '#5c5c5c',
                    borderBottomWidth: 7,
                  }}
                >
                  <p className="f4 fw5 darkRed">
                    {data.getSessionWinner.shared}
                  </p>
                </div>
              </div>
            </div>
            {/* -------------------------------------------TEAMS FLAG END-------------------------------------------------------------------- */}

            <div className="flex    justify-center items-center mt3">
              <div className="w-20 "></div>
              <div className="flex justify-center   w-100 ">
                {[0, 1, 2].map((session, i) => (
                  <div
                    key={i}
                    className={` w-33 fw6 f8 f6-l flex items-center justify-center pv1  ph1  bg-moon-gray br2 mh2  `}
                  >
                    SESSION {i + 1}
                  </div>
                ))}
              </div>
            </div>

            <div className="flex flex-column   pb2 w-100  ">
              {data &&
                data.getSessionWinner &&
                data.getSessionWinner.data &&
                data.getSessionWinner.data.map((day, i) => (
                  <div
                    key={i}
                    className="flex relative   items-center justify-center mt2 cursor-pointer"
                  >
                    {/* {props &&props.matchData && props.matchData[0].currentDay) } */}
                    {/* {i===activePieIndex ? <div className="dn db-l absolute left-0 rightArr"></div>:null} */}

                    <div className="flex   justify-center items-center w-100 ">
                      {day &&
                        day.sessions &&
                        day.sessions.map((session, j) => (
                          <>
                            {j == 0 && (
                              <div
                                className={
                                  firstSelect
                                    ? ` ${
                                        i === activePieIndex
                                          ? 'darkRed'
                                          : 'black'
                                      } black f7 f6-l cursor-pointer fw5 tc w-20 `
                                    : ` ${
                                        activePieIndex == i
                                          ? 'darkRed'
                                          : 'black '
                                      }  black f7 f6-l cursor-pointer fw5 tc w-20`
                                }
                                onClick={
                                  session.teamId != null && session.teamId != ''
                                    ? () => {
                                        SetactivePieIndex(i),
                                          SetactiveSession(0)
                                      }
                                    : null
                                }
                              >
                                DAY {i + 1}
                              </div>
                            )}

                            <div
                              key={j}
                              onClick={
                                session.teamId != null && session.teamId != ''
                                  ? () => {
                                      SetFirstSelect(true),
                                        SetactivePieIndex(i),
                                        SetactiveSession(j)
                                    }
                                  : null
                              }
                              className={` w-33 h2 ${
                                session.teamWon === 'Draw'
                                  ? 'bg-moon-gray'
                                  : 'bg-white'
                              }
                           ${
                             firstSelect
                               ? i === activePieIndex && j == activeSession
                                 ? 'ba b--red_10 bw1  '
                                 : ''
                               : activePieIndex === i && j == 0
                               ? 'ba b--red_10 bw1'
                               : ''
                           }  flex   shadow-4 justify-center items-center  br2 mh2 fw6 f6-l f8 tc`}
                            >
                              {session.teamId != 'None' &&
                              session.teamId !== null &&
                              session.teamId !== '' &&
                              session.teamWon !== '' ? (
                                <img
                                  alt=""
                                  className="w2   h13 shadow-4 "
                                  src={`https://images.cricket.com/teams/${session.teamId}_flag.png`}
                                />
                              ) : (
                                ''
                              )}
                            </div>

                            {/* <div key={j} 
                
                onClick={()=>{SetFirstSelect(true),SetactivePieIndex(i),SetactiveSession(j)}}
                className={`dn db-l w-33 h2 ${(i+1 === props.matchData[0].currentDay) && props.matchData[0].matchStatus==="live" && session.teamId==="None"?'bg-moon-gray':'bg-white'} ${firstSelect? (i===activePieIndex && j==activeSession)?' ':'':props.matchData[0].matchScore.length-1===i &&j==0  ?'':'' }  flex   shadow-4 justify-center items-center  br2 mh2 tc`}>
                {(session.teamId!="None" && session.teamId!==null) && <img
                    alt=''
                    className='w2  pa1  h13 '
                    src={`https://images.cricket.com/teams/${session.teamId}_flag.png`}
                  />}
                </div> */}
                          </>
                        ))}
                    </div>
                  </div>
                ))}
            </div>
          </div>
          <div className="bl b--moon-gray dn db-l"></div>
          {/* -------------------------------------------SESSION DATA ON DISPLAY BELOW PIECHART---------------------------------------------------------------------------------------- */}
          <div className="w-50-l dn-l  mt0-l mt3 ">
            {/* <div className={`f5 fw5 flex justify-end mr2 mb2 dn-l`}>{`Day - ${activePieIndex + 1}`}</div> */}
            <div className="divider" />
            <div className="flex justify-between pa2 bg-near-white">
              <div className={`f6 fw5`}>{`DAY ${
                firstSelect ? activePieIndex + 1 : firstindex + 1
              } SESSION ${activeSession + 1}`}</div>
              <div
                className={`f6 fw5`}
              >{`SESSION : ${data.getSessionWinner.data[activePieIndex].sessions[activeSession].teamWon}`}</div>
            </div>
            <div className="divider" />
            <div className="flex justify-between ph2">
              <p className={` f6 fw5`}>Start of Session</p>
              <p className={` f6 fw5`}>End of Session</p>
            </div>
            {data.getSessionWinner.data[
              firstSelect ? activePieIndex : firstindex
            ].sessions[activeSession].scores.map(
              (score, i) =>
                ((scr[0].inningNo === scr[1].inningNo && i === 0) ||
                  scr[0].inningNo !== scr[1].inningNo) && (
                  <div key={i} className="flex justify-between pa2 ph3 ">
                    <div className="w-25">
                      <div className={`f5 fw6 pt2`}>{`${
                        scr[0].inningNo === scr[1].inningNo
                          ? scr[0].startScore
                          : scr[i].startScore
                      }`}</div>
                    </div>
                    <div className="w-50">
                      <div className="flex justify-between items-center pv2">
                        <div
                          className="bg-green_10 mr1"
                          style={{
                            height: 5,
                            width: 5,
                            borderRadius: 3,
                          }}
                        />
                        {Array.apply(null, { length: 15 })
                          .map(Number.call, Number)
                          .map((x) => (
                            <div
                              key={x}
                              className="bg-grey_5"
                              style={{
                                width: 6,
                                height: 1,
                                borderRadius: 2,
                                marginRight: 1,
                              }}
                            />
                          ))}
                        <div
                          className="bg-red_10 ml1"
                          style={{
                            height: 5,
                            width: 5,
                            borderRadius: 3,
                          }}
                        />
                      </div>
                      <div className={`flex justify-center items-center `}>
                        <img
                          alt=""
                          className="w2 h13 shadow-4"
                          style={{ marginTop: '-1.3rem' }}
                          src={`https://images.cricket.com/teams/${
                            scr[0].inningNo === scr[1].inningNo
                              ? scr[0].teamId
                              : scr[i].teamId
                          }_flag.png`}
                        />
                      </div>
                      <div className={`f6 fw5 flex justify-center pt1 `}>
                        {scr[0].inningNo === scr[1].inningNo
                          ? scr[0].teamName
                          : scr[i].teamName}
                      </div>
                    </div>
                    <div className="w-25 tr">
                      <div className={`f5 fw6 pt2`}>{`${
                        scr[0].inningNo === scr[1].inningNo
                          ? scr[1].endScore
                          : scr[i].endScore
                      }`}</div>
                    </div>
                  </div>
                ),
            )}
            {/* score[0].endScore !== "" ? score[0].endScore: score[1].endScore */}
          </div>
          {/* desktop */}
          <div className="w-50-l db-l dn ">
            {data.getSessionWinner.data[
              firstSelect ? activePieIndex : firstindex
            ].sessions.map(
              (x, i) =>
                x.day && (
                  <div>
                    <div
                      className={`f5 fw5 flex justify-end mr2 mb2 dn-l`}
                    >{`Day - ${activePieIndex + 1}`}</div>
                    <div className="divider" />
                    <div className="flex justify-between pa2 bg-near-white">
                      <div className={`f6 fw5`}>
                        DAY {firstSelect ? activePieIndex + 1 : firstindex + 1}{' '}
                        SESSION {i + 1}
                      </div>
                      <div className={`f6 fw5`}>{`SESSION : ${x.teamWon}`}</div>
                    </div>
                    <div className="divider" />
                    <div className="flex justify-between ph2">
                      <p className={` f6 fw5`}>Start of Session</p>
                      <p className={` f6 fw5`}>End of Session</p>
                    </div>
                    {x.scores.map(
                      (score, j) =>
                        ((x.scores[0].inningNo === x.scores[1].inningNo &&
                          j === 0) ||
                          x.scores[0].inningNo !== x.scores[1].inningNo) && (
                          <div key={j} className="flex justify-between pa2 ph3">
                            <div className="w-25">
                              <div className={`f5 fw6 pt2`}>{`${
                                x.scores[0].inningNo === x.scores[1].inningNo
                                  ? x.scores[0].startScore
                                  : score.startScore
                              }`}</div>
                            </div>
                            <div className="w-50">
                              <div className="flex justify-between items-center pv2">
                                <div
                                  className="bg-green_10 mr1"
                                  style={{
                                    height: 5,
                                    width: 5,
                                    borderRadius: 3,
                                  }}
                                />
                                {Array.apply(null, { length: 15 })
                                  .map(Number.call, Number)
                                  .map((x) => (
                                    <div
                                      key={x}
                                      className="bg-grey_5"
                                      style={{
                                        width: 6,
                                        height: 1,
                                        borderRadius: 2,
                                        marginRight: 1,
                                      }}
                                    />
                                  ))}
                                <div
                                  className="bg-red_10 ml1"
                                  style={{
                                    height: 5,
                                    width: 5,
                                    borderRadius: 3,
                                  }}
                                />
                              </div>
                              <div
                                className={`flex justify-center items-center `}
                              >
                                <img
                                  alt=""
                                  className="w2 h13 shadow-4"
                                  style={{ marginTop: '-1.3rem' }}
                                  src={`https://images.cricket.com/teams/${
                                    x.scores[0].inningNo ===
                                    x.scores[1].inningNo
                                      ? x.scores[1].teamId
                                      : score.teamId
                                  }_flag.png`}
                                />
                              </div>
                              <div
                                className={`f6 fw5 flex justify-center pt1 `}
                              >
                                {x.scores[0].inningNo === x.scores[1].inningNo
                                  ? x.scores[1].teamName
                                  : score.teamName}
                              </div>
                            </div>
                            <div className="w-25 tr">
                              <div className={`f5 fw6 pt2`}>{`${
                                x.scores[0].inningNo === x.scores[1].inningNo
                                  ? x.scores[1].endScore
                                  : score.endScore
                              }`}</div>
                            </div>
                          </div>
                        ),
                    )}
                  </div>
                ),
            )}
          </div>
        </div>
      </div>
    )
  }
}
