import React, { useState, useEffect } from 'react'
// import ReactSpeedometer from 'react-d3-speedometer';
import { PRE_MATCH_PREDICTION } from '../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import Loading from '../../components/loading'

import CleverTap from 'clevertap-react'
// import moment from 'moment'
const BatIcon = '/svgs/batIcon.svg'
const ballIcon = '/svgs/ballIcon.svg'
const matchupPlayer1 = '/pngs/fallbackprojection.png'
const batsmen = '/pngs/bat_icon.png'
const bowler = '/pngs/ball_icon.png'
const allrounderIcon = '/pngs/allrounder_icon.png'
const ground = '/svgs/groundImageWicket.png '
const information = '/svgs/information.svg'

const PlayerProjection = ({ matchData, matchID, browser, ...props }) => {
  const [activeTab, setActiveTab] = useState('')
  const [indexA, setIndexA] = useState(0)
  const [statsTab, setStatsTab] = useState('battingRecord')

  const [indexB, setIndexB] = useState(0)
  const [playerA, setPlayerA] = useState({})
  const [playerB, setPlayerB] = useState({})
  const [maxValueA, setMaxValueA] = useState(0)
  const [maxValueB, setMaxValueB] = useState(0)
  const [minValue, setMinValue] = useState(0)
  const [infoToggle, setInfoToggle] = useState(false)

  const { loading, error, data } = useQuery(PRE_MATCH_PREDICTION, {
    variables: { matchId: matchData[0].matchID },
    onCompleted: (data) => {
      // variables: { matchId: "190962" }, onCompleted: (data) => {
      if (data && data.preMatchPredection) {
        setActiveTab(data.preMatchPredection.teams_Runs_Projection[0].teamId)
        if (data.preMatchPredection.playerList !== 0) {
          const playerListA = data.preMatchPredection.playerList.filter(
            (id) =>
              id.teamId ===
              data.preMatchPredection.teams_Runs_Projection[0].teamId,
          )
          // setMaxValueA(playerList[0], 0);
          getPieValA(playerListA[0], 0)
          const playerListB = data.preMatchPredection.playerList.filter(
            (id) =>
              id.teamId ===
              data.preMatchPredection.teams_Runs_Projection[1].teamId,
          )

          getPieValB(playerListB[0], 0)
        }
      }
    },
  })

  const getPieValA = (player, i, tab = '') => {
    setIndexA(i)
    setPlayerA(player)
    if (typeof player !== 'undefined') {
      let values =
        player.playerRole === 'All Rounder' && tab == 'bowlingRecord'
          ? player.listProjections[1].values[1].bound
          : player.listProjections[0].values[1].bound

      if (
        player.playerRole === 'Bowler' ||
        (player.playerRole === 'All Rounder' && tab == 'bowlingRecord')
      ) {
        if (Number(values) < 10) {
          if (Number(values) <= 3) {
            setMaxValueA(3)
          } else if (Number(values) > 3 && Number(values) <= 6) {
            setMaxValueA(6)
          } else if (Number(values) > 6 && Number(values) < 10) {
            setMaxValueA(15)
          }
        }
      } else {
        const dividedVal = Number(values) / 3

        let pieValue = (parseInt(dividedVal / 10, 10) + 1) * 10 * 3

        if (pieValue == 30) {
          setMaxValueA(30)
        } else if (pieValue === 60) {
          setMaxValueA(60)
        }
        if (pieValue > 60 && pieValue <= 150) {
          setMaxValueA(150)
        }
        if (pieValue > 150 && pieValue <= 300) {
          setMaxValueA(300)
        }
        if (pieValue > 300 && pieValue <= 450) {
          setMaxValueA(450)
        }
      }
    }
  }
  const getPieValB = (player, i, tab = '') => {
    setIndexB(i)
    setPlayerB(player)
    if (typeof player !== 'undefined') {
      let values =
        player.playerRole === 'All Rounder' && tab == 'bowlingRecord'
          ? player.listProjections[1].values[1].bound
          : player.listProjections[0].values[1].bound

      if (
        player.playerRole === 'Bowler' ||
        (player.playerRole === 'All Rounder' && tab == 'bowlingRecord')
      ) {
        if (Number(values) < 10) {
          if (Number(values) <= 3) {
            setMaxValueB(3)
          } else if (Number(values) > 3 && Number(values) <= 6) {
            setMaxValueB(6)
          } else if (Number(values) > 6 && Number(values) < 10) {
            setMaxValueB(15)
          }
        }
      } else {
        const dividedVal = Number(values) / 3

        //8.266
        let pieValue = (parseInt(dividedVal / 10, 10) + 1) * 10 * 3

        // 30
        if (pieValue == 30) {
          setMaxValueB(30)
        } else if (pieValue === 60) {
          setMaxValueB(60)
        }
        if (pieValue > 60 && pieValue <= 150) {
          setMaxValueB(150)
        }
        if (pieValue > 150 && pieValue <= 300) {
          setMaxValueB(300)
        }
        if (pieValue > 300 && pieValue <= 450) {
          setMaxValueB(450)
        }
      }
    }
  }

  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }

  if (error)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center">
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  if (loading) return <Loading />
  else
    return (
      <div className="bg-white mv2 shadow">
        <div className="flex justify-between items-center pa2 ph3-l">
          <h1 className="f7 f5-l fw5 grey_10 ttu">player projection</h1>
          <img
            src={information}
            onClick={() => toggleInfo()}
            alt=""
            className="h1 w1"
          />
        </div>
        <div className="outline light-gray" />
        {infoToggle && (
          <div>
            <div className="f7 grey_8 pa2">
              Historic performance against opposition, pitch conditions, recent
              form, etc. help determine the projected score/wickets for the
              match.
            </div>
            <div className="outline light-gray" />
          </div>
        )}
        {data &&
        data.preMatchPredection &&
        data.preMatchPredection.playerList.length > 0 ? (
          <div>
            <div className="flex flex-auto bg-white tc f6 fw5 items-center ">
              <div
                className={`w-50 grey_10 pv2 bb2 md:cursor-pointer shadow-4 ${
                  activeTab ===
                  (data.preMatchPredection.teams_Runs_Projection &&
                    data.preMatchPredection.teams_Runs_Projection[0].teamId)
                    ? 'b--darkRed'
                    : 'b--white'
                }`}
                onClick={() => {
                  setActiveTab(
                    data.preMatchPredection.teams_Runs_Projection[0].teamId,
                  )
                  setStatsTab('battingRecord')
                  const playerListA = data.preMatchPredection.playerList.filter(
                    (id) =>
                      id.teamId ===
                      data.preMatchPredection.teams_Runs_Projection[0].teamId,
                  )
                  getPieValA(playerListA[0], 0)
                }}
              >
                {data.preMatchPredection.teams_Runs_Projection[0].teamName}
              </div>
              <div className="h1 br b--grey_5" />
              <div
                className={`w-50 grey_8 pv2 bb2 md:cursor-pointer shadow-4 ${
                  activeTab ===
                  data.preMatchPredection.teams_Runs_Projection[1].teamId
                    ? 'b--darkRed'
                    : 'b--white'
                }`}
                onClick={() => {
                  setActiveTab(
                    data.preMatchPredection.teams_Runs_Projection[1].teamId,
                  )
                  setStatsTab('battingRecord')
                  const playerListB = data.preMatchPredection.playerList.filter(
                    (id) =>
                      id.teamId ===
                      data.preMatchPredection.teams_Runs_Projection[1].teamId,
                  )
                  getPieValB(playerListB[0], 0)
                }}
              >
                {data.preMatchPredection.teams_Runs_Projection[1].teamName}
              </div>
            </div>
            <div className="divider" />
            {/* Player A */}
            {activeTab ===
            data.preMatchPredection.teams_Runs_Projection[0].teamId ? (
              <div>
                <div className="bg-white flex justify-center pa3">
                  <div className="f6 fw6 grey_10 flex items-center">
                    Predicted 1st Inn Score
                  </div>
                  <div className="flex pl4">
                    {data.preMatchPredection.teams_Runs_Projection[0].teamScore
                      .split('')
                      .map((digit, i) => (
                        <div
                          key={i}
                          className="flex  justify-center items-center w15 h2 bg-grey_10 relative br1 mr1"
                        >
                          <div
                            className="absolute left-0 right-0 h01 o-40 z-1"
                            style={{
                              top: '50%',
                            }}
                          />
                          <div className="white flex justify-center items-center f3 oswald">
                            {digit}
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
                <div className="divider" />
                <div className="bg-gradientGrey  pa3 f8 ttu grey_8 fw5">
                  {' '}
                  player Score Projector{' '}
                </div>
                <div className="divider" />

                {/* <div className="flex justify-end pt2 ph2">
                        <div className="flex items-center justify-between ph2 pv1 bg-black-20 br-pill md:cursor-pointer w-25" onClick={() => setStatsTab(statsTab === "battingRecord" ? "bowlingRecord" : "battingRecord")} >
                           <img className={`h1 w1 pa1 br-50 ${statsTab === 'battingRecord' ? 'cdcgr' : ''}`} alt={BatIcon} src={BatIcon} />
                           <img className={` h1 w1 pa1 br-50 ${statsTab === 'bowlingRecord' ? 'cdcgr' : ''}`} alt={ballIcon} src={ballIcon} />
                        </div>
                     </div> */}

                {playerA && playerA.listProjections && (
                  <div className="relative">
                    <div className="absolute left-0 bottom-0">
                      <img
                        src={`https://images.cricket.com/players/${playerA.playerId}_headshot_safari.png`}
                        alt=""
                        className="h44"
                        onError={(evt) => (evt.target.src = matchupPlayer1)}
                      />
                    </div>
                    <div className="flex justify-end mr2">
                      <div className="pv3 flex justify-between w-70">
                        {/* <div className="w-60 pl2"> */}
                        <div className="pl1">
                          <div className="flex items-center">
                            {/* <img className="w1 h1" src={playerA.playerRole === 'Batsman' ? `${batsmen}` : playerA.playerRole == "Bowler" ? `${bowler}` : `${allrounder}`} alt="" /> */}
                            <img
                              className="w1 h1"
                              src={
                                playerA.playerRole === 'Batsman'
                                  ? `${batsmen}`
                                  : playerA.playerRole === 'Bowler'
                                  ? `${bowler}`
                                  : allrounderIcon
                              }
                              alt=""
                            />
                            <div className="f6 fw6 ml1">
                              {playerA.playerName}
                            </div>
                          </div>
                          <div className="flex items-center mt1">
                            <div className="f7 grey_8">Projected</div>
                            <div
                              className="pv1 ph2 ml2 br4 f8 fw6 grey_10"
                              style={{ backgroundColor: '#E5F1E1' }}
                            >
                              {/* {playerA.playerRole === "Batsman" && playerA.listProjections && playerA.listProjections[0].values[0].bound === "0" && playerA.listProjections[0].values[2].bound === "0" ? "NA runs" : null}
                                       {playerA.playerRole === "Bowler" && playerA.listProjections && playerA.listProjections[0].values[0].bound === "0" && playerA.listProjections[0].values[2].bound === "0" ? "NA wickets" : null}
                                       {playerA.playerRole === "Batsman" && playerA.listProjections && (playerA.listProjections[0].values[0].bound !== "0" || playerA.listProjections[0].values[2].bound !== "0") ? `${playerA.listProjections[0].values[0].bound} - ${playerA.listProjections[0].values[2].bound} runs` : null}
                                       {playerA.playerRole === "Bowler" && playerA.listProjections && (playerA.listProjections[0].values[0].bound !== "0" || playerA.listProjections[0].values[2].bound !== "0") ? `${playerA.listProjections[0].values[0].bound} - ${playerA.listProjections[0].values[2].bound} wickets` : null}
                                       {playerA.playerRole === "All Rounder" && playerA.listProjections && (playerA.listProjections[0].values[0].bound !== "0" || playerA.listProjections[0].values[2].bound !== "0") ? `${playerA.listProjections[0].values[0].bound} - ${playerA.listProjections[0].values[2].bound} wickets` : null} */}
                              {(
                                playerA.playerRole == 'All Rounder' &&
                                statsTab == 'bowlingRecord'
                                  ? playerA.listProjections[1].values[2]
                                      .bound == 0
                                  : playerA.listProjections[0].values[2]
                                      .bound == 0
                              )
                                ? `0 ${
                                    (playerA.playerRole == 'All Rounder' &&
                                      statsTab == 'bowlingRecord') ||
                                    playerA.playerRole == 'Bowler'
                                      ? ' wicket'
                                      : ' run'
                                  }`
                                : (
                                    playerA.playerRole == 'All Rounder' &&
                                    statsTab == 'bowlingRecord'
                                      ? playerA.listProjections[1].values[2]
                                          .bound == -1
                                      : playerA.listProjections[0].values[2]
                                          .bound == -1
                                  )
                                ? `NA`
                                : `${
                                    playerA.playerRole == 'All Rounder' &&
                                    statsTab == 'bowlingRecord'
                                      ? playerA.listProjections[1].values[0]
                                          .bound
                                      : playerA.listProjections[0].values[0]
                                          .bound
                                  }-${
                                    playerA.playerRole == 'All Rounder' &&
                                    statsTab == 'bowlingRecord'
                                      ? playerA.listProjections[1].values[2]
                                          .bound
                                      : playerA.listProjections[0].values[2]
                                          .bound
                                  } ${
                                    (playerA.playerRole == 'All Rounder' &&
                                      statsTab == 'battingRecord') ||
                                    playerA.playerRole === 'Batsman'
                                      ? 'runs'
                                      : 'wickets'
                                  }`}
                            </div>
                          </div>
                        </div>
                        {/* ==== */}
                        {playerA.playerRole === 'All Rounder' && (
                          <div className="flex justify-end w-25 w-15-l">
                            <div
                              className="flex items-center justify-between ph2 bg-black-20 br-pill md:cursor-pointer w-100"
                              onClick={() => {
                                setStatsTab(
                                  statsTab === 'battingRecord'
                                    ? 'bowlingRecord'
                                    : 'battingRecord',
                                )
                                getPieValA(
                                  playerA,
                                  indexA,
                                  statsTab === 'battingRecord'
                                    ? 'bowlingRecord'
                                    : 'battingRecord',
                                )
                              }}
                            >
                              <img
                                className={`h1 w1 pa1 br-50 ${
                                  statsTab === 'battingRecord' ? 'cdcgr' : ''
                                }`}
                                alt={BatIcon}
                                src={BatIcon}
                              />
                              <img
                                className={` h1 w1 pa1 br-50 ${
                                  statsTab === 'bowlingRecord' ? 'cdcgr' : ''
                                }`}
                                alt={ballIcon}
                                src={ballIcon}
                              />
                            </div>
                          </div>
                        )}
                        {/* === */}
                      </div>
                    </div>
                    <div className="flex justify-end bg-grey_10">
                      <div className="items-center pr5-ns justify-center w-50 w-30-ns pt2 pt1-ns ">
                        <div className="ma2 overflow-hidden  h35 h4-l">
                          {/* <ReactSpeedometer
                            key={`${playerA.playerId}_${maxValueA}`}
                            // value={Number(playerA.listProjections[1].bound)}
                            value={
                              playerA.playerRole == 'All Rounder' && statsTab == 'bowlingRecord'
                                ? Number(playerA.listProjections[1].values[1].bound) == -1
                                  ? 0
                                  : Number(playerA.listProjections[1].values[1].bound)
                                : Number(playerA.listProjections[0].values[1].bound) == -1
                                ? 0
                                : Number(playerA.listProjections[0].values[1].bound)
                            }
                            maxValue={maxValueA}
                            minValue={0}
                            fluidWidth={true}
                            ringWidth={30}
                            maxSegmentLabels={3}
                            needleHeightRatio={0.9}
                            needleTransitionDuration={500}
                            needleColor='#fff'
                            startColor='#f5a623'
                            endColor='#a70e13'
                            segments={3}
                            segmentColors={['#f5a623', '#f76b1c', '#a70e13']}
                            textColor={'#dbdbdb'}
                            valueFormat='d'
                          /> */}
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                {
                  <div className="mt3 player-projection w-100">
                    <div className="flex overflow-x-scroll justify-auto-l flex-wrap-l pb4">
                      {data.preMatchPredection.playerList
                        .filter((id) => id.teamId === activeTab)
                        .map((player, i) => (
                          <div
                            key={i}
                            onClick={() => getPieValA(player, i)}
                            className={`pa1-l ph2-l w-20-l ma3-l h4-l br3 mh1 ${
                              playerA.playerName === player.playerName
                                ? 'ba b--darkRed bw1'
                                : 'ba b--black-70'
                            } shadow-4 `}
                          >
                            <div className="ph1 w2-6 h2-6 dn-l">
                              <img
                                className={`w-100 h-100 object-cover br2 br3-l object-top md:cursor-pointer bg-white`}
                                src={`https://images.cricket.com/players/${player.playerId}_headshot_safari.png`}
                                alt=""
                                onError={(evt) =>
                                  (evt.target.src = matchupPlayer1)
                                }
                              />
                            </div>
                            <div className="br-50 w2-6 h2-6 overflow-hidden bg-white ba b--grey_7 dn db-l tc center mt3">
                              <img
                                className="contain"
                                src={`https://images.cricket.com/players/${player.playerId}_headshot_safari.png`}
                                alt=""
                                onError={(evt) =>
                                  (evt.target.src = matchupPlayer1)
                                }
                              />
                            </div>
                            <div className="f6 fw4 ml1 black dn db-l pt2 tc">
                              {player.playerName}
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                }
              </div>
            ) : (
              ''
            )}

            {/* Player B  */}

            {activeTab ===
            data.preMatchPredection.teams_Runs_Projection[1].teamId ? (
              <div>
                <div className="bg-white flex justify-center pa3">
                  <div className="f6 fw6 grey_10 flex items-center">
                    Predicted 1st Inn Score
                  </div>
                  <div className="flex pl4">
                    {data.preMatchPredection.teams_Runs_Projection[1].teamScore
                      .split('')
                      .map((digit, i) => (
                        <div
                          key={i}
                          className="flex  justify-center items-center w15 h2 bg-grey_10 relative br1 mr1"
                        >
                          <div
                            className="absolute left-0 right-0 h01 o-40 z-1"
                            style={{
                              top: '50%',
                            }}
                          />
                          <div className="white flex justify-center items-center f3 oswald">
                            {digit}
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
                <div className="divider" />
                <div className="bg-gradientGrey pa3 f8 ttu grey_8 fw5">
                  {' '}
                  player Score Projector{' '}
                </div>
                <div className="divider" />

                {/* <div className="flex justify-end pt2 ph2">
                        <div className="flex items-center justify-between ph2 pv1 bg-black-20 br-pill md:cursor-pointer w-25" onClick={() => setStatsTab(statsTab === "battingRecord" ? "bowlingRecord" : "battingRecord")} >
                           <img className={`h1 w1 pa1 br-50 ${statsTab === 'battingRecord' ? 'cdcgr' : ''}`} alt={BatIcon} src={BatIcon} />
                           <img className={` h1 w1 pa1 br-50 ${statsTab === 'bowlingRecord' ? 'cdcgr' : ''}`} alt={ballIcon} src={ballIcon} />
                        </div>
                     </div> */}

                {playerB && playerB.listProjections && (
                  <div className="relative">
                    <div className="absolute left-0 bottom-0">
                      <img
                        src={`https://images.cricket.com/players/${playerB.playerId}_headshot_safari.png`}
                        alt=""
                        className="h44"
                        onError={(evt) => (evt.target.src = matchupPlayer1)}
                      />
                    </div>

                    <div className="flex justify-end mr2">
                      <div className="pv3 flex justify-between w-70">
                        <div className="pl1">
                          <div className="flex items-center">
                            <img
                              className="w1 h1"
                              src={
                                playerB.playerRole === 'Batsman'
                                  ? `${batsmen}`
                                  : playerB.playerRole === 'Bowler'
                                  ? `${bowler}`
                                  : allrounderIcon
                              }
                              alt=""
                            />
                            <div className="f6 fw6 ml1">
                              {playerB.playerName}
                            </div>
                          </div>

                          <div className="flex items-center mt1">
                            <div className="f7 grey_8">Projected</div>
                            <div
                              className=" pv1 ph3 ml2 br4 f8 fw6 grey_10"
                              style={{ backgroundColor: '#E5F1E1' }}
                            >
                              {/* {playerB.playerRole === "Batsman" && playerB.listProjections && playerB.listProjections[0].values[0].bound === "0" && playerB.listProjections[0].values[2].bound === "0" ? "NA runs" : null}
                                       {playerB.playerRole === "Bowler" && playerB.listProjections && playerB.listProjections[0].values[0].bound === "0" && playerB.listProjections[0].values[2].bound === "0" ? "NA wickets" : null}
                                       {playerB.playerRole === "Batsman" && playerB.listProjections && (playerB.listProjections[0].values[0].bound !== "0" || playerB.listProjections[0].values[2].bound !== "0") ? `${playerB.listProjections[0].values[0].bound} - ${playerB.listProjections[0].values[2].bound} runs` : null}
                                       {playerB.playerRole === "Bowler" && playerB.listProjections && (playerB.listProjections[0].values[0].bound !== "0" || playerB.listProjections[0].values[2].bound !== "0") ? `${playerB.listProjections[0].values[0].bound} - ${playerB.listProjections[0].values[2].bound} wickets` : null} */}
                              {(
                                playerB.playerRole == 'All Rounder' &&
                                statsTab == 'bowlingRecord'
                                  ? playerB.listProjections[1].values[2]
                                      .bound == 0
                                  : playerB.listProjections[0].values[2]
                                      .bound == 0
                              )
                                ? `0 ${
                                    (playerB.playerRole == 'All Rounder' &&
                                      statsTab == 'bowlingRecord') ||
                                    playerB.playerRole == 'Bowler'
                                      ? ' wicket'
                                      : ' run'
                                  }`
                                : (
                                    playerB.playerRole == 'All Rounder' &&
                                    statsTab == 'bowlingRecord'
                                      ? playerB.listProjections[1].values[2]
                                          .bound == -1
                                      : playerB.listProjections[0].values[2]
                                          .bound == -1
                                  )
                                ? `NA`
                                : `${
                                    playerB.playerRole == 'All Rounder' &&
                                    statsTab == 'bowlingRecord'
                                      ? playerB.listProjections[1].values[0]
                                          .bound
                                      : playerB.listProjections[0].values[0]
                                          .bound
                                  }-${
                                    playerB.playerRole == 'All Rounder' &&
                                    statsTab == 'bowlingRecord'
                                      ? playerB.listProjections[1].values[2]
                                          .bound
                                      : playerB.listProjections[0].values[2]
                                          .bound
                                  } ${
                                    (playerB.playerRole == 'All Rounder' &&
                                      statsTab == 'battingRecord') ||
                                    playerB.playerRole === 'Batsman'
                                      ? 'runs'
                                      : 'wickets'
                                  }`}
                            </div>
                          </div>
                        </div>
                        {/* ==== */}
                        {playerB.playerRole === 'All Rounder' && (
                          <div className="flex justify-end w-25 w-15-l">
                            <div
                              className="flex items-center justify-between ph2 bg-black-20 br-pill md:cursor-pointer w-100"
                              onClick={() => {
                                setStatsTab(
                                  statsTab === 'battingRecord'
                                    ? 'bowlingRecord'
                                    : 'battingRecord',
                                )
                                getPieValB(
                                  playerB,
                                  indexB,
                                  statsTab === 'battingRecord'
                                    ? 'bowlingRecord'
                                    : 'battingRecord',
                                )
                              }}
                            >
                              <img
                                className={`h1 w1 pa1 br-50 ${
                                  statsTab === 'battingRecord' ? 'cdcgr' : ''
                                }`}
                                alt={BatIcon}
                                src={BatIcon}
                              />
                              <img
                                className={` h1 w1 pa1 br-50 ${
                                  statsTab === 'bowlingRecord' ? 'cdcgr' : ''
                                }`}
                                alt={ballIcon}
                                src={ballIcon}
                              />
                            </div>
                          </div>
                        )}
                        {/* === */}
                      </div>
                    </div>

                    <div className="flex justify-end bg-grey_10">
                      <div className="items-center  pr5-ns justify-center w-50 w-30-ns pt2 pt1-ns ">
                        <div className=" overflow-hidden ma2 h35 h4-l">
                          {/* <ReactSpeedometer
                            key={`${playerB.playerId}_${maxValueB}`}
                            // value={Number(playerB.listProjections[1].bound)}
                            value={
                              playerB.playerRole == 'All Rounder' && statsTab == 'bowlingRecord'
                                ? Number(playerB.listProjections[1].values[1].bound) == -1
                                  ? 0
                                  : Number(playerB.listProjections[1].values[1].bound)
                                : Number(playerB.listProjections[0].values[1].bound) == -1
                                ? 0
                                : Number(playerB.listProjections[0].values[1].bound)
                            }
                            maxValue={maxValueB}
                            minValue={0}
                            fluidWidth={true}
                            ringWidth={30}
                            // forceRender
                            maxSegmentLabels={3}
                            needleHeightRatio={0.9}
                            needleTransitionDuration={500}
                            needleColor='#fff'
                            // startColor="#d44030"
                            startColor='#f5a623'
                            endColor='#a70e13'
                            segments={3}
                            segmentColors={['#f5a623', '#f76b1c', '#a70e13']}
                            // endColor="#ffb11b"
                            textColor={'#dbdbdb'}
                            valueFormat='d'
                          /> */}
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                {
                  <div className="mt3 player-projection w-100">
                    <div className="flex overflow-x-scroll justify-auto-l flex-wrap-l pb4 ">
                      {data.preMatchPredection.playerList
                        .filter((id) => id.teamId === activeTab)
                        .map((player, i) => (
                          <div
                            key={i}
                            onClick={() => getPieValB(player, i)}
                            className={`mh1 pa1-l ph2-l w-20-l ma3-l h4-l ba-l br3 ${
                              playerB.playerName === player.playerName
                                ? 'ba b--darkRed bw1'
                                : 'ba b--black-70'
                            }`}
                          >
                            <div className="ph1 w2-6 h2-6 dn-l">
                              <img
                                className="w-100 h-100 object-cover br2 br3-l object-top cursor-pointer bg-white"
                                src={`https://images.cricket.com/players/${player.playerId}_headshot_safari.png`}
                                alt=""
                                onError={(evt) =>
                                  (evt.target.src = matchupPlayer1)
                                }
                              />
                            </div>
                            <div className="br-50 w2-6 h2-6 overflow-hidden bg-white ba b--grey_7 dn db-l tc center mt3">
                              <img
                                className="contain"
                                src={`https://images.cricket.com/players/${player.playerId}_headshot_safari.png`}
                                alt=""
                                onError={(evt) =>
                                  (evt.target.src = matchupPlayer1)
                                }
                              />
                            </div>
                            <div className="f6 fw4 ml1 black dn db-l pt2 tc">
                              {player.playerName}
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                }
              </div>
            ) : (
              ''
            )}
          </div>
        ) : (
          <div className=" ma4 pb4">
            <img
              className="w45-m h45-m w4 h4 w5-l h5-l "
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt="loading..."
            />
          </div>
        )}
      </div>
    )
}

export default PlayerProjection

//{playerA.playerRole === 'Batsman'  ? `${playerA.listProjections && playerA.listProjections[0].bound} - ${playerA.listProjections && playerA.listProjections[2].bound} runs` :
//`${playerA.listProjections && playerA.listProjections[0].bound}-${playerA.listProjections && playerA.listProjections[2].bound} wickets`}
