//old code
import React, { useEffect, useState } from 'react'
import * as d3 from 'd3'

import { useQuery } from '@apollo/react-hooks'
import { GET_RUN_RATE } from '../../api/queries'

export default function RunRate(props) {
  const [screenWidth, setScreenWidth] = useState('100%')
  const [screenHeight, setscreenHeight] = useState(350)
  const [screenHeight1, setscreenHeight1] = useState(450)

  const { loading, error, data } = useQuery(GET_RUN_RATE, {
    variables: { matchID: props?.matchID },
  })

  useEffect(() => {
    if (
      typeof window !== 'undefined' &&
      window &&
      window.screen &&
      window.screen.width
    ) {
      setScreenWidth(
        window.innerWidth > 1500
          ? props.cricltics
            ? window.innerWidth * 0.7
            : window.innerWidth * 0.37
          : window.innerWidth > 1300
          ? props.cricltics
            ? window.innerWidth * 0.64
            : window.innerWidth * 0.34
          : window.innerWidth < 1300 && window.innerWidth > 1000
          ? props.cricltics
            ? window.innerWidth * 0.7
            : window.innerWidth * 0.8
          : window.screen.width,
      )
    }
  }, [])

  /* For WEB */
  const margin = { top: 30, right: 120, bottom: 30, left: 150 },
    width = screenWidth - margin.left - margin.right,
    height = screenHeight1 - margin.top - margin.bottom
  const x = d3
    .scaleLinear()
    .domain([
      0,
      data?.getRunrate?.format === 'ODI'
        ? 50
        : data?.getRunrate?.format === 'T10'
        ? 10
        : 20,
    ])
    .range([0, width])
  let y = d3
    .scaleLinear()
    .domain([0, data?.getRunrate?.inningsPhase[0]?.highestRunrate + 20])
    .range([height, 0])
  const line = d3
    .line()
    .curve(d3.curveNatural)
    .x((d) => x(d.o))
    .y((d) => y(d.r))

  /* For MWeb */
  const margin1 = { top: 30, right: 20, bottom: 30, left: 80 },
    width1 = screenWidth - margin1.left - margin1.right,
    height1 = screenHeight - margin1.top - margin1.bottom
  const xM = d3
    .scaleLinear()
    .domain([
      0,
      data?.getRunrate?.format === 'ODI'
        ? 50
        : data?.getRunrate?.format === 'T10'
        ? 10
        : 20,
    ])
    .range([0, width1])
  let yM = d3
    .scaleLinear()
    .domain([0, data?.getRunrate?.inningsPhase[0]?.highestRunrate + 20])
    .range([height1, 0])
  const line1 = d3
    .line()
    .curve(d3.curveNatural)
    .x((d) => xM(d.o))
    .y((d) => yM(d.r))

  if (
    error ||
    (!loading &&
      (!data ||
        !data.getRunrate ||
        !data.getRunrate?.inningsPhase ||
        data.getRunrate?.inningsPhase?.length === 0))
    //  ||
    // !data ||
    // !data.runsComparsion ||
    // !data.runsComparsion.inningsPhase ||
    // data.runsComparsion.inningsPhase.length === 0
  ) {
    return <div></div>
  }
  if (loading) return <div></div>
  else {
    return (
      <div className="relative flex justify-center items-center">
        {/* <text
          className={` absolute font-medium text-gray-2 rotate-270`}
          style={{
            // bottom: 26,
            top: height + 14,
            left: 2,
            fontSize: 10,
            transform: [{ rotate: '270deg' }],
          }}
        >
          Runs
        </text> */}
        <div className="md:hidden">
          <svg height={height1 + 50} width={width1 + 60}>
            <g transform="translate(25,25)">
              <rect
                height={height1 + 10}
                width={width1 + 20}
                fill={'#1F2634'}
                style={{ borderRadius: 10, overflow: 'hidden' }}
                transform="translate(3,-9)"
              />
              <g transform="translate(-7,1)">
                {[...yM.ticks(5)].map((tick) => (
                  <g key={tick} transform="translate(-5,0)">
                    <line
                      x1="10"
                      y1={yM(tick)}
                      x2={width1 + 30}
                      y2={yM(tick)}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#2B323F'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={0}
                      y={yM(tick)}
                      fontSize="10"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
                {xM.ticks(5).map((tick) => (
                  <g key={tick} transform="translate(10,0)">
                    <line
                      x1={xM(tick)}
                      y1="0"
                      x2={xM(tick)}
                      y2={height1}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#2B323F'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={xM(tick)}
                      y={height1 + 20}
                      fontSize="10"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
              </g>
              <g>
                <path
                  transform="translate(4,0)"
                  id="line"
                  d={line1(data.getRunrate?.inningsPhase[0]?.data)}
                  fill="none"
                  stroke="#5F92AC"
                  strokeWidth="2"
                />
                {data?.getRunrate?.inningsPhase[0]?.data.map((ovr) =>
                  ovr.wickets ? (
                    [...new Array(ovr.wickets)].map((val, index) => (
                      <circle
                        transform={`translate(4,${
                          index === 0 ? 0 : -3.1 * (index + 1)
                        })`}
                        fill={'#5F92AC'}
                        r={3}
                        cx={xM(ovr.overNumber)}
                        cy={yM(ovr.score)}
                      />
                    ))
                  ) : (
                    <></>
                  ),
                )}
              </g>
              {data.getRunrate.inningsPhase[1] &&
                data.getRunrate.inningsPhase[1].data &&
                data.getRunrate.inningsPhase[1].data.length > 0 && (
                  <g>
                    <path
                      transform="translate(4,0)"
                      id="line"
                      d={line1(data.getRunrate.inningsPhase[1].data)}
                      fill="none"
                      stroke={'#38D926'}
                      // strokeDasharray="3 3"
                      strokeWidth="2"
                      // style={{ opacity: 0.8 }}
                    />
                    {data.getRunrate.inningsPhase[1].data.map((ovr) =>
                      ovr.wickets ? (
                        [...new Array(ovr.wickets)].map((val, index) => (
                          <circle
                            transform={`translate(4,${
                              index === 0 ? 0 : -3.1 * (index + 1)
                            })`}
                            fill={'#38D926'}
                            r={3}
                            cx={xM(ovr.overNumber)}
                            cy={yM(ovr.score)}
                          />
                        ))
                      ) : (
                        <></>
                      ),
                    )}
                  </g>
                )}
            </g>
            <text
              x={45}
              y={height1 + 45}
              fontSize="10"
              fill="#8C98B0"
              fontFamily="Manrope-Medium"
              textAnchor="middle"
            >
              Overs
            </text>
            {/* <div
            style={{
              color: '#8C98B0',
              fontFamily: 'Manrope-Medium',
              fontSize: 10,
              position: 'absolute',
              left: 5,
              top: height,
              transform: [{rotate: '270deg'}],
            }}>
            Runs
          </div> */}
            ​
            {/* <Text style={tw(`font-mnm`)}
            x={height + 4}
            y={-15}
            fontSize="10"
            fill="#8C98B0"
            // fontFamily="Manrope-Medium"
            transform="rotate(90)"
            textAnchor="start">
            Runs
          </Text> */}
          </svg>
        </div>
        <div className="hidden md:block">
          <svg height={height + 50} width={width + 60}>
            <g transform="translate(35,25)">
              <rect
                height={height + 12}
                width={width + 2}
                fill={'#EEEFF2'}
                style={{ borderRadius: 10, overflow: 'hidden' }}
                transform="translate(3,-9)"
              />
              <g transform="translate(-7,1)">
                {[...y.ticks(6)].map((tick) => (
                  <g key={tick} transform="translate(-5,0)">
                    <line
                      x1="15"
                      y1={y(tick)}
                      x2={width + 14}
                      y2={y(tick)}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#E2E2E2'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={0}
                      y={y(tick)}
                      fontSize="15"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
                {x.ticks(8).map((tick) => (
                  <g key={tick} transform="translate(-40,0)">
                    <line
                      x1={x(tick)}
                      y1="0"
                      x2={x(tick)}
                      y2={height}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#E2E2E2'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={x(tick)}
                      y={height + 20}
                      fontSize="15"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
              </g>
              {data.getRunrate.inningsPhase[0] &&
                data.getRunrate.inningsPhase[0].data &&
                data.getRunrate.inningsPhase[0].data.length > 0 && (
                  <>
                    {window.innerWidth < 1300 && window.innerWidth > 1000 ? (
                      <g>
                        <path
                          transform="translate(-1,0)"
                          id="line"
                          d={line(data.getRunrate.inningsPhase[0].data)}
                          fill="none"
                          stroke="#5F92AC"
                          strokeWidth="2"
                        />
                      </g>
                    ) : window.innerWidth < 1400 && window.innerWidth > 1300 ? (
                      <g>
                        <path
                          transform="translate(-25,0)"
                          id="line"
                          d={line(data.getRunrate.inningsPhase[0].data)}
                          fill="none"
                          stroke="#5F92AC"
                          strokeWidth="2"
                        />
                      </g>
                    ) : (
                      <g>
                        <path
                          transform="translate(-45,0)"
                          id="line"
                          d={line(data.getRunrate.inningsPhase[0].data)}
                          fill="none"
                          stroke="#5F92AC"
                          strokeWidth="2"
                        />
                      </g>
                    )}
                  </>
                )}

              {data.getRunrate.inningsPhase[1] &&
                data.getRunrate.inningsPhase[1].data &&
                data.getRunrate.inningsPhase[1].data.length > 0 && (
                  <>
                    {window.innerWidth < 1300 && window.innerWidth > 1000 ? (
                      <g>
                        <path
                          transform="translate(4,0)"
                          id="line"
                          d={line(data.getRunrate.inningsPhase[1].data)}
                          fill="none"
                          stroke={'#38D926'}
                          // strokeDasharray="3 3"
                          strokeWidth="2"
                          // style={{ opacity: 0.8 }}
                        />
                      </g>
                    ) : window.innerWidth < 1400 && window.innerWidth > 1300 ? (
                      <g>
                        <path
                          transform="translate(-25,0)"
                          id="line"
                          d={line(data.getRunrate.inningsPhase[1].data)}
                          fill="none"
                          stroke={'#38D926'}
                          // strokeDasharray="3 3"
                          strokeWidth="2"
                          // style={{ opacity: 0.8 }}
                        />
                      </g>
                    ) : (
                      <g>
                        <path
                          transform="translate(-45,0)"
                          id="line"
                          d={line(data.getRunrate.inningsPhase[1].data)}
                          fill="none"
                          stroke={'#38D926'}
                          // strokeDasharray="3 3"
                          strokeWidth="2"
                          // style={{ opacity: 0.8 }}
                        />
                      </g>
                    )}
                  </>
                )}
            </g>
            <text
              x={45}
              y={height + 45}
              fontSize="12"
              fill="#8C98B0"
              fontFamily="Manrope-Medium"
              textAnchor="middle"
            >
              Overs
            </text>
            <text
              x={18}
              y={height + 30}
              fontSize="12"
              fill="#8C98B0"
              fontFamily="Manrope-Medium"
              textAnchor="middle"
            >
              Runs
            </text>
            ​
            {/* <Text style={tw(`font-mnm`)}
            x={height + 4}
            y={-15}
            fontSize="10"
            fill="#8C98B0"
            // fontFamily="Manrope-Medium"
            transform="rotate(90)"
            textAnchor="start">
            Runs
          </Text> */}
          </svg>
        </div>
        <div
          className={` ${
            window.innerWidth < 1300 && window.innerWidth > 1000
              ? 'right-24'
              : 'right-6'
          } absolute flex bottom-10 md:hidden`}
        >
          <div className="flex-row items-center flex pr-2">
            <div
              style={{
                width: 6,
                height: 6,
                borderRadius: 3,
                backgroundColor: '#5F92AC',
                marginRight: 2,
              }}
            />
            <div className="text-gray-2 font-medium text-xs">{`${data.getRunrate.inningsPhase[0].teamShortName} ${data.getRunrate.inningsPhase[0].totalRuns}/${data.getRunrate.inningsPhase[0].totalWickets}`}</div>
          </div>
          {data.getRunrate.inningsPhase[1] &&
          data.getRunrate.inningsPhase[1].data ? (
            <div className="flex-row items-center flex">
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: 3,
                  backgroundColor: '#38D926',
                  marginRight: 2,
                }}
              />
              <div className="text-gray-2 font-medium text-xs">{`${data.getRunrate.inningsPhase[1].teamShortName} ${data.getRunrate.inningsPhase[1].totalRuns}/${data.getRunrate.inningsPhase[1].totalWickets}`}</div>
            </div>
          ) : (
            <div />
          )}
        </div>
        <div
          style={{ left: width - 150 }}
          className={`absolute bottom-10 md:flex hidden`}
        >
          <div className="flex-row items-center flex pr-2">
            <div
              style={{
                width: 6,
                height: 6,
                borderRadius: 3,
                backgroundColor: '#5F92AC',
                marginRight: 2,
              }}
            />
            <div className="text-gray-2 font-medium text-xs">{`${data.getRunrate.inningsPhase[0].teamShortName} ${data.getRunrate.inningsPhase[0].totalRuns}/${data.getRunrate.inningsPhase[0].totalWickets}`}</div>
          </div>
          {data.getRunrate.inningsPhase[1] &&
          data.getRunrate.inningsPhase[1].data ? (
            <div className="flex-row items-center flex">
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: 3,
                  backgroundColor: '#38D926',
                  marginRight: 2,
                }}
              />
              <div className="text-gray-2 font-medium text-xs">{`${data.getRunrate.inningsPhase[1].teamShortName} ${data.getRunrate.inningsPhase[1].totalRuns}/${data.getRunrate.inningsPhase[1].totalWickets}`}</div>
            </div>
          ) : (
            <div />
          )}
        </div>
      </div>
    )
  }
}
