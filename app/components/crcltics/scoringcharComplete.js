import React, { useEffect, useState, useRef } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { PHASES_OF_PLAY } from '../../api/queries'

export default function ScoringChartCompleted(props) {
  let scrl = useRef(null)

  const [screenWidth, setScreenWidth] = useState(0)
  const [screenHeight, setscreenHeight] = useState(350)
  const [screenHeightd, setscreenHeightd] = useState(350)

  const [maxRun, setMaxRun] = useState(10)
  const [oversArr, setOverArr] = useState([])
  const [oversTab, setoversTab] = useState(0)
  const [yRange, setYRange] = useState()
  const [team, setTeam] = useState(0)

  const { loading, error, data } = useQuery(PHASES_OF_PLAY, {
    variables: { matchID: props.matchID },
    // variables: { matchID: '205391' },
    onCompleted: (data) => {
      if (
        data.getPhaseOfInningsResolver &&
        data.getPhaseOfInningsResolver.inningsPhase &&
        data.getPhaseOfInningsResolver.inningsPhase[team]
      ) {
        let a = Math.max.apply(
          Math,
          [
            ...data.getPhaseOfInningsResolver.inningsPhase[0].data,
            ...data.getPhaseOfInningsResolver.inningsPhase[1].data,
          ].map((o) => {
            return o.runs
          }),
        )

        setoversTab(
          data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25
            ? 0
            : 0,
        )
        setOverArr(
          props.matchType === 'T20'
            ? [4, 8, 12, 16, 20]
            : props.matchType === 'T10'
            ? [2, 4, 6, 8, 10]
            : data.getPhaseOfInningsResolver.inningsPhase[team].data.length >
                22 &&
              data.getPhaseOfInningsResolver.inningsPhase[team].data.length < 26
            ? [4, 8, 12, 16, 20, 24, 28]
            : data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25
            ? [30, 35, 40, 45, 50]
            : [5, 10, 15, 20, 25],
        )
        setYRange(
          a < 8
            ? 10
            : a <= 13
            ? 15
            : a <= 18
            ? 20
            : a <= 23
            ? 25
            : a <= 28
            ? 30
            : a <= 33
            ? 35
            : a <= 38
            ? 40
            : 45,
        )
        setMaxRun(a)
      }
    },
  })

  //   ;

  useEffect(() => {
    // setScreenWidth(window.innerWidth);
    if (
      typeof window !== 'undefined' &&
      window &&
      window.screen &&
      window.screen.width
    ) {
      setScreenWidth(
        window.innerWidth > 1500
          ? props.cricltics
            ? window.innerWidth * 0.7
            : window.innerWidth * 0.37
          : window.innerWidth > 1300 && window.innerWidth < 1400
          ? props.cricltics
            ? window.innerWidth * 0.64
            : window.innerWidth * 0.34
          : window.innerWidth < 1300 && window.innerWidth > 1000
          ? props.cricltics
            ? window.innerWidth * 0.9
            : window.innerWidth * 0.9
          : window.innerWidth < 1400 && window.innerWidth > 1300
          ? props.cricltics
            ? window.innerWidth * 0.4
            : window.innerWidth * 0.4
          : window.screen.width,
      )
      setscreenHeightd(props.widthDIv)
    }
  }, [])

  const margin = { top: 30, right: 40, bottom: 30, left: 80 },
    width = screenWidth - margin.left - margin.right,
    height = screenHeight - margin.top - margin.bottom

  return data &&
    data.getPhaseOfInningsResolver &&
    data.getPhaseOfInningsResolver.inningsPhase &&
    data.getPhaseOfInningsResolver.inningsPhase.length > 0 &&
    data.getPhaseOfInningsResolver.inningsPhase[team] &&
    data.getPhaseOfInningsResolver.inningsPhase[team].data ? (
    <>
      <div
        className="text-white dark:bg-gray md:bg-white rounded-md py-3 px-2 hidden md:block"
        style={{ width: screenHeightd }}
      >
        <div className="flex items-center justify-between my-2 ml-2 px-3">
          <div className="text-gray-2 md:hidden text-sm font-medium">
            INNINGS
          </div>
          <div className="flex rounded-3xl items-center justify-between text-sm dark:bg-gray-8 md:py-1  md:bg-white md:w-3/12 w-2/3 mr-8">
            <div
              className={`w-1/2 lg:w-6/12 md:w-6/12 xl:w-6/12 lg:mx-1 md:cursor-pointer md:mx-1 xl:mx-1 text-center rounded-3xl py-1 ${
                team === 0
                  ? `border-2 lg:border-basered bg-basered dark:border-green dark:bg-gray-8 shadow-sm dark:shadow-green font-medium`
                  : 'lg:bg-[#EEEEEE] lg:text-black md:text-black md:bg-[#EEEE] xl:text-black xl:bg-[#EEEE]'
              }`}
              onClick={() => setTeam(0)}
            >
              {data.getPhaseOfInningsResolver.inningsPhase[0].teamShortName}
            </div>
            <div
              className={`w-1/2 lg:w-6/12 md:w-6/12 xl:w-6/12 lg:mx-1 md:cursor-pointer md:mx-1 xl:mx-1 text-center rounded-3xl py-1 ${
                team === 1
                  ? `border-2 border-green lg:border-basered lg:bg-basered bg-gray-8 dark:shadow-green font-medium`
                  : 'lg:bg-[#EEEEEE] lg:text-black md:text-black md:bg-[#EEEE] xl:text-black xl:bg-[#EEEE]'
              }`}
              onClick={() => setTeam(1)}
            >
              {data.getPhaseOfInningsResolver.inningsPhase[1].teamShortName}
            </div>
          </div>
        </div>

        {data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25 &&
          props.matchType == 'ODI' && (
            <div
              className="flex justify-between overflow-hidden"
              style={{
                width: screenHeightd - 190,
              }}
            >
              <div style={{ width: 31 }}></div>
              <div
                className={`bg-gray-4 rounded-t flex py-2 justify-between text-sm font-semibold `}
                style={{
                  width: screenHeightd - 200,
                }}
              >
                <div
                  className={`w-1/2 pb-2 text-center ${
                    oversTab == 0
                      ? 'text-blue border-b-2 border-blue'
                      : 'text-gray-2'
                  }`}
                  onClick={() => (
                    setoversTab(0), setOverArr([5, 10, 15, 20, 25])
                  )}
                >
                  1-25 Overs
                </div>
                <div
                  className={`w-1/2 pb-2 text-center ${
                    oversTab == 1
                      ? 'text-blue border-b-2 border-blue'
                      : 'text-gray-2'
                  }`}
                  onClick={() => (
                    setoversTab(1), setOverArr([30, 35, 40, 45, 50])
                  )}
                >
                  26-50 Overs
                </div>
              </div>
            </div>
          )}
        <div
          className="flex justify-between overflow-hidden"
          style={{
            width: screenHeightd + 30,
            height: 280,
          }}
        >
          <div
            className="flex flex-col-reverse text-gray-2 text-xs font-medium"
            style={{ width: 22 }}
          >
            {[1, 2, 3, 4, 5].map((val, i) => (
              <div
                key={i}
                className=""
                style={{
                  height: 280 / 5,
                }}
              >
                <p className="flex justify-end ">{(yRange / 5) * (i + 1)}</p>
                <p
                  className={`${
                    i === 0 ? '' : ''
                  } -rotate-90 flex justify-end mt-2 ml-3`}
                >
                  {i === 0 ? 'Runs' : ''}
                </p>
              </div>
            ))}
          </div>

          <div
            className=""
            style={{
              width: screenHeightd,
            }}
          >
            {window.innerWidth < 1300 && window.innerWidth > 1000 ? (
              <div
                className="rounded-b dark:bg-gray-4 lg:bg-gray-11 flex items-end justify-start "
                style={{
                  width: screenHeightd,
                  height: 280,
                  paddingBottom: 0,
                }}
              >
                {data &&
                  data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                    (over, i) =>
                      (props.matchType == 'ODI'
                        ? oversTab === 0 && i < 25
                        : true) && (
                        <div
                          key={i}
                          className={` dark:bg-gray-3 bg-blue-9 rounded relative`}
                          style={{
                            width:
                              screenHeightd /
                              (props.matchType == 'T10'
                                ? 10
                                : props.matchType == 'T20'
                                ? 23
                                : oversArr.length === 7
                                ? 28
                                : 25),
                            height: (275 / 5) * (over.runs / (yRange / 5)),
                            // height:(over.runs/yRange)*275,
                            marginRight: 2.5,
                          }}
                        >
                          <div className="flex-col">
                            {Array.apply(null, {
                              length: Number(over.wickets),
                            }).map((x, i) => (
                              <p
                                key={i}
                                className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                              />
                            ))}
                          </div>
                        </div>
                      ),
                  )}
                {data &&
                  data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                    (over, i) =>
                      (props.matchType == 'ODI'
                        ? oversTab === 1 && i >= 25
                        : false) && (
                        <div
                          key={i}
                          className={` bg-gray-3 rounded relative`}
                          style={{
                            width:
                              screenHeightd /
                              (props.matchType == 'T10'
                                ? 10
                                : props.matchType == 'T20'
                                ? 20
                                : 30),
                            height: (275 / 5) * (over.runs / (yRange / 5)),
                            marginRight: 2.5,
                          }}
                        >
                          <div className="flex-col ">
                            {Array.apply(null, {
                              length: Number(over.wickets),
                            }).map((x, i) => (
                              <p
                                key={i}
                                className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                              />
                            ))}
                          </div>
                        </div>
                      ),
                  )}
              </div>
            ) : window.innerWidth < 1400 && window.innerWidth > 1300 ? (
              <div
                className="rounded-b dark:bg-gray-4 lg:bg-gray-11 flex items-end justify-start "
                style={{
                  width: screenHeightd - 150,
                  height: 280,
                  paddingBottom: 0,
                }}
              >
                {data &&
                  data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                    (over, i) =>
                      (props.matchType == 'ODI'
                        ? oversTab === 0 && i < 25
                        : true) && (
                        <div
                          key={i}
                          className={` dark:bg-gray-3 bg-blue-9 rounded relative`}
                          style={{
                            width:
                              (screenWidth - 75) /
                              (props.matchType == 'T10'
                                ? 10
                                : props.matchType == 'T20'
                                ? 23
                                : oversArr.length === 7
                                ? 28
                                : 25),
                            height: (275 / 5) * (over.runs / (yRange / 5)),
                            // height:(over.runs/yRange)*275,
                            marginRight: 2.5,
                          }}
                        >
                          <div className="flex-col">
                            {Array.apply(null, {
                              length: Number(over.wickets),
                            }).map((x, i) => (
                              <p
                                key={i}
                                className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                              />
                            ))}
                          </div>
                        </div>
                      ),
                  )}
                {data &&
                  data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                    (over, i) =>
                      (props.matchType == 'ODI'
                        ? oversTab === 1 && i >= 25
                        : false) && (
                        <div
                          key={i}
                          className={` bg-gray-3 rounded relative`}
                          style={{
                            width:
                              (screenHeightd - 75) /
                              (props.matchType == 'T10'
                                ? 10
                                : props.matchType == 'T20'
                                ? 20
                                : 30),
                            height: (275 / 5) * (over.runs / (yRange / 5)),
                            marginRight: 2.5,
                          }}
                        >
                          <div className="flex-col ">
                            {Array.apply(null, {
                              length: Number(over.wickets),
                            }).map((x, i) => (
                              <p
                                key={i}
                                className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                              />
                            ))}
                          </div>
                        </div>
                      ),
                  )}
              </div>
            ) : (
              <div
                className="rounded-b dark:bg-gray-4 lg:bg-gray-11 flex items-end justify-start "
                style={{
                  width: screenHeightd - 220,
                  height: 280,
                  paddingBottom: 0,
                }}
              >
                {data &&
                  data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                    (over, i) =>
                      (props.matchType == 'ODI'
                        ? oversTab === 0 && i < 25
                        : true) && (
                        <div
                          key={i}
                          className={` dark:bg-gray-3 bg-blue-9 rounded relative`}
                          style={{
                            width:
                              (screenHeightd - 75) /
                              (props.matchType == 'T10'
                                ? 10
                                : props.matchType == 'T20'
                                ? 23
                                : oversArr.length === 7
                                ? 28
                                : 25),
                            height: (275 / 5) * (over.runs / (yRange / 5)),
                            // height:(over.runs/yRange)*275,
                            marginRight: 2.5,
                          }}
                        >
                          <div className="flex-col">
                            {Array.apply(null, {
                              length: Number(over.wickets),
                            }).map((x, i) => (
                              <p
                                key={i}
                                className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                              />
                            ))}
                          </div>
                        </div>
                      ),
                  )}
                {data &&
                  data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                    (over, i) =>
                      (props.matchType == 'ODI'
                        ? oversTab === 1 && i >= 25
                        : false) && (
                        <div
                          key={i}
                          className={` bg-gray-3 rounded relative`}
                          style={{
                            width:
                              (screenHeightd - 75) /
                              (props.matchType == 'T10'
                                ? 10
                                : props.matchType == 'T20'
                                ? 20
                                : 30),
                            height: (275 / 5) * (over.runs / (yRange / 5)),
                            marginRight: 2.5,
                          }}
                        >
                          <div className="flex-col ">
                            {Array.apply(null, {
                              length: Number(over.wickets),
                            }).map((x, i) => (
                              <p
                                key={i}
                                className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                              />
                            ))}
                          </div>
                        </div>
                      ),
                  )}
              </div>
            )}

            {window.innerWidth < 1300 && window.innerWidth > 1000 ? (
              <div className="pt-1 flex text-gray-2 text-xs font-medium">
                {oversArr.map((val, i) => (
                  // {
                  //   window.innerWidth < 1300 && window.innerWidth > 1000 ? '':''
                  // }
                  <div
                    key={i}
                    className="justify-between flex text-end"
                    style={{
                      width: screenHeightd - 1000,
                    }}
                  >
                    <p className={`${i === 0 ? 'pr-2' : ''}`}>
                      {i === 0 ? 'Overs' : ''}
                    </p>
                    <p className="text-end">{val}</p>
                  </div>
                ))}
              </div>
            ) : window.innerWidth < 1400 && window.innerWidth > 1300 ? (
              <div className="pt-1 flex text-gray-2 text-xs font-medium">
                {oversArr.map((val, i) => (
                  <div
                    key={i}
                    className="justify-between flex text-end"
                    style={{
                      width: screenHeightd - 730,
                    }}
                  >
                    <p className={`${i === 0 ? 'pr-2' : ''}`}>
                      {i === 0 ? 'Overs' : ''}
                    </p>
                    <p className="text-end">{val}</p>
                  </div>
                ))}
              </div>
            ) : (
              <div className="pt-1 flex text-gray-2 text-xs font-medium">
                {oversArr.map((val, i) => (
                  <div
                    key={i}
                    className="justify-between flex text-end"
                    style={{
                      width: screenHeightd - 1050,
                    }}
                  >
                    <p className={`${i === 0 ? 'pr-2' : ''}`}>
                      {i === 0 ? 'Overs' : ''}
                    </p>
                    <p className="text-end">{val}</p>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
        <div className="pt-1 flex text-gray-2 text-xs font-medium">
          {oversArr.map((val, i) => (
            <div
              key={i}
              className="justify-between flex text-end"
              style={{
                width: screenHeightd - 900,
              }}
            >
              <p className={`${i === 0 ? 'pr-2' : ''}`}>
                {i === 0 ? 'Overs' : ''}
              </p>
              <p className="text-end">{val}</p>
            </div>
          ))}
        </div>
        <div className="flex justify-start items-center mt-12 px-2 font-medium">
          <div className="flex items-center mr-3">
            <p className="w-2 h-2 rounded-full bg-gray-3"></p>
            <p className="text-gray-2 text-sm font-medium pl-1">
              {data.getPhaseOfInningsResolver.inningsPhase[team].teamShortName}{' '}
              {data.getPhaseOfInningsResolver.inningsPhase[team].totalRuns}/
              {data.getPhaseOfInningsResolver.inningsPhase[team].totalWickets}
            </p>
          </div>
          <div className="flex items-center ml-3">
            <p className="w-2 h-2 rounded-full bg-red"></p>
            <p className="text-gray-2 text-sm pl-1">Wicket</p>
          </div>
        </div>
      </div>

      <div
        className="text-white bg-gray rounded-md md:hidden py-3 px-2 "
        style={{ width: screenWidth - 8 }}
      >
        <div className="flex items-center justify-between my-2 px-3 ml-4">
          {/* <div className="text-gray-2 text-sm md:hidden font-medium">
            INNINGS
          </div> */}
          <div className="flex rounded-3xl py-0.5 items-center justify-between text-sm bg-gray-8 w-2/3">
            <div
              className={`w-1/2 text-center rounded-3xl ${
                team === 0 ? `border border-green bg-gray-8 font-medium` : ''
              }`}
              onClick={() => setTeam(0)}
            >
              {data.getPhaseOfInningsResolver.inningsPhase[0].teamShortName}
            </div>
            <div
              className={`w-1/2 text-center rounded-3xl  ${
                team === 1
                  ? `border border-green bg-gray-8 shadow-sm font-medium`
                  : ''
              }`}
              onClick={() => setTeam(1)}
            >
              {data.getPhaseOfInningsResolver.inningsPhase[1].teamShortName}
            </div>
          </div>
        </div>

        {data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25 &&
          props.matchType == 'ODI' && (
            <div
              className="flex justify-between"
              style={{
                width: screenWidth - 48,
              }}
            >
              <div style={{ width: 22 }}></div>
              <div
                className={`bg-gray-4 rounded-t flex py-2 justify-between text-sm font-semibold `}
                style={{
                  width: screenWidth - 75,
                }}
              >
                <div
                  className={`w-1/2 pb-2 text-center ${
                    oversTab == 0
                      ? 'text-blue border-b-2 border-blue'
                      : 'text-gray-2'
                  }`}
                  onClick={() => (
                    setoversTab(0), setOverArr([5, 10, 15, 20, 25])
                  )}
                >
                  1-25 Overs
                </div>
                <div
                  className={`w-1/2 pb-2 text-center ${
                    oversTab == 1
                      ? 'text-blue border-b-2 border-blue'
                      : 'text-gray-2'
                  }`}
                  onClick={() => (
                    setoversTab(1), setOverArr([30, 35, 40, 45, 50])
                  )}
                >
                  26-50 Overs
                </div>
              </div>
            </div>
          )}
        <div
          className="flex justify-between "
          style={{
            width: screenWidth - 48,
            height: 280,
          }}
        >
          <div
            className="flex flex-col-reverse text-gray-2 text-xs font-medium"
            style={{ width: 22 }}
          >
            {[1, 2, 3, 4, 5].map((val, i) => (
              <div
                key={i}
                className=""
                style={{
                  height: 280 / 5,
                }}
              >
                <p className="flex justify-end ">{(yRange / 5) * (i + 1)}</p>
                <p
                  className={`${
                    i === 0 ? '' : ''
                  } -rotate-90 flex justify-end mt-2 ml-3`}
                >
                  {i === 0 ? 'Runs' : ''}
                </p>
              </div>
            ))}
          </div>
          <div
            className=""
            style={{
              width: screenWidth - 75,
            }}
          >
            <div
              className="rounded-b bg-gray-4 flex items-end justify-start "
              style={{
                width: screenWidth - 75,
                height: 280,
                paddingBottom: 0,
              }}
            >
              {data &&
                data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                  (over, i) =>
                    (props.matchType == 'ODI'
                      ? oversTab === 0 && i < 25
                      : true) && (
                      <div
                        key={i}
                        className={` bg-gray-3 rounded relative`}
                        style={{
                          width:
                            (screenWidth - 75) /
                            (props.matchType == 'T10'
                              ? 10
                              : props.matchType == 'T20'
                              ? 23
                              : oversArr.length === 7
                              ? 28
                              : 25),
                          height: (275 / 5) * (over.runs / (yRange / 5)),
                          // height:(over.runs/yRange)*275,
                          marginRight: 2.5,
                        }}
                      >
                        <div className="flex-col ">
                          {Array.apply(null, {
                            length: Number(over.wickets),
                          }).map((x, i) => (
                            <p
                              key={i}
                              className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                            />
                          ))}
                        </div>
                      </div>
                    ),
                )}
              {data &&
                data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                  (over, i) =>
                    (props.matchType == 'ODI'
                      ? oversTab === 1 && i >= 25
                      : false) && (
                      <div
                        key={i}
                        className={` bg-gray-3 rounded relative`}
                        style={{
                          width:
                            (screenWidth - 75) /
                            (props.matchType == 'T10'
                              ? 10
                              : props.matchType == 'T20'
                              ? 20
                              : 30),
                          height: (275 / 5) * (over.runs / (yRange / 5)),
                          marginRight: 2.5,
                        }}
                      >
                        <div className="flex-col ">
                          {Array.apply(null, {
                            length: Number(over.wickets),
                          }).map((x, i) => (
                            <p
                              key={i}
                              className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                            />
                          ))}
                        </div>
                      </div>
                    ),
                )}
            </div>

            <div className="pt-1 flex text-gray-2 text-xs font-medium">
              {oversArr.map((val, i) => (
                <div
                  key={i}
                  className="justify-between flex text-end"
                  style={{
                    width: screenWidth - 75,
                  }}
                >
                  <p className={`${i === 0 ? 'pr-2' : ''}`}>
                    {i === 0 ? 'overs' : ''}
                  </p>
                  <p className="text-end">{val}</p>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="flex justify-start items-center mt-12 px-2 font-medium">
          <div className="flex items-center mr-3">
            <p className="w-2 h-2 rounded-full bg-gray-3"></p>
            <p className="text-gray-2 text-sm font-medium pl-1">
              {data.getPhaseOfInningsResolver.inningsPhase[team].teamShortName}{' '}
              {data.getPhaseOfInningsResolver.inningsPhase[team].totalRuns}/
              {data.getPhaseOfInningsResolver.inningsPhase[team].totalWickets}
            </p>
          </div>
          <div className="flex items-center ml-3">
            <p className="w-2 h-2 rounded-full bg-red"></p>
            <p className="text-gray-2 text-sm pl-1">Wicket</p>
          </div>
        </div>
      </div>
    </>
  ) : (
    <></>
  )
}
