import React, { useRef, useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'
import { format } from 'date-fns'
import Countdown from 'react-countdown-now'
import PlayerImpact from './playerimpact'
import RunComparison from './runComparison'
import GameChangingOver from './gameChanginOver'
import { EffectCoverflow, Pagination } from 'swiper'
import Homepage from '../homeScreen/homeslider'
export default function index(props) {
  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)
  const [swiper, updateSwiper] = useState(null)
  const [currentIndex, updateCurrentIndex] = useState(0)

  const router = useRouter()

  const routerTabName = router.query.id.slice(-1)[0]

  const indexTell = () => {
    if (routerTabName === 'player-impact') {
      return '0'
    }
    if (routerTabName === 'run-comparison') {
      return '1'
    }
    if (routerTabName === 'game-change-overs') {
      return '2'
    }
    if (routerTabName === 'match-reel') {
      return '3'
    }
    if (routerTabName === 'match-stat') {
      return '4'
    }
    if (routerTabName === 'phases-of-play') {
      return '5'
    }
  }

  const Bell = './pngsV2/bell.png'
  const location = './pngsV2/location.png'

  const swiperRef = useRef(null)

  // useEffect(() => {
  //   swiperRef.current = new Swiper(
  //       '.swiper-container', // You can also use a ref to the element here
  //       {
  //         loop: true,
  //         slidesPerView: '2',
  //         centeredSlides: true,
  //         pagination: {
  //           el: '.swiper-pagination',
  //           clickable: true,
  //         },
  //       },
  //   );

  //   return () => {
  //     swiperRef.current.destroy(true, true);
  //   };
  // }, []);

  useEffect(() => {
    updateCurrentIndex(indexTell())
    if (swiper !== null) {
      try {
        swiper.on('slideChange', () => {
          updateCurrentIndex(swiper.realIndex)
          // props.updateCurrentIndex(swiper.realIndex);
        })
        swiper.on('click', (swipeData) => {
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        })
      } catch (e) {
        console.log(e)
      }
    }
  }, [swiper, currentIndex])

  const components = [<PlayerImpact />, <RunComparison />]

  return (
    <div className="mx-2 mt-3 text-white w-full">
      <h1>{props.activeIndex}</h1>

      <Swiper
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        shouldSwiperUpdate={true}
        effect={'coverflow'}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={'1'}
        initialSlide={parseInt(props.activeIndex ? props.activeIndex : 4)}
        pagination={false}
        modules={[EffectCoverflow, Pagination]}
        className="mySwiper"
      >
        {components.map((component, i) => {
          return <SwiperSlide>{component}</SwiperSlide>
        })}
      </Swiper>

      {/* <Add index={props.activeIndex} /> */}
    </div>
  )
}

const Add = (props) => {
  return (
    <Swiper
      spaceBetween={50}
      centeredSlides={true}
      slidesPerView={1}
      initialSlide={props.index ? props.index : 3}
      onSlideChange={() => console.log('')}
      onSwiper={(swiper) => {
        alert(8)
      }}
    >
      <SwiperSlide>Slide 1</SwiperSlide>
      <SwiperSlide>Slide 2</SwiperSlide>
      <SwiperSlide>Slide 3</SwiperSlide>
      <SwiperSlide>Slide 4</SwiperSlide>
      <SwiperSlide>Slide 5</SwiperSlide>
      <SwiperSlide>Slide 6</SwiperSlide>
      <SwiperSlide>Slide 7</SwiperSlide>
    </Swiper>
  )
}
