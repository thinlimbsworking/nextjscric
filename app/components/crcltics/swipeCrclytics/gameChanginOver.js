import React from 'react'
import GameChangeView from '../gameChangingOvers'
import { useRouter } from 'next/navigation'

export default function GameChangingOver(props) {
  const router = useRouter()
  // console.log(">>>>>>",router.query.id[0])
  return (
    <div className="mx-1">
      {/* <div className='mx-2'>
          <div className='text-md  text-white tracking-wide font-bold '> Game Changing Overs </div>
          <div className='text-xs text-white pt-1 font-medium tracking-wide'>
            {' '}
            Forecast of where the match is headed, in the next few overs{' '}
          </div>
          <div className='w-12 h-1 bg-blue-8 mt-2'></div>
        </div> */}
      <GameChangeView matchID={props?.matchID} props={props} />
    </div>
  )
}
