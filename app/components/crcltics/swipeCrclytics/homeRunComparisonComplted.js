import React, { useState } from 'react'
import WormChart from '../wormChartHome'
import ScoringChartCompleted from '../homeScoringcharComplete'
import Heading from '../../commom/heading'
import Tab from '../../shared/Tab'
import RunRate from '../homeRunRate'
import DataNotFound from '../../commom/datanotfound'

const TabData = [
  { name: 'Worm Chart', value: 'Worm Chart' },
  { name: 'Scoring Chart', value: 'Scoring Chart' },
  { name: 'Run Rate', value: 'Run Rate' },
]

export default function runComparisonCompleted(props) {
  const [toggle2, setToggle2] = useState('worm')
  const [currentTab, setCurrentTab] = useState({
    name: 'Worm Chart',
    value: 'Worm Chart',
  })

  return (
    <div>
      {true&& props?.data?.scoreChart?.inningsPhase?.length > 0&& (
        <div className={`  ${props.matchStatus=== "live" ?'m-2 ': 'm-2 my-5'} `}>
          <div className="overflow-hidden  dark:bg-gray rounded-md dark:text-white lg:text-black lg:bg-white lg:border-[#E2E2E2] lg:border-2 lg:border-solid">
            <div className=" flex px-2 py-3">
              <Heading heading={'Graphs'} hideBottom={true} />

              
              
            </div>

            <div className="flex rounded-3xl items-center justify-between text-sm dark:bg-gray-8 lg:bg-white mx-3 lg:mx-20 md:mx-20">
              <Tab
                type="outline"
                data={TabData}
                handleTabChange={(item) => setCurrentTab(item)}
                selectedTab={currentTab}
              />
            </div>
            {currentTab.value == 'Worm Chart' ? (
              <WormChart
                cricltics={true}
                data={props.data}
                matchType={props?.matchType}
                matchID={props?.matchID}
              />
            ) : currentTab.value == 'Scoring Chart' ? (
              <ScoringChartCompleted
                cricltics={true}
                data={props.data}
                matchType={props?.data?.scoreChart?.format}
                matchID={props?.matchID}
              />
            ) : (
              <RunRate
                data={props.data}
                cricltics={true}
                matchType={props?.matchType}
                matchID={props?.matchID}
              />
            )}
          </div>
        </div>  
      ) }
    </div>
  )
}
