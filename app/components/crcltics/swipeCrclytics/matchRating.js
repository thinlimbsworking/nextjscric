import { useQuery } from '@apollo/react-hooks'
import React, { useEffect, useState } from 'react'
import DataNotFound from '../../../components/commom/datanotfound'
import { PLAYER_IMPACT } from '../../../api/queries'
import Loading from '../../loading'
import Heading from '../../shared/heading'
import Tab from '../../shared/Tab'
import BatIcon from '../../commom/baticon'
import BowlIcon from '../../commom/bowlicon'
const location = '/svgs/location-icon-color.png'
const information = '/svgs/information.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const ground = '/svgs/groundImageWicket.png'
const playerAvatar = '/placeHodlers/playerAvatar.png'
const bat = '/pngsV2/battericon.png'
const ball = '/pngsV2/bowlericon.png'

export default function MatchRating({ browser, ...props }) {
  const { error, loading, data: playerImpact } = useQuery(PLAYER_IMPACT, {
    variables: { matchID: props.matchID },
    onCompleted: (_playerImpact) => {
      setActiveTab({
        name: _playerImpact?.getPostMatchImpact?.team1Name || '',
        value: _playerImpact?.getPostMatchImpact?.team1Name || '',
      })
    },
  })
  const TabData = [
    {
      name: playerImpact?.getPostMatchImpact?.team1Name,
      value: playerImpact?.getPostMatchImpact?.team1Name,
    },
    {
      name: playerImpact?.getPostMatchImpact?.team2Name,
      value: playerImpact?.getPostMatchImpact?.team2Name,
    },
  ]

  const [activeTab, setActiveTab] = useState({
    name: playerImpact?.getPostMatchImpact?.team1Name,
    value: playerImpact?.getPostMatchImpact?.team1Name,
  })

  function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals).toFixed(
      decimals,
    )
  }

  if (loading) {
    return <Loading />
  }
  if (playerImpact) {
    return playerImpact?.getPostMatchImpact?.team1?.length ? (
      <>
        <div className="md:pl-3 pl-1">
          <Heading
            heading={'Player Impact'}
            subHeading={
              'An overview of individual players impact during the course of the match'
            }
          />
        </div>
        <div className="w-4/12 ml-1 py-2 dark:w-9/12 flex items-center justify-center dark:ml-12">
          <Tab
            data={TabData}
            type="block"
            handleTabChange={(item) => setActiveTab(item)}
            selectedTab={activeTab}
          />
        </div>
        <div className="md:ml-1 md:bg-white md:p-3 shadow rounded-md dark:border-basebg">
          {}
          <div className="md:static dark:mx-1">
            <div className="flex py-2 mt-4 bg-gray-4 shadow-4 justify-between items-center rounded-t-xl text-xs">
              <div className="flex md:w-4/12 w-full">
                <h2 className="text-xs ml-6 font-semibold dark:text-gray-3 text-white">
                  Player{' '}
                </h2>
              </div>

              {playerImpactTag?.map((list) => (
                <div className="flex items-center w-9/12 md:w-36 dark:text-gray-3 text-white py-2 rounded-sm justify-center text-center text-xs font-semibold">
                  {list?.label}
                </div>
              ))}
            </div>

            {activeTab?.value == playerImpact?.getPostMatchImpact?.team1Name ? (
              <div className="border border-y-solid border-y-gray-11 dark:border-gray-4">
                {playerImpact?.getPostMatchImpact?.team1?.map(
                  (player, index) => {
                    return (
                      <div className="">
                        <div
                          className="
                    flex py-1 lg:my-4 text-xs font-medium justify-between items-center px-1 dark:bg-gray-8 dark:my-4 bg-white"
                        >
                          <div className="flex dark:w-3/12 w-4/12 items-center break-words">
                            {' '}
                            <div className="flex items-center justify-between mx-2"></div>
                            <div className="flex dark:text-white text-black">
                              {player?.playerName}
                            </div>
                          </div>

                          <div
                            className={`flex items-center justify-center md:w-36 ${
                              player?.batting_impact < 0
                                ? 'text-red'
                                : 'text-green'
                            }`}
                          >
                            {round(player?.batting_impact, 2)}
                          </div>

                          <div
                            className={`flex items-center justify-center text-xs dark:w-2/12 md:w-36 ${
                              player?.bowling_impact < 0
                                ? 'text-red'
                                : 'text-green'
                            } text-xs justify-center 
                        `}
                          >
                            {round(player?.bowling_impact, 2)}
                          </div>

                          <div
                            className={`flex items-center md:w-36 dark:w-2/12 justify-center ${
                              player?.total_impact < 0
                                ? 'text-red'
                                : 'text-green'
                            }  text-xs md:pl-2`}
                          >
                            {round(player?.total_impact, 2)}
                          </div>
                        </div>
                      </div>
                    )
                  },
                )}

                <div className="flex gap-3 md:hidden py-3 bg-gray-4">
                  <BatIcon clr={'#5F92AC'} height={20} className={'ml-1'} />
                  <span className="text-xs">Batting Impact</span>
                  <BowlIcon clr={'#5F92AC'} height={16} />
                  <span className="text-xs">Bowling Impact</span>
                </div>
              </div>
            ) : (
              <>
                <div className="border border-y-solid border-gray-11 dark:border-gray-4">
                  {playerImpact?.getPostMatchImpact?.team2?.map(
                    (player, index) => {
                      return (
                        <div className="">
                          <div
                            className="
                        flex py-1 text-xs lg:my-4 font-medium justify-between items-center px-1 dark:bg-gray-8 dark:my-4 bg-white"
                          >
                            <div className="flex w-4/12 dark:w-3/12 items-center break-words">
                              {' '}
                              <div className="flex items-center justify-between mx-2"></div>
                              <div className="flex dark:text-white text-black">
                                {player?.playerName}
                              </div>
                            </div>

                            <div
                              className={`flex items-center justify-center md:w-36 ${
                                player?.batting_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              }`}
                            >
                              {round(player?.batting_impact, 2)}
                            </div>

                            <div
                              className={`flex items-center justify-center dark:w-2/12 md:w-36 text-xs ${
                                player?.bowling_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              } 
                      `}
                            >
                              {round(player?.bowling_impact, 2)}
                            </div>

                            <div
                              className={`flex items-center md:w-36 justify-center ${
                                player?.total_impact < 0
                                  ? 'text-red'
                                  : 'text-green'
                              }  text-xs dark:w-2/12 md:pl-2`}
                            >
                              {round(player?.total_impact, 2)}
                            </div>
                          </div>
                        </div>
                      )
                    },
                  )}
                  <div className="flex gap-3 md:hidden py-3 bg-gray-4">
                    <BatIcon clr={'#5F92AC'} height={20} className={'ml-1'} />
                    <span className="text-xs">Batting Impact</span>
                    <BowlIcon clr={'#5F92AC'} height={16} />
                    <span className="text-xs">Bowling Impact</span>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </>
    ) : (
      <div>
        <DataNotFound />
      </div>
    )
  }
}

const playerImpactTag = [
  {
    label: <BatIcon clr={'#5F92AC'} height={20} />,
    key: 'battingImpact',
  },
  {
    label: <BowlIcon clr={'#5F92AC'} height={16} />,
    key: 'bowlingImpact',
  },
  {
    label: 'Net Impact',
    key: 'netImpact',
  },
]
