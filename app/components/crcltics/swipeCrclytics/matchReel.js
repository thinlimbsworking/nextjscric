import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { LIVE_SCORE_PREDICTOR } from '../../../api/queries'

import Slider from 'rc-slider'
import Tooltip from 'rc-tooltip'
import Heading from '../../commom/heading'
const Handle = Slider.Handle
const play = '/pngsV2/playcric.png'
const pause = '/pngsV2/pausebutton.png'
const empty = '/pngsV2/empty.png'

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value + 1}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  )
}

export default function teamScoreProjection(props) {
  const [maxRun, setMaxRun] = useState()
  const [index, setIndex] = useState(0)
  const [button, setbutton] = useState(false)
  const [click, setclick] = useState(false)

  const { loading, error, data } = useQuery(LIVE_SCORE_PREDICTOR, {
    variables: { matchId: props.matchID, matchType: props.matchType },
    pollInterval: 8000,
    onCompleted: (data) => {
      if (data.liveScorePredictor.liveScores.length > 0) {
        setIndex(data.liveScorePredictor.liveScores.length - 1)
        setMaxRun(
          Math.max(
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].predictedScore,
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].secondPredictedScore,
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].thirdPredictedScore,
            data.liveScorePredictor.liveScores[
              data.liveScorePredictor.liveScores.length - 1
            ].fourthPredictedScore,
          ),
        )
      }
    },
  })
  let mytime = 0
  click &&
    (mytime = setTimeout(function () {
      index === data.liveScorePredictor.liveScores.length - 1
        ? (clearTimeout(mytime), setclick(!click), setbutton(false))
        : setIndex(index + 1)
    }, 500))
  //

  return (
    <div className="w-full">
      {props.matchStatus === 'completed' ? (
        <div className="mx-3 my-2">
          <Heading
            heading={'Match Reel'}
            subHeading={
              ' A prediction of final scores of the match that also shows how fortunes of the teams have swung until this moment.'
            }
          />
          {/* <div className='text-md  text-white tracking-wide font-bold '> Match Reel </div>
        <div className='text-xs text-white pt-1 font-medium tracking-wide'>
          {' '}
          An over by over breakdown of projected scores during the match{' '}
        </div>
        <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        </div>
      ) : (
        <div className="ml-2 mb-3">
          <Heading
            heading={'Team Score Projection'}
            subHeading={
              'A prediction of final scores of the match that also shows how fortunes of the teams have swung until this moment.'
            }
          />
          {/* <div className='text-md  text-white tracking-wide font-bold '> Team Score Projection </div>
        <div className='text-xs text-white pt-1 font-medium tracking-wide'>
          {' '}
          2A prediction of final scores if the match that also shows how fortunes of the teams have swung until this moment{' '}
        </div>
        <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        </div>
      )}

      {data &&
      data.liveScorePredictor &&
      data.liveScorePredictor.liveScores &&
      data.liveScorePredictor.liveScores.length > 0 ? (
        <div className="dark:bg-gray bg-white shadow md:rounded-md md:mt-5 mx-2 p-3 text-white">
          <div>
            <p className="font-semibold text-md md:hidden tracking-wide md:text-xl lg:text-xl ">
              Team Score Projection
            </p>
            <div className="flex flex-col">
              <div className="flex justify-center dark:justify-between items-center mt-4">
                <div className="flex items-center">
                  <img
                    className="dark:w-8 w-32 dark:h-5 h-16 rounded-sm"
                    src={`https://images.cricket.com/teams/${
                      data.liveScorePredictor.liveScores &&
                      data.liveScorePredictor.liveScores[index].team1Id
                    }_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = '/pngsV2/flag_dark.png')
                    }
                  />
                  <p className="px-2 text-md dark:text-white text-black dark:font-semibold font-normal">
                    {data.liveScorePredictor.liveScores[index].team1ShortName}
                  </p>
                </div>
                <div className="w-20 h-0.5 bg-basebg relative object-center flex justify-center">
                  <div className="dark:w-6 dark:h-6 w-9 h-9 bg-basebg md:-mt-4 -mt-3 rounded-full text-xs font-semibold flex items-center justify-center">
                    vs
                  </div>
                </div>
                <div className="flex items-center">
                  <p className="px-2 text-md dark:text-white text-black dark:font-semibold font-normal">
                    {data.liveScorePredictor.liveScores[index].team2ShortName}
                  </p>
                  <img
                    className="dark:w-8 w-32 dark:h-5 h-16 rounded-sm text-md"
                    src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team2Id}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = '/pngsV2/flag_dark.png')
                    }
                  />
                </div>
              </div>
              {data.liveScorePredictor.liveScores[index] &&
                data.liveScorePredictor.liveScores[index].inningIds &&
                data.liveScorePredictor.liveScores[index].inningIds.length ==
                  2 && (
                  <div className="mt-4 flex md:mx-28 md:justify-evenly justify-between items-center">
                    <div className="flex items-center text-md font-medium">
                      <p className="dark:pr-2 dark:text-white text-black">
                        {data.liveScorePredictor.liveScores[index].inningNo ==
                          1 &&
                        data.liveScorePredictor.liveScores[index]
                          .inningIds[0] ==
                          data.liveScorePredictor.liveScores[index].team1Id
                          ? data.liveScorePredictor.liveScores[index]
                              .currentScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .currentWickets
                          : data.liveScorePredictor.liveScores[index]
                              .predictedScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .predictedWicket}
                      </p>
                      <p className="font-normal dark:text-white text-black text-sm">
                        {data.liveScorePredictor.liveScores[index].inningNo ==
                          1 &&
                        data.liveScorePredictor.liveScores[index]
                          .inningIds[0] ==
                          data.liveScorePredictor.liveScores[index].team1Id
                          ? '(' +
                            data.liveScorePredictor.liveScores[index]
                              .currentOvers +
                            ')'
                          : '(' +
                            data.liveScorePredictor.liveScores[index]
                              .predictedOver +
                            ')'}
                      </p>
                    </div>
                    <div
                      className={`${
                        data.liveScorePredictor.liveScores[index].inningNo ==
                          1 &&
                        data.liveScorePredictor.liveScores[index]
                          .inningIds[0] ==
                          data.liveScorePredictor.liveScores[index].team1Id
                          ? 'text-gray-2 text-sm'
                          : 'text-white text-md'
                      }  font-medium flex items-center`}
                    >
                      <p className="dark:text-white text-black">
                        {data.liveScorePredictor.liveScores[index].inningNo ==
                          1 &&
                        data.liveScorePredictor.liveScores[index]
                          .inningIds[0] ==
                          data.liveScorePredictor.liveScores[index].team1Id ? (
                          <span className="text-gray-2">Yet to Bat</span>
                        ) : data.liveScorePredictor.liveScores[index]
                            .inningNo == 2 &&
                          data.liveScorePredictor.liveScores[index]
                            .inningIds[1] ==
                            data.liveScorePredictor.liveScores[index]
                              .team2Id ? (
                          data.liveScorePredictor.liveScores[index]
                            .currentScore +
                          '/' +
                          data.liveScorePredictor.liveScores[index]
                            .currentWickets
                        ) : (
                          data.liveScorePredictor.liveScores[index]
                            .secondPredictedScore +
                          '/' +
                          data.liveScorePredictor.liveScores[index]
                            .secondPredictedWicket
                        )}
                      </p>
                      <p className="font-normal dark:text-white text-black text-sm">
                        {data.liveScorePredictor.liveScores[index].inningNo ==
                          1 &&
                        data.liveScorePredictor.liveScores[index]
                          .inningIds[0] ==
                          data.liveScorePredictor.liveScores[index].team1Id
                          ? ''
                          : data.liveScorePredictor.liveScores[index]
                              .inningNo == 2 &&
                            data.liveScorePredictor.liveScores[index]
                              .inningIds[1] ==
                              data.liveScorePredictor.liveScores[index].team2Id
                          ? '(' +
                            data.liveScorePredictor.liveScores[index]
                              .currentOvers +
                            ')'
                          : '(' +
                            data.liveScorePredictor.liveScores[index]
                              .secondPredictedOVer +
                            ')'}
                      </p>
                    </div>
                  </div>
                )}
            </div>

            {/* --------------------------------------------------For more then 2 innings including test matches and superover--------------------------------- */}

            {data.liveScorePredictor.liveScores[index] &&
              data.liveScorePredictor.liveScores[index].inningIds &&
              data.liveScorePredictor.liveScores[index].inningIds.length > 2 &&
              props.matchType == 'Test' && (
                <div className="mt-4 flex lg:justify-center md:justify-center justify-between lg:gap-44 md:gap-44 items-center">
                  <div className="flex items-center text-md font-medium">
                    <p className="dark:text-white text-black">
                      {data.liveScorePredictor.liveScores[index].inningNo ==
                        '1' ||
                      data.liveScorePredictor.liveScores[index].inningNo == '3'
                        ? data.liveScorePredictor.liveScores[index]
                            .currentScore != ''
                          ? data.liveScorePredictor.liveScores[index]
                              .currentScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .currentWickets
                          : ''
                        : data.liveScorePredictor.liveScores[index].inningNo ==
                          '2'
                        ? data.liveScorePredictor.liveScores[index]
                            .predictedScore != '--'
                          ? data.liveScorePredictor.liveScores[index]
                              .predictedScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .predictedWicket
                          : ''
                        : data.liveScorePredictor.liveScores[index]
                            .thirdPredictedScore != '--'
                        ? data.liveScorePredictor.liveScores[index]
                            .thirdPredictedScore +
                          '/' +
                          data.liveScorePredictor.liveScores[index]
                            .thirdPredictedWicket
                        : ''}
                    </p>
                    <p className="font-normal text-sm">
                      {data.liveScorePredictor.liveScores[index].inningNo ==
                        '1' ||
                      data.liveScorePredictor.liveScores[index].inningNo == '3'
                        ? data.liveScorePredictor.liveScores[index]
                            .currentOvers != ''
                          ? '(' +
                            data.liveScorePredictor.liveScores[index]
                              .currentOvers +
                            ')'
                          : ''
                        : data.liveScorePredictor.liveScores[index].inningNo ==
                          '2'
                        ? '(' +
                          data.liveScorePredictor.liveScores[index]
                            .predictedScore +
                          ')'
                        : '(' +
                          data.liveScorePredictor.liveScores[index]
                            .thirdPredictedOver +
                          ')'}
                    </p>
                  </div>
                  <div className="text-gray-2 text-sm font-medium flex items-center">
                    <p className="dark:text-white text-black">
                      {data.liveScorePredictor.liveScores[index].inningNo ==
                        '1' ||
                      data.liveScorePredictor.liveScores[index].inningNo == '3'
                        ? 'Yet to Bat'
                        : data.liveScorePredictor.liveScores[index].inningNo ==
                            '2' ||
                          data.liveScorePredictor.liveScores[index].inningNo ==
                            '4'
                        ? data.liveScorePredictor.liveScores[index].currentScore
                          ? data.liveScorePredictor.liveScores[index]
                              .currentScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .currentWickets
                          : ''
                        : ''}
                    </p>
                    <p className="font-normal text-sm">
                      {data.liveScorePredictor.liveScores[index].inningNo ==
                        '1' ||
                      data.liveScorePredictor.liveScores[index].inningNo == '3'
                        ? ''
                        : data.liveScorePredictor.liveScores[index].inningNo ==
                            '2' ||
                          data.liveScorePredictor.liveScores[index].inningNo ==
                            '2'
                        ? '(' +
                          data.liveScorePredictor.liveScores[index]
                            .currentOvers +
                          ')'
                        : ''}
                    </p>
                  </div>
                </div>
              )}

            <div className="lg:flex md:flex lg:justify-between md:justify-between lg:gap-6 md:gap-6 md:mx-4 lg:mx-4">
              {data.liveScorePredictor.liveScores[index].inningNo <= 2 && (
                <div className="dark:bg-gray-4 bg-[#EEEFF2] p-2 py-3 lg:pb-8 md:pb-8 mt-3 rounded-md lg:w-6/12 md:w-6/12">
                  <p className="text-gray-2 text-xs font-medium">
                    PROJECTED SCORES
                  </p>
                  {data.liveScorePredictor.liveScores[index].inningNo == '1' &&
                    data.liveScorePredictor.liveScores[index].predictedScore !=
                      '--' && (
                      <div className="mt-3 flex items-center justify-between w-full lg:mt-4 md:mt-4">
                        <img
                          className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm"
                          src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team1Id}_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/flag_dark.png')
                          }
                        />
                        <div className="w-9/12 ml-1 overflow-hidden rounded-full">
                          <div
                            className="bg-gray-2 dark:bg-green rounded h-2"
                            style={{
                              width: `${
                                (data.liveScorePredictor.liveScores[index]
                                  .predictedScore /
                                  maxRun) *
                                100
                              }%`,
                            }}
                          ></div>
                        </div>
                        <p className="text-sm font-medium tracking-wider lg:text-black md:text-black">
                          {data.liveScorePredictor.liveScores[index]
                            .predictedScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .predictedWicket}
                        </p>
                      </div>
                    )}
                  {data.liveScorePredictor.liveScores[index].inningNo <= 2 &&
                    data.liveScorePredictor.liveScores[index]
                      .secondPredictedScore != '--' && (
                      <div className="mt-3 flex items-center justify-between w-100 lg:mt-4 md:mt-4">
                        <img
                          className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm"
                          src={`https://images.cricket.com/teams/${data.liveScorePredictor.liveScores[index].team2Id}_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/flag_dark.png')
                          }
                        />
                        <div className="w-9/12 ml-1 overflow-hidden rounded-md">
                          <div
                            className="md:bg-[#309F2A] bg-cyan-700 rounded h-2"
                            style={{
                              width: `${
                                (data.liveScorePredictor.liveScores[index]
                                  .secondPredictedScore /
                                  (maxRun + 20)) *
                                100
                              }%`,
                            }}
                          ></div>
                        </div>
                        <p className="text-sm font-medium tracking-wider md:text-black">
                          {data.liveScorePredictor.liveScores[index]
                            .secondPredictedScore +
                            '/' +
                            data.liveScorePredictor.liveScores[index]
                              .secondPredictedWicket}
                        </p>
                      </div>
                    )}
                </div>
              )}
              {data.liveScorePredictor.liveScores[index] &&
                data.liveScorePredictor.liveScores[index].inningIds &&
                data.liveScorePredictor.liveScores[index].inningIds.length >
                  2 && (
                  <div className="dark:bg-gray-4 bg-[#EEEFF2] p-2 py-3 mt-3 lg:pb-8 md:pb-8 lg:w-6/12 md:w-6/12 rounded-md">
                    <p className="text-gray-2 text-xs font-medium">
                      PROJECTED SCORES
                    </p>
                    {data.liveScorePredictor.liveScores[index].inningNo <= 3 &&
                      data.liveScorePredictor.liveScores[index]
                        .thirdPredictedScore != '' && (
                        <div className="mt-3 flex items-center justify-between w-100 lg:mt-4 md:mt-4">
                          <img
                            className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm mr-1"
                            src={`https://images.cricket.com/teams/${
                              data.liveScorePredictor.liveScores[index]
                                .inningIds[2] ==
                              data.liveScorePredictor.liveScores[index].team1Id
                                ? data.liveScorePredictor.liveScores[index]
                                    .team1Id
                                : data.liveScorePredictor.liveScores[index]
                                    .team2Id
                            }_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/pngsV2/flag_dark.png')
                            }
                          />
                          <div className="w-9/12 ml-1">
                            <div
                              className="dark:bg-green bg-[#309F2A] rounded h-2"
                              style={{
                                width: `${
                                  (data.liveScorePredictor.liveScores[index]
                                    .thirdPredictedScore /
                                    (maxRun + 20)) *
                                  100
                                }%`,
                              }}
                            ></div>
                          </div>

                          <p
                            className="text-sm font-medium tracking-wider lg:text-black md:text-black text-right"
                            style={{ width: '30%' }}
                          >
                            {
                              data.liveScorePredictor.liveScores[index]
                                .thirdPredictedScore
                            }
                            {/* {data && data.liveScorePredictor && data.liveScorePredictor.liveScores?.[index]
                              ?.thirdPredictedWicket
                              ? (
                              <>
                             <span>'/'</span>
                              {
                                data.liveScorePredictor.liveScores[index]
                                  .thirdPredictedWicket}
                             
                              </>):(
                              
                              ) */}
                            {data.liveScorePredictor.liveScores[index]
                              .thirdPredictedWicket ? (
                              <>
                                /
                                {
                                  data.liveScorePredictor.liveScores[index]
                                    .thirdPredictedWicket
                                }
                              </>
                            ) : (
                              <span className="flex items-center justify-center text-xs">
                                Yet to bat
                              </span>
                            )}
                          </p>
                        </div>
                      )}
                    {data.liveScorePredictor.liveScores[index].inningNo <= 4 &&
                      data.liveScorePredictor.liveScores[index]
                        .fourthPredictedScore != '' &&
                      data.liveScorePredictor.liveScores[index].inningIds
                        .length > 3 && (
                        <div className="mt-3 flex items-center justify-between w-100 lg:mt-4 md:mt-4">
                          <img
                            className="dark:w-6 dark:h-4 w-12 h-8 rounded-sm mr-1"
                            src={`https://images.cricket.com/teams/${
                              data.liveScorePredictor.liveScores[index]
                                .inningIds[3] ==
                              data.liveScorePredictor.liveScores[index].team1Id
                                ? data.liveScorePredictor.liveScores[index]
                                    .team1Id
                                : data.liveScorePredictor.liveScores[index]
                                    .team2Id
                            }_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/pngsV2/flag_dark.png')
                            }
                          />
                          <div className="w-9/12 ml-1">
                            <div
                              className="bg-gray-2 rounded h-2"
                              style={{
                                width: `${
                                  (data.liveScorePredictor.liveScores[index]
                                    .fourthPredictedScore /
                                    (maxRun + 20)) *
                                  100
                                }%`,
                              }}
                            ></div>
                          </div>
                          <p
                            className="text-sm font-medium tracking-wider md:text-black text-right"
                            style={{ width: '30%' }}
                          >
                            {
                              data.liveScorePredictor.liveScores[index]
                                .fourthPredictedScore
                            }
                            {data.liveScorePredictor.liveScores[index]
                              .fourthPredictedScore ? (
                              <>
                                /
                                {
                                  data.liveScorePredictor.liveScores[index]
                                    .fourthPredictedWicket
                                }
                              </>
                            ) : (
                              <span className="flex items-center justify-center text-xs">
                                Yet to bat
                              </span>
                            )}
                          </p>
                        </div>
                      )}
                  </div>
                )}
              <div className="dark:bg-gray-4 bg-[#EEEFF2] p-2 lg:pb-12 md:pb-12 py-3 mt-3 lg:w-6/12 md:w-6/12 rounded-md">
                <p className="text-gray-2 text-xs font-medium">
                  PROJECTED RESULT
                </p>
                <p className="md:text-green text-blue dark:mt-1 font-semibold text-sm mt-6">
                  {data.liveScorePredictor.liveScores[index].projected_result}
                </p>
              </div>
            </div>
          </div>
          <div className="mt2">
            {data.liveScorePredictor.liveScores.length > 0 &&
              data.liveScorePredictor.liveScores[
                data.liveScorePredictor.liveScores.length - 1
              ].winvizView !== null && (
                <div>
                  <div className="flex-l md:mx-4 lg:mx-4 md:mt-3 justify-between items-center">
                    <div className=" w-50-l">
                      {data.liveScorePredictor.liveScores[index]
                        .currentView && (
                        <>
                          <div className="text-gray-2 uppercase text-xs font-medium my-2">
                            Win percentage
                          </div>

                          <div className="flex py-1 justify-center items-center">
                            <div
                              style={{
                                width: `${
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView &&
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView.homeTeamPercentage
                                }%`,
                                height: 10,
                              }}
                              className={` dib bg-green rounded-l ${
                                data.liveScorePredictor.liveScores[index]
                                  .currentView.homeTeamPercentage == 100
                                  ? 'rounded-r'
                                  : ''
                              }`}
                            ></div>
                            {data.liveScorePredictor.liveScores[index]
                              .currentView &&
                            parseInt(
                              data.liveScorePredictor.liveScores[index]
                                .currentView.tiePercentage,
                            ) > 0 ? (
                              <div
                                className=" dib bg-gray-3"
                                style={{
                                  width: `${data.liveScorePredictor.liveScores[index].currentView.tiePercentage}%`,
                                  height: 10,
                                }}
                              />
                            ) : (
                              <div />
                            )}
                            <div
                              style={{
                                width: `${
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView &&
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView.awayTeamPercentage
                                }%`,
                                height: 10,
                              }}
                              className={`dib dark:bg-white bg-[#B7B7B7] rounded-r ${
                                data.liveScorePredictor.liveScores[index]
                                  .currentView.awayTeamPercentage == 100
                                  ? 'rounded-l'
                                  : ''
                              }`}
                            ></div>
                          </div>
                          <div className="flex flex-row justify-between font-semibold text-xs text-gray-2 pt-1">
                            <div className="flex items-center">
                              <div className="h-1.5 w-1.5 rounded-full bg-green"></div>
                              <div className="px-1">
                                {data.liveScorePredictor.liveScores[index]
                                  .currentView &&
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView.homeTeamShortName}
                              </div>
                              <div className="tracking-wider font-medium">
                                (
                                {
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView.homeTeamPercentage
                                }
                                %)
                              </div>
                            </div>
                            {data.liveScorePredictor.liveScores[index]
                              .currentView &&
                              data.liveScorePredictor.liveScores[index]
                                .currentView.tiePercentage &&
                              parseInt(
                                data.liveScorePredictor.liveScores[index]
                                  .currentView.tiePercentage,
                              ) > 0 && (
                                <div className="flex items-center ">
                                  <div className="h-1.5 w-1.5 rounded-full bg-gray-3"></div>

                                  {props.matchType === 'Test' ? (
                                    <div className="px-1">DRAW</div>
                                  ) : (
                                    <div className="px-1">Tie</div>
                                  )}
                                  <div className="tracking-wider font-medium">
                                    (
                                    {
                                      data.liveScorePredictor.liveScores[index]
                                        .currentView.tiePercentage
                                    }
                                    %)
                                  </div>
                                </div>
                              )}
                            <div className="flex items-center">
                              <div className="h-1.5 w-1.5 rounded-full bg-white"></div>

                              <div className="px-1">
                                {data.liveScorePredictor.liveScores[index]
                                  .currentView &&
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView.awayTeamShortName}
                              </div>
                              <div className="tracking-wider font-medium">
                                (
                                {
                                  data.liveScorePredictor.liveScores[index]
                                    .currentView.awayTeamPercentage
                                }
                                %)
                              </div>
                            </div>
                          </div>
                        </>
                      )}
                    </div>
                  </div>

                  <div className="dark:pa2 lg:mt-6 md:mt-6">
                    <div className="flex items-center justify-between justify-around-l ">
                      <img
                        src={button ? pause : play}
                        alt=""
                        className="h-12 md:cursor-pointer w-12 m-2"
                        onClick={() => (
                          index ===
                          data.liveScorePredictor.liveScores.length - 1
                            ? setIndex(0)
                            : '',
                          setbutton(!button),
                          setclick(!click)
                        )}
                      />
                      <div className="w-full pl-2 flex h-10 dark:bg-basebg bg-[#EEEFF2] justify-center items-center p-1 rounded">
                        <Slider
                          className="slider-main w-full"
                          max={data.liveScorePredictor.liveScores.length - 1}
                          min={0}
                          step={1}
                          value={index}
                          onChange={(val) => {
                            if (
                              val !==
                                data.liveScorePredictor.liveScores.length - 1 &&
                              val <=
                                data.liveScorePredictor.liveScores.length - 1 &&
                              !button
                            ) {
                              setIndex(val)
                            }
                          }}
                          handle={handle}
                          handleStyle={[
                            {
                              backgroundColor: 'white',
                              border: 'white',
                              width: '18px',
                              height: '18px',
                              marginTop: '-8px',
                              boxShadow:
                                '0 2px 7px 0 rgba(162, 167, 177, 0.51)',
                            },
                          ]}
                          trackStyle={[
                            { backgroundColor: '#38d925', height: '5px' },
                          ]}
                          railStyle={{
                            backgroundColor: 'gray',
                            height: '5px',
                          }}
                        />
                      </div>
                    </div>
                    <div className="text-center flex md:text-gray-2 md:gap-3 justify-start items-center">
                      {props.matchType === 'Test' && (
                        <div className="">
                          INN{' '}
                          {data.liveScorePredictor.liveScores[index].inningNo}{' '}
                        </div>
                      )}
                      <div className="px-2 md:text-gray-2">
                        {data.liveScorePredictor.liveScores[index].inningIds[
                          data.liveScorePredictor.liveScores[index].inningNo - 1
                        ] === data.liveScorePredictor.liveScores[index].team1Id
                          ? data.liveScorePredictor.liveScores[index]
                              .team1ShortName
                          : data.liveScorePredictor.liveScores[index]
                              .team2ShortName}{' '}
                        BAT
                      </div>
                      {}
                      {data.liveScorePredictor.liveScores[index].overNo > 0 && (
                        <div>
                          {/* <div className="w-4 h-4 rounded bg-gray-4"></div> */}
                          OVR {data.liveScorePredictor.liveScores[index].overNo}
                        </div>
                      )}
                    </div>
                    {}
                    <div className="flex items-end md:text-gray-2 justify-end -mt-6">
                      {/* <div className="w-4 h-4 rounded bg-gray-4"></div> */}
                      {data?.liveScorePredictor?.matchNumber || 'Match'}
                    </div>
                  </div>
                </div>
              )}
          </div>
        </div>
      ) : (
        <div>
          <div className="w-full flex justify-center ">
            <img className=" w-44" src={empty} />
          </div>
          <div className="text-center text-sm font-semibold pt-2 dark:text-white text-black">
            Data not Available
          </div>
        </div>
      )}
    </div>
  )
}
