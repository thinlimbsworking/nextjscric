import { useQuery } from '@apollo/react-hooks'
import React, { useEffect, useState } from 'react'
import { GET_MATCH_STATS } from '../../../api/queries'
import DataNotFound from '../../commom/datanotfound'
import Heading from '../../commom/heading'
import Loading from '../../loading'
const ground = '/svgs/groundImageWicket.png'
const information = '/svgs/information.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'

export default function MatchStats({ browser, ...props }) {
  const { data, loading } = useQuery(GET_MATCH_STATS, {
    variables: {
      matchID: props?.matchID,
    },
  })
  //
  const [infoToggle, setInfoToggle] = useState(false)

  const [innerWidth, updateInnerWidth] = useState(0)

  useEffect(() => {
    updateInnerWidth(window.innerWidth)
  }, [])
  {
  }
  const localStorage = {}

  const statsArr = [
    { key: 'highestBattingScore', value: 'Highest Score' },
    { key: 'highestPartnership', value: 'Highest Partnership' },
    { key: 'totalFours', value: '4s' },
    { key: 'totalSix', value: '6s' },
    { key: 'runsScoredInBoundaries', value: 'Runs Scored in Boundaries' },
    { key: 'totalDotBalls', value: 'Number of Dot Balls Faced' },
    { key: 'highestWickets', value: 'Best Bowling' },
    { key: 'extras', value: 'Extras Conceded' },
    { key: 'runRate', value: 'Run Rate in Inning' },
    { key: 'runRateInPowerplay1_6', value: 'Run Rate - Powerplay' }, // for T20
    { key: 'runRateInPowerplay1_10', value: 'Run Rate - Powerplay' }, // For ODI
    { key: 'runRateInPowerplay11_40', value: 'Run Rate in Mid-Over' }, // For ODI
    { key: 'runRateDeathOver', value: 'Run Rate - Death Over' },
  ]

  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }
  if (!data)
    return (
      <div>
        <div className="py-3 mt-2">
          <div>
            <img
              className="w45-m h45-m w-4 h-4 w5-l h5-l"
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt="loading..."
            />
          </div>
          <div className="text-center py-2 f5 dark:text-white text-black fw5">
            Data Not Available
          </div>
        </div>
      </div>
    )
  if (loading) {
    return <Loading />
  }

  if (data) {
    return (
      <div className="ml-3 dark:border-none">
        <div className="mt-2 mb-6">
          <Heading
            heading={'Match Stats'}
            subHeading={
              'An overview of all the important statistics from the match'
            }
          />
          {/* <div className='text-md  text-white tracking-wide font-bold '> Match Stats </div>
            <div className='text-base text-white pt-1 font-medium tracking-wide'>
              {' '}
              An overview of all the important statistics from the match{' '}
            </div>
            <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        </div>

        {data && data.getmatchStats && data.getmatchStats.matchStatsArray ? (
          <div className="dark:bg-gray bg-white rounded-md shadow p-3">
            {/*  */}

            {/* {false && (
                <div className='flex flex-column bg-gray-4  m-3 rounded  my-5 '>
                  <div className='flex justify-between items-center'>
                    <div className=''>
                      <div className='flex items-center  text-white text-md font-semibold'>
                        <img
                          className='w-10 h-6 shadow-2xl border-2 border-white rounded-sm mr-2'
                          src={`https://images.cricket.com/teams/${props.matchData.firstInningsTeamID}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                        />{' '}
                        {props.matchData.homeTeamName}
                      </div>
                    </div>
                    <div className='justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 '>
                      {props.matchData.matchType}
                    </div>
                    <div>
                      <div className='flex items-center  text-white text-md font-semibold'>
                        {' '}
                        {props.matchData.awayTeamName}
                        <img
                          className='w-10 h-6 shadow-2xl border-2 border-white rounded-sm ml-2'
                          src={`https://images.cricket.com/teams/${props.matchData.secondInningsTeamID}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                        />{' '}
                      </div>
                    </div>
                  </div>
  
                  {props.matchData.matchScore &&
                    props.matchData.matchScore[0] &&
                    props.matchData.matchScore[0].teamScore.length > 0 && (
                      <div className='flex justify-between items-center pt-3'>
                        <div className=''>
                          {props.matchData.matchScore &&
                            props.matchData.matchScore[0] &&
                            props.matchData.matchScore[0].teamScore &&
                            props.matchData.matchScore[0].teamScore.map((team, id) => (
                              <div key={id} className='flex items-center'>
                                <p
                                  className={`${
                                    team.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                  } text-sm font-semibold pb-1`}>
                                  {team.runsScored}/{team.wickets}
                                </p>
                                {team.overs && <p className='text-xs pl-1 font-medium'>({team.overs})</p>}
                              </div>
                            ))}
                        </div>
                        <div className=''>
                          {props.matchData.matchScore &&
                            props.matchData.matchScore[1] &&
                            props.matchData.matchScore[1].teamScore &&
                            props.matchData.matchScore[1].teamScore.map((team, id) => (
                              <div key={id} className='flex items-center'>
                                <p
                                  className={`${
                                    team.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                  } text-sm font-semibold pb-1`}>
                                  {team.runsScored}/{team.wickets}
                                </p>
                                {team.overs && <p className='text-xs pl-1 font-medium'>({team.overs})</p>}
                              </div>
                            ))}
                        </div>
                      </div>
                    )}
                </div>
              )} */}

            <div className="text-md font-semibold lg:hidden xl:hidden md:hidden">
              Match Stats
            </div>
            <div className="flex mb-3 mt-2 items-center justify-center">
              <div className="flex w-full flex-col dark:bg-gray-4 bg-[#E2E2E2] rounded-xl p-3">
                <div className="flex justify-between items-center">
                  <div className="">
                    <div className="flex items-center dark:text-white text-black text-base lg:text-xl md:text-xl font-semibold">
                      <img
                        className="w-12 h-10 lg:w-16 md:w-16 border-2 dark:border-white rounded mr-2"
                        src={`https://images.cricket.com/teams/${props.matchData.matchScore[0].teamID}_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/svgs/images/flag_empty.png')
                        }
                      />{' '}
                      {props.matchData.matchScore[0].teamShortName}
                    </div>
                  </div>
                  <div className="text-base border font-semibold border-green rounded-full text-green uppercase px-2 dark:bg-gray-8 bg-white py-0.5 text-center md:w-3/12">
                    {props.matchData.matchType}
                  </div>
                  <div>
                    <div className="flex items-center dark:text-white text-black lg:text-xl md:text-xl text-base font-semibold">
                      {' '}
                      {props.matchData.matchScore[1].teamShortName}
                      <img
                        className="w-12 h-10 lg:w-16 md:w-16 border-2 dark:border-white rounded ml-2"
                        src={`https://images.cricket.com/teams/${props.matchData.matchScore[1].teamID}_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/svgs/images/flag_empty.png')
                        }
                      />{' '}
                    </div>
                  </div>
                </div>

                {props.matchData.matchScore &&
                  props.matchData.matchScore[0] &&
                  props.matchData.matchScore[0].teamScore.length > 0 && (
                    <div className="flex justify-between items-center pt-3">
                      <div className="">
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[0] &&
                          props.matchData.matchScore[0].teamScore &&
                          props.matchData.matchScore[0].teamScore.map(
                            (team, id) => (
                              <div key={id} className="flex items-center">
                                <p
                                  className={`${
                                    props.matchData.winningTeamID ===
                                    team.teamID
                                      ? 'text-green'
                                      : 'text-gray-3'
                                  } text-xs md:text-sm font-semibold`}
                                >
                                  {team.runsScored}/{team.wickets}
                                  {team.winningTeamID}
                                </p>
                                {team.overs && (
                                  <p className="text-xs md:text-sm pl-1 font-medium">
                                    ({team.overs})
                                  </p>
                                )}
                              </div>
                            ),
                          )}
                      </div>
                      {}
                      <div className="">
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[1] &&
                          props.matchData.matchScore[1].teamScore &&
                          props.matchData.matchScore[1].teamScore.map(
                            (team, id) => (
                              <div key={id} className="flex items-center">
                                <p
                                  className={`${
                                    props?.matchData?.winningTeamID ===
                                    team.teamID
                                      ? 'text-green'
                                      : 'text-gray-3'
                                  }  font-semibold text-xs md:text-sm`}
                                >
                                  {team.runsScored}/{team.wickets}
                                </p>
                                {team.overs && (
                                  <p className="text-xs md:text-sm pl-1 font-medium">
                                    ({team.overs})
                                  </p>
                                )}
                              </div>
                            ),
                          )}
                      </div>
                    </div>
                  )}
              </div>
            </div>

            {statsArr.map(
              (category, j) =>
                data.getmatchStats.matchStatsArray[0] &&
                data.getmatchStats.matchStatsArray[1] &&
                data.getmatchStats.matchStatsArray[0][category.key] &&
                data.getmatchStats.matchStatsArray[1][category.key] &&
                data.getmatchStats.matchStatsArray[0][category.key].value &&
                data.getmatchStats.matchStatsArray[1][category.key].value && (
                  <>
                    <div key={j} className="my-2 dark:mx-0 mx-6">
                      <div className="text-center dark:text-[11px] text-base font-medium uppercase dark:text-gray-2 text-black">
                        {category.value}{' '}
                      </div>
                      <div className="flex items-center justify-between w-full">
                        <div className="dark:text-xs text-base dark:text-white text-black font-semibold">
                          {
                            data.getmatchStats.matchStatsArray[0][category.key]
                              .value
                          }
                        </div>
                        <div
                          className={`bg-gray-3 flex rounded mx-1 relative items-center w-4/5 z-0 o-100`}
                          style={{ height: 5 }}
                        >
                          <div
                            className={`absolute rounded-l left-0 bg-green `}
                            style={{
                              width: `${
                                (parseInt(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].percent,
                                ) *
                                  100) /
                                (parseInt(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].percent,
                                ) +
                                  parseInt(
                                    data.getmatchStats.matchStatsArray[1][
                                      category.key
                                    ].percent,
                                  ))
                              }%`,
                              zIndex: -1,
                              height: 5,
                            }}
                          ></div>
                        </div>

                        <div className="dark:text-xs text-base dark:text-white text-black font-semibold">
                          {' '}
                          {
                            data.getmatchStats.matchStatsArray[1][category.key]
                              .value
                          }
                        </div>
                      </div>
                    </div>
                  </>
                ),
            )}
          </div>
        ) : (
          <>
            <DataNotFound />
          </>
        )}
      </div>
    )
  }
}
