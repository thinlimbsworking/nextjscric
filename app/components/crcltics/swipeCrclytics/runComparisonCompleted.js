'use client'
import React, { useState, useRef, useEffect } from 'react'
import WormChart from '../wormChart'
import ScoringChartCompleted from '../scoringcharComplete'
import Heading from '../../commom/heading'
import Tab from '../../shared/Tab'
import RunRate from '../runRate'
import DataNotFound from '../../../components/commom/datanotfound'
// import { useEffect } from 'react/cjs/react.production.min'

const TabData = [
  { name: 'Worm Chart', value: 'Worm Chart' },
  { name: 'Scoring Chart', value: 'Scoring Chart' },
  { name: 'Run Rate', value: 'Run Rate' },
]
const TabData1 = [
  { name: 'Worm Chart', value: 'Worm Chart' },
  { name: 'Scoring Chart', value: 'Scoring Chart' },
]

export default function runComparisonCompleted(props) {
  const [toggle2, setToggle2] = useState('worm')
  const [widthDIv, setWidthDiv] = useState('')

  const [currentTab, setCurrentTab] = useState({
    name: 'Worm Chart',
    value: 'Worm Chart',
  })
  const [currentTab1, setCurrentTab1] = useState({
    name: 'Worm Chart',
    value: 'Worm Chart',
  })
  let scrl = useRef(null)

  useEffect(() => {
    // console.log("4567890",document.getElementById("mydiv").offsetWidth)
    setWidthDiv(document.getElementById('mydiv').offsetWidth)
  }, [widthDIv])
  // Statistical visulaizations to depict score progression and fall of wickets against the overs bowled during the innings
  return (
    <div>
      <div className="md:mx-3 mx-2 md:py-2">
        <Heading
          heading={'Run Comparison'}
          subHeading={
            'Compare team scores and measure their performance with every over'
          }
        />
        {/* <div className="text-md  text-white tracking-wide font-bold ">
          RUN COMPARSION{' '}
        </div>
        <div className="text-xs text-white pt-1 font-medium tracking-wide">
          {' '}
          Compare team scores and measure their performance with every over{' '}
        </div> */}
        {/* <div className="w-12 lg:hidden h-1 bg-blue-8 mt-2"></div> */}
      </div>
      {true ? (
        <>
          <div
            className="mt-5 hidden md:block md:ml-3 dark:bg-gray py-2 rounded-md shadow dark:text-white lg:text-black md:bg-white"
            id={'mydiv'}
            ref={scrl}
          >
            <div className="text-lg font-semibold tracking-wide px-2 md:hidden"></div>
            <div className="rounded-3xl items-center justify-between text-sm mt-3 dark:bg-gray-8 dark:w-10/12 lg:bg-white md:bg-white xl:bg-white ml-7 w-5/12">
              <Tab
                type="block"
                data={TabData}
                handleTabChange={(item) => setCurrentTab(item)}
                selectedTab={currentTab}
              />
            </div>

            {currentTab.value == 'Worm Chart' ? (
              <div className="">
                <WormChart
                  cricltics={true}
                  widthDIv={widthDIv}
                  data={props.data}
                  matchType={props?.matchType}
                  matchID={props?.matchID}
                />
              </div>
            ) : currentTab.value == 'Scoring Chart' ? (
              <ScoringChartCompleted
                cricltics={true}
                widthDIv={widthDIv}
                data={props.data}
                matchType={props?.matchType}
                matchID={props?.matchID}
              />
            ) : (
              <RunRate
                data={props.data}
                widthDIv={widthDIv}
                cricltics={true}
                matchType={props?.matchType}
                matchID={props?.matchID}
              />
            )}
          </div>
          <div className="mt-5 md:hidden md:ml-3 dark:bg-gray py-2 rounded-md shadow dark:text-white lg:text-black md:bg-white">
            <div className="text-lg font-semibold tracking-wide px-2 md:hidden">
              Run Comparison{' '}
            </div>
            <div className="hidden md:flex rounded-3xl items-center justify-between text-sm mt-3 dark:bg-gray-8 dark:w-10/12 lg:bg-white md:bg-white xl:bg-white ml-7 w-5/12">
              <Tab
                type="block"
                data={TabData}
                handleTabChange={(item) => setCurrentTab(item)}
                selectedTab={currentTab}
              />
            </div>
            <div className="flex md:hiddden rounded-3xl items-center justify-between text-sm mt-3 dark:bg-gray-8 dark:w-10/12 lg:bg-white md:bg-white xl:bg-white ml-7 w-5/12">
              <Tab
                type="block"
                data={TabData1}
                handleTabChange={(item) => setCurrentTab1(item)}
                selectedTab={currentTab1}
              />
            </div>
            {currentTab1.value == 'Worm Chart' ? (
              <div className="">
                <WormChart
                  cricltics={true}
                  data={props.data}
                  widthDIv={widthDIv}
                  matchType={props?.matchType}
                  matchID={props?.matchID}
                />
              </div>
            ) : currentTab1.value == 'Scoring Chart' ? (
              <ScoringChartCompleted
                cricltics={true}
                data={props.data}
                widthDIv={widthDIv}
                matchType={props?.matchType}
                matchID={props?.matchID}
              />
            ) : (
              ''
            )}
          </div>
        </>
      ) : (
        <div>
          <DataNotFound />
        </div>
      )}
    </div>
  )
}
