import React from 'react'
import { format, formatDistanceToNowStrict } from 'date-fns'
import Countdown from 'react-countdown-now'
export default function UpcomingMiniScoreCard(props) {
  console.log(props, 'oooooooooo')
  return (
    <div className="flex white ">
      {(props.matchData.matchStatus === 'upcoming' ||
        (props.matchData.matchStatus === 'live' &&
          !props.matchData.isLiveCriclyticsAvailable)) && (
        <div className="bg-gray rounded-lg px-2 w-full mt-14 mx-3 ">
          <div className=" bg-gray-4  p-3 rounded-lg my-3">
            <div className="flex justify-between items-center">
              <div className="flex items-center  text-white text-md font-semibold">
                <img
                  className="w-8 h-5  rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${props.matchData.matchScore[0].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                />
                {props.matchData.matchScore[0].teamShortName}
              </div>

              <div className=" border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3 bg-gray-8  py-1 ">
                {props.matchData.matchType}
              </div>
              <div className="flex items-center  text-white text-md font-semibold">
                {props.matchData.matchScore[1].teamShortName}
                <img
                  className="w-10 h-6 shadow-2xl border-2 border-white rounded-sm ml-2"
                  src={`https://images.cricket.com/teams/${props.matchData.matchScore[1].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                />
              </div>
            </div>
            <div className="text-white mt-2 text-sm font-medium text-center">
              {' '}
              {format(
                new Date(Number(props.matchData.startDate) - 19800000),
                ' do MMM,EEEE h:mm a ',
              )}
            </div>
          </div>
        </div>
      )}

      {props.matchData.matchStatus === 'live' &&
        props.matchData.isLiveCriclyticsAvailable && (
          <div className="bg-gray rounded-lg px-2  w-full mt-14 mx-3">
            <div className=" bg-gray-4   my-3 rounded p-3 ">
              <div className="flex justify-between items-center">
                <div className="flex items-center  text-white text-md font-semibold">
                  <img
                    className="w-8 h-5 rounded-sm mr-2"
                    src={`https://images.cricket.com/teams/${props.matchData.matchScore[0].teamID}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = '/pngsV2/flag_dark.png')
                    }
                  />{' '}
                  {props.matchData.matchScore[0].teamShortName}{' '}
                </div>
                <div className="justify-center items-center flex border-2 bg-gray-8 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-1 ">
                  {props.matchData.matchType}
                </div>
                <div>
                  <div className="flex items-center  text-white text-md font-semibold">
                    {' '}
                    {props.matchData.matchScore[1].teamShortName}{' '}
                    <img
                      className="w-8 h-5 rounded-sm ml-2"
                      src={`https://images.cricket.com/teams/${props.matchData.matchScore[1].teamID}_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngsV2/flag_dark.png')
                      }
                    />{' '}
                  </div>
                </div>
              </div>

              {props.matchData.matchScore &&
                props.matchData.matchScore[0] &&
                props.matchData.matchScore[0].teamScore.length > 0 && (
                  <div className="flex justify-between items-center pt-3">
                    {/* {props?.matchData?.matchStatus === 'live' &&
                      props?.matchData?.isLiveCriclyticsAvailable &&
                      props?.matchData?.currentinningsNo % 2 !== 0 && (
                        <div className="flex">
                          <div className="w-2 h-2 rounded-full bg-green-600"></div>
                        </div>
                      )} */}
                    <div className="flex gap-1">
                      {props?.matchData?.matchStatus === 'live' &&
                        props?.matchData?.isLiveCriclyticsAvailable &&
                        props?.matchData?.currentinningsNo % 2 !== 0 && (
                          <div className="mt-1">
                            <div className="w-2 h-2 rounded-full bg-green-600"></div>
                          </div>
                        )}
                      {props.matchData.matchScore &&
                        props.matchData.matchScore[0] &&
                        props.matchData.matchScore[0].teamScore &&
                        props.matchData.matchScore[0].teamScore.map(
                          (team, id) => (
                            <div key={id} className="flex items-center">
                              <p
                                className={`${
                                  team?.winningTeamID === team.teamID
                                    ? 'text-green'
                                    : 'text-white'
                                } text-sm font-semibold md:pb-1`}
                              >
                                {team.runsScored}/{team.wickets}
                              </p>
                              {team.overs && (
                                <p className="text-xs pl-1 font-medium text-white">
                                  ({team.overs})
                                </p>
                              )}
                            </div>
                          ),
                        )}
                    </div>
                    {props?.matchData?.matchStatus === 'live' &&
                    props?.matchData?.isLiveCriclyticsAvailable &&
                    props?.matchData?.currentinningsNo % 2 == 0 ? (
                      <div className="flex gap-1">
                        {props?.matchData?.matchStatus === 'live' &&
                          props?.matchData?.isLiveCriclyticsAvailable &&
                          props?.matchData?.currentinningsNo % 2 == 0 && (
                            <div className="mt-1">
                              <div className="w-2 h-2 rounded-full bg-green-600"></div>
                            </div>
                          )}
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[1] &&
                          props.matchData.matchScore[1].teamScore &&
                          props.matchData.matchScore[1].teamScore.map(
                            (team, id) => (
                              <div key={id} className="flex items-center">
                                <p
                                  className={`${
                                    props?.winningTeamID === team.teamID
                                      ? 'text-green'
                                      : 'text-white'
                                  } text-sm font-semibold md:pb-1`}
                                >
                                  {team.runsScored}/{team.wickets}
                                </p>
                                {team.overs && (
                                  <p className="text-xs pl-1 font-medium text-white">
                                    ({team.overs})
                                  </p>
                                )}
                              </div>
                            ),
                          )}
                      </div>
                    ) : (
                      <span className="text-gray-2 font-normal">
                        Yet To Bat
                      </span>
                    )}
                  </div>
                )}
              <div className=" text-center text-white/80 text-xs font-medium mt-2">
                {props.matchData.statusMessage}
              </div>
            </div>
          </div>
        )}

      {props.status === 'completed' && (
        <div className="bg-gray rounded-lg px-2 mx-2 w-full mt-14">
          <div className=" bg-gray-4  my-2 rounded p-3 ">
            <div className="flex justify-between items-center">
              <div className="">
                <div className="flex items-center  text-white text-sm font-semibold">
                  <img
                    className="w-8 h-5 shadow-2xl border-2 border-white rounded-sm mr-2"
                    src={`https://images.cricket.com/teams/${props.matchData.matchScore[0].teamID}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = '/pngsV2/flag_dark.png')
                    }
                  />{' '}
                  {props.matchData.matchScore[0].teamShortName}{' '}
                </div>
              </div>
              <div className="justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 ">
                {props.matchData.matchType}
              </div>
              <div>
                <div className="flex items-center  text-white text-sm font-semibold">
                  {' '}
                  {props.matchData.matchScore[1].teamShortName}{' '}
                  <img
                    className="w-8 h-5 shadow-2xl border-2 border-white rounded-sm ml-2"
                    src={`https://images.cricket.com/teams/${props.matchData.matchScore[1].teamID}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = '/pngsV2/flag_dark.png')
                    }
                  />{' '}
                </div>
              </div>
            </div>
            <div className="flex justify-between items-center pt-3">
              <div className="">
                {props.matchData.matchScore &&
                  props.matchData.matchScore[0] &&
                  props.matchData.matchScore[0].teamScore &&
                  props.matchData.matchScore[0].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center">
                      <p
                        className={`${
                          props?.winningTeamID === team.teamID
                            ? 'text-green'
                            : 'text-white'
                        } text-base font-semibold md:pb-1`}
                      >
                        {team.runsScored}/{team.wickets}
                      </p>
                      {team.overs && (
                        <p className=" pl-1 font-medium text-white text-base">
                          ({team.overs})
                        </p>
                      )}
                    </div>
                  ))}
              </div>
              <div className="">
                {props.matchData.matchScore &&
                  props.matchData.matchScore[1] &&
                  props.matchData.matchScore[1].teamScore &&
                  props.matchData.matchScore[1].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center">
                      <p
                        className={`${
                          props?.winningTeamID === team.teamID
                            ? 'text-green'
                            : 'text-white'
                        }  font-semibold text-base md:pb-1`}
                      >
                        {team.runsScored}/{team.wickets}
                      </p>
                      {team.overs && (
                        <p className=" pl-1 font-medium text-white text-base">
                          ({team.overs})
                        </p>
                      )}
                    </div>
                  ))}
              </div>
            </div>
          </div>

          <div className=" bg-gray-4 text-center mb-2 rounded py-1.5 text-white text-sm font-medium">
            {props.matchData.matchResult}
          </div>
        </div>
      )}
    </div>
  )
}
