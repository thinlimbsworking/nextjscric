import React from 'react'
import { GET_KEY_STATS } from '../../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import Heading from '../../commom/heading'

export default function KeyStat(props) {
  const { data: keyData } = useQuery(GET_KEY_STATS, {
    variables: {
      matchID: props?.matchID,
    },
  })

  const Pitch = '/pngsV2/keystatpitch.png'
  const Location = '/pngsV2/statlocation.png'

  {
  }
  return (
    <div className="">
      {/* <div className="flex text-2xl font-semibold mx-3 dark:text-white text-black">
        Key Stats
      </div> */}
      <div className="ml-3 md:my-1 dark:mt-3">
        <Heading
          heading={'Key Stats'}
          subHeading={
            'Just the right stats to help you determine which team statistically has the upper hand'
          }
        />
      </div>

      <div className="flex text-xs dark:text-white text-black mt-2 tracking-wide mb-5 ml-2.5 md:mt-4"></div>

      <div className="md:ml-2">
        {keyData?.getKeyStats?.head2headStats?.head2Head?.totalMatches > 0 && (
          <div className="md:bg-white md:rounded-md md:shadow-sm md:pb-4 md:pt-2 md:mb-3">
            <div className="flex items-center justify-between m-2 mx-3">
              {/* <div className="dark:text-white text-black text-base font-bold">
              Head To Head
            </div> */}
              <Heading heading={'Head To Head'} />
              <div className="text-xs text-gray-2 font-medium">
                *Last 3 Years
              </div>
            </div>
            {/* <div className="h-1  w-12 bg-blue-8 mx-3" /> */}

            <div className="flex flex-col mx-3 mt-4 rounded p-2 dark:bg-gray bg-white dark:border-none border-2 border-solid border-[#E2E2E2]">
              <div className="m-1 relative">
                <div className="py-2 flex justify-center">
                  <div className="dark:bg-gray-3 bg-[#E2E2E2] h-[2px] dark:w-full w-4/12 absolute"></div>
                  <div className="dark:text-gray-3 text-[#7E7E7E] text-xs font-medium tc absolute top-0 dark:bg-gray bg-white px-2 uppercase">
                    {
                      keyData?.getKeyStats?.head2headStats?.head2Head
                        ?.totalMatches
                    }{' '}
                    Matches{' '}
                  </div>
                </div>

                <div className="w-full flex bg-gray-3 rounded-full overflow-hidden  h-3  mt-2">
                  <div
                    className={`${'bg-green'}  h-3 rounded-l-lg`}
                    style={{
                      width:
                        keyData?.getKeyStats?.head2headStats?.head2Head?.teamA *
                          (100 /
                            (keyData?.getKeyStats?.head2headStats?.head2Head
                              ?.teamA +
                              keyData?.getKeyStats?.head2headStats?.head2Head
                                ?.teamB +
                              keyData?.getKeyStats?.head2headStats?.head2Head
                                .noResult)) +
                        '%',
                    }}
                  />
                  <div
                    className={`bg-gray-3 h-3`}
                    style={{
                      width:
                        keyData?.getKeyStats?.head2headStats?.head2Head
                          ?.noResult *
                          (100 /
                            (keyData?.getKeyStats?.head2headStats?.head2Head
                              ?.teamA +
                              keyData?.getKeyStats?.head2headStats?.head2Head
                                ?.teamB +
                              keyData?.getKeyStats?.head2headStats?.head2Head
                                .noResult)) +
                        '%',
                    }}
                  />
                  <div
                    className={`rounded-r-lg  ${'bg-[#B7B7B7]'}  h-3   `}
                    style={{
                      width:
                        keyData?.getKeyStats?.head2headStats?.head2Head?.teamB *
                          (100 /
                            (keyData?.getKeyStats?.head2headStats?.head2Head
                              ?.teamA +
                              keyData?.getKeyStats?.head2headStats?.head2Head
                                ?.teamB +
                              keyData?.getKeyStats?.head2headStats?.head2Head
                                .noResult)) +
                        '%',
                    }}
                  />
                </div>
              </div>

              <div className="flex mt-2 mx-1 justify-between items-center text-gray-2">
                <div className=" flex items-center text-xs font-medium">
                  <p className={`w-2 h-2 ${'bg-green'} rounded-full m-1  `}></p>
                  {keyData?.getKeyStats?.head2headStats &&
                    keyData?.getKeyStats?.head2headStats?.teamA?.teamShortName}
                  (
                  {keyData?.getKeyStats?.head2headStats?.head2Head?.teamA &&
                    keyData?.getKeyStats?.head2headStats?.head2Head?.teamA}
                  )
                </div>
                {true && (
                  <div className="flex items-center text-xs font-medium">
                    <p className="w-2 h-2 bg-gray-3 rounded-full m-1  "></p>
                    TIE (
                    {keyData?.getKeyStats?.head2headStats?.head2Head
                      ?.noResult || 0}
                    )
                  </div>
                )}
                <div className="flex items-center text-xs font-medium">
                  <p
                    className={`w-2 h-2 ${'bg-[#B7B7B7]'} rounded-full m-1  `}
                  ></p>
                  {keyData?.getKeyStats?.head2headStats &&
                    keyData?.getKeyStats?.head2headStats?.teamB?.teamShortName}
                  (
                  {keyData?.getKeyStats?.head2headStats?.head2Head?.teamB &&
                    keyData?.getKeyStats?.head2headStats?.head2Head?.teamB}
                  )
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="md:bg-white md:rounded-md md:shadow-sm md:p-3">
          <div className="flex flex-col items-start m-3 mt-4">
            <div className="dark:text-white text-black text-base  font-bold">
              Pitch Behaviour
            </div>
            <div className="h-1 my-2 w-12 bg-blue-8" />

            <div className="dark:text-white text-black text-xs flex items-center justify-center mt-1 font-medium">
              {' '}
              <img className="h-8 w-8 mr-2" src={Location} alt="" />{' '}
              {keyData?.getKeyStats?.head2headStats?.venueStatsData?.venueName}
            </div>
          </div>

          <div className="lg:flex md:flex ">
            <div className="flex flex-col lg:w-6/12 md:w-6/12">
              <div className="flex items-center justify-start p-2 mx-1">
                <div className="w-6/12 dark:bg-gray bg-white white mx-[1px] p-3 rounded-l-lg border-2 bordr-solid dark:border-gray border-[#E2E2E2]">
                  <div className="text-[10px] font-medium dark:text-gray-2 text-black">
                    {' '}
                    OVERALL
                  </div>
                  <div className="text-xs dark:text-white text-black font-bold pt-1">
                    {
                      keyData?.getKeyStats?.head2headStats?.venueStatsData
                        ?.overall
                    }
                  </div>
                </div>
                <div className="w-6/12 dark:bg-gray bg-white border-2 border-solid dark:border-gray border-[#E2E2E2] white mx-[1px] p-3 rounded-r-lg">
                  <div className="text-[10px] font-medium dark:text-gray-2 text-black uppercase">
                    {' '}
                    Best suited for
                  </div>
                  <div className="text-xs dark:text-white text-black font-bold pt-1">
                    {' '}
                    {
                      keyData?.getKeyStats?.head2headStats?.venueStatsData
                        ?.bestSuited
                    }
                  </div>
                </div>
              </div>

              <img
                className="flex  -mt-5 h-48 lg:h-24 md:h-24 w-full"
                src={Pitch}
              />
            </div>

            <div className="dark:-mt-6 lg:w-6/12 md:w-6/12">
              <div className="dark:bg-gray bg-white border-2 border-solid border-[#E2E2E2] dark:border-gray mx-3 p-3 rounded-lg ">
                <div className="flex justify-between">
                  <div className="w-6/12 pl-2">
                    <div className="flex dark:text-gray-2 text-black text-[10px] font-semibold">
                      {' '}
                      1ST BATTING AVG. SCORE
                    </div>
                    <div className="flex dark:text-white text-black text-xs font-bold mt-1  ">
                      {
                        keyData?.getKeyStats?.head2headStats?.venueStatsData
                          ?.avgFirstInningScore
                      }
                    </div>
                  </div>
                  <div className="w-6/12 pl-2 ">
                    <div className="flex dark:text-gray-2 text-black text-[10px] font-semibold">
                      {' '}
                      HIGHEST SCORE CHASED
                    </div>
                    <div className="flex dark:text-white text-black text-xs font-bold mt-1">
                      {
                        keyData?.getKeyStats?.head2headStats?.venueStatsData
                          ?.highestScoreChased
                      }
                    </div>
                  </div>
                </div>
                <div className="pl-2 mt-5">
                  <div className="flex dark:text-gray-2 text-black text-[10px] font-semibold">
                    {' '}
                    WICKET SPLIT
                  </div>
                  <div className="flex justify-between">
                    <div className="flex dark:text-white text-black text-xs font-bold mt-1 w-6/12">
                      <p className="dark:text-gray-2 text-black pr-1 font-medium">
                        {' '}
                        PACE
                      </p>{' '}
                      {
                        keyData?.getKeyStats?.head2headStats?.venueStatsData
                          ?.paceWicketPercent
                      }
                      %
                    </div>
                    <div className="flex dark:text-white text-black text-xs font-bold mt-1 w-6/12">
                      <p className="dark:text-gray-2 text-black pr-1 font-medium">
                        {' '}
                        SPIN
                      </p>{' '}
                      {
                        keyData?.getKeyStats?.head2headStats?.venueStatsData
                          ?.spinWicketPercent
                      }
                      %
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
