import React, { useEffect, useState } from 'react'
import * as d3 from 'd3'


import { useQuery, useMutation } from '@apollo/react-hooks'
import { RUN_COMPARISON } from '../../api/queries'



export default function WormChart(props) {
 let data={runsComparsion:props.data&& props.data.wormChart}
  const [screenWidth, setScreenWidth] = useState(0)
  const [screenHeight, setscreenHeight] = useState(350)




  useEffect(() => {
    if (
      typeof window !== 'undefined' &&
      window &&
      window.screen &&
      window.screen.width
    ) {
      setScreenWidth(
        window.innerWidth > 1500
          ? props.cricltics
            ? window.innerWidth * 0.7
            : window.innerWidth * 0.37
          : window.innerWidth > 1300
          ? props.cricltics
            ? window.innerWidth * 0.64
            : window.innerWidth * 0.34
          : window.screen.width,
      )
     
    }

  }, [])

  const margin = { top: 30, right: 20, bottom: 30, left: 80 },
    width = screenWidth - margin.left - margin.right,
    height = screenHeight - margin.top - margin.bottom
  const x = d3
    .scaleLinear()
    .domain([
      0,
      data?.runsComparsion?.format === 'ODI'
        ? 50
        : data?.runsComparsion?.format === 'T10'
        ? 10
        : 20,
    ])
    .range([0, width])
  let y = d3
    .scaleLinear()
    .domain([0, data?.runsComparsion?.inningsPhase[0]?.totalRuns + 10])
    .range([height, 0])
  const line = d3
    .line()
    .curve(d3.curveNatural)
    .x((d) => x(d.overNumber))
    .y((d) => y(d.score))
if(props.data)
  {
    return (
      <div className="relative flex justify-center items-center">
        <text
          className={` absolute font-medium text-gray-2 rotate-270`}
          style={{
            // bottom: 26,
            top: height + 14,
            left: 14,
            fontSize: 10,
            transform: [{ rotate: '270deg' }],
          }}
        >
          Runs
        </text>
        <div className="lg:hidden">
          <svg height={height + 50} width={width + 60}>
            <g transform="translate(25,25)">
              <rect
                height={height + 10}
                width={width + 20}
                fill={'#1F2634'}
                style={{ borderRadius: 10, overflow: 'hidden' }}
                transform="translate(3,-9)"
              />
              <g transform="translate(-7,1)">
                {[...y.ticks(5)].map((tick) => (
                  <g key={tick} transform="translate(-5,0)">
                    <line
                      x1="10"
                      y1={y(tick)}
                      x2={width + 30}
                      y2={y(tick)}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#2B323F'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={0}
                      y={y(tick)}
                      fontSize="10"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
                {x.ticks(5).map((tick) => (
                  <g key={tick} transform="translate(10,0)">
                    <line
                      x1={x(tick)}
                      y1="0"
                      x2={x(tick)}
                      y2={height}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#2B323F'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={x(tick)}
                      y={height + 20}
                      fontSize="10"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
              </g>
              <g>
              {data?.runsComparsion?.inningsPhase[0]?.data &&   <path
                  transform="translate(4,0)"
                  id="line"
                  d={line(data?.runsComparsion?.inningsPhase[0]?.data)}
                  fill="none"
                  stroke="#5F92AC"
                  strokeWidth="2"
                />}
                {data?.runsComparsion?.inningsPhase[0]?.data.map((ovr) =>
                  ovr.wickets ? (
                    [...new Array(ovr.wickets)].map((val, index) => (
                      <circle
                        transform={`translate(4,${
                          index === 0 ? 0 : -3.1 * (index + 1)
                        })`}
                        fill={'#5F92AC'}
                        r={3}
                        cx={x(ovr.overNumber)}
                        cy={y(ovr.score)}
                      />
                    ))
                  ) : (
                    <></>
                  ),
                )}
              </g>
              {data.runsComparsion.inningsPhase[1] &&
                data.runsComparsion.inningsPhase[1].data &&
                data.runsComparsion.inningsPhase[1].data.length > 0 && (
                  <g>
                    <path
                      transform="translate(4,0)"
                      id="line"
                      d={line(data.runsComparsion.inningsPhase[1].data)}
                      fill="none"
                      stroke={'#38D926'}
                      // strokeDasharray="3 3"
                      strokeWidth="2"
                      // style={{ opacity: 0.8 }}
                    />
                    {data.runsComparsion.inningsPhase[1].data.map((ovr) =>
                      ovr.wickets ? (
                        [...new Array(ovr.wickets)].map((val, index) => (
                          <circle
                            transform={`translate(4,${
                              index === 0 ? 0 : -3.1 * (index + 1)
                            })`}
                            fill={'#38D926'}
                            r={3}
                            cx={x(ovr.overNumber)}
                            cy={y(ovr.score)}
                          />
                        ))
                      ) : (
                        <></>
                      ),
                    )}
                  </g>
                )}
            </g>
            <text
              x={45}
              y={height + 45}
              fontSize="10"
              fill="#8C98B0"
              fontFamily="Manrope-Medium"
              textAnchor="middle"
            >
              Overs
            </text>
           
          </svg>
        </div>
        <div className="hidden lg:block md:block">
          <svg height={height + 50} width={width + 60}>
            <g transform="translate(25,25)">
              <rect
                height={height + 12}
                width={width + 2}
                fill={'#EEEFF2'}
                style={{ borderRadius: 10, overflow: 'hidden' }}
                transform="translate(3,-9)"
              />
              <g transform="translate(-7,1)">
                {[...y.ticks(6)].map((tick) => (
                  <g key={tick} transform="translate(-5,0)">
                    <line
                      x1="15"
                      y1={y(tick)}
                      x2={width + 14}
                      y2={y(tick)}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#E2E2E2'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={0}
                      y={y(tick)}
                      fontSize="15"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
                {x.ticks(8).map((tick) => (
                  <g key={tick} transform="translate(10,0)">
                    <line
                      x1={x(tick)}
                      y1="0"
                      x2={x(tick)}
                      y2={height}
                      //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                      stroke={'#E2E2E2'}
                      strokeWidth="0.6"
                    />
                    <text
                      x={x(tick)}
                      y={height + 20}
                      fontSize="15"
                      fill="#8C98B0"
                      fillOpacity={tick == 0 ? 0 : 1}
                      fontFamily="Manrope-Medium"
                      textAnchor="middle"
                    >
                      {tick}
                    </text>
                  </g>
                ))}
              </g>
              <g>
              {data?.runsComparsion?.inningsPhase[0]?.data &&     <path
                  transform="translate(4,0)"
                  id="line"
                  d={line(data.runsComparsion.inningsPhase[0].data)}
                  fill="none"
                  stroke="#5F92AC"
                  strokeWidth="2"
                />
              }
                {data?.runsComparsion?.inningsPhase[0]?.data.map((ovr) =>
                  ovr.wickets ? (
                    [...new Array(ovr.wickets)].map((val, index) => (
                      <circle
                        transform={`translate(4,${
                          index === 0 ? 0 : -3.1 * (index + 1)
                        })`}
                        fill={'#5F92AC'}
                        r={3}
                        cx={x(ovr.overNumber)}
                        cy={y(ovr.score)}
                      />
                    ))
                  ) : (
                    <></>
                  ),
                )}
              </g>
              {data.runsComparsion.inningsPhase[1] &&
                data.runsComparsion.inningsPhase[1].data &&
                data.runsComparsion.inningsPhase[1].data.length > 0 && (
                  <g>
                    <path
                      transform="translate(4,0)"
                      id="line"
                      d={line(data.runsComparsion.inningsPhase[1].data)}
                      fill="none"
                      stroke={'#38D926'}
                      // strokeDasharray="3 3"
                      strokeWidth="2"
                      // style={{ opacity: 0.8 }}
                    />
                    {data.runsComparsion.inningsPhase[1].data.map((ovr) =>
                      ovr.wickets ? (
                        [...new Array(ovr.wickets)].map((val, index) => (
                          <circle
                            transform={`translate(4,${
                              index === 0 ? 0 : -3.1 * (index + 1)
                            })`}
                            fill={'#38D926'}
                            r={3}
                            cx={x(ovr.overNumber)}
                            cy={y(ovr.score)}
                          />
                        ))
                      ) : (
                        <></>
                      ),
                    )}
                  </g>
                )}
            </g>
            <text
              x={45}
              y={height + 45}
              fontSize="15"
              fill="#8C98B0"
              fontFamily="Manrope-Medium"
              textAnchor="middle"
            >
              Overs
            </text>
          
          </svg>
        </div>
        <div className="absolute flex bottom-10 right-8">
          <div className="flex-row items-center flex pr-2">
            <div
              style={{
                width: 6,
                height: 6,
                borderRadius: 3,
                backgroundColor: '#5F92AC',
                marginRight: 2,
              }}
            />
            <div className="text-gray-2 font-medium text-xs">{`${data?.runsComparsion?.inningsPhase[0]?.teamShortName} ${data?.runsComparsion?.inningsPhase[0]?.totalRuns}/${data?.runsComparsion?.inningsPhase[0]?.totalWickets}`}</div>
          </div>
          {data.runsComparsion.inningsPhase[1] &&
          data.runsComparsion.inningsPhase[1].data ? (
            <div className="flex-row items-center flex">
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: 3,
                  backgroundColor: '#38D926',
                  marginRight: 2,
                }}
              />
              <div className="text-gray-2 font-medium text-xs">{`${data.runsComparsion.inningsPhase[1].teamShortName} ${data.runsComparsion.inningsPhase[1].totalRuns}/${data.runsComparsion.inningsPhase[1].totalWickets}`}</div>
            </div>
          ) : (
            <div />
          )}
        </div>
      </div>
    )
  }
}

