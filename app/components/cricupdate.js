import React, { useState } from 'react';
import { CRIC_UPDATES } from '../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { format } from 'date-fns';
import { useRouter } from 'next/router';
import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectCoverflow, Pagination } from 'swiper';
import CleverTap from 'clevertap-react';

export default function cricUpdates(props) {
  const [currentIndex, updateCurrentIndex] = useState(0);

  const router = useRouter();
  const navigate = router.push;
  const { loading, error, data } = useQuery(CRIC_UPDATES, {
    variables: { matchID: props.matchID || '' }
  });

  const redirectToFile = () => {
    let matchName = `${props.matchProjection}-${props.seriesName}`;
    let seriesSlug = matchName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase();
    navigate(`match-score/${props.matchID}/articles/${seriesSlug}`);
  };
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime();
    let distance = currentTime - updateTime;
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let sec = Math.floor((distance % (1000 * 60)) / 1000);

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago';
    return result;
  };

  return data && data.articlesHomeScreenCricUpdates && data.articlesHomeScreenCricUpdates.length > 0 ? (
    <div className='mt-5 mx-2'>
      <div className='  text-white '>
        <div className='flex justify-between items-center w-full px-2 py-2'>
          <div className='font-semibold text-lg tracking-wider" '>CricUpdates</div>
          <div
            className='rounded border-2  text-green border-green text-xs p-1 px-2 items-center font-semibold '
            onClick={() => redirectToFile()}>
            VIEW ALL
          </div>
        </div>
        <div className=''>
          <div className='flex justify-start px-1 pb-2 '>
            {data.articlesHomeScreenCricUpdates &&
              data.articlesHomeScreenCricUpdates.map((item, key) => (
                <div key={key} className={`w-2 h-2 ${currentIndex == key ? 'bg-blue-8' : 'bg-blue-4 '} rounded-full m-1 `}></div>
              ))}
          </div>
          <Swiper
            slidesPerView={'auto'}
            spaceBetween={10}
            onSlideChange={(swiper) => updateCurrentIndex(swiper.realIndex)}
            modules={[Pagination]}
            className='mySwiper'>
            {data &&
              data.articlesHomeScreenCricUpdates.map((item, y) => {
                return (
                  <SwiperSlide
                    key={y}
                    className=' cardUpdate   p-3   bg-gray   rounded-xl h-56 w-64 mr-4 '
                    onClick={() =>
                      item.type == 'videos' ? navigate(`videos/${item.videoID}`) : navigate(`news/${item.articleID}`)
                    }>
                    {item.type == 'videos' ? (
                      <div className='flex  relative items-center justify-center  w-full '>
                        <img
                          className=' h-30 rounded-lg w-full'
                          src={`https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg`}
                          alt=''
                        />

                        <img className='absolute h-12 w-12 right-0 bottom-0' src='/pngsV2/videos_icon.png' alt='' />
                      </div>
                    ) : (
                      <div className='flex  relative items-center justify-center  w-full '>
                        <img
                          className=' h-32 rounded-lg w-full object-cover object-top'
                          src={`${item.sm_image_safari}`}
                          alt=''
                        />
                      </div>
                    )}

                    <div className='overflow-hidden  mt-2 h-5 text-sm font-semibold text-ellipsis text-left'>
                      {item.title}
                    </div>
                    <div className='  my-1 text-xs  font-thin truncate text-left '>{item.description}</div>

                    <div className='text-gray-500 text-xs flex items-center justify-start font-medium'>
                      <div className='pr-2 '>By: {item.authors}</div>
                      <div>|</div>
                      <div className='pl-2'>
                        {' '}
                        {Date.now() - parseInt(item.createdAt) > 8640000
                          ? item && format(+item.createdAt, 'dd MMM yyyy')
                          : getUpdatedTime(item.createdAt)}
                      </div>
                    </div>
                  </SwiperSlide>
                );
              })}
          </Swiper>
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
