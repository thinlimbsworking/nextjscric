'use client'
import { useContext } from 'react'
import CleverTap from 'clevertap-react'
import { LocalStorageContext } from '../layout'

export default function useCleverTab() {
  const { getItem } = useContext(LocalStorageContext)
  const pushEvent = (tab, props) => {
    CleverTap.initialize(tab, {
      ...props,
      Platform: getItem('Platform'),
    })
  }
  return { pushEvent }
}
