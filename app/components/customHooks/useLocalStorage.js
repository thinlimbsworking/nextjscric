'use client'
import { useEffect, useState } from 'react'

export default function useLocalStorage() {

  const [tabContent,setTabcontent]= useState("selectMatch")
  const [lsData, setLsData] = useState()
  useEffect(() => {
    setLsData(localStorage)
  }, [])
  
  const setItem = (key, value) => {
    localStorage.setItem(key, value)
    setLsData({ ...localStorage })
  }
  const getItem = (key) => {
    // let temp = [];
    // keys.forEach(key => {
    //     temp.push(lsData[key])
    // })
    return lsData?.[key]
  }
  const removeItem = (key) => {
    localStorage.removeItem(key)
    setLsData({ ...localStorage })
  }
  const setTabContent = (item) => {
   
    setTabcontent(item)
    return tabContent
  }

  const getTabContent = () => {
   
   
    return tabContent
  }


  return { setItem, getItem, removeItem ,setTabContent,getTabContent}
}
