'use client'

import React from 'react'
import { format, formatDistanceToNowStrict } from 'date-fns'
import { DATA_DIGEST } from '../api/queries'
import { useQuery } from '@apollo/react-hooks'
// import { useRouter } from 'next/navigation';

export default function UpcomingMatches(props) {
  // const router = useRouter();
  // const navigate = router.push;
  let { loading, error, data } = useQuery(DATA_DIGEST, {})

  if (data) {
    return (
      <div className=" text-white mb:mt-5 md:mt-1 p-1 mx-2 mt-2  ">
        <div className="flex items-center justify-between">
          <div className="font-semibold text-lg tracking-wider capitalize ">
            {' '}
            Data digest{' '}
          </div>
          <div className="flex justify-center rounded  bg-gray-4 p-1  items-center  ">
            {/* <img className='w-8 h-8' src='/pngsV2/arrow.png' alt='' /> */}
          </div>
        </div>
        <div className="flex overflow-scroll w-full  ">
          {data &&
            data.getTopTenDataDigest &&
            data.getTopTenDataDigest.map((item, i) => {
              return (
                <div className="">
                  <div key={i} className="w-60 h-60 m-2">
                    <img
                      className="h-60 object-top object-cover rounded-md"
                      src={`${item.img}?w=400&h=400&auto=compress,format&fit=crop&crop=faces,top&dpr=2`}
                      alt=""
                    />
                  </div>
                </div>
              )
            })}
        </div>
      </div>
    )
  }
}
