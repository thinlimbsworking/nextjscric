import React, { Component } from 'react'

const changedArray = (a = [], b = []) =>
  a.length !== b.length || a.some((item, index) => !Object.is(item, b[index]))

const initialState = { error: null }


const ErrorFallback = ({ message, fallBackImageStyle, containerStyle }) => (
  <div role="alert" className={`tc ${containerStyle ? containerStyle : ''}`}>
    <div className='flex justify-center items-center w-100'>
      {/* <img className={`${fallBackImageStyle ? fallBackImageStyle : 'w-10'}`} src='/svgs/Empty.svg' alt='' /> */}
    </div>
    <div className='gray f6 tc'>{message}</div>
  </div>
)

class ErrorBoundary extends Component {
  static getDerivedStateFromError(error) {
    return { error }
  }

  state = initialState
  updatedWithError = false
  resetErrorBoundary = (...args) => {
    this.props.onReset?.(...args)
    this.reset()
  }

  reset() {
    this.updatedWithError = false
    this.setState(initialState)
  }

  componentDidCatch(error, info) {
    this.props.onError?.(error, info)
  }

  componentDidUpdate(prevProps) {
    const { error } = this.state
    const { resetKeys } = this.props

    // There's an edge case where if the thing that triggered the error
    // happens to *also* be in the resetKeys array, we'd end up resetting
    // the error boundary immediately. This would likely trigger a second
    // error to be thrown.
    // So we make sure that we don't check the resetKeys on the first call
    // of cDU after the error is set
    if (error !== null && !this.updatedWithError) {
      this.updatedWithError = true
      return
    }

    if (error !== null && changedArray(prevProps.resetKeys, resetKeys)) {
      this.props.onResetKeysChange?.(prevProps.resetKeys, resetKeys)
      this.reset()
    }
  }

  render() {
    const { error } = this.state
    const { fallbackRender, useFallback, fallback, message, fallBackImageStyle, containerStyle } = this.props

    if (error !== null) {
      const props = {
        error,
        message,
        fallBackImageStyle,
        containerStyle,
        resetErrorBoundary: this.resetErrorBoundary,
      }
      if (React.isValidElement(fallback)) {
        return fallback
      } else if (typeof fallbackRender === 'function') {
        return fallbackRender(props)
      } else if (useFallback) {
        return <ErrorFallback {...props} />
      } else {
        throw new Error(
          'Error Boundary requires either a useFallback, fallbackRender',
        )
      }
    }

    return this.props.children
  }
}

export const useErrorHandler = (givenError) => {
  const [error, setError] = React.useState(null)
  if (givenError) throw givenError
  if (error) throw error
  return setError
}

export default ErrorBoundary;