'use client'
import React from 'react'
import { useRouter } from 'next/navigation'

import Link from 'next/link'
export default function Footer(props) {
  let router = useRouter()
  return (
    <div className="bg-gray text-slate-400">
      <div className="mx-auto max-w-7xl px-48 py-6">
        <div className="grid grid-cols-2 gap-4 mt-12">
          <div className="w-full items-center">
            <div className="flex justify-start items-center">
              <Link href="https://apps.apple.com/us/app/cricket-com/id1460360497">
              <img

                className="w-40 mr-2 hover:opacity-60 shadow cursor-pointer transition-all"
                src={'/pngsV2/iosstoreicon.png'}
                alt="ioslogo"
              />
              </Link>
              <Link href="https://play.google.com/store/apps/details?id=com.crictec.cricket">
              <img
                className="w-40 ml-2 hover:opacity-60 cursor-pointer transition-all"
                src={'/pngsV2/androidstoreicon.png'}
                alt="androidlogo"
              />
              </Link>
            </div>

            <p className="text-xs mt-4">Follow us on</p>
            <div className="flex mt-2">
              <a
                className="mr-4 hover:opacity-60 shadow cursor-pointer transition-all"
                rel="noopener noreferrer"
                href="https://www.facebook.com/Cricketcom/"
                target="_blank"
              >
                <img
                  className="h-8"
                  src={'/svgs/facebook.svg'}
                  alt="Facebook"
                />
              </a>
              <a
                className="mr-4 hover:opacity-60 shadow cursor-pointer transition-all"
                rel="noopener noreferrer"
                href="https://twitter.com/weRcricket"
                target="_blank"
              >
                <img className="h-8" src={'/svgs/twitter.svg'} alt="Twitter" />
              </a>
              <a
                className="mr-4 hover:opacity-60 shadow cursor-pointer transition-all"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/company/cricketcom"
                target="_blank"
              >
                <img
                  className="h-8"
                  src={'/svgs/linkedin.svg'}
                  alt="Linkedin"
                />
              </a>
            </div>
          </div>

          <div className="w-full grid grid-cols-3 gap-6 text-sm">
            <div className="flex flex-col items-start">
              <a href="/criclytics" className="mb-4 hover:text-slate-300">
                Criclytics<sup>TM</sup>
              </a>
              <a
                href="/schedule/live-matches"
                className="mb-4 hover:text-slate-300"
              >
                Match Schedule
              </a>
              <a
                href="/fantasy-research-center"
                className="mb-4 hover:text-slate-300"
              >
                Fantasy Center
              </a>
              <a
                href="/series/ongoing/all"
                className="mb-4 hover:text-slate-300"
              >
                All Series
              </a>
              <a href="/news/latest" className="mb-4 hover:text-slate-300">
                Cricket News
              </a>
              <a href="/videos/latest" className="mb-4 hover:text-slate-300">
                Cricket Videos
              </a>
            </div>

            <div className="flex flex-col items-start">
              <a href="/players/all" className="mb-4 hover:text-slate-300">
                Players
              </a>
              <a
                href="/teams/international"
                className="mb-4 hover:text-slate-300"
              >
                Teams
              </a>
              <a href="/stadiums" className="mb-4 hover:text-slate-300">
                Stadiums
              </a>
              <a href="/rankings" className="mb-4 hover:text-slate-300">
                Rankings
              </a>
              <a href="/records" className="mb-4 hover:text-slate-300">
                Records
              </a>
              {/* <a href="/" className="mb-4 hover:text-slate-300">
                Play The Odds
              </a> */}
            </div>

            <div className="flex flex-col items-start">
              <a
                href="/about/privacy-policy"
                className="mb-4 hover:text-slate-300"
              >
                Privacy Policy
              </a>
              <a
                href="/about/terms-and-conditions"
                className="mb-4 hover:text-slate-300"
              >
                Terms of Use
              </a>
              <a
                href="/about/cookies-policy"
                className="mb-4 hover:text-slate-300"
              >
                Cookie Policy
              </a>
            </div>
          </div>
        </div>

        <div className="flex flex-col items-center mt-12">
          <img className="w-fit" src={'/svgs/cricket-logo.svg'} alt="logo" />
          <p className="text-xs font-light text-white mt-1">
            Cricket like never before!
          </p>
        </div>
        <p className="text-slate-500 text-[0.6rem] text-center mt-2">
          @{new Date().getFullYear()} cricket.com | All rights reserved
        </p>
      </div>
    </div>
  )
}
