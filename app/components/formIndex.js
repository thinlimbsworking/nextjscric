import React, { useEffect, useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { HOME_PAGE_FORM_INDEX } from '../api/queries'
const arrow = '/pngsV2/arrow.png'
import { useRouter } from 'next/router'

export default function FormIndex(props) {
  const router = useRouter()
  const navigate = router.push
  const [toggle, setToggle] = useState('')
  const [teamKey, setTeamKey] = useState('team1Data')
  const { loading, error, data } = useQuery(HOME_PAGE_FORM_INDEX, {
    // variables: { matchID: props.matchID},
    variables: { matchID: props.matchID },
    onCompleted: (data) => {
      setToggle(data.homepageFormIndex.team1Name)
    },
  })

  const redirectToFile = () => {
    let matchName = `${props.matchProjection}-${props.seriesName}`
    let seriesSlug = matchName
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()
    navigate(`criclytics/${props.matchID}/${seriesSlug}/form-index`)
  }
  const capsuleCss = {
    verygood: '  text-green  py-1 pl-2 pr-1 text-xs  font-semibold',
    good: '  text-green-4  py-1 pl-2 pr-1 text-xs  font-semibold',
    neutral: '  text-yellow  py-1 pl-2 pr-1 text-xs font-semibold',
    bad: ' text-orange-3  py-1 pl-2 pr-1 text-xs font-semibold',
    verybad: '  text-red  py-1 pl-2 pr-1 text-xs  font-semibold',
  }

  const round = {
    verygood: 'bg-green   rounded-full w-2 h-2   ',
    good: 'bg-green-4  rounded-full w-2 h-2  ',
    neutral: 'bg-yellow  rounded-full w-2 h-2    ',
    bad: 'bg-orange-3   rounded-full w-2 h-2    ',
    verybad: 'bg-red   rounded-full w-2 h-2   ',
  }

  return data && data.homepageFormIndex && data.homepageFormIndex.team1Name ? (
    <div className="bg-gray mx-2 md:mt-0 mt-5 rounded px-3 py-3 text-white">
      <div
        className="flex justify-between items-center"
        onClick={() => redirectToFile()}
      >
        <h3 className="font-semibold text-lg tracking-wider">Form Index</h3>
        <img className="bg-basebg p-1 w-9 rounded-lg" src={arrow} />
      </div>
      <div className="bg-gray-4 rounded-3xl mt-4 flex items-center justify-between text-sm ">
        <div
          className={`w-1/2 text-center rounded-3xl py-1.5  ${
            toggle === data.homepageFormIndex.team1Name
              ? `border-2  border-green`
              : ''
          }`}
          onClick={() => (
            setToggle(data.homepageFormIndex.team1Name), setTeamKey('team1Data')
          )}
        >
          {data.homepageFormIndex.team1Name}
        </div>
        <div
          className={`w-1/2 text-center rounded-3xl py-1.5  ${
            toggle === data.homepageFormIndex.team2Name
              ? `border-2  border-green`
              : ''
          }`}
          onClick={() => (
            setToggle(data.homepageFormIndex.team2Name), setTeamKey('team2Data')
          )}
        >
          {data.homepageFormIndex.team2Name}
        </div>
      </div>

      {
        <div>
          {data.homepageFormIndex[teamKey].map((player, index) => (
            <div
              className="bg-gray-4 rounded-lg mt-4 flex justify-between items-center p-2"
              key={index}
            >
              <div className="flex items-center justify-between">
                <img
                  className="h-10 w-10 bg-gray object-top object-cover rounded-full"
                  src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                />
                <div className="pl-1">
                  <div className="text-xs font-semibold pb-1">
                    {player.playerName}
                  </div>
                  <div className=" text-gray-2 text-[9px] font-medium">
                    <div className="">
                      {' '}
                      {player.playerRole == 'BOWLER'
                        ? 'BOWL AVG:'
                        : 'BAT AVG:'}{' '}
                      {player.avg} |{' '}
                      {player.playerRole == 'BOWLER' ? 'BOWL SR:' : 'BAT SR:'}{' '}
                      {player.sr}{' '}
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex items-center">
                <div
                  className={`${
                    capsuleCss[player.Form.split(' ').join('').toLowerCase()]
                  } uppercase`}
                >
                  {' '}
                  {player.Form}{' '}
                </div>
                <div
                  className={`${
                    round[player.Form.split(' ').join('').toLowerCase()]
                  }`}
                ></div>
              </div>
            </div>
          ))}
        </div>
      }
      <div className="flex justify-between pt-3 pb-1">
        {/* <div className='flex flex-wrap w-full'> */}
        <div className="flex items-center">
          <div className="h-2 w-2 rounded-full bg-green"></div>
          <div className="text-gray-2 text-xs font-thin pl-1">Very Good</div>
        </div>
        <div className="flex items-center justify-center">
          <div className="h-2 w-2 rounded-full bg-lime-500"></div>
          <div className="text-gray-2 text-xs font-thin pl-1">Good</div>
        </div>
        <div className="flex items-center justify-center">
          <div className="h-2 w-2 rounded-full bg-yellow"></div>
          <div className="text-gray-2 text-xs font-thin pl-1">Neutral</div>
        </div>

        <div className="flex items-center">
          <div className="h-2 w-2 rounded-full bg-orange-3"></div>
          <div className="text-gray-2 text-xs font-thin pl-1">Bad</div>
        </div>
        <div className="flex items-center">
          <div className="h-2 w-2 rounded-full bg-red"></div>
          <div className="text-gray-2 text-xs font-thin pl-1">Very Bad</div>
        </div>
        {/* </div> */}
        {/* <div className='bg-gray-4 p-2 rounded-md text-xs font-semibold border text-green border-green'>VIEW ALL</div> */}
      </div>
    </div>
  ) : (
    <div></div>
  )
}
