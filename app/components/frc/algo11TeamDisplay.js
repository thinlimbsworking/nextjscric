import React, { useState, useEffect } from 'react'
import html2canvas from 'html2canvas'
import Link from 'next/link'
import ImageWithFallback from '../commom/Image'
import PostSuccessModal from '../commom/postSuccessModal'
// import backIcon from '../../public/svgs/back_dark_black.svg'
// import stadium from '../../public/pngs/frc_stadium.png'
// import Countdown from 'react-countdown-now';
// import { format } from 'date-fns';
// import { useQuery, useMutation } from '@apollo/react-hooks';

// import { CopyToClipboard } from 'react-copy-to-clipboard';
// import { CREATE_AND_UPDATE_USER_TEAM } from '../../api/queries';
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'

const editIcon = '/svgs/icons/editteam.svg'
const downloadIcon = '/svgs/icons/downloadteam.svg'

export default function Algo11TeamDisplay({
  setclose,
  myTeam,
  lang,
  ...props
}) {
  const playerPlaceholder = '/pngs/fallbackprojection.png'
  const flagPlaceHolder = '/svgs/images/flag_empty.svg'
  const [credits, setcredits] = useState(null)
  const [total, setTotal] = useState(0)
  const [success, setSuccess] = useState(false)

  let playerCategories = [
    {
      name: 'BATTERS',
      value: 'BATSMAN',
    },
    {
      name: 'WICKET_KEEPERS',
      value: 'KEEPER',
    },
    {
      name: 'ALL_ROUNDERS',
      value: 'ALL_ROUNDER',
    },
    {
      name: 'BOWLERS',
      value: 'BOWLER',
    },
  ]

  useEffect(() => {
    let a = myTeam?.[props.status === 'completed' ? 'team' : 'data']?.reduce(
      (total, currentValue) => {
        return parseFloat(total) + parseFloat(currentValue.credits)
      },
      0,
    )
    let b = myTeam?.[props.status === 'completed' ? 'team' : 'data']?.reduce(
      (total, currentValue) => {
        return parseFloat(total) + parseFloat(currentValue.totalPoints)
      },
      0,
    )

    setcredits(a)
    setTotal(b)
  }, [])

  const downloadImage = () => {
    html2canvas(document.getElementById('image-part'), {
      useCORS: true,
      allowTaint: true,
    }).then(function (canvas) {
      var anchorTag = document.createElement('a')
      document.body.appendChild(anchorTag)
      //document.getElementById("previewImg").appendChild(canvas);
      anchorTag.download = `${props.teamName}.jpg`
      anchorTag.href = canvas.toDataURL()
      anchorTag.click()
      anchorTag.remove()
    })
    setSuccess(true)
    setTimeout(() => {
      setSuccess(false)
    }, 3000)
  }

  if (success) return <PostSuccessModal Msg="Download Success" />

  return (
    <>
      <div className="flex flex-col gap-4 ">
        <div
          className="flex flex-col pb-3 text-black dark:text-white  dark:bg-basebg"
          id="image-part"
        >
          <div className="flex items-center justify-around w-full md:my-2 lg:my-2">
            <div className="w-full p-3 flex justify-between items-center">
              <div className="flex items-center gap-2">
                <div
                  onClick={() => setclose(false)}
                  className="p-2 cursor-pointer bg-basered dark:bg-gray rounded-md w-8 h-8"
                >
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/svgs/empty.svg"
                    className=" flex items-center justify-center h-4 w-4 rotate-180"
                    src="/svgsV2/RightSchevronWhite.svg"
                    alt="right"
                  />
                </div>
                <span className="text-base text-black dark:text-white font-semibold ">
                  {myTeam.fantasy_teamName || props.teamName}
                </span>
              </div>
              <div className="flex gap-1 text-black dark:text-gray-2 items-center">
                <span>{lang === 'HIN' ? 'क्रेडिट:' : 'Credits:'}</span>
                <div className="  px-2 py-1 border border-basered bg-none text-basered dark:bg-gray dark:border-none flex items-center justify-center rounded-md dark:text-green ">
                  {total ? total : credits ? credits : 0}
                </div>
              </div>
            </div>
          </div>

          <div className="flex  mx-2 bg-white dark:bg-transparent shadow-sm ">
            <div className="w-full items-center">
              <div className="w-full text-center  white">
                <div className="bb b--white-20 "></div>

                {playerCategories.map((category) => (
                  <div className="center mt-2 text-center">
                    <div className="bb b--white-20  "></div>
                    <div className="text-center   uppercase oswald f5 fw4 p-2">
                      {getLangText(lang, words, category.name)}
                    </div>
                    <div className="w-full  flex flex-wrap   justify-center  items-center">
                      {myTeam?.[props.status === 'completed' ? 'team' : 'data']
                        .filter(
                          (player) => player.player_role === category.value,
                        )
                        .map((item, index) => {
                          return (
                            <div className="w-1/3 md:w-40 lg:w-40 p-2 h-40">
                              <div className="text-black dark:text-white bg-slate-100 dark: dark:bg-gray  h-full p-2   shadow-sm   w-full  rounded-md ">
                                <div className="flex w-full justify-center">
                                  <div className="flex flex-col items-center ">
                                    <div className="relative flex justify-center mb-2 ">
                                      <ImageWithFallback
                                        height={35}
                                        width={35}
                                        loading="lazy"
                                        fallbackSrc="/pngsV2/playerph.png"
                                        className="h-16 w-16  bg-gray-10 dark:bg-gray-4 rounded-full  object-cover object-top"
                                        src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                                        // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                                      />
                                      <ImageWithFallback
                                        height={18}
                                        width={18}
                                        loading="lazy"
                                        fallbackSrc="/svgs/empty.svg"
                                        className="w-4 h-4 drop-shadow-lg absolute left-0 bottom-0 object-center flex justify-center rounded-full ba"
                                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                        // onError={(evt) => (evt.target.src = '/svgs/empty.svg')}
                                      />
                                      {(item.captain == '1' ||
                                        item.vice_captain == '1') && (
                                        <div className="w-5 border text-green bg-gray-4 h-5 absolute  right-[-4px] bottom-0 rounded-md ba flex justify-center items-center text-xs font-medium">
                                          {item.captain == '1'
                                            ? 'C'
                                            : item.vice_captain == '1'
                                            ? 'VC'
                                            : ''}
                                        </div>
                                      )}
                                    </div>
                                    <p className="text-sm font-semibold flex justify-center mx-auto  w-24    ">
                                      {lang === 'HIN'
                                        ? item.playerNameHindi ||
                                          item.playerName
                                        : item.playerName}
                                    </p>
                                    <p className=" text-center text-gray-2 text-xs ">
                                      {item.totalPoints
                                        ? getLangText(lang, words, 'POINTS')
                                        : getLangText(
                                            lang,
                                            words,
                                            'Credits',
                                          )}{' '}
                                      <span className="text-basered dark:text-green font-bold">
                                        {' '}
                                        {item.totalPoints || item.credits}
                                      </span>{' '}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="flex gap-1 md:bg-black md:py-4 text-xs md:mx-2 dark:w-full justify-center items-center mt-3 text-white">
            <span>Powered by </span>
            <ImageWithFallback
              height={28}
              width={28}
              loading="lazy"
              className="w-16"
              src="/cricket.com.svg"
            />
          </div>
        </div>
        <div className="mt-2  p-2 w-full  text-center flex justify-center gap-3">
          <div
            onClick={downloadImage}
            className="flex border cursor-pointer bg-white hover:opacity-70 transition ease-in duration-150 border-basered text-basered dark:border-green dark:bg-gray-4  text-xs items-center justify-center gap-1 p-2 rounded dark:text-green"
          >
            {' '}
            <ImageWithFallback
              height={18}
              width={18}
              loading="lazy"
              fallbackSrc="/pngsV2/flag_dark.png"
              src={downloadIcon}
              alt={'download team'}
            />{' '}
            <span> {getLangText(lang, words, 'downloadTeam')} </span>
          </div>

          {props.calledBy === 'myTeams' && (
            <Link
              href={{
                pathname: `/fantasy-research-center/${props.matchID}/${
                  props.seriesSlug
                }/${
                  props.leagueType === 'statsHub'
                    ? 'fantasy-stats/players'
                    : 'create-team'
                }`,
                query: { ffCode: props.ffCode },
              }}
              passHref
              legacyBehavior
            >
              <div
                onClick={(e) => {
                  setclose(true)
                  props.handleEditTeam(e)
                }}
                className="flex border cursor-pointer bg-white hover:opacity-70 transition ease-in duration-150 border-basered text-basered dark:border-green dark:bg-gray-4  text-xs items-center justify-center gap-1 p-2 rounded dark:text-green"
              >
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  src={editIcon}
                  alt={'edit team'}
                />{' '}
                <span> {lang === 'HIN' ? 'टीम एडिट करें' : 'EDIT TEAM'}</span>
              </div>
            </Link>
          )}
        </div>
      </div>
    </>
  )
}
