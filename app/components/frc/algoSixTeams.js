import { useMutation, useQuery } from '@apollo/react-hooks'
import CleverTap from 'clevertap-react'
import { format } from 'date-fns'
import { useRouter } from 'next/navigation'
import React, { useEffect, useRef, useState } from 'react'
import {
  CREATE_AND_UPDATE_USER_TEAM,
  GET_FANTASY_SIX_TEAMS,
} from '../../api/queries'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import Heading from '../commom/heading'
import ImageWithFallback from '../commom/Image'
import Algo11TeamDisplay from './algo11TeamDisplay.js'
import FrcMatchLayout from './frcMatchLayout'
import SlideComp from '../commom/slideComp'
// import Swiper from 'react-id-swiper';

export default function AlgoSixTeams(props) {
  let scrl = useRef(null)

  let router = useRouter()
  let lang = props.language
  //
  let FantasymatchID = props.matchID

  const [click, setclick] = useState(false)
  const [index, setIndex] = useState()
  const [toggle, setToggle] = useState(false)
  const [windows, setLocalStorage] = useState({})
  const [newToken, setToken] = useState('')
  const [copyAllCodes, setCopyAllCodes] = useState(false)
  const [teamCode, setTeamCode] = useState('')
  const [teamCodeClick, setteamCodeClick] = useState(false)
  const [myTeam, setMyTeam] = useState({})

  const [algo11Code, setalgo11Code] = useState('')
  //
  const [sixteam, setsixteam] = useState(null)
  //
  //

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window })
      setToken(global.window.localStorage.getItem('tokenData'))
    }
  }, [global.window])

  const { loading, error, data } = useQuery(GET_FANTASY_SIX_TEAMS, {
    variables: { matchID: FantasymatchID, token: newToken },
    onCompleted: (data) => {
      //
      // let arrayMerge=[...algo11.getAlgo11.batsman, ...algo11.getAlgo11.all_rounder, ...algo11.getAlgo11.keeper, ...algo11.getAlgo11.bowler];
      // let captain=arrayMerge.filter(player=>player.captain==="1")
      // let viceCaptain=arrayMerge.filter(player=>player.vice_captain==="1")
      // let rem_player=arrayMerge.filter(player=>player.vice_captain!=="1" &&player.captain!=="1" )
      // setalgoTeam(algo11.getAlgo11.batsman && [...captain, ...viceCaptain,...rem_player]);
      if (data) {
        let sortedData = data.getFantasySixTeams.map((team) => {
          let batsman = team.batsman ? team.batsman : []
          let all_rounder = team.all_rounder ? team.all_rounder : []
          let keeper = team.keeper ? team.keeper : []
          let bowler = team.bowler ? team.bowler : []
          let arrayMerge = [...batsman, ...all_rounder, ...keeper, ...bowler]
          // let arrayMerge=[...team.batsman, ...team.all_rounder, ...team.keeper, ...team.bowler];
          let captain = arrayMerge.filter((player) => player.captain === '1')
          //
          let viceCaptain = arrayMerge.filter(
            (player) => player.vice_captain === '1',
          )
          //
          let rem_player = arrayMerge.filter(
            (player) => player.vice_captain !== '1' && player.captain !== '1',
          )
          //

          return {
            data: [...captain, ...viceCaptain, ...rem_player],
            teamtotalpoints: team.teamtotalpoints,
            timestamp: team.timestamp,
            totalProjectedPoints: team.totalProjectedPoints,
            fantasy_teamName: team.fantasy_teamName,
            teamTagline: team.teamTagline,
          }
        })
        //
        setsixteam(sortedData)
      }
    },
  })

  const [getAlgo11Code] = useMutation(CREATE_AND_UPDATE_USER_TEAM, {
    onCompleted: (data) => {
      setalgo11Code(data.createAndUpdateUserTeam.ffCode)
    },
  })
  const getAlgo11TeamDetails = (a, b) => {
    //
    //
    getAlgo11Code({
      variables: {
        matchID: FantasymatchID,
        token: newToken,
        team: {
          teamName: a,
          leagueType: '',
          selectCriteria: '',
          ffCode: '',
          team: b,
          isAlogo11: true,
        },
      },
    })
  }

  const fantasyTrack = () => {
    CleverTap.initialize('FantasyBYT', {
      Source: 'NewTeam',
      MatchID: props.matchID,
      SeriesID: seriesID || '',
      TeamAID: data[0].homeTeamID,
      TeamBID: data[0].awayTeamID,
      MatchFormat: data[0].matchType,
      MatchStartTime: format(Number(data[0].matchDateTimeGMT), 'd,y, h:mm a'),
      MatchStatus: data[0].matchStatus,
      Platform:
        windows && windows.localStorage ? windows.localStorage.Platform : '',
    })
  }

  const getUpdatedTime = (updateTime) => {
    // let uT=1618808000000;
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))

    let result =
      (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
      minutes +
      (minutes > 1 ? ' mins ' : ' min ') +
      (lang === 'HIN' ? 'पहले' : 'ago')
    return result
  }

  if (loading) {
    ;<div></div>
  }

  const scrollCheck = () => {
    setscrollX(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  return !click ? (
    <FrcMatchLayout
      titleName={getLangText(lang, words, 'Cricket.com teams')}
      matchData={props.matchData.miniScoreCard.data[0]}
      updateLanguage={props.setlanguage}
    >
      {/* <div className=' z-1 px-2 h2-4  flex items-center'>
        <img className='w1' onClick={() => window.history.back()} alt='' src={backIcon} />
        <div className='black f6 fw7 pl3 uppercase tc'>{getLangText(lang, words, 'Cricket.com teams')}</div>
      </div> */}
      {/* <MobileHeader title={getLangText(lang, words, "Cricket.com teams")} /> */}

      {true ? (
        <div className="px-3 md:px-0 lg:px-0">
          <div className=" dark:text-white text-gray-2 text-sm flex md:justify-end lg:justify-end mb-1">
            {`*${getLangText(lang, words, 'Last Updated')} ${getUpdatedTime(
              sixteam?.[0]?.timestamp,
            )}`}
          </div>
          {!click &&
            sixteam &&
            sixteam.length > 0 &&
            sixteam.map((item, index) => {
              return (
                <div className="flex items-center flex-col">
                  <div className=" w-full mb-2">
                    <div className="flex items-center justify-around w-full  lg:my-3 md:my-3 xl:my-3">
                      <div className="w-full ">
                        <div className="flex">
                          <Heading
                            heading={item.fantasy_teamName}
                            subHeading={item.teamTagline}
                            subHeadingClass={'text-black text-sm tracking-wide pt-1 dark:text-yellow-400'}
                          />
                        </div>
                      </div>
                      {/* <div className="w-full flex  items-center justify-end">
                        <div
                          className="border cursor-pointer rounded-md text-xs border-basered dark:border-green w-6/12 text-center p-2 text-basered dark:text-green font-bold"
                          onClick={() => (
                            setToggle(true),
                            setteamCodeClick(false),
                            setalgo11Code(''),
                            getAlgo11TeamDetails(
                              item.fantasy_teamName,
                              item.data,
                            )
                          )}
                        >
                          COPY CODE
                        </div>
                      </div> */}
                    </div>

                    {/* {lang === 'HIN' ? 'फ़ैंटसी अंक' : 'Fantasy Points'}: {item.teamtotalpoints} */}
                    <div className="  relative rounded-t-lg w-full mt-2">
                      <div className="flex  w-full justify-around items-center bg-gray-4 p-2  rounded-t-lg">
                        <div className="flex w-full gap-1  justify-start items-center text-xs font-bold ">
                          {lang === 'HIN'
                            ? item.teamtotalpoints
                              ? 'फ़ैंटसी अंक :'
                              : 'अनुमानित अंक :'
                            : item.teamtotalpoints
                            ? 'Fantasy Points :'
                            : 'Projected Points :'}{' '}
                          <span className="text-green font-semibold ">
                            {' '}
                            {'  '}{' '}
                            {item.teamtotalpoints || item.totalProjectedPoints}
                          </span>
                        </div>
                        <div
                          className="flex cursor-pointer bg-basered hover:opacity-70 transition ease-in duration-150 dark:bg-transparent rounded-md w-24 p-1  justify-center text-white items-center  text-xs uppercase"
                          onClick={() => (
                            setclick(true), setMyTeam(item), setalgo11Code('')
                          )}
                        >
                          {getLangText(lang, words, 'view_team')}
                        </div>
                      </div>

                      <div
                        className={`flex  w-full bg-white border  shadow-sm dark:border-none rounded-b-lg text-black dark:text-white dark:bg-gray   ${
                          index == 0
                            ? ' border-2  border-green '
                            : 'border-slate-200'
                        }  `}
                      >
                        {true && item && (
                          <SlideComp algoteam={item.data} lang={lang} />
                        )}

                        {/* bowler */}
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      ) : (
        <></>
      )}

      {false ? (
        <div className="p-3  ">
          {!click &&
            sixteam &&
            sixteam.length > 0 &&
            sixteam.map((item, index) => {
              return (
                <div className="flex items-center flex-col ">
                  <div>
                    {lang === 'HIN' ? 'फ़ैंटसी अंक' : 'Fantasy Points'}:{' '}
                    {item.teamtotalpoints}
                    <div className="flex">1</div>
                    <div
                      className="flex bg-red"
                      onClick={() => (
                        setToggle(true),
                        setteamCodeClick(false),
                        setalgo11Code(''),
                        getAlgo11TeamDetails(item.fantasy_teamName, item.data)
                      )}
                    >
                      2
                    </div>
                  </div>

                  <div className="flex  w-full items-center">
                    <div className=" w-full bg-gray-4 relative  my-3 rounded-md">
                      <div className="">
                        <div className="flex  rounded-b-lg p-1 ">
                          <div className="flex bg-gray   overflow-x-auto ">
                            {true && item && (
                              <SlideComp algoteam={item.team} lang={lang} />
                            )}

                            {/* bowler */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      ) : (
        <></>
      )}
    </FrcMatchLayout>
  ) : (
    click && (
      <Algo11TeamDisplay
        setclose={setclick}
        code={algo11Code}
        setMyTeam={setMyTeam}
        myTeam={myTeam}
        data={props.data.miniScoreCard?.data[0]}
        lang={lang}
      />
    )
  )
}
