import React, { useState, useEffect } from 'react'
import LoadingTeam from './loadingTeam.js'
import { useQuery, useMutation, useLazyQuery } from '@apollo/react-hooks'
import CleverTap from 'clevertap-react'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import {
  GET_FRC_TEAM,
  GET_FRC_PLAYER_REPLACEMENT,
  CREATE_AND_UPDATE_USER_TEAM,
  GET_ALL_USER_TEAM,
} from '../../api/queries'
import ImageWithFallback from '../commom/Image.js'
const flagPlaceHolder = '/pngsV2/flag_dark.png'

import Link from 'next/link'
import { getFrcSeriesTabUrl } from '../../api/services'
import Heading from '../commom/heading.js'
import Loading from '../loading.js'
const playerPlaceholder = '/pngsV2/playerph.png'

export default function ChooseTeam({
  playerID,
  setPlayerID,
  chooseLogic,
  leagueType,
  setCurrentFlag,
  FantasymatchID,
  buildTeam,
  editTeamName: teamName,
  ffcode,
  ...props
}) {
  const lang = props.language
  const [openModel, setOpenModel] = useState(false)
  const [replaceSelectedPlayer, setReplaceSelectedPlayer] = useState({})
  const [battingList, setBattingList] = useState([...buildTeam.batsman])
  const [bowlingList, setBowlingList] = useState([...buildTeam.bowler])
  const [keeperList, setKeeperList] = useState([...buildTeam.keeper])
  const [addNew, setAddNew] = useState('')
  const [allRounderList, setAllRounderList] = useState([
    ...buildTeam.all_rounder,
  ])
  const [allPlayers, setAllPlayer] = useState([
    ...buildTeam.batsman,
    ...buildTeam.bowler,
    ...buildTeam.keeper,
    ...buildTeam.all_rounder,
  ])
  const [allPlayersID, setAllPlayerID] = useState([])
  const [totalCredits, setTotalCredits] = useState(
    parseFloat(buildTeam.totalPoints),
  )
  const [captain, setCaptain] = useState(
    [
      ...buildTeam.batsman,
      ...buildTeam.bowler,
      ...buildTeam.keeper,
      ...buildTeam.all_rounder,
    ].find((x, y) => x.captain === '1'),
  )
  const [viceCaptain, setViceCaptain] = useState(
    [
      ...buildTeam.batsman,
      ...buildTeam.bowler,
      ...buildTeam.keeper,
      ...buildTeam.all_rounder,
    ].find((x, y) => x.vice_captain === '1'),
  )

  const [changeC_Vc, setChangeC_Vc] = useState(false)

  const [windows, setLocalStorage] = useState({})
  const [newToken, setToken] = useState('')

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window })
      setToken(global.window.localStorage.getItem('tokenData'))
    }
  }, [global.window])

  useEffect(() => {
    if (openModel) document.body.style.overflow = 'hidden'
    return () => {
      document.body.style = ''
    }
  }, [openModel])

  const [
    getFrcReplacementData,
    { error: replacementerror, loading: replacementloading, data: frcData },
  ] = useLazyQuery(GET_FRC_PLAYER_REPLACEMENT, {
    fetchPolicy: 'network-only',
  })

  if (replacementloading) {
    ;<div></div>
  }

  useEffect(() => {
    setAllPlayerID(allPlayers.map((x, y) => x.playerId))
    //setAllPlayer(allPlayers)
  }, [allPlayers])

  const callNextData = (a) => {
    //
    //
    getFrcReplacementData({
      variables: {
        matchID: FantasymatchID,
        replacementPlayerID: a,
        playerIds: allPlayersID,
        leagueType: leagueType,
        selectCriteria: chooseLogic,
        token: newToken,
      },
    })
  }

  const [createUserTeam] = useMutation(CREATE_AND_UPDATE_USER_TEAM, {
    refetchQueries: [
      {
        query: GET_ALL_USER_TEAM,
        variables: { matchID: FantasymatchID, token: newToken },
      },
    ],
  })

  const updateTeamDetails = () => {
    createUserTeam({
      variables: {
        matchID: FantasymatchID,
        token: global.window.localStorage.getItem('tokenData'),
        team: {
          teamName: teamName,
          leagueType: leagueType,
          selectCriteria: chooseLogic,
          ffCode: ffcode,
          team: allPlayers,
          isAlogo11: false,
        },
      },
    })
  }

  const fantasyTrack = () => {
    CleverTap.initialize('FantasySaveTeam', {
      MatchID: FantasymatchID,
      SeriesID: '',
      TeamAID: props.teamDetails[0].homeTeamID,
      TeamBID: props.teamDetails[0].awayTeamID,
      MatchFormat: props.teamDetails[0].matchType,
      PlayersSelected: playerID.length,
      LogicSelected:
        chooseLogic == 'projected_points'
          ? 'PP'
          : chooseLogic == 'selection_percent'
          ? 'Selection'
          : chooseLogic == 'fantasy_points'
          ? 'FP'
          : chooseLogic == 'dream_team_appearances'
          ? 'DTA'
          : '',
      LeagueSelected:
        leagueType == 'grand_league'
          ? 'Grand'
          : leagueType == 'small_league'
          ? 'Small'
          : leagueType == 'h2h'
          ? 'OneOnOne'
          : '',
      Platform:
        windows && windows.localStorage ? windows.localStorage.Platform : '',
    })
  }
  const addReplacedPlayer = (item) => {
    let batsmen = [...battingList]
    let bowler = [...bowlingList]
    let allround = [...allRounderList]
    let keeper = [...keeperList]

    replaceSelectedPlayer?.player_role === 'BATSMAN'
      ? (batsmen = batsmen.filter(
          (x, y) => x.playerId !== replaceSelectedPlayer?.playerId,
        ))
      : replaceSelectedPlayer?.player_role === 'BOWLER'
      ? (bowler = bowler.filter(
          (x, y) => x.playerId !== replaceSelectedPlayer?.playerId,
        ))
      : replaceSelectedPlayer?.player_role === 'ALL_ROUNDER'
      ? (allround = allround.filter(
          (x, y) => x.playerId !== replaceSelectedPlayer?.playerId,
        ))
      : replaceSelectedPlayer?.player_role === 'KEEPER'
      ? (keeper = keeper.filter(
          (x, y) => x.playerId !== replaceSelectedPlayer?.playerId,
        ))
      : ''

    if (replaceSelectedPlayer?.captain === '1' || addNew === 'captain') {
      item.player_role === 'BATSMAN'
        ? (batsmen = batsmen.filter((x, y) => x.playerId !== item.playerId))
        : item.player_role === 'BOWLER'
        ? (bowler = bowler.filter((x, y) => x.playerId !== item.playerId))
        : item.player_role === 'ALL_ROUNDER'
        ? (allround = allround.filter((x, y) => x.playerId !== item.playerId))
        : item.player_role === 'KEEPER'
        ? (keeper = keeper.filter((x, y) => x.playerId !== item.playerId))
        : ''
      item = { ...item, captain: '1' }
      let oldCap = { ...replaceSelectedPlayer, captain: '0' }
      setReplaceSelectedPlayer(oldCap)
      setAddNew('')
      replaceSelectedPlayer?.player_role === 'BATSMAN'
        ? batsmen.push(oldCap)
        : replaceSelectedPlayer?.player_role === 'BOWLER'
        ? bowler.push(oldCap)
        : replaceSelectedPlayer?.player_role === 'ALL_ROUNDER'
        ? allround.push(oldCap)
        : replaceSelectedPlayer?.player_role === 'KEEPER'
        ? keeper.push(oldCap)
        : ''

      setCaptain(item)
    }
    if (
      replaceSelectedPlayer?.vice_captain === '1' ||
      addNew === 'vice_captain'
    ) {
      item.player_role === 'BATSMAN'
        ? (batsmen = batsmen.filter((x, y) => x.playerId !== item.playerId))
        : item.player_role === 'BOWLER'
        ? (bowler = bowler.filter((x, y) => x.playerId !== item.playerId))
        : item.player_role === 'ALL_ROUNDER'
        ? (allround = allround.filter((x, y) => x.playerId !== item.playerId))
        : item.player_role === 'KEEPER'
        ? (keeper = keeper.filter((x, y) => x.playerId !== item.playerId))
        : ''
      item = { ...item, vice_captain: '1' }
      let oldVc = { ...replaceSelectedPlayer, vice_captain: '0' }
      setReplaceSelectedPlayer(oldVc)
      setAddNew('')
      replaceSelectedPlayer?.player_role === 'BATSMAN'
        ? batsmen.push(oldVc)
        : replaceSelectedPlayer?.player_role === 'BOWLER'
        ? bowler.push(oldVc)
        : replaceSelectedPlayer?.player_role === 'ALL_ROUNDER'
        ? allround.push(oldVc)
        : replaceSelectedPlayer?.player_role === 'KEEPER'
        ? keeper.push(oldVc)
        : ''
      setViceCaptain(item)
    }

    item.player_role === 'BATSMAN'
      ? batsmen.push(item)
      : item.player_role === 'BOWLER'
      ? bowler.push(item)
      : item.player_role === 'ALL_ROUNDER'
      ? allround.push(item)
      : item.player_role === 'KEEPER'
      ? keeper.push(item)
      : ''
    updateTeam()

    function updateTeam() {
      setBattingList([...batsmen])
      setBowlingList([...bowler])
      setAllRounderList([...allround])
      setKeeperList([...keeper])
      setAllPlayer([...batsmen, ...bowler, ...allround, ...keeper])
    }
  }

  useEffect(() => {
    let a = allPlayers.reduce((total, currentValue) => {
      return parseFloat(total) + parseFloat(currentValue.credits)
    }, 0)

    setTotalCredits(a)
  }, [allPlayers])

  return (
    <div>
      {
        <div className=" mt-5">
          <Heading heading={getLangText(lang, words, 'your_superteam')} />
          {/* <div className='white text-lg font-semibold '>{getLangText(lang, words, 'your_superteam')}</div>
          <div className='bg-blue-9 w-15 h-1 my-2'></div> */}
          {lang === 'HIN' ? (
            <div className="text-gray-2 font-normal text-xs my-2">
              अपनी सुपर टीम तैयार है। पर आप अभी भी इसमें बदलाव कर सकते है| उसके
              बाद आगे बढे
            </div>
          ) : (
            <div className="text-gray-2 font-normal text-xs my-2">
              Your superteam has been assembled. You can still make changes to
              this team here and proceed to the next step.
            </div>
          )}

          <div className="">
            <div className="border  bg-white flex justify-between items-center dark:bg-gray-4 dark:border-0 p-2 rounded-lg">
              <div className="w-4/12 md:w-1/12 lg:w-1/2 text-gray-2 text-sm">
                <div className=" font-medium ttu  text-sm ">
                  {getLangText(lang, words, 'League')}:
                </div>
                {lang === 'HIN' ? (
                  <div className="text-xs font-semibold ttu mt-1 ">{`${
                    leagueType == 'grand_league'
                      ? 'ग्रैंड लीग'
                      : leagueType == 'small_league'
                      ? 'छोटी लीग'
                      : leagueType == 'h2h'
                      ? 'वन ऑन वन'
                      : ''
                  }`}</div>
                ) : (
                  <div className="text-base font-semibold uppercase mt-1 leading-4">{`${
                    leagueType == 'grand_league'
                      ? 'Grand League'
                      : leagueType == 'small_league'
                      ? 'small league'
                      : leagueType == 'h2h'
                      ? 'One v One'
                      : ''
                  }`}</div>
                )}
              </div>
              <div className="w-1/4 flex justify-center text-sm ">
                <div className="bg-light_gray dark:bg-basebg rounded p-3 px-4 flex justify-center items-center">
                  <div className="tc">
                    <div className="text-xs text-gray-2">
                      {getLangText(lang, words, 'Credits')}
                    </div>
                    <div className="text-basered dark:text-green font-semibold text-center text-lg">
                      {totalCredits}
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-4/12 md:w-1/12 lg:w-1/2 text-gray-2 text-right text-sm">
                <div className=" font-medium ttu">
                  {getLangText(lang, words, 'LOGIC')}:
                </div>
                {lang === 'HIN' ? (
                  <div className="text-sm font-semibold ttu mt-1 leading-4 tr">{`${
                    chooseLogic == 'projected_points'
                      ? 'अनुमानित अंक'
                      : chooseLogic == 'selection_percent'
                      ? 'चयन प्रतिशत'
                      : chooseLogic == 'fantasy_points'
                      ? 'फ़ैंटसी अंक'
                      : chooseLogic == 'dream_team_appearances'
                      ? 'ड्रीम टीम की उपस्थिति'
                      : ''
                  }`}</div>
                ) : (
                  <div className=" text-base font-semibold uppercase mt-1 leading-4 tr">{`${
                    chooseLogic == 'projected_points'
                      ? 'Projected points'
                      : chooseLogic == 'selection_percent'
                      ? 'Selection Percentage'
                      : chooseLogic == 'fantasy_points'
                      ? 'Fantasy points'
                      : chooseLogic == 'dream_team_appearances'
                      ? 'DreamTeam Appearance'
                      : ''
                  }`}</div>
                )}
              </div>
            </div>

            {battingList.length > 0 && (
              <div className="center tc white mt-5">
                <div className="text-center pb-2 text-gray-2 text-sm font-medium">
                  {getLangText(lang, words, 'BATTERS')}
                </div>
                <div className="w-100  flex flex-wrap justify-center   items-center">
                  {battingList.map((item, index) => {
                    return (
                      <div className="w-1/3 md:w-40 lg:w-40 p-2 h-40">
                        <div
                          key={index}
                          className={`text-black dark:text-white bg-slate-100 dark: dark:bg-gray cursor-pointer h-full p-2   shadow-sm   w-full  rounded-md flex relative ${
                            item.isMyPick ? 'dark:bg-gray-9' : 'dark:bg-gray'
                          }  items-center justify-center `}
                          onClick={() => (
                            setOpenModel(true),
                            setReplaceSelectedPlayer(item),
                            callNextData(item.playerId)
                          )}
                        >
                          {item.isMyPick && (
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              className=" absolute left-0 top-0 w-8  "
                              src={'/pngsV2/your_pick.png'}
                            />
                          )}
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className="w-5 h-5 md:w-8 md:h-8 lg:w-8 lg:h-8 absolute right-1 top-1 "
                            src={`/pngsV2/frc_change.png`}
                          />
                          <div className="flex justify-center items-center flex-col">
                            <div className="relative bg-gray-10 dark:bg-gray-8 w-16 h-16 rounded-full">
                              <div
                                className={`overflow-hidden w-full h-full rounded-full `}
                              >
                                <ImageWithFallback
                                  height={38}
                                  width={38}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="object-top object-contain w-16  "
                                  src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                  alt=""
                                  // onError={(e) => (e.target.src = playerPlaceholder)}
                                />
                              </div>
                              <div className="absolute bottom-0 right-0   ">
                                <ImageWithFallback
                                  height={22}
                                  width={22}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="w-5 h-5   rounded-full"
                                  src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="mt-2">
                              <div className="text-sm font-semibold text-center">
                                {' '}
                                {lang === 'HIN' && item.playerNameHindi
                                  ? item.playerNameHindi
                                  : item.playerName}
                              </div>
                              <div className="text-center text-xs text-gray-2">
                                {' '}
                                {getLangText(lang, words, 'Credits')}:{' '}
                                <span className="text-basered dark:text-green font-medium">
                                  {item.credits}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
            {/* <div className='bg-gray-2 h-[1px] w-full my-3 '></div> */}
            {keeperList.length > 0 && (
              <div className="center tc white mt-5">
                <div className="text-center pb-2 text-gray-2 text-sm font-medium">
                  {getLangText(lang, words, 'WICKET_KEEPERS')}
                </div>
                <div className="w-full flex flex-wrap justify-around   items-center">
                  {keeperList.map((item, index) => {
                    return (
                      <div className="w-1/3 md:w-40 lg:w-40 p-1 h-40">
                        <div
                          key={index}
                          className={`text-black dark:text-white bg-slate-100 dark:bg-gray cursor-pointer h-full p-2   shadow-sm   w-full  rounded-md flex relative ${
                            item.isMyPick ? 'dark:bg-gray-9' : 'dark:bg-gray'
                          }  items-center justify-center `}
                          onClick={() => (
                            setOpenModel(true),
                            setReplaceSelectedPlayer(item),
                            callNextData(item.playerId)
                          )}
                        >
                          {item.isMyPick && (
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              className=" absolute left-0 top-0 w-8  "
                              src={'/pngsV2/your_pick.png'}
                            />
                          )}
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className="w-5 h-5 md:w-8 md:h-8 lg:w-8 lg:h-8 absolute right-1 top-1  "
                            src={`/pngsV2/frc_change.png`}
                          />
                          <div className="flex justify-center items-center flex-col">
                            <div className="relative bg-gray-10 dark:bg-gray-8 w-16 h-16 rounded-full">
                              <div
                                className={`overflow-hidden w-full h-full rounded-full `}
                              >
                                <ImageWithFallback
                                  height={38}
                                  width={38}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="object-top object-contain w-16  "
                                  src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                  alt=""
                                  // onError={(e) => (e.target.src = playerPlaceholder)}
                                />
                              </div>
                              <div className="absolute bottom-0 right-0 ">
                                <ImageWithFallback
                                  height={22}
                                  width={22}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className=" w-5 h-5   rounded-full"
                                  src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="mt-2">
                              <div className="text-sm font-semibold text-center">
                                {' '}
                                {lang === 'HIN' && item.playerNameHindi
                                  ? item.playerNameHindi
                                  : item.playerName}
                              </div>
                              <div className="text-center text-xs text-gray-2">
                                {' '}
                                {getLangText(lang, words, 'Credits')}:{' '}
                                <span className="text-basered dark:text-green font-medium">
                                  {item.credits}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
            {/* <div className='bg-gray-2 h-[1px] w-full my-3 '></div> */}

            {allRounderList.length > 0 && (
              <div className="center tc white mt-5">
                <div className="text-center pb-2 text-gray-2 text-sm font-medium">
                  {getLangText(lang, words, 'ALL_ROUNDERS')}
                </div>
                <div className="w-100  flex flex-wrap justify-center   items-center">
                  {allRounderList.map((item, index) => {
                    return (
                      <div className="w-1/3 md:w-40 lg:w-40 p-2 h-40">
                        <div
                          key={index}
                          className={`text-black dark:text-white bg-slate-100 dark: dark:bg-gray cursor-pointer h-full p-2   shadow-sm   w-full  rounded-md flex relative ${
                            item.isMyPick ? 'dark:bg-gray-9' : 'dark:bg-gray'
                          }  items-center justify-center `}
                          onClick={() => (
                            setOpenModel(true),
                            setReplaceSelectedPlayer(item),
                            callNextData(item.playerId)
                          )}
                        >
                          {item.isMyPick && (
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              className=" absolute left-0 top-0 w-8  "
                              src={'/pngsV2/your_pick.png'}
                            />
                          )}
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className="w-5 h-5 md:w-8 md:h-8 lg:w-8 lg:h-8 absolute right-1 top-1"
                            src={`/pngsV2/frc_change.png`}
                          />
                          <div className="flex justify-center items-center flex-col">
                            <div className="relative bg-gray-10 dark:bg-gray-8 w-16 h-16 rounded-full">
                              <div
                                className={`overflow-hidden w-full h-full rounded-full `}
                              >
                                <ImageWithFallback
                                  height={38}
                                  width={38}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="object-top object-contain w-16  "
                                  src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                  alt=""
                                  // onError={(e) => (e.target.src = playerPlaceholder)}
                                />
                              </div>
                              <div className="absolute bottom-0 right-0 ">
                                <ImageWithFallback
                                  height={22}
                                  width={22}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="w-5 h-5   rounded-full"
                                  src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="mt-2">
                              <div className="text-sm font-semibold text-center">
                                {' '}
                                {lang === 'HIN' && item.playerNameHindi
                                  ? item.playerNameHindi
                                  : item.playerName}
                              </div>
                              <div className="text-center text-xs text-gray-2">
                                {' '}
                                {getLangText(lang, words, 'Credits')}:{' '}
                                <span className="text-basered dark:text-green font-medium">
                                  {item.credits}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}

            {/* <div className='bg-gray-2 h-[1px] w-full my-3 '></div> */}

            {bowlingList.length > 0 && (
              <div className="center mt-5 tc white">
                <div className="text-center pb-2 text-gray-2 text-sm font-medium">
                  {getLangText(lang, words, 'BOWLERS')}
                </div>
                <div className="w-100  flex flex-wrap justify-center   items-center">
                  {bowlingList.map((item, index) => {
                    return (
                      <div className="w-1/3 md:w-40 lg:w-40 p-2 h-40">
                        <div
                          key={index}
                          className={`text-black dark:text-white bg-slate-100 dark: dark:bg-gray cursor-pointer h-full p-2   shadow-sm   w-full  rounded-md flex relative ${
                            item.isMyPick ? 'dark:bg-gray-9' : 'dark:bg-gray'
                          }  items-center justify-center `}
                          onClick={() => (
                            setOpenModel(true),
                            setReplaceSelectedPlayer(item),
                            callNextData(item.playerId)
                          )}
                        >
                          {item.isMyPick && (
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              className=" absolute left-0 top-0 w-8  "
                              src={'/pngsV2/your_pick.png'}
                            />
                          )}
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className="w-5 h-5 md:w-8 md:h-8 lg:w-8 lg:h-8 absolute right-1 top-1"
                            src={`/pngsV2/frc_change.png`}
                          />
                          <div className="flex justify-center items-center flex-col">
                            <div className="relative bg-gray-10 dark:bg-gray-8 w-16 h-16 rounded-full">
                              <div
                                className={`overflow-hidden w-full h-full rounded-full `}
                              >
                                <ImageWithFallback
                                  height={38}
                                  width={38}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="object-top object-contain w-16  "
                                  src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                  alt=""
                                  // onError={(e) => (e.target.src = playerPlaceholder)}
                                />
                              </div>
                              <div className="absolute bottom-0 right-0 ">
                                <ImageWithFallback
                                  height={22}
                                  width={22}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="w-5 h-5   rounded-full"
                                  src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="mt-2">
                              <div className="text-sm font-semibold text-center">
                                {' '}
                                {lang === 'HIN' && item.playerNameHindi
                                  ? item.playerNameHindi
                                  : item.playerName}
                              </div>
                              <div className="text-center text-xs text-gray-2">
                                {' '}
                                {getLangText(lang, words, 'Credits')}:{' '}
                                <span className="text-basered dark:text-green font-medium">
                                  {item.credits}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
          </div>
          {/* <div className='bg-gray-2 h-[1px] w-full my-3 '></div> */}

          <div className="mt-5">
            <div className="w-full flex justify-between mt-2 md:justify-around lg:justify-around white items-center">
              {/* {allPlayers.filter((id, i) => id.captain == "1").map((item, index) => {
              return ( */}
              <div
                className="w-[48%] md:w-[28%] lg:w-[28%] cursor-pointer"
                onClick={() => (
                  setChangeC_Vc(true),
                  setReplaceSelectedPlayer(captain),
                  setAddNew('captain')
                )}
              >
                <div className="bg-basered dark:bg-green br--top br2 text-white pt1 f8 fw7 w15 tc shadow-4 shadow-5 w-2/12 text-center rounded-t ">
                  C
                </div>
                <div className=" dark:border-none bg-slate-100 dark:bg-gray   tc relative flex items-center justify-center p-2 rounded-md">
                  {/* {} */}
                  {captain && captain?.isMyPick && (
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className=" absolute left-0 top-0 w-10"
                      src={'/pngsV2/your_pick.png'}
                    />
                  )}

                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    className="w-5 h-5 md:w-6 md:h-6 lg:w-6 lg:h-6 absolute right-1 top-1"
                    src={'/pngsV2/frc_change.png'}
                  />
                  <div className="w-[5%]"></div>
                  <div className="flex gap-3 items-center w-[95%] p-1 ">
                    <div className="relative bg-gray-10 dark:bg-gray-8 w-16 h-16 rounded-full">
                      <div
                        className={`overflow-hidden w-full h-full rounded-full `}
                      >
                        <ImageWithFallback
                          height={38}
                          width={38}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="object-top object-contain w-14  "
                          src={`https://images.cricket.com/players/${
                            captain && captain?.playerId
                          }_headshot.png`}
                          alt=""
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                        />
                      </div>
                      <div className="absolute bottom-0 right-0 ">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="w-5 h-5   rounded-full"
                          src={`https://images.cricket.com/teams/${
                            captain && captain?.teamID
                          }_flag_safari.png`}
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          alt=""
                        />
                      </div>
                    </div>
                    <div className="text-sm font-semibold pl-1  tracking-tight items--center ">
                      {/* {} */}
                      <div className="">
                        {' '}
                        {captain && lang === 'HIN' && captain?.playerNameHindi
                          ? captain?.playerNameHindi
                          : captain
                          ? captain?.playerName
                          : 'Choose Captain'}{' '}
                      </div>
                      <div className="text-xs text-normal tracking-tighter">
                        {' '}
                        {getLangText(lang, words, 'Credits')}:{' '}
                        <span className="ph1 text-green text-sm font-semibold ">
                          {captain ? captain?.credits : '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div
                className="w-[48%] md:w-[28%] lg:w-[28%] cursor-pointer"
                onClick={() => (
                  setChangeC_Vc(true),
                  setReplaceSelectedPlayer(viceCaptain),
                  setAddNew('vice_captain')
                )}
              >
                <div className="bg-basered dark:bg-green br--top br2 text-white pt1 f8 fw7 w15 tc shadow-4 shadow-5  w-2/12 text-center rounded-t ">
                  VC
                </div>
                <div className=" dark:border-none bg-slate-100 dark:bg-gray   tc relative flex items-center justify-center p-2 rounded-md">
                  {/* {} */}
                  {viceCaptain?.isMyPick && (
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className=" absolute left-0 top-0 w-10"
                      src={'/pngsV2/your_pick.png'}
                    />
                  )}

                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    className="w-5 h-5 md:w-6 md:h-6 lg:w-6 lg:h-6 absolute right-1 top-1"
                    src={'/pngsV2/frc_change.png'}
                  />
                  <div className="w-[5%]"></div>
                  <div className="flex gap-3 items-center w-[95%] p-1 ">
                    <div className="relative bg-light_gray dark:bg-gray-8 w-16 h-16 rounded-full">
                      <div
                        className={`overflow-hidden w-full h-full rounded-full `}
                      >
                        <ImageWithFallback
                          height={38}
                          width={38}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="object-top object-contain w-14  "
                          src={`https://images.cricket.com/players/${viceCaptain?.playerId}_headshot.png`}
                          alt=""
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                        />
                      </div>
                      <div className="absolute bottom-0 right-0 ">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="w-5 h-5   rounded-full"
                          src={`https://images.cricket.com/teams/${viceCaptain?.teamID}_flag_safari.png`}
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          alt=""
                        />
                      </div>
                    </div>
                    <div className="text-sm font-semibold pl-2 tracking-tight  tl">
                      {/* {} */}
                      <div className="">
                        {' '}
                        {lang === 'HIN' && viceCaptain?.playerNameHindi
                          ? viceCaptain?.playerNameHindi
                          : viceCaptain
                          ? viceCaptain?.playerName
                          : 'Choose Captain'}{' '}
                      </div>
                      <div className="text-xs  text-normal tracking-tighter">
                        {' '}
                        {getLangText(lang, words, 'Credits')}:{' '}
                        <span className="ph1 text-green text-sm font-semibold ">
                          {viceCaptain?.credits || '--'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="h-12 "></div>

          {/* {captain && viceCaptain ? (
            <Link
              {...getFrcSeriesTabUrl(props.teamDetails[0], 'my-teams')}
              passHref
              legacyBehavior>
              <div className=' bottom-0 fixed  z-999 left-0  right-0  cursor-pointer '>
                <div className=' bg-gray-4 w-100 mw75-l center  shadow-4 h-20  flex items-center justify-between px-4'>
                  <div className='text-xs font-medium'>
                    Save the team once
                    <br /> finalise the players
                  </div>
                  <div
                    className={` ${captain && viceCaptain ? 'bg-green' : 'bg-white-30'
                      } text-xs  font-semibold bg-gray-8 border border-green px-2 py-2 text-green rounded-md uppercase`}
                    onClick={() => {
                      updateTeamDetails(), fantasyTrack();
                    }}>
                    {getLangText(lang, words, 'Save_team')}
                  </div>
                </div>
              </div>
            </Link>
          ) : (
            <div className=' bottom-0  fixed z-999 left-0  right-0  cursor-pointer'>
              <div className=' bg-gray/80 w-100 mw75-l center  shadow-4 h3  flex items-center'>
                <div
                  className={`dib db bg-white-30 pa2 br2 ttu white f6 fw6 center flex justify-center w-40 w-25-l shadow-4  ttu`}>
                  {getLangText(lang, words, 'Save_team')}
                </div>
              </div>
            </div>
          )} */}

          {captain && viceCaptain ? (
            <Link
              {...getFrcSeriesTabUrl(props.teamDetails[0], 'my-teams')}
              passHref
              legacyBehavior
            >
              <div className="w-full ">
                <div className="flex justify-center items-center shadow-4 cursor-pointer ">
                  <div
                    className={`p-2 ${
                      captain && viceCaptain
                        ? 'bg-basered dark:border-green text-white dark:border-2 rounded-lg dark:bg-gray-8 dark:text-green  '
                        : 'bg-basered dark:border-gray-2 dark:border-2 rounded-lg dark:bg-gray-8 tc   text-white dark:text-green '
                    } uppercase  flex justify-center w-28 text-sm  shadow-4 `}
                    onClick={() => {
                      updateTeamDetails(), fantasyTrack()
                    }}
                  >
                    {getLangText(lang, words, 'Save_team')}{' '}
                  </div>
                </div>
              </div>
            </Link>
          ) : (
            <div className="w-full">
              <div className=" bg-gray/80 w-100 mw75-l center  shadow-4 h3  flex items-center cursor-pointer">
                <div
                  className={`dib db bg-white-30 pa2 br2 ttu white f6 fw6 center flex justify-center w-40 w-25-l shadow-4 `}
                >
                  {getLangText(lang, words, 'Save_team')}{' '}
                </div>
              </div>
            </div>
          )}
        </div>
      }

      {openModel &&
        (replacementloading ? (
          <Loading />
        ) : (
          <div
            className="flex justify-center items-center fixed absolute--fill z-80  mx-auto  cursor-pointer  dark:border-none bg-basebg/70 overflow-y-scroll"
            style={{ backdropFilter: 'blur(10px)' }}
          >
            <div className="md:w-2/3 lg:w-2/3 mx-auto border rounded absolute m-2 ">
              <div className="bg-white  p-4 dark:bg-gray  br3 shadow-4  relative  min-vh-80 pt2 ph2 ">
                <div>
                  <div className="flex justify-between  items-center mv2 mh3-l ">
                    <div className="w-9"></div>
                    <div className="text-gray-2 font-medium w-36 text-center">
                      selected
                      {/* {getLangText(lang, words, 'selected player')} */}
                    </div>
                    {/* <div className="f6 fw5 ttu">{`${chooseLogic == 'projected_points' ? 'Projected points' : chooseLogic == 'selection_percent' ? "Selection Percentage" : chooseLogic == 'fantasy_points' ? "Fantasy points" : chooseLogic == 'dream_team_appearances' ? "DreamTeam Appearance" : ""}`}</div> */}

                    <div className="dark:bg-gray-4 rounded-md  w-9 py-3 flex justify-center ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-3 "
                        src={'/svgs/close.png'}
                        onClick={() => setOpenModel(false)}
                      />
                    </div>
                  </div>

                  <div className="flex justify-center">
                    <div className=" tc mb-2 relative white mt2 p-2 border dark:border-none dark:bg-gray-4 dark:text-white w-36 h-38 rounded-lg flex items-center flex-col justify-center">
                      <div className="relative  w-20 h-20 center">
                        <ImageWithFallback
                          height={38}
                          width={38}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="overflow-hidden rounded-full w-20 h-20 bg-light_gray dark:bg-gray object-cover object-top"
                          src={`https://images.cricket.com/players/${replaceSelectedPlayer.playerId}_headshot.png`}
                          alt=""
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                        />
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="absolute bottom-0  right-0 h-5 rounded-full w-5 "
                          src={`https://images.cricket.com/teams/${replaceSelectedPlayer.teamID}_flag_safari.png`}
                          alt=""
                          // onError={(e) => (e.target.src = flagPlaceHolder)}
                        />
                        {/* <img
                        className='absolute bottom-0  left-0 h-5 w-5 '
                        src={`${
                          replaceSelectedPlayer.player_role === 'BATSMAN'
                            ? '/pngsV2/battericon.png'
                            : replaceSelectedPlayer.player_role === 'BOWLER'
                            ? '/pngsV2/bowlericon.png'
                            : ''
                        }`}
                        alt=''
                      /> */}
                      </div>
                      <div className="flex  justify-center items-center items--center pt-2">
                        <div className="flex flex-col justify-between items-center pt1 ph1">
                          <div className="text-xs text-center w-24 font-medium">
                            {lang === 'HIN' &&
                            replaceSelectedPlayer.playerNameHindi
                              ? replaceSelectedPlayer.playerNameHindi
                              : replaceSelectedPlayer.playerName}{' '}
                          </div>
                          <div className=" text-[10px] font-normal pr1 text-gray-2">
                            {' '}
                            {getLangText(lang, words, 'Credits')}:{' '}
                            <span className=" text-green font-medium">
                              {replaceSelectedPlayer.credits}
                            </span>
                          </div>

                          {/* {replaceSelectedPlayer.projectedPoints && <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{replaceSelectedPlayer.projectedPoints}</span></div>} */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="h-[1px] border-b my-1 "></div>
                <div className="overflow-y-scroll overflow-hidden hidescroll max-h-96">
                  <div className="w-full  flex flex-wrap justify-between justify-around-l items-center">
                    {frcData && (
                      <div className="">
                        <div className="text-center text-sm font-medium capitalize py-3 text-gray-2">
                          Top Replacement
                          {/* {getLangText(lang, words, 'Top Replacement')} */}
                        </div>
                        <div className="flex justify-around flex-wrap mh2-l">
                          {frcData.getFrcPlayerReplacement.best_replacements.map(
                            (item, index) => {
                              return (
                                <div
                                  key={index}
                                  className="tc mx-1 mb-2 relative white mt2 pv1 pv2-l border dark:border-none cursor-pointer dark:bg-gray-4 w-32 h-40 rounded-lg flex items-center flex-col justify-center"
                                  onClick={() => (
                                    setOpenModel(false),
                                    addReplacedPlayer(item, 'player')
                                  )}
                                >
                                  <div className="flex flex-col items-center justify-center">
                                    <div className="relative   w-16 h-16 center">
                                      <ImageWithFallback
                                        height={38}
                                        width={38}
                                        loading="lazy"
                                        fallbackSrc={playerPlaceholder}
                                        className="overflow-hidden rounded-full w-16 h-16 bg-light_gray dark:bg-gray object-cover object-top"
                                        src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                        alt=""
                                        // onError={(e) => (e.target.src = playerPlaceholder)}
                                      />
                                      <ImageWithFallback
                                        height={22}
                                        width={22}
                                        loading="lazy"
                                        fallbackSrc={playerPlaceholder}
                                        className="absolute bottom-0  right-0 h-5 rounded-full w-5  "
                                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                        alt=""
                                        // onError={(e) => (e.target.src = flagPlaceHolder)}
                                      />
                                      {/* <img
                                    className='absolute bottom-0  left-0 h-5 w-5 '
                                    src={`${
                                      item.player_role === 'BATSMAN'
                                        ? '/pngsV2/battericon.png'
                                        : item.player_role === 'BOWLER'
                                        ? '/pngsV2/bowlericon.png'
                                        : ''
                                    }`}
                                    alt=''
                                  /> */}
                                    </div>

                                    <div className=" justify-start items--center pt-2">
                                      <div className="text-xs w-24 text-center font-medium">
                                        {lang === 'HIN' && item.playerNameHindi
                                          ? item.playerNameHindi
                                          : item.playerName}{' '}
                                      </div>
                                      <div className="flex justify-center  items-center pt1 ph1 text-center">
                                        <div className=" text-[10px] font-normal pr1 text-gray-2 text-center">
                                          {' '}
                                          {getLangText(
                                            lang,
                                            words,
                                            'Credits',
                                          )}:{' '}
                                          <span className=" text-green font-medium">
                                            {item.credits}
                                          </span>
                                        </div>

                                        {/* {replaceSelectedPlayer.projectedPoints && <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{replaceSelectedPlayer.projectedPoints}</span></div>} */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              )
                            },
                          )}
                        </div>
                        <div className="ttu text-center  py-2 white pv2 ml2-l text-sm text-gray-2 ">
                          {getLangText(lang, words, 'other replacements')}
                        </div>
                        <div className="flex justify-around flex-wrap mh2-l">
                          {frcData.getFrcPlayerReplacement.other_replacements.map(
                            (item, index) => {
                              return (
                                <div
                                  key={index}
                                  className="tc mx-1 mb-2 relative white cursor-pointer border dark:border-none dark:bg-gray-4 w-32 h-40 rounded-lg flex items-center flex-col justify-center"
                                  onClick={() => (
                                    setOpenModel(false),
                                    addReplacedPlayer(item, 'player')
                                  )}
                                >
                                  <div className="flex flex-col items-center justify-center">
                                    <div className="relative   w-16 h-16 center">
                                      <ImageWithFallback
                                        height={38}
                                        width={38}
                                        loading="lazy"
                                        fallbackSrc={playerPlaceholder}
                                        className="overflow-hidden rounded-full w-16 h-16 bg-light_gray dark:bg-gray object-cover object-top"
                                        src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                        alt=""
                                        // onError={(e) => (e.target.src = playerPlaceholder)}
                                      />
                                      <ImageWithFallback
                                        height={22}
                                        width={22}
                                        loading="lazy"
                                        fallbackSrc={playerPlaceholder}
                                        className="absolute bottom-0  right-0 h-5 rounded-full w-5 "
                                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                        alt=""
                                        // onError={(e) => (e.target.src = flagPlaceHolder)}
                                      />
                                      {/* <img
                                    className='absolute bottom-0  left-0 h-5 w-5 '
                                    src={`${
                                      item.player_role === 'BATSMAN'
                                        ? '/pngsV2/battericon.png'
                                        : item.player_role === 'BOWLER'
                                        ? '/pngsV2/bowlericon.png'
                                        : ''
                                    }`}
                                    alt=''
                                  /> */}
                                    </div>

                                    <div className=" justify-start items--center pt-2">
                                      <div className="text-xs w-24 text-center font-medium">
                                        {lang === 'HIN' && item.playerNameHindi
                                          ? item.playerNameHindi
                                          : item.playerName}{' '}
                                      </div>
                                      <div className="flex justify-center  items-center pt1 ph1 text-center">
                                        <div className=" text-[10px] font-normal pr1 text-gray-2 text-center">
                                          {' '}
                                          {getLangText(
                                            lang,
                                            words,
                                            'Credits',
                                          )}:{' '}
                                          <span className=" text-green font-medium">
                                            {item.credits}
                                          </span>
                                        </div>

                                        {/* {replaceSelectedPlayer.projectedPoints && <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{replaceSelectedPlayer.projectedPoints}</span></div>} */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              )
                            },
                          )}
                        </div>

                        <div className="flex justify-around flex-wrap mh2-l">
                          {frcData &&
                            frcData.getFrcPlayerReplacement &&
                            frcData.getFrcPlayerReplacement.locked_players.map(
                              (item, index) => {
                                return (
                                  <div
                                    key={index}
                                    className="tc mx-1 mb-2 relative white mt2 pv1 pv2-l border dark:border-none dark:bg-gray-4  w-32 h-40 rounded-lg flex items-center flex-col justify-center"
                                    onClick={() => (
                                      setOpenModel(false),
                                      addReplacedPlayer(item, 'player')
                                    )}
                                  >
                                    <div className="flex flex-col items-center justify-center">
                                      <div className="relative   w-16 h-16 center">
                                        <ImageWithFallback
                                          height={38}
                                          width={38}
                                          loading="lazy"
                                          fallbackSrc={playerPlaceholder}
                                          className="overflow-hidden rounded-full w-16 h-16 bg-light_gray dark:bg-gray object-cover object-top"
                                          src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                          alt=""
                                          // onError={(e) => (e.target.src = playerPlaceholder)}
                                        />
                                        <ImageWithFallback
                                          height={22}
                                          width={22}
                                          loading="lazy"
                                          fallbackSrc={playerPlaceholder}
                                          className="absolute bottom-0  right-0 h-5 rounded-full w-5 "
                                          src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                          alt=""
                                          // onError={(e) => (e.target.src = flagPlaceHolder)}
                                        />
                                        {/* <img
                                    className='absolute bottom-0  left-0 h-5 w-5 '
                                    src={`${
                                      item.player_role === 'BATSMAN'
                                        ? '/pngsV2/battericon.png'
                                        : item.player_role === 'BOWLER'
                                        ? '/pngsV2/bowlericon.png'
                                        : ''
                                    }`}
                                    alt=''
                                  /> */}
                                      </div>

                                      <div className=" justify-start items--center pt-2">
                                        <div className="text-xs font-medium">
                                          {lang === 'HIN' &&
                                          item.playerNameHindi
                                            ? item.playerNameHindi
                                            : item.playerName}{' '}
                                        </div>
                                        <div className="flex justify-center  items-center pt1 ph1 text-center">
                                          <div className=" text-[10px] font-normal pr1 text-gray-2 text-center">
                                            {' '}
                                            {getLangText(
                                              lang,
                                              words,
                                              'Credits',
                                            )}
                                            :{' '}
                                            <span className=" text-green font-medium">
                                              {item.credits}
                                            </span>
                                          </div>

                                          {/* {replaceSelectedPlayer.projectedPoints && <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{replaceSelectedPlayer.projectedPoints}</span></div>} */}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                )
                              },
                            )}
                        </div>
                      </div>
                    )}
                    {/* {changeC_Vc && allPlayers.filter((id, i) => (id.captain !== "1" && id.vice_captain !== "1")).map((item, index) => {
                  return (
                    <div key={index} className='ba b--white-20 br3 tc relative white mt2 pv1 pv2-l dib cursor-pointer bg-white-10 w-49 w-45-l' onClick={() => (setOpenModel(false), setChangeC_Vc(false), addReplacedPlayer(item, replaceSelectedPlayer.captain == 1 ? "captain" : "viceCaptain"))}>
                      <div className="">
                        <div className="relative flex justify-center items--center w2-6 h2-6 center">
                          <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                            <img className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" onError={(e) => (e.target.src = playerPlaceholder)} />
                          </div>
                          <img className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                        </div>
                        <div className=" f8 justify-start items--center pt1">
                          <div className="mr1 oswald"> {item.playerName}  </div>
                          <div className="flex justify-center pt1 ph1">
                            <div className="mt1 fw5 f10 pr1"> Credits: <span className="ph1 black fw5  br2 bg-yellow oswald">{item.credits}</span></div>
                            {item.projectedPoints && <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{item.projectedPoints}</span></div>}
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })} */}
                  </div>
                </div>
                {/* <div className="flex justify-center pv1">
              <img className="w12" src={'/svgs/gold_arrow.svg'} />
            </div> */}
              </div>
            </div>
          </div>
        ))}

      {/* #################################---for captain and vice captain ---------------################################### */}

      {changeC_Vc && (
        <div
          className="flex   justify-center items-center fixed absolute--fill z-9999 bg-basebg/70 overflow-y-scroll"
          style={{ backdropFilter: 'blur(10px)' }}
        >
          <div className="md:w-2/3 lg:w-2/3 mx-auto border rounded absolute m-2 ">
            <div className="bg-white p-4 dark:bg-gray  shadow-4  ba b--white-20 relative  max-vh-80 pt2 ph2 ">
              <div>
                <div className="flex justify-center items-center p-2 ">
                  <div className=" text-center w-full text-gray-2 ">
                    {getLangText(lang, words, 'selected player')}
                  </div>
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    className="w-4 h-4 cursor-pointer"
                    src={'/svgs/close.png'}
                    onClick={() => setChangeC_Vc(false)}
                  />
                </div>

                <div className="flex items-center justify-center">
                  <div className="tc mx-1 mb-2 relative white mt2 pv1 pv2-l dib border dark:border-none bg-white dark:bg-gray-4 w-32 h-40 rounded-lg flex items-center flex-col justify-center">
                    <div className="flex flex-col items-center justify-center ">
                      <div className="overflow-hidden rounded-md b--white-20 ba w2-6 h2-6 bg-mid-gray relative">
                        <ImageWithFallback
                          height={38}
                          width={38}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="relative w-16 h-16 center bg-light_gray dark:bg-gray-4 rounded-full"
                          src={`https://images.cricket.com/players/${
                            captain
                              ? captain?.playerId
                              : viceCaptain
                              ? viceCaptain?.playerId
                              : ''
                          }_headshot.png`}
                          alt=""
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                        />
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={playerPlaceholder}
                          className="absolute mt-1 bottom-0 left-0  h-5 rounded-full w-5  "
                          src={`https://images.cricket.com/teams/${
                            captain
                              ? captain?.teamID
                              : viceCaptain
                              ? viceCaptain?.teamID
                              : ''
                          }_flag_safari.png`}
                          alt=""
                          onError={(e) => (e.target.src = flagPlaceHolder)}
                        />
                      </div>
                    </div>
                    <div className=" text-xs font-medium">
                      {lang === 'HIN' ? (
                        <div className="mr1 oswald">
                          {' '}
                          {captain
                            ? captain?.playerNameHindi
                            : viceCaptain
                            ? viceCaptain?.playerNameHindi
                            : ''}{' '}
                        </div>
                      ) : (
                        <div className="mr1 oswald">
                          {' '}
                          {captain
                            ? captain?.playerName
                            : viceCaptain
                            ? viceCaptain?.playerName
                            : ''}{' '}
                        </div>
                      )}
                      <div className="flex justify-center items-center pt1 ph1">
                        <div className="mt1 fw5 f10 pr1  text-[10px] font-normal pr1 text-gray-2 text-center">
                          {' '}
                          {getLangText(lang, words, 'Credits')}:{' '}
                          <span className="text-xs text-green">
                            {captain
                              ? captain?.credits
                              : viceCaptain
                              ? viceCaptain?.credits
                              : ''}
                          </span>
                        </div>
                        {/* {((cc && captain?.length>0 && captain?.projectedPoints) || (vv && viceCaptain?.length>0 && viceCaptain?.projectedPoints))&& <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{cc && captain?.length>0  ? captain?.projectedPoints: vv && viceCaptain?.length>0 ? viceCaptain?.projectedPoints:""}</span></div>} */}
                      </div>
                    </div>
                  </div>
                  <div className="w-49 w-45-l"></div>
                </div>
              </div>
              <div className="bb b--white-20 "></div>
              <div className="overflow-y-scroll overflow-hidden hidescroll max-h-96">
                <div className="w-full flex flex-wrap justify-center  ph2-l items-center">
                  {changeC_Vc &&
                    allPlayers
                      .filter(
                        (id, i) =>
                          id.captain !== '1' && id.vice_captain !== '1',
                      )
                      .map((item, index) => {
                        return (
                          <div
                            key={index}
                            className="text-center cursor-pointer mx-1 mb-2 relative white mt2 pv1 pv2-l dib border dark:border-none bg-white dark:bg-gray-4 w-32 h-40 rounded-lg flex items-center flex-col justify-center"
                            onClick={() => (
                              setChangeC_Vc(false), addReplacedPlayer(item)
                            )}
                          >
                            <div className="flex flex-col items-center justify-center">
                              <div className="relative   w-16 h-16 center">
                                <ImageWithFallback
                                  height={38}
                                  width={38}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="overflow-hidden rounded-full w-16 h-16 bg-light_gray dark:bg-gray object-cover object-top"
                                  src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                  alt=""
                                  // onError={(e) => (e.target.src = playerPlaceholder)}
                                />
                                <ImageWithFallback
                                  height={22}
                                  width={22}
                                  loading="lazy"
                                  fallbackSrc={playerPlaceholder}
                                  className="absolute bottom-0  right-0 h-5 rounded-full w-5 "
                                  src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                  alt=""
                                  // onError={(e) => (e.target.src = flagPlaceHolder)}
                                />
                                {/* <img
                                    className='absolute bottom-0  left-0 h-5 w-5 '
                                    src={`${
                                      item.player_role === 'BATSMAN'
                                        ? '/pngsV2/battericon.png'
                                        : item.player_role === 'BOWLER'
                                        ? '/pngsV2/bowlericon.png'
                                        : ''
                                    }`}
                                    alt=''
                                  /> */}
                              </div>

                              <div className=" justify-start items--center pt-2">
                                <div className="text-xs font-medium">
                                  {lang === 'HIN' && item.playerNameHindi
                                    ? item.playerNameHindi
                                    : item.playerName}{' '}
                                </div>
                                <div className="flex justify-center  items-center pt1 ph1 text-center">
                                  <div className=" text-[10px] font-normal pr1 text-gray-2 text-center">
                                    {' '}
                                    {getLangText(lang, words, 'Credits')}:{' '}
                                    <span className=" text-green font-medium">
                                      {item.credits}
                                    </span>
                                  </div>

                                  {/* {replaceSelectedPlayer.projectedPoints && <div className="mt1 f10 pl1"> PP: <span className="ph1 white fw5 br2 bg-white-20 oswald">{replaceSelectedPlayer.projectedPoints}</span></div>} */}
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      })}
                </div>
              </div>
              {/* <div className='flex justify-center pv1'>
                <img className='w12' src={'/svgs/gold_arrow.svg'} />
              </div> */}
            </div>
          </div>
        </div>
      )}
    </div>
  )
}
