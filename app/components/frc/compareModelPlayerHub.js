import React, { useState } from 'react'
import { getLangText } from '../../api/services'
import ImageWithFallback from '../commom/Image'
import Tab from '../shared/Tab'
import { words } from './../../constant/language'
const playerPlaceholder = '/pngs/fallbackprojection.png'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'

export default function CompareModelPlayerHub(props) {
  const categories = [
    { name: getLangText(props.lang, words, 'BATTERS'), value: 'BATSMAN' },
    { name: getLangText(props.lang, words, 'WK'), value: 'KEEPER' },
    { name: getLangText(props.lang, words, 'BOWLERS'), value: 'BOWLER' },
    { name: 'ARS', value: 'ALL_ROUNDER' },
  ]
  const [type, setType] = useState(
    categories.filter((c) => c.value === props.Player2Obj.playerSkill)[0],
  )
  console.log('getting tab is - ', props.Player2Obj.playerSkill)

  let filterRole = {
    Bowler: 'bowler',
    Batsman: 'batsman',
  }
  // console.log("Props model", props)
  return (
    <div
      className="flex justify-center items-center fixed absolute--fill z-20 w-full   overflow-y-scroll "
      style={{ backdropFilter: 'blur(5px)' }}
    >
      <div className=" rounded-lg bg-white p-4 text-black dark:text-white  z-80 dark:bg-gray  border relative w-11/12 md:w-5/12 lg:w-5/12 ">
        <div className="  flex  flex-row  items-center justify-between w-full mt-2">
          <div className="flex  justify-start   px-2 items-center  text-base uppercase  font-semibold">
            {props.lang === 'HIN' ? 'वर्तमान चयन' : 'current selection'}
          </div>
          <div
            className="flex cursor-pointer justify-end items-center  mr-2 text-base font-semibold"
            onClick={() => {
              props.closeNewModelV2()
            }}
          >
            X
          </div>
        </div>
        <div className="flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded my-3 ml-2 "></div>
        <div className=" ">
          <div className="flex  items-center m-2  justify-between border-b dark:border-black   py-2 ">
            <div className="flex items-center ">
              <div className="flex items-center justify-center w-14 h-14 rounded-full bg-gray-10 dark:bg-gray-8  overflow-hidden ">
                <img
                  className="w-full h-full object-cover object-top"
                  src={`https://images.cricket.com/players/${props.Player2Obj.playerID}_headshot_safari.png`}
                  onError={(evt) =>
                    (evt.target.src = '/pngs/fallbackprojection.png')
                  }
                  alt=""
                ></img>
              </div>

              <div className="text-xs font-semibold ml-2">
                {props.lang === 'HIN'
                  ? props.RecentPlayer.playerNameHindi
                  : props.Player2Obj.name}
              </div>
            </div>
            <div className="flex items-center">
              <div className="ttu f5 f4-l f4-m fw6 oswald white-40   ttu ph2">
                {props.Player2Obj.teamShortName}
              </div>
              <div className=" p-1 overflow-hidden ">
                <img
                  className="w-8 h-5 object-cover"
                  src={`https://images.cricket.com/teams/${props.Player2Obj.teamID}_flag_safari.png`}
                  alt=""
                  onError={(evt) => (evt.target.src = flagPlaceHolder)}
                ></img>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="mh2 mt4 mb2 bb b--white-20"></div> */}
        {/* <div className=" f5 fw6 white ph2  ttu">{props.lang==="HIN"?'खिलाड़ी बदलें':'CHANGE PLAYER'}</div> */}
        <div
          className={`flex mx-2 items-center justify-between   rounded-full  `}
        >
          <Tab
            data={categories}
            selectedTab={type}
            type="block"
            handleTabChange={(item) => setType(item)}
          />
        </div>
        <div
          className=" overflow-y-scroll overflow-hidden hidescroll "
          style={{ maxHeight: '333px', overflowY: 'auto' }}
        >
          {props.playersList
            .filter(
              (player) =>
                player.playerID !== props.RecentPlayer.playerID &&
                player.playerSkill === type?.value &&
                player.playerID !== props.Player2Obj.playerID,
            )
            .map((player, i) => (
              <div
                className="cursor-pointer mt-2"
                key={i}
                onClick={() => props.choosePlayer(player, props.SelectionType)}
              >
                <div className="flex  items-center m-2  justify-between border-b dark:border-black  py-2  ">
                  <div className="flex items-center ">
                    <div className="flex items-center justify-center w-14 h-14 rounded-full bg-gray-10 dark:bg-gray-8  overflow-hidden ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-14 h-14  object-cover object-top  rounded-full"
                        src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                        alt="player"
                        fallbackSrc={playerPlaceholder}
                      />
                    </div>
                    <div className="text-xs font-semibold ml-2">
                      {props.lang === 'HIN'
                        ? player.playerNameHindi
                        : player.name}
                    </div>
                  </div>
                  
                  <div className="flex items-center">
                    <div className=" text-xs font-semibold mr-2">
                      {player.teamShortName}
                    </div>
                    <div className="p-1 overflow-hidden ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={playerPlaceholder}
                        className="w-8 h-5 object-top object-cover"
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  )
}
