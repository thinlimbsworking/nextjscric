import React, { useState } from 'react'
import PostSuccessModal from '../commom/postSuccessModal'
import { InfoIcon } from '../playodds/infoIcon'

const Modal = ({
  title,
  showModal,
  submitButtonText,
  placeholderText,
  infoText,
  setshowModal,
  errorText,
  setName,
  name,
  submitMsg,
  submitCallback,
}) => {
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  return (
    <>
      {showModal && (
        <div
        className='flex text-black dark:text-white justify-center items-center fixed absolute--fill z-80  mx-auto  cursor-pointer  dark:border-none bg-basebg/70 overflow-y-scroll'
        style={{ backdropFilter: 'blur(10px)' }}>
          <div
            className={`${success
                ? 'relative flex flex-col w-80 md:w-3/12 lg:3/12 xl:3/12'
                : ' rounded-md shadow-lg relative flex flex-col w-80 md:w-3/12 lg:3/12 xl:3/12 bg-white dark:bg-gray-700  outline-none focus:outline-none'
              }`}
          >
            <div>
              {!success ? (
                <>
                  <div className="flex flex-col items-center   border-solid text-gray-300">
                    <div className='w-full py-1 px-1 flex justify-end'>
                      <button
                        className="  flex justify-center items-center  rounded-full border-white p-1 w-5 h-5  "
                        onClick={() => setshowModal((prev) => !prev)}>
                        X
                      </button>
                    </div>
                    <h3 className="text-xl pt-2">{title}</h3>

                  </div>
                  <div className="relative p-6 flex-auto ">
                    <span>
                      <input
                        type="text"
                        value={name}
                        maxlength="20"
                        id="name"
                        placeholder={placeholderText}
                        className="my-2 dark:bg-gray-700 text-black dark:text-white w-full border-b py-2 outline-none -500"
                        //  bg-gray  px-2 pb-1 border-b border-black ml-2 mt-4 w-full
                        onChange={(e) => setName(e.target.value)}
                      />
                      {error && <p className="text-red-500">{errorText}</p>}
                    </span>
                    <div className="flex items-center justify-center p-6 border-solid">
                      <button
                        className={`${name.length > 0
                            ? 'cursor-pointer rounded-md maxfont-bold text-basered dark:text-green-500 uppercase text-sm px-8 py-2 border-basered dark:border-green-500 border-2'
                            : 'cursor-pointer text-gray dark:text-white dark:bg-gray-800 rounded-md font-semibold uppercase text-sm px-8 py-2  border-2 opacity-70  '
                          }`}
                        type="button"
                        disabled={name.length === 0}
                        onClick={async () => {
                          if (name.length === 0) {
                            return
                          }
                          const res = await submitCallback()
                          if (res.data && !res.data.editMyFantasyTeamName) {
                            setError(true)
                          } else {
                            setSuccess(!success)
                            setName('')
                            setTimeout(() => setshowModal(!showModal), 2000)
                          }
                        }}
                      >
                        {submitButtonText}
                      </button>
                    </div>
                    <div className="flex flex-row justify-center">
                      <InfoIcon clr="#fff" height={22} width={10} />
                      <span className="text-sm mx-1 ">
                        {infoText}
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                <PostSuccessModal Msg={submitMsg} />
              )}
            </div>
            <div />
          </div>
        </div>
      )}
    </>
  )
}

export default Modal
