import { useQuery, useMutation } from '@apollo/react-hooks'
import React, { useState, useEffect } from 'react'
import Countdown from 'react-countdown-now'
import { format } from 'date-fns'
import FrcMatchCard from './frcMatchCard'
import CleverTap from 'clevertap-react'
import Link from 'next/link'
// import backIcon from '/svgs/back_dark_black.svg';
// import stadium from '/pngs/frc_stadium.png';
const flagPlaceHolder = '/svgs/images/flag_empty.png'
// import stats_hub_star from '/svgs/statHubStart.svg';
// import buildyourTeamArrow from '/svgs/buildYourTeamArrow.svg';

// import rightArrow from '/svgs/upArrowWhite.svg';
// import Swiper from 'react-id-swiper';
import { getFrcSeriesTabUrl } from '../../api/services'
import FeaturedArticlesFRC from './featureArtcileFrc'
import ScrollBar from './pegScroll'
import { GET_ALGO_11, GET_ARTICLES_BY_ID_AND_TYPE } from '../../api/queries'
import FrcLayout from './frcLayout'
const playerPlaceholder = '/pngs/fallbackprojection.png'
import { ARTICLE_VIEW, News_Live_Tab_URL } from '../../constant/Links'
import ArticleFRC from './featureArtcileFrc'
import { useRouter } from 'next/navigation'
import { words } from '../../constant/language'
// import BYTNew from '/svgs/BYTNew.png';
// import BYTNewMobile from '/svgs/BYTNewMobile.png';
// import FantasyStatsNew from '/svgs/newfanstat.svg';
// import language_icon from '/svgs/language_icon.svg';
import LanguageModel from './languageModel'
// import FRCVideoIcon from '/pngsV2/frcbanner.png';
import BuildYourTeam from './suggestTeam'

import PlayVideoModel from './playVideoModel'
import Loading from '../loading'
import DataNotFound from '../commom/datanotfound'
import ImageWithFallback from '../commom/Image'
import FeaturedVideoFRC from './featurevideo'
const BYTNew = '/pngsV2/bytnew.png'
const saveTeam = '/svgs/saveTeamYellowicon.svg'
const language_icon = '/pngsV2/lang.png'
const HindiFRCVideoBanner = '/HindiFRCVideoBanner.png'
export default function FantasyDetails(props) {
  const FRCVideoIcon = '/pngsV2/frcbanner.png'
  const FantasyStatsNew = '/svgs/newfanstat.svg'
  const [language, setlanguage] = useState(
    typeof window !== 'undefined' &&
      global.window.localStorage.getItem('FRClanguage')
      ? global.window.localStorage.getItem('FRClanguage')
      : 'ENG',
  )

  let router = useRouter()
  let lang = props.language
  //
  const [algoteam, setalgoTeam] = useState(null)
  let FantasymatchID = props.matchID
  const [newToken, setToken] = useState('')
  const [windows, setLocalStorage] = useState({})
  const [algo11Code, setalgo11Code] = useState('')
  const [toggle, setToggle] = useState(true)

  const [openAlgo11, setOpenalgo11] = useState(false)
  const [languagemodel, setlanguagemodel] = useState(false)
  const [load, setLoad] = useState(false)
  const [playvideomodel, setplayvideo] = useState(false)
  const data = {
    ...props?.data?.miniScoreCard?.data[0],
    awayTeamShortName: props?.data?.miniScoreCard?.data[0]?.awayTeamName,
    homeTeamShortName: props?.data?.miniScoreCard?.data[0]?.homeTeamName,
    MatchName: props?.data?.miniScoreCard?.data[0]?.matchName,
  }

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window })
      setToken(global.window.localStorage.getItem('tokenData'))
    }

  }, [global.window])
  useEffect(() => {
    refetch && refetch();
  })
  // const {
  //   loading: loadingArticle,
  //   error: errorArticle,
  //   data: articleData,
  // } = useQuery(GET_ARTICLES_BY_ID_AND_TYPE, {
  //   variables: { type: 'matches', Id: FantasymatchID },
  // })
  //

  let seriesID =
    props &&
    props.data &&
    props.data.miniScoreCard &&
    props.data.miniScoreCard.data &&
    props.data.miniScoreCard.data &&
    props.data.miniScoreCard.data.seriesID

  const fantasyTrack = (event, source) => {
    CleverTap.initialize(event, {
      Source: source,
      MatchID: props.matchID,
      SeriesID: seriesID || '',
      TeamAID: data?.homeTeamID,
      TeamBID: data?.awayTeamID,
      MatchFormat: data?.matchType,
      MatchStartTime:
        data?.matchDateTimeGMT &&
        format(Number(data?.matchDateTimeGMT), 'd,y, h:mm a'),
      MatchStatus: data.matchStatus,
      Platform:
        windows && windows.localStorage ? windows.localStorage.Platform : '',
    })
  }

  const { loading, error, data: algo11, refetch } = useQuery(GET_ALGO_11, {
    variables: { matchID: FantasymatchID, token: newToken },
    // fetchPolicy: 'network-only',

    onCompleted: (algo11) => {
      if (algo11 && algo11.getAlgo11) {
        setalgoTeam([
          ...(algo11.getAlgo11.batsman || []),
          ...(algo11.getAlgo11.all_rounder || []),
          ...(algo11.getAlgo11.bowler || []),
          ...(algo11.getAlgo11.keeper || []),
        ])
        setToggle(false)
      }
    },
  })

  const getUpdatedTime = (updateTime) => {
    // let uT=1618808000000;
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))

    let result =
      (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
      minutes +
      (minutes > 1 ? ' mins ' : ' min ') +
      (lang === 'HIN' ? 'पहले' : 'ago')
    return result
  }

  const getLangText = (lang, keys, key) => {
    //
    let [english, hindi] = keys[key]
    switch (lang) {
      case 'HIN':
        return hindi
      case 'ENG':
        return english
      default:
        return english
    }
  }
  const getNewsUrl = (article) => {
    if (article.type !== 'live-blog') {
      let articleId = article.articleID
      let tab = article.type
      return {
        as: eval(ARTICLE_VIEW.as),
        href: ARTICLE_VIEW.href,
      }
    }

    if (article.type === 'live-blog') {
      let matchId = article.matchIDs[0].matchID
      let articleID = article.articleID
      let matchName = article.matchIDs[0].matchSlug

      return {
        as: eval(News_Live_Tab_URL.as),
        href: News_Live_Tab_URL.href,
      }
    }
  }

  const params = {
    slidesPerView: 4,
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },

    navigation: {
      nextEl: '#nbutton',
      prevEl: '#pbutton',
    },
    spaceBetween: 5,
    breakpoints: { 650: { slidesPerView: 8, spaceBetween: 20 } },
  }

  const handleMonthHindi = () => {
    let monthformat = format(Number(data.matchDateTimeGMT) - 19800000, 'MMMM')

    let date = format(Number(data.matchDateTimeGMT) - 19800000, 'do')
    //

    if (lang === 'HIN') {
      return getLangText(lang, words, monthformat.toLowerCase()) + ' ' + date
    } else {
      return monthformat + ' ' + date
    }
  }

  if (loading || toggle) {
    return <Loading />
  }
  if (error) {
    return <DataNotFound />
  }
  return (
    // : openAlgo11 &&
    // <Algo11TeamDisplay setOpenalgo11={setOpenalgo11} data={data} setMyTeam={""} myTeam={algo11.getAlgo11} totalPoints={algo11.getAlgo11.teamtotalpoints} algo11codee={algo11Code} />
    <FrcLayout updateLanguage={props.setlanguage} lang={props.language}>
      {/* <div className=' flex bg-red items-center justify-center w-full hidden lg:block md:block'>
        <div className='flex items-center'>
          <img className='w1 dn-l' onClick={() => router.push(`/fantasy-research-center`)} alt='' src={backIcon} />
          <div className='black f6 fw7 pl3 ttu'>{getLangText(lang, words, 'fantasy_research_center')}</div>
        </div>
        <div
          className='flex cursor-pointer'
          onClick={() => {
            lang === 'HIN'
              ? (props.setlanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG'))
              : (props.setlanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'));
          }}>
          <img className='h-10 w-10 ' src={language_icon} />
        </div>
      
      </div> */}

      {data && data && !data.isLiveCriclyticsAvailable ? (
        <div className="flex mx-3 items-center justify-center md:hidden lg:hidden">
          <div className="flex flex-col mt-2 bg-gray border border-green  rounded  p-2 w-full    ">
            <div className="flex items-center justify-between ">
              <div className="flex  items-center">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  alt=""
                  src={`https://images.cricket.com/teams/${data.homeTeamID}_flag_safari.png`}
                  className="h-6 w-10 rounded shadow-4 mx-1"
                />
                <div className="oswald f4 fw5  white pl2">
                  {data.homeTeamShortName}
                </div>
              </div>
              <div className="flex  items-center justify-center">
                <div className="flex  items-center justify-center border border-green rounded-full h-8 w-16 bg-basebg">
                  {' '}
                  {data.matchType}
                </div>
              </div>

              <div className="flex items-center">
                <div className="oswald f4 fw5 white pr2">
                  {data.awayTeamShortName}
                </div>
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  alt=""
                  src={`https://images.cricket.com/teams/${data.awayTeamID}_flag_safari.png`}
                  className="h-6 w-10 rounded  shadow-4 mx-1"
                />
              </div>
            </div>
            <div className="flex items-center justify-center text-center text-xs font-bold mt-2">
              {' '}
              {data &&
                data &&
                data.matchDateTimeGMT &&
                format(Number(data.matchDateTimeGMT) - 19800000, 'h:mm a')}{' '}
              IST
            </div>
          </div>
        </div>
      ) : (
        <div className="bg-gray border border-green m-3 rounded-md p-3 flex flex-col md:hidden lg:hidden">{console.log('data in fs - ', data)}
          <div className="flex items-center justify-between">
            {data &&
              data &&
              data.matchScore.map((x, i) => (
                <>
                  {i !== 0 && (
                    <div className="flex text-center  items-center justify-center text-xs font-bold w-2/12 text-green border-green border p-1 rounded-xl bg-gray-8">
                      {data.matchType}
                    </div>
                  )}
                  <div
                    key={i}
                    className="flex items-center   justify-between py-2"
                  >
                    <div className="flex items-center gap-2">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}

                        alt={x.teamShortName}
                        src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        className="h-8 w-12 "
                      />

                      <span className="text-white font-semibold">
                        {' '}
                        {x.teamShortName}{' '}
                      </span>
                    </div>

                    
                  </div>
                </>
              ))}
          </div>
          
          <div className="flex items-center justify-between">
            {data &&
              data &&
              data.matchScore.map((x, i) => ( <div className=' flex items-center tracked-tight '>
              {x.teamScore &&
                x.teamScore.map((score, i) => (
                  <span key={i} className={`flex justify-center items-start text-sm font-semibold `}>

                    <span className={``}>
                      {score.runsScored}{' '}
                      {score.wickets && score.wickets !== '10' ? `/ ${score.wickets}` : null}

                    </span>

                    {((data.matchType === 'Test' &&
                      data.matchStatus !== 'completed' &&
                      score.inning == data.currentinningsNo) ||
                      data.matchType !== 'Test') && (
                        <div className='ml2'>{score.overs ? `(${score.overs})` : null}</div>
                      )}
                    {score.declared ? <span className='darkRed fw6 oswald f6 ml1'>d</span> : ''}
                    {score.folowOn ? <span className='darkRed fw6 f6 oswald ml1'> f/o</span> : ''}
                    {x.teamScore.length > 1 && i === 0 && <div className='mh2 fw5'>&</div>}
                  </span>
                ))}
            </div> ) )}
            </div>

          <div className=" py-1 text-center">
            {lang === 'HIN' ? (
              <span className="text-white text-xs font-semibold truncate  ">
                {data &&
                  data &&
                  (data.isLiveCriclyticsAvailable &&
                    data.statusMessageHindi
                    ? data.statusMessageHindi
                    : data.toss)}
              </span>
            ) : (
              <span className="text-white text-xs font-semibold  ">
                {data &&
                  data &&
                  (data.isLiveCriclyticsAvailable && data.statusMessage
                    ? data.statusMessage
                    : data.toss)}
              </span>
            )}
          </div>
        </div>
      )
      }

      <div className="w-full md:pt-3 lg:pt-3">
        <FrcMatchCard
          matchData={props.matchData.miniScoreCard?.data[0]}
          titleName="FANTASY CENTER"
        />
      </div>
      {/* <div className='flex items-center justify-center mx-2 '>
        <div className=' w-full' onClick={() => setplayvideo(true)}>
          <ImageWithFallback height={148} width={148} loading='lazy' className='w-full cursor-pointer object-contain object-top ' src={FRCVideoIcon} alt="fantasy-research-center" />
        </div>
      </div> */}
      {/* desktop */}

      {/* mobile */}
      <div className="flex flex-col md:flex-row lg:flex-row mb-6 mt-6 bg-white dark:bg-transparent md:px-6 md:py-6 md:rounded-md shadow px-1">
        <div className="w-full  flex flex-col gap-3 md:gap-8 lg:gap-8 justify-center md:w-1/2 lg:w-1/2 p-2 md:p-0 lg:p-0">
          <Link
            className="w-full"
            {...getFrcSeriesTabUrl(props, 'fantasy-stats/players')}
            passHref
            legacyBehavior
          >
            <div
              className="relative w-full cursor-pointer h-32 md:h-28 lg:h-28 md:mr-12 lg:mr-12 flex items-start  text-black dark:bg-gray-4 dark:text-white rounded-lg"
              onClick={() => {
                props.setUrlData({}), fantasyTrack('FantasyStatsPlayer', '')
              }}
            >
              <div className="h-full rounded-l-lg w-3 bg-red-5 dark:bg-green"></div>
              <div className="px-8 h-full border-r w-full border-t border-b rounded-r-lg ">
                <div className=" text-base mt-3  font-semibold capitalize">
                  {' '}
                  {getLangText(lang, words, 'FS')}{' '}
                </div>
                <div className="flex h-1 w-10 bg-blue-8 mt-2 rounded-lg" />
                <div className=" text-xs mt-3 pb-4">
                  {getLangText(lang, words, 'osst')}{' '}
                </div>
              </div>
              {/* <ImageWithFallback height={180} width={180} loading='lazy' className='h-90  md:h-40 lg:h-40 w-full' src={BYTNew} />  */}
            </div>
          </Link>

          {data && data && !data.isLiveCriclyticsAvailable ? (
            <Link
              className="w-full"
              {...getFrcSeriesTabUrl(props, 'create-team')}
              passHref
              legacyBehavior
            >
              <div
                className="relative w-full cursor-pointer h-32 md:h-28 lg:h-28 md:mr-12 lg:mr-12 flex items-start  text-black dark:bg-gray-4 dark:text-white rounded-lg"
                onClick={() => (
                  props.setUrlData({}), fantasyTrack('FantasyBYT', 'NewTeam')
                )}
              >
                <div className="h-full rounded-l-lg w-3 bg-red-5 dark:bg-green "></div>
                <div className="px-8 h-full border-r w-full border-t border-b rounded-r-lg  ">
                  <div className=" text-base mt-3  font-semibold capitalize">
                    {' '}
                    {getLangText(lang, words, 'BYT')}{' '}
                  </div>
                  <div className="flex h-1 w-10 bg-blue-8 mt-2 rounded-lg" />
                  <div className=" text-xs mt-3  pb-4">
                    {getLangText(lang, words, 'BYT_Description')}{' '}
                  </div>
                </div>
                {/* <ImageWithFallback height={180} width={180} loading='lazy' className='h-90 md:h-40 lg:h-40 w-full' src={BYTNew} /> */}
              </div>
            </Link>
          ) : (
            <div
              className="relative w-full cursor-pointer h-32 md:h-28 lg:h-28 md:mr-12 lg:mr-12 flex items-start  text-black dark:bg-gray-4 dark:text-white rounded-lg opacity-60 "
              style={
                {
                  // background: 'linear-gradient(230deg, rgba(144,144,88,.5), rgba(0,0,0,0) 100%)',
                  // borderLeftWidth: 'thick'
                }
              }
              onClick={() => (
                props.setUrlData({}), fantasyTrack('FantasyBYT', 'NewTeam')
              )}
            >
              {/* <div
              className='relative   flex items-start shadow-2xl text-black dark:bg-gray-4 dark:text-white  mt-2'
              onClick={() => (props.setUrlData({}), fantasyTrack('FantasyBYT', 'NewTeam'))}

            > */}
              <div className="h-full rounded-l-lg w-3 bg-red-5 dark:bg-green"></div>
              <div className="px-8 h-full border-r border-t border-b rounded-r-lg ">
                <div className="text-base mt-3  font-semibold capitalize">
                  {' '}
                  {getLangText(lang, words, 'BYT')}{' '}
                </div>
                <div className="flex h-1 w-10 bg-blue-8 mt-2 rounded-lg" />
                <div className=" text-xs mt-3 pb-4">
                  {getLangText(lang, words, 'BYT_Description')}{' '}
                </div>
              </div>
              {/* <ImageWithFallback height={108} width={108} loading='lazy' className='h-90 md:h-40 lg:h-40 w-full' src={BYTNew} /> */}
              {/* </div> */}
            </div>
          )}
        </div>
        <div className='w-full md:hidden lg:hidden mt-2'>
          {algo11 && algo11?.getAlgo11.saveTeams ? (
            data && data && !data.isLiveCriclyticsAvailable ? (
              <Link
                {...getFrcSeriesTabUrl(props, 'my-teams')}
                passHref
                legacyBehavior
              >
                <div
                  className="p-3 md:px-6 lg:px-6 border mx-2 md:mx-0 lg:mx-0  dark:border-none  rounded  bg-white dark:bg-gray  cursor-pointer flex items-center justify-between  "
                  onClick={() => {
                    fantasyTrack('FantasyMyTeams', '')
                    setLoad(true)
                  }}
                >
                  <div className="flex items-center">
                    <div className="text-black dark:text-white flex capitalize font-semibold text-base">
                      {getLangText(lang, words, 'Saved_Teams')}
                    </div>
                  </div>
                  <div className="flex items-center ">
                    {/* <div className='flex items-center  justify-center'>
                    {' '}
                    <div className='text-black dark:text-white text-base font-bold'> {">"} </div>
                  </div> */}
                    <ImageWithFallback
                      className="w-5 h-5"
                      height={10}
                      width={10}
                      loading="lazy"
                      src="/svgs/green-right.svg"
                    />
                  </div>
                </div>
              </Link>
            ) : (
              <Link
                {...getFrcSeriesTabUrl(props, 'live-teams')}
                passHref
                legacyBehavior
              >
                <div
                  className="p-3 md:px-6 lg:px-6 border mx-2 md:mx-0 lg:mx-0  dark:border-none  rounded  bg-white dark:bg-gray  cursor-pointer flex items-center justify-between  "
                  onClick={() => {
                    fantasyTrack('FantasyMyTeams', '')
                    setLoad(true)
                  }}
                >
                  <div className="flex items-center">
                    <div className="text-black dark:text-white flex capitalize font-semibold text-base">
                      {getLangText(lang, words, 'Saved_Teams')}
                    </div>
                  </div>
                  <div className="flex items-center ">
                    {/* <div className='flex items-center  justify-center'>
                    {' '}
                    <div className='text-black dark:text-white text-base font-bold'> {">"} </div>
                  </div> */}
                    <ImageWithFallback
                      className="w-5 h-5"
                      height={10}
                      width={10}
                      loading="lazy"
                      src="/svgs/green-right.svg"
                    />
                  </div>
                </div>
              </Link>
            )
          ) : (
            ''
          )}
        </div>
        <div className="w-full md:w-1/2 lg:w-1/2  flex md:justify-center lg:justify-center  md:pb-3 lg:pb-3 px-2 md:px-0 lg:px-0">
          <BuildYourTeam
            route={getFrcSeriesTabUrl(props, 'fantasyTeam')}
            comp={
              <Link
                {...getFrcSeriesTabUrl(props, 'fantasyTeam')}
                passHref
                legacyBehavior
              >
                <div
                  onClick={() => setLoad(true)}
                  className="bg-white dark:bg-gray-8 hover:opacity-70 transition ease-in duration-150 flex cursor-pointer  justify-center rounded border-2  mt-2 text-red-5 border-red-5 dark:text-green dark:border-green text-xs px-2 py-1 items-center font-bold "
                >
                  <div className="font-semibold">
                    {getLangText(lang, words, 'view all teams')}
                  </div>
                </div>
              </Link>
            }
            algoteam={algoteam}
            matchID={FantasymatchID}
            algo11={algo11}
            lang={lang}
            getLangText={getLangText}
            words={words}
          />
        </div>
      </div>
      <div className='w-full hidden md:block lg:block'>
        {algo11 && algo11?.getAlgo11.saveTeams ? (
          data && data && !data.isLiveCriclyticsAvailable ? (
            <Link
              {...getFrcSeriesTabUrl(props, 'my-teams')}
              passHref
              legacyBehavior
            >
              <div
                className="p-3 md:px-6 lg:px-6 border mx-2 md:mx-0 lg:mx-0  dark:border-none  rounded  bg-white dark:bg-gray  cursor-pointer flex items-center justify-between  "
                onClick={() => {
                  fantasyTrack('FantasyMyTeams', '')
                  setLoad(true)
                }}
              >
                <div className="flex items-center">
                  <div className="text-black dark:text-white flex capitalize font-semibold text-base">
                    {getLangText(lang, words, 'Saved_Teams')}
                  </div>
                </div>
                <div className="flex items-center ">
                  {/* <div className='flex items-center  justify-center'>
                    {' '}
                    <div className='text-black dark:text-white text-base font-bold'> {">"} </div>
                  </div> */}
                  <ImageWithFallback
                    className="w-5 h-5"
                    height={10}
                    width={10}
                    loading="lazy"
                    src="/svgs/red-right.svg"
                  />
                </div>
              </div>
            </Link>
          ) : (
            <Link
              {...getFrcSeriesTabUrl(props, 'live-teams')}
              passHref
              legacyBehavior
            >
              <div
                className="p-3 md:px-6 lg:px-6 border mx-2 md:mx-0 lg:mx-0  dark:border-none  rounded  bg-white dark:bg-gray  cursor-pointer flex items-center justify-between  "
                onClick={() => {
                  fantasyTrack('FantasyMyTeams', '')
                  setLoad(true)
                }}
              >
                <div className="flex items-center">
                  <div className="text-black dark:text-white flex capitalize font-semibold text-base">
                    {getLangText(lang, words, 'Saved_Teams')}
                  </div>
                </div>
                <div className="flex items-center ">
                  {/* <div className='flex items-center  justify-center'>
                    {' '}
                    <div className='text-black dark:text-white text-base font-bold'> {">"} </div>
                  </div> */}
                  <ImageWithFallback
                    className="w-5 h-5"
                    height={10}
                    width={10}
                    loading="lazy"
                    src="/svgs/red-right.svg"
                  />
                </div>
              </div>
            </Link>
          )
        ) : (
          ''
        )}
      </div>

      {/* <div className='mt-3 flex flex-col   md:hidden lg:hidden'>
        <FeaturedArticlesFRC title='FR' language={props.language} />
        <FeaturedVideoFRC title='FANTASY VIDEOS' language={props.language} />
        </div> */}

      {/* <div className=''>
        {articleData && articleData.getArticlesByIdAndType.length !== 0 ? (
          <div className='ma2 shadow-4 bg-white-10'>
            <div className='flex items-center justify-between ph2 bb b--white-20'>
              <span className='f7 fw6 white'>{getLangText(lang, words, 'FR')} </span>
              <div className='h2-3 flex items-center'></div>
            </div>

            <div className='flex-ns w-50-l w-50-m w-100 white'>
              <div className='flex-ns w-100 white mt3-ns'>
                <Link {...getNewsUrl(articleData.getArticlesByIdAndType[0])} passHref>
                  <a>
                    <ArticleFRC article={articleData.getArticlesByIdAndType[0]} isdark={true} />
                  </a>
                </Link>
              </div>
            </div>
          </div>
        ) : null}
      </div> */}

      {languagemodel && (
        <LanguageModel
          setlanguagemodel={setlanguagemodel}
          language={props.language ? props.language : 'english'}
          setlanguage={props.setlanguage}
          languagemodel={languagemodel}
        />
      )}

      {playvideomodel && (
        <PlayVideoModel
          setplayvideo={setplayvideo}
          playvideomodel={playvideomodel}
        />
      )}
    </FrcLayout>
  )
}
