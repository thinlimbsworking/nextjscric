import React,{useState} from 'react';
import { GET_ARTICLES_BY_CATEGORIES } from '../../api/queries';
import { useQuery } from '@apollo/react-hooks';
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import format from 'date-fns/format';
import { words } from "../../constant/language";
import { getNewsUrl ,getLiveArticleUrl, getLangText} from '../../api/services';
import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectCoverflow, Pagination } from 'swiper';
import { useRouter } from 'next/navigation';
import Heading from '../commom/heading';
import ImageWithFallback from '../commom/Image';

export default function FeaturedArticlesFRC({error, loading, data, browser, ...props }) {
  const [currentIndex, updateCurrentIndex] = useState(0);
  const router = useRouter();
  const navigate = router.push;
  // const { loading, error, data } = useQuery(GET_ARTICLES_BY_CATEGORIES, {
  //   variables: { type: 'fantasy', page: 0 }
  // });
  
  const lang=props.language;

  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'FantasyResearch',
      // MatchID: String(article.matchIDs),
      // SeriesID: String(article.seriesIDs),
      // PlayerID: String(article.playerIDs),
      ArticleType: article.type,
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };

  const newsHomeNav = () => {
    CleverTap.initialize('NewsHome', {
      Source: 'Homepage',
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };
  if (error) return <div></div>;
  if (loading) return <div></div>;
  else
    return (
      <div className='p-2'>
       <div className='flex justify-between items-center'>
          <div className='white capitalize  font-bold '>   
        <Heading heading={props.language?props.language==='HIN'?"फ़ैंटसी सुझाव":"Fantasy Previews":props.title}  />
          
             {/* {props.language? 
           <h2 className='text-base'>{props.language==='HIN'?"फ़ैंटसी सुझाव":"Fantasy Previews"}</h2>:<h2 className='text-base'>{props.title}</h2>} */}
          {/* <div className='flex h-1 w-22 bg-blue-8 mt-1' /> */}
          </div>
          <div
            className='flex cursor-pointer  justify-center rounded border-2  text-green border-green text-xs px-2 py-1 items-center font-bold '
            onClick={() => navigate(`/news/latest`)}>
            {getLangText(lang, words, "View All")}
          </div>
        </div>
        {/* <div className='flex items-center justify-between  cursor-pointer pa2 bb b--white-10'>
          
          {props.language?  <h2 className='f7 fw6 white'>{props.language==='HIN'?"फ़ैंटसी सुझाव":"FANTASY PREVIEWS"}</h2>:<h2 className='f7 fw6 white'>{props.title}</h2>}
        </div> */}
        
        <div className='flex flex-wrap cursor-pointer '>
          
                


<Swiper
          slidesPerView={'1.2'}
          spaceBetween={10}
          onSlideChange={(swiper) => updateCurrentIndex(swiper.realIndex)}
          modules={[Pagination]}
          className='mySwiper'>
          {data &&
            data.getArticlesByCategories.length > 0 &&
            data.getArticlesByCategories.slice(0,5)
            .map((item,i) => {
              return item.type !== 'wvideos' && (
                <SwiperSlide key={i} className=' w-80 bg-gray h-56  rounded-xl p-2  mr-4 my-2' >
                <Link key={i} {...getNewsUrl(item)} passHref legacyBehavior>
              
                  <div className=' cardUpdate w-full '>
                    <div className='flex relative items-center justify-center w-full '>
                      <ImageWithFallback height={108} width={108} loading='lazy' className=' h-28 w-full rounded-lg object-cover object-top ' src={item.featureThumbnail} alt='' />
                    </div>
                    <div className=' overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left tracking-wider truncate text-white'>
                      {item.title || ''}
                    </div>
                    <div className=' my-1 text-xs font-thin truncate text-left tracking-wide text-gray-2 '>
                      {item.description || ''}
                    </div>
                    <div className='w-full flex justify-between items-center  mt-1'>
                    <div className=' text-gray-2 text-xs text-left truncate '>{item.author}</div>
                    <div className='text-gray-2 text-xs text-left '>
                      {format(new Date(Number(item.createdAt) - 19800000), 'dd MMM yyyy')}
                    </div>
                    </div>
                    
                  </div>
               
                </Link>
                </SwiperSlide>
              );
            })}
        </Swiper>

        <div className='flex justify-start '>
          {data &&
            data.getArticlesByCategories.slice(0,5).map((item, key) => (
              <div key={key} className={`w-2 h-2 ${currentIndex == key ? 'bg-blue-8' : 'bg-blue-4 '} rounded-full mt-1 mx-[2px] `}></div>
            ))}
        </div>
        </div>
      </div>
    );
}


