import Login from "../../login/page";
import ImageWithFallback from "../commom/Image";
import { useQuery } from "@apollo/client";
import { FRCHome } from "../../constant/MetaDescriptions";
import MetaDescriptor from "../MetaDescriptor";
import LanguageModel from "./languageModel";
import SliderTab from "../shared/Slidertab";
import { GET_ARTICLES_BY_CATEGORIES, GET_VIDEOS_BY_CATEGORIES, VIDEO_BY_CATEGORY } from '../../api/queries';
import Loading from "../loading";
import { useEffect, useState } from "react";
import Heading from "../../../components/commom/heading";
import { getLangText } from "../../api/services";
import { words } from "../../constant/language";
import SwiperSlide from "../shared/swiperSlide";
import FeaturedArticlesFRC from "./featureArtcileFrc";
import FeaturedVideoFRC from "./featurevideo";
const language_icon = '/svgsV2/lang.svg';

export default function FrcLayout({ children, updateLanguage, lang }) {
    const [language, setlanguage] = useState('ENG');
    const [languagemodel, setlanguagemodel] = useState(false);
    const {
        loading: loading1,
        data: frcVideo,
        error: error1
    } = useQuery(GET_VIDEOS_BY_CATEGORIES, {
        variables: { type: 'fantasy', page: 0 },

    });

    const {
        loading: loading2,
        data: frcArticle,
        error: error2
    } = useQuery(GET_ARTICLES_BY_CATEGORIES, {
        variables: { type: 'fantasy' },

    });
    useEffect(() => {
        const defaultLanguageModel = localStorage.getItem('languageFirstVisit');
        if (defaultLanguageModel !== "true") {
          localStorage.setItem('languageFirstVisit', 'true')
          setlanguagemodel(true)
        }
        setlanguage(localStorage.getItem('FRClanguage') || 'ENG');
        updateLanguage && updateLanguage(localStorage.getItem('FRClanguage') || 'ENG')
      }, [])
    return (
        <>
            {loading1 || loading2 ? <Loading />
                : <div className='dark:bg-basebg ' >
                    <div className='md:hidden xl:hidden lg:hidden dark:bg-basebg z-1   flex items-center justify-between text-white p-3 '>
                        <div className='flex gap-2 items-center justify-center'>
                            <div className='p-2 bg-gray rounded-md ' onClick={() => window.history.back()}>
                                <ImageWithFallback height={18} width={18} loading='lazy'
                                    class="flex items-center justify-center h-4 w-4 rotate-180"
                                    src='/svgsV2/RightSchevronWhite.svg'
                                    alt='nav'
                                />
                            </div>
                            <span className='text-base font-bold capitalize'>{getLangText(lang, words, "fantasy_research_center")}</span></div>

                        <div className="cursor-pointer " onClick={() => {
                            language === 'HIN' ? (setlanguage('ENG'),updateLanguage && updateLanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG')) :
                                (setlanguage('HIN'),updateLanguage && updateLanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'))
                        }}>
                            <ImageWithFallback height={18} width={18} loading='lazy' className='h-10 w-10' src={language_icon} alt='nav' />
                        </div>
                    </div>

                    <div className=' hidden md:flex lg:flex   z-1  pt-4 items-center text-black dark:text-white   '>
                        <div className='w-6/12 flex items-center  '>

                            {/* <span className='text-xl  font-semibold capitalize'>FANTASY CENTER</span> */}
                            <Heading heading={getLangText(lang, words, "fantasy_research_center")}/>
                        </div>

                        <div className="cursor-pointer w-6/12  flex justify-end hover:opacity-70 transition ease-in duration-150" >
                            <ImageWithFallback onClick={() => {
                            language === 'HIN' ? (setlanguage('ENG'),updateLanguage && updateLanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG')) :
                                (setlanguage('HIN'),updateLanguage && updateLanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'))
                        }} height={18} width={18} loading='lazy' className='h-10 w-10' src={language_icon} alt='nav' />
                        </div>
                    </div>



                    {children}

                    <div className='hidden mt-6  md:block lg:block w-full bg-white md:p-6 rounded-md'>
                        <SliderTab type='articles' heading='FR' data={frcArticle?.getArticlesByCategories} viewUrl='news/latest' lang={lang}/>

                    </div>
                    {/* <div className='hidden mt-4  md:block lg:block w-full bg-white px-4 py-3 rounded-md '>
                        <SwiperSlide type='articles' heading='Fantasy Preview' data={frcArticle?.getArticlesByCategories} viewUrl='news/latest' lang={lang}/>

                    </div> */}

                    <div className='hidden mt-4  md:block lg:block w-full bg-white md:p-6 rounded-md'>
                        <SliderTab type='videos' heading='FANTASY VIDEOS' data={frcVideo?.getVideosByCategories} viewUrl='videos/latest' lang={lang}/>

                    </div>
                    <div className='flex px-2 flex-col w-full  md:hidden lg:hidden'>
                        <FeaturedArticlesFRC data={frcArticle} loading={loading2} error={error2} title='FR' language={language} />
                        <FeaturedVideoFRC title='FANTASY VIDEOS' data={frcVideo} loading={loading1} error={error1} language={language} />
                        </div>
                    

                    <MetaDescriptor section={FRCHome()} />

                </div> }
          {languagemodel && (
                <LanguageModel
                    setlanguagemodel={setlanguagemodel}
                    language={language}
                    setlanguage={setlanguage}
                    languagemodel={languagemodel}
                />
            )}
        </>
    )

}
