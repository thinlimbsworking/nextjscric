import ImageWithFallback from "../commom/Image"
import Countdown from "react-countdown-now"
import format from "date-fns/format";
import { words } from "../../constant/language";
import { data } from "autoprefixer";
const flagPlaceHolder = '/svgs/images/flag_empty.png';

export default function FrcMatchCard({ matchData, lang }) {
  // console.log('mymatcdatata -- ', matchData);
  const getLangText = (lang, keys, key) => {

    let [english, hindi] = keys[key]
    switch (lang) {
      case 'HIN':
        return hindi
      case 'ENG':
        return english
      default:
        return english
    }
  }


  // const handleMonthHindi = () => {
  //     let monthformat = format(Number(matchData.matchDateTimeGMT || matchData.startDate) - 19800000, 'MMMM')

  //     let date = format(Number(matchData.matchDateTimeGMT || matchData.startDate) - 19800000, 'do')
  //     let year = format(Number(matchData.matchDateTimeGMT || matchData.startDate) - 19800000, 'yyyy')


  //     if (lang === 'HIN') {
  //       return getLangText(lang, words, monthformat.toLowerCase()) + ' ' + date + ' ' + year
  //     } else {
  //       return monthformat + ' ' + date + ' ' + year
  //     }
  // }
console.log('matchdata in frcmatchcard --- ',matchData);
  return (
    <div className="w-full text-white relative hidden md:flex lg:flex bg-gray-8 h-48 rounded-md overflow-auto">
      <img src='/pngs/frcbg.png' alt='bg' className="w-full h-full  object-cover" />
      {/* <div>{`${matchData?.matchNumber}, ${matchData?.seriesName}`}</div> */}
      {
        matchData.matchStatus === 'live' || matchData.matchStatus === 'completed' ?
          <div className="absolute w-full h-full flex flex-col gap-2 justify-center items-center text-base font-semibold z-8">

            <div className="flex  items-center gap-8 justify-center">
              {
                matchData &&
                matchData.matchScore.map((x, i) => (
                  <>
                    {i !== 0 && (
                      <div className="flex text-center text-white items-center justify-center text-xs font-bold w-16  bg-basered  px-3 py-1 rounded-xl ">
                        {matchData.matchType}
                      </div>
                    )}
                    <div
                      key={i}
                      className={`flex w-full flex-col items-start gap-1   py-2 `}
                    >
                      <div className={`flex  w-24 gap-2 ${i === 0 ? 'flex-row-reverse' : ''}`}>
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={flagPlaceHolder}
                          alt={x.teamShortName}
                          src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          className="h-6  rounded w-10 object-cover  object-top"
                        />

                        <span className="text-white font-semibold">
                          {' '}
                          {x.teamShortName}{' '}
                        </span>
                      </div>

                      <div className={`w-24   flex items-center text-sm font-semibold  ${i === 0 ? 'justify-end' : ''}`}>
                        {x.teamScore &&
                          x.teamScore.map((score, i) => (
                            <span
                              key={i}
                              className={`flex flex-row justify-start gap-1 items-start  `}
                            >
                              <div className={`flex flex-row gap-1 `}>
                                <div className="flex gap-1">
                                  <span>{score.runsScored}</span>
                                  {x.teamScore.length > 1 && i === 0 && (
                                    <div className="mh2 fw5">&</div>
                                  )}
                                </div>
                                <span>{score.wickets && score.wickets !== '10'
                                  ? `/ ${score.wickets}`
                                  : null}</span>
                              </div>

                              {/* {((matchData.matchType === 'Test' &&
                                matchData.matchStatus !== 'completed' &&
                                score.inning == matchData.currentinningsNo) ||
                                matchData.matchType !== 'Test') && (
                                  <div className="ml2">
                                    {score.overs ? `(${score.overs})` : null}
                                  </div>
                                )} */}
                              {score.declared ? (
                                <span className="darkRed fw6 oswald f6 ml1">d</span>
                              ) : (
                                ''
                              )}
                              {score.folowOn ? (
                                <span className="darkRed fw6 f6 oswald ml1"> f/o</span>
                              ) : (
                                ''
                              )}

                            </span>
                          ))}
                      </div>


                    </div>
                  </>
                ))}
            </div>
            <div className=" py-1 text-center">
              {lang === 'HIN' ? (
                <span className="text-white text-xs font-semibold truncate  ">
                  {
                    matchData &&
                    (matchData.isLiveCriclyticsAvailable &&
                      matchData.statusMessageHindi
                      ? matchData.statusMessageHindi
                      : matchData.toss)}
                </span>
              ) : (
                <span className="text-white text-xs font-semibold truncate ">
                  {
                    matchData &&
                    (matchData.isLiveCriclyticsAvailable && matchData.statusMessage
                      ? matchData.statusMessage
                      : matchData.toss)}
                </span>
              )}
            </div>

          </div>

          : <div className="absolute w-full h-full flex flex-col gap-2 justify-center items-center text-base font-semibold z-8">

            <div className="flex gap-2"> <span>{new Date(matchData.matchDateTimeGMT - 19800000).toDateString()}</span> | <span>{new Date(matchData.matchDateTimeGMT - 19800000).toTimeString().substring(0, 5) + ' IST'}</span></div>
            <div className="mt-2 flex justify-between  gap-24">
              <div className="w-60 flex gap-3 ">

                <div className="flex gap-1 items-center justify-center ">

                  <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc={flagPlaceHolder}
                    alt=''
                    src={`https://images.cricket.com/teams/${matchData.homeTeamID}_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    className='h-6 mx-1 rounded w-10 object-cover  object-top'
                  />

                  <p className="">{matchData.homeTeamName}</p>
                </div>

                <div className=" text-yellow text-xs font-semibold rounded-full  flex items-center justify-center">
                  {'Vs'}
                </div>
                <div className="flex items-center gap-1 justify-center text-center ">
                  <p className="oswald text-white ">{matchData.awayTeamName}</p>

                  <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc={flagPlaceHolder}
                    alt=''
                    src={`https://images.cricket.com/teams/${matchData.awayTeamID}_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    className='h-6 mx-1 rounded w-10 object-cover object-top'
                  />

                </div>
              </div>
              <div className="w-1/2 h-full">
                <div className="rounded-lg p-1 flex justify-center bg-basered">
                  {new Date(Number(matchData.matchDateTimeGMT || matchData.startDate) - 19800000) - new Date().getTime() < 84600000 ? (
                    <Countdown
                      date={new Date(Number(matchData.matchDateTimeGMT || matchData.startDate) - 19800000)}
                      renderer={(props) => (
                        <div className=' rounded text-xs  text-center font-bold text-white truncate flex gap-1'>
                          <div> {`Starts in `}</div>
                          <div>{`${props.days !== 0 ? props.days * 24 + Number(props.hours) : props.hours} h ${props.minutes
                            } m ${props.seconds
                            } s `}</div>
                        </div>
                      )}
                    />) : <div className=' truncate'>
                    <div>
                    </div>
                    <div className='flex justify-center items-center font-bold text-xs'>{`${format(
                      Number(matchData.matchDateTimeGMT || matchData.startDate) - 19800000,
                      'h:mm a'
                    )}`}
                    </div>
                  </div>}
                </div>
              </div>
            </div>
          </div>}
    </div>
  )
}


