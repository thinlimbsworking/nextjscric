import Login from "../../login/page";
import ImageWithFallback from "../commom/Image";
import { useQuery } from "@apollo/client";
import { FRCHome } from "../../constant/MetaDescriptions";
import MetaDescriptor from "../MetaDescriptor";
import LanguageModel from "./languageModel";
import SliderTab from "../shared/Slidertab";
import { GET_ARTICLES_BY_CATEGORIES, VIDEO_BY_CATEGORY } from '../../api/queries';
import Loading from "../loading";
import Countdown from "react-countdown-now";
import { useEffect, useState } from "react";
import FrcMatchCard from "./frcMatchCard";
import Heading from "../../../components/commom/heading";
import { useSearchParams } from "next/navigation";
const language_icon = '/svgsV2/lang.svg';


export default function FrcMatchLayout({ children, matchData, titleName, updateLanguage, currenFlag, setCurrentFlag, ...props }) {
    const [language, setlanguage] = useState('ENG');
    const [languagemodel, setlanguagemodel] = useState(false);
    const query = useSearchParams();
    console.log('query in frclayout --- ',query.get('ffCode'));
    const {
        loading: loading1,
        data: frcVideo
    } = useQuery(VIDEO_BY_CATEGORY, {
        variables: { type: 'fantasy', page: 0 },

    });

    const {
        loading: loading2,
        data: frcArticle
    } = useQuery(GET_ARTICLES_BY_CATEGORIES, {
        variables: { type: 'fantasy' },

    });
    useEffect(() => {
        const defaultLanguageModel = localStorage.getItem('languageFirstVisit');
        if (defaultLanguageModel !== "true") {
          localStorage.setItem('languageFirstVisit', 'true')
          setlanguagemodel(true)
        }
        setlanguage(localStorage.getItem('FRClanguage') || 'ENG');
        updateLanguage && updateLanguage(localStorage.getItem('FRClanguage') || 'ENG')
      }, [])
    return (
        <>
            {loading1 || loading2 ? <Loading />
                : <div className=' dark:bg-basebg ' >
                    <div className='md:hidden xl:hidden lg:hidden dark:bg-basebg z-1  flex items-center justify-between text-white p-3 '>
                        <div className='flex gap-2 items-center justify-center'>
                            <div className='p-2 bg-gray rounded-md ' onClick={() => {
                                console.log('currenflagg - ',currenFlag);
                                currenFlag == 'choose players'
                                ? setCurrentFlag('choose League')
                                : currenFlag == 'choose logic'
                                ? setCurrentFlag('choose players')
                                : currenFlag == 'choose Team' && !query?.get('ffCode')
                                ? setCurrentFlag('choose logic')
                                : props.componentType === 'chooseCaptains'
                                ? props.setcomponentType('BYTPlayerStathubState')
                                : props.componentType === 'BYTPlayerStathubState'
                                ? props.setcomponentType('PlayerState')
                                : window.history.back()
                            }
                            }>
                                <ImageWithFallback height={18} width={18} loading='lazy'
                                    class="flex items-center justify-center h-4 w-4 rotate-180"
                                    src='/svgsV2/RightSchevronWhite.svg'
                                    alt='nav'
                                />
                            </div>
                            <span className='text-base font-bold capitalize'>{titleName}</span></div>

                        <div className="cursor-pointer" onClick={() => {
                            language === 'HIN' ? (setlanguage('ENG'),updateLanguage && updateLanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG')) :
                                (setlanguage('HIN'),updateLanguage && updateLanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'))
                        }}>
                            <ImageWithFallback height={18} width={18} loading='lazy' className='h-10 w-10' src={language_icon} alt='nav' />
                        </div>
                    </div>

                    <div className=' hidden md:flex lg:flex   z-1  py-4 items-center justify-center text-black dark:text-white  '>
                        <div className='w-6/12 flex items-center pr-8'>

                            {/* <span className='text-xl  font-semibold capitalize'>{titleName}</span> */}
                            <Heading heading={titleName}/>
                        </div>

                        <div className="cursor-pointer w-6/12  flex justify-end hover:opacity-70 transition ease-in duration-150">
                            <ImageWithFallback  onClick={() => {
                            language === 'HIN' ? (setlanguage('ENG'),updateLanguage && updateLanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG')) :
                                (setlanguage('HIN'),updateLanguage && updateLanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'))
                        }} height={18} width={18} loading='lazy' className='h-10 w-10' src={language_icon} alt='nav' />
                        </div>
                       
                    </div>
                    <FrcMatchCard matchData={matchData} lang={language}/>
                    
                  


                    {children}

                   

                    <MetaDescriptor section={FRCHome()} /> 

                </div> }
          {languagemodel && (
                <LanguageModel
                    setlanguagemodel={setlanguagemodel}
                    language={language}
                    setlanguage={setlanguage}
                    languagemodel={languagemodel}
                />
            )}
        </>
    )

}
