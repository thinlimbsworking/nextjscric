import React, { useEffect,useRef } from 'react'
import CleverTap from 'clevertap-react';

import ImageWithFallback from '../commom/Image';

const language_icon = '/svgs/language_icon.svg'
const close = '/pngs/closeIcon.png'

export default function LanguageModel(props) {

    const ref = useRef()
    //   useEffect(() => {
    //     props.callScroll(true);
    //     return () => {
    //       props.callScroll(false);
    //     };
    //   }, []);

    useEffect(() => {
        const checkIfClickedOutside = e => {
          // If the menu is open and the clicked target is not within the menu,
          // then close the menu
          if (props.languagemodel &&ref &&  ref.current && !ref.current.contains(e.target)) {
            props.setlanguagemodel(false)
          }
        }
        document.addEventListener("mousedown", checkIfClickedOutside)
        return () => {
          // Cleanup the event listener
          document.removeEventListener("mousedown", checkIfClickedOutside)
        }
      }, [props.languagemodel])


    return (
        <div
        // flex justify-center items-center fixed absolute--fill z-9999 bg-black-70 overflow-y-scroll
            className='flex justify-center text-center items-center fixed absolute--fill z-9999 bg-basebg/70 overflow-y-scroll'  style={{ backdropFilter: 'blur(4px)' }}
           >

            <div className=' bg-gray br3 shadow-4  p-3 rounded-md border b--black relative  min-vh-20 pt2 mh2  w-100 w-50-l' ref={ref}>

              {/* <img className='w-4 h-4 right-2 absolute top-[-20px]' src={close} onClick={()=>props.setlanguagemodel(false)}/> */}

                <div className="f5 fw6 text-white p-2">CHOOSE LANGUAGE</div>
                <div className="flex items-center justify-center">
                    <div className="w-6/12 flex items-center justify-center">
                        <div className={`w-8/12 rounded-md mx-2 text-center text-white py-1 px-2 cursor-pointer ${props.language==='ENG'?'bg-gray border border-green black':'bg-gray border border-green'}`} onClick={()=>{props.setlanguage('ENG');localStorage.setItem('FRClanguage', 'ENG'); props.setlanguagemodel(false);
                        CleverTap.initialize('LanguageChange', {
                            Language: "English",
                            Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
                          });
                    }}>
                            <div className=" text-white text-xs" >ENGLISH</div>
                            <div className={` text-white text-xs ${props.language==='ENG'?'black':'white'}`}>English</div>
                        </div>
                    </div>
                    <div className="w-6/12 flex items-center justify-center">
                        <div className={` w-8/12 rounded-md mx-2 text-center text-white py-1 px-2 cursor-pointer  ${props.language==='HIN'?'bg-gray border border-green ':'bg-gray border border-green '}`}  onClick={()=>{props.setlanguage('HIN');localStorage.setItem('FRClanguage', 'HIN'); props.setlanguagemodel(false);
                      CleverTap.initialize('LanguageChange', {
                        Language: "Hindi",
                        Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
                      });
                    }}>
                            <div className=" text-white text-xs">हिंदी</div>
                            <div className={` text-white text-xs`}>Hindi</div>
                        </div>
                    </div>
                </div>
                <div className="pt-2 px-2">
                <div className="flex items-center justify-center flex-col  white-70 f8 text-white text-xs f6-l f6-m  fw3" >
                 
                <span className="" style={{ verticalAlign: 'middle',display: 'inline-block'}}>{props.language==='HIN'?'भाषा बदलने के लिए, पेज के ऊपरी दाएं कोने पर स्थित':'To change language, tap on the'}</span>
                <span className="ph1"  style={{ verticalAlign: 'middle'}}> 
              <ImageWithFallback height={18} width={18} loading='lazy' className='h-10 w-10' src="/pngsV2/lang.png" alt="" srcset="" />
                    </span>
                    <span  >{props.language==='HIN'?'आइकन पर टैप करें !':'icon on the top right corner of the page.'}</span>
                    <br></br>
                    <span >{props.language==='HIN'?'भाषा परिवर्तन केवल फ़ैंटसी सेंटर के लिए है':'Language conversion is only for Fantasy Centre.'}</span>

                    {/* <span className="pl1 dn db-l db-m ">corner of the page</span> */}

                   
                </div>

               {/* <div className=" white-70 f8 flex items-center  dn-l dn-m  fw3"> corner of the page</div> */}
               </div>

            </div>


        </div>

    )
}
