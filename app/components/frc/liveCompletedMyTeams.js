import React, { useEffect, useState } from 'react'

import { useQuery } from '@apollo/react-hooks'
import { GET_USER_FRC_TEAM_LIVE_POINTS } from '../../api/queries'
import ImageWithFallback from '../commom/Image'
const backIcon = '/svgs/back_dark_black.svg'
import Loading from '../loading'
import FrcLayout from './frcLayout'
import FrcMatchLayout from './frcMatchLayout'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import Heading from '../commom/heading'
import SlideComp from '../commom/slideComp'
import Algo11TeamDisplay from './algo11TeamDisplay'
// import { navigate } from '@reach/router'
const playerPlaceholder = '/pngs/fallbackprojection.png'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'

export default function LiveCompletedMyTeams(props) {
  let FantasymatchID = props.matchID;
  let lang = props.language
  const allUserTeamData =
    (
      props.matchBasicData &&
      [
        ...props.matchBasicData.getFRCHomePage.completedmatches,
        ...props.matchBasicData.getFRCHomePage.livematches,
        ...props.matchBasicData.getFRCHomePage.upcomingmatches,
      ].filter((x) => x.matchID === FantasymatchID)
    ).length > 0
      ? [
        ...props.matchBasicData.getFRCHomePage.completedmatches,
        ...props.matchBasicData.getFRCHomePage.livematches,
        ...props.matchBasicData.getFRCHomePage.upcomingmatches,
      ].filter((x) => x.matchID === FantasymatchID)
      : [
        {
          ...props.data.miniScoreCard.data[0],
          awayTeamShortName: props.data.miniScoreCard.data[0].awayTeamName,
          homeTeamShortName: props.data.miniScoreCard.data[0].homeTeamName,
          MatchName: props.data.miniScoreCard.data[0].matchName,
        },
      ]
  const [click, setclick] = useState('MyTeamList')
  const [close, setclose] = useState(false)
  const [index, setIndex] = useState()
  const [toggle, setToggle] = useState(false)
  const [windows, setLocalStorage] = useState({})
  const [newToken, setToken] = useState('')
  const [myTeam, setMyTeam] = useState({})
  // console.log("clickclick",click)
  // useEffect(() => {
  //   if (global.window) {
  //     setLocalStorage({ ...global.window });
  //     setToken(global.window.localStorage.getItem('tokenData'))
  //   }
  // }, [global.window]);

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window })
      setToken(global.window.localStorage.getItem('tokenData'))
    }
  }, [global.window])

  const { loading, error, data } = useQuery(GET_USER_FRC_TEAM_LIVE_POINTS, {
    variables: { matchID: FantasymatchID, token: newToken },
  })

  if (loading) {
    return <Loading />
  }
  if (close) {
    return (
      <Algo11TeamDisplay
      setclose={setclose}
      myTeam={myTeam}
      lang={lang}
      status = 'completed'
      />
    )
  }

  return (
    <FrcMatchLayout
      titleName={getLangText(lang, words, 'BYT')}
      matchData={props.matchData.miniScoreCard.data[0]}
      updateLanguage={props.setlanguage}>

      <div className="hidden text-black shadow-4 ba b--frc-yellow ma2 br2 pa3 white">
        {allUserTeamData &&
          allUserTeamData[0] &&
          allUserTeamData[0].matchScore.map((x, i) => (
            <div key={i} className="flex items-center   justify-evenly pv2">
              <span className="flex w-30">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc={flagPlaceHolder}
                  alt={x.teamShortName}
                  src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                  className="h1 w15 shadow-4"
                />
                <span className="mh2 f6 fw6"> {x.teamShortName} </span>
              </span>
              <div className="w-70 flex items-center tracked-tight ">
                {x.teamScore &&
                  x.teamScore.map((score, i) => (
                    <span
                      key={i}
                      className={`flex justify-center items-start f6 `}
                    >
                      <span className={``}>
                        {score.runsScored}{' '}
                        {score.wickets && score.wickets !== '10'
                          ? `/ ${score.wickets}`
                          : null}
                      </span>

                      {((allUserTeamData[0].matchType === 'Test' &&
                        allUserTeamData[0].matchStatus !== 'completed' &&
                        score.inning == allUserTeamData[0].currentinningsNo) ||
                        allUserTeamData[0].matchType !== 'Test') && (
                          <div className="ml2">
                            {score.overs ? `(${score.overs})` : null}
                          </div>
                        )}
                      {score.declared ? (
                        <span className="darkRed fw6 oswald f6 ml1">d</span>
                      ) : (
                        ''
                      )}
                      {score.folowOn ? (
                        <span className="darkRed fw6 f6 oswald ml1"> f/o</span>
                      ) : (
                        ''
                      )}
                      {x.teamScore.length > 1 && i === 0 && (
                        <div className="mh2 fw5">&</div>
                      )}
                    </span>
                  ))}
              </div>
            </div>
          ))}
        <div className="pv1 white bg-white-20 br4 ph2 ">
          <span className="f8 fw5 tl truncate">
            {allUserTeamData &&
              allUserTeamData[0] &&
              (allUserTeamData[0].isLiveCriclyticsAvailable &&
                allUserTeamData[0].statusMessage
                ? allUserTeamData[0].statusMessage
                : allUserTeamData[0].toss)}
          </span>
        </div>
      </div>
      {click === 'MyTeamList' && (
        <div className=' mt-2  text-black dark:text-white'>
          {/* <div className="f5 fw5 white-50 ph2 mb2 mt3 ttu">SAVED TEAMS</div> */}

          {data &&
            data.getUserFrcTeamLivePoints?.map((team, i) => {
              // onClick={() => navigate(`/fantasy-research-center/${props.matchID}/${props.seriesSlug}/create-team`)}
              return (
                <div className="w-full p-2 md:p-0 lg:p-0 flex items-center flex-col ">
                  <div className="flex   items-center justify-between w-full ">
                    <div className="flex w-full flex-col text-base font-bold">
                      
                        <Heading heading={team.teamName} />
                          <div className=" w-full  bg-gray-4 text-white  my-3 rounded-lg">
                            <div className="relative">
                              <div className="p-2 flex  justify-between items-center">
                                <div className="text-xs flex gap-2 font-medium">

                                  <span className='uppercase'>{getLangText(lang, words, 'Fantasy points')} :</span>
                                  <div className="text-green">
                                    {team.teamtotalpoints}
                                  </div>
                                </div>
                                <div
                                  className="flex bg-basered text-white cursor-pointer hover:opacity-70 transition ease-in duration-150 bg-none dark:bg-transparent dark:border border-green dark:border-green dark:bg-gray-4  text-xs items-center justify-center p-2 rounded dark:text-green"
                                  onClick={() => {
                                    
                                    setMyTeam(team)
                                    setclose(true);
                                  }}
                          >
                            {getLangText(lang, words, 'view_team')}
                          </div>


                              </div>
                              <div className="flex w-full  bg-gray-10 rounded-b-lg text-black dark:bg-gray dark:text-white shadow-md ">
                                <SlideComp algoteam={team.team} lang={lang} />
                              </div>
                            </div>

                          </div>
                      
                      {/* {data && data.getUserFrcTeamLivePoints.length !== i + 1 && (
                        <div className="bb mh2 b--white-20 pb4"></div>
                      )} */}
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      )}

      {/* {click == 'openallUserTeamData' && (
        <LiveCompletedMyallUserTeamData
          setMyTeam={setMyTeam}
          myTeam={myTeam}
          islive={allUserTeamData[0].isLiveCriclyticsAvailable}
        />
      )} */}
      {/* {click == "openStatshuballUserTeamData" &&
        <LiveCompletedStatshuballUserTeamData  setMyTeam={setMyTeam} myTeam={myTeam} islive={allUserTeamData[0].isLiveCriclyticsAvailable} />
      } */}
    </FrcMatchLayout>
  )

}
            // { false && team.team.map((x, y) => (
            //   <div key={y} className="">
            //     <div className=" relative flex justify-center">
            //       <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray relative">
            //         <ImageWithFallback
            //           height={18}
            //           width={18}
            //           loading="lazy"
            //           fallbackSrc={playerPlaceholder}
            //           className=""
            //           src={`https://images.cricket.com/players/${x.playerId}_headshot_safari.png`}
            //         />
            //       </div>
            //       <ImageWithFallback
            //         height={18}
            //         width={18}
            //         loading="lazy"
            //         fallbackSrc={flagPlaceHolder}
            //         className="absolute w1 ml1 h1 left-0 bottom-0 mb1 br-100 ba b--black  bg-gray ml3-l"
            //         src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
            //       />
            //       {(x.captain == '1' ||
            //         x.vice_captain == '1') && (
            //           <div className="absolute w1 h1 mr1 right-0 bottom-0 mb1 tc black f10 flex items-center mr3-l fw6 justify-center br-100 ba b--white  bg-gold">
            //             {x.captain === '1' ? 'C' : 'VC'}
            //           </div>
            //         )}
            //     </div>
            //     <div className="oswald f8 pv1 tc ph1">
            //       {x.playerName}
            //     </div>
            //     <div className="f10 gray tc pt1 ttc">
            //       {x.player_role}
            //     </div>
            //   </div>
            // ))}