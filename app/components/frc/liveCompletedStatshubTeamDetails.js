import React, { useState, useEffect } from 'react'
import ImageWithFallback from '../commom/Image';

export default function LiveCompletedStatshubTeamDetails({ myTeam, setMyTeam,islive, ...props }) {

  const [buildTeam, setBuildTeam] = useState(myTeam.ffCode ?
    {
      batsman: myTeam.team.filter(pl => pl.player_role == 'BATSMAN'),
      bowler: myTeam.team.filter(pl => pl.player_role == 'BOWLER'),
      keeper: myTeam.team.filter(pl => pl.player_role == 'KEEPER'),
      all_rounder: myTeam.team.filter(pl => pl.player_role == 'ALL_ROUNDER')
    } : {})
  const playerPlaceholder = '/pngs/fallbackprojection.png';
  const flagPlaceHolder = '/svgs/images/flag_empty.svg';


  return (

    <div className="ma2 mt3  pb4">
      <div className="f5 ph2 silver">{myTeam.teamName}</div>
      <div className="br3 mt2 ba b--white-20 pa2">
        <div className="flex justify-between items-start w-90-l center ">
          <div className="w-33"></div>
          <div className="bg-gold br-100 h33 w33 black ba b--yellow shadow-4 flex justify-center items-center">
            <div className="tc">
              <div className="f8 fw6">TOTAL<br /> POINTS</div>
              <div className="f2-5 oswald fw6">{myTeam.teamtotalpoints}</div>
            </div>
          </div>
          {islive && <div className="flex justify-end items-center w-33">
            <div className="w04 h04 br-100 bg-green ba b--yellow"></div>
            <div className="oswald f6 fw6 green pl1">LIVE</div>
          </div>}

        </div>
        {buildTeam.keeper.length > 0 && <div className="center tc white">
          <div className="bb b--white-20 mb2 mt1"></div>
          <div className="tc ttu oswald f5 fw4 pb2">Wicket keepers</div>
          <div className="w-100  flex flex-wrap justify-around   items-center">
            {buildTeam.keeper.map((item, index) => {
              return (
                <div key={index} className='ba b--white-20 br3 w-30 w-15-l mb3-l tc mb2 relative h4  bg-white-10  flex items-center justify-center' >
                  {item.isMyPick && <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />}

                  <div className="">
                    <div className="relative flex justify-center items--center w2-6 h2-6 center">
                      <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                        <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt=""  />
                      </div>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                    </div>
                    <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                      <div className="oswald"> {item.playerName}  </div>
                      <div className="mt1 fw5 f8-l f10 pt1"> POINTS: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.totalPoints}</span></div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>}
        {buildTeam.batsman.length > 0 && <div className="center tc white">
          <div className="bb b--white-20  mv3"></div>
          <div className="tc ttu oswald f5 fw4 pb2">Batters</div>
          <div className="w-100  flex flex-wrap justify-around   items-center">
            {buildTeam.batsman.map((item, index) => {
              return (
                <div key={index} className='ba b--white-20 br3 w-30 w-15-l tc mb2 relative h4 flex bg-white-10 items-center justify-center mb3-l'>
                  {item.isMyPick && <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />}

                  <div className="">
                    <div className="relative flex justify-center items--center w2-6 h2-6 center">
                      <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                        <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt=""  />
                      </div>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                    </div>
                    <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                      <div className="oswald "> {item.playerName}</div>
                      <div className="mt1 fw5 f8-l f10 pt1"> POINTS: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.totalPoints}</span></div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>}

        {buildTeam.all_rounder.length > 0 && <div className="center tc white">
          <div className="bb b--white-20  mb2 mt1"></div>
          <div className="tc ttu oswald f5 fw4 pb2">All rounders</div>
          <div className="w-100  flex flex-wrap justify-around   items-center">
            {buildTeam.all_rounder.map((item, index) => {
              return (
                <div key={index} className='ba b--white-20 br3 w-30 w-15-l tc mb2 relative h4  bg-white-10  flex items-center justify-center mb3-l' >
                  {item.isMyPick && <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />}

                  <div className="">

                    <div className="relative flex justify-center items--center w2-6 h2-6 center">
                      <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                        <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt=""  />
                      </div>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                    </div>
                    <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                      <div className=" oswald"> {item.playerName}  </div>
                      <div className="mt1 fw5 f8-l f10 pt1"> POINTS: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.totalPoints}</span></div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>}


        {buildTeam.bowler.length > 0 && <div className="center tc white">
          <div className="bb b--white-20  mb2 mt1"></div>
          <div className="tc ttu oswald f5 fw4 pb2">bowlers</div>
          <div className="w-100  flex flex-wrap justify-around   items-center">
            {buildTeam.bowler.map((item, index) => {
              return (
                <div key={index} className='ba b--white-20 br3 w-30 tc w-15-l mb3-l mb2 relative h4  bg-white-10 flex items-center justify-center' >
                  {item.isMyPick && <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />}

                  <div className="">
                    <div className="relative flex justify-center items--center w2-6 h2-6 center">
                      <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                        <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt=""  />
                      </div>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                    </div>
                    <div className=" f8  f7-l  justify-center pl1 items--center pt1">
                      <div className="oswald"> {item.playerName}  </div>
                      <div className="mt1 fw5 f8-l pt1 f10"> POINTS: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.totalPoints}</span></div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>}


     
      </div>
    </div>
  )
}