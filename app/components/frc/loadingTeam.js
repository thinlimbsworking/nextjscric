import React, { useState, useEffect } from 'react';
import { GET_FRC_TEAM } from '../../api/queries';
import { useQuery, useMutation, useLazyQuery } from '@apollo/react-hooks';
import { getLangText } from '../../api/services';
import { words } from '../../constant/language';

export default function LoadingTeam({
  setCurrentFlag,
  playerID,
  chooseLogic,
  leagueType,
  FantasymatchID,
  setBuildTeam,
  ...props
}) {
  const lang = props.language;
  const [rotate, setRoate] = useState('circle');
  const [windows, setLocalStorage] = useState({});
  const [newToken, setToken] = useState('');

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem('tokenData'));
    }
  }, [global.window]);

  const { loading, error, data } = useQuery(GET_FRC_TEAM, {
    variables: {
      matchID: FantasymatchID,
      playerIds: playerID,
      leagueType: leagueType,
      selectCriteria: chooseLogic,
      token: newToken
    },
    onCompleted: (data) => {
      setBuildTeam(data.getFrcTeam);
      setTimeout(() => {
        setRoate('added');
      }, 1000);
      setTimeout(() => {
        setCurrentFlag('choose Team');
      }, 3000);
    }
  });

  return (
    <div className='md:absolute lg:absolute h-screen md:w-full lg:w-full flex justify-center items-center'>
      {rotate === 'circle' && (
        <div className='flex flex-col justify-center items-center gap-8 text-black dark:text-white '>
          <img src={'/gif/frc_loader.gif'} />

          <div className='text-sm font-semibold  text-center '>
            {getLangText(lang, words, 'Calculating and')} <br /> {getLangText(lang, words, 'adding players')}{' '}
          </div>
        </div>
      )}
      {rotate === 'added' && (
        <div className='flex flex-col justify-center items-center gap-8 text-black dark:text-white'>
          <img className='w-36 h-36' src={`/gif/start-green.png`} />
          <div className='text-sm font-semibold text-center dark:text-white'>
            {getLangText(lang, words, 'Players_Added')} <br /> {getLangText(lang, words, 'Successfully')}
          </div>
        </div>
      )}
    </div>
  );
}
