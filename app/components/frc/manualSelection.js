import React, { useEffect, useState } from 'react'
import ChoosePlayer from './choosePlayer'
import Countdown from 'react-countdown-now'
import LoadingTeam from './loadingTeam.js'
import ChooseTeam from './chooseTeam.js'
import ChooseLogic from './chooseLogic'
import { format } from 'date-fns'
import { words } from '../../constant/language'
import { getLangText } from '../../api/services'
import ImageWithFallback from '../commom/Image'
import FrcMatchLayout from './frcMatchLayout'

const flagPlaceHolder = '/pngsV2/flag_dark.png'
const langIcon = '/pngsV2/lang.png'
const leftarrow = '/svgsV2/RightSchevronWhite.svg'

const leagueTypes = [
  {
    title: 'GRAND_LEAGUE',
    ref: 'grand_league',
    desc: 'WBFCL',
    imgSrc: '/pngsV2/frc_gl.png',
  },
  {
    title: 'SMALL_LEAGUE',
    ref: 'small_league',
    desc: 'FTFC',
    imgSrc: '/pngsV2/frc_sl.png',
  },
  {
    title: 'ONE_V_ONE',
    ref: 'h2h',
    desc: 'AHDAYR',
    imgSrc: '/pngsV2/frc_ovo.png',
  },
]
export default function ManualSelection(props) {
  let FantasymatchID = props.matchID
  const data =
    props.matchBasicData &&
    [
      ...props.matchBasicData.getFRCHomePage.completedmatches,
      ...props.matchBasicData.getFRCHomePage.livematches,
      ...props.matchBasicData.getFRCHomePage.upcomingmatches,
    ].filter((x) => x.matchID === FantasymatchID)

  const Main = '/pngs/ban1.png'
  const Empty = '/svgs/rightarrow.svg'
  const backIcon = '/svgs/back_dark_black.svg'
  const activeText = 'text-sm dark:text-xs  text-basered dark:text-green tc  '
  const inactiveText = 'text-sm dark:text-xs  text-gray-2 tc '
  const stadium = '/pngs/frc_stadium.png'

  const chooseLeageText =
    '  border-y border-r border-basered dark:border-green'
  const unChooseLeageText = ' md:border lg:border '
  // const [currenFlag, setCurrentFlag] = useState('choose League');

  const [currenFlag, setCurrentFlag] = useState('')
  const [leagueType, setLeagueType] = useState('')
  const [chooseLogic, setChooseLogic] = useState('')
  const [playerID, setPlayerID] = useState([])
  const [players, setPlayers] = useState([])
  const [language, setLanguage] = useState(props.language)
  const [homeTeamNo, setHomeTeamNo] = useState(0)
  const [awayTeamNo, setAwayTeamNo] = useState(0)
  const [totalCredits, setTotalCredits] = useState(
    props.urlData?.ffCode
      ? props.urlData.team.reduce(
        (pl, i) => parseFloat(pl.credits) + parseFloat(i),
      )
      : 0,
  )
  const [buildTeam, setBuildTeam] = useState(
    props.urlData?.ffCode
      ? {
        batsman: props.urlData.team.filter(
          (pl) => pl.player_role == 'BATSMAN',
        ),
        bowler: props.urlData.team.filter((pl) => pl.player_role == 'BOWLER'),
        keeper: props.urlData.team.filter((pl) => pl.player_role == 'KEEPER'),
        all_rounder: props.urlData.team.filter(
          (pl) => pl.player_role == 'ALL_ROUNDER',
        ),
      }
      : {},
  )

  useEffect(() => {
    setCurrentFlag(props.urlData?.ffCode ? 'choose Team' : 'choose League')
    setLeagueType(props.urlData?.ffCode ? props.urlData.leagueType : '')
    setChooseLogic(props.urlData?.ffCode ? props.urlData.selectCriteria : '')
  }, [])

  const handleChange = (type) => {
    const index = leagueType.indexOf(type)
    if (index > -1) {
      leagueType.splice(index, 1)

      setLeagueType([...leagueType])
    } else {
      setLeagueType([...leagueType, type])
    }
  }
  // let month={
  //   "january":["january","जनवरी"],
  //   "february":["february","फ़रवरी"],
  //   "march":["march","मार्च"],
  //   "april":["april","अप्रैल"],
  //   "may":["may","मई"],
  //   "june":["june","जून"],
  //   "july":["july","जुलाई"],
  //   "august":["august","अगस्त"],
  //   'September':["september","सितंबर"],
  //   "october":["october","अक्टूबर"],
  //   "november":["november","नवंबर"],
  //   "december":["december","दिसंबर"]
  // }

  const handleMonthHindiTag = () => {
    let monthformat = format(
      Number(data[0]?.matchDateTimeGMT) - 19800000,
      'MMMM',
    )

    let date = format(Number(data[0]?.matchDateTimeGMT) - 19800000, 'do')
    let year = format(Number(data[0]?.matchDateTimeGMT) - 19800000, 'yyyy')
    //  console.log("monthformat",monthformat)

    if (language === 'HIN') {
      return (
        getLangText(language, words, monthformat.toLowerCase()) +
        ' ' +
        date +
        ' ' +
        year
      )
    } else {
      return monthformat + ' ' + date + ' ' + year
    }
  }
  const handleMonthHindi = () => {
    let monthformat = format(
      Number(data[0]?.matchDateTimeGMT) - 19800000,
      'MMMM',
    )

    let date = format(Number(data[0]?.matchDateTimeGMT) - 19800000, 'do')
    // console.log("monthformat",monthformat)

    if (language === 'HIN') {
      return (
        getLangText(language, words, monthformat.toLowerCase()) + ' ' + date
      )
    } else {
      return date + ' ' + monthformat
    }
  }

  if (currenFlag === 'Loading') {
    return (
      <div className="relative w-full">
        <LoadingTeam
          language={language}
          setCurrentFlag={setCurrentFlag}
          playerID={playerID}
          setPlayerID={setPlayerID}
          chooseLogic={chooseLogic}
          leagueType={leagueType}
          FantasymatchID={FantasymatchID}
          setBuildTeam={setBuildTeam}
        />{' '}
      </div>
    )
  }
  return (
    <FrcMatchLayout
      titleName={getLangText(language, words, "BYT")}
      updateLanguage={setLanguage}
      matchData={props.matchData?.miniScoreCard.data[0]}
      currenFlag={currenFlag}
      setCurrentFlag={setCurrentFlag}
    >
      <div className="text-black  dark:text-white p-3 md:p-0 lg:p-0 ">
        {currenFlag !== 'Loading' && (
          <div className="">
            <div className="flex items-center justify-between ">
              {/* <div className='flex items-center '>
              <div className='bg-gray p-3 rounded'>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  className='w-4 h-4  rotate-180'
                  onClick={() =>
                    currenFlag == 'choose players'
                      ? setCurrentFlag('choose League')
                      : currenFlag == 'choose logic'
                      ? setCurrentFlag('choose players')
                      : currenFlag == 'choose Team' && !props.urlData?.ffCode
                      ? setCurrentFlag('choose logic')
                      : window.history.back()
                  }
                  alt=''
                  src={leftarrow}
                />
              </div>
              <div className='text-lg font-semibold pl-3 capitalize'>{getLangText(lang, words, 'BYT')}</div>
            </div> */}
              {/* <img className='w-10 h-10' src={langIcon} /> */}
            </div>

            <div className=" p-2 rounded-md md:hidden lg:hidden flex items-center  bg-gray ">
              <div className="w-full">
                {/* {console.log('ssss', data[0])} */}
                <div>
                  {currenFlag === 'choose League' && (
                    <div className="text-sm font-medium">
                      {language === 'HIN'
                        ? data[0]?.matchNameHindi.split(',')[1] +
                        ' ' +
                        data[0]?.matchNameHindi.split(',')[2]
                        : data[0]?.tourName}
                    </div>
                  )}
                  {currenFlag === 'choose League' && (
                    <div className="text-sm font-semibold text-gray-2 pt-1 mb-2">
                      {language === 'HIN'
                        ? data[0]?.matchNameHindi.split(',')[1] +
                        ' ' +
                        data[0]?.matchNameHindi.split(',')[0]
                        : data[0]?.matchNumber}
                    </div>
                  )}

                  <div className="bg-gray-4 rounded p-3 ">
                    <div className="flex items-center justify-between ">
                      <div className="flex  items-center">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={flagPlaceHolder}

                          alt="home"
                          src={`https://images.cricket.com/teams/${data[0]?.homeTeamID}_flag_safari.png`}
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          className="h-5 w-8 shadow-4"
                        />
                        <div className="pl-1 text-sm font-semibold ">
                          {data[0]?.homeTeamShortName}
                        </div>
                      </div>
                      <div className="border-green border-2 rounded-full px-2 py-0.5 text-xs font-medium  text-green bg-gray-8">
                        {data[0]?.matchType}
                      </div>
                      <div className="flex items-center">
                        <div className=" pr-2 text-sm font-semibold">
                          {data[0]?.awayTeamShortName}
                        </div>
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc={flagPlaceHolder}
                          alt=""
                          src={`https://images.cricket.com/teams/${data[0]?.awayTeamID}_flag_safari.png`}
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          className="h-5 w-8 shadow-4"
                        />
                      </div>
                    </div>
                    <div className="flex mt-3 justify-center items-center text-xs font-medium">
                      <div className=""> {handleMonthHindi()} </div>
                      <div className="">
                        {' '}
                        ,
                        {format(
                          Number(data[0]?.matchDateTimeGMT) - 19800000,
                          'h:mm a',
                        )}{' '}
                        IST
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className=" my-3 text-xs font-medium  ">
              <div className="flex justify-around items-center  ">
                <div
                  className={`flex flex-col md:flex-row lg:flex-row md:gap-1 lg:gap-1 ${currenFlag == 'choose League'
                      ? activeText
                      : currenFlag == 'choose players' ||
                        currenFlag == 'choose logic' ||
                        currenFlag == 'choose Team'
                        ? 'text-basered dark:text-green opacity-40'
                        : inactiveText
                    }`}
                >
                  <span>
                    {' '}
                    {language == 'HIN'
                      ? getLangText(language, words, 'League')
                      : getLangText(language, words, 'Choose')}{' '}
                  </span>
                  <span>
                    {language == 'HIN'
                      ? getLangText(language, words, 'Choose')
                      : getLangText(language, words, 'League')}{' '}
                  </span>
                </div>
                <div
                  className={` flex flex-col md:flex-row lg:flex-row md:gap-1 lg:gap-1 ${currenFlag == 'choose players'
                      ? activeText
                      : currenFlag == 'choose logic' ||
                        currenFlag == 'choose Team'
                        ? 'text-basered dark:text-green opacity-40'
                        : inactiveText
                    }`}
                >
                  <span>
                    {language == 'HIN'
                      ? getLangText(language, words, 'PLAYERS')
                      : getLangText(language, words, 'Choose')}{' '}
                  </span>
                  <span>
                    {language == 'ENG'
                      ? getLangText(language, words, 'Players')
                      : getLangText(language, words, 'Choose')}
                  </span>
                </div>

                <div
                  className={` flex flex-col md:flex-row lg:flex-row md:gap-1 lg:gap-1 ${currenFlag == 'choose logic'
                      ? activeText
                      : currenFlag == 'choose Team'
                        ? 'text-basered dark:text-green opacity-40'
                        : inactiveText
                    }`}
                >
                  <span>
                    {language == 'HIN'
                      ? getLangText(language, words, 'LOGIC')
                      : getLangText(language, words, 'Choose')}{' '}
                  </span>
                  <span>
                    {language == 'ENG'
                      ? getLangText(language, words, 'LOGIC')
                      : getLangText(language, words, 'Choose')}
                  </span>
                </div>

                <div
                  className={` flex flex-col md:flex-row lg:flex-row md:gap-1 lg:gap-1 ${currenFlag == 'choose Team' ? activeText : inactiveText
                    }`}
                >
                  <span>
                    {' '}
                    {language == 'HIN'
                      ? getLangText(language, words, 'TEAM')
                      : getLangText(language, words, 'Choose')}{' '}
                  </span>
                  <span>
                    {' '}
                    {language == 'ENG'
                      ? getLangText(language, words, 'TEAM')
                      : getLangText(language, words, 'Choose')}
                  </span>
                </div>
              </div>
              <div className="w-100 h-1 bg-gray mt-3 rounded-full flex justify-start">
                <span
                  className={`bg-basered dark:bg-green rounded-full ${currenFlag == 'choose League'
                      ? 'w-1/4'
                      : currenFlag == 'choose players'
                        ? 'w-1/2'
                        : currenFlag == 'choose logic'
                          ? 'w-3/4'
                          : currenFlag == 'choose Team'
                            ? 'w-full'
                            : ''
                    }`}
                ></span>
              </div>
            </div>
          </div>
        )}
        <div className='bg-white dark:bg-transparent md:px-3 lg:px-3 md:py-1 lg:py-1'>

        {currenFlag == 'choose League' && (
          <>
            <div className="text-md font-semibold mt-4 tracking-wide">
              {getLangText(language, words, 'Your league')}{' '}
              {getLangText(language, words, 'your preference')}
            </div>
            <div className="bg-blue-8 w-16 h-1.5 mt-1 rounded"></div>
            <div className="py-2 text-gray-2 text-xs font-base">
              {getLangText(language, words, 'SYJBCAL')}
            </div>

            <div className="mt-3 md:flex lg:flex md:gap-3 lg:gap-3">
              {leagueTypes.map((league) => (
                <div
                  className={`mb-3  rounded-xl bg-white text-black dark:text-white dark:bg-gray cursor-pointer flex items-center overflow-hidden h-20 md:w-1/3 lg:w-1/3 ${leagueType === league.ref
                      ? chooseLeageText
                      : unChooseLeageText
                    }`}
                  onClick={() => setLeagueType(league.ref)}
                >
                  <div className="bg-basered dark:bg-green  w-2 h-full "></div>
                  <div className="px-3 flex items-center">
                    <ImageWithFallback height={48} width={48} loading='lazy'
                      className={`w-16 h-16`} src={league.imgSrc} alt='' />
                    <div className=" pl-2 cursor-pointer">
                      <div className="text-sm font-semibold">
                        {getLangText(language, words, league.title)}
                      </div>
                      <div className="h-1 rounded mt-1 w-12 bg-blue-8 "></div>
                      <div className="mt-2 text-xs  ">
                        {getLangText(language, words, league.desc)}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>

            <div className="flex items-center my-8 justify-end md:justify-center lg:justify-center  cursor-pointer">
              <div
                onClick={() =>
                  leagueType.length > 0 ? setCurrentFlag('choose players') : ''
                }
                className={
                  leagueType.length > 0
                    ? 'border-2 bg-basered text-white border-basered dark:border-green dark:bg-gray-8 text-center flex item-center justify-center dark:text-green py-2 px-4  uppercase text-sm font-medium rounded-md'
                    : ' dark:border-2 border-gray-2 bg-gray-8 text-center flex item-center justify-center text-gray-2 py-2 px-4 rounded-md uppercase text-sm font-medium'
                }
              >
                {getLangText(language, words, 'PROCEED')}
              </div>
            </div>
          </>
        )}

        {currenFlag === 'choose players' && (
          <ChoosePlayer
            language={language}
            playerID={playerID}
            setPlayerID={setPlayerID}
            leagueType={leagueType}
            setCurrentFlag={setCurrentFlag}
            setPlayers={setPlayers}
            players={players}
            rules={props.data.getPlayerSelectionComposition}
            FantasymatchID={FantasymatchID}
            teamDetails={data}
            setHomeTeamNo={setHomeTeamNo}
            setAwayTeamNo={setAwayTeamNo}
            totalCredits={totalCredits}
            setTotalCredits={setTotalCredits}
          />
        )}
        {currenFlag == 'choose logic' && (
          <ChooseLogic
            language={language}
            chooseLogic={chooseLogic}
            setChooseLogic={setChooseLogic}
            leagueType={leagueType}
            setCurrentFlag={setCurrentFlag}
            homeTeamNo={homeTeamNo}
            awayTeamNo={awayTeamNo}
            homeTeamID={data[0]?.homeTeamID}
            FantasymatchID={FantasymatchID}
            awayTeamID={data[0]?.awayTeamID}
            totalCredits={totalCredits}
            matchType={data[0]?.matchType}
          />
        )}
        {/* {currenFlag === 'Loading' && (
        <LoadingTeam
          language={language}
          setCurrentFlag={setCurrentFlag}
          playerID={playerID}
          setPlayerID={setPlayerID}
          chooseLogic={chooseLogic}
          leagueType={leagueType}
          FantasymatchID={FantasymatchID}
          setBuildTeam={setBuildTeam}
        />
      )} */}
        {currenFlag === 'choose Team' && (
          <ChooseTeam
            language={language}
            playerID={playerID}
            setPlayerID={setPlayerID}
            chooseLogic={chooseLogic}
            leagueType={leagueType}
            setCurrentFlag={setCurrentFlag}
            FantasymatchID={FantasymatchID}
            teamDetails={data}
            buildTeam={buildTeam}
            ffcode={props.urlData?.ffCode ? props.urlData?.ffCode : ''}
            matchType={data[0]?.matchType}
            editTeamName={props.urlData?.ffCode ? props.urlData.teamName : ''}
          />
        )}
        </div>
      </div>
    </FrcMatchLayout>
  )
}
