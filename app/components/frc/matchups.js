import React, { useEffect, useState } from 'react'
import SwiperModule from '../test'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING } from '../../api/queries'
import ImageWithFallback from '../commom/Image'
import Heading from '../commom/heading'
import Loading from '../loading'
export default function Matchups(props) {
  const [toggle, setToggle] = useState(0)
  const [tabIndex, setTabIndex] = useState(0)
  const [bowler, setBowler] = useState(0)

  
  function fetchMatchups(data) {
    if(!data) return
        let temp = props.playersList
        ?.filter(
          (player) => player.player1 === props.RecentPlayer.playerID,
        )
       
        let ind = data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
        .mappings.findIndex(e => e.batsmanId === props.RecentPlayer?.playerID)
        if(ind > -1) {
          setToggle(0);
          setTabIndex(ind);
          setBowler(data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
            .mappings?.[ind]?.bowlers.find(ele => ele.bowlerId === temp[0].player2
              ).bowlerId)
        } else {
          setToggle(1)
          ind = data.HomeScreenPlayerMatchupsUpcomming.preMatch[1]
          .mappings.findIndex(e => e.batsmanId === props.RecentPlayer?.playerID)
          setTabIndex(ind);
          setBowler(data.HomeScreenPlayerMatchupsUpcomming.preMatch[1]
            .mappings?.[ind]?.bowlers.find(ele => ele.bowlerId === temp[0].player2
              ).bowlerId)
        }
        
  }
  const { loading, error, data, refetch } = useQuery(
    HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING,
    {
      variables: { matchID: props.matchID },
      onCompleted: data => {
        fetchMatchups(data)
      }
    },
  )
  useEffect(() => {
    fetchMatchups(data)
  },[props.RecentPlayer])

  // useEffect(() => {
  //   console.log('running useeffect...');
  //   refetch && refetch();
  //   // fetchMatchups(data)
  // })
  const capsuleCss = {
    Strong: ' text-green  text-sm  font-bold text-center',
    Average: ' text-yellow  text-sm  font-bold text-center',
    Weak: ' text-red  text-sm  font-bold text-center',
  }
  if(loading) return <Loading/>

  return data && props.playersList && props.playersList
  .filter(
    (player) => player.player1 === props.RecentPlayer.playerID,
  ).length &&
    data.HomeScreenPlayerMatchupsUpcomming &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 ? (
    <div className="w-full mt-3 md:mt-0 lg:mt-0">
      <div className="bg-white dark:bg-transparent border   rounded-md  dark:border-none dark:text-white md:p-3 lg:p-3">
        {/* <div className=" uppercase text-left">Matchups</div>
        <div className="flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded-md"></div> */}
        <Heading heading="Matchups" />

        {/* <div className='flex w-full justify-between items-center mt-5 '>
        <div className=' w-3/12 text-gray-2 text-xs flex justify-between items-center  '> <span className='h-1 w-1 bg-green rounded-full'></span> BATTING</div>

        <div className='bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-9/12 p-1'>
          <div
            className={`w-1/2 text-center rounded-3xl py-2  ${toggle === 0 ? `border-2  border-green bg-gray-8` : ''}`}
            onClick={() => (setTabIndex(0), setToggle(0))}>
            {data &&
              data.HomeScreenPlayerMatchupsUpcomming &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamShortName}
          </div>
          <div
            className={`w-1/2 text-center rounded-3xl py-2 ${toggle === 1 ? `border-2  border-green bg-gray-8` : ''}`}
            onClick={() => (setTabIndex(0), setToggle(1))}>
            {data && data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamShortName}
          </div>
        </div>
      </div> */}

        {/* <SwiperModule
        toggle={toggle}
        homeTeamID={data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamID}
        awayTeamID={data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamID}
        tabIndex={tabIndex}
        setTabIndex={setTabIndex}
        data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
      /> */}
        <div className="flex mt-2  justify-center items-center">
          <div
            className={`flex bg-white dark:bg-gray border    dark:border-none dark:text-white  w-40 rounded-lg justify-center items-center py-4 relative  mb-3 `}
          >
            <div className="flex flex-col gap-4 relative items-center justify-center  ">
              <div className="flex relative items-center justify-center ">
                <ImageWithFallback
                  height={48}
                  width={48}
                  loading="lazy"
                  fallbackSrc="/pngsV2/playerph.png"
                  className="h-24 w-24 bg-gray-10 dark:bg-gray-8 object-top object-cover rounded-full"
                  src={`https://images.cricket.com/players/${
                    props.RecentPlayer && props.RecentPlayer.playerID
                  }_headshot_safari.png`}
                  // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                />

                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/svgs/images/flag_empty.svg"
                  className=" absolute h-6 w-6  border border-white bg-gray-4 p-1 rounded-lg  left-0 bottom-0 object-fill"
                  src={
                    props.RecentPlayer?.playerSkill === 'ALL_ROUNDER'
                      ? '/pngsV2/allrounder-line-white.png'
                      : props.RecentPlayer?.playerSkill === 'BOWLER'
                      ? '/pngsV2/bowlericon.png'
                      : '/pngsV2/battericon.png'
                  }
                />

                <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/pngsV2/playerph.png"
                    className="  h-5 w-5  rounded-full"
                    src={`https://images.cricket.com/teams/${
                      props.RecentPlayer && props.RecentPlayer.teamID
                    }_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                  />
                </div>
              </div>

              <div className=" text-center truncate text-base font-semibold mt-2 ">
                {props.RecentPlayer && props.RecentPlayer.name}
              </div>
            </div>
          </div>
        </div>

        <div className="flex flex-col bg-white dark:bg-transparent   rounded-lg items-center justify-between w-full   p-3">
          <div className="flex w-full  p-3 items-center justify-center dark:bg-gray  border dark:border-none rounded-lg">
            <div className="w-1/3 flex flex-col items-center justify-center">
              <div className="text-xs font-medium uppercase text-gray-2">
                Balls{' '}
              </div>
              <div className="uppercase text-xl font-semibold  ">
                {(data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex]?.bowlers.find(ele => ele.bowlerId === bowler)?.ballsFaced) ||
                  '--'}
              </div>
            </div>
            <div className="flex flex-col  w-1/3 items-center justify-center border-x dark:border-black ">
              <div className="uppercase text-xs font-medium text-gray-2">
                RUNS{' '}
              </div>
              <div className="uppercase text-xl font-semibold"> 
                {(data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex]?.bowlers.find(ele => ele.bowlerId === bowler)?.runsScored) ||
                  '--'}
                {/* {props.runs} */}
              </div>
            </div>
            <div className="flex flex-col  w-1/3  items-center justify-center">
              <div className="uppercase text-xs font-medium text-gray-2">
                WICKETS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {/* {props.wickets} */}
                {(data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex]?.bowlers.find(ele => ele.bowlerId === bowler)?.wicket) ||
                  '--'}{' '}
              </div>
            </div>
          </div>

          <div className="  flex flex-wrap justify-center w-full mt-4 items-center">
            {props.playersList &&
              props.playersList
                .filter(
                  (player) => player.player1 === props.RecentPlayer.playerID,
                )
                .map((item, i) => {
                  return (
                    <div className="w-1/2 p-2">
                      <div
                        key={i}
                        className={`flex w-full  p-3 justify-center bg-white dark:bg-gray border   cursor-pointer rounded-lg  relative  ${
                          item.player2 === bowler
                            ? 'border-green border'
                            : 'dark:border-none'
                        }`}
                        onClick={() => setBowler(item.player2)}
                      >
                        <div className="flex flex-col relative items-center justify-center gap-3">
                          <div className="flex relative items-center justify-center ">
                            <ImageWithFallback
                              height={48}
                              width={48}
                              loading="lazy"
                              fallbackSrc="/pngsV2/playerph.png"
                              className="h-20 w-20 bg-gray-10 dark:bg-gray-4 object-top object-cover rounded-full"
                              src={`https://images.cricket.com/players/${item.player2}_headshot_safari.png`}
                              // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                            />
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc="/pngsV2/playerph.png"
                              className="absolute h-6 w-6 left-0 bottom-0 object-fill"
                              src={
                                item?.player2Role === 'allrounder'
                                  ? '/pngsV2/allrounder-line-white.png'
                                  : item?.player2Role === 'batsman'
                                  ? '/pngsV2/battericon.png'
                                  : '/pngsV2/bowlericon.png'
                              }
                            />

                            <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc="/svgs/images/flag_empty.svg"
                                className="  h-5 w-5  rounded-full"
                                src={`https://images.cricket.com/teams/${
                                  toggle === 0
                                    ? data.HomeScreenPlayerMatchupsUpcomming
                                        .preMatch[1].awayTeamID
                                    : data.HomeScreenPlayerMatchupsUpcomming
                                        .preMatch[0].homeTeamID
                                }_flag_safari.png`}
                                // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                              />
                            </div>
                          </div>

                          <div className=" text-center truncate  text-sm font-semibold mt-2 ">
                            {item.player2Name}
                          </div>
                          <div className={` ${capsuleCss[item.label]}`}>
                            {item.label}
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })}
          </div>
        </div>

        {/* desktop */}
        <div className="p-3 bg-white dark:bg-gray border   rounded-md mx-2  hidden   ">
          {/* 
     <div className='flex w-full justify-between items-center mt-5  my-3'>
       <div className=' w-6/12 text-gray-2 text-xs flex  items-center  '><span className='mx-2 h-2 w-2 bg-green rounded-full'></span> BATTING</div>

       <div className='bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-4/12 '>
         <div
           className={`w-1/2 text-center rounded-3xl py-1  ${toggle === 0 ? `border-2  border-green bg-gray-8` : ''}`}
           onClick={() => (setTabIndex(0), setToggle(0))}>
           {data &&
             data.HomeScreenPlayerMatchupsUpcomming &&
             data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
             data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamShortName}
         </div>
         <div
           className={`w-1/2 text-center rounded-3xl py-1 ${toggle === 1 ? `border-2  border-green bg-gray-8` : ''}`}
           onClick={() => (setTabIndex(0), setToggle(1))}>
           {data && data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamShortName}
         </div>
       </div>
     </div> */}
          <Heading heading="Matchups" />
          <div className="flex hidden items-center justify-center mt-3">
            <div className="w-6/12">
              {' '}
              <div className="flex   justify-center items-center">
                <div
                  className={`flex   w-10/12 justify-center items-center bg-gray h-40 relative rounded-xl mb-3 `}
                >
                  <div className="flex flex-col relative items-center justify-center  ">
                    <div className="flex relative items-center justify-center ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc="/pngsV2/playerph.png"
                        className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                        src={`https://images.cricket.com/players/${
                          props.RecentPlayer && props.RecentPlayer.playerID
                        }_headshot_safari.png`}
                      />

                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc="/pngsV2/playerph.png"
                        className=" absolute h-6 w-6  border  left-0 bottom-0 object-fill"
                        src={'/pngsV2/bowlericon.png'}
                      />

                      <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc="/svgs/images/flag_empty.svg"
                          className="  h-5 w-5  rounded-full"
                          src={`https://images.cricket.com/teams/${
                            props.RecentPlayer && props.RecentPlayer.teamID
                          }_flag_safari.png`}
                          // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                        />
                      </div>
                    </div>

                    <div className=" text-center  text-xs font-semibold mt-2 ">
                      {props.RecentPlayer && props.RecentPlayer.name}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className=" w-6/12 bg-gray flex items-center justify-center rounded  h-28">
              <div className="flex w-1/3 flex-col  items-center justify-center  ">
                <div className="text-xs font-medium uppercase text-gray-2">
                  Balls{' '}
                </div>
                <div className="uppercase text-xl font-semibold  ">
                  {props.ballFaced}
                </div>
              </div>
              <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
                <div className="uppercase text-xs font-medium text-gray-2">
                  RUNS{' '}
                </div>
                <div className="uppercase text-xl font-semibold">
                  {(data &&
                    data.HomeScreenPlayerMatchupsUpcomming &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                      .mappings[tabIndex]?.bowlers[bowler]?.runsScored) ||
                    '--'}
                </div>
              </div>
              <div className="flex flex-col  w-1/3  items-center justify-center">
                <div className="uppercase text-xs font-medium text-gray-2">
                  WICKETS{' '}
                </div>
                <div className="uppercase text-xl font-semibold">
                  {(data &&
                    data.HomeScreenPlayerMatchupsUpcomming &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                      .mappings[tabIndex]?.bowlers[bowler]?.wicket) ||
                    '--'}{' '}
                </div>
              </div>
            </div>
          </div>

          <div className="  flex flex-wrap justify-center  mt-4 items-center">
            {props.playersList &&
              props.playersList
                .filter(
                  (player) => player.player1 === props.RecentPlayer.playerID,
                )
                .map((item, i) => {
                  return (
                    <div
                      key={i}
                      className={`flex w-5/12 my-2 mx-3 justify-center bg-gray h-40 relative rounded-xl ${
                        i === bowler ? 'border-green border' : ''
                      }`}
                      onClick={() => setBowler(i)}
                    >
                      <div className="flex flex-col relative items-center justify-center ">
                        <div className="flex relative items-center justify-center ">
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            fallbackSrc="/pngsV2/playerph.png"
                            className="h-20 w-20 bg-gray-4 lg:bg-gray-2 object-top object-cover rounded-full"
                            src={`https://images.cricket.com/players/${item.player2}_headshot_safari.png`}
                            // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                          />

                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            fallbackSrc="/svgs/images/flag_empty.svg"
                            className=" absolute h-6 w-6  border left-0 bottom-0 object-fill"
                            src={'/pngsV2/bowlericon.png'}
                          />

                          <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc="/svgs/images/flag_empty.svg"
                              className="  h-5 w-5  rounded-full"
                              src={`https://images.cricket.com/teams/${
                                toggle === 0
                                  ? data.HomeScreenPlayerMatchupsUpcomming
                                      .preMatch[1].awayTeamID
                                  : data.HomeScreenPlayerMatchupsUpcomming
                                      .preMatch[0].homeTeamID
                              }_flag_safari.png`}
                              onError={(evt) =>
                                (evt.target.src = '/svgs/images/flag_empty.svg')
                              }
                            />
                          </div>
                        </div>
                      </div>
                      <div className=" text-center  text-xs font-semibold mt-2 ">
                        {item.player2Name}
                      </div>
                      <div className={`mt-2  ${capsuleCss[item.label]}`}>
                        {item.label}
                      </div>
                    </div>
                  )
                })}
          </div>
        </div>
      </div>{' '}
    </div>
  ) : (
    <></>
  )
}
