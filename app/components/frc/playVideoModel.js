import React, { useEffect, useState,useRef } from 'react'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import CleverTap from 'clevertap-react';
export default function PlayVideoModel(props) {

    //  const [screenWidth, setscreenWidth] = useState(null);
    const ref = useRef()
    useEffect(() => {
        const checkIfClickedOutside = e => {
          // If the menu is open and the clicked target is not within the menu,
          // then close the menu
          if (props.languagemodel &&ref &&  ref.current && !ref.current.contains(e.target)) {
            props.setplayvideo(false)
          }
        }
        document.addEventListener("mousedown", checkIfClickedOutside)
        return () => {
          // Cleanup the event listener
          document.removeEventListener("mousedown", checkIfClickedOutside)
        }
      }, [props.playvideomodel])


    return (


        <div
            className='flex justify-center items-center fixed absolute--fill z-50 bg-black-300 overflow-y-scroll'
            style={{ backdropFilter: 'blur(4px)' }}>
                
            <div className='     relative     ' style={{height:'80vh'}} ref={ref}>
            <div className="absolute   pa2 fw6 f7" style={{right:5 ,top:-49}} onClick={()=>props.setplayvideo(false)}><div className="w2 h2 flex items-center justify-center shadow3 br-100 bg-black-70" style={{ backdropFilter: 'blur(4px)' }}>X</div></div>
                {/* <div className='w-100  flex flex-row '>

                    <div className='flex justify-end items-center white fw6 ph3 pv2 cursor-pointer' onClick={() => props.setplayvideo(false)}
                    >X</div>


                </div> */}
                <div className=" w-full "  style={{height:'100vh',width:'56vw'}} >
                <iframe width="100%" allowfullscreen height="80%" 
                    frameborder="0"
                    src='https://s3.ap-south-1.amazonaws.com/admin.devcdc.com/FRC+Final+Cut+Compressed.m4v' allow="autoplay"></iframe>
                    </div>
                {/* <video width="100%" height="400px" autoplay>
                    <source src={'https://s3.ap-south-1.amazonaws.com/admin.devcdc.com/FRC+Final+Cut+Compressed.m4v'} type="video/mp4" />

                    Your browser does not support the video tag.
                </video> */}
            </div>
        </div>
    )
}
