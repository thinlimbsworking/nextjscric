import React, { useEffect } from 'react'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import CleverTap from 'clevertap-react';
import ImageWithFallback from '../commom/Image';
export default function playerSlectionModal(props) {
 
  
  useEffect(() => {
    props.callScroll(true);
    return () => {
      props.callScroll(false);
    };
  }, []);


 const batsmanImage={
   'left-handed': {image:'/svgs/lefthand.svg',Name:"Left Hand"},
   'right-handed': {image:'/svgs/righthand.svg',Name:"Right Hand"},  
 }

  return (
   

      <div
        className='flex justify-center items-center fixed absolute--fill z-9999 bg-basebg/30 overflow-y-scroll'
        style={{ backdropFilter: 'blur(4px)' }}>
        <div className=' bg-gray-4 br3 shadow-4  ba b--white-20 relative  min-vh-90 pt2 mh2  w-100 w-50-l '>
          <div className='w-100  flex flex-row '>
            <div className='flex w-80 justify-start items-center ttu'></div>
            <div className='flex w-20 justify-end white items-center fw6 ph3 pv2 cursor-pointer'
              onClick={() => { props.setOpenSelectionBat(false) }}
            >X</div>
          </div>
          <div className='  w-100  flex pl2 flex-wrap  mt2 mb2' >
          {(props.Player2IDMatchUp!=="" && props.Player1IDMatchUp!==""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player =>  player.player2 === props.Player2IDMatchUp && player.player1 !== props.Player1IDMatchUp && player.player1Role==='Batting Type'):
           props.Player2IDMatchUp==="" && props.Player1IDMatchUp!=="" ?props.player1filter.filter(player=> player.player1 !== props.Player1IDMatchUp && player.player1Role==='Batting Type'):
           props.Player2IDMatchUp!=="" && props.Player1IDMatchUp===""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player2 === props.Player2IDMatchUp && player.player1Role==="Batting Type"): 
              props.player1filter.filter(player=>player.player1Role==='Batting Type')).map((item,i) => {
            return (<div key={i} onClick={()=>{
               props.setPlayer1IDMatchUp(item.player1);
                props.setPlayer1NameMatchUp(item.player1Name);
                props.setPlayer1TeamIDMatchUp(item.player1Team);
                props.setOpenSelectionBat(false);
               props. setPlayer1TypeMatchUp("Batting Type");
               CleverTap.initialize('FantasyPlayerSwap', {
                Source: 'AdvancedMU',
                SeriesID:props.seriesID,
                 TeamID:'',
                 PlayerID:batsmanImage[item.player1].Name,
                MatchStatus:props.matchStatus,
                MatchID:props.matchID,
                MatchType:props.matchType,
                Platform: localStorage ? localStorage.Platform : ''
              });
            }}
           
              className="w-20  w-15-l   cursor-pointer ba b--white-20  ma1 flex  flex-column justify-center items-center  bg-dark-gray   br2 white h4" style={{ background: 'rgb(16, 16, 16)' }} >
      
              <div className=' mt1  relative flex-column  justify-center items-center' style={{ background: 'rgb(16, 16, 16)' }}>
                <div
                  className='h3  overflow-hidden bg-gray' style={{ background: 'rgb(16, 16, 16)', height: '5rem' }}>
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    className='flex    justify-center items-center'
                    src={batsmanImage[item.player1].image}
                    alt=''
                  />
                </div>

              </div>
              <div className='mt1 w-100 h2  fw5   f8  fw6  justify-center items-center tc'>
                <div className="tc ttu"> {batsmanImage[item.player1].Name} </div>

              </div>
            </div>)
          })}
        </div>

        
        {(props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player2 === props.Player2IDMatchUp && 
        player.player1 !== props.Player1IDMatchUp &&
         player.player1Role==='Batting Type').length>0) && ( props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && 
        props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player2 === props.Player2IDMatchUp &&  player.player1!==props.Player1IDMatchUp 
        && player.player1Role==="batsman").length>0) &&
         <div className="bt b--white-20 "></div>}
         <div className='flex w-100 justify-start  items-center ttu white  pa3 '>Batters </div>

         <div className={`overflow-y-scroll pv2 overflow-hidden hidescroll ph2  ${(props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player2 === props.Player2IDMatchUp && player.player1 !== props.Player1IDMatchUp && player.player1Role==='Batting Type').length===0)?'max-vh-70':'max-vh-55'} `}  >
            
          <div className='w-100  flex flex-wrap   items-center ' >
            {(props.Player2IDMatchUp!=="" && props.Player1IDMatchUp!==""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player2 === props.Player2IDMatchUp &&  player.player1!==props.Player1IDMatchUp && player.player1Role==="batsman"): 
            props.Player2IDMatchUp==="" && props.Player1IDMatchUp!=="" ?props.player1filter.filter(player=> player.player1 !== props.Player1IDMatchUp && player.player1Role==='batsman'): 
            props.Player2IDMatchUp!=="" && props.Player1IDMatchUp===""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player2 === props.Player2IDMatchUp && player.player1Role==="batsman"): 
            props.player1filter.filter(player=>player.player1Role==='batsman')).map((item, index) => 
             <div key={index} className="w-50">
                 <div 
                  onClick={() => {
                    // handlePress(item, index, 'bol');
                      props.setPlayer1IDMatchUp(item.player1);
                      props.setPlayer1NameMatchUp(item.player1Name);
                      props.setPlayer1TeamIDMatchUp(item.player1Team);
                      props.setOpenSelectionBat(false);
                      props. setPlayer1TypeMatchUp("batsman");
                      CleverTap.initialize('FantasyPlayerSwap', {
                      Source: 'AdvancedMU',
                      SeriesID:props.seriesID,
                      TeamID:item.player1Team,
                      playerID:item.player1,
                      MatchStatus:props.matchStatus,
                      MatchID:props.matchID,
                      MatchType:props.matchType,
                      Platform: localStorage ? localStorage.Platform : ''
                   });
                  }}
                  className="  ph2  cursor-pointer   mh1 mv1 flex  flex-column justify-center items-center  bg-dark-gray  ba b--white-20 br2 white" style={{ background: 'rgb(16, 16, 16)' }}
                >
                  <div className=' mt1  relative flex-column  justify-center items-center' style={{ background: 'rgb(16, 16, 16)' }}>
                    <div
                      className='br-50  w3 h3  overflow-hidden bg-gray ba  b--white-20' style={{ background: 'rgb(16, 16, 16)' }}>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png'
                        className='flex    justify-center items-center'
                        src={`https://images.cricket.com/players/${item.player1}_headshot.png`}
                        alt=''
                        // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                      />
                    </div>

                    <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                      className='absolute bottom-0 left-0  br-100 w13 h13 ba b--black  object-cover '
                      src={`https://images.cricket.com/teams/${item.player1Team}_flag_safari.png`}
                      alt=''
                      // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    />
                  </div>
                  <div className='mt1 w-100 h2  fw5 ml1  f6  fw6  justify-center items-center tc'>
                    <div> {item.player1Name} </div>

                  </div>
                </div>
                </div>
             
            )}
          </div>
          {/* <div className="flex justify-center absolute bottom-0 right-0 left-0 pv3">
          <img className="w2" src={'/svgs/yellowDownArrow.svg'} />
        </div> */}
          </div>
         
          
        </div>
      </div>
    
  )
}
