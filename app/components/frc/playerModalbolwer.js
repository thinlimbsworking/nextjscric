import React, { useEffect } from 'react'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import CleverTap from 'clevertap-react';
import ImageWithFallback from '../commom/Image';
export default function playerSlectionModal(props) {
 


  useEffect(() => {
    props.callScroll(true);
    return () => {
      props.callScroll(false);
    };
  }, []);




let bowlerImage={
  
    'right-arm-fast':{ bowlerID:'/svgs/rightarmfast.svg',bowlerName:"R.arm fast"},
    'left-arm-fast': {bowlerID:'/svgs/leftarmfast.svg',bowlerName:"L.ARM fast"},    
    'off-spin': {bowlerID:'/svgs/rarmoffspin.svg',bowlerName:"R.ARM Off SPIN"},
    'slow-left-arm-orthodox':{ bowlerID:'/svgs/chinaman.svg',bowlerName:"L.ARM Orthodox"},
    'leg-spin': {bowlerID:'/svgs/rarmlegspin.svg',bowlerName:"R.arm Leg Spin"},
    'slow-left-arm-chinaman':{bowlerID: '/svgs/larmoffspin.svg',bowlerName:"L.ARM Chinaman"},
  }
    
  
  
  return (


    <div
      className='flex justify-center items-center fixed absolute--fill z-9999 bg-black-70 overflow-y-scroll'
      style={{ backdropFilter: 'blur(4px)' }}>
      <div className=' bg-black br3 shadow-4 w-50-l  ba b--white-20 relative w-100  min-vh-90 pt2 mh2 '>
        <div className='w-100 pt2  flex flex-row '>
          <div className='flex w-80 justify-start items-center ttu'></div>
          <div className='flex w-20 justify-end items-center fw6 ph2 white pv3 cursor-pointer'
            onClick={() => { props.setOpenSelectionBowl(false) }}
          >X</div>


        </div>
        <div className='  w-100  flex pl2 flex-wrap  mt2 mb2' >
          {(props.Player2IDMatchUp!=="" && props.Player1IDMatchUp!==""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2 !== props.Player2IDMatchUp && player.player2Role==='Bowling Type'):
           props.Player1IDMatchUp==="" && props.Player2IDMatchUp!=="" ?props.player2filter.filter(player=> player.player2 !== props.Player2IDMatchUp && player.player2Role==='Bowling Type'):
           props.Player1IDMatchUp!=="" && props.Player2IDMatchUp===""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2Role==='Bowling Type'):
              props.player2filter.filter(player=>player.player2Role==='Bowling Type')).map((item,i) => {
            return (<div key={i} onClick={()=>{
               props.setPlayer2IDMatchUp(item.player2);
                props.setPlayer2NameMatchUp(item.player2Name);
                props.setPlayer2TeamIDMatchUp(item.player2Team);
                props.setOpenSelectionBowl(false);
               props. setPlayer2TypeMatchUp("Bowling Type");
               CleverTap.initialize('FantasyPlayerSwap', {
                      Source: 'AdvancedMU',
                      SeriesID:props.seriesID,
                      TeamID:'',
                      MatchStatus:props.matchStatus,
                      MatchID:props.matchID,
                      playerID:bowlerImage[item.player2].bowlerName,
                      MatchType:props.matchType,
                      Platform: localStorage ? localStorage.Platform : '',
                      
                   });
            }}
           
              className="w-20  w-15-l   cursor-pointer ba b--white-20  ma1 flex  flex-column justify-center items-center  bg-dark-gray   br2 white h4" style={{ background: 'rgb(16, 16, 16)' }} >

              <div className=' mt1  relative flex-column  justify-center items-center' style={{ background: 'rgb(16, 16, 16)' }}>
                <div
                  className='h3  overflow-hidden bg-gray' style={{ background: 'rgb(16, 16, 16)', height: '5rem' }}>
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    className='flex    justify-center items-center'
                    src={bowlerImage[item.player2].bowlerID}
                    alt=''
                  />
                </div>

              </div>
              <div className='mt1 w-100 h2  fw5   f8  fw6  justify-center items-center tc'>
                <div className="tc ttu"> {bowlerImage[item.player2].bowlerName} </div>

              </div>
            </div>)
          })}
        </div>
          {(((props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2 !== props.Player2IDMatchUp && player.player2Role==="bowler").length>0) &&
          (props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2 !== props.Player2IDMatchUp && player.player2Role==='Bowling Type').length>0))) && <div className="bb b--white-20"></div>}
          <div className='flex w-100 justify-start  items-center ttu white  pa3   '>Bowlers </div>
          {/* {((props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2 !== props.Player2IDMatchUp && player.player2Role==="bowler").length>0)||( props.player2TypeMatchUp!=="")) && <div className='flex w-100 justify-start  items-center ttu white  pa3   '>Bowlers </div>} */}
        <div className={`overflow-y-scroll pv2 overflow-hidden hidescroll ph2  ${((props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2 !== props.Player2IDMatchUp && player.player2Role==='Bowling Type').length===0)||props.player2TypeMatchUp==="")?'max-vh-70':'max-vh-55-l min-vh-30 max-vh-40 '}  `} >
         




          <div className='  w-100  flex flex-wrap justify-start   items-center mt2' >
          
            {(props.Player2IDMatchUp!=="" && props.Player1IDMatchUp!==""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2 !== props.Player2IDMatchUp && player.player2Role==='bowler'):
             props.Player1IDMatchUp==="" && props.Player2IDMatchUp!=="" ?props.player2filter.filter(player=> player.player2 !== props.Player2IDMatchUp && player.player2Role==='bowler'):
             props.Player1IDMatchUp!=="" && props.Player2IDMatchUp===""?props.MatchUpsData && props.MatchUpsData.advanceMatchupsById && props.MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === props.Player1IDMatchUp && player.player2Role==='bowler'):
              props.player2filter.filter(player=>player.player2Role==='bowler')).map((item, index) =>
              <div  key={index} className="w-50">
              <div
                onClick={() => {
                  // handlePress(item, index, 'bol');
                  props.setPlayer2IDMatchUp(item.player2);
                  props.setPlayer2NameMatchUp(item.player2Name);
                  props.setPlayer2TeamIDMatchUp(item.player2Team);
                  props.setOpenSelectionBowl(false);
                  props. setPlayer2TypeMatchUp("bowler");
                  CleverTap.initialize('FantasyPlayerSwap', {
                      Source: 'AdvancedMU',
                      SeriesID:props.seriesID,
                      TeamID:item.player2Team,
                      MatchStatus:props.matchStatus,
                      playerID:item.player2,
                      MatchID:props.matchID,
                      MatchType:props.matchType,
                      Platform: localStorage ? localStorage.Platform : ''
                   });
                }}
                className="ph2    mh1 mv1  cursor-pointer pa2  ma1 flex  flex-column justify-center items-center  bg-dark-gray  ba b--white-20 br2 white" style={{ background: 'rgb(16, 16, 16)' }}
              >
                <div className=' mt1  relative flex-column  justify-center items-center' style={{ background: 'rgb(16, 16, 16)' }}>
                  <div
                    className='br-50  w3 h3  overflow-hidden bg-gray ba  b--white-20' style={{ background: 'rgb(16, 16, 16)' }}>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png'
                      className='flex    justify-center items-center'
                      src={`https://images.cricket.com/players/${item.player2}_headshot.png`}
                      alt=''
                      onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                    />
                  </div>

                  <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                    className='absolute bottom-0 left-0  br-100 w13 h13 ba b--black  object-cover '
                    src={`https://images.cricket.com/teams/${item.player2Team}_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    alt=''
                  />
                </div>
                <div className='mt1 w-100 h2  fw5 ml1  f6  fw6  justify-center items-center tc'>
                  <div> {item.player2Name} </div>

                </div>
              </div>
              </div>
            )}
          </div>
          {/* <div className="flex justify-center absolute bottom-0 right-0 left-0 pv3">
          <img className="w2" src={'/svgs/yellowDownArrow.svg'} />
        </div> */}
        </div>
        

      </div>
    </div>

  )
}