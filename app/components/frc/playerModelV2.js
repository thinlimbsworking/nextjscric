import React, { useState } from 'react'
import ImageWithFallback from '../commom/Image'
// const frc_allround='/svgs/frc_allround.svg'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import Tab from '../shared/Tab'
import Heading from '../commom/heading'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
export default function PlayerModelV2(props) {
  const categories = [
    { name: getLangText(props.lang, words, 'BATTERS'), value: 'BATSMAN' },
    { name: getLangText(props.lang, words, 'WK'), value: 'KEEPER' },
    { name: getLangText(props.lang, words, 'BOWLERS'), value: 'BOWLER' },
    { name: 'ARS', value: 'ALL_ROUNDER' },
  ]

  const [type, setType] = useState(
    props.SelectionType === 'compare'
      ? props.Player2 &&
          categories.find((ele) => ele.value === props.Player2.playerSkill)
      : props.RecentPlayer &&
          categories.find(
            (ele) => ele.value === props.RecentPlayer.playerSkill,
          ),
  )

  let filterRole = {
    Bowler: 'bowler',
    Batsman: 'batsman',
  }
  // console.log("Props model", props)
  return (
    <div
      className="flex justify-center items-center h-full fixed absolute--fill z-80 bg-black-70 "
      style={{ backdropFilter: 'blur(4px)' }}
    >
      <div className=" rounded border p-4  bg-white dark:bg-gray   relative w-11/12 md:w-5/12 lg:w-5/12 min-h-[420px] max-h-[420px] overflow-y-scroll">
        <div className=" py-2  flex  items-center justify-between ">
          <div className="px-2 ">
            <Heading heading= {props.lang === 'HIN' ? 'वर्तमान चयन' : 'CURRENT SELECTION'}/>
           
          </div>
          <div
            className="flex mr-3 justify-end items-center white fw6 ph3 pv2 cursor-pointer"
            onClick={() => {
              props.closeNewModelV2()
            }}
          >
            X
          </div>
        </div>
        <div className="w-full">
                <div className="flex  items-center p-2 justify-between  ">
                  <div className="flex items-center gap-2">

                      <ImageWithFallback height={28} width={28} className="w-14 h-14 bg-light_gray dark:bg-gray-18 rounded-full object-cover object-top" src={`https://images.cricket.com/players/${props.SelectionType==="compare"?props.Player2 && props.Player2.playerID:props.RecentPlayer && props.RecentPlayer.playerID}_headshot_safari.png`} fallbackSrc={'/pngs/fallbackprojection.png'}  alt="player"/>
                  
                    {props.lang==="HIN"?<div className="ttu f5 f4-l f4-m oswald fw6 ttu ph2">{ props.SelectionType==="compare"?props.Player2 && props.Player2.playerNameHindi : props.RecentPlayer && props.RecentPlayer.playerNameHindi}</div>:
                    <div className="text-sm font-semibold">{ props.SelectionType==="compare"?props.Player2 && props.Player2.name : props.RecentPlayer && props.RecentPlayer.name}</div>
                    }
                  </div>
                  <div className="flex gap-2 items-center">
                  <div className="font-semibold text-sm   ttu ph2">{props.SelectionType==="compare"?props.Player2 && props.Player2.teamShortName :props.RecentPlayer && props.RecentPlayer.teamShortName}</div>
                    <div className="w15 w2-l h2-l  w2-m h2-m h15 br-100 overflow-hidden ">
                      <ImageWithFallback height={18} width={18} className="w-10 h-6 object-top object-cover" src={`https://images.cricket.com/teams/${props.SelectionType==="compare"?props.Player2 && props.Player2.teamID :props.RecentPlayer && props.RecentPlayer.teamID}_flag_safari.png`} alt="" fallbackSrc={flagPlaceHolder}/>
                    </div>   
                  </div>
                </div>
              </div>
        <div className=" mt-2 mx-2 border-b dark:border-black "></div>
        <div className=" p-2"><Heading heading={props.lang==="HIN"?'खिलाड़ी बदलें':'CHANGE PLAYER'}/></div>

        <div className="w-full p-2">
          <Tab
            data={categories}
            selectedTab={type}
            type="block"
            handleTabChange={(item) => setType(item)}
          />
        </div>

        {/* <div className="flex pv2 items-center justify-between w-100">
             {<div className="w-25 flex justify-center white"><div className="cursor-pointer flex flex-column items-center " onClick={()=>{setType('KEEPER')}} >
                <div className="fw4 f8 gray" > <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='KEEPER'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='KEEPER'?black_keeper:white_wicket}/></div></div>
                  <div className={`fw6 f7 gray  tc pt1`} >{getLangText(props.lang,words,"WK")}</div>
                </div></div>}
                <div className={`${props.SelectionType==='matchups'?'w-50':'w-25 ' }  flex justify-center white`}><div className="cursor-pointer flex flex-column items-center " onClick={()=>{setType('BATSMAN')}}>
                <div className="fw4 f8 gray"> <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='BATSMAN'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='BATSMAN'?black_bat:white_bat}/></div></div>
                  <div className="fw6 f7 tc gray pt1" >{getLangText(props.lang,words,"BATTERS")}</div>
                </div></div>
                {<div className="w-25 flex justify-center white"><div className="cursor-pointer flex flex-column items-center" onClick={()=>{setType('ALL_ROUNDER')}}>
                 <div className="fw4 f8 gray"> <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='ALL_ROUNDER'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='ALL_ROUNDER'?black_allRounder:white_allRounder}/></div></div>
                  <div className="fw6 f7 gray tc pt1"  >{getLangText(props.lang,words,"ARS")}</div>
                </div></div>}
                <div className={`${props.SelectionType==='matchups'?'w-50':'w-25 ' }  flex justify-center white`}><div className="cursor-pointer flex flex-column items-center" onClick={()=>{setType('BOWLER')}}>
                <div className="fw4 f8 gray"> <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='BOWLER'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='BOWLER'?black_ball:white_ball}/></div></div>
                  <div className="fw6 f7 gray tc pt1" >{getLangText(props.lang,words,"BOWLERS")}</div>
                </div></div>
                </div> */}
        <div
          className=" overflow-y-scroll overflow-hidden hidescroll "
          style={{ maxHeight: '57vh' }}
        >
          {props.playersList
            .filter((player) =>
              props.Player2 && props.Player2.playerID
                ? player.playerID !== props.RecentPlayer.playerID &&
                  player.playerSkill === type.value &&
                  player.playerID !== props.Player2.playerID
                : player.playerID !== props.RecentPlayer.playerID &&
                  player.playerSkill === type.value,
            )
            .map((player, i) => (
              <div
                className="w-100"
                key={i}
                onClick={() => props.choosePlayer(player, props.SelectionType)}
              >
                <div className="flex  items-center m-2  justify-between border-b  dark:border-black cursor-pointer  py-2 ">
                  <div className="flex items-center ">
                    <div className="flex items-center justify-center w-14 h-14 rounded-full bg-light_gray dark:bg-gray-8  overflow-hidden ">
                      <ImageWithFallback
                        height={48}
                        width={48}
                        loading="lazy"
                        fallbackSrc="/pngs/fallbackprojection.png"
                        className=" dark w-14 h-14  object-cover object-top  rounded-full"
                        src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                        alt=""
                      />
                    </div>
                    <div className="text-sm font-semibold ml-2">
                      {props.lang === 'HIN'
                        ? player.playerNameHindi
                        : player.name}
                    </div>
                  </div>
                  <div className="flex items-center">
                    <div className=" text-sm  font-semibold mr-2">
                      {player.teamShortName}
                    </div>
                    <div className="w15 h15 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                      <ImageWithFallback
                        height={38}
                        width={38}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="w-10 h-6 object-top object-cover"
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                {/*                
                <div className="flex  items-center mv2 cursor-pointer ba b--gray justify-between  bg-white-10  ph2  ph3-l pv1 br2  mv2 mh2 mh3-l mh3-m">
                  <div className="flex items-center">
                    <div className="w2 h2 w2-6-l h2-6-l w2-6-m h2-6-m br-100  ba b--white-20 bg-gray overflow-hidden ">
                      <img className="w-100 h-100 object-cover object-top" src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`} alt="" onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}></img>
                    </div>
                    <div className="ttu f5 f4-l f4-m oswald fw6 ttu ph2">{props.lang==="ENG"?player.name:player.playerNameHindi}</div>
                  </div>
                  <div className="flex items-center">
                  <div className="ttu f5 f4-l f4-m fw6 oswald white-40 ttu ph2">{player.teamShortName}</div>
                    <div className="w15 h15 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                      <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                    </div>
                   
                  </div>
                </div> */}
              </div>
            ))}
        </div>
      </div>
    </div>
  )
}
