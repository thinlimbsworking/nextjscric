import React, { useEffect } from 'react'
import ImageWithFallback from '../commom/Image';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import CleverTap from 'clevertap-react';
export default function playerSlectionModal({ player1, player2, playerData, newSelection, currentSelection, ...props }) {

  
  useEffect(() => {
    props.callScroll(true);
    return () => {
      props.callScroll(false);
    };
  }, []);




  return (
    <div
      className='flex justify-center items-center fixed absolute--fill z-9999 bg-basebg/20 overflow-y-scroll'
      >

      <div className=' bg-gray-4 br3 shadow-4  ba b--white-20 relative  min-vh-90 pt2 mh2  w-100 w-50-l'>

        <div className='w-100  flex  flex-row '>
          <div className='flex w-80 justify-start f5 fw6 white ph2 items-center ttu'>current selection</div>
          <div className='flex w-20 justify-end items-center white fw6 ph3 pv2 cursor-pointer'
            onClick={() => { props.setOpenSelection(false) }}>X</div>


        </div>
        <div className='  w-100   flex   flex-wrap justify-start ph2  bb b--white-20 items-center ' >
          {[1].map((item, index) =>
            <div key={index} className="w-50 flex  flex-column justify-center">
            <div 
              className="  ph2  cursor-pointer   mh1 mv3 flex  flex-column justify-center items-center  bg-dark-gray  ba b--white-20 br2 white" style={{ background: 'rgb(16, 16, 16)' }}
            >
              <div className=' mt1  relative flex-column  justify-center items-center' style={{ background: 'rgb(16, 16, 16)' }}>
                <div
                  className='br-50  w3 h3  overflow-hidden bg-gray ba  b--white-20' style={{ background: 'rgb(16, 16, 16)' }}>
                  <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png'
                    className='flex    justify-center items-center'
                    src={`https://images.cricket.com/players/${currentSelection === "player1" ? player1 : player2}_headshot.png`}
                    alt={`https://images.cricket.com/players/${currentSelection === "player1" ? player1 : player2}_headshot.png`}
                    // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                  />
                </div>

                <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                  className='absolute bottom-0 left-0  br-100 w13 h13 ba b--black  object-cover '
                  src={`https://images.cricket.com/teams/${currentSelection === "player1" ? playerData.filter(player => player.playerID === player1)[0].teamID : playerData.filter(player => player.playerID === player2)[0].teamID}_flag_safari.png`}
                  alt={`https://images.cricket.com/teams/${currentSelection === "player1" ? playerData.filter(player => player.playerID === player1)[0].teamID : playerData.filter(player => player.playerID === player2)[0].teamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                />
              </div>
              <div className='mt1 w-100 h2  fw5 ml1  f6  fw6  justify-center items-center tc'>
                <div> {currentSelection === "player1" ? playerData.filter(player => player.playerID === player1)[0].name : playerData.filter(player => player.playerID === player2)[0].name} </div>

              </div>
            </div>
            </div>


          )}
        </div>

        <div className='flex w-100 justify-start white pv2 f5 fw6 ph2 items-center ttu'>Change Player</div>
        <div className=' overflow-y-scroll pv2 overflow-hidden hidescroll ph2 ' style={{maxHeight:"57vh"}}>
         




          <div className='  w-100   flex flex-wrap justify-between    items-center mt2' >
            {playerData.filter(player => player.playerID !== player1 && player.playerID !== player2).map((item, index) =>
              <div className="w-50 " key={index}>
              <div 
                onClick={() => {
                  CleverTap.initialize('FantasyPlayerSwap',{
                    source:'Face-Offs',
                    SeriesID:props.seriesID,
                    TeamID:item.teamID,
                    MatchStatus:props.matchStatus,
                    MatchID:props.matchID,
                    MatchType:props.matchType,
                    playerID:item.playerID,
                    Platform: localStorage ? localStorage.Platform : ''
                });
                  newSelection(item.name, item.playerID, item.teamID, item);
                 


                }}
                className="ph2  cursor-pointer   mh1 mv1  flex  flex-column justify-center items-center  bg-dark-gray  ba b--white-20 br2 white" style={{ background: 'rgb(16, 16, 16)' }}
              >




                
                <div className=' mt1  relative flex-column  justify-center items-center' style={{ background: 'rgb(16, 16, 16)' }}>
                  <div
                    className='br-50  w3 h3  overflow-hidden bg-gray ba b--white-20' style={{ background: 'rgb(16, 16, 16)' }}>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png'
                      className='flex    justify-center items-center'
                      src={`https://images.cricket.com/players/${item.playerID}_headshot.png`}
                      alt={`https://images.cricket.com/players/${item.playerID}_headshot.png`}
                      // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                     />
                  </div>

                  <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                    className='absolute bottom-0 left-0  br-100 w13 h13 ba b--black  object-cover'
                    src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                    alt={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                  />
                </div>
                <div className='mt1 w-100 h2  fw5 ml1  f6  fw6  justify-center items-center tc'>
                  <div> {item.name} </div>

                </div>
              </div>
              </div>)}


          </div>
          
        </div>
        {/* <div className="flex justify-center absolute bottom-0 right-0 left-0 pb3">
          <img className="w2" src={'/svgs/yellowDownArrow.svg'} />
        </div> */}

      </div>
    </div>
  )
}
