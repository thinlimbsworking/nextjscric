import React, { useState, useEffect, useRef } from 'react';
import ImageWithFallback from '../commom/Image';
import { useRouter } from 'next/navigation';
import PlayerModal from './playerSlectionModal';
import BatModal from './playerModalbat';
import BowlerModal from './playerModalbolwer';
import { STAT_HUB_PLAYER, ADVANCED_MATCHUPS } from '../../api/queries';
import { useQuery } from '@apollo/react-hooks';
import DataNotFound from '../commom/datanotfound';
// import playerSlectionModal from './playerModalbolwer';
import RecentFormModel from './RecentFormModel'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const reset= '/svgs/circular-arrows.svg';
const helmet='/svgs/cricket-helmet.svg';
const ball='/svgs/cricket-ball.svg';
import Loading from '../loading';
export default function Stathub(props) {
  
  const [Player1FaceOff, setPlayer1FaceOff] = useState('');
  const [player1Faceoffdata, setPlayer1FaceoffData] = useState({})
 const [player2Faceoffdata, setPlayer2FaceoffData] = useState({})
  const [PlayerBatsmanData, setPlayerBatsmanData] = useState(null)
  const [PlayerBowlerData, setPlayerBowlerData] = useState(null)
  const [player1RecentFormData, setplayer1RecentFormData] = useState(null)
  const [faceoff, setfaceoff] = useState(null)
  const [Player2FaceOff, setPlayer2FaceOff] = useState('');
  const [Player1IDFaceOff, setPlayer1IDFaceOff] = useState('');
  const [Player2IDFaceOff, setPlayer2IDFaceOff] = useState('');
  const downArrow = '/svgs/downArrowWhite.svg';
  const upArrow = '/svgs/upArrowWhite.svg';
  const [type, setType] = useState('All Rounder');
  const [scrollStop, setScrollStop] = useState(false);
  const [openSelection, setOpenSelection] = useState(false);
  const [openSelectionBat, setOpenSelectionBat] = useState(false);
  const [openSelectionBowl, setOpenSelectionBowl] = useState(false);
  const [currentSelectionFaceoff, setCurrentSelectionFaceoff] = useState('')
  const [Player1IDMatchUp, setPlayer1IDMatchUp] = useState(null)
  const [Player2IDMatchUp, setPlayer2IDMatchUp] = useState(null)
  const [Player2NameMatchUp, setPlayer2NameMatchUp] = useState(null)
  const [Player1NameMatchUp, setPlayer1NameMatchUp] = useState(null)
  const [Player2TeamIDMatchUp, setPlayer2TeamIDMatchUp] = useState(null)
  const [Player1TeamIDMatchUp, setPlayer1TeamIDMatchUp] = useState(null)
  const [player1TypeMatchUp, setPlayer1TypeMatchUp] = useState(null)
  const [player2TypeMatchUp, setPlayer2TypeMatchUp] = useState(null)
  const [recentForm,setRecentFormData]=useState(null)
  const [player1filter,setPlayer1filter]=useState(null)
  const [player2filter,setPlayer2filter]=useState(null)
  const [matchUpData, setMatchUpData] = useState(null)
  const [CompType,setCompType]= useState(null)
  const [recentFormModel, setrecentFormModel] = useState(false)
  const tabs = ['All Rounder', 'Bowler', 'Batsman', 'Wicket Keeper'];
  const battingStat = ['commonBatPosition', 'battingRunsInnings', 'battingAverage', 'battingStrikeRate', 'battingFoursSixes', 'battingHighestScore',]
  const bowlingStat = ['bowlingWickets', 'bowlingStrikeRate', 'bowlingEconomyRate', 'bowlingBestBowlingTotalSpell']
  const { loading, error, data: PlayerStatHubData } = useQuery(STAT_HUB_PLAYER, {
    variables: { matchID: props.matchID }, onCompleted: (data) => {
      
      if (data && data.playerHub && data.playerHub.faceOffs && data.playerHub.faceOffs.length>=2) {
        // let desc= data.playerHub.statsHubplayerList.slice(0).sort((pl1,pl2)=>  parseInt(pl2.statsHubPlayerbatting.battingRunsInnings) - parseInt(pl1.statsHubPlayerbatting.battingRunsInnings) )
       
        // const foPlayers = data.playerHub.statsHubplayerList.slice(0).sort((pl1,pl2) => parseInt(pl2.statsHubPlayerbatting.battingRunsInnings) - parseInt(pl1.statsHubPlayerbatting.battingRunsInnings) )
      // setfaceOffPlayers(foPlayers);
        //let d= data.playerHub.statsHubplayerList.sort((a,b) =>{b.statsHubPlayerbatting.battingRunsInnings-a.statsHubPlayerbatting.battingRunsInnings})
      
        setPlayer1FaceOff(data.playerHub.faceOffs[0].name);
        setPlayer2FaceOff(data.playerHub.faceOffs[1].name);
        setPlayer1IDFaceOff(data.playerHub.faceOffs[0].playerID);
        setPlayer2IDFaceOff(data.playerHub.faceOffs[1].playerID);
        setPlayer1FaceoffData(data.playerHub.faceOffs[0])
        setPlayer2FaceoffData(data.playerHub.faceOffs[1])
        setfaceoff(data.playerHub.faceOffs)
        setCompType(data.playerHub.compType)
      }

      if (data && data.playerHub && data.playerHub.recentForm && data.playerHub.recentForm.length>0) {
        setplayer1RecentFormData(data.playerHub.recentForm.filter(player => player.playerSkill === type)[0] ? data.playerHub.recentForm.filter(player => player.playerSkill === type)[0] : null)
         setPlayerBowlerData(data.playerHub.recentForm.filter(player => player.playerSkill === "Bowler")[0] ? data.playerHub.recentForm.filter(player => player.playerSkill === "Bowler")[0] : null)
        setPlayerBatsmanData(data.playerHub.recentForm.filter(player => player.playerSkill === "Batsman")[0] ? data.playerHub.recentForm.filter(player => player.playerSkill === "Batsman")[0] : null)
        setRecentFormData(data.playerHub.recentForm)
      }
        // setMatchUp(data)
        

    
    }
  });

  const { loading: MatchUpLoading, error: MatchupError, data: MatchUpsData } = useQuery(ADVANCED_MATCHUPS, {
    variables: { matchId: props.matchID }, onCompleted: (data) => {

      if (data && data.advanceMatchupsById && data.advanceMatchupsById.advanceMatchUpData && data.advanceMatchupsById.advanceMatchUpData.length !== 0) {
       
        setMatchUpData(data.advanceMatchupsById.advanceMatchUpData)
        setPlayer1IDMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player1)
        setPlayer2IDMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player2)
        setPlayer2NameMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player2Name)
        setPlayer1NameMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player1Name)
        setPlayer2TeamIDMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player2Team)
        setPlayer1TeamIDMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player1Team)
        setPlayer1TypeMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player1Role)
        setPlayer2TypeMatchUp(data.advanceMatchupsById.advanceMatchUpData[0].player2Role)
         let player1Filter = Array.from(new Set(data.advanceMatchupsById.advanceMatchUpData.map((s) => s.player1))).map((id) => {
            return {
              player1: id,
              player1Name: data.advanceMatchupsById.advanceMatchUpData.find((s) => s.player1 === id).player1Name,
              player1Role:data.advanceMatchupsById.advanceMatchUpData.find((s) => s.player1 === id).player1Role,
              player1Team:data.advanceMatchupsById.advanceMatchUpData.find((s) => s.player1 === id).player1Team,
            };
          });
         
          setPlayer1filter(player1Filter)

          let player2Filter = Array.from(new Set(data.advanceMatchupsById.advanceMatchUpData.map((s) => s.player2))).map((id) => {
            return {
              player2: id,
              player2Name: data.advanceMatchupsById.advanceMatchUpData.find((s) => s.player2 === id).player2Name,
              player2Role:data.advanceMatchupsById.advanceMatchUpData.find((s) => s.player2 === id).player2Role,
              player2Team:data.advanceMatchupsById.advanceMatchUpData.find((s) => s.player2 === id).player2Team,
            };
          });
          setPlayer2filter(player2Filter)
         
      
        
      }

    }
  });




  const callScroll = (data) => {
    data ? (document.body.style.overflow = 'hidden') : (document.body.style.overflow = 'unset');
  };

  const wrapperRef = useRef(null);
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setShowDropDown(false);
      }
    }

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [wrapperRef]);
  let router = useRouter();


  const [showDropDown, setShowDropDown] = useState(false);


  const setNewFaceoffPlayer = (playerName, playerID, teamID, playerData) => {
    setOpenSelection(false)
    currentSelectionFaceoff === 'player1' ? setPlayer1FaceoffData({ ...playerData }) : setPlayer2FaceoffData({ ...playerData })
    currentSelectionFaceoff === 'player1' ? setPlayer1IDFaceOff(playerID) : setPlayer2IDFaceOff(playerID)

  }
  const setNewRecentFormPlayer = (player) => {
    setrecentFormModel(false)
    setplayer1RecentFormData({ ...player })


  }

  const [matchID, matchName, screen, tabname] = router.query.slugs;



  const [statsTab, setStatsTab] = useState('battingRecord');
  const handlecssPlayer1 = (type, stat, player1value, player2value) => {
    if (type === "batting") {
      if (stat === "commonBatPosition") {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value !== "" && player2value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      } else {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value !== "" && player2value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      }
    } else {
      if (stat === "bowlingEconomyRate") {
        if ((player1value !== "" && player2value !== "") && parseFloat(player1value) < parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value !== "" && player2value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }

      } else if (stat === "bowlingStrikeRate") {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value !== "" && player2value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }

      } else if (stat === "bowlingBestBowlingTotalSpell") {
        if (player1value !== "" && player2value !== "") {

          const bb1 = player1value !== "" ? ~~player1value.split("/")[0] : 0
          const bb2 = player2value !== "" ? ~~player2value.split("/")[0] : 0
          if (bb1 > bb2) {
            return 'yellow-frc'
          } else if (bb1 == bb2) {
            const b21 = player1value !== "" ? ~~player1value.split("/")[1] : 0
            const b22 = player2value !== "" ? ~~player2value.split("/")[1] : 0
            if (b21 > b22) { return 'yellow-frc' } else return 'silver'

          } else {
            return 'silver'
          }
        } else if (player1value !== "" && player2value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }


      } else {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value !== "" && player2value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      }
    }
  }

  const handlecssPlayer2 = (type, stat, player1value, player2value) => {
    if (type === "batting") {

      if (stat === "commonBatPosition") {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player2value !== "" && player1value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      } else {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player2value !== "" && player1value === "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      }
    } else {
      if (stat === "bowlingEconomyRate") {
        if ((player1value !== "" && player2value !== "") && parseFloat(player1value) > parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value === "" && player2value !== "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      } else if (stat === "bowlingStrikeRate") {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value === "" && player2value !== "") {
          return "yellow-frc"
        } else {
          return "silver"
        }

      } else if (stat === "bowlingBestBowlingTotalSpell") {
        if (player1value !== "" && player2value !== "") {

          const bb1 = player1value !== "" ? ~~player1value.split("/")[0] : 0
          const bb2 = player2value !== "" ? ~~player2value.split("/")[0] : 0
          if (bb1 < bb2) {
            return 'yellow-frc'
          } else if (bb1 == bb2) {
            const b21 = player1value !== "" ? ~~player1value.split("/")[1] : 0
            const b22 = player2value !== "" ? ~~player2value.split("/")[1] : 0
            if (b21 < b22) { return 'yellow-frc' } else return 'silver'

          } else {
            return 'silver'
          }
        } else if (player1value === "" && player2value !== "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      } else {
        if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
          return "yellow-frc"
        } else if (player1value === "" && player2value !== "") {
          return "yellow-frc"
        } else {
          return "silver"
        }
      }
    }
  }

  let bowlerImage = {

    'right-arm-fast': { bowlerID: '/svgs/rightarmfast.svg', bowlerName: "R.arm fast" },
    'left-arm-fast': { bowlerID: '/svgs/leftarmfast.svg', bowlerName: "L.ARM fast" },
    'off-spin': { bowlerID: '/svgs/rarmoffspin.svg', bowlerName: "R.ARM Off SPIN" },
    'slow-left-arm-orthodox': { bowlerID: '/svgs/chinaman.svg', bowlerName: "L.ARM Orthodox" },
    'leg-spin': { bowlerID: '/svgs/rarmlegspin.svg', bowlerName: "R.arm Leg Spin" },
    'slow-left-arm-chinaman': { bowlerID: '/svgs/larmoffspin.svg', bowlerName: "L.ARM Chinaman" },
  }
  const batsmanImage = {
    'left-handed': { image: '/svgs/lefthand.svg', Name: "Left Hand" },
    'right-handed': { image: '/svgs/righthand.svg', Name: "Right Hand" },
  }


  if (loading) return <Loading />;
  else

    return (
      <div className=" bg-black">
        { ((MatchUpsData && MatchUpsData.advanceMatchupsById && MatchUpsData.advanceMatchupsById.advanceMatchUpData && MatchUpsData.advanceMatchupsById.advanceMatchUpData.length !== 0) || (faceoff  && faceoff.length > 0)) ?
          <div>
            <div className='bg-black  br2  ba b--white-20 '>
              <div className=' flex  flex-row  pt3  white w-100 ba2  pa2 '>
                <div className='flex w-100 ph2  '>
                  <div className='   flex w-50 justify-start items-center f5 fw6 ttu'>Face offs</div>
                </div>
              </div>
              <div className='mh3 bb b--white-20 ' />
              {faceoff && faceoff.length > 0 ? <div>
                <div className='w-100 flex  bg-black   items-center justify-center mv3'>
                  <div
                    className='w-40  pointer bg-dark-gray   ba b--white-20 br2   mh2 flex  items-center justify-center'
                    style={{ background: '#101010' }} onClick={() => { setOpenSelection(true); setCurrentSelectionFaceoff('player1') }}>
                    <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                      <div className='flex justify-end w-100' >
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        src='/svgs/greenarrow.svg' alt='' />
                      </div>
                      <div
                        className='h4 relative flex-column  justify-center items-center'
                        style={{ background: 'rgb(16, 16, 16)' }}>
                        <div className='flex   items-center  w-100 justify-center  ' >
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png' className=' rounded-full  h37 w37 bg-dark-gray  bw1 ba b--frc-yellow object-cover object-top' src={`https://images.cricket.com/players/${Player1IDFaceOff}_headshot.png`} alt=''  />
                        </div>


                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                          className='absolute bottom-1 object-cover    rounded-full w-10 h-10    rounded-full w-10 h-10 ' style={{ left: 2 }}
                          src={`https://images.cricket.com/teams/${faceoff && faceoff.length > 0 && faceoff.filter(player => player.playerID === Player1IDFaceOff)[0].teamID}_flag_safari.png`}
                          alt=''
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        />
                      </div>
                   
                      <div className="h2-5  pb3 pt2">
                        <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{faceoff  && faceoff.length > 0 && faceoff.filter(player => player.playerID === Player1IDFaceOff)[0].name}</div>
                      </div>
                    </div>
                  </div>
                  {/* <div className='h2 w2 rounded-full bg-yellow flex  items-center justify-center black f4 fw6'>vs</div> */}

                  <div className='h2 w2 rounded-full b--frc-yellow flex  black items-center bg-yellow f4 fw6 justify-center'>vs</div>

                  <div
                    className='w-40 pointer bg-dark-gray  ba b--white-20 br2  mh2 flex  items-center justify-center'
                    style={{ background: '#101010' }} onClick={() => { setOpenSelection(true); setCurrentSelectionFaceoff('player2') }}>
                    <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                      <div className='flex justify-end w-100' >
                        <ImageWithFallback height={18} width={18} loading='lazy'
                         src='/svgs/greenarrow.svg' alt='' />
                      </div>
                      <div
                        className='h4 relative flex-column   justify-center items-center'
                        style={{ background: 'rgb(16, 16, 16)' }}>
                        <div className='flex   items-center  w-100 justify-center ' style={{ background: '#101010' }}>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png' className=' rounded-full  h37 w37  bw1 ba b--frc-yellow bg-dark-gray object-cover object-top' src={`https://images.cricket.com/players/${Player2IDFaceOff}_headshot.png`}  alt='' />
                        </div>


                        {faceoff  && faceoff.length !== 0 ? <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                          className='absolute bottom-1 object-cover    rounded-full w-10 h-10  ' style={{ left: 2 }}
                          src={`https://images.cricket.com/teams/${faceoff.filter(player => player.playerID === Player2IDFaceOff)[0].teamID}_flag_safari.png`}
                          alt=''
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        /> : <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                          className='absolute bottom-1 object-cover    rounded-full w-10 h-10    rounded-full w-10 h-10'
                          src={flagPlaceHolder}
                          alt=''
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}

                        />}
                      </div>

                      <div className="h2-5 pb3 pt2">
                        <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{faceoff  && faceoff.length > 0 && faceoff.filter(player => player.playerID === Player2IDFaceOff)[0].name}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='flex  items-center  w-100 justify-center  '>
                  <div className='  flex w-45 justify-start items-start gray ttu  f6 tc fw6'>
                    {' '}
                    {statsTab == 'battingRecord' ? 'Batting Stats' : 'Bowling Stats'}
                  </div>

                  <div className='  flex w-45 justify-end items-center '>
                    <div className='tc '>
                      <div
                        className='flex items-center justify-between pa2   bg-dark-gray br-pill pointer'
                        onClick={() => setStatsTab(statsTab === 'battingRecord' ? 'bowlingRecord' : 'battingRecord')}>
                        <div className={` flex pointer items-center justify-center w13 h13 rounded-full pa1 ${statsTab === 'battingRecord' ? 'bg-yellow' : ''}`}>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        
                            className={`  `}
                            alt={statsTab === 'battingRecord' ? '/svgs/battogle.svg' : '/svgs/whitebat.svg'}
                            src={statsTab === 'battingRecord' ? '/svgs/battogle.svg' : '/svgs/whitebat.svg'}
                          />                </div>
                        {/* <img
                  className={` w1 h1 rounded-full  pa1 ${statsTab === 'battingRecord' ? 'bg-yellow' : ''}`}
                  alt='/svgs/battoggle.svg'
                  src='/svgs/battogle.svg'
                /> */}
                        <div className={`pointer flex items-center justify-center w13 h13 rounded-full ml2 pa1 ${statsTab === 'bowlingRecord' ? 'bg-yellow' : ''}`}>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                       
                            className={` `}
                            alt={statsTab === 'bowlingRecord' ? '/svgs/balltoggle.svg' : '/svgs/whiteball.svg'}
                            src={statsTab === 'bowlingRecord' ? '/svgs/balltoggle.svg' : '/svgs/whiteball.svg'}
                          />                </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="oswald ttu mt2">
                  {statsTab === 'battingRecord' ? <div>{battingStat.map((stat, i) =>
                    <div className={`${i !== 5 ? 'mh3 bb b--white-20' : 'mh3'}`} key={i}>
                      <div className="flex w-100 items-center justify-center mv2 f4">
                        <div className={`w-30 flex items-center ${handlecssPlayer1("batting", stat, player1Faceoffdata.statsHubPlayerbatting[stat], player2Faceoffdata.statsHubPlayerbatting[stat])} justify-center  f3 oswald fw6`}>{player1Faceoffdata && player1Faceoffdata.statsHubPlayerbatting[stat] !== "" ? player1Faceoffdata.statsHubPlayerbatting[stat] : "--"}</div>
                        <div className='w-30 flex items-center  justify-center silver   f5  fw6  oswald tc ttu'>
                          {stat === 'battingAverage' ? <div className="f5 lh-copy fw6  oswald">Average</div> : stat === 'battingFoursSixes' ? <div className="f5 lh-copy fw6  oswald">Boundaries <br />per match</div> : stat === 'battingHighestScore' ? <div className="f5  fw6 lh-copy oswald"> Highest <br /> Score</div> : stat === 'battingRunsInnings' ? <div className="f5 lh-copy fw6  oswald">Runs<br /> per innings</div> : stat === "commonBatPosition" ? <div className="f5  fw6 lh-copy  oswald">COMMON <br />BAT POSITION </div> : <div className="f5  fw6 lh-copy  oswald">SR</div>}
                        </div>
                        <div className={`w-30 flex items-center ${handlecssPlayer2("batting", stat, player1Faceoffdata.statsHubPlayerbatting[stat], player2Faceoffdata.statsHubPlayerbatting[stat])} justify-center  f3 oswald fw6`}>{player2Faceoffdata && player2Faceoffdata.statsHubPlayerbatting[stat] !== "" ? player2Faceoffdata.statsHubPlayerbatting[stat] : "--"}</div>
                      </div>

                    </div>)}</div> : <div>{bowlingStat.map((stat, i) =>
                      <div className={`${i !== 3 ? 'mh3 bb b--white-20' : 'mh3'}`} key={i}>
                        <div className="flex w-100 items-center justify-center mv2 f4">
                          <div className={`w-30   flex items-center justify-center ${handlecssPlayer1("bowling", stat, player1Faceoffdata.statsHubPlayerbowling[stat], player2Faceoffdata.statsHubPlayerbowling[stat])} f3  oswald fw6`}>
                            {player1Faceoffdata && player1Faceoffdata.statsHubPlayerbowling[stat] !== "" ? player1Faceoffdata.statsHubPlayerbowling[stat] : "--"}</div>
                          <div className='w-30 flex items-center justify-center silver oswald lh-copy f5 tc fw6 ttu'>
                            {stat === 'bowlingBestBowlingTotalSpell' ? <div className="f5 lh-copy fw6  oswald">best<br /> figures</div> : stat === 'bowlingEconomyRate' ? <div className="f5 lh-copy fw6  oswald">Economy</div> : stat === 'bowlingStrikeRate' ? <div className="f5 lh-copy fw6  oswald">SR</div> : <div className="f5 lh-copy fw6  oswald">Wickets per <br /> innings</div>}
                          </div>
                          <div className={`w-30 flex items-center justify-center   ${handlecssPlayer2("bowling", stat, player1Faceoffdata.statsHubPlayerbowling[stat], player2Faceoffdata.statsHubPlayerbowling[stat])} f3 oswald  fw6`}>{player2Faceoffdata && player2Faceoffdata.statsHubPlayerbowling[stat] !== "" ? player2Faceoffdata.statsHubPlayerbowling[stat] : "--"}</div>
                        </div>

                      </div>)}</div>}
                </div>
              </div> : <div className="fw5 f5 pv3 tc white"> Data not available</div>}


            </div>

            {recentForm &&  recentForm.length > 0 && <div className='ba br2 b--white-20 white  pa2 flex flex-column items-start justify-center mt3'>
              <div className="w-100 ">
                <div className='w-100  flex'>
                  <div className='w-50 flex flex-column   '>
                    <div className='flex mt2 w-100 mb2  flex-column justify-start items-start tr ttu f5 fw6 ttu ph2'>
                      {' '}
              Recent form
            </div>
                    <div className='w-100 bb b--white-20 ' />
                    <div></div>
                    <div className={` relative  w-100    br2 b--white-20  mt2 `} ref={wrapperRef}>
                      <div
                        className={`h-100  ${showDropDown ? 'br--top' : ''} br2 b--yellow white ba flex items-center flex-column pointer   bg-dark-gray`} onClick={() => {
                          setShowDropDown((prev) => !prev);
                        }}>
                        <div className='flex w-100 pa1 items-center justify-center '>
                          <div className='w-80  f6 fw5   pa1'>{type.toUpperCase()} </div>

                          <div className='w-20 tc  bl b--white-20 flex items-center justify-center'>
                            <ImageWithFallback height={18} width={18} loading='lazy'
                       className='w1  ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                          </div>
                        </div>
                      </div>
                      {showDropDown && (
                        <div
                          className=' b--yellow bl bb br bt  pointer br2 br--bottom white absolute w-100 h4 overflow-y-scroll z-99999 overflow-hidden bg-dark-gray' style={{ bottom: 0, left: 0, top: 32 }} >
                          {tabs.map((tabName, i) => (
                            <div
                              key={i}
                              onClick={() => {
                                setShowDropDown((prev) => !prev); setType(tabName);
                                setplayer1RecentFormData(recentForm && recentForm.filter(player => player.playerSkill === tabName)[0])
                              }}
                              className='ph2 pv2 white-80 flex items-center justify-start pointer hover-bg-black-30 '>
                              <div className='pl2 nowrap white   black fw5 f6'> {tabName.toUpperCase()}</div>
                            </div>
                          ))}
                        </div>
                      )}
                    </div>

                    <div className='flex flex-end justify-end  fw5 f5 pv3 lh-copy f5 fw6 '>{player1RecentFormData && player1RecentFormData.name}</div>
                  </div>

                  <div className='w-50  flex   items-center   justify-center' >

                    <div>
                      <div className='h3-7 relative  pointer  w3-7 flex  flex-row  items-start  justify-end ' onClick={() => { setrecentFormModel(true) }}>
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png' className=' rounded-full  h37 w37 bg-dark-gray bw1 ba b--frc-yellow object-cover object-top' src={player1RecentFormData && `https://images.cricket.com/players/${player1RecentFormData.playerID}_headshot.png`}  alt='' />

                        <ImageWithFallback height={18} width={18} loading='lazy'
                      className=' absolute' src='/svgs/greenarrow.svg' alt='' />
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                          className='absolute bottom-1 object-cover       rounded-full w-10 h-10' style={{ left: -6 }}
                          src={`https://images.cricket.com/teams/${recentForm.filter(player => player.playerID === player1RecentFormData.playerID)[0].teamID}_flag_safari.png`}
                          alt=''
                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        />

                      </div>
                    </div>
                  </div>
                </div>
                <div className='w-100  flex flex-column items-center justify-center white pa1 mt3'>
                  {player1RecentFormData && player1RecentFormData.lastFiveMatches.length !== 0 && <div className='flex flex-wrap w-100 item-center justify-center light-gray  lh-copy ttu  f7 fw8 '>
                    <div className=' w-30  flex items-center justify-center '>Opposition</div>
                    <div className='w-20 mh1 flex items-center justify-center '>Batting</div>

                    <div className='w-20 flex items-center justify-center '>Bowling</div>

                    <div className='w-20 mh1 flex items-center justify-center '>points</div>
                  </div>}

                  {player1RecentFormData && player1RecentFormData.lastFiveMatches.length !== 0 ? player1RecentFormData.lastFiveMatches.map((item, i) => {
                    return (
                      <div className='flex flex-wrap w-100 item-center justify-center mv1 light-gray f7 tc fw6' key={i}>
                        <div className=' w-30  flex items-center justify-between bg-dark-gray ph1  pv2 ttu   '>
                          <div className='w-30 tc'>vs</div>
                          <div className='w-40 flex items-center justify-center'>

                            <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder} className='h13 object-cover w13  w2-l h2-l rounded-full' src={`https://images.cricket.com/teams/${item.oppTeamID}_flag_safari.png`} alt=''  />
                          </div>

                          <div className='w-30 tc'> {item.oppTeamName} </div>
                        </div>
                        <div className='w-20  flex items-center justify-center bg-dark-gray pa1 '>{item.batting_stats}</div>

                        <div className='w-20  flex items-center justify-center bg-dark-gray pa1'>{item.bowling_stats}</div>

                        <div className='w-20  flex items-center justify-center yellow bg-dark-gray fw6 f5 pa1'>
                          {' '}
                          {item.points}
                        </div>
                      </div>
                    );
                  }) : <div className="white fw6 f6   tc ">Last 5 matches data unavailable</div>}
                </div>
              </div>
            </div>}

            {MatchUpsData && MatchUpsData.advanceMatchupsById && MatchUpsData.advanceMatchupsById.advanceMatchUpData && MatchUpsData.advanceMatchupsById.advanceMatchUpData.length !== 0 &&<div> <div className='ba b--white-20  br2 white   flex flex-column items-start justify-center mt3 pb2'>
              <div className='w-100  pv2 ph2 flex'>
                <div className='flex w-80 f5-l f5-m   justify-start items-center ttu f6 lh-title fw5   '>

                  Advanced player match-ups
                </div>
                <div className="w-20 flex items-center justify-end " onClick={()=>{
                setPlayer1IDMatchUp("");
                setPlayer1TeamIDMatchUp("");
                setPlayer1TypeMatchUp("");
                setPlayer1NameMatchUp("");
                setPlayer2IDMatchUp("");
                setPlayer2NameMatchUp("");
                setPlayer2TeamIDMatchUp("");
                setPlayer2TypeMatchUp("");

                }}><ImageWithFallback height={18} width={18} loading='lazy'
                src={reset} alt={reset} /><div className="white f7 f6-l pl1 f6-m fw5">Reset</div></div>
              </div>
              <div className="ph2 w-100">
                <div className='w-100 bb  b--white-20 ' />
              </div>



              <div className='w-100  bg-black '>
                <div className="w-100 flex items-center justify-center  ph2 mv3">
                  <div className="w-40 ">
                    <div className=' flex items-center f5 ttu justify-center '>Batsman</div>
                    <div
                      className=' pointer ba b--white-20 br2 mt1  bg-dark-gray  pa1  flex  items-center justify-center'
                      style={{ background: '#101010' }} onClick={() => setOpenSelectionBat(true)}>
                      <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 ' >
                        <div className='flex justify-end w-100 z-999' >
                          <ImageWithFallback height={18} width={18} loading='lazy'
                         src='/svgs/greenarrow.svg' alt='' />
                        </div>


                        <div
                          className='  h4 relative flex-column  justify-center items-center'
                          style={{ background: 'rgb(16, 16, 16)' }}>
                          {player1TypeMatchUp === "batsman" && <div
                            className='rounded-full   bg-gray ' style={{ background: 'rgb(16, 16, 16)' }}>
                            {/* <img className=' rounded-full  h37 w37  bw1 ba b--frc-yellow object-cover object-top' src={Player1MatchUp && `https://images.cricket.com/players/${Player1MatchUp.player1}_headshot.png`} alt='' /> */}
                            <ImageWithFallback height={18} width={18} loading='lazy' fallbackSrc='/pngs/fallbackprojection.png'
                       className=' rounded-full  h37 w37  bw1 bg-dark-gray ba b--frc-yellow object-cover object-top' src={`https://images.cricket.com/players/${Player1IDMatchUp}_headshot.png`}  alt='' />
                          </div>}
                          {player1TypeMatchUp === "" && <div
                            className='rounded-full h37  w37  flex items-center justify-center   bg-dark-gray  ' >
                            {/* <img className=' rounded-full  h37 w37  bw1 ba b--frc-yellow object-cover object-top' src={Player1MatchUp && `https://images.cricket.com/players/${Player1MatchUp.player1}_headshot.png`} alt='' /> */}
                            <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png'  src={helmet}  alt={helmet}  />
                          </div>}
                          

                          {player1TypeMatchUp === "Batting Type" && <div
                            className='rounded-full h37  w37  flex items-center justify-center   bg-dark-gray bw1 ba b--frc-yellow  '>
                            {/* <img className=' rounded-full  h37 w37  bw1 ba b--frc-yellow object-cover object-top' src={Player1MatchUp && `https://images.cricket.com/players/${Player1MatchUp.player1}_headshot.png`} alt='' /> */}
                            <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png' className=' ' src={`${batsmanImage[Player1IDMatchUp].image}`}  alt='' />
                          </div>}

                          {player1TypeMatchUp === "batsman" &&<div className="w15 ba b--black overflow-hidden  absolute bottom-1 h15 rounded-full"  style={{ left: 2 }}> <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                            className='   w-100 h-100 object-cover '
                            // src={ Player2MatchUp && `https://images.cricket.com/teams/${Player1MatchUp.player1Team}_flag_safari.png`}
                            src={`https://images.cricket.com/teams/${Player1TeamIDMatchUp}_flag_safari.png`}
                            alt=''
                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          /></div>}

                        </div>
                        <div className="h2-5  pb3 pt2">
                          <div className='flex  items-center   justify-center   white f7 fw6 tc'>{Player1NameMatchUp}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='h2 w2 rounded-full bg-yellow flex mh2 items-center justify-center black f4 fw6'>vs</div>

                  <div className="w-40">
                    <div className=' flex items-center f5 ttu justify-center '>Bowler</div>
                    <div
                      className='pointer bg-dark-gray ba b--white-20 br2 mt1   pa1 flex  items-center justify-center'
                      style={{ background: '#101010' }} onClick={() => setOpenSelectionBowl(true)}>
                      <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 '>
                        <div className='flex justify-end w-100 z-999' >
                          <ImageWithFallback height={18} width={18} loading='lazy'
                         src='/svgs/greenarrow.svg' alt='' />
                        </div>
                        <div
                          className=' h4  relative flex-column  justify-center items-center'
                          style={{ background: 'rgb(16, 16, 16)' }}>
                          {player2TypeMatchUp === "bowler" && <div
                            className='    '>
                            {/* <img className=' rounded-full  h37 w37  bw1 ba b--frc-yellow object-cover object-top' src={Player2MatchUp && `https://images.cricket.com/players/${Player2MatchUp.player2}_headshot.png`} alt='' /> */}
                            <ImageWithFallback height={18} width={18} loading='lazy'
                          fallbackSrc = '/pngs/fallbackprojection.png' className=' bw1  rounded-full  h37 w37 bg-dark-gray ba b--frc-yellow   object-cover object-top' src={`https://images.cricket.com/players/${Player2IDMatchUp}_headshot.png`} alt='' />
                          </div>}
                          {player2TypeMatchUp === "Bowling Type" && <div
                            className='rounded-full h37  w37  flex items-center justify-center   bg-dark-gray bw1 ba b--frc-yellow  '>
                            {/* <img className=' rounded-full  h37 w37  bw1 ba b--frc-yellow object-cover object-top' src={Player2MatchUp && `https://images.cricket.com/players/${Player2MatchUp.player2}_headshot.png`} alt='' /> */}
                            <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png' className=' ' src={`${bowlerImage[Player2IDMatchUp].bowlerID}`}  />
                          </div>}

                          {player2TypeMatchUp === "" && <div
                            className='rounded-full h37  w37  flex items-center justify-center   bg-dark-gray '>
                            {/* <img className=' rounded-full  h37 w37  bw1 ba b--frc-yellow object-cover object-top' src={Player2MatchUp && `https://images.cricket.com/players/${Player2MatchUp.player2}_headshot.png`} alt='' /> */}
                            <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = '/pngs/fallbackprojection.png' className=' ' src={ball} alt={ball} />
                          </div>}
                          {player2TypeMatchUp === "bowler" && <div className="w15 ba b--black overflow-hidden absolute bottom-1 h15 rounded-full"  style={{ left: 2 }}><ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                            className='w-100 h-100  object-cover' 
                            // src={ Player2MatchUp && `https://images.cricket.com/teams/${Player2MatchUp.player2Team}_flag_safari.png`}
                            src={`https://images.cricket.com/teams/${Player2TeamIDMatchUp}_flag_safari.png`}
                            alt=''
                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          /></div>}

                        </div>
                        <div className="h2-5  pb3 pt2">
                          <div className='flex   items-center   justify-center white f7 fw6 tc'>{Player2NameMatchUp}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <div className='flex w-100  flex-column justify-start items-start tr ttu f6 fw5 bt b--white-20  pa1'>
                  {' '}
                  <div className='w-100 flex flex-row items-center justify-center'>
                    <div className='w-33 flex flex-column  items-center justify-center'>
                   
                      <div className='w-100 tc yellow-frc f2 fw6'> {MatchUpsData && MatchUpsData.advanceMatchupsById && MatchUpsData.advanceMatchupsById.advanceMatchUpData.length > 0 &&   Player2IDMatchUp&& Player1IDMatchUp && (Player2IDMatchUp!=="" && Player1IDMatchUp!=="" ) ? MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === Player1IDMatchUp && player.player2 === Player2IDMatchUp)[0].ballsFaced : "_"}</div>
                      <div className='w-100 tc mt1  f7 fw6'>Balls Faced </div>
                    </div>

                    <div className='w-33 flex flex-column  items-center justify-center'>
                      <div className='w-100 tc yellow-frc f2 fw6'>{MatchUpsData && MatchUpsData.advanceMatchupsById && MatchUpsData.advanceMatchupsById.advanceMatchUpData.length > 0 &&  Player2IDMatchUp&& Player1IDMatchUp && (Player2IDMatchUp!=="" && Player1IDMatchUp!=="" )? MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === Player1IDMatchUp && player.player2 === Player2IDMatchUp)[0].Dismissals : "_"}</div>
                      <div className='w-100 tc mt1 f7 fw6'> Wickets</div>
                    </div>

                    <div className='w-33 flex flex-column  items-center justify-center'>
                      <div className='w-100 tc yellow-frc f2 fw6'>{MatchUpsData && MatchUpsData.advanceMatchupsById && MatchUpsData.advanceMatchupsById.advanceMatchUpData.length > 0 &&  Player2IDMatchUp&& Player1IDMatchUp && (Player2IDMatchUp!=="" && Player1IDMatchUp!=="" ) ? MatchUpsData.advanceMatchupsById.advanceMatchUpData.filter(player => player.player1 === Player1IDMatchUp && player.player2 === Player2IDMatchUp)[0].runsScored : "_"}</div>
                      <div className='w-100 tc mt1 f7 fw6'> Runs</div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div className="silver f7 pt1"><span>{`* ${CompType } data from last 5 years`}</span></div>
            </div>}

            {openSelection && (
              <PlayerModal
                seriesID={props.seriesID}
                matchStatus={props.matchStatus}
                matchID={props.matchID}
                matchType={props.matchType}
                setOpenSelection={setOpenSelection}
                setScrollStop={setScrollStop}
                scrollStop={scrollStop}
                callScroll={callScroll}
                player1={Player1IDFaceOff}
                player2={Player2IDFaceOff}
                newSelection={setNewFaceoffPlayer}
                playerData={faceoff}
                currentSelection={currentSelectionFaceoff}
               
              />
            )}

            {recentFormModel && (
              <RecentFormModel
                seriesID={props.seriesID}
                matchStatus={props.matchStatus}
                matchID={props.matchID}
                matchType={props.matchType}
                setrecentFormModel={setrecentFormModel}
                setScrollStop={setScrollStop}
                scrollStop={scrollStop}
                callScroll={callScroll}
                selectedPlayer={player1RecentFormData}
                setNewRecentFormPlayer={setNewRecentFormPlayer}
                playerData={recentForm}
                type={type}
              />
            )}

            {openSelectionBat && (
              <BatModal
                seriesID={props.seriesID}
                TeamBID={props.awayTeamID}
                TeamAID={props.homeTeamID}
                matchStatus={props.matchStatus}
                matchType={props.matchType}
                setOpenSelectionBat={setOpenSelectionBat}
                setScrollStop={setScrollStop}
                scrollStop={scrollStop}
                callScroll={callScroll}
                MatchUpsData={MatchUpsData}
                setPlayer1IDMatchUp={setPlayer1IDMatchUp}
                setPlayer1NameMatchUp={setPlayer1NameMatchUp}
                setPlayer1TeamIDMatchUp={setPlayer1TeamIDMatchUp}
                setPlayer1TypeMatchUp={setPlayer1TypeMatchUp}
                Player1IDMatchUp={Player1IDMatchUp}
                Player2IDMatchUp={Player2IDMatchUp}
                Player1NameMatchUp={Player1NameMatchUp}
                Player2NameMatchUp={Player2NameMatchUp}
                Player1TeamIDMatchUp={Player1TeamIDMatchUp}
                Player2TeamIDMatchUp={Player2TeamIDMatchUp}
                matchID={props.matchID}
                player1TypeMatchUp={player1TypeMatchUp}
                player1filter={player1filter}

              />
            )}

            {openSelectionBowl && (
              <BowlerModal
                seriesID={props.seriesID}
                TeamBID={props.awayTeamID}
                TeamAID={props.homeTeamID}
                matchStatus={props.matchStatus}
                matchType={props.matchType}
                setOpenSelectionBowl={setOpenSelectionBowl}
                setScrollStop={setScrollStop}
                scrollStop={scrollStop}
                callScroll={callScroll}
                MatchUpsData={MatchUpsData}
                setPlayer2IDMatchUp={setPlayer2IDMatchUp}
                setPlayer2NameMatchUp={setPlayer2NameMatchUp}
                setPlayer2TeamIDMatchUp={setPlayer2TeamIDMatchUp}
                setPlayer2TypeMatchUp={setPlayer2TypeMatchUp}
                Player1IDMatchUp={Player1IDMatchUp}
                Player2IDMatchUp={Player2IDMatchUp}
                Player1NameMatchUp={Player1NameMatchUp}
                Player2NameMatchUp={Player2NameMatchUp}
                Player1TeamIDMatchUp={Player1TeamIDMatchUp}
                Player2TeamIDMatchUp={Player2TeamIDMatchUp}
                matchID={props.matchID}
                player2TypeMatchUp={player2TypeMatchUp}
                player2filter={player2filter}
              />
            )}
           
            
          </div> : <div className="w-100 br2 ba  b--white-20 ">
          <DataNotFound />
              </div>}
      </div>
    );
}