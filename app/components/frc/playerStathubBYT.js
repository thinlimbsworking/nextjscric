import React, { useEffect, useState } from 'react'
import { GET_SQUAD_FOR_BUILD_TEAM } from '../../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
const playerPlaceholder = '/pngs/fallbackprojection.png';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import Loading from '../loading';
import ImageWithFallback from '../commom/Image';
export default function PlayerStathubBYT({ listID, matchID, matchStatus, homeTeamID, awayTeamName, homeTeamName, credit, categoryPlayer, awayTeamID, ...props }) {

  let lang = props.language ? props.language : 'ENG'
  const [windows, setLocalStorage] = useState({});
  const [errMsg, seterrMsg] = useState('')
  const [isError, setisError] = useState(false);
  const [newToken, setToken] = useState('')
  const [playerList, setplayerList] = useState([...props.playerList])
  // console.log("playerListplayerList",playerList)
  const [totalLeftcredits, settotalLeftcredits] = useState(credit)
  const [catPlayers, setcatPlayers] = useState(categoryPlayer ? { ...categoryPlayer } : {
    WK: [],
    AR: [],
    BOWL: [],
    BAT: []
  })
  // console.log("playerList playerList", playerList)
  let activeCategory = {
    'ALL_ROUNDER': 'AR',
    'BOWLER': 'BOWL',
    'BATSMAN': 'BAT',
    'KEEPER': 'WK'
  }


  let config = { ...props.frcComposition_config };
  let matchConfig = { ...props.frcComposition_matchConfig }
  // console.log("matchConfig", matchConfig)
  const [errrorMsg, seterrorMsg] = useState('')
  const [teamList, setTeamList] = useState([...listID])

  const notSelctPlayer = ' p-1 my-2 flex  justify-start items-center     ba b--gray br2 white'
  const selctPlayer = '  p-1 my-2 flex  justify-start items-center    border border-green br2 text-white'
  useEffect(() => {
    if (global.window) {
      // setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem('tokenData'))
    }
  }, []);




  const { loading, error, data } = useQuery(GET_SQUAD_FOR_BUILD_TEAM, {
    variables: { matchID: matchID, token: newToken },

    onCompleted: (data) => {
      if (data && data.getSquadForBuildTeam) {
        //  console.log("data && data.getSquadForBuildTeam",data && data.getSquadForBuildTeam)
        let allRounder = data.getSquadForBuildTeam.all_rounder ? data.getSquadForBuildTeam.all_rounder : []
        let batsman = data.getSquadForBuildTeam.batsman ? data.getSquadForBuildTeam.batsman : []
        let bowler = data.getSquadForBuildTeam.bowler ? data.getSquadForBuildTeam.bowler : []
        let keeper = data.getSquadForBuildTeam.keeper ? data.getSquadForBuildTeam.keeper : []
        let totalPlayer = [...allRounder, ...batsman, ...bowler, ...keeper]
        // console.log("totalPlayer",totalPlayer)

        let SelectedPlayerList = totalPlayer.filter((pl) => listID.includes(pl.playerId))
        // console.log("SelectedPlayerList",[...SelectedPlayerList.filter(pl=>pl.player_role==='KEEPER').map(pl=>pl.playerId)])

        let categoryList = {
          WK: [...SelectedPlayerList.filter(pl => pl.player_role === 'KEEPER').map(pl => pl.playerId)],
          AR: [...SelectedPlayerList.filter(pl => pl.player_role === 'ALL_ROUNDER').map(pl => pl.playerId)],
          BOWL: [...SelectedPlayerList.filter(pl => pl.player_role === 'BOWLER').map(pl => pl.playerId)],
          BAT: [...SelectedPlayerList.filter(pl => pl.player_role === 'BATSMAN').map(pl => pl.playerId)]
        }

        // console.log("categoryList",categoryList)
        setcatPlayers({ ...catPlayers, ...categoryList })
        setplayerList([...SelectedPlayerList])

      }
    }
  });

  let tout = null;
  const hideMsg = () => {
    console.log(tout);
    if (tout) {
      clearTimeout(tout);
      tout = setTimeout(() => {
        seterrorMsg('');
      }, 3000);
    } else {
      tout = setTimeout(() => {
        seterrorMsg('');
      }, 3000);
    }
  };
  const ruleCheckNew = (player, PlayerCategory) => {
    
    if (playerList.length + 1 > 11) {
      // seterrorMsg(`You can select maximum of 11 players`);
      seterrorMsg(getLangText(lang, words, 'selectMaximumPlayersTwo'));
      hideMsg()
    }

    else if (
      catPlayers[PlayerCategory].length + 1 >
      matchConfig[`${PlayerCategory}_max`]
    ) {
      // seterrorMsg(
      //   `Select maximum   ${matchConfig[`${PlayerCategory}_max`]
      //   } ${config[PlayerCategory].fullName} only`);
      seterrorMsg(
        `${getLangText(lang, words, 'selectMaximum')}   ${matchConfig[`${PlayerCategory}_max`]
        } ${config[PlayerCategory].fullName} ${getLangText(lang, words, 'only')} `);

      hideMsg()

      return false;
    }
    else if (player.teamID === homeTeamID && playerList.filter(player => player.teamID === homeTeamID).length + 1 > 7) {
      // seterrorMsg(`Maximum 7 players from each team allowed`);
      seterrorMsg(getLangText(lang, words, 'selectMaximumPlayers'));
      hideMsg()
      return false;
    } else if (player.teamID === awayTeamID && playerList.filter(player => player.teamID === awayTeamID).length > 7) {
      // seterrorMsg(`Maximum 7 players from each team allowed`);
      seterrorMsg(getLangText(lang, words, 'selectMaximumPlayers'));
      hideMsg()
      return false;
    }
    else if (totalLeftcredits - player.credits < 0) {
      // seterrorMsg(`Total credits cannot exceed 100`);
      seterrorMsg(getLangText(lang, words, 'totalCredits'));
      hideMsg()
      return false;
    }

    else if (
      PlayerCategory == 'WK' &&
      catPlayers.WK.length >= matchConfig.WK_min &&
      11 - (playerList.length + 1) <
      (catPlayers.BAT.length >= matchConfig.BAT_min
        ? 0
        : matchConfig.BAT_min - catPlayers.BAT.length) +
      (catPlayers.AR.length >= matchConfig.AR_min
        ? 0
        : matchConfig.AR_min - catPlayers.AR.length) +
      (catPlayers.BOWL.length >= matchConfig.BOWL_min
        ? 0
        : matchConfig.BOWL_min - catPlayers.BOWL.length)
    ) {
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${catPlayers.BAT.length < matchConfig.BAT_min
          ? ` ${matchConfig.BAT_min} ${getLangText(lang, words, 'BATSMAN_RULE')}`
          : ''
        }${catPlayers.AR.length < matchConfig.AR_min
          ? ` ${matchConfig.AR_min} ${getLangText(lang, words, 'AR_RULE')}`
          : ''
        }${catPlayers.BOWL.length < matchConfig.BOWL_min
          ? ` ${matchConfig.BOWL_min} ${getLangText(lang, words, 'BOWLER_RULE')}`
          : ''
        } ${getLangText(lang, words, 'choose')}`,
      );
      hideMsg()
      return false;
    }
    else if (
      PlayerCategory == 'BAT' &&
      catPlayers.BAT.length >= matchConfig.BAT_min &&
      11 - (playerList.length + 1) <
      (catPlayers.WK.length >= matchConfig.WK_min
        ? 0
        : matchConfig.WK_min - catPlayers.WK.length) +
      (catPlayers.AR.length >= matchConfig.AR_min
        ? 0
        : matchConfig.AR_min - catPlayers.AR.length) +
      (catPlayers.BOWL.length >= matchConfig.BOWL_min
        ? 0
        : matchConfig.BOWL_min - catPlayers.BOWL.length)
    ) {
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${catPlayers.WK.length < matchConfig.WK_min
          ? ` ${matchConfig.WK_min} ${getLangText(lang, words, 'KEEPER_RULE')}`
          : ''
        }${catPlayers.AR.length < matchConfig.AR_min
          ? ` ${matchConfig.AR_min} ${getLangText(lang, words, 'AR_RULE')}`
          : ''
        }${catPlayers.BOWL.length < matchConfig.BOWL_min
          ? ` ${matchConfig.BOWL_min} ${getLangText(lang, words, 'BOWLER_RULE')}`
          : ''
        } ${getLangText(lang, words, 'choose')}`,
      );
      hideMsg()
      return false;
    }
    else if (
      PlayerCategory == 'AR' &&
      catPlayers.AR.length >= matchConfig.AR_min &&
      11 - (playerList.length + 1) <
      (catPlayers.BAT.length >= matchConfig.BAT_min
        ? 0
        : matchConfig.BAT_min - catPlayers.BAT.length) +
      (catPlayers.WK.length >= matchConfig.WK_min
        ? 0
        : matchConfig.WK_min - catPlayers.WK.length) +
      (catPlayers.BOWL.length >= matchConfig.BOWL_min
        ? 0
        : matchConfig.BOWL_min - catPlayers.BOWL.length)
    ) {
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${catPlayers.BAT.length < matchConfig.BAT_min
          ? ` ${matchConfig.BAT_min} ${getLangText(lang, words, 'BATSMAN_RULE')}`
          : ''
        }${catPlayers.WK.length < matchConfig.WK_min
          ? ` ${matchConfig.WK_min} ${getLangText(lang, words, 'KEEPER_RULE')}`
          : ''
        }${catPlayers.BOWL.length < matchConfig.BOWL_min
          ? ` ${matchConfig.BOWL_min} ${getLangText(lang, words, 'BOWLER_RULE')}`
          : ''
        } ${getLangText(lang, words, 'choose')}`,
      );
      hideMsg()
      return false;
    }
    else if (
      PlayerCategory == 'BOWL' &&
      catPlayers.BOWL.length >= matchConfig.BOWL_min &&
      11 - (playerList.length + 1) <
      (catPlayers.BAT.length >= matchConfig.BAT_min
        ? 0
        : matchConfig.BAT_min - catPlayers.BAT.length) +
      (catPlayers.AR.length >= matchConfig.AR_min
        ? 0
        : matchConfig.AR_min - catPlayers.AR.length) +
      (catPlayers.WK.length >= matchConfig.WK_min
        ? 0
        : matchConfig.WK_min - catPlayers.WK.length)
    ) {
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${catPlayers.BAT.length < matchConfig.BAT_min
          ? ` ${matchConfig.BAT_min} ${getLangText(lang, words, 'BATSMAN_RULE')}`
          : ''
        }${catPlayers.AR.length < matchConfig.AR_min
          ? ` ${matchConfig.AR_min} ${getLangText(lang, words, 'AR_RULE')}`
          : ''
        }${catPlayers.WK.length < matchConfig.WK_min
          ? ` ${matchConfig.WK_min} ${getLangText(lang, words, 'KEEPER_RULE')}`
          : ''
        } ${getLangText(lang, words, 'choose')}`,
      );
      hideMsg()
      return false;
    }
    else return true; 
  };
  const handlePlayerSelection = (currentPlayer, operation) => {

    let PlayerCategory = activeCategory[currentPlayer.player_role]
    // console.log("PlayerCategory",PlayerCategory,)
    if (operation === 'add') {

      let checkRules = ruleCheckNew(currentPlayer, PlayerCategory)

      //     let playerDetail=Players.length>0? [...Players]:[]   
      //   let addPlayer= playerDetail.push({...currentPlayer})
      if (checkRules) {
        setplayerList([...playerList, currentPlayer])
        settotalLeftcredits(prev => prev - Number(currentPlayer.credits))
        //   setPlayersList([...PlayersList])
        setcatPlayers({
          ...catPlayers,
          [PlayerCategory]: [...catPlayers[PlayerCategory], currentPlayer.playerId]
        })
        setTeamList([...teamList, currentPlayer.playerId])
      }


    } else {
      // console.log("jhjjkj")
      let filterPlayer = [...playerList]
      let deletePlayer = filterPlayer.filter(player => player.playerId !== currentPlayer.playerId)
      setplayerList([...deletePlayer])
      if (props.captain == currentPlayer.playerId) {
        props.setcaptain(null)
      }

      if (props.viceCaptain == currentPlayer.playerId) {
        props.setviceCaptain(null)
      }
      setTeamList([...teamList.filter(pl => pl !== currentPlayer.playerId)])
      settotalLeftcredits(prev => prev + Number(currentPlayer.credits))
      setcatPlayers({
        ...catPlayers,
        [PlayerCategory]: [
          ...catPlayers[PlayerCategory].filter(pl => pl !== currentPlayer.playerId),
        ],
      });
    }
  }

  if (loading) return <Loading />;
  else if (error)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  else
    return (
      data && data.getSquadForBuildTeam ? <div className="text-black dark:text-white relative mb-8 px-3 md:px-0 lg:px-0">



        <div className=' dark:border-none bg-gray-4 rounded-lg mt-5'>
          <div className='text-sm text-white font-semibold p-3'>{getLangText(lang, words, 'WICKET_KEEPERS')} </div>
          <div className='border dark:border-none flex flex-wrap justify-around items-center bg-white dark:bg-gray rounded-b-lg px-2 pb-4'>
            {data &&
              data.getSquadForBuildTeam &&
              data.getSquadForBuildTeam.keeper &&
              data.getSquadForBuildTeam.keeper.map((item, index) => {
                return (
                  <div
                    key={index}
                    onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}
                    className={`cursor-pointer pt-3  flex  justify-center items-center w-1/3`}>
                    <div className="flex items-center justify-center flex-col text-center">
                      <div className='relative bg-light_gray dark:bg-gray-8 w-20 h-20 rounded-full'>
                        <div
                          className={`overflow-hidden w-full h-full rounded-full  ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? 'border-2 border-green' : ''
                            }`}>
                          <ImageWithFallback height={48} width={48} loading='lazy'
                            fallbackSrc={playerPlaceholder}
                            className='object-top object-contain w-20  '
                            src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                            alt=''
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                          />
                        </div>
                        <div className='absolute bottom-0 right-0 border rounded border-white'>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                            fallbackSrc={flagPlaceHolder}
                            className='w-6 h-4 rounded'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            alt=''
                          />
                        </div>
                      </div>
                      <div className=' flex justify-center flex-col items-center pt-3'>
                        <div className='text-xs font-semibold'>
                          {' '}
                          {lang === 'HIN' ? item.playerNameHindi : item.playerName}{' '}
                        </div>
                        <div className='mt-1 text-xs text-gray-2 font-medium'>
                          {' '}
                          {getLangText(lang, words, 'Credits')}:{' '}
                          <span className='text-green font-semibold'>{item.credits}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
        </div>


        <div className=' dark:border-none bg-gray-4 rounded-lg mt-5'>
          <div className='text-sm text-white font-semibold p-3'>{getLangText(lang, words, 'BATTERS')} </div>
          <div className='border dark:border-none flex flex-wrap justify-around items-center bg-white dark:bg-gray rounded-b-lg px-2 pb-4'>
            {data &&
              data.getSquadForBuildTeam &&
              data.getSquadForBuildTeam.batsman &&
              data.getSquadForBuildTeam.batsman.map((item, index) => {
                return (
                  <div
                    key={index}
                    onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}

                    className={`cursor-pointer pt-3  flex  justify-center items-center w-1/3`}>
                    <div className="flex items-center justify-center flex-col text-center">
                      <div className='relative bg-light_gray dark:bg-gray-8 w-20 h-20 rounded-full'>
                        <div
                          className={`overflow-hidden w-full h-full rounded-full  ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? 'border-2 border-green' : ''
                            }`}>
                          <ImageWithFallback height={48} width={48} loading='lazy'
                            fallbackSrc={playerPlaceholder}
                            className='object-top object-contain w-20  '
                            src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                            alt=''
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                          />
                        </div>
                        <div className='absolute bottom-0 right-0 border rounded border-white'>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                            fallbackSrc={flagPlaceHolder}
                            className='w-6 h-4 rounded'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            alt=''
                          />
                        </div>
                      </div>
                      <div className=' flex justify-center flex-col items-center pt-3'>
                        <div className='text-xs font-semibold'>
                          {' '}
                          {lang === 'HIN' ? item.playerNameHindi : item.playerName}{' '}
                        </div>
                        <div className='mt-1 text-xs text-gray-2 font-medium'>
                          {' '}
                          {getLangText(lang, words, 'Credits')}:{' '}
                          <span className='text-green font-semibold'>{item.credits}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
        </div>



        <div className=' dark:border-none bg-gray-4 rounded-lg mt-5'>
          <div className='text-sm text-white font-semibold p-3'>{getLangText(lang, words, 'ALL_ROUNDERS')} </div>
          <div className='border dark:border-none flex flex-wrap justify-around items-center bg-white dark:bg-gray rounded-b-lg px-2 pb-4'>
            {data &&
              data.getSquadForBuildTeam &&
              data.getSquadForBuildTeam.all_rounder &&
              data.getSquadForBuildTeam.all_rounder.map((item, index) => {
                return (
                  <div
                    key={index}
                    onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}

                    className={`cursor-pointer pt-3  flex  justify-center items-center w-1/3`}>
                    <div className="flex items-center justify-center flex-col text-center">
                      <div className='relative bg-light_gray dark:bg-gray-8 w-20 h-20 rounded-full'>
                        <div
                          className={`overflow-hidden w-full h-full rounded-full  ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? 'border-2 border-green' : ''
                            }`}>
                          <ImageWithFallback height={48} width={48} loading='lazy'
                            fallbackSrc={playerPlaceholder}
                            className='object-top object-contain w-20  '
                            src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                            alt=''
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                          />
                        </div>
                        <div className='absolute bottom-0 right-0 border rounded border-white'>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                            fallbackSrc={flagPlaceHolder}
                            className='w-6 h-4 rounded'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            alt=''
                          />
                        </div>
                      </div>
                      <div className=' flex justify-center flex-col items-center pt-3'>
                        <div className='text-xs font-semibold'>
                          {' '}
                          {lang === 'HIN' ? item.playerNameHindi : item.playerName}{' '}
                        </div>
                        <div className='mt-1 text-xs text-gray-2 font-medium'>
                          {' '}
                          {getLangText(lang, words, 'Credits')}:{' '}
                          <span className='text-green font-semibold'>{item.credits}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
        </div>


        <div className=' dark:border-none bg-gray-4 rounded-lg mt-5'>
          <div className='text-sm text-white font-semibold p-3'>{getLangText(lang, words, 'BOWLERS')} </div>
          <div className='border dark:border-none flex flex-wrap justify-around items-center bg-white dark:bg-gray rounded-b-lg px-2 pb-4'>
            {data &&
              data.getSquadForBuildTeam &&
              data.getSquadForBuildTeam.bowler &&
              data.getSquadForBuildTeam.bowler.map((item, index) => {
                return (
                  <div
                    key={index}
                    onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}

                    className={`cursor-pointer pt-3  flex  justify-center items-center w-1/3`}>
                    <div className="flex items-center justify-center flex-col text-center">
                      <div className='relative bg-light_gray dark:bg-gray-8 w-20 h-20 rounded-full'>
                        <div
                          className={`overflow-hidden w-full h-full rounded-full  ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? 'border-2 border-green' : ''
                            }`}>
                          <ImageWithFallback height={48} width={48} loading='lazy'
                            fallbackSrc={playerPlaceholder}
                            className='object-top object-contain w-20  '
                            src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                            alt=''
                          // onError={(e) => (e.target.src = playerPlaceholder)}
                          />
                        </div>
                        <div className='absolute bottom-0 right-0 border rounded border-white'>
                          <ImageWithFallback height={18} width={18} loading='lazy'
                            fallbackSrc={flagPlaceHolder}
                            className='w-6 h-4 rounded'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            alt=''
                          />
                        </div>
                      </div>
                      <div className=' flex justify-center flex-col items-center pt-3'>
                        <div className='text-xs font-semibold'>
                          {' '}
                          {lang === 'HIN' ? item.playerNameHindi : item.playerName}{' '}
                        </div>
                        <div className='mt-1 text-xs text-gray-2 font-medium'>
                          {' '}
                          {getLangText(lang, words, 'Credits')}:{' '}
                          <span className='text-green font-semibold'>{item.credits}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
        </div>






        {false && <div className='bg-gray-4 rounded-xl'>
          {<div className="flex items-center mt2 justify-between">
            <div className="text-sm font-semibold p-3 uppercase" >{getLangText(lang, words, 'WICKET_KEEPERS')}
              <span className="pl2 oswald yellow-frc">{catPlayers.WK.length}</span></div>
            {lang === "HIN" ?
              <div className="fw4 f7 gray">{matchConfig.WK_min + "-" + matchConfig.WK_max + " " + "चुनें"}</div> :

              <div className="fw4 f7 gray">{"SELECT" + " " + matchConfig.WK_min + "-" + matchConfig.WK_max}</div>}</div>}


          <div className="   flex  flex-wrap justify-around items-center bg-gray rounded-xl px-2 pb-4">
            {data.getSquadForBuildTeam && data.getSquadForBuildTeam.keeper && data.getSquadForBuildTeam.keeper.map((item, index) => {
              return (
                <div key={index} onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}
                  className={`cursor-pointer ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? selctPlayer : notSelctPlayer}`}>

                  <div className="relative flex justify-start items-center">
                    <div className="overflow-hidden br-100 b--white-20 ba w2 h2 w2-3-l h2-3-l bg-mid-gray">
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc={playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" />
                    </div>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                      fallbackSrc={flagPlaceHolder} className="absolute bottom-0 right-0 top-1 mt2-l left-0 bg-gray mt1 h07 br-100 w07 ba b--black-20 object-cover" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                  </div>
                  <div className=" f8 f7-l justify-start pl1 items-center">
                    <div className="mr1 oswald"> {lang === "HIN" ? item.playerNameHindi : item.playerName}  </div>
                    <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang, words, 'Credits')}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                  </div>
                </div>
              )
            }
            )}

          </div>
        </div>}
        {false && <>
          {<div className="flex items-center mt2 justify-between"><div className="f6 fw5 ttu" ><span>{getLangText(lang, words, 'BATTERS')} </span> <span className="pl2 oswald yellow-frc">{catPlayers.BAT.length}</span></div>
            {lang === "HIN" ?
              <div className="fw4 f7 gray">{matchConfig.BAT_min + "-" + matchConfig.BAT_max + " " + "चुनें"}</div>
              : <div className="fw4 f7 gray">{"SELECT" + " " + matchConfig.BAT_min + "-" + matchConfig.BAT_max}</div>}</div>
          }
          <div className="w-100 flex flex-wrap justify-around items-center">
            {data && data.getSquadForBuildTeam && data.getSquadForBuildTeam.batsman && data.getSquadForBuildTeam.batsman.map((item, index) => {
              return (
                <div key={index} onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}
                  className={`cursor-pointer ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? selctPlayer : notSelctPlayer}`}>

                  <div className="relative flex justify-start items-center">
                    <div className="overflow-hidden br-100 b--white-20 ba w2 h2 w2-3-l h2-3-l bg-mid-gray">
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc={playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" />
                    </div>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                      fallbackSrc={flagPlaceHolder} className="absolute bottom-0 mt1 mt2-l top-1 left-0 h07 br-100 bg-gray w07 ba b--black-20 object-cover" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                  </div>
                  <div className=" f8 f7-l justify-start pl1 items-center">
                    <div className="mr1 oswald">{lang === "HIN" ? item.playerNameHindi : item.playerName}   </div>
                    <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang, words, 'Credits')}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                  </div>
                </div>
              )
            }
            )}

          </div>
        </>}
        {false && <>
          {<div className="flex items-center mt2 justify-between"> <div className="f6 fw5 ttu" >{getLangText(lang, words, 'ALL_ROUNDERS')}<span className="pl2 oswald yellow-frc">{catPlayers.AR.length}</span>  </div>
            {lang === "HIN" ?
              <div className="fw4 f7 gray">{matchConfig.AR_min + "-" + matchConfig.AR_max + " " + "चुनें"}</div>
              : <div className="fw4 f7 gray">{"SELECT" + " " + matchConfig.AR_min + "-" + matchConfig.AR_max}</div>}</div>
          }
          {/* <div className="mt2 f6 fw5 ttu" >ALL Rounders <span className="pl2 oswald yellow-frc">{catPlayers.AR.length}</span>  </div> */}

          <div className="w-100  flex flex-wrap justify-around   items-center">
            {data.getSquadForBuildTeam && data.getSquadForBuildTeam.all_rounder && data.getSquadForBuildTeam.all_rounder.map((item, index) => {
              return (
                <div key={index} onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}
                  className={`cursor-pointer ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? selctPlayer : notSelctPlayer}`}>
                  <div className="relative flex justify-start items-center">
                    <div className="overflow-hidden br-100 b--white-20 ba w2 h2 w2-3-l h2-3-l bg-mid-gray">
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc={playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" />
                    </div>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                      fallbackSrc={flagPlaceHolder} className="absolute bottom-0 right-0 top-1 mt1 mt2-l bg-gray left-0 h07 br-100 w07 ba b--black-20 object-cover" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                  </div>
                  <div className=" f8 f7-l justify-start pl1 items-center">
                    <div className="mr1 oswald">{lang === "HIN" ? item.playerNameHindi : item.playerName}   </div>
                    <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang, words, 'Credits')}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                  </div>
                </div>
              )
            }
            )}

          </div>
        </>}

        {false && <>
          {<div className="flex items-center mt2 justify-between"> <div className="mt2 f6 fw5 ttu" >{getLangText(lang, words, 'BOWLERS')} <span className="pl2 oswald yellow-frc">{catPlayers.BOWL.length}</span> </div>
            {lang === "HIN" ?
              <div className="fw4 f7 gray">{matchConfig.BOWL_min + "-" + matchConfig.BOWL_max + " " + "चुनें"}</div>
              : <div className="fw4 f7 gray">{"SELECT" + " " + matchConfig.BOWL_min + "-" + matchConfig.BOWL_max}</div>}
          </div>
          }
          {/* <div className="mt2 f6 fw5 ttu" >Bowlers <span className="pl2 oswald yellow-frc">{catPlayers.BOWL.length}</span> </div> */}
          <div className="w-100  flex flex-wrap justify-around   items-center">

            {data.getSquadForBuildTeam && data.getSquadForBuildTeam.bowler && data.getSquadForBuildTeam.bowler.map((item, index) => {
              return (
                <div key={index} onClick={() => { handlePlayerSelection(item, playerList.filter(player => player.playerId == item.playerId).length == 0 ? 'add' : 'delete') }}
                  className={`cursor-pointer ${playerList.filter((x) => x.playerId === item.playerId).length > 0 ? selctPlayer : notSelctPlayer}`}>

                  <div className="relative flex justify-start items-center">
                    <div className="overflow-hidden br-100 b--white-20 ba w2 h2 w2-3-l h2-3-l bg-mid-gray">
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc={playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" />
                    </div>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                      fallbackSrc={flagPlaceHolder} className="absolute bottom-0 mt1 mt2-l right-0 top-1 left-0 bg-gray h07 br-100 w07 ba b--black-20 object-cover" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                  </div>
                  <div className=" f8 f7-l justify-start pl1 items-center">
                    <div className="mr1 oswald">{lang === "HIN" ? item.playerNameHindi : item.playerName}   </div>
                    <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang, words, 'Credits')}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                  </div>
                </div>
              )
            }
            )}

          </div>
        </>}
        <div className=" h2-l"></div>
        <div className=" bottom-0 fixed bg-gray-4  z-40 left-0  right-0 lg:hidden md:hidden">
          {errrorMsg !== '' && (
            <div className="flex w-full">
              <div className="bg-frc-yellow p-2 flex justify-center items-center pa2 w-full ">
                <div className="text-sm font-bold text-green   flex justify-center items-center ">
                
                 
                </div>
                <div className="text-green pl-2 font-semibold"> !{errrorMsg}</div>
              </div>
            </div>
          )}
          <div className="  rounded-t-xl border-t  flex items-center">
            <div className="w-full pb-3 pt-1 px-3 flex justify-between items-center">
              <div className="  flex items-center justify-between w-4/6">
                <div className=" bg- p-2 rounded-md text-lg font-semibold justify-center  flex items-center flex-col h-[60px]">
                  {' '}
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/pngsv2/flag_dark.png"
                    className="w-8 h-5 mb-1"
                    src={`https://images.cricket.com/teams/${homeTeamID}_flag_safari.png`}
                    alt=""
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                  />{' '}
                  <span>
                    {
                      playerList.filter((x) => x.teamName === homeTeamName).length
                    }
                  </span>
                </div>
                <div className=" flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-lg font-semibold  h-[60px]">
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/pngsv2/flag_dark.png"
                    className="w-8 h-5 mb-1"
                    src={`https://images.cricket.com/teams/${awayTeamID}_flag_safari.png`}
                    alt=""
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                  />{' '}
                  <span>
                    {
                      playerList.filter((x) => x.teamName === awayTeamName).length
                    }
                  </span>
                </div>

                <div className=" flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-sm font-semibold  h-[60px] ">
                  <span className="text-xs font-medium text-gray-2">
                    {getLangText(lang, words, 'Players')}
                  </span>
                  <div className="text-green">
                    <span className="text-base font-semibold">
                      {playerList.length}
                    </span>
                    <span className="text-md font-medium">/11</span>
                  </div>
                </div>
                <div className=" flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-sm font-semibold  h-[60px]">
                  <span className="text-xs font-medium text-gray-2 text-center">
                    {getLangText(lang, words, 'Credits')} <br></br> Left
                  </span>

                  <span className="text-base font-semibold text-green">
                    {totalLeftcredits}
                  </span>
                </div>
              </div>
              <div className="flex items-center  center justify-end w-1/3 uppercase text-sm font-semibold">
                <div
                  onClick={() => {
                    playerList.length === 11 ? (
                      props.changeComponentState('chooseCaptains'),
                      props.playerScores(playerList, totalLeftcredits, catPlayers, teamList, 'chooseCaptains', props.recentplayer)) : (seterrorMsg("Please select 11 players to proceed"), hideMsg())
                  }}
                  className={
                    playerList.length === 11
                      ? ' border-green border-2 rounded-lg bg-gray-8 tc px-3 py-1 text-green  '
                      : ' border-gray-2 border-2 rounded-lg bg-gray-8 tc px-3 py-1 text-gray-2 '
                  }
                >
                  {getLangText(lang, words, 'PROCEED')}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="hidden lg:block md:block sticky bottom-1 w-full">
          {errrorMsg !== '' && (
            <div className='flex'>
              <div className=' bg-white dark:bg-gray-4 w-full text-center text-basered  dark:text-green py-2 text-sm font-bold justify-center items-center pa2 w-100 mw75-l center'>
                <div className='ba br-100 bw1 pa2 h2 w2 white flex justify-center items-center fw5'></div>
                <div className="pl2 f6 fw5"> !  {errrorMsg}</div>
              </div>
            </div>
          )}
          <div>
            <div className="w-full rounded-md border   bg-white flex justify-between items-center p-3">
              <div className="flex gap-3 items-center px-4">
                <div className="w-24 px-4 py-2 bg-light_gray flex gap-2 items-center rounded-md">
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/pngsv2/flag_dark.png"
                    className="w-12 h-5 "
                    src={`https://images.cricket.com/teams/${homeTeamID}_flag_safari.png`}
                    alt=""
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                  />
                  <span>
                    {
                      playerList.filter((x) => x.teamName === homeTeamName).length
                    }
                  </span>

                </div>

                <div className=" px-4 py-2 bg-light_gray flex gap-2 items-center rounded-md">
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/pngsv2/flag_dark.png"
                    className="w-12 h-5 "
                    src={`https://images.cricket.com/teams/${awayTeamID}_flag_safari.png`}
                    alt=""
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                  />{' '}
                  <span>
                    {playerList.filter((x) => x.teamName === awayTeamName).length}
                  </span>
                </div>

                <div className="px-4 py-2 bg-light_gray flex gap-2 items-center rounded-md text-xs">
                  {/* {getLangText(lang, words, 'TOTAL')} <br /> {getLangText(lang, words, 'SELECTED')} <br />{' '} */}
                  {getLangText(lang, words, 'Players')}
                  <span className="text-green text-base font-semibold flex">
                    {playerList.length}
                    {'/11'}
                  </span>
                </div>

                <div className="w-48 px-4 py-2 bg-light_gray flex gap-2 items-center justify-center rounded-md text-xs">
                  {/* {getLangText(lang, words, 'TOTAL')} <br /> {getLangText(lang, words, 'SELECTED')} <br />{' '} */}
                  {/* {getLangText(lang, words, 'Credits')} */}
                  <span className="flex ">{getLangText(lang, words, 'Credits')} Left</span>
                  <span className="text-green text-base font-semibold flex">
                    {totalLeftcredits}
                  </span>
                </div>

              </div>
              <div className='flex items-center w-full justify-end px-3  cursor-pointer'>
                <div
                  onClick={() => {
                    playerList.length === 11 ? (
                      props.changeComponentState('chooseCaptains'),
                      props.playerScores(playerList, totalLeftcredits, catPlayers, teamList, 'chooseCaptains', props.recentplayer)) : (seterrorMsg("Please select 11 players to proceed"), hideMsg())
                  }}
                  className={
                    playerList.length === 11
                      ? 'w-24 bg-basered  rounded-md border-solid dark:bg-green text-center flex item-center justify-center text-white p-2 uppercase font-mmedium'
                      : 'w-24  border-2 border-gray rounded-md border-solid bg-dark-gray text-center flex item-center justify-center dark:text-white p-2 uppercase text-sm font-medium'
                  }>
                  {getLangText(lang, words, 'PROCEED')}
                </div>
              </div>
            </div>
          </div>
          {/* <div className="  rounded-xl   center  shadow-md h-28  flex items-center">
            <div className="w-full ">
              <div className=" flex-wrap flex justify-around items-center f1 yellow-frc  fw6 ">
                <div className="w-2/12 flex flex-col justify-center items-center  text-xs  oswald" > <img className="w2-3 h15 mr1" src={`https://images.cricket.com/teams/${homeTeamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)} /> {playerList.filter((x) => x.teamName === homeTeamName).length}</div>
                <div className="w-2/12 flex flex-col  justify-center items-center  text-xs oswald"><img className="w2-3 h15 mr1" src={`https://images.cricket.com/teams/${awayTeamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)} />  {playerList.filter((x) => x.teamName === awayTeamName).length}</div>
                <div className="w-2/12 flex flex-col  justify-center items-center white oswald text-xs ">{getLangText(lang, words, 'TOTAL')}  <br />  {getLangText(lang, words, 'SELECTED')} <br />  {getLangText(lang, words, 'Players')}<span className="yellow-frc oswald pl1 ">{playerList.length} </span>  </div>
                <div className="w-2/12 flex flex-col  justify-center items-center white oswald text-xs ">{lang === 'HIN' ? 'बचा हुआ' : 'Credit'}  <br /> {lang === 'HIN' ? 'क्रेडिट' : 'Left'}<span className="yellow-frc oswald pl1 text-xs">{totalLeftcredits} </span>  </div>
                <div
                  className="w-2/12 flex  flex-col  justify-center items-center white oswald text-xs"
                  onClick={() => {
                    playerList.length === 11 ? (
                      props.changeComponentState('chooseCaptains'),
                      props.playerScores(playerList, totalLeftcredits, catPlayers, teamList, 'chooseCaptains', props.recentplayer)) : (seterrorMsg("Please select 11 players to proceed"), hideMsg())
                  }}
                  className={
                    (playerList.length === 11)
                      ? 'w-30  ba b--gray bg-green tc flex item-center justify-center white pa2 ttu f6 fw5 br2'
                      : 'w-30  ba b--gray bg-dark-gray tc flex item-center justify-center white pa2 ttu f6 fw5 br2'
                  }>
                  {getLangText(lang, words, 'PROCEED')}
                </div>
              </div>

            </div>
          </div> */}
        </div>
        <div className="hidden">
          {errrorMsg && (
            <div className='flex'>
              <div className='bg-frc-yellow f6 white fw1 flex justify-center items-center pa2 w-100 mw75-l center'>
                <div className='ba br-100 bw1 pa2 h2 w2 white flex justify-center items-center fw5'> ! </div>
                <div className="pl2 f6 fw5">{errrorMsg}</div>
              </div>
            </div>
          )}



          <div className=" bg-black-80 w-100 mw75-l center  shadow-4 h37  flex items-center">
            <div className="w-100 ">
              <div className=" flex-wrap flex justify-around items-center f1 yellow-frc  fw6 ">
                <div className="w-25 flex justify-center items-center f2 oswald" > <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc={playerPlaceholder} className="w2-3 h15 mr1" src={`https://images.cricket.com/teams/${homeTeamID}_flag_safari.png`} alt="" /> {playerList.filter((x) => x.teamName === homeTeamName).length}</div>
                <div className="w-25 flex justify-center items-center f2 oswald"><ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc={flagPlaceHolder} className="w2-3 h15 mr1" src={`https://images.cricket.com/teams/${awayTeamID}_flag_safari.png`} alt="" />  {playerList.filter((x) => x.teamName === awayTeamName).length}</div>
                <div className="w-25 flex justify-center items-center white oswald f8 f7-l ttu tr ">{getLangText(lang, words, 'TOTAL')}  <br />  {getLangText(lang, words, 'SELECTED')} <br />  {getLangText(lang, words, 'Players')}  <span className="yellow-frc oswald pl1 f2">{playerList.length} </span>  </div>
                <div className="w-25 flex justify-center items-center white oswald f8 f7-l ttu tr "> {lang === 'HIN' ? 'बचा हुआ' : 'Credit'}  <br /> {lang === 'HIN' ? 'क्रेडिट' : 'Left'}<span className="yellow-frc oswald pl1 f2">{totalLeftcredits} </span>  </div>

              </div>
              <div className='flex items-center  center w-100 justify-center mt2 cursor-pointer'>
                <div
                  onClick={() => {
                    playerList.length === 11 ? (
                      props.changeComponentState('chooseCaptains'),
                      props.playerScores(playerList, totalLeftcredits, catPlayers, teamList, 'chooseCaptains', props.recentplayer)) : (seterrorMsg("Please select 11 players to proceed"), hideMsg())
                  }}
                  className={
                    playerList.length === 11
                      ? 'w-30  ba b--gray bg-green tc flex item-center justify-center white pa2 ttu f6 fw5 br2'
                      : 'w-30  ba b--gray bg-dark-gray tc flex item-center justify-center white pa2 ttu f6 fw5 br2'
                  }>
                  {getLangText(lang, words, 'PROCEED')}
                </div>
              </div>
            </div>
          </div>
        </div>

      </div> : <div></div>
    )
}
