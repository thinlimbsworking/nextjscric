import React, { useState } from 'react'

import { usePathname, useRouter } from 'next/navigation'
import { useQuery, useMutation } from '@apollo/react-hooks'
import ImageWithFallback from '../commom/Image'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const allRounder = '/pngs/allRounder-white-solid.png'
const ball = '/pngs/ball-white-solid.png'
const bat = '/pngs/bat-white-solid.png'
const wicket = '/pngs/wicketKeeper-white-solid.png'
import { getLangText } from '../../api/services'
import { words } from './../../constant/language'
import { CREATE_AND_UPDATE_USER_TEAM } from '../../api/queries'
const saveTeam = '/svgs/saveTeam.svg'
import { getStatshubFrcUrl } from '../../api/services'
import Link from 'next/link'
import Heading from '../commom/heading'
const playerPlaceholder = '/pngs/fallbackprojection.png'
//playerList,  ffCode={ffCode} viceCaptain={viceCaptain} captain={captain} teamName={teamName}
export default function playerStatsChooseCaptains({
  matchID,
  playerScores,
  changeComponentState,
  categoryPlayer,
  ...props
}) {
  let lang = props.language ? props.language : 'ENG'
  let router = useRouter()
  const query = usePathname()
  // console.log("matchID captainvc",matchID)
  // console.log("matchID playerScores",playerScores)
  // console.log("matchID changeComponentState",changeComponentState)
  // console.log("matchID categoryPlayer",categoryPlayer)

  const [captain, setcaptain] = useState(props.captain ? props.captain : null)
  const [vice_captain, setvice_captain] = useState(
    props.viceCaptain ? props.viceCaptain : null,
  )
  const [Submit, setSubmit] = useState(false)
  // console.log("captain",captain)
  // console.log("vice capt",vice_captain)
  const [teamName, setTeamName] = useState(props.teamName ? props.teamName : '')
  const [playerList, setplayerList] = useState([...props.playerList])
  // console.log("playerList",playerList)
  const checkCaptains = (id, type) => {
    if (type === 'captain') {
      if (id !== vice_captain) {
        setcaptain(id)
      }
    }
    if (type === 'vice_captain') {
      if (id !== captain) {
        setvice_captain(id)
      }
    }
  }
  const [, , matchId, matchSlug, pageCategory, stahubCategory] = query.split('/')
  // console.log("router.query.slugs",router.query.slugs) ;
  console.log('matchidddd -- ', matchId);
  const [createUserTeam] = useMutation(CREATE_AND_UPDATE_USER_TEAM, {
    onCompleted: (res) => {
      
  console.log('res after saved team -- ',res);
    }
  })

  const updateTeamDetails = async () => {
    let finalList = playerList.map((player) => {
      return {
        ...player,
        captain: player.playerId === captain ? '1' : '0',
        vice_captain: player.playerId === vice_captain ? '1' : '0',
      }
    })
    // console.log("finalList",finalList)
    await createUserTeam({
      variables: {
        matchID: matchID,
        token: global.window.localStorage.getItem('tokenData'),
        team: {
          teamName: props.teamName ? props.teamName : teamName,
          leagueType: 'statsHub',
          selectCriteria: 'projected_points',
          ffCode: props.ffCode ? props.ffCode : '',
          team: finalList,
          isAlogo11: false,
        },
      },
    })
    //api call here
    setSubmit(false)
  }

  return (
    <div className="w-full  text-black dark:text-white px-3 md:p-0 lg:p-0">
      <div className='w-full '>
        <Heading heading={lang === 'HIN'
          ? 'अपना कप्तान और उप कप्तान चुनें'
          : 'CHOOSE YOUR CAPTAIN AND VICE CAPTAIN'} />
      </div>
      <div className="w-full mt-3 bg-white dark:bg-transparent">
        <div className="mt-3">
          {playerList
            .filter((pl) => pl.player_role === 'KEEPER')
            .map((player, i) => (
              <div
                key={i}
                className=" py-2  flex items-center justify-between  border-b mt-2 "
              >
                <div className="flex items-center w-2/3 ">
                  <div className="  flex items-center justify-end ml-2">
                    <div className=" relative overflow-hidden    ">
                      <ImageWithFallback
                        height={38}
                        width={38}
                        loading="lazy"
                        fallbackSrc={playerPlaceholder}
                        className="rounded-full h-16 w-16  
                            object-cover object-top"
                        src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                        alt=""
                      />
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5    bg-gray-4  "
                        style={{ left: 2 }}
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                      // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      />
                    </div>
                  </div>
                  <div className="pl-2 w-70 w-50-l justify-start">
                    <div className="fw6 f8 f6-l f6-m">
                      {lang === 'HIN'
                        ? player.playerNameHindi
                        : player.playerName}
                    </div>
                    <div className="mt-1 p-1 bg-gray-4 rounded-md w-6 h-6">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-4 h-4"
                        src={wicket}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                <div className='w-1/3 flex px-2 gap-6  justify-end  '>
                  <div className="  flex items-center justify-center cursor-pointer">
                    <div
                      className={`flex items-center h-7 w-7 p-1 rounded-full bg-gray-4 text-white  ${player.playerId === captain
                        ? 'bg-green text-white border  '
                        : 'border border-white'
                        }  justify-center text-sm  `}
                      onClick={() => {
                        checkCaptains(player.playerId, 'captain')
                      }}
                    >
                      C
                    </div>
                  </div>
                  <div className=" flex items-center justify-center cursor-pointer">
                    <div
                      className={`flex items-center  h-7 w-7 p-1 rounded-full bg-gray-4 text-white ${player.playerId === vice_captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                        } 
                      justify-center text-sm`}
                      onClick={() => {
                        checkCaptains(player.playerId, 'vice_captain')
                      }}
                    >
                      VC
                    </div>
                  </div>
                </div>
                <div></div>
              </div>
            ))}
        </div>


        {playerList
          .filter((pl) => pl.player_role === 'BATSMAN')
          .map((player, i) => (
            <div
              key={i}
              className="py-2  flex items-center justify-between  border-b mt-2"
              mt-2
            >
              <div className="flex items-center w-2/3">
                <div className=" flex items-center justify-end ml-2">
                  <div className=" relative overflow-hidden    ">
                    <ImageWithFallback
                      height={38}
                      width={38}
                      loading="lazy"
                      fallbackSrc={playerPlaceholder}
                      className="rounded-full h-16 w-16 h3-l w3-l h3-m w3-m    object-cover object-top"
                      src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                      alt=""
                    />
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      fallbackSrc={flagPlaceHolder}
                      className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5    bg-gray-4 "
                      style={{ left: 2 }}
                      src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                      alt=""
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    />
                  </div>
                </div>
                <div className="pl-2  justify-start">
                  <div className="fw6 f8 f6-l f6-m">
                    {lang === 'HIN'
                      ? player.playerNameHindi
                      : player.playerName}
                  </div>
                  <div className="mt-1 p-1 bg-gray-4 rounded-md w-6 h-6">
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className=""
                      src={bat}
                      alt="w-4 h-4"
                    />
                  </div>
                </div>
              </div>
              <div className='w-1/3 flex px-2 gap-6  justify-end '>

                <div className="  flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center h-7 w-7 p-1 rounded-full bg-gray-4 text-white  ${player.playerId === captain
                      ? 'bg-green text-white border  '
                      : 'border border-white'
                      }  justify-center text-sm  `}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className=" flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center  h-7 w-7 p-2 rounded-full bg-gray-4 text-white ${player.playerId === vice_captain
                      ? 'bg-green text-white border  baorder-green'
                      : 'border border-white'
                      } 
                      justify-center text-sm`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>

              </div>
              <div></div>
            </div>
          ))}


        {playerList
          .filter((pl) => pl.player_role === 'ALL_ROUNDER')
          .map((player, i) => (
            <div
              key={i}
              className="py-2  flex items-center justify-between  border-b mt-2 "
            >
              <div className="flex items-center w-2/3">
                <div className="  flex items-center justify-end ml-2">
                  <div className=" relative overflow-hidden    ">
                    <ImageWithFallback
                      height={38}
                      width={38}
                      loading="lazy"
                      fallbackSrc={playerPlaceholder}
                      className=" rounded-full h-16 w-16 h3-l w3-l h3-m w3-m    object-cover object-top"
                      src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                      alt=""
                    />
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      fallbackSrc={flagPlaceHolder}
                      className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5 bg-gray-4 "
                      style={{ left: 2 }}
                      src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                      alt=""
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    />
                  </div>
                </div>
                <div className="pl-2 w-70 w-50-l justify-start">
                  <div className="fw6 f8 f6-l f6-m">
                    {lang === 'HIN'
                      ? player.playerNameHindi
                      : player.playerName}
                  </div>
                  <div className="mt-1 p-1 bg-gray-4 rounded-md w-6 h-6">
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className="w-4 h-4"
                      src={allRounder}
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className='w-1/3 flex px-2 gap-6  justify-end '>
                <div className="  flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center h-7 w-7 p-1 rounded-full bg-gray-4 text-white  ${player.playerId === captain
                      ? 'bg-green text-white border  '
                      : 'border border-white'
                      }  justify-center text-sm  `}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className=" flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center  h-7 w-7 p-1 rounded-full bg-gray-4 text-white ${player.playerId === vice_captain
                      ? 'bg-green text-white border  baorder-green'
                      : 'border border-white'
                      } 
                      justify-center text-sm`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>
              </div>
              <div></div>
            </div>
          ))}


        {playerList
          .filter((pl) => pl.player_role === 'BOWLER')
          .map((player, i) => (
            <div
              key={i}
              className="py-2  flex items-center justify-between  border-b mt-2"
            >
              <div className="flex items-center w-2/3">
                <div className=" flex items-center justify-end ml-2">
                  <div className=" relative overflow-hidden    ">
                    <ImageWithFallback
                      height={38}
                      width={38}
                      loading="lazy"
                      fallbackSrc={playerPlaceholder}
                      className=" rounded-full h-16 w-16 h3-l w3-l h3-m w3-m    object-cover object-top"
                      src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                      alt=""
                    />
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      fallbackSrc={flagPlaceHolder}
                      className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5 bg-gray-4"
                      style={{ left: 2 }}
                      src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                      alt=""
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    />
                  </div>
                </div>
                <div className="pl-2 w-70 w-50-l justify-start">
                  <div className="fw6 f8 f6-l f6-m">
                    {lang === 'HIN'
                      ? player.playerNameHindi
                      : player.playerName}
                  </div>
                  <div className="mt-1 p-1 bg-gray-4 rounded-md w-6 h-6">
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className="w-4 h-4"
                      src={ball}
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className='w-1/3 flex px-2 gap-6  justify-end '>
                <div className="  flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center h-7 w-7 p-1 rounded-full bg-gray-4 text-white  ${player.playerId === captain
                      ? 'bg-green text-white border  '
                      : 'border border-white'
                      }  justify-center text-sm  `}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className=" flex items-center justify-center cursor-pointer ">
                  <div
                    className={`flex items-center  h-7 w-7 p-1 rounded-full bg-gray-4 text-white ${player.playerId === vice_captain
                      ? 'bg-green text-white border  baorder-green'
                      : 'border border-white'
                      } 
                      justify-center text-sm`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>
              </div>
              <div></div>
            </div>
          ))}
    </div>
      {
    captain && vice_captain ? (
      <Link
        {...getStatshubFrcUrl(matchId, matchSlug, 'my-teams')}
        passHref
        legacyBehavior
      >
        <div className=" mt-4 flex items-center justify-center ">
          <div
            className={`p-2 w-32 text-sm font-semibold ${captain && vice_captain ? 'bg-basered dark:bg-green text-white' : 'bg-white opacity-70 '
              } uppercase flex justify-center items-center w-40 gap-2 rounded-lg shadow-4 cursor-pointer`}
            onClick={() =>
              captain && vice_captain ? updateTeamDetails() : ''
            }
          >
            <ImageWithFallback
              height={18}
              width={18}
              loading="lazy"
              src={saveTeam}
            />

            {lang === 'HIN' ? 'सेव टीम' : 'Save team'}{' '}

          </div>
        </div>
      </Link>
    ) : (
    <div className=" mt-4  flex items-center justify-center">
      <div
        className={`bg-basered dark:bg-transparent text-white  opacity-70 p-2 flex justify-center w-32 gap-2 shadow-4 cursor-pointer border  rounded-lg`}
      >
        <ImageWithFallback
          height={18}
          width={18}
          loading="lazy"
          src={saveTeam}
        />
        <span className="pl1">
          {lang === 'HIN' ? 'सेव टीम' : 'Save team'}
        </span>
      </div>
    </div>
  )
  }
  {/* {
             Submit &&  
        <div className="flex justify-center items-center fixed absolute--fill z-9999 bg-black-70 cursor-pointer" style={{ backdropFilter: "blur(10px)" }} >
          <div className="w-90 mw6-l relative">
            <img src={'/svgs/close.png'} className="absolute right-0 top--2" onClick={() => setSubmit(false)} />
            <div className="bg-black br3 shadow-4  ba b--white-20 max-vh-80  white pa2 pv4" >
              <div className="fw6 f4 tc pb2">Name your team</div>
              <input className="w-80 br2 h2 tc flex justify-center center bg-white-30 ba b--yellow white ttu mv4 f6 fw5" placeholder="Enter Your Team Name" value={teamName} onChange={(e) => setTeamName(e.target.value)}></input>
              <Link {...getStatshubFrcUrl(matchId,matchSlug,'my-teams')} passHref>
                <div className={`f5 fw6 white ${teamName.length>0 ? "bg-green":"bg-white-30"} db flex justify-center center w-40 items-center pa2 br2 dib`} onClick={() => (teamName.length>0 ? (updateTeamDetails()):"")}>
                <div className="pl2">SAVE</div></div>
              </Link>
            </div>
          </div>
        </div>
      
          } */}
    </div >
  )
}
