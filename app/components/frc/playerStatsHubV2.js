import { useQuery } from '@apollo/react-hooks'
import { usePathname } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import { GET_MATCHUP_BY_ID, STAT_HUB_PLAYER_V2 } from '../../api/queries'
import DataNotFound from '../commom/datanotfound'
import Heading from '../commom/heading'
import ImageWithFallback from '../commom/Image'
import Loading from '../loading'
import { words } from './../../constant/language'
import CompareModelPlayerHub from './compareModelPlayerHub'
import PerformanceIndicator from './fRCPerformanceIndicator'
import LineChartFRC from './lineChartFRC'
import Matcups from './matchups'
import MatchUpsModel from './matchUpsModel'
import PlayerModelV2 from './playerModelV2'
import RecentPlayerBattingType from './recentPlayerBattingType'
import RecentPlayerBowlingType from './recentPlayerBowlingType'
const flagPlaceHolder = '/pngsV2/flag_dark.png'
const compare = '/pngsV2/swap.png'
const compareGreen = '/pngsV2/star.png'

const addicon = '/pngsV2/addicon.png'
const minusIcon = '/pngsV2/minusicon.png'

const playerPlaceholder = '/pngs/fallbackprojection.png'
const rightWhiteArrow = '/svgs/RightSchevronWhite.svg'
export default function PlayerStatsHubV2(props) {
  const [PlayersList, setPlayersList] = useState([])
  // const [Player2List, setPlayer2List] = useState([])
  const [OpenModelMatchups, setOpenModelMatchups] = useState(false)
  const [SelectionType, setSelectionType] = useState('recentForm')
  const [RecentPlayer, setRecentPlayer] = useState(props.recentplayer)
  const [openCompareModel, setCompareModel] = useState(false)
  //
  const [bowlingType, setBowlingType] = useState('S/R')
  const [openSelectionModelV2, setopenSelectionModelV2] = useState(false)
  const [battingType, setBattingType] = useState('WICKETS')
  const [credits, setcredits] = useState(props.credit)
  //
  const [innerWidth, updateInnerWidth] = useState(0)
  const [showMyTeam, setShowmyteam] = useState(true)
  const [MatchUpsPlayer2, setMatchUpsPlayer2] = useState(null)
  //
  const [Player2Compare, setPlayer2Compare] = useState(null)
  const [quality, setquality] = useState('strength')
  const [catPlayers, setcatPlayers] = useState(
    props.categoryPlayer
      ? { ...props.categoryPlayer }
      : {
          WK: [],
          AR: [],
          BOWL: [],
          BAT: [],
        },
  )

  useEffect(() => {
    updateInnerWidth(window.innerWidth)
  }, [])
  //
  // const [playerTeamIDList,setplayerTeamIDList]=useState([...props.teamList])
  const [playerTeamIDList, setplayerTeamIDList] = useState([...props.teamList])

  //

  const {
    loading: MatchupLoading,
    error: MatchUpsDataError,
    data: MatchUpsData,
  } = useQuery(GET_MATCHUP_BY_ID, {
    variables: { crictecMatchId: props.matchID },
    onCompleted: (data) => {
      //
      if (
        data &&
        data.matchupsById &&
        data.matchupsById.matchUpData &&
        RecentPlayer &&
        RecentPlayer?.playerID
      ) {
        setMatchUpsPlayer2(
          data.matchupsById.matchUpData.filter(
            (player) => player.player1 === RecentPlayer?.playerID,
          )[0],
        )
      }
    },
  })
  let lang = props.language ? props.language : 'ENG'

  const getLangText = (lang, keys, key) => {
    //
    let [english, hindi] = keys[key]
    switch (lang) {
      case 'HIN':
        return hindi
      case 'ENG':
        return english
      default:
        return english
    }
  }

  const { loading, error, data: PlayerStatHubData } = useQuery(
    STAT_HUB_PLAYER_V2,
    {
      variables: { matchID: props.matchID },
      onCompleted: (data) => {
        //
        if (
          data &&
          data.playerHub &&
          data.playerHub.recentForm &&
          data.playerHub.recentForm.length > 0
        ) {
          //
          setRecentPlayer(data.playerHub.recentForm[0])
          // console.log()
          setPlayer2Compare(
            data.playerHub.recentForm.filter(
              (pl) =>
                pl.playerSkill === data.playerHub.recentForm[0].playerSkill &&
                pl.playerID !== data.playerHub.recentForm[0].playerID,
            )[0],
          )
          //
          //  getMatchUpData({ variables: { crictecMatchId: props.matchID } })
          //
          let SelectedPlayerList = data.playerHub.recentForm.filter((pl) =>
            playerTeamIDList.includes(pl.playerID),
          )

          let categoryList = {
            WK: [
              ...SelectedPlayerList.filter(
                (pl) => pl.playerSkill === 'KEEPER',
              ).map((pl) => pl.playerID),
            ],
            AR: [
              ...SelectedPlayerList.filter(
                (pl) => pl.playerSkill === 'ALL_ROUNDER',
              ).map((pl) => pl.playerID),
            ],
            BOWL: [
              ...SelectedPlayerList.filter(
                (pl) => pl.playerSkill === 'BOWLER',
              ).map((pl) => pl.playerID),
            ],
            BAT: [
              ...SelectedPlayerList.filter(
                (pl) => pl.playerSkill === 'BATSMAN',
              ).map((pl) => pl.playerID),
            ],
          }

          //
          setcatPlayers({ ...catPlayers, ...categoryList })
          setPlayersList([...SelectedPlayerList])
          // setplayerList([...SelectedPlayerList])
        }
      },
    },
  )

  // const callScroll = (data) => {
  //    data ? (document.body.style.overflow = 'hidden') : (document.body.style.overflow = 'unset');
  // }

  const choosePlayer = (player, type) => {
    if (type === 'recentForm') {
      setRecentPlayer(player)
      setPlayer2Compare(
        PlayerStatHubData &&
          PlayerStatHubData.playerHub &&
          PlayerStatHubData.playerHub.recentForm.filter(
            (players) =>
              players.playerID !== player.playerID &&
              players.playerSkill === player.playerSkill,
          )[0],
      )
      setopenSelectionModelV2(false)
      setMatchUpsPlayer2(
        MatchUpsData.matchupsById.matchUpData.filter(
          (players) => players.player1 === player.playerID,
        )[0],
      )
    }
    if (type === 'compare') {
      setPlayer2Compare(player)
      setCompareModel(false)
    }
    if (type === 'matchups') {
      setMatchUpsPlayer2(player)

      setOpenModelMatchups(false)
    }
  }
  /*BYT*/

  //
  let activeCategory = {
    ALL_ROUNDER: 'AR',
    BOWLER: 'BOWL',
    BATSMAN: 'BAT',
    KEEPER: 'WK',
  }

  let config = { ...props.frcComposition_config }
  //
  let matchConfig = { ...props.frcComposition_matchConfig }
  const [errrorMsg, seterrorMsg] = useState('')

  let tout = null
  const hideMsg = () => {
    // ;
    if (tout) {
      clearTimeout(tout)
      tout = setTimeout(() => {
        seterrorMsg('')
      }, 3000)
    } else {
      tout = setTimeout(() => {
        seterrorMsg('')
      }, 3000)
    }
  }

  const ruleCheckNew = (player, PlayerCategory) => {
    //
    //
    if (PlayersList.length + 1 > 11) {
      // seterrorMsg(`You can select maximum of 11 players`);
      seterrorMsg(getLangText(lang, words, 'selectMaximumPlayersTwo'))
      hideMsg()
    } else if (
      catPlayers[PlayerCategory]?.length + 1 >
      matchConfig[`${PlayerCategory}_max`]
    ) {
      seterrorMsg(
        `${getLangText(lang, words, 'selectMaximum')}  ${
          matchConfig[`${PlayerCategory}_max`]
        } ${config[PlayerCategory].fullName} ${getLangText(
          lang,
          words,
          'only',
        )}`,
      )
      hideMsg()
      return false
    } else if (
      player.teamID === props.homeTeamID &&
      PlayersList.filter((player) => player.teamID === props.homeTeamID)
        .length +
        1 >
        7
    ) {
      // seterrorMsg(`Maximum 7 players from each team allowed`);
      seterrorMsg(getLangText(lang, words, 'selectMaximumPlayers'))
      hideMsg()
      return false
    } else if (
      player.teamID === props.awayTeamID &&
      PlayersList.filter((player) => player.teamID === props.awayTeamID)
        .length > 7
    ) {
      seterrorMsg(getLangText(lang, words, 'selectMaximumPlayers'))
      hideMsg()
      return false
    }
    //   else if (PlayersList.filter(player=>player.teamID==='2').length==7|| PlayersList.filter(player=>player.teamID==='1').length==7) {
    //      seterrorMsg(`Maximum 7 players from each team allowed`);
    //      hideMsg()
    //      return false;
    //    }
    else if (credits - player.playerCredits < 0) {
      // seterrorMsg(`Total credits cannot exceed 100`);
      seterrorMsg(etLangText(lang, words, 'totalCredits'))
      hideMsg()
      return false
    } else if (
      PlayerCategory == 'WK' &&
      catPlayers.WK.length >= matchConfig.WK_min &&
      11 - (PlayersList.length + 1) <
        (catPlayers.BAT.length >= matchConfig.BAT_min
          ? 0
          : matchConfig.BAT_min - catPlayers.BAT.length) +
          (catPlayers.AR.length >= matchConfig.AR_min
            ? 0
            : matchConfig.AR_min - catPlayers.AR.length) +
          (catPlayers.BOWL.length >= matchConfig.BOWL_min
            ? 0
            : matchConfig.BOWL_min - catPlayers.BOWL.length)
      //   (catPlayers.BAT.length ) +
      //   (catPlayers.BOWL.length ) +
      //   (catPlayers.AR.length )
      //   ))
      // && ((11 - (catPlayers.WK.length +1)) < (
      //   (catPlayers.BAT.length >0 ? catPlayers.BAT.length : matchConfig.BAT_min ) +
      //   (catPlayers.AR.length >0 ? catPlayers.AR.length : matchConfig.AR_min ) +
      //   (catPlayers.BOWL.length >0 ? catPlayers.BOWL.length : matchConfig.BOWL_min )
      // ) )
    ) {
      // seterrorMsg(
      //    `Please select at least${catPlayers.BAT.length < matchConfig.BAT_min
      //       ? ` ${matchConfig.BAT_min} Batsmen`
      //       : ''
      //    }${catPlayers.AR.length < matchConfig.AR_min
      //       ? ` ${matchConfig.AR_min} All-Rounders`
      //       : ''
      //    }${catPlayers.BOWL.length < matchConfig.BOWL_min
      //       ? ` ${matchConfig.BOWL_min} Bowlers`
      //       : ''
      //    }`,
      // );
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${
          catPlayers.BAT.length < matchConfig.BAT_min
            ? ` ${matchConfig.BAT_min} ${getLangText(
                lang,
                words,
                'BATSMAN_RULE',
              )}`
            : ''
        }${
          catPlayers.AR.length < matchConfig.AR_min
            ? ` ${matchConfig.AR_min} ${getLangText(lang, words, 'AR_RULE')}`
            : ''
        }${
          catPlayers.BOWL.length < matchConfig.BOWL_min
            ? ` ${matchConfig.BOWL_min} ${getLangText(
                lang,
                words,
                'BOWLER_RULE',
              )}`
            : ''
        }  ${getLangText(lang, words, 'choose')}`,
      )
      hideMsg()
      return false
    } else if (
      PlayerCategory == 'BAT' &&
      catPlayers.BAT.length >= matchConfig.BAT_min &&
      11 - (PlayersList.length + 1) <
        (catPlayers.WK.length >= matchConfig.WK_min
          ? 0
          : matchConfig.WK_min - catPlayers.WK.length) +
          (catPlayers.AR.length >= matchConfig.AR_min
            ? 0
            : matchConfig.AR_min - catPlayers.AR.length) +
          (catPlayers.BOWL.length >= matchConfig.BOWL_min
            ? 0
            : matchConfig.BOWL_min - catPlayers.BOWL.length)
      //   (catPlayers.AR.length ) +
      //   (catPlayers.BOWL.length ) +
      //   (catPlayers.WK.length )
      //   ))
      // && ((11 - (catPlayers.BAT.length +1)) < (
      //   (catPlayers.WK.length >0 ? catPlayers.WK.length : matchConfig.WK_min ) +
      //   (catPlayers.AR.length >0 ? catPlayers.AR.length : matchConfig.AR_min ) +
      //   (catPlayers.BOWL.length >0 ? catPlayers.BOWL.length : matchConfig.BOWL_min )
      // ) )
    ) {
      // seterrorMsg(
      //    `Please select at least${catPlayers.WK.length < matchConfig.WK_min
      //       ? ` ${matchConfig.WK_min} Wicket-Keeper`
      //       : ''
      //    }${catPlayers.AR.length < matchConfig.AR_min
      //       ? ` ${matchConfig.AR_min} All-Rounders`
      //       : ''
      //    }${catPlayers.BOWL.length < matchConfig.BOWL_min
      //       ? ` ${matchConfig.BOWL_min} Bowlers`
      //       : ''
      //    }`,
      // );
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${
          catPlayers.WK.length < matchConfig.WK_min
            ? ` ${matchConfig.WK_min} ${getLangText(
                lang,
                words,
                'KEEPER_RULE',
              )}`
            : ''
        }${
          catPlayers.AR.length < matchConfig.AR_min
            ? ` ${matchConfig.AR_min} ${getLangText(lang, words, 'AR_RULE')}`
            : ''
        }${
          catPlayers.BOWL.length < matchConfig.BOWL_min
            ? ` ${matchConfig.BOWL_min} ${getLangText(
                lang,
                words,
                'BOWLER_RULE',
              )}`
            : ''
        }  ${getLangText(lang, words, 'choose')}`,
      )
      hideMsg()
      return false
    } else if (
      PlayerCategory == 'AR' &&
      catPlayers.AR.length >= matchConfig.AR_min &&
      11 - (PlayersList.length + 1) <
        (catPlayers.BAT.length >= matchConfig.BAT_min
          ? 0
          : matchConfig.BAT_min - catPlayers.BAT.length) +
          (catPlayers.WK.length >= matchConfig.WK_min
            ? 0
            : matchConfig.WK_min - catPlayers.WK.length) +
          (catPlayers.BOWL.length >= matchConfig.BOWL_min
            ? 0
            : matchConfig.BOWL_min - catPlayers.BOWL.length)
      //   (catPlayers.BAT.length ) +
      //   (catPlayers.BOWL.length ) +
      //   (catPlayers.WK.length )
      //   ))
      // && ((11 - (catPlayers.AR.length +1)) < (
      //   (catPlayers.BAT.length >0 ? catPlayers.BAT.length : matchConfig.BAT_min ) +
      //   (catPlayers.WK.length >0 ? catPlayers.WK.length : matchConfig.WK_min ) +
      //   (catPlayers.BOWL.length >0 ? catPlayers.BOWL.length : matchConfig.BOWL_min )
      // ) )
    ) {
      // seterrorMsg(
      //    `Please select at least${catPlayers.BAT.length < matchConfig.BAT_min
      //       ? ` ${matchConfig.BAT_min} Batsmen`
      //       : ''
      //    }${catPlayers.WK.length < matchConfig.WK_min
      //       ? ` ${matchConfig.WK_min} Wicket-Keeper`
      //       : ''
      //    }${catPlayers.BOWL.length < matchConfig.BOWL_min
      //       ? ` ${matchConfig.BOWL_min} Bowlers`
      //       : ''
      //    }`,
      // );
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${
          catPlayers.BAT.length < matchConfig.BAT_min
            ? ` ${matchConfig.BAT_min} ${getLangText(
                lang,
                words,
                'BATSMAN_RULE',
              )}`
            : ''
        }${
          catPlayers.WK.length < matchConfig.WK_min
            ? ` ${matchConfig.WK_min} ${getLangText(
                lang,
                words,
                'KEEPER_RULE',
              )}`
            : ''
        }${
          catPlayers.BOWL.length < matchConfig.BOWL_min
            ? ` ${matchConfig.BOWL_min} ${getLangText(
                lang,
                words,
                'BOWLER_RULE',
              )}`
            : ''
        } ${getLangText(lang, words, 'choose')}`,
      )
      hideMsg()
      return false
    } else if (
      PlayerCategory == 'BOWL' &&
      catPlayers.BOWL.length >= matchConfig.BOWL_min &&
      11 - (PlayersList.length + 1) <
        (catPlayers.BAT.length >= matchConfig.BAT_min
          ? 0
          : matchConfig.BAT_min - catPlayers.BAT.length) +
          (catPlayers.AR.length >= matchConfig.AR_min
            ? 0
            : matchConfig.AR_min - catPlayers.AR.length) +
          (catPlayers.WK.length >= matchConfig.WK_min
            ? 0
            : matchConfig.WK_min - catPlayers.WK.length)
      //   (catPlayers.BAT.length ) +
      //   (catPlayers.AR.length  ) +
      //   (catPlayers.WK.length )
      //   ))
      // && ((11 - (catPlayers.BOWL.length +1)) < (
      //   (catPlayers.BAT.length >0 ? catPlayers.BAT.length : matchConfig.BAT_min ) +
      //   (catPlayers.AR.length >0 ? catPlayers.AR.length : matchConfig.AR_min ) +
      //   (catPlayers.WK.length >0 ? catPlayers.WK.length : matchConfig.WK_min )
      // ) )
    ) {
      // seterrorMsg(
      //    `Please select at least${catPlayers.BAT.length < matchConfig.BAT_min
      //       ? ` ${matchConfig.BAT_min} Batsmen`
      //       : ''
      //    }${catPlayers.AR.length < matchConfig.AR_min
      //       ? ` ${matchConfig.AR_min} All-Rounders`
      //       : ''
      //    }${catPlayers.WK.length < matchConfig.WK_min
      //       ? ` ${matchConfig.WK_min} Wicket-Keeper`
      //       : ''
      //    }`,
      // );
      seterrorMsg(
        `${getLangText(lang, words, 'selectMini')} ${
          catPlayers.BAT.length < matchConfig.BAT_min
            ? ` ${matchConfig.BAT_min} ${getLangText(
                lang,
                words,
                'BATSMAN_RULE',
              )}`
            : ''
        }${
          catPlayers.AR.length < matchConfig.AR_min
            ? ` ${matchConfig.AR_min} ${getLangText(lang, words, 'AR_RULE')}`
            : ''
        }${
          catPlayers.WK.length < matchConfig.WK_min
            ? ` ${matchConfig.WK_min} ${getLangText(
                lang,
                words,
                'KEEPER_RULE',
              )}`
            : ''
        } ${getLangText(lang, words, 'choose')}`,
      )
      hideMsg()
      return false
    } else {
      return true
    }
  }
  const handlePlayerSelection = (currentPlayer, operation) => {
    let PlayerCategory = activeCategory[currentPlayer.playerSkill]
    //
    if (operation === 'add') {
      let checkRules = ruleCheckNew(currentPlayer, PlayerCategory)

      //     let playerDetail=Players.length>0? [...Players]:[]
      //   let addPlayer= playerDetail.push({...currentPlayer})
      if (checkRules) {
        setPlayersList([...PlayersList, currentPlayer])
        setcredits((prev) => prev - Number(currentPlayer.playerCredits))
        //   setPlayersList([...PlayersList])
        setcatPlayers({
          ...(catPlayers || []),
          [PlayerCategory]: [
            ...(catPlayers[PlayerCategory] || []),
            currentPlayer.playerID,
          ],
        })

        setplayerTeamIDList([...playerTeamIDList, currentPlayer.playerID])
      }

      let maintainPlayerList = [...PlayersList, currentPlayer]
      let maintainPlayerTeamIDList = [
        ...playerTeamIDList,
        currentPlayer.playerID,
      ]
      let maintainCredit = credits - Number(currentPlayer.playerCredits)
      let maintaincatPlayer = {
        ...(catPlayers || []),
        [PlayerCategory]: [
          ...(catPlayers[PlayerCategory] || []),
          currentPlayer.playerID,
        ],
      }
      props.playerScores(
        maintainPlayerList,
        maintainCredit,
        maintaincatPlayer,
        maintainPlayerTeamIDList,
        'maintainState',
        RecentPlayer,
      )
    } else {
      //
      let filterPlayer = [...PlayersList]
      let deletePlayer = filterPlayer.filter(
        (player) => player.playerID !== currentPlayer.playerID,
      )
      setPlayersList([...deletePlayer])
      setplayerTeamIDList([
        ...playerTeamIDList.filter((pl) => pl !== currentPlayer.playerID),
      ])
      setcredits((prev) => prev + Number(currentPlayer.playerCredits))
      setcatPlayers({
        ...catPlayers,
        [PlayerCategory]: [
          ...catPlayers[PlayerCategory].filter(
            (pl) => pl !== currentPlayer.playerID,
          ),
        ],
      })
      let maintainPlayerList = [...deletePlayer]
      let maintainPlayerTeamIDList = [
        ...playerTeamIDList.filter((pl) => pl !== currentPlayer.playerID),
      ]
      let maintainCredit = credits + Number(currentPlayer.playerCredits)
      let maintaincatPlayer = {
        ...catPlayers,
        [PlayerCategory]: [
          ...catPlayers[PlayerCategory].filter(
            (pl) => pl !== currentPlayer.playerID,
          ),
        ],
      }

      props.playerScores(
        maintainPlayerList,
        maintainCredit,
        maintaincatPlayer,
        maintainPlayerTeamIDList,
        'maintainState',
        RecentPlayer,
      )
    }

    // if(checkRules && Players.filter(player=>player.playerID==currentPlayer.playerID).length==0){
    //
    //
    //     let playerDetail=Players.length>0? [...Players]:[]
    //
    //   let addPlayer= playerDetail.push({...currentPlayer})
    //
    //   setcredits(prev=>prev-currentPlayer.playerCredits)
    //     setPlayers(playerDetail)
    // }else{
    //    let filterPlayer=[...Players]
    //   let deletePlayer = filterPlayer.filter(player=>player.playerID!==currentPlayer.playerID)
    //   setPlayers([...deletePlayer])
    //   setcredits(prev=>prev+currentPlayer.playerCredits)
    // }
  }

  const handlecssPlayer1 = (type, stat, player1value, player2value) => {
    //  if (PlayersList.length + 1 <= 11) {
    //    if (tout) {
    //      clearTimeout(tout);
    //      seterrorMsg('');
    //    }
    if (type === 'batting') {
      if (stat === 'commonBatPosition') {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) < parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value !== '' && player2value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) > parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value !== '' && player2value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      }
    } else if (type === 'fantasy') {
      if (player1value > player2value) {
        return 'text-green'
      } else {
        return 'text-gray-2'
      }
    } else {
      if (stat === 'bowlingEconomyRate') {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) < parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value !== '' && player2value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else if (stat === 'bowlingStrikeRate') {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) < parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value !== '' && player2value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else if (stat === 'bowlingBestBowlingTotalSpell') {
        if (player1value !== '' && player2value !== '') {
          const bb1 = player1value !== '' ? ~~player1value.split('/')[0] : 0
          const bb2 = player2value !== '' ? ~~player2value.split('/')[0] : 0
          if (bb1 > bb2) {
            return 'text-green'
          } else if (bb1 == bb2) {
            const b21 = player1value !== '' ? ~~player1value.split('/')[1] : 0
            const b22 = player2value !== '' ? ~~player2value.split('/')[1] : 0
            if (b21 > b22) {
              return 'text-green'
            } else return ''
          } else {
            return 'text-gray-2'
          }
        } else if (player1value !== '' && player2value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) > parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value !== '' && player2value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      }
    }
  }

  const handlecssPlayer2 = (type, stat, player1value, player2value) => {
    if (type === 'batting') {
      if (stat === 'commonBatPosition') {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) > parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player2value !== '' && player1value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) < parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player2value !== '' && player1value === '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      }
    } else if (type === 'fantasy') {
      if (player1value < player2value) {
        return 'text-green'
      } else {
        return 'text-gray-2'
      }
    } else {
      if (stat === 'bowlingEconomyRate') {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) > parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value === '' && player2value !== '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else if (stat === 'bowlingStrikeRate') {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) > parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value === '' && player2value !== '') {
          return 'text-green'
        } else {
          return 'text-gray-2'
        }
      } else if (stat === 'bowlingBestBowlingTotalSpell') {
        if (player1value !== '' && player2value !== '') {
          const bb1 = player1value !== '' ? ~~player1value.split('/')[0] : 0
          const bb2 = player2value !== '' ? ~~player2value.split('/')[0] : 0
          if (bb1 < bb2) {
            return 'text-green'
          } else if (bb1 == bb2) {
            const b21 = player1value !== '' ? ~~player1value.split('/')[1] : 0
            const b22 = player2value !== '' ? ~~player2value.split('/')[1] : 0
            if (b21 < b22) {
              return 'text-green'
            } else return ''
          } else {
            return ''
          }
        } else if (player1value === '' && player2value !== '') {
          return 'text-green'
        } else {
          return ''
        }
      } else {
        if (
          player1value !== '' &&
          player2value !== '' &&
          parseFloat(player1value) < parseFloat(player2value)
        ) {
          return 'text-green'
        } else if (player1value === '' && player2value !== '') {
          return 'text-green'
        } else {
          return ''
        }
      }
    }
  }
  let pathName = usePathname()
  const closeNewModelV2 = () => {
    if (SelectionType === 'matchups') {
      setOpenModelMatchups(false)
    }
    if (SelectionType === 'compare') {
      setCompareModel(false)
    } else {
      setopenSelectionModelV2(false)
    }
  }

  const [, , matchID, matchName, screen, tabname] = pathName.split('/')

  if (loading) return <Loading />
  else if (error)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-col  justify-center items-center">
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  else
    return (
      <>
        <div className="w-full">
          <div className="w-full px-3 md:hidden lg:hidden md:mx-0 lg:mx-0 dark:text-white ">
            {PlayerStatHubData &&
            PlayerStatHubData.playerHub &&
            PlayerStatHubData.playerHub.recentForm &&
            PlayerStatHubData.playerHub.recentForm.length > 0 ? (
              <div className=" w-full relative flex flex-wrap ">
                {/* <div className='md:flex lg:flex '> */}
                <div className="w-full md:w-1/2 lg:w-1/2 md:px-2 lg:px-2">
                  <div className="w-full ">
                    <div
                      className={`flex mb-3 items-center justify-between w-full`}
                    >
                      <div className={`h-12  w-full`}>
                        <div
                          className={`flex h-full cursor-pointer text-white items-center  p-3  bg-gray justify-between 
                                          rounded-md  ${
                                            props.showHideFRC ? '' : ''
                                          } `}
                          onClick={() => {
                            setopenSelectionModelV2(true)
                            setSelectionType('recentForm')
                            setPlayer2Compare(
                              PlayerStatHubData &&
                                PlayerStatHubData.playerHub &&
                                PlayerStatHubData.playerHub.recentForm.filter(
                                  (players) =>
                                    players.playerID !== RecentPlayer?.playerID,
                                )[0],
                            )
                          }}
                        >
                          <div className="flex items-center">
                            {/* <div className="w-8 h-8 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                                       <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${RecentPlayer && RecentPlayer?.teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                                    </div> */}
                            <div className="uppercase text-sm font-semibold">
                              {RecentPlayer &&
                                (lang === 'HIN'
                                  ? RecentPlayer?.playerNameHindi
                                  : RecentPlayer?.name)}
                            </div>
                          </div>
                          <div className="p-1">
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              className="w-5 h-5 "
                              src={compare}
                              alt=""
                            />
                          </div>
                        </div>
                      </div>

                      {props.showHideFRC && (
                        <div
                          className=" h-14 flex items-center justify-end  "
                          onClick={() => {
                            handlePlayerSelection(
                              RecentPlayer,
                              playerTeamIDList.filter(
                                (id) => id == RecentPlayer?.playerID,
                              ).length == 0
                                ? 'add'
                                : 'delete',
                            )
                          }}
                        >
                          {playerTeamIDList.filter(
                            (id) => id == RecentPlayer?.playerID,
                          ).length == 0 ? (
                            <div className="flex items-center bg-gray-4 justify-center text-center center h-12 rounded-lg ml-2 w-12  ">
                              <ImageWithFallback
                                height={28}
                                width={28}
                                loading="lazy"
                                className="w-full h-full cursor-pointer"
                                src={addicon}
                                alt=""
                                srcset=""
                              />
                            </div>
                          ) : (
                            <div className="flex items-center bg-gray-4 justify-center text-center center h-12 rounded-lg ml-2 w-12  ">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                className="w-full h-full cursor-pointer"
                                src={minusIcon}
                                alt=""
                                srcset=""
                              />
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  </div>

                  {/* {errrorMsg !== "" && <div className='flex'>
                              <div className=' f6 white fw1 flex justify-center items-center pa2 w-full center'>
                                 <div className='border rounded-full  p-2 h-2 w-2 text-white flex justify-center items-center'> ! </div>
                                 <div className="pl2 f6 fw5">{errrorMsg}</div>
                              </div>
                           </div>} */}

                  <div className="flex gap-2 w-full items-center justify-center ">
                    <div className="flex w-6/12 md:w-4/12 lg:w-4/12 h-full  items-center justify-center">
                      <div className=" h-full w-full">
                        <div className="h-full border bg-white  dark:text-white  dark:border-none  dark:bg-gray-4 py-16  flex flex-col items-center justify-center rounded-md">
                          <div className="relative bg-gray-10 dark:bg-gray-8 h-full rounded-full flex items-center justify-center ">
                            <div
                              className={`overflow-hidden w-32 h-32 rounded-full `}
                            >
                              <ImageWithFallback
                                height={180}
                                width={180}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className="object-top object-cover w-full h-full   "
                                src={`https://images.cricket.com/players/${
                                  RecentPlayer && RecentPlayer?.playerID
                                }_headshot_safari.png`}
                                alt=""
                                //   onError={(e) => (e.target.src = playerPlaceholder)}
                              />
                            </div>
                            <div className="absolute bottom-2 left-0   ">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc={flagPlaceHolder}
                                className="w-6 h-6  border rounded-full "
                                src={`https://images.cricket.com/teams/${
                                  RecentPlayer && RecentPlayer?.teamID
                                }_flag_safari.png`}
                                //   onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                alt=""
                              />
                            </div>
                          </div>
                          <div className="flex text-base font-semibold py-2 text-center">
                            {RecentPlayer &&
                              (lang === 'HIN'
                                ? RecentPlayer?.playerNameHindi
                                : RecentPlayer?.name)}
                          </div>
                          <div className="flex">
                            {lang === 'HIN' ? (
                              <div className="text-xs  font-semibold">
                                {RecentPlayer &&
                                  (RecentPlayer?.playerSkill === 'BATSMAN'
                                    ? 'बल्लेबाज़'
                                    : RecentPlayer?.playerSkill === 'BOWLER'
                                    ? 'गेंदबाज'
                                    : RecentPlayer?.playerSkill === 'KEEPER'
                                    ? 'विकेट कीपर'
                                    : 'ऑल राउंडर')}
                              </div>
                            ) : (
                              <div className="text-xs  font-semibold">
                                {RecentPlayer &&
                                  (RecentPlayer?.playerSkill === 'BATSMAN'
                                    ? 'BATTER'
                                    : RecentPlayer?.playerSkill === 'BOWLER'
                                    ? 'BOWLER'
                                    : RecentPlayer?.playerSkill === 'KEEPER'
                                    ? 'KEEPER'
                                    : 'ALL ROUNDER')}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="w-6/12 md:w-8/12 lg:w-8/12 h-full ">
                      <div className="h-full  flex flex-col gap-3 ">
                        <div className="flex  bg-white  dark:text-white border  dark:border-none items-center justify-evenly dark:bg-gray-4  rounded-md p-2 ">
                          <div className="  text-base  font-bold w-1/3">
                            {RecentPlayer && RecentPlayer?.avgFantasyPoints}
                          </div>

                          <div className="text-xs text-gray-2 w-2/3 text-left font-bold  p-1">
                            {RecentPlayer &&
                              (RecentPlayer?.lastFiveMatches.length === 5
                                ? getLangText(
                                    lang,
                                    words,
                                    'AVG POINTS IN LAST 5 MATCHES',
                                  )
                                : RecentPlayer?.lastFiveMatches.length > 0 &&
                                  RecentPlayer?.lastFiveMatches.length < 5
                                ? getLangText(
                                    lang,
                                    words,
                                    'AVG POINTS IN LAST FEW MATCHES',
                                  )
                                : '')}
                          </div>
                        </div>

                        <div className="bg-white  border  dark:border-none dark:bg-gray-4 rounded p-1 ">
                          <div className="relative w-full h-9/12">
                            <LineChartFRC
                              data={
                                RecentPlayer &&
                                RecentPlayer?.lastFiveMatches.length > 0
                                  ? [...RecentPlayer?.lastFiveMatches].reverse()
                                  : []
                              }
                              strokeClr={'green'}
                              width={
                                innerWidth > 700
                                  ? (innerWidth * 17) / 100
                                  : innerWidth > 500
                                  ? (innerWidth * 30) / 100
                                  : (innerWidth * 30) / 100
                              }
                              height={innerWidth > 700 ? 200 : 170}
                            />
                            <div
                              className="absolute items-center  bottom-0 nowrap flex gap-1"
                              style={{ color: 'gray', left: 25 }}
                            >
                              <div className="flex items-center justify-center rounded-full bg-green h-2 w-2"></div>

                              <div style={{ fontSize: 9 }}>
                                {getLangText(
                                  lang,
                                  words,
                                  'DreamTeam_Appearance',
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div></div>
                </div>
                {RecentPlayer && RecentPlayer?.lastFiveMatches.length > 0 && (
                  <div className="w-full md:w-1/2 lg:w-1/2  md:px-2 lg:px-2 h-full pt-3">
                    <div className="bg-white w-full   relative  dark:bg-gray dark:text-white border  dark:border-none  rounded-md h-full  p-3 ">
                      <Heading
                        heading={getLangText(
                          lang,
                          words,
                          'RECENT PERFORMANCES',
                        )}
                      />

                      <div className=" flex flex-col items-center justify-center p-2 dark:text-white w-full  mt-3 overflow-x-scroll ">
                        <div className="flex   item-center justify-center text-xs  w-full ">
                          <div className=" w-1/3 item-center  bg-gray-4 rounded-tl-lg flex items-center justify-center  text-xs p-1 uppercase ">
                            {getLangText(lang, words, 'OPPOSITION')}
                          </div>
                          <div className="w-1/3  bg-gray-4  flex items-center justify-center text-xs p-1 uppercase ">
                            {getLangText(lang, words, 'BATTING')}
                          </div>

                          <div className="w-1/3  bg-gray-4 flex items-center justify-center text-xs  uppercase  p-1">
                            {getLangText(lang, words, 'BOWLING')}
                          </div>

                          <div className="w-1/3  bg-gray-4  flex items-center justify-center uppercase   rounded-tr-lg text-xs p-1">
                            {getLangText(lang, words, 'POINTS')}
                          </div>
                        </div>

                        {RecentPlayer?.lastFiveMatches.map((item, i) => {
                          return (
                            <div
                              className="flex  w-full  h-12 item-center justify-center  text-center "
                              key={i}
                            >
                              <div className="w-1/3  flex items-center  border-b dark:border-black  ">
                                <div className="ml-2  flex items-center justify-center ">
                                  <ImageWithFallback
                                    height={18}
                                    width={18}
                                    loading="lazy"
                                    fallbackSrc={flagPlaceHolder}
                                    className="h-5 object-cover object-top w-8  
                                          rounded"
                                    src={`https://images.cricket.com/teams/${item.oppTeamID}_flag_safari.png`}
                                    alt=""
                                  />
                                  <span className="ml-2 text-xs text-gray-2">
                                    {' '}
                                    {item.oppTeamName}
                                  </span>
                                </div>
                              </div>
                              <div className="w-1/3 border-b dark:border-black   flex items-center  flex-col justify-center  text-gray-2 text-xs ">
                                <div>
                                  <span>
                                    {item.batting_stats.split('&')[0]}
                                  </span>
                                  {item.batting_stats.split('&').length > 1 && (
                                    <span>&</span>
                                  )}
                                </div>

                                {item.batting_stats.split('&')[1]}
                              </div>

                              <div className=" w-1/3 border-b dark:border-black   item-center  rounded-tl-lg   p-1 uppercase   flex flex-col items-center justify-center  text-gray-2 text-xs">
                                <div>
                                  <span>
                                    {item.bowling_stats.split('&')[0]}
                                  </span>
                                  {item.bowling_stats.split('&').length > 1 && (
                                    <span>&</span>
                                  )}
                                </div>
                                <div className="pt1">
                                  {item.bowling_stats.split('&')[1]}
                                </div>
                              </div>

                              <div className=" w-1/3 border-b dark:border-black  item-center justify-center rounded-tl-lg flex items-center text-xs p-1 uppercase      ">
                                <div
                                  className={` ml-2 w-50 oswald font-bold ${
                                    item.dta_flag ? 'text-green' : 'text-white'
                                  } flex justify-end fw6 f7`}
                                >
                                  {item.points}
                                </div>
                                {item.dta_flag && (
                                  <div className=" pt-1 px-1 ">
                                    <div className="flex items-center justify-center rounded-full bg-green  text-green h-1 w-1    text-2xl"></div>
                                  </div>
                                )}
                              </div>
                            </div>
                          )
                        })}
                        <div
                          className="absolute items-center  nowrap flex gap-1"
                          style={{ color: 'gray', left: 20, bottom: 8 }}
                        >
                          <div className="flex items-center justify-center rounded-full bg-green h-2 w-2"></div>
                          <div className="text-xs">
                            {getLangText(lang, words, 'DreamTeam_Appearance')}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                {RecentPlayer &&
                  RecentPlayer?.performanceIndicator &&
                  (RecentPlayer?.performanceIndicator.overallRPI ||
                    RecentPlayer?.performanceIndicator.overallWPI) && (
                    <div className="w-full  md:w-1/2 lg:w-1/2 md:px-2 lg:px-2 pt-3">
                      <div className="flex flex-col gap-2 w-full p-2 border  bg-white  dark:text-white h-min dark:border-none dark:bg-gray rounded-md">
                        <Heading
                          heading={getLangText(
                            lang,
                            words,
                            'PERFORMANCE INDICATOR',
                          )}
                        />

                        <PerformanceIndicator
                          width={500}
                          height={400}
                          top={20}
                          bottom={30}
                          left={30}
                          right={0}
                          playerRole={RecentPlayer && RecentPlayer?.playerSkill}
                          data={
                            RecentPlayer && RecentPlayer?.performanceIndicator
                              ? RecentPlayer?.performanceIndicator
                              : {}
                          }
                          runs={getLangText(lang, words, 'RUN PER INNINGS')}
                          wickets={getLangText(
                            lang,
                            words,
                            'WICKETS PER INNINGS',
                          )}
                          lang={lang}
                        />
                      </div>
                    </div>
                  )}

                {RecentPlayer && RecentPlayer?.highLowFlag && (
                  <div className=" w-full md:w-1/2 lg:w-1/2 md:px-2 lg:px-2 pt-3 ">
                    <div className="border bg-white  dark:border-none dark:bg-gray dark:text-white p-3 rounded-md">
                      <div className="flex flex-col justify-start items-start pv2  ph2">
                        <Heading
                          heading={getLangText(lang, words, 'HIGHS & LOWS')}
                        />
                        {/* <div className=' captilize text-base font-bold   '>
                                 {getLangText(lang, words, 'HIGHS & LOWS')}
                              </div>
                              <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded' /> */}
                      </div>

                      <div className="mh2 bb b--white-20"></div>
                      {lang === 'HIN' ? (
                        <div>
                          {RecentPlayer &&
                            (RecentPlayer?.strengthInHindi.stmt1 ||
                              RecentPlayer?.strengthInHindi.stmt2 ||
                              RecentPlayer?.strengthInHindi.stmt3) &&
                            (RecentPlayer?.weaknessInHindi.stmt1 ||
                              RecentPlayer?.weaknessInHindi.stmt2 ||
                              RecentPlayer?.weaknessInHindi.stmt3) && (
                              <div className="bg-dark-gray  flex flex-col items-start justify-start ma3 br3 ">
                                <div
                                  className="br2 flex w-50 mv2 items-center cursor-pointer justify-center bg-gray-4 text-white"
                                  onClick={() =>
                                    setquality(
                                      quality === 'strength'
                                        ? 'weakness'
                                        : 'strength',
                                    )
                                  }
                                >
                                  <div
                                    className={`w-50 flex items-center br2 ${
                                      quality === 'strength'
                                        ? 'bg-frc-yellow black'
                                        : 'bg-gray-4 text-white'
                                    } pv2 ph2  justify-center  fw6  f8 f7-l f7-m`}
                                  >
                                    {' '}
                                    {getLangText(lang, words, 'STRENGTH')}
                                  </div>
                                  <div
                                    className={`w-50  flex items-center br2 ph2 ${
                                      quality === 'weakness'
                                        ? 'bg-frc-yellow black'
                                        : 'bg-gray-4 text-white'
                                    } pv2 justify-center  fw6  f8 f7-l f7-m`}
                                  >
                                    {getLangText(lang, words, 'WEAKNESS')}
                                  </div>
                                </div>

                                <div className=" flex flex-col items-start justify-start ">
                                  {['stmt1', 'stmt2', 'stmt3'].map(
                                    (stmt, i) => (
                                      <>
                                        {RecentPlayer &&
                                          RecentPlayer[
                                            quality === 'strength'
                                              ? 'strengthInHindi'
                                              : 'weaknessInHindi'
                                          ][stmt] && (
                                            <div
                                              key={i}
                                              className="tc  white    pv1 w-70  f8 f6-l f6-m lh-copy"
                                            >
                                              {`${
                                                RecentPlayer[
                                                  quality === 'strength'
                                                    ? 'strengthInHindi'
                                                    : 'weaknessInHindi'
                                                ][stmt]
                                              }`
                                                .split('$')
                                                .map((word, j) => (
                                                  <span
                                                    key={j}
                                                    className={`  ${
                                                      j % 2 === 0
                                                        ? ' fw4 '
                                                        : ' ph1 fw6'
                                                    }`}
                                                  >
                                                    {word}
                                                  </span>
                                                ))}
                                            </div>
                                          )}
                                      </>
                                    ),
                                  )}
                                </div>
                              </div>
                            )}
                        </div>
                      ) : (
                        <div>
                          {RecentPlayer &&
                            (RecentPlayer?.strength.stmt1 ||
                              RecentPlayer?.strength.stmt2 ||
                              RecentPlayer?.strength.stmt3) &&
                            (RecentPlayer?.weakness.stmt1 ||
                              RecentPlayer?.weakness.stmt2 ||
                              RecentPlayer?.weakness.stmt3) && (
                              <div className="  flex flex-col items-center justify-center m-2 rounded ">
                                <div
                                  className="rounded-full w-2/3 p-1 flex  my-2 items-center  justify-center dark:bg-gray-4 text-white"
                                  onClick={() =>
                                    setquality(
                                      quality === 'strength'
                                        ? 'weakness'
                                        : 'strength',
                                    )
                                  }
                                >
                                  <div
                                    className={`w-6/12 text-xs  uppercase rounded-full  flex items-center 
                                             ${
                                               quality === 'strength'
                                                 ? 'bg-basered dark:bg-gray-4 dark:border dark:border-green text-white'
                                                 : 'bg-light_gray dark:bg-gray-4 text-white '
                                             } p-2 justify-center  font-semibold cursor-pointer `}
                                  >
                                    {' '}
                                    {getLangText(lang, words, 'STRENGTH')}
                                  </div>
                                  <div
                                    className={` w-6/12 rounded-full uppercase flex items-center  ph2 ${
                                      quality === 'weakness'
                                        ? 'bg-gray-4 border border-green '
                                        : 'bg-gray-4 text-white '
                                    } cursor-pointer p-2 justify-center  font-semibold text-xs`}
                                  >
                                    {getLangText(lang, words, 'WEAKNESS')}
                                  </div>
                                </div>

                                <div className=" flex flex-col items-center justify-center  w-full ">
                                  {['stmt1', 'stmt2', 'stmt3'].map(
                                    (stmt, i) =>
                                      RecentPlayer &&
                                      RecentPlayer[quality][stmt] && (
                                        <div className="flex items-center justify-start w-full">
                                          <div className="w-2 h-2 rounded-full bg-green mx-1"></div>

                                          <div
                                            key={i}
                                            className=" py-2 text-xs  "
                                          >
                                            {`${RecentPlayer[quality][stmt]}`
                                              .split('$')
                                              .map((word, j) => (
                                                <>
                                                  <span
                                                    key={j}
                                                    className={`  ${
                                                      j % 2 === 0
                                                        ? ' fw4 '
                                                        : ' ph1 fw6'
                                                    }`}
                                                  >
                                                    {word}
                                                  </span>
                                                </>
                                              ))}
                                          </div>
                                        </div>
                                      ),
                                  )}
                                </div>
                              </div>
                            )}
                          {lang === 'HIN' ? (
                            <span className="text-xs text-gray-2">{`*  पिछले 5 वर्षों के ${PlayerStatHubData.playerHub.compType}  डेटा`}</span>
                          ) : (
                            <span className="text-xs text-gray-2">{`* ${PlayerStatHubData.playerHub.compType} data from last 5 years`}</span>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                )}
                {RecentPlayer &&
                  RecentPlayer?.bowlingDetails &&
                  RecentPlayer?.bowlingDetails.bowlingType &&
                  RecentPlayer?.bowlingDetails.bowlingType.length > 0 && (
                    <div className="w-full md:w-1/2 lg:w-1/2 md:px-2 lg:px-2 pt-3 ">
                      <div className="w-full border  dark:text-white dark:border-none bg-white dark:bg-gray p-3  rounded-md ">
                        <Heading
                          heading={getLangText(
                            lang,
                            words,
                            'AGAINST BOWLING TYPE',
                          )}
                        />
                        {/* <div className="pa2 ttu f6 fw6 ttu tc text-left">{getLangText(lang, words, 'AGAINST BOWLING TYPE')}</div>
                              <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded' /> */}
                        <div className="flex w-full mt-3 items-center justify-center cursor-pointer cursor-pointer p-1 bg-gray-8 rounded-full">
                          {['S/R', 'AVERAGE', 'DISMISSALS'].map((type, i) => (
                            <div
                              onClick={() => setBowlingType(type)}
                              className={`w-4/12 cursor-pointer text-center text-xs font-semibold uppercase f7 p-2   ${
                                type === bowlingType
                                  ? 'bg-gray-8 black  border-green  rounded-full'
                                  : 'bg-gray-8 white border-transparent rounded-full '
                              } border mont-semibold b--black  text-white font-semidbold`}
                              key={i}
                            >
                              {getLangText(lang, words, type)}
                            </div>
                          ))}
                        </div>
                        <RecentPlayerBowlingType
                          RecentPlayer={RecentPlayer}
                          bowlingType={bowlingType}
                          language={lang}
                        />
                        {/* {RecentPlayer && RecentPlayer?.bowlingDetails &&
                              RecentPlayer?.bowlingDetails.bowlingType && RecentPlayer?.bowlingDetails.bowlingType.length > 0 && RecentPlayer?.bowlingDetails.bowlingType.map((compare, i) =>
                                 <div className="ph3 pv2" key={i}>
                                    <div className="flex pv2 items-center justify-between">
                                       <div className="fw4 f8"><span className="gray">v</span><span className="white ttu pl1">{compare.types}</span></div>
                                       <div className="fw7 white f8">{bowlingType === "S/R" ? Number(compare.bowlingSR) : bowlingType === "AVERAGE" ? Number(compare.bowlingAvg) : Number(compare.Dismissals)}</div>
                                    </div>
                                    { / Number(RecentPlayer?.bowlingDetails.maxDismissals))}
                                    { : bowlingType === "AVERAGE" ? Number(compare.bowlingAvg) : Number(compare.Dismissals) / bowlingType === "S/R" ? Number(RecentPlayer?.bowlingDetails.maxSR) : bowlingType === "AVERAGE" ? Number(RecentPlayer?.bowlingDetails.maxAvg) : Number(RecentPlayer?.bowlingDetails.maxDismissals)) * 100)}
                                    <div
                                       className={`  br2  relative items-center w-100 z-0 `}
                                       style={{ height: 6, backgroundColor: "#333a46" }}>
                                       <div
                                          className={`absolute h-100 br2  br--left left-0 bg-frc-yellow `}
                                          style={{
                                             width: `${bowlingType === "DISMISSALS" ? Number(compare.Dismissals) / Number(RecentPlayer?.bowlingDetails.maxDismissals) * 100 : bowlingType === "AVERAGE" ? Number(compare.bowlingAvg) / Number(RecentPlayer?.bowlingDetails.maxAvg) * 100 : Number(compare.bowlingSR) / Number(RecentPlayer?.bowlingDetails.maxSR) * 100}%`,


                                             zIndex: -1
                                          }}></div>
                                    </div>
                                 </div>
                              )} */}
                      </div>
                    </div>
                  )}

                {RecentPlayer &&
                  RecentPlayer?.battingDetails &&
                  RecentPlayer?.battingDetails.battingType &&
                  RecentPlayer?.battingDetails.battingType.length > 0 && (
                    <div className="w-full md:w-1/2 lg:w-1/2 md:px-2 lg:px-2 pt-3">
                      <div className="bg-white  border  dark:border-none dark:bg-gray rounded-md p-3">
                        <Heading
                          heading={getLangText(
                            lang,
                            words,
                            'AGAINST BATTING TYPE',
                          )}
                        />
                        {/* <div className="p-3 uppercase  text-left"> {getLangText(lang, words, 'AGAINST BATTING TYPE')}</div>
                              <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded' /> */}
                        <div className="flex  mt-3 mx-2 items-center justify-center p-1 bg-gray-8 rounded-full">
                          {['WICKETS', 'AVERAGE', 'ECONOMY'].map((type, i) => (
                            <div
                              onClick={() => setBattingType(type)}
                              className={`w-4/12 cursor-pointer text-center text-xs font-semibold uppercase  p-2 ${
                                type === battingType
                                  ? 'bg-gray-8 black  border-green  rounded-full'
                                  : 'bg-gray-8 white border-transparent rounded-full '
                              }  border mont-semiBold b--black  text-white font-semidbold`}
                              key={i}
                            >
                              {getLangText(lang, words, type)}
                            </div>
                          ))}
                        </div>
                        <RecentPlayerBattingType
                          RecentPlayer={RecentPlayer}
                          battingType={battingType}
                          language={lang}
                        />
                        {/* {RecentPlayer && RecentPlayer?.battingDetails &&
                                 RecentPlayer?.battingDetails.battingType && RecentPlayer?.battingDetails.battingType.length > 0 && RecentPlayer?.battingDetails.battingType.map((compare, i) =>
                                    <div className="ph3 pv2" key={i}>
                                       <div className="flex pv2 items-center justify-between">
                                          <div className="fw4 f8"><span className="gray">v</span><span className="white ttu pl1">{compare.types}</span></div>
                                          <div className="fw7 white f8">{battingType === "WICKETS" ? Number(compare.wickets) : battingType === "AVERAGE" ? Number(compare.battingAvg) : Number(compare.economyRate)}</div>
                                       </div>

                                       <div
                                          className={`  br2  relative items-center w-100 z-0 `}
                                          style={{ height: 6, backgroundColor: "#333a46" }}>
                                          <div
                                             className={`absolute h-100 br2  br--left left-0 bg-frc-yellow `}
                                             style={{
                                                width: `${battingType === "WICKETS" ? Number(compare.wickets) / Number(RecentPlayer?.battingDetails.maxWickets) * 100 : battingType === "AVERAGE" ? Number(compare.battingAvg) / Number(RecentPlayer?.battingDetails.maxAvg) * 100 : Number(compare.economyRate) / Number(RecentPlayer?.battingDetails.maxEconomy) * 100}%`,


                                                zIndex: -1
                                             }}></div>
                                       </div>
                                    </div>
                                 )} */}
                      </div>
                    </div>
                  )}

                {MatchUpsData &&
                  MatchUpsData.matchupsById &&
                  MatchUpsData.matchupsById.matchUpData &&
                  MatchUpsData.matchupsById.matchUpData.length > 0 && (
                    <Matcups
                      matchID={matchID}
                      closeNewModelV2={closeNewModelV2}
                      playersList={
                        RecentPlayer &&
                        MatchUpsData &&
                        MatchUpsData.matchupsById &&
                        MatchUpsData.matchupsById.matchUpData.filter(
                          (player) => player.player1 === RecentPlayer?.playerID,
                        )
                      }
                      choosePlayer={choosePlayer}
                      runs={
                        RecentPlayer &&
                        MatchUpsData &&
                        MatchUpsData.matchupsById &&
                        MatchUpsData.matchupsById.matchUpData.filter(
                          (pl) => pl.player1 === RecentPlayer?.playerID,
                        ).length > 0
                          ? MatchUpsData.matchupsById.matchUpData.filter(
                              (pl) => pl.player1 === RecentPlayer?.playerID,
                            )[0].runsScored
                          : 0
                      }
                      wickets={
                        RecentPlayer &&
                        MatchUpsData &&
                        MatchUpsData.matchupsById &&
                        MatchUpsData.matchupsById.matchUpData.filter(
                          (pl) => pl.player1 === RecentPlayer?.playerID,
                        ).length > 0
                          ? MatchUpsData.matchupsById.matchUpData.filter(
                              (pl) => pl.player1 === RecentPlayer?.playerID,
                            )[0].Dismissals
                          : 0
                      }
                      lang={lang}
                      ballFaced={
                        RecentPlayer &&
                        MatchUpsData &&
                        MatchUpsData.matchupsById &&
                        MatchUpsData.matchupsById.matchUpData.filter(
                          (pl) => pl.player1 === RecentPlayer?.playerID,
                        ).length > 0
                          ? MatchUpsData.matchupsById.matchUpData.filter(
                              (pl) => pl.player1 === RecentPlayer?.playerID,
                            )[0].ballsFaced
                          : 0
                      }
                      RecentPlayer={RecentPlayer && RecentPlayer}
                      Player2Obj={
                        !MatchUpsPlayer2 &&
                        RecentPlayer &&
                        MatchUpsData &&
                        MatchUpsData.matchupsById &&
                        MatchUpsData.matchupsById.matchUpData.length > 0 &&
                        MatchUpsData.matchupsById.matchUpData.filter(
                          (pl) => pl.player1 === RecentPlayer?.playerID,
                        ).length > 0
                          ? MatchUpsData.matchupsById.matchUpData.filter(
                              (pl) => pl.player1 === RecentPlayer?.playerID,
                            )[0]
                          : MatchUpsPlayer2
                      } //do same for matchups
                      SelectionType={SelectionType}
                    />
                  )}

                {MatchUpsData &&
                  MatchUpsData.matchupsById &&
                  MatchUpsData.matchupsById.matchUpData &&
                  MatchUpsData.matchupsById.matchUpData.length > 0 &&
                  RecentPlayer &&
                  RecentPlayer?.threat && (
                    <div className="w-full md:w-1/2 lg:w-1/2 md:px-2 lg:px-2 pt-3">
                      <div className="rounded-lg bg-white dark:text-white border dark:bg-gray dark:border-none">
                        <div className="text-white bg-gray-4 rounded-t-lg p-2">
                          {lang === 'HIN' ? 'खतरा' : 'Threats'}
                        </div>
                        <div className="flex w-full p-3">
                          <div className="w-1/3 flex justify-center ">
                            <div className="h-24 w-24 flex justify-center items-center rounded-full bg-light_gray dark:bg-gray-8">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className="w-full h-full object-cover rounded-full object-top"
                                src={`https://images.cricket.com/players/${
                                  RecentPlayer &&
                                  RecentPlayer?.threatOppPlayerID
                                }_headshot_safari.png`}
                              />
                            </div>
                          </div>
                          <div className="w-2/3  flex items-center justify-center rounded-md">
                            <div className="text-sm font-semibold">
                              {' '}
                              {lang === 'HIN' ? (
                                <div>
                                  {`${RecentPlayer?.threatInHindi}`
                                    .split('$')
                                    .map((word, j) => (
                                      <span
                                        key={j}
                                        className={`  ${
                                          j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'
                                        }`}
                                      >
                                        {word}
                                      </span>
                                    ))}
                                </div>
                              ) : (
                                <div>
                                  {`${RecentPlayer?.threat}`
                                    .split('$')
                                    .map((word, j) => (
                                      <span
                                        key={j}
                                        className={`  ${
                                          j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'
                                        }`}
                                      >
                                        {word}
                                      </span>
                                    ))}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                <div className="w-full md:w-1/2 lg:w-1/2 dark:text-white rounded  mt-3 ">
                  <div className="w-full   dark:bg-gray-8 dark:border-none dark:text-white rounded-md ">
                    <Heading heading={getLangText(lang, words, 'COMPARE')} />
                    {/* <div className=' uppercase text-left ml-2'>{getLangText(lang, words, 'COMPARE')}</div>
                        <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 ml-2 rounded mb-2'></div> */}
                    {/* <div className=' f5-l f5-m   pa2  ttu f6 lh-title fw6  capitalize font-bold mt-3 ml-2   '> {getLangText(lang, words, 'COMPARE')}  </div> */}

                    <div className=" flex mt-3    items-center justify-between ">
                      <div className="w-5/12 h-48  cursor-pointer   bg-white  dark:bg-gray dark:text-white  flex  items-center justify-center  rounded-md py-3">
                        <div className="  flex gap-3  flex-col items-center w-full justify-center shadow-4 pa2">
                        <div className="h4 relative flex-col   justify-center items-center">
                            <div className="flex  overflow-hidden items-center  w-100 justify-center  ">
                              <ImageWithFallback
                                height={58}
                                width={58}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className=" rounded-full  h-28 w-28 bg-gray-10 dark:bg-gray-8   object-cover object-top"
                                src={`https://images.cricket.com/players/${
                                  RecentPlayer && RecentPlayer?.playerID
                                }_headshot.png`}
                                alt=""
                              />
                            </div>

                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc={flagPlaceHolder}
                              className="absolute bottom-1 object-cover  w-6 h-6 rounded-full"
                              style={{ left: 2 }}
                              src={`https://images.cricket.com/teams/${
                                RecentPlayer && RecentPlayer?.teamID
                              }_flag_safari.png`}
                              alt=""
                              // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            />
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc="/svgs/images/flag_empty.svg"
                              className=" absolute h-6 w-6  border border-white bg-gray-4 p-1 rounded-lg  right-0 bottom-1 object-fill"
                              src={
                                RecentPlayer &&
                                RecentPlayer?.playerSkill === 'ALL_ROUNDER'
                                  ? '/pngsV2/allrounder-line-white.png'
                                  : RecentPlayer?.playerSkill === 'BATSMAN'
                                  ? '/pngsV2/battericon.png'
                                  : RecentPlayer?.playerSkill === 'KEEPER'
                                  ? '/pngsV2/keeper-line-white.png'
                                  : '/pngsV2/bowlericon.png'
                              }
                            />
                          </div>
                          

                          <div className="h2-5  pb3 pt2">
                            <div className="flex  items-center  w-full justify-center white f7 pv2 text-center text-sm">
                              {RecentPlayer &&
                                (lang === 'HIN'
                                  ? RecentPlayer?.playerNameHindi
                                  : RecentPlayer?.name)}
                            </div> 
                          </div>
                        </div>
                      </div>

                      <div className="w-1/12 mx-2 flex   items-center   justify-center">
                        <ImageWithFallback
                          height={28}
                          width={28}
                          loading="lazy"
                          className="h-10"
                          src={compareGreen}
                          alt=""
                        />
                      </div>

                      <div
                        className="w-5/12 relative h-48 cursor-pointer   bg-white  dark:bg-gray dark:text-white  rounded-md flex py-3 items-center justify-center"
                        onClick={() => {
                          setCompareModel(true)
                          setSelectionType('compare')
                        }}
                      >
                        <div className="  flex w-full gap-3 flex-col items-center  w-100 justify-center shadow-4 pa2">
                          {/* <div className="flex w-full items-end justify-end relative "> */}
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className=" absolute w-4 mr-3 pt-2 top-0 right-0 -bottom-5"
                            src={compare}
                            alt=""
                          />
                          {/* </div> */}
                          <div className="h4 relative flex-col   justify-center items-center">
                            <div className="flex   items-center  w-full justify-center ">
                              <ImageWithFallback
                                height={58}
                                width={58}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className=" rounded-full  h-28 w-28 bg-gray-8   object-cover object-top"
                                src={`https://images.cricket.com/players/${
                                  Player2Compare && Player2Compare?.playerID
                                }_headshot.png`}
                                alt=""
                              />
                            </div>

                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc={flagPlaceHolder}
                              className="absolute bottom-1 object-cover    rounded-full w-6 h-6  left-0 ba b--black h08  "
                              style={{ left: 2 }}
                              src={`https://images.cricket.com/teams/${
                                Player2Compare && Player2Compare?.teamID
                              }_flag_safari.png`}
                              alt=""
                              // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            />
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc="/svgs/images/flag_empty.svg"
                              className=" absolute h-6 w-6  border border-white bg-gray-4 p-1 rounded-lg  right-0 bottom-1 "
                              src={
                                Player2Compare &&
                                Player2Compare?.playerSkill === 'ALL_ROUNDER'
                                  ? '/pngsV2/allrounder-line-white.png'
                                  : Player2Compare?.playerSkill === 'BATSMAN'
                                  ? '/pngsV2/battericon.png'
                                  : Player2Compare?.playerSkill === 'KEEPER'
                                  ? '/pngsV2/keeper-line-white.png'
                                  : '/pngsV2/bowlericon.png'
                              }
                            />
                          </div>
                          

                          <div className="h2-5 pb3 pt2">
                            <div className="flex  items-center  w-full justify-center white f7 pv2 tc fw6 text-center text-sm">
                              {Player2Compare &&
                                (lang === 'HIN'
                                  ? Player2Compare?.playerNameHindi
                                  : Player2Compare?.name)}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className=" mt-3 text-black dark:text-white">
                      {
                        <div className="  bg-white  dark:bg-gray dark:text-white  rounded-lg">
                          {[1].map((stat, i) => (
                            <div className="mx-3" key={i}>
                              <div className="flex  items-center justify-center py-3">
                                <div
                                  className={`w-4/12 flex items-center justify-center  text-2xl  font-bold    ${
                                    RecentPlayer &&
                                    Player2Compare &&
                                    handlecssPlayer1(
                                      'fantasy',
                                      stat,
                                      RecentPlayer?.avgFantasyPoints,
                                      Player2Compare?.avgFantasyPoints,
                                    )
                                  }`}
                                >
                                  {RecentPlayer &&
                                  RecentPlayer?.avgFantasyPoints !== ''
                                    ? RecentPlayer?.avgFantasyPoints
                                    : '--'}
                                </div>
                                <div className="w-4/12 flex items-center  justify-center     text-base  font-bold border-x dark:border-black dark:border-black">
                                  <div className=" text-center  font-bold text-gray-2 text-xs ">
                                    {lang === 'HIN' ? 'औसत' : 'Average'} <br />
                                    {lang === 'HIN'
                                      ? 'फ़ैंटसी अंक'
                                      : 'Fantasy Points'}
                                  </div>
                                </div>
                                <div
                                  className={`w-4/12 flex font-bold items-center justify-center text-2xl text-gray-2  ${
                                    RecentPlayer &&
                                    Player2Compare &&
                                    handlecssPlayer2(
                                      'fantasy',
                                      stat,
                                      RecentPlayer?.avgFantasyPoints,
                                      Player2Compare?.avgFantasyPoints,
                                    )
                                  }`}
                                >
                                  {Player2Compare &&
                                  Player2Compare?.avgFantasyPoints !== ''
                                    ? Player2Compare?.avgFantasyPoints
                                    : '--'}
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                      }
                      {RecentPlayer &&
                      RecentPlayer?.playerSkill === 'BOWLER' ? (
                        <>
                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className="text-sm capitalize font-semibold">
                                {getLangText(lang, words, 'BOWLING STATS')}
                              </div>

                              <div className="border mt-2 rounded-lg p-2 bg-white dark:bg-gray dark:border-none">
                                {[
                                  'bowlingWickets',
                                  'bowlingStrikeRate',
                                  'bowlingEconomyRate',
                                ].map((stat, i) => (
                                  <div
                                    className={`${
                                      i !== 2 ? 'mh3 bb b--white-20' : 'mh3'
                                    }`}
                                    key={i}
                                  >
                                    <div className="flex items-center justify-center my-2">
                                      <div
                                        className={`w-4/12 flex items-center justify-center  text-2xl oswald font-bold  
                                             ${
                                               RecentPlayer &&
                                               Player2Compare &&
                                               handlecssPlayer1(
                                                 'bowling',
                                                 stat,
                                                 RecentPlayer
                                                   .statsHubPlayerbowling[stat],
                                                 Player2Compare
                                                   .statsHubPlayerbowling[stat],
                                               )
                                             }`}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbowling &&
                                        RecentPlayer?.statsHubPlayerbowling[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ]
                                          : '--'}
                                      </div>
                                      <div className="w-4/12 flex items-center justify-center  text-uppercase text-sm border-x dark:border-black dark:border-black">
                                        {stat ===
                                        'bowlingBestBowlingTotalSpell' ? (
                                          <div className="text-gray-2 ">
                                            best
                                            <br /> figures
                                          </div>
                                        ) : stat === 'bowlingEconomyRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'इकॉनमी'
                                              : 'ECONOMY'}
                                          </div>
                                        ) : stat === 'bowlingStrikeRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'SR'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति पारी'
                                              : 'Wickets per'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' विकेट'
                                              : 'innings'}
                                          </div>
                                        )}
                                      </div>
                                      <div
                                        className={`w-4/12 flex items-center justify-center   text-2xl oswald font-bold  text-gray-2  ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'bowling',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbowling[stat],
                                          )
                                        }`}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbowling &&
                                        Player2Compare?.statsHubPlayerbowling[
                                          stat
                                        ]
                                          ? Player2Compare
                                              ?.statsHubPlayerbowling[stat]
                                          : '-'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}

                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className="text-sm capitalize font-semibold">
                                {getLangText(lang, words, 'BATTING STATS')}
                              </div>

                              <div className="border mt-2 rounded-lg p-2 bg-white dark:bg-gray dark:border-none">
                                {[
                                  'commonBatPosition',
                                  'battingRunsInnings',
                                  'battingStrikeRate',
                                  'battingFoursSixes',
                                ].map((stat, i) => (
                                  <div
                                    className={`${
                                      i !== 3 ? 'mh3 bb b--white-20' : 'mh3'
                                    }`}
                                    key={i}
                                  >
                                    <div className="flex  items-center justify-center my-2">
                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl oswald font-bold   ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer1(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        } `}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbatting &&
                                        RecentPlayer?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ]
                                          : '-'}
                                      </div>
                                      <div className="w-4/12 flex items-center text-sm justify-center uuppercase border-x dark:border-black">
                                        {stat === 'battingAverage' ? (
                                          <div className="text-gray-2 ">
                                            {getLangText(
                                              lang,
                                              words,
                                              'AVERAGE',
                                            )}
                                          </div>
                                        ) : stat === 'battingFoursSixes' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति'
                                              : 'Boundaries'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' मैच बाउंड्री'
                                              : 'per match'}
                                          </div>
                                        ) : stat === 'battingHighestScore' ? (
                                          <div className="f5  fw6 lh-copy oswald">
                                            {' '}
                                            Highest <br /> Score
                                          </div>
                                        ) : stat === 'battingRunsInnings' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN' ? 'प्रति' : 'Runs'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'पारी रन'
                                              : 'per innings'}
                                          </div>
                                        ) : stat === 'commonBatPosition' ? (
                                          <div className="text-gray-2 text-xs">
                                            {lang === 'HIN'
                                              ? 'सामान्य'
                                              : 'COMMON'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'बल्लेबाजी की स्थिति'
                                              : 'BAT POSITION'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'S/R'}
                                          </div>
                                        )}
                                      </div>

                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl oswald font-bold   ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        } `}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbatting &&
                                        Player2Compare?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? Player2Compare
                                              ?.statsHubPlayerbatting[stat]
                                          : '-'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                        </>
                      ) : (
                        <>
                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className=" text-sm capitalize  font-bold mt-3">
                                {getLangText(lang, words, 'BATTING STATS')}
                              </div>

                              <div className="border mt-2 rounded-lg p-2 bg-white dark:bg-gray dark:border-none">
                                {[
                                  'commonBatPosition',
                                  'battingRunsInnings',
                                  'battingStrikeRate',
                                  'battingFoursSixes',
                                ].map((stat, i) => (
                                  <div
                                    className={`${i !== 3 ? 'mx-3 ' : 'mx-3'}`}
                                    key={i}
                                  >
                                    <div className="flex items-center justify-center my-2">
                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl oswald font-bold    ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer1(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        }`}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbatting &&
                                        RecentPlayer?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ]
                                          : '--'}
                                      </div>
                                      <div className="w-4/12 border-x dark:border-black flex items-center  justify-center   text-xs font-semibold oswald text-center">
                                        {stat === 'battingAverage' ? (
                                          <div className="text-gray-2 text-xs">
                                            {getLangText(
                                              lang,
                                              words,
                                              'AVERAGE',
                                            )}
                                          </div>
                                        ) : stat === 'battingFoursSixes' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति'
                                              : 'Boundaries'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' मैच बाउंड्री'
                                              : 'per match'}
                                          </div>
                                        ) : stat === 'battingHighestScore' ? (
                                          <div className="f5  fw6 lh-copy oswald">
                                            {' '}
                                            Highest <br /> Score
                                          </div>
                                        ) : stat === 'battingRunsInnings' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN' ? 'प्रति' : 'Runs'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'पारी रन'
                                              : 'per innings'}
                                          </div>
                                        ) : stat === 'commonBatPosition' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'सामान्य'
                                              : 'COMMON'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'बल्लेबाजी की स्थिति'
                                              : 'BAT POSITION'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'S/R'}
                                          </div>
                                        )}
                                      </div>

                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl   oswald font-bold   ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        } `}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbatting &&
                                        Player2Compare?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? Player2Compare
                                              ?.statsHubPlayerbatting[stat]
                                          : '--'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className=" capitalize text-sm font-bold mt-3">
                                {getLangText(lang, words, 'BOWLING STATS')}
                              </div>

                              <div className="my-1 p-2  bg-white  dark:bg-gray dark:text-white  rounded-lg">
                                {[
                                  'bowlingWickets',
                                  'bowlingStrikeRate',
                                  'bowlingEconomyRate',
                                ].map((stat, i) => (
                                  <div
                                    className={`${
                                      i !== 2 ? 'mh3 bb b--white-20' : 'mh3'
                                    }`}
                                    key={i}
                                  >
                                    <div className="flex  items-center justify-center my-2">
                                      <div
                                        className={`w-4/12 flex items-center justify-center   text-2xl oswald font-bold  
                                             ${
                                               RecentPlayer &&
                                               Player2Compare &&
                                               handlecssPlayer1(
                                                 'bowling',
                                                 stat,
                                                 RecentPlayer
                                                   .statsHubPlayerbowling[stat],
                                                 Player2Compare
                                                   .statsHubPlayerbowling[stat],
                                               )
                                             }`}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbowling &&
                                        RecentPlayer?.statsHubPlayerbowling[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ]
                                          : '--'}
                                      </div>
                                      <div className="w-4/12 flex items-center justify-center  oswald border-x dark:border-black text-center text-sm text-gray-2 font-bold">
                                        {stat ===
                                        'bowlingBestBowlingTotalSpell' ? (
                                          <div className="text-gray-2 text-base">
                                            best
                                            <br /> figures
                                          </div>
                                        ) : stat === 'bowlingEconomyRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'इकॉनमी'
                                              : 'ECONOMY'}
                                          </div>
                                        ) : stat === 'bowlingStrikeRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'S/R'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति पारी'
                                              : 'Wickets per'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' विकेट'
                                              : 'innings'}
                                          </div>
                                        )}
                                      </div>
                                      <div
                                        className={`w-4/12 flex items-center justify-center    text-2xl font-semibold oswald  fw6  ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'bowling',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbowling[stat],
                                          )
                                        }`}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbowling &&
                                        Player2Compare?.statsHubPlayerbowling[
                                          stat
                                        ]
                                          ? Player2Compare
                                              ?.statsHubPlayerbowling[stat]
                                          : '--'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                        </>
                      )}
                    </div>
                  </div>
                </div>

                {openSelectionModelV2 && (
                  <PlayerModelV2
                    closeNewModelV2={closeNewModelV2}
                    playersList={PlayerStatHubData.playerHub.recentForm}
                    choosePlayer={choosePlayer}
                    RecentPlayer={RecentPlayer}
                    lang={lang}
                    Player2={
                      SelectionType === 'compare' ? Player2Compare : null
                    } //do same for matchups
                    SelectionType={SelectionType}
                  />
                )}

                {OpenModelMatchups && (
                  <MatchUpsModel
                    closeNewModelV2={closeNewModelV2}
                    playersList={
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (player) => player.player1 === RecentPlayer?.playerID,
                      )
                    }
                    choosePlayer={choosePlayer}
                    lang={lang}
                    RecentPlayer={RecentPlayer}
                    Player2Obj={
                      !MatchUpsPlayer2 &&
                      RecentPlayer &&
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.length > 0 &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (pl) => pl.player1 === RecentPlayer?.playerID,
                      ).length > 0
                        ? MatchUpsData.matchupsById.matchUpData.filter(
                            (pl) => pl.player1 === RecentPlayer?.playerID,
                          )[0]
                        : MatchUpsPlayer2
                    } //do same for matchups
                    SelectionType={SelectionType}
                  />
                )}

                {openCompareModel && (
                  <CompareModelPlayerHub
                    closeNewModelV2={closeNewModelV2}
                    playersList={PlayerStatHubData.playerHub.recentForm}
                    choosePlayer={choosePlayer}
                    lang={lang}
                    RecentPlayer={RecentPlayer}
                    Player2Obj={Player2Compare} //do same for matchups
                    SelectionType={SelectionType}
                  />
                )}
              </div>
            ) : (
              <div className="w-full br2 ba  b--white-20 ">
                <DataNotFound />
              </div>
            )}
          </div>

          {playerTeamIDList.length > 0 && (
            <div className="fixed md:hidden lg:hidden flex items-center justify-center bottom-2  md:bottom-2 lg:bottom-2 px-3  md:pr-0 lg:pr-0 w-full ">
              <div
                onClick={() => {
                  props.changeComponentState('BYTPlayerStathubState'),
                    props.playerScores(
                      PlayersList,
                      credits,
                      catPlayers,
                      playerTeamIDList,
                      'BYTPlayerStathubState',
                      RecentPlayer,
                    )
                }}
                className=" cursor-pointer hidescroll flex items-center text-white dark:text-black h-16  rounded-full bg-basered dark:bg-green justify-center w-full  md:w-[588px] lg:w-[588px]"
              >
                <div className="flex w-10/12 gap-1 px-3 items-center">
                  <div className=" w-3/12 oswald py-2  border-r border-gray-4 font-semibold px-2 text-xs ">
                    {lang === 'HIN' ? 'मेरी टीम' : 'MY TEAM'}
                  </div>

                  <div className=" flex items-center gap-1 justify-between w-9/12">
                    <div className="flex w-1/2 flex-col">
                      <div className="text-xs text-center   w-full font-semibold">
                        {lang === 'HIN' ? 'क्रेडिट शेष' : 'CREDITS REMAINING'}
                      </div>
                      <div className=" text-sm text-center">
                        <span className="text-base oswald black font-bold ">
                          {credits}
                        </span>
                        <span className=" white oswald  ">/100</span>
                      </div>
                    </div>
                    <div className=" flex flex-col  w-1/2 ">
                      <div className=" text-xs text-center  w-full font-semibold ">
                        {lang === 'HIN' ? 'चयनित खिलाड़ी' : 'PLATERS SELECTED'}{' '}
                      </div>
                      <div className=" text-sm text-center">
                        <span className="font-bold text-base">
                          {PlayersList.length}
                        </span>
                        <span className="oswald   ">/11</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="w-2/12 flex justify-end px-2 ">
                  <div className=" rounded-full flex items-center font-semibold text-basered dark:text-white justify-center h-12 text-center w-12 bg-white dark:bg-gray-4 ">
                    <div className="flex  text-xs   p-2 white">
                      {lang === 'HIN' ? 'टीम' : 'VIEW'}
                      <br /> {lang === 'HIN' ? ' देखें' : 'TEAM'}
                      {/* <ImageWithFallback
                                          height={18}
                                          width={18}
                                          loading="lazy"
                                          className="ml-1"
                                          src={rightWhiteArrow}
                                          alt=""
                                       /> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
        {/* for desktop only */}

        <div className=" hidden md:block lg:block mx-2  md:mx-0 lg:mx-0 dark:text-white ">
          {PlayerStatHubData &&
          PlayerStatHubData.playerHub &&
          PlayerStatHubData.playerHub.recentForm &&
          PlayerStatHubData.playerHub.recentForm.length > 0 ? (
            <div className=" w-full  relative flex gap-3">
              <div className="flex w-1/2  flex-col gap-3">
                <div className="w-full flex flex-col">
                  <div
                    className={`flex mb-3 items-center justify-between w-full`}
                  >
                    <div className={`h-12  w-full`}>
                      <div
                        className={`flex h-full cursor-pointer text-white items-center  p-3  bg-gray justify-between 
                                          rounded-md  ${
                                            props.showHideFRC ? '' : ''
                                          } `}
                        onClick={() => {
                          setopenSelectionModelV2(true)
                          setSelectionType('recentForm')
                          setPlayer2Compare(
                            PlayerStatHubData &&
                              PlayerStatHubData.playerHub &&
                              PlayerStatHubData.playerHub.recentForm.filter(
                                (players) =>
                                  players.playerID !== RecentPlayer?.playerID,
                              )[0],
                          )
                        }}
                      >
                        <div className="flex items-center">
                          {/* <div className="w-8 h-8 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                                       <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${RecentPlayer && RecentPlayer?.teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                                    </div> */}
                          <div className="uppercase text-sm font-semibold">
                            {RecentPlayer &&
                              (lang === 'HIN'
                                ? RecentPlayer?.playerNameHindi
                                : RecentPlayer?.name)}
                          </div>
                        </div>
                        <div className="p-1">
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className="w-5 h-5 "
                            src={compare}
                            alt=""
                          />
                        </div>
                      </div>
                    </div>

                    {props.showHideFRC && (
                      <div
                        className=" h-14 flex items-center justify-end  "
                        onClick={() => {
                          handlePlayerSelection(
                            RecentPlayer,
                            playerTeamIDList.filter(
                              (id) => id == RecentPlayer?.playerID,
                            ).length == 0
                              ? 'add'
                              : 'delete',
                          )
                        }}
                      >
                        {playerTeamIDList.filter(
                          (id) => id == RecentPlayer?.playerID,
                        ).length == 0 ? (
                          <div className="flex items-center bg-gray-4 justify-center text-center center h-12 rounded-lg ml-2 w-12  ">
                            <ImageWithFallback
                              height={28}
                              width={28}
                              loading="lazy"
                              className="w-full h-full cursor-pointer"
                              src={addicon}
                              alt=""
                              srcset=""
                            />
                          </div>
                        ) : (
                          <div className="flex items-center bg-gray-4 justify-center text-center center h-12 rounded-lg ml-2 w-12  ">
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              className="w-full h-full cursor-pointer"
                              src={minusIcon}
                              alt=""
                              srcset=""
                            />
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                  <div className="flex gap-2 w-full items-center justify-center ">
                    <div className="flex w-6/12 md:w-4/12 lg:w-4/12 h-full  items-center justify-center">
                      <div className=" h-full w-full">
                        <div className="h-full border bg-white  dark:text-white  dark:border-none  dark:bg-gray-4 py-16  flex flex-col items-center justify-center rounded-md">
                          <div className="relative bg-gray-10 dark:bg-gray-8 h-full rounded-full flex items-center justify-center ">
                            <div
                              className={`overflow-hidden w-32 h-32 rounded-full `}
                            >
                              <ImageWithFallback
                                height={180}
                                width={180}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className="object-top object-cover w-full h-full  "
                                src={`https://images.cricket.com/players/${
                                  RecentPlayer && RecentPlayer?.playerID
                                }_headshot_safari.png`}
                                alt=""
                                //   onError={(e) => (e.target.src = playerPlaceholder)}
                              />
                            </div>
                            <div className="absolute bottom-2 left-0   ">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc={flagPlaceHolder}
                                className="w-6 h-6  border rounded-full "
                                src={`https://images.cricket.com/teams/${
                                  RecentPlayer && RecentPlayer?.teamID
                                }_flag_safari.png`}
                                //   onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                alt=""
                              />
                            </div>
                          </div>
                          <div className="flex text-base font-semibold py-2 text-center">
                            {RecentPlayer &&
                              (lang === 'HIN'
                                ? RecentPlayer?.playerNameHindi
                                : RecentPlayer?.name)}
                          </div>
                          <div className="flex">
                            {lang === 'HIN' ? (
                              <div className="text-xs  font-semibold">
                                {RecentPlayer &&
                                  (RecentPlayer?.playerSkill === 'BATSMAN'
                                    ? 'बल्लेबाज़'
                                    : RecentPlayer?.playerSkill === 'BOWLER'
                                    ? 'गेंदबाज'
                                    : RecentPlayer?.playerSkill === 'KEEPER'
                                    ? 'विकेट कीपर'
                                    : 'ऑल राउंडर')}
                              </div>
                            ) : (
                              <div className="text-xs  font-semibold">
                                {RecentPlayer &&
                                  (RecentPlayer?.playerSkill === 'BATSMAN'
                                    ? 'BATTER'
                                    : RecentPlayer?.playerSkill === 'BOWLER'
                                    ? 'BOWLER'
                                    : RecentPlayer?.playerSkill === 'KEEPER'
                                    ? 'KEEPER'
                                    : 'ALL ROUNDER')}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="w-6/12 md:w-8/12 lg:w-8/12 h-full ">
                      <div className="h-full  flex flex-col gap-3 ">
                        <div className="flex  bg-white  dark:text-white border  dark:border-none items-center justify-evenly dark:bg-gray-4  rounded-md p-2 ">
                          <div className="  text-base  font-bold w-1/3">
                            {RecentPlayer && RecentPlayer?.avgFantasyPoints}
                          </div>

                          <div className="text-xs text-gray-2 w-2/3 text-left font-bold  p-1">
                            {RecentPlayer &&
                              (RecentPlayer?.lastFiveMatches.length === 5
                                ? getLangText(
                                    lang,
                                    words,
                                    'AVG POINTS IN LAST 5 MATCHES',
                                  )
                                : RecentPlayer?.lastFiveMatches.length > 0 &&
                                  RecentPlayer?.lastFiveMatches.length < 5
                                ? getLangText(
                                    lang,
                                    words,
                                    'AVG POINTS IN LAST FEW MATCHES',
                                  )
                                : '')}
                          </div>
                        </div>

                        <div className="bg-white  border  dark:border-none dark:bg-gray-4 rounded p-1 ">
                          <div className="relative w-full h-9/12">
                            <LineChartFRC
                              data={
                                RecentPlayer &&
                                RecentPlayer?.lastFiveMatches.length > 0
                                  ? [...RecentPlayer?.lastFiveMatches].reverse()
                                  : []
                              }
                              strokeClr={'green'}
                              width={
                                innerWidth > 700
                                  ? (innerWidth * 16) / 100
                                  : innerWidth > 500
                                  ? (innerWidth * 30) / 100
                                  : (innerWidth * 30) / 100
                              }
                              height={innerWidth > 700 ? 200 : 170}
                            />
                            <div
                              className="absolute items-center  bottom-0 nowrap flex gap-1"
                              style={{ color: 'gray', left: 25 }}
                            >
                              <div className="flex items-center justify-center rounded-full bg-green h-2 w-2"></div>

                              <div style={{ fontSize: 9 }}>
                                {getLangText(
                                  lang,
                                  words,
                                  'DreamTeam_Appearance',
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {RecentPlayer &&
                  RecentPlayer?.performanceIndicator &&
                  (RecentPlayer?.performanceIndicator.overallRPI ||
                    RecentPlayer?.performanceIndicator.overallWPI) && (
                    <div className="w-full">
                      <div className="flex flex-col gap-2 w-full p-2 border  bg-white  dark:text-white h-min dark:border-none dark:bg-gray rounded-md">
                        <Heading
                          heading={getLangText(
                            lang,
                            words,
                            'PERFORMANCE INDICATOR',
                          )}
                        />

                        <PerformanceIndicator
                          width={500}
                          height={400}
                          top={20}
                          bottom={30}
                          left={30}
                          right={0}
                          playerRole={RecentPlayer && RecentPlayer?.playerSkill}
                          data={
                            RecentPlayer && RecentPlayer?.performanceIndicator
                              ? RecentPlayer?.performanceIndicator
                              : {}
                          }
                          runs={getLangText(lang, words, 'RUN PER INNINGS')}
                          wickets={getLangText(
                            lang,
                            words,
                            'WICKETS PER INNINGS',
                          )}
                          lang={lang}
                        />
                      </div>
                    </div>
                  )}
                <div></div>
                {RecentPlayer &&
                  RecentPlayer?.bowlingDetails &&
                  RecentPlayer?.bowlingDetails.bowlingType &&
                  RecentPlayer?.bowlingDetails.bowlingType.length > 0 && (
                    <div className="w-full border  dark:text-white dark:border-none bg-white dark:bg-gray p-3  rounded-md ">
                      <Heading
                        heading={getLangText(
                          lang,
                          words,
                          'AGAINST BOWLING TYPE',
                        )}
                      />

                      <div className="flex w-full mt-3 gap-3 items-center justify-center p-1 dark:bg-gray-8 rounded-full">
                        {['S/R', 'AVERAGE', 'DISMISSALS'].map((type, i) => (
                          <div
                            onClick={() => setBowlingType(type)}
                            className={`w-full rounded-full  text-center text-xs font-semibold uppercase cursor-pointer p-2   ${
                              type === bowlingType
                                ? 'bg-basered dark:bg-gray-4 dark:border dark:border-green text-white'
                                : 'bg-light_gray dark:bg-gray-4 text-black '
                            }  font-semibold   font-semidbold`}
                            key={i}
                          >
                            {getLangText(lang, words, type)}
                          </div>
                        ))}
                      </div>
                      <RecentPlayerBowlingType
                        RecentPlayer={RecentPlayer}
                        bowlingType={bowlingType}
                        language={lang}
                      />
                    </div>
                  )}
                {!!(
                  MatchUpsData &&
                  MatchUpsData.matchupsById &&
                  MatchUpsData.matchupsById.matchUpData &&
                  MatchUpsData.matchupsById.matchUpData.length > 0 &&
                  MatchUpsData.matchupsById.matchUpData.filter(
                    (player) => player.player1 === RecentPlayer?.playerID,
                  )?.length
                ) && (
                  <Matcups
                    matchID={matchID}
                    closeNewModelV2={closeNewModelV2}
                    playersList={
                      RecentPlayer &&
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (player) => player.player1 === RecentPlayer?.playerID,
                      )
                    }
                    choosePlayer={choosePlayer}
                    runs={
                      RecentPlayer &&
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (pl) => pl.player1 === RecentPlayer?.playerID,
                      ).length > 0
                        ? MatchUpsData.matchupsById.matchUpData.filter(
                            (pl) => pl.player1 === RecentPlayer?.playerID,
                          )[0].runsScored
                        : 0
                    }
                    wickets={
                      RecentPlayer &&
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (pl) => pl.player1 === RecentPlayer?.playerID,
                      ).length > 0
                        ? MatchUpsData.matchupsById.matchUpData.filter(
                            (pl) => pl.player1 === RecentPlayer?.playerID,
                          )[0].Dismissals
                        : 0
                    }
                    lang={lang}
                    ballFaced={
                      RecentPlayer &&
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (pl) => pl.player1 === RecentPlayer?.playerID,
                      ).length > 0
                        ? MatchUpsData.matchupsById.matchUpData.filter(
                            (pl) => pl.player1 === RecentPlayer?.playerID,
                          )[0].ballsFaced
                        : 0
                    }
                    RecentPlayer={RecentPlayer && RecentPlayer}
                    Player2Obj={
                      !MatchUpsPlayer2 &&
                      RecentPlayer &&
                      MatchUpsData &&
                      MatchUpsData.matchupsById &&
                      MatchUpsData.matchupsById.matchUpData.length > 0 &&
                      MatchUpsData.matchupsById.matchUpData.filter(
                        (pl) => pl.player1 === RecentPlayer?.playerID,
                      ).length > 0
                        ? MatchUpsData.matchupsById.matchUpData.filter(
                            (pl) => pl.player1 === RecentPlayer?.playerID,
                          )[0]
                        : MatchUpsPlayer2
                    } //do same for matchups
                    SelectionType={SelectionType}
                  />
                )}
              </div>

              <div className="flex w-1/2  flex-col gap-3">
                {RecentPlayer && RecentPlayer?.lastFiveMatches.length > 0 && (
                  <div className="w-full ">
                    <div className="bg-white w-full   relative  dark:bg-gray dark:text-white border  dark:border-none  rounded-md h-full  p-3 ">
                      <Heading
                        heading={getLangText(
                          lang,
                          words,
                          'RECENT PERFORMANCES',
                        )}
                      />

                      <div className=" flex flex-col items-center justify-center p-2 text-white w-full  mt-3 overflow-x-scroll ">
                        <div className="flex   item-center justify-center text-xs  w-full ">
                          <div className=" w-1/3 item-center  bg-gray-4 rounded-tl-lg flex items-center justify-center  text-xs p-1 uppercase ">
                            {getLangText(lang, words, 'OPPOSITION')}
                          </div>
                          <div className="w-1/3  bg-gray-4  flex items-center justify-center text-xs p-1 uppercase ">
                            {getLangText(lang, words, 'BATTING')}
                          </div>

                          <div className="w-1/3  bg-gray-4 flex items-center justify-center text-xs  uppercase  p-1">
                            {getLangText(lang, words, 'BOWLING')}
                          </div>

                          <div className="w-1/3  bg-gray-4  flex items-center justify-center uppercase   rounded-tr-lg text-xs p-1">
                            {getLangText(lang, words, 'POINTS')}
                          </div>
                        </div>

                        {RecentPlayer?.lastFiveMatches.map((item, i) => {
                          return (
                            <div
                              className="flex  w-full  h-12 item-center justify-center  text-center "
                              key={i}
                            >
                              <div className="w-1/3  flex items-center  border-b dark:border-black  ">
                                <div className="ml-2  flex items-center justify-center ">
                                  <ImageWithFallback
                                    height={18}
                                    width={18}
                                    loading="lazy"
                                    fallbackSrc={flagPlaceHolder}
                                    className="h-5 object-cover object-top w-8  
                                       rounded"
                                    src={`https://images.cricket.com/teams/${item.oppTeamID}_flag_safari.png`}
                                    alt=""
                                  />
                                  <span className="ml-2 text-xs text-gray-2">
                                    {' '}
                                    {item.oppTeamName}
                                  </span>
                                </div>
                              </div>
                              <div className="w-1/3 border-b  dark:border-black  flex items-center  flex-col justify-center  text-gray-2 text-xs ">
                                <div>
                                  <span>
                                    {item.batting_stats.split('&')[0]}
                                  </span>
                                  {item.batting_stats.split('&').length > 1 && (
                                    <span>&</span>
                                  )}
                                </div>

                                {item.batting_stats.split('&')[1]}
                              </div>

                              <div className=" w-1/3 border-b  dark:border-black item-center  rounded-tl-lg   p-1 uppercase   flex flex-col items-center justify-center  text-gray-2 text-xs">
                                <div>
                                  <span>
                                    {item.bowling_stats.split('&')[0]}
                                  </span>
                                  {item.bowling_stats.split('&').length > 1 && (
                                    <span>&</span>
                                  )}
                                </div>
                                <div className="pt1">
                                  {item.bowling_stats.split('&')[1]}
                                </div>
                              </div>

                              <div className=" w-1/3 border-b  dark:border-black item-center justify-center rounded-tl-lg flex items-center text-xs p-1 uppercase      ">
                                <div
                                  className={` ml-2 w-50 oswald font-bold ${
                                    item.dta_flag ? 'text-green' : 'text-black'
                                  } flex justify-end fw6 f7`}
                                >
                                  {item.points}
                                </div>
                                {item.dta_flag && (
                                  <div className=" pt-1 px-1 ">
                                    <div className="flex items-center justify-center rounded-full bg-green  text-green h-1 w-1    text-2xl"></div>
                                  </div>
                                )}
                              </div>
                            </div>
                          )
                        })}
                        <div
                          className="absolute items-center  nowrap flex gap-1"
                          style={{ color: 'gray', left: 20, bottom: 8 }}
                        >
                          <div className="flex items-center justify-center rounded-full bg-green h-2 w-2"></div>
                          <div className="text-xs">
                            {getLangText(lang, words, 'DreamTeam_Appearance')}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {RecentPlayer && RecentPlayer?.highLowFlag && (
                  <div className=" w-full  ">
                    <div className="border bg-white  dark:border-none dark:bg-gray dark:text-white p-3 rounded-md">
                      <div className="flex flex-col justify-start items-start pv2  ph2">
                        <Heading
                          heading={getLangText(lang, words, 'HIGHS & LOWS')}
                        />
                        {/* <div className=' captilize text-base font-bold   '>
                                 {getLangText(lang, words, 'HIGHS & LOWS')}
                              </div>
                              <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded' /> */}
                      </div>

                      <div className="mh2 bb b--white-20"></div>
                      {lang === 'HIN' ? (
                        <div>
                          {RecentPlayer &&
                            (RecentPlayer?.strengthInHindi.stmt1 ||
                              RecentPlayer?.strengthInHindi.stmt2 ||
                              RecentPlayer?.strengthInHindi.stmt3) &&
                            (RecentPlayer?.weaknessInHindi.stmt1 ||
                              RecentPlayer?.weaknessInHindi.stmt2 ||
                              RecentPlayer?.weaknessInHindi.stmt3) && (
                              <div className="bg-dark-gray  flex flex-col items-start justify-start ma3 br3 ">
                                <div
                                  className="br2 flex w-50 mv2 items-center cursor-pointer justify-center bg-gray-4 text-white"
                                  onClick={() =>
                                    setquality(
                                      quality === 'strength'
                                        ? 'weakness'
                                        : 'strength',
                                    )
                                  }
                                >
                                  <div
                                    className={`w-50 flex items-center br2 ${
                                      quality === 'strength'
                                        ? 'bg-frc-yellow black'
                                        : 'bg-gray-4 text-white'
                                    } pv2 ph2  justify-center  fw6  f8 f7-l f7-m`}
                                  >
                                    {' '}
                                    {getLangText(lang, words, 'STRENGTH')}
                                  </div>
                                  <div
                                    className={`w-50  flex items-center br2 ph2 ${
                                      quality === 'weakness'
                                        ? 'bg-frc-yellow black'
                                        : 'bg-gray-4 text-white'
                                    } pv2 justify-center  fw6  f8 f7-l f7-m`}
                                  >
                                    {getLangText(lang, words, 'WEAKNESS')}
                                  </div>
                                </div>

                                <div className=" flex flex-col items-start justify-start ">
                                  {['stmt1', 'stmt2', 'stmt3'].map(
                                    (stmt, i) => (
                                      <>
                                        {RecentPlayer &&
                                          RecentPlayer[
                                            quality === 'strength'
                                              ? 'strengthInHindi'
                                              : 'weaknessInHindi'
                                          ][stmt] && (
                                            <div
                                              key={i}
                                              className="tc  white    pv1 w-70  f8 f6-l f6-m lh-copy"
                                            >
                                              {`${
                                                RecentPlayer[
                                                  quality === 'strength'
                                                    ? 'strengthInHindi'
                                                    : 'weaknessInHindi'
                                                ][stmt]
                                              }`
                                                .split('$')
                                                .map((word, j) => (
                                                  <span
                                                    key={j}
                                                    className={`  ${
                                                      j % 2 === 0
                                                        ? ' fw4 '
                                                        : ' ph1 fw6'
                                                    }`}
                                                  >
                                                    {word}
                                                  </span>
                                                ))}
                                            </div>
                                          )}
                                      </>
                                    ),
                                  )}
                                </div>
                              </div>
                            )}
                        </div>
                      ) : (
                        <div>
                          {RecentPlayer &&
                            (RecentPlayer?.strength.stmt1 ||
                              RecentPlayer?.strength.stmt2 ||
                              RecentPlayer?.strength.stmt3) &&
                            (RecentPlayer?.weakness.stmt1 ||
                              RecentPlayer?.weakness.stmt2 ||
                              RecentPlayer?.weakness.stmt3) && (
                              <div className="  flex flex-col items-center justify-center m-2 rounded ">
                                <div
                                  className="flex w-full mt-3 gap-3 items-center justify-center p-1 dark:bg-gray-8 rounded-full"
                                  onClick={() =>
                                    setquality(
                                      quality === 'strength'
                                        ? 'weakness'
                                        : 'strength',
                                    )
                                  }
                                >
                                  <div
                                    className={`w-6/12 text-xs  uppercase rounded-full  flex items-center 
                                             ${
                                               quality === 'strength'
                                                 ? 'bg-basered dark:bg-gray-4 dark:border dark:border-green text-white'
                                                 : 'bg-light_gray dark:bg-gray-4 text-black '
                                             } p-2 justify-center  font-semibold cursor-pointer `}
                                  >
                                    {' '}
                                    {getLangText(lang, words, 'STRENGTH')}
                                  </div>
                                  <div
                                    className={` w-6/12 rounded-full uppercase flex items-center  ph2 ${
                                      quality === 'weakness'
                                        ? 'bg-basered dark:bg-gray-4 dark:border dark:border-green text-white'
                                        : 'bg-light_gray dark:bg-gray-4 text-black '
                                    } cursor-pointer p-2 justify-center  font-semibold text-xs`}
                                  >
                                    {getLangText(lang, words, 'WEAKNESS')}
                                  </div>
                                </div>

                                <div className=" flex flex-col items-center justify-center  w-full ">
                                  {['stmt1', 'stmt2', 'stmt3'].map(
                                    (stmt, i) =>
                                      RecentPlayer &&
                                      RecentPlayer[quality][stmt] && (
                                        <div className="flex items-center justify-start w-full">
                                          <div className="w-2 h-2 rounded-full bg-green mx-1"></div>

                                          <div
                                            key={i}
                                            className=" py-2 text-xs  "
                                          >
                                            {`${RecentPlayer[quality][stmt]}`
                                              .split('$')
                                              .map((word, j) => (
                                                <>
                                                  <span
                                                    key={j}
                                                    className={`  ${
                                                      j % 2 === 0
                                                        ? ' fw4 '
                                                        : ' ph1 fw6'
                                                    }`}
                                                  >
                                                    {word}
                                                  </span>
                                                </>
                                              ))}
                                          </div>
                                        </div>
                                      ),
                                  )}
                                </div>
                              </div>
                            )}
                          {lang === 'HIN' ? (
                            <span className="text-xs text-gray-2">{`*  पिछले 5 वर्षों के ${PlayerStatHubData.playerHub.compType}  डेटा`}</span>
                          ) : (
                            <span className="text-xs text-gray-2">{`* ${PlayerStatHubData.playerHub.compType} data from last 5 years`}</span>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                )}
                {RecentPlayer &&
                  RecentPlayer?.battingDetails &&
                  RecentPlayer?.battingDetails.battingType &&
                  RecentPlayer?.battingDetails.battingType.length > 0 && (
                    <div className="w-full ">
                      <div className="bg-white  border  dark:border-none dark:bg-gray rounded-md p-3">
                        <Heading
                          heading={getLangText(
                            lang,
                            words,
                            'AGAINST BATTING TYPE',
                          )}
                        />
                        {/* <div className="p-3 uppercase  text-left"> {getLangText(lang, words, 'AGAINST BATTING TYPE')}</div>
                              <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded' /> */}
                        <div className="flex w-full mt-3 gap-3 items-center justify-center p-1 dark:bg-gray-8 rounded-full">
                          {['WICKETS', 'AVERAGE', 'ECONOMY'].map((type, i) => (
                            <div
                              onClick={() => setBattingType(type)}
                              className={`w-4/12 rounded-full cursor-pointer text-center text-xs font-semibold uppercase f7 p-2 ${
                                type === battingType
                                  ? 'bg-basered dark:bg-gray-4 dark:border dark:border-green text-white'
                                  : 'bg-light_gray dark:bg-gray-4 text-black '
                              }      font-semidbold`}
                              key={i}
                            >
                              {getLangText(lang, words, type)}
                            </div>
                          ))}
                        </div>
                        <RecentPlayerBattingType
                          RecentPlayer={RecentPlayer}
                          battingType={battingType}
                          language={lang}
                        />
                        {/* {RecentPlayer && RecentPlayer?.battingDetails &&
                                 RecentPlayer?.battingDetails.battingType && RecentPlayer?.battingDetails.battingType.length > 0 && RecentPlayer?.battingDetails.battingType.map((compare, i) =>
                                    <div className="ph3 pv2" key={i}>
                                       <div className="flex pv2 items-center justify-between">
                                          <div className="fw4 f8"><span className="gray">v</span><span className="white ttu pl1">{compare.types}</span></div>
                                          <div className="fw7 white f8">{battingType === "WICKETS" ? Number(compare.wickets) : battingType === "AVERAGE" ? Number(compare.battingAvg) : Number(compare.economyRate)}</div>
                                       </div>

                                       <div
                                          className={`  br2  relative items-center w-100 z-0 `}
                                          style={{ height: 6, backgroundColor: "#333a46" }}>
                                          <div
                                             className={`absolute h-100 br2  br--left left-0 bg-frc-yellow `}
                                             style={{
                                                width: `${battingType === "WICKETS" ? Number(compare.wickets) / Number(RecentPlayer?.battingDetails.maxWickets) * 100 : battingType === "AVERAGE" ? Number(compare.battingAvg) / Number(RecentPlayer?.battingDetails.maxAvg) * 100 : Number(compare.economyRate) / Number(RecentPlayer?.battingDetails.maxEconomy) * 100}%`,


                                                zIndex: -1
                                             }}></div>
                                       </div>
                                    </div>
                                 )} */}
                      </div>
                    </div>
                  )}
                {MatchUpsData &&
                  MatchUpsData.matchupsById &&
                  MatchUpsData.matchupsById.matchUpData &&
                  MatchUpsData.matchupsById.matchUpData.length > 0 &&
                  RecentPlayer &&
                  RecentPlayer?.threat && (
                    <div className="w-full">
                      <div className="rounded-lg bg-white dark:text-white border dark:bg-gray dark:border-none">
                        <div className="text-white bg-gray-4 rounded-t-lg p-2">
                          {lang === 'HIN' ? 'खतरा' : 'Threats'}
                        </div>
                        <div className="flex w-full p-3">
                          <div className="w-1/3 flex justify-center ">
                            <div className="h-24 w-24 flex justify-center items-center rounded-full bg-light_gray dark:bg-gray-8">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className="w-full h-full object-cover rounded-full object-top"
                                src={`https://images.cricket.com/players/${
                                  RecentPlayer &&
                                  RecentPlayer?.threatOppPlayerID
                                }_headshot_safari.png`}
                              />
                            </div>
                          </div>
                          <div className="w-2/3  flex items-center justify-center rounded-md">
                            <div className="text-sm font-semibold">
                              {' '}
                              {lang === 'HIN' ? (
                                <div>
                                  {`${RecentPlayer?.threatInHindi}`
                                    .split('$')
                                    .map((word, j) => (
                                      <span
                                        key={j}
                                        className={`  ${
                                          j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'
                                        }`}
                                      >
                                        {word}
                                      </span>
                                    ))}
                                </div>
                              ) : (
                                <div>
                                  {`${RecentPlayer?.threat}`
                                    .split('$')
                                    .map((word, j) => (
                                      <span
                                        key={j}
                                        className={`  ${
                                          j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'
                                        }`}
                                      >
                                        {word}
                                      </span>
                                    ))}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                <div className="w-full  dark:text-white rounded    ">
                  <div className="w-full   dark:bg-gray-8 dark:border-none dark:text-white rounded-md ">
                    <Heading heading={getLangText(lang, words, 'COMPARE')} />
                    {/* <div className=' uppercase text-left ml-2'>{getLangText(lang, words, 'COMPARE')}</div>
                        <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 ml-2 rounded mb-2'></div> */}
                    {/* <div className=' f5-l f5-m   pa2  ttu f6 lh-title fw6  capitalize font-bold mt-3 ml-2   '> {getLangText(lang, words, 'COMPARE')}  </div> */}

                    <div className=" flex mt-3    items-center justify-between ">
                      <div className="w-5/12 h-48  cursor-pointer border  bg-white  dark:bg-gray dark:text-white  flex  items-center justify-center  rounded-md py-3">
                        <div className="  flex gap-3  flex-col items-center w-full justify-center shadow-4 pa2">
                          <div className="h4 relative flex-col   justify-center items-center">
                            <div className="flex  overflow-hidden items-center  w-100 justify-center  ">
                              <ImageWithFallback
                                height={58}
                                width={58}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className=" rounded-full  h-28 w-28 bg-gray-10 dark:bg-gray-8   object-cover object-top"
                                src={`https://images.cricket.com/players/${
                                  RecentPlayer && RecentPlayer?.playerID
                                }_headshot.png`}
                                alt=""
                              />
                            </div>
                            <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc={flagPlaceHolder}
                                className=" w-5 h-5 rounded-full"
                                style={{ left: 2 }}
                                src={`https://images.cricket.com/teams/${
                                  RecentPlayer && RecentPlayer?.teamID
                                }_flag_safari.png`}
                                alt=""
                                // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                              />
                            </div>
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc="/svgs/images/flag_empty.svg"
                              className=" absolute h-6 w-6  border border-white bg-gray-4 p-1 rounded-lg  left-0 bottom-0 object-fill"
                              src={
                                RecentPlayer &&
                                RecentPlayer?.playerSkill === 'ALL_ROUNDER'
                                  ? '/pngsV2/allrounder-line-white.png'
                                  : RecentPlayer?.playerSkill === 'BATSMAN'
                                  ? '/pngsV2/battericon.png'
                                  : RecentPlayer?.playerSkill === 'KEEPER'
                                  ? '/pngsV2/keeper-line-white.png'
                                  : '/pngsV2/bowlericon.png'
                              }
                            />
                          </div>

                          <div className="h2-5  pb3 pt2">
                            <div className="flex  items-center  w-full justify-center white f7 pv2 text-center text-sm">
                              {RecentPlayer &&
                                (lang === 'HIN'
                                  ? RecentPlayer?.playerNameHindi
                                  : RecentPlayer?.name)}
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="w-1/12 mx-2 flex   items-center   justify-center">
                        <ImageWithFallback
                          height={28}
                          width={28}
                          loading="lazy"
                          className="h-10"
                          src={compareGreen}
                          alt=""
                        />
                      </div>

                      <div
                        className="w-5/12 relative h-48 cursor-pointer border border-green bg-white  dark:bg-gray dark:text-white  rounded-md flex py-3 items-center justify-center"
                        onClick={() => {
                          setCompareModel(true)
                          setSelectionType('compare')
                        }}
                      >
                        <div className="  flex w-full gap-3 flex-col items-center  w-100 justify-center shadow-4 pa2">
                          {/* <div className="flex w-full items-end justify-end relative "> */}
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            className=" absolute w-4 mr-3 pt-2 top-0 right-0 -bottom-5"
                            src={compare}
                            alt=""
                          />
                          {/* </div> */}
                          <div className=" relative flex-col   justify-center items-center">
                            <div className="flex   items-center  w-full justify-center ">
                              <ImageWithFallback
                                height={58}
                                width={58}
                                loading="lazy"
                                fallbackSrc={playerPlaceholder}
                                className=" rounded-full  h-28 w-28 bg-gray-10 dark:bg-gray-8   object-cover object-top"
                                src={`https://images.cricket.com/players/${
                                  Player2Compare && Player2Compare?.playerID
                                }_headshot.png`}
                                alt=""
                              />
                            </div>
                            <ImageWithFallback
                              height={18}
                              width={18}
                              loading="lazy"
                              fallbackSrc="/svgs/images/flag_empty.svg"
                              className=" absolute h-6 w-6  border border-white bg-gray-4 p-1 rounded-lg  left-0 bottom-0 "
                              src={
                                Player2Compare &&
                                Player2Compare?.playerSkill === 'ALL_ROUNDER'
                                  ? '/pngsV2/allrounder-line-white.png'
                                  : Player2Compare?.playerSkill === 'BATSMAN'
                                  ? '/pngsV2/battericon.png'
                                  : Player2Compare?.playerSkill === 'KEEPER'
                                  ? '/pngsV2/keeper-line-white.png'
                                  : '/pngsV2/bowlericon.png'
                              }
                            />
                            <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                              <ImageWithFallback
                                height={18}
                                width={18}
                                loading="lazy"
                                fallbackSrc={flagPlaceHolder}
                                className="   rounded-full w-5 h-5      "
                                style={{ left: 2 }}
                                src={`https://images.cricket.com/teams/${
                                  Player2Compare && Player2Compare?.teamID
                                }_flag_safari.png`}
                                alt=""
                              />
                            </div>
                          </div>

                          <div className="h2-5 pb3 pt2">
                            <div className="flex  items-center  w-full justify-center white f7 pv2 tc fw6 text-center text-sm">
                              {Player2Compare &&
                                (lang === 'HIN'
                                  ? Player2Compare?.playerNameHindi
                                  : Player2Compare?.name)}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className=" mt-3 text-black dark:text-white">
                      {
                        <div className="border  bg-white  dark:bg-gray dark:text-white  rounded-lg">
                          {[1].map((stat, i) => (
                            <div className="mx-3" key={i}>
                              <div className="flex  items-center justify-center py-3">
                                <div
                                  className={`w-4/12 flex items-center justify-center  text-2xl  font-bold    ${
                                    RecentPlayer &&
                                    Player2Compare &&
                                    handlecssPlayer1(
                                      'fantasy',
                                      stat,
                                      RecentPlayer?.avgFantasyPoints,
                                      Player2Compare?.avgFantasyPoints,
                                    )
                                  }`}
                                >
                                  {RecentPlayer &&
                                  RecentPlayer?.avgFantasyPoints !== ''
                                    ? RecentPlayer?.avgFantasyPoints
                                    : '--'}
                                </div>
                                <div className="w-4/12 flex items-center  justify-center     text-base  font-bold border-x dark:border-black ">
                                  <div className=" text-center  font-bold text-gray-2 text-xs ">
                                    {lang === 'HIN' ? 'औसत' : 'Average'} <br />
                                    {lang === 'HIN'
                                      ? 'फ़ैंटसी अंक'
                                      : 'Fantasy Points'}
                                  </div>
                                </div>
                                <div
                                  className={`w-4/12 flex font-bold items-center justify-center text-2xl text-gray-2  ${
                                    RecentPlayer &&
                                    Player2Compare &&
                                    handlecssPlayer2(
                                      'fantasy',
                                      stat,
                                      RecentPlayer?.avgFantasyPoints,
                                      Player2Compare?.avgFantasyPoints,
                                    )
                                  }`}
                                >
                                  {Player2Compare &&
                                  Player2Compare?.avgFantasyPoints !== ''
                                    ? Player2Compare?.avgFantasyPoints
                                    : '--'}
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                      }
                      {RecentPlayer &&
                      RecentPlayer?.playerSkill === 'BOWLER' ? (
                        <>
                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className="text-sm capitalize font-semibold">
                                {getLangText(lang, words, 'BOWLING STATS')}
                              </div>

                              <div className="border mt-2 rounded-lg p-2 bg-white dark:bg-gray dark:border-none">
                                {[
                                  'bowlingWickets',
                                  'bowlingStrikeRate',
                                  'bowlingEconomyRate',
                                ].map((stat, i) => (
                                  <div
                                    className={`${
                                      i !== 2 ? 'mh3 bb b--white-20' : 'mh3'
                                    }`}
                                    key={i}
                                  >
                                    <div className="flex items-center justify-center my-2">
                                      <div
                                        className={`w-4/12 flex items-center justify-center  text-2xl oswald font-bold  
                                             ${
                                               RecentPlayer &&
                                               Player2Compare &&
                                               handlecssPlayer1(
                                                 'bowling',
                                                 stat,
                                                 RecentPlayer
                                                   .statsHubPlayerbowling[stat],
                                                 Player2Compare
                                                   .statsHubPlayerbowling[stat],
                                               )
                                             }`}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbowling &&
                                        RecentPlayer?.statsHubPlayerbowling[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ]
                                          : '--'}
                                      </div>
                                      <div className="w-4/12 flex items-center justify-center  text-uppercase text-sm border-x dark:border-black">
                                        {stat ===
                                        'bowlingBestBowlingTotalSpell' ? (
                                          <div className="text-gray-2 ">
                                            best
                                            <br /> figures
                                          </div>
                                        ) : stat === 'bowlingEconomyRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'इकॉनमी'
                                              : 'ECONOMY'}
                                          </div>
                                        ) : stat === 'bowlingStrikeRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'SR'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति पारी'
                                              : 'Wickets per'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' विकेट'
                                              : 'innings'}
                                          </div>
                                        )}
                                      </div>
                                      <div
                                        className={`w-4/12 flex items-center justify-center   text-2xl oswald font-bold  text-gray-2  ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'bowling',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbowling[stat],
                                          )
                                        }`}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbowling &&
                                        Player2Compare?.statsHubPlayerbowling[
                                          stat
                                        ]
                                          ? Player2Compare
                                              ?.statsHubPlayerbowling[stat]
                                          : '-'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}

                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className="text-sm capitalize font-semibold">
                                {getLangText(lang, words, 'BATTING STATS')}
                              </div>

                              <div className="border mt-2 rounded-lg p-2 bg-white dark:bg-gray dark:border-none">
                                {[
                                  'commonBatPosition',
                                  'battingRunsInnings',
                                  'battingStrikeRate',
                                  'battingFoursSixes',
                                ].map((stat, i) => (
                                  <div
                                    className={`${
                                      i !== 3 ? 'mh3 bb b--white-20' : 'mh3'
                                    }`}
                                    key={i}
                                  >
                                    <div className="flex  items-center justify-center my-2">
                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl oswald font-bold   ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer1(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        } `}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbatting &&
                                        RecentPlayer?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ]
                                          : '-'}
                                      </div>
                                      <div className="w-4/12 flex items-center text-sm justify-center uuppercase border-x dark:border-black">
                                        {stat === 'battingAverage' ? (
                                          <div className="text-gray-2 ">
                                            {getLangText(
                                              lang,
                                              words,
                                              'AVERAGE',
                                            )}
                                          </div>
                                        ) : stat === 'battingFoursSixes' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति'
                                              : 'Boundaries'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' मैच बाउंड्री'
                                              : 'per match'}
                                          </div>
                                        ) : stat === 'battingHighestScore' ? (
                                          <div className="f5  fw6 lh-copy oswald">
                                            {' '}
                                            Highest <br /> Score
                                          </div>
                                        ) : stat === 'battingRunsInnings' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN' ? 'प्रति' : 'Runs'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'पारी रन'
                                              : 'per innings'}
                                          </div>
                                        ) : stat === 'commonBatPosition' ? (
                                          <div className="text-gray-2 text-xs">
                                            {lang === 'HIN'
                                              ? 'सामान्य'
                                              : 'COMMON'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'बल्लेबाजी की स्थिति'
                                              : 'BAT POSITION'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'S/R'}
                                          </div>
                                        )}
                                      </div>

                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl oswald font-bold   ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        } `}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbatting &&
                                        Player2Compare?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? Player2Compare
                                              ?.statsHubPlayerbatting[stat]
                                          : '-'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                        </>
                      ) : (
                        <>
                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className=" text-sm capitalize  font-bold mt-3">
                                {getLangText(lang, words, 'BATTING STATS')}
                              </div>

                              <div className="border mt-2 rounded-lg p-2 bg-white dark:bg-gray dark:border-none">
                                {[
                                  'commonBatPosition',
                                  'battingRunsInnings',
                                  'battingStrikeRate',
                                  'battingFoursSixes',
                                ].map((stat, i) => (
                                  <div
                                    className={`${i !== 3 ? 'mx-3 ' : 'mx-3'}`}
                                    key={i}
                                  >
                                    <div className="flex items-center justify-center my-2">
                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl oswald font-bold    ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer1(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        }`}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbatting &&
                                        RecentPlayer?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ]
                                          : '--'}
                                      </div>
                                      <div className="w-4/12 border-x dark:border-black flex items-center  justify-center   text-xs font-semibold oswald text-center">
                                        {stat === 'battingAverage' ? (
                                          <div className="text-gray-2 text-xs">
                                            {getLangText(
                                              lang,
                                              words,
                                              'AVERAGE',
                                            )}
                                          </div>
                                        ) : stat === 'battingFoursSixes' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति'
                                              : 'Boundaries'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' मैच बाउंड्री'
                                              : 'per match'}
                                          </div>
                                        ) : stat === 'battingHighestScore' ? (
                                          <div className="f5  fw6 lh-copy oswald">
                                            {' '}
                                            Highest <br /> Score
                                          </div>
                                        ) : stat === 'battingRunsInnings' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN' ? 'प्रति' : 'Runs'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'पारी रन'
                                              : 'per innings'}
                                          </div>
                                        ) : stat === 'commonBatPosition' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'सामान्य'
                                              : 'COMMON'}
                                            <br />
                                            {lang === 'HIN'
                                              ? 'बल्लेबाजी की स्थिति'
                                              : 'BAT POSITION'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'S/R'}
                                          </div>
                                        )}
                                      </div>

                                      <div
                                        className={` w-4/12 flex items-center justify-center  text-2xl   oswald font-bold   ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'batting',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbatting[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbatting[stat],
                                          )
                                        } `}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbatting &&
                                        Player2Compare?.statsHubPlayerbatting[
                                          stat
                                        ] !== ''
                                          ? Player2Compare
                                              ?.statsHubPlayerbatting[stat]
                                          : '--'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                          {RecentPlayer && (
                            <div className="w-full mt-4">
                              <div className=" capitalize text-sm font-bold mt-3">
                                {getLangText(lang, words, 'BOWLING STATS')}
                              </div>

                              <div className="my-1 border  bg-white  dark:bg-gray dark:text-white  rounded-lg">
                                {[
                                  'bowlingWickets',
                                  'bowlingStrikeRate',
                                  'bowlingEconomyRate',
                                ].map((stat, i) => (
                                  <div
                                    className={`${
                                      i !== 2 ? 'mh3 bb b--white-20' : 'mh3'
                                    }`}
                                    key={i}
                                  >
                                    <div className="flex  items-center justify-center my-2">
                                      <div
                                        className={`w-4/12 flex items-center justify-center   text-2xl oswald font-bold  
                                             ${
                                               RecentPlayer &&
                                               Player2Compare &&
                                               handlecssPlayer1(
                                                 'bowling',
                                                 stat,
                                                 RecentPlayer
                                                   .statsHubPlayerbowling[stat],
                                                 Player2Compare
                                                   .statsHubPlayerbowling[stat],
                                               )
                                             }`}
                                      >
                                        {RecentPlayer &&
                                        RecentPlayer?.statsHubPlayerbowling &&
                                        RecentPlayer?.statsHubPlayerbowling[
                                          stat
                                        ] !== ''
                                          ? RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ]
                                          : '--'}
                                      </div>
                                      <div className="w-4/12 flex items-center justify-center  oswald border-x dark:border-black text-center text-sm text-gray-2 font-bold">
                                        {stat ===
                                        'bowlingBestBowlingTotalSpell' ? (
                                          <div className="text-gray-2 text-base">
                                            best
                                            <br /> figures
                                          </div>
                                        ) : stat === 'bowlingEconomyRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'इकॉनमी'
                                              : 'ECONOMY'}
                                          </div>
                                        ) : stat === 'bowlingStrikeRate' ? (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'स्ट्राइक रेट'
                                              : 'S/R'}
                                          </div>
                                        ) : (
                                          <div className="text-gray-2 ">
                                            {lang === 'HIN'
                                              ? 'प्रति पारी'
                                              : 'Wickets per'}
                                            <br />
                                            {lang === 'HIN'
                                              ? ' विकेट'
                                              : 'innings'}
                                          </div>
                                        )}
                                      </div>
                                      <div
                                        className={`w-4/12 flex items-center justify-center    text-2xl font-semibold oswald  fw6  ${
                                          RecentPlayer &&
                                          Player2Compare &&
                                          handlecssPlayer2(
                                            'bowling',
                                            stat,
                                            RecentPlayer?.statsHubPlayerbowling[
                                              stat
                                            ],
                                            Player2Compare
                                              ?.statsHubPlayerbowling[stat],
                                          )
                                        }`}
                                      >
                                        {Player2Compare &&
                                        Player2Compare?.statsHubPlayerbowling &&
                                        Player2Compare?.statsHubPlayerbowling[
                                          stat
                                        ]
                                          ? Player2Compare
                                              ?.statsHubPlayerbowling[stat]
                                          : '--'}
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                        </>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              {openSelectionModelV2 && (
                <PlayerModelV2
                  closeNewModelV2={closeNewModelV2}
                  playersList={PlayerStatHubData.playerHub.recentForm}
                  choosePlayer={choosePlayer}
                  RecentPlayer={RecentPlayer}
                  lang={lang}
                  Player2={SelectionType === 'compare' ? Player2Compare : null} //do same for matchups
                  SelectionType={SelectionType}
                />
              )}

              {OpenModelMatchups && (
                <MatchUpsModel
                  closeNewModelV2={closeNewModelV2}
                  playersList={
                    MatchUpsData &&
                    MatchUpsData.matchupsById &&
                    MatchUpsData.matchupsById.matchUpData.filter(
                      (player) => player.player1 === RecentPlayer?.playerID,
                    )
                  }
                  choosePlayer={choosePlayer}
                  lang={lang}
                  RecentPlayer={RecentPlayer}
                  Player2Obj={
                    !MatchUpsPlayer2 &&
                    RecentPlayer &&
                    MatchUpsData &&
                    MatchUpsData.matchupsById &&
                    MatchUpsData.matchupsById.matchUpData.length > 0 &&
                    MatchUpsData.matchupsById.matchUpData.filter(
                      (pl) => pl.player1 === RecentPlayer?.playerID,
                    ).length > 0
                      ? MatchUpsData.matchupsById.matchUpData.filter(
                          (pl) => pl.player1 === RecentPlayer?.playerID,
                        )[0]
                      : MatchUpsPlayer2
                  } //do same for matchups
                  SelectionType={SelectionType}
                />
              )}

              {openCompareModel && (
                <CompareModelPlayerHub
                  closeNewModelV2={closeNewModelV2}
                  playersList={PlayerStatHubData.playerHub.recentForm}
                  choosePlayer={choosePlayer}
                  lang={lang}
                  RecentPlayer={RecentPlayer}
                  Player2Obj={Player2Compare} //do same for matchups
                  SelectionType={SelectionType}
                />
              )}
            </div>
          ) : (
            <div className="w-full br2 ba  b--white-20 ">
              <DataNotFound />
            </div>
          )}
          {playerTeamIDList.length > 0 && (
            <div className="hidden md:sticky lg:sticky md:flex items-center justify-center bottom-2  md:bottom-2 lg:bottom-2  pr-4 md:pr-0 lg:pr-0 w-full ">
              <div
                onClick={() => {
                  props.changeComponentState('BYTPlayerStathubState'),
                    props.playerScores(
                      PlayersList,
                      credits,
                      catPlayers,
                      playerTeamIDList,
                      'BYTPlayerStathubState',
                      RecentPlayer,
                    )
                }}
                className=" cursor-pointer hidescroll flex items-center text-white dark:text-black h-16  rounded-full bg-basered dark:bg-green justify-center w-full  md:w-[588px] lg:w-[588px]"
              >
                <div className="flex w-10/12 gap-1 px-3 items-center">
                  <div className=" w-3/12 oswald py-2  border-r border-gray-4 font-semibold px-2 text-xs ">
                    {lang === 'HIN' ? 'मेरी टीम' : 'MY TEAM'}
                  </div>

                  <div className=" flex items-center gap-1 justify-between w-9/12">
                    <div className="flex w-1/2 flex-col">
                      <div className="text-xs text-center   w-full font-semibold">
                        {lang === 'HIN' ? 'क्रेडिट शेष' : 'CREDITS REMAINING'}
                      </div>
                      <div className=" text-sm text-center">
                        <span className="text-base oswald black font-bold ">
                          {credits}
                        </span>
                        <span className=" white oswald  ">/100</span>
                      </div>
                    </div>
                    <div className=" flex flex-col  w-1/2 ">
                      <div className=" text-xs text-center  w-full font-semibold ">
                        {lang === 'HIN' ? 'चयनित खिलाड़ी' : 'PLATERS SELECTED'}{' '}
                      </div>
                      <div className=" text-sm text-center">
                        <span className="font-bold text-base">
                          {PlayersList.length}
                        </span>
                        <span className="oswald   ">/11</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="w-2/12 flex justify-end px-2 ">
                  <div className=" rounded-full flex items-center font-semibold text-basered dark:text-white justify-center h-12 text-center w-12 bg-white dark:bg-gray-4 ">
                    <div className="flex  text-xs   p-2 white">
                      {lang === 'HIN' ? 'टीम' : 'VIEW'}
                      <br /> {lang === 'HIN' ? ' देखें' : 'TEAM'}
                      {/* <ImageWithFallback
                                          height={18}
                                          width={18}
                                          loading="lazy"
                                          className="ml-1"
                                          src={rightWhiteArrow}
                                          alt=""
                                       /> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </>
    )
}
