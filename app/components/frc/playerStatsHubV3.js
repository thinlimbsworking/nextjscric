import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import CompareModelPlayerHub from './compareModelPlayerHub'
import { STAT_HUB_PLAYER_V2, GET_MATCHUP_BY_ID } from '../../api/queries';
import PlayerModelV2 from './playerModelV2'
import { useQuery } from '@apollo/react-hooks';
import RecentPlayerBowlingType from './recentPlayerBowlingType'
import RecentPlayerBattingType from './recentPlayerBattingType'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const compare = '/svgs/compareYellow.svg'
const black_bat = '/svgs/black_bat.svg'
const black_allRounder = '/svgs/black_allRounder.svg'
const black_ball = '/svgs/black_ball.svg'
const black_keeper = '/svgs/black_keeper.svg'
const rightWhiteArrow = '/svgs/RightSchevronWhite.svg'
import Loading from '../loading';
import PerformanceIndicator from './fRCPerformanceIndicator'
import LineChartFRC from './lineChartFRC'
import MatchUpsModel from './matchUpsModel'
import { words } from './../../constant/language'
export default function PlayerStatsHubV2(props) {

   // console.log("=>>?>>>>>>from a23 data",props)




   const [PlayersList, setPlayersList] = useState([])
   // const [Player2List, setPlayer2List] = useState([])
   const [OpenModelMatchups, setOpenModelMatchups] = useState(false)
   const [SelectionType, setSelectionType] = useState('recentForm')
   const [RecentPlayer, setRecentPlayer] = useState(props.recentplayer)
   const [openCompareModel,setCompareModel]=useState(false)
   // console.log("RecentPlayer", RecentPlayer)
   const [bowlingType, setBowlingType] = useState('S/R');
   const [openSelectionModelV2, setopenSelectionModelV2] = useState(false)
   const [battingType, setBattingType] = useState('WICKETS')

   const [innerWidth, updateInnerWidth] = useState(0);
   const [showMyTeam, setShowmyteam] = useState(true)
   const [MatchUpsPlayer2, setMatchUpsPlayer2] = useState(null)
   // console.log("MatchUpsPlayer2", MatchUpsPlayer2)
   const [Player2Compare, setPlayer2Compare] = useState(null)
   const [quality, setquality] = useState("strength")
   const [catPlayers, setcatPlayers] = useState(props.categoryPlayer ? { ...props.categoryPlayer } : {
      WK: [],
      AR: [],
      BOWL: [],
      BAT: []
   })

   useEffect(() => {
      updateInnerWidth(window.innerWidth);
   }, []);
   // console.log("statshub props catPlayers", catPlayers)
   // const [playerTeamIDList,setplayerTeamIDList]=useState([...props.teamList])
   const [playerTeamIDList, setplayerTeamIDList] = useState([...props.teamList])

   // console.log("statshub props playerTeamIDList", playerTeamIDList)



   const { loading: MatchupLoading, error: MatchUpsDataError, data: MatchUpsData } = useQuery(GET_MATCHUP_BY_ID,
      {
         variables: { crictecMatchId: props.matchID },
         onCompleted: (data) => {
            // console.log("while calling  MatchUpsData")
            if (data && data.matchupsById && data.matchupsById.matchUpData && RecentPlayer && RecentPlayer.playerID) {
               setMatchUpsPlayer2(data.matchupsById.matchUpData.filter(player => player.player1 === RecentPlayer.playerID)[0])

            }
         }
      }
   );
   let lang=props.language?props.language:'ENG'
   
   const getLangText = (lang,keys,key) => {
      // console.log("keys[key]",keys[key])
      let [english,hindi] = keys[key]
      switch(lang){
        case 'HIN':
          return hindi
        case 'ENG':
          return english
        default:
          return english
      }
        }

   const { loading, error, data: PlayerStatHubData } = useQuery(STAT_HUB_PLAYER_V2, {
      variables: { matchID: props.matchID }, onCompleted: (data) => {
         // console.log("data.playerHub.recentForm", data)
         if (data && data.playerHub && data.playerHub.recentForm && data.playerHub.recentForm.length > 0) {
            // console.log("before setting states")
            setRecentPlayer(data.playerHub.recentForm[0])
            // console.log()
            setPlayer2Compare(data.playerHub.recentForm.filter(pl=>pl.playerSkill===data.playerHub.recentForm[0].playerSkill && pl.playerID!==data.playerHub.recentForm[0].playerID)[0])
            // console.log("before calling  MatchUpsData")
            //  getMatchUpData({ variables: { crictecMatchId: props.matchID } })
            // console.log("after calling  MatchUpsData")
            let SelectedPlayerList = data.playerHub.recentForm.filter((pl) => playerTeamIDList.includes(pl.playerID))


            let categoryList = {
               WK: [...SelectedPlayerList.filter(pl => pl.playerSkill === 'KEEPER').map(pl => pl.playerID)],
               AR: [...SelectedPlayerList.filter(pl => pl.playerSkill === 'ALL_ROUNDER').map(pl => pl.playerID)],
               BOWL: [...SelectedPlayerList.filter(pl => pl.playerSkill === 'BOWLER').map(pl => pl.playerID)],
               BAT: [...SelectedPlayerList.filter(pl => pl.playerSkill === 'BATSMAN').map(pl => pl.playerID)]
            }

            // console.log("categoryList",categoryList)
            setcatPlayers({ ...catPlayers, ...categoryList })
            setPlayersList([...SelectedPlayerList])
            // setplayerList([...SelectedPlayerList])



         }
      }
   })

   // const callScroll = (data) => {
   //    data ? (document.body.style.overflow = 'hidden') : (document.body.style.overflow = 'unset');
   // }



   const choosePlayer = (player, type) => {

      if (type === 'recentForm') {
         setRecentPlayer(player)
         setPlayer2Compare(PlayerStatHubData && PlayerStatHubData.playerHub && PlayerStatHubData.playerHub.recentForm.filter(players => players.playerID !== player.playerID && players.playerSkill===player.playerSkill)[0])
         setopenSelectionModelV2(false)
         setMatchUpsPlayer2(MatchUpsData.matchupsById.matchUpData.filter(players => players.player1 === player.playerID)[0])
      }
      if (type === 'compare') {
         setPlayer2Compare(player)
         setCompareModel(false)
      }
      if (type === 'matchups') {
         setMatchUpsPlayer2(player)

         setOpenModelMatchups(false)
      }

   }
 

 
 



   let tout = null;
   const hideMsg = () => {
      // console.log(tout);
      if (tout) {
         clearTimeout(tout);
         tout = setTimeout(() => {
            seterrorMsg('');
         }, 3000);
      } else {
         tout = setTimeout(() => {
            seterrorMsg('');
         }, 3000);
      }
   };

  

   const handlecssPlayer1 = (type, stat, player1value, player2value) => {

      //  if (PlayersList.length + 1 <= 11) {
      //    if (tout) {
      //      clearTimeout(tout);
      //      seterrorMsg('');
      //    }
      if (type === "batting") {
         if (stat === "commonBatPosition") {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value !== "" && player2value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         } else {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value !== "" && player2value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         }
      } else if (type === "fantasy") {
         if (player1value > player2value) {
            return 'yellow-frc'
         } else {
            return "silver"
         }
      } else {
         if (stat === "bowlingEconomyRate") {
            if ((player1value !== "" && player2value !== "") && parseFloat(player1value) < parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value !== "" && player2value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }

         } else if (stat === "bowlingStrikeRate") {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value !== "" && player2value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }

         } else if (stat === "bowlingBestBowlingTotalSpell") {
            if (player1value !== "" && player2value !== "") {

               const bb1 = player1value !== "" ? ~~player1value.split("/")[0] : 0
               const bb2 = player2value !== "" ? ~~player2value.split("/")[0] : 0
               if (bb1 > bb2) {
                  return 'yellow-frc'
               } else if (bb1 == bb2) {
                  const b21 = player1value !== "" ? ~~player1value.split("/")[1] : 0
                  const b22 = player2value !== "" ? ~~player2value.split("/")[1] : 0
                  if (b21 > b22) { return 'yellow-frc' } else return 'silver'

               } else {
                  return 'silver'
               }
            } else if (player1value !== "" && player2value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }


         } else {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value !== "" && player2value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         }
      }
   }

   const handlecssPlayer2 = (type, stat, player1value, player2value) => {
      if (type === "batting") {

         if (stat === "commonBatPosition") {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player2value !== "" && player1value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         } else {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player2value !== "" && player1value === "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         }
      } else if (type === "fantasy") {
         if (player1value < player2value) {
            return 'yellow-frc'
         } else {
            return "silver"
         }
      } else {
         if (stat === "bowlingEconomyRate") {
            if ((player1value !== "" && player2value !== "") && parseFloat(player1value) > parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value === "" && player2value !== "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         } else if (stat === "bowlingStrikeRate") {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) > parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value === "" && player2value !== "") {
               return "yellow-frc"
            } else {
               return "silver"
            }

         } else if (stat === "bowlingBestBowlingTotalSpell") {
            if (player1value !== "" && player2value !== "") {

               const bb1 = player1value !== "" ? ~~player1value.split("/")[0] : 0
               const bb2 = player2value !== "" ? ~~player2value.split("/")[0] : 0
               if (bb1 < bb2) {
                  return 'yellow-frc'
               } else if (bb1 == bb2) {
                  const b21 = player1value !== "" ? ~~player1value.split("/")[1] : 0
                  const b22 = player2value !== "" ? ~~player2value.split("/")[1] : 0
                  if (b21 < b22) { return 'yellow-frc' } else return 'silver'

               } else {
                  return 'silver'
               }
            } else if (player1value === "" && player2value !== "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         } else {
            if (player1value !== "" && player2value !== "" && parseFloat(player1value) < parseFloat(player2value)) {
               return "yellow-frc"
            } else if (player1value === "" && player2value !== "") {
               return "yellow-frc"
            } else {
               return "silver"
            }
         }
      }
   }
   let router = useRouter();
   const closeNewModelV2 = () => {
      if (SelectionType === "matchups") {
         setOpenModelMatchups(false)
      }if(SelectionType === "compare"){
         setCompareModel(false)
      } else {
         setopenSelectionModelV2(false)
      }


   }
   // console.log("PlayerStatHubDataPlayerStatHubDataPlayerStatHubData",PlayerStatHubData)

   const [matchID, matchName, screen, tabname] = router.query.slugs;

   if (loading) return <Loading />;
   else if(error)
   return (
     <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
       <span style={{color:'white'}}>DATA NOT AVAIBLE</span>
       <span style={{color:'white'}}>Please refresh and try again...</span>
     </div>
   )
   else
      return (
         <div className="hidescroll white   bg-black">

            {props.mainData&& props.mainData[0] && props.mainData[0] && props.mainData[0] && props.mainData.length > 0 ?
               <div className="">

                  <div className="ba  br2 b--white-20 white ">
                     <div className={`flex items-center ${props.showHideFRC ? 'justify-between' : 'justify-center'} w-100 `}>
                        <div className={` ${props.showHideFRC ? 'w-80 w-90-l w-90-m' : 'w-95'}   `} >
                           <div className={`flex cursor-pointer  items-center mv2 h2-3 h2-6-l h2-6-m bg-white-10 justify-between  ba b--yellow ph2 pv1 br2  ${props.showHideFRC ? 'ml2' : ''} `} onClick={() => {
                          
                              // setPlayer2Compare(props.mainData[0] && props.mainData[0] && props.mainData[0].playerHub.recentForm.filter(players => players.playerID !== RecentPlayer.playerID)[0])

                           }}>
                              <div className="flex items-center">
                                 <div className="w15 h15 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                                    <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${ props.mainData[0] &&  props.mainData[0].teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                                 </div>
                                 <div className="ttu f6 f5-l f5-m fw6 ttu ph2">{props.mainData[0] && (lang==="HIN" ?props.mainData[0].playerNameHindi:props.mainData[0].name)}</div>
                              </div>
                            
                           </div>
                        </div>
                        {/* {console.log("before adding players to list", playerTeamIDList.filter(id => id == RecentPlayer.playerID))} */}
                      
                     </div>

                  
                     <div className="flex mv3">
                        <div className="w-50 flex ">
                           <div className="w-20 flex justify-center white"><div className="pa3 flex flex-column  items-center ml3 mt3">
                              <div className="fw4 f8 gray"> <div className="w15 w2-l w2-m   h15-l h15-m h13 br2 flex items-center justify-center bg-frc-yellow">
                                 <img className="h1 h13-l h13-m" src={props.mainData[0] && (props.mainData[0].playerSkill.toUpperCase() === "BATSMAN" ? black_bat : props.mainData[0].playerSkill.toUpperCase() === "BOWLER" ? black_ball : props.mainData[0].playerSkill.toUpperCase() === "KEEPER" ? black_keeper : black_allRounder)} /></div></div>
                              {lang==='HIN'?<div className="fw4 f8 gray pt1">{props.mainData[0] && (props.mainData[0].playerSkill.toUpperCase()=== "BATSMAN" ? "बल्लेबाज़" : props.mainData[0].playerSkill === "BOWLER" ? "गेंदबाज" : props.mainData[0].playerSkill === "KEEPER" ? "विकेट कीपर" : "ऑल राउंडर")}</div>
                              
                              :<div className="fw4 f8 gray pt1">{props.mainData[0] && (props.mainData[0].playerSkill.toUpperCase() === "BATSMAN" ? "BATTER" : props.mainData[0].playerSkill.toUpperCase() === "BOWLER" ? "BOWLER" : props.mainData[0].playerSkill.toUpperCase() === "KEEPER" ? "WK" : "AR")}</div>}
                           </div></div>
                           <div className="w-80 mb2 flex justify-center items-end">
                              <div className="h45   h6-l h6-m w4 w47-l w47-m">
                                 <img className="w-100 h-100" src={`https://images.cricket.com/players/${props.mainData[0] && props.mainData[0].playerID}_headshot_safari.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} />
                              </div>
                           </div>
                        </div>
                        <div className="w-50  flex flex-column   justify-center">
                           <div className="flex items-center justify-center">
                              <div className="yellow-frc  oswald fw6 f2">{props.mainData[0] && props.mainData[0].avgFantasyPoints||''}</div>
                              {/*  {getLangText(lang,words,'PLAYERS')} */}
                              {props.mainData[0] && props.mainData[0].avgFantasyPoints && <div className="white-50 fw4 oswald f7 f6-l f6-m  ph2"> {props.mainData[0] && (props.mainData[0].lastFiveMatches.length ===5? getLangText(lang,words,'AVG POINTS IN LAST 5 MATCHES'): props.mainData[0].lastFiveMatches.length>0 && props.mainData[0].lastFiveMatches.length<5?getLangText(lang,words,'AVG POINTS IN LAST FEW MATCHES'):'')}</div>}
                           </div>

                           <div className="bg-white-20 br2 ph2 pv3 ma2">
                              <div className="fw6 f7 white pv2">{getLangText(lang,words,'FANTASY PERFORMANCE')}</div>
                              <div className=" bb b--white-20"></div>
                              <div className="relative ">
                                 <LineChartFRC  data={props.mainData[0] && props.mainData[0].lastFiveMatches.length > 0 ? [...props.mainData[0].lastFiveMatches].reverse() : []}
                                    strokeClr={"#EEBA04"}
                                    width={innerWidth > 700 ? innerWidth * 25 / 100 : innerWidth > 500 ? innerWidth * 40 / 100 : innerWidth * 35 / 100}
                                    height={innerWidth > 700 ? 200 : 120} />
                                 <div className="absolute flex items-center  bottom-0 nowrap flex f0-4 f7-l f0-5-m ml0  ml5-l ml5-m" style={{ color: "#EEBA04", left: 25 }}><div className="flex items-center justify-center br-100    bg-frc-yellow black   pa1 fw7 " style={{ width: "14px", height: "14px", fontSize: "0.3rem" }}>DTA</div><span className="ph1">:</span><div className="text-xs" >{getLangText(lang,words,'DreamTeam_Appearance')}</div></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div>


                        <div className="pa2 ttu f6 fw6 ttu">{getLangText(lang,words,'RECENT PERFORMANCES')}</div>
                        <div className="mh2 bb b--white-20"></div>
                        <div className='w-100  flex flex-column items-center justify-center white pa1 mt3'>
                           {<div className='flex flex-wrap w-100 item-center justify-center light-gray  lh-copy ttu  f7 fw8 '>
                              <div className=' w-30  flex items-center justify-center '>{getLangText(lang,words,'OPPOSITION')}</div>
                              <div className='w-20 mh1 flex items-center justify-center ttu'>{getLangText(lang,words,'BATTING')}</div>

                              <div className='w-20 flex items-center justify-center '>{getLangText(lang,words,'BOWLING')}</div>

                              <div className='w-20 mh1 flex items-center justify-center '>{getLangText(lang,words,'POINTS')}</div>
                           </div>}

                           {props.mainData[0] && props.mainData[0].lastFiveMatches.length > 0 && props.mainData[0].lastFiveMatches.map((item, i) => {
                              return (
                                 <div className='flex flex-wrap w-100 h2-7 item-center justify-center mv1  white-60 f8 f7-l f7-m tc ' key={i}>
                                    <div className=' w-30  flex items-center justify-center bg-dark-gray ph1  pv2 ttu   '>
                                       <div className=' w-20  flex items-center justify-end tc '>vs</div>
                                       <div className=' w-50 w-25-l w-25-m flex items-center justify-center '>

                                          <img className='h13 object-cover w13  w2-l h2-l br-100' src={`https://images.cricket.com/teams/${item.oppTeamID}_flag_safari.png`} alt='' onError={(evt) => (evt.target.src = flagPlaceHolder)} />
                                       </div>

                                       <div className='w-30 flex items-center justify-start tc'> {item.oppTeamName} </div>
                                    </div>
                                    <div className='w-20  flex items-center  flex-column justify-center bg-dark-gray pa1 '>
                                       <div><span>{item.batting_stats.split('&')[0]}</span>{item.batting_stats.split('&').length>1 && <span>&</span>}</div>

                                       <div className="pt1">{item.batting_stats.split('&')[1]}</div>
                                    </div>

                                    <div className='w-20  flex flex-column items-center justify-center bg-dark-gray pa1'>
                                       <div><span>{item.bowling_stats&& item.bowling_stats.split('&')[0]}</span>{item.bowling_stats&& item.bowling_stats.split('&').length>1 && <span>&</span>}</div>
                                       <div className="pt1">{item.bowling_stats&& item.bowling_stats.split('&')[1]}</div>
                                      </div>

                                    <div className='w-20  flex items-center  bg-dark-gray  '>
                                       <div className={` w-50 oswald ${item.dta_flag ? "yellow-frc" : "white-60"} flex justify-end fw6 f7`}>{item.points}</div>
                                       {item.dta_flag && <div className=" w-50 ml2 " ><div className="flex items-center justify-center br-100   bg-frc-yellow black   pa1 fw7 " style={{ width: "18px", height: "18px", fontSize: "0.4rem" }}>DTA</div></div>}
                                    </div>
                                 </div>
                              );
                           })}
                        </div>




                        
                     </div>
                  </div>



                  { RecentPlayer && RecentPlayer.performanceIndicator && (RecentPlayer.performanceIndicator.overallRPI ||
                     RecentPlayer.performanceIndicator.overallWPI) &&
                     <div className="ba br2 b--white-20 white mt3">
                        <div className="flex justify-between items-center pv2  ph2">
                           <div className=' f5-l f5-m    ttu f6 lh-title fw6   '>
                           {getLangText(lang,words,'PERFORMANCE INDICATOR')} 
                           </div>
                           {/* {lang==='HIN'?<span className="gray f8 fw4 ">{`*  पिछले 5 वर्षों के ${PlayerStatHubData.playerHub.compType}  डेटा`}</span>:
               <span className="gray f8 fw4 ">{`* ${ PlayerStatHubData.playerHub.compType} data from last 5 years`}</span>} */}
               
                           {/* <div className="gray f8 fw4 ">{lang==='HIN'?'*पिछले 5 वर्षों के डेटा':'* data from last 5 years'}</div> */}

                        </div>
                        {/* {console.log("7787878",props.mainData[0])} */}
                        <div className="mh2  mb2 bb b--white-20"></div>
                        <PerformanceIndicator
                           width={500}
                           height={500}
                           top={20}
                           bottom={30}
                           left={30}
                           right={0}
                           playerRole={props.mainData[0] && props.mainData[0].playerSkill}
                           data={props.mainData[0] && props.mainData[0].performanceIndicator ? props.mainData[0].performanceIndicator : {}}
                           runs={getLangText(lang,words,'RUN PER INNINGS')}
                           wickets={getLangText(lang,words,'WICKETS PER INNINGS')}
                           lang={lang}
                        />

                     </div>}
               



                     { props.mainData[0] && props.mainData[0].highLowFlag && <div className='bg-black  br2 mt3 ba b--white-20 '>
                     <div className="flex justify-between items-center pv2  ph2">
                        <div className=' f5-l f5-m    ttu f6 lh-title fw6   '>
                        {getLangText(lang,words,'HIGHS & LOWS')}
                        </div>
                        {lang==='HIN'?<span className="gray f8 fw4 ">{`*  पिछले 5 वर्षों के ${PlayerStatHubData.playerHub.compType}  डेटा`}</span>:
               <span className="gray f8 fw4 ">{`* ${ PlayerStatHubData.playerHub.compType} data from last 5 years`}</span>}

                     </div>

                     <div className="mh2 bb b--white-20"></div>
                     {lang==="HIN"?<div>
                     {props.mainData[0] && (props.mainData[0].strengthInHindi.stmt1 || props.mainData[0].strengthInHindi.stmt2 || props.mainData[0].strengthInHindi.stmt3) && (props.mainData[0].weaknessInHindi.stmt1 || props.mainData[0].weaknessInHindi.stmt2 || props.mainData[0].weaknessInHindi.stmt3) && <div className="bg-dark-gray  flex flex-column items-center justify-center ma3 br3 ">
                        <div className="br2 flex w-50 mv2 items-center cursor-pointer justify-center bg-black" onClick={() => setquality(quality === "strength" ? "weakness" : "strength")}>
                           <div className={`w-50 flex items-center br2 ${quality === "strength" ? 'bg-frc-yellow black' : 'bg-black gray'} pv2 ph2  justify-center  fw6  f8 f7-l f7-m`} > {getLangText(lang,words,'STRENGTH')}</div>
                           <div className={`w-50  flex items-center br2 ph2 ${quality === "weakness" ? 'bg-frc-yellow black' : 'bg-black gray'} pv2 justify-center  fw6  f8 f7-l f7-m`}>{getLangText(lang,words,'WEAKNESS')}</div>


                        </div>

                        <div className=" flex flex-column items-center justify-center ">
                           {["stmt1", "stmt2", "stmt3"].map((stmt, i) =>
                              <>

                                 {props.mainData[0] && props.mainData[0][quality==="strength"?"strengthInHindi":"weaknessInHindi"][stmt] && <div key={i} className="tc  white    pv1 w-70  f8 f6-l f6-m lh-copy">
                                    {`${props.mainData[0][quality==="strength"?"strengthInHindi":"weaknessInHindi"][stmt]}`.split('$').
                                       map((word, j) =>
                                          <span key={j} className={`  ${j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'}`}>
                                             {word}
                                          </span>

                                       )}</div>}
                              </>
                           )}

                        </div>
                     </div>}
                     </div>:
                     <div>
                     {props.mainData[0] && (props.mainData[0].strength.stmt1 || props.mainData[0].strength.stmt2 || props.mainData[0].strength.stmt3) && (props.mainData[0].weakness.stmt1 || props.mainData[0].weakness.stmt2 || props.mainData[0].weakness.stmt3) && <div className="bg-dark-gray  flex flex-column items-center justify-center ma3 br3 ">
                        <div className="br2 flex w-50 mv2 items-center cursor-pointer justify-center bg-black" onClick={() => setquality(quality === "strength" ? "weakness" : "strength")}>
                           <div className={`w-50 flex items-center br2 ${quality === "strength" ? 'bg-frc-yellow black' : 'bg-black gray'} pv2 ph2  justify-center  fw6  f8 f7-l f7-m`} > {getLangText(lang,words,'STRENGTH')}</div>
                           <div className={`w-50  flex items-center br2 ph2 ${quality === "weakness" ? 'bg-frc-yellow black' : 'bg-black gray'} pv2 justify-center  fw6  f8 f7-l f7-m`}>{getLangText(lang,words,'WEAKNESS')}</div>


                        </div>

                        <div className=" flex flex-column items-center justify-center ">
                           {["stmt1", "stmt2", "stmt3"].map((stmt, i) =>
                              <>

                                 {props.mainData[0] && props.mainData[0][quality][stmt] && <div key={i} className="tc  white    pv1 w-70  f8 f6-l f6-m lh-copy">
                                    {`${props.mainData[0][quality][stmt]}`.split('$').
                                       map((word, j) =>
                                          <span key={j} className={`  ${j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'}`}>
                                             {word}
                                          </span>

                                       )}</div>}
                              </>
                           )}

                        </div>
                     </div>}
                     </div>}
                     {props.mainData[0] && props.mainData[0].bowlingDetails && props.mainData[0].bowlingDetails.bowlingType && props.mainData[0].bowlingDetails.bowlingType.length > 0 && <div>
                        <div className="pa2 ttu f6 fw6 ttu tc">{getLangText(lang,words,'AGAINST BOWLING TYPE')}</div>
                        <div className="flex  ma1 items-center justify-center pv3">
                           {["S/R", "AVERAGE", "DISMISSALS"].map((type, i) =>
                              <div
                                 onClick={() => setBowlingType(type)}
                                 className={`w-30 tc f7 pa2 ${type === bowlingType ? 'bg-frc-yellow black ' : 'bg-dark-gray white '
                                    } ba mh2  cursor-pointer mh5-l mont-semiBold b--black  fw6 br2`}
                                 key={i}>
                                {getLangText(lang,words,type)}
                              </div>)}
                        </div>
                        <RecentPlayerBowlingType RecentPlayer={props.mainData[0]} bowlingType={bowlingType} language={lang}/>
                       

                     </div>}


                     {props.mainData[0] && props.mainData[0].battingDetails && props.mainData[0].battingDetails.battingType && props.mainData[0].battingDetails.battingType.length > 0 &&
                        <div>
                           <div className="pa2 ttu f6 fw6 ttu tc"> {getLangText(lang,words,'AGAINST BATTING TYPE')}</div>
                           <div className="flex  ma1 items-center justify-center pv3">
                              {["WICKETS", "AVERAGE", "ECONOMY"].map((type, i) =>
                                 <div
                                    onClick={() => setBattingType(type)}
                                    className={`w-30 f7 pa2 tc ${type === battingType ? 'bg-frc-yellow black pa1' : 'bg-dark-gray white pa1'
                                       } ba mh2  cursor-pointer ttu mh5-l mont-semiBold b--black  fw6 br2`}
                                    key={i}>
                                    {getLangText(lang,words,type)}
                                 </div>)}
                           </div>
                           <RecentPlayerBattingType RecentPlayer={props.mainData[0]} battingType={battingType} language={lang}/>
                           

                        </div>}

                  </div>}
                  {/* high lo end */}

                
                  {MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData && MatchUpsData.matchupsById.matchUpData.length > 0 && <div className="bg-black  br2 mt3 ba b--white-20">
                     {props.mainData[0] && props.mainData[0].threat && <div>
                        <div className=' f5-l f5-m   pa2  ttu f6 lh-title fw6   '> {lang==='HIN'?'खतरा':'THREATS'} </div>
                        <div className="mh2 bb b--white-20"></div>
                        <div className="flex w-100">
                           <div className="w-50"><div className="w-100 flex items-center justify-center h4 bg-white-20 br2 mh2 mv3">


                              <div className="f7 fw5 white tc pa3">  {lang==='HIN'?<div>
                                 {`${props.mainData[0].threatInHindi}`.split('$').
                                    map((word, j) =>
                                       <span key={j} className={`  ${j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'}`}>
                                          {word}
                                       </span>

                                    )}</div>:<div>
                                    {`${props.mainData[0].threat}`.split('$').
                                       map((word, j) =>
                                          <span key={j} className={`  ${j % 2 === 0 ? ' fw4 ' : ' ph1 fw6'}`}>
                                             {word}
                                          </span>
   
                                       )}</div>}</div>
                           </div></div>

                           <div className="w-50 flex justify-center">
                              <div className="h4 pt2 mh2 mv3 w33 w44-l w44-m">
                                 <img className="w-100 h-100" src={`https://images.cricket.com/players/${props.mainData[0] && props.mainData[0].threatOppPlayerID}_headshot.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} />
                              </div>
                           </div>
                        </div>
                     </div>}

                     <div className=' f5-l f5-m   pa2  ttu f6 lh-title fw6   '>{getLangText(lang,words,'MATCHUPS')}  </div>
                     <div className="mh2 bb b--white-20"></div>
                     {true ? <div>
                        <div className='w-100 flex  bg-black   items-center justify-center mv3'>
                           <div
                              className='w-40  cursor-pointer      mh2 flex  items-center justify-center' >
                              <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                                 <div className='h2' ></div>
                                 <div
                                    className='h4 relative flex-column   justify-center items-center'>
                                    <div className='flex  overflow-hidden items-center  w-100 justify-center  ' >
                                       <img className=' br-100  h37 w37 bg-dark-gray   object-cover object-top' src={`https://images.cricket.com/players/${props.mainData[0] && props.mainData[0].playerID}_headshot.png`} alt='' onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} />
                                    </div>


                                    <img
                                       className='absolute bottom-1 object-cover    rounded-full w-10 h-10   ba b--black h08 br-100 w15 h15 absolute' style={{ left: 2 }}
                                       src={`https://images.cricket.com/teams/${props.mainData[0] && props.mainData[0].teamID}_flag_safari.png`}
                                       alt=''
                                       onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                    />
                                 </div>

                                 <div className="h2-5  pb3 pt2">
                                    <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{props.mainData[0] && (lang==="HIN" ?props.mainData[0].playerNameHindi:props.mainData[0].name)}</div>
                                 </div>
                              </div>
                           </div>

                           <div className='h2 w2 br-100 b--frc-yellow flex  black items-center bg-frc-yellow f4 fw6 justify-center'>vs</div>
                           {/* {console.log("MatchUpsData",MatchUpsData)}
                              {console.log("MatchUpsPlayer2",MatchUpsPlayer2)}
                              {console.log("ldjlflkff",RecentPlayer.playerID)}
                              {console.log("fsjnfsfs",RecentPlayer  && MatchUpsData &&  MatchUpsData.matchupsById&&  MatchUpsData.matchupsById.matchUpData.filter((pl=>pl.player1===RecentPlayer.playerID)))} */}
                           
                           
                           {!MatchUpsPlayer2 ? <div
                              className='w-40 cursor-pointer bg-dark-gray  ba b--yellow br2  mh2 flex  items-center justify-center'
                              style={{ background: '#101010' }} onClick={() => { setOpenModelMatchups(true); setSelectionType('matchups') }}>
                              <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                                 <div className='flex justify-end w-100' >
                                    <img className="w13" src={compare} alt="" />
                                 </div>
                                 <div
                                    className='h4 relative flex-column   justify-center items-center'
                                    style={{ background: 'rgb(16, 16, 16)' }}>
                                    <div className='flex   items-center  w-100 justify-center ' style={{ background: '#101010' }}>
                                       <img className=' br-100  h37 w37   bg-dark-gray object-cover object-top' src={`https://images.cricket.com/players/${props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.length > 0 && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].player2 : null}_headshot.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} alt='' />
                                    </div>


                                    {true ? <img
                                       className='absolute bottom-1 object-cover    rounded-full w-10 h-10  left-0 ba b--black h08 br-100 w15 h15 absolute' style={{ left: 2 }}
                                       src={`https://images.cricket.com/teams/${props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].player2Team : null}_flag_safari.png`}
                                       alt=''
                                       onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                    /> : <img
                                       className='absolute bottom-1 object-cover    rounded-full w-10 h-10   ba b--black h08 br-100 w13 h13 absolute'
                                       src={flagPlaceHolder}
                                       alt=''
                                       onError={(evt) => (evt.target.src = flagPlaceHolder)}

                                    />}
                                 </div>

                                {lang==="HIN"?<div className="h2-5 pb3 pt2">
                                    <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].player2HindiName : ""}</div>
                                 </div>: <div className="h2-5 pb3 pt2">
                                    <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].player2Name : ""}</div>
                                 </div>}
                              </div>
                           </div> :
                              <div
                                 className='w-40 cursor-pointer bg-dark-gray  ba b--yellow br2  mh2 flex  items-center justify-center'
                                 style={{ background: '#101010' }} onClick={() => { setOpenModelMatchups(true); setSelectionType('matchups') }}>
                                 <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                                    <div className='flex justify-end w-100' >
                                       <img className="w13" src={compare} alt="" />
                                    </div>
                                    <div
                                       className='h4 relative flex-column   justify-center items-center'
                                       style={{ background: 'rgb(16, 16, 16)' }}>
                                       <div className='flex   items-center  w-100 justify-center ' style={{ background: '#101010' }}>
                                          <img className=' br-100  h37 w37   bg-dark-gray object-cover object-top' src={`https://images.cricket.com/players/${MatchUpsPlayer2 && MatchUpsPlayer2.player2}_headshot.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} alt='' />
                                       </div>


                                       {true ? <img
                                          className='absolute bottom-1 object-cover    rounded-full w-10 h-10  left-0 ba b--black h08 br-100 w15 h15 absolute' style={{ left: 2 }}
                                          src={`https://images.cricket.com/teams/${MatchUpsPlayer2 && MatchUpsPlayer2.player2Team}_flag_safari.png`}
                                          alt=''
                                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                       /> : <img
                                          className='absolute bottom-1 object-cover    rounded-full w-10 h-10   ba b--black h08 br-100 w13 h13 absolute'
                                          src={flagPlaceHolder}
                                          alt=''
                                          onError={(evt) => (evt.target.src = flagPlaceHolder)}

                                       />}
                                    </div>

                                   { lang==="HIN"?<div className="h2-5 pb3 pt2">
                                       <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{MatchUpsPlayer2 && MatchUpsPlayer2.player2HindiName }</div>
                                    </div>:
                                    <div className="h2-5 pb3 pt2">
                                       <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{MatchUpsPlayer2 && MatchUpsPlayer2.player2Name }</div>
                                    </div>}
                                 </div>
                              </div>
                           }
                        </div>

                        {!MatchUpsPlayer2 ? <div className='flex w-100  flex-column justify-start items-start tr f6 fw5 bt b--white-20  pa1'>
                           {' '}
                           <div className='w-100 flex flex-row items-center justify-center'>
                              <div className='w-33  flex flex-column  items-center justify-center'>

                                 <div className='w-100 tc yellow-frc f2 fw6'> {props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].ballsFaced : 0}</div>
                                 <div className='w-100 tc mt1 ttc f7 fw6'>{getLangText(lang,words,'BALLS FACED')} </div>
                              </div>

                              <div className='w-33 flex  bl br b--white-20 flex-column  items-center justify-center'>
                                 <div className='w-100 tc yellow-frc f2 fw6'>{props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].Dismissals : 0}</div>
                                 <div className='w-100 tc mt1 f7 ttc fw6'>{getLangText(lang,words,'WICKETS')} </div>
                              </div>

                              <div className='w-33 flex flex-column  items-center justify-center'>
                                 <div className='w-100 tc yellow-frc f2 fw6'>{props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0].runsScored : 0}</div>
                                 <div className='w-100 tc mt1 f7 fw6  ttc'> {getLangText(lang,words,'RUNS')}</div>
                              </div>
                           </div>
                        </div> : <div className='flex w-100  flex-column justify-start items-start tr f6 fw5 bt b--white-20  pa1'>
                           {' '}
                           <div className='w-100 flex flex-row items-center justify-center'>
                              <div className='w-33  flex flex-column  items-center justify-center'>

                                 <div className='w-100 tc yellow-frc  f2 fw6'> {MatchUpsPlayer2 && MatchUpsPlayer2.ballsFaced}</div>
                                 <div className='w-100 tc mt1 ttc  f7 fw6'>{getLangText(lang,words,'BALLS FACED')}  </div>
                              </div>

                              <div className='w-33 flex  bl br b--white-20 flex-column  items-center justify-center'>
                                 <div className='w-100 tc yellow-frc f2 fw6'>{MatchUpsPlayer2 && MatchUpsPlayer2.Dismissals}</div>
                                 <div className='w-100 tc mt1 ttc f7 fw6'> {getLangText(lang,words,'WICKETS')}</div>
                              </div>

                              <div className='w-33 flex flex-column  items-center justify-center'>
                                 <div className='w-100 tc yellow-frc f2 fw6'>{MatchUpsPlayer2 && MatchUpsPlayer2.runsScored}</div>
                                 <div className='w-100 tc mt1 ttc f7 fw6'>  {getLangText(lang,words,'RUNS')}</div>
                              </div>
                           </div>
                        </div>}
                     </div>
                        : <div className="fw5 f5 pv3 tc white"> Data not available</div>}
                  </div>}


                  <div className="bg-black  br2 mt3 ba b--white-20">

                     <div className=' f5-l f5-m   pa2  ttu f6 lh-title fw6   '> {getLangText(lang,words,'COMPARE')}  </div>
                     <div className="mh2 bb b--white-20"></div>
                     {true ? <div>
                        <div className='w-100 flex  bg-black   items-center justify-center mv3'>
                           <div
                              className='w-40  cursor-pointer      mh2 flex  items-center justify-center' >
                              <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                                 <div className='h2' ></div>
                                 <div
                                    className='h4 relative flex-column   justify-center items-center'>
                                    <div className='flex  overflow-hidden items-center  w-100 justify-center  ' >
                                       <img className=' br-100  h37 w37 bg-dark-gray   object-cover object-top' src={`https://images.cricket.com/players/${props.mainData[0] && props.mainData[0].playerID}_headshot.png`} alt='' onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} />
                                    </div>


                                    <img
                                       className='absolute bottom-1 object-cover    rounded-full w-10 h-10   ba b--black h08 br-100 w15 h15 absolute' style={{ left: 2 }}
                                       src={`https://images.cricket.com/teams/${props.mainData[0] && props.mainData[0].teamID}_flag_safari.png`}
                                       alt=''
                                       onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                    />
                                 </div>

                                 <div className="h2-5  pb3 pt2">
                                    <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{props.mainData[0] && (lang==="HIN" ?props.mainData[0].playerNameHindi:props.mainData[0].name)}</div>
                                 </div>
                              </div>
                           </div>

                           <div className='h2 w2 br-100 b--frc-yellow flex  black items-center bg-frc-yellow f4 fw6 justify-center'>vs</div>

                           <div
                              className='w-40 cursor-pointer bg-dark-gray  ba b--yellow br2  mh2 flex  items-center justify-center'
                              style={{ background: '#101010' }} onClick={() => { setCompareModel(true); setSelectionType('compare') }}>
                              <div className='  flex   flex-column items-center  w-100 justify-center shadow-4 pa2'>
                                 <div className='flex justify-end w-100' >
                                    <img className="w13" src={compare} alt="" />
                                 </div>
                                 <div
                                    className='h4 relative flex-column   justify-center items-center'
                                    style={{ background: 'rgb(16, 16, 16)' }}>
                                    <div className='flex   items-center  w-100 justify-center ' style={{ background: '#101010' }}>
                                       <img className=' br-100  h37 w37   bg-dark-gray object-cover object-top' src={`https://images.cricket.com/players/${Player2Compare && Player2Compare.playerID}_headshot.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} alt='' />
                                    </div>


                                    {true ? <img
                                       className='absolute bottom-1 object-cover    rounded-full w-10 h-10  left-0 ba b--black h08 br-100 w15 h15 absolute' style={{ left: 2 }}
                                       src={`https://images.cricket.com/teams/${Player2Compare && Player2Compare.teamID}_flag_safari.png`}
                                       alt=''
                                       onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                    /> : <img
                                       className='absolute bottom-1 object-cover    rounded-full w-10 h-10   ba b--black h08 br-100 w13 h13 absolute'
                                       src={flagPlaceHolder}
                                       alt=''
                                       onError={(evt) => (evt.target.src = flagPlaceHolder)}

                                    />}
                                 </div>

                                 <div className="h2-5 pb3 pt2">
                                    <div className='flex  items-center  w-100 justify-center white f7 pv2 tc fw6 tc'>{Player2Compare && (lang==="HIN" ?Player2Compare.playerNameHindi:Player2Compare.name) }</div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="oswald ttu mt2">
                          { <div>{[1].map((stat, i) =>
                              <div className="mh3" key={i}>
                                 <div className="flex w-100 items-center justify-center mv2 f4">
                                    <div className={`w-30 flex items-center justify-center  f3 oswald fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer1("fantasy", stat, props.mainData[0].avgFantasyPoints, Player2Compare.avgFantasyPoints)}`}>{props.mainData[0] && props.mainData[0].avgFantasyPoints !== "" ? props.mainData[0].avgFantasyPoints : "--"}</div>
                                    <div className='w-30 flex items-center  justify-center silver   f5  fw6  oswald tc ttu'>
                                       <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'औसत':'Average'} <br />{lang==='HIN'?'फ़ैंटसी अंक': 'Fantasy Points'}</div>
                                    </div>
                                    <div className={`w-30 flex items-center justify-center  f3 oswald fw6 ${props.mainData[0] && Player2Compare && handlecssPlayer2("fantasy", stat, props.mainData[0].avgFantasyPoints, Player2Compare.avgFantasyPoints)}`}>{Player2Compare && Player2Compare.avgFantasyPoints !== "" ? Player2Compare.avgFantasyPoints : "--"}</div>
                                 </div>

                              </div>)}</div>}
                      { props.mainData[0] && props.mainData[0].playerSkill === "BOWLER"?<>
                      {props.mainData[0] && <div>
                              <div className="fw6 f6 gray ph2 pv3">{getLangText(lang,words,'BOWLING STATS')}</div>
                              <div className="mh2 bb b--white-20"></div>
                              <div>{['bowlingWickets', 'bowlingStrikeRate', 'bowlingEconomyRate'].map((stat, i) =>
                                 <div className={`${i !== 2 ? 'mh3 bb b--white-20' : 'mh3'}`} key={i}>
                                    <div className="flex w-100 items-center justify-center mv2 f4">
                                       <div className={`w-30 flex items-center justify-center  f3 oswald fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer1("bowling", stat, props.mainData[0].statsHubPlayerbowling[stat], Player2Compare.statsHubPlayerbowling[stat])}`}>{props.mainData[0] && props.mainData[0].statsHubPlayerbowling && props.mainData[0].statsHubPlayerbowling[stat] !== "" ? props.mainData[0].statsHubPlayerbowling[stat] : "--"}</div>
                                       <div className='w-30 flex items-center justify-center silver oswald lh-copy f5 tc fw6 ttu'>
                                          {stat === 'bowlingBestBowlingTotalSpell' ? <div className="f5 lh-copy fw6  oswald">best<br /> figures</div> : stat === 'bowlingEconomyRate' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'इकॉनमी':'ECONOMY'}</div> : stat === 'bowlingStrikeRate' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'स्ट्राइक रेट':'SR'}</div> : <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'प्रति पारी':'Wickets per'}<br />{lang==='HIN'?' विकेट':'innings'}</div>}
                                       </div>
                                       <div className={`w-30 flex items-center justify-center    f3 oswald  fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer2("bowling", stat, props.mainData[0].statsHubPlayerbowling[stat], Player2Compare.statsHubPlayerbowling[stat])}`}>{Player2Compare && Player2Compare.statsHubPlayerbowling && Player2Compare.statsHubPlayerbowling[stat] ? Player2Compare.statsHubPlayerbowling[stat] : "--"}</div>
                                    </div>

                                 </div>)}</div>
                           </div>}

                           {props.mainData[0] && <div>
                              <div className="fw6 f6 gray ph2 pv3">{getLangText(lang,words,'BATTING STATS')}</div>
                              <div className="mh2 bb b--white-20"></div>
                              <div className="mv1">{['commonBatPosition', 'battingRunsInnings', 'battingStrikeRate', 'battingFoursSixes'].map((stat, i) =>
                                 <div className={`${i !== 3 ? 'mh3 bb b--white-20' : 'mh3'}`} key={i}>
                                    <div className="flex w-100 items-center justify-center mv2 f4">
                                       <div className={`w-30 flex items-center justify-center  f3 oswald fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer1("batting", stat, props.mainData[0].statsHubPlayerbatting[stat], Player2Compare.statsHubPlayerbatting[stat])}`}>{props.mainData[0] && props.mainData[0].statsHubPlayerbatting && props.mainData[0].statsHubPlayerbatting[stat] !== "" ? props.mainData[0].statsHubPlayerbatting[stat] : "--"}</div>
                                       <div className='w-30 flex items-center  justify-center silver   f5  fw6  oswald tc ttu'>
                                          {stat === 'battingAverage' ? <div className="f5 lh-copy fw6  oswald">{getLangText(lang,words,'AVERAGE')}</div> : stat === 'battingFoursSixes' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'प्रति':'Boundaries'} <br />{lang==='HIN'?' मैच बाउंड्री':'per match'}</div> : stat === 'battingHighestScore' ? <div className="f5  fw6 lh-copy oswald"> Highest <br /> Score</div> : stat === 'battingRunsInnings' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'प्रति':'Runs'}<br />{lang==='HIN'?'पारी रन':'per innings'}</div> : stat === "commonBatPosition" ? <div className="f5  fw6 lh-copy  oswald">{lang==='HIN'?'सामान्य':'COMMON'} <br/>{lang==='HIN'?'बल्लेबाजी की स्थिति':'BAT POSITION'}</div> : <div className="f5  fw6 lh-copy  oswald">{lang==='HIN'?'स्ट्राइक रेट':'SR'}</div>}
                                       </div>

                                       <div className={`w-30 flex items-center justify-center  f3 oswald fw6 ${props.mainData[0] && Player2Compare && handlecssPlayer2("batting", stat, props.mainData[0].statsHubPlayerbatting[stat], Player2Compare.statsHubPlayerbatting[stat])}`}>{Player2Compare && Player2Compare.statsHubPlayerbatting && Player2Compare.statsHubPlayerbatting[stat] !== "" ? Player2Compare.statsHubPlayerbatting[stat] : "--"}</div>
                                    </div>

                                 </div>)}</div>
                           </div>}
                      </>:<>
                      {props.mainData[0] && <div>
                              <div className="fw6 f6 gray ph2 pv3">{getLangText(lang,words,'BATTING STATS')}</div>
                              <div className="mh2 bb b--white-20"></div>
                              <div className="mv1">{['commonBatPosition', 'battingRunsInnings', 'battingStrikeRate', 'battingFoursSixes'].map((stat, i) =>
                                 <div className={`${i !== 3 ? 'mh3 bb b--white-20' : 'mh3'}`} key={i}>
                                    <div className="flex w-100 items-center justify-center mv2 f4">
                                       <div className={`w-30 flex items-center justify-center  f3 oswald fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer1("batting", stat, props.mainData[0].statsHubPlayerbatting[stat], Player2Compare.statsHubPlayerbatting[stat])}`}>{props.mainData[0] && props.mainData[0].statsHubPlayerbatting && props.mainData[0].statsHubPlayerbatting[stat] !== "" ? props.mainData[0].statsHubPlayerbatting[stat] : "--"}</div>
                                       <div className='w-30 flex items-center  justify-center silver   f5  fw6  oswald tc ttu'>
                                          {stat === 'battingAverage' ? <div className="f5 lh-copy fw6  oswald">{getLangText(lang,words,'AVERAGE')}</div> : stat === 'battingFoursSixes' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'प्रति':'Boundaries'} <br />{lang==='HIN'?' मैच बाउंड्री':'per match'}</div> : stat === 'battingHighestScore' ? <div className="f5  fw6 lh-copy oswald"> Highest <br /> Score</div> : stat === 'battingRunsInnings' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'प्रति':'Runs'}<br />{lang==='HIN'?'पारी रन':'per innings'}</div> : stat === "commonBatPosition" ? <div className="f5  fw6 lh-copy  oswald">{lang==='HIN'?'सामान्य':'COMMON'} <br/>{lang==='HIN'?'बल्लेबाजी की स्थिति':'BAT POSITION'}</div> : <div className="f5  fw6 lh-copy  oswald">{lang==='HIN'?'स्ट्राइक रेट':'SR'}</div>}
                                       </div>

                                       <div className={`w-30 flex items-center justify-center  f3 oswald fw6 ${props.mainData[0] && Player2Compare && handlecssPlayer2("batting", stat, props.mainData[0].statsHubPlayerbatting[stat], Player2Compare.statsHubPlayerbatting[stat])}`}>{Player2Compare && Player2Compare.statsHubPlayerbatting && Player2Compare.statsHubPlayerbatting[stat] !== "" ? Player2Compare.statsHubPlayerbatting[stat] : "--"}</div>
                                    </div>

                                 </div>)}</div>
                           </div>}
                      {props.mainData[0] && <div>
                              <div className="fw6 f6 gray ph2 pv3">{getLangText(lang,words,'BOWLING STATS')}</div>
                              <div className="mh2 bb b--white-20"></div>
                              <div>{['bowlingWickets', 'bowlingStrikeRate', 'bowlingEconomyRate'].map((stat, i) =>
                                 <div className={`${i !== 2 ? 'mh3 bb b--white-20' : 'mh3'}`} key={i}>
                                    <div className="flex w-100 items-center justify-center mv2 f4">
                                       <div className={`w-30 flex items-center justify-center  f3 oswald fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer1("bowling", stat, props.mainData[0].statsHubPlayerbowling[stat], Player2Compare.statsHubPlayerbowling[stat])}`}>{props.mainData[0] && props.mainData[0].statsHubPlayerbowling && props.mainData[0].statsHubPlayerbowling[stat] !== "" ? props.mainData[0].statsHubPlayerbowling[stat] : "--"}</div>
                                       <div className='w-30 flex items-center justify-center silver oswald lh-copy f5 tc fw6 ttu'>
                                          {stat === 'bowlingBestBowlingTotalSpell' ? <div className="f5 lh-copy fw6  oswald">best<br /> figures</div> : stat === 'bowlingEconomyRate' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'इकॉनमी':'ECONOMY'}</div> : stat === 'bowlingStrikeRate' ? <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'स्ट्राइक रेट':'SR'}</div> : <div className="f5 lh-copy fw6  oswald">{lang==='HIN'?'प्रति पारी':'Wickets per'}<br />{lang==='HIN'?' विकेट':'innings'}</div>}
                                       </div>
                                       <div className={`w-30 flex items-center justify-center    f3 oswald  fw6  ${props.mainData[0] && Player2Compare && handlecssPlayer2("bowling", stat, props.mainData[0].statsHubPlayerbowling[stat], Player2Compare.statsHubPlayerbowling[stat])}`}>{Player2Compare && Player2Compare.statsHubPlayerbowling && Player2Compare.statsHubPlayerbowling[stat] ? Player2Compare.statsHubPlayerbowling[stat] : "--"}</div>
                                    </div>

                                 </div>)}</div>
                           </div>}

                          
                      </>}
                        
                        </div>
                     </div>
                        : <div className="fw5 f5 pv3 tc white"> Data not available</div>}
                  </div>

                  {openSelectionModelV2 && (
                     <PlayerModelV2
                        closeNewModelV2={closeNewModelV2}
                        playersList={PlayerStatHubData.playerHub.recentForm}
                        choosePlayer={choosePlayer}
                        RecentPlayer={props.mainData[0]}
                        lang={lang}
                        Player2={SelectionType === 'compare' ? Player2Compare : null}//do same for matchups
                        SelectionType={SelectionType}


                     />)}

                  {OpenModelMatchups && (
                     <MatchUpsModel
                        closeNewModelV2={closeNewModelV2}
                        playersList={MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.filter(player => player.player1 === props.mainData[0].playerID)}
                        choosePlayer={choosePlayer}
                        lang={lang}
                        RecentPlayer={props.mainData[0]}
                        Player2Obj={!MatchUpsPlayer2 && props.mainData[0] && MatchUpsData && MatchUpsData.matchupsById && MatchUpsData.matchupsById.matchUpData.length > 0 && MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID)).length > 0 ? MatchUpsData.matchupsById.matchUpData.filter((pl => pl.player1 === props.mainData[0].playerID))[0] : MatchUpsPlayer2}//do same for matchups
                        SelectionType={SelectionType}

                     />)}

                     {openCompareModel && 
                        <CompareModelPlayerHub
                        closeNewModelV2={closeNewModelV2}
                        playersList={PlayerStatHubData.playerHub.recentForm}
                        choosePlayer={choosePlayer}
                        lang={lang}
                        RecentPlayer={props.mainData[0]}
                        Player2Obj={Player2Compare}//do same for matchups
                        SelectionType={SelectionType} />

                     }



              




                 

                




                
               </div> : <div className=" br2 ba  b--white-20 ">
               <DataNotFound /></div>}

         </div>
      )
}