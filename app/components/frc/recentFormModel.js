import React, { useEffect } from 'react';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import CleverTap from 'clevertap-react';
import ImageWithFallback from '../commom/Image';
const playerPlaceholder = '/pngs/fallbackprojection.png';
export default function RecentFormModel({ selectedPlayer, playerData, setNewRecentFormPlayer, type, ...props }) {
  useEffect(() => {
    props.callScroll(true);
    return () => {
      props.callScroll(false);
    };
  }, []);

  return (
    <div
      className='flex justify-center items-center fixed absolute--fill z-9999 bg-basebg/30 overflow-y-scroll'
      style={{ backdropFilter: 'blur(4px)' }}>
      <div className=' bg-basebg br3 shadow-4  ba b--white-20 relative  min-vh-90 pt2 mh2  w-100 w-50-l'>
        <div className='w-100  flex flex-row '>
          <div className='flex w-80 justify-start f5 white fw6 ph2 items-center ttu'>current selection</div>
          <div
            className='flex w-20 justify-end items-center white fw6 ph3 pv2 cursor-pointer'
            onClick={() => {
              props.setrecentFormModel(false);
            }}>
            X
          </div>
        </div>
        <div className='  w-100   flex   flex-wrap justify-start ph2  bb b--white-20 items-center'>
          <div className='w-50 flex  flex-column justify-center'>
            <div
              className=' ph2  cursor-pointer   mh1 mv3 flex  flex-column justify-center items-center  bg-dark-gray  ba b--white-20 br2 white'
              style={{ background: 'rgb(16, 16, 16)' }}>
              <div
                className=' mt1  relative flex-column  justify-center items-center'
                style={{ background: 'rgb(16, 16, 16)' }}>
                <div
                  className='br-50  w3 h3  overflow-hidden bg-gray ba  b--white-20'
                  style={{ background: 'rgb(16, 16, 16)' }}>
                  <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {playerPlaceholder}
                    className='flex    justify-center items-center'
                    src={`https://images.cricket.com/players/${selectedPlayer.playerID}_headshot.png`}
                    alt={`https://images.cricket.com/players/${selectedPlayer.playerID}_headshot.png`}
                    // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                  />
                </div>

                <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                  className='absolute bottom-0 left-0  br-100 w13 h13 ba b--black  object-cover '
                  src={`https://images.cricket.com/teams/${selectedPlayer.teamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                  alt=''
                />
              </div>
              <div className='mt1 w-100 h2  fw5 ml1  f6  fw6  justify-center items-center tc'>
                <div> {selectedPlayer.name} </div>
              </div>
            </div>
          </div>
        </div>

        <div className='flex w-100 justify-start pv2 white f5 fw6 ph2 items-center ttu'>Change Player</div>
        <div className='overflow-y-scroll pv2 overflow-hidden hidescroll ph2 ' style={{ maxHeight: '57vh' }}>
          <div className='  w-100  flex flex-wrap justify-between    items-center mt2'>
            {playerData
              .filter((player) => player.playerSkill === type && player.playerID !== selectedPlayer.playerID)
              .map((item, index) => (
                <div key={index} className='w-50'>
                  <div
                    onClick={() => {
                      CleverTap.initialize('FantasyPlayerSwap', {
                        source: 'RecentForm',
                        SeriesID: props.seriesID,
                        TeamID: item.teamID,
                        MatchStatus: props.matchStatus,
                        MatchID: props.matchID,
                        MatchType: props.matchType,
                        playerID: item.playerID,
                        Platform: localStorage ? localStorage.Platform : ''
                      });
                      setNewRecentFormPlayer(item);
                    }}
                    className=' mh1 mv1 cursor-pointer   flex  flex-column justify-center items-center  bg-dark-gray  ba b--white-20 br2 white'
                    style={{ background: 'rgb(16, 16, 16)' }}>
                    <div
                      className=' mt1  relative flex-column  justify-center items-center'
                      style={{ background: 'rgb(16, 16, 16)' }}>
                      <div
                        className='br-50  w3 h3  overflow-hidden bg-gray ba b--white-20'
                        style={{ background: 'rgb(16, 16, 16)' }}>
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {playerPlaceholder}
                          className='flex    justify-center items-center'
                          src={`https://images.cricket.com/players/${item.playerID}_headshot.png`}
                          alt={`https://images.cricket.com/players/${item.playerID}_headshot.png`}
                          // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                        />
                      </div>

                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                        className='absolute bottom-0 left-0 rounded-full w-24 h-24 ba b--black  object-cover'
                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                        alt={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      />
                    </div>
                    <div className='mt1 w-100 h2  fw5 ml1  f6  fw6  justify-center items-center tc'>
                      <div> {item.name} </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
        {/* <div className="flex justify-center absolute bottom-0 right-0 left-0 pb3 pb4-l">
          <img className="w2" src={'/svgs/yellowDownArrow.svg'} />
        </div> */}
      </div>
    </div>
  );
}
