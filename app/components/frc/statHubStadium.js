import React, { useState, useEffect, useRef } from 'react'
import { STAT_HUB_STADIUM, MATCH_SUMMARY } from '../../api/queries'
import { format } from 'date-fns'
import { useQuery, useLazyQuery } from '@apollo/react-hooks'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const location = '/pngsV2/location.png'
const rightArrow = '/svgs/yellowRightArrow.svg'
const downArrow = '/svgs/downArrow.svg'
const upArrow = '/svgs/downArrow.svg'
import { getLangText } from '../../api/services'
const yellowDownArrow = '/svgs/yellowDownArrow.svg'
import { words } from '../../constant/language'
import DataNotFound from '../commom/datanotfound'
import Tab from '../shared/Tab'
import axios from 'axios'
import Heading from '../commom/heading'
import ImageWithFallback from '../commom/Image'
import Loading from '../loading'
import Topperform from '../commom/topPerformerCard'
const playerPlaceholder = '/pngs/fallbackprojection.png'

export default function StatHubStadium(props) {
  let lang = props.language ? props.language : 'ENG'
  const { loading, error, data: StadiumStatHub } = useQuery(STAT_HUB_STADIUM, {
    variables: { matchID: props.matchID },
  })
  const playerRecordTab = [
    { name: getLangText(lang, words, 'BATTING'), value: 'batting' },
    { name: getLangText(lang, words, 'BOWLING'), value: 'bowling' },
  ]
  const [
    getSummary,
    { error: scoreError, loading: scoreLoading, data: summaryData },
  ] = useLazyQuery(MATCH_SUMMARY)

  const [viewAll, setViewAll] = useState(false)
  const [playerRecord, setPlayerRecord] = useState(playerRecordTab[0])
  const [showDropDown, setShowDropDown] = useState(false)
  const [battingType, setBattingType] = useState('battingAverage')
  const [bowlingType, setBowlingType] = useState('totalWickets')
  const battingTab = [
    'totalRuns',
    'battingAverage',
    'battingStrikeRate',
    'fifties',
    'hundreds',
    'totalFours',
    'totalSixes',
  ]
  const bowlingTab = [
    'totalWickets',
    'bowlingStrikeRate',
    'economy',
    'threeWickets',
    'fiveWickets',
  ]

  const [RecentMatchIndex, setRecentMatchIndex] = useState({})
  const [dropDown, setDropDown] = useState(false)
  const [type, setType] = useState('battingStrikeRate')
  // const [bowlingType, setBowlingType] = useState("bowlingStrikeRate")
  const RecentMatchTab = ['HEAD TO HEAD', 'DC', 'MI']

  // const getLangText = (lang, keys, key) => {
  //   let [english, hindi] = keys[key]
  //   switch (lang) {
  //     case 'HIN':
  //       return hindi
  //     case 'ENG':
  //       return english
  //     default:
  //       return english
  //   }
  // }
  const getSummaryData = async (matchID, i) => {
    if (!RecentMatchIndex[i]) {
      await getSummary({ variables: { matchID: matchID, status: 'completed' } })
    }
    setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] })
  }
  const wrapperRef = useRef(null)
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setShowDropDown(false)
      }
    }

    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [wrapperRef])

  const isEnglish = (lang) => {
    if (lang == 'ENG') return true
    else false
  }

  if (loading) return <Loading />
  else
    return (
      <>
        {StadiumStatHub && StadiumStatHub.stadiumHub ? (
          <div className="pb-2 mx-3 ">
            <div className="  text-black dark:text-white  py-2 text-center dark:bg-basebg  rounded   ">
              <div className="flex flex-col items-center justify-center ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  className="h-12 py-2 w-8"
                  src={location}
                />
                <div className=" font-semibold  ">
                  {StadiumStatHub &&
                  StadiumStatHub.stadiumHub &&
                  isEnglish(lang)
                    ? StadiumStatHub.stadiumHub.venueName
                    : StadiumStatHub.stadiumHub.venueNameInHindi}
                </div>
              </div>

              <div className="border  dark:border-none bg-white  text-black dark:bg-none dark:text-white dark:bg-gray my-2 rounded-lg">
                {StadiumStatHub &&
                  StadiumStatHub.stadiumHub &&
                  StadiumStatHub.stadiumHub.characteristics && (
                    <div className="w-full  p-2">
                      <div className=" flex w-1/2 ">
                        {' '}
                        <Heading
                          heading={getLangText(lang, words, 'CHARACTERISTICS')}
                        />
                      </div>

                      <div className="flex w-full flex-wrap items-center  justify-center gap-2 md:gap-8 lg:gap-8 my-4 ">
                        {lang === 'HIN'
                          ? StadiumStatHub &&
                            StadiumStatHub.stadiumHub &&
                            Object.keys(
                              StadiumStatHub.stadiumHub.characteristicsInHindi,
                            ).map((character, i) => (
                              <div
                                className={`  ${
                                  character === 'weather' &&
                                  StadiumStatHub.stadiumHub
                                    .characteristicsInHindi['weather'] ===
                                    'अनुपलब्ध' || StadiumStatHub.stadiumHub.characteristics[
                                      character] === null
                                    ? ''
                                    : ' black  bg-green p-2  capatilize rounded-lg text-xs text-gray '
                                } `}
                                key={i}
                              >
                                {character === 'weather' &&
                                StadiumStatHub.stadiumHub
                                  .characteristicsInHindi['weather'] ===
                                  'अनुपलब्ध'
                                  ? ''
                                  : character === 'weather' &&
                                    StadiumStatHub.stadiumHub
                                      .characteristicsInHindi['weather'] !==
                                      'अनुपलब्ध'
                                  ? 'मौसम : ' +
                                    StadiumStatHub.stadiumHub
                                      .characteristicsInHindi[character]
                                  : StadiumStatHub.stadiumHub
                                      .characteristicsInHindi[character]}
                              </div>
                            ))
                          : StadiumStatHub &&
                            StadiumStatHub.stadiumHub &&
                            Object.keys(
                              StadiumStatHub.stadiumHub.characteristics,
                            ).map((character, i) => (
                              <div
                                className={`  ${
                                  character === 'weather' &&
                                  StadiumStatHub.stadiumHub.characteristics[
                                    'weather'
                                  ] === 'N/A' || StadiumStatHub.stadiumHub.characteristics[
                                    character] === null
                                    ? ''
                                    : ' black bg-green p-2 capatilize rounded-lg text-xs text-gray '
                                } `}
                                key={i}
                              >{console.log('blank data --  ',StadiumStatHub.stadiumHub.characteristics[
                                character])}
                                {character === 'weather' &&
                                StadiumStatHub.stadiumHub.characteristics[
                                  'weather'
                                ] === 'N/A'
                                  ? ''
                                  : character === 'weather' &&
                                    StadiumStatHub.stadiumHub.characteristics[
                                      'weather'
                                    ] !== 'N/A'
                                  ? 'weather : ' +
                                    StadiumStatHub.stadiumHub.characteristics[
                                      character
                                    ]
                                  : StadiumStatHub.stadiumHub.characteristics[
                                    character
                                  ]
                                }
                              </div>
                            ))}
                      </div>
                    </div>
                  )}
                <div className="flex  py-2">
                  <div className="w-3/12 flex flex-col gap-2  border-r dark:border-black ">
                    <div className="text-gray-2 font-semibold   text-xs  ">
                      {lang === 'HIN' ? 'मैचों की' : 'Number of'}
                      <br /> {lang === 'HIN' ? 'संख्या' : 'matches'}
                    </div>
                    <div className=" text-basered dark:text-white font-bold text-xl  ">
                      {(StadiumStatHub &&
                        StadiumStatHub.stadiumHub &&
                        StadiumStatHub.stadiumHub.groundStats &&
                        StadiumStatHub.stadiumHub.groundStats.totalMatches) ||
                        0}
                    </div>
                  </div>
                  <div className="bl b--white-20"></div>
                  <div className="w-3/12 flex flex-col gap-2 border-r dark:border-black ">
                    <div className="text-gray-2 font-semibold text-xs ">
                      {lang === 'HIN' ? 'पहली पारी' : 'Avg 1st'}
                      <br /> {lang === 'HIN' ? 'का औसत' : 'Innings Total'}
                    </div>
                    <div className="  text-basered dark:text-white font-bold text-xl   ">
                      {StadiumStatHub &&
                        StadiumStatHub.stadiumHub &&
                        StadiumStatHub.stadiumHub.groundStats &&
                        StadiumStatHub.stadiumHub.groundStats
                          .avgFirstInningScore}
                    </div>
                  </div>
                  <div className="bl b--white-20"></div>
                  <div className="w-3/12 flex flex-col gap-2 border-r dark:border-black">
                    <div className="text-gray-2 font-semibold  text-xs ">
                      {lang === 'HIN' ? 'पहले बैटिंग करते' : 'Won by team'}
                      <br />
                      {lang === 'HIN' ? 'हुए जीत' : 'batting 1st'}
                    </div>
                    {/* <div className=" text-white font-bold text-xl tracked ">18/38</div> */}
                    <div className="  text-basered dark:text-white font-bold text-xl  tracked  ">
                      {StadiumStatHub &&
                        StadiumStatHub.stadiumHub &&
                        StadiumStatHub.stadiumHub.groundStats &&
                        StadiumStatHub.stadiumHub.groundStats
                          .firstBattingWinPercent}
                      %
                    </div>
                  </div>
                  <div className="bl  b--white-20"></div>
                  <div className="w-3/12 flex flex-col gap-2">
                    <div className="text-gray-2 font-semibold  text-xs ">
                      {lang === 'HIN' ? 'विकेट विभाजन:' : 'Wickets split'}{' '}
                      <br /> {lang === 'HIN' ? 'पेस बनाम स्पिन ' : 'Pace-Spin'}
                    </div>
                    <div className="  text-basered dark:text-white font-bold text-xl  tracked  ">
                      {StadiumStatHub &&
                        StadiumStatHub.stadiumHub &&
                        StadiumStatHub.stadiumHub.groundStats &&
                        StadiumStatHub.stadiumHub.groundStats.pacerWickets}
                      -
                      {StadiumStatHub &&
                        StadiumStatHub.stadiumHub &&
                        StadiumStatHub.stadiumHub.groundStats &&
                        StadiumStatHub.stadiumHub.groundStats.spinnerWickets}
                    </div>
                    {StadiumStatHub &&
                      StadiumStatHub.stadiumHub &&
                      StadiumStatHub.stadiumHub.groundStats &&
                      StadiumStatHub.stadiumHub.groundStats
                        .paceWicketPercent !== '' &&
                      StadiumStatHub.stadiumHub.groundStats
                        .spinWicketPercent !== '' && (
                        <div className="text-xs">
                          {Math.round(
                            StadiumStatHub.stadiumHub.groundStats
                              .paceWicketPercent,
                          )}
                          %-
                          {Math.round(
                            StadiumStatHub.stadiumHub.groundStats
                              .spinWicketPercent,
                          )}
                          %
                        </div>
                      )}
                    {/* <div className="f6-l f7 fw5">49%</div> */}
                  </div>
                </div>
                {StadiumStatHub &&
                  StadiumStatHub.stadiumHub &&
                  StadiumStatHub.stadiumHub.characteristics && (
                    <div className="text-left text-xs p-2">
                      {lang === 'HIN' ? (
                        <span>{`*  पिछले 3 वर्षों के ${StadiumStatHub.stadiumHub.compType}  डेटा`}</span>
                      ) : (
                        <span>{`* ${StadiumStatHub.stadiumHub.compType} data from last 3 years`}</span>
                      )}
                    </div>
                  )}
              </div>
            </div>

            {StadiumStatHub &&
              StadiumStatHub.stadiumHub &&
              StadiumStatHub.stadiumHub.recentMatches && (
                <div>
                  <div className="mv2">
                    <div className="h-solid-divider-light"></div>
                  </div>

                  <div className="border  dark:border-none bg-white text-black dark:bg-none dark:text-white dark:bg-gray mb-3 p-2 rounded-lg">
                    <div className="px-2">
                      <Heading
                        heading={getLangText(lang, words, 'RECENT MATCHES')} 
                      />
                    </div>

                    <div className=" p-1 pb-2  ">
                      {StadiumStatHub &&
                      StadiumStatHub.stadiumHub &&
                      StadiumStatHub.stadiumHub.recentMatches &&
                      StadiumStatHub.stadiumHub.recentMatches.length > 0 ? (
                        StadiumStatHub.stadiumHub.recentMatches.map(
                          (team, i) => (
                            <>
                              <div
                                className={
                                  'flex  cursor-pointer  bg-gray-10 text-black dark:text-white  items-center py-2 px-1 rounded-lg justify-between dark:bg-gray-8 mt-3'
                                }
                                key={i}
                                onClick={() => {
                                  getSummaryData(team.matchID, i)
                                  // setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });
                                }}
                              >
                                <div className="flex items-center w-11/12 ">
                                  <div className=" w-7/12 gap-1 lg:px-8 md:px-8 flex md:gap-4 lg:gap-2 items-center ">
                                    <img
                                      className="h-4 w-6 object-cover"
                                      src={
                                        team.winnerTeam === team.awayTeam
                                          ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png`
                                          : team.winnerTeam === team.homeTeam
                                          ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png`
                                          : flagPlaceHolder
                                      }
                                      alt=""
                                      onError={(evt) =>
                                        (evt.target.src =
                                          '/pngsV2/flag_dark.png')
                                      }
                                    />
                                    <span className="text-sm">Vs</span>
                                    <img
                                      className="h-4 w-6 object-cover"
                                      src={
                                        team.winnerTeam === team.awayTeam
                                          ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png`
                                          : team.winnerTeam === team.homeTeam
                                          ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png`
                                          : flagPlaceHolder
                                      }
                                      alt=""
                                      onError={(evt) =>
                                        (evt.target.src =
                                          '/pngsV2/flag_dark.png')
                                      }
                                    />
                                    <div className="md:px-4 lg:px-4 px-2 flex items-center">
                                      <div className="text-xs text-black dark:text-white tracked ">
                                        {lang === 'ENG'
                                          ? team.matchResult
                                          : team.matchResultHindi}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="w-2/12">
                                    <div className="tracked text-gray-2 text-xs">
                                      {format(
                                        +team.matchDate,
                                        'MMM dd, yyyy. ',
                                      )}
                                    </div>
                                  </div>
                                  <div className=" w-2/12">
                                    {/* <div className='flex items-center pv1'>
                                             <div className={`ba  pv1 border ${team.winnerTeam === team.homeTeam ? ' border-green text-gray-2 ' : 'border-gray-2 white'}  br2   ph2  f6 fw5 text-xs p-1`}>{team.homeTeam}</div>
                                             <div className='ph2 moon-gray mx-1'> vs</div>
                                             <div className={`ba  br2 border  pt-2 ${team.winnerTeam === team.awayTeam ? 'border-green text-gray-2 ' : 'border-gray-2 white'} pv1    ph2 f6 fw5 text-xs p-1`}>{team.awayTeam}</div>
                                          </div> */}

                                    <div className="flex items-center  ">
                                      {/* <div className="tracked text-gray-2 text-xs">{format(+team.matchDate, 'MMM dd, yyyy. ')}</div> */}
                                      <img
                                        className="w-4 px-1 "
                                        src={location}
                                      ></img>
                                      <div className="text-gray-2 font-semiBold text-xs truncate">
                                        {isEnglish(lang)
                                          ? team.venue
                                          : team.venueHindi}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className=" w-1/12 flex items-center justify-end px-2">
                                  <img
                                    className={`w-4 h-4 ${
                                      RecentMatchIndex[i] ? 'rotate-90' : ''
                                    }`}
                                    src={'/svgs/red-right.svg'}
                                  />
                                </div>

                                {/* <div className='flex items-center w-10/12 '>
                                             <div className=' w-1/4 tc flex justify-center items-center'>
                                                <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc={flagPlaceHolder}
                                                   className='h-8 w-12 object-cover'
                                                   src={team.winnerTeam === team.awayTeam ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png` :
                                                      team.winnerTeam === team.homeTeam ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png` : flagPlaceHolder
                                                   }
                                                   alt=''
                                                // onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                                                />
                                             </div>
                                             <div className='  w-70'>
                                                <div className='flex items-center pv1'>
                                                   <div className={`ba  pv1 border text-xs p-1 ${team.winnerTeam === team.homeTeam ? ' border-green text-gray-2 ' : 'b--gray white'}  br2   ph2  f6 fw5`}>{team.homeTeam}</div>
                                                   <div className='ph2 moon-gray mx-1'>vs</div>
                                                   <div className={`ba  br2 border text-xs p-1 ${team.winnerTeam === team.awayTeam ? 'border-green text-gray-2 ' : 'b--gray white'} pv1    ph2 f6 fw5`}>{team.awayTeam}</div>
                                                </div>
                                                <div className='text-xs text-white tracked mt-2'>{isEnglish(lang) ? team.matchResult : team.matchResultHindi}</div>
                                                <div className='flex items-center  f7 moon-gray lh-title '>
                                                   <div className="tracked text-gray-2  text-xs">{format(+team.matchDate, 'MMM dd, yyyy. ')}</div>
                                                   <ImageWithFallback height={18} width={18} loading='lazy'
                                                      className='w-5 px-1 ' src={location} />
                                                   <div className='text-gray-2 font-semiBold text-xs'>{isEnglish(lang) ? team.venue : team.venueHindi}</div>
                                                </div>
                                             </div>
                                          </div>
                                          <div
                                             className=' flex items-center justify-end mr-2'>
                                             {RecentMatchIndex[i] ? <svg width='30' focusable='false' viewBox='0 0 24 24'>
                                                <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                                                <path fill='none' d='M0 0h24v24H0z'></path>
                                             </svg> : <svg width='30' viewBox='0 0 24 24'>
                                                <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                                                <path fill='none' d='M0 0h24v24H0z'></path>
                                             </svg>}

                                          </div> */}
                              </div>
                              {RecentMatchIndex[i] &&
                                summaryData &&
                                summaryData.matchSummary && (
                                  <div className=' my-2 p-1  dark:border border-gray-8 text-black dark:text-white white bg-white dark:bg-gray-8'>
                                    <Topperform
                                      data={summaryData}
                                      lang={lang}
                                      matchType={props.matchType}
                                      
                                    />
                                    <div className="bg-gray hidden mx-3 pb-3 mb-3">
                                      {/* <div className='f6 silver ph3 pv2 fw7'>MATCH SUMMARY</div> */}
                                      <div className="flex">
                                        <div className=" w-6/12 flex flex-col p-2 items-center  ">
                                          <div className="text-gray-2 font-semibold mb-1 text-xs">
                                            TOP BATTER
                                          </div>
                                          <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                                            <div className="overflow-hidden w-full h-full rounded-full ">
                                              <ImageWithFallback
                                                height={18}
                                                width={18}
                                                loading="lazy"
                                                fallbackSrc={playerPlaceholder}
                                                className=" object-top object-contain w-24   "
                                                style={{
                                                  objectPosition: '0 0%',
                                                }}
                                                src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBatsman.playerID}_headshot.png`}
                                                // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                alt=""
                                              />
                                            </div>
                                          </div>

                                          <div className="w-full -mt-10 bg-gray-4 text-center  rounded-md px-2">
                                            <div className="text-base  font-semibold mt-10  truncate">
                                              {lang === 'HIN'
                                                ? summaryData.matchSummary
                                                    .bestBatsman.playerNameHindi
                                                : summaryData.matchSummary
                                                    .bestBatsman.playerName}
                                            </div>
                                            {summaryData.matchSummary
                                              .bestBatsman.battingStatsList &&
                                            team.matchType !== 'Test' ? (
                                              <div>
                                                <div className="flex  flex-col items-center py-1">
                                                  {summaryData.matchSummary
                                                    .bestBatsman
                                                    .battingStatsList[0] && (
                                                    <div className=" font-semibold">
                                                      <span className="f5 fw5 mr1">
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .battingStatsList[0]
                                                            .runs
                                                        }
                                                        {summaryData
                                                          .matchSummary
                                                          .bestBatsman
                                                          .battingStatsList[0]
                                                          .isNotOut === true
                                                          ? '*'
                                                          : ''}
                                                      </span>
                                                      <span className="f6">
                                                        ({' '}
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .battingStatsList[0]
                                                            .balls
                                                        }{' '}
                                                        )
                                                      </span>
                                                    </div>
                                                  )}
                                                  <div className="flex items-center justify-between mt-2">
                                                    <div className="mr-1">
                                                      <span className="text-xs text-white ">
                                                        4s{' '}
                                                      </span>
                                                      <span className="text-xs text-white">
                                                        {` `}
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .battingStatsList[0]
                                                            .fours
                                                        }
                                                      </span>
                                                    </div>
                                                    <div className="ml-1 border-l">
                                                      <span className="fw4 f6 gray ml-2">
                                                        6s
                                                      </span>
                                                      <span className="fw6 f6 white">
                                                        {` `}
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .battingStatsList[0]
                                                            .sixes
                                                        }
                                                      </span>
                                                    </div>
                                                  </div>
                                                </div>
                                                {false &&
                                                  summaryData.matchSummary
                                                    .bestBatsman
                                                    .bowlingStatsList && (
                                                    <div className="flex  items-center pv1">
                                                      <div className="ba bg-silver br2 pv1 fw5 b--black  ph2">
                                                        <span className="f5 fw5 mr1 nowrap">
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[0]
                                                              .wickets
                                                          }
                                                          /
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[0]
                                                              .runsConceded
                                                          }
                                                        </span>
                                                        <span className="f7 ">
                                                          (
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[0]
                                                              .overs
                                                          }
                                                          )
                                                        </span>
                                                      </div>
                                                      <div className="pl2 ">
                                                        <span className="fw4 f6 gray">
                                                          Eco
                                                        </span>
                                                        <span className="fw6 pl1 f7 white">
                                                          {summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .bowlingStatsList &&
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[0]
                                                              .economyRate}
                                                        </span>
                                                      </div>
                                                    </div>
                                                  )}
                                              </div>
                                            ) : (
                                              <div>
                                                <div className="flex  items-center pv1">
                                                  {summaryData.matchSummary
                                                    .bestBatsman
                                                    .battingStatsList[0] && (
                                                    <div className="ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap ph2">
                                                      <span className="f5 fw5 mr1">
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .battingStatsList[0]
                                                            .runs
                                                        }
                                                        {summaryData
                                                          .matchSummary
                                                          .bestBatsman
                                                          .battingStatsList[0]
                                                          .isNotOut === true
                                                          ? '*'
                                                          : ''}
                                                      </span>
                                                      <span className="f6">
                                                        ({' '}
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .battingStatsList[0]
                                                            .balls
                                                        }{' '}
                                                        )
                                                      </span>
                                                    </div>
                                                  )}
                                                  {summaryData.matchSummary
                                                    .bestBatsman
                                                    .battingStatsList[1] && (
                                                    <div className="ba bg-frc-yellow black nowrap fw5 br2 pv1 b--black  ml2 ph2">
                                                      {summaryData.matchSummary
                                                        .bestBatsman
                                                        .battingStatsList[1] && (
                                                        <span className="f5 fw5">
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .battingStatsList[1]
                                                              .runs
                                                          }
                                                        </span>
                                                      )}
                                                      {summaryData.matchSummary
                                                        .bestBatsman
                                                        .battingStatsList[1] && (
                                                        <span className="f7">{`(${summaryData.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                                                      )}
                                                    </div>
                                                  )}
                                                </div>
                                                {summaryData.matchSummary
                                                  .bestBatsman
                                                  .bowlingStatsList && (
                                                  <div className="flex  items-center pv1">
                                                    <div className="ba bg-silver br2 pv1 fw5 b--black  ph2">
                                                      {' '}
                                                      <span className="f5 fw5">
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .bowlingStatsList[0]
                                                            .wickets
                                                        }
                                                        /
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .bowlingStatsList[0]
                                                            .runsConceded
                                                        }
                                                      </span>
                                                      <span className="f7 pl2">
                                                        (
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBatsman
                                                            .bowlingStatsList[0]
                                                            .overs
                                                        }
                                                        )
                                                      </span>
                                                    </div>
                                                    {summaryData.matchSummary
                                                      .bestBatsman
                                                      .bowlingStatsList[1] && (
                                                      <div className="ph1">
                                                        &
                                                      </div>
                                                    )}
                                                    {summaryData.matchSummary
                                                      .bestBatsman
                                                      .bowlingStatsList[1] && (
                                                      <div className="ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap  ph2">
                                                        <span className="f5 fw5">
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[1]
                                                              .wickets
                                                          }
                                                          /
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[1]
                                                              .runsConceded
                                                          }
                                                        </span>
                                                        <span className="f7 pl2">
                                                          (
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBatsman
                                                              .bowlingStatsList[1]
                                                              .overs
                                                          }
                                                          )
                                                        </span>
                                                      </div>
                                                    )}
                                                  </div>
                                                )}
                                              </div>
                                            )}
                                          </div>
                                        </div>

                                        {summaryData.matchSummary.bestBowler
                                          .bowlingStatsList &&
                                        team.matchType !== 'Test' ? (
                                          <div className="w-6/12 flex flex-col p-2 items-center">
                                            <div className="text-gray-2 font-semibold mb-1 text-xs">
                                              TOP BOWLER
                                            </div>
                                            <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                                              <div className="overflow-hidden w-full h-full rounded-full ">
                                                <ImageWithFallback
                                                  height={18}
                                                  width={18}
                                                  loading="lazy"
                                                  fallbackSrc={
                                                    playerPlaceholder
                                                  }
                                                  className=" object-top object-contain w-24  rounded-full  "
                                                  src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                                  // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                  alt=""
                                                />
                                              </div>
                                            </div>

                                            <div className="w-full -mt-10 bg-gray-4 text-center  rounded-md px-2">
                                              <div className="text-base  font-semibold mt-10 truncate">
                                                {lang === 'HIN'
                                                  ? summaryData.matchSummary
                                                      .bestBowler
                                                      .playerNameHindi
                                                  : summaryData.matchSummary
                                                      .bestBowler.playerName}
                                              </div>
                                              <div className="flex flex-col  items-center justify-between ">
                                                {summaryData.matchSummary
                                                  .bestBowler
                                                  .battingStatsList &&
                                                  (summaryData.matchSummary
                                                    .bestBowler
                                                    .battingStatsList[0]
                                                    .runs !== 0 ||
                                                    summaryData.matchSummary
                                                      .bestBowler
                                                      .battingStatsList[0]
                                                      .balls !== 0) && (
                                                    <div className="ba bg-frc-yellow br2 pv1 b--black  black ph2">
                                                      {' '}
                                                      <span className="f5 fw5 mr1 nowrap">
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .battingStatsList[0]
                                                            .runs
                                                        }
                                                        {summaryData
                                                          .matchSummary
                                                          .bestBowler
                                                          .battingStatsList[0]
                                                          .isNotOut === true
                                                          ? '*'
                                                          : ''}
                                                      </span>
                                                      <span className="f7 ">
                                                        (
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .battingStatsList[0]
                                                            .balls
                                                        }
                                                        )
                                                      </span>
                                                    </div>
                                                  )}
                                                {summaryData.matchSummary
                                                  .bestBowler
                                                  .battingStatsList[1] && (
                                                  <div className="f6 fw6 ph2">
                                                    &
                                                  </div>
                                                )}
                                                {summaryData.matchSummary
                                                  .bestBowler
                                                  .bowlingStatsList && (
                                                  <div className="">
                                                    <span className=" font-bold text-white">
                                                      {
                                                        summaryData.matchSummary
                                                          .bestBowler
                                                          .bowlingStatsList[0]
                                                          .wickets
                                                      }
                                                      /
                                                      {
                                                        summaryData.matchSummary
                                                          .bestBowler
                                                          .bowlingStatsList[0]
                                                          .runsConceded
                                                      }
                                                    </span>
                                                    <span className="f7 ">
                                                      (
                                                      {
                                                        summaryData.matchSummary
                                                          .bestBowler
                                                          .bowlingStatsList[0]
                                                          .overs
                                                      }
                                                      )
                                                    </span>
                                                  </div>
                                                )}
                                              </div>
                                              <div className="text-center mt-2  pb-2">
                                                <span className="text-white text-xs">
                                                  Eco :
                                                </span>
                                                <span className="text-white text-base font-semibold ml-1">
                                                  {summaryData.matchSummary
                                                    .bestBatsman
                                                    .bowlingStatsList &&
                                                    summaryData.matchSummary
                                                      .bestBatsman
                                                      .bowlingStatsList[0]
                                                      .economyRate}
                                                </span>
                                              </div>
                                            </div>
                                          </div>
                                        ) : (
                                          <div className="flex w-100 items-center justify-center pv2 ">
                                            <div className="w-40 flex items-end justify-end">
                                              {' '}
                                              <ImageWithFallback
                                                height={18}
                                                width={18}
                                                loading="lazy"
                                                fallbackSrc={playerPlaceholder}
                                                className="h4 h45-l"
                                                src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                                // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                alt=""
                                              />
                                            </div>
                                            <div className="w-60  flex lh-copy flex-column  items-start justify-start ">
                                              <div className="f6 fw5">
                                                {lang === 'HIN'
                                                  ? summaryData.matchSummary
                                                      .bestBowler
                                                      .playerNameHindi
                                                  : summaryData.matchSummary
                                                      .bestBowler.playerName}
                                              </div>
                                              <div className="flex   ">
                                                <div className="">
                                                  {summaryData.matchSummary
                                                    .bestBowler &&
                                                    summaryData.matchSummary
                                                      .bestBowler
                                                      .battingStatsList &&
                                                    summaryData.matchSummary
                                                      .bestBowler
                                                      .battingStatsList.length >
                                                      0 && (
                                                      <div className="ba bg-frc-yellow br2 pv1 tc b--black black ph2">
                                                        {' '}
                                                        <span className="f6 fw5">
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBowler
                                                              .battingStatsList[0]
                                                              .runs
                                                          }
                                                          {summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .battingStatsList[0]
                                                            .isNotOut === true
                                                            ? '*'
                                                            : ''}
                                                        </span>
                                                        <span className="f7 ">
                                                          &nbsp;(
                                                          {
                                                            summaryData
                                                              .matchSummary
                                                              .bestBowler
                                                              .battingStatsList[0]
                                                              .balls
                                                          }
                                                          )
                                                        </span>
                                                      </div>
                                                    )}
                                                  {summaryData.matchSummary
                                                    .bestBowler
                                                    .bowlingStatsList && (
                                                    <div className="ba mv2  bg-silver br2 tc pv1 b--black  ph2">
                                                      {' '}
                                                      <span className="f6 fw5">
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .bowlingStatsList[0]
                                                            .wickets
                                                        }
                                                        /
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .bowlingStatsList[0]
                                                            .runsConceded
                                                        }
                                                      </span>
                                                      {/* <span>&nbsp;</span> */}
                                                      <span className="f7 ">
                                                        &nbsp;(
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .bowlingStatsList[0]
                                                            .overs
                                                        }
                                                        )
                                                      </span>
                                                    </div>
                                                  )}
                                                </div>
                                                <div className="">
                                                  {summaryData.matchSummary
                                                    .bestBowler
                                                    .battingStatsList[1] && (
                                                    <div className="pa1 f7">
                                                      &
                                                    </div>
                                                  )}
                                                  {summaryData.matchSummary
                                                    .bestBowler
                                                    .bowlingStatsList[1] && (
                                                    <div className="mv3 ph1  f7">
                                                      &
                                                    </div>
                                                  )}
                                                </div>
                                                <div className="">
                                                  {summaryData.matchSummary
                                                    .bestBowler &&
                                                    summaryData.matchSummary
                                                      .bestBowler
                                                      .battingStatsList &&
                                                    summaryData.matchSummary
                                                      .bestBowler
                                                      .battingStatsList.length >
                                                      0 &&
                                                    summaryData.matchSummary
                                                      .bestBowler
                                                      .battingStatsList[1] && (
                                                      <div className="ba bg-frc-yellow  tc br2 pv1 b--black black  ph2">
                                                        {summaryData
                                                          .matchSummary
                                                          .bestBowler
                                                          .battingStatsList[1] && (
                                                          <span className="f6 fw5">
                                                            {
                                                              summaryData
                                                                .matchSummary
                                                                .bestBowler
                                                                .battingStatsList[1]
                                                                .runs
                                                            }
                                                            {summaryData
                                                              .matchSummary
                                                              .bestBowler
                                                              .battingStatsList[1]
                                                              .isNotOut === true
                                                              ? '*'
                                                              : ''}
                                                          </span>
                                                        )}
                                                        {summaryData
                                                          .matchSummary
                                                          .bestBowler
                                                          .battingStatsList[1] && (
                                                          <span className="f7 ">
                                                            &nbsp;(
                                                            {
                                                              summaryData
                                                                .matchSummary
                                                                .bestBowler
                                                                .battingStatsList[1]
                                                                .balls
                                                            }
                                                            )
                                                          </span>
                                                        )}
                                                      </div>
                                                    )}
                                                  {summaryData.matchSummary
                                                    .bestBowler
                                                    .bowlingStatsList[1] && (
                                                    <div className="ba mv2 bg-silver br2 pv1 b--black  tc ph2">
                                                      {' '}
                                                      <span className="f6 fw5">
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .bowlingStatsList[1]
                                                            .wickets
                                                        }
                                                        /
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .bowlingStatsList[1]
                                                            .runsConceded
                                                        }
                                                      </span>
                                                      <span className="f7 ">
                                                        &nbsp;(
                                                        {
                                                          summaryData
                                                            .matchSummary
                                                            .bestBowler
                                                            .bowlingStatsList[1]
                                                            .overs
                                                        }
                                                        )
                                                      </span>
                                                    </div>
                                                  )}
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        )}
                                      </div>

                                      {summaryData.matchSummary.inningOrder
                                        .slice(
                                          0,
                                          team.matchType !== 'Test'
                                            ? 2
                                            : summaryData.matchSummary
                                                .inningOrder.length,
                                        )
                                        .map((inning, i) => (
                                          <div key={i}>
                                            <div className="h-solid-divider-light mh3 mv2 "></div>
                                            <div>
                                              <div className="bg-gray-4 m-2 rounded-md p-2">
                                                <div className="flex ph3 items-center justify-between">
                                                  <div className="flex items-center">
                                                    <ImageWithFallback
                                                      height={18}
                                                      width={18}
                                                      loading="lazy"
                                                      fallbackSrc={
                                                        flagPlaceHolder
                                                      }
                                                      className=" w-10 h-10 rounded-full  object-cover objetc-top"
                                                      src={`https://images.cricket.com/teams/${summaryData.matchSummary[inning].teamID}_flag_safari.png`}
                                                      alt=""
                                                      // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                                    />
                                                    <div className="pl2 f6  fw5">
                                                      {summaryData.matchSummary[
                                                        inning
                                                      ].teamShortName.toUpperCase()}
                                                    </div>
                                                  </div>
                                                  <div className="flex items-center  justify-center">
                                                    <div className="text-white font-semibold  oswald f3 fw6">
                                                      {
                                                        summaryData
                                                          .matchSummary[inning]
                                                          .runs[i <= 1 ? 0 : 1]
                                                      }
                                                      /
                                                      {
                                                        summaryData
                                                          .matchSummary[inning]
                                                          .wickets[
                                                          i <= 1 ? 0 : 1
                                                        ]
                                                      }
                                                    </div>
                                                    <div className="text-white font-semibold  ml-1">
                                                      (
                                                      {
                                                        summaryData
                                                          .matchSummary[inning]
                                                          .overs[i <= 1 ? 0 : 1]
                                                      }
                                                      )
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div className="ph3 pv2 flex">
                                                {summaryData.matchSummary[
                                                  inning
                                                ][
                                                  i <= 1
                                                    ? 'batsmanSummary1'
                                                    : 'batsmanSummary2'
                                                ].topBatsman &&
                                                  summaryData.matchSummary[
                                                    inning
                                                  ][
                                                    i <= 1
                                                      ? 'batsmanSummary1'
                                                      : 'batsmanSummary2'
                                                  ].topBatsman
                                                    .battingStatsList && (
                                                    <div className="w-5/12 ph2">
                                                      <div className="text-gray-2 uppercase text-xs font-semibold">
                                                        Batter
                                                      </div>
                                                      <div className="flex items-center moon-gray justify-between pv1">
                                                        {lang === 'HIN' ? (
                                                          <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                            {' '}
                                                            {
                                                              summaryData
                                                                .matchSummary[
                                                                inning
                                                              ][
                                                                i <= 1
                                                                  ? 'batsmanSummary1'
                                                                  : 'batsmanSummary2'
                                                              ].topBatsman
                                                                .playerNameHindi
                                                            }
                                                          </div>
                                                        ) : (
                                                          <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                            {' '}
                                                            {
                                                              summaryData
                                                                .matchSummary[
                                                                inning
                                                              ][
                                                                i <= 1
                                                                  ? 'batsmanSummary1'
                                                                  : 'batsmanSummary2'
                                                              ].topBatsman
                                                                .playerName
                                                            }
                                                          </div>
                                                        )}
                                                        <div className="text-white font-semibold">
                                                          {' '}
                                                          {
                                                            summaryData
                                                              .matchSummary[
                                                              inning
                                                            ][
                                                              i <= 1
                                                                ? 'batsmanSummary1'
                                                                : 'batsmanSummary2'
                                                            ].topBatsman
                                                              .battingStatsList[0]
                                                              .runs
                                                          }
                                                          {summaryData
                                                            .matchSummary[
                                                            inning
                                                          ][
                                                            i <= 1
                                                              ? 'batsmanSummary1'
                                                              : 'batsmanSummary2'
                                                          ].topBatsman
                                                            .battingStatsList[0]
                                                            .isNotOut === true
                                                            ? '*'
                                                            : ''}
                                                        </div>
                                                      </div>
                                                      {team.matchType !==
                                                        'Test' &&
                                                        summaryData
                                                          .matchSummary[inning][
                                                          i <= 1
                                                            ? 'batsmanSummary1'
                                                            : 'batsmanSummary2'
                                                        ].runnerBatsman &&
                                                        summaryData
                                                          .matchSummary[inning][
                                                          i <= 1
                                                            ? 'batsmanSummary1'
                                                            : 'batsmanSummary2'
                                                        ].runnerBatsman
                                                          .battingStatsList && (
                                                          <div className="flex moon-gray items-center justify-between pv1 ">
                                                            {lang === 'HIN' ? (
                                                              <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                                {' '}
                                                                {
                                                                  summaryData
                                                                    .matchSummary[
                                                                    inning
                                                                  ][
                                                                    i <= 1
                                                                      ? 'batsmanSummary1'
                                                                      : 'batsmanSummary2'
                                                                  ]
                                                                    .runnerBatsman
                                                                    .playerNameHindi
                                                                }
                                                              </div>
                                                            ) : (
                                                              <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                                {' '}
                                                                {
                                                                  summaryData
                                                                    .matchSummary[
                                                                    inning
                                                                  ][
                                                                    i <= 1
                                                                      ? 'batsmanSummary1'
                                                                      : 'batsmanSummary2'
                                                                  ]
                                                                    .runnerBatsman
                                                                    .playerName
                                                                }
                                                              </div>
                                                            )}
                                                            <div className="text-white font-semibold">
                                                              {' '}
                                                              {
                                                                summaryData
                                                                  .matchSummary[
                                                                  inning
                                                                ][
                                                                  i <= 1
                                                                    ? 'batsmanSummary1'
                                                                    : 'batsmanSummary2'
                                                                ].runnerBatsman
                                                                  .battingStatsList[0]
                                                                  .runs
                                                              }
                                                              {summaryData
                                                                .matchSummary[
                                                                inning
                                                              ][
                                                                i <= 1
                                                                  ? 'batsmanSummary1'
                                                                  : 'batsmanSummary2'
                                                              ].runnerBatsman
                                                                .battingStatsList[0]
                                                                .isNotOut ===
                                                              true
                                                                ? '*'
                                                                : ''}
                                                            </div>
                                                          </div>
                                                        )}
                                                    </div>
                                                  )}
                                                <div className="bl  b--white-20"></div>

                                                <div className="w-5/12 ph2 center">
                                                  <div className="text-gray-2 uppercase text-xs font-semibold">
                                                    Bowler
                                                  </div>
                                                  {summaryData.matchSummary[
                                                    inning === 'homeTeamData'
                                                      ? 'awayTeamData'
                                                      : 'homeTeamData'
                                                  ][
                                                    i <= 1
                                                      ? 'bowlerSummary1'
                                                      : 'bowlerSummary2'
                                                  ].topBowler &&
                                                    summaryData.matchSummary[
                                                      inning === 'homeTeamData'
                                                        ? 'awayTeamData'
                                                        : 'homeTeamData'
                                                    ][
                                                      i <= 1
                                                        ? 'bowlerSummary1'
                                                        : 'bowlerSummary2'
                                                    ].topBowler
                                                      .bowlingStatsList && (
                                                      <div className="flex moon-gray items-center justify-between pv1 ">
                                                        {lang === 'HIN' ? (
                                                          <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                            {' '}
                                                            {
                                                              summaryData
                                                                .matchSummary[
                                                                inning ===
                                                                'homeTeamData'
                                                                  ? 'awayTeamData'
                                                                  : 'homeTeamData'
                                                              ][
                                                                i <= 1
                                                                  ? 'bowlerSummary1'
                                                                  : 'bowlerSummary2'
                                                              ].topBowler
                                                                .playerNameHindi
                                                            }
                                                          </div>
                                                        ) : (
                                                          <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                            {' '}
                                                            {
                                                              summaryData
                                                                .matchSummary[
                                                                inning ===
                                                                'homeTeamData'
                                                                  ? 'awayTeamData'
                                                                  : 'homeTeamData'
                                                              ][
                                                                i <= 1
                                                                  ? 'bowlerSummary1'
                                                                  : 'bowlerSummary2'
                                                              ].topBowler
                                                                .playerName
                                                            }
                                                          </div>
                                                        )}
                                                        <div className="text-white font-semibold">
                                                          {
                                                            summaryData
                                                              .matchSummary[
                                                              inning ===
                                                              'homeTeamData'
                                                                ? 'awayTeamData'
                                                                : 'homeTeamData'
                                                            ][
                                                              i <= 1
                                                                ? 'bowlerSummary1'
                                                                : 'bowlerSummary2'
                                                            ].topBowler
                                                              .bowlingStatsList[0]
                                                              .wickets
                                                          }
                                                          /
                                                          {
                                                            summaryData
                                                              .matchSummary[
                                                              inning ===
                                                              'homeTeamData'
                                                                ? 'awayTeamData'
                                                                : 'homeTeamData'
                                                            ][
                                                              i <= 1
                                                                ? 'bowlerSummary1'
                                                                : 'bowlerSummary2'
                                                            ].topBowler
                                                              .bowlingStatsList[0]
                                                              .runsConceded
                                                          }
                                                        </div>
                                                      </div>
                                                    )}
                                                  {team.matchType !== 'Test' &&
                                                    summaryData.matchSummary[
                                                      inning === 'homeTeamData'
                                                        ? 'awayTeamData'
                                                        : 'homeTeamData'
                                                    ][
                                                      i <= 1
                                                        ? 'bowlerSummary1'
                                                        : 'bowlerSummary2'
                                                    ].runnerBowler &&
                                                    summaryData.matchSummary[
                                                      inning === 'homeTeamData'
                                                        ? 'awayTeamData'
                                                        : 'homeTeamData'
                                                    ][
                                                      i <= 1
                                                        ? 'bowlerSummary1'
                                                        : 'bowlerSummary2'
                                                    ].runnerBowler
                                                      .bowlingStatsList && (
                                                      <div className="flex moon-gray items-center justify-between pv1  ">
                                                        {lang === 'HIN' ? (
                                                          <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                            {' '}
                                                            {
                                                              summaryData
                                                                .matchSummary[
                                                                inning ===
                                                                'homeTeamData'
                                                                  ? 'awayTeamData'
                                                                  : 'homeTeamData'
                                                              ][
                                                                i <= 1
                                                                  ? 'bowlerSummary1'
                                                                  : 'bowlerSummary2'
                                                              ].runnerBowler
                                                                .playerNameHindi
                                                            }
                                                          </div>
                                                        ) : (
                                                          <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs">
                                                            {' '}
                                                            {
                                                              summaryData
                                                                .matchSummary[
                                                                inning ===
                                                                'homeTeamData'
                                                                  ? 'awayTeamData'
                                                                  : 'homeTeamData'
                                                              ][
                                                                i <= 1
                                                                  ? 'bowlerSummary1'
                                                                  : 'bowlerSummary2'
                                                              ].runnerBowler
                                                                .playerName
                                                            }
                                                          </div>
                                                        )}
                                                        <div className="text-white font-semibold">
                                                          {' '}
                                                          {
                                                            summaryData
                                                              .matchSummary[
                                                              inning ===
                                                              'homeTeamData'
                                                                ? 'awayTeamData'
                                                                : 'homeTeamData'
                                                            ][
                                                              i <= 1
                                                                ? 'bowlerSummary1'
                                                                : 'bowlerSummary2'
                                                            ].runnerBowler
                                                              .bowlingStatsList[0]
                                                              .wickets
                                                          }
                                                          /
                                                          {
                                                            summaryData
                                                              .matchSummary[
                                                              inning ===
                                                              'homeTeamData'
                                                                ? 'awayTeamData'
                                                                : 'homeTeamData'
                                                            ][
                                                              i <= 1
                                                                ? 'bowlerSummary1'
                                                                : 'bowlerSummary2'
                                                            ].runnerBowler
                                                              .bowlingStatsList[0]
                                                              .runsConceded
                                                          }
                                                        </div>
                                                      </div>
                                                    )}
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        ))}
                                    </div>
                                  </div>
                                )}
                            </>
                          ),
                        )
                      ) : (
                        <div className="white f5 fw5 pv2  tc ">
                          No match found
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}

            {/* start */}

            {StadiumStatHub.stadiumHub &&
              StadiumStatHub.stadiumHub.playerRecordBatting &&
              StadiumStatHub.stadiumHub.playerRecordBatting.totalRuns &&
              StadiumStatHub.stadiumHub.playerRecordBatting.totalRuns.length >
                0 && (
                <div>
                  {/* <div className='bb b--white-20 mt3 mb2'></div> */}

                  <div className="border  bg-white text-black dark:bg-gray dark:border-none dark:text-white   mt-2 rounded-lg p-2">
                    <div className="flex items-center justify-between ">
                      {/* <div className='text-base ml-3  font-bold  uppercase  py-2 '>{getLangText(lang,words,'PLAYER RECORDS')}</div> */}
                      <div className="px-1 pb-2">
                        <Heading
                          heading={getLangText(lang, words, 'PLAYER RECORDS')} subHeading={isEnglish(lang) ? `*From matches in ${StadiumStatHub.stadiumHub.venueName}` : `*
                          ${StadiumStatHub.stadiumHub.venueNameInHindi} में खेले गए मैचों से`  }
                        />{' '}
                      </div>
                      {(playerRecord.value === 'batting' &&
                        StadiumStatHub.stadiumHub.playerRecordBatting[
                          battingType
                        ].length > 6) ||
                      (playerRecord.value === 'bowling' &&
                        StadiumStatHub.stadiumHub.playerRecordBowling[
                          bowlingType
                        ].length > 6) ? (
                        <div className="p-2 ">
                          {!viewAll && (
                            <div
                              className="cursor-pointer hover:opacity-70 transition ease-in duration-150 w-20 p-1 border border-basered text-red-5 font-semibold text-xs flex justify-center items-center rounded-md dark:bg-gray-8 dark:text-green dark:border-green "
                              onClick={() => {
                                setViewAll(true)
                              }}
                            >
                              <div className=" text-xs  font-semibold">
                                {getLangText(lang, words, 'View All')}
                              </div>
                            </div>
                          )}
                        </div>
                      ) : (
                        <></>
                      )}
                    </div>

                    <div className="flex flex-col items-center mt-1 justify-center ">
                      <div className="w-full flex justify-between ">
                        <div className="md:w-1/3 lg:w-1/3 w-8/12 px-1">
                          <Tab
                            data={playerRecordTab}
                            selectedTab={playerRecord}
                            type="block"
                            handleTabChange={(item) => {
                              setPlayerRecord(item)
                              setViewAll(false)
                            }}
                          />
                        </div>
                        <div className="w-4/12 flex justify-end px-2">
                          {playerRecord.value === 'batting' ? (
                            <div
                              className={`  relative  w-40  `}
                              ref={wrapperRef}
                            >
                              <div
                                className={`  ${
                                  showDropDown ? '' : ''
                                } rounded-lg text-black dark:text-white border dark:border-none flex items-center justify-center flex-col cursor-pointer   dark:bg-gray-4`}
                                onClick={() => {
                                  setShowDropDown((prev) => !prev)
                                }}
                              >
                                <div className="flex w-full  p-1 items-center justify-center ">
                                  <div className="w-8/12  text-center text-xs font-semibold capitalize ml-2 p-1">
                                    {battingType === 'battingAverage'
                                      ? getLangText(lang, words, 'AVERAGE')
                                      : battingType === 'battingStrikeRate'
                                      ? getLangText(lang, words, 'S/R')
                                      : battingType === 'fifties'
                                      ? '50s'
                                      : battingType === 'hundreds'
                                      ? '100s'
                                      : battingType === 'totalFours'
                                      ? '4s'
                                      : battingType === 'totalRuns'
                                      ? getLangText(lang, words, 'RUNS')
                                      : '6s'}{' '}
                                  </div>

                                  <div className="w-4/12  flex items-center justify-center">
                                  <div className='w-4/12  flex items-center justify-center'>
                                          <img className={`w-3 md:hidden lg:hidden   ${showDropDown ? 'rotate-180' : ''}`} src={'/pngsV2/whitearrow_down.png'} alt='dropdown' />
                                          <img className={`w-3 hidden md:block lg:block  ${showDropDown ? 'rotate-180' : ''}`} src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                       </div>
                                  </div>
                                </div>
                              </div>
                              {showDropDown && (
                                <div
                                  className="  border  bg-white text-black dark:text-white  text-xs rounded w-full h-40   absolute   overflow-y-scroll z-20 overflow-hidden dark:bg-gray-4"
                                  style={{ bottom: 0, left: 0, top: 33 }}
                                >
                                  {battingTab.map((tabName, i) => (
                                    <div
                                      key={i}
                                      onClick={() => {
                                        setShowDropDown((prev) => !prev),
                                          setBattingType(tabName)
                                        setViewAll(false)
                                      }}
                                      className="p-2 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30"
                                    >
                                      <div className="capitalize">
                                        {tabName === 'battingAverage' ? (
                                          <div>
                                            {getLangText(
                                              lang,
                                              words,
                                              'AVERAGE',
                                            )}
                                          </div>
                                        ) : tabName === 'battingStrikeRate' ? (
                                          <div>
                                            {getLangText(lang, words, 'S/R')}
                                          </div>
                                        ) : tabName === 'fifties' ? (
                                          <div>50s</div>
                                        ) : tabName === 'hundreds' ? (
                                          <div>100s</div>
                                        ) : tabName === 'totalFours' ? (
                                          <div>4s</div>
                                        ) : tabName === 'totalRuns' ? (
                                          <div>
                                            {getLangText(lang, words, 'RUNS')}
                                          </div>
                                        ) : (
                                          '6s'
                                        )}
                                      </div>
                                    </div>
                                  ))}
                                </div>
                              )}
                            </div>
                          ) : (
                            <div className={` relative  w-40`} ref={wrapperRef}>
                              <div
                                className={`  ${
                                  showDropDown ? 'border-t' : ''
                                } rounded-lg text-black dark:text-white border flex items-center justify-center flex-col cursor-pointer   dark:bg-gray-4`}
                                onClick={() => {
                                  setShowDropDown((prev) => !prev)
                                }}
                              >
                                <div className="flex w-full  p-1 items-center justify-center ">
                                  <div className="w-8/12  text-center text-xs font-semibold capitalize ml-2 p-1">
                                    {bowlingType === 'bowlingStrikeRate'
                                      ? getLangText(lang, words, 'S/R')
                                      : bowlingType === 'economy'
                                      ? getLangText(lang, words, 'ECONOMY')
                                      : bowlingType === 'fiveWickets'
                                      ? '5-W'
                                      : bowlingType === 'threeWickets'
                                      ? '3-W'
                                      : getLangText(
                                          lang,
                                          words,
                                          'wickets',
                                        )}{' '}
                                  </div>

                                  <div className=" text-center w-4/12  flex items-center justify-center">
                                  <img className={`w-3 md:hidden lg:hidden   ${showDropDown ? 'rotate-180' : ''}`} src={'/pngsV2/whitearrow_down.png'} alt='dropdown' />
                                          <img className={`w-3 hidden md:block lg:block  ${showDropDown ? 'rotate-180' : ''}`} src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                  </div>
                                </div>
                              </div>
                              {showDropDown && (
                                <div
                                  className="  border  bg-white text-black dark:text-white   rounded w-full h-40   absolute   overflow-y-scroll z-20 overflow-hidden dark:bg-gray-4"
                                  style={{ bottom: 0, left: 0, top: 33 }}
                                >
                                  {bowlingTab.map((tabName, i) => (
                                    <div
                                      key={i}
                                      onClick={() => {
                                        setShowDropDown((prev) => !prev)
                                        setBowlingType(tabName)
                                        setViewAll(false)
                                      }}
                                      className="p-2 flex items-center justify-start cursor-pointer hover-bg-gay-10 border-b border-gary-2"
                                    >
                                      <div className="capitalize text-xs font-semibold">
                                        {' '}
                                        {tabName === 'bowlingStrikeRate' ? (
                                          <div>
                                            {getLangText(lang, words, 'S/R')}
                                          </div>
                                        ) : tabName === 'economy' ? (
                                          <div>
                                            {getLangText(
                                              lang,
                                              words,
                                              'ECONOMY',
                                            )}
                                          </div>
                                        ) : tabName === 'fiveWickets' ? (
                                          <div>5-W</div>
                                        ) : tabName === 'threeWickets' ? (
                                          <div>3-W</div>
                                        ) : (
                                          getLangText(lang, words, 'wickets')
                                        )}
                                      </div>
                                    </div>
                                  ))}
                                </div>
                              )}
                            </div>
                          )}
                        </div>
                      </div>

                      <div className=" w-full bg-white text-black  dark:bg-gray  dark:border-none dark:text-white  rounded-lg mt-2 flex flex-col items-end ">
                        {(playerRecord.value === 'batting' &&
                          StadiumStatHub.stadiumHub.playerRecordBatting[
                            battingType
                          ].length > 0) ||
                        (playerRecord.value === 'bowling' &&
                          StadiumStatHub.stadiumHub.playerRecordBowling[
                            bowlingType
                          ].length > 0) ? (
                          <div className="w-full">
                            {StadiumStatHub.stadiumHub &&
                              (playerRecord.value === 'batting'
                                ? StadiumStatHub.stadiumHub.playerRecordBatting[
                                    battingType
                                  ]
                                : StadiumStatHub.stadiumHub.playerRecordBowling[
                                    bowlingType
                                  ]
                              ).map((item, index) => {
                                return (
                                  <div
                                    key={index}
                                    className={` ${
                                      !viewAll && index > 5 ? 'hidden' : 'block'
                                    }`}
                                  >
                                    <div className="flex   bg-gray-10 text-black  dark:bg-gray-4  dark:text-white    justify-between p-2  rounded-lg mx-1 my-2">
                                      <div className="flex  w-9/12   justify-start items-center">
                                        <div className="mr-1 md:mx-2 lg:mx-2 flex   items-center  bg-white dark:bg-dark-gray h-12 w-12 rounded-full   justify-center ">
                                          <img
                                            className="h-12 w-12 rounded-full  object-cover object-top "
                                            src={`https://images.cricket.com/players/${item.playerID}_headshot.png`}
                                            onError={(evt) =>
                                              (evt.target.src =
                                                '/pngsV2/playerph.png')
                                            }
                                            alt=""
                                          />
                                        </div>
                                        <div className="flex gap-1 w-8/12  justify-start items-center">
                                          <div className="flex w-6/12 flex-col gap-1  justify-between">
                                            <div className="  text-[0.6rem] md:text-xs lg:text-xs truncate font-semibold">
                                              {isEnglish(lang)
                                                ? item.playerName
                                                : item.playerNameHindi}
                                            </div>
                                            <div className=" flex ">
                                              <div className="border border-green text-xs rounded-md bg-gray-8 text-gray-2   p-1">
                                                {item.shortName}
                                              </div>
                                            </div>
                                          </div>
                                          <div className=" w-4/12 flex justify-between items-center ">
                                            <div className="  flex items-start justify-start  ">
                                              <div className="bg-gray-8 capitalize flex flex-col py-2 rounded-md text-center mr-2 text-xs text-white  captilize p-1">
                                                <div>{item.totalMatches}</div>
                                                <span className='text-[0.6rem]'>{getLangText(
                                                  lang,
                                                  words,
                                                  'matches',
                                                )}</span>
                                              </div>
                                            </div>
                                            {playerRecord.value === 'batting' &&
                                              battingType !== 'totalRuns' && (
                                                <div className="  flex items-center text-xs justify-center">
                                                  <div className=" capitalize bg-gray-8 py-2 flex flex-col items-center text-xs  text-white rounded  p-2">
                                                    <span>
                                                      {item.playerRuns}
                                                    </span>{' '}
                                                    <span>
                                                      {' '}
                                                      <span className='text-[0.6rem]'>{getLangText(
                                                  lang,
                                                  words,
                                                  'RUNS',
                                                )}</span>
                                                    </span>
                                                  </div>
                                                </div>
                                              )}
                                            {playerRecord.value === 'bowling' &&
                                              bowlingType !==
                                                'totalWickets' && (
                                                <div className="  flex items-end justify-end">
                                                  <div className="bg-gray-8 capitalize py-2 flex flex-col  text-xs items-center  text-white rounded  p-2">
                                                    {item.totalWickets}{' '}
                                                    <div>
                                                    <span className='text-[0.6rem]'>{getLangText(
                                                  lang,
                                                  words,
                                                  'WICKETS',
                                                )}</span>
                                                    </div>
                                                  </div>
                                                </div>
                                              )}
                                          </div>
                                        </div>
                                      </div>

                                      <div className=" bg-white dark:bg-gray-8 rounded-md w-16 p-1  text-xs flex flex-col items-center justify-center">
                                        <div className=" capitalize text-gray-2 text-xs">
                                          {playerRecord.value === 'batting'
                                            ? battingType === 'battingAverage'
                                              ? getLangText(
                                                  lang,
                                                  words,
                                                  'AVERAGE',
                                                )
                                              : battingType ===
                                                'battingStrikeRate'
                                              ? getLangText(lang, words, 'S/R')
                                              : battingType === 'fifties'
                                              ? '50s'
                                              : battingType === 'hundreds'
                                              ? '100s'
                                              : battingType === 'totalFours'
                                              ? '4s'
                                              : battingType === 'totalRuns'
                                              ? getLangText(lang, words, 'RUNS')
                                              : '6s'
                                            : bowlingType ===
                                              'bowlingStrikeRate'
                                            ? getLangText(lang, words, 'S/R')
                                            : bowlingType === 'economy'
                                            ? getLangText(
                                                lang,
                                                words,
                                                'ECONOMY',
                                              )
                                            : bowlingType === 'fiveWickets'
                                            ? '5-W'
                                            : bowlingType === 'threeWickets'
                                            ? '3-W'
                                            : getLangText(
                                                lang,
                                                words,
                                                'wickets',
                                              )}
                                        </div>

                                        <div>
                                          {playerRecord.value === 'batting'
                                            ? StadiumStatHub.stadiumHub
                                                .playerRecordBatting[
                                                battingType
                                              ][index][
                                                battingType === 'totalRuns'
                                                  ? 'playerRuns'
                                                  : battingType
                                              ]
                                            : StadiumStatHub.stadiumHub
                                                .playerRecordBowling[
                                                bowlingType
                                              ][index][bowlingType]}{' '}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                )
                              })}
                          </div>
                        ) : (
                          <DataNotFound/>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              )}
          </div>
        ) : (
          
            <DataNotFound />
        
        )}
      </>
    )
}
