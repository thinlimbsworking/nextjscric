import React, { useState, useEffect, useRef } from 'react'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import { format } from 'date-fns';
import { STAT_HUB_TEAM, MATCH_SUMMARY } from '../../api/queries';
import Loading from '../loading';
import DataNotFound from '../commom/datanotfound';
import Heading from '../commom/heading';
import Tab from '../shared/Tab';
import Topperform from '../commom/topPerformerCard';
const yellowDownArrow = '/svgs/yellowDownArrow.svg';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const location = '/svgs/location-icon-color.png';
const rightArrow = '/svgs/yellowRightArrow.svg';
const downArrow = '/svgs/downArrow.svg';
const upArrow = '/svgs/downArrow.svg';

export default function StatHubTeam(props) {
   // console.log('stathubteam props - ',props)
   // const lang = 'ENG'
   const lang = props.language;
   const playerRecordTab = [{ name: getLangText(lang, words, 'BATTING'), value: 'batting' }, { name: getLangText(lang, words, 'BOWLING'), value: 'bowling' }];
   const teamTab = [{ name: getLangText(lang, words, 'OVERALL'), value: 'overAllRecord' }, { name: `${getLangText(lang, words, 'BATTING')} 1st`, value: 'battingFirst' }, { name: `${getLangText(lang, words, 'BATTING')} 2nd`, value: 'battingSecond' }];
   // const recentMatchTabs = [{name:'BATTING', value:'batting'},{name: 'BOWLING', value:'bowling'}];
   const [recentMatchTabs, setRecentMatchTabs] = useState([])
   const [teamCategory, setTeamCategory] = useState(teamTab[0]);
   const [hide, setHide] = useState(false);
   const [recentMatch, setRecentMatch] = useState({ name: 'HEAD TO HEAD', value: 'lastFiveH2H' });
   const [playerRecord, setPlayerRecord] = useState(playerRecordTab[0]);
   const [showDropDown, setShowDropDown] = useState(false);
   const tabs = ['wicket', 'runs', 'Average'];
   const [RecentMatchIndex, setRecentMatchIndex] = useState({});
   const [dropDown, setDropDown] = useState(false);
   const [type, setType] = useState('Average');
   const RecentMatchTab = ['HEAD TO HEAD', 'DC', 'MI'];

   const [battingType, setBattingType] = useState("battingAverage")
   const [bowlingType, setBowlingType] = useState("totalWickets")
   const battingTab = ['totalRuns', 'battingAverage', 'battingStrikeRate', 'fifties', 'hundreds', 'totalFours', 'totalSixes'];
   const bowlingTab = ['totalWickets', 'bowlingStrikeRate', 'economy', 'threeWickets', 'fiveWickets']
   const [viewAll, setViewAll] = useState(false)
   const { loading, error, data: TeamStatHub } = useQuery(STAT_HUB_TEAM, {
      variables: { matchID: props.matchID },
      onCompleted: function (data) {
         setRecentMatchTabs([{ name: 'HEAD TO HEAD', value: 'lastFiveH2H' }, { name: data.teamHub.teamRecords?.homeTeamShortName, value: 'lastFiveHome' }, { name: data.teamHub.teamRecords?.awayTeamShortName, value: 'lastFiveAway' }])
      }
   });


   const [getSummary, { error: scoreError, loading: scoreLoading, data: summaryData }] = useLazyQuery(
      MATCH_SUMMARY
   );

   const wrapperRef = useRef(null);
   useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
         if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setShowDropDown(false);
         }
      }

      document.addEventListener('mousedown', handleClickOutside);
      return () => {
         document.removeEventListener('mousedown', handleClickOutside);
      };
   }, [wrapperRef]);




   const isEnglish = (lang) => {
      if (lang === 'ENG') return true
      else return false
   }

   const handlecssHomeTeam = (team, HomeTeam, awayTeam) => {

      if (team === "lowestScore" || team === "avgWickets") {
         if (awayTeam > HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      } else {
         if (awayTeam < HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      }
   }

   const handlecssAwayTeam = (team, HomeTeam, awayTeam) => {

      if (team === "lowestScore" || team === "avgWickets") {
         if (awayTeam < HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      } else {
         if (awayTeam > HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      }
   }
   const getSummaryData = async (matchID, i) => {
      if (!RecentMatchIndex[i]) {

         await getSummary({ variables: { matchID: matchID, status: "completed" } })
      }
      setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });


   }
   // if (error)
   // return (
   //   <div className='w-100 vh-100 white fw2 f7 gray flex flex-column  min-vh-100 bg-black justify-center items-center'>
   //     <span>Something isn't right!</span>
   //     <span>Please refresh and try again...</span>
   //   </div>
   // );
   if (loading) return <Loading />;
   else

      return (
         <>
            {TeamStatHub && TeamStatHub.teamHub ? <div className='w-full '>
               <div className=' mx-3 lg:mx-0 md:mx-0 '>

                  {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords ?
                     <div className="my-4 pb-4 w-full  text-black dark:text-white bg-white  dark:bg-gray p-2 rounded-lg  md:border lg:border " >
                        <div className=' md:px-16 lg:px-16 w-full ' >
                           <Tab type='block' data={teamTab} selectedTab={teamCategory} handleTabChange={(item) => setTeamCategory(item)} />
                        </div>

                        {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div>
                           <div className='dark:bg-gray-4 dark:text-white mt-2 rounded-lg white    py-2 flex  items-center justify-between'>
                              <div className='flex gap-1 justify-center items-center w-4/12'>
                                 <img
                                    className='flex items-center justify-center w-10 h-6 '
                                    src={`https://images.cricket.com/teams/${TeamStatHub.teamHub.teamRecords?.homeTeamID}_flag_safari.png`}
                                    alt=''
                                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                 />
                                 <div className=' text-base font-bold '>{TeamStatHub.teamHub.teamRecords?.homeTeamShortName}</div>
                              </div>
                              <div className="w-4/12 flex items-center justify-center">
                                 <div
                                    className='border border-basered dark:border-green bg-white text-basered w-6/12 md:w-4/12 lg:w-4/12  p-1 text-sm flex dark:text-green dark:bg-gray-8 font-semibold  items-center justify-center rounded-full'
                                 >
                                    {props.matchType}
                                 </div>
                      
                              </div>

                              <div className='flex items-center gap-1  w-4/12 justify-center'>
                                 <div className=' text-base font-bold'>{TeamStatHub.teamHub.teamRecords?.awayTeamShortName}</div>
                                 <img
                                    className=' flex items-center justify-center  w-10 h-6  '
                                    src={`https://images.cricket.com/teams/${TeamStatHub.teamHub.teamRecords?.awayTeamID}_flag_safari.png`}
                                    alt=''
                                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                 />
                              </div>
                           </div>
                           <div className="oswald mt-3 lh-copy uppercase">
                              {(TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'] !== null) &&
                                 ["wins", "avgTeamRuns", "highestScore", "lowestScore", "avgWickets"].map((team, i) => <div key={i} >

                                    <div className='flex  items-center justify-center pv1  f4 '>
                                       <div className={`w-4/12 flex ${handlecssHomeTeam(team, TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value] && TeamStatHub.teamHub.teamRecords[teamCategory.value]['homeData'] !== null &&
                                          TeamStatHub.teamHub.teamRecords[teamCategory.value]['homeData'][team], TeamStatHub &&
                                          TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value] && TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'] !== null && TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'][team])} items-center justify-center  f3 oswald fw6`}>{TeamStatHub


                                             && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value] && TeamStatHub.teamHub.teamRecords[teamCategory.value]['homeData'] !== null && TeamStatHub.teamHub.teamRecords[teamCategory.value]['homeData'][team]}




                                       </div>
                                       <div className='w-4/12  border-x dark:border-black flex items-center justify-center  p-1 m-1 '>
                                          {team === "wins" ? <div className="oswald text-center text-base text-gray-2 ">{getLangText(lang, words, 'WINS')}</div>
                                             : team === "avgTeamRuns" ? <div className="oswald text-center text-base text-gray-2">{getLangText(lang, words, 'AVERAGE')}
                                                <br />{getLangText(lang, words, 'Score')} </div> : team === "avgWickets" ?
                                                <div className="oswald text-center text-base text-gray-2">{getLangText(lang, words, 'AVG.WICKETS')}
                                                   <br />{getLangText(lang, words, 'LOST/INN')}</div> :
                                                team === "highestScore" ?
                                                   <div className="oswald    text-base  text-center text-gray-2">
                                                      {getLangText(lang, words, 'HIGHEST')} <br />{getLangText(lang, words, 'Score')} </div> : team === "lowestScore" ? <div className="oswald text-center text-base text-gray-2">{getLangText(lang, words, 'LOWEST')}<br />{getLangText(lang, words, 'Score')} </div> : team === "teamRuns" ? <div className="oswald f5  f4-l fw6  tc text-gray-2">{getLangText(lang, words, 'RUNS')}</div> : <div className="oswald f5 f4-l  fw6  tc text-gray-2">{getLangText(lang, words, 'TOTAL')} <br />{getLangText(lang, words, 'MATCHES')}</div>}
                                       </div>
                                       <div className={`w-4/12  flex items-center justify-center ${handlecssAwayTeam(team,
                                          TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value] && TeamStatHub.teamHub.teamRecords[teamCategory.value]['homeData'] !== null
                                          && TeamStatHub.teamHub.teamRecords[teamCategory.value]['homeData'][team],

                                          TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'] !== null &&
                                          TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'][team])} f3 oswald fw6`}>


                                          {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'] !== null && TeamStatHub.teamHub.teamRecords[teamCategory.value]['awayData'][team]}</div>

                                    </div>

                                 </div>)
                              }
                           </div>
                           {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div className="text-gray-2 mt-2 text-xs">
                              {lang === 'ENG' ? <span>{`* ${TeamStatHub.teamHub.compType} data from last 3 years`}</span>
                                 : <span>{`* पिछले 3 वर्षों के  ${TeamStatHub.teamHub.compType} डेटा`}</span>}
                           </div>}
                        </div>}
                     </div> : <DataNotFound />}





                  {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div>
                     <div className='bg-white border  text-black dark:text-white dark:border-none  dark:bg-gray p-2  rounded-lg w-full '>
                        <div className='w-full px-1'>
                           <Heading heading={getLangText(lang, words, 'RECENT MATCHES')} />
                        </div>

                        {recentMatchTabs.length && <div className='w-full py-2 md:w-1/3 lg:w-1/3 mt-1'>
                           <Tab data={recentMatchTabs} selectedTab={recentMatch} type='block' handleTabChange={(item) => {
                              setRecentMatch(item); if (recentMatch.value !== item.value) {
                                 setRecentMatchIndex({})
                              }
                           }} />
                        </div>}

                        <>
                           <div className='  p-1 pb-2 '>
                              {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub[recentMatch.value] && TeamStatHub.teamHub[recentMatch.value].length > 0 ? TeamStatHub.teamHub[recentMatch.value].map((team, i) => (
                                 <>

                                    <div
                                       className={'flex  cursor-pointer  bg-gray-10  items-center py-2  justify-between dark:bg-gray-8 mt-1 w-full rounded-lg px-1'}
                                       key={i}
                                       onClick={() => {
                                          getSummaryData(team.matchID, i)
                                          // set.valueIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });
                                       }}
                                    >
                                       <div className='flex items-center w-11/12 '>
                                          <div className=' w-7/12 gap-1 lg:px-8 md:px-8 flex md:gap-4 lg:gap-2 items-center '>
                                             <img
                                                className='h-4 w-6 object-cover'
                                                src={team.winnerTeam === team.awayTeam ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png` :
                                                   team.winnerTeam === team.homeTeam ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png` : flagPlaceHolder
                                                }
                                                alt=''
                                                onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                                             />
                                             <span className='text-sm'>Vs</span>
                                             <img
                                                className='h-4 w-6 object-cover'
                                                src={team.winnerTeam === team.awayTeam ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png` :
                                                   team.winnerTeam === team.homeTeam ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png` : flagPlaceHolder
                                                }
                                                alt=''
                                                onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                                             />
                                             <div className='md:px-4 lg:px-4 px-2 flex items-center'>
                                                <div className='text-xs text-black dark:text-white tracked '>{lang === 'ENG' ? team.matchResult : team.matchResultHindi}</div>
                                             </div>

                                          </div>
                                          <div className='w-2/12'>
                                             <div className="tracked text-gray-2 text-xs">{format(+team.matchDate, 'MMM dd, yyyy. ')}</div>
                                          </div>
                                          <div className=' w-2/12'>
                                             {/* <div className='flex items-center pv1'>
                                             <div className={`ba  pv1 border ${team.winnerTeam === team.homeTeam ? ' border-green text-gray-2 ' : 'border-gray-2 white'}  br2   ph2  f6 fw5 text-xs p-1`}>{team.homeTeam}</div>
                                             <div className='ph2 moon-gray mx-1'> vs</div>
                                             <div className={`ba  br2 border  pt-2 ${team.winnerTeam === team.awayTeam ? 'border-green text-gray-2 ' : 'border-gray-2 white'} pv1    ph2 f6 fw5 text-xs p-1`}>{team.awayTeam}</div>
                                          </div> */}

                                             <div className='flex items-center  '>
                                                {/* <div className="tracked text-gray-2 text-xs">{format(+team.matchDate, 'MMM dd, yyyy. ')}</div> */}
                                                <img className='w-4 px-1 ' src={location}></img>
                                                <div className='text-gray-2 font-semiBold text-xs truncate'>{isEnglish(lang) ? team.venue : team.venueHindi}</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div
                                          className=' w-1/12 flex items-center justify-end px-2'
                                       >

                                          <img className={`w-4 h-4 ${RecentMatchIndex[i] ? 'rotate-90' : ''}`} src={'/svgs/red-right.svg'} />
                                       </div>
                                    </div>
                                    {RecentMatchIndex[i] && summaryData && summaryData.matchSummary && (
                                       <div className=' my-2 p-1  dark:border border-gray-8 text-black dark:text-white white bg-white dark:bg-gray-8'>
                                          <Topperform data={summaryData} matchType={props.matchType} lang={lang}  />

                                       </div>
                                    )}

                                 </>
                              )) : <div className="white f5 fw5 pv2  tc ">No match found</div>}

                           </div>
                        </>
                     </div>
                  </div>}

               </div>
               {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.playerRecordBatting && TeamStatHub.teamHub.playerRecordBatting.totalRuns && TeamStatHub.teamHub.playerRecordBatting.totalRuns.length > 0 && <div>
                  {/* <div className='bb b--white-20 mt3 mb2'></div> */}


                  <div className='border  bg-white text-black dark:bg-gray dark:border-none dark:text-white mx-3 md:mx-0 lg:mx-0 mt-3 rounded-lg p-2 '>
                     <div className='flex items-center justify-between'>
                        {/* <div className='text-base ml-3  font-bold  uppercase  py-2 '>{getLangText(lang,words,'PLAYER RECORDS')}</div> */}

                        <div className='px-1  pb-2'><Heading heading={getLangText(lang, words, 'PLAYER RECORDS')} subHeading={`${isEnglish(lang) ? `*From matches involving ${TeamStatHub?.teamHub?.teamRecords?.homeTeamShortName} and ${TeamStatHub?.teamHub?.teamRecords?.awayTeamShortName}` : `सभी ${TeamStatHub?.teamHub?.teamRecords?.homeTeamShortName} vs ${TeamStatHub?.teamHub?.teamRecords?.awayTeamShortName} मैचों से`}`}/></div>
                        {(playerRecord.value === 'batting' && TeamStatHub.teamHub.playerRecordBatting[battingType].length > 6) || (playerRecord.value === 'bowling' &&
                           TeamStatHub.teamHub.playerRecordBowling[bowlingType].length > 6) ?
                           <div className='p-2 '>
                              {!viewAll && <div className='cursor-pointer hover:opacity-70 transition ease-in duration-150 w-20 p-1 border border-basered text-red-5 font-semibold text-xs flex justify-center items-center rounded-md dark:bg-gray-8 dark:text-green dark:border-green ' onClick={() => { setViewAll(true) }}>
                                 <div className=' text-xs  font-semibold'>{getLangText(lang, words, 'View All')}</div>

                              </div>}

                           </div> : <></>}
                     </div>





                     <div className='flex flex-col items-center mt-1 justify-center '>
                        <div className='w-full flex justify-between '>
                           <div className=' w-2/3 md:w-1/3 lg:w-1/3 px-1 '>
                              <Tab data={playerRecordTab} selectedTab={playerRecord} type='block' handleTabChange={(item) => { setPlayerRecord(item); setViewAll(false) }} />
                           </div>
                           <div className='w-4/12 flex justify-end px-2'>
                              {playerRecord.value === "batting" ? <div className={`  relative  w-40  `} ref={wrapperRef}>
                                 <div
                                    className={`  ${showDropDown ? '' : ''} rounded-lg text-black dark:text-white border dark:border-none flex items-center justify-center flex-col cursor-pointer   dark:bg-gray-4`}
                                    onClick={() => {
                                       setShowDropDown((prev) => !prev);
                                    }}>
                                    <div className='flex w-full  p-1 items-center justify-center '>
                                       <div className='w-8/12  text-center text-xs font-semibold capitalize ml-2 p-1'>{battingType === "battingAverage" ? getLangText(lang, words, 'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang, words, 'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang, words, 'RUNS') : "6s"} </div>

                                       <div className='w-4/12  flex items-center justify-center'>
                                          
                                          <img className={`w-3 md:hidden lg:hidden   ${showDropDown ? 'rotate-180' : ''}`} src={'/pngsV2/whitearrow_down.png'} alt='dropdown' />
                                          <img className={`w-3 hidden md:block lg:block  ${showDropDown ? 'rotate-180' : ''}`} src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                       </div>
                                    </div>
                                 </div>
                                 {showDropDown && (
                                    <div
                                       className='  border text-xs bg-white text-black dark:text-white   rounded w-full h-40   absolute   overflow-y-scroll z-20 overflow-hidden dark:bg-gray-4' style={{ bottom: 0, left: 0, top: 33 }}>
                                       {battingTab.map((tabName, i) => (
                                          <div
                                             key={i}
                                             onClick={() => {
                                                setShowDropDown((prev) => !prev), setBattingType(tabName); setViewAll(false)
                                             }}
                                             className='p-2 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30'>
                                             <div className='capitalize'>{tabName === "battingAverage" ? <div>{getLangText(lang, words, 'AVERAGE')}</div> : tabName === "battingStrikeRate" ? <div>{getLangText(lang, words, 'S/R')}</div> : tabName === "fifties" ? <div>50s</div> : tabName === "hundreds" ? <div>100s</div> : tabName === "totalFours" ? <div>4s</div> : tabName === "totalRuns" ? <div>{getLangText(lang, words, 'RUNS')}</div> : "6s"}</div>
                                          </div>
                                       ))}
                                    </div>
                                 )}
                              </div> : <div className={` relative  w-40`} ref={wrapperRef}>
                                 <div
                                    className={`  ${showDropDown ? 'border-t' : ''} rounded-lg text-black dark:text-white border flex items-center justify-center flex-col cursor-pointer   dark:bg-gray-4`}
                                    onClick={() => {
                                       setShowDropDown((prev) => !prev);
                                    }}>
                                    <div className='flex w-full  p-1 items-center justify-center '>
                                       <div className='w-8/12  text-center text-xs font-semibold capitalize ml-2 p-1'>{bowlingType === "bowlingStrikeRate" ? getLangText(lang, words, 'S/R') : bowlingType === "economy" ? getLangText(lang, words, 'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" : getLangText(lang, words, 'wickets')} </div>

                                       <div className=' text-center w-4/12  flex items-center justify-center'>
                                       <img className={`w-3 md:hidden lg:hidden   ${showDropDown ? 'rotate-180' : ''}`} src={'/pngsV2/whitearrow_down.png'} alt='dropdown' />
                                          <img className={`w-3  hidden md:block lg:block ${showDropDown ? 'rotate-180' : ''}`} src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                       </div>
                                    </div>
                                 </div>
                                 {showDropDown && (
                                    <div
                                       className='  border  bg-white text-black dark:text-white  text-xs rounded w-full h-40   absolute   overflow-y-scroll z-20 overflow-hidden dark:bg-gray-4' style={{ bottom: 0, left: 0, top: 33 }}>
                                       {bowlingTab.map((tabName, i) => (
                                          <div
                                             key={i}
                                             onClick={() => {
                                                setShowDropDown((prev) => !prev); setBowlingType(tabName); setViewAll(false)
                                             }}
                                             className='p-2 flex items-center justify-start cursor-pointer hover-bg-gay-10 border-b border-gary-2'>
                                             <div className='capitalize text-xs '> {tabName === "bowlingStrikeRate" ? <div>{getLangText(lang, words, 'S/R')}</div> : tabName === "economy" ? <div>{getLangText(lang, words, 'ECONOMY')}</div> : tabName === "fiveWickets" ? <div>5-W</div> : tabName === "threeWickets" ? <div>3-W</div> : getLangText(lang, words, 'wickets')}</div>
                                          </div>
                                       ))}
                                    </div>
                                 )}
                              </div>}
                           </div>

                        </div>



                        <div className=' w-full  bg-white text-black  dark:bg-gray  dark:border-none dark:text-white  rounded-lg mt-2 flex flex-col items-end ' >

                           {(playerRecord.value === 'batting' && TeamStatHub.teamHub.playerRecordBatting[battingType].length > 0) || (playerRecord.value === 'bowling' && TeamStatHub.teamHub.playerRecordBowling[bowlingType].length > 0) ?
                              <div className='w-full'>
                                 {TeamStatHub && TeamStatHub.teamHub && (playerRecord.value === 'batting' ? TeamStatHub.teamHub.playerRecordBatting[battingType] : TeamStatHub.teamHub.playerRecordBowling[bowlingType]).map((item, index) => {
                                    return (
                                       <div key={index} className={` ${!viewAll && index > 5 ? 'hidden' : 'block'}`}>
                                          <div className='flex   bg-gray-10 text-black  dark:bg-gray-4  dark:text-white    justify-between   rounded-lg p-2 mx-1 my-2'>
                                             <div className='flex  w-9/12   justify-start items-center'>
                                                <div className='mr-1 md:mx-2 lg:mx-2 flex   items-center  bg-white dark:bg-dark-gray h-12 w-12 rounded-full   justify-center '>
                                                   <img className='h-12 w-12 rounded-full  object-cover object-top ' src={`https://images.cricket.com/players/${item.playerID}_headshot.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} alt='' />
                                                </div>
                                                <div className='flex gap-1 w-8/12  justify-start items-center'>

                                                   <div className='flex w-6/12 flex-col gap-1  justify-between'>
                                                      <div className='  text-[0.6rem] md:text-xs lg:text-xs truncate font-semibold'>{isEnglish(lang) ? item.playerName : item.playerNameHindi}</div>
                                                      <div className=" flex ">
                                                         <div className='border border-green text-xs rounded-md bg-gray-8 text-gray-2   p-1' >{item.shortName}</div>
                                                      </div>
                                                   </div>
                                                   <div className=' w-4/12 flex justify-between items-center '>

                                                      <div className="  flex  items-start justify-start  ">
                                                         <div className='bg-gray-8  py-2 rounded-md text-center mr-2 text-xs text-white flex flex-col captilize p-1 capitalize' >{item.totalMatches} <div className='text-[0.6rem]'>{getLangText(lang, words, 'matches')}</div></div>

                                                      </div>
                                                      {playerRecord.value === 'batting' && battingType !== "totalRuns" &&
                                                         <div className="  flex items-center text-xs justify-center">
                                                            <div className='  bg-gray-8 py-2 flex flex-col  items-center  text-white rounded  p-2 capitalize'><span>{item.playerRuns}</span> <span className=' text-[0.6rem]'> {getLangText(lang, words, 'RUNS')}</span></div></div>}
                                                      {playerRecord.value === 'bowling' && bowlingType !== "totalWickets" && <div className="  flex items-end justify-end">
                                                         <div className='bg-gray-8 py-2 flex flex-col  text-xs items-center  text-white rounded  p-2 capitalize'>{item.totalWickets} <div className=' text-[0.6rem]'>{getLangText(lang, words, 'WICKETS')}</div></div></div>}
                                                   </div>
                                                </div>
                                             </div>

                                             <div className=' bg-white dark:bg-gray-8 rounded-md  p-1 w-16 text-xs flex flex-col items-center justify-center'>

                                                <div className=" uppercase text-gray-2 text-xs">{playerRecord.value === 'batting' ? (battingType === "battingAverage" ? getLangText(lang, words, 'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang, words, 'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang, words, 'RUNS') : "6s") : (bowlingType === "bowlingStrikeRate" ? getLangText(lang, words, 'S/R') :
                                                   bowlingType === "economy" ? getLangText(lang, words, 'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" : getLangText(lang, words, 'wickets'))}</div>

                                                <div className='text-green'>{playerRecord.value === 'batting' ? TeamStatHub.teamHub.playerRecordBatting[battingType][index][battingType === "totalRuns" ? "playerRuns" : battingType] : TeamStatHub.teamHub.playerRecordBowling[bowlingType][index][bowlingType]} </div>
                                             </div>
                                          </div>
                                       </div >
                                    );
                                 })}
                                 {/* <div className='text-xs'>{`${isEnglish(lang) ? `*From matches involving ${TeamStatHub?.teamHub?.teamRecords?.homeTeamShortName} and ${TeamStatHub?.teamHub?.teamRecords?.awayTeamShortName}` : `सभी ${TeamStatHub?.teamHub?.teamRecords?.homeTeamShortName} vs ${TeamStatHub?.teamHub?.teamRecords?.awayTeamShortName} मैचों से`}`}</div> */}
                              </div> : <DataNotFound/>
                           }


                        </div>

                     </div>

                  </div>
               </div>}

            </div> : 
               <DataNotFound />
            }
         </>

      );
}

// WebkitLineClamp: 2