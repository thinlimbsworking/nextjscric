import React, { useEffect, useState } from 'react'
import { HOME_PAGE_GET_ALGO11 } from '../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
const arrow = '/pngsV2/fantasy_Icon.png'
import { useRouter } from 'next/navigation'
import TeamSlider from '../commom/teamslider'
import Heading from '../commom/heading'
import ImageWithFallback from '../commom/Image'
import SlideComp from '../commom/slideComp'

export default function BuildYourTeam({
  algoteam,
  getLangText,
  lang,
  algo11,
  words,
  ...props
}) {
  // console.log('algoteam in buildteam - ', algoteam)
  const handleNav = e => {
    router.push(props.route.href)
  }
  const router = useRouter()
  const navigate = router.push
  const { loading, error, data } = useQuery(HOME_PAGE_GET_ALGO11, {
    variables: { matchID: props.matchID },
  })
  console.log('data in buildteam - ', data)
  const [myTeam, setMyTeam] = useState(algoteam)
  useEffect(() => {
    if (!algoteam) return
    let temp = algoteam?.filter((ele) => ele.captain === '1')
    temp.push(algoteam?.filter((ele) => ele.vice_captain === '1')?.[0])
    temp = [
      ...temp,
      ...algoteam?.filter(
        (ele) => ele.vice_captain === '0' && ele.captain === '0',
      ),
    ]
    setMyTeam(temp)
  }, [algoteam])
  // console.log("matchIDmatchIDmatchIDmatchIDmatchIDmatchID====>>>",data&&data.homePageGetAlgo11)
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    let sec = Math.floor((distance % (1000 * 60)) / 1000)

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
        minutes +
        (minutes > 0 ? ' mins ' : ' min ') +
        (lang === 'HIN' ? 'पहले' : 'ago')
        : (sec > 0 ? sec + ' secs' : ' sec') + (lang === 'HIN' ? 'पहले' : 'ago')
    return result
  }

  return algoteam && algoteam.length > 0 ? (
    <div className=" border-gray-2 md:ml-12  lg:ml-12 md:mx-8 lg:mx-8 w-11/12 md:w-full lg:w-full h-80 md:h-full lg:h-full  border-y-3 border-r-3 md:border-none rounded-r-3xl mt-5 md:mt-0 lg:mt-0 relative text-black dark:text-white   py-3">
      <div className="flex justify-between items-center">
        <Heading heading={getLangText(lang, words, 'Fantasy Teams')} />
        {/* <div>
          <div className='font-semibold text-lg'>{getLangText(lang, words, 'Cricket.com teams')}</div>
          <div className='w-14 bg-blue-8 h-1 rounded-full mt-1'></div>
        </div> */}
        {/* <img className='w-10' src={arrow} /> */}
      </div>
      <div className="pt-2">
        {/* <p className='font-semibold text-sm'>{getLangText(lang, words, 'Cricket.com teams')} </p> */}
        <p className="text-sm font-light pt-1">
          {getLangText(
            lang,
            words,
            'A few of our best and curated suggestions',
          )}
        </p>
      </div>
      {/* {console.log("ddede",data.homePageGetAlgo11)} */}
      <p className="pt-2 text-xs text-gray-2 font-light">
        {`*${getLangText(lang, words, 'Last Updated')} ${getUpdatedTime(
          algo11.getAlgo11.timestamp,
        )}`}
      </p>
      <div
        className="absolute -right-8 md:right-0 top-2 md:top-0"
      >
        {props.comp}
      </div>
      <div className="absolute  w-[90vw] md:w-full lg:w-full shadow-md my-8 md:my-3 lg:my-3 rounded-md">
        <div onClick={handleNav} className="relative cursor-pointer">
          <div className="flex justify-center items-center p-1 bg-gray-4 rounded-t-lg">
            <p className="text-white text-xs font-medium uppercase">
              {getLangText(
                lang,
                words,
                data?.homePageGetAlgo11.teamtotalpoints
                  ? 'Fantasy points'
                  : 'Projected_Points',
              )}{' '}
              :
            </p>
            <p className="text-green pl-1 text-sm font-medium">
              {' '}
              {data?.homePageGetAlgo11.teamtotalpoints ||
                data?.homePageGetAlgo11.totalProjectedPoints}
            </p>
          </div>
        </div>
        <div>
          <SlideComp algoteam={myTeam} lang={lang} from="suggestTeam" handleNav={handleNav} />
        </div>
      </div>
    </div>
  ) : (
    <></>
  )
}
