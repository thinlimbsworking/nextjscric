'use client'
import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { HEADER } from '../constant/Links'
import CleverTap from 'clevertap-react'

import Image from 'next/image'

import { usePathname, useRouter } from 'next/navigation'
const logo_desktop = '/cricket.com.svg'
const moreIcon = '/svgs/icons/more.svg'
const a23_mobile = '/pngs/a23_mweb.jpg'
const frc = '/svgs/fanhome.svg'
const stathub = '/svgs/stathub.svg'

// SCDULEHOME.svg
// CRICHOME.svg
// FANTSAYHOME.svg
const criclyticsIcon = '/svgs/crichome.svg'
const scheduleIcon = '/svgs/schedulehome.svg'
const seriesIcon = '/svgs/icons/series-icon.svg'
const newsAndArticlesIcon = '/svgs/icons/news-and-articles-icon.svg'
const teamsIcon = '/svgs/icons/teams-icon.svg'
const videoIcon = '/svgs/icons/video-icon.svg'
const stadiumIcon = '/svgs/icons/stadium-icon.svg'
const rankingsIcon = '/svgs/icons/rankings-icon.svg'
const recordsIcon = '/svgs/icons/records-icon.svg'
const playersIcon = '/svgs/icons/players-icon.svg'
const cricket = '/svgs/images/icon-128x128.png'
// const season_fantasy = '/svgs/fantasynav.svg';
const playtheodds = '/svgs/playtheod.svg'
const profileIcon = '/svgs/icons/profile.svg'
// const arhiveIcon = '/svgs/GroupArchive.svg'
export default function Header({ params }) {
  let router = useRouter()
  let pathname = usePathname()
  let to = null
  let path = pathname?.split('/')[1]
  let predefineSchedulePaths = ['match-score', 'live-score']
  if (predefineSchedulePaths.includes(path)) {
    path = 'schedule'
  }
  const [dropDown, setDropDown] = useState(false)
  const [showlogin, setShowLogin] = useState(false)
  const [activePath, setActivePath] = useState(path)
  const [modalOpen, setModalOpen] = useState(false)

  const calls =
    (async () => {
      const partnershipData = await axios.post(
        'https://apiv2.cricket.com/cricket',
        {
          query: SEASON_FANTASY_LOGIN_AXIOS,
          variables: { token: global.window.localStorage.getItem('tokenData') },
        },
      )

      if (partnershipData.data.data.sessionFantasyToken.code == 200) {
        setStatusCode(200)

        setUserID(partnershipData.data.data.sessionFantasyToken.message)

        window.location.href = `https://seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`
      }
    },
    [])

  useEffect(() => {
    const appDownloaded = global.window.localStorage.getItem('appDownloaded')
    const laterTime = global.window.localStorage.getItem('laterTime')
    const webviewcheck = global.window.location.pathname
    if (
      !appDownloaded &&
      !webviewcheck.includes('a23-fantasy') &&
      !webviewcheck.includes('stat-attack') &&
      !webviewcheck.includes('appiplteams') &&
      !webviewcheck.includes('who-was-the-winner')
    ) {
      if (!laterTime) {
        to = setTimeout(() => {
          setModalOpen(true)
        }, 30000)
      } else {
        let diff = new Date().getTime() - new Date(+laterTime).getTime()
        const hours = Math.floor(diff / 1000 / 60 / 60)
        diff -= hours * 1000 * 60 * 60
        const minutes = Math.floor(diff / 1000 / 60)
        if (minutes > 72 * 60) {
          to = setTimeout(() => {
            setModalOpen(true)
          }, 30000)
        }
      }
    }
  }, [])

  const handleRedirect = () => {
    CleverTap.initialize('DownloadApp', {
      Source: 'Pop-up',
      Platform: global.window.localStorage.Platform,
    })

    if (typeof window !== 'undefined') {
      setModalOpen(false)
      global.window.localStorage.setItem('appDownloaded', 'true')
      if (window && window.navigator.userAgent.indexOf('Android') > -1) {
        window.open('https://cricketapp.app.link/pBXhcKWNf4', '_blank')
      } else {
        window.open('https://cricketapp.app.link/pBXhcKWNf4', '_blank')
      }
    }
  }
  const handleMaybeLater = () => {
    CleverTap.initialize('DownloadLater', {
      Source: 'Pop-up',
      Platform: localStorage.Platform,
    })
    const nw = new Date().getTime()
    setModalOpen(false)
    localStorage.setItem('laterTime', nw)
  }

  var baseDomain = '.cricket.com'
  var expireAfter = new Date()

  //setting up  cookie expire date after a week
  expireAfter.setDate(expireAfter.getDate() + 100000)

  //now setup cookie

  const handleCleverTap = (event) => {
    CleverTap.initialize(event, {
      Source: 'WebNav',
      Platform: localStorage.Platform,
    })
  }
  const handleNavigation = (href, as) => {
    router.push(href, as)
  }

  const siteNavOptions = [
    {
      id: 'schedule',
      path: '/schedule/live-matches',
      prefetch: '/schedule/[...slugs]',
      title: 'Schedule',
      icon: scheduleIcon,
      event: 'Schedule',
    },

    {
      id: 'criclytics',
      path: '/criclytics',
      prefetch: '/criclytics',
      title: 'Criclytics',
      icon: criclyticsIcon,
      event: 'CriclyticsHome',
    },

    {
      id: 'fantasy-research-center',
      path: '/fantasy-research-center',
      prefetch: '/fantasy-research-center',
      title: 'Fantasy ',
      icon: frc,
      event: 'FantasyCentre',
    },

    {
      id: 'series',
      path: '/series/ongoing',
      prefetch: '/series/[slug]',
      title: 'Series',
      icon: seriesIcon,
      event: 'SeriesHome',
    },
    {
      id: 'news',
      path: '/news/latest',
      prefetch: '/news/[slug]',
      title: 'News',
      icon: newsAndArticlesIcon,
      event: 'NewsHome',
    },

    {
      id: 'videos',
      path: '/videos/latest',
      icon: videoIcon,
      prefetch: '/videos/[...slugs]',
      title: 'Videos',
      event: 'VideosHome',
    },
    {
      id: 'players',
      path: '/players/all',
      prefetch: '/players/[slug]',
      title: 'Stats Hub',
      icon: stathub,
      event: 'PlayerDiscovery',
    },
    {
      id: 'profile',
      path: '/login',
      prefetch: '/login',
      title: 'Profile',
      icon: profileIcon,
      event: 'Login',
    },

    // {
    //   id: 'teams',
    //   path: '/teams/international',
    //   prefetch: '/teams/[slug]',
    //   title: 'Teams',
    //   icon: teamsIcon,
    //   event: 'TeamDiscovery'
    // },
  ]
  const moreList = [
    {
      id: 'stadiums',
      prefetch: '/stadiums',
      path: '/stadiums',
      title: 'Stadiums',
      icon: stadiumIcon,
      event: 'StadiumsDiscovery',
    },
    {
      id: 'rankings',
      path: '/rankings',
      prefetch: '/rankings',
      title: 'Rankings',
      icon: rankingsIcon,
      event: 'Rankings',
    },
    {
      id: 'records',
      path: '/records',
      prefetch: '/records',
      icon: recordsIcon,
      title: 'Records',
      event: 'Records',
    },
    {
      id: 'play-the-odd',
      path: '/play-the-odd',
      prefetch: '/play-the-odd',
      title: 'Play The Odds',
      icon: teamsIcon,
      event: 'PlayTheOdds',
    },
    // {
    //   id: 'season-fantasy',
    //   path: '/season-fantasy',
    //   prefetch: '/season-fantasy',
    //   title: 'Season Fantasy',
    //   icon: season_fantasy,
    //   event: 'SeasonFantasy'
    // },
    {
      id: 'profile',
      path: '/login',
      prefetch: '/login',
      title: 'Profile',
      icon: profileIcon,
      event: 'Login',
    },
  ]

  return (
    <div className="bg-basebg fixed w-full z-50 h-20 hidden md:block lg:block  cursor-pointer items-center justify-center ">
      {/* {false&& (<Login
          onCLose={'/'}
          navTo={'/'}
          sucessPath={'/'}
          entryRoute={'more'}
          modalText='Login/Register to Cricket.com'
          setShowLogin={setShowLogin}
          ShowLoginSucess={false}
        />
      )} */}

      {false && modalOpen && (
        <div
          className="flex dn-ns justify-center items-center  fixed absolute--fill z-max bg-black-20"
          style={{ backdropFilter: 'blur(10px)' }}
        >
          <div className="flex self-end">
            <div className="flex flex-column fw6 items-center relative bg-white ">
              <div
                className="absolute bg-white br-100"
                style={{ padding: '12px 16px', top: -32 }}
              >
                <img style={{ width: 50 }} src={cricket} alt="" />
              </div>
              <h1
                className="grey_10 tc ma0"
                style={{ fontSize: 18, padding: '0 40px', marginTop: 64 }}
              >
                Have you tried the
              </h1>
              <h1
                className="grey_10 tc ma0"
                style={{ fontSize: 18, padding: '0 40px', marginBottom: 8 }}
              >
                <span style={{ color: '#cb462a' }}>cricket</span>
                <span style={{ color: '#ec601d' }}>.</span>
                <span style={{ color: '#f7a100' }}>com</span> App yet?
              </h1>
              <div className="grey_8 fw4 tc ph4 pt2 pb3">
                Experience Fastest Live Scores, Dark Mode and Social updates,
                now on the latest version of the App
              </div>
              <div
                className="white br2 tc"
                style={{
                  margin: '0 32px',
                  padding: '10px 80px',
                  background: 'linear-gradient(to right, #d44030, #9b000d)',
                }}
                onClick={() => handleRedirect()}
              >
                Download Now
              </div>
              <div
                className="tc cdc underline"
                style={{ marginTop: '0 10px' }}
                onClick={() => handleMaybeLater()}
              >
                <div>Maybe Later</div>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="text-white  flex  h-full items-center justify-between  top-0 left-0 right-0  ">
        <div className=" flex justify-between items-center relative  w-full h-full mx-auto max-w-7xl lg:px-24">
          <Link href="/" as="/" passHref className="">
            <img
              alt={logo_desktop}
              className=""
              height="43"
              src={logo_desktop}
              onClick={() => handleNavigation('/', '/')}
            />
          </Link>
          {/* h-100 pv3 ph3 white-80 flex items-center hover-bg-white-10 flex-column justify-center pointer */}
          <div className="w-full flex h-full justify-end">
            {siteNavOptions.map((menu, i) => (
              <>
                {menu.id !== 'playtheodds' ? (
                  <Link
                    href={HEADER[menu.id].href}
                    as={HEADER[menu.id].as}
                    key={i}
                    passHref
                    className="flex items-center"
                    title={menu.title}
                  >
                    <div
                      key={i}
                      onClick={() => handleCleverTap(menu.event)}
                      className={`flex h-full flex-col items-center justify-center w-20 z-60 hover:bg-slate-800 hover:text-green cursor-pointer transition ease-in duration-150 group
                      ${
                        menu.id === path
                          ? 'border-b-green border-b-2 text-green   bg-slate-800 '
                          : ''
                      }`}
                    >
                      <img
                        alt={menu.icon}
                        className=""
                        height="22"
                        width="20"
                        src={menu.icon}
                      />
                      {/* <menu.icon /> */}

                      <div className="text-center  font-normal leading-4 text-bold text-xs pt-1 font-mnr">
                        {menu.title === 'Criclytics' ? (
                          <div>
                            <span>Criclytics</span>
                            <sup>TM</sup>
                          </div>
                        ) : (
                          <span>{menu.title}</span>
                        )}
                        {/* <img src="/svgs/black_records.svg" alt="" /> */}

                        {/* <span className="hidden">s</span> */}
                      </div>
                    </div>
                  </Link>
                ) : (
                  <div
                    title={menu.title}
                    onClick={async () => {
                      if (
                        global.window.localStorage.getItem('tokenData') !== null
                      ) {
                        const partnershipData = await axios.post(
                          'https://apiv2.cricket.com/cricket',
                          {
                            query: SEASON_FANTASY_LOGIN_AXIOS,
                            variables: {
                              token: global.window.localStorage.getItem(
                                'tokenData',
                              ),
                            },
                          },
                        )
                        // Set-Cookie: name=value; domain=mydomain.com
                        if (
                          partnershipData.data.data.sessionFantasyToken.code ==
                          200
                        ) {
                          // var domainName = window.location.hostname;
                          document.cookie =
                            'uniqueUserId=' +
                            partnershipData.data.data.sessionFantasyToken
                              .message +
                            '; domain=' +
                            baseDomain +
                            '; expires=' +
                            expireAfter +
                            '; path=/'
                          // document.cookie="test=ssssss; domain=" + baseDomain + "; expires=" + expireAfter + "; path=/";
                          // document.cookie = `uniqueUserId=${partnershipData.data.data.sessionFantasyToken.message}`;

                          window.location.href = `http://playtheodds.cricket.com/?uniqueUserId=${partnershipData.data.data.sessionFantasyToken.message}`
                        }
                      } else {
                        window.location = '/playtheodds/login'
                      }
                    }}
                  >
                    <div
                      key={i}
                      className={`h-100 pv3 ph3 white-80 flex items-center hover-bg-white-10 flex-col justify-center pointer ${
                        menu.id === path ? 'bg-white-10 bb b--red bw2' : ''
                      }`}
                    >
                      <img
                        alt={menu.icon}
                        className=""
                        height="22"
                        width="20"
                        src={menu.icon}
                      />
                      <div className="ma0  pb0 pt1 tc pv1  nowrap f8 pt-1">
                        Play The Odds
                      </div>
                    </div>
                  </div>
                )}
              </>
            ))}

            <div className="dropdown relative  hidden">
              <div className="flex items-center px-4 h-full flex-col justify-center gap-2 hover-bg-white-10 ">
                <img className="h-5 w-5" src={moreIcon} alt="moreIcon" />
                <div className="flex items-center justify-center gap-1">
                  <div className=" text-xs ">More </div>
                  <img
                    className=" "
                    src={'/svgs/downArrowFilled.svg'}
                    alt="downArrowFilled"
                  />
                </div>
              </div>
              <div
                className=" absolute dropdown-content flex items-center justify-center z-60 bg-gray-4 top-8 left-0"
                style={{ height: 'auto', width: '200px', zIndex: 1000 }}
              >
                {moreList.map((menuA, z) =>
                  menuA.path != '/playtheodds/login' ? (
                    <div
                      key={z}
                      onClick={async () => (
                        handleCleverTap(menuA.event),
                        menuA.path == '/playtheodds/loin'
                          ? null
                          : handleNavigation(menuA.path)
                      )}
                      className={`flex p-2 text-center items-center hover-bg-white-10 justify-start  ${
                        menuA.path === activePath
                          ? 'bg-white-10 bb b--red bw1'
                          : ''
                      }`}
                    >
                      <div>
                        <img
                          alt={menuA.icon}
                          height="22"
                          width="20"
                          src={menuA.icon}
                        />
                      </div>
                      <div className="text-xs ml-2">{menuA.title}</div>
                    </div>
                  ) : (
                    <div
                      key={z}
                      onClick={async () =>
                        // handleCleverTap(menuA.event),
                        menuA.path == '/playtheodds/loin'
                          ? null
                          : handleNavigation(menuA.path)
                      }
                      className={`ph3 pv2 white-80 flex items-center hover-bg-white-10 justify-start pointer ${
                        menuA.path === activePath
                          ? 'bg-white-10 bb b--red bw1'
                          : ''
                      }`}
                    >
                      <div>
                        <img
                          alt={menuA.icon}
                          height="22"
                          width="20"
                          src={menuA.icon}
                        />
                      </div>
                      <div className="ma0 pb0 pt1 nowrap pl2  pb2 f7">
                        {menuA.title}
                      </div>
                    </div>
                  ),
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
