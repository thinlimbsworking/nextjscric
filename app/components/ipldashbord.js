import React, { useState } from 'react';

import { IPL_DASHBOARD_QUERY } from '../api/queries';
import { useQuery } from '@apollo/react-hooks';
import { useRouter } from 'next/navigation';


export default function Ipldashbord(props) {
  const router = useRouter();
  const navigate = router.push;
  const [maxval, setMaxVal] = useState(240);
  let { loading, error, data } = useQuery(IPL_DASHBOARD_QUERY, {
    onCompleted: (data) => {
      data.iplDashboard &&
        data.iplDashboard.completed &&
        data.iplDashboard.completed.length > 0 &&
        setMaxVal(
          Math.max(
            data.iplDashboard.completed[0] && data.iplDashboard.completed[0][0].runsScored,
            data.iplDashboard.completed[1] && data.iplDashboard.completed[1][0].runsScored,
            data.iplDashboard.completed[2] && data.iplDashboard.completed[2][0].runsScored
          )
        );
    }
  });
  if (error) return <div></div>;
  if (loading) return <div></div>;
  else
    return data && data.iplDashboard && data.iplDashboard.completed && data.iplDashboard.completed.length > 0 ? (
      <div className='lp:mt-0  mt-5 text-white px-2 mx-2'>
        <div className='font-semibold text-lg tracking-wider'>IPL Dashboard</div>
        <div
          className='flex items-center justify-between mt-4'
          onClick={() =>
            navigate(
              'series/[...slugs]',
              `series/domestic/${data.iplDashboard.tourID}/${data.iplDashboard.seriesName
                .replace(/[^a-zA-Z0-9]+/g, ' ')
                .split(' ')
                .join('-')
                .toLowerCase()}/matches`
            ).then(() => window.scrollTo(0, 0))
          }>
          <div className='w-full rounded bg-gray-4 p-3 '>
            <div className='text-gray-5 text-sm font-semibold rounded-full border-gray-5 border-2 p-1 flex  uppercase  justify-center items-center w-32 '>
              <span className='w-2 h-2 bg-white rounded-full m-1'></span> COMPLETED
            </div>

            {/* {data &&
              data.iplDashboard &&
              data.iplDashboard.completed.map((item, index) => <div className='w-full flex items-center justify-center my-4'>
            <div className='w-6/12  flex items-center justify-center text-white text-sm'>{item[0].runsScored}/{item[0].wickets}{' '}({item[0].overs || '0'})</div>
            <div className='w-6/12  flex items-center justify-center text-white text-sm'>{item[1].runsScored}</div>
          </div>)} */}

            <div className='flex items-center justify-between'>
              {data &&
                data.iplDashboard &&
                data.iplDashboard.completed &&
                data.iplDashboard.completed.map((item, index) => {
                  return (
                    item.length > 1 && (
                      <div key={index} className=''>
                        <div className='my-4 '>
                          <div className=' text-white text-sm font-semibold'>
                            {item[0].runsScored}/{item[0].wickets} ({item[0].overs || '0'})
                          </div>
                          <div className='flex items-center justify-center text-gray-2 text-xs pt-1 font-medium text-left'>
                            {' '}
                            {item[1].runsScored}/{item[1].wickets} ({item[1].overs || '0'})
                          </div>
                        </div>
                        <div className=''>
                          <div className='flex items-end justify-center ' style={{ height: 260 }}>
                            <div
                              className='flex w-5 rounded-md bg-blue mr-2'
                              style={{
                                height: `${(item[0].runsScored / (maxval + 10)) * 100}%`
                              }}></div>

                            <div
                              className='flex  w-5 rounded-md bg-gray-3 ml-2'
                              style={{
                                height: `${(item[1].runsScored / (maxval + 10)) * 100}%`
                              }}></div>
                          </div>
                          <div className='mt-3 flex items-center justify-center  '>
                            <div className=' h-3.5 w-5 rounded mr-2'>
                              <img
                                src={`https://images.cricket.com/teams/${item[0].teamID}_flag_safari.png`}
                                onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                                alt=''
                              />
                            </div>

                            <div className='ml-2 h-3.5 w-5 rounded'>
                              <img
                                src={`https://images.cricket.com/teams/${item[1].teamID}_flag_safari.png`}
                                onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                                alt=''
                              />
                            </div>
                          </div>
                          <div className='mt-2 text-center font-semibold text-gray-2 '>
                            <div className='text-sm'>
                              {item[0].teamShortName} / {item[1].teamShortName}
                            </div>
                            <div className='mt-1 text-md'>{item[0].matchNumber}</div>
                          </div>
                        </div>
                      </div>
                    )
                  );
                })}
            </div>
          </div>
          {/*  */}

          {/* {data&&data.iplDashboard&& data.iplDashboard.live && data.iplDashboard.live.length>0 && <div className='w-4/12 bg-gray rounded p-3'>
          <div className=' font-semibold text-sm  rounded-full border-red border-2 p-1 flex  uppercase text-red justify-center items-center w-20'>
            <div className='w-2 h-2 bg-red rounded-full m-1  '></div> Live
          </div>
          <div className='text-center my-4'>
            <div className='text-white text-sm font-semibold'>
              {data&&data.iplDashboard&& data.iplDashboard.live && data.iplDashboard.live.length>0 && data.iplDashboard.live[0].runsScored}/{data&&data.iplDashboard&&data.iplDashboard.live&& data.iplDashboard.live[0].wickets} (
               {data&&data.iplDashboard&& data.iplDashboard.live&&data.iplDashboard.live[0].overs})
            </div>
            <div
              className={`${
                data&&data.iplDashboard&&  data.iplDashboard.live[1].runsScored ? 'text-white' : 'text-gray-2'
              } text-xs pt-1 font-medium `}>
               {data&&data.iplDashboard&& data.iplDashboard.live[1].runsScored
                ? data.iplDashboard.live[1].runsScored / data.iplDashboard.live[1].wickets +
                  ' ' +
                  data.iplDashboard.live[1].overs
                : 'Yet To Bat'}
            </div>
          </div>

          <div className=''>
            <div className='flex items-end justify-center ' style={{ height: 260 }}>
              <div className='bg-gray-4 w-6 rounded-md mr-2 flex items-end' style={{ height: 260 }}>
                <div
                  className={`flex w-6 rounded-md ${
                    data&&data.iplDashboard&&  data.iplDashboard.live&&  data.iplDashboard.live[1].runsScored ? 'bg-blue' : 'bg-green'
                  }`}
                  style={{
                    height: `${
                      data&&data.iplDashboard&& data.iplDashboard.live&&  data.iplDashboard.live[1].runsScored
                        ? ( data.iplDashboard.live[0].runsScored /
                            Math.max(
                              data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[0].runsScored,
                              data&&data.iplDashboard&&    data.iplDashboard.live&&     data.iplDashboard.live[1].runsScored
                            )) *
                          100
                        : (parseInt(  data&&data.iplDashboard&& data.iplDashboard.live && data.iplDashboard.live[0].runsScored) / 270) * 100
                    }%`
                  }}></div>
              </div>
              <div className='bg-gray-4 w-6 rounded-md ml-2 flex items-end' style={{ height: 260 }}>
                <div
                  className={`flex w-6 rounded-md ${data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[1].runsScored ? 'bg-green' : ''}`}
                  style={{
                    height: `${
                      (data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[1].runsScored /
                        Math.max(
                          data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[0].runsScored,
                          data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[1].runsScored
                        )) *
                      100
                    }%`
                  }}></div>
              </div>
            </div>
            <div className='mt-3 flex items-center justify-center  '>
              <div className=' h-3.5 w-5 rounded mr-2'>
                <img
                  src={`https://images.cricket.com/teams/${data && data.iplDashboard && data.iplDashboard.live&&  data.iplDashboard.live[0].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                  alt=''
                />
              </div>

              <div className='ml-2 h-3.5 w-5 rounded'>
                <img
                  src={`https://images.cricket.com/teams/${data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[1].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                  alt=''
                />
              </div>
            </div>
            <div className='mt-2 text-center font-semibold text-gray-2'>
              <div className='text-sm'>
                {data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[0].teamShortName} / {data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[1].teamShortName}
              </div>
              <div className='mt-1 text-md'>{data&&data.iplDashboard&&    data.iplDashboard.live&&  data.iplDashboard.live[0].matchNumber}</div>
            </div>
          </div>
        </div>} */}
        </div>
        <div className='rounded p-2 mt-3 flex bg-gray justify-start items-center'>
          <div className='flex  items-center justify-center text-xs text-gray-2 font-medium mr-3 '>
            <div className=' h-2 w-2 bg-blue rounded-full  mx-1'></div> Batted 1st
          </div>
          <div className='flex  items-center  justify-center text-xs text-gray-2 font-medium ml-3'>
            <div className='flex  items-center justify-center h-2 w-2 bg-gray-3 rounded-full  mx-1'></div> Batted 2nd
          </div>
          {/* <div className='flex  items-center justify-center text-xs text-gray-2 font-medium '>
          <div className='flex  items-center justify-center h-2 w-2 bg-green-3 rounded-full mx-1'></div> Batting Now
        </div> */}
        </div>
      </div>
    ) : (
      <div></div>
    );
}
