import React from 'react';
import { HOME_PAGE_POINTS_TABLE_QUEY } from '../api/queries';
import { useQuery } from '@apollo/react-hooks';
import { useRouter } from 'next/navigation';
import Link from 'next/link'
export default function Iplstanding(props) {
  const router = useRouter();
  const navigate = router.push;

  let { loading: loading, error: error, data } = useQuery(HOME_PAGE_POINTS_TABLE_QUEY, {});
  if (error) return <div></div>;
  if (loading) return <div></div>;
  if(data)
  {
// console.log("data1234567899",data)
    return data && data.topFourTeams && data.topFourTeams.teams && data.topFourTeams.teams[0] ? (
      <div className='lp:mt-0 mt-5 text-white px-1 mx-2'>
          <div className=' font-semibold text-base line-clamp-1 mb-1'>{data.topFourTeams.seriesName}</div>
        <div className='flex justify-between items-center'>
          <div className='font-semibold text-base pl-2'>Team Standings</div>
          <Link href={`/series/domestic/${data.topFourTeams.tourID}/${data.topFourTeams.seriesName
                  .replace(/[^a-zA-Z0-9]+/g, ' ')
                  .split(' ')
                  .join('-')
                  .toLowerCase()}/points-table`}>
         
         
         {/* series/domestic/2814/indian-premier-league-2023/stats */}
          <div
            className='rounded border  text-green border-green text-xs p-2 font-semibold bg-gray-8'
         >
            VIEW ALL
          </div>
          </Link>
        </div>
        <div className='flex flex-col bg-gray rounded-lg  mt-3'>
          <div className='flex justify-between bg-gray-4 rounded-t-lg px-2 py-3 text-white font-regular text-sm'>
            <div className=' flex items-center justify-start w-4/12 '> TEAMS </div>
            <div className=' flex items-center  w-8/12  justify-between'>
              <div className='w-1/4 text-center'>P</div>
              <div className='w-1/4 text-center'>W</div>
              <div className='w-1/4 text-center'>L</div>

              <div className='w-1/4 text-center'>PTS</div>
              <div className='w-1/4 text-center'>QL%</div>
            </div>
          </div>

          {data &&
            data.topFourTeams &&
            data.topFourTeams.teams &&
            data.topFourTeams.teams[0].map((item, index) => {
              return (
               true&& (
                  <div key={index} className='flex   my-3 px-2 '>
                    <div className=' flex items-center justify-start w-4/12  text-gray-2 font-regular text-xs  '>
                      <img
                        className='h-5 w-7  rounded-sm mr-2'
                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                        alt=''
                      />
                      {item.teamName}
                    </div>
                    <div className=' flex items-center  w-8/12  justify-between text-gray-2 font-regular text-xs'>
                      <div className=' w-1/4 text-center'>{item.matchesPlayed}</div>
                      <div className=' w-1/4 text-center'>{item.wins}</div>
                      <div className=' w-1/4 text-center'>{item.lost}</div>

                      <div className='text-white text-bold w-1/4 text-center '>{item.points}</div>

                      {item.isQualified?  <div className=' w-1/4 flex text-center  justify-center '> 
                      <div className='bg-green-4  px-2 py-0.5 text-xs rounded-md text-white flex text-center  justify-center'>Q</div>
                      </div>: <div className=' w-1/4 text-center'> {item.qp}%</div>}
                    </div>
                  </div>
                )
              );
            })}
        </div>
      </div>
    ) : (
      <></>
    )
  }
}
