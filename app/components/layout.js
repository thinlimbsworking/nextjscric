'use client'
import Header from './header'
import Footer from './footer'
import React, { useEffect, useState, createContext } from 'react'
import { useTheme } from 'next-themes'
import useLocalStorage from './customHooks/useLocalStorage'
import { usePathname } from 'next/navigation'

// import useTabContent from './customHooks/useTabContent'

// import Footer from './footer';
//import BottomNavBar from '../components/BottomNavBar';

import BottomNavBar from './commom/bottomnavbar'
// import Loading from '../components/Loading';
import { useRouter } from 'next/navigation'

export const LocalStorageContext = createContext()
// export const TabConetnt = createContext()

const Layout = ({ children, isLoading, ...props }) => {
  let pathname = usePathname()

  let router = useRouter()
  const {
    setItem,
    getItem,
    removeItem,
    setTabContent,
    getTabContent,
  } = useLocalStorage()

  const { systemTheme, theme, setTheme } = useTheme()
  useEffect(() => {
    if (window.innerWidth < 700) {
      setTheme('dark')
    } else {
      setTheme('light')
    }
  }, [])

  return (
    <>
      <link
        rel="shortcut icon"
        href="https://images.cricket.com/icons/mainlogoico.ico"
      />
      <LocalStorageContext.Provider
        value={{ setItem, getItem, removeItem, setTabContent, getTabContent }}
      >
        <Header params={props.params} />
        <div
          className={
            pathname === '/'
              ? `text-black dark:bg-basebg dark:text-white`
              : 'mx-auto max-w-7xl lg:px-24 md:px-24 flex align-center justify-between text-black dark:bg-basebg dark:text-white'
          }

          // className={`mx-auto max-w-7xl px-10 flex align-center justify-between -mt-1    text-black dark:bg-basebg   dark:text-white  lg:h-screen md:h-screen overflow-auto`}
        >
          <div className=" w-full dark:bg-basebg md:pt-20 lg:pt-20 pb-16 md:min-h-screen">
            {children}
          </div>
        </div>

        {!pathname?.includes('/criclytics/') &&
          !pathname?.includes('match-score') &&
          !pathname?.includes('/series/') &&
          !pathname?.includes('create-team') &&
          !pathname?.includes('/fantasy-research-center/') &&
          !pathname?.includes('live-score') &&
          !pathname?.includes('teams/') &&
          !pathname?.includes('rankings') &&
          !pathname?.includes('records') &&
          !pathname?.includes('/players/') &&
          !pathname?.includes('/stadiums') &&
          !pathname?.includes('/exclusive') &&
          !pathname?.includes('/videos/') &&
          (pathname?.includes('/news/')
            ? pathname?.length < 22
              ? true
              : false
            : true) && (
            <div className=" w-full z-50 md:hidden">
              <BottomNavBar />
            </div>
          )}
        <div className={`hidden md:block lg:block w-full`}>
          <Footer />
        </div>
      </LocalStorageContext.Provider>
    </>
  )
}

export default Layout
