import React, { useEffect, useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { LAST_12_BALLS } from '../api/queries'
import Heading from '../components/commom/heading'
export default function Liveballs({ ...props }) {
  const [toggle, setToggle] = useState('LIVE')
  const { loading, error, data } = useQuery(LAST_12_BALLS, {
    variables: {
      matchID: props?.matchID,
      innings: props?.currentinningsNo,
    },
    pollInterval: 8000,
  })

const isOneBallOver = data?.last12Balls?.length && data?.last12Balls.findIndex(i => i.over.length == 1);

  return props?.matchData?.matchStatus !== 'completed' &&

  data &&
    data.last12Balls &&
    data.last12Balls.length > 0 ? (
      <div className="mx-2 mt-1">
       
    <div className="m rounded text-white w-full">
      {!props.hideTitle && <Heading heading={'Last 12 Balls'} />}

      <div className="flex md:justify-between mt-3 w-full gap-2 ">
        {data &&
          data.last12Balls &&
          data.last12Balls.map((overs, i) => (
              <div
                className={`p-1 dark:bg-gray dark:h-16 bg-white dark:border-gray-4 dark:flex dark:flex-col rounded-md md:mb-2 md:w-6/12 py-3 items-center
                w-${isOneBallOver !== -1 ? isOneBallOver === i ? "2" : overs.over.length-0.5 : overs.over.length}/12
                `}
                key={i}
              >
                <div
                // className='flex w-full justify-between'
                  className={`flex w-full ${
                    overs.over?.length <= 3
                      ? 'justify-start'
                      : 'justify-between'
                  }`}
                >
                  {overs.over.map((balls, y) => (
                    <div className="flex  justify-around" key={y}>
                      {/* <div className='h-5 w-5 text-xs flex items-center justify-center m-1 rounded-full bg-green '> </div> */}
                      {}
                      {balls.isBall ? (
                        balls.isWicket === 'true' ? (
                          <div
                            key={i}
                            className={`bg-red h-5 w-5 md:w-8 md:h-8 dark:text-xs rounded-full justify-center flex items-center mx-px text-black `}
                          >
                            W
                          </div>
                        ) : (
                          balls.runs && (
                            <div
                              key={i}
                              className={`h-5 w-5 md:w-8 md:h-8 dark:text-xs rounded-full justify-center flex items-center text-black ${
                                balls.runs === '6'
                                  ? 'bg-green'
                                  : balls.runs === '4'
                                  ? 'bg-green'
                                  : 'bg-gray-2'
                              } white f7 fw6 mx-px`}
                            >
                           
                              {balls.type === 'no ball' ||
                              balls.type === 'leg bye' ||
                              balls.type === 'bye'
                                ? balls.runs >= 1
                                  ? balls.runs
                                  : ''
                                : balls.runs}
                                   {balls.type === 'no ball'
                                ? 'nb'
                                : balls.type === 'leg bye'
                                ? 'lb'
                                : balls.type === 'bye'
                                ? 'b'
                                : ''}
                            </div>
                          )
                        )
                      ) : (
                        <div
                          key={y}
                          className={`justify-center flex items-center mx-px h-6 w-6 text-xs font-normal bg-gray-3 rounded-full
                              `}
                        >
                         
                          {balls.type === 'no ball' ||
                          balls.type === 'leg bye' ||
                          balls.type === 'bye' ||
                          balls.type === 'wide'
                            ? balls.runs >= 1
                              ? balls.runs
                              : ''
                            : balls.runs}
                             {balls.type === 'no ball'
                            ? 'nb'
                            : balls.type === 'leg bye'
                            ? 'lb'
                            : balls.type === 'bye'
                            ? 'b'
                            : balls.type === 'wide'
                            ? 'wd'
                            : ''}
                        </div>
                      )}
                    </div>
                  ))}
                </div>
                <div className="hidden lg:block md:block text-gray-2 font-medium text-xs dark:text-sm dark:text-left mt-1 dark:w-20 ml-2 ">
                  Over {overs.overNumber}
                </div>
                <div className="w-full md:hidden pt-1 text-gray-2 font-medium whitespace-nowrap text-xs text-left mt-1">
                  Over {overs.overNumber}
                </div>
              </div>
          ))}
      </div>
    </div>
    </div>
  ) : (
    <></>
  )
}
// ${ data.getcriclyticsCommonApi.lasttenball.length > 6 ? '' : 'mh2' }
