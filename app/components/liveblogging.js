import React, { useEffect, useState } from 'react';
import { format } from 'date-fns';
import Router from 'next/router';
import { useRouter } from 'next/router';


const backIconWhite = '/svgs/backIconWhite.svg';
const arrowLeft = '/pngs/arrow-left.png';
let scrollTop = null;
const LiveBlogging = ({ data }) => {

   let article = data&& data.data.getLiveBlockComments;
   const [showHeader, setShowHeader] = useState(false);


   useEffect(() => {
      if (typeof window !== 'undefined') {
         window.addEventListener('scroll', handleScroll);
      }

      return () => {
         if (typeof window !== 'undefined') {
            window.removeEventListener('scroll', handleScroll);
         }
      };
   }, []);
   const handleScroll = (event) => {
      ({ scrollTop } = event.target.scrollingElement);
      if (scrollTop > 208 && !showHeader) {
         setShowHeader(true);
      }
      if (scrollTop < 159) {
         setShowHeader(false);
      }
   };
   
   return (article&& <div className="max-w-5xl center">
      <div className='mr-2 absolute left-0 right-0 m-4 lg:hidden flex justify-between items-center '>
         {/* <Link {}> */}
         <img src={backIconWhite} alt='back icon' onClick={() => Router.back()} />
         {/* </Link> */}
      </div>
      {showHeader && (
         <div
            className='lg:hidden fixed top-0 left-0 right-0 bg-white px-2 flex items-center flex-auto justify-between z-4'
            style={{ height: 46 }}>
            <div className='flex items-center w-full'>
               {/* <Link {...getArticleTabUrl()}> */}
               <img src={arrowLeft} alt='back icon' onClick={() => Router.back()} />
               {/* </Link> */}
               <div className='f5 font-semibold ml-2 truncate'>{(article && article.title) || ''}</div>
            </div>
         </div>
      )}
      <div className='cover bg-top border-2'>
         <img className='h-5 w-full object-cover md:h-9 lg:h-9 xl:h-9 object-top' src={article&& article.bg_image_safari} alt='safari' />
      </div>


      <div className="bg-white px-2 py-2">
         <div className="flex justify-between py-2 items-center">
            <div className='flex items-center py-1 px-2 border-solid border-black rounded-full'>
               <span className='bg-green h-4 w-4 rounded-full' />
               <span className='f10 font-medium black-40 pl-1'>LIVE</span>
            </div>
            <div className="font-medium darkRed f6">{article.matchNumber} : {article.matchName}</div>
            <div className=" flex items-center justify-between  br2 py-1 white px-2 cdcgr">
               <div className="font-medium f6">Live Scores</div>
               <img className='cursor-pointer pl-1' alt='/svgs/RightSchevronWhite.svg'
                  src='/svgs/RightSchevronWhite.svg' />
            </div>
         </div>
         <div className="py-2 font-semibold f4">{article.title}</div>
         <div className="py-2 f6 font-medium">{(article && article.createdAt && format(+article.createdAt, 'dd MMM yyyy')) || ''}</div>
         <div className="timelineLiveBlog w-full">
            {article.comments.map((comm, i) =>
               <div className={` ${i === 0 ? '' : 'pt-2'} w-full   flex `} key={i}>
                  <div className="w-1 h-1 relative bg-darkRed border-red border-4"></div>
                  <div className="w-full  pl-2">
                     <div className="flex justify-between"><div className="font-medium darkRed f5 ">{comm.currentScore}</div><div className="font-medium f7 gray">{`${(comm && comm.createdAt && format(+comm.createdAt, ' hh:mm a'))} IST `}</div></div>
                     <div className="font-semibold f5">{comm.title}</div>
                     <div>{comm.content}</div></div>
               </div>
            )}

         </div>
      </div>
   </div>)
}

export default LiveBlogging;

