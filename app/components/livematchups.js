import React, { useState } from 'react'
import { HOME_SCREEN_PLAYER_MATCHUPS_LIVE } from '../api/queries'
import Image from 'next/Image'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Heading from './commom/heading'
export default function Livemtachups(props) {
  const [scaleIndex, setSlaceIndex] = useState(0)
  const { loading, error, data } = useQuery(HOME_SCREEN_PLAYER_MATCHUPS_LIVE, {
    variables: { matchID: props.matchID },
  })

  {
    // ;
  }

  return data &&
    data.HomeScreenPlayerMatchupsLive &&
    data.HomeScreenPlayerMatchupsLive.liveMatch &&
    data.HomeScreenPlayerMatchupsLive.liveMatch.batsman.length > 0 ? (
    <>
      {/* only MObile */}
      <div className=" lg:hidden md:hidden   mt-5 mx-3 text-white ">
        <div className="font-semibold text-lg tracking-wider uppercase">
          Live Player MatchUp
        </div>
        <div className="flex items-center justify-center mt-4">
          {data.HomeScreenPlayerMatchupsLive &&
            data.HomeScreenPlayerMatchupsLive.liveMatch &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.batsman && (
              <div
                className={` bg-gray  drop-shadow-2xl rounded-lg flex flex-col items-center justify-center  p-2 ${
                  scaleIndex === 0 ? 'w-1/2 h-56  z-50  ' : ' w-44 h-44'
                }`}
                style={{ transform: scaleIndex === 0 ? 'scale(1.05) ' : '' }}
                onClick={() => setSlaceIndex(0)}
              >
                <div className="w-full   flex items-center justify-center  ">
                  <div className=" relative  ">
                    <img
                      className="h-24 w-24 bg-gray-4 object-top object-cover rounded-full"
                      src={`https://images.cricket.com/players/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[0].playerID}_headshot_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngsV2/playerph.png')
                      }
                    />
                    <img
                      className=" absolute h-6 w-6  border  left-0 bottom-1 object-fill"
                      src="./pngsV2/battericon.png"
                    />
                    <div className=" absolute h-6 w-6 bg-white   right-0 bottom-1 rounded-full flex justify-center items-center">
                      <img
                        className="rounded-full h-5 w-5"
                        src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[0].teamID}_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngsV2/playerph.png')
                        }
                      />
                    </div>
                  </div>
                </div>

                <div className="text-base mt-3 font-semibold text-center text-white">
                  {
                    data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[0]
                      .playerName
                  }
                </div>
                {/* <div className='mt-1 text-gray-2 text-xs tracking-wide font-semibold'>BATTER</div> */}
              </div>
            )}

          {data &&
            data.HomeScreenPlayerMatchupsLive &&
            data.HomeScreenPlayerMatchupsLive.liveMatch &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.batsman &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.batsman.length > 1 && (
              <div
                className={`bg-gray  rounded-lg  flex flex-col items-center justify-center  p-2 ${
                  scaleIndex === 1 ? 'w-1/2 h-56   z-50' : ' w-44 h-44'
                }`}
                style={{ transform: scaleIndex === 1 ? 'scale(1.05)' : '' }}
                onClick={() => setSlaceIndex(1)}
              >
                <div className="flex relative  items-center justify-center ">
                  <img
                    className="  h-24 w-24  bg-gray-4  object-top object-cover rounded-full"
                    src={`https://images.cricket.com/players/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[1].playerID}_headshot_safari.png`}
                    onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                  />
                  <img
                    className=" absolute h-6 w-6  border  left-0 bottom-1 object-fill"
                    src="./pngsV2/battericon.png"
                  />
                  <div className=" absolute h-6 w-6 bg-white   right-0 bottom-1 rounded-full flex justify-center items-center">
                    <img
                      className="rounded-full h-5 w-5"
                      src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[1].teamID}_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngsV2/playerph.png')
                      }
                    />
                  </div>
                </div>
                <div className="text-base mt-3 font-semibold text-white text-center">
                  {
                    data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[1]
                      .playerName
                  }
                </div>
                {/* <div className='mt-1 text-gray-2 text-xs tracking-wide font-semibold'>BATTER</div> */}
              </div>
            )}
        </div>

        <div className="flex mt-3  items-center justify-between px-4">
          <div className="text-base my-3 text-gray font-bold text-center">
            <div className="text-gray-2 text-sm font-semibold">BALLS</div>
            <div className="text-green text-3xl font-bold">
              {
                data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[scaleIndex]
                  .ballfaced
              }
            </div>
            <div className="text-white font-semibold text-xl pt-1">
              {
                data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[scaleIndex]
                  .ballfacedPre
              }
            </div>
          </div>
          <div className="bg-gray-2 w-0.5 h-14"></div>
          <div className="text-base my-3 text-gray font-bold text-center">
            <div className="text-gray-2 text-sm font-semibold">RUNS</div>
            <div className="text-green text-3xl font-bold">
              {
                data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[scaleIndex]
                  .runsscored
              }
            </div>
            <div className="text-white font-semibold text-xl pt-1">
              {
                data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[scaleIndex]
                  .runsscoredPre
              }
            </div>
          </div>
          <div className="bg-gray-2 w-0.5 h-14"></div>
          <div className="text-base my-3 text-gray font-bold text-center">
            <div className="text-gray-2 text-sm font-semibold">WICKETS</div>
            <div className="text-green text-3xl font-bold">--</div>
            <div className="text-white font-semibold text-xl pt-1">
              {
                data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[scaleIndex]
                  .dismissalsPre
              }
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center my-3">
          <div className="flex items-center mr-3 ">
            <div className="w-2 h-2 rounded-full bg-green"></div>
            <div className="text-xs text-gray-2 pl-1 font-medium">
              Live Score
            </div>
          </div>
          <div className="flex items-center ml-3">
            <div className="w-2 h-2 rounded-full bg-white"></div>
            <div className="text-xs text-gray-2 pl-1 font-medium">
              Overall Score
            </div>
          </div>
        </div>
        {data &&
          data.HomeScreenPlayerMatchupsLive &&
          data.HomeScreenPlayerMatchupsLive.liveMatch &&
          data.HomeScreenPlayerMatchupsLive.liveMatch.bowler && (
            <div className="flex justify-center  items-center bg-gray rounded-lg py-3">
              <div className=" flex items-center justify-center  ">
                <div className="relative ">
                  <img
                    className="h-24 w-24 bg-gray-4 object-top object-cover rounded-full"
                    src={`https://images.cricket.com/players/${data.HomeScreenPlayerMatchupsLive.liveMatch.bowler.playerID}_headshot_safari.png`}
                    onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                  />

                  <img
                    className="absolute h-6 w-6  border  left-0 bottom-1 object-fill"
                    src="./pngsV2/bowlericon.png"
                  />
                  <div className=" absolute h-6 w-6 bg-white   right-0 bottom-1 rounded-full flex justify-center items-center">
                    <img
                      className="rounded-full h-5 w-5"
                      src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsLive.liveMatch.bowler.teamID}_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngsV2/playerph.png')
                      }
                    />
                  </div>
                </div>
              </div>
              <div className="ml-3">
                <div className="text-white text-lg font-semibold">
                  {
                    data.HomeScreenPlayerMatchupsLive.liveMatch.bowler
                      .playerName
                  }
                </div>
                <div className="mt-1 text-gray-2 text-xs tracking-wide font-semibold">
                  BOWLER
                </div>
              </div>
            </div>
          )}
      </div>

      {/* DESktop */}
      <div className="mt-5 mx-3 text-white hidden  lg:block md:block ">
        <Heading heading={'Live Player MatchUp'} />

        <div className="font-semibold mt-2 flex text-xs tracking-wider  w-full">
          <div className="w-8/12 ml-3  text-base font-bold"> Batting</div>
          <div className="w-3/12  text-base font-bold ">Bowling</div>
        </div>
        <div className="flex items-center justify-center mt-4">
          {data.HomeScreenPlayerMatchupsLive &&
            data.HomeScreenPlayerMatchupsLive.liveMatch &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.batsman && (
              <>
                <div
                  className={`flex rounded-xl  bg-gray  ${
                    scaleIndex == 0 ? 'w-6/12' : 'w-2/12 opacity-50'
                  } `}
                >
                  <div
                    className={`   drop-shadow-2xl rounded-lg flex flex-col items-center justify-center  p-2 ${
                      scaleIndex === 0 ? 'w-1/2 h-56  z-50  ' : ' w-44 h-44'
                    }`}
                    style={{
                      transform: scaleIndex === 0 ? 'scale(1.05) ' : '',
                    }}
                    onClick={() => setSlaceIndex(0)}
                  >
                    <div className="w-full   flex items-center justify-center  ">
                      <div className=" relative  ">
                        <img
                          className="h-32 w-32 bg-gray-4 object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[0].playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />
                        <img
                          className=" absolute h-6 w-6  border  left-0 bottom-1 object-fill"
                          src="./pngsV2/battericon.png"
                        />
                        <div className=" absolute h-6 w-6 bg-white   right-0 bottom-1 rounded-full flex justify-center items-center">
                          <img
                            className="rounded-full h-5 w-5"
                            src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[0].teamID}_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/pngsV2/playerph.png')
                            }
                          />
                        </div>
                      </div>
                    </div>

                    <div className="text-base mt-3 font-semibold text-center text-white">
                      {
                        data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[0]
                          .playerName
                      }
                    </div>
                  </div>

                  {scaleIndex == 0 && (
                    <div>
                      <div className="flex mt-3  items-center justify-between px-4">
                        <div className="text-base my-3 text-gray font-bold text-center">
                          <div className="text-gray-2 text-sm font-semibold">
                            BALLS
                          </div>
                          <div className="text-green text-3xl font-bold">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].ballfaced
                            }
                          </div>
                          <div className="text-white font-semibold text-xl pt-1">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].ballfacedPre
                            }
                          </div>
                        </div>
                        <div className="bg-gray-2 w-0.5 h-14"></div>
                        <div className="text-base my-3 text-gray font-bold text-center">
                          <div className="text-gray-2 text-sm font-semibold">
                            RUNS
                          </div>
                          <div className="text-green text-3xl font-bold">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].runsscored
                            }
                          </div>
                          <div className="text-white font-semibold text-xl pt-1">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].runsscoredPre
                            }
                          </div>
                        </div>
                        <div className="bg-gray-2 w-0.5 h-14"></div>
                        <div className="text-base my-3 text-gray font-bold text-center">
                          <div className="text-gray-2 text-sm font-semibold">
                            WICKETS
                          </div>
                          <div className="text-green text-3xl font-bold">
                            --
                          </div>
                          <div className="text-white font-semibold text-xl pt-1">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].dismissalsPre
                            }
                          </div>
                        </div>
                      </div>
                      <div className="flex justify-center items-center my-3">
                        <div className="flex items-center mr-3 ">
                          <div className="w-2 h-2 rounded-full bg-green"></div>
                          <div className="text-xs text-gray-2 pl-1 font-medium">
                            Live Score
                          </div>
                        </div>
                        <div className="flex items-center ml-3">
                          <div className="w-2 h-2 rounded-full bg-white"></div>
                          <div className="text-xs text-gray-2 pl-1 font-medium">
                            Overall Score
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {/* <div className='mt-1 text-gray-2 text-xs tracking-wide font-semibold'>BATTER</div> */}
                </div>
              </>
            )}

          {data &&
            data.HomeScreenPlayerMatchupsLive &&
            data.HomeScreenPlayerMatchupsLive.liveMatch &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.batsman &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.batsman.length > 1 && (
              <>
                <div
                  className={`flex  rounded-xl   bg-gray ${
                    scaleIndex == 1 ? 'w-6/12' : 'w-2/12 opacity-50'
                  } `}
                >
                  <div
                    className={` flex flex-col items-center justify-center   ${
                      scaleIndex === 1 ? 'w-1/2 h-56   z-50' : ' w-44 h-44 p-3'
                    }`}
                    style={{ transform: scaleIndex === 1 ? 'scale(1.05)' : '' }}
                    onClick={() => setSlaceIndex(1)}
                  >
                    <div className="flex relative  items-center justify-center ">
                      <img
                        className="  h-32 w-32 bg-gray-4  object-top object-cover rounded-full"
                        src={`https://images.cricket.com/players/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[1].playerID}_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngsV2/playerph.png')
                        }
                      />
                      <img
                        className=" absolute h-6 w-6  border  left-0 bottom-1 object-fill"
                        src="./pngsV2/battericon.png"
                      />
                      <div className=" absolute h-6 w-6 bg-white   right-0 bottom-1 rounded-full flex justify-center items-center">
                        <img
                          className="rounded-full h-5 w-5"
                          src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[1].teamID}_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />
                      </div>
                    </div>
                    <div className="text-base mt-3 font-semibold text-center text-white">
                      {
                        data.HomeScreenPlayerMatchupsLive.liveMatch.batsman[1]
                          .playerName
                      }
                    </div>
                  </div>

                  {scaleIndex == 1 && (
                    <div className="bg-gray rounded-xl ">
                      <div className="flex mt-3  items-center justify-between px-4">
                        <div className="text-base my-3 text-gray font-bold text-center">
                          <div className="text-gray-2 text-sm font-semibold">
                            BALLS
                          </div>
                          <div className="text-green text-3xl font-bold">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].ballfaced
                            }
                          </div>
                          <div className="text-white font-semibold text-xl pt-1">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].ballfacedPre
                            }
                          </div>
                        </div>
                        <div className="bg-gray-2 w-0.5 h-14"></div>
                        <div className="text-base my-3 text-gray font-bold text-center">
                          <div className="text-gray-2 text-sm font-semibold">
                            RUNS
                          </div>
                          <div className="text-green text-3xl font-bold">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].runsscored
                            }
                          </div>
                          <div className="text-white font-semibold text-xl pt-1">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].runsscoredPre
                            }
                          </div>
                        </div>
                        <div className="bg-gray-2 w-0.5 h-14"></div>
                        <div className="text-base my-3 text-gray font-bold text-center">
                          <div className="text-gray-2 text-sm font-semibold">
                            WICKETS
                          </div>
                          <div className="text-green text-3xl font-bold">
                            --
                          </div>
                          <div className="text-white font-semibold text-xl pt-1">
                            {
                              data.HomeScreenPlayerMatchupsLive.liveMatch
                                .batsman[scaleIndex].dismissalsPre
                            }
                          </div>
                        </div>
                      </div>
                      <div className="flex justify-center items-center my-3">
                        <div className="flex items-center mr-3 ">
                          <div className="w-2 h-2 rounded-full bg-green"></div>
                          <div className="text-xs text-gray-2 pl-1 font-medium">
                            Live Score
                          </div>
                        </div>
                        <div className="flex items-center ml-3">
                          <div className="w-2 h-2 rounded-full bg-white"></div>
                          <div className="text-xs text-gray-2 pl-1 font-medium">
                            Overall Score
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {/* <div className='mt-1 text-gray-2 text-xs tracking-wide font-semibold'>BATTER</div> */}
                </div>
              </>
            )}

          {data &&
            data.HomeScreenPlayerMatchupsLive &&
            data.HomeScreenPlayerMatchupsLive.liveMatch &&
            data.HomeScreenPlayerMatchupsLive.liveMatch.bowler && (
              <div className="flex flex-col mx-4  w-4/12  py-5 justify-center  items-center bg-gray rounded-lg ">
                <div className=" flex items-center justify-center  ">
                  <div className="relative ">
                    <img
                      className="h-32 w-32 bg-gray-4 object-top object-cover rounded-full"
                      src={`https://images.cricket.com/players/${data.HomeScreenPlayerMatchupsLive.liveMatch.bowler.playerID}_headshot_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngsV2/playerph.png')
                      }
                    />

                    <img
                      className="absolute h-6 w-6    left-0 bottom-1 object-fill"
                      src="./pngsV2/bowlericon.png"
                    />
                    <div className=" absolute h-6 w-6 bg-white   right-0 bottom-1 rounded-full flex justify-center items-center">
                      <img
                        className="rounded-full h-5 w-5"
                        src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsLive.liveMatch.bowler.teamID}_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngsV2/playerph.png')
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="ml-3">
                  <div className="text-white text-lg font-semibold text-center py-2">
                    {
                      data.HomeScreenPlayerMatchupsLive.liveMatch.bowler
                        .playerName
                    }
                  </div>
                  {/* <div className='mt-1 text-gray-2 text-xs tracking-wide font-semibold'>BOWLER</div> */}
                </div>
              </div>
            )}
        </div>
      </div>
    </>
  ) : (
    <div></div>
  )
}
