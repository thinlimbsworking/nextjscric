import React, { useEffect, useRef, useState } from 'react';
// import { Handle } from 'rc-slider';

import { useMutation } from '@apollo/react-hooks';
import { VERIFY_OTP_MUTATION, LOGIN_OTP_MUTATION } from '../../api/queries';

import tick from '../../public/pngs/tick.png';
import expertTeam from '../../public/svgs/expertTeam.svg';
import pointproj from '../../public/svgs/pointproj.svg';
import exportTeam from '../../public/svgs/exportTeam.svg';

export default function ConfirmDesktopOtp(props) {
  const [remainingTime, setRemainingTime] = useState(30);

  const enbaleButton = 'ba br2 fw5 f7 pa2 ph3 tc cursor-pointer white mt2 cdcgr h2 w-50';
  const disbleButton = 'ba br2 fw5 f7 pa2 ph3 tc cursor-pointer white mt2 bg-gray h2 w-50';

  const re = /^[0-9\b]+$/;
  const [resendEnable, setReEnable] = useState(false);
  const [otp1, setOtp1] = useState('');
  const [otp2, setOtp2] = useState('');
  const [otp3, setOtp3] = useState('');
  const [otp4, setOtp4] = useState('');
  const [otpResponse, setOtpResponse] = useState('');
  const refotp1 = useRef(null);
  const refotp2 = useRef(null);
  const refotp3 = useRef(null);
  const refotp4 = useRef(null);
  const timeout = useRef();

  const [LoginOtp, response2] = useMutation(LOGIN_OTP_MUTATION, {
    onCompleted: (responseOtp) => {
      setOtpResponse(responseOtp.login.message);
    }
  });

  const [confrimOtp, response] = useMutation(VERIFY_OTP_MUTATION, {
    onCompleted: (responseOtp) => {
      setOtpResponse(responseOtp.verifyOtp.message);
      setRemainingTime(0);
      if (responseOtp.verifyOtp.code !== 202) {
        localStorage.setItem('tokenData', responseOtp.verifyOtp.token);
        localStorage.setItem('userNumber', props.number);
        localStorage.setItem('userName', responseOtp.verifyOtp.username);

        setTimeout(() => {
          window.location = '/';
        }, 5000);
      }
    }
  });

  useEffect(() => {
    refotp1.current.focus();

    return () => {};
  }, []);

  useEffect(() => {
    if (timeout.current && remainingTime === 0) {
      setReEnable(true);
    } else {
      timeout.current = setTimeout(() => {
        setRemainingTime((t) => t - 1);
      }, 1000);
    }
    return () => {
      clearTimeout(timeout.current);
    };
  }, [remainingTime]);

  const handleChange = (value, method, ref) => {
    // for number only
    value === '' || re.test(value) ? method(value) : null;

    // for shifting of input feild
    if (re.test(value) && value.length > 0) {
      ref.current.focus();
    }
  };
  const handlePress = () => {
    let otp = `${otp1}${otp2}${otp3}${otp4}`;
    confrimOtp({ variables: { number: props.number, otp: parseInt(otp), email: '' } });
  };

  const reSend = () => {
    setOtpResponse('');
    setOtp1('');
    setOtp2('');
    setOtp3('');
    setOtp4('');

    LoginOtp({ variables: { type: 'mobile', number: props.number, email: '' } })(
      otpResponse !== 'Tried more than 5 times !! try after 24hrs' ? setRemainingTime(30) : null
    );
  };

  return (
    <>
      {' '}
      <div className='ma3 nt6  mw75-l center '>
        <div className='bg-white br2'>
          <div className='pt2 ph3 relative'>
            {otpResponse === 'Successfully Signup !!' ? (
              <div className='fl bg-white w-100 w-100-ns '>
                <div className='flex mt4 h3 justify-center'>
                  <img src={tick} alt='' />
                </div>
                <div className='  f4 fw6  tc mt-4   red center p-1'>
                  Login Successfully
                  <hr class='mw4 bb bw1 b--red-10' />
                  <hr class='mw3 bb bw1 b--red-10' />
                </div>
              </div>
            ) : (
              <>
                <div class='mw9 center ph3-ns'>
                  <div className='cf ph2-ns'>
                    <div className='fl w-100 w-50-ns pa2'>
                      <>
                        {otpResponse !== 'Successfully Signup !!' ? (
                          <div>
                            <div className='pa2  f8 fw6  tl mt4   gray center pa1'>ENTER OTP</div>
                            <div class='pa4 black-80 center '>
                              <div class='measure flex justify-center '>
                                <input
                                  id='name2'
                                  ref={refotp1}
                                  value={otp1}
                                  onChange={(e) => handleChange(e.target.value, setOtp1, refotp2)}
                                  maxLength={1}
                                  class='tc w-100 border-input ma2'
                                  type='text'
                                  aria-describedby='name-desc'
                                />

                                <input
                                  id='name2'
                                  ref={refotp2}
                                  maxLength={1}
                                  value={otp2}
                                  class='tc w-100 border-input ma2'
                                  onChange={(e) => handleChange(e.target.value, setOtp2, refotp3)}
                                  type='text'
                                  aria-describedby='name-desc'
                                />

                                <input
                                  id='name2'
                                  maxLength={1}
                                  class='tc w-100 border-input ma2'
                                  value={otp3}
                                  ref={refotp3}
                                  onChange={(e) => handleChange(e.target.value, setOtp3, refotp4)}
                                  type='text'
                                  aria-describedby='name-desc'
                                />

                                <input
                                  id='name2'
                                  maxLength={1}
                                  class='tc w-100 border-input ma2'
                                  ref={refotp4}
                                  value={otp4}
                                  onChange={(e) => handleChange(e.target.value, setOtp4)}
                                  type='text'
                                  aria-describedby='name-desc'
                                />
                              </div>

                              {otpResponse !== 'Tried more than 5 times !! try after 24hrs' ? (
                                <div className='  f8 fw6  mt3 tc gray center pa1'>
                                  Don't Recived OTP?{' '}
                                  <b
                                    className={resendEnable ? 'red_10' : 'gray'}
                                    onClick={resendEnable ? reSend : null}>
                                    RESEND
                                  </b>
                                </div>
                              ) : null}
                              {/* show if otpResponse is empty */}
                              {true && <div className='  f8 fw6  tc gray center pa1 mt3'>{remainingTime} sec </div>}

                              {/* show if otpResponse is Non empty */}

                              {otpResponse !== '' && <div className='  f7 fw6  tc red center mt2 '>{otpResponse}</div>}

                              <div className='flex justify-center  mt3'>
                                <div
                                  onClick={
                                    otpResponse !== 'Tried more than 5 times !! try after 24hrs' ? handlePress : null
                                  }
                                  className={
                                    otpResponse !== 'Tried more than 5 times !! try after 24hrs'
                                      ? enbaleButton
                                      : disbleButton
                                  }>
                                  {' '}
                                  CONFIRM
                                </div>
                              </div>
                            </div>
                          </div>
                        ) : (
                          <>
                            <div className='flex mt4 h3 justify-center'>
                              <img src={tick} alt='' />
                            </div>
                            <div className='pa2  f4 fw6  tc mt4   red center pa1'>
                              Login Successfully
                              <hr class='mw4 bb bw1 b--red-10' />
                              <hr class='mw3 bb bw1 b--red-10' />
                            </div>
                          </>
                        )}
                      </>
                    </div>

                    {/* 2nd div */}
                    <div className='fl w-100 w-50-ns pa2'>
                      <div className=' pv4'>
                        <div class='flex pa2 mb2 '>
                          <div class=' w-10   '>
                            <img src={expertTeam} alt='' />
                          </div>
                          <div class=' w-80 tl'>
                            <div class='f8  fw5  orange lh-copy'>EXPERT TEAM</div>
                            <div class='f8  fw5  black lh-copy'>
                              {' '}
                              Use our algorithm-generated ready-to-use teams to build your perfect fantasy team.
                            </div>
                          </div>
                        </div>

                        <div class='flex pa2 mb2 '>
                          <div class=' w-80 tr '>
                            <div class='f8  fw5  orange lh-copy'>POINTS PROJECTIONS</div>
                            <div class='f8  fw5  black  lh-copy'>
                              Points projections for each player to help you to choose your best XI.
                            </div>
                          </div>
                          <div class=' w-10 ma1 '>
                            <img src={pointproj} alt='' />
                          </div>
                        </div>
                        <div class='flex pa2 mb2 '>
                          <div class=' w-10  '>
                            <img src={exportTeam} alt='' />
                          </div>
                          <div class=' w-80 tl'>
                            <div class='f8  fw5  orange lh-copy'>EXPORTS YOUR TEAM</div>
                            <div class='f8  fw5  black lh-copy'>
                              Exporting your saved teams to FanFight is now just a tap away
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
