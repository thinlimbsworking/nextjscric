// 'use client'
// import React, { useContext, useEffect, useRef, useState } from 'react'
// // import { Handle } from 'rc-slider';
// import CleverTap from 'clevertap-react'
// import axios from 'axios'
// import { useQuery, useMutation } from '@apollo/react-hooks'
// import ImageWithFallback from '../commom/Image'
// import {
//   VERIFY_OTP_MUTATION,
//   LOGIN_OTP_MUTATION,
//   MERGE_PROFILE_AXIOS,
//   SEASON_FANTASY_LOGIN_AXIOS,
// } from '../../api/queries'
// import { useRouter } from 'next/navigation'
// import { LocalStorageContext } from '../layout'
// const URL ='https://apiv2.cricket.com/cricket'
//   ?'https://apiv2.cricket.com/cricket'
//   : 'https://api.devcdc.com/cricket/cricket'

// export default function ConfirmOtp(props) {
//   const { setItem, getItem } = useContext(LocalStorageContext)
//   console.log(
//     '',
//     getItem('Platform'),
//   )
//   const [remainingTime, setRemainingTime] = useState(30)
//   let router = useRouter()

//   let navigate = router.push()
//   const disbleButton =
//     ' border-2 fw5 f7 p-1 mt-2 text-center  cursor-pointer white mt2 bg-gray border rounded border-gray-2 text-gray-2 text-xs w-4/12 uppercase'
//   const enbaleButton =
//     'border-2 fw5 f7 p-1 mt-2 text-center  cursor-pointer white mt2 bg-gray h2 w-4/12 uppercase rounded border border-green text-green'

//   const re = /^[0-9\b]+$/
//   const [resendEnable, setReEnable] = useState(false)
//   const [errorMsj, setErrorMsg] = useState('')
//   const [errorMsjCode, setErrorMsgCode] = useState('')

//   const [otp1, setOtp1] = useState('')
//   // const [otp2, setOtp2] = useState('');
//   // const [otp3, setOtp3] = useState('');
//   // const [otp4, setOtp4] = useState('');
//   // const [otp5, setOtp5] = useState('');

//   const [otp6, setOtp6] = useState('')

//   const [otpResponse, setOtpResponse] = useState('')
//   const refotp1 = useRef(null)
//   // const refotp2 = useRef(null);
//   // const refotp3 = useRef(null);
//   // const refotp4 = useRef(null);
//   // const refotp5 = useRef(null);
//   // const refotp6 = useRef(null);

//   const timeout = useRef()

//   var baseDomain = '.cricket.com'
//   var expireAfter = new Date()

//   //setting up  cookie expire date after a week
//   expireAfter.setDate(expireAfter.getDate() + 100000)

//   var otp = `${otp1}`

//   const [LoginOtp, response2] = useMutation(LOGIN_OTP_MUTATION, {
//     onCompleted: (responseOtp) => {
//       setOtpResponse(responseOtp.login.message)
//     },
//   })

//   const [confrimOtp, response] = useMutation(VERIFY_OTP_MUTATION, {
//     onCompleted: (responseOtp) => {
//       setOtpResponse(responseOtp.verifyOtp.code)
//       setErrorMsg(responseOtp.verifyOtp.message)

//       if (responseOtp.verifyOtp.code == 200) {
//         setItem('tokenData', responseOtp.verifyOtp.token)
//         setItem('userNumber', props.number)

//         setItem('userName', responseOtp.verifyOtp.username)

//         setTimeout(() => {
//           window.location = '/'
//         }, 5000)
//       }
//     },
//   })

//   useEffect(() => {
//     refotp1.current.focus()

//     return () => {}
//   }, [])

//   useEffect(() => {
//     if (timeout.current && remainingTime === 0) {
//       setReEnable(true)
//     } else {
//       timeout.current = setTimeout(() => {
//         setRemainingTime((t) => t - 1)
//       }, 1000)
//     }
//     return () => {
//       clearTimeout(timeout.current)
//     }
//   }, [remainingTime])

//   const handleChange = (keycode, value, method, ref, backref) => {
//     if (value != '') {
//       setErrorMsg('')

//       value === '' || re.test(value) ? method(value) : null

//       if (
//         re.test(value) &&
//         value.length > 0 &&
//         (otp1 != '' || method !== setOtp1)
//       ) {
//         ref.current.focus()
//       }
//     } else {
//       value === '' || re.test(value) ? method(value) : null
//     }
//   }

//   // code: 200
//   // email: "kunal.patley@hdworks.in"
//   // emailotp: true
//   // mergeCheck: false
//   // phnumber: ""
//   // photp: false
//   // token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InBobnVtYmVyIjoiIiwiZW1haWwiOiJrdW5hbC5wYXRsZXlAaGR3b3Jrcy5pbiJ9LCJpYXQiOjE2MTUzNTgwOTQsImV4cCI6MTYyMDU0MjA5NH0.9QW8TJytRiVaZTVBZG7un9ydMjN4tHd1CB5t4L962mE"
//   // userID: "6045c65ce817151bcea075cd"
//   // username: ""

//   const handlePress = async () => {
//     axios
//       .post(`https://api.devcdc.com/cricket/services/verifyotp`, {
//         email: props.email,
//         phnumber: props.number,
//         checkphone: props.emailLogin ? false : true,
//         otp: parseInt(otp),
//       })
//       .then(async (res) => {
//
//         if (res.data.code === 200) {
//           setItem('tokenData', res.data.token)
//           setItem('userName', res.data.username || '')
//           setItem('userEmail', res.data.email)
//           setItem('userID', res.data.userID)
//           setItem('userNumber', res.data.phnumber)
//           setItem('photp', res.data.photp)
//           setItem('emailotp', res.data.emailotp)
//           setItem('mergeCheck', res.data.mergeCheck)
//           if (props.step != 'checkemail' && props.step != 'checkphone') {
//             !props.ShowLoginSucess ? setOtpResponse(res.data.code) : ''
//           }

//           CleverTap.initialize('LoginSuccess', {
//             source: props.emailLogin ? 'EmailLogin' : 'MobileLogin',
//             Platform: typeof window !== 'undefined' && getItem('Platform'),
//             mobile: res.data.phnumber,
//           })

//           if (
//             router.asPath == '/seasonfantasy/login' ||
//             router.asPath == '/playtheodds/login'
//           ) {
//             const partnershipData = await axios.post(
//               router.asPath == '/playtheodds/login'
//                 ? 'https://api.devcdc.com/cricket/cricket'
//                 : 'https://api.devcdc.com/cricket/cricket',
//               {
//                 query: SEASON_FANTASY_LOGIN_AXIOS,
//                 variables: { token: res.data.token },
//               },
//             )

//             if (
//               (router.asPath == '/seasonfantasy/login' ||
//                 router.asPath == '/playtheodds/login') &&
//               partnershipData.data.data.sessionFantasyToken.code === 200
//             ) {
//               if (router.asPath == '/seasonfantasy/login') {
//                 getItem('deviceType') === 'desktop' || props.mobile == false
//                   ? (window.location.href = `https://seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`)
//                   : (window.location.href = `https://m-seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`)
//               }
//               if (router.asPath == '/playtheodds/login') {
//                 document.cookie =
//                   'uniqueUserId=' +
//                   partnershipData.data.data.sessionFantasyToken.message +
//                   '; domain=' +
//                   baseDomain +
//                   '; expires=' +
//                   expireAfter +
//                   '; path=/'
//                 window.location.href = `http://playtheodds.cricket.com/?uniqueUserId=${partnershipData.data.data.sessionFantasyToken.message}`
//               }
//             }
//           }

//           if (
//             (props.step == 'checkemail' || props.step == 'checkphone') &&
//             !res.data.mergeCheck &&
//             router.asPath !== '/seasonfantasy/login'
//           ) {
//             var data2 = await axios.post(process.env.API, {
//               query: MERGE_PROFILE_AXIOS,
//               variables: { token: getItem('tokenData') },
//             })

//             if (data2 && data2.data.data.mergeProfiles.code == 200) {
//               setItem('mergeCheck', true)
//               window.location = '/login'
//             }
//           } else {
//             if (props.direct === 'true') {
//               navigate(props.navTo)
//             }

//             if (
//               router.asPath !== '/seasonfantasy/login' ||
//               router.asPath !== '/playtheodds/login'
//             ) {
//               !props.ShowLoginSucess
//                 ? setTimeout(() => {
//                     props.setShowLogin
//                       ? props.setShowLogin(false)
//                       : props.navTo
//                       ? navigate(props.navTo)
//                       : (window.location = '/login')
//                   }, 2000)
//                 : setTimeout(() => {
//                     setOtpResponse(200)
//                     props.setShowLogin(false)
//                     props.navTo
//                       ? navigate(props.navTo)
//                       : (window.location = '/login')
//                   }, 2000)
//             }
//           }
//         } else {
//           if (res.data.code == 203) {
//             setErrorMsgCode(res.data.code)
//           }
//           setErrorMsg(res.data.Details)
//         }
//       })
//       .catch((error) => {})
//   }

//   const reSend = () => {
//     setOtpResponse('')
//     setOtp1('')
//     // setOtp2('');
//     // setOtp3('');
//     // setOtp4('');
//     // setOtp5('');
//     // setOtp6('');
//     setReEnable(false)
//     setErrorMsg('')

//     axios
//       .post(`https://api.devcdc.com/cricket/services/getotp`, {
//         email: props.email,
//         checkphone: props.emailLogin ? false : true,

//         phnumber: props.number,
//       })
//       .then((res) => {
//         setErrorMsg('')
//         props.setSession(res.data.Details)
//         res.data.Status === 'Error' ? setErrorMsg(res.data.Details) : ''

//         if (res.data.code == 203) {
//           setErrorMsgCode(res.data.code)
//         }
//       })
//       .catch((error) => {})

//     remainingTime == 0 ? setRemainingTime(30) : null
//   }

//   const handleKeyPress = (keycode, value, method, ref, backref) => {
//     // if (e.keyCode == 13 && otp.length == 4) {

//     if (keycode == 8 && value == '') backref.current.focus()
//   }
//   // const handleKeyPress = (e, refotp) => {
//   //   if (e.keyCode == 13 && otp.length == 4) {
//   //     handlePress();
//   //   }
//   // };

//   return (
//     <>
//       {otpResponse !== 200 ? (
//         <div className="text-white">
//           {!router.asPath?.includes('/login') && (
//             <div
//               className="flex justify-end right-0 z-9999"
//               style={{ top: '-30px' }}
//             >
//               <ImageWithFallback
//                 height={18}
//                 width={18}
//                 loading="lazy"
//                 className="h-6 w-6  cursor-pointer rounded-full"
//                 alt="close icon"
//                 src={'/pngs/closeIcon.png'}
//                 onClick={() =>
//                   props.onCLose === '/more' ||
//                   router?.asPath == '/fantasy-research-center/upcoming'
//                     ? props.setShowLogin(false)
//                     : (window.location = '/login')
//                 }
//               />
//             </div>
//           )}
//           {props.emailLogin && (
//             <div className="flex justify-center flex-col items-center ">
//               <ImageWithFallback
//                 height={18}
//                 width={18}
//                 loading="lazy"
//                 src={'/pngs/email-icon-large-new.png'}
//                 alt="email"
//               />
//               <div className="mt-2 flex justify-center items-center flex-col">
//                 <div class="bg-red_10 h-1 w-24" />
//                 <div class="bg-red_10 h-1 mt-2 w-20" />
//               </div>

//               <div className="text-xs text-center mt-4 center p-1">
//                 We sent an OTP to the email {props.email}
//               </div>
//             </div>
//           )}
//           {!props.emailLogin && (
//             <div className="font-bold  text-xs text-center  py-3">
//               ENTER OTP
//             </div>
//           )}
//           <div className="pa2 center ">
//             <div className="flex justify-center ">
//               <input
//                 id="name2"
//                 ref={refotp1}
//                 value={otp1}
//                 pattern="[0-9]*"
//                 onKeyDown={(e) =>
//                   handleKeyPress(
//                     e.keyCode,
//                     e.target.value,
//                     setOtp1,
//                     refotp1,
//                     refotp1,
//                   )
//                 }
//                 type="tel"
//                 onChange={(e) =>
//                   handleChange(e, e.target.value, setOtp1, refotp1, refotp1)
//                 }
//                 maxLength={6}
//                 className="text-center bg-gray-8 m-2 p-2 outline-none"
//                 aria-describedby="name-desc"
//               />

//               {/* <input
//                 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp2, refotp2, refotp1)}
//                 id='name2'
//                 ref={refotp2}
//                 maxLength={1}
//                 value={otp2}
//                 className='text-center  w-2/12 border bg-gray-4 m-2'
//                 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp2, refotp3, refotp1)}
//                 pattern='[0-9]*'
//                 type='tel'
//                 aria-describedby='name-desc'
//               /> */}

//               {/* <input
//                 id='name2'
//                 maxLength={1}
//                 pattern='[0-9]*'
//                 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp3, refotp3, refotp2)}
//                 className='text-center  w-2/12 border bg-gray-4 m-2'
//                 value={otp3}
//                 ref={refotp3}
//                 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp3, refotp4, refotp2)}
//                 type='tel'
//                 aria-describedby='name-desc'
//               /> */}
//               {/*
//               <input
//                 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp4, refotp4, refotp3)}
//                 id='name2'
//                 maxLength={1}
//                 className='text-center  w-2/12 border bg-gray-4 m-2'
//                 ref={refotp4}
//                 pattern='[0-9]*'
//                 type='tel'
//                 value={otp4}
//                 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp4, refotp5, refotp3)}
//                 aria-describedby='name-desc'
//               /> */}

//               {/* <input
//                 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp5, refotp5, refotp4)}
//                 id='name2'
//                 maxLength={1}
//                 className='text-center  w-2/12 border bg-gray-4 m-2'
//                 ref={refotp5}
//                 value={otp5}
//                 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp5, refotp6, refotp4)}
//                 pattern='[0-9]*'
//                 type='tel'
//                 aria-describedby='name-desc'
//               /> */}

//               {/* <input
//                 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp6, refotp6, refotp5)}
//                 id='name2'
//                 maxLength={1}
//                 className='text-center  w-2/12 border bg-gray-4 m-2 '
//                 ref={refotp6}
//                 value={otp6}
//                 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp6, refotp6, refotp5)}
//                 pattern='[0-9]*'
//                 type='tel'
//                 aria-describedby='name-desc'
//               /> */}
//             </div>

//             {errorMsjCode !== 203 && (
//               <div className="  text-xs  mt-3 text-center  center p-1 cursor-pointer">
//                 Didn’t receive OTP ? Request for a new one in
//               </div>
//             )}
//             <div className="text-xs mb-2 text-center  center p-1 cursor-pointer">
//               {remainingTime == 0 && (
//                 <span
//                   className={resendEnable ? 'text-red' : 'text-red'}
//                   onClick={resendEnable ? reSend : null}
//                 >
//                   RESEND
//                 </span>
//               )}
//             </div>

//             {/* show if otpResponse is empty */}
//             {errorMsjCode !== 203 && remainingTime != 0 && (
//               <div className="  f8 fw6  text-center white center pa1 mt3">
//                 {remainingTime} sec{' '}
//               </div>
//             )}

//             {/* show if otpResponse is Non empty */}

//             {otpResponse !== 200 && (
//               <div className="  f7 fw6  text-center red center mt2 ">
//                 {errorMsj}
//               </div>
//             )}

//             <div className="flex justify-center cursor-pointer">
//               <div
//                 onClick={otp.length == 6 ? handlePress : null}
//                 className={otp.length == 6 ? enbaleButton : disbleButton}
//               >
//                 {' '}
//                 CONFIRM
//               </div>
//             </div>
//           </div>
//         </div>
//       ) : (
//         <>
//           {router.asPath !== '/seasonfantasy/login' ? (
//             <div className="flex flex-col  item-center mt4 ph5 justify-center">
//               <div className="  f4 fw6  text-center mt4   red center pa1">
//                 <ImageWithFallback
//                   height={68}
//                   width={68}
//                   loading="lazy"
//                   src={'/pngs/loggedin-successfull.png'}
//                   alt="success"
//                 />
//               </div>

//               <div className="  f4 fw6  text-center mt4   red center pa1">
//                 <div className="flex flex-col items-center justify-center text-center w-100">
//                   <div> Login Successful</div>
//                   <div class="bg-red_10 h-1 w-28 mt-2" />
//                   <div class="bg-red_10 h-1 mt-2 w-20" />
//                 </div>
//               </div>
//             </div>
//           ) : (
//             <></>
//           )}
//         </>
//       )}
//     </>
//   )
// }

// 'use client'
// import React, { useContext, useEffect, useRef, useState } from 'react'
// // import { Handle } from 'rc-slider';
// import CleverTap from 'clevertap-react'
// import axios from 'axios'
// import { useQuery, useMutation } from '@apollo/react-hooks'
// import ImageWithFallback from '../commom/Image'
// import {
// VERIFY_OTP_MUTATION,
// LOGIN_OTP_MUTATION,
// MERGE_PROFILE_AXIOS,
// SEASON_FANTASY_LOGIN_AXIOS,
// } from '../../api/queries'
// import { useRouter } from 'next/navigation'
// import { LocalStorageContext } from '../layout'
// const URL ='https://apiv2.cricket.com/cricket'
// ?'https://apiv2.cricket.com/cricket'
// : 'https://api.devcdc.com/cricket/cricket'

// export default function ConfirmOtp(props) {
// const { setItem, getItem } = useContext(LocalStorageContext)
// console.log(
// '',
// getItem('Platform'),
// )
// const [remainingTime, setRemainingTime] = useState(30)
// let router = useRouter()

// let navigate = router.push
// const disbleButton =
// ' border-2 fw5 f7 p-1 mt-2 text-center cursor-pointer white mt2 bg-gray border rounded border-gray-2 text-gray-2 text-xs w-4/12 uppercase'
// const enbaleButton =
// 'border-2 fw5 f7 p-1 mt-2 text-center cursor-pointer white mt2 bg-gray h2 w-4/12 uppercase rounded border border-green text-green'

// const re = /^[0-9\b]+$/
// const [resendEnable, setReEnable] = useState(false)
// const [errorMsj, setErrorMsg] = useState('')
// const [errorMsjCode, setErrorMsgCode] = useState('')

// const [otp1, setOtp1] = useState('')
// // const [otp2, setOtp2] = useState('');
// // const [otp3, setOtp3] = useState('');
// // const [otp4, setOtp4] = useState('');
// // const [otp5, setOtp5] = useState('');

// const [otp6, setOtp6] = useState('')

// const [otpResponse, setOtpResponse] = useState('')
// const refotp1 = useRef(null)
// // const refotp2 = useRef(null);
// // const refotp3 = useRef(null);
// // const refotp4 = useRef(null);
// // const refotp5 = useRef(null);
// // const refotp6 = useRef(null);

// const timeout = useRef()

// var baseDomain = '.cricket.com'
// var expireAfter = new Date()

// //setting up cookie expire date after a week
// expireAfter.setDate(expireAfter.getDate() + 100000)

// var otp = `${otp1}`

// const [LoginOtp, response2] = useMutation(LOGIN_OTP_MUTATION, {
// onCompleted: (responseOtp) => {
// setOtpResponse(responseOtp.login.message)
// },
// })

// const [confrimOtp, response] = useMutation(VERIFY_OTP_MUTATION, {
// onCompleted: (responseOtp) => {
// setOtpResponse(responseOtp.verifyOtp.code)
// setErrorMsg(responseOtp.verifyOtp.message)

// if (responseOtp.verifyOtp.code == 200) {
// setItem('tokenData', responseOtp.verifyOtp.token)
// setItem('userNumber', props.number)

// setItem('userName', responseOtp.verifyOtp.username)

// setTimeout(() => {
// window.location = '/'
// }, 5000)
// }
// },
// })

// useEffect(() => {
// refotp1.current.focus()

// return () => {}
// }, [])

// useEffect(() => {
// if (timeout.current && remainingTime === 0) {
// setReEnable(true)
// } else {
// timeout.current = setTimeout(() => {
// setRemainingTime((t) => t - 1)
// }, 1000)
// }
// return () => {
// clearTimeout(timeout.current)
// }
// }, [remainingTime])

// const handleChange = (keycode, value, method, ref, backref) => {
// if (value != '') {
// setErrorMsg('')

// value === '' || re.test(value) ? method(value) : null

// if (
// re.test(value) &&
// value.length > 0 &&
// (otp1 != '' || method !== setOtp1)
// ) {
// ref.current.focus()
// }
// } else {
// value === '' || re.test(value) ? method(value) : null
// }
// }

// // code: 200
// // email: "kunal.patley@hdworks.in"
// // emailotp: true
// // mergeCheck: false
// // phnumber: ""
// // photp: false
// // token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InBobnVtYmVyIjoiIiwiZW1haWwiOiJrdW5hbC5wYXRsZXlAaGR3b3Jrcy5pbiJ9LCJpYXQiOjE2MTUzNTgwOTQsImV4cCI6MTYyMDU0MjA5NH0.9QW8TJytRiVaZTVBZG7un9ydMjN4tHd1CB5t4L962mE"
// // userID: "6045c65ce817151bcea075cd"
// // username: ""

// const handlePress = async () => {
// axios
// .post(`https://api.devcdc.com/cricket/services/verifyotp`, {
// email: props.email,
// phnumber: props.number,
// checkphone: props.emailLogin ? false : true,
// otp: parseInt(otp),
// })
// .then(async (res) => {
//
// if (res.data.code === 200) {
// setItem('tokenData', res.data.token)
// setItem('userName', res.data.username || '')
// setItem('userEmail', res.data.email)
// setItem('userID', res.data.userID)
// setItem('userNumber', res.data.phnumber)
// setItem('photp', res.data.photp)
// setItem('emailotp', res.data.emailotp)
// setItem('mergeCheck', res.data.mergeCheck)
// if (props.step != 'checkemail' && props.step != 'checkphone') {
// !props.ShowLoginSucess ? setOtpResponse(res.data.code) : ''
// }

// CleverTap.initialize('LoginSuccess', {
// source: props.emailLogin ? 'EmailLogin' : 'MobileLogin',
// Platform: typeof window !== 'undefined' && getItem('Platform'),
// mobile: res.data.phnumber,
// })

// if (
// router.asPath == '/seasonfantasy/login' ||
// router.asPath == '/playtheodds/login'
// ) {
// const partnershipData = await axios.post(
// router.asPath == '/playtheodds/login'
// ? 'https://api.devcdc.com/cricket/cricket'
// : 'https://api.devcdc.com/cricket/cricket',
// {
// query: SEASON_FANTASY_LOGIN_AXIOS,
// variables: { token: res.data.token },
// },
// )

// if (
// (router.asPath == '/seasonfantasy/login' ||
// router.asPath == '/playtheodds/login') &&
// partnershipData.data.data.sessionFantasyToken.code === 200
// ) {
// if (router.asPath == '/seasonfantasy/login') {
// getItem('deviceType') === 'desktop' || props.mobile == false
// ? (window.location.href = `https://seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`)
// : (window.location.href = `https://m-seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`)
// }
// if (router.asPath == '/playtheodds/login') {
// document.cookie =
// 'uniqueUserId=' +
// partnershipData.data.data.sessionFantasyToken.message +
// '; domain=' +
// baseDomain +
// '; expires=' +
// expireAfter +
// '; path=/'
// window.location.href = `http://playtheodds.cricket.com/?uniqueUserId=${partnershipData.data.data.sessionFantasyToken.message}`
// }
// }
// }

// if (
// (props.step == 'checkemail' || props.step == 'checkphone') &&
// !res.data.mergeCheck &&
// router.asPath !== '/seasonfantasy/login'
// ) {
// var data2 = await axios.post(process.env.API, {
// query: MERGE_PROFILE_AXIOS,
// variables: { token: getItem('tokenData') },
// })

// if (data2 && data2.data.data.mergeProfiles.code == 200) {
// setItem('mergeCheck', true)
// window.location = '/login'
// }
// } else {
// if (props.direct === 'true') {
// navigate(props.navTo)
// }

// if (
// router.asPath !== '/seasonfantasy/login' ||
// router.asPath !== '/playtheodds/login'
// ) {
// !props.ShowLoginSucess
// ? setTimeout(() => {
// props.setShowLogin
// ? props.setShowLogin(false)
// : props.navTo
// ? navigate(props.navTo)
// : (window.location = '/login')
// }, 2000)
// : setTimeout(() => {
// setOtpResponse(200)
// props.setShowLogin(false)
// props.navTo
// ? navigate(props.navTo)
// : (window.location = '/login')
// }, 2000)
// }
// }
// } else {
// if (res.data.code == 203) {
// setErrorMsgCode(res.data.code)
// }
// setErrorMsg(res.data.Details)
// }
// })
// .catch((error) => {})
// }

// const reSend = () => {
// setOtpResponse('')
// setOtp1('')
// // setOtp2('');
// // setOtp3('');
// // setOtp4('');
// // setOtp5('');
// // setOtp6('');
// setReEnable(false)
// setErrorMsg('')

// axios
// .post(`https://api.devcdc.com/cricket/services/getotp`, {
// email: props.email,
// checkphone: props.emailLogin ? false : true,

// phnumber: props.number,
// })
// .then((res) => {
// setErrorMsg('')
// props.setSession(res.data.Details)
// res.data.Status === 'Error' ? setErrorMsg(res.data.Details) : ''

// if (res.data.code == 203) {
// setErrorMsgCode(res.data.code)
// }
// })
// .catch((error) => {})

// remainingTime == 0 ? setRemainingTime(30) : null
// }

// const handleKeyPress = (keycode, value, method, ref, backref) => {
// // if (e.keyCode == 13 && otp.length == 4) {

// if (keycode == 8 && value == '') backref.current.focus()
// }
// // const handleKeyPress = (e, refotp) => {
// // if (e.keyCode == 13 && otp.length == 4) {
// // handlePress();
// // }
// // };

// return (
// <>
// {otpResponse !== 200 ? (
// <div className="text-white">
// {!router.asPath?.includes('/login') && (
// <div
// className="flex justify-end right-0 z-9999"
// style={{ top: '-30px' }}
// >
// <ImageWithFallback
// height={18}
// width={18}
// loading="lazy"
// className="h-6 w-6 cursor-pointer rounded-full"
// alt="close icon"
// src={'/pngs/closeIcon.png'}
// onClick={() =>
// props.onCLose === '/more' ||
// router?.asPath == '/fantasy-research-center/upcoming'
// ? props.setShowLogin(false)
// : (window.location = '/login')
// }
// />
// </div>
// )}
// {props.emailLogin && (
// <div className="flex justify-center flex-col items-center ">
// <ImageWithFallback
// height={18}
// width={18}
// loading="lazy"
// src={'/pngs/email-icon-large-new.png'}
// alt="email"
// />
// <div className="mt-2 flex justify-center items-center flex-col">
// <div class="bg-red_10 h-1 w-24" />
// <div class="bg-red_10 h-1 mt-2 w-20" />
// </div>

// <div className="text-xs text-center mt-4 center p-1">
// We sent an OTP to the email {props.email}
// </div>
// </div>
// )}
// {!props.emailLogin && (
// <div className="font-bold text-xs text-center py-3">
// ENTER OTP
// </div>
// )}
// <div className="pa2 center ">
// <div className="flex justify-center ">
// <input
// id="name2"
// ref={refotp1}
// value={otp1}
// pattern="[0-9]*"
// onKeyDown={(e) =>
// handleKeyPress(
// e.keyCode,
// e.target.value,
// setOtp1,
// refotp1,
// refotp1,
// )
// }
// type="tel"
// onChange={(e) =>
// handleChange(e, e.target.value, setOtp1, refotp1, refotp1)
// }
// maxLength={6}
// className="text-center bg-gray-8 m-2 p-2 outline-none"
// aria-describedby="name-desc"
// />

// {/* <input
// onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp2, refotp2, refotp1)}
// id='name2'
// ref={refotp2}
// maxLength={1}
// value={otp2}
// className='text-center w-2/12 border bg-gray-4 m-2'
// onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp2, refotp3, refotp1)}
// pattern='[0-9]*'
// type='tel'
// aria-describedby='name-desc'
// /> */}

// {/* <input
// id='name2'
// maxLength={1}
// pattern='[0-9]*'
// onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp3, refotp3, refotp2)}
// className='text-center w-2/12 border bg-gray-4 m-2'
// value={otp3}
// ref={refotp3}
// onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp3, refotp4, refotp2)}
// type='tel'
// aria-describedby='name-desc'
// /> */}
// {/*
// <input
// onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp4, refotp4, refotp3)}
// id='name2'
// maxLength={1}
// className='text-center w-2/12 border bg-gray-4 m-2'
// ref={refotp4}
// pattern='[0-9]*'
// type='tel'
// value={otp4}
// onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp4, refotp5, refotp3)}
// aria-describedby='name-desc'
// /> */}

// {/* <input
// onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp5, refotp5, refotp4)}
// id='name2'
// maxLength={1}
// className='text-center w-2/12 border bg-gray-4 m-2'
// ref={refotp5}
// value={otp5}
// onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp5, refotp6, refotp4)}
// pattern='[0-9]*'
// type='tel'
// aria-describedby='name-desc'
// /> */}

// {/* <input
// onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp6, refotp6, refotp5)}
// id='name2'
// maxLength={1}
// className='text-center w-2/12 border bg-gray-4 m-2 '
// ref={refotp6}
// value={otp6}
// onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp6, refotp6, refotp5)}
// pattern='[0-9]*'
// type='tel'
// aria-describedby='name-desc'
// /> */}
// </div>

// {errorMsjCode !== 203 && (
// <div className=" text-xs mt-3 text-center center p-1 cursor-pointer">
// Didn’t receive OTP ? Request for a new one in
// </div>
// )}
// <div className="text-xs mb-2 text-center center p-1 cursor-pointer">
// {remainingTime == 0 && (
// <span
// className={resendEnable ? 'text-red' : 'text-red'}
// onClick={resendEnable ? reSend : null}
// >
// RESEND
// </span>
// )}
// </div>

// {/* show if otpResponse is empty */}
// {errorMsjCode !== 203 && remainingTime != 0 && (
// <div className=" f8 fw6 text-center white center pa1 mt3">
// {remainingTime} sec{' '}
// </div>
// )}

// {/* show if otpResponse is Non empty */}

// {otpResponse !== 200 && (
// <div className=" f7 fw6 text-center red center mt2 ">
// {errorMsj}
// </div>
// )}

// <div className="flex justify-center cursor-pointer">
// <div
// onClick={otp.length == 6 ? handlePress : null}
// className={otp.length == 6 ? enbaleButton : disbleButton}
// >
// {' '}
// CONFIRM
// </div>
// </div>
// </div>
// </div>
// ) : (
// <>
// {router.asPath !== '/seasonfantasy/login' ? (
// <div className="flex flex-col item-center mt4 ph5 justify-center">
// <div className=" f4 fw6 text-center mt4 red center pa1">
// <ImageWithFallback
// height={68}
// width={68}
// loading="lazy"
// src={'/pngs/loggedin-successfull.png'}
// alt="success"
// />
// </div>

// <div className=" f4 fw6 text-center mt4 red center pa1">
// <div className="flex flex-col items-center justify-center text-center w-100">
// <div> Login Successful</div>
// <div class="bg-red_10 h-1 w-28 mt-2" />
// <div class="bg-red_10 h-1 mt-2 w-20" />
// </div>
// </div>
// </div>
// ) : (
// <></>
// )}
// </>
// )}
// </>
// )
// }

'use client'
import React, { useContext, useEffect, useRef, useState } from 'react'
// import { Handle } from 'rc-slider';
import CleverTap from 'clevertap-react'
import axios from 'axios'
import { useQuery, useMutation } from '@apollo/react-hooks'
import ImageWithFallback from '../commom/Image'
import {
  VERIFY_OTP_MUTATION,
  LOGIN_OTP_MUTATION,
  MERGE_PROFILE_AXIOS,
  SEASON_FANTASY_LOGIN_AXIOS,
} from '../../api/queries'
import { useRouter } from 'next/navigation'
import { LocalStorageContext } from '../layout'
const URL = 'https://apiv2.cricket.com/cricket'
  ? 'https://apiv2.cricket.com/cricket'
  : 'https://api.devcdc.com/cricket/cricket'

export default function ConfirmOtp(props) {
  const { setItem, getItem } = useContext(LocalStorageContext)
  console.log('', getItem('Platform'))
  const [remainingTime, setRemainingTime] = useState(30)
  let router = useRouter()

  let navigate = router.push
  const disbleButton =
    ' border-2 fw5 f7 p-1 mt-2 text-center md:cursor-pointer white mt2 bg-gray border rounded border-gray-2 text-gray-2 text-xs w-4/12 uppercase'
  const enbaleButton =
    'border-2 fw5 f7 p-1 mt-2 text-center md:cursor-pointer white mt2 bg-gray h2 w-4/12 uppercase rounded border border-green text-green'

  const re = /^[0-9\b]+$/
  const [resendEnable, setReEnable] = useState(false)
  const [errorMsj, setErrorMsg] = useState('')
  const [errorMsjCode, setErrorMsgCode] = useState('')

  const [otp1, setOtp1] = useState('')
  // const [otp2, setOtp2] = useState('');
  // const [otp3, setOtp3] = useState('');
  // const [otp4, setOtp4] = useState('');
  // const [otp5, setOtp5] = useState('');

  const [otp6, setOtp6] = useState('')

  const [otpResponse, setOtpResponse] = useState('')
  const refotp1 = useRef(null)
  // const refotp2 = useRef(null);
  // const refotp3 = useRef(null);
  // const refotp4 = useRef(null);
  // const refotp5 = useRef(null);
  // const refotp6 = useRef(null);

  const timeout = useRef()

  var baseDomain = '.cricket.com'
  var expireAfter = new Date()

  //setting up cookie expire date after a week
  expireAfter.setDate(expireAfter.getDate() + 100000)

  var otp = `${otp1}`

  const [LoginOtp, response2] = useMutation(LOGIN_OTP_MUTATION, {
    onCompleted: (responseOtp) => {
      setOtpResponse(responseOtp.login.message)
    },
  })

  const [confrimOtp, response] = useMutation(VERIFY_OTP_MUTATION, {
    onCompleted: (responseOtp) => {
      setOtpResponse(responseOtp.verifyOtp.code)
      setErrorMsg(responseOtp.verifyOtp.message)

      if (responseOtp.verifyOtp.code == 200) {
        setItem('tokenData', responseOtp.verifyOtp.token)
        setItem('userNumber', props.number)

        setItem('userName', responseOtp.verifyOtp.username)

        setTimeout(() => {
          window.location = '/'
        }, 5000)
      }
    },
  })

  useEffect(() => {
    refotp1.current.focus()

    return () => {}
  }, [])

  useEffect(() => {
    if (timeout.current && remainingTime === 0) {
      setReEnable(true)
    } else {
      timeout.current = setTimeout(() => {
        setRemainingTime((t) => t - 1)
      }, 1000)
    }
    return () => {
      clearTimeout(timeout.current)
    }
  }, [remainingTime])

  const handleChange = (keycode, value, method, ref, backref) => {
    if (value != '') {
      setErrorMsg('')

      value === '' || re.test(value) ? method(value) : null

      if (
        re.test(value) &&
        value.length > 0 &&
        (otp1 != '' || method !== setOtp1)
      ) {
        ref.current.focus()
      }
    } else {
      value === '' || re.test(value) ? method(value) : null
    }
  }

  // code: 200
  // email: "kunal.patley@hdworks.in"
  // emailotp: true
  // mergeCheck: false
  // phnumber: ""
  // photp: false
  // token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InBobnVtYmVyIjoiIiwiZW1haWwiOiJrdW5hbC5wYXRsZXlAaGR3b3Jrcy5pbiJ9LCJpYXQiOjE2MTUzNTgwOTQsImV4cCI6MTYyMDU0MjA5NH0.9QW8TJytRiVaZTVBZG7un9ydMjN4tHd1CB5t4L962mE"
  // userID: "6045c65ce817151bcea075cd"
  // username: ""

  const handlePress = async () => {
    axios
      .post(`https://apiV2.cricket.com/cricket/services/verifyotp`, {
        email: props.email,
        phnumber: props.number,
        checkphone: props.emailLogin ? false : true,
        otp: parseInt(otp),
      })
      .then(async (res) => {
        if (res.data.code === 200) {
          setItem('tokenData', res.data.token)
          setItem('userName', res.data.username || '')
          setItem('userEmail', res.data.email)
          setItem('userID', res.data.userID)
          setItem('userNumber', res.data.phnumber)
          setItem('photp', res.data.photp)
          setItem('emailotp', res.data.emailotp)
          setItem('mergeCheck', res.data.mergeCheck)
          if (props.step != 'checkemail' && props.step != 'checkphone') {
            !props.ShowLoginSucess ? setOtpResponse(res.data.code) : ''
          }

          CleverTap.initialize('LoginSuccess', {
            source: props.emailLogin ? 'EmailLogin' : 'MobileLogin',
            Platform: typeof window !== 'undefined' && getItem('Platform'),
            mobile: res.data.phnumber,
          })

          if (
            router.asPath == '/seasonfantasy/login' ||
            router.asPath == '/playtheodds/login'
          ) {
            const partnershipData = await axios.post(
              router.asPath == '/playtheodds/login'
                ? 'https://api.devcdc.com/cricket/cricket'
                : 'https://api.devcdc.com/cricket/cricket',
              {
                query: SEASON_FANTASY_LOGIN_AXIOS,
                variables: { token: res.data.token },
              },
            )

            if (
              (router.asPath == '/seasonfantasy/login' ||
                router.asPath == '/playtheodds/login') &&
              partnershipData.data.data.sessionFantasyToken.code === 200
            ) {
              if (router.asPath == '/seasonfantasy/login') {
                getItem('deviceType') === 'desktop' || props.mobile == false
                  ? (window.location.href = `https://seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`)
                  : (window.location.href = `https://m-seasonfantasy.cricket.com/ChooseFantasy.aspx/?uniqueuserID=${partnershipData.data.data.sessionFantasyToken.message}`)
              }
              if (router.asPath == '/playtheodds/login') {
                document.cookie =
                  'uniqueUserId=' +
                  partnershipData.data.data.sessionFantasyToken.message +
                  '; domain=' +
                  baseDomain +
                  '; expires=' +
                  expireAfter +
                  '; path=/'
                window.location.href = `http://playtheodds.cricket.com/?uniqueUserId=${partnershipData.data.data.sessionFantasyToken.message}`
              }
            }
          }

          if (
            (props.step == 'checkemail' || props.step == 'checkphone') &&
            !res.data.mergeCheck &&
            router.asPath !== '/seasonfantasy/login'
          ) {
            var data2 = await axios.post(process.env.API, {
              query: MERGE_PROFILE_AXIOS,
              variables: { token: getItem('tokenData') },
            })

            if (data2 && data2.data.data.mergeProfiles.code == 200) {
              setItem('mergeCheck', true)

              window.location = '/login'
            }
          } else {
            if (props.direct === 'true') {
              window.location.reload()
            }

            if (
              router.asPath !== '/seasonfantasy/login' ||
              router.asPath !== '/playtheodds/login'
            ) {
              !props.ShowLoginSucess
                ? setTimeout(() => {
                    props.setShowLogin
                      ? props.setShowLogin(false)
                      : props.navTo
                      ? navigate(props.navTo)
                      : (window.location = '/login')
                  }, 2000)
                : setTimeout(() => {
                    setOtpResponse(200)
                    props.setShowLogin(false)
                    props.navTo
                      ? navigate(props.navTo)
                      : window.location.reload()
                  }, 2000)
            }
          }
        } else {
          if (res.data.code == 203) {
            setErrorMsgCode(res.data.code)
          }
          setErrorMsg(res.data.Details)
        }
      })
      .catch((error) => {})
  }

  const reSend = () => {
    setOtpResponse('')
    setOtp1('')
    // setOtp2('');
    // setOtp3('');
    // setOtp4('');
    // setOtp5('');
    // setOtp6('');
    setReEnable(false)
    setErrorMsg('')

    axios
      .post(`https://apiV2.cricket.com/cricket/services/getotp`, {
        email: props.email,
        checkphone: props.emailLogin ? false : true,

        phnumber: props.number,
      })
      .then((res) => {
        setErrorMsg('')
        props.setSession(res.data.Details)
        res.data.Status === 'Error' ? setErrorMsg(res.data.Details) : ''

        if (res.data.code == 203) {
          setErrorMsgCode(res.data.code)
        }
      })
      .catch((error) => {})

    remainingTime == 0 ? setRemainingTime(30) : null
  }

  const handleKeyPress = (keycode, value, method, ref, backref) => {
    // if (e.keyCode == 13 && otp.length == 4) {

    if (keycode == 8 && value == '') backref.current.focus()
  }
  // const handleKeyPress = (e, refotp) => {
  // if (e.keyCode == 13 && otp.length == 4) {
  // handlePress();
  // }
  // };

  return (
    <>
      {otpResponse !== 200 ? (
        <div className="text-white">
          {!router.asPath?.includes('/login') && (
            <div
              className=" hidden justify-end right-0 z-9999"
              style={{ top: '-30px' }}
            >
              <ImageWithFallback
                height={18}
                width={18}
                loading="lazy"
                className="h-6 w-6 cursor-pointer rounded-full"
                alt="close icon"
                src={'/pngs/closeIcon.png'}
                onClick={() =>
                  props.onCLose === '/more' ||
                  router?.asPath == '/fantasy-research-center/upcoming'
                    ? props.setShowLogin(false)
                    : (window.location = '/login')
                }
              />
            </div>
          )}
          {props.emailLogin && (
            <div className="flex justify-center flex-col items-center ">
              <ImageWithFallback
                height={18}
                width={18}
                loading="lazy"
                src={'/pngs/email-icon-large-new.png'}
                alt="email"
              />
              <div className="mt-2 flex justify-center items-center flex-col">
                <div class="bg-red_10 h-1 w-24" />
                <div class="bg-red_10 h-1 mt-2 w-20" />
              </div>

              <div className="text-xs text-center mt-4 center p-1">
                We sent an OTP to the email {props.email}
              </div>
            </div>
          )}
          {!props.emailLogin && (
            <div className="font-bold text-xs text-center py-3">ENTER OTP</div>
          )}
          <div className="pa2 center ">
            <div className="flex justify-center ">
              <input
                id="name2"
                ref={refotp1}
                value={otp1}
                pattern="[0-9]*"
                onKeyDown={(e) =>
                  handleKeyPress(
                    e.keyCode,
                    e.target.value,
                    setOtp1,
                    refotp1,
                    refotp1,
                  )
                }
                type="tel"
                onChange={(e) =>
                  handleChange(e, e.target.value, setOtp1, refotp1, refotp1)
                }
                maxLength={6}
                className="text-center bg-gray-8 m-2 p-2 outline-none"
                aria-describedby="name-desc"
              />

              {/* <input
 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp2, refotp2, refotp1)}
 id='name2'
 ref={refotp2}
 maxLength={1}
 value={otp2}
 className='text-center w-2/12 border bg-gray-4 m-2'
 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp2, refotp3, refotp1)}
 pattern='[0-9]*'
 type='tel'
 aria-describedby='name-desc'
 /> */}

              {/* <input
 id='name2'
 maxLength={1}
 pattern='[0-9]*'
 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp3, refotp3, refotp2)}
 className='text-center w-2/12 border bg-gray-4 m-2'
 value={otp3}
 ref={refotp3}
 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp3, refotp4, refotp2)}
 type='tel'
 aria-describedby='name-desc'
 /> */}
              {/* 
 <input
 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp4, refotp4, refotp3)}
 id='name2'
 maxLength={1}
 className='text-center w-2/12 border bg-gray-4 m-2'
 ref={refotp4}
 pattern='[0-9]*'
 type='tel'
 value={otp4}
 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp4, refotp5, refotp3)}
 aria-describedby='name-desc'
 /> */}

              {/* <input
 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp5, refotp5, refotp4)}
 id='name2'
 maxLength={1}
 className='text-center w-2/12 border bg-gray-4 m-2'
 ref={refotp5}
 value={otp5}
 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp5, refotp6, refotp4)}
 pattern='[0-9]*'
 type='tel'
 aria-describedby='name-desc'
 /> */}

              {/* <input
 onKeyDown={(e) => handleKeyPress(e.keyCode, e.target.value, setOtp6, refotp6, refotp5)}
 id='name2'
 maxLength={1}
 className='text-center w-2/12 border bg-gray-4 m-2 '
 ref={refotp6}
 value={otp6}
 onChange={(e) => handleChange(e.target.keyCode, e.target.value, setOtp6, refotp6, refotp5)}
 pattern='[0-9]*'
 type='tel'
 aria-describedby='name-desc'
 /> */}
            </div>

            {errorMsjCode !== 203 && (
              <div className=" text-xs mt-3 text-center center p-1 cursor-pointer">
                Didn’t receive OTP ? Request for a new one in
              </div>
            )}
            <div className="text-xs mb-2 text-center center p-1 cursor-pointer">
              {remainingTime == 0 && (
                <span
                  className={resendEnable ? 'text-red' : 'text-red'}
                  onClick={resendEnable ? reSend : null}
                >
                  RESEND
                </span>
              )}
            </div>

            {/* show if otpResponse is empty */}
            {errorMsjCode !== 203 && remainingTime != 0 && (
              <div className=" f8 fw6 text-center white center pa1 mt3">
                {remainingTime} sec{' '}
              </div>
            )}

            {/* show if otpResponse is Non empty */}

            {otpResponse !== 200 && (
              <div className=" f7 fw6 text-center red center mt2 ">
                {errorMsj}
              </div>
            )}

            <div className="flex justify-center cursor-pointer">
              <div
                onClick={otp.length == 6 ? handlePress : null}
                className={otp.length == 6 ? enbaleButton : disbleButton}
              >
                {' '}
                CONFIRM
              </div>
            </div>
          </div>
        </div>
      ) : (
        <>
          {router.asPath !== '/seasonfantasy/login' ? (
            <div className="flex flex-col item-center mt4 ph5 justify-center">
              <div className=" f4 fw6 text-center mt4 red center pa1">
                <ImageWithFallback
                  height={68}
                  width={68}
                  loading="lazy"
                  src={'/pngs/loggedin-successfull.png'}
                  alt="success"
                />
              </div>

              <div className=" f4 fw6 text-center mt4 red center pa1">
                <div className="flex flex-col items-center justify-center text-center w-100">
                  <div> Login Successful</div>
                  <div class="bg-red_10 h-1 w-28 mt-2" />
                  <div class="bg-red_10 h-1 mt-2 w-20" />
                </div>
              </div>
            </div>
          ) : (
            <></>
          )}
        </>
      )}
    </>
  )
}
