'use client'
import React, { useState, useContext } from 'react'
import { MERGE_PROFILE } from '../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { LocalStorageContext } from '../layout'
import axios from 'axios'
import ImageWithFallback from '../commom/Image'
const URL ='https://apiv2.cricket.com/cricket' ?'https://apiv2.cricket.com/cricket' :'https://apiv2.cricket.com/cricket'

export default function Merge(props) {
  const { getItem } = useContext(LocalStorageContext)
  const token = getItem('tokenData')
  // const Empty = '/svgs/Empty.svg';
  const login = '/pngs/bigind.png'

  const re = /^[0-9\b]+$/

  const enbaleButton =
    'border-2 border-solid p-0.5 text-center cursor-pointer text-white cdcgr h-8 w-full '
  //const disbleButton = 'ba br2 fw5 f7 pa2 ph3 text-center cursor-pointer white mt2 bg-gray h2 w-50';

  const [mobNum, setMobNum] = useState('')
  // const [email, setEmail] = useState('');

  const [errorMsj, setErrorMsg] = useState('')

  const [MergeProfile, response] = useMutation(MERGE_PROFILE, {
    onCompleted: (responseProfile) => {
      if (responseProfile.mergeProfiles.code == 200) {
        // localStorage.setItem('userName', userName);

        props.setSHowMerge(false)
        props.setStep('profile')
        // setUpdateSucess(responseProfile.updateProfileInfo.message);
      }
    },
  })
  const OtpWithNum = () => {
    props.setEmail(getItem('userEmail'))
    axios
      .post(`https://apiV2.cricket.com/cricket/services/getotp`, {
        email: getItem('userEmail'),
        checkphone: true,

        phnumber: props.number,
      })
      .then((res) => {
        // const persons = res.data.Details;
        // const persons2 = res.data.Status;

        if (res.data.code == 200) {
          props.setEmail(getItem('userEmail'))

          props.setStep('checkphone')
          props.setSHowMerge(false)

          props.setSession(res.data.Details)
        } else {
          setErrorMsg(res.data.Details)
        }
      })
      .catch((error) => {
        console.log('error', error)
      })
  }

  const closeButton = () => {
    props.setStep('profile')
    props.setSHowMergeMSG('merge')
    props.setSHowMerge(false)
    //  props.step=="checkemail"?props.setEmail(""):props.setMobnum("")
  }

  const handleGetOtp = () => {
    props.setMobnum(getItem('userNumber'))
    axios
      .post(`https://apiV2.cricket.com/cricket/services/getotp`, {
        email: props.email,
        checkphone: false,

        phnumber: getItem('userNumber'),
      })
      .then((res) => {
        // const persons = res.data.Details;
        // const persons2 = res.data.Status;

        if (res.data.code == 200) {
          props.setMobnum(getItem('userNumber'))

          props.setStep('checkemail')
          props.setSHowMerge(false)

          props.setSession(res.data.Details)
        } else {
          setErrorMsg(res.data.Details)
        }
      })
      .catch((error) => {
        console.log('error', error)
      })
  }

  // const handleKeyPress = (e) => {
  //   if (e.keyCode == 13 && mobNum.length === 10) {
  //     handlePress();
  //   }
  // };
  // const handleChange = (value) => {
  //   setErrorMsg('');
  //   value === '' || re.test(value) ? setMobNum(value) : null;
  // };

  const handlePress = () => {
    MergeProfile({
      variables: {
        token: token,
      },
    })
  }
  // const updateName = () => {

  // };

  return (
    <div className="text-white">
      {props.showMergeMSG == 'merge' && (
        <div
          className=" flex justify-end right-0 z-9999"
          style={{ top: '-30px' }}
        >
          <ImageWithFallback
            height={18}
            width={18}
            loading="lazy"
            className="h-5 w-5   rounded-full cursor-pointer"
            alt="close icon"
            src={'/pngs/closeIcon.png'}
            onClick={() =>
              props.onCLose === '/more'
                ? (props.setShowLogin(false), props.setSHowMerge(false))
                : (props.setStep('profile'), props.setSHowMerge(false))
            }
          />
        </div>
      )}

      <div className="p-2  f8 fw6  text-center  center pa1">
        {props.modalText}
      </div>
      <div className="p-4  center bg-gray -mt-3 ">
        <div className=" mt-3 flex flex-col items-center justify-center">
          <ImageWithFallback
            height={18}
            width={18}
            loading="lazy"
            src={'/pngs/origami-new.png'}
            alt=""
            className="mx-1"
          />
          {props.showMergeMSG == 'merge' ? (
            <>
              <div className="flex  flex-col items-center justify-center text-center  center mt2 f6 ">
                <div>
                  This{' '}
                  {props.step == 'checkemail' ? ' email' : ' mobile number '} is
                  linked to another user profile,{' '}
                </div>

                <div>Do you wish to merge both the </div>
                <div>account profiles?</div>
              </div>

              <div className="flex  fw5 items-center justify-center mt4 center mt2 f7 tc-l mb2">
                By clicking Confirm, you agree to the Terms and Conditions of
                Cricket.com
              </div>
            </>
          ) : (
            <div className="tc">
              {' '}
              This
              {props.step == 'checkemail' ? ' email' : ' mobile number '}{' '}
              already registered as an account. Please choose a separate{' '}
              {props.step == 'checkemail' ? 'email' : 'mobile number '}
            </div>
          )}
          {props.showMergeMSG == 'merge' ? (
            <div
              onClick={
                props.step == 'checkemail'
                  ? handleGetOtp
                  : props.step == 'checkphone'
                  ? OtpWithNum
                  : handlePress
              }
              className={enbaleButton}
            >
              Confirm
            </div>
          ) : (
            <div onClick={closeButton} className={enbaleButton}>
              Close
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
