// 'use client'
// import React, { useEffect, useRef, useState, useContext } from 'react'
// import { useQuery, useMutation } from '@apollo/react-hooks'
// import axios from 'axios'layer
// // import Head from 'next/head'
// import { useRouter } from 'next/navigation'
// import ImageWithFallback from '../commom/Image'
// import { ACCOUNT_CHECK_AXIOS } from '../../api/queries'
// import CleverTap from 'clevertap-react'
// import { UPDATE_PROFILE } from '../../api/queries'
// import { LocalStorageContext } from '../layout'
// import Head from 'next/head'

// // import Facebook  from '../../public/svgs/facebook.svg'
// export default function Profile(props) {
//   const { setItem, getItem } = useContext(LocalStorageContext)

//   const [checkStat, setCheckStat] = useState()

//   const [currentError, setCurrentError] = useState('')

//   const [nameupdateDone, setNameupdate] = useState(false)

//   const re2 = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
//   // const backIconWhite = '/svgs/backIconWhite.svg';
//   let router = useRouter()
//   let navigate = router.push
//
//   const [updateSuccess, setUpdateSucess] = useState('')
//   const re = /^[0-9\b]+$/
//   const [userName, setUserName] = useState(
//     getItem('userName') === 'null' ? '' : getItem('userName'),
//   )

//   // const [userEmailUpdate, setUserEmail] = useState('');
//   const token = getItem('tokenData')

//   const enbaleButton =
//     'border-2 border-gray rounded-md border-solid text-white bg-gray-8 border-green border-2 center fw5 f7 p-2 py-7 text-center cursor-pointer text-white mt-2 bg-gray h-2 w-32'
//   const disbleButton =
//     'bg-gray-8 rounded-md border-black text-white fw5 f7 px-2 border-2 px-4 py-1 mt-4 mx-2 cursor-pointer w-30'

//   const [updateProfile, response] = useMutation(UPDATE_PROFILE, {
//     onCompleted: (responseProfile) => {
//       if (responseProfile.updateProfileInfo.code == 200) {
//         setItem('userName', userName)

//         setUpdateSucess(responseProfile.updateProfileInfo.message)
//         setNameupdate(false)
//       }
//     },
//   })
//   const updateName = () => {
//     updateProfile({
//       variables: {
//         username: userName,
//         token: token,
//       },
//     })
//   }

//   // const signOut = () => {
//   //   CleverTap.initialize('Signout', {
//   //     Source: 'Profile',
//   //     Platform: localStorage ? localStorage.Platform : ''
//   //   });
//   //   setCheckStat('2');
//   //   props.setStep('Getotp');
//   //   props.setLoginStatus(false);
//   //   localStorage.removeItem('tokenData');
//   //   localStorage.removeItem('userName');
//   //   localStorage.removeItem('userNumber');
//   //   localStorage.removeItem('userEmail');
//   //   localStorage.removeItem('mergecheck');
//   //   localStorage.removeItem('photp');
//   //   localStorage.removeItem('emailotp');
//   // };

//   const verifyEmail = async () => {
//     props.setEmailLogin(true)
//     const checkAccount = await axios.post(process.env.API, {
//       query: ACCOUNT_CHECK_AXIOS,
//       variables: { account: props.email },
//     })

//     if (
//       checkAccount &&
//       checkAccount.data.data.accountCheck.code == 200 &&
//       !checkAccount.data.data.accountCheck.isMerged
//     ) {
//       // props.setStep("checkemail")

//       if (checkAccount.data.data.accountCheck.isAccountExist) {
//         props.setSHowMerge(true)
//         props.setStep('checkemail')
//       } else {
//         axios
//           .post(`https://api.devcdc.com/cricket/services/getotp`, {
//             email: props.email,
//             checkphone: false,

//             phnumber: getItem('userNumber'),
//           })
//           .then((res) => {
//             const persons = res.data.Details
//             const persons2 = res.data.Status

//             if (res.data.code == 200) {
//               props.setMobnum(getItem('userNumber'))
//               props.setEmailLogin(true)
//               props.setStep('checkemail')
//             } else {
//               setCurrentError(res.data.Details)
//             }
//           })
//           .catch((error) => {
//
//           })
//       }
//     } else {
//       props.setSHowMerge(true)
//       props.setSHowMergeMSG('email')
//       props.setStep('checkemail')
//     }
//   }

//   const verifyPhone = async () => {
//     const checkAccountPhone = await axios.post(process.env.API, {
//       query: ACCOUNT_CHECK_AXIOS,
//       variables: { account: props.number },
//     })

//     if (
//       checkAccountPhone &&
//       checkAccountPhone.data.data.accountCheck.code == 200 &&
//       !checkAccountPhone.data.data.accountCheck.isMerged
//     ) {
//       if (checkAccountPhone.data.data.accountCheck.isAccountExist) {
//         props.setSHowMerge(true)
//         props.setStep('checkphone')
//       } else {
//         // props.setStep('checkphone');
//         axios
//           .post(`https://api.devcdc.com/cricket/services/getotp`, {
//             email: getItem('userEmail'),
//             checkphone: true,

//             phnumber: props.number,
//           })
//           .then((res) => {
//             // const persons = res.data.Details;
//             // const persons2 = res.data.Status;

//             if (res.data.code == 200) {
//               props.setEmailLogin(false)
//               props.setEmail(getItem('userEmail'))
//               props.setStep('checkphone')
//             } else {
//               setCurrentError(res.data.Details)
//             }
//           })
//           .catch((error) => {
//
//           })
//       }
//     } else {
//       props.setStep('checkphone')
//       props.setSHowMerge(true)
//       props.setSHowMergeMSG('phone')
//     }
//   }

//   const handleChangeMObile = (value) => {
//     value === '' || re.test(value) ? props.setMobnum(value) : null
//   }

//   return (
//     <>
//       {/* <Head>
//         <title>
//           Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com
//         </title>
//         <meta
//           name="description"
//           content="Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com"
//         />
//         <h1>Stadiums</h1>
//         <meta
//           property="og:title"
//           content="Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com"
//         />
//         <meta
//           property="og:description"
//           content="Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com"
//         />
//         <meta property="og:url" content="https://www.cricket.com/stadiums" />
//         <meta
//           name="twitter:title"
//           content="Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com"
//         />
//         <meta
//           name="twitter:description"
//           content="Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com"
//         />
//         <meta name="twitter:url" content="https://www.cricket.com/stadiums" />

//         <meta name="Access-Control-Allow-Headers" value="accesstoken" />
//         <link rel="canonical" href="https://www.cricket.com/stadiums" />
//       </Head> */}
//       <div
//         className=" hidescroll

//        text-white min-vh-50"
//       >
//         <div className="fixed  top-0 left-0 right-0 flex gap-2 items-center p-2  text-white text-base md:hidden lg:hidden">
//           {/* <img
//           className='mr2  h1 cursor-pointer'
//           onClick={() => (props.onCLose === '/more' ? props.setShowLogin(false) : window.history.back())}
//           src={'/svgs/backIconWhite.svg'}
//           alt='back icon'
//         /> */}
//           <div
//             onClick={() =>
//               props.onCLose === '/more'
//                 ? props.setShowLogin(false)
//                 : window.history.back()
//             }
//             className=" p-1 cursor-pointer   bg-gray rounded-md"
//           >
//             <svg width="30" focusable="false" viewBox="0 0 24 24">
//               <path
//                 fill="#ffff"
//                 d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
//               ></path>
//               <path fill="none" d="M0 0h24v24H0z"></path>
//             </svg>
//           </div>
//           <span className="fw6 f5 pl3 white">Profile </span>
//         </div>

//         <div className="flex justify-end hidden ">
//           <ImageWithFallback
//             height={18}
//             width={18}
//             loading="lazy"
//             className="h-5 w-5   rounded-full cursor-pointer"
//             alt="close icon"
//             src={'/pngs/closeIcon.png'}
//             onClick={() =>
//               props.onCLose === '/more'
//                 ? props.setShowLogin(false)
//                 : (window.location = props.onCLose ? props.onCLose : '/')
//             }
//           />
//         </div>
//         <div className="relative flex items-center justify-center flex-col w-full">
//           <div className="h-28 w-20 flex items-center justify-center absolute -top-20">
//             <ImageWithFallback
//               height={24}
//               width={24}
//               loading="lazy"
//               className="absolute h-16 w-16"
//               src={'/pngs/loggedin-new.png'}
//               alt="success"
//               srcset=""
//             />
//           </div>

//           <div className="w-full p-4">
//             <div className="flex justify-center items-center text-red f7">
//               {currentError}
//             </div>
//             <div className="measure center">
//               <div className="flex items-center mb-2">
//                 <div className="w-1/12 mt-3">
//                   {' '}
//                   <ImageWithFallback
//                     height={10}
//                     width={10}
//                     loading="lazy"
//                     src={'/svgs/username.svg'}
//                     className="h-8 w-5"
//                     alt="user"
//                   />
//                 </div>
//                 <div className="w-9/12">
//                   {' '}
//                   <input
//                     placeholder="username"
//                     value={userName}
//                     // onBlur={updateName}
//                     onChange={(e) =>
//                       setUserName(
//                         e.target.value.replace(/ +/g, ' '),
//                         setNameupdate(true),
//                       )
//                     }
//                     id="name"
//                     className="border-input bg-gray input-reset px-2 pb-1 border-b border-black ml-2 mt-4 w-full "
//                     type="text"
//                     aria-describedby="name-desc"
//                   />
//                 </div>

//                 <div className="w-2/12 w-10-l flex items-center input-reset justify-center border-b-black">
//                   {' '}
//                   <ImageWithFallback
//                     height={18}
//                     width={18}
//                     loading="lazy"
//                     src={'/svgs/pencil-edit-button.svg'}
//                     alt="edit"
//                     className="h-8 w-4"
//                   />
//                 </div>
//               </div>

//               <div className="flex items-center mb- w-full">
//                 <div className="w-1/12">
//                   {' '}
//                   <ImageWithFallback
//                     height={18}
//                     width={18}
//                     loading="lazy"
//                     src={'/svgs/phone.svg'}
//                     className=" h1 w-5"
//                     alt="phone"
//                   />
//                 </div>

//                 <div className="flex ml-2 w-9/12">
//                   <input
//                     id="name"
//                     value={'+91'}
//                     className="w-10 w-10-l bg-gray  input-reset py-2 border-b border-black"
//                   />

//                   <input
//                     onChange={(e) => {
//                       handleChangeMObile(e.target.value), setCurrentError('')
//                     }}
//                     id="name"
//                     maxLength={10}
//                     readOnly={getItem('photp') == 'true'}
//                     value={
//                       getItem('userNumber')
//                         ? `${
//                             getItem('userNumber') === 'null'
//                               ? props.number
//                               : getItem('userNumber')
//                           }`
//                         : props.number
//                     }
//                     className="w-9/12  bg-gray input-reset pv2 border-b border-black  "
//                     pattern="[0-9]*"
//                     type="tel"
//                     aria-describedby="name-desc"
//                     placeholder="Mobile Number"
//                   />
//                 </div>
//                 {getItem('photp') == 'false' ? (
//                   <div
//                     onClick={props.number.length == 10 ? verifyPhone : null}
//                     className=" w-2/12 px-1 flex items-center input-reset justify-center border-b-black"
//                     style={
//                       props.number.length == 10
//                         ? { color: '#DD5353' }
//                         : { color: '#708090' }
//                     }
//                   >
//                     verify
//                   </div>
//                 ) : (
//                   <div className="">
//                     {' '}
//                     <ImageWithFallback
//                       height={18}
//                       width={18}
//                       loading="lazy"
//                       className="h-4 w-5"
//                       src={'/pngs/verified.png'}
//                       alt="verify"
//                     />
//                   </div>
//                 )}
//               </div>

//               <div className="flex items-center my-2">
//                 <div className="w-1/12">
//                   <ImageWithFallback
//                     height={18}
//                     width={18}
//                     loading="lazy"
//                     src={'/svgs/email.svg'}
//                     className=" h1 w-5"
//                     alt="email"
//                   />
//                 </div>

//                 <div className="w-9/12 ml-1">
//                   {' '}
//                   <input
//                     value={
//                       getItem('emailotp') == 'false'
//                         ? props.email
//                         : getItem('userEmail')
//                     }
//                     // onBlur={updateName}
//                     onChange={(e) =>
//                       props.setEmail(e.target.value.replace(/ +/g, ' '))
//                     }
//                     id="name"
//                     placeholder="email"
//                     readOnly={getItem('emailotp') == 'true'}
//                     className=" border-input bg-gray input-reset pb-1 border-b border-black border-0 w-full"
//                     type="text"
//                     aria-describedby="name-desc"
//                   />
//                 </div>

//                 {getItem('emailotp') == 'false' ? (
//                   <div
//                     onClick={
//                       re2.test(String(props.email).toLowerCase())
//                         ? verifyEmail
//                         : null
//                     }
//                     className="text-xs f8 ttu center cursor-pointer w-1/12 ml-1 "
//                     style={
//                       re2.test(String(props.email).toLowerCase())
//                         ? { color: '#DD5353' }
//                         : { background: 'transparent' }
//                     }
//                   >
//                     verify
//                   </div>
//                 ) : (
//                   // <img className=' w-2/12 h1 ml1 pa1 w1 br-100 bg-green' src={'/svgs/squarecheck.svg'} alt='' />
//                   <ImageWithFallback
//                     height={18}
//                     width={18}
//                     loading="lazy"
//                     className="h-4 w-4"
//                     src={'/pngs/verified.png'}
//                     alt="verify"
//                   />
//                 )}
//               </div>
//             </div>
//           </div>

//           <div
//             className={
//               (userName !== '' && getItem('userName') !== userName) ||
//               nameupdateDone
//                 ? enbaleButton
//                 : disbleButton
//             }
//             onClick={
//               userName !== '' && getItem('userName') !== userName && userName
//                 ? updateName
//                 : null
//             }
//           >
//             Submit
//           </div>

//           {/* <div className='pa2 text-xs font-semibold mt-2  tc   center pa1'>Love CRICKET.COM? Share With Friends</div>
//       <div className='pa2   tc flex justify-center  center pa1'>
//         <a target='_blank ' href={'https://www.facebook.com/sharer/sharer.php?u=http://cricket.com'}>
//           <img src={'/svgs/fb.svg'} className='w-10 h-8 m-2 ' alt='' />
//         </a>

//         <a target='_blank' href={'https://twitter.com/intent/tweet?url=http://cricket.com'}>
//           <img src={'/svgs/colorTwiter.svg'} className='w-10 h-8 m-2' alt='' />
//         </a>

//         <a target='_blank' href='https://api.whatsapp.com/send?text=http://cricket.com'>
//           <img src={'/svgs/whatsapp.svg'} className='w-10 h-8 m-2' alt='' />
//         </a>
//       </div>  */}
//         </div>
//       </div>
//     </>
//   )
// }

'use client'
import React, { useEffect, useRef, useState, useContext } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import axios from 'axios'
import Head from 'next/head'
import { useRouter } from 'next/navigation'
import ImageWithFallback from '../commom/Image'
import { ACCOUNT_CHECK_AXIOS } from '../../api/queries'
import CleverTap from 'clevertap-react'
import { UPDATE_PROFILE } from '../../api/queries'
import { LocalStorageContext } from '../layout'

// import Facebook  from '../../public/svgs/facebook.svg'
export default function Profile(props) {
  const { setItem, getItem } = useContext(LocalStorageContext)

  const [checkStat, setCheckStat] = useState()

  const [currentError, setCurrentError] = useState('')

  const [nameupdateDone, setNameupdate] = useState(false)

  const re2 = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  // const backIconWhite = '/svgs/backIconWhite.svg';
  let router = useRouter()
  let navigate = router.push

  const [updateSuccess, setUpdateSucess] = useState('')
  const re = /^[0-9\b]+$/
  const [userName, setUserName] = useState(
    getItem('userName') === 'null' ? '' : getItem('userName'),
  )

  // const [userEmailUpdate, setUserEmail] = useState('');
  const token = getItem('tokenData')

  const enbaleButton =
    'bg-gray-8 rounded-md border-green border text-white px-2 border-2 px-4 py-1 mt-4 mx-2 cursor-pointer w-30'
  const disbleButton =
    'bg-gray-8 rounded-md border-gray text-gray px-2 border-2 px-4 py-1 mt-4 mx-2 cursor-pointer w-30'

  const [updateProfile, response] = useMutation(UPDATE_PROFILE, {
    onCompleted: (responseProfile) => {
      if (responseProfile.updateProfileInfo.code == 200) {
        setItem('userName', userName)

        setUpdateSucess(responseProfile.updateProfileInfo.message)
        setNameupdate(false)
      }
    },
  })
  const updateName = () => {
    updateProfile({
      variables: {
        username: userName,
        token: token,
      },
    })
  }

  // const signOut = () => {
  //   CleverTap.initialize('Signout', {
  //     Source: 'Profile',
  //     Platform: localStorage ? localStorage.Platform : ''
  //   });
  //   setCheckStat('2');
  //   props.setStep('Getotp');
  //   props.setLoginStatus(false);
  //   localStorage.removeItem('tokenData');
  //   localStorage.removeItem('userName');
  //   localStorage.removeItem('userNumber');
  //   localStorage.removeItem('userEmail');
  //   localStorage.removeItem('mergecheck');
  //   localStorage.removeItem('photp');
  //   localStorage.removeItem('emailotp');
  // };

  const verifyEmail = async () => {
    props.setEmailLogin(true)
    const checkAccount = await axios.post(process.env.API, {
      query: ACCOUNT_CHECK_AXIOS,
      variables: { account: props.email },
    })

    if (
      checkAccount &&
      checkAccount.data.data.accountCheck.code == 200 &&
      !checkAccount.data.data.accountCheck.isMerged
    ) {
      // props.setStep("checkemail")

      if (checkAccount.data.data.accountCheck.isAccountExist) {
        props.setSHowMerge(true)
        props.setStep('checkemail')
      } else {
        axios
          .post(`https://apiV2.cricket.com/cricket/services/getotp`, {
            email: props.email,
            checkphone: false,

            phnumber: getItem('userNumber'),
          })
          .then((res) => {
            const persons = res.data.Details
            const persons2 = res.data.Status

            if (res.data.code == 200) {
              props.setMobnum(getItem('userNumber'))
              props.setEmailLogin(true)
              props.setStep('checkemail')
            } else {
              setCurrentError(res.data.Details)
            }
          })
          .catch((error) => {})
      }
    } else {
      props.setSHowMerge(true)
      props.setSHowMergeMSG('email')
      props.setStep('checkemail')
    }
  }

  const verifyPhone = async () => {
    const checkAccountPhone = await axios.post(process.env.API, {
      query: ACCOUNT_CHECK_AXIOS,
      variables: { account: props.number },
    })

    if (
      checkAccountPhone &&
      checkAccountPhone.data.data.accountCheck.code == 200 &&
      !checkAccountPhone.data.data.accountCheck.isMerged
    ) {
      if (checkAccountPhone.data.data.accountCheck.isAccountExist) {
        props.setSHowMerge(true)
        props.setStep('checkphone')
      } else {
        // props.setStep('checkphone');
        axios
          .post(`https://apiV2.cricket.com/cricket/services/getotp`, {
            email: getItem('userEmail'),
            checkphone: true,

            phnumber: props.number,
          })
          .then((res) => {
            // const persons = res.data.Details;
            // const persons2 = res.data.Status;

            if (res.data.code == 200) {
              props.setEmailLogin(false)
              props.setEmail(getItem('userEmail'))
              props.setStep('checkphone')
            } else {
              setCurrentError(res.data.Details)
            }
          })
          .catch((error) => {})
      }
    } else {
      props.setStep('checkphone')
      props.setSHowMerge(true)
      props.setSHowMergeMSG('phone')
    }
  }

  const handleChangeMObile = (value) => {
    value === '' || re.test(value) ? props.setMobnum(value) : null
  }

  return (
    <div
      className=" hidescroll  
        
        
       text-white min-vh-50"
    >
      {/* <>
        <title>
          Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com
        </title>
        <meta
          name="description"
          content="Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com"
        />
        <h1>Stadiums</h1>
        <meta
          property="og:title"
          content="Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com"
        />
        <meta
          property="og:description"
          content="Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com"
        />
        <meta property="og:url" content="https://www.cricket.com/stadiums" />
        <meta
          name="twitter:title"
          content="Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com"
        />
        <meta
          name="twitter:description"
          content="Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com"
        />
        <meta name="twitter:url" content="https://www.cricket.com/stadiums" />

        <meta name="Access-Control-Allow-Headers" value="accesstoken" />
        <link rel="canonical" href="https://www.cricket.com/stadiums" />
      </> */}

      <div className="fixed  top-0 left-0 right-0 flex gap-2 items-center p-2  text-white text-base md:hidden lg:hidden">
        {/* <img
          className='mr2  h1 cursor-pointer'
          onClick={() => (props.onCLose === '/more' ? props.setShowLogin(false) : window.history.back())}
          src={'/svgs/backIconWhite.svg'}
          alt='back icon'
        /> */}
        <div
          onClick={() =>
            props.onCLose === '/more'
              ? props.setShowLogin(false)
              : window.history.back()
          }
          className=" p-1 cursor-pointer   bg-gray rounded-md"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
        <span className="fw6 f5 pl3 white">Profile </span>
      </div>

      <div className="flex justify-end hidden ">
        <ImageWithFallback
          height={18}
          width={18}
          loading="lazy"
          className="h-5 w-5   rounded-full cursor-pointer"
          alt="close icon"
          src={'/pngs/closeIcon.png'}
          onClick={() =>
            props.onCLose === '/more'
              ? props.setShowLogin(false)
              : (window.location = props.onCLose ? props.onCLose : '/')
          }
        />
      </div>
      <div className="relative flex items-center justify-center flex-col w-full">
        <div className="h-28 w-20 flex items-center justify-center absolute -top-24">
          <ImageWithFallback
            height={24}
            width={24}
            loading="lazy"
            className="absolute h-16 w-16"
            src={'/pngs/loggedin-new.png'}
            alt="success"
            srcset=""
          />
        </div>

        <div className="w-full p-4">
          <div className="flex justify-center items-center text-red f7">
            {currentError}
          </div>
          <div className="measure center">
            <div className="flex items-center mb-2">
              <div className="w-1/12 mt-3">
                {' '}
                <ImageWithFallback
                  height={10}
                  width={10}
                  loading="lazy"
                  src={'/svgs/username.svg'}
                  className="h-8 w-5"
                  alt="user"
                />
              </div>
              <div className="w-9/12">
                {' '}
                <input
                  placeholder="username"
                  value={userName}
                  // onBlur={updateName}
                  onChange={(e) =>
                    setUserName(
                      e.target.value.replace(/ +/g, ' '),
                      setNameupdate(true),
                    )
                  }
                  id="name"
                  className="border-input bg-gray input-reset px-2 outline-none pb-1 border-b border-black ml-2 mt-4 w-full "
                  type="text"
                  aria-describedby="name-desc"
                />
              </div>

              <div className="w-2/12 w-10-l flex items-center input-reset justify-center border-b-black">
                {' '}
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  src={'/svgs/pencil-edit-button.svg'}
                  alt="edit"
                  className="h-8 w-4"
                />
              </div>
            </div>

            <div className="flex items-center mb- w-full">
              <div className="w-1/12">
                {' '}
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  src={'/svgs/phone.svg'}
                  className=" h1 w-5"
                  alt="phone"
                />
              </div>

              <div className="flex ml-2 w-9/12">
                <input
                  id="name"
                  value={'+91'}
                  className="w-10 w-10-l bg-gray  input-reset py-2 border-b border-black"
                />

                <input
                  onChange={(e) => {
                    handleChangeMObile(e.target.value), setCurrentError('')
                  }}
                  id="name"
                  maxLength={10}
                  readOnly={getItem('photp') == 'true'}
                  value={
                    getItem('userNumber')
                      ? `${
                          getItem('userNumber') === 'null'
                            ? props.number
                            : getItem('userNumber')
                        }`
                      : props.number
                  }
                  className="w-9/12  bg-gray input-reset pv2 border-b border-black  "
                  pattern="[0-9]*"
                  type="tel"
                  aria-describedby="name-desc"
                  placeholder="Mobile Number"
                />
              </div>
              {getItem('photp') == 'false' ? (
                <div
                  onClick={props.number.length == 10 ? verifyPhone : null}
                  className=" w-2/12 px-1 flex items-center input-reset justify-center border-b-black"
                  style={
                    props.number.length == 10
                      ? { color: '#DD5353' }
                      : { color: '#708090' }
                  }
                >
                  verify
                </div>
              ) : (
                <div className="">
                  {' '}
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    className="h-4 w-5"
                    src={'/pngs/verified.png'}
                    alt="verify"
                  />
                </div>
              )}
            </div>

            <div className="flex items-center my-2">
              <div className="w-1/12">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  src={'/svgs/email.svg'}
                  className=" h1 w-5"
                  alt="email"
                />
              </div>

              <div className="w-9/12 ml-1">
                {' '}
                <input
                  value={
                    getItem('emailotp') == 'false'
                      ? props.email
                      : getItem('userEmail')
                  }
                  // onBlur={updateName}
                  onChange={(e) =>
                    props.setEmail(e.target.value.replace(/ +/g, ' '))
                  }
                  id="name"
                  placeholder="email"
                  readOnly={getItem('emailotp') == 'true'}
                  className=" border-input bg-gray input-reset pb-1 border-b border-black border-0 w-full"
                  type="text"
                  aria-describedby="name-desc"
                />
              </div>

              {getItem('emailotp') == 'false' ? (
                <div
                  onClick={
                    re2.test(String(props.email).toLowerCase())
                      ? verifyEmail
                      : null
                  }
                  className="text-xs f8 ttu center cursor-pointer w-1/12 ml-1 "
                  style={
                    re2.test(String(props.email).toLowerCase())
                      ? { color: '#DD5353' }
                      : { background: 'transparent' }
                  }
                >
                  verify
                </div>
              ) : (
                // <img className=' w-2/12 h1 ml1 pa1 w1 br-100 bg-green' src={'/svgs/squarecheck.svg'} alt='' />
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  className="h-4 w-4"
                  src={'/pngs/verified.png'}
                  alt="verify"
                />
              )}
            </div>
          </div>
        </div>

        <div
          className={
            (userName !== '' && getItem('userName') !== userName) ||
            nameupdateDone
              ? enbaleButton
              : disbleButton
          }
          onClick={
            userName !== '' && getItem('userName') !== userName && userName
              ? updateName
              : null
          }
        >
          Submit
        </div>
        {/* <div className='pa2 text-xs font-semibold mt-2  tc   center pa1'>Love CRICKET.COM? Share With Friends</div> 
      <div className='pa2   tc flex justify-center  center pa1'>
        <a target='_blank ' href={'https://www.facebook.com/sharer/sharer.php?u=http://cricket.com'}>
          <img src={'/svgs/fb.svg'} className='w-10 h-8 m-2 ' alt='' />
        </a>

        <a target='_blank' href={'https://twitter.com/intent/tweet?url=http://cricket.com'}>
          <img src={'/svgs/colorTwiter.svg'} className='w-10 h-8 m-2' alt='' />
        </a>

        <a target='_blank' href='https://api.whatsapp.com/send?text=http://cricket.com'>
          <img src={'/svgs/whatsapp.svg'} className='w-10 h-8 m-2' alt='' />
        </a>
      </div>  */}
      </div>
    </div>
  )
}
