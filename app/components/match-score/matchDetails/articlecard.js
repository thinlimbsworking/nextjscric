import React from 'react'
import LazyImageLoader, { trackScroll } from '../../commom/lazyload'
import { format } from 'date-fns'

{
  /* <div className="dn db-l"></div> */
}
const ArticleCard = ({ isFeatured, isDetails, article, ...props }) => {
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    let sec = Math.floor((distance % (1000 * 60)) / 1000)

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago'
    return result
  }

  return (
    <div className="md:py-1 md:px-2">
      <div className="md:block lg:block hidden">
        <div className="flex pl-1 pr-3 pb-2">
          <div className=" w-1/2 br1 flex relative cover bg-top h-56 rounded-md">
            {/* <LazyImageLoader
              className=''
              src={`${article.bg_image_safari}?auto=compress&dpr=2&w=1&h=200`}
            /> */}
            <img
              className="object-cover object-top px-1 py-2 rounded-3xl"
              style={{ width: '100%' }}
              src={`${article.bg_image_safari}?auto=compress&dpr=2&w=400&h=224`}
            />

            <div className="px-2 rounded-r-lg ml-2 rounded-md absolute flex py-0.5 bottom-3 cdcgr text-white bg-basered dark:bg-green-6 text-sm font-medium ">
              {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
            </div>
          </div>

          <div className="px-2 pt-2 w-1/2 ">
            <h2 className="text-md lg:text-black md:text-black font-bold tracking-wider line-clamp ">
              {article && (article.title || '')}
            </h2>
            <div
              className="text-sm lg:text-gray-9 md:text-gray-9 xl:text-gray-9 font-normal mt-1 line-clamp"
              style={{ WebkitLineClamp: 3 }}
            >
              {article && (article.description || '')}
            </div>
            <div className="mt-2 flex justify-between items-center">
              <div className="flex items-center w-2/3">
                <img
                  className={`${
                    props.authorPic ? ' h1 w1 br-100 ' : 'h-5 w-5'
                  }`}
                  src={`${
                    props.authorPic
                      ? props.authorPic
                      : '/pngsV2/UserProfile.png'
                  }`}
                  alt="user"
                />
                <div className="text-gray-2 text-xs italic pl-1 font-medium">
                  {article &&
                    article.author &&
                    (article.author.split(',').join(', ') || '')}
                </div>
              </div>
              <div className="w-1/3 text-right text-sm font-medium text-gray-2">
                {Date.now() - parseInt(article.createdAt) > 8640000
                  ? article && format(+article.createdAt, 'dd MMM yyyy')
                  : getUpdatedTime(article.createdAt)}
              </div>
            </div>
          </div>
        </div>
        <div className="dark:bg-black bg-[#E2E2E2] h-[1px]  "></div>
      </div>

      {/* ---------------------------------mobile------------------------------- */}
      <div className="md:hidden lg:hidden">
        {isFeatured ? (
          <div className="pb-1">
            <div className="py-1 px-2">
              <div className="rounded  relative w-full">
                {/* <LazyImageLoader
                className='object-cover object-top h-40'
                style={{width:'100%'}}
                src={`${article.bg_image_safari}?auto=compress&dpr=2&w=1&h=160`}
              /> */}
                <img
                  className="object-cover object-top h-44"
                  style={{ width: '100%' }}
                  src={`${article.bg_image_safari}?auto=compress&dpr=2&w=360&h=176`}
                />

                <div className="px-2 rounded-r-md absolute flex py-0.5 bottom-2 cdcgr text-black bg-green-6 text-sm">
                  {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
                </div>
              </div>

              <div className=" mt-1">
                <div className="font-semibold text-md line-clamp leading-snug">
                  {article && (article.title || '')}
                </div>
                <div
                  className="font-normal line-clamp mt-2 text-xs leading-tight"
                  style={{ WebkitLineClamp: 3 }}
                >
                  {article && (article.description || '')}
                </div>
                <div className="mt-2 flex items-center">
                  <img
                    src="/pngsV2/UserProfile.png"
                    alt="user"
                    className="w-5 h-5"
                  />
                  <span className="text-gray-2 text-xs font-medium pl-1 italic">
                    {article &&
                      article.author &&
                      (article.author.split(',').join(', ') || '')}
                  </span>
                </div>
                <div className="mt-2 text-gray-2 text-xs font-medium">
                  {' '}
                  {Date.now() - parseInt(article.createdAt) > 8640000
                    ? article && format(+article.createdAt, 'dd MMM yyyy')
                    : getUpdatedTime(article.createdAt)}
                </div>
              </div>
            </div>
            <div className="dark:bg-black bg-[#E2E2E2] w-full h-[1px] "></div>
          </div>
        ) : !isDetails ? (
          <div className="pb-1">
            <div className="p-1 flex items-start">
              <div className="relative w-40">
                {article.sm_image_safari ? (
                  <img
                    className="object-cover object-top h-28 w-36 rounded"
                    src={`${article.sm_image_safari}?auto=compress&dpr=2&w=144&h=144`}
                  />
                ) : (
                  <div className="bg-gray-2 h-36 w-36"></div>
                )}

                <div className="px-2 rounded-r-lg absolute flex py-0.5 bottom-2 cdcgr  text-black bg-green-6 text-xs">
                  {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
                </div>
              </div>
              <div className=" w-3/5">
                <div className="text-xs font-semibold mt-1 line-clamp ">
                  {article && (article.title || '')}
                </div>
                <div className="text-xs mt-1 line-clamp-2">
                  {article && (article.description || '')}
                </div>

                <div className="flex justify-between">
                  <div className="flex items-center w-6/12 justify-start mt-1">
                    <div
                      className={`${
                        props.authorPic
                          ? 'flex items-center justify-center w-10 h-10  '
                          : 'flex items-center justify-center w-5 h-5'
                      }`}
                    >
                      {' '}
                      <img
                        className={`${
                          props.authorPic ? ' h-1 w-1 rounded-full ' : ''
                        }`}
                        src={`${
                          props.authorPic
                            ? props.authorPic
                            : '/pngsV2/UserProfile.png'
                        }`}
                        alt="user"
                      />
                    </div>

                    <div className="flex">
                      {' '}
                      <span className="text-gray-2 text-xs pl-1">
                        {article &&
                          article.author &&
                          article.author.split(',') &&
                          article.author.split(',')[0]}
                        {article &&
                        article.author &&
                        article.author.split(',') &&
                        article.author.split(',').length > 1
                          ? ',...'
                          : ''}
                      </span>
                    </div>
                  </div>
                  <div className="mt-1 md:w-4/12 text-xs text-gray-2 font-medium">
                    {Date.now() - parseInt(article.createdAt) > 8640000
                      ? article && format(+article.createdAt, 'dd MMM yyyy')
                      : getUpdatedTime(article.createdAt)}
                  </div>{' '}
                </div>
              </div>
            </div>
            <div className="bg-black h-[1px] "></div>
          </div>
        ) : null}
      </div>
    </div>
  )
}

export default ArticleCard
