import React from 'react'
import ArticleCard from './articlecard'
import { ARTICLE_VIEW, News_Live_Tab_URL } from '../../../constant/Links'
import CleverTap from 'clevertap-react'
import Link from 'next/link'
import Router from 'next/router'
import Loading from '../../loading'
import { useQuery } from '@apollo/client'
import { GET_ARTICLES_MATCH_INFO_SCORECARD } from '../../../api/queries'
const RightSchevronBlack = '/svgs/RightSchevronBlack.svg'

export default function ArticlesTab({ params, match, ...props }) {
  const { data } = useQuery(GET_ARTICLES_MATCH_INFO_SCORECARD, {
    variables: { type: 'matches', Id: params?.slug?.[0] },
  })
  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'ArticlesTab',
      ArticleType:
        article.type === 'match_report'
          ? 'MatchReport'
          : article.type === 'news'
          ? 'News'
          : article.type === 'fantasy'
          ? 'Fantasy'
          : article.type === 'interview'
          ? 'Interview'
          : 'Others',
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: localStorage ? localStorage.Platform : '',
    })
  }
  const getNewsUrl = (article) => {
    let articleId = article.articleID
    let tab = 'latest'
    return {
      as: eval(ARTICLE_VIEW.as),
      href: ARTICLE_VIEW.href,
    }
  }

  const navigateMatchDetailBlog = (article) => {
    ///news/live-blog/${matchId}/${matchName}/${articleID}
    let matchId = match?.matchID
    let name = `${
      match?.homeTeamName ? match?.homeTeamName : match?.homeTeamShortName
    }-vs-${
      match?.awayTeamName ? match?.awayTeamName : match?.awayTeamShortName
    }-${match?.matchNumber ? match?.matchNumber.split(' ').join('-') : ''}-`

    let matchName =
      name.toLowerCase() +
      (match?.seriesName
        ? match?.seriesName
            .replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()
        : '')
    let articleID = article.articleID
    return {
      as: eval(News_Live_Tab_URL.as),
      href: News_Live_Tab_URL.href,
    }
  }

  const newsHomeNav = () => {
    CleverTap.initialize('NewsHome', {
      Source: 'Homepage',
      Platform: global.window && global.window.localStorage.Platform,
    })
    Router.push(`/news/[type]`, '/news/latest')
  }

  if (!data)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center">
        <Loading />
      </div>
    )
  if (data?.length === 0) return <Loading />
  else
    return data?.getArticlesByIdAndType?.length ? (
      <div>
        {data?.getArticlesByIdAndType?.map((article, i) => (
          <>
            {article.type != 'live-blogs' && article.type != 'live-blog' ? (
              <Link {...getNewsUrl(article)} passHref>
                <div
                  key={i}
                  onClick={() => {
                    handleCleverTab(article)
                  }}
                >
                  <ArticleCard article={article} />
                </div>
              </Link>
            ) : (
              <Link {...navigateMatchDetailBlog(article)} passHref>
                <div
                  key={i}
                  onClick={() => {
                    handleCleverTab(article)
                  }}
                >
                  <ArticleCard article={article} />
                </div>
              </Link>
            )}
          </>
        ))}
      </div>
    ) : (
      <div>
        {/* <div className="dark:bg-gray-4 bg-gray-10 text-xs font-semibold py-3 my-2 dark:text-white text-black text-center">
          <div>No articles available for this match.</div>
          <div>Do check out other stories.</div>
        </div> */}
        <div>
          <div className="flex justify-between items-center p-2.5 dark:bg-gray bg-gray-11 text-black dark:text-white">
            <div className="text-xs font-semibold">RECENT NEWS</div>
            <Link
              legacyBehavior
              className="text-xs white font-medium md:pr-3"
              href={`/news/latest`}
            >
              <img
                className="cursor-pointer"
                alt={RightSchevronBlack}
                src={RightSchevronBlack}
                // onClick={() => newsHomeNav()}
              />
            </Link>
          </div>
          <div className="h-[1px] dark:bg-black bg-[#E2E2E2] w-full" />
          {data &&
            data.getArticleByPostitions &&
            data.getArticleByPostitions.map(
              (article, i) =>
                article && (
                  <>
                    {article.type != 'live-blogs' &&
                    article.type != 'live-blog' ? (
                      <Link {...getNewsUrl(article)} passHref>
                        <div
                          key={i}
                          onClick={() => {
                            handleCleverTab(article)
                          }}
                        >
                          <ArticleCard article={article} />
                        </div>
                      </Link>
                    ) : (
                      <Link {...navigateMatchDetailBlog(article)} passHref>
                        <div
                          key={i}
                          onClick={() => {
                            handleCleverTab(article)
                          }}
                        >
                          <ArticleCard article={article} />
                        </div>
                      </Link>
                    )}
                  </>
                ),
            )}
        </div>
      </div>
    )
}
