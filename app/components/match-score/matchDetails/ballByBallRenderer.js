import React from 'react'

export default function BallByBallRenderer(props) {

   const { inningsSummary, overs } = props

   return (

      <div>
         <div>
            {inningsSummary && inningsSummary.map((inning, i) => <div key={i} className="ba b--navy">
               <div className="bg-navy white flex pa3 justify-between items-center">
                  <span className="f6 fw5">{inning.score.battingTeamName.toUpperCase()}</span>
                  <div className="flex items-center">
                     <span className="f6 fw5 ph1">{inning.score.runsScored}/{inning.score.wickets}</span>
                     <span className="f6 fw5">({inning.score.overs})</span>
                  </div>
               </div>

               <div className="flex justify-between items-center">

                  <div className=" w-100 ">
                     {inning.battingList.map((player, i) => <div key={i} className="flex justify-between pa2 pv3 bb w-100" >
                        <span className="f7 fw5 truncate w-80 ">{player.playerName}</span>
                        <span className="f7 fw5 truncate">{player.playerMatchRuns}</span>
                     </div>)}
                  </div>

                  <div className="v-divider flex w-10 " />

                  <div className="w-100 ">
                     {inning.bowlingList.map((player, i) => <div key={i} className="flex justify-between pa2 pv3 bb w-100" >
                        <span className="f7 fw5 truncate w-80 ">{player.playerName}</span>
                        <span className="f7 fw5 truncate">{player.playerWicketsTaken}/{player.playerRunsConceeded}</span>
                     </div>)}
                  </div>

               </div>

            </div>)}

            {overs && overs.map((over, i) =>

               <div key={i} >

                  {i === 0 ? over.balls.length >= 6 : over.balls.length >= 0 && <div className=" flex bg-light-title items-center pa2 ">
                     <div className="pa1 br2" style={{ background: "rgb(114, 118, 130)", minWidth: "50px" }}>
                        <p className="tc ma0 fw7 white f4 pa2" >{over.over}</p>
                        <div className="divider" />
                        <p className="tc ma0 white f8 fw5 pa1" >over </p>
                     </div>
                     <div className="flex ml2 overflow-x-scroll overflow-y-hidden hidescroll ">
                        {over.balls.reverse().map((ball, i) => <div key={i} className="tc ml2 ">
                           {ball.wicket ? <div className="flex justify-center items-center ba f6 fw7 black w2 h2 br-100 bg-cdc white b--cdc" >W</div> : <div className={`ba flex justify-center items-center f7 fw4 black w2 h2 br-100 ${ball.runs === '4' ? 'bg-green white b--green' : ball.runs === '6' ? 'bg-blue white b--blue' : 'bg-white '} `}>{ball.type === "no ball" ? "nb" : ball.type === "leg bye" ? "lb" : ball.type === "bye" ? "b" : ball.type === "wide" ? "wd" : ""}{ball.runs}</div>}
                           <p className="f8 black-30 pt1 ma0 fw6">{ball.over}</p>
                        </div>)}
                     </div>
                  </div>}

                  <div className="divider" />

                  {over.summary && <div className="flex justify-between items-center bg-title">
                     <div className="w-100">
                        <div className="flex justify-between pa2">
                           <span className="f7 truncate w-70">{over.summary.batsmen[0].batsmanName}*</span>
                           <span className="f7 fw5">{over.summary.batsmen[0].runs} ({over.summary.batsmen[0].balls})</span>
                        </div>
                        <div className="flex justify-between pa2">
                           <span className="f7 truncate w-70">{over.summary.batsmen[1].batsmanName}</span>
                           <span className="f7  fw5">{over.summary.batsmen[1].runs} ({over.summary.batsmen[1].balls})</span>
                        </div>
                     </div>

                     <div className="w-100">
                        <div className="flex justify-between pa2">
                           <span className="f7 truncate w-70">{over.summary.bowler[0].bowlerName}</span>
                           <span className="f7  fw5">{over.summary.bowler[0].wickets}/{over.summary.bowler[0].runs}</span>
                        </div>
                        <div className="flex justify-between pa2">
                           <span className="f7 ">{over.summary.teamShortName}</span>
                           <span className="f7  fw5">{over.summary.score}</span>
                        </div>
                     </div>

                  </div>}

                  <div className="divider" />


                  {over.balls.reverse().map((ball, i) => <div key={i}>
                     <div className=" flex bg-light-title items-center pa2 ">
                        {ball.type === "wicket" ? (
                           <div className={`pa1 br2 ba bg-cdc white b--cdc`} style={{ minWidth: "50px" }}>
                              <p className={`tc ma0 fw7 f4 pv1 `} >W</p>
                              <div className="divider-dark" />
                              <p className="tc ma0 f8 fw5 pa1" >{ball.over} </p>
                           </div>
                        )
                           :
                           (<div className={`pa1 br2 ba ${ball.runs === '4' ? 'bg-green white b--green' : ball.runs === '6' ? 'bg-blue white b--blue' : 'bg-white'}`} style={{ minWidth: "50px" }}>
                              <p className={`tc ma0 fw7 f4 pv1`} >{ball.runs}</p>
                              <div className="divider-dark" />
                              <p className="tc ma0 f8 fw5 pa1" >{ball.over} </p>
                           </div>)
                        }

                        <div className="flex ml2 f7 gray "> {ball.bowlerName} to {ball.batsmanName}, {ball.commentary} </div>
                     </div>
                  </div>)}

                  <div className="divider" />
               </div>

            )}

         </div>

      </div>
   )
}
