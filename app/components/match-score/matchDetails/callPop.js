import React, { useEffect } from 'react';
import Frd from './try1';
import CleverTap from 'clevertap-react';
import PopupDugout from './popupDugout';
import format from 'date-fns/format';

const playerAvatar = '/placeHodlers/playerAvatar.png';
export default function callPop(props) {
  const handlePopCle = () => {
    CleverTap.initialize(`DugoutWagonWheel`, {
      source: 'Dugout',
      MatchID: props.CleaverData && props.CleaverData.matchID,
      SeriesID: props.CleaverData && props.CleaverData.matchData.seriesID,
      TeamAID: props.CleaverData && props.CleaverData.matchData.matchScore[0].teamID,
      TeamBID: props.CleaverData && props.CleaverData.matchData.matchScore[1].teamID,
      MatchFormat: props.CleaverData && props.CleaverData.matchType,
      MatchStartTime: props.CleaverData && format(Number(props.CleaverData.matchData.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: props.CleaverData && props.CleaverData.matchData.matchStatus,
      Platform: global.window.localStorage.Platform
    });
  };

  const item = props.data;
  return (
    <div className='flex ma1  flex-column  justify-start  w-50   shadow-4  pv2 '>
      <div className='flex h37 relative justify-center bottom-0 items-end'>
        <img className='h37  z-0  w-80 left-0 bottom-0 ' alt='/' src='/svgs/newwhite.svg' />
        <div
          className=' absolute  h37 overflow-hidden '
          onClick={() => (handlePopCle(), props.setPopWHeelIndex(props.index), props.setPopWHeel(true))}>
          <div className='flex  items-start justify-center center'>
            <div>
              <img
                className='   z-0 ph1   object-contain  bw0 '
                alt=''
                src={`https://images.cricket.com/players/${props.item.playerID}_headshot_safari.png`}
                onError={(evt) => (evt.target.src = playerAvatar)}
              />
            </div>

            <div className='w-100 pa2 flex flex-column items-start justify-start '>
              <div className='flex red_10 f3  fw6'>
                {props.item.playerMatchRuns} {props.item.isNotOut && <span>*</span>}
              </div>

              {props.item && props.item.playerMatchBalls && (
                <div className='flex f6'>
                  {'('}
                  {props.item.playerMatchBalls}
                  {')'}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <div
        className='flex  pa1 f6 fw5 white'
        style={{ background: props.batsmanColor ? props.batsmanColor : 'red_10' }}>
        {props.item.playerName}
      </div>
      <div className='flex  bb b--black  pa1'>
        <div className='w-30 flex items-center justify-center f5 red_10 fw6  '>
          <span className='f6 text-gray-2 mr1  fw5'>4s</span>
          {props.item.playerMatchFours}{' '}
        </div>

        <div className='w-30 flex items-center justify-center red_10  f5 fw6'>
          {' '}
          <span className='f6 text-gray-2 mr1 fw5'>6s</span>
          {props.item.playerMatchSixes}{' '}
        </div>
      </div>

      <div
        className='flex w-100 items-center justify-center mt2'
        onClick={() => (handlePopCle(), props.setPopWHeelIndex(props.index), props.setPopWHeel(true))}>
        {' '}
        <Frd smallWheel={true} battingStyle={props.item.battingStyle} zad={props.item.zad} />
      </div>

      {props.popUpWheel && props.popUpWheelIndex == props.index && (
        <PopupDugout
          popUpWheel={props.popUpWheel}
          key={props.index}
          callScroll={props.callScroll}
          item={props.item}
          nout={props.nout}
          upcoming={false}
          battingStyle={props.item.battingStyle}
          setPopWHeel={props.setPopWHeel}
          name={props.index}
        />
      )}
    </div>
  );
}
