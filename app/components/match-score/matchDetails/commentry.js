import React, { useState, useEffect, useRef } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { NEW_COMMENTARY, POLL_COMMENTARY } from '../../../api/queries'
import ProbablePlayingXITab from './probablePlayingXITab'
import HighlightsTab from './highlightsTab'
import MatchLiveTab from './matchLiveTab'
const ground = '/svgs/ground.png'
const groundSmall = '/pngs/groundImageCommentary.png'
// import moment from "moment";
import { format } from 'date-fns'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const fliter = '/pngsV2/fliter.png'
import PlayingXITab from './playingXITab'
import DataNotFound from '../../commom/datanotfound'
import ImageWithFallback from '../../commom/Image'
import Loading from '../../loading'
function Commentary({ matchID, matchData, getId, matchType }) {
  const [ts, setTs] = useState(0)
  const [showHigh, setShowHigh] = useState(false)

  const [pollTs, setpollTs] = useState(0)
  const [isPoll, setisPoll] = useState(false)
  const [isScrolled, setisScrolled] = useState(false)
  const [showZad, setShowZad] = useState(false)
  const [ballId, setBallId] = useState()
  const { loading, error, data, fetchMore, refetch } = useQuery(
    NEW_COMMENTARY,
    {
      variables: { matchId: matchID, timeStamp: 0 },
      pollInterval: 8000,
      onCompleted: (data) => {
        if (data.getCommentary.commentary) {
          setTimeStamp(data)
          setPollTimeStamp(data)
        }
      },
      context: { clientName: 'commentary' },
    },
  )
  const { data: pollData, startPolling, stopPolling } = useQuery(
    POLL_COMMENTARY,
    {
      variables: { matchId: matchID, timeStamp: pollTs },
      context: { clientName: 'commentary' },
      pollInterval: 8000,
      // notifyOnNetworkStatusChange: true,
      // fetchPolicy: 'network-only',
      onCompleted: (data) => {
        if (data && data.pollCommentary) {
          setisPoll(true)
        }
      },
    },
  )
  //  )

  const getZadCo = (ball) => {
    if (ball.battingStyle && ball.battingStyle === 'LHB') {
      return 0 - (180 - parseInt(ball.zad.split(',')[1]))
    }
    return 0 - ball.zad.split(',')[1]
  }
  // useEffect(() => {
  //   if (pollData && pollData.pollCommentary) {
  //     refetch();
  //   }
  // }, [pollData]);

  useEffect(() => {
    if (false && data && data.getCommentary && data.getCommentary.commentary) {
      setTimeStamp(data)
      setPollTimeStamp(data)
    }
  }, [data])

  useEffect(() => {
    if (isPoll) {
      if (!isScrolled) {
        refetch()
        setisPoll(false)
      }
    }
  }, [isPoll])

  useEffect(() => {
    if (ts !== 0 && matchData?.matchStatus === 'live') {
      startPolling(8000)
    }
    // return () => {
    //   stopPolling();
    // };
  }, [ts])

  if (loading) {
    return <div></div>
  }
  if (matchData?.length == 0) {
    return <Loading />
  }
  if (!pollData) {
    return <Loading />
  }
  const callSocialWidget = () => {
    window.twttr.ready().then(() => {
      window.twttr.widgets.load(document.getElementsByClassName('twitter-div'))
    })
    window.instgrm.Embeds.process()
  }

  const refetchData = () => {
    refetch()
    setisPoll(false)
    setisScrolled(false)
  }

  const setTimeStamp = (data) => {
    try {
      let dt = JSON.parse(data.getCommentary.commentary)
      const topTS = JSON.parse(dt[dt.length - 1]).id
      setTs(topTS)
    } catch (e) {}
  }
  const setPollTimeStamp = (data) => {
    try {
      let dt = JSON.parse(data.getCommentary.commentary)
      const lastTS = JSON.parse(dt[0]).id
      getId(lastTS)
      setpollTs(lastTS)
    } catch (e) {}
  }

  const getRuns = (runs) => {
    if (runs.includes('nb')) {
      if (parseInt(runs) > 1) {
        return `${parseInt(runs) - 1}nb`
      } else {
        return 'nb'
      }
    } else if (runs.includes('wd')) {
      if (parseInt(runs) > 1) {
        return `${parseInt(runs) - 1}wd`
      } else {
        return 'wd'
      }
    } else if (runs === '0W') {
      return 'W'
    } else return runs
  }

  const comWidgets = (ball) => {
    switch (ball.ballType) {
      case 'IS':
        return (
          <div className="text-white m-2 border-gray-4 border rounded">
            {ball && ball.score && (
              <div className="white flex p-2 justify-between items-center bg-gray-4">
                <div className="flex items-center">
                  <ImageWithFallback
                    height={22}
                    width={22}
                    loading="lazy"
                    fallbackSrc={flagPlaceHolder}
                    className="w-8 h-5"
                    src={`https://images.cricket.com/teams/${ball.score.battingTeamID}_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    alt=""
                  />
                  <span className="text-xs font-bold  pl-2 ">
                    {ball.score.battingTeamName.toUpperCase()}
                  </span>
                </div>
                <div className="flex items-center">
                  <span className="text-sm font-bold px-1">
                    {ball.score.runsScored}/{ball.score.wickets}
                  </span>
                  <span className="text-sm font-bold">
                    ({ball.score.overs})
                  </span>
                </div>
              </div>
            )}

            <div className="flex justify-between items-center bg-gray">
              <div className="w-1/2  border-r border-gray-4">
                {ball.battingList.map((player, i) => (
                  <div key={i} className="flex justify-between p-2">
                    {}
                    <span className="text-xs font-bold">
                      {player.playerName}
                    </span>
                    <span className="text-xs font-bold">
                      {player.playerMatchRuns} {'('}
                      {player.playerMatchBalls}
                      {')'}
                    </span>
                  </div>
                ))}
              </div>

              {/* <div className='bg-white w-[1px] h-10' /> */}

              <div className="w-1/2 ">
                {ball.bowlingList.map((player, i) => (
                  <div key={i} className="flex justify-between p-2  ">
                    {}
                    <span className="text-xs font-bold">
                      {player.playerName}{' '}
                    </span>
                    <span className="text-xs font-bold">
                      {player.playerWicketsTaken}/{player.playerRunsConceeded}
                      {'('}
                      {player.playerOversBowled}
                      {')'}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
        )
      case 'extra':
        return (
          <div className="flex text-xs font-normal dark:text-white text-black m-2 ">
            <p>{ball.commentary}</p>
          </div>
        )
      case 'social':
        return (
          <div className="relative  ">
            <div className="twitter-div   ">
              <div dangerouslySetInnerHTML={{ __html: ball.commentary }} />
              {callSocialWidget()}
            </div>
            {/* <iframe src={ball.commentary} /> */}
          </div>
        )
      case 'quick_byte':
        return (
          <div
            style={{ border: '2px solid #F0F1F5' }}
            className="flex f6 border-gray-2 rouded flex-column relative my-1 dark:text-white text-black"
          >
            <div className="flex font-semibold justify-between ">
              <p
                style={{
                  backgroundColor: '#F0F1F5',
                  borderBottomRightRadius: 3,
                }}
                className="p-1 text-xs md:text-black text-black font-medium"
              >
                {ball.ballType.replace('_', ' ').toUpperCase()}
              </p>
            </div>
            <div className="p-2 leading-tight">
              <p className="">{ball.commentary}</p>
            </div>
            <ImageWithFallback
              height={28}
              width={28}
              loading="lazy"
              alt=""
              src="/pngs/48X48.png"
              className="absolute "
              style={{ width: 20, height: 20, right: 5, top: 5 }}
            />
          </div>
        )
      case 'match_update':
        return (
          <div
            style={{ border: '2px solid #3E485B' }}
            className="flex ml-2 text-semibold border border-red flex-col my-2 relative dark:text-white text-black"
          >
            <div className="flex f7 justify-between pr-3">
              <p
                style={{
                  borderBottomRightRadius: 3,
                }}
                className="px-2 py-1 dark:text-white text-black m-0"
              >
                {ball.ballType.replace('_', ' ').toUpperCase()}
              </p>
            </div>
            <div className="p-2">
              <p className="mb-1 text-white md:text-black">
                {format(ball.timestamp, 'dd MMM yyyy | hh:mm a ')}
              </p>
              <p className="">{ball.commentary}</p>
            </div>
            <ImageWithFallback
              height={28}
              width={28}
              loading="lazy"
              alt=""
              src="/pngs/48X48.png"
              className="absolute"
              style={{ width: 20, height: 20, right: 5, top: 5 }}
            />
          </div>
        )

      case 'summary':
        return (
          <>
            <div className="dark:text-white text-black dark:px-2 border-black">
              <div className="flex items-center dark:justify-between dark:px-2 dark:py-1 p-3 md:mx-2 md:gap-2 bg-gray dark:mt-1 rounded-md">
                <div
                  className="p-1 md:mt-1 xl:mt-1 lg:mt-1 rounded-sm flex flex-col justify-center"
                  style={{ minWidth: '50px' }}
                >
                  {/* parseInt(ball.balls[0].over.split('.')[0]) + 1} */}

                  <p className=" text-center text-white font-semibold text-sm">
                    over{' '}
                    {ball.over
                      ? parseInt(ball.over.split('.')[0]) + 1
                      : parseInt(ball.balls[0].over.split('.')[0]) + 1}
                  </p>
                </div>
                <div className="flex items-center gap-2 justify-between md:py-1 dark:justify-between dark:w-8/12 w-3/12 dark:mx-1 dark:mt-2 dark:overflow-x-scroll dark:overflow-y-hidden">
                  {ball.balls.reverse().map((ball, i) => (
                    <div key={i} className="text-center">
                      {ball.runs.includes('W') ? (
                        <div className="flex justify-center items-center black w-8 h-8 rounded-full bg-red white text-xs font-bold">
                          {getRuns(ball.runs)}
                        </div>
                      ) : (
                        <div
                          className={`flex justify-center items-center text-xs font-bold black w-8 h-8 rounded-full ${
                            parseInt(ball.runs) === 4
                              ? 'bg-green white b--green'
                              : parseInt(ball.runs) === 6
                              ? 'bg-green white b--green text-xs font-bold'
                              : 'bg-gray-2 text-black text-xs font-bold '
                          } `}
                        >
                          {getRuns(ball.runs)}
                        </div>
                      )}
                      {/* <p className='pt-1 text-xs font-bold text-gray-2 font-medium mx-1'>{ball.over}</p> */}
                    </div>
                  ))}
                </div>
              </div>
              <div className="flex justify-between items-center mt-2 md:bg-[#E2E2E2] bg-gray rounded-md md:mx-2 dark:p-1 p-2">
                <div className="w-6/12 border-r-2 dark:border-gray-4 border-gray-11">
                  <div className="flex justify-between p-1">
                    <span className="text-xs font-bold">
                      {ball.batsmen[0].batsmanName}*
                    </span>
                    <span className="text-xs font-bold">
                      {ball.batsmen[0].runs} ({ball.batsmen[0].balls})
                    </span>
                  </div>
                  <div className="flex justify-between p-1">
                    <span className="text-xs font-bold">
                      {ball.batsmen[1].batsmanName}
                    </span>
                    <span className="text-xs font-bold">
                      {ball.batsmen[1].runs} ({ball.batsmen[1].balls})
                    </span>
                  </div>
                </div>
                <div className="w-[1px] h-10 mx-1 md:bg-black"></div>
                <div className=" w-6/12">
                  <div className="flex justify-between p-1">
                    <span className="text-xs font-bold">
                      {ball.bowler[0].bowlerName}
                    </span>
                    <span className="text-xs font-bold">
                      {ball.bowler[0].wickets}/{ball.bowler[0].runs}
                    </span>
                  </div>
                  <div className="flex justify-between p-1">
                    <span className="text-xs font-bold ">
                      {ball.teamShortName}
                    </span>
                    <span className="text-xs font-bold">{ball.score}</span>
                  </div>
                </div>
              </div>
            </div>
          </>
        )
      case 'ball':
        return (
          <div>
            {/* {showZad?'showww':'notshoww'}<br/> */}
            {/* {ball.runs} <br/>
            {ball.zad}<br/><br/> */}
            {/* {ballId} */}
            {/* {ballId === ball.id ? '444' : ball.id } */}
            <div
              className=" flex text-white items-start py-3 mx-2 z-50 "
              onClick={() =>
                (parseInt(ball.runs) === 6 || parseInt(ball.runs) === 4) &&
                ball.zad != ''
                  ? (setShowZad(true),
                    setBallId(ballId === ball.id ? '' : ball.id))
                  : ''
              }
            >
              <div
                className={`flex items-start ${
                  parseInt(ball.runs) === 4 || parseInt(ball.runs) === 6
                    ? ''
                    : ''
                }`}
              >
                {ball.wicket === true ? (
                  <div
                    className={`p-1 rounded dark:bg-gray bg-[#E2E2E2] text-center dark:text-white text-black`}
                  >
                    <p
                      className={`text-center font-semibold flex items-center justify-center text-xs py-1 border-b border-b-gray-2`}
                    >
                      <div className="w-8 h-8 flex items-center justify-center bg-red rounded-full font-semibold">
                        {getRuns(ball.runs)}
                      </div>
                    </p>

                    <p className="fw5 text-xs p-1">{ball.over} </p>
                  </div>
                ) : (
                  <div
                    className={`p-1 rounded dark:bg-gray bg-[#E2E2E2] flex flex-col items-center justify-center`}
                  >
                    <p
                      className={`text-center font-semibold text-sm py-1 border-b border-b-gray-2 md:text-black text-white ${
                        parseInt(ball.runs) === 4
                          ? 'bg-green h-8 w-8  flex items-center justify-center rounded-full'
                          : parseInt(ball.runs) === 6
                          ? 'bg-green h-8 w-8 flex items-center justify-center rounded-full'
                          : 'bg-gray-2 h-8 w-8 flex items-center justify-center rounded-full'
                      }`}
                    >
                      {' '}
                      {getRuns(ball.runs)}
                    </p>

                    <p className="ma0 text-xs fw5 text-center pa1 border-t dark:border-gray-4 border-gray-6 lg:text-black md:text-black xl:text-black mt-1 w-full">
                      {ball.over}{' '}
                    </p>
                  </div>
                )}

                <div className="text-xs font-bold dark:mx-2 md:m-2 text-gray-2">
                  {' '}
                  <span className="text-xs font-bold">{ball.bowlerName}</span>
                  <span> to </span>
                  <span className="text-xs font-bold">
                    {ball.batsmanName},{' '}
                  </span>
                  <span>{ball.commentary} </span>
                </div>
              </div>
              {(parseInt(ball.runs) === 4 ||
                parseInt(ball.runs) === 6 ||
                parseInt(ball.runs) === 5) &&
                ball.zad !== '' && (
                  <div className="md:w-10 w-28 h-10">
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className="h-8 w-8"
                      alt=""
                      src={groundSmall}
                    />
                  </div>
                )}
            </div>

            {showZad && ballId === ball.id && (
              <div className="flex justify-center my-1 overflow-hidden">
                <div className="relative w-32 h-32 m-1 overflow-hidden">
                  <ImageWithFallback
                    height={58}
                    width={58}
                    loading="lazy"
                    className="w-32 h-32 absolute overflow-hidden"
                    src={ground}
                    alt=""
                  />
                  <div className="w-32 h-32 absolute overflow-hidden">
                    <div
                      className={`border-b w-16 absolute overflow-hidden ${
                        ball.runs === '0'
                          ? 'hidden'
                          : ball.runs === '6'
                          ? 'border-blue'
                          : 'border-white'
                      } `}
                      style={{
                        bottom: '55%',
                        left: '50%',
                        right: '0%',
                        transform: `rotate(${-getZadCo(ball)}deg)`,
                        transformOrigin: '0 0',
                      }}
                    ></div>
                  </div>
                </div>
              </div>
            )}
          </div>
        )
    }
  }
  // let scrl = useRef(null)

  window.addEventListener('scroll', () => handlePagination())

  const handleScroll = () => {
    //

    if (true) {
      fetchMore({
        variables: { timeStamp: ts },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (
            !fetchMoreResult ||
            !fetchMoreResult.getCommentary.commentary ||
            JSON.parse(fetchMoreResult.getCommentary.commentary).length == 0
          ) {
            return prev
          } else {
            let parsedPre = JSON.parse(prev.getCommentary.commentary)
            let newData = JSON.parse(fetchMoreResult.getCommentary.commentary)

            fetchMoreResult &&
              fetchMoreResult.getCommentary &&
              fetchMoreResult.getCommentary.commentary !== '[]' &&
              setTimeStamp(fetchMoreResult)

            if (!isScrolled) {
              setisScrolled(true)
            }
            let hereData = [...parsedPre, ...newData]

            const uniq = new Set(
              hereData && hereData.map((e) => JSON.stringify(e)),
            )
            const res = Array.from(uniq).map((e) => JSON.parse(e))

            return Object.assign({}, prev, {
              getCommentary: {
                commentary: JSON.stringify(res),
                __typename: 'commentary',
              },
            })
          }
        },
      })
    }
  }
  const handlePagination = (evt) => {
    //
    //
    if (
      window.innerHeight + window.scrollY >=
      document.body.offsetHeight - 200
    ) {
      // setPagination((prev)=>prev + 1)

      // alert('end')
      //
      handleScroll()
    }
  }

  // if (error) return <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center"><span>Something isn't right!</span><span>Please refresh and try again...</span></div>
  if (loading)
    return (
      <div>
        <Loading />
      </div>
    )
  if (error) {
    return <DataNotFound />
  }
  return (
    <div
      className="hidescroll relative px-2"
      // style={{ maxHeight: '75vh' }}
      onScroll={(evt) => handleScroll(evt)}
    >
      {matchData?.matchStatus === 'live' &&
      matchData?.isLiveCriclyticsAvailable ? (
        <MatchLiveTab matchData={matchData} path="live" />
      ) : null}
      {isScrolled && isPoll && (
        <div
          onClick={() => refetchData()}
          style={{ left: '40%' }}
          className="sticky top-1 br4 f5 tc bg-cdc w-25 white ph2 pv1"
        >
          refresh
        </div>
      )}
      {/* {!showHigh && (
        <div
          className={
            matchData?.matchStatus.toLowerCase() === 'upcoming'
              ? 'hidden'
              : 'relative'
          }
        >
          <ImageWithFallback
            height={28}
            width={28}
            loading="lazy"
            className="h-10 w-10 p-2 rounded-sm  fixed right-0 bg-gray"
            onClick={() => setShowHigh(!showHigh)}
            style={{ top: '70%' }}
            src={fliter}
            alt=""
          />
        </div>
      )} */}
      {!showHigh && (
        <>
          {data &&
          data.getCommentary &&
          data.getCommentary.commentary !== 'false' &&
          data.getCommentary.commentary ? (
            JSON.parse(data.getCommentary.commentary)
              .map((cm) => JSON.parse(cm))
              .map((comm, i) => (
                <div key={i} className="dark:bg-basebg dark:border-gray">
                  <div>{comWidgets(comm)}</div>

                  {/* <div className='bg-black/80 h-[1px] ' /> */}
                </div>
              ))
          ) : matchType === 'completed' ? (
            <DataNotFound />
          ) : (
            <div className="bg-light-gray text-center text-lg f7-ns p-2 font-light">
              Live commentary will begin as soon as the match commences.
            </div>
          )}
        </>
      )}

      {matchData?.matchStatus === 'live' &&
      !matchData?.isLiveCriclyticsAvailable &&
      matchData?.playing11Status ? (
        <PlayingXITab matchData={matchData} path="playing11" />
      ) : null}
      {matchData?.matchStatus === 'upcoming' && matchData?.probable11Status ? (
        <ProbablePlayingXITab matchData={matchData} path="probableplaying11" />
      ) : null}

      {showHigh && (
        <HighlightsTab
          showHigh={showHigh}
          setShowHigh={setShowHigh}
          matchData={matchData}
          matchID={matchID}
          matchType={matchType}
        />
      )}
    </div>
  )
}

export default Commentary
