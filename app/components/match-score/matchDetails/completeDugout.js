import React, { useEffect } from 'react'
import Frd from './completedPie'
import format from 'date-fns/format'
import PopupDugout from './popupDugout'
import CleverTap from 'clevertap-react'
const playerAvatar = '/pngsV2/playerph.png'

export default function callPop(props) {
  let show

  const callPopName = (data) => {
    show = data
  }

  const handlePopCle = () => {
    CleverTap.initialize('DugoutWagonWheel', {
      source: 'Dugout',
      MatchID: props.CleaverData && props.CleaverData.matchID,
      SeriesID: props.CleaverData && props.CleaverData.matchData.seriesID,
      TeamAID:
        props.CleaverData && props.CleaverData.matchData.matchScore[0].teamID,
      TeamBID:
        props.CleaverData && props.CleaverData.matchData.matchScore[1].teamID,
      MatchFormat: props.CleaverData && props.CleaverData.matchType,
      MatchStartTime:
        props.CleaverData &&
        format(
          Number(props.CleaverData.matchData.startDate),
          'do MMM yyyy, h:mm a',
        ),
      MatchStatus: props.CleaverData && props.CleaverData.matchData.matchStatus,
      Platform: global.window.localStorage.Platform,
    })
  }

  const item = props.data
  return (
    <div className="flex  mt2 flex-row  justify-between  w-100   shadow-4  ph2 pt1  ">
      {props.popUpWheel && props.popUpWheelIndex === props.index && (
        <PopupDugout
          mainData={props.mainData}
          index={props.index}
          callScroll={props.callScroll}
          callPopName={callPopName}
          popUpWheel={props.popUpWheel}
          key={props.index}
          chart={0}
          item={props.item}
          teamColor={props.teamColor}
          battingStyle={props.item.battingStyle}
          setPopWHeelIndex={props.setPopWHeelIndex}
          upcoming={true}
          mainIndex={props.mainIndex}
          popUpWheelIndex={props.popUpWheelIndex}
          setPopWHeel={props.setPopWHeel}
          name={props.index}
        />
      )}

      {props.popUpWheel2 && props.popUpWheelIndex2 === props.index && (
        <PopupDugout
          mainData={props.mainData}
          index={props.index}
          callPopName={callPopName}
          chart={0}
          teamColor={props.teamColor}
          battingStyle={props.item.battingStyle}
          popUpWheel={props.popUpWheel2}
          key={props.index}
          callScroll={props.callScroll}
          item={props.item}
          popUpWheelIndex2={props.popUpWheelIndex2}
          upcoming={true}
          mainIndex={props.mainIndex}
          setPopWHeel={props.setPopWHeel2}
          name={props.index}
          setPopWHeelIndex={props.setPopWHeelIndex2}
        />
      )}

      {!props.popUpWheel && !props.popUpWheel2 && (
        <>
          <div
            className="w-4/12 flex text-white"
            onClick={() => {
              props.mainIndex == 0
                ? (handlePopCle(),
                  props.setPopWHeelIndex(props.index),
                  props.setPopWHeel(true))
                : (handlePopCle(),
                  props.setPopWHeelIndex2(props.index),
                  props.setPopWHeel2(true))
            }}
          >
            <div className="flex  flex-column  w-100 ">
              <div className="h37 flex justify-center items-end">
                {' '}
                <img
                  class="absolute cover z-0 h-35  "
                  width="100"
                  alt="/svgs/newwhite.png"
                  src="/svgs/newwhite.svg"
                />
                <img
                  className="h35 w35 z-0  object-cover object-top "
                  alt=""
                  src={`https://images.cricket.com/players/${props.item.playerID}_headshot_safari.png`}
                  onError={(evt) => (evt.target.src = playerAvatar)}
                />
              </div>
              <div
                className={`  flex  pa1 text-xs white  bg-gray `}
                style={
                  props.teamColor == ''
                    ? { background: '' }
                    : { background: props.teamColor }
                }
              >
                {props.item.playerName}{' '}
              </div>
            </div>
          </div>
          <div
            className="w-4/12 flex justify-around items-center"
            onClick={() => {
              props.mainIndex == 0
                ? (handlePopCle(),
                  props.setPopWHeelIndex(props.index),
                  props.setPopWHeel(true))
                : (handlePopCle(),
                  props.setPopWHeelIndex2(props.index),
                  props.setPopWHeel2(true))
            }}
          >
            <div className="flex  items-center justify-start ">
              <div className="flex red_10 text-lg font-semibold">
                {' '}
                {props.item.playerMatchRuns}{' '}
                {props.item.isNotOut && <span>*</span>}
              </div>
              <div className="text-xs white">
                {'('}
                {props.item.playerMatchBalls}
                {')'}
              </div>
            </div>

            <div className=" flex items-center justify-center f5 red_10 fw6  ">
              <span className="f6 white mr1  fw5">4s</span>
              {props.item.playerMatchFours}{' '}
            </div>

            <div className=" flex items-center justify-center red_10  f5 fw6">
              {' '}
              <span className="f6 white mr1 fw5">6s</span>
              {props.item.playerMatchSixes}{' '}
            </div>
          </div>

          <div
            className="flex w-4/12 items-center justify-center mt2"
            onClick={() => {
              props.mainIndex == 0
                ? (handlePopCle(),
                  props.setPopWHeelIndex(props.index),
                  props.setPopWHeel(true))
                : (handlePopCle(),
                  props.setPopWHeelIndex2(props.index),
                  props.setPopWHeel2(true))
            }}
          >
            {' '}
            <Frd
              NamePlayer={props.item.playerName}
              teamIndex={props.teamIndex}
              setTeamIndex={props.setTeamIndex}
              battingStyle={props.item.battingStyle}
              zad={props.data.zad}
            />
          </div>
        </>
      )}
    </div>
  )
}
