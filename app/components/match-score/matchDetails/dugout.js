import React, { useEffect, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';

import Frd from './try1';
import BowlerDug from './bowler_Dug';
import Livematch from './liveMatchUpDugout';
import StatNug from './statNugut';
import NextBatsMan from './nextBatsMan';
import PopupDugout from './popupDugout';
import RunRate from './runRateDug';
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import Call from './callPop';
import DugoutChart from './chartdugout';
import { DUGOUT } from '../../../api/queries';
import CompleteDugut from './completeDugout';
import DataNotFound from '../../commom/datanotfound'
const ground = '/svgs/cricketGroundNew.png';
const playerAvatar = '/placeHodlers/playerAvatar.png';
const teamFallback = '/svgs/images/flag_empty.svg';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const d3 = {
  scale,
  shape
};

export default function Dugout(props) {
  const { error, loading, data } = useQuery(DUGOUT, {
    variables: { matchID: props.matchID },
    pollInterval: props.pollDugout ? 4000 : null
  });

  const ground = '/svgs/ground.png';
  const [popUpWheel, setPopWHeel] = useState(false);

  const [activeTabTeam, setActiveTabTeam] = useState(1);

  const [popUpWheelIndex, setPopWHeelIndex] = useState('');

  const [popUpWheel2, setPopWHeel2] = useState(false);
  const [popUpWheelIndex2, setPopWHeelIndex2] = useState('');
  const [teamIndex, setTeamIndex] = useState('');

  const [infoToggle, setInfoToggle] = useState(false);
  const [windowObject, updateWindowObject] = useState(null);
  useEffect(() => {
    updateWindowObject({ ...global.window });
  }, []);
  const [activePieIndex, SetactivePieIndex] = useState(0);
  const [activeSession, SetactiveSession] = useState(0);

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  var shortestEdge = (windowObject ? (windowObject.innerWidth > 470 ? 470 : windowObject.innerWidth) : 320) - 64;

  const radius = 55;

  const scaleValue = scale
    .scaleLinear()
    .domain([0, 100])
    .range([0, 2 * Math.PI]);
  const scaleValue1 = scale
    .scaleLinear()
    .domain([0, 132])
    .range([0, 2 * Math.PI]);
  let startAngle = 0;
  let reduceThickness = 0;
  let reduceXY = 10;

  shortestEdge = 150;

  const callScroll = (data) => {
    data ? (document.body.style.overflow = 'hidden') : (document.body.style.overflow = 'unset');
  };

  if (error) return <div></div>;
  if (loading) <div> loading </div>;
  return (
    <>
      {data && data.dugoutDetails && (data.dugoutDetails.status == 'live' || data.dugoutDetails.status == 'mid') ? (
        <div className=' dn-l text-white '>
          <div className=''>
            {data &&
            data.dugoutDetails &&
            data.dugoutDetails.matchBatsman &&
            data.dugoutDetails.matchBatsman.length > 0 ? (
              <div className='flex  flex-column  bg-gray-4  shadow-4  '>
                <div className='flex w-100 bb b--black pa2 f7 fw5'>
                  {' '}
                  {data.dugoutDetails.status == 'mid' ? 'TOP BATTERS' : 'BATTER'}{' '}
                </div>

                <div className='w-100 flex pa2'>
                  {data &&
                    data.dugoutDetails &&
                    data.dugoutDetails.matchBatsman &&
                    data.dugoutDetails.matchBatsman.map((item, index) => {
                      return (
                        <Call
                          CleaverData={props}
                          callScroll={callScroll}
                          data={data.dugoutDetails.matchBatsman}
                          batsmanColor={data.dugoutDetails.batsmanColor}
                          popUpWheelIndex={popUpWheelIndex}
                          popUpWheel={popUpWheel}
                          item={item}
                          nout={true}
                          index={index}
                          setPopWHeel={setPopWHeel}
                          setPopWHeelIndex={setPopWHeelIndex}
                        />
                      );
                    })}
                </div>
              </div>
            ) : (
              <div></div>
            )}

            {data &&
              data.dugoutDetails &&
              data.dugoutDetails.matchBowler &&
              data.dugoutDetails.matchBowler.length > 0 && (
                <BowlerDug
                  bgColor={data && data.dugoutDetails && data.dugoutDetails.bowlerColor}
                  data={data && data.dugoutDetails && data.dugoutDetails.matchBowler}
                  status={data && data.dugoutDetails && data.dugoutDetails.status}
                />
              )}

            {data.dugoutDetails.status == 'mid' && <DugoutChart matchID={props.matchID} />}
            {data.dugoutDetails.status == 'live' && (
              <>
                {data && data.dugoutDetails && data.dugoutDetails.matchUps !== null && (
                  <Livematch
                    data={data && data.dugoutDetails && data.dugoutDetails.matchUps}
                    bowlerName={
                      data &&
                      data.dugoutDetails &&
                      data.dugoutDetails.matchBowler[0] &&
                      data.dugoutDetails.matchBowler[0].playerName
                    }
                  />
                )}

                {data &&
                  data.dugoutDetails &&
                  data &&
                  data.dugoutDetails.StatNuggets &&
                  data.dugoutDetails.StatNuggets.isDisplay && (
                    <StatNug
                      matchType={props.matchType}
                      data={data && data.dugoutDetails && data.dugoutDetails.StatNuggets}
                    />
                  )}
                {data &&
                data.dugoutDetails &&
                data.dugoutDetails.nextBatsmans &&
                data.dugoutDetails.nextBatsmans.length > 0 ? (
                  <NextBatsMan data={data && data.dugoutDetails && data.dugoutDetails.nextBatsmans} />
                ) : (
                  <div className=''></div>
                )}
              </>
            )}

            {data && data.dugoutDetails && data.dugoutDetails.last18balls && data.dugoutDetails.status == 'live' && (
              <div className='w-100 mt2 flex  flex-column  bg-gray-4  shadow-4   '>
                <div className='flex w-100 bb b--black pa2 f7 fw5 ttu'>
                  <div className='w-50 ttu'>Last 18 Balls</div>
                </div>

                <div className='w-100 flex flex-row pa3'>
                  <div className=' w-25 flex flex-column  items-center justify-center'>
                    {' '}
                    <div className='fw5'>
                      {data.dugoutDetails && data.dugoutDetails.last18balls && data.dugoutDetails.last18balls.runs}
                    </div>
                    <div className='fw5'>Runs</div>
                  </div>
                  <div className='flex h3  justify-between items-center w-50 relative '>
                    <div className='bg-orange br-50' style={{ height: 10, width: 10 }} />
                    <div className='flex justify-center ' style={{ height: 1, width: '95%', borderBottom: '1px dashed gray' }}>
                      <div className='absolute  bottom-[-8px] justify-center'>

                      <img
                        className='ib w-10 h-6 shadow-4 '
                        src={`https://images.cricket.com/teams/${data.dugoutDetails.last18balls.teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = teamFallback)}
                        />
                      <div className='ma1 tc text-gray-2 f6 fw6 '>{data.dugoutDetails.last18balls.teamShortName}</div>
                        </div>
                    </div>
                    <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                  </div>

                  <div className=' w-25 flex flex-column  items-center justify-center'>
                    {' '}
                    <div className='fw5'>{data.dugoutDetails.last18balls.wickets}</div>
                    <div className='fw5'>wickets</div>
                  </div>
                </div>
              </div>
            )}

            {/* RUN RATE */}

            {data &&
              data.dugoutDetails &&
              data.dugoutDetails.runRateProjection &&
              data.dugoutDetails.status == 'live' &&
              data.dugoutDetails.runRateProjection.length > 0 && (
                <RunRate data={data && data.dugoutDetails && data.dugoutDetails.runRateProjection} />
              )}
          </div>
        </div>
      ) : (
        <div className='dn-l text-white'>
          {data && data.dugoutDetails && data.dugoutDetails.completed ? (
            <>
              {' '}
              <div className='w-100 flex items-center justify-center  '>
                <div
                  onClick={() => setActiveTabTeam(data.dugoutDetails.completed[0].battingTeamShortName)}
                  className={`w-15 ba ma1 pa1 flex items-center justify-center ttu cursor-pointer f7  ${
                    data &&
                    data.dugoutDetails &&
                    (data.dugoutDetails.completed[0].battingTeamShortName == activeTabTeam || activeTabTeam == 1)
                      ? 'bg-gray border-red-2 white'
                      : ''
                  } `}>
                  {' '}
                  {data && data.dugoutDetails && data.dugoutDetails.completed[0].battingTeamShortName}{' '}
                </div>
                {data && data.dugoutDetails && data.dugoutDetails.completed && data.dugoutDetails.completed[1] && (
                  <div
                    onClick={() => setActiveTabTeam(data.dugoutDetails.completed[1].battingTeamShortName)}
                    className={`w-15 ba ma1 pa1 flex items-center justify-center ttu cursor-pointer  f7 ${
                      data &&
                      data.dugoutDetails &&
                      data.dugoutDetails.completed &&
                      data.dugoutDetails.completed[1] &&
                      data.dugoutDetails.completed[1].battingTeamShortName == activeTabTeam
                        ? 'bg-gray border-red-2 white'
                        : ''
                    } `}>
                    {' '}
                    {data &&
                      data.dugoutDetails &&
                      data.dugoutDetails.completed &&
                      data.dugoutDetails.completed[1] &&
                      data.dugoutDetails.completed[1].battingTeamShortName}{' '}
                  </div>
                )}
              </div>
              {data &&
              data.dugoutDetails &&
              data.dugoutDetails.status != 'mid' &&
              data.dugoutDetails.completed &&
              data.dugoutDetails.completed &&
              (activeTabTeam == data.dugoutDetails.completed[0].battingTeamShortName || activeTabTeam == 1) ? (
                <div className='w-100 flex items-center justify-center '>
                  <div className=' ma1 w-25 flex items-center justify-center  fw5 f6'>
                    <img
                      className=' ml1 w2 h17'
                      src={`  https://images.cricket.com/teams/${data.dugoutDetails.completed[0].battingTeamID}_flag_safari.png`}
                      onError={(evt) => (evt.target.src = teamFallback)}
                      alt=''
                      srcset=''
                    />
                    <span className='ml1'>{data.dugoutDetails.completed[0].battingTeamShortName}</span>
                  </div>

                  <div className=' ma1 w-15 flex items-center justify-center'>
                    <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>1s</div>
                    <span className='f7'>{data.dugoutDetails.completed[0].scores.ones}</span>
                  </div>
                  <div className=' ma1 w-15 flex items-center justify-center'>
                    <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>2s</div>
                    <span className='f7'>{data.dugoutDetails.completed[0].scores.twos}</span>
                  </div>
                  <div className=' ma1 w-15 flex items-center justify-center'>
                    <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>3s</div>
                    <span className='f7'>{data.dugoutDetails.completed[0].scores.threes}</span>
                  </div>
                  <div className=' ma1 w-15 flex items-center justify-center'>
                    <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>4s</div>
                    <span className='f7'>{data.dugoutDetails.completed[0].scores.fours}</span>
                  </div>
                  <div className=' ma1 w-15 flex items-center justify-center'>
                    <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>6s</div>
                    <span className='f7'>{data.dugoutDetails.completed[0].scores.sixs}</span>
                  </div>
                </div>
              ) : (
                <div>
                  {data.dugoutDetails.status != 'mid' && (
                    <div className='w-100 flex items-center justify-center '>
                      <div className=' ma1 w-25 flex items-center justify-center fw5 f6'>
                        <img
                          className=' ml1 w2 h17'
                          src={`  https://images.cricket.com/teams/${data.dugoutDetails.completed[1].battingTeamID}_flag_safari.png`}
                          alt=''
                          srcset=''
                          onError={(evt) => (evt.target.src = teamFallback)}
                        />
                        <span className='ml1'>{data.dugoutDetails.completed[1].battingTeamShortName}</span>
                      </div>

                      <div className=' ma1 w-15 flex items-center justify-center'>
                        <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>
                          1s
                        </div>
                        <span className='f7'>{data.dugoutDetails.completed[1].scores.ones}</span>
                      </div>
                      <div className=' ma1 w-15 flex items-center justify-center'>
                        <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>
                          2s
                        </div>
                        <span className='f7'>{data.dugoutDetails.completed[1].scores.twos}</span>
                      </div>
                      <div className=' ma1 w-15 flex items-center justify-center'>
                        <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>
                          3s
                        </div>
                        <span className='f7'>{data.dugoutDetails.completed[1].scores.threes}</span>
                      </div>
                      <div className=' ma1 w-15 flex items-center justify-center'>
                        <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>
                          4s
                        </div>
                        <span className='f7'>{data.dugoutDetails.completed[1].scores.fours}</span>
                      </div>
                      <div className=' ma1 w-15 flex items-center justify-center'>
                        <div className=' flex justify-center items-center f7 fw5 white w13 bg-gray  h-6 w-6 br-100 mr1'>
                          6s
                        </div>
                        <span className='f7'>{data.dugoutDetails.completed[1].scores.sixs}</span>
                      </div>
                    </div>
                  )}
                </div>
              )}
              {/* team 1 */}
              {data &&
                data.dugoutDetails.completed &&
                (activeTabTeam == data.dugoutDetails.completed[0].battingTeamShortName || activeTabTeam == 1) &&
                data.dugoutDetails.completed.map((items, index) => {
                  return items.matchBatsman.map((item, index1) => {
                    return (
                      index == 0 && (
                        <CompleteDugut
                          CleaverData={props}
                          callScroll={callScroll}
                          data={item}
                          mainData={data && data.dugoutDetails.completed[0].matchBatsman}
                          popUpWheelIndex={popUpWheelIndex}
                          popUpWheel={popUpWheel}
                          item={item}
                          teamColor={
                            data &&
                            data.dugoutDetails &&
                            data.dugoutDetails.completed &&
                            data.dugoutDetails.completed[0].teamColor
                          }
                          index={index1}
                          setPopWHeelIndex={setPopWHeelIndex}
                          mainIndex={index}
                          teamName={'srh'}
                          setPopWHeel={setPopWHeel}
                        />
                      )
                    );
                  });
                })}
              {data &&
                data.dugoutDetails.completed &&
                data.dugoutDetails.completed &&
                data.dugoutDetails.completed[1] &&
                activeTabTeam == data.dugoutDetails.completed[1].battingTeamShortName &&
                data.dugoutDetails.completed.map((items, index) => {
                  return items.matchBatsman.map((item, index1) => {
                    return (
                      index == 1 && (
                        <CompleteDugut
                          CleaverData={props}
                          callScroll={callScroll}
                          data={item}
                          teamColor={
                            data &&
                            data.dugoutDetails &&
                            data.dugoutDetails.completed &&
                            data.dugoutDetails.completed[1].teamColor
                          }
                          mainData={data && data.dugoutDetails.completed[1].matchBatsman}
                          popUpWheelIndex2={popUpWheelIndex2}
                          popUpWheel2={popUpWheel2}
                          item={item}
                          index={index1}
                          teamIndex={index1}
                          setTeamIndex={setTeamIndex}
                          mainIndex={index}
                          teamName={'dc'}
                          setPopWHeel2={setPopWHeel2}
                          setPopWHeelIndex2={setPopWHeelIndex2}
                        />
                      )
                    );
                  });
                })}
            </>
          ) : (
            <DataNotFound/>
          )}

    
        </div>
      )}
    </>
  );
}
