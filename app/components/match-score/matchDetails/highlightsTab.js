import React, { useState, useEffect, useRef } from 'react'
import {
  GET_ARTICLES_MATCH_INFO_SCORECARD,
  GET_HIGHLIGHTS,
  MATCH_DATA_FOR_SCORECARD,
} from '../../../api/queries'
import * as d3 from 'd3'

import { useQuery } from '@apollo/react-hooks'
import DataNotFound from '../../commom/datanotfound'
import Tab from '../../shared/Tab'
import Loading from '../../loading'
import PieClass from '../../piechart'

const empty = '/svgs/Empty.svg'

// fliter.png

const TabData = [
  { name: 'All', value: 'All' },
  { name: 'Fours', value: 'Fours' },
  { name: 'Sixes', value: 'Sixes' },
  { name: 'Wickets', value: 'Wickets' },
]

const HighlightsTab = ({ matchID, params }) => {
  const { data } = useQuery(MATCH_DATA_FOR_SCORECARD, {
    variables: { matchID: matchID },
    onCompleted: (res) => {
      console.log('datadatadatadatadata12121', res)
    },
  })
  // const { data: scorecardData } = useQuery(GET_ARTICLES_MATCH_INFO_SCORECARD, {
  //   variables: { type: 'matches', Id: matchID },
  // })

  const [finalZadData, setFinalZadData] = useState([])
  const [finalFourData, setFinalFourData] = useState([])

  const [finalsixData, setFinalsixData] = useState([])

  const matchData = data?.miniScoreCard?.data[0]
  const [Team, setTeam] = useState({})
  const [click, setclick] = useState(false)
  const [activeFilter, setActiveFilter] = useState(TabData[0])
  const [ActiveArray, setActiveArray] = useState([])
  const [innings, setInnings] = useState(2)
  const [showHigh, setShowHigh] = useState(false)
  // const [tab, setTab] = useState()
  // const isMounted = useRef(false)
  const [exp, setExp] = useState(false)

  var z1SquarLeg = 0
  var z2FineLeg = 0
  var z3ThirdMan = 0
  var z4Point = 0
  var z5Cover = 0
  var z6MidOff = 0
  var z7MidOn = 0
  var z8MidWicket = 0

  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20,
    }))

  const [dataa, setData] = useState(generateData(0))
  useEffect(() => {
    setData(generateData())
    setExp(window.screen.width > 540 ? true : false)
  }, [!dataa])

  useEffect(() => {
    if (
      matchData &&
      matchData.matchType !== 'Test' &&
      matchData.currentinningsNo > 2
    ) {
      setInnings(2)
    } else if (matchData && matchData.currentinningsNo) {
      setInnings(matchData.currentinningsNo)
    } else {
      setInnings(2)
    }
  }, [matchData, matchData && matchData.currentinningsNo])
  const inningObject = []
  const { loading, error, data: highLightData } = useQuery(GET_HIGHLIGHTS, {
    variables: {
      matchID: matchID,
      innings: `${innings}`,
      type: '',
      page: 1,
    },
    onCompleted: (res) => {
      var zadDatahere = []
      var zadDatahereSix = []

      var zadDatahereFour = []

      let fourTag = { name: 'Fours', value: 'Fours' }
      let sixesTag = { name: 'Sixes', value: 'Sixes' }

      {
        res &&
          res.getHighlights.HighlightBall.filter((x) =>
            activeFilter?.value === 'All'
              ? x
              : activeFilter?.value === 'Fours'
              ? x.runs === '4'
              : activeFilter?.value === 'Sixes'
              ? x.runs === '6'
              : null,
          ).map((x, i) => {
            zadDatahere.push({ runs: x.runs, zadval: x.zad })
          })
      }

      {
        res &&
          res.getHighlights.HighlightBall.filter((x) =>
            fourTag?.value === 'All'
              ? x
              : fourTag?.value === 'Fours'
              ? x.runs === '4'
              : fourTag?.value === 'Sixes'
              ? x.runs === '6'
              : null,
          ).map((x, i) => {
            zadDatahereFour.push({ runs: x.runs, zadval: x.zad })
          })
      }

      {
        res &&
          res.getHighlights.HighlightBall.filter((x) =>
            sixesTag?.value === 'All'
              ? x
              : sixesTag?.value === 'Fours'
              ? x.runs === '4'
              : sixesTag?.value === 'Sixes'
              ? x.runs === '6'
              : null,
          ).map((x, i) => {
            zadDatahereSix.push({ runs: x.runs, zadval: x.zad })
          })
      }

      setFinalZadData(zadDatahere)
      setFinalFourData(zadDatahereFour)
      setFinalsixData(zadDatahereSix)
    },
  })

  const replaceZad = (filterValue) => {
    var zadDatahere = []
    {
      highLightData &&
        highLightData.getHighlights.HighlightBall.filter((x) =>
          filterValue?.value === 'All'
            ? x
            : filterValue?.value === 'Fours'
            ? x.runs === '4'
            : filterValue?.value === 'Sixes'
            ? x.runs === '6'
            : null,
        ).map((x, i) => {
          zadDatahere.push({ runs: x.runs, zadval: x.zad })
        })
    }
    setFinalZadData(zadDatahere)
  }

  const filterTabs = ['All', 'Fours', 'Sixes', 'Wickets']
  useEffect(() => {
    if (matchData) {
      // isMounted.current = true
      matchData?.matchScore &&
        matchData?.matchScore.map((y, j) =>
          y.teamScore.map((x, i) =>
            matchData?.matchType !== 'Test'
              ? i === 0
                ? inningObject.push(x)
                : ''
              : inningObject.push(x),
          ),
        )
      const sortedInning = inningObject.sort((a, b) => {
        return a.inning - b.inning
      })

      setActiveArray(sortedInning)
      setTeam({
        teamID: matchData?.currentInningteamID,
        inning: matchData?.currentinningsNo,
      })
    }
    // setTeam({ teamID: ActiveArray[ActiveArray.length-1].teamID, inning: ActiveArray[ActiveArray.length-1].inning });
  }, [matchData])
  if (loading) return <Loading />
  if (!matchData) {
    return <Loading />
  }
  {
  }

  if (highLightData) {
    // return <div>jjjjjjjjjjjjjjjjjjjjjj</div>
    return (
      <div className="text-white md:px-2">
        {highLightData.getHighlights.HighlightBall.length !== 0 ? (
          <div>
            {matchData.matchType === 'Test' && (
              <div className="dark:py-2 my-2 md:pb-4 tc center relative flex items-center justify-center">
                <div className="">
                  {ActiveArray.map((team, i) => (
                    <span
                      key={i}
                      className={`black-20 cursor-pointer text-xs mx-0.5 font-bold rounded-full px-9 py-2 text-center border ${
                        team.teamID === Team.teamID &&
                        team.inning == Team.inning
                          ? 'text-white bg-red-6 dark:basebg dark:border dark:border-green dark:text-green dark:bg-transparent'
                          : 'bg-light_gray text-black dark:text-white dark:border-gray-4 dark:border-2 dark:border-solid dark:bg-transparent'
                      }`}
                      onClick={() => {
                        setInnings(i + 1)
                        setTeam({ teamID: team.teamID, inning: team.inning })
                        setActiveFilter(TabData[0])
                      }}
                    >
                      {team.inning === 1 || team.inning === 2
                        ? `${team.battingTeamShortName} 1`
                        : `${team.battingTeamShortName} 2`}
                    </span>
                  ))}
                </div>
              </div>
            )}

            {matchData.matchType !== 'Test' && (
              <div className="dark:py-2 my-2 md:pb-4 tc center relative flex items-center justify-center">
                <div className="flex items-center justify-center md:w-6/12 w-8/12 rounded-full">
                  {ActiveArray.map(
                    (team, i) =>
                      i <= 1 && (
                        <span
                          key={i}
                          className={`black-20 mx-0.5 cursor-pointer text-xs font-bold rounded-full px-14 py-2 text-center border ${
                            (i === 1 && click === false) ||
                            (team.teamID === Team.teamID &&
                              team.inning == Team.inning)
                              ? 'text-white bg-red-6 dark:basebg dark:border dark:border-green dark:text-green dark:bg-gray'
                              : ' bg-light_gray text-black dark:text-white dark:border-gray-4 dark:bg-gray'
                          }`}
                          onClick={() => {
                            setInnings(i + 1)
                            setclick(true),
                              setTeam({
                                teamID: team.teamID,
                                inning: team.inning,
                              })
                            setActiveFilter(TabData[0])
                          }}
                        >
                          {team.battingTeamShortName}{' '}
                        </span>
                      ),
                  )}
                </div>
              </div>
            )}

            <div className="flex justify-center items-center flex-col mt-4 md:mt-0">
              <div className="flex m-1 justify-center items-center relative dark:w-44 dark:h-44 w-44 h-44 ">
                <img
                  className="dark:w-44 dark:h-44 w-44 h-44 absolute"
                  src={
                    activeFilter?.value === 'Wickets'
                      ? '/svgs/groundImageWicket.png'
                      : '/svgs/ground.png'
                  }
                  alt=""
                />
                <div
                  className=" absolute overflow-hidden rounded-full"
                  style={{
                    width: window.screen.width > 975 ? 210 : 202,
                    height: window.screen.width > 975 ? 210 : 202,
                  }}
                >
                  {highLightData.getHighlights.HighlightBall.filter((x) =>
                    activeFilter?.value === 'All'
                      ? x
                      : activeFilter?.value === 'Fours'
                      ? x.runs === '4'
                      : activeFilter?.value === 'Sixes'
                      ? x.runs === '6'
                      : null,
                  ).map((x, i) => (
                    <div className="flex w-full overflow-hidden ">
                      {console.log('dez', x)}
                      <div
                        key={i}
                        className={`border-[.3px] overflow-hidden z-0 absolute ${
                          x.runs === '0'
                            ? 'hidden'
                            : x.runs === '6'
                            ? ' border-blue'
                            : ' border-white'
                        } `}
                        style={{
                          bottom: '55%',
                          left: '50%',
                          right: '0%',
                          overflow: 'hidden',
                          transform: `rotate(${360 - x.zad.split(',')[1]}deg)`,
                          transformOrigin: '0 0',
                        }}
                      ></div>

                      <div className="relative border rounded-full border-basebg z-50 over ">
                        {i == 0 && (
                          <PieClass
                            data={dataa}
                            zoneRun={[
                              z2FineLeg,
                              z1SquarLeg,
                              z8MidWicket,
                              z7MidOn,
                              z6MidOff,
                              z5Cover,
                              z4Point,
                              z3ThirdMan,
                            ]}
                            width={window.screen.width > 975 ? 210 : 203}
                            height={window.screen.width > 975 ? 210 : 203}
                            battingStyle={x.battingStyle}
                            zadData={
                              x.zad
                                ? activeFilter?.value.toLowerCase() == 'all'
                                  ? finalZadData
                                  : activeFilter?.value.toLowerCase() == 'fours'
                                  ? finalFourData
                                  : finalsixData
                                : []
                            }
                            innerRadius={window.screen.width > 975 ? 82 : 82}
                            outerRadius={window.screen.width > 975 ? 104 : 100}
                          />
                        )}
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div className="flex md:hidden gap-2 mt-5 ">
                <span className="w-2 h-2 mt-1 bg-white rounded-full"></span>

                <span className="text-xs">4s</span>
                <span className="w-2 h-2 mt-1 bg-blue rounded-full"></span>

                <span className="text-xs">6s</span>
              </div>

              {activeFilter?.value === 'All' ? (
                <div className="f7 dark:text-gray-2 text-center py-2 font-medium text-xs">
                  <span className="text-[#555A64]">TOTAL </span>
                  <span>
                    <span className="text-[#555A64]">4's: </span>
                    <span className="mr-2 font-bold dark:text-white text-black text-sm">
                      {highLightData.getHighlights.totalCount.totalFours}
                    </span>
                  </span>
                  <span>
                    <span className="text-[#555A64]">TOTAL </span>
                    <span className="text-[#555A64]">6's: </span>
                    <span className=" mr-2 font-bold dark:text-white text-black text-sm ">
                      {highLightData.getHighlights.totalCount.totalSix}
                    </span>
                  </span>
                  <span>
                    <span className="text-[#555A64]">Wickets: </span>
                    <span className="mr-2 font-bold dark:text-white text-black text-sm">
                      {highLightData.getHighlights.totalCount.totalWickets}
                    </span>
                  </span>
                </div>
              ) : (
                ''
              )}
              {activeFilter?.value === 'Fours' ? (
                <div className="f7 text-gray-2  font-medium tc py-2">
                  <span>
                    <span>Fours: </span>
                    <span className=" mr2 ">
                      {highLightData.getHighlights.totalCount.totalFours}
                    </span>
                  </span>
                </div>
              ) : (
                ''
              )}
              {activeFilter?.value === 'Sixes' ? (
                <div className="f7 text-gray-2  font-medium tc py-2">
                  <span>
                    <span>Sixes: </span>
                    <span className=" mr2 ">
                      {highLightData.getHighlights.totalCount.totalSix}
                    </span>
                  </span>
                </div>
              ) : (
                ''
              )}
              {activeFilter?.value === 'Wickets' ? (
                <div className="f7 tc py-2 text-gray-2 font-medium">
                  <span>
                    <span>Wickets: </span>
                    <span className=" mr2 ">
                      {highLightData.getHighlights.totalCount.totalWickets}
                    </span>
                  </span>
                </div>
              ) : (
                ''
              )}
            </div>
            {Team && (
              <div>
                <div className="font-medium dark:mx-1 dark:py-2">
                  {/* {filterTabs.map((value, key) => (
                    <div
                      key={key}
                      className={` cursor-pointer w-16 mx-2 py-2 flex justify-center rounded ${
                        activeFilter?.value === value
                          ? 'bg-basebg border-2 border-green'
                          : 'bg-gray-4 '
                      }`}
                      onClick={() => setActiveFilter(value)}
                    >
                      {value == 'Fours' ? '4s' : ''}
                      {value == 'Sixes' ? '6s' : ''}

                      {value == 'Wickets' ? 'Wkts' : ''}

                      {value == 'All' ? 'All' : ''}
                    </div>
                  ))} */}
                  {/* <div
                    onClick={() => setShowHigh(() => !showHigh)}
                    className="w-5 h-5 flex items-center justify-center border rounded-full"
                  >
                    X
                  </div> */}
                  <div className="lg:hidden md:hidden xl:hidden w-full">
                    <Tab
                      data={TabData}
                      type="block"
                      handleTabChange={(item) => {
                        setActiveFilter(item)
                      }}
                      selectedTab={activeFilter}
                    />
                  </div>
                  <div className="lg:block xl:block md:block hidden">
                    <Tab
                      data={TabData}
                      type="line"
                      handleTabChange={(item) => setActiveFilter(item)}
                      selectedTab={activeFilter}
                    />
                  </div>
                </div>

                <div className="overflow-y-scroll">
                  {highLightData?.getHighlights?.HighlightBall?.filter((x) =>
                    activeFilter?.value === 'All'
                      ? x
                      : activeFilter?.value === 'Fours'
                      ? x.runs === '4'
                      : activeFilter?.value === 'Sixes'
                      ? x.runs === '6'
                      : activeFilter?.value === 'Wickets'
                      ? x.wicket === true
                      : null,
                  ).map((x, i) => (
                    <div key={i}>
                      <div className="flex p-2 dark:justify-center justify-start items-center">
                        <div className="flex-col flex justify-center items-center bg-[#EEEFF2] dark:bg-gray py-2 dark:w-2/12 w-14 dark:mx-1 rounded-lg dark:overflow-x-scroll overflow-y-hidden hidescroll">
                          <div
                            key={i}
                            className="text-center w-full flex items-center justify-center border-b-2 dark:border-black border-gray-11 pb-2"
                          >
                            {x.runs.includes('W') ? (
                              <div className="flex justify-center items-center  black w-8 h-8 rounded-full bg-red white text-xs font-bold">
                                W
                              </div>
                            ) : (
                              <>
                                {activeFilter?.value !== 'Wickets' ? (
                                  <div
                                    className={`flex justify-center items-center text-xs font-bold black w-8 h-8 rounded-full ${
                                      parseInt(x.runs) === 4
                                        ? 'bg-green white b--green'
                                        : parseInt(x.runs) === 6
                                        ? 'bg-green white b--green text-xs font-bold'
                                        : 'bg-red text-white text-xs font-bold '
                                    } `}
                                  >
                                    {x.runs == '0' ? 'W' : x.runs}
                                  </div>
                                ) : (
                                  <div
                                    className={`flex justify-center items-center bg-red-4 text-xs font-bold  black w-8 h-8 rounded-full 
                           `}
                                  >
                                    {x.runs == '0' ? 'W' : x.runs}
                                  </div>
                                )}
                              </>
                            )}
                          </div>
                          {true && (
                            <div
                              className={`flex w-full justify-center items-center text-xs font-bold text-black text-center`}
                            >
                              <p className="text-center w-full text-black dark:text-white font-semibold pt-1">
                                {x.over}{' '}
                              </p>
                            </div>
                          )}
                        </div>

                        <div className="f8 ml-1 text-xs text-gray-2 w-10/12">
                          {' '}
                          {x.commentary}{' '}
                        </div>
                      </div>

                      {i ==
                        highLightData.getHighlights.HighlightBall.filter((x) =>
                          activeFilter?.value === 'All'
                            ? x
                            : activeFilter?.value === 'Fours'
                            ? x.runs === '4'
                            : activeFilter?.value === 'Sixes'
                            ? x.runs === '6'
                            : activeFilter?.value === 'Wickets'
                            ? x.wicket === true
                            : null,
                        ).length -
                          1 && <div className="h-2"> </div>}
                    </div>
                  ))}
                </div>

                {/* <div
                          className={` flex justify-center items-center text-xs font-bold font-bold black w-8 h-8 rounded-full ${
                            parseInt(ball.runs) === 4
                              ? 'bg-green white b--green'
                              : parseInt(ball.runs) === 6
                              ? 'bg-green white b--green text-xs font-bold'
                              : 'bg-gray-2 text-black text-xs font-bold '
                          } `}>
                          {getRuns(ball.runs)}
                        </div> */}
              </div>
            )}
          </div>
        ) : (
          <DataNotFound />
        )}
      </div>
    )
  }
}
export default HighlightsTab
