import React, { useState } from 'react'
import { format } from 'date-fns'
import VenuDeatils from './venueDetail'
import Link from 'next/link'
import {
  STADIUM_VIEW,
  PLAYER_VIEW,
  SERIES_DETAILS,
} from '../../../constant/Links'
import { useRouter } from 'next/navigation'
import CleverTap from 'clevertap-react'
import Heading from '../../commom/heading'
import Loading from '../../loading'
const upArrowBlack = '/svgs/RightSchevronBlack.svg'
const downArrowBlack = '/svgs/downArrowBlack.svg'
const ManOfMatch = '/pngsV2/playerph.png'
const ALLPIC = '/pngsV2/allrounder-line-white.png'
const WKPIC = '/pngsV2/keeper-line-white.png'

const BATPIC = '/pngsV2/bat-line-white.png'
const BALLPIC = '/pngsV2/ball-line-white.png'

export default function MatchInfoTab({
  data,
  matchData,
  newData,
  match,
  ...props
}) {
  console.log('data-------', matchData, 'matchInfo', data, 'newData', newData)
  const [homeXI, setHomeXI] = useState(false)
  const [awayXI, setAwayXI] = useState(false)
  // const []
  const [HomeTeam, setHomeTeam] = useState(0)
  const [showTeam, setShowTeam] = useState(false)

  const PlayerRoleImg = (playerRole) => {
    // alert(playerRole)
    if (playerRole === 'wicket-keeper') {
      return WKPIC
    }
    if (playerRole === 'batsman') {
      return BATPIC
    }
    if (playerRole === 'all-rounder') {
      return ALLPIC
    }
    if (playerRole === 'bowler') {
      return BALLPIC
    }
  }

  let router = useRouter()
  let navigate = router.push
  const handleVenueNavigation = (venueID) => {
    CleverTap.initialize('Stadiums', {
      Source: 'MatchInfo',
      VenueID: venueID,
      Platform: localStorage ? localStorage.Platform : '',
    })
  }
  const getPlayerUrl = (player) => {
    let playerSlug = player.playerName.split(' ').join('-').toLowerCase()
    let playerId = player.playerID
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href,
    }
  }
  const getStadiumUrl = (matchInfo) => {
    let stadiumId = matchInfo.venueID
    let stadiumSlug = matchInfo.venue
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase()
    return {
      as: eval(STADIUM_VIEW.as),
      href: STADIUM_VIEW.href,
    }
  }
  const getSeriesUrl = (match) => {
    // @needToChange need to add dynamic type
    let type = match && match.type && match.type.toLowerCase()

    let currentTab = 'matches'
    let seriesName =
      match &&
      match.seriesName
        .replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
    seriesName = `${seriesName}`
    let seriesId = match && match.seriesID
    return { as: eval(SERIES_DETAILS.as), href: SERIES_DETAILS.href }
  }
  const handleSeriesNavigation = () => {
    CleverTap.initialize('Series', {
      Source: 'MatchInfoTab',
      Series: matchData.seriesID,
      TeamAID: matchData.homeTeamID,
      TeamBID: matchData.awayTeamID,
      Platform: localStorage ? localStorage.Platform : '',
    })
  }
  {
  }
  {
    console.log(data, 'data**')
  }
  if (data?.length == 0) {
    return <Loading />
  }
  if (!data)
    return (
      <div className="w-full vh-100 fw2 f7 gray flex flex-column  justify-center items-center">
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  else {
    return (
      <div className="dark:text-white md:px-2">
        {/* series name */}
        {/* <div className='bg-black h-[1px] w-full' /> */}

        <div className="flex flex-col justify-between m-2">
          <div className="flex items-center lg:hidden md:hidden justify-between">
            <Heading heading="Tournament" />
            <div className="bg-gray rounded p-0.5 mb-1">
              <Link
                // {...getSeriesUrl(data.getMatchInfo)}
                href={`/series/${newData?.getMatchCardTabWiseByMatchID?.type}/${
                  matchData?.miniScoreCard?.data?.[0]?.seriesID
                }/${matchData?.miniScoreCard?.data?.[0]?.seriesName
                  ?.replace(/[^a-zA-Z0-9]+/g, ' ')
                  ?.split(' ')
                  ?.join('-')
                  ?.toLowerCase()}/matches`}
                passHref
                legacyBehavior
              >
                <img
                  className="md:cursor-pointer h-6 w-6"
                  src="/pngsV2/arrow.png"
                  alt=""
                  // onClick={handleSeriesNavigation}
                />
              </Link>
            </div>
          </div>

          <div className="lg:flex md:flex items-center hidden justify-between mb-4 border-b-gray-11 border-b-solid border-b-2">
            <span className="text-sm py-1">
              {newData &&
                newData.getMatchCardTabWiseByMatchID &&
                newData.getMatchCardTabWiseByMatchID.tourName}
            </span>
            {console.log(matchData, 'haanji')}
            <div className="bg-gray rounded p-0.5 mb-1">
              <Link
                // {...getSeriesUrl(data.getMatchInfo)}
                href={`/series/${newData?.getMatchCardTabWiseByMatchID?.type}/${
                  matchData?.miniScoreCard?.data?.[0]?.seriesID
                }/${matchData?.miniScoreCard?.data?.[0]?.seriesName
                  ?.replace(/[^a-zA-Z0-9]+/g, ' ')
                  ?.split(' ')
                  ?.join('-')
                  ?.toLowerCase()}/matches`}
                passHref
                legacyBehavior
              >
                <img
                  className="md:cursor-pointer h-6 w-6"
                  src="/pngsV2/arrow.png"
                  alt=""
                  // onClick={handleSeriesNavigation}
                />
                {/* <div>nnnnn</div> */}
              </Link>
            </div>
          </div>

          <span className="text-sm py-1 lg:hidden md:hidden xl:hidden">
            {' '}
            {newData &&
              newData.getMatchCardTabWiseByMatchID &&
              newData.getMatchCardTabWiseByMatchID.tourName}
          </span>
        </div>
        {/* <div className='bg-black h-[1px] w-full' /> */}

        {/* match details  newData.getMatchCardTabWiseByMatchID.matchDetails.toss*/}

        <div className="p-2 font-medium text-xs">
          <p className="lg:hidden md:hidden xl:hidden">
            <Heading heading="Match Details" />
          </p>

          <div className="py-2 flex dark:justify-between items-center ">
            <div className="w-2/12 dark:w-6/12 text-xs dark:font-semibold font-normal">
              Date & Time
            </div>
            <div className="w-6/12 text-sm">
              {/* {format(
                new Date(
                  newData?.getMatchCardTabWiseByMatchID?.matchDetails?.date,
                ) - 19800000,
                'do MMM yyyy, h:mm a',
              )} */}

              {(newData &&
                newData.getMatchCardTabWiseByMatchID?.matchDetails &&
                format(
                  new Date(
                    +newData?.getMatchCardTabWiseByMatchID?.matchDetails?.date -
                      19800000,
                  ),
                  'dd MMM yyyy | hh:mm a',
                )) ||
                ''}
            </div>
          </div>
          <div className="py-2 flex dark:justify-between items-center">
            <span className="dark:w-6/12 w-2/12 text-xs font-normal dark:font-semibold ">
              Match Number
            </span>
            <span className=" w-6/12 text-left text-sm">
              {' '}
              {newData?.getMatchCardTabWiseByMatchID?.matchDetails
                ?.matchNumber || '-'}{' '}
            </span>
          </div>
          <div className="py-2 flex dark:justify-between items-center">
            <span className="dark:w-6/12 w-2/12 text-xs font-normal dark:font-semibold">
              Toss Result
            </span>
            <span className=" w-6/12 text-left  text-sm">
              {' '}
              {newData?.getMatchCardTabWiseByMatchID?.matchDetails?.toss ||
                '-'}{' '}
            </span>
          </div>
        </div>

        {/* playing XI */}
        {}
        {}
        {props?.matchData?.matchStatus && (
          <>
            <div className="flex flex-col items-center justify-between dark:bg-gray bg-gray-10 rounded-md px-2 pt-2 pb-4 m-2">
              {data?.getMatchInfo?.homePlayingXI &&
              newData?.getMatchCardTabWiseByMatchID ? (
                <div className=" w-full flex items-center justify-between">
                  <div className="dark:font-bold font-semibold dark:text-white text-black text-base">
                    Check Probable 11 <br />
                  </div>
                  <div className="mt-2">
                    <img
                      className="cursor-pointer rounded-xl bg-basebg h-10 w-10 p-1"
                      src={!showTeam ? '/pngsV2/plus.png' : '/pngsV2/minus.png'}
                      alt=""
                      onClick={() => setShowTeam(!showTeam)}
                    />
                  </div>
                </div>
              ) : (
                <div className=" w-full flex items-center justify-between">
                  <div className="font-bold text-base text-black dark:text-white ">
                    Playing 11 for this match is not available ! <br />
                  </div>
                </div>
              )}

              {showTeam &&
                data?.getMatchInfo?.homePlayingXI &&
                newData?.getMatchCardTabWiseByMatchID && (
                  <div className="w-full flex items-center justify-center">
                    <div className="flex items-center justify-center dark:w-10/12 w-5/12 bg-gray-4 rounded-2xl text-white">
                      <div
                        className={`w-6/12 rounded-2xl ${
                          HomeTeam == 0 ? 'border border-green bg-basebg' : ''
                        } p-1 `}
                        onClick={() => setHomeTeam(0)}
                      >
                        <div className="flex items-center justify-center ">
                          <span className="pl-2 text-sm font-semibold ">
                            {newData.getMatchCardTabWiseByMatchID &&
                              newData.getMatchCardTabWiseByMatchID.squad
                                .length > 0 &&
                              newData.getMatchCardTabWiseByMatchID.squad[0]
                                .teamShortName}{' '}
                          </span>
                        </div>
                      </div>
                      <div
                        className={`w-6/12 rounded-2xl ${
                          HomeTeam == 1 ? ' border border-green bg-basebg' : ''
                        } p-1 `}
                        onClick={() => setHomeTeam(1)}
                      >
                        <div className="flex items-center justify-center">
                          {/* <img
                    className='ib h1 w15 shadow-4 '
                    alt='location'
                    src={`https://images.cricket.com/teams/${data.getMatchInfo.homeTeamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                  /> */}
                          <span className="pl-2 text-sm font-semibold ">
                            {' '}
                            {newData.getMatchCardTabWiseByMatchID &&
                              newData.getMatchCardTabWiseByMatchID.squad
                                .length > 1 &&
                              newData.getMatchCardTabWiseByMatchID.squad[1]
                                .teamShortName}{' '}
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
            </div>
          </>
        )}

        {/* Team View */}
        {props?.matchData?.matchStatus && (
          <div>
            <div className="py-2">
              {showTeam && (
                <div className="">
                  {data?.getMatchInfo?.homePlayingXI &&
                    newData?.getMatchCardTabWiseByMatchID?.squad[
                      HomeTeam
                    ].playingxi.map((player, i) => (
                      <React.Fragment key={i}>
                        <div className="flex items-center justify-between dark:bg-gray bg-white border-2 border-solid dark:border-basebg border-[#E2E2E2] p-1 rounded-md mx-3 my-2">
                          <div className=" flex items-center justify-center">
                            <div className="relative bg-gray-8 w-12 h-12 rounded-full flex items-center justify-center">
                              <img
                                className="h-10 w-10 overflow-hidden rounded-full object-top object-cover absolute bottom-0 "
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                alt=""
                                onError={(evt) => (evt.target.src = ManOfMatch)}
                              />
                            </div>
                            <Link
                              {...getPlayerUrl(player)}
                              passHref
                              onClick={() =>
                                CleverTap.initialize('Players', {
                                  Source: 'MatchInfoTab',

                                  Platform: localStorage
                                    ? localStorage.Platform
                                    : '',
                                })
                              }
                              className="f7 fw5 cursor-pointer"
                            >
                              {' '}
                              <span className="f7 fw5 cursor-pointer pl-2">
                                {player.playerName}
                              </span>
                              <span className="pl1 fw6 f7">
                                {' '}
                                {player.captain && player.keeper
                                  ? '(c) & (Wk)'
                                  : player.captain
                                  ? '(c)'
                                  : player.keeper
                                  ? '(Wk)'
                                  : ''}
                              </span>
                            </Link>
                          </div>
                          <div className="border lg:bg-gray-3 md:bg-gray-3 p-1 rounded-md">
                            {/* {player.playerRole.toLowerCase()} */}
                            {/* {PlayerRoleImg(player.playerRole.toLowerCase())} */}
                            <img
                              className="h-3 w-3  "
                              src={PlayerRoleImg(
                                player.playerRole.toLowerCase(),
                              )}
                              alt=""
                              srcset=""
                            />
                          </div>
                        </div>
                      </React.Fragment>
                    ))}
                </div>
              )}

              {showTeam && (
                <div className="flex ml-2 lg:my-4 md:my-4 font-bold dark:text-xs text-base capitalize">
                  Bench{' '}
                </div>
              )}

              {showTeam && (
                <div className="">
                  {data?.getMatchInfo?.homePlayingXI &&
                    newData?.getMatchCardTabWiseByMatchID?.squad[
                      HomeTeam
                    ].benchplayers.map((player, i) => (
                      <React.Fragment key={i}>
                        <div className="flex items-center justify-between dark:bg-gray bg-white p-1 border-2 border-solid dark:border-basebg border-[#E2E2E2] rounded-md mx-3 my-2">
                          <div className=" flex items-center justify-center">
                            <div className="relative bg-gray-8 w-12 h-12 rounded-full flex items-center justify-center">
                              <img
                                className="h-10 w-10 overflow-hidden rounded-full  object-top object-cover   absolute bottom-0 "
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                alt=""
                                onError={(evt) => (evt.target.src = ManOfMatch)}
                              />
                            </div>
                            <Link
                              {...getPlayerUrl(player)}
                              passHref
                              onClick={() =>
                                CleverTap.initialize('Players', {
                                  Source: 'MatchInfoTab',

                                  Platform: localStorage
                                    ? localStorage.Platform
                                    : '',
                                })
                              }
                              className="f7 fw5 cursor-pointer"
                            >
                              {' '}
                              <span className="f7 fw5 cursor-pointer pl-2">
                                {player.playerName}
                              </span>
                              <span className="pl1 fw6 f7">
                                {' '}
                                {player.captain && player.keeper
                                  ? '(c) & (Wk)'
                                  : player.captain
                                  ? '(c)'
                                  : player.keeper
                                  ? '(Wk)'
                                  : ''}
                              </span>
                            </Link>
                          </div>
                          <div className="border p-1 lg:bg-gray-3 md:bg-gray-3 rounded-md">
                            {/* {player.playerRole.toLowerCase()} */}
                            {/* {PlayerRoleImg(player.playerRole.toLowerCase())} */}
                            <img
                              className="h-3 w-3  "
                              src={PlayerRoleImg(
                                player.playerRole.toLowerCase(),
                              )}
                              alt=""
                              srcset=""
                            />
                          </div>
                        </div>
                      </React.Fragment>
                    ))}
                </div>
              )}

              {/* <div className='pv3 flex justify-between items-center' onClick={() => setAwayXI(!awayXI)}>
                <div className='flex items-center'>
                  <img
                    className='ib h1 w15 shadow-4 '
                    alt='location'
                    src={`https://images.cricket.com/teams/${data.getMatchInfo.awayTeamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                  />
                  <span className='pl2 f6 fw6'> {data.getMatchInfo.awayTeamShortName} </span>
                </div>
                <img className={`w1 h1 cursor-pointer ${awayXI ? 'rotate-270' : 'rotate-90'}`} src={upArrowBlack} alt={''} />
              </div> */}
              <div className="dark:bg-black bg-gray-11 h-[1px] w-full" />
              {false && showTeam && !HomeTeam && (
                <div className="">
                  {newData.getMatchCardTabWiseByMatchID &&
                    newData.getMatchCardTabWiseByMatchID.squad &&
                    newData.getMatchCardTabWiseByMatchID.squad[1].playingxi.map(
                      (player, i) => (
                        <React.Fragment key={i}>
                          <div className="flex items-center pv2">
                            <div className="w-8 h-8 bg-gray rounded-full">
                              <img
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                alt=""
                                onError={(evt) => (evt.target.src = ManOfMatch)}
                              />
                            </div>
                            <Link {...getPlayerUrl(player)} passHref>
                              <span className="f7 fw5 cursor-pointer pl-2">
                                {player.playerName}
                              </span>
                              <span className="pl1 fw6 f7">
                                {' '}
                                {player.captain &&
                                player.playerRole === 'Wicket-Keeper'
                                  ? '(c) & (Wk)'
                                  : player.captain
                                  ? '(c)'
                                  : player.playerRole === 'Wicket-Keeper'
                                  ? '(Wk)'
                                  : ''}
                              </span>
                            </Link>
                          </div>
                          <div className="dark:bg-black bg-gray-11 h-[1px] w-full" />
                        </React.Fragment>
                      ),
                    )}
                </div>
              )}
            </div>
          </div>
        )}

        <VenuDeatils
          data={newData?.getMatchCardTabWiseByMatchID}
          venueStatsData={newData?.getMatchCardTabWiseByMatchID?.venueDetails}
          venueName={newData?.getMatchCardTabWiseByMatchID?.venue}
        />
        <div className="bg-black h-[1px] w-full" />

        {/* <div className='flex justify-between p-2.5'>
          <span className='f7 fw5'> {data.getMatchInfo.venue} </span>
          <Link {...getStadiumUrl(data.getMatchInfo)} passHref>
            <a>
              <img
                className='cursor-pointer'
                src='/svgs/RightSchevronBlack.svg'
                onClick={() => handleVenueNavigation(data.getMatchInfo.venueID)}
                alt=''
              />
            </a>
          </Link>
        </div> */}

        {/* Match officials */}

        <div className="flex mt-4 p-2 flex-col">
          <Heading heading="Match Officials" />
          <div className="flex  items-center justify-between mt-2">
            <div className="flex w-5/12 text-sm text-gray-2">Umpire:</div>
            <div className="flex dark:text-white text-black w-6/12 text-left text-sm">
              {newData?.getMatchCardTabWiseByMatchID?.matchOfficials?.umpire
                ? newData?.getMatchCardTabWiseByMatchID?.matchOfficials?.umpire
                : '-'}
            </div>
          </div>
          <div className="flex  items-center justify-between mt-2">
            <div className="flex w-5/12 text-sm text-gray-2">Third Umpire:</div>
            <div className="flex w-6/12 text-black dark:text-white text-left text-sm">
              {newData?.getMatchCardTabWiseByMatchID?.matchOfficials
                .thirdrdumpire
                ? newData?.getMatchCardTabWiseByMatchID?.matchOfficials
                    .thirdrdumpire
                : '-'}
            </div>
          </div>
          <div className="flex  items-center justify-between mt-2">
            <div className="flex w-5/12 text-sm text-gray-2">
              Match Referee:
            </div>
            <div className="flex w-6/12 text-left dark:text-white text-black text-sm">
              {newData?.getMatchCardTabWiseByMatchID?.matchOfficials?.referee
                ? newData?.getMatchCardTabWiseByMatchID?.matchOfficials?.referee
                : '-'}
            </div>
          </div>
        </div>

        {/* <div className='bg-black h-[1px] w-full' />
        <div className='flex justify-between p-2.5 bg-gray'>
          <span className='f7 fw5 '> MATCH OFFICIALS </span>
        </div>
        <div className='bg-black h-[1px] w-full' />

       

        <div className='ph2 font-medium'>
          <div className='py-2 flex justify-between items-center'>
            <span className='f7 tl w-40'>Umpires</span>
            <span className='f7 fw6 tl w-60'> {data.getMatchInfo?.umpires || '-'} </span>
          </div>
          <div className='pv2 flex justify-between items-center'>
            <span className='f7 tl w-40'>Third Umpire</span>
            <span className='f7 fw6 tl w-60'> {data.getMatchInfo?.thirdUmpire || '-'} </span>
          </div>
          <div className='pv2 flex justify-between items-center'>
            <span className='f7 tl w-40'>Match Referee</span>
            <span className='f7 fw6 tl w-60'> {data.getMatchInfo?.matchReferee || '-'} </span>
          </div>
        </div> */}
      </div>
    )
  }
}
