import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { MINI_SCORE_CARD } from '../../../api/queries'
// import Ballbyball from "./BallbyballTab";
// import Overbyover from "./overbyoverTab";
// import Quickbytes from "../Home/quickBytes";
// import Commentary from "./commentary";
// import OverbyOverV2 from "./overbyOverV2";
export default function ScorecardTab(props) {
  const [tab, setTab] = useState('BALL BY BALL')

  const [isRefresh, setisRefresh] = useState(false)

  const { loading, error, data } = useQuery(MINI_SCORE_CARD, {
    variables: { matchID: props.matchData.matchID },
  })

  if (error) return <></>
  if (loading || !data) return <p>Loading...</p>
  else
    return (
      <>
        <div className="dark:bg-gray-4 bg-white">
          <div className="">
            {/* <table className="w-full">
              <thead className="dark:bg-gray bg-white">
                <tr className="">
                  <th className="dark:w-50 w-50 px-3 lg:py-2 md:py-2 xl:py-2 text-xs font-semibold text-left pl-3 dark:text-white text-[#221F20B3] dark:border-none border-b-2 border-b-gray-10 border-b-solid">
                    BATTER
                  </th>
                  <th className="dark:w-10 text-xs dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold text-right px-2 dark:text-white text-[#221F20B3]">
                    R
                  </th>
                  <th className="dark:w-10 text-xs dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold text-right px-2 dark:text-white text-[#221F20B3]">
                    B
                  </th>
                  <th className="dark:w-10 text-xs dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold text-right px-2 dark:text-white text-[#221F20B3]">
                    4s
                  </th>
                  <th className="dark:w-10 text-xs dark:border-none  border-b-solid border-b-2 border-b-gray-10 font-semibold text-right px-2 dark:text-whitete text-[#221F20B3]">
                    6s
                  </th>
                  <th className="dark:w-10 text-xs dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  dark:text-right px-2 dark:text-white text-[#221F20B3]">
                    SR
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.miniScoreCard.batting.map((batsmen, i) => (
                  <tr key={i}>
                    <td className="border-black text-left font-medium text-xs py-3 pl-2">
                      {batsmen.playerName}
                      {batsmen.playerOnStrike === true ? '*' : ''}
                    </td>
                    <td className="text-xs font-medium px-2 text-center">
                      {batsmen.runs}
                    </td>
                    <td className="text-xs font-medium px-2 text-center">
                      {batsmen.playerMatchBalls}
                    </td>
                    <td className="text-xs font-medium px-2 text-center">
                      {batsmen.fours}
                    </td>
                    <td className="text-xs font-medium px-2 text-center">
                      {batsmen.sixes}
                    </td>
                    <td className="text-xs font-medium lg:flex md:flex xl:flex lg:items-center xl:items-center md:items-center dark:text-right lg:mt-3 md:mt-3 xl:mt-3 lg:justify-center md:justify-center xl:justify-center">
                      {batsmen.playerMatchStrikeRate}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table> */}
            <table className="w-full">
              <thead className="dark:bg-gray bg-white">
                <tr className="">
                  <th className="w-7/12 px-3 lg:py-2 md:py-2 xl:py-2 py-2 dark:text-xs text-sm font-semibold text-left pl-3 dark:text-white text-[#221F20B3] dark:border-none border-b-2 border-b-gray-10 border-b-solid">
                    BATTER
                  </th>
                  <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                    R
                  </th>
                  <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                    B
                  </th>
                  <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                    4S
                  </th>
                  <th className="w-1/12 dark:text-xs text-sm dark:border-none  border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                    6S
                  </th>
                  <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold   px-2 dark:text-white text-[#221F20B3]">
                    SR
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.miniScoreCard.batting.map((batsmen, i) => (
                  <tr key={i}>
                    <td className="border-black flex w-7/12 text-left font-medium dark:text-xs text-sm py-3 pl-3">
                      {batsmen.playerName}
                      {batsmen.playerOnStrike === true ? (
                        <div className="w-2 h-2 rounded-full md:mt-1.5 mt-1 ml-1 bg-green-600"></div>
                      ) : (
                        ''
                      )}
                    </td>
                    <td className="dark:text-xs text-sm font-medium px-2 text-center">
                      {batsmen.runs}
                    </td>
                    <td className="dark:text-xs text-sm font-medium px-2 text-center">
                      {batsmen.playerMatchBalls}
                    </td>
                    <td className="dark:text-xs text-sm w-1/12 dark:text-white text-[#221F20B3] text-center font-medium  ">
                      {batsmen.fours}
                    </td>
                    <td className="dark:text-xs text-sm w-1/12 text-center font-medium dark:text-white text-[#221F20B3]">
                      {batsmen.sixes}
                    </td>
                    <td className="dark:text-xs text-sm w-1/12 text-center font-medium dark:text-white text-[#221F20B3]">
                      {batsmen.playerMatchStrikeRate}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <table className="w-full px-2" style={{ borderSpacing: 0 }}>
              <thead className="dark:bg-gray bg-white">
                <tr className="flex justify-between items-center px-2 dark:border-gray-4 border-y-2 border-y-solid border-y-gray-11">
                  <th
                    className={`flex text-left w-20 lg:ml-1 md:ml-1 xl:ml-1 w-25-l dark:text-white text-[#646262] font-medium dark:text-xs text-sm`}
                  >
                    P'SHIP:
                    <span className="font-semibold">
                      {' '}
                      {data.miniScoreCard.partnership}
                    </span>
                  </th>
                  {props.matchData.matchType === 'Test' &&
                    data.miniScoreCard.oversRemaining != '0.0' && (
                      <th className="py-3 w-30 text-left dark:text-white text-[#646262] w-[25%] font-medium dark:text-xs text-sm">
                        OVS REM:
                        <span className="font-semibold">
                          {data.miniScoreCard.oversRemaining}
                        </span>
                      </th>
                    )}
                  <th
                    className={`font-medium py-3 dark:text-white lg:mr-4 md:mr-4 xl:mr-4 text-[#646262] text-right w-25 w-25-l dark:text-xs text-sm`}
                  >
                    CRR :
                    <span className="font-semibold">
                      {' '}
                      {data.miniScoreCard.runRate}
                    </span>
                  </th>
                  {data.miniScoreCard.rRunRate !== '' && (
                    <th
                      className={`w-25 font-medium  w-25-l text-right dark:text-white text-[#646262] dark:text-xs text-sm`}
                    >
                      Req RR:
                      <span className="font-semibold">
                        {data.miniScoreCard.rRunRate}
                      </span>
                    </th>
                  )}
                </tr>

                {(data.miniScoreCard.reviewDetails[0].review !== '' ||
                  data.miniScoreCard.reviewDetails[1].review !== '') && (
                  <tr className="flex p-2 lg:py-4 items-center">
                    <th className="font-medium text-right flex dark:border-gray-4 dark:text-xs text-sm">
                      <div className="dark:text-gray-2 text-[#646262]">
                        REVIEWS REM: &nbsp;{' '}
                      </div>
                      <div className="flex dark:text-white text-[#221F20]">
                        {data.miniScoreCard.reviewDetails.map((review, i) => (
                          <div className="dark:text-gray-2 text-[#221F20]">
                            {review.teamName.toUpperCase()}{' '}
                            {review.review || '0'}{' '}
                            {i ===
                            data.miniScoreCard.reviewDetails.length - 1 ? (
                              ''
                            ) : (
                              <span className="mr-1">{' , '}</span>
                            )}
                          </div>
                        ))}
                      </div>
                    </th>
                  </tr>
                )}
              </thead>
            </table>

            {data.miniScoreCard.bowling &&
              data.miniScoreCard.bowling.length > 0 && (
                // <table className="w-full">
                //   <thead className="dark:bg-gray bg-white ">
                //     <tr className="">
                //       <th className="dark:w-full w-8 pr-5 text-xs font-semibold text-left pl-3 dark:text-white text-black">
                //         BOWLER
                //       </th>
                //       <th className="text-xs font-semibold text-right dark:px-3 dark:text-white text-black">
                //         0
                //       </th>
                //       <th className="text-xs font-semibold text-right dark:px-5 dark:text-white text-black">
                //         M
                //       </th>
                //       <th className=" text-xs font-semibold text-right dark:px-4 dark:text-white text-black">
                //         R
                //       </th>
                //       <th className=" text-xs font-semibold text-right dark:px-4 dark:text-white text-black">
                //         W
                //       </th>
                //       <th className="dark:w-10 flex items-center justify-center dark:border-none border-b-solid border-b-2 border-b-gray-10   dark:text-right px-2  text-[#221F20B3] text-xs font-semibold text-right dark:px-2 dark:text-white text-black">
                //         ER
                //       </th>
                //     </tr>
                //   </thead>
                //   <tbody>
                //     {data.miniScoreCard.bowling.map((bowler, i) => (
                //       <tr key={i}>
                //         <td className="text-left font-medium text-xs py-3 pl-3">
                //           {bowler.playerName}
                //         </td>
                //         <td className="text-xs font-medium dark:px-2 text-right">
                //           {bowler.overs}
                //         </td>
                //         <td className="text-xs font-medium dark:pr-5 text-right">
                //           {bowler.maiden}
                //         </td>
                //         <td className="text-xs font-medium dark:pr-3 text-right">
                //           {bowler.RunsConceeded}
                //         </td>
                //         <td className="text-xs font-medium dark:pr-5 text-right">
                //           {bowler.wickets}
                //         </td>
                //         <td className=" dark:pr-1 text-right text-xs font-medium lg:flex md:flex xl:flex lg:items-center xl:items-center md:items-center dark:text-right lg:mt-3 md:mt-3 xl:mt-3 lg:justify-center md:justify-center xl:justify-center">
                //           {bowler.economy}
                //         </td>
                //       </tr>
                //     ))}
                //   </tbody>
                // </table>
                <table className="w-full lg:mt-3 md:mt-3 xl:mt-3">
                  <thead className="dark:bg-gray bg-white dark:border-gray-4 border-t-2 border-t-solid border-t-gray-11">
                    <tr className="">
                      <th className="w-7/12 px-3 lg:py-4 md:py-4 xl:py-4 py-2 dark:text-xs text-sm font-semibold text-left pl-3 dark:text-white text-[#221F20B3] dark:border-none border-b-2 border-b-gray-10 border-b-solid">
                        BOWLER
                      </th>
                      <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                        O
                      </th>
                      <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                        M
                      </th>
                      <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                        R
                      </th>
                      <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold  px-2 dark:text-white text-[#221F20B3]">
                        W
                      </th>
                      <th className="w-1/12 dark:text-xs text-sm dark:border-none border-b-solid border-b-2 border-b-gray-10 font-semibold   px-2 dark:text-white text-[#221F20B3]">
                        ER
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.miniScoreCard.bowling.map((bowler, i) => (
                      <tr key={i}>
                        <td className="border-black w-7/12 text-left font-medium text-xs py-4 pl-3">
                          {bowler.playerName}
                        </td>
                        <td className="dark:text-xs text-sm font-medium px-2 text-center">
                          {bowler.overs}
                        </td>
                        <td className="dark:text-xs text-sm font-medium px-2 text-center">
                          {bowler.maiden}
                        </td>
                        <td className="dark:text-xs text-sm w-1/12 dark:text-white text-[#221F20B3] text-center font-medium">
                          {bowler.RunsConceeded}
                        </td>
                        <td className="dark:text-xs text-sm w-1/12 dark:text-white text-[#221F20B3] text-center font-medium">
                          {bowler.wickets}
                        </td>
                        <td className="dark:text-xs text-sm w-1/12 dark:text-white text-[#221F20B3] text-center font-medium">
                          {bowler.economy}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              )}
          </div>
          {/* <div className="bg-white mt3 pt0 br2">
              <Quickbytes matchID={props.matchID} />
            </div> */}
        </div>
      </>
    )
}
