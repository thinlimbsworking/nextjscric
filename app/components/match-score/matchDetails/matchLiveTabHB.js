import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { MINI_SCORE_CARD } from '../../../api/queries'
// import Ballbyball from "./BallbyballTab";
// import Overbyover from "./overbyoverTab";
// import Quickbytes from "../Home/quickBytes";
// import Commentary from "./commentary";
// import OverbyOverV2 from "./overbyOverV2";
import ScorecardTabTest from './scorecardTab'
import PieClass from '../../piechart'
import * as d3 from 'd3'

export default function MatchLiveCommertry(props) {
  const [showWheel, setShowWheel] = useState()

  var z1SquarLeg = 0
  var z2FineLeg = 0
  var z3ThirdMan = 0
  var z4Point = 0
  var z5Cover = 0
  var z6MidOff = 0
  var z7MidOn = 0
  var z8MidWicket = 0
  const [tab, setTab] = useState('BALL BY BALL')

  const [isRefresh, setisRefresh] = useState(false)
  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20,
    }))
  const percentage = (run, total) => {
    return (run * 100) / total
  }
  const getTotal = (type, wagonData) => {
    let total = 0
    if (type === 'off') {
      total =
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 4)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 3)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 5)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 6)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0)
    } else {
      total =
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 1)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 2)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 7)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 8)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0)
    }

    return `${total}`
  }
  const [dataa, setData] = useState(generateData(0))
  const { loading, error, data } = useQuery(MINI_SCORE_CARD, {
    variables: { matchID: props.matchData.matchID },
    pollInterval: 8000,
  })

  if (error) return <></>
  if (loading) return <p>Loading...</p>
  if (data) {
    return (
      <>
        {data && data.miniScoreCard && data.miniScoreCard.batting.length > 0 && (
          <div className="m-2">
            <div className="  relative  ">
              <div className="bg-gray p-3 rounded-md">
                <div className="w-full bg-basebg lg:border-solid lg:border-2 lg:border-[#E2E2E2] rounded-t-lg">
                  <div className="flex items-center justify-between p-2 py-3">
                    <div className="text-sm font-bold w-5/12  text-white">
                      BATTER
                    </div>
                    <div className="flex items-center justify-between w-6/12 text-sm font-semibold lg:mr-3">
                      <div className="w-2/12 text-center text-xs text-white">
                        R
                      </div>
                      <div className="w-2/12 text-center text-xs text-white">
                        B
                      </div>
                      <div className="w-3/12 text-center text-xs text-white">
                        S/R
                      </div>
                      <div className="w-2/12 text-center text-xs text-white">
                        4s
                      </div>
                      <div className="w-1/12 text-center text-xs text-white">
                        6s
                      </div>
                    </div>
                    <div className="flex items-center justify-between w-1/12 text-sm font-semibold "></div>
                  </div>
                </div>

                {data.miniScoreCard.batting.map((batsmen, i) => (
                  <>
                    <div className="flex w-full bg-gray-4 px-1 py-2 ">
                      <div
                        className={`flex text-xs font-semibold w-5/12 items-center  ${
                          batsmen.playerOnStrike === true
                            ? 'text-white'
                            : 'text-gray-2'
                        } `}
                      >
                        {batsmen.playerOnStrike === true ? (
                          <div className="h-1 w-1 bg-green rounded-full mr-1"></div>
                        ) : (
                          <div className="h-1 w-1 bg-transparent rounded-full mr-1"></div>
                        )}
                        {batsmen.playerName}
                        <span className="text-xs ml-1">
                          {batsmen.playerOnStrike === true ? '*' : ''}
                        </span>
                      </div>
                      <div className="flex items-center justify-between w-6/12 text-sm font-semibold ">
                        <div className="w-2/12 text-center text-xs font-semibold dark:text-white text-black">
                          {batsmen.runs}
                        </div>
                        <div className="w-2/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                          {batsmen.playerMatchBalls}
                        </div>
                        <div className="w-3/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                          {batsmen.playerMatchStrikeRate}
                        </div>
                        <div className="w-2/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                          {batsmen.fours}
                        </div>
                        <div className="w-1/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                          {batsmen.sixes}
                        </div>
                      </div>

                      <div
                        className={`flex items-center justify-between  ml-3 text-sm font-semibold ${
                          showWheel == i ? 'rotate-180' : ''
                        } `}
                      >
                        <img
                          className="h-2 w-4"
                          onClick={() =>
                            showWheel !== i
                              ? setShowWheel(i)
                              : setShowWheel(1000)
                          }
                          src="/pngsV2/dwarrow.png"
                          alt=""
                        />
                      </div>
                    </div>

                    {/* {i == 1 && (
                        <div className="flex w-full bg-gray-4  border-t border-black rounded-b  py-2 text-xs font-medium text-gray-2  absolute bottom-0 ">
                          <div className="flex items-center w-4/12 justify-center text-white ">
                            {' '}
                            P'ship: {data.miniScoreCard.partnership}
                          </div>
                          <div className="flex items-center w-4/12 justify-center ">
                            CRR: {data.miniScoreCard.runRate}{' '}
                          </div>
                          <div className="flex items-center w-4/12 justify-center ">
                            RRR: {data.miniScoreCard.rRunRate}
                          </div>
                        </div>
                      )} */}

                    {/* <tr key={i}>
                        {}
                        <td className="border-black text-left font-medium text-xs py-3 pl-2">
                          {batsmen.playerName}
                          {batsmen.playerOnStrike === true ? '*' : ''}
                        </td>
                        <td className="text-xs font-medium px-2 text-right">
                          {batsmen.runs}
                        </td>
                        <td className="text-xs font-medium px-2 text-right">
                          {batsmen.playerMatchBalls}
                        </td>
                        <td className="text-xs font-medium px-2 text-right">
                          {batsmen.fours}
                        </td>
                        <td className="text-xs font-medium px-2 text-right">
                          {batsmen.sixes}
                        </td>
                        <td className="text-xs font-medium px-2 text-right">
                          {batsmen.playerMatchStrikeRate}
                        </td>
                        <td className="text-xs font-medium px-2 text-right" onClick={()=>setShowWheel(i)}>
                        {">"}
                        </td>

                      
                      </tr> */}
                    {showWheel == i && (
                      <div className="flex w-full h-48 bg-gray-4">
                        <div className="w-6/12 flex justify-center items-center flex-col md:-mt-3">
                          <div className="flex m-1 justify-center items-center relative w-28 h-28">
                            <img
                              className="w-28 h-28 absolute"
                              src={
                                'ssss' === 'Wickets'
                                  ? '/svgs/groundImageWicket.png'
                                  : '/pngsV2/wagon.png'
                              }
                              alt=""
                            />
                            <div className="flex m-1 justify-center items-center relative w-28 h-28">
                              {/* {} */}
                              <div className="w-28 h-28 absolute overflow-hidden rounded-full">
                                {batsmen.zad &&
                                  batsmen.zad.map((x, i) => (
                                    <div
                                      key={i}
                                      className={`border-[.2px] overflow-hidden absolute ${
                                        x.runs === '0'
                                          ? 'hidden'
                                          : (x.runs === '6' &&
                                              'border-red w-56') ||
                                            (x.runs === '5' &&
                                              'border-green w-56') ||
                                            (x.runs === '4' &&
                                              'border-yellow w-56') ||
                                            (x.runs === '2' &&
                                              `border-blue-2 ${
                                                x?.zadval?.split(',')[2] == '3'
                                                  ? 'w-[23px]'
                                                  : x?.zadval?.split(',')[2] ==
                                                    '2'
                                                  ? 'w-[25px]'
                                                  : x?.zadval.split(',')[2] ==
                                                    '1'
                                                  ? 'w-[20px]'
                                                  : 'w-[50px]'
                                              } `) ||
                                            (x.runs === '3' &&
                                              'border-pink-500 w-56') ||
                                            `border-white ${
                                              x?.zadval?.split(',')[2] == '3'
                                                ? 'w-[27px]'
                                                : x?.zadval?.split(',')[2] ==
                                                  '2'
                                                ? 'w-[24px]'
                                                : x?.zadval.split(',')[2] == '1'
                                                ? 'w-[20px]'
                                                : 'w-[50px]'
                                            } `
                                      } `}
                                      style={{
                                        bottom: '55%',
                                        left: '50%',
                                        right: '0%',
                                        transform: `rotate(${
                                          360 - x.zadval.split(',')[1]
                                        }deg)`,
                                        transformOrigin: '0 0',
                                      }}
                                    ></div>
                                  ))}
                              </div>
                              <div className="relative border rounded-full border-gray-3">
                                {/* height 5.5 rem */}
                                {/* window.screen.width > 975 ? window.screen.width/1.5 : window.screen.width+window.screen.width */}
                                <PieClass
                                  data={dataa}
                                  zoneRun={[
                                    z2FineLeg,
                                    z1SquarLeg,
                                    z8MidWicket,
                                    z7MidOn,
                                    z6MidOff,
                                    z5Cover,
                                    z4Point,
                                    z3ThirdMan,
                                  ]}
                                  width={window.screen.width > 975 ? 300 : 150}
                                  height={window.screen.width > 975 ? 300 : 150}
                                  battingStyle={batsmen.battingStyle}
                                  zadData={batsmen.zad ? batsmen.zad : []}
                                  innerRadius={
                                    window.screen.width > 975 ? 57 * 2 : 57
                                  }
                                  outerRadius={
                                    window.screen.width > 975 ? 75 * 2 : 75
                                  }
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="flex items-center justify-center flex-col w-6/12">
                          <div className="flex flex-col items-center justify-center w-10/12">
                            <div className="flex border-white w-full">
                              <div className="w-6/12 text-center border-r">
                                <div className="flex w-full text-xs text-gray-2">
                                  {batsmen.runs != '0'
                                    ? 100 -
                                      parseInt(
                                        percentage(
                                          parseInt(
                                            batsmen.battingStyle == 'LHB'
                                              ? getTotal('off', batsmen.zad)
                                              : getTotal('LEG', batsmen.zad),
                                          ),
                                          parseInt(batsmen.runs),
                                        ),
                                      )
                                    : '0'}
                                  % <br />
                                  {batsmen.battingStyle == 'LHB'
                                    ? 'LEG SIDE'
                                    : 'OFF SIDE'}
                                </div>
                              </div>

                              <div className="w-6/12 text-center text-xs dark:text-white text-black">
                                <>
                                  {batsmen.battingStyle != 'LHB'
                                    ? getTotal('off', batsmen.zad)
                                    : getTotal('LEG', batsmen.zad)}
                                  <span className="px-1 text-gray-2">Runs</span>
                                </>
                              </div>
                            </div>
                            <div className="flex border-white w-full items-center">
                              <div className="w-6/12 text-center  border-r mt-2">
                                <div className="flex w-full text-xs text-gray-2">
                                  {batsmen.runs != '0'
                                    ? parseInt(
                                        percentage(
                                          parseInt(
                                            batsmen.battingStyle == 'LHB'
                                              ? getTotal('off', batsmen.zad)
                                              : getTotal('LEG', batsmen.zad),
                                          ),
                                          parseInt(batsmen.runs),
                                        ),
                                      )
                                    : '0'}
                                  % <br />
                                  {batsmen.battingStyle == 'LHB'
                                    ? 'OFF SIDE'
                                    : 'LEG SIDE'}
                                </div>
                              </div>

                              <div className="w-6/12 text-center text-xs dark:text-white text-black">
                                <>
                                  {batsmen.battingStyle == 'LHB'
                                    ? getTotal('off', batsmen.zad)
                                    : getTotal('LEG', batsmen.zad)}
                                  <span className="px-1 text-gray-2">Runs</span>
                                </>
                              </div>
                            </div>
                          </div>
                          <div className="flex flex-wrap items-center justify-center">
                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                              <div className="w-2 h-2 mx-1 bg-white md:border rounded-full"></div>{' '}
                              <span className="text-white">1 Run</span>
                            </div>
                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                              <div className="w-2 h-2 mx-1 bg-blue rounded-full"></div>{' '}
                              <span className="text-white">2 Run</span>
                            </div>
                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                              <div className="w-2 h-2 mx-1 bg-pink-500 rounded-full"></div>{' '}
                              <span className="text-white">3 Run</span>
                            </div>
                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                              <div className="w-2 h-2 mx-1 bg-yellow rounded-full"></div>{' '}
                              <span className="text-white">4 Run</span>
                            </div>
                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                              <div className="w-2 h-2 mx-1 bg-red rounded-full"></div>{' '}
                              <span className="text-white">6 Run</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </>
                ))}

                <div className="flex w-full bg-gray-4  border-t border-black rounded-b  p-2 text-xs font-medium text-gray-2  justify-between  ">
                  <div className="flex items-center justify-center text-white ">
                    P'ship: {data.miniScoreCard.partnership}
                  </div>
                  <div className="flex items-center justify-center ">
                    CRR: {data.miniScoreCard.runRate}{' '}
                  </div>
                  {data.miniScoreCard.rRunRate ? (
                    <div className="flex items-center justify-center ">
                      RRR: {data.miniScoreCard.rRunRate}
                    </div>
                  ) : (
                    <></>
                  )}
                </div>
                {data.miniScoreCard.bowling &&
                  data.miniScoreCard.bowling.length > 0 && (
                    <div className="w-full mt-2">
                      <div className="w-full bg-basebg lg:border-solid lg:border-2 lg:border-[#E2E2E2] rounded-t-lg">
                        <div className="flex items-center justify-between p-2 py-3">
                          <div className="text-sm font-bold w-6/12  text-white">
                            BOWLER
                          </div>
                          <div className="flex items-center justify-between w-6/12 text-sm font-semibold lg:mr-3">
                            <div className="w-3/12 text-center text-xs text-white">
                              O
                            </div>
                            <div className="w-2/12 text-center text-xs text-white">
                              R
                            </div>
                            <div className="w-2/12 text-center text-xs text-white">
                              W
                            </div>
                            <div className="w-2/12 text-center text-xs text-white">
                              M
                            </div>

                            <div className="w-3/12 text-center text-xs text-white">
                              ER
                            </div>
                          </div>
                          {/* <div className="flex items-center justify-between w-1/12 text-sm font-semibold "></div> */}
                        </div>
                        {/* <div className="flex items-center justify-between p-2 py-3 bg-gray-4">
                            {console.log('7890',data&& data.miniScoreCard.bowling)}
                            <div className=" font-bold w-5/12  text-white text-xs">
                            {data?.miniScoreCard?.bowling[0]?.playerName}
                            </div>
                            <div className="flex items-center justify-between w-6/12 text-sm font-semibold lg:mr-3">
                              <div className="w-2/12 text-center text-xs text-gray-2">
                              {data?.miniScoreCard?.bowling[0]?.overs}
                              </div>
                              <div className="w-2/12 text-center text-xs text-gray-2">
                              {data?.miniScoreCard?.bowling[0]?.RunsConceeded}
                              </div>
                              <div className="w-2/12 text-center text-xs text-gray-2">
                              {data?.miniScoreCard?.bowling[0]?.wickets}
                              </div>
                              <div className="w-1/12 text-center text-xs text-gray-2">
                              {data?.miniScoreCard?.bowling[0]?.maiden}
                              </div>

                              <div className="w-3/12 text-center text-xs text-gray-2">
                              {data?.miniScoreCard?.bowling[0]?.economy}
                              </div>
                            </div>
                            <div className="flex items-center justify-between w-1/12 text-sm font-semibold ">

                              
                            </div>
                          </div> */}
                      </div>

                      <div>
                        {data.miniScoreCard.bowling.map((bowler, i) => (
                          <div className="flex w-full bg-gray-4 p-2 ">
                            <div className="text-xs font-semibold w-6/12 dark:text-white text-black">
                              {bowler.playerName}
                            </div>
                            <div className="flex items-center justify-between w-6/12 text-sm font-semibold ">
                              <div className="w-3/12 text-center text-xs font-semibold dark:text-white text-black">
                                {bowler.overs}
                              </div>
                              <div className="w-2/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                                {bowler.RunsConceeded}
                              </div>
                              <div className="w-2/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                                {bowler.wickets}
                              </div>
                              <div className="w-2/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                                {bowler.maiden}
                              </div>
                              <div className="w-3/12 text-center text-xs font-semibold dark:text-gray-2 text-black">
                                {bowler.economy}
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  )}
              </div>
            </div>
          </div>
        )}
        {/* <div className="bg-white mt3 pt0 br2">
              <Quickbytes matchID={props.matchID} />
            </div> */}
      </>
    )
  }
}
