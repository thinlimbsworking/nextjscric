import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import CleverTap from 'clevertap-react'
import { useRouter } from 'next/navigation'
import { format, formatDistanceStrict } from 'date-fns'
import {
  GET_MATCH_BASED_VIDEOS,
  GET_VIDEO_POSITIONS,
} from '../../../api/queries'
const RightSchevronBlack = '/svgs/RightSchevronBlack.svg'
const VideoIcon = '/svgs/videoIcon.svg'

export default function MatchVideosTab(props) {
  const { loading, error, data: matchVideoData } = useQuery(
    GET_MATCH_BASED_VIDEOS,
    {
      variables: { matchID: props.matchID },
    },
  )
  const { data: getRecentVideos } = useQuery(GET_VIDEO_POSITIONS)
  const router = useRouter()
  const navigate = router.push
  const handleVideoNavigation = (type, videoID) => {
    CleverTap.initialize('Videos', {
      Source: 'VideosTab',
      VideoID: videoID,
      Platform: localStorage.Platform,
    })
    navigate('/videos/[...slugs]', `/videos/${videoID}`)
  }

  const handleNavigation = () => {
    CleverTap.initialize('VideosHome', {
      Source: 'Homepage',
      Platform: localStorage.Platform,
    })
    navigate('/videos/[...slugs]', '/videos/latest')
  }
  return (
    <div>
      {matchVideoData &&
      matchVideoData.getVideoByMatchID &&
      matchVideoData.getVideoByMatchID.length > 0 ? (
        matchVideoData.getVideoByMatchID.map((video, i) => (
          <div
            key={i}
            className="flex m-2 my-3 dark:text-white text-black"
            onClick={() => {
              handleVideoNavigation('matchrelated', video.videoID)
            }}
          >
            {/* <div className='w45 h37 flex justify-center items-center overflow-hidden z-0 contain ba b--black cursor-pointer'>
              <img src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`} alt='' className='' />
              <img className='absolute' src={VideoIcon} alt='' />
            </div> */}
            <div className="w-56 h-24 z-0 contain relative">
              <img
                src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                alt=""
                className="lazyload"
              />
              <img
                className="absolute w-11 h-11 right-0 bottom-0"
                src="/pngsV2/videos_icon.png"
                alt=""
              />
            </div>

            <div className="w-56 w-80-l center">
              <div className="text-sm text-black dark:text-white font-semibold line-clamp-1">
                {video.title}
              </div>

              <div className="text-xs text-black dark:text-white/80 pt-1 line-clamp-2">
                {video.description}
              </div>

              {video?.startDate && (
                <div className="text-xs text-black dark:text-gray-2 bg-red pt-2">
                  {video.startDate
                    ? new Date().getTime() - Number(video.startDate) > 86400000
                      ? format(
                          new Date(Number(video.startDate)),
                          'dd MMM, yyyy',
                        )
                      : formatDistanceStrict(
                          new Date(Number(video.startDate)),
                          new Date(),
                        ) + ' ago'
                    : ''}
                </div>
              )}
            </div>
          </div>
        ))
      ) : (
        <div className="dark:text-white text-black">
          <div className="dark:bg-gray-4 bg-gray-11 fw1 f7 p-2 my-2 dark:text-white text-black text-center">
            <div>No videos available for this match.</div>
            <div>Do check out other videos.</div>
          </div>
          <div
            className="flex p-2.5 justify-between dark:bg-gray bg-gray-11"
            onClick={() => handleNavigation()}
          >
            <span className="f7 fw6">RECENT VIDEOS</span>
            <img
              className="cursor-pointer"
              alt={RightSchevronBlack}
              src={RightSchevronBlack}
            />
          </div>

          <div className="flex overflow-x-scroll py-2 my-2 hidescroll">
            {getRecentVideos &&
              getRecentVideos.getVideosPostitions.map((x, i) => (
                <div
                  key={i}
                  className="flex flex-column my-2 mr-4 cursor-pointer"
                  onClick={() => {
                    handleVideoNavigation('latest', x.videoID)
                  }}
                >
                  <div
                    className="w-44 h-32 flex justify-center items-center z-0 bg-dark-gray contain"
                    style={{
                      background: `url(https://img.youtube.com/vi/${x.videoYTId}/mqdefault.jpg) center top / cover no-repeat`,
                    }}
                  >
                    <img className="z-0 center" src={VideoIcon} alt="" />
                  </div>
                  <span className="font-medium mt-1 text-xs">{x.title}</span>
                </div>
              ))}
          </div>
        </div>
      )}
    </div>
  )
}
