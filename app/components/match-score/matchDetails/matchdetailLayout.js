import React, { useState } from 'react'
import MiniScoreCard from '../matchDetails/miniScoreCard'
import Link from 'next/link'
import { scheduleMatchView } from '../../../api/services'
import CleverTap from 'clevertap-react'
import format from 'date-fns/format'
import MetaDescriptor from '../../metadescription'
import { usePathname } from 'next/navigation'
import { Schedule } from '../../../constant/MetaDescriptions'
import { useRouter } from 'next/navigation'
import Loading from '../../loading'
const MatchLayout = ({ status, browser, routerPath, ...props }) => {
  const pathname = usePathname()
  const [tab, setTab] = useState(status || props.status)
  let router = useRouter()
  const tabs = [
    'Live',
    'Summary',
    // 'Playing11',
    // 'probablePlaying11',
    'MatchInfo',

    'Commentary',
    'Scorecard',
    // 'Dugout',
    'Highlights',
    'Articles',
    // 'Videos',
  ]

  const completedTabs = [
    'Summary',
    'Commentary',
    // 'Dugout',
    'Scorecard',
    'Highlights',
    // 'MatchInfo',
    'Articles',
    // 'Videos',
  ]

  const liveTabs = [
    'Commentary',
    'Scorecard',
    'Articles',
    'MatchInfo',
    'Highlights',
  ]

  const scheduledTabs = [
    'MatchInfo',
    // 'Commentary',
    // 'Scorecard',
    // 'Highlights',
    'Commentary',
    'Articles',
    'Playing11',

    // 'Videos',
  ]
  const abondonedTabs = [
    'Commentary',
    'Scorecard',
    'Articles',
    'Videos',
    'MatchInfo',
  ]
  const CancelledTabs = ['Commentary', 'Articles', 'Videos', 'MatchInfo']

  const { error, data } = { error: null, data: props.data }
  const handleNavigation = (tabName) => {
    CleverTap.initialize(`${tabName}Tab`, {
      source:
        tabName == 'Dugout'
          ? 'Dugout'
          : tabName === 'Commentary' || tabName === 'Summary'
          ? 'Homepage'
          : '',
      MatchID: data.miniScoreCard.data[0].matchID,
      SeriesID: data.miniScoreCard.data[0].seriesID,
      TeamAID: data.miniScoreCard.data[0].matchScore[0].teamID,
      TeamBID: data.miniScoreCard.data[0].matchScore[1].teamID,
      MatchFormat: data.miniScoreCard.data[0].matchType,
      MatchStartTime: format(
        Number(data.miniScoreCard.data[0].startDate),
        'do MMM yyyy, h:mm a',
      ),
      MatchStatus: data.miniScoreCard.data[0].matchStatus,
      Platform: global.window.localStorage.Platform,
    })
  }

  return (
    <>
      <MetaDescriptor
        section={Schedule(
          data?.miniScoreCard?.data[0],
          data?.miniScoreCard?.liveScoreUrl,
          pathname,
          status,
        )}
      />

      {error ? (
        <Loading />
      ) : (
        <div className="md:grid md:grid-cols-3 md:gap-4 w-full">
          <div className="dark:w-full md:shadow-sm col-span-2">
            {}
            <MiniScoreCard data={data} browser={browser} />

            {/* {alert( data.miniScoreCard.data[0].matchType)} */}
            <div className="dark:bg-basebg bg-white border-solid md:mt-2 w-full dark:border-gray-4 rounded-md">
              <div className="flex overflow-x-scroll z-999 hidescroll md:pt-2 rounded-md text-white font-semibold text-xs dark:bg-basebg bg-white">
                {tabs
                  .filter((tab) =>
                    data?.miniScoreCard?.data[0].isAbandoned
                      ? abondonedTabs.includes(tab)
                      : data?.miniScoreCard?.data[0]?.matchResult ===
                        'Match Cancelled'
                      ? CancelledTabs.includes(tab)
                      : data?.miniScoreCard?.data[0]?.matchStatus ===
                        'completed'
                      ? completedTabs.includes(tab)
                      : data?.miniScoreCard?.data?.[0]?.matchStatus ===
                        'upcoming'
                      ? scheduledTabs.includes(tab)
                      : liveTabs.includes(tab),
                  )
                  .map((tabName, i) => {
                    return (
                      <>
                        {(true
                          ? true
                          : (data?.miniScoreCard?.data?.[0]?.matchType ==
                              'ODI' ||
                              data?.miniScoreCard?.data?.[0]?.matchType ==
                                'T20' ||
                              data?.miniScoreCard?.data?.[0]?.matchType ==
                                'T100') &&
                            data?.miniScoreCard?.isDisplayDugout &&
                            data?.miniScoreCard?.data?.[0]
                              .isLiveCriclyticsAvailable) &&
                          data &&
                          data.miniScoreCard && (
                            <Link
                              key={i}
                              {...scheduleMatchView(
                                data?.miniScoreCard?.data?.[0],
                                tabName,
                              )}
                              replace
                              passHref
                              title={tabName}
                              onClick={() => {
                                setTab(tabName)
                                // handleNavigation(tabName)
                              }}
                              className={`${
                                false ? 'hidden' : ''
                              } cursor-pointer p-2 border-b border-b-solid md:w-full text-center ${
                                tabName?.toLowerCase() === tab?.toLowerCase()
                                  ? 'dark:border-blue-9 border-b-basered text-basered dark:text-blue-9 '
                                  : 'dark:text-blue-6 text-black dark:border-b-gray-6'
                              }`}
                            >
                              <div className="">
                                {' '}
                                {tabName === 'probablePlaying11'
                                  ? 'Probable Playing11'
                                  : tabName === 'Playing11'
                                  ? 'Squad'
                                  : tabName == 'jjj'
                                  ? 'jo send karna'
                                  : tabName}
                              </div>
                            </Link>
                          )}
                      </>
                    )
                  })}
              </div>

              {props.children}
            </div>
          </div>

          <div className=""></div>
        </div>
      )}
    </>
  )
}

export default MatchLayout
