import React from 'react'
import Scores from '../../commom/score'
import Liveballs from '../../liveballs'
const AbandonedIcon = '/svgs/images/Abandoned_Icon.png'
const ManOfMatch = '/svgs/Man_of_the_match.svg'
const backIconWhite = '/svgs/backIconWhite.svg'

export default function MiniScoreCard({ data, browser, ...props }) {
  return data?.miniScoreCard &&
    data.miniScoreCard.data.length &&
    data.miniScoreCard.data[0].matchID ? (
    <>
      {/* <div className="h43 pt3 ph3 mb3 truncate" style={{ background: "linear-gradient(to left, rgb(186, 82, 34), rgb(167, 15, 20))" }} >
         <img className="mr3" onClick={() => window.history.back()} src={backIconWhite} alt="back icon" />
         <span className="white fw6 f4 "> {data.miniScoreCard.data[0].homeTeamName} VS {data.miniScoreCard.data[0].awayTeamName}, {data.miniScoreCard.data[0].matchNumber}</span>
      </div> */}
      {!props.a23 && (
        <div className="pt-4 flex md:hidden items-center dark:bg-gray-8 bg-white text-white md:pt-5">
          <img
            className="px-3 py-0.5 md:hidden"
            onClick={() => window.history.back()}
            src={backIconWhite}
            alt="back icon"
          />
          <h1 className="text-md font-semibold uppercase lg:hidden md:hidden xl:hidden">
            {' '}
            {data.miniScoreCard.data[0].homeTeamName} VS{' '}
            {data.miniScoreCard.data[0].awayTeamName},{' '}
            {data.miniScoreCard.data[0].matchNumber}
          </h1>
        </div>
      )}

      <div className="w-full my-1">
        <div className="dark:bg-basebg bg-white md:rounded-md shadow-1 dark:p-1 px-2 py-3">
          <div className="p-2 md:p-0 lg:p-0 relative lg:flex dark:bg-gray-8 bg-white w-full">
            {data.miniScoreCard.data[0].matchStatus === 'live' && (
              <div className="px-2 py-0.5 items-center rounded-full hidden md:block lg:block border border-white/80  absolute right-2 top-3 text-white">
                <span className="bg-green h04 w04 rounded-full mr1" />
              </div>
            )}

            <Scores
              isDetails
              featured={true}
              showCriclytics={true}
              data={data.miniScoreCard.data[0]}
              browser={browser}
              a23={props.a23}
              from={'schedule'}
              isMiniScorecard
            />
          </div>
        </div>
      </div>
      <Liveballs
        hideTitle={true}
        matchID={data && data.miniScoreCard.data[0].matchID}
        matchData={data && data.miniScoreCard.data[0]}
        currentinningsNo={data && data.miniScoreCard.data[0].currentinningsNo}
      />
    </>
  ) : (
    <></>
  )
}
