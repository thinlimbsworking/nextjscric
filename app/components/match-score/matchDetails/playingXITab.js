import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import { PROBABLE_PLAYING_XI } from '../../../api/queries'
// import FanFightCard from '../FanFightCard';
import Loading from '../../commom/loading'
// import { navigate } from '@reach/router';
import Link from 'next/link'
import { getPlayerUrl } from '../../../api/services'
import CleverTap from 'clevertap-react'
export default function PlayingXITab({
  newData,
  matchID,
  matchData,
  match,
  ...props
}) {
  const { loading, error, data, params } = useQuery(PROBABLE_PLAYING_XI, {
    variables: { matchID: matchID },
  })

  const clevertab = (id) => {
    CleverTap.initialize('Players', {
      Source: 'CommentaryTab',
      PlayerID: id,
      Platform: localStorage.Platform,
    })
  }
  if (error)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center"></div>
    )
  if (loading) return <Loading />
  else {
  }
  return (
    <React.Fragment>
      {/* <div className="text-white pb-3">
          <div className="text-sm font-semibold p-2 dark:bg-gray dark:border-none border-b-2 border-b-gray-11 border-b-solid">
            PLAYING XI
          </div>

          {data.probablePlaying11.homeTeamPP11 && (
            <div className="p-1 justify-center dark:text-white text-black">
              <span className="text-xs font-semibold">
                {' '}
                {data.probablePlaying11.homeTeamShortName.toUpperCase()} :
              </span>
              {data.probablePlaying11.homeTeamPP11.map((player, i) => (
                <Link
                  key={i}
                  {...getPlayerUrl({ ...player })}
                  onClick={() => clevertab(player.playerID)}
                  passHref
                  className="text-xs pl-1 cursor-pointer"
                >
                  {`${player.playerName}${player.captain ? ' (C)' : ''}${
                    player.keeper ? ' (Wk)' : ''
                  }${i === 10 ? '.' : ','}`}
                </Link>
              ))}
            </div>
          )}

          {data.probablePlaying11.awayTeamPP11 && (
            <div className="p-1 justify-center dark:text-white text-black">
              <span className="font-semibold text-xs">
                {' '}
                {data.probablePlaying11.awayTeamShortName.toUpperCase()} :
              </span>
              {data.probablePlaying11.awayTeamPP11.map((player, i) => (
                <Link
                  key={i}
                  {...getPlayerUrl({ ...player })}
                  onClick={() => clevertab(player.playerID)}
                  passHref
                  className="text-xs pl-1 cursor-pointer"
                >
                  {`${player.playerName}${player.captain ? ' (C)' : ''}${
                    player.keeper ? ' (Wk)' : ''
                  }${i === 10 ? '.' : ','}`}
                </Link>
              ))}
            </div>
          )}
        </div> */}
      {props?.matchData?.matchStatus && (
        <>
          <div className="flex flex-col items-center justify-between dark:bg-gray bg-gray-10 rounded-md px-2 pt-2 pb-4 m-2">
            {data?.getMatchInfo?.homePlayingXI &&
            newData?.getMatchCardTabWiseByMatchID ? (
              <div className=" w-full flex items-center justify-between">
                <div className="dark:font-bold font-semibold dark:text-white text-black text-base">
                  Check Playing 11 <br />
                </div>
                <div className="mt-2">
                  <img
                    className="cursor-pointer rounded-xl bg-basebg h-10 w-10 p-1"
                    src={!showTeam ? '/pngsV2/plus.png' : '/pngsV2/minus.png'}
                    alt=""
                    onClick={() => setShowTeam(!showTeam)}
                  />
                </div>
              </div>
            ) : (
              <div className=" w-full flex items-center justify-between">
                <div className="font-bold text-base text-black dark:text-white ">
                  Playing 11 for this match is not available ! <br />
                </div>
              </div>
            )}

            {showTeam &&
              data?.getMatchInfo?.homePlayingXI &&
              newData?.getMatchCardTabWiseByMatchID && (
                <div className="w-full flex items-center justify-center">
                  <div className="flex items-center justify-center dark:w-10/12 w-5/12 bg-gray-4 rounded-2xl text-white">
                    <div
                      className={`w-6/12 rounded-2xl ${
                        HomeTeam == 0 ? 'border border-green bg-basebg' : ''
                      } p-1 `}
                      onClick={() => setHomeTeam(0)}
                    >
                      <div className="flex items-center justify-center ">
                        <span className="pl-2 text-sm font-semibold ">
                          {newData.getMatchCardTabWiseByMatchID &&
                            newData.getMatchCardTabWiseByMatchID.squad.length >
                              0 &&
                            newData.getMatchCardTabWiseByMatchID.squad[0]
                              .teamShortName}{' '}
                        </span>
                      </div>
                    </div>
                    <div
                      className={`w-6/12 rounded-2xl ${
                        HomeTeam == 1 ? ' border border-green bg-basebg' : ''
                      } p-1 `}
                      onClick={() => setHomeTeam(1)}
                    >
                      <div className="flex items-center justify-center">
                        {/* <img
                    className='ib h1 w15 shadow-4 '
                    alt='location'
                    src={`https://images.cricket.com/teams/${data.getMatchInfo.homeTeamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                  /> */}
                        <span className="pl-2 text-sm font-semibold ">
                          {' '}
                          {newData.getMatchCardTabWiseByMatchID &&
                            newData.getMatchCardTabWiseByMatchID.squad.length >
                              1 &&
                            newData.getMatchCardTabWiseByMatchID.squad[1]
                              .teamShortName}{' '}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              )}
          </div>
        </>
      )}

      {/* Team View */}
      {props?.matchData?.matchStatus && (
        <div>
          <div className="py-2">
            {showTeam && (
              <div className="">
                {data?.getMatchInfo?.homePlayingXI &&
                  newData?.getMatchCardTabWiseByMatchID?.squad[
                    HomeTeam
                  ].playingxi.map((player, i) => (
                    <React.Fragment key={i}>
                      <div className="flex items-center justify-between dark:bg-gray bg-white border-2 border-solid dark:border-basebg border-[#E2E2E2] p-1 rounded-md mx-3 my-2">
                        <div className=" flex items-center justify-center">
                          <div className="relative bg-gray-8 w-12 h-12 rounded-full flex items-center justify-center">
                            <img
                              className="h-10 w-10 overflow-hidden rounded-full object-top object-cover absolute bottom-0 "
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              alt=""
                              onError={(evt) => (evt.target.src = ManOfMatch)}
                            />
                          </div>
                          <Link
                            {...getPlayerUrl(player)}
                            passHref
                            onClick={() =>
                              CleverTap.initialize('Players', {
                                Source: 'MatchInfoTab',

                                Platform: localStorage
                                  ? localStorage.Platform
                                  : '',
                              })
                            }
                            className="f7 fw5 cursor-pointer"
                          >
                            {' '}
                            <span className="f7 fw5 cursor-pointer pl-2">
                              {player.playerName}
                            </span>
                            <span className="pl1 fw6 f7">
                              {' '}
                              {player.captain && player.keeper
                                ? '(c) & (Wk)'
                                : player.captain
                                ? '(c)'
                                : player.keeper
                                ? '(Wk)'
                                : ''}
                            </span>
                          </Link>
                        </div>
                        <div className="border lg:bg-gray-3 md:bg-gray-3 p-1 rounded-md">
                          {/* {player.playerRole.toLowerCase()} */}
                          {/* {PlayerRoleImg(player.playerRole.toLowerCase())} */}
                          <img
                            className="h-3 w-3  "
                            src={PlayerRoleImg(player.playerRole.toLowerCase())}
                            alt=""
                            srcset=""
                          />
                        </div>
                      </div>
                    </React.Fragment>
                  ))}
              </div>
            )}

            {showTeam && (
              <div className="flex ml-2 lg:my-4 md:my-4 font-bold dark:text-xs text-base capitalize">
                Bench{' '}
              </div>
            )}

            {showTeam && (
              <div className="">
                {data?.getMatchInfo?.homePlayingXI &&
                  newData?.getMatchCardTabWiseByMatchID?.squad[
                    HomeTeam
                  ].benchplayers.map((player, i) => (
                    <React.Fragment key={i}>
                      <div className="flex items-center justify-between dark:bg-gray bg-white p-1 border-2 border-solid dark:border-basebg border-[#E2E2E2] rounded-md mx-3 my-2">
                        <div className=" flex items-center justify-center">
                          <div className="relative bg-gray-8 w-12 h-12 rounded-full flex items-center justify-center">
                            <img
                              className="h-10 w-10 overflow-hidden rounded-full  object-top object-cover   absolute bottom-0 "
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              alt=""
                              onError={(evt) => (evt.target.src = ManOfMatch)}
                            />
                          </div>
                          <Link
                            {...getPlayerUrl(player)}
                            passHref
                            onClick={() =>
                              CleverTap.initialize('Players', {
                                Source: 'MatchInfoTab',

                                Platform: localStorage
                                  ? localStorage.Platform
                                  : '',
                              })
                            }
                            className="f7 fw5 cursor-pointer"
                          >
                            {' '}
                            <span className="f7 fw5 cursor-pointer pl-2">
                              {player.playerName}
                            </span>
                            <span className="pl1 fw6 f7">
                              {' '}
                              {player.captain && player.keeper
                                ? '(c) & (Wk)'
                                : player.captain
                                ? '(c)'
                                : player.keeper
                                ? '(Wk)'
                                : ''}
                            </span>
                          </Link>
                        </div>
                        <div className="border p-1 lg:bg-gray-3 md:bg-gray-3 rounded-md">
                          {/* {player.playerRole.toLowerCase()} */}
                          {/* {PlayerRoleImg(player.playerRole.toLowerCase())} */}
                          <img
                            className="h-3 w-3  "
                            src={PlayerRoleImg(player.playerRole.toLowerCase())}
                            alt=""
                            srcset=""
                          />
                        </div>
                      </div>
                    </React.Fragment>
                  ))}
              </div>
            )}

            {/* <div className='pv3 flex justify-between items-center' onClick={() => setAwayXI(!awayXI)}>
                <div className='flex items-center'>
                  <img
                    className='ib h1 w15 shadow-4 '
                    alt='location'
                    src={`https://images.cricket.com/teams/${data.getMatchInfo.awayTeamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                  />
                  <span className='pl2 f6 fw6'> {data.getMatchInfo.awayTeamShortName} </span>
                </div>
                <img className={`w1 h1 cursor-pointer ${awayXI ? 'rotate-270' : 'rotate-90'}`} src={upArrowBlack} alt={''} />
              </div> */}
            <div className="dark:bg-black bg-gray-11 h-[1px] w-full" />
            {false && showTeam && !HomeTeam && (
              <div className="">
                {newData.getMatchCardTabWiseByMatchID &&
                  newData.getMatchCardTabWiseByMatchID.squad &&
                  newData.getMatchCardTabWiseByMatchID.squad[1].playingxi.map(
                    (player, i) => (
                      <React.Fragment key={i}>
                        <div className="flex items-center pv2">
                          <div className="w-8 h-8 bg-gray rounded-full">
                            <img
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              alt=""
                              onError={(evt) => (evt.target.src = ManOfMatch)}
                            />
                          </div>
                          <Link {...getPlayerUrl(player)} passHref>
                            <span className="f7 fw5 cursor-pointer pl-2">
                              {player.playerName}
                            </span>
                            <span className="pl1 fw6 f7">
                              {' '}
                              {player.captain &&
                              player.playerRole === 'Wicket-Keeper'
                                ? '(c) & (Wk)'
                                : player.captain
                                ? '(c)'
                                : player.playerRole === 'Wicket-Keeper'
                                ? '(Wk)'
                                : ''}
                            </span>
                          </Link>
                        </div>
                        <div className="dark:bg-black bg-gray-11 h-[1px] w-full" />
                      </React.Fragment>
                    ),
                  )}
              </div>
            )}
          </div>
        </div>
      )}

      {/* <FanFightCard /> */}
    </React.Fragment>
  )
}
