import React, { useEffect } from 'react'
import Try1 from './try1'
import CleverTap from 'clevertap-react'
import Batsman from './popbatsman'
import Head from 'next/head'
import MetaDescriptor from '../../components/MetaDescriptor'

const playerAvatar = '/pngsV2/playerph.png'

export default function PopupDugout(props) {
  const ground = '/svgs/redground.svg'

  var isSafari = window['safari'] ? true : false

  // Chrome 1 - 71
  var isChrome = window.chrome ? true : false

  const getTotal = (type, wagonData) => {
    let total = 0
    if (type === 'off') {
      total =
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 4)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 3)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 5)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 6)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0)
    } else {
      total =
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 1)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 2)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 7)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          .filter((dt) => dt.zadval.split(',')[0] == 8)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0)
    }
    return `${total}`
  }

  const getTotalALl = (wagonData) => {
    let total2 = 0

    total2 =
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 4)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0) +
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 3)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0) +
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 5)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0) +
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 6)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0)
    wagonData
      .filter((dt) => dt.zadval.split(',')[0] == 1)
      .map((x) => Number(x.runs))
      .reduce((a, x) => a + x, 0) +
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 2)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0) +
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 7)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0) +
      wagonData
        .filter((dt) => dt.zadval.split(',')[0] == 8)
        .map((x) => Number(x.runs))
        .reduce((a, x) => a + x, 0)

    return `${total2}`
  }

  const percentage = (run, total) => {
    return (run * 100) / total
  }

  const IMAGES = {
    EMPTY: '/svgs/Empty.svg',
    backIconWhite: '/svgs/closepop.svg',
  }
  useEffect(() => {
    props.callScroll(true)
    props.chart && props.callPopName(1)
    return () => {
      props.callScroll(false)
      props.chart && props.callPopName(3)
    }
  }, [])

  const handleShare = (name) => {
    let ShareTile = `Scoring areas of  ${name}. Check out all Wagon Wheels at cricket.com. Download Now: https://cricketapp.app.link/downloadapp`

    if (window.navigator.share !== undefined) {
      window.navigator
        .share({
          title: ShareTile,
          url: window.location.href,
        })
        .then((res) => {
          CleverTap.initialize('Share', {
            Source: 'Dugout',

            Platform: localStorage ? localStorage.Platform : '',
          })
        })
        .catch((error) => )
    }
  }

  const APP_IPL_TEAMS = (name) => {
    return {
      title: 'Dugout Feature | Cricket Updates | Cricket.com',
      description: `Scoring areas of  ${name}. Check out all Wagon Wheels at cricket.com. Download Now: https://cricketapp.app.link/downloadapp`,
      url: window.location.href,
    }
  }
  return (
    <div className="min-vh-100 z-9999 bg-gray-4 text-white">
      <Head>
        <title>Dugout |cricket.com</title>
        <h1 className="dn">Dugout</h1>
        <meta
          name="description"
          content={`Scoring areas of  ${
            props.item && props.item.playerName
          }. Check out all Wagon Wheels at cricket.com. Download Now: https://cricketapp.app.link/downloadapp`}
        />
        <meta
          name="og:description"
          content={`Scoring areas of  ${
            props.item && props.item.playerName
          }. Check out all Wagon Wheels at cricket.com. Download Now: https://cricketapp.app.link/downloadapp`}
        />
        {/* <meta name='keywords' content={article.seoTags} />
        <meta itemprop='width' content='595' />
        <meta itemprop='height' content='450' />
        <meta itemprop='url' content={article.thumbnail} />
        <meta property='og:image' content={article.thumbnail} />
        <meta property='og:width' content='595' />
        <meta property='og:height' content='450' />
        <meta property='og:image:width' content='595' />
        <meta property='og:image:height' content='450' />
        <meta property='og:image:secure_url' content={article.thumbnail} />
        <meta property='og:title' content={article.title || 'cricket.com article'} />
        <meta property='og:url' content={`https://www.cricket.com/news/${article.articleID}`} />
        <meta property='og:type' content='Article' />
        <meta property='fb:app_id' content='631 889693979290' />
        <meta property='og:site_name' content='cricket.com' />
        <link rel='canonical' href={`https://www.cricket.com/news/${article.articleID}`} />
        <link rel='apple-touch-icon' href='http://mysite.com/img/apple-touch-icon-57x57.png' />
        <script async src='https://platform.twitter.com/widgets.js'></script>
        <script async src='https://platform.instagram.com/en_US/embeds.js'></script> */}
      </Head>
      <div
        className="bg-basebg flex justify-centr fixed absolute--fill z-9999   flex-column"
        style={{ backdropFilter: 'blur(10px)' }}
      >
        <div className="w-100 flex h2-3 justify-start items-center pa2 pv3 z-2 shadow-4">
          <div className="flex w-30 items-center justify-center">
            <img
              onClick={() => props.setPopWHeel(false)}
              className="mr2 pa1 cursor-pointer"
              src={IMAGES.backIconWhite}
              // onError={(evt) => (evt.target.src = playerAvatar)}
              alt="back icon"
            />{' '}
            <span className="fw6 f5 white ttu"> Dugout </span>
          </div>

          <div className="flex w-70 justify-end items-center">
            {' '}
            <img
              className=" w13 dn-l mr3 "
              onClick={() => handleShare(props.item.playerName)}
              src="/svgs/share-line.png"
              alt="back icon"
            />{' '}
          </div>
        </div>

        <div className="w-100 flex pa3  flex-row bg-gray-4  shadow-4 ">
          <div className="w-30 flex   items-center justify-start flex-column">
            <div class="w-14 h-14 cursor-pointer br-100  object-top overflow-hidden object-cover  bg-gray ">
              <img
                src={`https://images.cricket.com/players/${props.item.playerID}_headshot_safari.png`}
                onError={(evt) => (evt.target.src = playerAvatar)}
              />
            </div>
          </div>
          <div className="w-70 flex ">
            <div class=" f5 fw5 flex w-100 flex-column">
              <div className="flex"> {props.item.playerName}</div>
              <div className="flex w-100 ">
                <div className="w-2/5  red_10 flex flex-row ">
                  <div className="flex w-100 items-center">
                    {' '}
                    <div className="f3 fw5 oswald">
                      {props.item.playerMatchRuns}
                      {(props.item.isNotOut || props.nout == true) && (
                        <span>*</span>
                      )}
                    </div>{' '}
                    <div className="fw5 f7 white pl-1 ">
                      {'('}
                      {props.item.playerMatchBalls}
                      {')'}
                    </div>
                  </div>
                </div>
                <div className="w-3/5 ">
                  <div className="w-100    flex flex-row ">
                    <div className="flex w-2/5 br  flex-column ph1  item-center">
                      {' '}
                      <div className="tc oswald f4">
                        {props.item.playerMatchFours}
                      </div>{' '}
                      <div className="fw5 tc f7 ">Fours</div>
                    </div>

                    <div className="flex w-2/5  flex-column   item-center ph1  b--gray">
                      {' '}
                      <div className="tc oswald f4">
                        {props.item.playerMatchSixes}
                      </div>{' '}
                      <div className="fw5 f7 tc ">Sixes</div>
                    </div>
                  </div>
                </div>{' '}
              </div>
            </div>
          </div>
        </div>

        <div className=" w-100 mt2  pa2 flex items-center justify-center bg-gray-4 shadow-4">
          <div className="flex w-100 ma2">
            <div className="w-20 pa1 flex flex-column items-center h37 justify-center bg-gray">
              <div className="f7 fw5 tc">
                <div className="f6 fw5 red_10 bb b--silver pa1 mb1">
                  {props.item.playerMatchRuns != '0'
                    ? 100 -
                      parseInt(
                        percentage(
                          parseInt(
                            props.item.battingStyle == 'LHB'
                              ? getTotal('off', props.item.zad)
                              : getTotal('LEG', props.item.zad),
                          ),
                          parseInt(props.item.playerMatchRuns),
                        ),
                      )
                    : '0'}
                  %{' '}
                </div>

                {props.item.battingStyle == 'LHB' ? 'LEG SIDE' : 'OFF SIDE'}
              </div>
              <div className="red_10 fw5  tc f4 mt1 mb1">
                {' '}
                {props.item.battingStyle != 'LHB'
                  ? getTotal('off', props.item.zad)
                  : getTotal('LEG', props.item.zad)}
              </div>

              <div className="f7 fw5 tc"> RUNS</div>
            </div>
            <div className="w-60  mt4 flex items-center justify-center">
              <Try1
                zad={props.item.zad}
                battingStyle={props.item.battingStyle}
                upcoming={false}
              />
            </div>
            <div className="w-20 pa1 flex flex-column items-center h37 justify-center bg-gray">
              <div className="f6 fw5 red_10 f6 fw5 red_10 bb b--silver pa1 mb1">
                {props.item.playerMatchRuns != '0'
                  ? parseInt(
                      percentage(
                        parseInt(
                          props.item.battingStyle == 'LHB'
                            ? getTotal('off', props.item.zad)
                            : getTotal('LEG', props.item.zad),
                        ),
                        parseInt(props.item.playerMatchRuns),
                      ),
                    )
                  : '0'}
                %{' '}
              </div>
              <div className="f7 fw5 tc">
                {props.item.battingStyle == 'LHB' ? 'OFF SIDE' : 'LEG SIDE'}
              </div>

              <div className="red_10 tc fw5 f4 mt1 mb1">
                {props.item.battingStyle == 'LHB'
                  ? getTotal('off', props.item.zad)
                  : getTotal('LEG', props.item.zad)}
              </div>

              <div className="f7 fw5 tc"> RUNS</div>
            </div>
          </div>
        </div>

        <div className="nt4   bg-gray-4 items-center justify-center shadow-4">
          <div className="pt4 ph4  flex w-100 flex-row pa3 flex-row items-center justify-center">
            <div className="w-25  br flex items-center f7 fw5 justify-center">
              <div className="h1 w1 ba b--gray bg-gray-4 mr1 "></div>Singles
            </div>

            <div className="w-25 br flex items-center f7 fw5 justify-center">
              <div className="h1 w1 ba b--gray bg-blue mr1"></div> Twos
            </div>

            <div className="w-25  flex items-center f7 fw5 justify-center">
              <div className="h1 w1 ba b--gray bg-purple mr1"></div>Threes
            </div>
          </div>

          <div className=" pa2 pb3 flex w-100 flex-row  flex-row items-center justify-center">
            <div className="w-25 br flex items-center f7 fw5 justify-center">
              <div className="h1 w1 ba b--gray bg-yellow mr1"></div> Fours
            </div>

            <div className="w-25  flex items-center f7 fw5 justify-center">
              <div className="h1 w1 ba b--gray bg-red mr1"></div>Sixes
            </div>
          </div>
        </div>

        {props.upcoming && (
          <Batsman
            idKey={props.item.playerID}
            key={props.key}
            teamColor={'#ff4136'}
            popUpWheelIndex={props.popUpWheelIndex}
            setPopWHeelIndex={props.setPopWHeelIndex}
            swipeData={props.mainData}
            popUpWheel={props.popUpWheel}
            setPopWHeel={props.setPopWHeel}
          />
        )}
      </div>
    </div>
  )
}
