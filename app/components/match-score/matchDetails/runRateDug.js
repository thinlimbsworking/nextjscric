import React from 'react';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';

export default function runRateDug(props) {
  return (
    props.data && (
      <div>
        <div className='w-100 mt2 flex  flex-column  bg-gray-4 text-white  shadow-4   '>
          <div className='flex w-100 bb b--black pa2 f7 fw5 ttu'>
            <div className='w-50 ttu'>Run rate projections</div>
          </div>

          <div className='w-100 flex flex-row items-center justify-center py-3 px-2'>
            {props.data &&
              props.data.map((item, index) => {
                return (
                  parseInt(item.rr) > -1 && (
                    <div className={` w-33 flex flex-column items-center justify-center ${index == 2 ? '' : props.data.length - 1 > index ? 'br b--black' : ''}`}>
                      <div className='text-lg  pv1 fw6'>{item.runs}</div>
                      <div className='text-xs pv1  '>@ {item.rr} Runs / over</div>
                    </div>
                  )
                );
              })}
          </div>
        </div>
      </div>
    )
  );
}
