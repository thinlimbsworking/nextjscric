'use client'
import React, { useState, useEffect, useMemo } from 'react'
import { useRouter } from 'next/navigation'
import Link from 'next/link'
import * as d3 from 'd3'

import { PLAYER_VIEW } from '../../../constant/Links'
import CleverTap from 'clevertap-react'
import Runwheel from './runwheel'
import PieClass from '../../piechart'
import Loading from '../../loading'
import { useQuery } from '@apollo/client'
import { GET_ARTICLES_MATCH_INFO_SCORECARD } from '../../../api/queries'
import ArrowIcon from '../../commom/arrow'
const upArrowWhite = '/svgs/upArrowWhite.svg'
const ManOfMatch = '/svgs/Man_of_the_match.svg'
const upwarrow = '/pngsV2/dwarrow.png'
const downicon = '/pngsV2/downiconnew.png'
export default function ScorecardTab({ params, ...props }) {
  const { data, loading } = useQuery(GET_ARTICLES_MATCH_INFO_SCORECARD, {
    variables: { type: 'matches', Id: params?.slug?.[0] },
  })
  let router = useRouter()
  let navigate = router.push
  const [card, setCard] = useState(3)
  const [showWheel, setShowWheel] = useState()

  const [exp, setExp] = useState(false)

  var z1SquarLeg = 0
  var z2FineLeg = 0
  var z3ThirdMan = 0
  var z4Point = 0
  var z5Cover = 0
  var z6MidOff = 0
  var z7MidOn = 0
  var z8MidWicket = 0

  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20,
    }))

  const [dataa, setData] = useState(generateData(0))
  const [teamShow, setTeamShow] = useState(0)
  //
  const changeData = () => {
    setData(generateData())
  }

  useEffect(() => {
    setData(generateData())
    setExp(window.screen.width > 540 ? true : false)
  }, [!dataa])

  useEffect(() => {
    setCard(
      data?.getScoreCard?.fullScoreCard &&
        data?.getScoreCard?.fullScoreCard.length - 1,
    )
    setTeamShow(
      data &&
        data?.getScoreCard?.fullScoreCard &&
        data?.getScoreCard?.fullScoreCard.length - 1,
    )
  }, [data])

  const handlePlayerNavigation = (playerName, playerID) => {
    CleverTap.initialize('Players', {
      Source: 'ScorecardTab',
      playerID: playerID,
      Platform: localStorage.Platform,
    })
  }

  const percentage = (run, total) => {
    return (run * 100) / total
  }

  const getTotal = (type, wagonData) => {
    let total = 0
    if (type === 'off') {
      total =
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 4)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 3)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 5)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 6)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0)
    } else {
      total =
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 1)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 2)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 7)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0) +
        wagonData
          ?.filter((dt) => dt.zadval.split(',')[0] == 8)
          .map((x) => Number(x.runs))
          .reduce((a, x) => a + x, 0)
    }
    return `${total}`
  }

  const getPlayerUrl = (playerName, playerID) => {
    let playerSlug = `${
      playerName ? playerName.split(' ').join('-').toLowerCase() : 'N/A'
    }`
    let playerId = playerID
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href,
    }
  }
  if (data?.length === 0) {
    return <Loading />
  }
  if (data?.getScoreCard?.fullScoreCard?.length == 0) {
    return (
      <div className="w-full vh-100 fw2 f7 gray flex flex-col justify-center items-center">
        <Loading />
      </div>
    )
  }
  if (loading) {
    return <Loading />
  }
  if (data?.getScoreCard?.fullScoreCard?.length != 0) {
    return (
      <div className="w-full">
        <div className="flex flex-col w-full md:px-2">
          {data &&
          data.getScoreCard &&
          data.getScoreCard.fullScoreCard &&
          data.getScoreCard.fullScoreCard.length != 0 ? (
            data.getScoreCard.fullScoreCard.map((item, index) => {
              return (
                <div
                  className={`flex flex-col w-full items-center justify-between dark:border-b py-2 border-gray`}
                >
                  <div
                    className={`flex w-full lg:py-2 md:py-2 ${
                      index == teamShow
                        ? 'md:bg-gray-10 md:text-black'
                        : 'md:bg-gray-11'
                    }`}
                  >
                    <div className="flex w-full">
                      <div className={`flex p-2 w-full`}>
                        <img
                          className="h-6 w-10 rounded"
                          src={`https://images.cricket.com/teams/${item.battingTeamID}_flag_safari.png`}
                          alt=""
                          srcset=""
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/flag_dark.png')
                          }
                        />
                        <span
                          className={`ml-2 ${
                            index == teamShow
                              ? 'md:text-black'
                              : 'lg:text-black md:text-black'
                          }`}
                        >
                          {item.battingTeamShortName}
                        </span>
                      </div>
                      <div className="flex items-center justify-center">
                        <span
                          className={`dark:text-white ${
                            index == teamShow ? 'text-black' : 'text-black'
                          } `}
                        >
                          {' '}
                          {item.total.runsScored}
                          {item.total.wickets && item.total.wickets !== '10'
                            ? `/${item.total.wickets}`
                            : null}
                          {item.total.overs ? `(${item.total.overs})` : ''}
                        </span>
                        <img
                          onClick={() =>
                            setTeamShow(index == teamShow ? 4 : index)
                          }
                          className="cursor-pointer mx-2 lg:hidden md:hidden rounded-xl bg-gray h-10 w-10 p-1"
                          src={
                            teamShow == index
                              ? `/pngsV2/minus.png`
                              : '/pngsV2/plus.png'
                          }
                          alt=""
                        />
                        <img
                          onClick={() =>
                            setTeamShow(index == teamShow ? 4 : index)
                          }
                          className={`cursor-pointer mx-2 ${
                            teamShow == index ? 'rotate-180' : ''
                          } hidden lg:flex md:flex rounded-xl h-5 w-8 p-1`}
                          src={
                            teamShow == index
                              ? `/pngsV2/downiconnew.png`
                              : '/pngsV2/downiconnew.png'
                          }
                          alt=""
                        />
                      </div>
                    </div>

                    <div className="flex flex-col items-start justify-center m-2"></div>
                  </div>
                  {index === teamShow && (
                    <>
                      {/* batter */}

                      {
                        <>
                          <div className="mt-4 mx-4 w-full bg-white dark:bg-gray dark:border-none border rounded-tr-md rounded-tl-md border-gray-11 dark:rounded-t-lg">
                            <div className="flex items-center justify-between p-2 py-3">
                              <div className="text-sm dark:font-bold font-normal w-5/12 dark:text-blue text-[#221F20B3]">
                                BATTER
                              </div>
                              <div className="flex items-center justify-between w-6/12 text-sm font-semibold lg:mr-3">
                                <div className="w-2/12 text-center dark:text-xs text-sm dark:text-white text-[#221F20B3]">
                                  R
                                </div>
                                <div className="w-2/12 text-center dark:text-xs text-sm dark:text-white text-[#221F20B3]">
                                  B
                                </div>
                                <div className="w-2/12 text-center dark:text-xs text-sm dark:text-white text-[#221F20B3]">
                                  4S
                                </div>
                                <div className="w-1/12 text-center dark:text-xs text-sm dark:text-white text-[#221F20B3]">
                                  6S
                                </div>

                                <div className="w-3/12 text-center dark:text-xs text-sm dark:text-white text-[#221F20B3]">
                                  SR
                                </div>
                              </div>
                              <div className="flex items-center justify-between w-1/12 text-sm font-semibold "></div>
                            </div>
                          </div>
                          {data.getScoreCard.fullScoreCard[
                            teamShow
                          ].batting.map((item, index) => {
                            if (!item?.playerHowOut?.length == 0)
                              return (
                                <div className="w-full dark:bg-gray bg-white dark:border-gray border border-t-[#E2E2E2] border-b-[#E2E2E2]">
                                  <div className="flex flex-col items-center  justify-between p-2 py-3">
                                    <div className="flex w-full ">
                                      <div className="text-sm font-semibold w-5/12 dark:text-white text-black">
                                        {item.playerName}
                                        <span className="mx-0.5">
                                          {item?.isCaptain && !item?.isKeeper
                                            ? '(c)'
                                            : item?.isKeeper && !item?.isCaptain
                                            ? '(wk)'
                                            : item?.isKeeper && item?.isCaptain
                                            ? '(c) & (wk)'
                                            : ''}
                                        </span>
                                      </div>
                                      <div className="flex items-center justify-between w-6/12 text-sm font-semibold ">
                                        <div className="w-2/12 text-center dark:text-xs text-sm font-semibold dark:text-white text-black">
                                          {item.playerMatchRuns}
                                        </div>
                                        <div className="w-2/12 text-center dark:text-xs text-sm font-semibold dark:text-gray-2 text-black">
                                          {item.playerMatchBalls}
                                        </div>
                                        <div className="w-2/12 text-center dark:text-xs text-sm font-semibold dark:text-gray-2 text-[#221F20B3]">
                                          {item.playerMatchFours}
                                        </div>
                                        <div className="w-1/12 text-center dark:text-xs text-sm font-semibold dark:text-gray-2 text-[#221F20B3]">
                                          {item.playerMatchSixes}
                                        </div>
                                        <div className="w-3/12 text-center dark:text-xs text-sm font-semibold dark:text-gray-2 text-[#221F20B3]">
                                          {item.playerMatchStrikeRate}
                                        </div>
                                      </div>

                                      {/* <div className="flex items-center justify-between w-1/12 ml-3 text-sm font-semibold ">
                                        <img
                                          className="h-2 w-4 cursor-pointer lg:hidden md:hidden"
                                          onClick={() => setShowWheel(index)}
                                          src={upwarrow}
                                          alt=""
                                        />
                                        <img
                                          className="h-2 w-4 hidden cursor-pointer lg:flex md:flex"
                                          onClick={() => setShowWheel(index)}
                                          src={downicon}
                                          alt=""
                                        />
                                      </div> */}
                                      <div
                                        className={`hidden items-center md:flex justify-between ml-3 text-sm font-semibold ${
                                          showWheel == index ? 'rotate-180' : ''
                                        } `}
                                      >
                                        {item.playerMatchRuns > 0 ? (
                                          <img
                                            className="h-2 w-4 cursor-pointer"
                                            onClick={() =>
                                              showWheel !== index &&
                                              item.playerMatchRuns > 0
                                                ? setShowWheel(index)
                                                : setShowWheel(1000)
                                            }
                                            src={downicon}
                                            alt=""
                                          />
                                        ) : (
                                          // <ArrowIcon clr={'#020617'} />
                                          ''
                                        )}
                                      </div>
                                      {item.playerMatchRuns > 0 ? (
                                        <div
                                          className={`flex items-center md:hidden justify-between ml-3 text-sm font-semibold ${
                                            showWheel == index
                                              ? 'rotate-180'
                                              : ''
                                          } `}
                                        >
                                          <img
                                            className="h-2 w-4 cursor-pointer"
                                            onClick={() =>
                                              showWheel !== index &&
                                              item.playerMatchRuns > 0
                                                ? setShowWheel(index)
                                                : setShowWheel(1000)
                                            }
                                            src="/pngsV2/dwarrow.png"
                                            alt=""
                                          />
                                        </div>
                                      ) : (
                                        <div
                                          className={`flex items-center md:hidden justify-between ml-3 text-sm font-semibold ${
                                            showWheel == index
                                              ? 'rotate-180'
                                              : ''
                                          } `}
                                        >
                                          {/* <ArrowIcon clr="#1f2937" /> */}
                                        </div>
                                      )}
                                    </div>
                                    <div className="flex w-full dark:text-gray-2 text-[#221F2099] text-xs items-start justify-start">
                                      {item.playerHowOut}
                                    </div>

                                    {showWheel === index && (
                                      <div className="flex w-full h-48 dark:bg-gray bg-white mt-5">
                                        <div className="w-6/12 flex justify-center items-center flex-col md:-mt-3">
                                          <div className="flex m-1 justify-center items-center relative ">
                                            <img
                                              className="absolute w-44 dark:w-32"
                                              src={
                                                'ssss' === 'Wickets'
                                                  ? '/svgs/groundImageWicket.png'
                                                  : '/pngsV2/wagon.png'
                                              }
                                              alt=""
                                            />
                                            <div className="flex m-1 justify-center items-center relative ">
                                              {/* {} */}
                                              <div className=" w-full h-full absolute  overflow-hidden rounded-full ">
                                                {item.zad &&
                                                  item.zad.map((x, i) => (
                                                    <div
                                                      key={i}
                                                      className={`border-[.2px] overflow-hidden absolute ${
                                                        x.runs === '0'
                                                          ? 'hidden'
                                                          : (x.runs === '6' &&
                                                              'border-red w-56') ||
                                                            (x.runs === '5' &&
                                                              'border-green w-56') ||
                                                            (x.runs === '4' &&
                                                              'border-yellow w-56') ||
                                                            (x.runs === '2' &&
                                                              `border-blue-2 ${
                                                                x?.zadval?.split(
                                                                  ',',
                                                                )[2] == '3'
                                                                  ? 'w-[23px]'
                                                                  : x?.zadval?.split(
                                                                      ',',
                                                                    )[2] == '2'
                                                                  ? 'w-[25px]'
                                                                  : x?.zadval.split(
                                                                      ',',
                                                                    )[2] == '1'
                                                                  ? 'w-[20px]'
                                                                  : 'w-[50px]'
                                                              } `) ||
                                                            (x.runs === '3' &&
                                                              'border-pink-500 w-56') ||
                                                            `border-white ${
                                                              x?.zadval?.split(
                                                                ',',
                                                              )[2] == '3'
                                                                ? 'w-[27px]'
                                                                : x?.zadval?.split(
                                                                    ',',
                                                                  )[2] == '2'
                                                                ? 'w-[24px]'
                                                                : x?.zadval.split(
                                                                    ',',
                                                                  )[2] == '1'
                                                                ? 'w-[20px]'
                                                                : 'w-[50px]'
                                                            } `
                                                      } `}
                                                      style={{
                                                        bottom: '55%',
                                                        left: '50%',
                                                        right: '0%',
                                                        transform: `rotate(${
                                                          x.zadval.split(',')[1]
                                                        }deg)`,
                                                        transformOrigin: '0 0',
                                                      }}
                                                    ></div>
                                                  ))}
                                              </div>
                                              <div className="relative border rounded-full border-gray-3">
                                                {/* height 5.5 rem */}
                                                {/* window.screen.width > 975 ? window.screen.width/1.5 : window.screen.width+window.screen.width */}
                                                <PieClass
                                                  data={dataa}
                                                  zoneRun={[
                                                    z2FineLeg,
                                                    z1SquarLeg,
                                                    z8MidWicket,
                                                    z7MidOn,
                                                    z6MidOff,
                                                    z5Cover,
                                                    z4Point,
                                                    z3ThirdMan,
                                                  ]}
                                                  width={
                                                    window.screen.width > 975
                                                      ? 208
                                                      : 150
                                                  }
                                                  height={
                                                    window.screen.width > 975
                                                      ? 208
                                                      : 150
                                                  }
                                                  battingStyle={
                                                    item.battingStyle
                                                  }
                                                  zadData={
                                                    item.zad ? item.zad : []
                                                  }
                                                  innerRadius={
                                                    window.screen.width > 975
                                                      ? 82
                                                      : 57
                                                  }
                                                  outerRadius={
                                                    window.screen.width > 975
                                                      ? 104
                                                      : 75
                                                  }
                                                />
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div className="flex items-center justify-center flex-col w-6/12">
                                          <div className="flex flex-col items-center justify-center w-10/12">
                                            <div className="flex dark:border-white border-black w-full">
                                              <div className="w-6/12 text-center border-r">
                                                <div className="flex w-full text-xs text-gray-2">
                                                  {item.playerMatchRuns != '0'
                                                    ? 100 -
                                                      parseInt(
                                                        percentage(
                                                          parseInt(
                                                            item.battingStyle ==
                                                              'LHB'
                                                              ? getTotal(
                                                                  'off',
                                                                  item.zad,
                                                                )
                                                              : getTotal(
                                                                  'LEG',
                                                                  item.zad,
                                                                ),
                                                          ),
                                                          parseInt(
                                                            item.playerMatchRuns,
                                                          ),
                                                        ),
                                                      )
                                                    : '0'}
                                                  % <br />
                                                  {item.battingStyle == 'LHB'
                                                    ? 'LEG SIDE'
                                                    : 'OFF SIDE'}
                                                </div>
                                              </div>

                                              <div className="w-6/12 text-center text-xs dark:text-white text-black">
                                                <>
                                                  {item.battingStyle != 'LHB'
                                                    ? getTotal('off', item.zad)
                                                    : getTotal('LEG', item.zad)}
                                                  <span className="px-1 text-gray-2">
                                                    Runs
                                                  </span>
                                                </>
                                              </div>
                                            </div>
                                            <div className="flex border-white w-full items-center">
                                              <div className="w-6/12 text-center  border-r mt-2">
                                                <div className="flex w-full text-xs text-gray-2">
                                                  {item.playerMatchRuns != '0'
                                                    ? parseInt(
                                                        percentage(
                                                          parseInt(
                                                            item.battingStyle ==
                                                              'LHB'
                                                              ? getTotal(
                                                                  'off',
                                                                  item.zad,
                                                                )
                                                              : getTotal(
                                                                  'LEG',
                                                                  item.zad,
                                                                ),
                                                          ),
                                                          parseInt(
                                                            item.playerMatchRuns,
                                                          ),
                                                        ),
                                                      )
                                                    : '0'}
                                                  % <br />
                                                  {item.battingStyle == 'LHB'
                                                    ? 'OFF SIDE'
                                                    : 'LEG SIDE'}
                                                </div>
                                              </div>

                                              <div className="w-6/12 text-center text-xs dark:text-white text-black">
                                                <>
                                                  {item.battingStyle == 'LHB'
                                                    ? getTotal('off', item.zad)
                                                    : getTotal('LEG', item.zad)}
                                                  <span className="px-1 text-gray-2">
                                                    Runs
                                                  </span>
                                                </>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="flex flex-wrap items-center justify-center">
                                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                                              <div className="w-2 h-2 mx-1 bg-white md:border rounded-full"></div>{' '}
                                              <span className="dark:text-gray-2 text-[#221F2099]">
                                                1 Run
                                              </span>
                                            </div>
                                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                                              <div className="w-2 h-2 mx-1 bg-blue rounded-full"></div>{' '}
                                              <span className="dark:text-gray-2 text-[#221F2099]">
                                                2 Run
                                              </span>
                                            </div>
                                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                                              <div className="w-2 h-2 mx-1 bg-pink-500 rounded-full"></div>{' '}
                                              <span className="dark:text-gray-2 text-[#221F2099]">
                                                3 Run
                                              </span>
                                            </div>
                                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                                              <div className="w-2 h-2 mx-1 bg-yellow rounded-full"></div>{' '}
                                              <span className="dark:text-gray-2 text-[#221F2099]">
                                                4 Run
                                              </span>
                                            </div>
                                            <div className="flex items-center justify-center ml-2 text-xs py-2 ">
                                              <div className="w-2 h-2 mx-1 bg-red rounded-full"></div>{' '}
                                              <span className="dark:text-gray-2 text-[#221F2099]">
                                                6 Run
                                              </span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              )
                          })}
                          <div className=" flex justify-between dark:bg-gray-4 py-3 px-2 border-b dark:border-gray border-white w-full">
                            <span className="dark:text-xs text-sm dark:text-white text-black font-bold">
                              Extras
                            </span>
                            <span className="text-xs text-gray-2 font-bold">
                              <span className="text-white font-bold">
                                {
                                  data.getScoreCard.fullScoreCard[teamShow]
                                    .extras.totalExtras
                                }
                              </span>{' '}
                              {` (b ${data.getScoreCard.fullScoreCard[teamShow].extras.byes}, lb ${data.getScoreCard.fullScoreCard[teamShow].extras.legByes}, nb ${data.getScoreCard.fullScoreCard[teamShow].extras.noBalls}, wd ${data.getScoreCard.fullScoreCard[teamShow].extras.wides})`}
                            </span>
                          </div>
                          <div className="w-full flex justify-between pv3 ph2 bg-gray-4 md:bg-white py-2 items-center">
                            <span className="w-4/12 text-xs lg:py-3 md:py-3 md:text-black text-white font-bold ml-2">
                              Total:{' '}
                              {data.getScoreCard.fullScoreCard[teamShow].total
                                .runsScored &&
                                `${data.getScoreCard.fullScoreCard[teamShow].total.runsScored}/${data.getScoreCard.fullScoreCard[teamShow].total.wickets}`}{' '}
                            </span>

                            <span className="w-4/12  text-xs text-white md:text-black font-bold ml-2 dark:text-gray-2">
                              Overs:{' '}
                              {data.getScoreCard.fullScoreCard[teamShow].total
                                .runsScored &&
                                data.getScoreCard.fullScoreCard[teamShow].total
                                  .overs}{' '}
                            </span>
                            <span className=" w-4/12  text-xs text-left md:text-black text-white font-bold ml-2 dark:text-gray-2">
                              RR:{' '}
                              {
                                data.getScoreCard.fullScoreCard[teamShow].total
                                  .runRate
                              }{' '}
                            </span>
                          </div>
                          <div className="w-full flex-col dark:py-2 flex dark:bg-gray justify-start rounded-b-md">
                            <div className="flex gap-1 md:mt-3">
                              {props?.matchStatus === 'completed' ? (
                                <span className="md:w-3/12 w-3/12 whitespace-nowrap dark:text-white text-black text-xs ml-2">
                                  Did not Bat:
                                </span>
                              ) : (
                                <span className="md:w-3/12 whitespace-nowrap w-3/12 dark:text-white text-black text-xs ml-2">
                                  Yet to Bat:
                                </span>
                              )}

                              <span className="text-xs text-gray-2">
                                {data.getScoreCard.fullScoreCard[
                                  teamShow
                                ].batting
                                  .filter((e) => e.playerHowOut === '')
                                  .map((batsmen, i) => ' ' + batsmen.playerName)
                                  .toString()}
                              </span>
                            </div>
                            {data?.getScoreCard?.fullScoreCard[0].impactPlayer
                              .length > 0 && (
                              <div className="pt-2">
                                <span className="md:text-black text-white ml-2 ">
                                  (ip) ={' '}
                                  <span className="text-gray-2">
                                    Impact Player
                                  </span>
                                </span>
                              </div>
                            )}
                          </div>
                        </>
                      }

                      {data?.getScoreCard?.fullScoreCard[0].impactPlayer
                        .length > 0 && (
                        <div className="flex w-full flex-col  items-start justify-center m-2">
                          <div className="mt-4 w-full dark:bg-gray-4 lg:border lg:border-solid lg:border-[#E2E2E2] dark:rounded-t-lg">
                            <div className="flex items-center justify-between p-2 py-3">
                              <div className="text-sm w-5/12 dark:text-blue text-[#221F2099] font-bold uppercase">
                                Impact Player
                              </div>

                              <div className="flex items-center justify-between w-1/12 text-sm font-semibold "></div>
                            </div>
                            {/* {data?.getScoreCard?.fullScoreCard[
                            teamShow
                          ].impactPlayer.map((item, index) => {
                            return <span classname="mx-2">{'' + item}</span>
                          })} */}
                            <div className="mx-2 text-gray-2 md:text-sm text-xs">
                              {data?.getScoreCard?.fullScoreCard[
                                teamShow
                              ].impactPlayer.join(', ')}
                            </div>
                            <div className="h-[1px] my-2 w-full md:bg-[#E2E2E2] bg-light_gray"></div>
                            {data?.getScoreCard?.fullScoreCard[
                              teamShow
                            ].Substitutes?.map((e) => {
                              return (
                                <div className="flex py-2 px-1 justify-between">
                                  <span className="text-gray-2 md:text-sm text-xs">
                                    {e.playerInName}(IN)
                                  </span>
                                  <span className="text-gray-2 md:text-sm text-xs">
                                    {e.playerOutName}(OUT)
                                  </span>
                                </div>
                              )
                            })}
                          </div>
                        </div>
                      )}

                      {/* bowler */}
                      {
                        <div className="flex w-full flex-col  items-start justify-center  m-2  ">
                          <div className="mt-4 w-full dark:bg-gray-4 lg:border lg:border-solid lg:border-[#E2E2E2] dark:rounded-t-lg">
                            <div className="flex items-center justify-between p-2 py-3">
                              <div className="text-sm w-5/12 dark:text-blue text-[#221F2099] font-bold uppercase">
                                BOWLER
                              </div>
                              <div className="flex items-center justify-between w-6/12 text-sm font-semibold   ">
                                <div className="w-2/12 text-center dark:text-xs text-sm dark:text-white text-[#221F2099]">
                                  O
                                </div>
                                <div className="w-2/12 text-center dark:text-xs text-sm dark:text-white text-[#221F2099]">
                                  M
                                </div>
                                <div className="w-2/12 text-center dark:text-xs text-sm dark:text-white text-[#221F2099]">
                                  R
                                </div>
                                <div className="w-1/12 text-center dark:text-xs text-sm dark:text-white text-[#221F2099]">
                                  W
                                </div>

                                <div className="w-3/12 text-center dark:text-xs text-sm dark:text-white text-[#221F2099]">
                                  ECO
                                </div>
                              </div>
                              <div className="flex items-center justify-between w-1/12 text-sm font-semibold "></div>
                            </div>
                          </div>
                          {data.getScoreCard.fullScoreCard[
                            teamShow
                          ].bowling.map((item, index) => {
                            return (
                              <div className=" w-full dark:bg-gray bg-white dark:border-gray border border-t-[#E2E2E2] border-b-[#E2E2E2] ">
                                <div className="flex flex-col items-center  justify-between p-2 py-3">
                                  <div className="flex w-full">
                                    <div className="text-sm font-semibold w-5/12 dark:text-white text-black">
                                      {item.playerName}
                                      <span className="mx-0.5">
                                        {item?.isCaptain && !item?.isKeeper
                                          ? '(c)'
                                          : item?.isKeeper && !item?.isCaptain
                                          ? '(wk)'
                                          : item?.isKeeper && item?.isCaptain
                                          ? '(c) & (wk)'
                                          : ''}
                                      </span>
                                    </div>
                                    <div className="flex items-center justify-between w-6/12 text-sm font-semibold ">
                                      <div className="w-2/12 text-center dark:text-xs text-sm font-semibold dark:text-white text-black">
                                        {item.playerOversBowled}
                                      </div>
                                      <div className="w-2/12 text-center dark:text-xs text-sm font-semibold dark:text-white text-black">
                                        {item.playerMaidensBowled}
                                      </div>
                                      <div className="w-2/12 text-center dark:text-xs text-sm font-semibold dark:text-white text-[#221F2099]">
                                        {item.playerRunsConceeded}
                                      </div>
                                      <div className="w-1/12 text-center dark:text-xs text-sm font-semibold dark:text-white text-[#221F2099]">
                                        {item.playerWicketsTaken}
                                      </div>
                                      <div className="w-3/12 text-center dark:text-xs text-sm font-semibold dark:text-white text-[#221F2099]">
                                        {item.playerEconomyRate}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            )
                          })}
                        </div>
                      }

                      {/* FOW */}

                      {
                        <div className="flex w-full flex-col items-start dark:justify-center justify-start m-2">
                          <div className="flex items-center dark:justify-center justify-start dark:text-gray-3 text-[#221F2099] font-bold w-full ml-2 text-sm">
                            FALL OF WICKETS
                          </div>
                          <div className="mt-4 w-full dark:bg-gray-4 rounded-t-lg">
                            <div className="flex items-center justify-center w-full p-2 py-3">
                              <div className="text-sm flex items-center justify-start w-4/12 dark:text-blue text-black font-bold uppercase">
                                Score{' '}
                              </div>
                              <div className="text-sm flex items-center justify-center w-4/12 dark:text-blue text-black font-bold uppercase">
                                Player
                              </div>

                              <div className="text-sm flex items-center justify-end w-4/12 dark:text-blue text-black font-bold uppercase">
                                Over
                              </div>
                            </div>
                          </div>
                          {data.getScoreCard.fullScoreCard[teamShow].fow.map(
                            (item, index) => {
                              return (
                                <div className="w-full dark:bg-gray bg-white dark:border-gray border border-t-[#E2E2E2] border-b-[#E2E2E2] ">
                                  <div className="flex items-center  justify-center w-full p-2 py-3">
                                    <div className="text-xs flex items-center  justify-start w-3/12 dark:text-white text-black font-bold uppercase">
                                      {' '}
                                      {item.runs}/{item.order}
                                    </div>
                                    <div className=" flex items-center  justify-center  w-6/12 text-gray-2 font-bold uppercase text-xs">
                                      {item.playerName}
                                    </div>

                                    <div className="text-xs flex items-center  justify-end w-3/12 dark:text-gray-2 md:text-black font-bold uppercase">
                                      {item.over_ball}
                                    </div>
                                  </div>
                                </div>
                              )
                            },
                          )}
                        </div>
                      }
                    </>
                  )}{' '}
                </div>
              )
            })
          ) : (
            <div className="dark:text-white text-black text-xs p-4 text-center mt-5">
              Full Scorecard & Commentary is not available for this match{' '}
            </div>
          )}
        </div>

        {false && (
          <>
            {data && data.getScoreCard.fullScoreCard !== null ? (
              data.getScoreCard.fullScoreCard.map((ex, i) => (
                <div className="flex items-center justify-center" key={i}>
                  <div
                    className={`flex justify-between  ${
                      i === card ? 'cdcgr' : 'bg-gray'
                    } `}
                    onClick={() => setCard((prev) => (prev === i ? 5 : i))}
                  >
                    <span className="f6 fw6 ">
                      {ex.battingTeamShortName}
                      {props.matchData.matchType === 'Test' &&
                        (i <= 1 ? '1st Inn' : '2nd Inn')}
                    </span>
                    <div className="flex items-center">
                      <span className="f6 fw6 mh2">
                        {ex.total.runsScored}
                        {ex.total.wickets && ex.total.wickets !== '10'
                          ? `/${ex.total.wickets}`
                          : null}
                        {ex.total.overs ? `(${ex.total.overs})` : ''}
                      </span>
                      <img
                        className={`w1 h1 pointer ${
                          i === card ? '' : 'rotate-180'
                        }  `}
                        src={upArrowWhite}
                        alt=""
                      />
                    </div>
                  </div>

                  {/* batsman details */}
                  <div
                    className={`${card === i ? 'flex  flex-col' : 'hidden'}`}
                  >
                    <div className="">
                      <div className="text-gray-2 bg-gray  font-semibold flex justify-between items-center w-full py-3 px-2">
                        <div className="  f8 w-50">BATTER</div>
                        <div className="  f8 tr ph2 w-10">R</div>
                        <div className="  f8 tr ph2 w-10">B</div>
                        <div className="  f8 tr ph2 w-10">4s</div>
                        <div className="  f8 tr ph2 w-10">6s</div>
                        <div className="  f8 tr ph2 tr w-10">SR</div>
                      </div>
                      <div>
                        {ex.batting.map(
                          (batsmen, i) =>
                            batsmen.playerHowOut && (
                              <div>
                                <div
                                  key={i}
                                  className="flex justify-between items-center p-2"
                                >
                                  <Link
                                    {...getPlayerUrl(
                                      batsmen.playerName,
                                      batsmen.playerID,
                                    )}
                                    passHref
                                  >
                                    <a className="w-50">
                                      <div
                                        className=" tl fw5 f7  pointer"
                                        onClick={() =>
                                          handlePlayerNavigation(
                                            batsmen.playerName,
                                            batsmen.playerID,
                                          )
                                        }
                                      >
                                        <span className="tl font-medium f7">
                                          {' '}
                                          {batsmen.playerName}{' '}
                                        </span>
                                        <span className="pl2 fw6 f7">
                                          {' '}
                                          {batsmen.isCaptain && batsmen.isKeeper
                                            ? '(c) & (Wk)'
                                            : batsmen.isCaptain
                                            ? '(c)'
                                            : batsmen.isKeeper
                                            ? '(Wk)'
                                            : ''}
                                        </span>
                                        <br />
                                        <p
                                          className={`f8 mb0 ${
                                            batsmen.playerDismissalInfo ===
                                            'not out'
                                              ? 'red font-medium'
                                              : 'text-gray-2'
                                          }`}
                                        >
                                          {batsmen.playerHowOut}
                                        </p>
                                      </div>
                                    </a>
                                  </Link>
                                  <div className=" f8 ph2 tr w-10">
                                    {batsmen.playerMatchRuns}
                                  </div>
                                  <div className=" f8 ph2 tr w-10">
                                    {batsmen.playerMatchBalls}
                                  </div>
                                  <div className=" f8 ph2 tr w-10">
                                    {batsmen.playerMatchFours}
                                  </div>
                                  <div className=" f8 ph2 tr w-10">
                                    {batsmen.playerMatchSixes}
                                  </div>
                                  <div className=" f8 ph2 tr w-10">
                                    {batsmen.playerMatchStrikeRate}
                                  </div>
                                </div>
                                <div className="h-[1px] w-full bg-black/80"></div>
                              </div>
                            ),
                        )}
                      </div>
                    </div>
                    <div className="w-100 flex justify-between pv3 ph2">
                      <span className="f7 fw5">Extras</span>
                      <span className="f7 fw5">{`${ex.extras.totalExtras} (b ${ex.extras.byes}, lb ${ex.extras.legByes}, nb ${ex.extras.noBalls}, wd ${ex.extras.wides})`}</span>
                    </div>
                    <div className="w-100 flex justify-between pv3 ph2 bg-gray-4 items-center">
                      <span className="f7 white fw5">Total</span>
                      <span className="f7 white fw5">
                        {ex.total.runsScored &&
                          `${ex.total.runsScored}/${ex.total.wickets} ( ${ex.total.overs} overs RR ${ex.total.runRate}  ) `}
                      </span>
                    </div>
                    {ex.batting.filter((e) => e.playerHowOut === '').length >
                      0 && (
                      <div className="w-100  f6 pv3 ph2">
                        <span className=" fw5">
                          {props.matchData.matchStatus === 'live'
                            ? 'Yet To Bat'
                            : 'Did Not Bat'}
                          :
                        </span>
                        <span className="f6 text-gray-2">
                          {ex.batting
                            .filter((e) => e.playerHowOut === '')
                            .map((batsmen, i) => ' ' + batsmen.playerName)
                            .toString()}
                        </span>
                      </div>
                    )}

                    {/* Bowlers Details */}

                    <div className="">
                      <div className="bg-gray text-gray-2 flex items-center justify-center p-2 font-semibold">
                        <div className="  f8 tl w-50">BOWLER</div>
                        <div className="  f8 tr w-10">0</div>
                        <div className="  f8 tr w-10">M</div>
                        <div className="  f8 tr w-10">R</div>
                        <div className="  f8 tr w-10">W</div>
                        <div className="  f8 tr w-10">Econ</div>
                      </div>
                      <div>
                        {ex.bowling.map((bowler, i) => (
                          <div>
                            <div
                              key={i}
                              className="w-full justify-between items-center flex p-2"
                            >
                              <Link
                                {...getPlayerUrl(
                                  bowler.playerName,
                                  bowler.playerID,
                                )}
                                passHref
                              >
                                <a className="w-50">
                                  <div
                                    className=" tl fw5 f7  pointer"
                                    onClick={() =>
                                      handlePlayerNavigation(
                                        bowler.playerName,
                                        bowler.playerID,
                                      )
                                    }
                                  >
                                    <span className="tl fw5 f7">
                                      {' '}
                                      {bowler.playerName}{' '}
                                    </span>
                                    <span className="pl2 fw6 f7">
                                      {' '}
                                      {bowler.isCaptain ? '(c)' : ''}
                                    </span>
                                    <br />
                                  </div>
                                </a>
                              </Link>
                              <div className=" f8 tr w-10">
                                {bowler.playerOversBowled}
                              </div>
                              <div className=" f8 tr w-10">
                                {bowler.playerMaidensBowled}
                              </div>
                              <div className=" f8 tr w-10">
                                {bowler.playerRunsConceeded}
                              </div>
                              <div className=" f8 tr w-10">
                                {bowler.playerWicketsTaken}
                              </div>
                              <div className=" f8 tr w-10">
                                {bowler.playerEconomyRate}
                              </div>
                            </div>
                            <div className="h-[1px] w-full bg-black/80"></div>
                          </div>
                        ))}
                      </div>
                    </div>
                    {ex.fow && ex.fow.length > 0 && (
                      <div className="">
                        <div className="p-2  bg-gray-4">
                          <span className="f7 fw5">Fall of Wickets</span>
                        </div>
                        <div className="flex justify-between p-2 items-center">
                          <span className="f7 fw6 fw5">Score</span>
                          <span className="f7 fw6 fw5"> Player</span>
                          <span className="f7 fw6 fw5"> Over </span>
                        </div>

                        {ex.fow.map((fow, i) => (
                          <div>
                            <div key={i} className="flex justify-between p-2 ">
                              <span className="f8 fw5">
                                {fow.runs}/{fow.order}
                              </span>
                              <div className="flex items-center">
                                <span className="f8 fw5">
                                  {' '}
                                  {fow.playerName}{' '}
                                </span>
                                <span className=" ml2  fw6 f7">
                                  {' '}
                                  {fow.isCaptain && fow.isKeeper
                                    ? '(c) & (Wk)'
                                    : fow.isCaptain
                                    ? '(c)'
                                    : fow.isKeeper
                                    ? '(Wk)'
                                    : ''}
                                </span>
                              </div>
                              {/* <span className='f8 fw5'> {fow.playerName} </span> */}
                              <span className="f8 fw5"> {fow.over_ball} </span>
                            </div>
                            <div className="h-[1px] w-full bg-black/80"></div>
                          </div>
                        ))}
                      </div>
                    )}
                  </div>
                </div>
              ))
            ) : (
              <div className="bg-gray font-semibold text-sm p-2 mt2  tc text-white">
                Scorecard will begin as soon as the match commences
              </div>
            )}
          </>
        )}
      </div>
    )
  }
}
