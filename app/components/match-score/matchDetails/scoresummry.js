'use client'
import React from 'react'
import CleverTap from 'clevertap-react'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/navigation'
import { MATCH_DETAIL_LAYOUT } from '../../../constant/Links'
import {
  GET_ARTICLES_MATCH_INFO_SCORECARD,
  MATCH_DATA_FOR_SCORECARD,
  MATCH_DATA_TAB_WISE,
  MATCH_SUMMARY,
} from '../../../api/queries'
import MatchInfoTab from '../matchDetails/matchInfoTab'
import { format } from 'date-fns'
import TopPerformerCard from '../../commom/topPerformerCard'
import Link from 'next/link'
import Heading from '../../commom/heading'
import DataNotFound from '../../commom/datanotfound'
import Loading from '../../loading'
const empty = '/svgs/Empty.svg'
const RightSchevronWhite = '/svgs/RightSchevronWhite.svg'
const cricket = '/pngs/cricket.png'
const bowlerOverlay = '/pngs/bowlerOverlay.png'
const flagPlaceHolder = '/pngsV2/flag_dark.png'
const playerAvatar = '/pngsV2/playerph.png'

export default function ScorecardSummary({
  browser,
  matchID,
  params,
  noURL,
  ...props
}) {
  const router = useRouter()

  const { data: matchData } = useQuery(MATCH_DATA_FOR_SCORECARD, {
    variables: { matchID: matchID },
  })
  const { loading, error, data } = useQuery(MATCH_SUMMARY, {
    variables: {
      matchID: matchID,
      status: matchData?.miniScoreCard?.data[0].matchStatus,
    },
  })

  const { data: newData } = useQuery(MATCH_DATA_TAB_WISE, {
    variables: { matchID: matchID },
  })

  const { data: matchInfo } = useQuery(GET_ARTICLES_MATCH_INFO_SCORECARD, {
    variables: { type: 'matches', Id: matchID },
  })

  const match = matchData?.miniScoreCard?.data[0]

  const getScoreUrl = (match) => {
    let matchStatus =
      match.matchStatus === 'completed' ? 'match-score' : 'live-score'
    let matchID = match.matchID.toString()
    let matchName = `${match.homeTeamName}-vs-${
      match.awayTeamName
    }-${match.matchNumber.split(' ').join('-')}-`
    let seriesName =
      matchName.toLowerCase() +
      match.seriesName
        .replace(/[^a-zA-Z0-9]+/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase()
    let currentTab = match.isAbandoned
      ? 'commentary'
      : match.matchStatus === 'completed'
      ? 'summary'
      : 'scorecard'

    return {
      as: eval(MATCH_DETAIL_LAYOUT.as),
      href: eval(MATCH_DETAIL_LAYOUT.href),
    }
  }
  const handleCriclyticsNav = () => {
    if (match.isCricklyticsAvailable) {
      CleverTap.initialize('SummaryTab', {
        Source: 'Summary',
        MatchID: match.matchID,
        SeriesID: match.seriesID,
        TeamAID: match.matchScore[0].teamID,
        TeamBID: match.matchScore[1].teamID,
        MatchFormat: match.matchType,
        MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
        MatchStatus: match.matchStatus,
        // CriclyticsEngine: "CompletedMS",
        Platform: localStorage.Platform,
      })
    }
  }

  if (error) return <div></div>
  if (loading) {
    return (
      <div>
        <Loading />
      </div>
    )
  }
  // scorecard-up-arrow-triangle

  if (data?.matchSummary?.length === 0) {
    return <DataNotFound />
  } else
    return (
      <div className="">
        {data && data.matchSummary ? (
          <div
            className={`bg-basebg  ${!props.isDetails ? '' : ''} shadow-4 ${
              global && global.window && global.window.location.pathname === '/'
                ? ''
                : ''
            }`}
          >
            {/* title */}
            {!props.isDetails && noURL === undefined && !props.a23 && (
              <div className="flex scorecard-up-arrow-triangle   p-3 items-center justify-between border-b-2 b border-gray-2">
                <Heading heading="MATCH SUMMARY" />

                <Link {...getScoreUrl(match)} passHref>
                  <img
                    className="cursor-pointer"
                    alt={RightSchevronWhite}
                    src={RightSchevronWhite}
                    onClick={handleCriclyticsNav}
                  />
                </Link>
              </div>
            )}

            {/* content */}

            <TopPerformerCard
              data={data}
              matchSummary={
                newData && newData.getMatchCardTabWiseByMatchID?.matchSummary
              }
            />

            {/* topBatsman */}
            {false && (
              <div className="dn-ns">
                <div className="">
                  <div className="flex justify-start items-center relative  ">
                    <img
                      className="absolute contain z-0 top-0 left-0"
                      width="120"
                      height="120"
                      alt={cricket}
                      src={cricket}
                    />

                    <img
                      className="h-36 z-0 px-4 mt-2 "
                      alt=""
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBatsman.playerID}_headshot_safari.png`}
                      onError={(e) => {
                        e.target.src = playerAvatar
                      }}
                    />

                    <div className="">
                      <div className="white flex items-center">
                        <img
                          alt=""
                          src={`https://images.cricket.com/teams/${data.matchSummary.bestBatsman.playerTeamID}_flag_safari.png`}
                          onError={(e) => {
                            e.target.src = flagPlaceHolder
                          }}
                          className="w-8 h-5 shadow-4 mr2"
                        />
                        <span className="text-md font-semibold">
                          {' '}
                          {data.matchSummary.bestBatsman.playerName}
                        </span>
                      </div>

                      {data.matchSummary.bestBatsman.battingStatsList &&
                      props.matchType !== 'Test' ? (
                        <div className="rounded white   mt2 bg-white/20 mr3">
                          <div className="flex justify-between items-center">
                            <div className="flex justify-center items-center px-2 ">
                              <span className="f5 fw5 mr1">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="text-xs font-medium">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>

                            <div className="flex  m-1 bg-basebg rounded items-center">
                              <span className="text-xs near-white  p-1 tc ">
                                4s{' '}
                                <span className="white text-sm font-semibold">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].fours
                                  }
                                </span>{' '}
                              </span>
                              <span className="w-[1px] h-5 bg-gray-2" />

                              <span className="text-xs near-white  p-1  ">
                                6s{' '}
                                <span className="white text-sm font-semibold">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].sixes
                                  }
                                </span>{' '}
                              </span>
                            </div>
                          </div>

                          {data.matchSummary.bestBatsman.bowlingStatsList && (
                            <div>
                              <div className="flex  justify-between items-center">
                                <div className="flex w-40 justify-center items-center pa2">
                                  <span className="f5 fw5 mr1 nowrap">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="f7 ">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                                <div className="flex w-50 pa1 justify-end">
                                  <span className="f6 near-white w-100 bg-navy pv2 br2 tc nowrap">
                                    Eco:{' '}
                                    <span className="white text-sm font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[0].economyRate
                                      }
                                    </span>{' '}
                                  </span>
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                      ) : (
                        <div className="br2 white mt2 bg-basebg mr3">
                          <div className="flex justify-between items-center">
                            <div className="flex justify-center items-center pa2">
                              <span className="f5 fw5">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="f7 pl2">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>
                            <div className="flex justify-center items-center pa2">
                              {data.matchSummary.bestBatsman
                                .battingStatsList[1] && (
                                <span className="f5 fw5">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[1].runs
                                  }
                                </span>
                              )}
                              {data.matchSummary.bestBatsman
                                .battingStatsList[1] && (
                                <span className="f7">{`(${data.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                              )}
                            </div>
                          </div>

                          {data.matchSummary.bestBatsman.bowlingStatsList && (
                            <div>
                              <div className="flex justify-between items-center">
                                <div className="flex justify-center items-center pa2">
                                  <span className="f5 fw5">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="f7 pl2">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                                {data.matchSummary.bestBatsman
                                  .bowlingStatsList[1] && (
                                  <div className="">&</div>
                                )}
                                {data.matchSummary.bestBatsman
                                  .bowlingStatsList[1] && (
                                  <div className="flex justify-center items-center pa2">
                                    <span className="f5 fw5">
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[1].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[1].runsConceded
                                      }
                                    </span>
                                    <span className="f7 pl2">
                                      (
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[1].overs
                                      }
                                      )
                                    </span>
                                  </div>
                                )}
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                </div>

                {/* score details */}
                {false && (
                  <div className="br2 flex-ns flex-wrap ">
                    {data.matchSummary.inningOrder
                      .slice(
                        0,
                        props.matchType !== 'Test'
                          ? 2
                          : data.matchSummary.inningOrder.length,
                      )
                      .map((inning, i) => (
                        <div key={i} className=" w-50-ns br-ns bb-ns">
                          <div>
                            {/* Team Details */}

                            <div className="flex bg-gray justify-between items-center pa2 px-3 ">
                              <div className="flex  items-center">
                                <img
                                  alt=""
                                  src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                                  onError={(e) => {
                                    e.target.src = flagPlaceHolder
                                  }}
                                  className="w-7 h-4 mr2"
                                />
                                <div className="text-xs font-semibold">
                                  {data.matchSummary[inning].teamName}
                                </div>
                              </div>
                              <div>
                                <span className="text-sm font-semibold">
                                  {
                                    data.matchSummary[inning].runs[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                  /
                                  {
                                    data.matchSummary[inning].wickets[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                </span>
                                <span className="text-xs">
                                  {' '}
                                  (
                                  {
                                    data.matchSummary[inning].overs[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                  )
                                </span>
                              </div>
                            </div>

                            <div className="flex  justify-between items-center">
                              {data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].topBatsman &&
                                data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].topBatsman.battingStatsList && (
                                  <div className="flex w-100 justify-between pa2 pv3">
                                    <span className="f7 fw5 truncate w-75">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerName
                                      }
                                    </span>
                                    <span className="f7 fw5">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.battingStatsList[0].runs
                                      }
                                      {data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.battingStatsList[0]
                                        .isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                  </div>
                                )}

                              <div className="h-8 w-[1px] bg-black/80" />

                              {data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .topBowler &&
                                data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .topBowler.bowlingStatsList && (
                                  <div className="flex w-100  justify-between pa2 pv3">
                                    <span className="text-xs font-medium truncate w-75">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerName
                                      }
                                    </span>
                                    <span className="f7 fw5">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.bowlingStatsList[0].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.bowlingStatsList[0]
                                          .runsConceded
                                      }
                                    </span>
                                  </div>
                                )}
                            </div>

                            <div className="bg-black/80 h-[1px] mx-2 " />

                            {props.matchType !== 'Test' && (
                              <div className="flex  justify-between items-center">
                                {data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].runnerBatsman &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman.battingStatsList && (
                                    <div className="flex w-100 justify-between pa2 pv3">
                                      <span className="text-xs font-medium truncate w-75">
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.playerName
                                        }
                                      </span>
                                      <span className="f7 fw5">
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.battingStatsList[0]
                                            .runs
                                        }
                                        {data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.battingStatsList[0]
                                          .isNotOut === true
                                          ? '*'
                                          : ''}
                                      </span>
                                    </div>
                                  )}

                                <div className="h-8 w-[2px] bg-black/80 " />

                                {data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .runnerBowler &&
                                  data.matchSummary[
                                    inning === 'homeTeamData'
                                      ? 'awayTeamData'
                                      : 'homeTeamData'
                                  ][
                                    i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                  ].runnerBowler.bowlingStatsList && (
                                    <div className="flex w-100 justify-between pa2  pv3">
                                      <span className="text-xs font-medium truncate w-75 ">
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.playerName
                                        }
                                      </span>
                                      <span className="f7 fw5">
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.bowlingStatsList[0]
                                            .wickets
                                        }
                                        /
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.bowlingStatsList[0]
                                            .runsConceded
                                        }
                                      </span>
                                    </div>
                                  )}
                              </div>
                            )}
                          </div>
                        </div>
                      ))}
                  </div>
                )}

                {/* topBowler */}

                {false &&
                data.matchSummary.bestBowler.bowlingStatsList &&
                props.matchType !== 'Test' ? (
                  <div className="flex justify-between   white items-center mh2 mt2 relative overflow-hidden ">
                    {/* player NAME */}
                    <div className="flex items-center w-1/4">
                      <img
                        alt=""
                        src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = flagPlaceHolder
                        }}
                        className="ib w15 h1 shadow-4"
                      />
                      <div className="text-md font-semibold px-2">
                        {data.matchSummary.bestBowler.playerName}
                      </div>
                    </div>

                    {/* player Image */}
                    <div className=" w-1/2 flex justify-center h-24 overflow-hidden">
                      <img
                        className="object-cover object-top h-100 "
                        alt=""
                        src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                        onError={(e) => {
                          e.target.src = playerAvatar
                        }}
                      />
                      <img
                        width="100"
                        height="100"
                        className="absolute contain z-0 top-0"
                        alt={cricket}
                        src={cricket}
                      />
                    </div>

                    {/* player stats */}
                    <div className="w-1/4">
                      {data.matchSummary.bestBowler.battingStatsList &&
                        (data.matchSummary.bestBowler.battingStatsList[0]
                          .runs !== 0 ||
                          data.matchSummary.bestBowler.battingStatsList[0]
                            .balls !== 0) && (
                          <div className="flex justify-center items-center pa1">
                            <span className="f5 fw5 mr1 nowrap">
                              {
                                data.matchSummary.bestBowler.battingStatsList[0]
                                  .runs
                              }
                              {data.matchSummary.bestBowler.battingStatsList[0]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="f7 ">
                              (
                              {
                                data.matchSummary.bestBowler.battingStatsList[0]
                                  .balls
                              }
                              )
                            </span>
                          </div>
                        )}
                      {data.matchSummary.bestBowler.bowlingStatsList && (
                        <div className="flex justify-center items-center pa1">
                          <span className="f5 fw5 mr1 nowrap">
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .wickets
                            }
                            /
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .runsConceded
                            }
                          </span>
                          <span className="f7 ">
                            (
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                ) : (
                  <div className="flex justify-between white items-center relative mh3">
                    {/* player Image */}
                    <div>
                      <img
                        className="absolute contain z-0 top-0 left-0 bottom-0 "
                        width="80"
                        height="70"
                        alt={bowlerOverlay}
                        src={bowlerOverlay}
                      />
                      <img
                        className="nt3 h35 "
                        alt=""
                        src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                        onError={(e) => {
                          e.target.src = playerAvatar
                        }}
                      />
                    </div>

                    {/* player NAME */}
                    <div className="flex justify-center items-center center">
                      <img
                        alt=""
                        src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = flagPlaceHolder
                        }}
                        className="w15 h1 shadow-4 mr2"
                      />
                      <div className="f6 fw3">
                        {data.matchSummary.bestBowler.playerName}
                      </div>
                    </div>

                    {/* player stats */}
                    <div className="flex justify-between items-center pv3 w-45">
                      <div className="">
                        {data.matchSummary.bestBowler.battingStatsList && (
                          <div className="flex justify-start items-center pv1">
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.battingStatsList[0]
                                  .runs
                              }
                              {data.matchSummary.bestBowler.battingStatsList[0]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="f7 ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.battingStatsList[0]
                                  .balls
                              }
                              )
                            </span>
                          </div>
                        )}
                        {data.matchSummary.bestBowler.bowlingStatsList && (
                          <div className="flex justify-start items-center pv1">
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  .wickets
                              }
                              /
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  .runsConceded
                              }
                            </span>
                            {/* <span>&nbsp;</span> */}
                            <span className="f7 ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  .overs
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                      <div className="">
                        {data.matchSummary.bestBowler.battingStatsList[1] && (
                          <div className="pa1 f7">&</div>
                        )}
                        {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                          <div className="pa1 f7">&</div>
                        )}
                      </div>
                      <div className="">
                        {data.matchSummary.bestBowler.battingStatsList && (
                          <div className="flex justify-start items-center pv1">
                            {data.matchSummary.bestBowler
                              .battingStatsList[1] && (
                              <span className="f6 fw5">
                                {
                                  data.matchSummary.bestBowler
                                    .battingStatsList[1].runs
                                }
                                {data.matchSummary.bestBowler
                                  .battingStatsList[1].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                            )}
                            {data.matchSummary.bestBowler
                              .battingStatsList[1] && (
                              <span className="f7 ">
                                &nbsp;(
                                {
                                  data.matchSummary.bestBowler
                                    .battingStatsList[1].balls
                                }
                                )
                              </span>
                            )}
                          </div>
                        )}
                        {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                          <div className="flex justify-start items-center pv1">
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .wickets
                              }
                              /
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .runsConceded
                              }
                            </span>
                            <span className="f7 ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .overs
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}

            {/* -------------------------------------------------------desktop only----------------------------------------------------------------------- */}
            {false && (
              <div className="dn db-ns">
                <div className="bg-gray-4 flex items-starts">
                  <div className="w-50 relative ">
                    {/* ---------------top batman---------------------------- */}
                    <div className="flex justify-start items-center  h4 ">
                      <img
                        className="h-100 z-0 ph3 bw0"
                        alt=""
                        src={`https://images.cricket.com/players/${data.matchSummary.bestBatsman.playerID}_headshot_safari.png`}
                        onError={(e) => {
                          e.target.src = playerAvatar
                        }}
                      />

                      <div className="">
                        <div className="white flex items-center pa2">
                          <img
                            alt=""
                            src={`https://images.cricket.com/teams/${data.matchSummary.bestBatsman.playerTeamID}_flag_safari.png`}
                            onError={(e) => {
                              e.target.src = flagPlaceHolder
                            }}
                            className="ib w15 h1 shadow-4 mr2"
                          />
                          <span className="f5 fw5">
                            {' '}
                            {data.matchSummary.bestBatsman.playerName}
                          </span>
                        </div>

                        {data.matchSummary.bestBatsman.battingStatsList &&
                        props.matchType !== 'Test' ? (
                          // -----------------------------------ODI && T20----------------------------------------------

                          <div className="br2 white mt2   mr3">
                            <div className="flex justify-between items-center">
                              <div className="flex justify-center items-center pv2 ph3 bg-white-10 br-pill">
                                <span className="f5 fw5 mr1">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].runs
                                  }
                                  {data.matchSummary.bestBatsman
                                    .battingStatsList[0].isNotOut === true
                                    ? '*'
                                    : ''}
                                </span>
                                <span className="f6 fw5 ">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].balls
                                  }
                                  )
                                </span>
                              </div>

                              <div className="flex justify-end pa1">
                                <span className="f5  pa2 fw5">
                                  6s{' '}
                                  <span className=" fw6">
                                    {
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[0].sixes
                                    }
                                  </span>{' '}
                                </span>
                                <span className="f5  pa2 fw5">
                                  4s{' '}
                                  <span className=" fw6">
                                    {
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[0].fours
                                    }
                                  </span>{' '}
                                </span>
                              </div>
                            </div>

                            {data.matchSummary.bestBatsman.bowlingStatsList && (
                              <div className="flex justify-between items-center">
                                <div className="flex justify-center items-center pv2 ph3 bg-white-10 br-pill">
                                  <span className="f5 fw5 mr1 nowrap">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="f7 ">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                                <div className="flex ph3 pv2 justify-center items-center bg-white-10 br-pill">
                                  <span className="f6 near-white w-100 br2 tc ">
                                    Eco:{' '}
                                    <span className=" fw6">
                                      {' '}
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[0].economyRate
                                      }
                                    </span>{' '}
                                  </span>
                                </div>
                              </div>
                            )}
                          </div>
                        ) : (
                          // ***************************************************  Test Match Top Batman *****************************************************
                          <div className="br2 white mt2  mr3">
                            <div className="flex justify-between items-center">
                              <div className="flex justify-center items-center pv2 bg-white-10 br-pill ph3">
                                <span className="f5 fw5 mr1">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].runs
                                  }
                                  {data.matchSummary.bestBatsman
                                    .battingStatsList[0].isNotOut === true
                                    ? '*'
                                    : ''}
                                </span>
                                <span className="f6 fw5 ">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].balls
                                  }
                                  )
                                </span>
                              </div>
                              {data.matchSummary.bestBatsman
                                .battingStatsList[1] && (
                                <div className="ph2">&</div>
                              )}
                              {data.matchSummary.bestBatsman
                                .battingStatsList[1] && (
                                <div className="flex justify-center items-center pv2 bg-white-10 br-pill ph3">
                                  {data.matchSummary.bestBatsman
                                    .battingStatsList[1] && (
                                    <span className="f5 fw5">
                                      {
                                        data.matchSummary.bestBatsman
                                          .battingStatsList[1].runs
                                      }
                                    </span>
                                  )}
                                  {data.matchSummary.bestBatsman
                                    .battingStatsList[1] && (
                                    <span className="f6 fw5 ">
                                      (
                                      {
                                        data.matchSummary.bestBatsman
                                          .battingStatsList[1].balls
                                      }
                                      )
                                    </span>
                                  )}
                                </div>
                              )}
                            </div>

                            {data.matchSummary.bestBatsman.bowlingStatsList && (
                              <div className="flex justify-between items-center mt2">
                                <div className="flex justify-center items-center pv2 bg-white-10 br-pill ph3">
                                  <span className="f5 fw5">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="f6 fw5 ">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                                {data.matchSummary.bestBatsman
                                  .bowlingStatsList[1] && (
                                  <div className="ph2">&</div>
                                )}
                                {data.matchSummary.bestBatsman
                                  .bowlingStatsList[1] && (
                                  <div className="flex justify-center items-center pv2 bg-white-10 br-pill ph3">
                                    <span className="f5 fw5">
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[1].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[1].runsConceded
                                      }
                                    </span>
                                    <span className="f6 fw5 ">
                                      (
                                      {
                                        data.matchSummary.bestBatsman
                                          .bowlingStatsList[1].overs
                                      }
                                      )
                                    </span>
                                  </div>
                                )}
                              </div>
                            )}
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="h-[1px] bg-black/80 " />

                    {/* *---------------------------- TOP Bowler desktop -----------------------------* */}
                    <div>
                      {data.matchSummary.bestBowler.bowlingStatsList &&
                      props.matchType !== 'Test' ? (
                        /* ************************************* TOP Bowler ODI &&  T20************************************************************* */

                        <div className="flex  justify-start white items-center h4  ">
                          {/* player Image */}
                          <img
                            className="h-100 z-0 ph3 bw0"
                            alt=""
                            src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                            onError={(e) => {
                              e.target.src = playerAvatar
                            }}
                          />

                          <div className="pl4">
                            {/* player NAME */}
                            <div className="flex items-center white pv2">
                              <img
                                alt=""
                                src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                                onError={(e) => {
                                  e.target.src = flagPlaceHolder
                                }}
                                className="ib w15 h1 shadow-4 mr2"
                              />
                              <div className="f5 fw5">
                                {data.matchSummary.bestBowler.playerName}
                              </div>
                            </div>

                            {/* player stats */}
                            <div className="">
                              {data.matchSummary.bestBowler.battingStatsList &&
                                (data.matchSummary.bestBowler
                                  .battingStatsList[0].runs !== 0 ||
                                  data.matchSummary.bestBowler
                                    .battingStatsList[0].balls !== 0) && (
                                  <div className="flex justify-center items-center">
                                    <div className="pv2 ph3 br-pill bg-white-10">
                                      <span className="f5 fw5 mr1">
                                        {
                                          data.matchSummary.bestBowler
                                            .battingStatsList[0].runs
                                        }
                                        {data.matchSummary.bestBowler
                                          .battingStatsList[0].isNotOut === true
                                          ? '*'
                                          : ''}
                                      </span>
                                      <span className="f6 fw5  ">
                                        (
                                        {
                                          data.matchSummary.bestBowler
                                            .battingStatsList[0].balls
                                        }
                                        )
                                      </span>
                                    </div>
                                  </div>
                                )}
                              {data.matchSummary.bestBowler
                                .bowlingStatsList && (
                                <div className="flex justify-center items-center pt2">
                                  <div className=" bg-white-10 br-pill pv2 ph3 ">
                                    <span className="f5 fw5 mr1">
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[0].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[0].runsConceded
                                      }
                                    </span>
                                    <span className="f6 fw5  ">
                                      (
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[0].overs
                                      }
                                      )
                                    </span>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      ) : (
                        /* ************************************* TOP Bowler Test ************************************************************* */

                        <div className="flex justify-start white items-center  mt2 h4 ">
                          {/* player Image */}

                          <img
                            className="h-100 ph3 bw0 "
                            alt=""
                            src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                            onError={(e) => {
                              e.target.src = playerAvatar
                            }}
                          />

                          {/* player NAME */}
                          <div className="pl4 ">
                            <div className="flex justify-start items-center pv2">
                              <img
                                alt=""
                                src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                                onError={(e) => {
                                  e.target.src = flagPlaceHolder
                                }}
                                className="w15 h1 shadow-4 mr2"
                              />
                              <div className="f5 fw5">
                                {data.matchSummary.bestBowler.playerName}
                              </div>
                            </div>

                            {/* player stats */}
                            <div className="mb2">
                              <div className="flex items-center">
                                {data.matchSummary.bestBowler
                                  .battingStatsList && (
                                  <div className="flex justify-center items-center pv1  bg-white-10 br-pill  pv2 ph3">
                                    <span className="f5 fw5">
                                      {
                                        data.matchSummary.bestBowler
                                          .battingStatsList[0].runs
                                      }
                                      {data.matchSummary.bestBowler
                                        .battingStatsList[0].isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                    <span className="f6 fw5 ">
                                      &nbsp;(
                                      {
                                        data.matchSummary.bestBowler
                                          .battingStatsList[0].balls
                                      }
                                      )
                                    </span>
                                  </div>
                                )}
                                {data.matchSummary.bestBowler
                                  .battingStatsList[1] && (
                                  <div className="ph1 f5 fw5 ">&</div>
                                )}
                                {data.matchSummary.bestBowler
                                  .battingStatsList &&
                                  data.matchSummary.bestBowler
                                    .battingStatsList[1] && (
                                    <div className="flex justify-center items-center pv1 bg-white-10 br-pill ma1 pv2 ph3">
                                      {
                                        <span className="f5 fw5">
                                          {
                                            data.matchSummary.bestBowler
                                              .battingStatsList[1].runs
                                          }
                                          {data.matchSummary.bestBowler
                                            .battingStatsList[1].isNotOut ===
                                          true
                                            ? '*'
                                            : ''}
                                        </span>
                                      }
                                      {
                                        <span className="f6 fw5  ">
                                          &nbsp;(
                                          {
                                            data.matchSummary.bestBowler
                                              .battingStatsList[1].balls
                                          }
                                          )
                                        </span>
                                      }
                                    </div>
                                  )}
                              </div>

                              <div className="flex items-center mt2">
                                {data.matchSummary.bestBowler
                                  .bowlingStatsList && (
                                  <div className="flex justify-center items-center  bg-white-10 br-pill pv2 ph3">
                                    <span className="f5 fw5">
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[0].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[0].runsConceded
                                      }
                                    </span>
                                    <span className="f6 fw5  ">
                                      &nbsp;(
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[0].overs
                                      }
                                      )
                                    </span>
                                  </div>
                                )}
                                {data.matchSummary.bestBowler
                                  .bowlingStatsList[1] && (
                                  <div className="ph1 f5 fw5 ">&</div>
                                )}
                                {data.matchSummary.bestBowler
                                  .bowlingStatsList[1] && (
                                  <div>
                                    <div className="flex justify-start items-center pv1 bg-white-10 br-pill ma1 pv2 ph3">
                                      <span className="f5 fw5">
                                        {
                                          data.matchSummary.bestBowler
                                            .bowlingStatsList[1].wickets
                                        }
                                        /
                                        {
                                          data.matchSummary.bestBowler
                                            .bowlingStatsList[1].runsConceded
                                        }
                                      </span>
                                      <span className="f6 fw5  ">
                                        &nbsp;(
                                        {
                                          data.matchSummary.bestBowler
                                            .bowlingStatsList[1].overs
                                        }
                                        )
                                      </span>
                                    </div>
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                  {/* ************************************************** Team score details ****************************************************************** */}

                  <div className="w-50 ">
                    <div className=" ">
                      {data.matchSummary.inningOrder
                        .slice(
                          0,
                          props.matchType !== 'Test'
                            ? 2
                            : data.matchSummary.inningOrder.length,
                        )
                        .map((inning, i) => (
                          <div key={i} className="">
                            {/* Team Details */}

                            <div className="flex justify-between items-center pa2 bg-gray">
                              <div className="flex  items-center">
                                <img
                                  alt=""
                                  src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                                  onError={(e) => {
                                    e.target.src = flagPlaceHolder
                                  }}
                                  className="ib w15 h1 shadow-4 mr3"
                                />
                                <div className="f6 fw6">
                                  {data.matchSummary[
                                    inning
                                  ].teamName.toUpperCase()}
                                </div>
                              </div>
                              <div>
                                <span className="f6 fw7 darkRed">
                                  {
                                    data.matchSummary[inning].runs[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                  /
                                  {
                                    data.matchSummary[inning].wickets[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                </span>
                                <span className="f7 fw6  pl2">
                                  {' '}
                                  (
                                  {
                                    data.matchSummary[inning].overs[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                  )
                                </span>
                              </div>
                            </div>

                            <div className="flex  justify-between ">
                              {data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].topBatsman &&
                                data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].topBatsman.battingStatsList && (
                                  <div className="flex w-100 justify-between pa2 pv2">
                                    <span className="f6 fw5 truncate w-75 ">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerName
                                      }
                                    </span>
                                    <span className="f6 fw6 ">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.battingStatsList[0].runs
                                      }
                                      {data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.battingStatsList[0]
                                        .isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                  </div>
                                )}
                              <div className="br b--moon-gray mt1" />
                              {data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .topBowler &&
                                data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .topBowler.bowlingStatsList && (
                                  <div className="flex w-100  justify-between pa2 pv2">
                                    <span className="f6 fw5  truncate w-75">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerName
                                      }
                                    </span>
                                    <span className="f6 fw6 ">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.bowlingStatsList[0].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.bowlingStatsList[0]
                                          .runsConceded
                                      }
                                    </span>
                                  </div>
                                )}
                            </div>

                            {props.matchType !== 'Test' && (
                              <div className="flex  justify-between ">
                                {data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].runnerBatsman &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman.battingStatsList && (
                                    <div className="flex w-100 justify-between pa2 pv3">
                                      <span className="f6 fw5  truncate w-75">
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.playerName
                                        }
                                      </span>
                                      <span className="f6 fw6 ">
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.battingStatsList[0]
                                            .runs
                                        }
                                        {data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.battingStatsList[0]
                                          .isNotOut === true
                                          ? '*'
                                          : ''}
                                      </span>
                                    </div>
                                  )}

                                <div className="br b--moon-gray mb2" />

                                {data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .runnerBowler &&
                                  data.matchSummary[
                                    inning === 'homeTeamData'
                                      ? 'awayTeamData'
                                      : 'homeTeamData'
                                  ][
                                    i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                  ].runnerBowler.bowlingStatsList && (
                                    <div className="flex w-100 justify-between pa2  pv3">
                                      <span className="f6 fw5  truncate w-75">
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.playerName
                                        }
                                      </span>
                                      <span className="f6 fw6 ">
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.bowlingStatsList[0]
                                            .wickets
                                        }
                                        /
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.bowlingStatsList[0]
                                            .runsConceded
                                        }
                                      </span>
                                    </div>
                                  )}
                              </div>
                            )}
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        ) : (
          <div>
            <DataNotFound />
          </div>
        )}
        <MatchInfoTab
          data={matchInfo}
          matchData={matchData}
          newData={newData}
        />
        {/* <div className='bg-red'>ssss</div> */}
      </div>
    )
}
