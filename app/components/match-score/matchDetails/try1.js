import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import * as d3 from "d3";
import PieClass from "./try";

// ./svgs/cricketGroundNew.pn
// import "./styles.css";

export default function MAIn(props) {

const ground = '/svgs/cricketGroundNew.svg';
// const ground = 


  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20,
    }));

  const [data, setData] = useState(generateData(0));
  const changeData = () => {
    setData(generateData());
  };

  useEffect(
    () => {
      setData(generateData());
    },
    [!data]
  );
  const getZadCo = (ball) => {
 
    if (props.battingStyle && props.battingStyle === "LHB"){
     return 0 - (180 - parseInt(ball.zadval.split(',')[1]));
    }
    return 0 - ball.zadval.split(',')[1];
  }

  var isSafari =window && window['safari']?true:false

  // Chrome 1 - 71
  var isChrome = window &&  window.chrome ?true:false

  return (
    <div className="flex items-center justify-center">
     {props.smallWheel?  <div className={` absolute w77 h77  z-2   flex items-center justify-center   bw-01 ${isChrome?'nt1':''}`}>
              <img className='w77 h77 absolute' src={ground} alt='' />
              <div className="flex h03 z-2 ba b--white w03 items-center justify-center absolute bg-red  bb-white  br-100"
                   
                   style={{
                    bottom: '58%',
                    left: '48%',
                    right: '0%',
                    
               
                    transform: `rotate(0deg)`,
                    transformOrigin: '0 0'
                  }}></div>
              <div
                className='w77 h77 absolute 
                   overflow-hidden br-100 ba wheel bw-01'>
            {props.zad.map((item)=>{return( <div
                      className={`bb  absolute ${
                        item.runs === '0' ? 'dn' : (item.runs === '6' && 'b--red w5' ) ||(item.runs === '1' && 'b--white w2') ||(item.runs === '2' && 'b--blue w2-3')||(item.runs === '3' && 'b--purple w2-7')||(item.runs === '5' && 'b--green ')||(item.runs === '4' && 'b--yellow w4')
                      } `}
                      style={{
                        bottom: '58%',
                    left: '51%',
                    right: '0%',
                        transform: `rotate(${getZadCo(item)}deg)`,
                        transformOrigin: '0 0'
                      }}></div>)}
            )
            } 
               
                 
                
              </div>
            </div>

:

            <div className={` absolute w99 h99  z-2   flex items-center justify-center   bw-01 ${isChrome?'nt1':''}`}>
              <img className='w99 h99 absolute' src={ground} alt='' />
              <div
                className='w99 h99 absolute 
                   overflow-hidden br-100 ba wheel bw-01'>

                   <div className="flex h03 z-2 ba b--white w03 items-center justify-center absolute bg-red  bb-white  br-100"
                   
                   style={{
                    bottom: '58%',
                    left: '48%',
                    right: '0%',
                    
               
                    transform: `rotate(0deg)`,
                    transformOrigin: '0 0'
                  }}></div>
            {props.zad.map((item)=>{return( <div
                      className={`bb  absolute ${
                        item.runs === '0' ? 'dn' : (item.runs === '6' && 'b--red w5' ) ||(item.runs === '1' && 'b--white w2') ||(item.runs === '2' && 'b--blue w2-6')||(item.runs === '3' && 'b--purple w2-7')||(item.runs === '5' && 'b--green ')||(item.runs === '4' && 'b--yellow w4')
                      } `}
                      style={{
                        bottom: '58%',
                        left: '49%',
                        right: '0%',
                   
                        transform: `rotate(${getZadCo(item)}deg)`,
                        transformOrigin: '0 0'
                      }}></div>)}
            )
            } 
               
                 
                
              </div>
            </div>
     }
      <div className="relative">
      
        <PieClass
          data={data}
          width={props.smallWheel?150:180}
          battingStyle={props.battingStyle}
          zadData={props.zad}
          height={props.smallWheel?150: 180}
          innerRadius={props.smallWheel?56:70}
          outerRadius={props.smallWheel?75:90}
        />
      </div>
     
    </div>
  );
}




