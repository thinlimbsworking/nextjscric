import React, { useEffect, useState } from 'react'
import SwiperModule from './test'
import { useRouter } from 'next/navigation'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING } from '../api/queries'
import Heading from './commom/heading'
import DataNotFound from './commom/datanotfound'
import Loading from '../../components/loading'
export default function Matchups(props) {
  const [toggle, setToggle] = useState(0)
  const [tabIndex, setTabIndex] = useState(0)
  const [bowler, setBowler] = useState(0)
  //
  const teamFallback = '/svgs/images/flag_empty.png'
  const router = useRouter()
  const { loading, error, data } = useQuery(
    HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING,
    {
      variables: { matchID: props.matchID },
    },
  )
  const capsuleCss = {
    Strong: 'text-green text-xs font-bold text-center',
    Average: 'text-yellow text-xs font-bold text-center',
    Weak: 'text-red text-xs font-bold text-center',
  }
  // alert(888)

  if (loading) {
    return <Loading />
  }
  return data &&
    data.HomeScreenPlayerMatchupsUpcomming &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 ? (
    <>
      <div className="w-full ml-2 dark:mt-3">
        {/* <div className='text-md  text-white tracking-wide font-bold '> Player MatchUp</div>
        <div className='text-xs  text-white tracking-wide font-bold '>  Select a batter and see how they perform against opposing bowlers</div>
       
      <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        <Heading
          heading={'Player MatchUp'}
          subHeading={
            'Select a batter and see how they perform against opposing bowlers'
          }
        />
        <div className="mt-2 dark:mt-4 md:text-gray-9 text-white text-xs">
          *Data from last 5 Year
        </div>
      </div>
      <div className="rounded mx-2 text-white lg:hidden md:hidden">
        <div className="flex w-full justify-between items-center mt-5 ">
          <div className="md:w-3/12 w-2/12 text-gray-2 dark:text-xs flex justify-between items-center  ">
            {' '}
            <span className="h-1 w-1 lg:h-5 lg:w-5 bg-green rounded-full lg:text-2xl"></span>{' '}
            BATTING
          </div>

          <div className="bg-gray-4 rounded-3xl flex items-center justify-between text-sm w-9/12 p-1">
            <div
              className={`w-1/2 text-center rounded-3xl px-2 py-0.5  ${
                toggle === 0 ? `border-2  border-green bg-gray-8` : ''
              }`}
              onClick={() => (setTabIndex(0), setToggle(0))}
            >
              {data &&
                data.HomeScreenPlayerMatchupsUpcomming &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
                  .homeTeamShortName}
            </div>
            <div
              className={`w-1/2 text-center rounded-3xl px-2 py-0.5 ${
                toggle === 1 ? `border-2  border-green bg-gray-8` : ''
              }`}
              onClick={() => (setTabIndex(0), setToggle(1))}
            >
              {data &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch[1]
                  .awayTeamShortName}
            </div>
          </div>
        </div>

        <SwiperModule
          toggle={toggle}
          bowler={bowler}
          setBowler={setBowler}
          homeTeamID={
            data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamID
          }
          awayTeamID={
            data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamID
          }
          tabIndex={tabIndex}
          setTabIndex={setTabIndex}
          data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
        />

        {false && (
          <div className="bg-gray h-52 flex flex-col items-center justify-center rounded-lg ">
            <div className="flex relative items-center justify-center ">
              <img
                className="h-36 w-36 bg-gray-4 object-top object-cover rounded-full"
                src={`https://images.cricket.com/players/${item.batsmanId}_headshot_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
              />

              <img
                className=" bg-red absolute h-6 w-6 left-0 bottom-0 object-fill"
                src={item.role == 'Bowler' ? ball : bat}
              />
              <img
                className=" absolute h-5 w-5 border   right-0 bottom-0 object-fill rounded-full"
                src={`https://images.cricket.com/teams/${props.data[0].homeTeamID}_flag_safari.png`}
              />

              <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center">
                <img
                  className="h-5 w-5 rounded-full"
                  src={`https://images.cricket.com/teams/${
                    props.toggle === 0 ? props.homeTeamID : props.awayTeamID
                  }_flag_safari.png`}
                  onError={(evt) => (evt.target.src = teamFallback)}
                />
              </div>
            </div>
            <div className=" text-center text-xs font-semibold mt-2 ">
              {item.name}
            </div>
          </div>
        )}
        {true && (
          <div className="flex bg-gray items-center justify-between rounded py-3">
            <div className="flex w-1/3 flex-col  items-center justify-center  ">
              <div className="text-xs font-medium uppercase text-gray-2">
                Balls{' '}
              </div>
              <div className="uppercase text-xl font-semibold  ">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex]?.bowlers[bowler]?.ballsFaced}
              </div>
            </div>
            <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
              <div className="uppercase text-xs font-medium text-gray-2">
                RUNS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex]?.bowlers[bowler]?.runsScored}
              </div>
            </div>
            <div className="flex flex-col  w-1/3  items-center justify-center">
              <div className="uppercase text-xs font-medium text-gray-2">
                WICKETS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex]?.bowlers[bowler]?.wicket}{' '}
              </div>
            </div>
          </div>
        )}

        {true && (
          <div className="  flex flex-wrap justify-center  mt-4 items-center">
            {data &&
              data.HomeScreenPlayerMatchupsUpcomming &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]?.mappings[
                tabIndex
              ]?.bowlers?.map((item, i) => {
                return (
                  <div
                    key={i}
                    className={`flex w-5/12 my-2 mx-3 justify-center bg-gray h-40 relative rounded-xl ${
                      i === bowler ? 'border-green border' : ''
                    }`}
                    onClick={() => setBowler(i)}
                  >
                    <div className="flex flex-col relative items-center justify-center ">
                      <div className="flex relative items-center justify-center ">
                        <img
                          className="h-20 w-20 lg:bg-gray-2 bg-gray-4 object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${item.bowlerId}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />

                        {/* <img
                          className="absolute h-6 w-6 left-0 bottom-0 object-fill"
                          src={'/pngsV2/bowlericon.png'}
                        /> */}

                        {item?.role === 'Batsman' ? (
                          <img
                            src="/svgs/whitebat.svg"
                            height="10px"
                            width="16px"
                            className="absolute h-5 w-5  left-0 bottom-0 bg-basebg border rounded object-fill"
                          />
                        ) : item?.role === 'Bowler' ? (
                          <img
                            src="/svgs/bowling.svg"
                            height="10px"
                            width="13px"
                            className="absolute h-5 w-5 left-0 bottom-0 bg-basebg border rounded object-fill"
                          />
                        ) : item?.role === 'Keeper' ? (
                          <img
                            src="/svgs/wicketKeeper.svg"
                            height="10px"
                            width="16px"
                            className="left-0 bottom-0 absolute h-5 w-5 bg-basebg md:h-5 object-fill border rounded md:border"
                          />
                        ) : (
                          <img
                            src="/svgs/allRounderWhite.svg"
                            height="10px"
                            width="16px"
                            className="h-5 w-5 left-0 bottom-0 absolute bg-basebg md:h-5 object-fill border rounded md:border"
                          />
                        )}

                        <div className="absolute w-5 h-5 bg-white right-0 bottom-0 rounded-full flex justify-center items-center ">
                          <img
                            className="h-5 w-5 rounded-full"
                            src={`https://images.cricket.com/teams/${
                              toggle === 0
                                ? data?.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[1].awayTeamID
                                : data?.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[0].homeTeamID
                            }_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/svgs/images/flag_empty.svg')
                            }
                          />
                        </div>
                      </div>

                      <div className="text-center text-xs font-semibold mt-2">
                        {item.name}
                      </div>
                      <div className={`mt-2 ${capsuleCss[item.label]}`}>
                        {item.label}
                      </div>
                    </div>
                  </div>
                )
              })}
          </div>
        )}
      </div>

      {/* desktop */}
      <div className="rounded ml-2 text-white hidden lg:block md:block">
        {router.asPath === '/' && (
          <div className="flex w-full justify-between items-center mt-5 my-3">
            <div className=" w-6/12 text-gray-2 dark:text-xs flex items-center">
              <span className="mx-2 h-2 w-2 lg:h-5 lg:w-5 bg-green rounded-full lg:text-2xl"></span>{' '}
              BATTING
            </div>

            <div className="bg-gray-4 rounded-3xl flex items-center justify-between text-sm w-4/12 ">
              <div
                className={`w-1/2 text-center rounded-3xl py-1 ${
                  toggle === 0 ? `border-2 border-green bg-gray-8` : ''
                }`}
                onClick={() => (setTabIndex(0), setToggle(0))}
              >
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
                    ?.homeTeamShortName}
              </div>
              <div
                className={`w-1/2 text-center rounded-3xl py-1 ${
                  toggle === 1 ? `border-2 border-green bg-gray-8` : ''
                }`}
                onClick={() => (setTabIndex(0), setToggle(1))}
              >
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming?.preMatch[1]
                    ?.awayTeamShortName}
              </div>
            </div>
          </div>
        )}

        <div className="flex items-center justify-center">
          <div className="dark:bg-gray-4 pt-1 pb-3 rounded-3xl flex items-center justify-between text-sm w-4/12 ">
            <div
              className={`w-1/2 text-center md:cursor-pointer rounded-2xl mx-2 py-1 ${
                toggle === 0
                  ? `dark:border-2 dark:border-green lg:bg-basered md:bg-basered xl:bg-basered lg:border-none md:border-none xl:border-none dark:bg-gray-8`
                  : 'lg:text-black lg:bg-[#EEEEEE] lg:font-semibold'
              }`}
              onClick={() => (setTabIndex(0), setToggle(0))}
            >
              {data &&
                data.HomeScreenPlayerMatchupsUpcomming &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
                data.HomeScreenPlayerMatchupsUpcomming?.preMatch[0]
                  ?.homeTeamShortName}
            </div>
            <div
              className={`w-1/2 text-center md:cursor-pointer rounded-2xl mx-2 py-1 ${
                toggle === 1
                  ? `dark:border-2 dark:border-green lg:bg-basered md:bg-basered xl:bg-basered lg:border-none md:border-none xl:border-none dark:bg-gray-8`
                  : 'lg:text-black lg:bg-[#EEEEEE] lg:font-semibold'
              }`}
              onClick={() => (setTabIndex(0), setToggle(1))}
            >
              {data &&
                data.HomeScreenPlayerMatchupsUpcomming?.preMatch[1]
                  ?.awayTeamShortName}
            </div>
          </div>
        </div>
        <div className="md:bg-white md:rounded-md md:shadow md:p-4">
          {router.asPath !== '/' && (
            <div className="flex w-full justify-start items-center mt-5 my-3">
              <div className=" w-6/12 text-gray-2 dark:text-2xs flex text-sm items-center  ">
                <span className="mx-2 h-2 w-2 lg:h-3 md:h-3 xl:h-3 lg:w-3 md:w-3 xl:w-3 bg-green rounded-full lg:text-2xl"></span>{' '}
                BATTING
              </div>
            </div>
          )}

          <div className="flex items-center justify-center">
            <div className="w-6/12">
              <SwiperModule
                bowler={bowler}
                setBowler={setBowler}
                toggle={toggle}
                homeTeamID={
                  data?.HomeScreenPlayerMatchupsUpcomming?.preMatch[0]
                    ?.homeTeamID
                }
                awayTeamID={
                  data?.HomeScreenPlayerMatchupsUpcomming?.preMatch[1]
                    ?.awayTeamID
                }
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                data={data && data?.HomeScreenPlayerMatchupsUpcomming?.preMatch}
              />
            </div>

            <div className=" w-4/12 dark:bg-gray lg:border-2 lg:border-solid lg:border-[#E2E2E2] bg-white flex items-center justify-center rounded h-28">
              <div className="flex w-1/3 flex-col  items-center justify-center  ">
                <div className="text-xs md:text-xs font-medium uppercase text-gray-2">
                  Balls{' '}
                </div>
                <div className="uppercase md:text-black md:text-lg text-xl font-semibold">
                  {data &&
                    data.HomeScreenPlayerMatchupsUpcomming &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                      .mappings[tabIndex]?.bowlers[bowler]?.ballsFaced}
                </div>
              </div>
              <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
                <div className="uppercase md:text-xs text-xs font-medium text-gray-2">
                  RUNS{' '}
                </div>
                <div className="uppercase md:text-black md:text-lg text-xl font-semibold">
                  {data &&
                    data.HomeScreenPlayerMatchupsUpcomming &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                      .mappings[tabIndex]?.bowlers[bowler]?.runsScored}
                </div>
              </div>
              <div className="flex flex-col w-1/3 items-center justify-center">
                <div className="uppercase md:text-xs text-xs font-medium text-gray-2">
                  WICKETS{' '}
                </div>
                <div className="uppercase md:text-black md:text-lg text-xl font-semibold">
                  {data &&
                    data.HomeScreenPlayerMatchupsUpcomming &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                    data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                      .mappings[tabIndex]?.bowlers[bowler]?.wicket}{' '}
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-wrap justify-center mt-4 items-stretch">
            {data &&
              data.HomeScreenPlayerMatchupsUpcomming &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 &&
              data.HomeScreenPlayerMatchupsUpcomming?.preMatch[
                toggle
              ]?.mappings[tabIndex]?.bowlers?.map((item, i) => {
                return (
                  <div
                    key={i}
                    className={`flex w-2/12 my-2 mx-3 py-2 justify-center bg-gray lg:bg-white relative border rounded-xl ${
                      i === bowler
                        ? 'border-green border'
                        : 'md:border-[#E2E2E2]'
                    }`}
                    onClick={() => setBowler(i)}
                  >
                    <div className="flex flex-col relative items-center justify-center ">
                      <div className="flex relative items-center justify-center ">
                        <img
                          className="h-20 w-20 bg-gray-4 lg:bg-gray-2 object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${item.bowlerId}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />

                        {/* <img
                          className=" absolute h-6 w-6 left-0 bottom-0 object-fill"
                          src={'/pngsV2/bowlericon.png'}
                        /> */}
                        {item?.role === 'Batsman' ? (
                          <img
                            src="/svgs/whitebat.svg"
                            height="10px"
                            width="16px"
                            className="absolute h-5 w-5  left-0 bottom-0 bg-basebg border rounded object-fill"
                          />
                        ) : item?.role === 'Bowler' ? (
                          <img
                            src="/svgs/bowling.svg"
                            height="10px"
                            width="13px"
                            className="absolute h-5 w-5 left-0 bottom-0 bg-basebg border rounded object-fill"
                          />
                        ) : item?.role === 'Keeper' ? (
                          <img
                            src="/svgs/wicketKeeper.svg"
                            height="10px"
                            width="16px"
                            className="left-0 bottom-0 absolute h-5 w-5 bg-basebg md:h-5 object-fill border rounded md:border"
                          />
                        ) : (
                          <img
                            src="/svgs/allRounderWhite.svg"
                            height="10px"
                            width="16px"
                            className="h-5 w-5 left-0 bottom-0 absolute bg-basebg md:h-5 object-fill border rounded md:border"
                          />
                        )}
                        <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                          <img
                            className="h-5 w-5 rounded-full"
                            src={`https://images.cricket.com/teams/${
                              toggle === 0
                                ? data?.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[1]?.awayTeamID
                                : data?.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[0]?.homeTeamID
                            }_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/svgs/images/flag_empty.svg')
                            }
                          />
                        </div>
                      </div>

                      <div className="text-center px-2 leading-5 md:text-md md:text-black dark:text-xs font-semibold mt-2">
                        {item.name}
                      </div>
                      <div className={`mt-2 ${capsuleCss[item?.label]}`}>
                        {item.label.toUpperCase()}
                      </div>
                    </div>
                  </div>
                )
              })}
          </div>
        </div>
      </div>
    </>
  ) : (
    <DataNotFound />
  )
}
