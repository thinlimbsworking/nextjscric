'use client'
import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import ArticleCard from '../articlecard'
import axios from 'axios'
import { useQuery } from '@apollo/react-hooks'
import { AUTHOR_DATA, AUTHOR_DATA2 } from '../../api/queries'
import { NEWS } from '../../constant/MetaDescriptions'
import MetaDescriptor from '../MetaDescriptor'
import { FilterIcon, CalenderIcon } from './authorBlogIcons'
import {
  getNewsUrl,
  getArticleTabUrl,
  getLiveArticleUrl,
} from '../../api/services'
import { useRouter } from 'next/navigation'
import DataNotFound from '../commom/datanotfound'
import ImageWithFallback from '../commom/Image'

const NewsDetailsLayout = ({ params, ...props }) => {
  let router = useRouter()

  let authorName = params?.slugs?.[1]

  // 2021-07-01
  const monthsArr = [
    { month: 'JAN', date: '-01-31', key: 0 },
    { month: 'FEB', date: '-02-28', key: 1 },
    { month: 'MAR', date: '-03-31', key: 2 },
    { month: 'APR', date: '-04-30', key: 3 },
    { month: 'MAY', date: '-05-31', key: 4 },
    { month: 'JUN', date: '-06-30', key: 5 },
    { month: 'JUL', date: '-07-31', key: 6 },
    { month: 'AUG', date: '-08-31', key: 7 },
    { month: 'SEP', date: '-09-30', key: 8 },
    { month: 'OCT', date: '-10-31', key: 9 },
    { month: 'NOV', date: '-11-30', key: 10 },
    { month: 'DEC', date: '-12-31', key: 11 },
  ]
  const [showYears, setShowYears] = useState(false)
  const [dateChange, setDateChange] = useState(false)
  const [selectYear, setSelectYear] = useState(new Date().getFullYear())
  const [selectMonth, setSelectMonth] = useState(
    monthsArr[parseInt(new Date().getMonth())],
  )
  const [apiMonth, setApiMonth] = useState('')
  const [apiYear, setApiYear] = useState('')

  const [typeName, setTypeName] = useState({ title: 'Type', value: '' })
  const [showTypes, setShowTypes] = useState(false)
  let tab = 'new'

  const [pagination, setPagination] = useState(1)
  const [windows, updateWindows] = useState()
  useEffect(() => {
    if (global.window) updateWindows(global.window)
    return () => {
      document.body.style.overflow = 'unset'
    }
  }, [global.window])

  let { loading, error, data, fetchMore } = useQuery(
    AUTHOR_DATA,
    {
      variables: {
        authorID: authorName,
        page: 0,
        type: typeName.value,
        fromDate: dateChange ? `${apiYear}${apiMonth.date}` : '',
      },

      fetchPolicy: 'no-cache',
    },
    // return() document.body.style.overflow = 'unset';
  )
  // const  aaa=async()=>{
  //   alert(6)
  // let ata2  =await  axios.post(process.env.API, {
  //   query: AUTHOR_DATA2,
  //   variables: { authorID: authorName, page: 0 ,type:typeName.value,fromDate:dateChange? `${apiYear}${apiMonth.date}`:''},
  // })

  //
  // }
  if (loading) {
    return <div></div>
  }

  const handleScroll = (evt) => {
    if (
      evt.target.scrollTop + evt.target.clientHeight >=
      0.9 * evt.target.scrollHeight
    ) {
      setPagination((prevPage) => prevPage + 1)

      handlePagination(fetchMore)
    }
  }
  const handlePagination = (fetchMore) => {
    fetchMore({
      variables: { authorID: authorName, page: pagination },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult
        }
        let resultedArticle = Object.assign({}, previousResult, {
          getAuthorsArticles: [
            ...previousResult.getAuthorsArticles,
            ...fetchMoreResult.getAuthorsArticles,
          ],
        })
        return { ...resultedArticle }
      },
    })
  }

  const typeArr = [
    { title: 'News', value: 'news' },
    { title: 'Match Related', value: 'matchrelated' },
    { title: 'Features', value: 'features' },
    { title: 'ipl', value: 'ipl' },
  ]
  if (data) {
    const stopBackGroundScroll = () => {
      document.body.style.overflow = 'hidden'
    }
    const onBackGroundScroll = () => {
      document.body.style.overflow = 'unset'
    }

    var authorData =
      data && data.getAuthorsArticles && data.getAuthorsArticles.author
    return (
      <>
        <MetaDescriptor section={NEWS[tab]} />

        <div className="">
          {/* <div className="bg-navy z-1 px-2 h-2  lg:hidden md:hidden flex items-center">
            onClick={() =>router.push('/fantasy-research-center/[...slugs]', `${matchID}/${matchName}`)}
            <div className="w-5" onClick={() => window.history.back()}>
              <img
                className="w-1 lg:hidden"
                alt=""
                src="/svgs/backIconWhite.svg"
              />
            </div>
            <div className="white text-sm fw7 pl3 ">Authors</div>
          </div> */}
          <div className="bg-gray-8 flex items-center p-2 rounded-md lg:hidden md:hidden">
            <div
              onClick={() => window.history.back()}
              className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
            >
              <svg width="30" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#ffff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <span className="text-base text-white font-bold lg:hidden">
              Authors
            </span>
          </div>

          {authorData ? (
            <>
              <div className="w-11/12 md:w-full flex p-4 md:p-3 md:bg-white md:rounded-md md:shadow md:my-3">
                <div className="w-4/12 flex items justify-center p-2">
                  <ImageWithFallback
                    height={18}
                    width={18}
                    className="h-24 w-24 object-cover"
                    src={
                      (authorData && authorData.pic) ||
                      'https://devcdc.com/svgs/Man-of-the-match.svg'
                    }
                    alt=""
                  />
                  <div className="border-r-solid ml-2 h-24 border-r-2"></div>
                </div>

                <div className="flex-col w-8/12 items-start justify-start p-2">
                  <div className="text-base dark:text-white text-black font-medium w-full flex">
                    {authorData && authorData.userName}{' '}
                  </div>
                  <div className="text-sm font-medium w-full flex mt-1">
                    {' '}
                    {authorData && authorData.twtID && (
                      <ImageWithFallback
                        height={18}
                        width={18}
                        src="/pngs/tweet.png"
                        alt="tweet"
                      />
                    )}{' '}
                    <span className="ml-1 gray">
                      {authorData && authorData.twtID}
                    </span>
                  </div>
                  <div className="text-xs dark:text-white text-black">
                    {authorData && authorData.bio}
                  </div>

                  <div className="flex w-full mt-2 items-center lg:w-6/12 gap-2 justify-evenly">
                    <div className="w-6/12 md:w-full">
                      <div
                        className={`border-solid border-2 p-0.5 flex items-center justify-center rounded-md w-full
                         ${
                           apiMonth == '' || apiYear == ''
                             ? ''
                             : 'dark:border-green bg-basered dark:bg-basebg text-white'
                         } `}
                      >
                        <span
                          className={`text-xs dark:text-white gap-2 font-medium ml-1 flex items-center justify-center cursor-pointer `}
                          onClick={() => (
                            setShowTypes(false),
                            setShowYears((prev) => !prev),
                            stopBackGroundScroll()
                          )}
                        >
                          {apiMonth == '' || apiYear == ''
                            ? 'MM/YY'
                            : `${apiMonth.month}/${apiYear}`}

                          {/* <img
                            className="mx-1 "
                            height="18px"
                            width="18px"
                            src={`${
                              apiMonth == '' || apiYear == ''
                                ? '/svgs/calander.svg'
                                : '/svgs/redcalander.svg'
                            }`}
                            alt=""
                          /> */}
                          <div className="lg:hidden">
                            <CalenderIcon
                              clr={`${
                                apiMonth == '' || apiYear == ''
                                  ? '#ffff'
                                  : '#38D926'
                              }`}
                            />
                          </div>
                          <div className="hidden lg:block md:block">
                            <CalenderIcon
                              clr={`${
                                apiMonth == '' || apiYear == ''
                                  ? '#182132'
                                  : '#ffff'
                              }`}
                            />
                          </div>
                        </span>
                      </div>

                      {showYears && (
                        <div
                          className="w-5/12
                         md:w-44 rounded-md flex items-start justify-start flex-col p-1 fixed dark:bg-gray-4 bg-white z-50 dark:border-basebg border-[#E2E2E2] border-2 borddr-solid shadow-5"
                        >
                          <div className="flex text-xs dark:text-white text-black py-2 lg:pl-1">
                            {' '}
                            Years
                          </div>
                          <div className="flex w-full items-center justify-center">
                            {[
                              new Date().getFullYear(),
                              new Date().getFullYear() - 1,
                              new Date().getFullYear() - 2,
                            ].map((item) => {
                              return (
                                <div
                                  onClick={() =>
                                    item == new Date().getFullYear() &&
                                    selectMonth.key > new Date().getMonth()
                                      ? (setSelectMonth(
                                          monthsArr[new Date().getMonth()],
                                        ),
                                        setSelectYear(item))
                                      : setSelectYear(item)
                                  }
                                  // item==new Date().getFullYear()&&selectMonth.month==  )?(     setSelectYear(item)):null)}
                                  className={`cursor-pointer text-xs border-solid border-2 mx-1 px-2 rounded-full flex-row dark:bg-basebg flex items-center justify-center 

                                  
                                  ${
                                    selectYear == item
                                      ? 'dark:border-green border-basered bg-basered text-white '
                                      : 'border-[#E2E2E2] dark:border-white dark:text-white text-black'
                                  }
`}
                                >
                                  {item}
                                </div>
                              )
                            })}
                          </div>

                          <div className="flex text-xs dark:text-white text-black py-2 lg:pl-1">
                            {' '}
                            Month
                          </div>
                          <div className="flex flex-wrap py-2 items-center justify-center">
                            {monthsArr.map((item, index) => {
                              return (
                                <div
                                  onClick={() =>
                                    selectYear == new Date().getFullYear() &&
                                    index > new Date().getMonth()
                                      ? null
                                      : setSelectMonth(item)
                                  }
                                  // onClick={()=>

                                  //   setSelectMonth(item)}
                                  className={`cursor-pointer text-xs px-5 py-1 w-3/12 my-2 lg:m-1 dark:bg-basebg border-solid border-2 rounded-full flex items-center justify-center
                          ${
                            selectYear == new Date().getFullYear() &&
                            index > new Date().getMonth()
                              ? 'opacity-30'
                              : ''
                          }
                          
                          
                          ${
                            selectMonth.month == item.month
                              ? 'dark:border-green text-white bg-basered'
                              : 'dark:border-gray-4 border-[#E2E2E2] dark:text-white text-black'
                          } `}
                                >
                                  {item.month}
                                </div>
                              )
                            })}
                          </div>

                          <div
                            className="flex w-full items-center justify-center py-2 cursor-pointer"
                            onClick={() => (
                              onBackGroundScroll(),
                              setDateChange(true),
                              setApiMonth(selectMonth),
                              setApiYear(selectYear),
                              setShowYears(false)
                            )}
                          >
                            <div className="w-4/12 flex items-center justify-center dark:bg-green dark:text-black text-white bg-basered text-xs p-1 rounded-full">
                              Done
                            </div>
                          </div>
                        </div>
                      )}
                    </div>

                    <div className="w-6/12 md:w-full relative">
                      <div
                        onClick={() => (
                          setShowTypes((prev) => !prev), setShowYears(false)
                        )}
                        className={`${
                          typeName.title != 'Type'
                            ? 'border-2 border-solid dark:border-green border-basered rounded-full text-white dark:bg-basebg bg-basered w-full md:w-full'
                            : 'border-solid border-2 lg:w-full rounded-full'
                        } truncate cursor-pointer flex items-center py-0.5 justify-around rounded-md border-2 border-solid`}
                      >
                        {/* rounded-t bg-gray-4 truncate lg:py-6 lg:px-4
                        lg:text-ellipsis max-w-[13ch] text-white text-xs
                        hover:bg-gray-400 py-1 px-2 block whitespace-no-wrap
                        capitalize */}
                        <span
                          className={`text-xs line-clamp-2 font-semibold flex items-center justify-center`}
                        >
                          {typeName.title}{' '}
                        </span>
                        <ImageWithFallback
                          height={18}
                          width={18}
                          className={`mx-1 w-4 ${
                            showTypes ? '-rotate-360' : 'rotate-180'
                          }`}
                          src="/svgs/up-arrow.png"
                          alt="upArrow"
                        />
                      </div>

                      {showTypes && (
                        <div className="dark:border-basebg border-[#E2E2E2] border-2 rounded-tr-md rounded-tl-md md:w-full w-3/12 z-50 flex flex-col absolute dark:bg-gray-4 bg-white shadow-5">
                          {/* <div
                            className={`w-full  text-xs flex items-center cursor-pointer  justify-center  `}
                            onClick={() => (
                              setTypeName({ title: 'Type', value: '' }),
                              setShowTypes(false)
                            )}
                          >
                            {' '}
                            Type{' '}
                            <img
                              className="mx-1 h-6 "
                              src="/svgs/downArrow.svg"
                              alt=""
                            />
                          </div> */}

                          {typeArr.map((item) => {
                            return (
                              <div
                                className={`w-full flex-col text-xs text-center border-b-2 dark:border-b-gray border-b-[#E2E2E2] border-b-solid
                   p-1 border-silver black flex dark:text-white text-black items-center justify-center`}
                                onClick={() => (
                                  setTypeName(item), setShowTypes(false)
                                )}
                              >
                                <div className="pb-1">
                                  {' '}
                                  {item.title.toUpperCase()}
                                </div>

                                {item.title !== 'IPL' ? (
                                  <div className="h-2 w-full divider"></div>
                                ) : (
                                  ''
                                )}
                              </div>
                            )
                          })}
                        </div>
                      )}
                    </div>
                    <div className="">
                      {(apiMonth !== '' ||
                        apiYear !== '' ||
                        typeName.title !== 'Type') && (
                        <div className="mx-1">
                          <div
                            className="border-solid cursor-pointer w-full md:pr-1 dark:px-3 flex items-center justify-center rounded-full ml-4 lg:ml-0 border-green h-12"
                            onClick={() => (
                              setDateChange(false),
                              setApiMonth(''),
                              setApiYear(''),
                              setTypeName({ title: 'Type', value: '' })
                            )}
                          >
                            <span className="text-xs border-2 dark:text-green px-2 dark:border-green dark:bg-transparent bg-basered text-white rounded-full font-medium ml-1 lg:w-24 flex items-center justify-center">
                              <div className="lg:hidden md:hidden">
                                <FilterIcon />
                              </div>
                              <div className="lg:block md:block hidden">
                                <FilterIcon clr={'#FFFF'} />
                              </div>
                              <div className="lg:hidden md:hidden">
                                <span
                                  className={`ml-1 text-sm font-semibold flex items-center justify-center ${
                                    apiMonth != '' || apiYear != ''
                                      ? 'border-green border-solid '
                                      : ''
                                  }`}
                                >
                                  Clear
                                </span>
                              </div>
                              <div className="hidden md:block lg:block">
                                <span
                                  className={`ml-1 font-semibold py-0.5 flex items-center justify-center ${
                                    apiMonth != '' || apiYear != ''
                                      ? ' border-solid text-white'
                                      : ''
                                  }`}
                                >
                                  Clear
                                </span>
                              </div>
                            </span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="w-full">
                {error ? (
                  <div className="w-full h-screen font-extralight text-xs gray flex flex-col justify-center items-center">
                    <span className="dark:text-white text-black">
                      Something isn't right!
                    </span>
                    <span className="dark:text-white text-black">
                      Please refresh and try again...
                    </span>
                  </div>
                ) : (
                  <div
                    className={`w-full flex flex-wrap items-center justify-center  dark:min-h-fit dark:max-h-full dark:hidescroll dark:overflow-y-scroll ${
                      showYears || showTypes ? 'opacity-20' : ''
                    }`}
                    onScroll={(evt) => handleScroll(evt)}
                  >
                    {data &&
                    data.getAuthorsArticles &&
                    data.getAuthorsArticles.articles.length > 0 ? (
                      data.getAuthorsArticles.articles.map((article, key) => (
                        <>
                          {article.type != 'live-blog' &&
                          article.type != 'live-blogs' ? (
                            <Link
                              key={key}
                              {...getNewsUrl(article, tab)}
                              passHref
                              className="w-full w-full-l cursor-pointer"
                            >
                              <ArticleCard
                                isFeatured={false}
                                article={article}
                                authorPic={authorData && authorData.pic}
                              />
                              <div className="divider" />
                            </Link>
                          ) : (
                            <Link
                              key={key}
                              {...props.News_Live_Tab_URL(article, tab)}
                              passHref
                              className="w-full lg:w-full cursor-pointer"
                              onClick={() => {
                                props.handleCleverTab(article)
                              }}
                            >
                              <ArticleCard
                                // isFeatured={
                                //   window
                                //     ? windows.localStorage.Platform === 'Web'
                                //       ? key === 0 || key === 1
                                //       : key === 0
                                //     : ''
                                // }
                                article={article}
                                isBanner
                              />

                              {/* <div className='divider' /> */}
                            </Link>
                          )}

                          {/* {article.type != 'live-blog' && article.type != 'live-blogs' ? (
                    <Link key={key} {...getNewsUrl(article, tab)} passHref>
                      <a className='w-full w-full-l cursor-pointer' key={key}>
                        <ArticleCard isFeatured={false} article={article} />
                        <div className='divider' />
                      </a>
                    </Link>
                  ) : (
                    <Link key={key} {...props.News_Live_Tab_URL(article, tab)} passHref>
                      <a
                        className='w-full w-full-l cursor-pointer'
                        key={key}
                        onClick={() => {
                          props.handleCleverTab(article);
                        }}>
                        <ArticleCard
                          isFeatured={
                            windows
                              ? windows.localStorage.Platform === 'Web'
                                ? key === 0 || key === 1
                                : key === 0
                              : ''
                          }
                          article={article}
                        />
                        <div className='divider' />
                       
                      </a>
                    </Link>
                  )} */}
                        </>
                      ))
                    ) : (
                      <div className="pt-4 center">
                        {' '}
                        {/* {...props.News_Live_Tab_URL(article, tab)} */}
                        <div className="flex justify-center items-center">
                          <ImageWithFallback
                            height={18}
                            width={18}
                            className="w-44"
                            src="/svgs/Empty.svg"
                            alt="empty..."
                          />
                        </div>
                        <div className="dark:text-white text-black text-sm text-center mr-4">
                          No Articles found
                        </div>
                      </div>
                    )}
                  </div>
                )}
              </div>
            </>
          ) : (
            <div className="pt-4 center">
              {' '}
              {/* {...props.News_Live_Tab_URL(article, tab)} */}
              <div className="flex justify-center items-center">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  className="w-44"
                  src="/svgs/Empty.svg"
                  alt="empty"
                />
              </div>
              {/* <div className="gray text-sm text-center mr-4">No Data Found</div> */}
              <DataNotFound />
            </div>
          )}
        </div>
      </>
    )
  }
}

export default NewsDetailsLayout
