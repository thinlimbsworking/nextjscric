import React, { useEffect, useState } from 'react'
import parse from 'html-react-parser'
import {
  LIVE_BLOGGING_AXIOS,
  LIVE_BLOGGING,
  UPDATE_ARTICLE_LIKE,
  GET_ARTICLE_LIKES,
} from '../../api/queries'
import Router from 'next/navigation'
import axios from 'axios'
import { format } from 'date-fns'
const backIconWhite = '/svgs/backIconWhite.svg'
const arrowLeft = '/pngs/arrow-left.png'
import { MATCH_DETAIL_LAYOUT, LIVE_SCORE_ARTICLE } from '../../constant/Links'
import { useRouter } from 'next/navigation'
import { useQuery, useMutation } from '@apollo/react-hooks'
import CleverTap from 'clevertap-react'
import Head from 'next/head'
import { getAUthorUrl } from '../../api/services'

let scrollTop = null
export default function liveBlog({ params, ...props }) {
  const [fontSize, setFontSize] = useState('f6')
  const [temp, setTemp] = useState(10)
  const [article, setArticle] = useState(props?.data?.getLiveBlockComments)

  const [matchID, setmatchID] = useState(null)
  const [matchName, setMatchName] = useState(null)
  const [articleID, setArticleID] = useState(null)
  const [articleCount, setArticleCount] = useState('')
  const [flagCount, setFlagCount] = useState(true)
  const [apiCall, setApicall] = useState(true)
  const [path, setPath] = useState('')
  const [loadFirstTime, setloadFirstTime] = useState(false)
  const [flagCount2, setFlagCount2] = useState(false)
  const [FlagArray, setFlagArray] = useState([])
  const [rArticleEvent, setArticleEvent] = useState(true)
  const [scrolledValue, setScrolledValue] = useState(10)
  const [screenWidth, setscreenWidth] = useState()
  const [showHeader, setShowHeader] = useState(false)

  const router = useRouter()

  useEffect(() => {
    if (
      typeof window !== 'undefined' &&
      window &&
      window.screen &&
      window.screen.width
    ) {
      setscreenWidth(window.screen.width > 975 ? 976 : window.screen.width)
    }
  }, [])

  const fontHeading = {
    f55: '1.4rem',
    f5: '1.3rem',
    f6: '1.1rem',
  }

  const fontSubHeading = {
    f55: '1.3rem',
    f5: '1.2rem',
    f6: '1rem',
  }

  const [showFont, setShowFont] = useState(false)
  const goToAUthor = (item) => {
    //     let authorName = `${name && name.split(' ').join('-').toLowerCase()}`
    //     console.log("getAUthorUrl(authorName)", getAUthorUrl(authorName));
    //     router.push(getAUthorUrl(authorName).as)
    const name = item.name
    const id = item.id
    let authorName = `${name && name.split(' ').join('-').toLowerCase()}`
    let authorID = item.id
    router.push(getAUthorUrl(authorName, authorID).as)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  const handleScroll = (event) => {
    ;({ scrollTop } = event.target.scrollingElement)
    if (scrollTop > 208 && !showHeader) {
      setShowHeader(true)
    }
    if (scrollTop < 159) {
      setShowHeader(false)
    }
  }

  const scrollProgress = () => {
    const scrollPx = document.documentElement.scrollTop
    const winHeightPx =
      document.documentElement.scrollHeight -
      (document.documentElement.clientHeight + 50)
    const scrolled = `${(scrollPx / winHeightPx) * 100}`

    if (scrolled > 10) {
      setScrolledValue(scrolled)
    }
  }
  useEffect(() => {
    window.addEventListener('scroll', scrollProgress)
    return () => {
      window.removeEventListener('scroll', scrollProgress)
    }
  }, [])

  const { loading, error, data } = useQuery(GET_ARTICLE_LIKES, {
    variables: { articleID: params?.slugs?.[3] },

    onCompleted: (data) => {
      setArticleCount(data.getArticlesLikes.articlesCount)
    },
  })
  const { data: dataR, error: errorR, loading: landingR } = useQuery(
    LIVE_BLOGGING,
    {
      variables: { articleID: params?.slugs?.[3] },
      pollInterval: 60000,
      onCompleted: (dataR) => {
        setArticle(dataR.getLiveBlockComments)
      },
    },
  )

  useEffect(() => {
    document.documentElement.scrollHeight < 900
      ? setScrolledValue(100)
      : setScrolledValue(10)
    setTemp(document.documentElement.scrollHeight)
    setloadFirstTime(true)
  }, [])

  useEffect(() => {
    let route = params?.slugs

    setmatchID(route[1])
    setMatchName(route[2])
    setArticleID(route[3])

    // if (typeof window !== 'undefined') {
    //   window.addEventListener('scroll', handleScroll);
    // }

    // return () => {
    //   if (typeof window !== 'undefined') {
    //     window.removeEventListener('scroll', handleScroll);
    //   }
    // };
  }, [])

  useEffect(() => {
    setFlagArray(
      localStorage.getItem('FlagArray') == null
        ? []
        : JSON.parse(localStorage.getItem('FlagArray')),
    )
    //setPath(router.query.type);

    setFlagCount2(!flagCount2)
    localStorage.getItem('FlagArray') !== null
      ? JSON.parse(localStorage.getItem('FlagArray')).indexOf(
          params?.slugs?.[1],
        ) >= 0
        ? setFlagCount(false)
        : setFlagCount(true)
      : false
  }, [articleCount])

  useEffect(() => {
    setloadFirstTime(true)
  }, [])

  const likeHandle = () => {
    if (typeof window !== 'undefined') {
      setApicall(false)
      const flag = FlagArray.indexOf(params?.slugs?.[3]) >= 0 ? false : true

      UPDATE_ARTICLE_LIKE2({
        variables: {
          articleID: params?.slugs?.[3],
          like: loadFirstTime ? flag : flagCount,
        },
      })
      CleverTap?.event('Like', {
        Source: 'Article',
        ArticleHeading: article.title || 'TYPE',
        Platform: localStorage ? localStorage.Platform : '',
      })
    }
  }

  const handleShare = () => {
    if (window.navigator.share !== undefined) {
      window.navigator
        .share({
          title: article.title || 'TITLE',
          url: window.location.href,
        })
        .then((res) => {
          CleverTap.initialize('Share', {
            Source: 'Article',
            ArticleType: article.type || 'TYPE',
            Platform: localStorage ? localStorage.Platform : '',
          })
        })
    }
  }

  const debounce = (fn, delay) => {
    let timeoutID
    return function (...args) {
      if (timeoutID) {
        clearTimeout(timeoutID)
      }
      timeoutID = setTimeout(() => {
        fn(...args)
      }, delay)
    }
  }

  const [UPDATE_ARTICLE_LIKE2, response2] = useMutation(UPDATE_ARTICLE_LIKE, {
    onCompleted: (data) => {
      let arr = FlagArray
      const index = FlagArray.indexOf(params?.slugs?.[3])
      if (index > -1) {
        arr.splice(index, 1)
      } else {
        arr.push(params?.slugs?.[3])
      }
      setloadFirstTime(false)

      setFlagArray(arr)

      localStorage.setItem('FlagArray', JSON.stringify(FlagArray))
      setArticleCount(data.updateLikes)
      setFlagCount(!flagCount)

      setApicall(true)
    },
  })

  const handleScorecardNav = (matchStatus, matchID, currentTab, seriesName) => {
    return {
      as: eval(LIVE_SCORE_ARTICLE.as),
      href: eval(LIVE_SCORE_ARTICLE.as),
    }
  }

  const handleNavigation = (matchStatus, matchID, seriesName) => {
    if (matchStatus === 'live') {
      router.push(
        handleScorecardNav('live-score', matchID, 'commentary', seriesName)
          .href,
        handleScorecardNav('live-score', matchID, 'commentary', seriesName).as,
      )
    }

    if (matchStatus === 'completed') {
      router.push(
        handleScorecardNav('match-score', matchID, 'summary', seriesName).href,
        handleScorecardNav('match-score', matchID, 'summary', seriesName).as,
      )
    }
    CleverTap.initialize(
      matchStatus === 'live' ? 'MatchLive Tab' : 'Scorecard Tab',
      {
        Source: 'LiveBlog',
        Platform: localStorage.Platform,
      },
    )
  }
  console.log(article, 'articleeee-->>')
  if (errorR) return <div></div>
  if (landingR) {
    // return article = props && props.data &&  props.data.data.getLiveBlockComments
    return <div></div>
  } else {
    return (
      <>
        {article && (
          <>
            <title>{`${article.title} | ${article.type}`}</title>
            <h1 className="dn">
              {/* {article.title ? article.title : 'Cricket News'} */}
            </h1>
            <meta name="description" content={article.description} />
            <meta name="keywords" content={article.seoTags} />
            <meta itemprop="width" content="595" />
            <meta itemprop="height" content="450" />
            <meta itemprop="url" content={article.thumbnail} />
            <meta property="og:image" content={article.thumbnail} />
            <meta property="og:width" content="595" />
            <meta property="og:height" content="450" />
            <meta property="og:image:width" content="595" />
            <meta property="og:image:height" content="450" />
            <meta property="og:image:secure_url" content={article.thumbnail} />
            <meta
              property="og:title"
              content={article.title || 'cricket.com article'}
            />
            <meta
              property="og:url"
              content={`https://www.cricket.com/news/live-blog/${article.matchID}/${article.matchSlug}/${article.articleID}`}
            />
            <meta property="og:type" content="Article" />
            <meta property="fb:app_id" content="631 889693979290" />
            <meta property="og:site_name" content="cricket.com" />
            <link
              rel="canonical"
              href={`https://www.cricket.com/news/live-blog/${article.matchID}/${article.matchSlug}/${article.articleID}`}
            />
            <link
              rel="apple-touch-icon"
              href="http://mysite.com/img/apple-touch-icon-57x57.png"
            />
            <script
              async
              src="https://platform.twitter.com/widgets.js"
            ></script>
            <script
              async
              src="https://platform.instagram.com/en_US/embeds.js"
            ></script>
          </>
        )}
        <div className="max-w-8xl center text-white md:pb-20 md:mr-12 lg:mr-12 xl:mr-12">
          <div className="mr-2 absolute left-0 right-0 m-4 lg:hidden flex justify-between items-center ">
            <img
              src={backIconWhite}
              alt="back icon"
              onClick={() => window.history.back()}
            />
          </div>
          {showHeader && (
            <div
              className="lg:hidden fixed top-0 left-0 right-0 bg-gray-8 dark:bg-gray-1 px-2 flex items-center flex-auto justify-between md:hidden z-40"
              style={{ height: 46 }}
            >
              <div className="flex items-center w-full">
                {/* <Link {...getArticleTabUrl()}> */}
                <img
                  src={backIconWhite}
                  alt="back icon"
                  onClick={() => window.history.back()}
                />
                {/* </Link> */}
                <div
                  className="font-semibold ml-2 truncate"
                  style={{ fontSize: fontSubHeading[fontSize] }}
                >
                  {(article && article.title) || ''}
                </div>
              </div>
            </div>
          )}
          <div className="cover md:p-3 md:rounded-md md:shadow md:my-4 md:bg-white bg-top lg:border-r-2">
            <img
              className="hidden lg:block md:block h-[500px] w-full object-cover md:rounded-md md:p-2 object-top"
              src={`${
                article && article.bg_image_safari
              }?auto=compress&dpr=1&fit=clip&w=800&h=480 `}
              alt="safari"
            />
            <img
              className="md:hidden lg:hidden h-56 w-full object-cover object-top"
              src={`${
                article && article.bg_image_safari
              }?auto=compress&dpr=2&fit=clip&h=256&w=${screenWidth}`}
              alt="safari"
            />
          </div>
          <div className="f6 lh-copy ">
            {/* <div className='flex  z-9999 flex-column'>
          <div
            className='text-center z-9999 bg-gray-4-50  fixed right-0 fw8 f6'
            style={{
              borderColor: 'transparent'
            }}>
            <img
              alt='heart'
              className='shadow-1 pa2 z-9999
                        b--near-white   bg-gray-4 text-center w-40 pa1
                         br-100 mr-2 cursor-pointer dn-ns'
  
              onClick={debounce(likeHandle, 1000)}
  
              // onClick={() => (apiCall ? likeHandle() : null)}
              src={FlagArray.indexOf(router.asPath.split('/')[5]) >= 0 ? '/svgs/heart.svg' : '/svgs/gray.svg'}
              style={{ height: 25, width: 25 }}
            />
            <p className='dn-ns db mt1 mr-2'>{articleCount}</p>
          </div>
  
          <div className=' text-center z-9999  mt5 fixed items-center justify-center right-0 '>
            <img
              className='shadow-1 z-9999 pa2 b--near-white mt3  bg-gray-4 text-center w-30 pa1 br-100 mr-2 cursor-pointer dn-ns'
              src='/svgs/smallShare.svg'
              style={{ height: 25, width: 23 }}
              onClick={handleShare}
              alt='Share'
            />
          </div>
        </div> */}
            {article && article.content && parse(article.content)}{' '}
          </div>
          {}
          <div className="md:px-4 px-2 pt-2 md:shadow dark:bg-basebg bg-white dark:h-screen dark:border-gray-1 ">
            <div className="flex md:flex-row-reverse flex-wrap justify-between items-center text-sm font-medium md:mt-3 py-2 text-gray-2">
              {
                article?.newAuthors && (
                  <div className="flex mt-1">
                    {/* <span className="text-gray-2 text-xs font-medium ">By:</span> */}
                    <img
                      src="/pngsV2/authoriconnew.png"
                      alt=""
                      className="md:block hidden"
                      height={'20px'}
                      width={'20px'}
                    />
                    <img
                      src="/pngsV2/user.png"
                      alt=""
                      className="md:hidden"
                      height={'20px'}
                      width={'20px'}
                    />
                    {article?.newAuthors &&
                      article?.newAuthors.map((item, index) => {
                        return (
                          <div
                            onClick={() => {
                              item.id && item.name && goToAUthor(item)
                            }}
                          >
                            <div
                              className={`cursor-pointer ${
                                index != 0 ? 'mt-1' : ''
                              }  flex justify-start text-xs text-gray-2 `}
                            >
                              {/* {index == 0 && <img alt='author' src={'/svgs/author-icon.svg'} />} */}

                              <span
                                className={`${
                                  index == 0
                                    ? 'ml-1 helvetica text-black dark:text-gray-2'
                                    : 'ml-4 text-black helvetica dark:text-gray-2'
                                } `}
                              >
                                {item.name}{' '}
                              </span>
                            </div>
                          </div>
                        )
                      })}
                  </div>
                )
                // :
                // <div
                //   onClick={() => {
                //     article.author && goToAUthor(article.author)
                //   }}
                // >
                //   <div
                //     className={`cursor-pointer mt-1 flex justify-start text-xs text-gray-2 `}
                //   >
                //     <span className={'ml-1 helvetica'} >
                //       {article.author}{' '}
                //     </span>
                //   </div>
                // </div>
              }
              <div className=" text-gray-2 text-xs font-medium">
                {(article &&
                  article?.createdAt &&
                  format(+article.createdAt, 'dd MMM yyyy')) ||
                  ''}
              </div>
            </div>
            <div className="bg-gray-4 md:bg-gray-2 h-[1px] w-full md:mb-5"></div>

            {/* <div className="lg:hidden">
              <h2 className="text-sm border-l-2 border-2 ml-2 dark:border-l-green-6 border-l-basered border-r-gray-1 border-y-gray-1 my-4 pt-1 helvetica font-bold text-white leading-5">
                <div
                  className="noto dark:pl-2 dark:font-bold font-semibold"
                  style={{ fontSize: fontSubHeading[fontSize] }}
                >
                  {(article?.description && article.description) || ''}
                </div>
              </h2>
            </div> */}
            <div className={`${fontSize} leading-6 serif-noto`}>
              {article && article.content && parse(article.content)}
            </div>

            <div className="flex justify-between items-center ">
              <div className="flex items-center justify-center">
                {article && article.matchStatus === 'live' && (
                  <div className="flex items-center py-1 px-2 border-solid border-black rounded-full mr-2">
                    <span className="bg-green h-3 w-3 rounded-full" />
                    <span className="text-sm md:text-gray-4 text-white font-medium black-40 pl-1">
                      LIVE
                    </span>
                  </div>
                )}
                {/* {article && article.blogType !== "blog" && (
                  <div className="font-semibold text-center darkRed md:pl-4 lg:text-base md:text-base text-sm ">
                    {article && article.matchNumber} :{" "}
                    {article && article.matchName}
                  </div>
                )} */}
              </div>

              {/* <div className=" flex items-center justify-between cursor-pointer  br2 py-1 white px-2 cdcgr">
            <div className="fw5 f6-l  py-1  f6-m f7" onClick={() => { handleNavigation(article.matchStatus, matchID, matchName) }}>{article && article.matchStatus === "live" ? 'Live Scores' : 'Scorecard'} </div>
            <img className='cursor-pointer  pl1' alt='/svgs/RightSchevronWhite.svg'
              src='/svgs/RightSchevronWhite.svg' />
          </div> */}
            </div>
            {/* `${fontSize} lh-copy serif-noto */}
            {/* <div
              className="py-2 font-bold lh-title f5 lg:f5 md:f5 helvetica"
              style={{ fontSize: fontHeading[fontSize] }}
            >
              {article && article.title}
            </div> */}
            <div className="flex items-center dark:my-0.5 my-3 justify-between">
              {/* <div className=" text-gray-2 text-xs font-medium">
                {(article &&
                  article.createdAt &&
                  format(+article.createdAt, "dd MMM yyyy")) ||
                  ""}
              </div> */}
              {/* <div className="text-center darkRed md:pl-2 lg:text-base md:text-base text-sm dark:text-gray-2 text-black font-bold">
                <span className="border-l-2 border-l-solid border-l-basered"></span>

                <span className="mx-2">{article && article.title}</span> 
              </div> */}
              <h2 className="text-sm border-l-2 border-left-2 ml-1 dark:border-l-green-6 border-l-basered my-2 text-black font-bold dark:text-white">
                <div
                  className="noto pl-2 dark:font-bold font-semibold"
                  style={{ fontSize: fontHeading[fontSize] }}
                >
                  {article?.title && article.title}
                </div>
              </h2>
              {/* {console.log(
                article.matchStatus,
                matchID,
                matchName,
                'dataOfScoreCard--->>>>>',
              )} */}
              {article && article.blogType !== 'blog' && (
                <div className="flex items-center justify-between cursor-pointer border-2 rounded-md dark:bg-gray-8 bg-basered px-3 py-0.5 dark:border-green-6">
                  <div
                    className="font-medium dark:text-green-6 text-white rounded-full"
                    onClick={() => {
                      handleNavigation(article?.matchStatus, matchID, matchName)
                    }}
                  >
                    {article && article.matchStatus === 'live'
                      ? 'Live Scores'
                      : 'Scorecard'}{' '}
                  </div>
                  {console.log(
                    article.matchStatus,
                    matchID,
                    matchName,
                    'dataOfScoreCard--->>>>>',
                  )}
                  <div className="lg:hidden">
                    <svg
                      width="24"
                      viewBox="0 0 24 24"
                      className="cursor-pointer"
                    >
                      <path
                        fill="#38d925"
                        d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                      ></path>
                      <path fill="none" d="M0 0h24v24H0z"></path>
                    </svg>
                  </div>

                  <div className="hidden lg:block md:block">
                    <svg
                      width="24"
                      viewBox="0 0 24 24"
                      className="cursor-pointer"
                    >
                      <path
                        fill="#FFFFFF"
                        d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                      ></path>
                      <path fill="none" d="M0 0h24v24H0z"></path>
                    </svg>
                  </div>
                </div>
              )}
            </div>
            <div
              className="dark:text-gray-2 text-black font-semibold md:pl-2 pb-3"
              style={{ fontSize: fontSubHeading[fontSize] }}
            >
              {article && article.matchNumber} : {article && article.matchName}
            </div>
            <div
              className={
                article && article.comments && article.comments.length > 0
                  ? 'pt-3 w-full pl-1'
                  : 'pt-4 w-full'
              }
            >
              {article &&
                article.comments &&
                article.comments.length > 0 &&
                article.comments.map((comm, i) => (
                  <div
                    className={`
                    ${
                      fontSize === 'f6'
                        ? 'text-xs'
                        : fontSize === 'f5'
                        ? 'text-sm'
                        : fontSize === 'f55'
                        ? 'text-lg'
                        : ''
                    }
                    w-full flex border-l border-l-black dark:border-l-gray-700 dark:border-l-2 relative md:my-3`}
                    id={i}
                    key={i}
                  >
                    <div className="w-2 h-2 absolute -left-2 -top-2 mt-2 m-1 dark:bg-green bg-basered rounded-full"></div>

                    <div className="w-full pl-2 md:-mt-2">
                      <div className="flex justify-between">
                        <div
                          className={` ${fontSize} serif-noto font-semibold text-bold text-md dark:text-white text-basered`}
                        >
                          {comm.currentScore}
                        </div>
                        {article && article.blogType !== 'blog' && (
                          <div
                            className={` ${fontSize} serif-noto font-medium text-gray-2`}
                          >{`${
                            comm &&
                            comm.createdAt &&
                            format(+comm.createdAt, ' hh:mm a')
                          } IST `}</div>
                        )}
                      </div>
                      <div
                        className={`font-semibold lh-title dark:text-white text-black text-sm  ${
                          comm.currentScore === '' ? '' : 'pt-1'
                        }`}
                      >
                        {comm.title}
                      </div>
                      {article && article.blogType === 'blog' && (
                        <div
                          className={` ${fontSize}  serif-noto font-normal text-gray-1 py-1`}
                        >{`${
                          comm &&
                          comm.createdAt &&
                          format(+comm.createdAt, ' hh:mm a')
                        } IST `}</div>
                      )}
                      <div
                        className={`font-light lh-copy lg:text-base removemargin mb-2 dark:text-gray-2 text-black lh-copy serif-noto`}
                        style={{ whiteSpace: 'break-spaces' }}
                      >
                        {comm && comm.content && parse(comm.content)}
                      </div>
                      {comm.image && (
                        <div className="md:hidden w-full">
                          <img
                            className="w-full"
                            style={{ height: '180px' }}
                            src={`${comm.image_desk}?auto=compress&h=180&w=${
                              screenWidth - 30
                            }`}
                          ></img>
                        </div>
                      )}
                      {comm.image && (
                        <div className="hidden md:block w-full">
                          <img
                            className="pt-2 w-full"
                            style={{ height: '400px' }}
                            src={`${comm.image_desk}?auto=compress&h=400&w=${
                              screenWidth - 100
                            }dpr=2`}
                          ></img>
                        </div>
                      )}
                    </div>
                  </div>
                ))}
            </div>
            <div className="hidden pl-4 pt-4 lg:block">
              <div className="flex flex-wrap">
                <div
                  className="flex cursor-pointer p-2 flex-wrap justify-center items-center dark:text-white text-black font-semibold text-center w-20 f5 shadow-5"
                  onClick={debounce(likeHandle, 1000)}
                >
                  <img
                    alt="heart"
                    className="p-2 z-9999 
                        text-center w-40 
                         mr-2 cursor-pointer "
                    // onClick={() => (apiCall ? likeHandle() : null)}
                    src={
                      FlagArray.indexOf(params?.slugs?.[3]) >= 0
                        ? '/svgs/redHeart.svg'
                        : '/svgsV2/heartWhite.svg'
                    }
                    style={{ height: 45, width: 40 }}
                  />
                  {articleCount !== null &&
                  FlagArray.indexOf(params?.slugs?.[3]) >= 0
                    ? articleCount
                    : ''}
                </div>

                <div
                  onClick={handleShare}
                  className="flex cursor-pointer p-1 px-3 flex-wrap justify-center items-center font-bold text-center w-20 f6 shadow-5 md:hidden"
                >
                  <img
                    className="mr-2 cursor-pointer"
                    src={'/svgs/shareWhite.svg'}
                    alt="shareGray"
                  />{' '}
                  Share
                </div>
              </div>
            </div>
            <div className="md:py-4 dark:pb-16">
              {article && article.tags && article.tags.length > 0 && (
                <div className="font-medium dark:text-white text-black text-base py-2">
                  Tags
                </div>
              )}
              <div className="flex">
                {article &&
                  article.tags &&
                  article.tags.map((tag, key) => (
                    <span
                      key={key}
                      className="px-2 border-2 text-base font-medium cursor-pointer dark:text-green-6 mr-2 rounded-md dark:border-green-6 dark:bg-gray-4 bg-basered text-white"
                    >
                      {tag.name}
                    </span>
                  ))}
              </div>
            </div>
          </div>
          {showFont && (
            <div
              className="w-full z-4 flex items-center justify-center lg:hidden fixed left-0 right-0"
              style={{ bottom: '1.8rem' }}
            >
              <div className="w-1/4 flex items-center justify-center"></div>

              <div className="w-1/4 flex items-center justify-center"></div>
              <div className="w-1/4 flex-column block justify-around items-center shadow-triangleMain">
                <div
                  className={` flex items-center justify-center p-1 py-2 f4 border-b-2 border-b-black font-semibold text-gray-2 bg-basebg shadow-triangle1 ${
                    fontSize == 'f55' ? 'text-green-6' : ''
                  }  `}
                  onClick={() => (setFontSize('f55'), setShowFont(false))}
                >
                  Aa
                </div>
                <div
                  className={` flex items-center justify-center f5 p-1 py-2 text-gray-2 border-b-2 border-b-black font-normal bg-basebg shadow-triangle2 ${
                    fontSize == 'f5' ? 'text-green-6' : ''
                  }  `}
                  onClick={() => (setFontSize('f5'), setShowFont(false))}
                >
                  Aa
                </div>

                <div
                  className={`flex items-center justify-center text-gray-2 font-thin f6 py-2 border-b-black border-b-2 p-1 bg-basebg shadow-triangle ${
                    fontSize == 'f6' ? 'text-green-6' : ''
                  } `}
                  onClick={() => (setFontSize('f6'), setShowFont(false))}
                >
                  Aa
                </div>

                <div className=" flex items-center justify-center font-normal red_10 p-1">
                  <div className="flex items-center justify-center">
                    <div className="flex items-center justify-center arrow-down1"></div>
                  </div>
                </div>
              </div>
              <div className="w-1/4 flex items-center justify-center"></div>
            </div>
          )}
          {/*  */}
          <div className="w-full items-center justify-center block lg:hidden bottom-0 left-0 right-0 fixed">
            <div className="w-full flex justify-around items-center shadow-1 z-50 bg-gray py-1 pb-2">
              <div className="w-1/4 flex items-center justify-center">
                <img
                  src={
                    FlagArray.indexOf(params?.slugs?.[3]) >= 0
                      ? '/svgs/redHeart.svg'
                      : '/svgsV2/heartWhite.svg'
                  }
                  // src={ global.window && global.window.localStorage.getItem('FlagArray') !== null && global.window.localStorage.getItem('FlagArray') !== null && JSON.parse(global.window.localStorage.getItem('FlagArray')).indexOf(router.query.type)>0?`/svgs/redHeart.svg`:'/svgsV2/heartWhite.svg'}
                  onClick={debounce(likeHandle, 1000)}
                  alt=""
                />

                <span className="darkRed font-medium f6">
                  {articleCount !== null &&
                  FlagArray.indexOf(params?.slugs?.[3]) >= 0
                    ? articleCount
                    : ''}
                </span>
              </div>
              <div className="w-1/4 flex items-center justify-center">
                <img onClick={handleShare} src="/svgs/shareWhite.svg" alt="" />
              </div>

              <div className="w-1/4 flex items-center justify-center white">
                <img
                  src="/svgsV2/font_news.svg"
                  onClick={() => setShowFont(true)}
                  alt=""
                />
              </div>

              <div className="w-1/4 flex items-center justify-center">
                <CircularProgressBar
                  strokeWidth="2"
                  sqSize="30"
                  percentage={
                    0 >= 100 - (parseInt(scrolledValue) / 100) * 100
                      ? 0
                      : 100 - (parseInt(scrolledValue) / 100) * 100
                  }
                  value={scrolledValue}
                />
              </div>
            </div>
          </div>
          {/*  */}
        </div>
      </>
    )
  }
}

const CircularProgressBar = (props) => {
  const sqSize = props.sqSize
  // SVG centers the stroke width on the radius, subtract out so circle fits in square
  const radius = (props.sqSize - props.strokeWidth) / 2
  // Enclose cicle in a circumscribing square
  const viewBox = `0 0 ${sqSize} ${sqSize}`
  // Arc length at 100% coverage is the circle circumference
  const dashArray = radius * Math.PI * 2
  // Scale 100% coverage overlay with the actual percent
  const dashOffset = dashArray - (dashArray * (100 - props.percentage)) / 100

  return (
    <svg width={props.sqSize} height={props.sqSize} viewBox={viewBox}>
      <circle
        style={{
          fill: '#1F2634',
          stroke: '#000000',
          strokeLinecap: 'round',
          strokeLinejoin: 'round',
        }}
        className="circle-background"
        cx={props.sqSize / 2}
        cy={props.sqSize / 2}
        r={radius}
        strokeWidth={`${props.strokeWidth}px`}
      />

      <circle
        className="circle-progress"
        cx={props.sqSize / 2}
        cy={props.sqSize / 2}
        r={radius}
        strokeWidth={`${props.strokeWidth}px`}
        // Start progress marker at 12 O'Clock
        transform={`rotate(-90 ${props.sqSize / 2} ${props.sqSize / 2})`}
        style={{
          strokeDasharray: dashArray,
          strokeDashoffset: dashOffset,
          fill: 'none',
          stroke: '#38D926',
          transition: 'stroke-dashoffset 0s linear 0s',
          strokeLinecap: 'round',
          strokeLinejoin: 'round',
        }}
      />
      {props.percentage < 3 && (
        <text
          style={{
            fontSize: 12,
            fontWeight: '900',
            fill: '#38D926',
            stroke: '#38D926',
          }}
          x="50%"
          y="50%"
          dy=".3em"
          textAnchor="middle"
        >
          ✓
        </text>
      )}
    </svg>
  )
}
