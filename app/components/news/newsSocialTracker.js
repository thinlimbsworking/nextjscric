import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'

import Heading from '../shared/heading'
import parse from 'html-react-parser'
import { GET_NEWS_SOCIAL_TRACKER } from '../../api/queries'
import { TwitterTweetEmbed } from 'react-twitter-embed'
import Loading from '../loading'
import TwitterTimeline from 'react-twitter-embedded-timeline'
import Head from 'next/head'
import { Timeline } from 'react-twitter-widgets'
export default function NewsSocialTracker() {
  const [windows, updateWindows] = useState()
  const callSocialWidget = () => {
    global.windows &&
      global.windows.twttr.ready().then(() => {
        global.windows &&
          global.windows.twttr.widgets.load(
            document.getElementsByClassName('twitter-div'),
          )
      })
  }

  const { loading, error, data: socialData } = useQuery(GET_NEWS_SOCIAL_TRACKER)

  useEffect(() => {
    if (global.window) updateWindows(global.window)
  }, [global.window])

  useEffect(() => {
    const s = document.createElement('script')
    s.setAttribute('src', 'https://platform.twitter.com/widgets.js')
    s.setAttribute('async', 'true')
    document.head.appendChild(s)
  }, [])

  if (loading) {
    return <Loading />
  }

  if (socialData) {
    return (
      <>
        <div className="flex flex-col overflow-scroll ">
          {socialData &&
            socialData.getSocialTracker?.data?.map((item, index) => {
              return (
                <div className=" " key={item.twitterID}>
                  <TwitterTweetEmbed
                    key={item.twitterID}
                    tweetId={item.twitterID}
                  />
                </div>
              )
            })}
        </div>
      </>
    )
  }
}
