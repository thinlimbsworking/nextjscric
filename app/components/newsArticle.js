import React, { useState, useRef, useEffect } from 'react'
import { format, formatDistanceToNowStrict } from 'date-fns'
import { useRouter } from 'next/navigation'
import { Swiper, SwiperSlide } from 'swiper/react'
import {
  EffectCoverflow,
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Controller,
  FreeMode,
} from 'swiper'
import Heading from './commom/heading'
import Link from 'next/link'
{
  /* <Link href={`/news/${data.getArticleByPostitions[0].articleID}`}></Link> */
}
export default function NewsArticle({ data, ...props }) {
  const [swiper, setSwiper] = useState()
  const router = useRouter()
  const navigate = router.push
  console.log('1234data', data)
  return (
    <div className="flex flex-col bg-gray-4  px-2 rounded-md ">
      <div className="flex justify-evenly items-center  p-2 ">
        <div className="text-left w-full">
          <Heading heading={'Articles'} lineColor={'#D8993B'} />
        </div>
        <Link href={`/news/latest`}>
          <div className="bg-basebg px-2 py-1.5 rounded-md">
            <img
              className="h-6 w-6 object-cover object-top"
              src="/pngsV2/arrow.png"
              alt=""
            />
          </div>
        </Link>
      </div>
      <Link
        href={
          data.getArticleByPostitions[0].type === 'live-blog'
            ? `/news/live-blog/${data?.getArticleByPostitions[0]?.matchIDs?.[0]?.matchID}/${data?.getArticleByPostitions[0]?.matchIDs?.[0]?.matchSlug}/${data?.getArticleByPostitions[0]?.articleID}`
            : `/news/${data?.getArticleByPostitions[0]?.articleID}`
        }
      >
        <div className="  mx-2  pb-2 border-black">
          <div className="flex relative items-center justify-center  w-full ">
            <img
              className="  flex relative items-center justify-center w-full  rounded-md h-48  object-top obejct-cover"
              src={`${data?.getArticleByPostitions[0]?.featureThumbnail}?auto=compress&crop=fit&w=350&h=250`}
              alt=""
            />
            {data?.getArticleByPostitions[0]?.type ? (
              <div
                style={{ fontSize: 10, maxWidth: 100 }}
                className="absolute left-0 bottom-1 text-sm bg-green px-1 uppercase font-medium text-black"
              >
                <p className="line-clamp-1 overflow-ellipses">
                  {data?.getArticleByPostitions[0]?.type.replace(/[_]/g, ' ')}
                </p>
              </div>
            ) : (
              <></>
            )}
          </div>
          <div className="    overflow-hidden mt-2 text-sm font-semibold  font-sans text-left   ">
            {data?.getArticleByPostitions[0]?.title || ''}
          </div>
          <div className="  text-xs font-normal mt-1 line-clamp-2 text-left text-white ">
            {data?.getArticleByPostitions[0]?.description || ''}
          </div>
          <div className="flex items-center  justify-start pt-1">
            <div className="pr-2  text-gray-2 text-xs text-left  line-clamp-1  border-r">
              {data?.getArticleByPostitions[0]?.author}
            </div>
            <div className="px-2 text-gray-2 text-xs text-left ">
              {format(
                new Date(
                  Number(data?.getArticleByPostitions[0]?.createdAt) - 19800000,
                ),
                'dd MMM yyyy',
              )}
            </div>
          </div>
        </div>
      </Link>
      <div className="flex overflow-x-scroll w-full ">
        {data &&
          data.getArticleByPostitions.map((item, i) => {
            return (
              0<i && i< 5 && (
                <Link
                  href={
                    item.type === 'live-blog'
                      ? `/news/live-blog/${item?.matchIDs[0]?.matchID}/${item?.matchIDs[0]?.matchSlug}/${item.articleID}`
                      : `/news/${item.articleID}`
                  }
                  // href={
                  //   item.type === 'wlive-blog'
                  //     ? `/news/live-blog/${item.articleID}`
                  //     : `/news/${item.articleID}`
                  // }
                >
                
                 
                  <div className="  w-52 h-52 m-1">
                    <div className="flex relative items-center justify-center w-52 ">
                      <img
                        className="h-32   w-52 rounded-lg object-conatin object-top "
                        src={`${item.featureThumbnail}?auto=compress&crop=fit&w=350&h=250`}
                        alt=""
                      />
                      {item.type ? (
                        <div
                          style={{ fontSize: 10, maxWidth: 120 }}
                          className="absolute left-0 bottom-1 text-sm bg-green px-1 uppercase font-medium text-black line-clamp-1"
                        >
                          {item.type.replace(/[_]/g, ' ')}
                        </div>
                      ) : (
                        <></>
                      )}
                    </div>
                    <div className="  overflow-hidden text-xs font-medium  font-sans line-clamp-2 pt-2 ">
                      {item.title || ''}
                    </div>

                    <div className="flex items-center  justify-start p-1 pt-2">
                      <div className="pr-2  text-gray-2 text-xs text-left     border-r">
                        {item.author}
                      </div>
                      <div className="px-2 text-gray-2 text-xs text-left ">
                        {format(
                          new Date(Number(item.createdAt) - 19800000),
                          'dd MMM yyyy',
                        )}
                      </div>
                    </div>
                  </div>{' '}
                </Link>
              )
            )
          })}
      </div>
    </div>
  )
}
