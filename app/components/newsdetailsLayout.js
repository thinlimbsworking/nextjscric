'use client'
import { useQuery } from '@apollo/react-hooks'
import Link from 'next/link'

import React, { useEffect, useState, useRef } from 'react'
// import NewsSocialTracker from '../components/news/newsSocialTracker'
import { ARTICLE_LIST, SERIES_NAME_FOR_ARTICLES } from '../api/queries'
import ArticleCard from '../components/articlecard'
import { NEWS } from '../constant/MetaDescriptions'
import CommonDropdown from './commom/commonDropdown'
import MetaDescriptor from './MetaDescriptor'
import Heading from './shared/heading'
import Tab from './shared/Tab'
import DataNotFound from './commom/datanotfound'
import Loading from '../components/loading'
import { event } from 'clevertap-react'

const NewsDetailsLayout = ({ tabs, tabsConfig, filter, tab, ...props }) => {
  const [data2, setData2] = useState(filter)
  const [articlesFilter, setArticlesFilter] = useState([
    // { valueLable: 'All', valueId: '' },
  ])

  // useEffect(()=>{

  // },[])
  const [activefilter, setActivefilter] = useState(articlesFilter?.[0])
  const [activeTab, setActiveTab] = useState({
    name: tabs[0],
    value: tabs[0],
  })

  const [pagination, setPagination] = useState(0)
  const [windows, updateWindows] = useState()

  const { loading, error, data, fetchMore } = useQuery(ARTICLE_LIST, {
    variables: {
      type: activeTab?.value,
      page: 0,
      seriesID: filter.valueId,
    },
    fetchPolicy: 'network-only',

    // onCompleted: (data) => {

    //
    // }
  })

  window.addEventListener('scroll', () => handleScroll())

  let scrl = useRef(null)

  const handlePagination = () => {
    if (data && filter.valueLable.toUpperCase() === 'ALL') {
      fetchMore({
        variables: {
          type: activeTab.name.toLowerCase(),
          // seriesID: filter.valueId,
          page: Math.floor(data?.getArticles?.length / 20),
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!previousResult) return previousResult

          let parsedPre = previousResult.getArticles
          let newData = fetchMoreResult.getArticles

          let removeDupli = [...parsedPre, ...newData]
          let uniq = new Set(
            removeDupli && removeDupli.map((e) => JSON.stringify(e)),
          )

          let res = Array.from(uniq).map((e) => JSON.parse(e))

          let resultedArticle = Object.assign({}, previousResult, {
            getArticles: res,
            __typename: 'getArtciles',
          })

          return { ...resultedArticle }
        },
      })
    }

    if (data && filter.valueLable.toUpperCase() !== 'ALL') {
      // alert(data2.valueLable)
      // alert(filter.valueLable.toUpperCase())

      fetchMore({
        variables: {
          type: tab,
          seriesID: filter.valueId,
          page: Math.floor(data?.getArticles?.length / 20),
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!previousResult) return previousResult

          let parsedPre = previousResult.getArticles
          let newData = fetchMoreResult.getArticles

          let removeDupli = [...parsedPre, ...newData]
          let uniq = new Set(
            removeDupli && removeDupli.map((e) => JSON.stringify(e)),
          )

          let res = Array.from(uniq).map((e) => JSON.parse(e))

          let resultedArticle = Object.assign({}, previousResult, {
            getArticles: res,
            __typename: 'getArtciles',
          })

          return { ...resultedArticle }
        },
      })
    }
  }

  const handleScroll = (evt) => {
    //
    //
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      // setPagination((prev)=>prev + 1)

      // alert("end")

      // alert(filter.valueLable)
      handlePagination()
    }
  }

  if (loading) {
    return <></>
  }

  if (data) {
    return (
      <>
        {/* <div className="hidden md:flex lg:flex bg-basered z-1 px-2 py-4 items-center justify-center text-white">
        <div className="w-2/12 flex items-center justify-end pr-8">
          <span className="text-xl font-bold capitalize">NEWS</span>
        </div>
      </div> */}

        <MetaDescriptor section={NEWS[tab]} />
        {}
        <div className="text-white dark:bg-basebg ">
          <div className="w-full dark:bg-gray-8 xl:top-16 lg:top-16 md:top-16 top-0">
            <div className="md:hidden lg:hidden xl:hidden">
              <div className="flex sticky justify-between scrollmenu flex-col gap-1">
                <div className="">
                  {filter?.valueLable.toUpperCase() === 'ALL' && (
                    <Tab
                      data={tabs.map((tab) => ({
                        name:
                          tab?.toUpperCase() === 'MATCHRELATED' ? (
                            <span className="whitespace-nowrap">
                              MATCH RELATED
                            </span>
                          ) : (
                            tab.toUpperCase()
                          ),
                        value: tab,
                      }))}
                      selectedTab={activeTab}
                      handleTabChange={(tab) => setActiveTab(tab)}
                      type="line"
                    />
                  )}
                </div>
                {/* <CommonDropdown
                  options={articlesFilter || []}
                  onChange={(option) => setActivefilter(option)}
                  defaultSelected={activefilter}
                  placeHolder={activefilter?.valueLable}
                  showExpandIcon
                  variant="outline"
                  isFixedHeight
                /> */}
              </div>
            </div>
            {/* <div className="hidden lg:block md:block mt-4"> */}
            {/* {!loading && (
              <span className="bg-white dark:bg-gray-8">
                <Link
                  key={data?.getArticles?.[0]?.type}
                  {...props.getNewsUrl(data?.getArticles?.[0], activeTab)}
                  passHref
                  className="w-full lg:w-full md:cursor-pointer"
                  onClick={() => {
                    props.handleCleverTab(data?.getArticles?.[0])
                  }}
                >
                  {data?.getArticles?.[0] ? (
                    <ArticleCard isFeatured article={data?.getArticles?.[0]} />
                  ) : (
                    <DataNotFound />
                  )}
                </Link>
              </span>
            )} */}
            {/* </div> */}
            {data?.getArticles?.length !== 0 && (
              <div className="lg:hidden md:hidden xl:hidden">
                {!loading &&
                data?.getArticles?.length !== 0 &&
                data?.getArticles?.[0]?.type !== 'live-blog' ? (
                  <span className="bg-white dark:bg-gray-8">
                    <Link
                      key={data?.getArticles?.[0]?.type}
                      {...props.getNewsUrl(data?.getArticles?.[0], activeTab)}
                      passHref
                      className="w-full lg:w-full md:cursor-pointer"
                      onClick={() => {
                        props.handleCleverTab(data?.getArticles?.[0])
                      }}
                    >
                      <ArticleCard
                        pagination={pagination}
                        isFeatured
                        article={data?.getArticles?.[0]}
                      />
                      {/* <div className='divider' /> */}
                    </Link>
                  </span>
                ) : (
                  <span className="bg-white dark:bg-gray-8">
                    <Link
                      key={data?.getArticles?.[0]?.type}
                      {...props.News_Live_Tab_URL(
                        data?.getArticles?.[0],
                        activeTab,
                      )}
                      passHref
                      className="w-full lg:w-full md:cursor-pointer"
                      onClick={() => {
                        props.handleCleverTab(data?.getArticles?.[0])
                      }}
                    >
                      <ArticleCard
                        pagination={pagination}
                        isFeatured
                        article={data?.getArticles?.[0]}
                      />
                      {/* <div className='divider' /> */}
                    </Link>
                  </span>
                )}
              </div>
            )}

            <div className="grid dark:flex dark:w-full ">
              <div className="col-span-2 dark:w-full">
                {/* <div className="hidden lg:block md:block">
                <CommonDropdown
                  options={articlesFilter || []}
                  onChange={(option) => setActivefilter(option)}
                  defaultSelected={activefilter}
                  placeHolder={activefilter?.valueLable}
                  showExpandIcon
                  variant="outline"
                  isNews
                  isFixedHeight
                />
              </div> */}
                {data && data.getArticles.length !== 0 && (
                  <div className="hidden lg:block md:block mt-1" ref={scrl}>
                    {!loading &&
                    data?.getArticles?.[0]?.type !== 'live-blog' ? (
                      <span className="bg-white dark:bg-gray-8">
                        <Link
                          key={data?.getArticles?.[0]?.type}
                          {...props.getNewsUrl(
                            data?.getArticles?.[0],
                            activeTab,
                          )}
                          passHref
                          className="w-full md:w-full md:cursor-pointer"
                          onClick={() => {
                            props.handleCleverTab(data?.getArticles?.[0])
                          }}
                        >
                          {data?.getArticles?.[0] ? (
                            <>
                              <ArticleCard
                                isFeatured
                                article={data?.getArticles?.[0]}
                              />
                            </>
                          ) : (
                            <div />
                          )}
                        </Link>
                      </span>
                    ) : (
                      <span className="bg-white dark:bg-gray-8">
                        <Link
                          key={data?.getArticles?.[0]?.type}
                          {...props.News_Live_Tab_URL(
                            data?.getArticles?.[0],
                            activeTab,
                          )}
                          passHref
                          className="w-full lg:w-full md:md:cursor-pointer"
                          onClick={() => {
                            props.handleCleverTab(data?.getArticles?.[0])
                          }}
                        >
                          {data?.getArticles?.[0] ? (
                            <>
                              <ArticleCard
                                isFeatured
                                article={data?.getArticles?.[0]}
                              />
                            </>
                          ) : (
                            <div />
                          )}
                        </Link>
                      </span>
                    )}
                  </div>
                )}

                <div className="flex justify-start flex-col gap-4">
                  {/* <div className="hidden lg:block md:block">
                  <CommonDropdown
                    options={articlesFilter || []}
                    onChange={(option) => setActivefilter(option)}
                    defaultSelected={activefilter}
                    placeHolder={activefilter?.valueLable}
                    showExpandIcon
                    variant="outline"
                    isNews
                    isFixedHeight
                  />
                </div> */}
                  {/* {filter?.valueLable} */}
                  {filter?.valueLable?.toUpperCase() === 'ALL' && (
                    <div className="lg:block md:block hidden mt-4">
                      <Tab
                        data={tabs.map((tab) => ({
                          name:
                            tab?.toUpperCase() === 'MATCHRELATED'
                              ? 'MATCH RELATED'
                              : tab.toUpperCase(),
                          value: tab,
                        }))}
                        selectedTab={activeTab}
                        handleTabChange={(tab) => setActiveTab(tab)}
                        type="block"
                      />
                    </div>
                  )}
                  <div className="w-full desktopScroll ">
                    {error ? (
                      <div className="w-full h-screen text-sm font-semibold text-gray-2 flex flex-col justify-center items-center">
                        <span>Something isn't right!</span>
                        <span>Please refresh and try again...</span>
                      </div>
                    ) : (
                      <div
                        className="dark:bg-basebg "
                        // style={{max}}
                      >
                        {data && data.getArticles.length !== 0 ? (
                          data.getArticles.slice(1).map((article, key) => (
                            <>
                              {article.type != 'live-blog' &&
                              article.type != 'live-blogs' ? (
                                <Link
                                  key={key}
                                  {...props.getNewsUrl(article, tab)}
                                  passHref
                                  className="w-full lg:w-full md:cursor-pointer"
                                  onClick={() => {
                                    props.handleCleverTab(article)
                                  }}
                                >
                                  <ArticleCard
                                    // isFeatured={
                                    //   windows
                                    //     ? windows.localStorage.Platform === 'Web'
                                    //       ? key === 0 || key === 1
                                    //       : key === 0
                                    //     : ''
                                    // }
                                    article={article}
                                    isBanner
                                  />
                                </Link>
                              ) : (
                                <Link
                                  key={key}
                                  {...props.News_Live_Tab_URL(article, tab)}
                                  passHref
                                  className="w-full lg:w-full md:cursor-pointer"
                                  onClick={() => {
                                    props.handleCleverTab(article)
                                  }}
                                >
                                  <ArticleCard
                                    // isFeatured={
                                    //   window
                                    //     ? windows.localStorage.Platform === 'Web'
                                    //       ? key === 0 || key === 1
                                    //       : key === 0
                                    //     : ''
                                    // }
                                    article={article}
                                    isBanner
                                  />

                                  {/* <div className='divider' /> */}
                                </Link>
                              )}
                            </>
                          ))
                        ) : (
                          <div className="pt-4 center">
                            {' '}
                            {/* {...props.News_Live_Tab_URL(article, tab)} */}
                            <div className="flex justify-center items-center">
                              <img
                                className="w-44"
                                src="/svgs/Empty.svg"
                                alt=""
                              />
                            </div>
                            <div className="gray text-black dark:text-white font-semibold text-center mr-4">
                              No Articles found
                            </div>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <mobileHeading title={'News & Articles'}/> */}
          {/* <h1 className='text-lg lg:text-2xl lg:py-5 lg:px-0 font-semibold p-3'>News & Articles </h1> */}

          {/* --------------------------------for mobile screen----------------------- */}
          {/* <div className='md:hidden '>
        <div className='flex overflow-x-scroll hidescroll sticky justify-between scrollmenu top-4'>
          <div>
            {tabs.map((tabName, i) => (
              <Link key={i} {...props.getTabUrl(tabName)} replace passHref>
                <a
                  key={i}
                  className={`text-white text-sm pt-2 nowrap inline-block mx-2 py-1 mb-1 text-center capitalize ${
                    tabsConfig[tabName] === tab && 'border-b-2 border-green-6'
                  }`}>
                  {tabName}
                </a>
              </Link>
            ))}
          </div>
        </div>
      </div> */}

          {/* <div className='items-center py-4 justify-start scrollmenu hidden lg:block md:block'>
        <div className='flex justify-start' >
          {tabs.map((tabName, i) => (
            <>
              <Link key={i} {...props.getTabUrl(tabName)} replace passHref>
                <a
                 
                  className={`rounded flex items-center justify-center  text-white text-sm  py-2  mr-4 pointer w-36 ${tabsConfig[tabName] === tab ? 'border-green-6 rounded-lg text-green border-2 bg-gray-4' : 'text-white'}`}
                 >
                  {tabName.toUpperCase()}
                </a>
              </Link>
            </>
          ))}
        </div>
      </div> */}
        </div>
      </>
    )
  }
}

export default NewsDetailsLayout
