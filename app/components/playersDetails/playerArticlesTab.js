import React from 'react';
import Loading from '../loading';
import ArticleCard from '../articlecard';
import { ARTICLE_VIEW, News_Live_Tab_URL } from '../../constant/Links';
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import Router from 'next/router';
const empty = '/svgs/Empty.svg';
const RightSchevronBlack = '/svgs/RightSchevronBlack.svg';

export default function PlayerArticlesTab({ data, ...props }) {

  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'ArticlesTab',
      ArticleType:
        article.type === 'match_report'
          ? 'MatchReport'
          : article.type === 'news'
            ? 'News'
            : article.type === 'fantasy'
              ? 'Fantasy'
              : article.type === 'interview'
                ? 'Interview'
                : 'Others',
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: localStorage ? localStorage.Platform : ''
    });
  };
  const getNewsUrl = (article) => {
    let articleId = article.articleID;
    let tab = 'latest';
    return {
      as: eval(ARTICLE_VIEW.as),
      href: ARTICLE_VIEW.href
    };
  };


  const navigateMatchDetailBlog = (article) => {
    ///news/live-blog/${matchId}/${matchName}/${articleID}
    let matchId = props.matchID
    let name = `${props.match.homeTeamName ? props.match.homeTeamName : props.match.homeTeamShortName}-vs-${props.match.awayTeamName ? props.match.awayTeamName : props.match.awayTeamShortName
      }-${props.match.matchNumber ? props.match.matchNumber.split(' ').join('-') : ''}-`;

    let matchName =
      name.toLowerCase() +
      (props.match.seriesName
        ? props.match.seriesName
          .replace(/[^a-zA-Z0-9]+/g, ' ')
          .split(' ')
          .join('-')
          .toLowerCase()
        : '');
    let articleID = article.articleID;
    return {
      as: eval(News_Live_Tab_URL.as),
      href: News_Live_Tab_URL.href
    };
  };

  const newsHomeNav = () => {
    CleverTap.initialize('NewsHome', {
      Source: 'Homepage',
      Platform: global.window && global.window.localStorage.Platform
    });
    Router.push(`/news/[type]`, '/news/latest');
  };

  if (!data)
    return (
      <div className='w-full fw2 f7 dark:bg-gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  if (data.length === 0) return <Loading />;
  else
    return data.getArticlesByIdAndType.length ? (
      <div className=" dark:text-white mb-4 mx-3 md:mx-0 lg:mx-0">
        {data.getArticlesByIdAndType.map((article, i) => (
          <>
            {article.type != 'live-blogs' && article.type != 'live-blog' ?
              <Link key={i} href={getNewsUrl(article).as} passHref>

                <div
                  key={i}
                  onClick={() => {
                    handleCleverTab(article);
                  }}>
                  <ArticleCard article={article} />
                </div>

              </Link> : <Link {...navigateMatchDetailBlog(article)} passHref>

              <div
                key={i}
                onClick={() => {
                  handleCleverTab(article);
                }}>
                <ArticleCard article={article} />
              </div>

            </Link>}
          </>
        ))}
      </div>
    ) : (
      <div className="bg-gray">
        <div className='fw5 f6 f5-l f5-m flex flex-column items-center justify-center ph2 pv3    tc'>
        <div className="w5 flex items-center justify-center ml3 h5">
         <img className="h-100 w-100" src={empty} alt=""/>
         </div>
          <div>No articles available for the player.</div>
         
        </div>
        <div className='bg-washed-blue h04 mt2'></div>
        {data && data.getArticleByPostitions && data.getArticleByPostitions.length>0 && <div>
          <div className='flex justify-between items-center bg-light_gray dark:bg-gray pa2'>
            <div className='f7 fw6'>RECENT NEWS</div>
            <img className='cursor-pointer' alt={RightSchevronBlack} src={RightSchevronBlack} onClick={() => newsHomeNav()} />
          </div>
          <div className='bg-black h-[1px] w-full' />
          {data.getArticleByPostitions &&
            data.getArticleByPostitions.map(
              (article, i) =>
                article && (
                  <>
                    {article.type != 'live-blogs' && article.type != 'live-blog' ?
                      <Link key={i}  {...getNewsUrl(article)} passHref>

                        <div
                          key={i}
                          onClick={() => {
                            handleCleverTab(article);
                          }}>
                          <ArticleCard article={article} />
                        </div>

                      </Link> : <Link {...navigateMatchDetailBlog(article)} passHref>

                      <div
                        key={i}
                        onClick={() => {
                          handleCleverTab(article);
                        }}>
                        <ArticleCard article={article} />
                      </div>

                    </Link>}
                  </>
                )
            )}
        </div>}
      </div>
    );
}
