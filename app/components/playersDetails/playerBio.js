import React from 'react';
import parse from 'html-react-parser';
import Heading from '../commom/heading';
let  backIconWhite = '/svgsV2/RightSchevronWhite.svg';


export default function PlayerBio({ playerDetils }) {

  let playerbio = playerDetils.getPlayersProfileV2;
  return (
    <div className=' center dark:text-white bg-white dark:bg-transparent p-2'>
      <div className=' hidden md:block lg:block '>
        <Heading heading='Player Bio'/>
      </div>
      <div className='  flex gap-2 items-center md:hidden'>
        <div className='p-2 rounded-md bg-gray-4'>
        <img className="flex items-center justify-center h-4 w-4 rotate-180" onClick={() => window.history.back()} src={backIconWhite} alt='back icon' />
        </div>
        <span className='text-base font-bold'>Player Bio</span>
      </div>
      <div className='dark:bg-gray-4 border  dark:border-none rounded-md mt-3'>
        <div className='w-full h-full hidescroll overflow-y-auto'>
          <div className=' p-2 text-xs font-bold '>{playerbio.name}</div>
          <div className='dark:bg-gray p-2 text-xs  dark:border:none dark:rounded-b-md'>{parse(playerbio.description)}</div>
        </div>
      </div>
    </div>
  );
}
