import React, { useState, useRef, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/navigation'
import { PLAYER_CAREER_STATS_DATA } from '../../api/queries'
import Loading from '../loading'
import Link from 'next/link'
//import Swiper from 'react-id-swiper';
// import Playerfallback from '/pngs/fallbackprojection.png';
const Playerfallback = '/pngsV2/playerph.png'

const Compare = '/svgs/compare.svg'
import CleverTap from 'clevertap-react'
import { getPlayerUrl } from '../../api/services'

import LineChart from './lineChart'
const empty = '/svgs/Empty.svg'
import Imgix from 'react-imgix'
import Tab from '../shared/Tab'

export default function PlayerCareerStatsTab({
  playerID,
  playerSlug,
  playerRole,
}) {
  let router = useRouter()
  const [ActiveTab, setActiveTab] = useState(
    playerRole && playerRole === 'Bowler'
      ? { name: 'Bowling Stats', value: 'bowlingStats' }
      : { name: 'Batting Stats', value: 'battingStats' },
  )
  const [swiper, updateSwiper] = useState(null)
  const [currentIndex, updateCurrentIndex] = useState(0)
  let WicketKeeper = [
    { name: 'Batting Stats', value: 'battingStats' },
    { name: 'Fielding Stats', value: 'fieldingStats' },
  ]
  let bowlingStyle = [
    { name: 'Batting Stats', value: 'battingStats' },
    { name: 'Bowling Stats', value: 'bowlingStats' },
  ]

  useEffect(() => {
    if (swiper !== null || swiper != undefined) {
      try {
        // swiper.lazy.load()
        //
        swiper.on('slideChange', () => {
          updateCurrentIndex(swiper.realIndex)
        })
      } catch (e) {}
    }
  }, [swiper])

  const { loading, error, data: playerData } = useQuery(
    PLAYER_CAREER_STATS_DATA,
    {
      variables: { playerID: playerID },
    },
  )

  const playerRoleFields = {
    battingStats: {
      label: [
        'Format',
        'Matches',
        'Innings',
        'Rec. Form',
        'Runs',
        'Bat S/R',
        'Average',
        '50s/100s',
        '4s/6s',
      ],
      value: [
        'format',
        'matches',
        'innings',
        'recForm',
        'runs',
        'strikeRate',
        'average',
        'hundredsfifties',
        'foursix',
      ],
    },
    bowlingStats: {
      label: [
        'Format',
        'Matches',
        'Innings',
        'Rec. Form',
        'Balls',
        'Wickets',
        'Economy',
        'Bowl S/R',
        'Average',
        '5w',
      ],
      value: [
        'format',
        'matches',
        'innings',
        'recForm',
        'ballsBowled',
        'wickets',
        'economyRate',
        'strikeRate',
        'average',
        'fivetenWicketHauls',
      ],
    },
    fieldingStats: {
      label: ['Format', 'Catches', 'Stumpings', 'Run Outs'],
      value: ['format', 'catches', 'stupms', 'runOuts'],
    },
  }

  const params1 = {
    preloadImages: true,
    updateOnImagesReady: true,
    loadPrevNext: true,
    loadPrevNextAmount: 3,
    effect: 'coverflow',
    loop: true,
    setWrapperSize: true,
    slidesPerView: 3,
    // setWrapperSize: false,
    centeredSlides: true,
    loopedSlides: 3,
    coverflowEffect: {
      rotate: 10,
      stretch: 0,
      depth: 50,
      slideShadows: false,
      modifier: 1,
      observer: true,
      observeParents: true,
    },
    // breakpoints: { 640: { slidesPerView: , spaceBetween: 0 } },
    getSwiper: updateSwiper,
    navigation: {
      nextEl: '#nbutton',
      prevEl: '#pbutton',
    },
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      // 320: {
      //    slidesPerView: 3,
      //    spaceBetween: 20,
      //    coverflowEffect: {
      //       rotate: 10,
      //       stretch: 0,
      //       depth: 50,
      //       slideShadows: false,
      //       modifier: 1
      //    }
      // },
      // when window width is >= 480px
      // when window width is >= 640px
      760: {
        preloadImages: true,
        updateOnImagesReady: true,
        loadPrevNext: true,
        loadPrevNextAmount: 5,
        loopedSlides: 5,
        loop: true,
        observer: true,
        observeParents: true,
        slidesPerView: 5,
        spaceBetween: 40,
        coverflowEffect: {
          rotate: 0,
          stretch: 0,
          depth: 20,
          slideShadows: false,
          modifier: 1,
        },
      },
    },
  }

  const NavigateToPlayerProfile = (playerId, playerSlug) => {
    return { as: eval(PLAYER_VIEW.as), href: eval(PLAYER_VIEW.href) }
  }

  if (error) return <div></div>
  else if (loading) {
    return <Loading />
  } else
    return (
      <>
        {playerData.getPlayersProfileV2 ? (
          <div className="w-full ">
            <div className="w-full px-16 md:w-1/2 lg:w-1/2 mx-auto pb-4">
              <Tab
                data={
                  playerData.getPlayersProfileV2.role === 'Wicket Keeper'
                    ? WicketKeeper
                    : bowlingStyle
                }
                selectedTab={ActiveTab}
                type="block"
                handleTabChange={(item) => setActiveTab(item)}
              />
            </div>
            {/* <div className='flex items-center justify-center  w-min mx-auto rounded-full  my-2  p-1 bg-gray md:bg-gray-8 lg:bg-gray-4 gap-2'>
              {(playerData.getPlayersProfileV2.role === 'Wicket Keeper' ? WicketKeeper : bowlingStyle).map(
                (categ, i) => (
                  <div
                    key={i}
                    className=' flex  itemse-center justify-center '
                    onClick={() => {
                      setActiveTab(categ);
                    }}>
                    <div
                      className={`p-2 w-32 text-xs uppercase ${ActiveTab === categ ? 'bg-gray-8 border-2 border-green-6' : 'bg-gray '
                        } text-center rounded-full cursor-pointer`}>
                      {categ === 'battingStats'
                        ? 'BATTING STATS'
                        : categ === 'fieldingStats'
                          ? 'KEEPING STATS'
                          : 'BOWLING STATS'}
                    </div>
                  </div>
                )
              )}
            </div> */}
            {/* mobile */}
            <div className=" lg:hidden md:hidden  rounded-md px-3 md:px-0 lg:px-0">
              <div className="flex hidescroll rounded-md text-sm">
                <div>
                  {playerRoleFields[ActiveTab.value].label.map((x, i) => (
                    <div
                      key={i}
                      className={`border-b shadow-3 ${
                        i === 0 ? 'bg-gray-4' : 'bg-gray'
                      }
                        } fw6 flex items-center  border-black pl-1  h-9 f6`}
                      style={{
                        width: `${
                          playerData.getPlayersProfileV2[ActiveTab.value]
                            .length < 4
                            ? '105px'
                            : '90px'
                        }`,
                      }}
                    >
                      {x}
                    </div>
                  ))}
                </div>
                <div className="flex  overflow-scroll">
                  {playerData.getPlayersProfileV2[ActiveTab.value].map(
                    (item, j) => (
                      <div
                        key={j}
                        className=""
                        style={{
                          width: `${
                            playerData.getPlayersProfileV2[ActiveTab.value]
                              .length < 4
                              ? '105px'
                              : '90px'
                          }`,
                        }}
                      >
                        {playerRoleFields[ActiveTab.value].value.map(
                          (value, k) => (
                            <div
                              key={k}
                              className={`border-b ${
                                k === 0 ? 'bg-gray-4' : 'bg-gray'
                              }
                            } border-black overflow h-9  flex items-center`}
                              style={{
                                width: `${
                                  playerData.getPlayersProfileV2[
                                    ActiveTab.value
                                  ].length < 4
                                    ? '105px'
                                    : '90px'
                                }`,
                              }}
                            >
                              {value === 'recForm' &&
                              item['recForm'] &&
                              item['recForm'].length !== 0 ? (
                                <LineChart
                                  color={item['color']}
                                  data={item['recForm']}
                                />
                              ) : value !== 'recForm' && item[value] !== '' ? (
                                item[value]
                              ) : value === 'recForm' &&
                                item['recForm'] &&
                                item['recForm'].length === 0 ? (
                                '-'
                              ) : (
                                '-'
                              )}
                            </div>
                          ),
                        )}
                      </div>
                    ),
                  )}
                </div>
                {/* desktop */}
              </div>
            </div>
            <div className="hidden text-black dark:text-white   md:flex lg:flex flex-wrap overflow-scroll">
              <div
                className={`${
                  playerData.getPlayersProfileV2[ActiveTab.value].length < 4
                    ? 'w-4/12'
                    : 'w-1/5'
                }`}
              >
                {playerRoleFields[ActiveTab.value].label.map((x, i) => (
                  <div
                    key={i}
                    className={`border-b ${
                      i === 0
                        ? 'bg-light_gray dark:bg-gray-4'
                        : 'bg-white dark:bg-gray'
                    }
                    fw6 text-center  h-9  flex items-center justify-center f6 w-full`}
                  >
                    {x}
                  </div>
                ))}
              </div>

              <div
                className={`flex ${
                  playerData.getPlayersProfileV2[ActiveTab.value].length < 4
                    ? 'w-8/12'
                    : 'w-4/5'
                } flex-between`}
              >
                {playerData.getPlayersProfileV2[ActiveTab.value].map(
                  (item, j) => (
                    <div
                      key={j}
                      className={` ${
                        playerData.getPlayersProfileV2[ActiveTab.value].length <
                        4
                          ? 'w-2/5'
                          : 'w-1/5'
                      }`}
                    >
                      {playerRoleFields[ActiveTab.value].value.map(
                        (value, k) => (
                          <div
                            key={k}
                            className={`border-b ${
                              k === 0
                                ? 'bg-light_gray dark:bg-gray-4'
                                : 'bg-white dark:bg-gray'
                            }
                          h-9   flex items-center justify-center  w-full text-center f6`}
                          >
                            {value === 'recForm' &&
                            item['recForm'] &&
                            item['recForm'].length !== 0 ? (
                              <LineChart
                                color={item['color']}
                                data={item['recForm']}
                              />
                            ) : value !== 'recForm' && item[value] !== '' ? (
                              item[value]
                            ) : value === 'recForm' &&
                              item['recForm'] &&
                              item['recForm'].length === 0 ? (
                              '-'
                            ) : (
                              '-'
                            )}
                          </div>
                        ),
                      )}
                    </div>
                  ),
                )}
              </div>
            </div>
          </div>
        ) : (
          <div className="bg-white flex flex-column items-center justify-center fw5 text-centerpv4">
            <div className="w-8 h-8">
              <img className="h-100 w-full" src={empty} alt="" />
            </div>
            <div>Player Stats data not available</div>
          </div>
        )}
      </>
    )
}
