import React, { useState, useEffect } from 'react'
import differenceInYears from 'date-fns/differenceInYears'
import Link from 'next/link'
import CleverTap from 'clevertap-react'
import parse from 'html-react-parser'
import { useRouter } from 'next/navigation'
import { usePathname } from 'next/navigation'
import { getPlayerUrlBio, PlayerMatchView } from '../../api/services'
import SliderCommon from '../commom/slider'
//import SliderCommon from '../commom/slider';
import { PLAYER_VIEW_BIO, HEADER } from '../../constant/Links'
import { useQuery } from '@apollo/react-hooks'
import MetaDescriptor from '../MetaDescriptor'
// import Loading from '.';
import {
  PLAYER_CAREERSTATS_DETAILS_GQL,
  PLAYER_CAREER_STATS_DATA,
} from '../../api/queries'
import { PLAYER_COMPARE_VIEW } from '../../constant/Links'
import Imgix, { Picture, Source } from 'react-imgix'
import { Schedule } from '../../constant/MetaDescriptions'
import Tab from '../shared/Tab'

let whitebat = '/svgs/whitebat.svg'
let wicketKeeper = '/svgs/wicketKeeper.svg'
let bowler = '/svgs/bowling.svg'
let allRounder = '/svgs/allRounderWhite.svg'
let PlayerBackground = '/svgs/PlayerBackground.svg'
let backIconWhite = '/svgs/backIconWhite.svg'

let flagPlaceHolder = '/svgs/flag_empty.svg'
let Playerfallback = '../../public/pngsV2/playerph.png'
let background = '/pngsV2/player-role-tile-black.png'

export default function PlayerDetailsLayout({
  data,
  tab,
  playerID,
  playerSlug,
  ...props
}) {
  let PlayerTabs = [
    { name: 'Carreer Stats', value: 'career-stats' },
    { name: 'Recent', value: 'recent' },
    { name: 'Fantasy', value: 'fantasy' },
    { name: 'Articles', value: 'articles' },
  ]

  let router = useRouter()
  const asPath = usePathname()
  const [playerName, setPlayerName] = useState(asPath.split('/')[3])
  const [swiper, updateSwiper] = useState(null)
  const [selectedTab, setSelectedTab] = useState(
    PlayerTabs.find((ele) => asPath.split('/')[4] === ele.value),
  )
  const [currentIndex, updateCurrentIndex] = useState(window?.innerWidth > 700 ? 0 : 2)
  const { loading, error, data: playerData } = useQuery(
    PLAYER_CAREER_STATS_DATA,
    {
      variables: { playerID: playerID },
    },
  )
  let playerDetails = data && data.getPlayersProfileV2

  useEffect(() => {
    setPlayerName(asPath.split('/')[3])
    updateCurrentIndex(window?.innerWidth > 700 ? 0 : 2)
    // setSelectedTab(asPath.split('/')[4]);
  }, [])
  const handlePlayerBio = (playerID, playerName) => {
    return { as: eval(PLAYER_VIEW_BIO.as), href: PLAYER_VIEW_BIO.href }
  }

  const handleBioNav = (playerID, playerName) => {
    router.push(handlePlayerBio(playerID, playerName).as)
  }

  const handleNav = (item) => {
    setSelectedTab(item)
    router.push(PlayerMatchView(playerName, playerID, item.value).href)
  }
  const handlePlayerCompare = (playerID1, playerSlug, playerID2) => {
    return {
      as: eval(PLAYER_COMPARE_VIEW.as),
      href: eval(PLAYER_COMPARE_VIEW.href),
    }
  }

  const handleCompare = () => {
    console.log('curren index on compare - ',currentIndex);
    console.log('player list on compare - ',playerData.getPlayersProfileV2.similarplayers.filter(
      (id) => id.playerID !== playerID,
    ))
    CleverTap.initialize('PlayersCompare', {
      Source: 'Players',
      Platform: global.window.localStorage
        ? global.window.localStorage.Platform
        : '',
    })
    let playerID2 = playerData.getPlayersProfileV2.similarplayers.filter(
      (id) => id.playerID !== playerID,
    )[currentIndex === 0 && window.innerWidth < 700 ? 2 : currentIndex].playerID
    let playerID1 = playerID

    router.push(
      handlePlayerCompare(playerID1, playerSlug, playerID2).href,
      // handlePlayerCompare(playerID1, playerSlug, playerID2).as
    )
  }

  let date = new Date()

  //   const handleBack=()=>{
  //     return { as: eval(HEADER['players'].as), href: HEADER['players'].href };
  //   }
  // const handleBckPlayer=()=>{
  //   router.push(handleBack().href, handleBack().as);

  // }
  // if (error) return <div></div>;
  // else if (loading) {
  //    return (
  //       <Loading />
  //    );
  // }
  // else
  return (
    <>
      {/* <MetaDescriptor
        section={Schedule(
          data && data.miniScoreCard && data.miniScoreCard.data[0],
          router.asPath,
          status,
        )}
      /> */}
      <div className=" hidescroll text-black dark:text-white ">
        <div className="sticky top-0 z-40 bg-gray-8 md:hidden lg:hidden">
          <div className="flex items-center p-3 gap-2">
            <div className="bg-gray-4 p-2 rounded-md">
              <img
                className="flex items-center justify-center h-4 w-4 rotate-180"
                onClick={() => window.history.back()}
                src='/svgsV2/RightSchevronWhite.svg'
                alt="back icon"
              />{' '}
            </div>
            <div className="text-white  text-xl font-bold">
              {playerDetails && playerDetails.name}
            </div>
          </div>
          <div
            className=" mx-3 mt-1  rounded-md relative overflow-hidden shadow-1 "
            style={{
              backgroundImage: `url(${PlayerBackground})`,
              //   backgroundPosition: 'center',
              backgroundSize: 'cover',
            }}
          >
            <div
              className="absolute   left-0 right-0 w-full"
              style={{ bottom: -10 }}
            >
              <div className="absolute w-full flex flex-col  justify-center p-2 text-xs">
                <div className="" style={{ width: '37%' }}>
                  <div className="font-bold">Batting Style</div>
                  <div className=" truncate ">
                    {playerDetails && playerDetails.battingStyle}
                  </div>
                </div>
                <div className="mt-1 w-60">
                  <div className="font-bold">Bowling Style</div>
                  <div className="">
                    {playerDetails && playerDetails.bowlingStyle}
                  </div>
                </div>
              </div>

              <img className="w-full" src={background} />
            </div>

            <div className="flex flex-col ">
              <div className="flex flex-row mt-2">
                <div className="w-50  flex flex-col px-2 gap-2">
                  {/* <div className="h3 absolute bottom-0 right-0 left-0 bg-green z-1" style={{background: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5))"}}></div> */}
                  <div className="text-xl text-bold  font-bold">
                    {playerDetails && playerDetails.name}
                  </div>
                  <div className="flex  items-center gap-2">
                    <img
                      alt=""
                      src={`https://images.cricket.com/teams/${
                        playerDetails && playerDetails.teamID
                      }_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/svgs/images/flag_empty.svg')
                      }
                      className="h-4 w-6 shadow-4"
                    />
                    <div className=" text-sm font-semibold white uppercase">
                      {playerDetails && playerDetails.birthPlace}
                    </div>
                  </div>

                  <div className="flex  gap-2 ">
                    <div className="p-2 bg-gray-8 rounded-md">
                      <img
                        className=""
                        src={
                          playerDetails &&
                          playerDetails.role &&
                          (playerDetails.role === 'Batsman'
                            ? whitebat
                            : playerDetails.role === 'Bowler'
                            ? bowler
                            : playerDetails.role === 'Wicket Keeper'
                            ? wicketKeeper
                            : allRounder)
                        }
                        alt={
                          playerDetails &&
                          playerDetails.role &&
                          (playerDetails.role === 'Batsman'
                            ? whitebat
                            : playerDetails.role === 'Bowler'
                            ? bowler
                            : playerDetails.role === 'Wicket Keeper'
                            ? wicketKeeper
                            : allRounder)
                        }
                      />
                    </div>
                    <div className="white flex gap-2 items-center">
                      <span className="text-xs">
                        {playerDetails && playerDetails.dob
                          ? differenceInYears(
                              new Date(
                                `${date.getUTCFullYear()}`,
                                `${date.getMonth()}`,
                                `${date.getDay()}`,
                              ),
                              new Date(
                                ...playerDetails.dob.split('/').reverse(),
                              ),
                            ) + ' YEARS'
                          : '--'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="w-50 flex  justify-end ">
                  <div className=" overflow-hidden flex justify-center items-center object-cover">
                    {/* <img
                    className='  center pt-3 '
                    style={{ height: 250, width: 180 }}
                    src={playerDetails.headShotImage !=="" ? `${playerDetails.headShotImage}?fit=crop&crop=faces&w=180&h=250&auto=compress&q=30`:`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                    alt={Playerfallback}
                    onError={(evt) => (evt.target.src = Playerfallback)}
                  /> */}
                    {playerDetails?.headShotImage !== '' ? (
                      <Imgix
                        src={`${playerDetails.headShotImage}?fit=crop&crop=face`}
                        width={180}
                        height={250}
                      />
                    ) : (
                      <img
                        className="  center pt-3 "
                        style={{ height: 250, width: 180 }}
                        src={`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                        alt={Playerfallback}
                        onError={(evt) => (evt.target.src = Playerfallback)}
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='pb-2'>
          <Tab
                data={PlayerTabs}
                selectedTab={selectedTab}
                type="line"
                handleTabChange={(item) => handleNav(item)}
              /></div>
          <div className=" w-full ">
            <div className="flex justify-center  overflow-x-scroll z-999 hidescroll  w-full text-center">
             
              {/* {PlayerTabs.map((tabName, i) => {
              return (
                (<Link
                  key={i}
                  {...PlayerMatchView(playerName, playerID, tabName)}
                  replace
                  passHref
                  title={tabName}
                  onClick={() => {
                    setSelectedTab(tabName);
                  }}
                  className={` w-1/4 p-2 cursor-pointer nowrap  text-center text-xs font-semibold capitalize`}
                  style={{
                    paddingBottom: '.8rem',
                    paddingTop: '0.8rem',
                    color: tabName.toLowerCase() === selectedTab.toLowerCase() ? '#6A91A9' : null,
                    borderBottom:
                      tabName.toLowerCase() === selectedTab.toLowerCase() ? '2px solid #6A91A9' : null
                  }}>

                  <div
                    className={`flex-nowrap  text-center `}>
                    {tabName === 'career-stats' ? 'Career Stats' : tabName}
                  </div>

                </Link>)
              );
            })} */}
            </div>
          </div>
        </div>

        {/* desktop view */}

        <div className="hidden md:block lg:block ">
          <div
            className=" hidden md:block lg:block shadow-2 flex-l flex-m w-full flex-col   "
            style={{
              backgroundImage: `url(${PlayerBackground})`,
              backgroundSize: 'cover',
            }}
          >
            <div className="hidden md:block lg:block">
              <img
                className="w-2  p-2"
                onClick={() => window.history.back()}
                src="/svgs/backIconWhite.svg"
                alt="back icon"
              />
            </div>
            <div className="flex w-full pt-3 justify-center items-center text-white">
              <div className="w-50 flex  justify-end pt-5 ">
                <div className="w-40 pt-3 p-2">
                  <div className="text-xl font-bold ">
                    {playerDetails && playerDetails.name}
                  </div>
                  <div className="flex items-center gap-2 pt-3 ">
                    <img
                      className="w-5"
                      src={
                        playerDetails &&
                        (playerDetails.role === 'Batsman'
                          ? whitebat
                          : playerDetails.role === 'Bowler'
                          ? bowler
                          : playerDetails.role === 'Wicket Keeper'
                          ? wicketKeeper
                          : allRounder)
                      }
                      alt={
                        playerDetails &&
                        (playerDetails.role === 'Batsman'
                          ? whitebat
                          : playerDetails.role === 'Bowler'
                          ? bowler
                          : playerDetails.role === 'Wicket Keeper'
                          ? wicketKeeper
                          : allRounder)
                      }
                    />
                    <div className="text-white">
                      <span className="text-xs">
                        {playerDetails && playerDetails.dob
                          ? differenceInYears(
                              new Date(
                                `${date.getUTCFullYear()}`,
                                `${date.getMonth()}`,
                                `${date.getDay()}`,
                              ),
                              new Date(
                                ...playerDetails.dob.split('/').reverse(),
                              ),
                            ) + ' YEARS'
                          : '--'}
                      </span>
                    </div>
                  </div>
                  <div className="flex items-center gap-2 mt-4">
                    <img
                      alt=""
                      src={`https://images.cricket.com/teams/${
                        playerDetails && playerDetails.teamID
                      }_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/svgs/images/flag_empty.svg')
                      }
                      className="h-4 w-8 shadow-4"
                    />
                    <div className=" text-xs uppercase ">
                      {playerDetails && playerDetails.birthPlace}
                    </div>
                  </div>
                  <div className="pt-4">
                    <div className="text-sm">Batting Style</div>
                    <div className="text-xs mt-1 truncate ">
                      {playerDetails && playerDetails.battingStyle}
                    </div>
                  </div>
                  <div className="pt-4 ">
                    <div className="text-sm">Bowling Style</div>
                    <div className="text-xs mt-1 truncate ">
                      {playerDetails && playerDetails.bowlingStyle}
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-50 flex justify-start">
                <div className="  overflow-hidden flex  items-center object-cover ">
                  {/* <img
                  className='  center '
                  style={{ height: 320, width: 220 }}
                  src={playerDetails.headShotImage !=="" ? `${playerDetails.headShotImage}?fit=crop&crop=faces&w=220&h=320&auto=compress&q=30`:`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                  alt={Playerfallback}
                  onError={(evt) => (evt.target.src = Playerfallback)}
                /> */}
                  {/* <img
                  className='  center '
                  src={playerDetails.headShotImage !=="" ? `${playerDetails.headShotImage}?fit=crop&crop=faces&w=220&h=320`:`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                  alt={Playerfallback}
                  onError={(evt) => (evt.target.src = Playerfallback)}
                /> */}
                  {playerDetails.headShotImage !== '' ? (
                    <Imgix
                      src={`${playerDetails.headShotImage}?fit=crop&crop=face`}
                      width={220}
                      height={320}
                    />
                  ) : (
                    <img
                      className="  center "
                      style={{ height: 320, width: 220 }}
                      src={`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                      alt={Playerfallback}
                      onError={(evt) => (evt.target.src = Playerfallback)}
                    />
                  )}

                  {/* <picture>
                  <img srcSet={`${playerDetails.headShotImage}?fit=crop&crop=face&w=220&h=320&dpr=1 1x`,
                  `${playerDetails.headShotImage}?fit=crop&crop=face&w=220&h=320&dpr=2 2x`,
                  `${playerDetails.headShotImage}?fit=crop&crop=face&w=220&h=320&dpr=3 3x`
                }
                src={`${playerDetails.headShotImage}?w=220&h=320`}
                ></img>
                </picture> */}
                  {/* <img srcSet={}/> */}
                </div>
              </div>
            </div>
          </div>
          <div className=" shadow-4   ">
            <div className="flex mt-1 overflow-x-scroll z-999 hidescroll   dark:bg-gray text-xs font-semibold capitalize">
              <Tab
                data={PlayerTabs}
                selectedTab={selectedTab}
                type="line"
                handleTabChange={(item) => handleNav(item)}
              />

              {/* {PlayerTabs.map((tabName, i) => {
              return <>
                <Link
                  key={i}
                  {...PlayerMatchView(playerName, playerID, tabName)}
                  replace
                  passHref
                  title={tabName}
                  onClick={() => {
                    setSelectedTab(tabName);
                  }}
                  className={` ${tabName === 'career-stats' ? 'w-25' : 'w-25'
                    } cursor-pointer nowrap px-3 w-1/4 text-center `}
                  style={{
                    paddingBottom: '.8rem',
                    paddingTop: '0.8rem',

                    color: tabName.toLowerCase() === selectedTab.toLowerCase() ? '#6A91A9' : null,
                    borderBottom:
                      tabName.toLowerCase() === selectedTab.toLowerCase() ? '2px solid #6A91A9' : null
                  }}>

                  <div
                    className={`f6 flex-nowrap ttext-center`}>
                    {tabName === 'career-stats' ? 'Career Stats' : tabName}
                  </div>

                </Link>
              </>;
            })} */}
            </div>
          </div>
        </div>

        {props.children}

        {playerData &&
          playerData.getPlayersProfileV2 &&
          playerData.getPlayersProfileV2.similarplayers &&
          playerData.getPlayersProfileV2.similarplayers.filter(
            (id) => id.playerID !== playerID,
          ).length > 0 && (
            <div className="border mx-3 dark:border-none bg-white dark:bg-gray-4 text-black dark:text-white   md:mx-0 lg:mx-0 rounded-md">
              <div className="flex justify-between pt-1">
                <div className="fw6  f5-l f6  p-2">SIMILAR PLAYERS</div>
                <div className={` mx-2 mt-2`} onClick={() => handleCompare()}>
                  <span className="px-4 py-2 hover:opacity-70 transition ease-in duration-150 text-basered rounded-md dark:text-green-6 border border-basered dark:border-green-6 text-xs font-bold cursor-pointer">
                    COMPARE
                  </span>
                </div>
              </div>
              {/* <div className='bb b--black'>indian player</div> */}
              {/* mobile */}

              {/* desktop */}
              <div className="border-t bo  dark:border-none relative bg-white dark:bg-gray mt-2 p-1 ">
                <SliderCommon
                  data={playerData?.getPlayersProfileV2?.similarplayers || []}
                  event="players"
                  source="SimilarPlayers"
                  updateCurrentIndex = {updateCurrentIndex}
                  currentIndex = {currentIndex}
                  NavigateToPlayerProfile={(playerId) =>
                    NavigateToPlayerProfile(playerId, playerSlug)
                  }
                />

                {/* <div className='absolute absolute--fill flex justify-between items-center md:hidden lg:hidden'>
                      <div id={`pbutton`} className='white cursor-pointer  outline-0  mt4 hidden md:block lg:block'>
                        <svg width='44' focusable='false' viewBox='0 0 24 24'>
                          <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                          <path fill='none' d='M0 0h24v24H0z'></path>
                        </svg>
                      </div>
                      <div id={`nbutton`} className='white cursor-pointer md:hidden lg:hidden outline-0  mt4'>
                        <svg width='44' viewBox='0 0 24 24'>
                          <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                          <path fill='none' d='M0 0h24v24H0z'></path>
                        </svg>
                      </div>
                    </div> */}
              </div>
            </div>
          )}
        {playerDetails && playerDetails.description && (
          <div className="border mx-3  dark:border-none bg-white dark:bg-gray-4 text-black dark:text-white mt-4  md:mx-0 lg:mx-0 rounded-md">
            <div className="text-xs font-bold p-2 ">PLAYER BIO</div>
            <div className="dark:bg-gray mt-1 dark:rounded-b-md  ">
              <div className="text-xs  p-2 ">About</div>
              <div className="text-ellipsis overflow-hidden text-xs p-2  text-gray-2">
                {parse(
                  playerDetails && playerDetails.description !== null
                    ? playerDetails.description.slice(0, 350)
                    : '--',
                )}
              </div>
              <div
                // players/4912/kemar-andre-jamal-roach/undefined/playerbio
                className=" text-xs font-bold text-right p-2 cursor-pointer"
                onClick={() => handleBioNav(playerID, playerName)}
              >
                {playerDetails && playerDetails.description !== null
                  ? 'More...'
                  : ' '}
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  )
}
