import React, { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'

// import { PLAYER_DISCOVERY_V2 } from './../api/queries';
// import { useQuery, useMutation, useLazyQuery } from '@apollo/react-hooks';
// import PlayerAlternate from './PlayersDetails/PlayerAlternate';
// import { LazyLoadImageComponent } from './Common/LazyImageLoader';
import { PLAYER_VIEW } from '../../constant/Links'
import Loading from '../../components/loading'
import { useRouter } from 'next/navigation'
import CleverTap from 'clevertap-react'

const PlayerAlternate = dynamic(
  () => import('../../components/playersDetails/playerAlternate'),
  {
    loading: () => <Loading />,
    ssr: false,
  },
)
export default function playerDiscoveryGallery({
  TeamTab,
  Page,
  screenWidth,
  players,
}) {
  let router = useRouter()
  const playerChunk = players

  const [pagination, setPagination] = useState(1)

  const NavigateToPlayerProfile = (playerId, playerSlug) => {
    return { as: eval(PLAYER_VIEW.as), href: eval(PLAYER_VIEW.href)};
  };

  const goToPlayerProfile = (name, playerID) => {
    CleverTap.initialize('Players', {
      Source: 'PlayerDiscovery',
      PlayerID: playerID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
    let playerId = playerID;
    let playerSlug = `${name && name.split(' ').join('-').toLowerCase()}`;

    router.push(NavigateToPlayerProfile(playerId, playerSlug).href);
  };

  //   if (error) return <div></div>;
  //   else if (loading) {
  //     return <Loading />;
  //   } else
  return (
    <div className="w-full  overflow-scroll flex justify-center">
      <div className="md:hidden lg:hidden pv2">
        {playerChunk &&
          playerChunk.length > 0 &&
          playerChunk.map((rows, i) => (
            <div key={i} className="flex flex-column ">
              {i % 2 === 0 ? (
                <div>
                  <div
                    className=" flex flex-row flex-wrap "
                    style={{ width: '100%' }}
                  >
                    {rows.slice(0, 1).map((player) => (
                      <div
                        key={player.ID}
                        onClick={() => {
                          goToPlayerProfile(player.PN, player.ID)
                        }}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          actionShot={false}
                          ScreenWidth={screenWidth * 0.46}
                          ScreenHeight={screenWidth * 0.46}
                          marginRight={4}
                          animate={true}
                          marginTop={4}
                          MediumDeviceHeight={'h45'}
                          BackgroundHeight={'10.5rem'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          url={
                            player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.46
                                }&h=${screenWidth * 0.46}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                    <div
                      className=" flex flex-row-l justify-between  flex-wrap  "
                      style={{
                        width: screenWidth * 0.46,
                        height: screenWidth * 0.46,
                        marginRight: 4,
                        marginTop: 4,
                        overflow: 'hidden',
                      }}
                    >
                      {rows.slice(1, 4).map((player, j) => (
                        <div
                          className=""
                          key={player.ID}
                          onClick={() =>
                            goToPlayerProfile(player.PN, player.ID)
                          }
                        >
                          <PlayerAlternate
                            flag={player.TI}
                            skills={player.PS}
                            ScreenWidth={
                              j == 0 ? screenWidth * 0.46 : screenWidth * 0.225
                            }
                            animate={false}
                            actionShot={j === 0 ? true : false}
                            ScreenHeight={screenWidth * 0.225}
                            marginRight={0}
                            MediumDeviceHeight={'h-full'}
                            marginTop={j === 1 || j === 2 ? 4 : 0}
                            BackgroundHeight={'4.5rem'}
                            PlayerPositionTop={0}
                            key={player.ID}
                            player={player}
                            url={
                              j === 0
                                ? player.ASI
                                  ? `${player.ASI}?fit=clamp&crop=entropy&w=${
                                      screenWidth * 0.46
                                    }&h=${
                                      screenWidth * 0.225
                                    }&auto=compress&dpr=2`
                                  : `https://images.cricket.com/players/${player.ID}_actionshot_safari.jpg`
                                : player.HSI
                                ? `${player.HSI}?fit=crop&crop=faces&w=${
                                    screenWidth * 0.225
                                  }&h=${
                                    screenWidth * 0.225
                                  }&auto=compress&dpr=2`
                                : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                            }

                            // url={`${j === 0 ? player.ASI : player.HSI}${j === 0 ? '?fit=clamp&crop=entropy' : '?fit=crop&crop=faces'}&w=${j == 0 ? screenWidth * 0.46 : screenWidth * 0.225}&h=${screenWidth * 0.225}&auto=compress&q=30`}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                  <div
                    className=" flex flex-row flex-wrap"
                    style={{ width: '100%' }}
                  >
                    {rows.slice(4, 8).map((player, j) => (
                      <div
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={screenWidth * 0.225}
                          animate={false}
                          ScreenHeight={screenWidth * 0.225}
                          marginRight={4}
                          actionShot={false}
                          marginTop={4}
                          MediumDeviceHeight={'h-full'}
                          BackgroundHeight={'4.5rem'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          url={
                            player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.225
                                }&h=${screenWidth * 0.225}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                  </div>
                </div>
              ) : (
                <div>
                  <div
                    className=" flex flex-row flex-wrap"
                    style={{ width: '100%' }}
                  >
                    <div
                      className=" flex flex-row justify-between  flex-wrap  "
                      style={{
                        width: screenWidth * 0.46,
                        height: screenWidth * 0.46,
                        marginRight: 4,
                        marginTop: 4,
                        overflow: 'hidden',
                      }}
                    >
                      {rows.slice(0, 3).map((player, j) => (
                        <div
                          className=""
                          key={player.ID}
                          onClick={() =>
                            goToPlayerProfile(player.PN, player.ID)
                          }
                        >
                          <PlayerAlternate
                            flag={player.TI}
                            skills={player.PS}
                            ScreenWidth={
                              j == 0 ? screenWidth * 0.46 : screenWidth * 0.225
                            }
                            animate={false}
                            ScreenHeight={screenWidth * 0.225}
                            marginRight={0}
                            actionShot={j === 2 ? true : false}
                            MediumDeviceHeight={'h-full'}
                            marginTop={j === 2 ? 4 : 0}
                            BackgroundHeight={'4.5rem'}
                            PlayerPositionTop={0}
                            key={player.ID}
                            player={player}
                            // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                            // url={`${j === 2 ? player.ASI : player.HSI}${
                            //   j === 0 ? '?fit=clamp&crop=entropy' : '?fit=crop&crop=faces'
                            // }&w=${j == 2 ? screenWidth * 0.46 : screenWidth * 0.225}&h=${
                            //   screenWidth * 0.225
                            // }&auto=compress&q=30`}

                            url={
                              j === 0
                                ? player.ASI
                                  ? `${player.ASI}?fit=clamp&crop=entropy&w=${
                                      screenWidth * 0.46
                                    }&h=${
                                      screenWidth * 0.225
                                    }&auto=compress&dpr=2`
                                  : `https://images.cricket.com/players/${player.ID}_actionshot_safari.jpg`
                                : player.HSI
                                ? `${player.HSI}?fit=crop&crop=faces&w=${
                                    screenWidth * 0.225
                                  }&h=${
                                    screenWidth * 0.225
                                  }&auto=compress&dpr=2`
                                : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                            }
                          />
                        </div>
                      ))}
                    </div>

                    {rows.slice(3, 4).map((player, j) => (
                      <div
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={screenWidth * 0.46}
                          ScreenHeight={screenWidth * 0.46}
                          marginRight={4}
                          animate={true}
                          actionShot={false}
                          marginTop={4}
                          MediumDeviceHeight={'h45'}
                          BackgroundHeight={'10.5rem'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          url={
                            player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.46
                                }&h=${screenWidth * 0.46}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />{' '}
                      </div>
                    ))}
                  </div>

                  <div
                    className=" flex flex-row flex-wrap"
                    style={{ width: '100%' }}
                  >
                    {rows.slice(4, 8).map((player, j) => (
                      <div
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={screenWidth * 0.225}
                          animate={false}
                          ScreenHeight={screenWidth * 0.225}
                          marginRight={4}
                          MediumDeviceHeight={'h-full'}
                          marginTop={4}
                          actionShot={false}
                          BackgroundHeight={'4.5rem'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          url={
                            player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.225
                                }&h=${screenWidth * 0.225}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>
          ))}
      </div>

      {/* dn db-l  */}

      <div className="hidden md:block lg:block  pv-4 ">
        {playerChunk &&
          playerChunk.length > 0 &&
          playerChunk.map((rows, i) => (
            <div key={i}>
              {i % 2 === 0 ? (
                <div
                  className=" w-full  flex flex-row  flex-wrap items-center  justify-center"
                  style={{ width: '100%' }}
                >
                  {rows.slice(0, 1).map((player, j) => (
                    <div
                      className=""
                      key={player.ID}
                      onClick={() => goToPlayerProfile(player.PN, player.ID)}
                    >
                      <PlayerAlternate
                        flag={player.TI}
                        skills={player.PS}
                        ScreenWidth={screenWidth * 0.4}
                        ScreenHeight={screenWidth * 0.3}
                        marginRight={4}
                        actionShot={false}
                        animate={true}
                        MediumDeviceHeight={'h-full'}
                        marginTop={4}
                        BackgroundHeight={'10.5rem'}
                        PlayerPositionTop={0}
                        key={player.ID}
                        player={player}
                        // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                        // url={`https://cdcdev.imgix.net/Williamson.JPG?fit=crop&crop=faces&w=${screenWidth * 0.3}&h=${screenWidth * 0.3}&auto=compress&q=30`}
                        url={
                          player.HSI
                            ? `${player.HSI}?fit=crop&crop=faces&w=${
                                screenWidth * 0.3
                              }&h=${screenWidth * 0.3}&auto=compress&dpr=2`
                            : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                        }
                        // url={` https://cdcdev.imgix.net/bhuvneshwar-kumar-1627891755088?fit=crop&crop=faces&w=${screenWidth * 0.3}&h=${screenWidth * 0.3}&auto=compress&q=30&q=30`}
                      />
                    </div>
                  ))}

                  <div
                    className=" flex flex-row justify-between  flex-wrap   "
                    style={{
                      width: screenWidth * 0.3,
                      height: screenWidth * 0.3,
                      marginRight: 4,
                      marginTop: 4,
                      overflow: 'hidden',
                    }}
                  >
                    {rows.slice(1, 4).map((player, j) => (
                      <div
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={
                            j === 0 ? screenWidth * 0.3 : screenWidth * 0.148
                          }
                          animate={false}
                          ScreenHeight={screenWidth * 0.148}
                          marginRight={0}
                          MediumDeviceHeight={'h-full'}
                          marginTop={j === 1 || j === 2 ? 4 : 0}
                          actionShot={j === 0 ? true : false}
                          BackgroundHeight={'4.5rem'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://cdcdev.imgix.net/mm.jpg?&fit=clamp&w=${j === 0 ? screenWidth * 0.3 : screenWidth * 0.148}&h=${screenWidth * 0.148}&auto=compress&q=30`}
                          // url={`${j === 0 ? player.ASI : player.HSI}${
                          //   j === 0 ? '?fit=clamp&crop=entropy' : '?fit=crop&crop=faces'
                          // }&w=${j === 0 ? screenWidth * 0.3 : screenWidth * 0.148}&h=${
                          //   screenWidth * 0.148
                          // }&auto=compress&q=30`}

                          url={
                            j === 0
                              ? player.ASI
                                ? `${player.ASI}?fit=clamp&crop=entropy&w=${
                                    screenWidth * 0.3
                                  }&h=${
                                    screenWidth * 0.148
                                  }&auto=compress&dpr=2`
                                : `https://images.cricket.com/players/${player.ID}_actionshot_safari.jpg`
                              : player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.148
                                }&h=${screenWidth * 0.148}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                  </div>
                  <div
                    className=" flex flex-row   items-center justify-between flex-wrap  "
                    style={{
                      width: screenWidth * 0.3,
                      height: screenWidth * 0.3,
                      marginRight: 4,
                      marginTop: 4,
                      overflow: 'hidden',
                    }}
                  >
                    {rows.slice(4, 8).map((player, j) => (
                      <div
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={screenWidth * 0.148}
                          animate={false}
                          ScreenHeight={screenWidth * 0.148}
                          actionShot={false}
                          marginRight={0}
                          marginTop={j === 2 || j === 3 ? 4 : 0}
                          BackgroundHeight={'4.5rem'}
                          MediumDeviceHeight={'h-full'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          url={
                            player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.148
                                }&h=${screenWidth * 0.148}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                  </div>
                </div>
              ) : (
                <div
                  className=" flex flex-row flex-wrap  items-center justify-center "
                  style={{ width: '100%' }}
                >
                  <div
                    className=" flex flex-row justify-between  flex-wrap  "
                    style={{
                      width: screenWidth * 0.3,
                      height: screenWidth * 0.3,
                      marginRight: 4,
                      marginTop: 4,
                      overflow: 'hidden',
                    }}
                  >
                    {rows.slice(0, 3).map((player, j) => (
                      <div
                        className=""
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={
                            j === 0 ? screenWidth * 0.3 : screenWidth * 0.148
                          }
                          animate={false}
                          actionShot={j === 0 ? true : false}
                          ScreenHeight={screenWidth * 0.148}
                          marginRight={0}
                          MediumDeviceHeight={'h-full'}
                          marginTop={j === 1 || j === 2 ? 4 : 0}
                          BackgroundHeight={'4.5rem'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          // url={`${j === 0 ? player.ASI : player.HSI}${
                          //   j === 0 ? '?fit=clamp&crop=entropy' : '?fit=crop&crop=faces'
                          // }&w=${j === 0 ? screenWidth * 0.3 : screenWidth * 0.148}&h=${
                          //   screenWidth * 0.148
                          // }&auto=compress&q=30`}

                          url={
                            j === 0
                              ? player.ASI
                                ? `${player.ASI}?fit=clamp&crop=entropy&w=${
                                    screenWidth * 0.3
                                  }&h=${
                                    screenWidth * 0.148
                                  }&auto=compress&dpr=2`
                                : `https://images.cricket.com/players/${player.ID}_actionshot_safari.jpg`
                              : player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.148
                                }&h=${screenWidth * 0.148}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                  </div>
                  <div
                    className=" flex flex-row items-center justify-between flex-wrap "
                    style={{
                      width: screenWidth * 0.3,
                      height: screenWidth * 0.3,
                      marginRight: 4,
                      marginTop: 4,
                      overflow: 'hidden',
                    }}
                  >
                    {rows.slice(3, 7).map((player, j) => (
                      <div
                        key={player.ID}
                        onClick={() => goToPlayerProfile(player.PN, player.ID)}
                      >
                        <PlayerAlternate
                          flag={player.TI}
                          skills={player.PS}
                          ScreenWidth={screenWidth * 0.148}
                          actionShot={false}
                          animate={false}
                          ScreenHeight={screenWidth * 0.148}
                          marginRight={0}
                          marginTop={j === 2 || j === 3 ? 4 : 0}
                          BackgroundHeight={'4.5rem'}
                          MediumDeviceHeight={'h-full'}
                          PlayerPositionTop={0}
                          key={player.ID}
                          player={player}
                          // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                          url={
                            player.HSI
                              ? `${player.HSI}?fit=crop&crop=faces&w=${
                                  screenWidth * 0.148
                                }&h=${screenWidth * 0.148}&auto=compress&dpr=2`
                              : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                          }
                        />
                      </div>
                    ))}
                  </div>

                  {rows.slice(7, 8).map((player, j) => (
                    <div
                      key={player.ID}
                      onClick={() => goToPlayerProfile(player.PN, player.ID)}
                    >
                      <PlayerAlternate
                        flag={player.TI}
                        skills={player.PS}
                        actionShot={false}
                        ScreenWidth={screenWidth * 0.4}
                        ScreenHeight={screenWidth * 0.3}
                        marginRight={4}
                        animate={true}
                        MediumDeviceHeight={'h-full'}
                        marginTop={4}
                        BackgroundHeight={'10.5rem'}
                        PlayerPositionTop={0}
                        key={player.ID}
                        player={player}
                        // url={`https://images.cricket.com/players/${player.ID}_headshot_safari.png`}
                        url={
                          player.HSI
                            ? `${player.HSI}?fit=crop&crop=faces&w=${
                                screenWidth * 0.3
                              }&h=${screenWidth * 0.3}&auto=compress&dpr=2`
                            : `https://images.cricket.com/players/${player.ID}_headshot_safari.png`
                        }
                      />
                    </div>
                  ))}
                </div>
              )}
            </div>
          ))}
      </div>
    </div>
  )
}
