import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { PLAYER_FANTASY_MATCH } from '../../api/queries';
import Loading from '../loading';
const empty = '/svgs/Empty.svg';
import DataNotFound from '../commom/datanotfound';
import { usePathname } from 'next/navigation';
import Tab from '../shared/Tab';
const category = [{name:'ALL', value:'all'}, {name:'TEST', value:'test'},{name:'ODI', value:'odi'},{name:'T20I', value:'t20'},{name:'T20s', value:'t20Domestic'}];
export default function PlayerFantasyTab({ playerID }) {
  const asPath = usePathname();
  const [format, setFormat] = useState(category[0]);
  const [playerid, setPlayerid] = useState(asPath.split('/')[2]);

  useEffect(() => {
    let playerid = asPath.split('/')[2];
    setPlayerid(playerid);
  }, []);

  const {
    loading,
    error,
    data: playerFantasyData
  } = useQuery(PLAYER_FANTASY_MATCH, {
    variables: { playerID: playerID },
    onCompleted: (data) => {}
  });


  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return (
      <>
        <div className='w-full pb-4 px-3 md:px-0 lg:px-0 '>
          <Tab data={category} selectedTab={format} type='block' handleTabChange={(item) => setFormat(item)}/>
          {/* {category.map((y, i) => (
            <div
              key={i}
              className={`w-14 h-8 p-1 text-xs font-bold  ${
                format === y ? 'border-2 border-green-6 text-green-6 bg-gray-8' : 'white  bg-gray'
              } text-center cursor-pointer flex items-center justify-center rounded-full cursor-pointer`}
              onClick={() => {
                setFormat(y);
              }}>
              {y === 'test'
                ? 'Test'
                : y === 'odi'
                ? 'ODI'
                : y === 't20'
                ? 'T20I'
                : y === 't20Domestic'
                ? 'T20s'
                : 'ALL'}
            </div>
          ))} */}
        
        <div className=' w-full mt-4'>
          {
            <div className='flex w-full h-10 item-center justify-between text-sm font-normal p-2 bg-light_gray dark:bg-gray-4  lh-copy  '>
              <div className=' w-3/12 p-2 flex justify-start items-center  '>Opposition</div>
              <div className='w-3/12 p-2 flex items-center justify-center '>Batting</div>
              <div className='w-3/12 p-2 flex items-center justify-center '>Bowling</div>
              <div className='w-2/12 p-2 flex items-center justify-center '>Points</div>
              <div className='w-1/12 p-2 flex items-center justify-center '>DTA</div>
            </div>
          }

          {playerFantasyData &&
          playerFantasyData.getPlayerFantasyMatches &&
          playerFantasyData &&
          playerFantasyData.getPlayerFantasyMatches[format.value].length > 0 ? (
            playerFantasyData.getPlayerFantasyMatches[format.value].map((fantasy, i) => (
              <div
                key={i}
                className={`flex bg-white dark:bg-gray  w-full bb b--black p-2 items-center border-b  text-xs ${i === playerFantasyData.getPlayerFantasyMatches[format.value].length-1 ? ' border-none':''} `}>
                {
                  <div className='w-3/12 p-2 flex items-center justify-start text-xs gap-1 truncate '>
                    <span className=''>{fantasy.opposition.split('vs')[0]} vs</span>
                    <span className=''>{fantasy.opposition.split('vs')[1]}</span>
                  </div>
                }
                {
                  <div className='w-3/12 text-center '>
                    <div className='fw4 '>
                      {fantasy.battingStats.split('&')[0]}
                      {fantasy.battingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.battingStats.split('&')[1]}</div>
                  </div>
                }
                {
                  <div className='w-3/12  text-center fw4'>
                    <div>
                      {fantasy.bowlingStats.split('&')[0]}
                      {fantasy.bowlingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.bowlingStats.split('&')[1]}</div>
                  </div>
                }
                {<div className='w-2/12   flex items-center justify-center  fw6'>{fantasy.points}</div>}
                {
                  <div className='w-1/12   flex items-center justify-center'>
                    <div className={`${fantasy.isDreamTeam ? 'bg-green' : 'bg-red'}  rounded-md p-1`}></div>
                  </div>
                }
              </div>
            ))
          ) : (
            <DataNotFound/>
          )}
        </div>
        </div>
      </>
    );
}
