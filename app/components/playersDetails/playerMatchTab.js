import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { usePathname } from 'next/navigation';
import Loading from '../loading';
import { format } from 'date-fns';
import { PLAYER_MATCH } from '../../api/queries';
import DataNotFound  from '../commom/datanotfound';
import Tab from '../shared/Tab';
const empty = '/svgs/Empty.svg';
const category = [{name:'ALL', value:'all'}, {name:'TEST', value:'test'},{name:'ODI', value:'odi'},{name:'T20I', value:'t20'},{name:'T20s', value:'t20Domestic'}];

export default function PlayerMatchTab({ playerID }) {

  const asPath = usePathname();
  const [types, setTypes] = useState(category[0]);
  const [playerid, setPlayerid] = useState(asPath.split('/')[2]);

  useEffect(() => {
    let playerid = asPath.split('/')[2];
    setPlayerid(playerid);
  }, []);

  const {
    loading,
    error,
    data: playerMatchData
  } = useQuery(PLAYER_MATCH, {
    variables: { playerID: playerID },
    onCompleted: (data) => {
      // console.log("dat match", data)
    }
  });
  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return (
      <div className='w-full px-3 md:px-0 lg:px-0 pb-4'>
        <div className='w-full pb-4 '>
        <Tab data={category} selectedTab={types} type='block' handleTabChange={(item) => setTypes(item)}/>
        </div>
        {/* <div className='flex w-min  my-2 center p-1 dark:bg-gray border border-gray-2 dark:border-none  items-center justify-between mx-auto rounded-full  md:gap-8'>
          {category.map((y, i) => (
            <div
              key={i}
              className={`w-14 h-8 p-1  ${
                types === y ? 'border-2 border-green-6 text-green-6 bg-gray-8' : 'text-white '
              } text-xs font-bold cursor-pointer  text-center  flex items-center justify-center rounded-full cursor-pointer`}
              onClick={() => {
                setTypes(y);
              }}>
              {y === 'test'
                ? 'Test'
                : y === 'odi'
                ? 'ODI'
                : y === 't20'
                ? 'T20I'
                : y === 't20Domestic'
                ? 'T20s'
                : 'ALL'}
            </div>
          ))}
        </div> */}
        <div className='  bg-light_gray dark:bg-gray  text-black dark:text-white '>
          {
            <div className='flex w-full item-center justify-center  dark:bg-gray-4 text-sm py-3 font-normal     '>
              <div className={`${types.value === 'all' ? 'w-1/4' : 'w-1/3'} justify-center p-1 flex items-center `}>
                Opposition
              </div>
              <div className={`${types.value === 'all' ? 'w-1/5' : 'w-1/3'}  justify-center p-1 flex items-center `}>
                Batting
              </div>
              <div className={`${types.value === 'all' ? 'w-1/5' : 'w-1/3'} justify-center p-1  flex items-center`}>Bowling</div>
              {types.value === 'all' && <div className={`w-1/6  justify-center flex items-center  `}>Format</div>}
              {<div className={`w-1/5  flex items-center justify-center `}>Date</div>}
            </div>
          }

          {playerMatchData &&
          playerMatchData.getPlayerMatches &&
          playerMatchData.getPlayerMatches[types.value] &&
          playerMatchData.getPlayerMatches[types.value].length > 0 ? (
            playerMatchData.getPlayerMatches[types.value].map((fantasy, i) => (
              <div
                key={i}
                className={`flex w-full bg-white border-b dark:border-black dark:bg-gray  item-center justify-center py-2 text-xs ${i === playerMatchData.getPlayerMatches[types.value].length-1 ? '':''}`}>
                {fantasy.opposition !== '' ? (
                  <div className={`${types.value === 'all' ? 'w-1/4' : 'w-1/3'}  flex p-2 items-center justify-center`}>
                    <div className='fw4 f8 f7-l f7-m'>{fantasy.opposition.split('vs')[0]} vs</div>
                    <div className='fw6 pl1'>{fantasy.opposition.split('vs')[1]}</div>
                  </div>
                ) : (
                  <div className={`${types.value === 'all' ? 'w-1/4' : 'w-1/3'}  flex justify-center items-center p-2`}>--</div>
                )}
                {fantasy.battingStats !== '' ? (
                  <div
                    className={`${types.value === 'all' ? 'w-1/5' : 'w-1/3'} flex  items-center justify-center `}>
                    <div className='fw4 '>
                      {fantasy.battingStats.split('&')[0]}
                      {fantasy.battingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.battingStats.split('&')[1]}</div>
                  </div>
                ) : (
                  <div className={`${types.value === 'all' ? 'w-1/5' : 'w-1/3'} justify-center items-center flex  `}>
                    <span className='fw4 '>--</span>
                  </div>
                )}
                {fantasy.bowlingStats !== '' ? (
                  <div
                    className={`${
                      types.value === 'all' ? 'w-1/5' : 'w-1/3'
                    }  flex flex-col items-center justify-center `}>
                    <div className='fw4 '>
                      {fantasy.bowlingStats.split('&')[0]}
                      {fantasy.bowlingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.bowlingStats.split('&')[1]}</div>
                  </div>
                ) : (
                  <div className={`${types.value === 'all' ? 'w-1/5' : 'w-1/3'} justify-center  flex  items-center  `}>--</div>
                )}
                {types.value === 'all' && (
                  <div className={`w-1/6 flex items-center justify-center   `}>
                    {fantasy.matchType !== '' ? fantasy.matchType : '--'}
                  </div>
                )}
                {fantasy.matchDate !== '' ? (
                  <div className={` w-1/5 justify-center flex items-center  `}>
                    {format(parseInt(fantasy.matchDate), 'dd-MMM-yyyy')}
                  </div>
                ) : (
                  <div className={` w-1/5 flex items-center justify-center   `}>--</div>
                )}
              </div>
            ))
          ) : (
           <DataNotFound/>
          )}
        </div>
      </div>
    );
}
