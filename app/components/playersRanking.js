'use client'
import React, { useState } from 'react'
import { GET_ALL_RANKINGS } from '../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import DataNotFound from './commom/datanotfound'
import Link from 'next/link'
import { getPlayerUrl } from '../api/services'
const playerfallback = '/pngsV2/playerph.png'
const teamFallback = '/svgs/images/flag_empty.png'

const PlayerLink = ({ playerID, playerName }) => {
  return (
    <>
      {typeof playerName === 'string' && (
        <Link
          {...getPlayerUrl({ id: playerID, playerName: playerName || '' })}
          passHref
          legacyBehavior
        >
          <h5 class="text-sm font-normal md:cursor-pointer hover:text-blue hover:underline">
            {playerName}
          </h5>
        </Link>
      )}
    </>
  )
}
const TeamBadge = ({ teamID, teamName }) => {
  return (
    <div className="flex flex-row items-center text-xs text-inherit dark:text-white font-medium text-left py-4 ml-5 rounded-xl">
      {/* <span className="flex-row"> */}
      <div className="h-4 w-6">
        <img
          alt="teamFlag"
          style={{ height: '100%', width: '100%' }}
          className="shadow-xl"
          src={`https://images.cricket.com/teams/${teamID}_flag_safari.png`}
          onError={(e) => {
            e.target.src = teamFallback
          }}
        />
      </div>
      <div className="ml-1 w-24 truncate">{teamName.toUpperCase()}</div>
      {/* </span> */}
    </div>
  )
}

const DisplayComponents = {
  TeamBadge,
  PlayerLink,
}

const RenderCell = (props) => {
  const Cmp = React?.useMemo(() => {
    return DisplayComponents[props?.displayType]
  }, [props?.displayType])
  if (Cmp) {
    return <Cmp {...props} />
  }
  return <p>Invalid Render</p>
}

const Table = ({ headers, data, category, className = '' }) => {
  if (!data?.length) {
    return <></>
  }
  return (
    <div class="flex flex-col">
      <div class="overflow-x-auto sm:mx-0.5 lg:mx-0.5">
        <div class="py-2 inline-block min-w-full">
          <div class="overflow-hidden">
            {}
            {data && (
              <PlayerCard
                category={category}
                playerName={data[0][1]?.playerName}
                playerRank={data[0][0]}
                playerTeam={data[0][2]}
                playerRating={data[0][3]}
                playerID={data[0][4]}
              />
            )}
            <table class="min-w-full border-2 border-solid rounded-xl border-gray-11">
              <thead class="bg-gray-200 border-b">
                <tr>
                  {headers?.map((column) => {
                    return (
                      <th
                        scope="col"
                        class={`text-xs font-medium text-gray-900 ${
                          column == 'TEAM' ? 'px-8' : 'px-3'
                        } py-4 text-left`}
                      >
                        {column}
                      </th>
                    )
                  })}
                </tr>
              </thead>
              <tbody>
                {}
                {data?.slice(1)?.map((rowData) => {
                  return (
                    <tr class="bg-white border-b px-2 text-center transition duration-300 ease-in-out hover:bg-gray-100">
                      {rowData.map((column, index) => {
                        if (index > headers.length - 1) return
                        if (typeof column === 'object') {
                          return (
                            <td class="text-xs text-center py-1 font-medium text-gray-900 hover:underline-offset-4">
                              <RenderCell {...column} />
                            </td>
                          )
                        }
                        return (
                          <td class="text-sm text-center font-medium text-gray-900">
                            {column}
                          </td>
                        )
                      })}
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

const PlayerCard = ({
  playerID,
  playerName,
  playerTeam,
  playerRank,
  playerRating,
  category = '',
}) => {
  return (
    <div className="bg-[#B1252B]">
      <div className="text-white flex py-0.5 px-1 items-center justify-start border-2 border-solid border-basered rounded-tl-md rounded-tr-md">
        {category}
      </div>

      <div className="flex justify-around items-center bg-basered border-basered md:flex-row md:max-w-xl dark:border-gray-700 dark:bg-gray-800 text-white dark:hover:bg-gray-700 pb-2">
        <div className="flex flex-col">
          <div class="flex flex-row-reverse justify-evenly gap-2 py-2">
            <Link
              {...getPlayerUrl({
                id: playerID,
                playerName: playerName || '',
              })}
              passHref
              legacyBehavior
            >
              <h5 class="text-xl font-normal md:cursor-pointer mt-0.5">
                {playerName}
              </h5>
            </Link>
            <p class="font-bold text-2xl">{playerRank}</p>
          </div>
          <div class="flex flex-row-reverse justify-around items-center py-2">
            <h5 class="text-xl font-semibold tracking-tight">{playerRating}</h5>
            <p class="font-normal text-white">
              <DisplayComponents.TeamBadge
                teamID={playerTeam?.teamID}
                teamName={playerTeam?.teamName}
              />
            </p>
          </div>
        </div>
        <img
          key={playerID}
          className="object-cover object-top dark:w-full dark:h-full h-44 w-24"
          src={`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
          onError={(e) => {
            e.target.src = playerfallback
            e.target.className =
              'object-contain object-center dark:w-full dark:h-full h-44 w-24 self-center'
          }}
          alt=""
        />
      </div>
    </div>
  )
}

export default function PlayersRanking({ gender = 'men', format = 'Test' }) {
  const { data: rankingData } = useQuery(GET_ALL_RANKINGS)
  const tableHeader = ['RANK', 'PLAYER', 'TEAM', 'RATING']

  if (rankingData) {
    const playerRankings = rankingData?.getPlayerRankings?.[format][gender]

    return (
      <div className="flex gap-1">
        {typeOfPlayers?.map((type, index) => {
          const data = playerRankings[type?.key]?.rank?.map((player) => [
            player?.position,
            {
              displayType: 'PlayerLink',
              playerID: player?.playerID,
              playerName: player?.playerName || '',
            },
            // <TeamBadge  teamID={player?.teamID} teamName={player?.teamName} />,
            {
              displayType: 'TeamBadge',
              teamID: player?.teamID,
              teamName: player?.teamName,
            },
            player?.Points,
            player?.playerID,
          ])
          if (!data?.length) {
            return <></>
          }
          return (
            <div className="flex flex-col">
              <Table
                className=""
                headers={tableHeader}
                category={type?.label}
                data={data}
                // data={playerRankings[type?.label]?.rank}
              />
            </div>
          )
        })}
      </div>
    )
  }
  if (!rankingData) {
    return <DataNotFound />
  }

  return <></>
}

const typeOfPlayers = [
  {
    label: 'Batter',
    key: 'bat',
  },
  {
    label: 'Bowler',
    key: 'bowl',
  },
  {
    label: 'All-Rounder',
    key: 'allrounder',
  },
]
