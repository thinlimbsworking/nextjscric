import React from "react";

export default function Fireicon({className=''}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="11.505"
      height="22.67"
      viewBox="0 0 11.505 22.67"
      className={className}
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="0.5"
          x2="0.5"
          y2="1"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#38d926" />
          <stop offset="1" stop-color="#1f2634" />
        </linearGradient>
      </defs>
      <g id="Group_8784" data-name="Group 8784" transform="translate(0)">
        <g id="Group_8785" data-name="Group 8785" transform="translate(0)">
          <path
            id="Path_7094"
            data-name="Path 7094"
            d="M4.486,5.282c-3.111,3.256-4.875,6.46-4.413,11,.8,7.814,10.243,7.867,11.286,1.953,1.085-6.15-4.269-4.775-3.545-11.286C8.375,1.9,5.282,0,5.282,0s1.133,3.263-.8,5.282"
            transform="translate(0 0)"
            fill="#38d926"
          />
          <path
            id="Path_7095"
            data-name="Path 7095"
            d="M10.532,24.907c.507,5.354-5.933,5.643-6,12.227-.063,5.76,3.473,7.235,6.439,6.584s5.426-2.749,2.026-8.031A8.776,8.776,0,0,1,11.4,30.116s-2.508,2.677-1.206,4.775S7.3,37.038,8.6,33.348s2.306-3.2,2.6-5.354a4.8,4.8,0,0,0-.675-3.087"
            transform="translate(-4.092 -22.519)"
            fill="url(#linear-gradient)"
          />
          <path
            id="Path_7096"
            data-name="Path 7096"
            d="M9.716,98.425c-5.064-.507-6.83-3.492-6.728-6.584C3.132,87.428,7.4,84.534,7.4,84.534s-2.46,2.749-1.085,5.426a7.425,7.425,0,0,0,4.2,3.473,7.494,7.494,0,0,1-.941-3.617,13.834,13.834,0,0,1,.145-1.809,5.554,5.554,0,0,0,1.809,2.966c2.17,1.809,2.17,3.762,1.519,5.282s-3.328,2.17-3.328,2.17"
            transform="translate(-2.697 -76.431)"
            fill="#38d926"
          />
          <path
            id="Path_7097"
            data-name="Path 7097"
            d="M22.778,115.479q-.073.125-.143.25c-2.377,4.213-2.062,7.308-.146,9.011,1.953,1.736,4.461,3.5,5.764.675.7-1.517-.772-1.905-3.3-4s-2.17-5.933-2.17-5.933"
            transform="translate(-18.943 -104.41)"
            fill="url(#linear-gradient)"
          />
          <path
            id="Path_7098"
            data-name="Path 7098"
            d="M91,107.68c-2.315,2.7-1.568,4.1-.8,5.836a5.034,5.034,0,0,1,.627,2.556,3.457,3.457,0,0,0,.362-3.642c-1.543-3.5-.193-4.751-.193-4.751"
            transform="translate(-80.871 -97.358)"
            fill="url(#linear-gradient)"
          />
          <path
            id="Path_7099"
            data-name="Path 7099"
            d="M10.987,129.065c-1.109,3.328,2.6,5.547,4.486,6.222a2.439,2.439,0,0,1,1.64,2.556,3.976,3.976,0,0,0,.241-2.749c-.482-1.254-.724-1.109-.724-1.109a2.683,2.683,0,0,1,.627,5.113c-.965.386-3.569.82-5.981-2.363s-.289-7.669-.289-7.669"
            transform="translate(-9.154 -116.693)"
            fill="url(#linear-gradient)"
          />
          <path
            id="Path_7100"
            data-name="Path 7100"
            d="M22.5,54.6a4.491,4.491,0,0,1,.121,4.968c-.482.555-.145-1.568.145-2.508A4.941,4.941,0,0,0,22.5,54.6"
            transform="translate(-20.236 -49.362)"
            fill="url(#linear-gradient)"
          />
        </g>
      </g>
    </svg>
  );
}
