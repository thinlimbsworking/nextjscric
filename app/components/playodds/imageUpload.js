import React, { useState } from 'react'

export default function ImageUpload() {
  const [selectedImage, setSelectedImage] = useState(null)

  return (
    <div className="text-white bg-gray">
      {selectedImage && (
        <div>
          <img
            alt="not fount"
            width={'250px'}
            src={URL.createObjectURL(selectedImage)}
          />
          <br />
          <button onClick={() => setSelectedImage(null)}>Remove</button>
        </div>
      )}
      <input
        type="file"
        name="myImage"
        onChange={(event) => {
          console.log(event.target.files[0])
          setSelectedImage(event.target.files[0])
        }}
      />
      upload
    </div>
  )
}
