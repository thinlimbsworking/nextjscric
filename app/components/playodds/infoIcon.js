import React from 'react'

export function InfoIcon({ clr, width, height }) {
  return (
    <svg width={width || 13} height={height || 13} viewBox="0 0 13 13">
      <g data-name="Group 25528">
        <text
          transform="translate(6)"
          fill={clr || '#6b9db7'}
          fontSize={9}
          fontFamily="Manrope"
          fontWeight={800}
        >
          <tspan x={-1} y={10}>
            {'i'}
          </tspan>
        </text>
        <g
          data-name="Ellipse 467"
          fill="none"
          stroke={clr || '#6b9db7'}
          strokeWidth={1.2}
        >
          <circle cx={6.5} cy={6.5} r={6.5} stroke="none" />
          <circle cx={6.5} cy={6.5} r={5.9} />
        </g>
      </g>
    </svg>
  )
}
