'use client'
import { useQuery } from '@apollo/react-hooks'
import dynamic from 'next/dynamic'
import React, { useContext } from 'react'
import { useState } from 'react'
import {
  LEADERBOARD_PROFILE,
  SERIES_NAME_FOR_LEADERBOARD,
} from '../../api/queries'
import ImageWithFallback from '../commom/Image'
import { LocalStorageContext } from '../layout'
import Loading from '../loading'

const Leaderboardfilter = dynamic(() => import('./leaderboardfilter'), {
  loading: () => <Loading />,
})

export default function Leaderboard(props) {
  const [leaderboardfilter, setleaderboardfilter] = useState([
    { valueLabel: 'weekly', valueId: '' },
    { valueLabel: 'monthly', valueId: '' },
  ])

  // const myToken=localStorage.getItem("tokenData")
  const { getItem } = useContext(LocalStorageContext)

  const [activefilter, setactivefilter] = useState(leaderboardfilter[0])
  const { data, error, loading } = useQuery(LEADERBOARD_PROFILE, {
    variables: {
      id: (props?.matchID && props?.matchID) || activefilter?.valueId,
      // key: props?.matchID ? 'matchwise':activefilter?.valueLabel && activefilter?.valueLabel || "series",
      key: props?.matchID
        ? 'matchwise'
        : activefilter?.valueId
        ? 'series'
        : activefilter?.valueLabel,
      token: getItem('tokenData'),
    },
  })

  useQuery(SERIES_NAME_FOR_LEADERBOARD, {
    onCompleted: (data) => {
      if (data?.predictionHomeSeries?.tourID) {
        setleaderboardfilter((prev) => [
          ...prev,
          {
            valueId: data.predictionHomeSeries.tourID,
            valueLabel: data.predictionHomeSeries.tourName,
          },
        ])
      }
    },
  })

  if (error) {
  }

  return (
    <div className="h-screen">
      <div className="w-full lg:hidden">
        <div className="p-2 fixed items-center justify-between flex">
          <div
            onClick={() => window.history.back()}
            className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
          >
            <svg width="30" focusable="false" viewBox="0 0 24 24">
              <path
                fill="#ffff"
                d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
              ></path>
              <path fill="none" d="M0 0h24v24H0z"></path>
            </svg>
          </div>
          <span
            className={`${
              props?.matchID
                ? 'text-lg  font-semibold w-11/12'
                : 'text-lg  font-semibold w-8/12'
            }`}
          >
            Leaderboard
          </span>
          <div className="flex  text-right bg-green"></div>
        </div>

        <div className="md:hidden lg:hidden xl:hidden p-3 my-0 flex items-center -mt-1">
          <span className="font-semibold md:hidden lg:hidden xl:hidden"></span>
        </div>
      </div>
      <div className="flex items-end justify-end ">
        <Leaderboardfilter
          options={leaderboardfilter}
          defaultSelected={activefilter}
          onChange={(options) => setactivefilter(options)}
          props={props}
        />
      </div>
      <div className="ml-2 mr-2 lg:mt-0 md:mt-0 md:overflow-hidden lg:overflow-hidden lg:static md:static">
        {data?.leaderboard}
        <div className="flex items-end justify-end text-gray-3 p-2 text-xs">
          Total Players: {data?.leaderBoardByKeys?.count || 0}
        </div>
        <div className="flex py-2 mt-4 bg-gray-4 shadow-4 justify-between items-center rounded-t-xl text-xs">
          <div className="flex md:w-8/12 lg:w-8/12 xl:w-8/12 w-full">
            <h2 className="text-xs mx-2 font-semibold text-gray-3">Name</h2>
          </div>

          {/* <div className='flex items-center w-2/12 md:w-20 justify-center xl:w-20 lg:w-20 text-center text-xs font-semibold'></div> */}

          {leaderboardtag?.map((list) => (
            <div className="flex items-center w-3/12 md:w-20 text-gray-3 py-2 rounded-sm justify-center xl:w-20 lg:w-20 text-center text-xs font-semibold">
              {list?.label}
            </div>
          ))}
        </div>

        {data?.leaderBoardByKeys?.data?.map((item, index) => {
          return (
            <div className="">
              <div
                className={`${
                  index === 0
                    ? 'flex py-1 text-xs font-medium justify-between items-center border-2 border-green text-green  px-1 bg-gray'
                    : 'flex py-1 text-xs  font-medium justify-between items-center dark:bg-gray px-1 text-gray-2'
                }`}
              >
                <div className="flex w-8/12 items-center break-words">
                  {' '}
                  <div className="flex items-center justify-between mx-2">
                    <ImageWithFallback
                      src="/pngsV2/playerph.png"
                      fallbackSrc={'/pngsV2/playerph.png'}
                      loading="lazy"
                      width={10}
                      height={10}
                      className="h-10 w-10 bg-gray object-top object-cover rounded-full"

                      // onError={(evt) =>
                      //   (evt.target.src = "/pngsV2/playerPH.png")
                      // }
                    />
                  </div>
                  <div className="flex ">{item.username}</div>
                </div>

                <div
                  className={`flex items-center md:w-20 text-xs justify-center lg:w-20 xl:w-20 w-2/12 ${
                    item.totalpoints < 0 ? 'text-red' : ''
                  }`}
                >
                  {Math.ceil(Math.abs(item.totalpoints)) || '0'}
                </div>
                <div
                  className={`flex items-center md:w-20 justify-center text-xs lg:w-20 xl:w-20 w-2/12 `}
                >
                  {item.rank || '0'}#
                </div>
              </div>
              <div className="bg-black/80 h-[1px]" />
            </div>
          )
        })}
      </div>
    </div>
  )
}

const leaderboardtag = [
  {
    label: 'Coins',
    key: 'mycoins',
  },
  {
    label: 'Rank',
    key: 'myrank',
  },
]
