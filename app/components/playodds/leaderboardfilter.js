'use client'
import React from 'react'
const Dropdown = ({
  options,
  placeHolder = 'Dropdown',
  defaultSelected,
  onChange,
  props,
}) => {
  const [selectedOption, setSelectedOption] = React.useState(defaultSelected)
  const [openDropDown, setDropDown] = React.useState(false)

  React.useEffect(() => {
    if (onChange) {
      onChange(selectedOption)
    }
  }, [selectedOption, onChange])
  console.log(props, 'okayThikHai')

  return !props?.matchID ? (
    <div className="inline-block relative">
      <button
        className="bg-gray rounded-full w-[100%] text-white font-semibold py-2 z-10 px-4 inline-flex items-center"
        onClick={() => setDropDown(!openDropDown)}
      >
        <span className="mr-1 capitalize truncate max-w-[13ch]">
          {selectedOption?.valueLabel || placeHolder}
        </span>
        <svg
          className="fill-current h-4 w-4"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
        >
          <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
        </svg>
      </button>
      <ul
        className={`absolute rounded-lg right-0 bg-gray-4 border-2 border-gray-8 ${
          openDropDown ? '' : 'hidden'
        } text-gray-700 pt-1`}
      >
        {options.map((option, index) => (
          <li
            role="button"
            className={`${
              index === options.length - 1
                ? ''
                : 'border-b-2 border-b-gray pb-2'
            } border-t-solid border-t-gray-7`}
            onClick={() => {
              setSelectedOption(option)
              setDropDown(false)
            }}
          >
            <p className="rounded-t bg-gray-4 truncate lg:py-6 lg:px-4 lg:text-ellipsis max-w-[13ch] text-white text-xs hover:bg-gray-400 py-1 px-2 block whitespace-no-wrap capitalize">
              {option.valueLabel}
            </p>
          </li>
        ))}
      </ul>
    </div>
  ) : (
    ''
  )
}

export default Dropdown
