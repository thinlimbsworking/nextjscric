// 'use client'
import React from 'react'

export default function Monthfilter({
  filterShow,
  setYear,
  setMonth,
  setfilterShow,
  selectedMonth,
  selectedYear,
}) {
  return (
    filterShow && (
      <div className="grid absolute lg:absolute place-items-center pb-2 lg:top-96 top-96 lg:ml-48 left-[50%] mr-3 rounded-md bg-gray border-2 border-solid border-gray-2 -mt-1 lg:mt-24 z-[99999999]">
        <span className="flex w-full ml-2 text-white text-xs lg:text-sm">
          Year
        </span>
        <div className="flex w-full flex-wrap place-items-start py-2">
          {year?.map((item) => (
            <button
              key={item?.key}
              onClick={() =>
                selectedYear === item?.key ? setYear('') : setYear(item?.key)
              }
              className={`cursor-pointer w-3/12 place-items-start text-xs py-2 bg-basebg  ${
                item?.key === selectedYear ? 'border-green border-2' : ''
              } rounded-lg px-1 py-2 h-1 mx-1 text-white flex items-center justify-center`}
            >
              {item?.yearlabel}
            </button>
          ))}
        </div>
        <div></div>

        {/* <hr className=" w-full"/> */}
        <span className="flex w-full ml-2 text-xs pb-1 text-white lg:text-sm">
          Months
        </span>
        <div className="flex w-full flex-wrap items-center justify-center">
          {months?.map((list, key) => (
            <button
              disabled={new Date().getMonth() < key}
              key={list?.key}
              className={`cursor-pointer w-3/12 py-2 text-xs rounded-lg bg-basebg ${
                list?.key === selectedMonth ? 'border-green border-2 ' : ''
              } disabled:border-gray  disabled:text-[#515A6C] rounded-full py-2 px-1 m-1 h-1 text-white flex items-center justify-center`}
              onClick={() =>
                selectedMonth === list?.key ? setMonth('') : setMonth(list?.key)
              }
            >
              {list?.monthLabel}
            </button>
          ))}
        </div>
      </div>
    )
  )
}

const months = [
  {
    monthLabel: 'Jan',
    key: 'jan',
  },
  {
    monthLabel: 'Feb',
    key: 'feb',
  },
  {
    monthLabel: 'Mar',
    key: 'mar',
  },
  {
    monthLabel: 'Apr',
    key: 'apr',
  },
  {
    monthLabel: 'May',
    key: 'may',
  },
  {
    monthLabel: 'Jun',
    key: 'jun',
  },
  {
    monthLabel: 'July',
    key: 'jul',
  },
  {
    monthLabel: 'Aug',
    key: 'aug',
  },
  {
    monthLabel: 'Sep',
    key: 'sep',
  },
  {
    monthLabel: 'Oct',
    key: 'oct',
  },
  {
    monthLabel: 'Nov',
    key: 'nov',
  },
  {
    monthLabel: 'Dec',
    key: 'dec',
  },
]

const year = [
  {
    yearlabel: '2023',
    key: '2023',
  },
]
