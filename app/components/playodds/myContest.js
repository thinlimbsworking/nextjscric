'use client'

import { useQuery } from '@apollo/react-hooks'
import { useMutation } from '@apollo/react-hooks'
import React from 'react'
import { useState, useContext } from 'react'
import {
  UPDATE_WALLET_COINS,
  PREDICTION_PROFILE,
  PREDICTION_PROFILE_STREAK,
  PREDICTION_PROFILE_MATCH,
  GET_PREURL,
} from '../../api/queries'
import { useRouter } from 'next/navigation'
import { getContestUrl } from '../../api/services'
import { LocalStorageContext } from '../../components/layout'

import Link from 'next/link'
import axios from 'axios'
import ImageWithFallback from '../commom/Image'
import dynamic from 'next/dynamic'
import Loading from '../loading'

import Profilecard from './profilecard'
import Streakcard from './streakcard'
import Heading from '../commom/heading'
import DataNotFound from '../commom/datanotfound'

const Scores = dynamic(() => import('../commom/score'), {
  loading: () => <Loading />,
})
const Addcoinsmodal = dynamic(() => import('./addcoinsmodal'), {
  loading: () => <Loading />,
})
const Monthfilter = dynamic(() => import('./monthfilter'), {
  loading: () => <Loading />,
})

export default function MyContest(props) {
  let router = useRouter()
  // const token = localStorage.getItem('tokenData')
  const { getItem } = useContext(LocalStorageContext)

  const [preUrl, setpreUrl] = useState('')
  const [path, setpath] = useState('')
  const [photoUrl, setphotoUrl] = useState('')

  const [showFilter, setshowFilter] = useState(false)
  const [selectedYear, setSelectedYear] = useState('')
  const [selectedMonth, setSelectedMonth] = useState('')

  const getcontestSummary = (matchID) => {
    return getContestUrl(matchID)
  }

  const [updatecoins] = useMutation(UPDATE_WALLET_COINS, {
    onCompleted: (data) => {
      if (data) {
      }
    },
  })

  const [imgupload] = useMutation(GET_PREURL, {
    variables: {
      token: getItem('tokenData'),
    },
  })

  const [displaypopup, setdisplaypopup] = useState(false)
  const { data } = useQuery(PREDICTION_PROFILE_MATCH, {
    variables: {
      matchID: props[1] || '',
      token: getItem('tokenData'),
    },
  })
  console.log(
    'datatatadatadatadatdatadtadtadtadtadtadtadtadtadatdatdatadtadatat',
    data,
  )

  const {
    data: profileDetails,
    loading,
    refetch: fetchProfileDetails,
  } = useQuery(PREDICTION_PROFILE, {
    variables: {
      year: selectedYear,
      month: selectedMonth,
      token: getItem('tokenData'),
    },
    onCompleted: (data) => {
      fetchProfileDetails()
    },
  })

  const { data: streakData } = useQuery(PREDICTION_PROFILE_STREAK, {
    variables: {
      token: getItem('tokenData'),
    },
  })

  const [showcoinsmodal, setshowcoinsmodal] = useState(false)

  // const [images, setImages] = useState([]);

  async function handleImageUpload(e) {
    const image = e.target.files[0]
    if (image) {
      const uploadEndPoints = await imgupload()
      if (uploadEndPoints) {
        const conifg = {
          method: 'PUT',
          url: uploadEndPoints?.data?.getPresignUrl?.preurl,
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'image/png',
          },
          data: image,
        }
        axios(conifg).then(function (response) {
          const imgUrl = 'https://api.devcdc.com/cricket/services/updateImgix'
          axios(imgUrl, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              url: uploadEndPoints?.data?.getPresignUrl?.preurl,
              path: uploadEndPoints?.data?.getPresignUrl?.path,
              photoUrl: uploadEndPoints?.data?.getPresignUrl?.photoUrl,
              token: getItem('tokenData'),
            }),
          })
            .then((res) => res.json())
            .then((response) => {
              // imageUploadCB(response, res);
              // setisLoading(false);
              // handleModalClose(false);
            })
            .catch((err) => {
              // handleModalClose(false);
              // setisLoading(false);
            })
          // .then(res => res), "afteriage");
        })
      }
    }
  }

  if (loading) {
    return <div></div>
  }

  return (
    <>
      {}
      <div className={`${displaypopup ? 'opacity-25' : ''}`}>
        <div className="flex w-full justify-between items-center fixed lg:hidden top-0 z-50 bg-gray-8 lg:relative">
          <div className="bg-gray-8 flex items-center p-2 rounded-md">
            <div
              onClick={() => window.history.back()}
              className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
            >
              <svg width="30" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#ffff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <span className="text-base text-white font-bold lg:hidden">
              Profile
            </span>
          </div>
          <button
            style={{ color: '#6ED54A' }}
            className="pr-1 border-2 border-solid border-gray-3 pl-2 rounded-md py-1 mr-2 lg:mr-4 mt-1 sm:z-50"
            onClick={() => setshowcoinsmodal(!showcoinsmodal)}
          >
            <span className="flex gap-2">
              <ImageWithFallback
                height={22}
                width={22}
                className="w-6 h-6"
                loading="lazy"
                src="/pngsV2/wallet.png"
              />
              <div className="border-r-gray-2 border-r-2 border-r-solid"></div>
              <ImageWithFallback
                width={20}
                height={2}
                src="/pngsV2/coin.png"
                alt=""
              />
              {profileDetails &&
                profileDetails?.predictionProfile?.totalEarnings}
            </span>
          </button>

          <Addcoinsmodal
            showpopup={showcoinsmodal}
            profileDetails={profileDetails?.predictionProfile}
            setshowpopup={() => setshowcoinsmodal(!showcoinsmodal)}
            submitCallback={() => {
              return updatecoins({
                variables: {
                  token: getItem('tokenData'),
                },
              }).then(() => fetchProfileDetails())
            }}
          />
        </div>

        {/* Smaller Screens AddCoinsModal */}
        <div className="text-end hidden lg:block md:block">
          <Addcoinsmodal
            showpopup={showcoinsmodal}
            profileDetails={profileDetails?.predictionProfile}
            setshowpopup={() => setshowcoinsmodal(!showcoinsmodal)}
            submitCallback={() => {
              return updatecoins({
                variables: {
                  token: getItem('tokenData'),
                },
              }).then(() => fetchProfileDetails())
            }}
          />

          <button
            style={{ color: '#6ED54A' }}
            className="pr-2 border-2 border-solid border-gray-3 pl-3 rounded-md py-1 mr-2 mt-1"
            onClick={() => setshowcoinsmodal(!showcoinsmodal)}
          >
            <span className="flex gap-2">
              <ImageWithFallback
                width={22}
                height={2}
                className="w-6 h-6"
                src="/pngsV2/wallet.png"
              />
              <div className="border-r-gray-2 border-r-2 border-r-solid"></div>
              <ImageWithFallback
                width={20}
                height={2}
                src="/pngsV2/coin.png"
                alt=""
              />
              {profileDetails &&
                profileDetails?.predictionProfile?.totalEarnings}
            </span>
          </button>
        </div>
        <Profilecard
          profileinfo={profileDetails?.predictionProfile}
          handleclick={() => setdisplaypopup(!displaypopup)}
        />

        <Streakcard streakInfo={streakData?.contestStreak} />
        <div
          className={`${
            showcoinsmodal
              ? 'flex flex-col items-start opacity-25 justify-start px-3 pb-4'
              : 'flex flex-col items-start justify-start px-3 pb-4'
          }`}
        >
          <div className="flex items-end justify-between w-full">
            <Heading heading={'My Contests'} />

            <ImageWithFallback
              width={22}
              height={22}
              className="lg:sticky w-8"
              src="/pngsV2/filter.png"
              role="button"
              onClick={() => setshowFilter(!showFilter)}
            />
            {showFilter && (
              <Monthfilter
                filterShow={showFilter}
                setfilterShow={() => setshowFilter(!showFilter)}
                selectedMonth={selectedMonth}
                selectedYear={selectedYear}
                setMonth={setSelectedMonth}
                setYear={setSelectedYear}
              />
            )}
          </div>

          <div
            className={`w-full mt-3 flex-wrap lg:flex md:flex ${
              showFilter ? 'opacity-25 overflow-hidden' : ''
            }`}
          >
            <>
              {profileDetails?.predictionProfile?.matches ? (
                <>
                  {profileDetails?.predictionProfile?.matches?.map(
                    ({ matchDetails: card }) => (
                      <div
                        key={card.matchID}
                        className="w-full md:w-1/2 lg:w-1/2 overflow-hidden flex justify-center  items-center cursor-pointer "
                      >
                        {card?.matchResult !== null &&
                        card?.matchResult === 'Match Cancelled' ? (
                          <></>
                        ) : (
                          <Link
                            className="w-full"
                            {...getcontestSummary(card.matchID)}
                            as={`${
                              getcontestSummary(card.matchID).as
                            }/matchSummary`}
                            href={`${
                              getcontestSummary(card.matchID).href
                            }/matchSummary`}
                            passHref
                          >
                            <div className="w-full cursor-pointer flex flex-col p-2">
                              {card && card.matchID ? (
                                <Scores data={card} isPredictedMatch />
                              ) : (
                                ''
                              )}
                            </div>
                          </Link>
                        )}
                      </div>
                    ),
                  )}
                </>
              ) : (
                <DataNotFound displayText={'No match played yet'} />
              )}
            </>
          </div>
        </div>
        {/* <ImageUpload
        setshowupload={() => setdisplaypopup()}
        showupload={displaypopup}
      /> */}

        {/* <ImageUpload/> */}
      </div>
      {displaypopup && (
        <div className="bottom-0 w-full border-gray-6 border-solid border-2 fixed bg-gray-6 z-[9999] text-white border-t-4 h-16">
          <div>
            <div className="flex items-center justify-evenly mt-3">
              {/* { <input type="file" className="bg-gray text-green" multiple accept="image/*" onChange={handleImageUpload}/>} */}
              {/* <span>
                    <input
                      type="file"
                      className="my-2 bg-gray-700 w-full border-b-gray-600 border-b-2 outline-none text-white-500"
                      onChange={handleFileSelector}
                      
                    />  

                    Gallery 
                  </span>  */}
              <label class="block">
                <span class="sr-only">Choose profile photo</span>
                <input
                  type="file"
                  className=" w-full text-sm file:mr-4 file:py-2 file:px-4 file:text-sm file:font-semibold file:text-green file:bg-gray"
                  onChange={handleImageUpload}
                />
              </label>
            </div>
          </div>
        </div>
      )}
    </>
  )
}
