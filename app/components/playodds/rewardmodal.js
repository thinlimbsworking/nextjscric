'use client'

import React from 'react'
import ImageWithFallback from '../commom/Image'

export default function Rewardmodal({ showmodal, setshowmodal }) {
  return (
    <div>
      {showmodal && (
        <div className="top-0 lg:top-32 lg:w-[72%] rounded-tl-xl rounded-tr-xl mx-1 h-80 w-full border-gray-6 border-solid border-2 fixed bg-gray-6 z-[200] text-white border-t-4">
          <div className="flex items-center justify-end mr-8 mt-2">
            <button onClick={() => setshowmodal(!showmodal)}>X</button>
          </div>

          <div className="flex-col justify-center border-2 border-solid border-gray-9 rounded-xl mr-6 lg:ml-8 pt-9 pb-3">
            <ImageWithFallback
              src="/pngsV2/rewardicon.png"
              width={200}
              height={4}
              className="ml-24 lg:flex lg:justify-center lg:items-center lg:ml-[40%] lg:mt-6"
            />
            <h2 className="text-xl pt-6 text-center ">
              Stay tuned for <br />
              <span className="">the Reward Scheme</span>
            </h2>
          </div>
        </div>
      )}
    </div>
  )
}
