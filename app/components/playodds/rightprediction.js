'use client'

import React from 'react'
import Predictionbar from './predictionDetails'

export default function Rightprediction({
  predictionDetails,
  props,
  profiledata,
}) {
  // console.log(props, 'propsssssssssssssssssssssssssss')
  console.log(props[1], 'propsssssssssssssssssssssssssss')

  return (
    <div className="pt-3">
      <div className="border dark:border-none dark:bg-gray-6 lg:mt-3 mt-28 flex  border-solid rounded-md py-4 px-2 m-2 flex-col">
        <div className="flex items-center justify-between pb-1">
          <div>
            {profiledata?.predictionProfile?.matches.map((item) => (
              <>
                {item?.matchDetails?.matchID === props[1] && (
                  <span className="text-xs ">
                    {item?.matchDetails?.totalParticipants || 0}
                  </span>
                )}
                {item?.matchDetails?.matchID === props[1] && (
                  <span className="text-xs "> Participants </span>
                )}
              </>
            ))}
          </div>

          <h2 className=" text-xs uppercase">
            MY RANK{' '}
            <span className="text-lg font-bold pl-1">
              # {predictionDetails?.rank}
            </span>
          </h2>
        </div>
        <hr className="border-gray-2 w-full" />

        <Predictionbar matchID={props[1]} isContestSummary={true} />
      </div>
    </div>
  )
}
