'use client'

import React from 'react'
import ImageWithFallback from '../commom/Image'

export default function Rulebook({ showrulebook, setshowrulebook }) {
  return (
    showrulebook && (
      <div className="overflow-auto h-[500px]">
        {/* {ruleBook?.map(item => <p className='h-32 p-4 flex items-center justify-start'><span className='px-4'><img src="/pngsV2/tag.png" alt="" height='15px' width='24px' /></span> <span>{item?.lable}</span> </p>)} */}
        {/* {ruleBook?.map(item => <p className='h-32 p-4 flex items-center justify-start'><span className='px-4'><img src="/pngsV2/tag.png" alt="" height='15px' width='24px' /></span> <span>{item?.lable}</span> </p>)} */}
        {ruleBook?.map((item, key) => (
          <div
            className="flex flex-row mb-2 flex-1 items-baseline "
            key={`${key}`}
          >
            <div className="flex flex-[0.1] justify-center">
              <div>
                <ImageWithFallback
                  src="/pngsV2/tag.png"
                  alt=""
                  height={10}
                  width={12}
                />
              </div>
            </div>
            <div className="flex flex-[0.9]">
              <div>{item?.lable}</div>
            </div>
          </div>
        ))}
      </div>
    )
  )
}

const ruleBook = [
  {
    lable:
      'In every match you can use maximum 100 coins to answer the questions.',
  },
  // {
  //   lable:
  //     "Unused coins from every match will get lapsed and won't be carried forward.",
  // },
  {
    lable: "Once you lock an answer, it can't be undone.",
  },
  {
    lable:
      'You can view your locked answers and contests entered under Profile.',
  },
  {
    lable: 'All questions will be open only till commencement of the match.',
  },
  {
    lable: 'Earnings = Amount Entered x Odds',
  },
  {
    lable:
      'If you play on consecutive featured match days, only then the streak will be maintained.',
  },
  {
    lable:
      'You can view Weekly, Monthly and Series leaderboard on the homepage.',
  },
  {
    lable:
      'You can view your match specific rank and the leaderboard for it by clicking on relevant contest cards under profile.',
  },
  {
    lable:
      'Ranks in the leaderboard are based on the number of coins earned during the week/month/series.',
  },
  {
    lable:
      'You can check your Level, Total Earnings & Number of contests entered under profile.',
  },
  {
    lable:
      'In case of canceled or abandoned events, locked answers will get canceled.',
  },
  {
    lable:
      'If a tie/draw is the result of a question, then the question will stand canceled.',
  },
  {
    lable: 'For canceled bet the amount will be refunded to the wallet.',
  },
  {
    lable: 'You can reload the coins when coins reach below 100 in the wallet.',
  },
]
