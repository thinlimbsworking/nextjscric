'use client'
import React, { useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { CHECK_UNIQUE_USERNAME, UPDATE_PROFILE } from '../../api/queries'
import ImageWithFallback from '../commom/Image'
export default function SetUsername({ setNext, handleSubmit, token }) {
  const [state, setState] = useState('')
  const [isValid, setIsvalid] = useState(false)
  const [checkUser, response] = useMutation(CHECK_UNIQUE_USERNAME, {
    onCompleted: (res) => {
      console.log('res is: ', res)
      setIsvalid(!res.checkUniqueUSer.userExist)
    },
  })
  const [updateUser, resp] = useMutation(UPDATE_PROFILE, {
    onCompleted: (res) => {
      if (res.updateProfileInfo.code !== 200) {
        console.log('couldnt update')
        return
      }
      localStorage.setItem('userName', state)
      setNext(false)
      handleSubmit()
    },
  })

  const handleName = (name) => {
    setState(name)
    checkUser({
      variables: {
        username: name,
        token,
      },
    })
  }
  const goToNext = () => {
    if (!isValid) return
    updateUser({
      variables: {
        username: state,
        token,
        cleverTapID: '',
      },
    })
  }
  return (
    <div className="w-full h-full flex flex-col items-center py-8 gap-8">
      <div className="text-center text-white text-base font-semibold mt-8">
        Set your unique username
      </div>
      <ImageWithFallback
        className="rounded-full h-24 w-24"
        src="/svgs/avtaar.svg"
        height={24}
        width={24}
      />
      <div className="flex flex-col w-[90%] m-2 gap-2">
        <input
          type="text"
          placeholder="Maximum 10 characters"
          className="bg-gray w-full rounded-md p-3 text-white"
          value={state}
          onChange={(e) => handleName(e.target.value)}
        />
        <span className=" text-gray-3 text-xs ">
          You can use alphabets and numbers only
        </span>
      </div>
      <div
        className={`rounded-md border-2 text-sm font-semibold text-center cursor-pointer px-5 py-2 mt-8 ${
          isValid ? 'border-green text-green' : 'border-gray-2 text-white'
        }`}
        onClick={goToNext}
      >
        SUBMIT
      </div>
    </div>
  )
}
