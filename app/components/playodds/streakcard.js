'use client'
import dynamic from 'next/dynamic'
import React, { useState } from 'react'

import Tooltip from '../commom/tooltip'
import Loading from '../loading'
import Fireicon from './fireicon'
const Matchespopup = dynamic(() => import('./matchespopup'), {
  loading: () => <Loading />,
})

export default function Streakcard({ streakInfo = { matchesData: [] } }) {
  const [showthePopup, setshowthePopup] = useState(false)
  const [popupData, setPopUpData] = useState('')

  return (
    <div>
      {/* < data /> */}
      <div className="flex relative bg-gray flex-col mx-4 my-4 p-4 h-auto">
        <div className="flex justify-between">
          <span className="text-white">Streak - {streakInfo?.streak}</span>
          <h4 className="text-green">
            Highest Streak - {streakInfo?.heighestStreak}
          </h4>
        </div>
        <hr className="w-3/4 absolute top-[70%] left-[12%] lg:left-[14%] lg:w-[73%] m-auto border-gray-8 border-1" />

        <div
          className="flex-row flex justify-evenly"
          onClick={() => setshowthePopup(!showthePopup)}
        >
          {[
            ...streakInfo?.matchesData,
            ...Array(Math.abs(7 - streakInfo?.matchesData.length)).fill({}),
          ].map((item, index) => {
            return (
              <div className="w-8 h-8 mt-4 rounded-full border-2 border-solid border-gray z-30 bg-basebg relative">
                {item?.matchesList ? (
                  <Tooltip
                    className="absolute text-white"
                    direction="down"
                    content={item?.matchesList?.map((x) => {
                      return (
                        <>
                          <h3>
                            {x?.awayTeamName} vs {x?.homeTeamName}
                          </h3>
                        </>
                      )
                    })}
                    addCoinsToolTip={false}
                  >
                    {}
                    {streakInfo?.continousStreak > index && (
                      <Fireicon className="relative top-[10%] m-auto flex justify-center items-center" />
                    )}
                  </Tooltip>
                ) : (
                  ''
                )}
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
