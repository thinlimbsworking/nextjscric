import React from 'react'

import { getArticleTabUrl } from '../api/services'
import CleverTap from 'clevertap-react'

import { useRouter } from 'next/navigation'
import { format } from 'date-fns'

const RelatedArticle = (article, rArticleEvent, setArticleEvent) => {
  var article = article.article
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    let sec = Math.floor((distance % (1000 * 60)) / 1000)

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago'

    return result
  }
  const handleNav = () => {
    CleverTap.initialize('Article', {
      Source: 'Related',
      ArticleType:
        article.type === 'match_report'
          ? 'MatchReport'
          : article.type === 'news'
          ? 'News'
          : article.type === 'fantasy'
          ? 'Fantasy'
          : article.type === 'interview'
          ? 'Interview'
          : 'Others',
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: localStorage ? localStorage.Platform : '',
    })
    router.push(
      getArticleTabUrl(article.articleID).as,
      getArticleTabUrl(article.articleID).as,
    )
  }

  const router = useRouter()
  return (
    <div
      onClick={handleNav}
      className="flex flex-row items-center"
      style={{ flex: 1 }}
    >
      {/* For Large and Web screens */}
      <div className="pb-1 hidden lg:block md:block">
        <div className="p-1 flex items-start m-1">
          <div className="relative dark:w-40">
            {article.sm_image_safari ? (
              <img
                className=" object-cover object-top dark:h-20 dark:w-36 h-32 w-80 rounded"
                src={`${article?.sm_image_safari}?auto=compress&dpr=2&w=240&h=144`}
              />
            ) : (
              <div className="bg-gray-2 h-36 w-36"></div>
            )}

            <div className="px-1 absolute bottom-2 dark:bg-green-6 text-white bg-basered dark:text-black text-xs font-medium">
              {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
            </div>
          </div>
          <div className="w-9/12 mx-2">
            <div className="text-xs dark:text-white text-[#121F2A] dark:font-semibold font-bold mt-1 line-clamp">
              {article && (article.title || '')}
            </div>
            <div className="text-xs dark:text-gray-2 line-clamp-3 text-[#000000] mt-1 lg:font-normal md:font-normal xl:font-normal font-normal hidden md:flex">
              {article && (article.description || '')}
            </div>

            <div className="justify-center">
              <div className="mt-1">
                {/* <div
                      className={`${
                        props.authorPic
                          ? 'flex items-center justify-center w-10 h-10  '
                          : 'flex items-center justify-center w-5 h-5'
                      }`}>
                      {' '}
                      <img
                        className={`${props.authorPic ? ' h-1 w-1 rounded-full ' : ''}`}
                        src={`${props.authorPic ? props.authorPic : '/pngsV2/UserProfile.png'}`}
                        alt='user'
                      />
                    </div> */}

                <div className="flex mt-1">
                  {' '}
                  {/* <span className="dark:text-gray-2 text-black text-xs font-medium ">
                    By:
                  </span> */}
                  <img
                    src="/pngsV2/authoriconnew.png"
                    alt="md:block hidden"
                    height={'20px'}
                    width={'20px'}
                  />
                  <img
                    src="/pngsV2/user.png"
                    alt=""
                    className="md:hidden"
                    height={'20px'}
                    width={'20px'}
                  />
                  <span className="text-gray-2 truncate md:w-20 text-xs pl-1">
                    {article &&
                      article.author &&
                      article.author.split(',') &&
                      article.author.split(',')[0]}
                    {article &&
                    article.author &&
                    article.author.split(',') &&
                    article.author.split(',').length > 1
                      ? ',...'
                      : ''}
                  </span>
                </div>
              </div>
              <div className="flex items-center text-xs text-gray-2 justify-end -mt-4">
                <div className="flex">
                  <span className="text-gray-white -mt-1 text-sm pr-1 lg:hidden md:hidden xl:hidden">
                    .
                  </span>
                  {}
                  {Date.now() - parseInt(article?.createdAt) > 8640000
                    ? article && format(+article?.createdAt, 'dd MMM yyyy')
                    : getUpdatedTime(article?.createdAt)}
                </div>
              </div>{' '}
            </div>
          </div>
        </div>
        <div className="bg-black h-[1px] lg:hidden md:hidden"></div>
      </div>

      {/* For mobile screens */}

      <div className="pb-1 lg:hidden md:hidden">
        <div className="p-1 flex items-start m-1">
          <div className="relative w-40">
            {article.sm_image_safari ? (
              <img
                className=" object-cover object-top h-20 w-36 rounded"
                src={`${article?.sm_image_safari}?auto=compress&dpr=2&w=240&h=144`}
              />
            ) : (
              <div className="bg-gray-2 h-36 w-36"></div>
            )}

            <div className="px-1 absolute bottom-2 dark:bg-green-6 text-white bg-basered dark:text-black text-xs font-medium">
              {article && article.type.replace(/[_]/g, ' ').toUpperCase()}
            </div>
          </div>
          <div className="w-3/5">
            <div className="text-xs font-semibold mt-1 line-clamp ">
              {article && (article.title || '')}
            </div>
            <div className="text-xs mt-1 hidden lg:block md:block xl:block ">
              {article && (article.description || '')}
            </div>

            <div className="flex justify-center mt-6 lg:hidden">
              <div className="flex items-center justify-start mt-1 w-7/12">
                {/* <div
                      className={`${
                        props.authorPic
                          ? 'flex items-center justify-center w-10 h-10  '
                          : 'flex items-center justify-center w-5 h-5'
                      }`}>
                      {' '}
                      <img
                        className={`${props.authorPic ? ' h-1 w-1 rounded-full ' : ''}`}
                        src={`${props.authorPic ? props.authorPic : '/pngsV2/UserProfile.png'}`}
                        alt='user'
                      />
                    </div> */}

                <div className="flex w-full">
                  {' '}
                  <span className="text-gray-2 text-xs font-medium ">By:</span>
                  <span className="text-gray-2 text-xs pl-1 line-clamp-1">
                    {article &&
                      article.author &&
                      article.author.split(',') &&
                      article.author.split(',')[0]}
                    {article &&
                    article.author &&
                    article.author.split(',') &&
                    article.author.split(',').length > 1
                      ? ',...'
                      : ''}
                  </span>
                </div>
              </div>
              <div className="flex text-xs text-gray-2 items-center justify-end mt-1 w-5/12">
                <div className="flex w-full">
                  {}
                  <span className="line-clamp-1">
                    {Date.now() - parseInt(article?.createdAt) > 8640000
                      ? article && format(+article?.createdAt, 'dd MMM yyyy')
                      : getUpdatedTime(article?.createdAt)}
                  </span>
                </div>
              </div>{' '}
            </div>
          </div>
        </div>
        {/* <div className="bg-black h-[1px] "></div> */}
      </div>
    </div>
  )
}

export default RelatedArticle
