import React, { useEffect, useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { PHASES_OF_PLAY } from '../api/queries'

export default function ScoringChartCompleted(props) {
  const [screenWidth, setScreenWidth] = useState(0)
  const [maxRun, setMaxRun] = useState(10)
  const [oversArr, setOverArr] = useState([])
  const [oversTab, setoversTab] = useState(0)
  const [yRange, setYRange] = useState()
  const [team, setTeam] = useState(0)

  const { loading, error, data } = useQuery(PHASES_OF_PLAY, {
    variables: { matchID: props.matchID },
    // variables: { matchID: '205391' },
    onCompleted: (data) => {
      if (
        data.getPhaseOfInningsResolver &&
        data.getPhaseOfInningsResolver.inningsPhase &&
        data.getPhaseOfInningsResolver.inningsPhase[team]
      ) {
        let a = Math.max.apply(
          Math,
          [
            ...data.getPhaseOfInningsResolver.inningsPhase[0].data,
            ...data.getPhaseOfInningsResolver.inningsPhase[1].data,
          ].map((o) => {
            return o.runs
          }),
        )

        setoversTab(
          data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25
            ? 1
            : 0,
        )
        setOverArr(
          props.matchType === 'T20'
            ? [4, 8, 12, 16, 20]
            : props.matchType === 'T10'
            ? [2, 4, 6, 8, 10]
            : data.getPhaseOfInningsResolver.inningsPhase[team].data.length >
                22 &&
              data.getPhaseOfInningsResolver.inningsPhase[team].data.length < 26
            ? [4, 8, 12, 16, 20, 24, 28]
            : data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25
            ? [30, 35, 40, 45, 50]
            : [5, 10, 15, 20, 25],
        )
        setYRange(
          a < 8
            ? 10
            : a <= 13
            ? 15
            : a <= 18
            ? 20
            : a <= 23
            ? 25
            : a <= 28
            ? 30
            : a <= 33
            ? 35
            : a <= 38
            ? 40
            : 45,
        )
        setMaxRun(a)
      }
    },
  })

  // if (typeof window !== 'undefined' && window && window.screen && window.screen.width) {
  //   setScreenWidth(window.screen.width > 975  ?props.cricltics?976: 976/1.5 : window.screen.width);
  //   // setscreenHeight(window.screen.height > 975 ? 976/3.5 : window.screen.height/3);
  // }
  useEffect(() => {
    // setScreenWidth(window.innerWidth);
    if (
      typeof window !== 'undefined' &&
      window &&
      window.screen &&
      window.screen.width
    ) {
      // setScreenWidth(window.screen.width > 1500  ?window.screen.width *.37 : window.screen.width > 975? window.screen.width:window.screen.width);
      setScreenWidth(
        window.innerWidth > 1570
          ? window.innerWidth * 0.37
          : window.innerWidth > 1300
          ? window.innerWidth * 0.34
          : window.screen.width,
      )

      //   setScreenWidth(window.screen.width > 975 ? 976/2 : window.screen.width);
      //   setscreenHeight(window.screen.height > 975 ? 976/4 : window.screen.height);
    }
  }, [])
  return data &&
    data.getPhaseOfInningsResolver &&
    data.getPhaseOfInningsResolver.inningsPhase &&
    data.getPhaseOfInningsResolver.inningsPhase.length > 0 &&
    data.getPhaseOfInningsResolver.inningsPhase[team] &&
    data.getPhaseOfInningsResolver.inningsPhase[team].data ? (
    <div className="text-white  rounded-md mt-3 py-3 px-2 ">
      <div className="flex items-center justify-between my-3 px-3">
        <div className="text-gray-2 text-sm font-medium">INNINGS</div>
        <div className="flex rounded-3xl items-center justify-between text-sm bg-gray-8 w-2/3">
          <div
            className={`w-1/2 text-center rounded-3xl py-1.5 text-xs  ${
              team === 0
                ? `border-2  border-green bg-gray-8 shadow-sm shadow-green font-medium`
                : ''
            }`}
            onClick={() => setTeam(0)}
          >
            {data.getPhaseOfInningsResolver.inningsPhase[0].teamShortName}
          </div>
          <div
            className={`w-1/2 text-center rounded-3xl py-1.5  text-xs ${
              team === 1
                ? `border-2  border-green bg-gray-8 shadow-sm shadow-green font-medium`
                : ''
            }`}
            onClick={() => setTeam(1)}
          >
            {data.getPhaseOfInningsResolver.inningsPhase &&
              data.getPhaseOfInningsResolver.inningsPhase[1].teamShortName}
          </div>
        </div>
      </div>

      {data.getPhaseOfInningsResolver.inningsPhase[team].data.length > 25 &&
        props.matchType == 'ODI' && (
          <div
            className="flex justify-between"
            style={{
              width: screenWidth - 48,
            }}
          >
            <div style={{ width: 22 }}></div>
            <div
              className={`bg-gray-4 rounded-t flex py-2 justify-between text-sm font-semibold `}
              style={{
                width: screenWidth - 75,
              }}
            >
              <div
                className={`w-1/2 pb-2 text-center ${
                  oversTab == 0
                    ? 'text-blue border-b-2 border-blue'
                    : 'text-gray-2'
                }`}
                onClick={() => (
                  setoversTab(0), setOverArr([5, 10, 15, 20, 25])
                )}
              >
                1-25 Overs
              </div>
              <div
                className={`w-1/2 pb-2 text-center ${
                  oversTab == 1
                    ? 'text-blue border-b-2 border-blue'
                    : 'text-gray-2'
                }`}
                onClick={() => (
                  setoversTab(1), setOverArr([30, 35, 40, 45, 50])
                )}
              >
                26-50 Overs
              </div>
            </div>
          </div>
        )}
      <div
        className="flex justify-between "
        style={{
          width: screenWidth - 48,
          height: 130,
        }}
      >
        <div
          className="flex flex-col-reverse text-gray-2 text-xs font-medium"
          style={{ width: 22 }}
        >
          {[1, 2, 3, 4, 5].map((val, i) => (
            <div
              key={i}
              className=""
              style={{
                height: 130 / 5,
              }}
            >
              <div className="flex justify-end ">{(yRange / 5) * (i + 1)}</div>
              <div
                className={`${
                  i === 0 ? '' : ''
                } -rotate-90 flex justify-end mt-2 ml-3`}
              >
                {i === 0 ? 'Runs' : ''}
              </div>
            </div>
          ))}
        </div>
        <div
          className=""
          style={{
            width: screenWidth - 75,
          }}
        >
          <div
            className="rounded-b bg-gray-4 flex items-end justify-start "
            style={{
              width: screenWidth - 75,
              height: 130,
              paddingBottom: 0,
            }}
          >
            {data &&
              data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                (over, i) =>
                  (props.matchType == 'ODI'
                    ? oversTab === 0 && i < 25
                    : true) && (
                    <div
                      key={i}
                      className={` bg-gray-3 rounded relative`}
                      style={{
                        width:
                          (screenWidth - 75) /
                          (props.matchType == 'T10'
                            ? 10
                            : props.matchType == 'T20'
                            ? 23
                            : oversArr.length === 7
                            ? 28
                            : 25),
                        height: (140 / 5) * (over.runs / (yRange / 5)),
                        // height:(over.runs/yRange)*275,
                        marginRight: 2.5,
                      }}
                    >
                      <div className="flex-col ">
                        {Array.apply(null, {
                          length: Number(over.wickets),
                        }).map((x, i) => (
                          <div
                            key={i}
                            className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                          />
                        ))}
                      </div>
                    </div>
                  ),
              )}
            {data &&
              data.getPhaseOfInningsResolver.inningsPhase[team].data.map(
                (over, i) =>
                  (props.matchType == 'ODI'
                    ? oversTab === 1 && i >= 25
                    : false) && (
                    <div
                      key={i}
                      className={` bg-gray-3 rounded relative`}
                      style={{
                        width:
                          (screenWidth - 75) /
                          (props.matchType == 'T10'
                            ? 10
                            : props.matchType == 'T20'
                            ? 20
                            : 30),
                        height: (140 / 5) * (over.runs / (yRange / 5)),
                        marginRight: 2.5,
                      }}
                    >
                      <div className="flex-col ">
                        {Array.apply(null, {
                          length: Number(over.wickets),
                        }).map((x, i) => (
                          <div
                            key={i}
                            className="bg-red flex rounded-full -mt-5 w-2 h-2 m-auto"
                          />
                        ))}
                      </div>
                    </div>
                  ),
              )}
          </div>

          <div className="pt-1 flex text-gray-2 text-xs font-medium">
            {oversArr.map((val, i) => (
              <div
                key={i}
                className="justify-between flex text-end"
                style={{
                  width: screenWidth - 75,
                }}
              >
                <div className={`${i === 0 ? 'pr-2' : ''}`}>
                  {i === 0 ? 'overs' : ''}
                </div>
                <div className="text-end">{val}</div>
              </div>
            ))}
          </div>
        </div>
      </div>

      <div className="flex justify-start items-center mt-8 px-2 ">
        <div className="flex items-center mr-3">
          <div className="w-2 h-2 rounded-full bg-gray-3"></div>
          <div className="text-gray-2  text-xs font-bold pl-1">
            {data.getPhaseOfInningsResolver.inningsPhase[team].teamShortName}{' '}
            {data.getPhaseOfInningsResolver.inningsPhase[team].totalRuns}/
            {data.getPhaseOfInningsResolver.inningsPhase[team].totalWickets}
          </div>
        </div>
        <div className="flex items-center ml-3 ">
          <div className="w-2 h-2 rounded-full bg-red"></div>
          <div className="text-gray-2 text-xs font-bold pl-1 ">Wicket</div>
        </div>
      </div>
    </div>
  ) : (
    <></>
  )
}
