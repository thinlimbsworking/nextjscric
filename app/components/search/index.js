import React, { useEffect, useRef, useState } from 'react'
import CleverTap from 'clevertap-react'
import { useRouter } from 'next/navigation'

function Search({
  onSearchChange,
  getSearchResult,
  style,
  className,
  placeHolder,
}) {
  let router = useRouter()

  const wrapperRef = useRef(null)
  const [focus, setfocus] = useState(false)
  const [searchData, setSearchData] = useState([])
  useEffect(() => {
    function handleClickOutside(event) {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setfocus(false)
      }
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [wrapperRef])

  function debounce(fn, delay) {
    let timeoutID
    return function (...args) {
      const context = this
      if (timeoutID) {
        clearTimeout(timeoutID)
      }
      timeoutID = setTimeout(() => {
        timeoutID = null

        fn.apply(context, args)
      }, delay)
    }
  }

  const handlechange = async (event) => {
    // alert(8)
    const { value } = event.target

    if (value.length >= 3) {
      let result = await onSearchChange(value)
      console.log('searchrslt =  ', result)
      if (result && result.length) {
        setSearchData(result)
        setfocus(true)
      }
    } else {
      setfocus(false)
      setSearchData([])
    }

    CleverTap.initialize('Teams', {
      Source: 'Team',
      Search: value,
      teamId: '',
      Platform: global.window.localStorage
        ? global.window.localStorage.Platform
        : '',
    })
  }
  const onSub = (evt) => {
    // console.log(event.target.search.value)
    evt.preventDefault()
    const { value } = event.target.search
    if (value.length >= 3 && searchData && searchData.length) {
      let [data] = searchData
      router.push(onSubmitUrl(data).href, onSubmitUrl(data).as)
    }
  }
  const debouncedSave = debounce(handlechange, 500)

  return (
    <div ref={wrapperRef}>
      {/* <form onSubmit={(evt) => onSub(evt)} className='db' > */}

      <label className=" relative">
        <input
          name="search"
          style={style}
          onChange={debouncedSave}
          type="text"
          className={className}
          aria-describedby="name-desc"
          autoComplete="off"
          placeholder={placeHolder}
        />

        {focus && searchData && searchData && searchData.length > 0 && (
          <ul
            className={`border dark:border-gray-2 border-[#E2E2E2]  ${
              searchData.length > 2 ? 'h-40' : ''
            } pl-0 overflow-y-scroll w-full absolute border-2`}
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 28,
              zIndex: 12,
            }}
          >
            {searchData.map((data, index) => getSearchResult(data, index))}
          </ul>
        )}
      </label>
      {/* </form> */}
    </div>
  )
}

export default Search
