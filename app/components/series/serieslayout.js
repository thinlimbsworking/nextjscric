import React, { useState, useEffect } from 'react'
import CleverTap from 'clevertap-react'
import Link from 'next/link'

import {
  SERIES_PRIORITY,
} from '../../api/queries'
import Loading from '../../components/loading'
import { SERIES } from '../../constant/MetaDescriptions'
//import Urltracking from '../../components/Common/urltracking';
import { useQuery } from '@apollo/react-hooks'
import { format } from 'date-fns'
import DataNotFound from '../commom/datanotfound'
import { usePathname, useRouter } from 'next/navigation'
//import ErrorBoundary, { useErrorHandler } from '../../components/ErrorBoundary';

/**
 * @description series listing with multiple tab view ex - international,domestic
 * @route /series/seriesTab
 * @param { Object } props
 * clearfliter.svg
 */


export default function Series({tab, formatType, currType, countryName, setDataList, ...props}) {
  
  let query = usePathname()
  let page = 0

  
 
  

 

  const monthsArr = [
    { January: 'jan' },
    { February: 'Feb' },
    { March: 'Mar' },
    { April: 'Apr' },
    { May: 'May' },
    { Jun: 'Jun' },
    { July: 'Jul' },
    { August: 'Aug' },
    { September: 'Sept' },
    { October: 'Oct' },
    { November: 'Nov' },
    { December: 'Dec' },
  ]

  const [currentMonth, setCurrentMonth] = useState(new Date().getMonth())
  const [pagination, setPagination] = useState(1)

  const [refresh, setRefresh] = useState(false)

  let [filterTAB, setfilterTAB] = useState(query.split('/')[2])

  // let page = router.query.page ? router.query.page : 0;

  // alert(filterCall)
  var { loading, error, data, fetchMore = function () {} } = useQuery(
    SERIES_PRIORITY,
    {
      variables: {
        tab: tab.value === 'ongoing' ? 'live' : tab.value,
        filter: currType.value,
        country: countryName.toString(),
        format:
          formatType.toString() == 'T100'
            ? 'T100'
            : formatType.toString().toLocaleLowerCase(),
        page: -1,
      },
      onCompleted: res => {
        console.log('res oncmplted --- ',res);
        setDataList(res.seriesPriority)
      },
      fetchPolicy: 'network-only',
    },
  )


    
  
 

  const handleSeriesNavigation = (match) => {
    CleverTap.initialize('Series', {
      Source: 'SeriesHome',
      SeriesID: match.seriesID,
      Platform: global.window.localStorage
        ? global.window.localStorage.Platform
        : '',
    })
  }
  const timeConvert = (timestamp) => {
    // var xx = new Date();
    // return  xx.setTime(timestamp*1000)

    var newDate = new Date()
    newDate.setTime(timestamp)
    let dateString = newDate.getDate()

    return dateString
  }

  const MonthData = (team) => {
    switch (team) {
      case 'January':
        return 'Jan'
      case 'February':
        return 'Feb'
      case 'March':
        return 'Mar'
      case 'April':
        return 'Apr'
      case 'May':
        return 'May'
      case 'Jun':
        return 'Jun'
      case 'July':
        return 'Jul'
      case 'August':
        return 'Aug'
      case 'September':
        return 'Sept'
      case 'October':
        return 'Oct'
      case 'November':
        return 'NOV'
      default:
        return 'Dec'
    }
  }

  const borderColor = (seriesName) => {
    let team = seriesName.split(' ')[0]
    switch (team) {
      case 'England':
        return 'b--black-90'
      case 'India':
        return 'b--blue'
      case 'Sri':
        return 'b--dark-green'
      case 'Afghanistan':
        return 'b--dark-green'
      case 'Pakistan':
        return 'b--dark-green'
      case 'South':
        return 'b--dark-green'
      case 'Australia':
        return 'b--yellow'
      case "Women's":
        return 'b--dark-pink'
      case 'New':
        return 'b--black-90'
      case 'West':
        return 'b--dark-red'
      case 'ICC':
        return 'b--blue'
      default:
        return 'b--black-50'
    }
  }

  let borderColorTeam = {
    all: 'b--inter',
    international: 'b--inter',
    domestic: 'completed',
  }
  // const handleScroll = async (evt) => {
  //   if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
  //     // if (evt.target.scrollTop + evt.target.clientHeight >= evt.target.scrollHeight) {
  //     setPagination((prevPage) => prevPage + 1)
  //     handlePagination(fetchMore)
  //   }
  // }

  const handlePagination = (fetchMore) => {
    fetchMore({
      variables: {
        tab: tab.value === 'ongoing' ? 'live' : tab.value,
        filter: currType.value,
        country: countryName.toString(),
        format:
          formatType.toString() == 'T100'
            ? 'T100'
            : formatType.toString().toLocaleLowerCase(),

        page: pagination,
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult
        }
        if (
          previousResult.seriesPriority.some(
            (ele) => ele.tourID === fetchMoreResult.seriesPriority?.[0]?.tourID,
          )
        )
          return previousResult
        let Priority = Object.assign({}, previousResult, {
          seriesPriority: [
            ...previousResult.seriesPriority,
            ...fetchMoreResult.seriesPriority,
          ],
        })

        return { ...Priority }
      },
    })
  }
  // window.addEventListener('scroll', handleScroll)

  return (
    <div className="w-full">
      

      { loading ? <Loading/> 
       : (
        <div className="w-full   px-2 md:mb-24 lg:mb-24">
          {/* <div className='flex w-96 center   justify-center items-center flex-col flex-wrap-l  flex-row-l p-1 md:w-full'> */}
          {data && data.seriesPriority.length > 0 ? (
            data.seriesPriority.map((item, i) => {
              let a = data.seriesPriority.map((x) => x.seriesStartDate).sort()
              return (
                <Link
                  href={`/series/${item.matchLevel}/${
                    item.tourID
                  }/${item.tourName
                    .replace(/[^a-zA-Z0-9]+/g, ' ')
                    .split(' ')
                    .join('-')
                    .toLowerCase()}/${
                    window?.innerWidth < 700 ? 'matches' : 'home'
                  }`}
                  key={i}
                >
                  <div
                    className={`cursor-pointer flex h-20 my-2 border dark:border-none text-black dark:text-white bg-white dark:bg-gray-4 rounded-md`}
                  >
                    <div className="w-full h-full  flex justify-between items-center ">
                      <div
                        className={`flex h-full w-3/12 md:w-2/12 lg:w-2/12 items-center flex-col text-xs font-bold justify-center     gap-1 `}
                      >
                        <div className="hidden md:block lg:block font-semibold">
                          {' '}
                          {format(Number(a[i]) - 19800000, 'MMMM y ')}{' '}
                        </div>
                        <div className="md:hidden lg:hidden">
                          <div className=" font-semibold">
                            {' '}
                            {format(
                              Number(item.seriesStartDate) - 19800000,
                              'MMM do ',
                            )}{' '}
                          </div>
                          <div className="text-gray-2 "> To</div>
                          <div className="font-semibold">
                            {' '}
                            {format(
                              Number(item.seriesEndDate) - 19800000,
                              'MMM do',
                            )}{' '}
                          </div>
                        </div>
                      </div>
                      <div className="h-16  border-r dark:border-black "></div>
                      <div className="w-9/12 md:w-10/12 lg:w-10/12 px-3 md:px-8 lg:px-8 h-full py-3 flex flex-col md:flex-row lg:flex-row justify-start">
                        <div className="md:w-8/12 lg:w-8/12 flex flex-col justify-center gap-3 ">
                          <div className=" text-xs font-semibold ">
                            {' '}
                            {item.tourName}{' '}
                          </div>
                          <div className={`hidden md:flex lg:flex text-xs `}>
                            <div className=" font-semibold">
                              {' '}
                              {format(Number(a[i]) - 19800000, 'MMM do')}{' '}
                            </div>
                            <div className="text-gray-2 ">-</div>
                            <div className="font-semibold">
                              {' '}
                              {format(
                                Number(item.seriesEndDate) - 19800000,
                                'MMM do',
                              )}{' '}
                            </div>
                          </div>
                        </div>
                        <div className=" py-2  md:w-4/12 lg:w-4/12 text-xs gap-4 flex flex-wrap justify-start">
                          {item.Testcount && (
                            <div className="flex gap-1 justify-center items-center">
                              <span className="font-semibold bg-gray-2 text-white w-6 h-6 rounded-full flex justify-center items-center">
                                {item.Testcount}
                              </span>
                              <span className="text-gray-2">Test</span>
                            </div>
                          )}{' '}
                          {item.Odicount && (
                            <div className="flex gap-1 justify-center items-center">
                              <span className="font-semibold bg-gray-2 text-white w-6 h-6 rounded-full flex justify-center items-center">
                                {item.Odicount}
                              </span>
                              <span className="text-gray-2">ODIs</span>
                            </div>
                          )}
                          {item.T20count && (
                            <div className="flex gap-1 justify-center items-center">
                              <span className="font-semibold bg-gray-2 text-white w-6 h-6 rounded-full flex justify-center items-center">
                                {item.T20count}
                              </span>
                              <span className="text-gray-2">T20s</span>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              )
            })
          ) : (
            <DataNotFound />
            // <div className='dark:bg-gray w-full mt2  p-4'>
            //   <img className='h-4 w-5  ' src='/pngsV2/datanotfound.png' alt='loading...' />
            //   <div className='f5 fw5 text-centerwhite pt-2'>No Series Found</div>
            // </div>
          )}
          {/* </div> */}
        </div>
      )}

      
    </div>
  )
}
