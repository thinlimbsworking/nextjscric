import React, { useState, useEffect, useContext } from 'react'
//import { useQuery } from '@apollo/react-hooks';

//import { navigate } from '@reach/router';
// import { TOP_GUNS } from '../../api/queries'
import { seriesViewStatBest } from '../../api/services'
import { useRouter } from 'next/navigation'
import Heading from '../commom/heading'
import ImageWithFallback from '../commom/Image'
import useCleverTab from '../customHooks/useCleverTab'
const backIconBlack = '/svgsV2/RightSchevronWhite.svg'
const RightSchevronBlack = '/svgs/RightSchevronBlack.svg'
const fallback = '/pngsV2/playerph.png'
export default function SeriesBattingStats(props) {
  const { pushEvent } = useCleverTab()
  const { type, format, data, seriesID, slug, seriesName, seriesType } = props

  const [value, setValue] = useState({})
  const [open, setOpen] = useState(false)
  const router = useRouter()

  let cricketType = type === 'Fielding' ? 'fielding' : type

  const handleViewNavigation = (types) => {
    // pushEvent('SeriesStats', {
    //   Source: `Series${cricketType}Stats`,
    //   StatsType: types.toLowerCase(),
    // });

    let sendData = { seriesType, seriesID, slug, type, types, format }

    router.push(seriesViewStatBest(sendData).as)
  }

  const text = {
    Highest_Score: 'HIGHEST SCORE',
    Most_Runs: 'MOST RUNS',
    Most_Fifties: 'MOST FIFTIES',
    Most_Hundreds: 'MOST HUNDREDS',
    Most_4s: 'MOST 4S',
    Most_6s: 'MOST 6S',
    Best_Strike_Rate: 'BEST STRIKE RATE',
    Best_Batting_Average: 'BEST BATTING AVERGAE',
    Most_Wickets: 'MOST WICKETS',
    Best_figures: 'BEST FIGURES',
    Five_Wicket_Haul: '5 WICKETS HAUL',
    Three_Wicket_Haul: '3 WICKETS HAUL',
    Best_Bowling_Strike_Rate: 'BEST BOWLING STRIKE RATE',
    Best_Bowling_Average: 'BEST BOWLING AVERAGE',
    Best_Economy_Rates: 'BEST ECONOMY RATES',
    Most_Catches: 'MOST CATCHES',
    Most_Run_Outs: 'MOST RUN OUTS',
    Most_Dismissals: 'MOST DISMISSALS',
  }

  const scores = {
    Highest_Score: 'highest_score',
    Most_Runs: 'runs_scored',
    Most_Fifties: 'fifties',
    Most_Hundreds: 'hundred',
    Most_4s: 'fours',
    Most_6s: 'sixes',
    Best_Strike_Rate: 'batting_strike_rate',
    Best_Batting_Average: 'average',
    Most_Wickets: 'wickets',
    Best_figures: 'best_bowling_figures',
    Five_Wicket_Haul: 'five_wickets_haul',
    Three_Wicket_Haul: 'three_wickets_haul',
    Best_Bowling_Strike_Rate: 'bowling_strike_rate',
    Best_Bowling_Average: 'best_bowling_average',
    Best_Economy_Rates: 'economy',
    Most_Catches: 'most_catches',
    Most_Run_Outs: 'most_run_outs',
    Most_Dismissals: 'most_dismissals',
  }

  return (
    <div className="dark:text-white  center bg-white rounded shadow-sm dark:bg-transparent md:p-3 lg:p-3 ">
      <div className=" flex justify-between  items-center md:hidden lg:hidden">
        <div className="w-full px-3   flex gap-2 items-center cursor-pointer">
          <div className="flex items-center justify-center p-2 bg-gray rounded-md">
            <ImageWithFallback
              height={18}
              width={18}
              loading="lazy" 
              class="flex items-center justify-center h-4 w-4 rotate-180"
              onClick={() => window.history.back()}
              src={backIconBlack}
              alt=""
            />
          </div>
          <span className="text-base font-bold   truncate">{`Series ${type} Stats`}</span>
        </div>
      </div>
      <div className="mx-2 hidden md:block lg:block w-full ">
        <Heading heading={`Series ${type} Stats`} />
      </div>
      <div className=" flex flex-wrap mt-2 p-1 ">
        {/* mobiel */}

        {data &&
          data.getStatsResolver[format] &&
          data.getStatsResolver[format][cricketType] &&
          Object.keys(data.getStatsResolver[format][cricketType]).map(
            (types, i) =>
              data.getStatsResolver[format][cricketType][types] &&
              data.getStatsResolver[format][cricketType][types].length > 0 && (
                <div key={i} className={`w-full md:w-1/3 lg:w-1/3 p-2 `}>
                  <div
                    className="border cursor-pointer dark:border-none bg-white dark:bg-gray rounded-md"
                    onClick={() => {
                      handleViewNavigation(types)
                    }}
                  >
                    <div className="flex items-center justify-between bg-light_gray dark:bg-gray-4 p-2 rounded-t-md">
                      <h2 className="text-xs font-semibold  ">{text[types]}</h2>
                      <div className="cursor-pointer">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          className="w-5 h-5"
                          src={RightSchevronBlack}
                          alt="expand"
                        />
                      </div>
                    </div>

                    <div className="flex gap-4 items-center py-4 px-3 relative">
                    <div className='absolute left-24 bottom-5 '>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-7 h-5 '
                      src={`https://images.cricket.com/teams/${data.getStatsResolver[format][cricketType][types][0].team_id}_flag_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                      <div className=" h-28 w-28  overflow-hidden rounded-full bg-gray-8">
                        <ImageWithFallback
                          height={38}
                          width={38}
                          loading="lazy"
                          fallbackSrc={fallback}
                          alt="player"
                          className="h-28 w-28 object-contain object-top"
                          src={`https://images.cricket.com/players/${data.getStatsResolver[format][cricketType][types][0].player_id}_headshot_safari.png`}
                          // onError={(evt) => (evt.target.src = fallback)}
                        />
                      </div>
                      <div className="flex flex-col gap-2  items-center w-1/2">
                        <div className="text-center">
                          <div className="text-basered dark:text-green text-xl font-semibold ">
                            {
                              data.getStatsResolver[format][cricketType][
                                types
                              ][0][scores[types]]
                            }{' '}
                            <span className="text-sm">
                              {' '}
                              {types === 'Highest_Score'
                                ? '(' +
                                  data.getStatsResolver[format][cricketType][
                                    types
                                  ][0].balls_faced +
                                  ')'
                                : ''}{' '}
                            </span>
                          </div>
                          <div className="text-gray-2 text-xs">
                            {types === 'Most_Wickets'
                              ? 'Wickets'
                              : types === 'Most_Runs'
                              ? 'Runs'
                              : types === 'Most_Fifties'
                              ? 'Fifties'
                              : types === 'Most_Hundreds'
                              ? 'Hundreds'
                              : types === 'Most_Catches'
                              ? 'Catches'
                              : types === 'Highest_Score'
                              ? `Runs`
                              : types === 'Most_4s'
                              ? 'Fours'
                              : types === 'Most_6s'
                              ? 'Sixes'
                              : types === 'Best_Batting_Average' ||
                                types === 'Best_Bowling_Average'
                              ? 'AVG'
                              : types === 'Best_Strike_Rate' ||
                                types === 'Best_Bowling_Strike_Rate'
                              ? 'S/R'
                              : types === 'Best_Economy_Rates'
                              ? 'ECO'
                              : types === 'Most_Catches'
                              ? 'Catches'
                              : ''}
                          </div>
                        </div>
                        <div className="text-center">
                          <div
                            className={` truncate  text-sm font-semibold '
                              }`}
                          >
                            {
                              data.getStatsResolver[format][cricketType][
                                types
                              ][0].player_name
                            }{' '}
                          </div>
                          <div className="text-gray-2 font-semibold text-xs">
                            {
                              data.getStatsResolver[format][cricketType][
                                types
                              ][0].team_short_name
                            }
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ),
          )}
      </div>
    </div>
  )
}
