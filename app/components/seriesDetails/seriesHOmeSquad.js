import React, { useState } from 'react'
// import { PLAYER_VIEW } from '../../api/services';
import { useRouter } from 'next/navigation'
import { PLAYER_VIEW } from '../../constant/Links'
import Tab from '../shared/Tab'
import CommonDropdown from '../commom/commonDropdown'

// import downArrow from '../../public/svgs/downArrow.svg'
// const Empty = "/svgs/Empty.svg";
// import flagPlaceHolder from "`../../public/pngsV2/flag_dark.png";

export default function SeriesSquad(props) {
  let data = props.data
  const dropDownOptns = data.getSeriesSquads.squadData.map((ele, i) => ({
    valueLable: ele.name,
    valueId: i,
  }))
  const [selected, setSelected] = useState(
    dropDownOptns[dropDownOptns.length - 1],
  )
  const matchTypes = data.getSeriesSquads.displayFormats.map((ele) => ({
    name: ele.toUpperCase(),
    value: ele,
  }))
  const router = useRouter()
  const [status, setStatus] = useState(matchTypes[0])
  const [card, setCard] = useState(
    props &&
      props.data &&
      props.data.getSeriesSquads &&
      props.data.getSeriesSquads.squadData.length > 0 &&
      props.data.getSeriesSquads.squadData.length - 1,
  )
  const [format, setFormat] = useState('')
  const [index, setIndex] = useState(
    props &&
      props.data &&
      props.data.getSeriesSquads &&
      props.data.getSeriesSquads.isNewSeries
      ? props.data.getSeriesSquads.squadData[
          props.data.getSeriesSquads.squadData.length - 1
        ].squad.findIndex((x) => x.matchType === matchTypes[0].value)
      : 0,
  )
  let localStorage = {}

  const images = {
    BATSMAN: '/pngsV2/bat-line-white.png',
    BOWLER: '/pngsV2/ball-line-white.png',
    KEEPER: '/pngsV2/keeper-line-white.png',
    ALL_ROUNDER: '/pngsV2/allrounder-line-white.png',
    NAV_ARROW: '/svgs/RightSchevronWhite.svg',
  }
  // player/name/id
  const handlePlayer = (playerId, playerSlug) => {
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href,
    }
  }

  const handlePlayerNav = (playerID, playerName) => {
    router.push(
      // handlePlayer(playerID, playerName.split(' ').join('-')).href,
      handlePlayer(playerID, playerName.split(' ').join('-')).as,
    )
  }

  const fallbackProjection = '/pngsV2/playerph.png'
  // if (error) return <div></div>;
  // if (loading)
  // 	return (
  // 		<div className='w-full vh-100 fw2 f7 gray flex justify-center items-center'>LOADING...</div>
  // 	);
  return (
    <>
      <div className="md:hidden lg:hidden">
        {data.getSeriesSquads.isNewSeries &&
          data.getSeriesSquads.displayFormats.length > 1 &&
          data.getSeriesSquads.displayFormats.length !==
            data.getSeriesSquads.disableFormats.length && (
            <div className="p-2 flex items-center justify-center  ">
              <Tab
                data={matchTypes}
                selectedTab={status}
                type="block"
                handleTabChange={(item) => {
                  data.getSeriesSquads.disableFormats.includes(item.value)
                    ? ''
                    : (setStatus(item),
                      data.getSeriesSquads.squadData.map((x, i) =>
                        x.squad.map((team, y) =>
                          team.matchType === item.value ? setIndex(y) : '',
                        ),
                      ))
                }}
              />
            </div>
          )}

        <div className=" m-2 ">
          {data &&
          data.getSeriesSquads &&
          data.getSeriesSquads.squadData.length > 0 ? (
            <div>
              {data.getSeriesSquads.squadData.map((team, i) => (
                <div key={i}>
                  <div
                    className={`bg-gray-4 mt-3 rounded-md flex justify-between p-3 cursor-pointer  ${
                      i === card ? 'rounded-none' : ''
                    } `}
                    onClick={() => setCard((prev) => (prev === i ? '' : i))}
                  >
                    <div className="flex justify-start gap-2 items-center bg-gray-8 rounded-md p-2 w-5/6">
                      <img
                        className="h-4 w-7"
                        src={`https://images.cricket.com/teams/${team.teamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = '/pngsV2/flag_dark.png'
                        }}
                        alt=""
                      />
                      <span className="text-xs font-bold">{team.name}</span>
                    </div>
                    <div className="flex items-center p-2 rounded-md bg-gray-8">
                      <img
                        className={`w-3 h-3 cursor-pointer text-white ${
                          i === card ? 'rotate-90' : ''
                        }`}
                        src={images.NAV_ARROW}
                        alt={
                          i === card
                            ? '/pngsV2/uwarrow.png'
                            : '/pngsV2/dwarrow.png'
                        }
                      />
                    </div>
                  </div>
                  <div
                    className={`p-2 bg-gray-4 ${
                      card === i ? 'block ' : 'hidden'
                    } `}
                  >
                    {team.squad[index] ? (
                      team.squad[index].playersArray.map((player, i) => (
                        <div
                          key={i}
                          onClick={() =>
                            handlePlayerNav(player.playerID, player.playerName)
                          }
                        >
                          <div className="flex  justify-center items-center p-2 cursor-pointer border-b-2 border-b-gray-2">
                            <div className="w-4/5 flex gap-2 items-center justify-start">
                              {/* <img
                                className="h-10 w-10 bg-gray-10 dark:bg-gray-8 object-cover object-top  rounded-full"
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                onError={(evt) =>
                                  (evt.target.src = '/pngsV2/playerph.png')
                                }
                                alt=""
                              /> */}
                              <span className="text-xs font-semibold">
                                {player.playerName}{' '}
                                <span className="">
                                  {player.iswk && player.iscap
                                    ? '(c) & (Wk)'
                                    : player.iswk === true
                                    ? '(Wk)'
                                    : player.iscap === true
                                    ? '(c)'
                                    : null}
                                </span>{' '}
                              </span>
                            </div>
                            <div className="w-1/5 flex items-end justify-end ">
                              <div className=" rounded-md p-1 border-2 border-gray-8">
                                <img
                                  className="w-5 h-5 "
                                  src={images[player.playerRole]}
                                  alt=""
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className="w-full p-3 bg-gray-4 shadow-4  flex justify-center item-center">
                        <p className="gray text-base">
                          Squads not yet announced
                        </p>
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <div className="flex flex-col justify-center items-center">
              <div>
                <img className="pl-3 w-8 h-8" src="/svgs/Empty.svg" alt="" />
              </div>
              <div className="gray text-base">No Squads found</div>
            </div>
          )}
        </div>
      </div>
      {/* this is for Web  */}
      <div className="hidden  md:block border md:border-none lg:border-none px-1 py-2 rounded-md">
        <div className="w-full flex items-center">
          {/* <div className="w-1/2 flex items-center justify-start text-white ">
            {data.getSeriesSquads.isNewSeries &&
              data.getSeriesSquads.displayFormats.length > 1 &&
              data.getSeriesSquads.displayFormats.length !==
                data.getSeriesSquads.disableFormats.length && (
                <div className="w-full py-3">
                  <Tab
                    data={matchTypes}
                    selectedTab={status}
                    type="block"
                    handleTabChange={(item) => {
                      data.getSeriesSquads.disableFormats.includes(item.value)
                        ? ''
                        : (setStatus(item),
                          data.getSeriesSquads.squadData.map((x, i) =>
                            x.squad.map((team, y) =>
                              team.matchType === item.value ? setIndex(y) : '',
                            ),
                          ))
                    }}
                  />
                </div>
              )}
          </div> */}
          <div className="md:w-full w-1/2 flex justify-end">
            <div className="md:w-full w-56">
              <CommonDropdown
                options={dropDownOptns}
                defaultSelected={selected}
                variant="normal"
                isFixedHeight
                onChange={(item) => {
                  console.log(
                    dropDownOptns.findIndex(
                      (ele) => ele.valueLable === item.valueLable,
                    ),
                  )
                  setCard(
                    dropDownOptns.findIndex(
                      (ele) => ele.valueLable === item.valueLable,
                    ),
                  )
                }}
              />
            </div>
          </div>
        </div>
        <div className="border-b mt-3 mx-4"></div>
        <div className=" w-full ">
          {data &&
          data.getSeriesSquads &&
          data.getSeriesSquads.squadData.length > 0 ? (
            <div className=" w-full bg-white ">
              {/* <div class='w-1/4 '>
                {data.getSeriesSquads.squadData.map((team, i) => (
                  <div
                    key={i}
                    className={`flex  gap-2 cursor-pointer ${card === i ? 'bg-gray-8 ' : ''}  p-2`}
                    onClick={() => setCard(i)}>
                    <div className='flex justify-center items-center'>
                      <img
                        className='h-5 w-8'
                        src={`https://images.cricket.com/teams/${team.teamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = '/pngsV2/flag_dark.png';
                        }}
                        alt=''
                      />
                    </div>
                    <div>
                      <span className=' text-sm font-semibold'>{team.name}</span>
                    </div>
                  </div>
                ))}
              </div> */}

              {data.getSeriesSquads.squadData
                .filter((x, i) => i === card)
                .map((team, i) => (
                  <div key={i} className="w-full">
                    <div className="w-full flex flex-wrap text-black">
                      {team.squad[index] ? (
                        team.squad[index].playersArray.map((player, i) => (
                          <div
                            key={i}
                            className="w-1/3 md:w-full py-2 md:px-0 px-8 "
                            onClick={() =>
                              handlePlayerNav(
                                player.playerID,
                                player.playerName,
                              )
                            }
                          >
                            <div
                              className={`flex ${
                                i !== team.squad[index].playersArray.length - 1
                                  ? 'border-b'
                                  : ''
                              }justify-center items-center p-2 cursor-pointer`}
                            >
                              <div className="w-4/5 flex items-center justify-start gap-2">
                                {/* <img
                                  className="h-12 w-12 bg-gray-10 dark:bg-gray-8 object-cover object-top rounded-full"
                                  src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = '/pngsV2/playerph.png')
                                  }
                                  alt=""
                                /> */}
                                <span className="f6 text-sm">
                                  {player.playerName}{' '}
                                  <span className="">
                                    {player.iswk && player.iscap
                                      ? '(c) & (Wk)'
                                      : player.iswk === true
                                      ? '(Wk)'
                                      : player.iscap === true
                                      ? '(c)'
                                      : null}
                                  </span>{' '}
                                </span>
                              </div>
                              <div className="w-1/5 flex items-end justify-end ">
                                <div className="rounded-md  bg-gray-8 p-1">
                                  <img
                                    className="w-3 h-3"
                                    src={images[player.playerRole]}
                                    alt=""
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        ))
                      ) : (
                        <div className="w-full pa4 bg-gray-4     flex justify-center item-center">
                          <p className="white f7">Squads not yet announced</p>
                        </div>
                      )}
                    </div>
                  </div>
                ))}
            </div>
          ) : (
            <div className="flex flex-col justify-center items-center">
              <div>
                <img className="w5 pl3 h5" src="/svgs/Empty.svg" alt="" />
              </div>
              <div className="gray f6 pb1">No Squads found</div>
            </div>
          )}
        </div>
      </div>
    </>
  )
}
