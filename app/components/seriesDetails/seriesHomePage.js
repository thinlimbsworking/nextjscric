import React, { useRef, useState, useEffect } from 'react'
import Scores from '../commom/score'
import Button from '../shared/button'
import Link from 'next/link'
import { usePathname, useRouter } from 'next/navigation'
import {
  GET_POINTS_TABLE,
  GET_ARTICLES_BY_ID_AND_TYPE,
  GET_SERIES_SQUADS,
} from '../../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import PointsTable from './pointsTable'
import Heading from '../shared/heading'
import SeriesArticles from './seriesArticles'
import SeriesSquad from './seriesHOmeSquad'
// GET_POINTS_TABLE
export default function SeriesHomePage(props) {
  console.log(
    'GET_ARTICLES_BY_ID_AND_TYPE_AXIOSGET_ARTICLES_BY_ID_AND_TYPE_AXIOS',
    props,
  )
  const { loading, error, data } = useQuery(GET_POINTS_TABLE, {
    variables: {
      tourID: props.seriesID,
    },
  })

  const { loading: loading2, error: error2, data: data2 } = useQuery(
    GET_ARTICLES_BY_ID_AND_TYPE,
    {
      variables: {
        Id: props.seriesID,
        type: 'series',
      },
    },
  )

  const { loading: loading3, error: error3, data: data3 } = useQuery(
    GET_SERIES_SQUADS,
    {
      variables: {
        seriesID: props.seriesID,
      },
    },
  )
  // GET_ARTICLES_BY_ID_AND_TYPE_AXIOS
  let pathname = usePathname()
  const [completedMatch, setCompletedMatch] = useState(0)
  const [upcomingMatch, setUpcomingMatch] = useState(0)
  const [realIndex, setRealIndex] = useState(0)
  const [realIndex1, setRealIndex1] = useState(0)

  const [realSwiper, setRealSwiper] = useState(0)
  const [realSwiper1, setRealSwiper1] = useState(0)

  const [selectedTabData, setSelectedTabData] = useState({
    name: 'schedule',
    label: 'schedule',
  })

  let scrl = useRef(null)
  let scrl1 = useRef(null)
  const [scrollWidth, setScrollWidth] = useState(0)
  const [scrolEnd, setscrolEnd] = useState(false)

  const [scrollX, setscrollX] = useState(0)
  const [scrollX1, setscrollX1] = useState(0)
  const [tabData, setTabData] = useState([
    { name: 'schedule', label: 'schedule' },
    { name: 'result', label: 'result' },
  ])

  const slide = (shift) => {
    // scrl.current.scrollLeft += shift;

    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: 'smooth',
    })

    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  useEffect(() => {
    if (scrl && scrl.current && scrl.current.offsetWidth) {
      setScrollWidth(scrl && scrl.current && scrl.current.offsetWidth)
    }
  }, [selectedTabData])

  const stripeCheckout = () => {
    let count = 0
    let count2 = 0
    let upArr = []
    let compArr = []

    props.data &&
      props.data.matcheslist.length &&
      props.data.matcheslist.map((x, i) => {
        if (x.matchStatus.toLowerCase() == 'completed') {
          count++
          compArr.push(x)
        } else {
          count2++
          upArr.push(x)
        }
      })
    setCompletedMatchArr(compArr)
    setUpcomingMatchArr(upArr)
    setCompletedMatch(count)
    setUpcomingMatch(count2)
  }
  const handleNavigation = (match) => {
    // alert(8)

    const event =
      match.matchStatus === 'upcoming' || match.matchStatus === 'live'
        ? 'CommentaryTab'
        : 'SummaryTab'

    // const event = match.matchStatus === 'upcoming' || match.matchStatus === 'live' ? 'CommentaryTab' : 'SummaryTab';
    let matchStatus =
      match?.matchStatus === 'completed' ? 'match-score' : 'live-score'

    let currentTab = match.isAbandoned
      ? 'commentary'
      : match.matchStatus === 'upcoming' || match.matchStatus === 'live'
      ? 'Commentary'
      : 'Summary'
    let matchName = `${
      match?.homeTeamName ? match?.homeTeamName : match?.homeTeamShortName
    }-vs-${
      match?.awayTeamName ? match?.awayTeamName : match?.awayTeamShortName
    }-${match?.matchNumber ? match?.matchNumber.split(' ').join('-') : ''}-`

    let seriesName =
      matchName.toLowerCase() +
      (match?.seriesName
        ? match.seriesName
            .replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()
        : '')

    return `/${matchStatus.toLowerCase()}/${
      match.matchID
    }/${currentTab.toLowerCase()}/${seriesName}`
  }

  if (data) {
    return (
      <div className="w-full  ">
        <div className="  bg-white  pt-4 px-6 pb-2 rounded-md   w-full">
          <div className="flex">
            <div className="flex pb-3  w-2/12">
              {tabData.map((item, index) => (
                <>
                  <div
                    onClick={() => {
                      setSelectedTabData(item)
                    }}
                    className={`flex rounded-full p-1 justify-center items-center cursor-pointer truncate  w-full m-1 ${
                      item.name === selectedTabData.name
                        ? 'text-white bg-red-6 dark:border dark:border-green dark:text-green dark:bg-transparent'
                        : 'bg-light_gray dark:bg-transparent '
                    }`}
                  >
                    <span className="font-mnr  leading-5 text-sm font-medium  capitalize">
                      {' '}
                      {item.name}{' '}
                    </span>
                  </div>
                </>
              ))}
            </div>
            {/* 
       <div className=' w-full   flex items-center    '>


{upcomingMatch !== 0 && <div className={` br2 w-25 pa2 tc mr3 pointer br-pill    ${tabName == "schedule" ? ' bg-red white' : 'black ba'}`} onClick={() => setName('schedule')}>
  <h2 className='fw5 f6'>

    Schedule</h2>
</div>
}
{completedMatch !== 0 && <div className={` br-pill   w-25 ba pa2 tc mr3 pointer     ${tabName == "result" ? ' bg-red white' : 'ba black'}`} onClick={() => setName('result')}>
  <h2 className='fw5 f6'>


    Result</h2>
</div>
}



</div> */}
          </div>
          <div className="flex  center w-full justify-center items-center relative ">
            <div
              className={`${
                scrollX < 280 ? 'opacity-100' : 'opacity-100'
              }  white pointer dn db-ns z-999 outline-0 flex  item-center justify-center absolute -left-5 z-50  cursor-pointer bg-black rounded-full `}
              onClick={() => slide(-300)}
              id="swiper-button-prev cursor-pointer"
            >
              <svg width="20" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            {}
            <div
              className="flex overflow-scroll w-full items-center "
              ref={scrl}
              onScroll={scrollCheck}
            >
              {/* props.data &&
              props.data.matcheslist.filter(opt => opt.matchStatus=="completed").map(opt => someNewObject) */}
              {props.data &&
                selectedTabData.name !== 'result' &&
                props.data.matcheslist.length > 0 &&
                props.data.matcheslist
                  .filter((opt) => opt.matchStatus !== 'completed')
                  .map((card, index) =>
                    true && index < 3 ? (
                      <>
                        <div
                          // style={{
                          //   width:
                          //     (scrl &&
                          //       scrl.current &&
                          //       scrl.current.offsetWidth) / 3,
                          // }}
                          key={card.matchID}
                          className=" mx-1  flex justify-center  w-full items-center cursor-pointer "
                        >
                          {/* <Link className='w-full' {...getContest(card.matchID)} passHref > */}
                          <div className=" w-full  cursor-pointer flex flex-col ">
                            <div className="  cursor-pointer flex flex-col shadow-md w-80 ">
                              {/* {card.matchStatus} */}

                              {card && (
                                <Link href={handleNavigation(card)}>
                                  <Scores
                                    homePage={true}
                                    featured={true}
                                    data={card}
                                    playTheOdds={false}
                                  />
                                </Link>
                              )}
                            </div>
                          </div>
                        </div>
                      </>
                    ) : (
                      index == 3 && (
                        <div className="mx-3">
                          {' '}
                          <Button
                            title={'View All'}
                            url={`/series/${props.seriesType}/${props.seriesID}/${props.slug}/matches`}
                          />
                        </div>
                      )
                    ),
                  )}

              {props.data &&
                selectedTabData.name == 'result' &&
                props.data.matcheslist.length > 0 &&
                props.data.matcheslist
                  .filter((opt) => opt.matchStatus == 'completed')
                  .map((card, index) =>
                    true && index < 3 ? (
                      <>
                        <div
                          // style={{
                          //   width:
                          //     (scrl &&
                          //       scrl.current &&
                          //       scrl.current.offsetWidth) / 3,
                          // }}
                          key={card.matchID}
                          className=" mx-1  flex justify-center  w-full items-center cursor-pointer "
                        >
                          {/* <Link className='w-full' {...getContest(card.matchID)} passHref > */}
                          <div className=" w-full  cursor-pointer flex flex-col ">
                            <div className=" w-80  cursor-pointer flex flex-col shadow-md">
                              {/* {card.matchStatus} */}

                              {card && (
                                <Link href={handleNavigation(card)}>
                                  <Scores
                                    homePage={true}
                                    featured={true}
                                    data={card}
                                    playTheOdds={false}
                                  />
                                </Link>
                              )}
                            </div>
                          </div>
                        </div>
                      </>
                    ) : (
                      index == 3 && (
                        <div className="mx-3">
                          {' '}
                          <Button
                            title={'View All'}
                            url={`/series/${props.seriesType}/${props.seriesID}/${props.slug}/matches`}
                          />
                        </div>
                      )
                    ),
                  )}
            </div>
            <div className="flex relative items-center justify-center ">
              <div
                onClick={() =>
                  slide(
                    +((scrl && scrl.current && scrl.current.offsetWidth) / 3) +
                      30,
                  )
                }
                id="swiper-button-prev"
                className="white pointer dn db-ns z-999 outline-0 flex  item-center justify-center  absolute  cursor-pointer bg-black rounded-full left-0 "
              >
                <svg width="20" viewBox="0 0 24 24">
                  <path
                    fill="#fff"
                    d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
            </div>
          </div>
        </div>

        <div className="flex w-full mt-5 items-start justify-center">
          <div className="flex flex-col w-full">
            <div className="w-full bg-white  flex flex-col shadow-md m-1 p-4">
              <div className="">
                <Heading heading={'LATEST NEWS '} />
              </div>

              {data2 && (
                <SeriesArticles
                  props={props}
                  url={
                    props &&
                    `/series/${props.seriesType}/${props.seriesID}/${props.slug}/news`
                  }
                  data={data2 && data2}
                  seriesHomePage={true}
                />
              )}
            </div>
            <div className="w-full bg-white  flex flex-col shadow-md m-1 p-4">
              <div className="">
                <Heading heading={'POINTS TABLES'} />
              </div>

              <PointsTable data={data} />
            </div>
          </div>
          <div className="w-5/12 m-1">
            <div className="w-full bg-white flex flex-col shadow rounded-md mx-4 my-1 p-4">
              <div className="">
                <Heading heading={'Squad'} />
              </div>

              {data3 && (
                <SeriesSquad
                  props={props}
                  url={
                    props &&
                    `/series/${props.seriesType}/${props.seriesID}/${props.slug}/news`
                  }
                  data={data3 && data3}
                  seriesHomePage={true}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
