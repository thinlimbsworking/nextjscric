import React, { useEffect, useRef } from 'react'
import CleverTap from 'clevertap-react'
import format from 'date-fns/format'
import Scores from '../commom/score'
import { scheduleMatchView } from '../../api/services'
import Link from 'next/link'

export default function SeriesMatches({ data, ...props }) {
  const refList = useRef([])

  useEffect(() => {
    let curr = data.matcheslist.findLast(e => e.matchStatus === 'completed')?.matchID;
    if(curr && (window.innerWidth < 700 || (data.matcheslist.length > 9 && window.innerHeight > 700))) {
      refList.current[curr]?.scrollIntoView({behaviour:'smooth'})
    }
  }, [])
  const handleNavigation = (match) => {
    const event =
      match.matchStatus === 'upcoming'
        ? 'CommentaryTab'
        : (match.matchStatus === 'upcoming' || match.matchStatus === null) &&
          match.playing11Status === false &&
          match.probable11Status === false
        ? 'matchInfoTab'
        : match.matchStatus === 'completed'
        ? 'ScorecardTab'
        : 'CommentaryTab'
    CleverTap.initialize(event, {
      Source: 'Series',
      MatchID: match.matchID,
      SeriesID: match.seriesID,
      TeamAID: match.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: match.matchStatus,
      Platform: localStorage.Platform,
    })
  }

  function getSeriesView(match, tabName) {
    let currentTab =
      match.matchStatus === 'upcoming' &&
      match.playing11Status === false &&
      match.probable11Status === true
        ? 'probableplaying11'
        : match.matchStatus === 'upcoming' &&
          match.playing11Status === true &&
          match.probable11Status === false
        ? 'playing11'
        : match.matchStatus === 'upcoming' &&
          match.playing11Status === true &&
          match.probable11Status === true
        ? 'playing11'
        : (match.matchStatus === 'upcoming' || match.matchStatus === null) &&
          match.playing11Status === false &&
          match.probable11Status === false
        ? 'matchInfo'
        : match.matchStatus === 'completed'
        ? 'scorecard'
        : 'live'
    return scheduleMatchView(match)
  }

  return (
    <div className="flex  flex-wrap items-center justify-start px-1 md:p-4 lg:p-4 bg-white dark:bg-transparent rounded shadow-sm">
      {data.matcheslist?.length ? (
        data.matcheslist.map((x, i) => (
          <div
            ref={(el) => (refList.current[x.matchID] = el)}
            key={i}
            className="w-full md:w-1/3 lg:w-1/3 overflow-hidden  flex justify-center  items-center"
            onClick={() => handleNavigation(x)}
          >
            <Link {...getSeriesView(x)} passHref legacyBehavior>
              <div className=" w-full flex flex-col cursor-pointer p-2 ">
                {x && x.matchID ? (
                  <Scores
                    featured={true}
                    data={x}
                    from={'series'}
                    // hideSeriesName={true}
                    {...props}
                  />
                ) : (
                  <></>
                )}
              </div>
            </Link>
          </div>
        ))
      ) : (
        <></>
      )}
    </div>
  )
}
