import React, { useState } from 'react';
// import ellipse from "`../../public/svgs/Ellipse.svg";
// import SeriesBattingStats from "../SeriesDetails/SeriesBattingStats";
// import fielding from "`../../public/svgs/fielding.svg";
// import redCap from "`../../public/svgs/rCap.svg";
// import purpleCap from "`../../public/svgs/pCap.svg";
// import { TOP_GUNS } from "../../api/queries";
// const Empty = "/svgs/Empty.svg";

import { useRouter } from 'next/navigation';
import ImageWithFallback from '../commom/Image';
import { seriesViewStat, getSeries } from '../../api/services';
import { Background } from 'react-imgix';
import Heading from '../commom/heading';
import useCleverTab from '../customHooks/useCleverTab';
import Tab from '../shared/Tab';
const IMAGES = {
  fallback: '/placeHodlers/playerAvatar.png',
  circle: '/svgs/stats-circle.svg'
};

export default function SeriesStats(props) {
  const {pushEvent} = useCleverTab();
  const { seriesID, path, seriesType, slug, seriesName, data, error } = props;
  let formatTabs = data.getStatsResolver.formatArray.map(ele => ({name: ele.toUpperCase(), value:ele}))
  console.log('props in stats - ', data);
  let matchType;
  if (
    data &&
    data.getStatsResolver &&
    data.getStatsResolver.formatArray &&
    data.getStatsResolver.formatArray.length > 0
  ) {
    matchType = data.getStatsResolver.formatArray[0];
  }

  let [type, setType] = useState(formatTabs[0]);
  const router = useRouter(data.getStatsResolver.formatArray[0].value);

  const handleViewNavigation = (types, view) => {
    // pushEvent('SeriesStats', {
    //   Source: 'SeriesStatsHome',
    //   StatsType: view,
    // });
    let sendData = {
      seriesID,
      seriesType,
      seriesID,
      type:type.value,
      types,
      slug
    };
    router.push(`${seriesViewStat(sendData).as}/${view}`);
  };
  const handleNavigation = (types) => {
    // pushEvent(`Series${types}Stats`, {
    //   Source: 'SeriesStatsHome',
    // });
    let sendData = {
      seriesID,
      seriesType,
      seriesID,
      type: type.value,
      types,
      slug
    };

    router.push(seriesViewStat(sendData).as);
  };

  if (error)
    return <div className='w-full  text-sm font-semibold bg-gray flex justify-center items-center'>Something went wrong.</div>;
  return (
    <div className='w-full'>
      {/* <div className="bg-series-stats w-full"> */}
      <div className=' w-full z-1 p-1 bg-white dark:bg-transparent rounded shadow-sm md:p-3 lg:p-3'>
        {data.getStatsResolver && data.getStatsResolver.formatArray && data.getStatsResolver.formatArray.length > 1 && (
          <div className='w-full md:w-1/2 lg:w-1/2 pt-2  pb-4'>
            {/* { data.getStatsResolver.formatArray.map((tabName, i) => */}
            <Tab data={formatTabs} selectedTab={type} type='block' handleTabChange={tabName => {data && data.getStatsResolver[tabName.value] && setType(tabName)}}/>
            {/* {data.getStatsResolver.formatArray.map((tabName, i) => (
              <span
                key={i}
                className={` cursor-pointer p-1 font-semibold rounded-full full w-1/${data.getStatsResolver.formatArray.length} text-sm font-semibold text-center uppercase  ${type === tabName ? 'bg-gray-8 border border-green-3' : ''
                  }`}
                onClick={() => {
                  data && data.getStatsResolver[tabName] && setType(tabName);
                }}>
                {tabName}
              </span>
            ))} */}
          </div>
        )}
        {data && data.getStatsResolver && data.getStatsResolver[type.value] && data.getStatsResolver[type.value].topGuns && <div className='px-2'> <Heading heading='Top Guns' /></div>}

        {data && data.getStatsResolver && data.getStatsResolver[type.value] && data.getStatsResolver[type.value].topGuns ? (
          <div className='flex  flex-wrap justify-center '>
            {/* <div className="flex w-full justify-around"> */}
            {data.getStatsResolver[type.value].topGuns.Most_Runs && (
              <div className='w-1/2 h-48 p-2 my-2 md:w-1/4 lg:w-1/4'>
                <div
                  className='border dark:border-none bg-white dark:bg-gray relative  rounded-md   flex flex-col gap-2 items-center py-2 cursor-pointer'
                  onClick={() => {
                    handleViewNavigation('Batting', 'most_runs');
                  }}>
                  <div className='absolute right-8 bottom-24 md:right-[66px] lg:right-[66px]'>
                   <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-7 h-5 '
                      src={`https://images.cricket.com/teams/${data.getStatsResolver[type.value].topGuns.Most_Runs.team_id}_flag_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div
                    className=' p-2  w-24 h-24 overflow-hidden  rounded-full bg-light_gray dark:bg-gray-8 flex items-center justify-center  '>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-24 h-24 object-top object-contain'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type.value].topGuns.Most_Runs.player_id}_headshot_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div className='text-center text-sm font-bold'>
                    {data.getStatsResolver[type.value].topGuns.Most_Runs.player_name}
                  </div>
                  <div className='text-center'>
                    <div className=' text-xs text-gray-2 '>Most Runs</div>
                    <div className='text-xl font-semibold text-center text-basered dark:text-green'>{data.getStatsResolver[type.value].topGuns.Most_Runs.runs_scored}</div>
                  </div>

                </div>
              </div>
            )}


            {data.getStatsResolver[type.value].topGuns.Most_Wickets && (
              <div className='w-1/2 h-48 p-2 my-2 md:w-1/4 lg:w-1/4'>
                <div
                  className='border dark:border-none bg-white dark:bg-gray relative  rounded-md   flex flex-col gap-2 items-center py-2 cursor-pointer'
                  onClick={() => {
                    handleViewNavigation('Bowling', 'most_wickets');
                  }}>
                  <div className='absolute right-8 bottom-24 md:right-[66px] lg:right-[66px]'>
                   <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-7 h-5 '
                      src={`https://images.cricket.com/teams/${data.getStatsResolver[type.value].topGuns.Most_Wickets.team_id}_flag_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div
                    className=' p-2  w-24 h-24 overflow-hidden  rounded-full bg-light_gray dark:bg-gray-8 flex items-center justify-center  '>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                      fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-24 h-24 object-top object-contain'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type.value].topGuns.Most_Wickets.player_id}_headshot_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div className='text-center w-full text-sm font-bold'>
                    {data.getStatsResolver[type.value].topGuns.Most_Wickets.player_name}
                  </div>
                  <div className='text-center'>
                    <div className=' text-xs text-gray-2 '>Most Wickets</div>
                    <div className='text-xl font-semibold text-center text-basered dark:text-green'>{data.getStatsResolver[type.value].topGuns.Most_Wickets.wickets}</div>
                  </div>

                </div>
              </div>
            )}

            {data.getStatsResolver[type.value].topGuns.Highest_Score && (
              <div className='w-1/2 h-48 p-2 my-2 md:w-1/4 lg:w-1/4'>
                <div
                  className='border dark:border-none bg-white dark:bg-gray relative  rounded-md   flex flex-col gap-2 items-center py-2 cursor-pointer'
                  onClick={() => {
                    handleViewNavigation('Batting', 'highest_score');
                  }}>
                  <div className='absolute right-8 bottom-24 md:right-[66px] lg:right-[66px]'>
                   <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-7 h-5 '
                      src={`https://images.cricket.com/teams/${data.getStatsResolver[type.value].topGuns.Highest_Score.team_id}_flag_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div
                    className=' p-2  w-24 h-24 overflow-hidden  rounded-full bg-light_gray dark:bg-gray-8 flex items-center justify-center  '>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                      fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-24 h-24 object-top object-contain'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type.value].topGuns.Highest_Score.player_id}_headshot_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div className='text-center text-sm font-bold'>
                    {data.getStatsResolver[type.value].topGuns.Highest_Score.player_name}
                  </div>
                  <div className='text-center'>
                    <div className=' text-xs text-gray-2 '>Highest Score</div>
                    <div className='text-xl font-semibold text-center text-basered dark:text-green'>{data.getStatsResolver[type.value].topGuns.Highest_Score.highest_score}</div>
                  </div>

                </div>
              </div>
            )}


            {data.getStatsResolver[type.value].topGuns.Best_figures && (
              <div className='w-1/2 h-48 p-2 my-2 md:w-1/4 lg:w-1/4'>
                <div
                  className='border dark:border-none bg-white dark:bg-gray relative  rounded-md   flex flex-col gap-2 items-center py-2 cursor-pointer'
                  onClick={() => {
                    handleViewNavigation('Bowling', 'best_figures');
                  }}>
                  <div className='absolute right-8 bottom-24 md:right-[66px] lg:right-[66px]'>
                   <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-7 h-5 '
                      src={`https://images.cricket.com/teams/${data.getStatsResolver[type.value].topGuns.Best_figures.team_id}_flag_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div
                    className=' p-2  w-24 h-24 overflow-hidden  rounded-full bg-light_gray dark:bg-gray-8 flex items-center justify-center  '>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                      fallbackSrc='/placeHodlers/playerAvatar.png'
                      className='w-24 h-24 object-top object-contain'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type.value].topGuns.Best_figures.player_id}_headshot_safari.png`}
                      // onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt='player'/>
                  </div>
                  <div className='text-center text-sm font-bold'>
                    {data.getStatsResolver[type.value].topGuns.Best_figures.player_name}
                  </div>
                  <div className='text-center'>
                    <div className=' text-xs text-gray-2 '>Most Wickets</div>
                    <div className='text-xl font-semibold text-center text-basered dark:text-green'>{data.getStatsResolver[type.value].topGuns.Best_figures.best_bowling_figures}</div>
                  </div>

                </div>
              </div>
            )}

          </div>
        ) : (
          <div className='flex  justify-center items-center flex-column my-4 w-full bg-gray text-white'>
            <ImageWithFallback height={88} width={88} loading='lazy'
                fallbackSrc='/placeHodlers/playerAvatar.png' className='w-48 h-48' src='/svgs/Empty.svg' alt='empty' />
            <p className='text-base font-semibold text-center'>Series Stats will be available once the series commences</p>
          </div>
        )}
      </div>

      <div className='mt-4 w-full p-1  flex flex-wrap bg-white dark:bg-transparent rounded shadow-sm md:p-3 lg:p-3'>
        {data && data.getStatsResolver && data.getStatsResolver[type.value] && data.getStatsResolver[type.value].Batting &&
        <div className={`w-full md:w-1/${Object.keys(data.getStatsResolver[type.value]).filter(ele => data.getStatsResolver[type.value][ele]).length-1} p-2 lg:w-1/${Object.keys(data.getStatsResolver[type.value]).filter(ele => data.getStatsResolver[type.value][ele]).length-1} p-2`}>
          <div
            className={`border bg-white dark:border-none dark:bg-gray flex rounded-md justify-between w-full items-center p-2 cursor-pointer`}
            // style={{ borderColor: '#C63935' }}
            onClick={() => {
              data && data.getStatsResolver && data.getStatsResolver[type.value] && handleNavigation('Batting');
            }}>
            <div className='flex bg-light_gray dark:bg-gray-8 items-center  w-10/12 rounded-md p-2 gap-4'>
              <div className='flex justify-center items-center bg-gray rounded-md p-1'>
                <ImageWithFallback height={18} width={18} loading='lazy'
                   className=' w-5 h-5 ' src='/svgs/Bat_Icon.svg' alt='bat icon' />
              </div>
              <div className='text-sm text-left font-semibold'>Batting</div>
            </div>
            <div className='bg-light_gray dark:bg-gray-8  rounded-md p-2 cursor-pointer'>
              <ImageWithFallback height={18} width={18} loading='lazy'
                   className='w-5 h-5' src='/svgs/RightSchevronBlack.svg' alt='right'/>
            </div>
          </div>
          </div>
        }

        {data && data.getStatsResolver && data.getStatsResolver[type.value] && data.getStatsResolver[type.value].Bowling &&
        <div className={`w-full md:w-1/${Object.keys(data.getStatsResolver[type.value]).filter(ele => data.getStatsResolver[type.value][ele]).length-1} p-2 lg:w-1/${Object.keys(data.getStatsResolver[type.value]).filter(ele => data.getStatsResolver[type.value][ele]).length-1} p-2`}>
          <div
          className={`border bg-white dark:border-none dark:bg-gray flex rounded-md justify-between w-full items-center p-2 cursor-pointer`}
            // style={{ borderColor: '#C63935' }}
            onClick={() => {
              data && data.getStatsResolver && data.getStatsResolver[type.value] && handleNavigation('Bowling');
            }}>
            <div className='flex bg-light_gray dark:bg-gray-8  items-center  w-10/12 rounded-md p-2 gap-4'>
              <div className='flex justify-center items-center bg-gray rounded-md p-1'>
                <ImageWithFallback height={18} width={18} loading='lazy'
                 className=' w-5 h-5 ' src='/svgs/bowling.svg' alt='bat icon' />
              </div>
              <div className='text-sm text-left font-semibold'>Bowling</div>
            </div>
            <div className='bg-light_gray dark:bg-gray-8  rounded-md p-2 cursor-pointer'>
              <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc='/placeHodlers/playerAvatar.png' className='w-5 h-5' src='/svgs/RightSchevronBlack.svg' alt='right'/>
            </div>
          </div>
        </div>
        }

        {data &&
          data.getStatsResolver &&
          data.getStatsResolver.tourType &&
          data.getStatsResolver.tourType.length > 0 &&
          data.getStatsResolver.tourType === 'Tournament' && data && data.getStatsResolver && data.getStatsResolver[type.value] && data.getStatsResolver[type.value].fielding && (
            <div className={`w-full md:w-1/${Object.keys(data.getStatsResolver[type.value]).filter(ele => data.getStatsResolver[type.value][ele]).length-1} p-2 lg:w-1/${Object.keys(data.getStatsResolver[type.value]).filter(ele => data.getStatsResolver[type.value][ele]).length-1} p-2`}>
            <div
            className={`border dark:border-none bg-white dark:bg-gray flex rounded-md justify-between w-full items-center p-2 cursor-pointer`}
              // style={{ borderColor: '#C63935' }}
              onClick={() => {
                data && data.getStatsResolver && data.getStatsResolver[type.value] && handleNavigation('Fielding');
              }}>
              <div className='flex bg-light_gray dark:bg-gray-8  items-center  w-10/12 rounded-md p-2 gap-4'>
                <div className='flex justify-center items-center bg-gray rounded-md p-1'>
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    className=' w-5 h-5 ' src='/svgs/fielding.svg' alt='bat icon' />
                </div>
                <div className='text-sm text-left font-semibold'>Fielding</div>
              </div>
              <div className='bg-light_gray dark:bg-gray-8  rounded-md p-2 cursor-pointer'>
                <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png' className='w-5 h-5' src='/svgs/RightSchevronBlack.svg' alt='right'/>
              </div>
            </div>
            </div>
          )}
      </div>
    </div>
  );
}