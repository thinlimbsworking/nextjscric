import React from 'react';
import { getStadiumUrl } from '../../api/services';
import CleverTap from 'clevertap-react';
import ImageWithFallback from '../commom/Image';

import Link from 'next/link';
export default function SeriesVenues(props) {
  const { error, loading, data } = props;
  const getUrl = (stadium) => {
    let id = stadium.venueId;
    let name = stadium.stadiumName;
    return getStadiumUrl({ id, name });
  };

  const handleVenueCleverTap = (venueID) => {
    CleverTap.initialize('Stadiums', {
      Source: 'SeriesStadiumsHome',
      VenueID: venueID,
      Platform: localStorage ? localStorage.Platform : ''
    });
  };
  if (error) return <div></div>;
  if (loading) return <div className='w-full   gray flex justify-center items-center'>LOADING...</div>;
  return (
    <div className='mx-3 md:mx-1 lg:mx-1'>
      {data && data.venuedetails.length > 0 ? (
        <div className='dark:text-white   '>
          {data.venuedetails &&
            data.venuedetails.map((x, i) => (
              <div key={i} className='bg-white dark:bg-gray-4 border dark:border-none rounded-md mt-3 p-2.5 '>
                <Link
                  key={i}
                  {...getUrl(x)}
                  passHref
                  className=' flex justify-between items-center '
                  onClick={() => handleVenueCleverTap(x.venueId)}>

                  <div className='text-sm p-2  dark:bg-gray-8 rounded-md w-5/6'>{x.stadiumName}</div>
                  <span className='bg-light_gray dark:bg-gray-8 p-2 rounded-md gray'> <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc='/placeHodlers/playerAvatar.png' className='w-5 h-5' src='/svgs/RightSchevronBlack.svg' alt='right'/> </span>

                </Link>
              </div>
            ))}
        </div>
      ) : (
        <div className='flex flex-col bg-white justify-center mx-3   items-center'>
          <div>
            <img className='w-5 h-5' src='/svgs/Empty.svg' alt='' />
          </div>
          <div className='gray  pb-1'>No Venue found</div>
        </div>
      )}
    </div>
  );
}
