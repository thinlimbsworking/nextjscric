import React from 'react';

import { TOP_GUNS } from '../../api/queries';

import { useRouter } from 'next/navigation';
import ImageWithFallback from '../commom/Image';
import { seriesViewStatBest } from '../../api/services';
import Heading from '../commom/heading';


import useCleverTab from '../customHooks/useCleverTab';

const RightSchevronBlack = '/svgs/RightSchevronBlack.svg';
const left = '/svgs/left.svg';
const right = '/svgs/right.svg';
const fallbackProjection = '/pngs/fallbackprojection.png';
const fallback = '/placeHodlers/playerAvatar.png';
const backIconBlack = '/svgsV2/RightSchevronWhite.svg';


export default function SeriesView(props) {
  const {pushEvent} = useCleverTab();
  var { type, format, slug, seriesID, scoretype, data, seriesType } = props;
  console.log('format --- ', format);

  // seriesID={props.seriesID}
  //    seriesType={props.type}

  //    slug={props.seriesSlug}

  //     type={props.slugs[5]}
  //      format={props.slugs[4]}
  //      scoretype={props.slugs[6]}

  //      data={props.comp.data}
  // scoretype=scoretype.toLowerCase();

  const router = useRouter();

  let cricketType = type;

  type = type?.toLowerCase();

  const categories = {
    Most_Wickets: {
      heading:  'Most Wickets',
      score: 'overs',
      stats: ['R', 'player', 'match', 'Wkt', 'overs'],
    },
    Highest_Score: {
      heading:  'Highest Score',
      score: 'vs_team_short_name',
      stats: ['R', 'player', 'Highest Score', 'opponent'],
    },
    Most_Runs: {
      heading:  'Most Runs',
      score: 'innings_played',
      stats: ['R', 'player', 'match', 'inns', 'runs'],
    },
    Most_Fifties: {
      heading:  'Most fifties',
      score: 'fifties',
      stats: ['R', 'player', 'match', 'runs', '50s'],
    },
    Most_Hundreds: {
      heading:  'Most hundreds',
      score: 'hundred',
      stats: ['R', 'player', 'match', 'runs', '100s'],
    },
    Most_4s: {
      heading:  'Most 4s',
      score: 'fours',
      stats: ['R', 'player', 'match', 'runs', '4s'],
    },
    Most_6s: {
      heading:  'Most 6s',
      score: 'sixes',
      stats: ['R', 'player', 'match', 'runs', '6s'],
    },
    Best_Batting_Average: {
      heading:  'Batting Average',
      score: 'average',
      stats: ['R', 'player', 'match', 'runs', 'Avg.'],
    },
    Best_Strike_Rate: {
      heading:  'Best Strike-Rite',
      score: 'batting_strike_rate',
      stats: ['R', 'player', 'match', 'runs', 's/r'],
    },
    Best_figures: {
      heading:  'Best Figures',
      score: 'vs_team_short_name',
      stats: ['R', 'player', 'best', 'opponent'],
    },
    Five_Wicket_Haul: {
      heading:  'Five Wicket Haul',
      score: 'five_wickets_haul',
      stats: ['R', 'player', 'match', 'Wkt', '5W'],
    },
    Three_Wicket_Haul: {
      heading:  '3W Haul',
      score: 'three_wickets_haul',
      stats: ['R', 'player', 'match', 'Wkt', '3W'],
    },
    Best_Bowling_Average: {
      heading:  'Bowling Average',
      score: 'best_bowling_average',
      stats: ['R', 'player', 'match', 'Wkt', 'Avg'],
    },
    Best_Economy_Rates: {
      heading:  'Bowling Economy Rates',
      score: 'economy',
      stats: ['R', 'player', 'match', 'Wkt', 'E/R'],
    },
    Best_Bowling_Strike_Rate: {
      heading:  'Bowling Strike Rate',
      score: 'bowling_strike_rate',
      stats: ['R', 'player', 'match', 'Wkt', 'S/R'],
    },
    Most_Catches: {
      heading:  'Catches',
      score: 'most_catches',
      stats: ['R', 'player', 'match', 'catches'],
    },
    Most_Run_Outs: {
      heading:  'Run-Outs',
      score: 'most_run_outs',
      stats: ['R', 'player', 'match', 'R/O'],
    },
    Most_Dismissals: {
      heading:  'Dismissals',
      score: 'most_dismissals',
      stats: ['R', 'player', 'match', 'dismissals'],
    }
  }

  let caseHandle = {
    most_runs: 'Most_Runs',
    highest_score: 'Highest_Score',
    most_fifties: 'Most_Fifties',
    most_hundreds: 'Most_Hundreds',
    most_4s: 'Most_4s',
    most_6s: 'Most_6s',
    best_batting_average: 'Best_Batting_Average',
    best_strike_rate: 'Best_Strike_Rate',
    most_wickets: 'Most_Wickets',
    best_figures: 'Best_figures',
    five_wicket_haul: 'Five_Wicket_Haul',
    three_wicket_haul: 'Three_Wicket_Haul',
    best_bowling_average: 'Best_Bowling_Average',
    best_economy_rates: 'Best_Economy_Rates',
    best_bowling_strike_rate: 'Best_Bowling_Strike_Rate',
    most_catches: 'Most_Catches',
    most_run_outs: 'Most_Run_Outs',
    most_dismissals: 'Most_Dismissals'
  };




  const scores = {
    Most_Runs: 'innings_played',
    Highest_Score: 'vs_team_short_name',
    Most_Fifties: 'fifties',
    Most_Hundreds: 'hundred',
    Most_4s: 'fours',
    Most_6s: 'sixes',
    Best_Batting_Average: 'average',
    Best_Strike_Rate: 'batting_strike_rate',
    Best_figures: 'vs_team_short_name',
    Five_Wicket_Haul: 'five_wickets_haul',
    Three_Wicket_Haul: 'three_wickets_haul',
    Best_Bowling_Average: 'best_bowling_average',
    Best_Economy_Rates: 'economy',
    Best_Bowling_Strike_Rate: 'bowling_strike_rate',
    Most_Catches: 'most_catches',
    Most_Run_Outs: 'most_run_outs',
    Most_Dismissals: 'most_dismissals',
    Most_Wickets: 'overs'
  };
  const colorMap = {
    IND: '#0062A7',
    AUS: '#E8BF00',
    WI: '#890633',
    PAK: '#2A562C',
    ENG: '#6BB9DE',
    NZ: '#0B0F0E',
    SA: '#006F5C',
    SL: '#20447A',
    AFG: '#1049C3',
    BAN: '#00956C',
    MI: '#1473AD',
    KXIP: '#B32229',
    CSK: '#FCC032',
    RCB: '#1D1D1D',
    DC: '#D41C27',
    SRH: '#F7A34F',
    RR: '#F064A1',
    RCB: '#39235C'
  };
  let array = [];

  const uppercase = (str) => {
    var array1 = str.split('_');
    var newarray1 = [];

    for (var x = 0; x < array1.length; x++) {
      newarray1.push(array1[x].charAt(0).toUpperCase() + array1[x].slice(1));
    }
    return newarray1.join('_');
  };

  if (data && data?.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType]) {
    let category = Object.keys(data?.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType]);
    category.map((nav, i) => {
      if (
        data?.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType][nav] &&
        data?.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType][nav].length > 0
      ) {
        array.push(nav.toLowerCase());
      }
    });
  }

  let currentIndex = array.indexOf(scoretype.toLowerCase());
  const changeIndexForward = () => {
    let types = uppercase(array[currentIndex === array.length - 1 ? 0 : currentIndex + 1]);

    const name = type;
    const typeCapitalized = name.charAt(0).toUpperCase() + name.slice(1);
    type = typeCapitalized;

    let sendData = { seriesType, seriesID, slug, type, format, types };

    router.push(seriesViewStatBest(sendData).as);
    //  pushEvent('SeriesStats', {
    //   Source: `Series${cricketType}Stats`,
    //   StatsType: array[currentIndex === array.length - 1 ? 0 : currentIndex + 1],
    // });
  };
  const changeIndexBackward = () => {
    let types = uppercase(array[currentIndex === 0 ? array.length - 1 : currentIndex - 1]);

    const name = type;
    const typeCapitalized = name.charAt(0).toUpperCase() + name.slice(1);
    type = typeCapitalized;

    let sendData = { seriesType, seriesID, slug, type, format, types };

    router.push(seriesViewStatBest(sendData).as);

    // pushEvent('SeriesStats', {
    //   Source: `Series${cricketType}Stats`,
    //   StatsType: array[currentIndex === 0 ? array.length - 1 : currentIndex - 1],
    // });
  };


  const camelCase = (text) => {
    if(text === 'best_figures') return 'Best_figures'
     return text.split('_').map(ele => ele[0].toUpperCase() + ele.slice(1)).join('_');
  }



  return (
    <div className='px-2   dark:text-white bg-white dark:bg-transparent rounded shadow-sm md:p-3 lg-p-3'>
      <div className='p-1 flex gap-2 md:hidden lg:hidden items-center '>
        <div className='flex justify-center items-center cursor-pointer bg-gray p-2 rounded-md'>
          <ImageWithFallback height={18} width={18} loading='lazy'
             class="flex items-center justify-center h-4 w-4 rotate-180" onClick={() => window.history.back()} src={backIconBlack} alt='back icon' />
        </div>
        <span className='text-base font-bold truncate'>{`Series ${cricketType} Stats`}</span>
      </div>

      {/* <div className='hidden md:block lg:block w-full p-2 text-xl font-semibold'>
         {`Series ${cricketType} Stats`}
      </div> */}

      <div className='w-full p-1'>
        <div className='  w-full flex items-center justify-between'>
          <Heading heading = {categories[camelCase(scoretype.toLowerCase())].heading} />
          <div className='flex gap-3 cursor-pointer'>
            <div className='rotate-180' onClick={() => {changeIndexBackward()}}>
              <svg width="30" viewBox="0 0 24 24">
                <path
                  fill="#38d925"
                  d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <div onClick={() => {changeIndexForward()}}>
              <svg width="30" viewBox="0 0 24 24">
                <path
                  fill="#38d925"
                  d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
          </div>
        </div>
      <div className='dark:bg-gray rounded-md  my-2'>
        <div className='w-full flex bg-gray-4 text-white py-3 rounded-t-md justify-between'>
          {categories[camelCase(scoretype.toLowerCase())].stats.map((text, i) => (
              <div
                className={`capitalize text-sm mr-1 truncate ${i !== 1 ? 'text-center block' : ''}`}
                style={{
                  width : `${i === 0 ? '10%' : i===1 ? '48%' : categories[camelCase(scoretype.toLowerCase())].stats.length > 4 ? '14%': '21%'}`
                }}
                key={i}>
                {text}
              </div>
            ))}
        </div>

        {type === 'batting' &&
          data?.getStatsResolver[format][cricketType][camelCase(scoretype.toLowerCase())].map((scoreType, i) => (
            <>
            <div className='w-full'>
              <div
                className={`flex  text-sm font-semibold  items-center ${i===0 && 'bg-basered  dark:bg-gray text-white'}  `}
                key={i}>
                <div style={{width: '10%'}} className={`  pl-2`}>{i + 1}</div>
                <div style={{width: '48%'}} className={`flex gap-2 items-center py-2`}>
                  <div
                    className={` h-12 w-12  rounded-full bg-gray-10 dark:bg-gray-8 overflow-hidden`}>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc={fallbackProjection}
                      className='w-12 h-12 object-cover object-top'
                      src={`https://images.cricket.com/players/${scoreType.player_id}_headshot_safari.png`}
                      alt=''
                      // onError={(e) => (e.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className=' flex flex-col justify-center mr-3'>
                    <div className=' text-sm font-semibold   '>{scoreType.player_name}</div>
                    <div className={`text-xs text-gray-2 `}>{scoreType.team_short_name}</div>
                  </div>
                </div>

                <div
                  className={`flex flex-col  text-sm  justify-center font-semibold text-center  ${ camelCase(scoretype.toLowerCase()) === 'Highest_Score' ? 'text-green text-lg' : ''}}`}
                  style={{width : `${ camelCase(scoretype.toLowerCase()) === 'Highest_Score' ? '21%' : '14%'}`}}>
                  <span>{camelCase(scoretype.toLowerCase()) === 'Highest_Score' ? scoreType.highest_score : scoreType.matches_played}</span>
                 
                  <span>{camelCase(scoretype.toLowerCase()) === 'Highest_Score' && (
                    `(${scoreType.balls_faced})`
                  )}</span>
                </div>
                <div
                  className={` flex items-center gap-2 justify-center text-center  `}
                  style={{width : `${ camelCase(scoretype.toLowerCase()) === 'Highest_Score' ? '21%' : '14%'}`}}>
                  {camelCase(scoretype.toLowerCase()) === 'Highest_Score' && (
                    <div className={`  text-sm`}>vs</div>
                  )}
                  <div className='flex text-sm'>
                    {camelCase(scoretype.toLowerCase()) === 'Highest_Score'
                      ? `${
                          scoreType[
                            categories[camelCase(scoretype.toLowerCase())].score]
                        }`
                      : camelCase(scoretype.toLowerCase()) === 'Most_Runs'
                      ? scoreType.innings_played
                      : scoreType.runs_scored}
                  </div>
                </div>
                {camelCase(scoretype.toLowerCase()) !== 'Highest_Score' && (
                  <div
                    className={` flex-col text-center justify-center text-green text-lg`}
                    style={{width:'15%'}}>
                    {camelCase(scoretype.toLowerCase()) === 'Most_Runs'
                      ? scoreType.runs_scored
                      : scoreType[
                          categories[camelCase(scoretype.toLowerCase())].score
                        ]}
                  </div>
                )}
              </div>
            </div>
              <div className='border-b dark:border-b-black ' />
            </>
          ))}

        {type === 'bowling' &&
          data?.getStatsResolver[format][cricketType][camelCase(scoretype.toLowerCase())].map((scoreType, i) => (
            <>
            <div className='w-full'>
              <div
                className={`flex  text-sm font-semibold  items-center  ${i===0 && 'bg-basered  dark:bg-gray text-white'} `}
                key={i}>
                <div style={{width: '10%'}} className={`  pl-2`}>{i + 1}</div>
                <div style={{width: '48%'}} className={`flex gap-2 items-center py-2`}>
                  <div
                    className={` h-12 w-12  rounded-full bg-gray-10 dark: overflow-hidden`}>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc={fallbackProjection}
                      className='w-12 h-12 object-cover object-top'
                      src={`https://images.cricket.com/players/${scoreType.player_id}_headshot_safari.png`}
                      alt=''
                      // onError={(e) => (e.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className=' flex flex-col justify-center'>
                    <div className='sm font-semibold   '>{scoreType.player_name}</div>
                    <div className={`text-xs text-gray-2 `}>{scoreType.team_short_name}</div>
                  </div>
                </div>

                <div
                  className={`flex flex-col  text-sm  justify-center font-semibold text-center ${ camelCase(scoretype.toLowerCase()) === 'Best_figures' ? 'text-green text-lg' : ''}`}
                  style={{width : `${ camelCase(scoretype.toLowerCase()) === 'Best_figures' ? '21%' : '14%'}`}}>
                  <span>{camelCase(scoretype.toLowerCase()) === 'Best_figures' ? scoreType.best_bowling_figures
                      : scoreType.matches_played}</span>
                 
                  <span>{camelCase(scoretype.toLowerCase()) === 'Best_figures' && (
                    `(${scoreType.overs})`
                  )}</span>
                </div>

                <div
                  className={` flex items-center gap-2 justify-center`}
                  style={{width : `${ camelCase(scoretype.toLowerCase()) === 'Best_figures' ? '21%' : '14%'}`}}>
                  {camelCase(scoretype.toLowerCase()) === 'Best_figures' && (
                    <div className={`  text-sm`}>vs</div>
                  )}
                  <div className={`text-sm ${camelCase(scoretype.toLowerCase()) === 'Most_Wickets' ? 'text-green text-lg ' : ''}`}>
                    {camelCase(scoretype.toLowerCase()) === 'Best_figures'
                      ? `${
                          scoreType[
                            categories[camelCase(scoretype.toLowerCase())].score]
                        }`
                      : scoreType.wickets}
                  </div>
                </div>
                
                {/* {camelCase(scoretype.toLowerCase()) !== 'Most_Wickets' && (
                  <div
                    className={`flex flex-col items-center justify-center self-stretch ${
                      camelCase(scoretype.toLowerCase()) === 'Best_figures' ? 'w-15 ' : 'w-10'
                    }  `}>
                    {camelCase(scoretype.toLowerCase()) === 'Best_figures' && (
                      <div className={`fw4 pr-1 ${i === 0 ? 'white' : 'gray'}`}>vs</div>
                    )}
                    <div>
                      {camelCase(scoretype.toLowerCase()) === 'Best_figures'
                        ? `${
                            scoreType[
                              scores[camelCase(scoretype.toLowerCase())]
                            ]
                          }`
                        : scoreType.wickets}
                    </div>
                  </div>
                )} */}
                {/* {camelCase(scoretype.toLowerCase()) === 'Most_Wickets' && (
                  <div
                    className={`flex flex-col items-center justify-center self-stretch w-10  ${
                      i === 0 ? '' : 'bg-light-gray'
                    } `}>
                    <div>{scoreType.wickets}</div>
                  </div>
                )} */}

                {camelCase(scoretype.toLowerCase()) !== 'Best_figures' &&
                  camelCase(scoretype.toLowerCase()) !== 'Most_Wickets' && (
                    <div
                    className={`flex flex-col text-center font-semibold justify-center text-lg text-green `}
                    style={{width:'14%'}}>
                      {
                        scoreType[
                          categories[camelCase(scoretype.toLowerCase())].score 
                        ]
                      }
                    </div>
                  )}
                {camelCase(scoretype.toLowerCase()) === 'Most_Wickets' && (
                  <div className={` flex  flex-col text-sm font-semibold justify-center text-center `}
                  style={{width:'14%'}}> {console.log('seeee - ',data?.getStatsResolver[format][cricketType])}
                    {
                      scoreType[
                        categories[camelCase(scoretype.toLowerCase())].score 
                      ]
                      
                      // data?.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]][i][
                      //   scores[caseHandle[scoretype.toLowerCase()]]
                      // ]
                    }
                  </div>
                )}
              </div>
            </div>
              <div className='bg-black h-[1px] ' />
            </>
          ))}

        {type === 'fielding' &&
          data?.getStatsResolver[format][cricketType.toLowerCase()][camelCase(scoretype.toLowerCase())].map(
            (scoreType, i) => (
              <>
              <div className='w-full'>
                <div
                  className={`flex  text-sm font-semibold  items-center  ${i===0 && 'bg-basered  dark:bg-gray text-white'} `}
                  key={i}>
                  <div style={{width: '10%'}} className={`  pl-2`}>{i + 1}</div>
                <div style={{width: '48%'}} className={`flex gap-2 items-center py-2`}>
                  <div
                    className={` h-12 w-12  rounded-full bg-gray-10 dark:bg-gray-8 overflow-hidden`}>
                    <ImageWithFallback height={35} width={35} loading='lazy'
                    fallbackSrc={fallbackProjection}
                      className='w-12 h-12 object-cover object-top'
                      src={`https://images.cricket.com/players/${scoreType.player_id}_headshot_safari.png`}
                      alt=''
                      // onError={(e) => (e.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className=' flex flex-col justify-center'>
                    <div className=' text-sm font-semibold   '>{scoreType.player_name}</div>
                    <div className={`text-xs text-gray-2 `}>{scoreType.team_short_name}</div>
                  </div>
                </div>

                  <div
                    className={`flex flex-col text-sm font-semibold justify-center text-center `}
                    style={{width:'21%'}}>
                      {
                        scoreType.matches_played
                      }
                  </div>
                  <div
                    className={`flex flex-col text-center font-semibold justify-center text-lg text-green `}
                    style={{width:'21%'}}>
                      {
                        scoreType[
                          categories[camelCase(scoretype.toLowerCase())].score 
                        ]
                      }
                  </div>
                  
                </div>
              </div>
                <div className='bg-black h-[1px] ' />
              </>
            )
          )}
          </div>
      </div>
    </div>
  );
}