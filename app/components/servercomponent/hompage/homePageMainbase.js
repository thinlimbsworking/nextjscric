// 'use client'
import React from 'react'
import Image from 'next/image'
import { PREDICTION_LANDING_AXIOS,HOME_SERIES_TO_LOOK_OUT_AXIOS,GET_WEB_HOMW_PAGE_DATA_AXIOS, SOCIAL_TRACKER_AXIOS,CDC_EXCLUSIVE_AXIOS,FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE_AXIOS ,GET_VIDEO_POSITIONS_AXIOS} from '../../../api/queries';
import News from './newHome';
import VideoHome from './homepageVideo';
import Exclusive from './cricexculsive'
import Social from './scoialTracker'
import PicofTheDay from './picOfTheDay'
import CricUpdate from './cricUpdates'
import SeriesLookOut from './seriesLookOut';
import OnthisDay from './onThisday'
import QuizNews from './quizNews'

import axios from 'axios'
async function getData() {
 


  const here= await axios.post(process.env.API, {
        query: FEATURED_MATCHES_AND_GET_ARTICLE_BY_POSITION_HOME_PAGE_AXIOS,
        
    })

    

   
  return here.data
}

async function getDataVideo() {
 


    const here= await axios.post(process.env.API, {
          query: GET_VIDEO_POSITIONS_AXIOS,

          
      })
  
      
  
     
    return here.data
  }

  async function getExclusiveData() {
 


    const videoData= await axios.post(process.env.API, {
          query: CDC_EXCLUSIVE_AXIOS,
            variables: { type: 3 },
          
      })
  
      
  
     
    return videoData&&videoData.data  && videoData.data.data
  }
  async function getDataSeriesHome() {
 


    const here= await axios.post(process.env.API, {
          query: HOME_SERIES_TO_LOOK_OUT_AXIOS,

          
      })
  
      
  
     
    return here.data
  }


  async function getSocialTracker() {
 


    const here= await axios.post(process.env.API, {
          query: SOCIAL_TRACKER_AXIOS,

          
      })
  
      
  
     
    return here.data
  }



  async function sideBar() {
 


    const here= await axios.post(process.env.API, {
          query: GET_WEB_HOMW_PAGE_DATA_AXIOS,

          
      })
  
      
  
     
    return here.data
  }
export default async function Page() {
  const data = await getData();
  // const dataVideo = await getDataVideo();
  // const exclusive=await getExclusiveData()
  // const social =await getSocialTracker()
  // const sideBarData =await sideBar()
  const seriesHomePage =await getDataSeriesHome()

  



  return (<div className='flex flex-col   w-full '>
     





     <div className="  py-3 w-full" />


<OnthisDay  data={data&& data.data.getArticleByPostitions} />
<div className="  py-3 w-full" />
<QuizNews  data={data&& data.data.getArticleByPostitions} />
<div className="  py-3 w-full" />
{seriesHomePage&& seriesHomePage.data.getWebHomePageData&&
<SeriesLookOut data={seriesHomePage&& seriesHomePage.data.getWebHomePageData &&seriesHomePage.data.getWebHomePageData.seriesToLookoutFor} />
}

  </div>)
}