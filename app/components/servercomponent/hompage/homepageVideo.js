// 'use client'
import React from 'react'
import Image from 'next/image'
// import { PREDICTION_LANDING_AXIOS } from '../../../api/queries';
import axios from 'axios'
import Heading from '../../shared/heading';
import Link from 'next/link'
import Button from '../../shared/button';

export default async function NewsVideo({ data, dataVideo, ...props }) {



  return (
  <div className='flex  flex-col items-center justify-center  w-full  '>
    
    <div className='flex items-start justify-start  flex-col  w-full ' >

      <Heading heading={'Videos'} />




      <div className='flex flex-col  w-full '>
<Link href={`videos/${dataVideo[0].videoID}`}>
        <div className='flex flex-col  w-full relative items-center justify-center' >


          <div className='flex   items-center justify-center w-full  ' style={{height:'500px'}}>

            <img
              className=' flex  h-full w-full items-center justify-center rounded-md object-conatin object-top'
              src={`https://img.youtube.com/vi/${dataVideo[0].videoYTId}/mqdefault.jpg?auto=compress&dpr=1&fit=crop&w=800&h=480`}
              alt='' />

            <img className="absolute  h-12 w-12" src="/pngsV2/redplay.png" alt="" />
          </div>

          <div className='flex w-full  ml-2 overflow-hidden mt-2 text-base font-semibold text-ellipsis text-left tracking-wider font-mnr'> {data[0].title || ''} </div>
          <div className='flex w-full my-1 ml-2  text-base  font-thin  text-left tracking-wide text-gray  text-mnr '> {data[0].description || ''}</div>

        </div>
        </Link>


        <div className='w-full  text-mnr '>
          <div className=''>
            <div className='flex w-full '>

              {dataVideo &&
                dataVideo.map((item, i) => {
                  return 0 < i && i < 4 && (<div className=' flex flex-col items-center justify-center  relative w-4/12 m-1  border-2 p-2 rounded-md '>
                    <Link  href={`videos/${item.videoID}`}>

                    <div className='flex  items-center justify-center w-full  ' >
                      <img className=' w-full  rounded-lg object-conatin object-top ' style={{ height: '180px' }}  src={`https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg?auto=compress&dpr=1&fit=crop&w=800&h=600`} alt='' />
                      <img className="absolute  h-12 w-12" src="/pngsV2/redplay.png" alt="" />
                    </div>
                    <div className='my-1 ml-2   font-normal  text-left tracking-wide text-xs  flex items-start w-full  text-mnr '> {item.title || ''}</div>
                    </Link>
                  </div>
                 

                  )
                })}
            </div>
          </div>



        </div>




      </div>
    </div>
    <div className='flex w-full items-center justify-end  py-2'>
    <Button title={"More Videos"} url={"/videos/latest "}   />
     </div>
    {/* <Link className='w-full flex w-full items-center justify-end ' href="/videos/latest "> 
    <div className='flex w-full items-center justify-end '>
      <div className='flex w-2/12 items-center justify-center text-center cursor-pointer   my-2 border border-red-5 font-mnr  text-red-5 p-2 text-xs rounded-md '> More Videos</div>
    
     </div>
     </Link> */}

  </div>)
}