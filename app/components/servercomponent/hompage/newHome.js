// 'use client'
import React from 'react'
import Image from 'next/image'
// import { PREDICTION_LANDING_AXIOS } from '../../../api/queries';
import axios from 'axios'
import Heading from '../../shared/heading';

import Link from 'next/link'
import Button from '../../shared/button';


export default async function NewsVideo({ data, ...props }) {
 


  return (<div className='flex flex-col items-center justify-center    w-full  '>


    
    <div className='flex items-start justify-start  flex-col  w-full  ' >

      <Heading heading={'News Articles'} />




      <div className='flex flex-col  w-full '>
      <Link href={`/news/${data[0].articleID}`}>

        <div className='flex flex-col  w-full'>


          <div className='flex   w-full ' style={{height:'500px'}}>
         
            <img
              className=' w-full h-full  rounded-md'
              src={`${data[1].featureThumbnail}?auto=compress&fit=crop&crop=faces,top&w=full&h=600`}
              // src={`${data[1].featureThumbnail}?auto=compress&dpr=1&fit=clip&w=full&h=380`}
              alt='' />


          </div>
          <div className=' ml-2 overflow-hidden mt-2 text-base font-semibold text-ellipsis text-left tracking-wider   font-mnr'> {data[0].title || ''} </div>
          <div className='my-1 ml-2  text-base  font-thin  text-left tracking-wide text-gray  text-mnr '> {data[0].description || ''}</div>

        </div>
        </Link>


        <div className='w-full text-mnr '>
          <div className=''>
            <div className='flex w-full '>

          { data &&
          data.map((item, i) =>  {
            return    0<i&&i<4 &&(<div className='  w-4/12 m-1  border-2  p-1 rounded-md'>
               <Link href={`/news/${item.articleID}`}>
              <div className='flex  items-center justify-center  '  style={{height:'180px'}} >
                <img className=' w-full  rounded-lg object-cover object-top h-full'  src={`${item.featureThumbnail}?auto=compress&fit=crop&crop=faces,top&w=1800&h=180}`} alt='' />
              </div>
<div className='my-1 ml-2   font-normal  text-left tracking-wide text-xs   text-mnr '> {item.title || ''}</div>

</Link>
            
           
            </div>

            )
          })}
</div>
          </div>


        {false && data &&
          data.map((item, i) => {
            return (<div className='   pb-4 bg-gray cardUpdate  rounded-xl  m-1'>
            <div className='flex  items-center justify-center w-full '>
                <img className='h-32   w-full rounded-lg object-conatin object-top ' src={item.featureThumbnail} alt='' />
              </div>
              <div className=' px-2 overflow-hidden mt-2 text-sm font-semibold text-ellipsis text-left tracking-wider truncate'>
                {item.title || ''}
              </div>
              <div className=' my-1 px-2 text-xs font-thin truncate text-left tracking-wide '>
                {item.description || ''}
              </div>
              <div className='px-2 text-gray-2 text-xs text-left truncate '>{item.author}</div>
           
            </div>

            )
          })}

        </div>





        {false && data &&
          data.map((item, i) => {
            return (<div className='   pb-4 bg-gray cardUpdate  rounded-xl  m-1'>
              <div className='flex  items-center justify-center w-full '>
                <img className='h-32   w-full rounded-lg object-conatin object-top ' src={item.featureThumbnail} alt='' />
              </div>
              <div className=' px-2 overflow-hidden mt-2 text-sm font-semibold text-ellipsis text-left tracking-wider truncate'>
                {item.title || ''}
              </div>
              <div className=' my-1 px-2 text-xs font-thin truncate text-left tracking-wide '>
                {item.description || ''}
              </div>
              <div className='px-2 text-gray-2 text-xs text-left truncate '>{item.author}</div>
           
            </div>

            )
          })}
      </div>
    </div>

    
  
   
   
   
    <div className='flex w-full items-center justify-end  py-2'>
    <Button title={"More News"} url={"/news/latest"}   />
     </div>
     {/* <Link className='w-full flex w-full items-center justify-end ' href="/news/latest"> 
     </Link> */}

  </div>)
}