// 'use client'
import React from 'react'
import Image from 'next/image'
// import { PREDICTION_LANDING_AXIOS } from '../../../api/queries';
import axios from 'axios'
import Heading from '../../shared/heading';

import Link from 'next/link'
import Button from '../../shared/button';

import { format } from 'date-fns'

export default async function NewsVideo({ data, ...props }) {
 


  return (<div className='flex flex-col items-center justify-center    w-full  '>

    
<div  className='flex items-start justify-evenly   w-full  ' > 
      <div className='flex w-full'> <Heading heading={'On This Day '} /></div>
   
     
     </div>
    <div className='flex items-start justify-evenly   w-full  ' >
     
     


     




      <div className='flex flex-col  w-full '>
   


        <div className='w-full text-mnr '>
          <div className=''>
            <div className='flex w-full '>

          { data &&
          data.map((item, i) =>  {
            return    0<i&&i<4 &&(<div className='  w-4/12 m-1  border-2   rounded-md'>
               <Link href={`/news/${item.articleID}`}>
                 {console.log("1212121212",item)}
              <div className='flex  items-center justify-center   h-56 '  >
                <img className=' w-full   rounded-t-lg object-cover object-top h-full'  src={`${item.featureThumbnail}?auto=compress&fit=crop&crop=faces,top&w=1800&h=224}`} alt='' />
              </div>
              <div className='flex w-full items-center justify-center border-b  text-base font-semibold capitalize p-1'> {format(new Date(Number(item.createdAt) - 19800000), 'Mo MMM')}
</div>
<div className='my-1 ml-2   font-normal  text-center tracking-wide text-xs     text-mnr '> {item.title || ''}</div>

</Link>
            
           
            </div>

            )
          })}
</div>
          </div>


     

        </div>



      </div>
    </div>

  </div>)
}