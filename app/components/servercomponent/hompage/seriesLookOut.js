import React from 'react'
import Heading from '../../shared/heading';
import { format, formatDistanceToNowStrict } from 'date-fns';

export default function SeriesLookOut(props) {
    console.log("kunalalakaks",props.data)
  return (
    <div className='flex  flex-col w-full'>
         <div className='flex items-center justify-between'>
    <Heading heading={'Series To Lookout For'} />
   
    
    </div>

    <div className='flex items-center justify-center   w-full'>
    <div className='flex overflow-scroll w-full  py-2  '>
   { props.data&& props.data.map((item,i)=>{return(<div className=''>
   <div className=''>

{item.seriesType=="Bilateral" &&
                    
                  <div key={i} className=' w-44  mx-2  bg-gray   rounded-xl'>
                      <div className='flex flex-col  h-40 items-center justify-center'>
                        <div className='flex items-center justify-center  '>
                          <div className='z-10'>
                            <img
                              className='h-6 w-9 rounded  '
                              src={`https://images.cricket.com/teams/${item.teamid[0]}_flag_safari.png`}
                                
                              alt=''
                            />
                          </div>
                          <div>
                            <img className='h-6 w-6 mx-1' src='/pngsV2/bluecross.png' alt='' />
                          </div>
                          <div className=' z-10'>
                            <img
                              className='h-6 w-9 rounded  '
                              src={`https://images.cricket.com/teams/${item.teamid[1]}_flag_safari.png`}
                                 
                              alt=''
                            />
                          </div>
                        </div>

                        <div className='flex text-md mt-3 items-center justify-center'>
                          {item.match}
                        </div>
                        
                        <div className='font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1'>
                        {format(new Date(Number(item.seriesStartDate) - 19800000), 'MMM do')}-  {format(new Date(Number(item.seriesEndDate) - 19800000), 'MMM do')}
                        </div>
                      </div>
                    </div>
              }
              {item.seriesType=="Tournament" &&
                    
                    <div key={i} className=' w-44  mx-2  bg-gray   rounded-xl' >
                        <div className='flex flex-col  h-40 items-center justify-center'>
                          <div className='flex items-center justify-center  '>
                            <div className='z-10'>
                              <img
                                className=' object-cover  object-top rounded-md  '
                               
                                src={`${item.teamid[0]}?w=40&h=40&auto=compress,format&fit=crop&dpr=2`}
                                   
                                alt=''
                              />
                            </div>
                        
                           
                          </div>
  
                        
                          
                          <div className='font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1'>
                          {format(new Date(Number(item.seriesStartDate) - 19800000), ' MMM dd')}-  {format(new Date(Number(item.seriesEndDate) - 19800000), ' MMM dd')}
                          </div>
                        </div>
                      </div>
                }
                    </div>
            </div>)}) }

        </div>


        
    </div>
    </div>
  )
}
