import React, { useEffect, useRef, useState } from 'react'
import Heading from '../commom/heading';
import { useRouter } from 'next/navigation';
import { words } from '../../constant/language';
import { format, formatDistanceStrict } from 'date-fns';
import { getLangText } from '../../api/services';
import { getArticleTabUrl, getVideoUrl } from '../../api/services';


const playVideo = '/svgs/buttonplay.svg';
export default function SliderTab({ data, heading, lang, type, ...props }) {
  let router = useRouter();
  const [index, setIndex] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  const [scrollX, setscrollX] = useState(0);
  const [scrollX1, setscrollX1] = useState(0);
  const scrl = useRef();
  const [displayList, setDisplayList] = useState(data)

  useEffect(() => {
    // let temp = [];
    // if(index + 4 > data?.length) {  
    //     let extra = 4 - (data?.length - index);
    //     temp = [...data?.slice(index)]
    //     temp.push(...data?.slice(0, extra))
    // } else {
    //     temp = data?.slice(index,index+4)
    // }
    // setDisplayList(temp)
  }, [index])

  const slide = (shift) => {
    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: "smooth",
    });
    setscrollX(scrollX + shift);

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  const handleNav = item => {
    switch (type) {
      case 'videos':
        router.push(getVideoUrl(item).as)
        break;
      case 'articles':
        router.push(getArticleTabUrl(item.articleID).as)
        break;
      default:
        break;
    }
  }

  return (
    <div className='w-full  relative'>
      <div className='flex justify-between  mb-4'>
        <Heading heading={getLangText(lang, words, heading)} />
        <div onClick={() => router.push(props.viewUrl)} className='cursor-pointer hover:opacity-70 transition ease-in duration-150 w-20 p-1 border border-basered text-red-5 font-semibold text-xs flex justify-center items-center rounded-md'>{getLangText(lang, words, 'View All')}</div>
      </div>
      <div className='flex w-full gap-4 overflow-auto ' ref={scrl} onScroll={scrollCheck}>
        <img onClick={() => slide(-((scrl && scrl.current && scrl.current.offsetWidth) / 3) - 30)} src='/svgs/downArrow.svg' className={`z-20 cursor-pointer rotate-90 black absolute w-6 h-6 -left-6 bottom-[170px] ${scrolEnd ? 'opacity-70 blur-0' : ''} `} />

        {
          displayList?.map(item => <div onClick={() => handleNav(item)} className='relative flex flex-col min-w-[350px] w-[350px] h-[300px]  rounded-md cursor-pointer'>
            {/* <div className=' min-w-[350px] h-[200px] rounded-t-md'> */}
            <img className=' relative object-top flex justify-center items-center rounded-t-md w-full h-[180px] object-cover' src={type === 'videos' ? `https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg?auto=compress` : `${item.bg_image}?auto=compress&w=700`} />


            {type === 'videos' && <div className='absolute w-full h-[180px]  flex justify-center items-center'> <img className=' w-12 h-12 cursor-pointer' src={playVideo} alt='video' /> </div>}
            {/* </div> */}
            <div className='h-[120px]  text-black dark:text-white flex flex-col justify-between border rounded-b-md p-3 border-t'>
              <div className='w-full flex flex-col gap-1'>
                <div className='text-sm font-semibold'>{item.title}</div>
                <div className='text-xs max-h-[24px] truncate  text-gray-2'>{item.description}</div>
              </div>
              <div className='w-full  flex justify-between '>
                {!!(item.author || item.authors?.length) && (
                  <div className=' text-xs text-gray-2 '>
                    By <span>{item.author || (item.authors && item.authors[0])}</span>
                  </div>
                )}
                {item.createdAt && (
                  <div className=' text-xs text-gray-2  '>
                    {item.createdAt
                      ? new Date().getTime() - Number(item.createdAt) > 86400000
                        ? format(new Date(Number(item.createdAt)), 'dd MMM, yyyy')
                        : formatDistanceStrict(new Date(Number(item.createdAt)), new Date()) + ' ago'
                      : ''}
                  </div>
                )}
              </div>
            </div>
          </div>)
        }
        <img onClick={() => slide(+((scrl && scrl.current && scrl.current.offsetWidth) / 3) + 30)} src='/svgs/downArrow.svg' className={`z-20 cursor-pointer -rotate-90 black absolute w-6 h-6 -right-6 bottom-[170px] ${scrolEnd ? 'opacity-70 blur-0' : ''} `} />
      </div>
    </div>
  )

}