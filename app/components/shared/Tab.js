import React, { useState } from 'react'

export default function Tab({
  data,
  selectedTab,
  handleTabChange,
  type,
  ...props
}) {
  
  const [tab, setTab] = useState(selectedTab)
  switch (type) {
    case 'line':
      return (
        <div
          className={`w-full p-1 md:p-0 lg:p-0  text-xs font-semibold flex text-black dark:text-sky-100 `}
        >
          {data.map((item) => (
            <div
              onClick={() => {
                setTab(item)
                handleTabChange(item)
              }}
              className={`flex gap-2 justify-center items-center md:p-2 lg:p-2 p-1 md:cursor-pointer border-b dark:border-b-2 whitespace-nowrap   w-full ${
                item.value === tab.value
                  ? 'border-b-2 border-red-6 text-red-6 dark:border-sky-300 dark:text-sky-300  '
                  : 'dark:border-sky-100'
              }`}
            >
              <span> {item.name} </span>
              {item.src && <img src={item.src} alt="icon" />}
            </div>
          ))}
        </div>
      )
    case 'outline':
      return (
        <div
          className={`w-full border  dark:border-none dark:bg-gray-4 rounded-full p-1 md:p-0 lg:p-0 text-sm font-regular flex md:gap-2 lg:gap-2  text-black dark:text-white`}
        >
          {data.map((item) => (
            <div
              onClick={() => {
                setTab(item)
                handleTabChange(item)
              }}

              className={`flex gap-2 rounded-full justify-center items-center md:cursor-pointer md:p-2 lg:p-2 p-1  w-full ${

                item.value === tab.value
                  ? 'border border-red-6 text-red-6 dark:border-green dark:text-white'
                  : ''
              }`}
            >
              <span> {item.name} </span>
              {item.src && <img src={item.src} alt="icon" />}
            </div>
          ))}
        </div>
      )
    case 'block':
      return (
        <div
          className={`w-full   dark:border-none dark:bg-gray-4 rounded-full p-1 md:p-0 lg:p-0 text-xs  font-semibold flex md:gap-2 lg:gap-2  text-black dark:text-white`}
        >
          {data.map((item) => (
            <div
              onClick={() => {
                setTab(item)
                handleTabChange(item)
              }}
              className={`flex gap-3 rounded-full justify-center items-center md:p-2 lg:p-2 px-2 py-1.5 md:cursor-pointer truncate w-full ${
                item.value === selectedTab.value
                  ? 'text-white bg-red-6 dark:basebg dark:border  dark:border-green dark:text-white dark:bg-transparent'
                  : 'bg-light_gray dark:bg-transparent '
              }`}
            >
              <span> {item.name} </span>
              {item.src && (
                <img className="w-5 h-5 " src={item.src} alt="icon" />
              )}
            </div>
          ))}
        </div>
      )
    default:
      return
  }
}
