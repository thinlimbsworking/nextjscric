import React from 'react'
import Link from 'next/link'
export default function Button(props) {
  return (
    <Link href={props.url}>
  <div  className='cursor-pointer w-28 text-[0.66rem]   hover:opacity-70 transition ease-in duration-150   p-1  border leading-5 border-red-5  text-red-5 font-normal  flex justify-center items-center rounded-md font-mnr '>
      {props.title}
  </div>
  </Link>

  )
}
