import { exclusiveTabs } from '../../constant/constants'
import Link from 'next/link'
import ImageWithFallback from '../commom/Image'
import { useRouter } from 'next/navigation'
import Heading from '../commom/heading'
export default function ExclusiveTabs() {
    const router = useRouter();
  return <div className=' w-full  '>
    <div className='w-full flex justify-between'>
        <Heading heading='CDC Exclusive'/>
        <Link href='exclusive/1' className='w-20 flex justify-center p-2 text-basered border border-basered rounded-md text-xs font-semibold hover:opacity-70 transition ease-in duration-150 cursor-pointer '>VIEW ALL</Link>
    </div>
    <div className='mt-8 overflow-y-auto flex flex-col gap-2  w-full'>
        {
            exclusiveTabs.filter(ele => ele.type).map(ele => <div onClick={() => { router.push(`/exclusive/${ele.type}`)}} className={`w-full   rounded-lg  cursor-pointer border bg-white  dark:bg-gray p-2`}>
                <ImageWithFallback height={100} width={100} loading='lazy' className="w-full h-32 rounded-md object-cover " src={ele.src}/>
                <div className="w-full  text-center p-1 text-xs font-bold">{ele.name}</div>
            </div>)
        }
    </div>
  </div>
}
