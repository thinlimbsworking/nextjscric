import React from 'react'

export default function Heading(props) {
  return (
    <div className={props.noPad ? '' : `py-2`}>
      <div className="font-semibold md:text-base md:text-black text-white text-base capitalize leading-6 font-ttb">
        {props.heading}{' '}
      </div>
      {props.subHeading && (
        <div className="text-sm tracking-wide md:text-black text-white pt-1">
          {props.subHeading}{' '}
        </div>
      )}

      {props.heading && !props.borderlineHide && (
        <div className="bg-blue-8 h-1 rounded-md mt-1 w-16"></div>
      )}
    </div>
  )
}
