'use client'
import { useRouter } from 'next/navigation'
import { useEffect, useState } from 'react'
import Tab from './Tab'
const teamsIcon = '/svgs/icons/teams-icon.svg'
const blackTeamsIcon = '/svgs/black_teams.svg'
const blackRecordsIcon = '/svgs/black_records.svg'
const blackRankingIcon = '/svgs/black_rankings.svg'
const blackPlayersIcon = '/svgs/black_players.svg'
const blackStadiumIcon = '/svgs/black_stadiums.svg'
const stadiumIcon = '/svgs/icons/stadium-icon.svg'
const rankingsIcon = '/svgs/icons/rankings-icon.svg'
const recordsIcon = '/svgs/icons/records-icon.svg'
const playersIcon = '/svgs/icons/players-icon.svg'
let List = [
  {
    id: 'players',
    value: 'players',
    as: '/players/all',
    href: '/players/all',
    prefetch: '/players/[slug]',
    name: 'Players',
    src: playersIcon,
    activeTabIcon: playersIcon,
    nonActiveTabIcon: blackPlayersIcon,
    event: 'PlayerDiscovery',
  },
  {
    id: 'teams',
    value: 'teams',
    as: '/teams/international',
    href: '/teams/international',
    prefetch: '/teams/[slug]',
    name: 'Teams',
    src: teamsIcon,
    activeTabIcon: teamsIcon,
    nonActiveTabIcon: blackTeamsIcon,
    event: 'TeamDiscovery',
  },
  {
    id: 'stadiums',
    value: 'stadiums',
    as: '/stadiums',
    href: '/stadiums',
    prefetch: '/stadiums',
    name: 'Stadiums',
    src: stadiumIcon,
    activeTabIcon: stadiumIcon,
    nonActiveTabIcon: blackStadiumIcon,
    event: 'StadiumsDiscovery',
  },
  {
    id: 'rankings',
    value: 'rankings',
    as: '/rankings',
    href: '/rankings',
    prefetch: '/rankings',
    name: 'Rankings',
    src: rankingsIcon,
    activeTabIcon: rankingsIcon,
    nonActiveTabIcon: blackRankingIcon,
    event: 'Rankings',
  },
  {
    id: 'records',
    value: 'records',
    as: '/records',
    href: '/records',
    prefetch: '/records',
    src: recordsIcon,
    activeTabIcon: recordsIcon,
    nonActiveTabIcon: blackRecordsIcon,
    name: 'Records',
    event: 'Records',
  },
]

export default function StatHubLayout({ children, currIndex }) {
  let router = useRouter();
  const [TabList, setTabList] = useState(List);
  useEffect(() => {
    setTabList(TabList?.map((tab, i) => {
      return currIndex === i
        ? { ...tab, src: tab?.activeTabIcon }
        : { ...tab, src: tab?.nonActiveTabIcon }
    }))
  },[])
  const handleNav = (item) => {
    router.push(item.href);
  }
  return (
    <div className="w-full md:py-4 lg:py-4">
      <div className="w-full hidden md:block lg:block">
        <Tab
          data={TabList}
          selectedTab={TabList[currIndex]}
          type="block"
          handleTabChange={(item) => {
            handleNav(item)
          }}
        />
      </div>
      <div className="w-full md:mt-4 lg:mt-4">{children}</div>
    </div>
  )
}
