"use client";
import { exclusiveTabs } from "../../../constant/constants";
import { useState, useEffect, useRef } from "react";


import {format, formatDistanceStrict} from 'date-fns';
import Link from "next/link";
import Heading from "../../../components/commom/heading";
import Button from "./button";
export default function SliderTab({data, heading , lang, type, ...props}) {
  let scrl = useRef(null);
  let scrl1 = useRef(null);
  const [scrollWidth, setScrollWidth] = useState(0);
  const [displayList, setDisplayList] = useState(data)
  const [scrollX, setscrollX] = useState(0);
  const [scrollX1, setscrollX1] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  const [scrolEnd1, setscrolEnd1] = useState(false);
  const [windows, updateWindows] = useState();
  const callSocialWidget = () => {
    global.windows &&
      global.windows.twttr.ready().then(() => {
        global.windows &&
          global.windows.twttr.widgets.load(
            document.getElementsByClassName("twitter-div")
          );
      });
  };
  const [tabName, setTabName] = useState({
    name: "SOCIAL TRACKER",
    value: "SOCIAL_TRACKER",
  });
  const [counter, setCointer] = useState(0);

  useEffect(() => {
    if (scrl1 && scrl1.current && scrl1.current.clientWidth) {
      setScrollWidth(scrl1.current.clientWidth);
    }
  }, []);

  const slide = (shift) => {
    scrl.current.scrollLeft += shift;
    setscrollX(scrollX + shift);

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  const slide1 = (shift) => {
    scrl1.current.scrollLeft += shift;
    setscrollX1(scrollX1 + shift);

    if (
      Math.floor(scrl1.current.scrollWidth - scrl1.current.scrollLeft) <=
      scrl1.current.offsetWidth
    ) {
      setscrolEnd1(true);
    } else {
      setscrolEnd1(false);
    }
  };
  const scrollCheck = () => {
    setscrollX1(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  if (true) {
    return (
      <div className="flex flex-col  w-full ">
        <Heading heading={heading} />
        <div className="flex flex-col relative items-center justify-center ">
          <div className="flex flex-col overflow-scroll w-full items-center justify-center ">
            <div
              onClick={() => scrollX !== 0 && scrollX > -1 && slide(+-205)}
              className={`${
                scrollX < 90 ? "opacity-30" : "opacity-100"
              }  flex  absolute -left-4 cursor-pointer  bg-black rounded-full   item-center justify-center z-10  white pointer dn db-ns z-999 outline-0 `}
              id="swiper-button-prev"
            >
              <svg width="20" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>

            <div className="w-full  relative">
              <div
                className={` relative  py-2 flex  overflow-x-scroll items-center  w-full} `}
                ref={scrl}
                onScroll={scrollCheck}
              >
                {
                displayList?.map(item => <div  className='relative flex flex-col w-72 h-60 m-1  rounded-md cursor-pointer'>
                   
                         <img className=' relative  flex justify-center items-center    rounded-t-md object-top  object-contain ' src={type === 'videos' ? `https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg?auto=compress?auto=compress&fit=crop&w=320&h=250` : `${item.bg_image}?auto=compress&fit=crop&w=320&h=250`} />
                            
                         
                       {/* {type === 'videos' && <div className='absolute w-full h-[180px]  flex justify-center items-center'> <img className=' w-12 h-12 cursor-pointer' src={playVideo} alt='video'/> </div>} */}
                       {/* https://images-devcdc.imgix.net/news-1676877864053?auto=compress&fit=crop&w=260&h=150 */}
                    <div className='h-24   text-black dark:text-white flex flex-col justify-between border rounded-b-md p-3 border-t w-full'>
                        <div className='w-full flex flex-col '>
                            <div className='text-xs font-medium line-clamp-2'>{item.title}</div>
                            <div className='text-xs truncate  text-gray-2'>{item.description}</div>
                        </div>
                        <div className='w-full  flex justify-between mt-2 '>
                        { item.author && (
                          <div className=' text-xs text-gray-2 '>
                            By <span>{item.author}</span>
                          </div>
                        )}
                        {item.createdAt && (
                          <div className=' text-xs text-gray-2  '>
                            {item.createdAt
                              ? new Date().getTime() - Number(item.createdAt) > 86400000
                                ? format(new Date(Number(item.createdAt)), 'dd MMM, yyyy')
                                : formatDistanceStrict(new Date(Number(item.createdAt)), new Date()) + ' ago'
                              : ''}
                          </div>
                        )}
                        </div>
                    </div>
                </div>)
            }
              </div>
            </div>

            <div
              onClick={() => !scrolEnd && slide(205)}
              id="swiper-button-prev"
              className={`${
                scrolEnd ? "opacity-40" : "opacity-100"
              } white pointer absolute  bg-black rounded-full -right-5 flex  item-center justify-center cursor-pointer shadow-md `}
            >
              <svg width="20" viewBox="0 0 24 24">
                <path
                  fill="#fff"
                  d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
          </div>
          <div className="flex items-center justify-end w-full -mt-5">
            <Button url="/exclusive/1" title={"View All"} />
            {/* <div className='cursor-pointer w-24 p-1 border-2 border-red-5 text-red-5 font-semibold text-sm flex justify-center items-center rounded-md'>VIEW ALL</div> */}
          </div>
        </div>
      </div>
    );
  }
}
