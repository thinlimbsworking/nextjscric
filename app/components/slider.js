import React, { useRef, useState, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import {
  EffectCoverflow,
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Controller,
} from 'swiper'
import 'swiper/css'
import { Swiper, SwiperSlide } from 'swiper/react'
import { format } from 'date-fns'
import Countdown from 'react-countdown-now'
import Image from 'next/image'
import { scheduleMatchView } from '../api/services'
import Link from 'next/link'
// Import Swiper React components
import CleverTap from 'clevertap-react'
// import CleverTap from 'clevertap-react';

// import ClevertapReact from 'clevertap-react';
import dynamic from 'next/dynamic'

import Score from '../components/commom/score'

const mom = '/pngsV2/MOM2.png'
const LGarrow = '/pngsV2/lgarrow.png'

const RGarrow = '/pngsV2/rgarrow.png'

export default function Index({ setPag, pegIndex, ...props }) {
  CleverTap.initialize(process.env.CLEVERTAP_ID)

  const swiperRef = useRef()

  const router = useRouter()

  const deviceType = () => {
    const ua = navigator.userAgent
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 'tablet'
    } else if (
      /Mobile|Android|iP(hone|od)|IEMobile|BlackBerry|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua,
      )
    ) {
      return 'mobile'
    }
    return 'desktop'
  }

  // useEffect(async() => {

  //   ClevertapReact.initialize(process.env.CLEVERTAP_ID);
  // }
  // )

  const [swiper, setSwiper] = useState()
  const prevRef = useRef()
  const nextRef = useRef()

  useEffect(() => {
    if (swiper) {
      // swiper.params.navigation.prevEl = prevRef.current;
      // swiper.params.navigation.nextEl = nextRef.current;
      // swiper.navigation.init();
      // swiper.navigation.update();
    }
  }, [swiper])

  const [currentIndex, updateCurrentIndex] = useState(0)
  const [test, setText] = useState(0)
  const [swiperIndex, setSwiperIndex] = useState(0)

  const Bell = './pngsV2/bell.png'
  const location = './pngsV2/location.png'
  const navigate = router.push

  // useEffect(() => {
  //   if (swiper !== null) {
  //     try {
  //       swiper.on('slideChange', () => {
  //         updateCurrentIndex(swiper.realIndex);
  //         props.updateCurrentIndex(swiper.realIndex);
  //       });
  //       swiper.on('click', () => {

  //       });
  //     } catch (e) {
  //       console.log(e);
  //     }
  //   }
  // }, [swiper, test]);

  const HandleSwipeCLick = (index) => {
    // alert(22)
    //  window.location.href=`/${matchStatus}/${matchID}/${currentTab}/${seriesName}`
    return handleNavigation(props.data[index])
  }
  const HandleSwipeChnage = (index) => {
    // alert(index)
    setPag(index)
  }

  const handleNavigation = (match) => {
    // alert(8)

    const event =
      match.matchStatus === 'upcoming' || match.matchStatus === 'live'
        ? 'CommentaryTab'
        : 'SummaryTab'

    CleverTap.event('10Article', {
      Source: 'Homepage',
      MatchID: match.matchID,
      SeriesID: match.seriesID,
      TeamAID: match.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: match.matchStatus,
      Platform: localStorage.Platform,
    })

    CleverTap.initialize('Article', {
      Source: '10Homepage',
      MatchID: match.matchID,
      SeriesID: match.seriesID,
      TeamAID: match.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: match.matchStatus,
      Platform: localStorage.Platform,
    })

    // const event = match.matchStatus === 'upcoming' || match.matchStatus === 'live' ? 'CommentaryTab' : 'SummaryTab';
    let matchStatus =
      match?.matchStatus === 'completed' ? 'match-score' : 'live-score'

    let currentTab = match.isAbandoned
      ? 'commentary'
      : match.matchStatus === 'upcoming' || match.matchStatus === 'live'
      ? 'commentary'
      : 'Summary'
    let matchName = `${
      match?.homeTeamName ? match?.homeTeamName : match?.homeTeamShortName
    }-vs-${
      match?.awayTeamName ? match?.awayTeamName : match?.awayTeamShortName
    }-${match?.matchNumber ? match?.matchNumber.split(' ').join('-') : ''}-`

    let seriesName =
      matchName.toLowerCase() +
      (match?.seriesName
        ? match.seriesName
            .replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()
        : '')

    return `/${matchStatus.toLowerCase()}/${
      match.matchID
    }/${currentTab.toLowerCase()}/${seriesName}`
    //   console.log("currentTabcurrentTab",currentTab,match)
    // let { as, href } = scheduleMatchView(match, currentTab);
    // navigate(href, as);
  }

  return (
    <div className="">
      <div className="flex items-center justify-center ">
        <div className="   hidden lg:block md:block">
          <div
            style={{ zIndex: 1000 }}
            onClick={() => swiperRef.current?.slidePrev()}
            id="swiper-button-prev"
            className="white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer"
          >
            <svg width="30" focusable="false" viewBox="0 0 24 24">
              <path
                fill="#000"
                d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
              ></path>
              <path fill="none" d="M0 0h24v24H0z"></path>
            </svg>
          </div>
          {/* <button className='bg-red-500'>Prev</button> */}
        </div>

        <Swiper
          //  className="external-buttons "
          onBeforeInit={(swiper) => {
            swiperRef.current = swiper
          }}
          modules={[Navigation]}
          slidesPerView={deviceType() == 'mobile' ? 1 : 3}
          className="mySwiper"
          // modules={[Navigation, Pagination, Scrollbar, A11y, Controller]}

          navigation={{
            prevEl: prevRef?.current,
            nextEl: nextRef?.current,
          }}
          updateOnWindowResize
          // observer
          // observeParents
          initialSlide={0}
          spaceBetween={30}
          onSwiper={setSwiper}
          // onClick={()=>alert(8)}

          onClick={(swiper) => HandleSwipeCLick(swiper.realIndex)}
          onSlideChange={(swiper) => HandleSwipeChnage(swiper.realIndex)}
          shouldSwiperUpdate={true}
          // effect={"coverflow"}
          // grabCursor={true}
          // centeredSlides={true}
          // slidesPerView={deviceType()=="mobile"?'1':'3'}
          loop={true}

          // coverflowEffect={{

          //     rotate: 0,
          //     stretch: 0,
          //     depth: 600,

          // }}
        >
          {true &&
            props.data &&
            props.data.map((item, index) => {
              return (
                (props.selectedTabData
                  ? item.seriesID == props.selectedTabData.tourID ||
                    props.selectedTabData === ''
                  : true) && (
                  <SwiperSlide key={index} className="">
                    <Link href={HandleSwipeCLick(pegIndex)}>
                      <Score data={item} homePage={true} from={'schedule'} />
                      {/* -----------------------------completed match card starts---------------------- */}
                    </Link>
                  </SwiperSlide>
                )
              )
            })}
        </Swiper>

        <div className=" mx-2 lg:mx-0 md:mx-0  hidden lg:block md:block">
          <div
            style={{ zIndex: 1000 }}
            onClick={() => swiperRef.current?.slideNext()}
            id="swiper-button-prev"
            className="white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer"
          >
            <svg width="30" viewBox="0 0 24 24">
              <path
                fill="#000"
                d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
              ></path>
              <path fill="none" d="M0 0h24v24H0z"></path>
            </svg>
          </div>
          {/* <button  className='bg-red-500' onClick={() => swiperRef.current?.slideNext()}>Next</button> */}
        </div>
      </div>
      <div className="flex justify-center mt-2 lg:hidden md:hidden">
        {props.data &&
          props.data.map((item, key) => (
            <div
              key={key}
              className={`w-2 h-2  ${
                pegIndex == key ? 'bg-blue-8' : 'bg-blue-4 '
              } rounded-full m-1  `}
            >
              {' '}
            </div>
          ))}
      </div>
    </div>
  )
}

// import "./styles.css";

const SliderComponent = () => {
  const swiperRef = useRef()

  // For Typescript!
  // const swiperRef = useRef<SwiperCore>();

  return (
    <div>
      <button onClick={() => swiperRef.current?.slidePrev()}>Prev</button>

      <Swiper
        onBeforeInit={(swiper) => {
          swiperRef.current = swiper
        }}
        modules={[Navigation]}
        slidesPerView={3}
        className="mySwiper"
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
        <SwiperSlide>Slide 5</SwiperSlide>
      </Swiper>

      <button onClick={() => swiperRef.current?.slideNext()}>Next</button>
    </div>
  )
}
