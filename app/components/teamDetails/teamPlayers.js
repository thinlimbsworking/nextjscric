import React, { useState } from 'react'
const Playerfallback = '/pngsV2/playerph.png'
import { TEAM_PLAYER } from '../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Loading from '../loading'
import { getPlayerUrl } from '../../api/services'
import Link from 'next/link'
import Tab from '../shared/Tab'
const empty = '/svgs/Empty.svg'

const TabData = [
  { name: 'TEST', value: 'test' },
  { name: 'ODI', value: 'odi' },
  { name: 'T20', value: 't20' },
]

export default function TeamPlayers({ teamID }) {
  const [ActiveTab, setActiveTab] = useState(TabData[0])

  const matchType = {
    label: ['test', 'odi', 't20'],
    value: ['test', 'odi', 't20'],
  }
  const { loading, error, data } = useQuery(TEAM_PLAYER, {
    variables: { teamID: teamID },
    onCompleted: (PlayerData) => {
      if (PlayerData && PlayerData.teamPlayersV2) {
        setActiveTab(
          PlayerData.teamPlayersV2.hideTabs
            ? TabData[2]
            : PlayerData.teamPlayersV2.testSquad &&
              PlayerData.teamPlayersV2.testSquad.length !== 0
            ? TabData[0]
            : PlayerData.teamPlayersV2.odiSquad &&
              PlayerData.teamPlayersV2.odiSquad.length !== 0
            ? TabData[1]
            : TabData[2],
        )
      }
    },
  })

  if (error) return <div></div>
  else if (loading) {
    return <Loading />
  } else
    return (
      <>
        {data && data.teamPlayersV2 ? (
          <div className=" py-3">
            {
              <div className="flex items-center justify-center dark:w-8/12 dark:ml-16 w-6/12 ml-[24%] my-4 dark:mb-2">
                {/* {data &&
                  data.teamPlayersV2 &&
                  !data.teamPlayersV2.hideTabs &&
                  matchType.label.map((categ, i) => (
                    <div
                      key={i}
                      className=" flex  items-center   justify-center"
                      onClick={() => {
                        setActiveTab(categ)
                      }}
                    >
                      <div
                        className={` py-1 px-6 text-center text-sm font-semibold uppercase  ${
                          ActiveTab === categ
                            ? 'bg-gray-8 border-2 border-green-3'
                            : ' '
                        }  rounded-full cursor-pointer`}
                      >
                        {matchType.value[i]}
                      </div>
                    </div>
                  ))} */}
                {data.teamPlayersV2.odiSquad.length !== 0 && (
                  <Tab
                    type="block"
                    data={TabData}
                    handleTabChange={(item) => setActiveTab(item)}
                    selectedTab={ActiveTab}
                  />
                )}
              </div>
            }
            {data &&
            data.teamPlayersV2[`${ActiveTab?.value}Squad`].length > 0 ? (
              <div className="flex md:bg-white md:rounded-md md:shadow flex-wrap flex-row p-2">
                {data.teamPlayersV2[`${ActiveTab?.value}Squad`].map(
                  (player, i) => (
                    <div className="w-1/3 flex items-center justify-center md:w-1/6 lg:w-1/6">
                      {}
                      <div
                        key={i}
                        className="dark:w-full w-11/12 m-1 dark:bg-gray bg-white dark:border-gray border-2 border-solid border-[#E2E2E2] h-40 flex flex-col gap-3 items-center justify-center rounded-md md:h-64 lg:h-64"
                      >
                        <div className="lg:hidden md:hidden bg-gray-8 w-20 h-20 relative overflow-hidden cursor-pointer rounded-full md:h-32 lg:h-32 md:w-32 lg:w-32">
                          <Link
                            {...getPlayerUrl(player)}
                            passHref
                            legacyBehavior
                          >
                            <img
                              className="object-cover object-top dark:w-full dark:h-full h-[130px]"
                              src={`https://images.cricket.com/players/${player?.playerID}_headshot_safari.png`}
                              loading="lazy"
                              onError={(e) => {
                                e.target.src = Playerfallback
                              }}
                              alt=""
                            />
                          </Link>
                        </div>

                        <div className="lg:flex md:flex hidden">
                          <Link
                            {...getPlayerUrl(player)}
                            passHref
                            legacyBehavior
                          >
                            <img
                              className="object-cover object-top md:ml-4 dark:w-full dark:h-full h-36 mt-[6px]"
                              src={`https://images.cricket.com/players/${player?.playerID}_headshot_safari.png`}
                              loading="lazy"
                              onError={(e) => {
                                e.target.src = Playerfallback
                                e.target.className =
                                  'object-contain object-center h-48 w-24 '
                              }}
                              alt=""
                            />
                          </Link>
                          <div className="bg-gray-8 h-6 w-5 ml-2 mt-2.5 rounded-md">
                            {player?.playerRole === 'BATSMAN' ? (
                              <img
                                src="/svgs/whitebat.svg"
                                width="16px"
                                loading="lazy"
                                className="ml-0.5 mt-0.5"
                              />
                            ) : player?.playerRole === 'BOWLER' ? (
                              <img
                                src="/svgs/bowling.svg"
                                height="10px"
                                loading="lazy"
                                width="16px"
                                className="ml-0.5 mt-0.5"
                              />
                            ) : player?.playerRole === 'KEEPER' ? (
                              <img
                                src="/svgs/wicketKeeper.svg"
                                height="10px"
                                width="16px"
                                loading="lazy"
                                className="ml-0.5 mt-0.5"
                              />
                            ) : (
                              <img
                                src="/svgs/allRounderWhite.svg"
                                height="10px"
                                width="16px"
                                loading="lazy"
                                className="ml-0.5 mt-0.5"
                              />
                            )}
                          </div>
                        </div>

                        <div className="text-center flex items-center justify-center border-t-2 border-t-solid border-t-bg-gray-11 dark:border-none w-4/5 h-1/5 text-xs font-bold">
                          <div className="py-2">{player?.playerName}</div>
                        </div>
                      </div>
                    </div>
                  ),
                )}
              </div>
            ) : (
              <div className="dark:bg-gray md:bg-white fw5 text-centerf6 flex flex-col items-center justify-center py-4 ">
                <div className="w5 flex items-center justify-center ml3 h5">
                  <img className="h-full w-full" src={empty} alt="" />
                </div>
                <div>Player data not available</div>
              </div>
            )}
          </div>
        ) : (
          <div className="dark:bg-gray md:bg-white fw5 f6 text-centerflex flex-col items-center justify-center py-4">
            <div className="w5 flex items-center justify-center ml3 h5">
              <img className="h-full w-full" src={empty} alt="" />
            </div>
            <div>Player data not available</div>
          </div>
        )}
      </>
    )
}
