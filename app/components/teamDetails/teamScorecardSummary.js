import React from 'react'
import { MATCH_SUMMARY } from '../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Loading from './../loading'
const cricket = '/pngs/cricket.png'
const bowlerOverlay = '/pngs/bowlerOverlay.png'
const flagPlaceHolder = '/pngsV2/flag_dark.png'
const playerAvatar = '/placeHodlers/playerAvatar.png'
export default function TeamScorecardSummary(props) {
  const { loading, error, data } = useQuery(MATCH_SUMMARY, {
    variables: { matchID: props.matchID, status: props.status },
  })
  if (error) return <div className=""></div>
  else if (loading) {
    return <Loading />
  } else
    return (
      <>
        {data && data.matchSummary && (
          <div className="bg-navy shadow-3 lg:mx-2">
            <div className=" xl:hidden lg:hidden  ">
              <div className="">
                <div className="flex justify-between white items-center relative mx-4">
                  <img
                    className="absolute contain z-0 top-0 left-0"
                    width="100"
                    height="135"
                    alt={cricket}
                    src={cricket}
                  />

                  <img
                    className="h-32 z-0 px-4 border-0"
                    alt=""
                    src={`https://images.cricket.com/players/${data.matchSummary.bestBatsman.playerID}_headshot_safari.png`}
                    onError={(e) => {
                      e.target.src = playerAvatar
                    }}
                  />

                  <div className="flex justify-center items-center center">
                    <div className="flex justify-center items-center center">
                      <img
                        alt=""
                        src={`https://images.cricket.com/teams/${data.matchSummary.bestBatsman.playerTeamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = flagPlaceHolder
                        }}
                        className="ib h-5 w-16 shadow-4 mr-2"
                      />
                      <span className="text-sm font-medium">
                        {' '}
                        {data.matchSummary.bestBatsman.playerName}
                      </span>
                    </div>

                    {data.matchSummary.bestBatsman.battingStatsList &&
                    props.matchType !== 'Test' ? (
                      <div className="rounded white mt-2 bg-white-10 mr-4">
                        <div className="flex justify-between items-center">
                          <div className="flex justify-center items-center p-2 ">
                            <span className="text-base font-medium mr-1">
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].runs
                              }
                              {data.matchSummary.bestBatsman.battingStatsList[0]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="text-sm">
                              ({' '}
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].balls
                              }{' '}
                              )
                            </span>
                          </div>

                          <div className="flex justify-end p-1">
                            <span className="text-sm near-white bg-navy p-2 rounded text-center br--left ">
                              6s{' '}
                              <span className="white font-semibold">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].sixes
                                }
                              </span>{' '}
                            </span>
                            <span className="my-2 v-divider-dark" />
                            <span className="text-sm near-white bg-navy p-2 rounded text-center br--right">
                              4s{' '}
                              <span className="white font-semibold">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].fours
                                }
                              </span>{' '}
                            </span>
                          </div>
                        </div>

                        {data.matchSummary.bestBatsman.bowlingStatsList && (
                          <div>
                            <div className="flex  justify-between items-center">
                              <div className="flex w-40 justify-center items-center p-2">
                                <span className="text-base font-medium mr-1 nowrap">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-xs ">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              <div className="flex w-52 p-1 justify-end">
                                <span className="text-sm near-white w-full bg-navy py-2 rounded text-center nowrap">
                                  Eco:{' '}
                                  <span className="white font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].economyRate
                                    }
                                  </span>{' '}
                                </span>
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    ) : (
                      <div className="rounded white mt-2 bg-white-10 mr-2">
                        <div className="flex justify-between items-center">
                          <div className="flex justify-center items-center p-2">
                            <span className="text-base font-medium">
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].runs
                              }
                              {data.matchSummary.bestBatsman.battingStatsList[0]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                            <span className="text-xs pl-2">
                              (
                              {
                                data.matchSummary.bestBatsman
                                  .battingStatsList[0].balls
                              }
                              )
                            </span>
                          </div>
                          <div className="flex justify-center items-center p-2">
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <span className="text-base font-medium">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[1].runs
                                }
                              </span>
                            )}
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <span className="text-xs">{`(${data.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                            )}
                          </div>
                        </div>

                        {data.matchSummary.bestBatsman.bowlingStatsList && (
                          <div>
                            <div className="flex justify-between items-center">
                              <div className="flex justify-center items-center p-2">
                                <span className="text-base font-medium">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-xs pl-2">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="">&</div>
                              )}
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="flex justify-center items-center p-2">
                                  <span className="text-base font-medium">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].runsConceded
                                    }
                                  </span>
                                  <span className="text-xs pl-2">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].overs
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                            </div>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>

              {/* score details */}
              <div className="rounded mx-2 overflow-hidden md:flex lg:flex xl:flex flex-wrap ">
                {data.matchSummary.inningOrder
                  .slice(
                    0,
                    props.matchType !== 'Test'
                      ? 2
                      : data.matchSummary.inningOrder.length,
                  )
                  .map((inning, i) => (
                    <div
                      key={i}
                      className="bg-gray xl:w-52 lg:w-52 md:w-52 lg:border-r-solid lg:border-r-2 border-b-2 border-b-solid "
                    >
                      <div>
                        {/* Team Details */}

                        <div className="divider" />
                        <div className="flex bg-near-white justify-between items-center p-2">
                          <div className="flex items-center">
                            <img
                              alt=""
                              src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                              onError={(e) => {
                                e.target.src = flagPlaceHolder
                              }}
                              className="ib w-8 h-5 shadow-4 mr-2"
                            />
                            <div className="text-xs font-semibold">
                              {data.matchSummary[inning].teamName.toUpperCase()}
                            </div>
                          </div>
                          <div>
                            <span className="text-xs font-semibold">
                              {data.matchSummary[inning].runs[i <= 1 ? 0 : 1]}/
                              {
                                data.matchSummary[inning].wickets[
                                  i <= 1 ? 0 : 1
                                ]
                              }
                            </span>
                            <span className="text-xs font-medium">
                              {' '}
                              ({data.matchSummary[inning].overs[i <= 1 ? 0 : 1]}
                              )
                            </span>
                          </div>
                        </div>
                        <div className="divider" />

                        <div className="flex  justify-between ">
                          {data.matchSummary[inning][
                            i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                          ].topBatsman &&
                            data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].topBatsman.battingStatsList && (
                              <div className="flex w-full justify-between p-2 py-4">
                                <span className="text-xs font-medium truncate w-75">
                                  {
                                    data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.playerName
                                  }
                                </span>
                                <span className="text-xs font-medium">
                                  {
                                    data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.battingStatsList[0].runs
                                  }
                                  {data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].topBatsman.battingStatsList[0].isNotOut ===
                                  true
                                    ? '*'
                                    : ''}
                                </span>
                              </div>
                            )}

                          <div className="v-divider" />

                          {data.matchSummary[
                            inning === 'homeTeamData'
                              ? 'awayTeamData'
                              : 'homeTeamData'
                          ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                            .topBowler &&
                            data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .topBowler.bowlingStatsList && (
                              <div className="flex w-full justify-between p-2 py-4">
                                <span className="text-xs truncate w-75 font-medium">
                                  {
                                    data.matchSummary[
                                      inning === 'homeTeamData'
                                        ? 'awayTeamData'
                                        : 'homeTeamData'
                                    ][
                                      i <= 1
                                        ? 'bowlerSummary1'
                                        : 'bowlerSummary2'
                                    ].topBowler.playerName
                                  }
                                </span>
                                <span className="text-xs font-medium">
                                  {
                                    data.matchSummary[
                                      inning === 'homeTeamData'
                                        ? 'awayTeamData'
                                        : 'homeTeamData'
                                    ][
                                      i <= 1
                                        ? 'bowlerSummary1'
                                        : 'bowlerSummary2'
                                    ].topBowler.bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary[
                                      inning === 'homeTeamData'
                                        ? 'awayTeamData'
                                        : 'homeTeamData'
                                    ][
                                      i <= 1
                                        ? 'bowlerSummary1'
                                        : 'bowlerSummary2'
                                    ].topBowler.bowlingStatsList[0].runsConceded
                                  }
                                </span>
                              </div>
                            )}
                        </div>

                        <div className="divider" />

                        {props.matchType !== 'Test' && (
                          <div className="flex  justify-between ">
                            {data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].runnerBatsman &&
                              data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].runnerBatsman.battingStatsList && (
                                <div className="flex w-full justify-between p-2 py-4">
                                  <span className="text-xs truncate w-75 font-medium">
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].runnerBatsman.playerName
                                    }
                                  </span>
                                  <span className="text-xs font-medium">
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].runnerBatsman.battingStatsList[0].runs
                                    }
                                    {data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].runnerBatsman.battingStatsList[0]
                                      .isNotOut === true
                                      ? '*'
                                      : ''}
                                  </span>
                                </div>
                              )}

                            <div className="v-divider" />

                            {data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .runnerBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler.bowlingStatsList && (
                                <div className="flex w-full justify-between p-2  py-4">
                                  <span className="text-xs truncate w-75 font-medium">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.playerName
                                    }
                                  </span>
                                  <span className="text-xs font-medium">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0]
                                        .runsConceded
                                    }
                                  </span>
                                </div>
                              )}
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
              </div>

              {/* topBowler */}

              {data.matchSummary.bestBowler.bowlingStatsList &&
              props.matchType !== 'Test' ? (
                <div className="flex justify-between white items-center mx-2 py-2 mt-4">
                  {/* player NAME */}
                  <div className="flex items-center">
                    <img
                      alt=""
                      src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                      onError={(e) => {
                        e.target.src = flagPlaceHolder
                      }}
                      className="ib w-16 h-1 shadow-4"
                    />
                    <div className="text-sm px-2 font-medium">
                      {data.matchSummary.bestBowler.playerName}
                    </div>
                  </div>

                  {/* player Image */}
                  <div className="relative ">
                    <div className="overflow-hidden absolute bottom-0 -mt-2">
                      <img
                        className="object-cover object-top"
                        width="85"
                        height="80"
                        alt=""
                        src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                        onError={(e) => {
                          e.target.src = playerAvatar
                        }}
                      />
                    </div>
                    <img width="85" alt={bowlerOverlay} src={bowlerOverlay} />
                  </div>

                  {/* player stats */}
                  <div className="">
                    {data.matchSummary.bestBowler.battingStatsList &&
                      (data.matchSummary.bestBowler.battingStatsList[0].runs !==
                        0 ||
                        data.matchSummary.bestBowler.battingStatsList[0]
                          .balls !== 0) && (
                        <div className="flex justify-center items-center p-1">
                          <span className="text-base font-medium mr-1 nowrap">
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .runs
                            }
                            {data.matchSummary.bestBowler.battingStatsList[0]
                              .isNotOut === true
                              ? '*'
                              : ''}
                          </span>
                          <span className="text-xs ">
                            (
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .balls
                            }
                            )
                          </span>
                        </div>
                      )}
                    {data.matchSummary.bestBowler.bowlingStatsList && (
                      <div className="flex justify-center items-center p-1">
                        <span className="text-base font-medium mr-1 nowrap">
                          {
                            data.matchSummary.bestBowler.bowlingStatsList[0]
                              .wickets
                          }
                          /
                          {
                            data.matchSummary.bestBowler.bowlingStatsList[0]
                              .runsConceded
                          }
                        </span>
                        <span className="text-xs ">
                          (
                          {
                            data.matchSummary.bestBowler.bowlingStatsList[0]
                              .overs
                          }
                          )
                        </span>
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <div className="flex justify-between white items-center relative mx-4">
                  {/* player Image */}
                  <div>
                    <img
                      className="absolute contain z-0 top-0 left-0 bottom-0 "
                      width="80"
                      height="70"
                      alt={bowlerOverlay}
                      src={bowlerOverlay}
                    />
                    <img
                      className="nt3 h35 "
                      alt=""
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                      onError={(e) => {
                        e.target.src = playerAvatar
                      }}
                    />
                  </div>

                  {/* player NAME */}
                  <div className="flex justify-center items-center center">
                    <img
                      alt=""
                      src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                      onError={(e) => {
                        e.target.src = flagPlaceHolder
                      }}
                      className="w-16 h-5 shadow-4 mr-2"
                    />
                    <div className="text-sm font-light">
                      {data.matchSummary.bestBowler.playerName}
                    </div>
                  </div>

                  {/* player stats */}
                  <div className="flex justify-between items-center py-4 w-44">
                    <div className="">
                      {data.matchSummary.bestBowler.battingStatsList && (
                        <div className="flex justify-start items-center py-1">
                          <span className="text-sm font-medium">
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .runs
                            }
                            {data.matchSummary.bestBowler.battingStatsList[0]
                              .isNotOut === true
                              ? '*'
                              : ''}
                          </span>
                          <span className="text-xs">
                            &nbsp;(
                            {
                              data.matchSummary.bestBowler.battingStatsList[0]
                                .balls
                            }
                            )
                          </span>
                        </div>
                      )}
                      {data.matchSummary.bestBowler.bowlingStatsList && (
                        <div className="flex justify-start items-center py-1">
                          <span className="text-sm font-medium">
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .wickets
                            }
                            /
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .runsConceded
                            }
                          </span>
                          {/* <span>&nbsp;</span> */}
                          <span className="text-xs ">
                            &nbsp;(
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                    <div className="">
                      {data.matchSummary.bestBowler.battingStatsList[1] && (
                        <div className="pa1 text-xs">&</div>
                      )}
                      {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                        <div className="pa1 text-xs">&</div>
                      )}
                    </div>
                    <div className="">
                      {data.matchSummary.bestBowler.battingStatsList && (
                        <div className="flex justify-start items-center py-1">
                          {data.matchSummary.bestBowler.battingStatsList[1] && (
                            <span className="text-sm font-medium">
                              {
                                data.matchSummary.bestBowler.battingStatsList[1]
                                  .runs
                              }
                              {data.matchSummary.bestBowler.battingStatsList[1]
                                .isNotOut === true
                                ? '*'
                                : ''}
                            </span>
                          )}
                          {data.matchSummary.bestBowler.battingStatsList[1] && (
                            <span className="text-xs ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.battingStatsList[1]
                                  .balls
                              }
                              )
                            </span>
                          )}
                        </div>
                      )}
                      {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                        <div className="flex justify-start items-center py-2">
                          <span className="text-sm text-end pl-2 font-medium">
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[1]
                                .wickets
                            }
                            /
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[1]
                                .runsConceded
                            }
                          </span>
                          <span className="text-xs text-end ">
                            &nbsp;(
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[1]
                                .overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}
            </div>

            {/* -------------------------------------------------------desktop only----------------------------------------------------------------------- */}
            <div className="hidden">
              <div className="bg-white flex">
                <div className="w-52 bg-navy relative">
                  {/* ---------------top batman---------------------------- */}
                  <div className="flex justify-start items-center h-42">
                    <img
                      className="h-full z-0 px-4 border-0"
                      alt=""
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBatsman.playerID}_headshot_safari.png`}
                      onError={(e) => {
                        e.target.src = playerAvatar
                      }}
                    />

                    <div className="">
                      <div className="white flex items-center p-2">
                        <img
                          alt=""
                          src={`https://images.cricket.com/teams/${data.matchSummary.bestBatsman.playerTeamID}_flag_safari.png`}
                          onError={(e) => {
                            e.target.src = flagPlaceHolder
                          }}
                          className="ib w-16 h-1 shadow-4 mr-2"
                        />
                        <span className="text-base font-light">
                          {' '}
                          {data.matchSummary.bestBatsman.playerName}
                        </span>
                      </div>

                      {data.matchSummary.bestBatsman.battingStatsList &&
                      props.matchType !== 'Test' ? (
                        // -----------------------------------ODI && T20----------------------------------------------

                        <div className="rounded white mt-2 mr-4">
                          <div className="flex justify-between items-center">
                            <div className="flex justify-center items-center py-2 px-3 bg-white-10 rounded-full">
                              <span className="text-base font-medium mr-1">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="text-sm font-medium white-80">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>

                            <div className="flex justify-end p-1">
                              <span className="text-base white-80 p-2 font-medium">
                                6s{' '}
                                <span className="white-90 font-semibold">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].sixes
                                  }
                                </span>{' '}
                              </span>
                              <span className="text-base white-80 p-2 font-medium">
                                4s{' '}
                                <span className="white-90 font-semibold">
                                  {
                                    data.matchSummary.bestBatsman
                                      .battingStatsList[0].fours
                                  }
                                </span>{' '}
                              </span>
                            </div>
                          </div>

                          {data.matchSummary.bestBatsman.bowlingStatsList && (
                            <div className="flex justify-between items-center">
                              <div className="flex justify-center items-center py-2 px-3 bg-white-10 rounded-full">
                                <span className="text-base font-medium mr-1 nowrap">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-xs ">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              <div className="flex px-4 py-2 justify-center items-center bg-white-10 rounded-full">
                                <span className="text-sm near-white w-full rounded text-center">
                                  Eco:{' '}
                                  <span className="white-90 font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[0].economyRate
                                    }
                                  </span>{' '}
                                </span>
                              </div>
                            </div>
                          )}
                        </div>
                      ) : (
                        // ***************************************************  Test Match Top Batman *****************************************************
                        <div className="rounded white mt-2 mr-4">
                          <div className="flex justify-between items-center">
                            <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-4">
                              <span className="text-base font-medium mr-1">
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="text-sm font-medium white-80">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <div className="px-2">&</div>
                            )}
                            {data.matchSummary.bestBatsman
                              .battingStatsList[1] && (
                              <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-4">
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[1] && (
                                  <span className="text-base font-medium">
                                    {
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[1].runs
                                    }
                                  </span>
                                )}
                                {data.matchSummary.bestBatsman
                                  .battingStatsList[1] && (
                                  <span className="text-sm font-medium white-80">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .battingStatsList[1].balls
                                    }
                                    )
                                  </span>
                                )}
                              </div>
                            )}
                          </div>

                          {data.matchSummary.bestBatsman.bowlingStatsList && (
                            <div className="flex justify-between items-center mt-2">
                              <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-4">
                                <span className="text-base font-medium">
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].wickets
                                  }
                                  /
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].runsConceded
                                  }
                                </span>
                                <span className="text-sm font-medium white-80">
                                  (
                                  {
                                    data.matchSummary.bestBatsman
                                      .bowlingStatsList[0].overs
                                  }
                                  )
                                </span>
                              </div>
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="px-2">&</div>
                              )}
                              {data.matchSummary.bestBatsman
                                .bowlingStatsList[1] && (
                                <div className="flex justify-center items-center py-2 bg-white-10 rounded-full px-4">
                                  <span className="text-base font-medium">
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].runsConceded
                                    }
                                  </span>
                                  <span className="text-sm font-medium white-80">
                                    (
                                    {
                                      data.matchSummary.bestBatsman
                                        .bowlingStatsList[1].overs
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="border-b-solid border-b-2 border-white mb-2" />

                  {/* *---------------------------- TOP Bowler desktop -----------------------------* */}
                  <div>
                    {data.matchSummary.bestBowler.bowlingStatsList &&
                    props.matchType !== 'Test' ? (
                      /* ************************************* TOP Bowler ODI &&  T20************************************************************* */

                      <div className="flex justify-start white items-center h-4  ">
                        {/* player Image */}
                        <img
                          className="h-full z-0 px-4 border-0"
                          alt=""
                          src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                          onError={(e) => {
                            e.target.src = playerAvatar
                          }}
                        />

                        <div className="pl-4">
                          {/* player NAME */}
                          <div className="flex items-center white py-2">
                            <img
                              alt=""
                              src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                              onError={(e) => {
                                e.target.src = flagPlaceHolder
                              }}
                              className="ib w-16 h-1 shadow-4 mr-2"
                            />
                            <div className="text-base font-medium">
                              {data.matchSummary.bestBowler.playerName}
                            </div>
                          </div>

                          {/* player stats */}
                          <div className="">
                            {data.matchSummary.bestBowler.battingStatsList &&
                              (data.matchSummary.bestBowler.battingStatsList[0]
                                .runs !== 0 ||
                                data.matchSummary.bestBowler.battingStatsList[0]
                                  .balls !== 0) && (
                                <div className="flex justify-center items-center">
                                  <div className="py-2 px-4 rounded-full bg-white-10">
                                    <span className="text-base font-medium mr-1">
                                      {
                                        data.matchSummary.bestBowler
                                          .battingStatsList[0].runs
                                      }
                                      {data.matchSummary.bestBowler
                                        .battingStatsList[0].isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                    <span className="text-sm font-medium white-80 ">
                                      (
                                      {
                                        data.matchSummary.bestBowler
                                          .battingStatsList[0].balls
                                      }
                                      )
                                    </span>
                                  </div>
                                </div>
                              )}
                            {data.matchSummary.bestBowler.bowlingStatsList && (
                              <div className="flex justify-center items-center pt2">
                                <div className=" bg-white-10 rounded-full py-2 px-4 ">
                                  <span className="text-base font-medium mr-1">
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="text-sm font-medium white-80 ">
                                    (
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    ) : (
                      /* ************************************* TOP Bowler Test ************************************************************* */

                      <div className="flex justify-start white items-center white-80 mt-2 h-4 ">
                        {/* player Image */}

                        <img
                          className="h-full px-4 border-0 "
                          alt=""
                          src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                          onError={(e) => {
                            e.target.src = playerAvatar
                          }}
                        />

                        {/* player NAME */}
                        <div className="pl-4 ">
                          <div className="flex justify-start items-center py-2">
                            <img
                              alt=""
                              src={`https://images.cricket.com/teams/${data.matchSummary.bestBowler.playerTeamID}_flag_safari.png`}
                              onError={(e) => {
                                e.target.src = flagPlaceHolder
                              }}
                              className="w-16 h-1 shadow-4 mr-2"
                            />
                            <div className="text-base font-medium">
                              {data.matchSummary.bestBowler.playerName}
                            </div>
                          </div>

                          {/* player stats */}
                          <div className="mb-2">
                            <div className="flex items-center">
                              {data.matchSummary.bestBowler
                                .battingStatsList && (
                                <div className="flex justify-center items-center bg-white-10 rounded-full py-2 px-4">
                                  <span className="text-base font-medium">
                                    {
                                      data.matchSummary.bestBowler
                                        .battingStatsList[0].runs
                                    }
                                    {data.matchSummary.bestBowler
                                      .battingStatsList[0].isNotOut === true
                                      ? '*'
                                      : ''}
                                  </span>
                                  <span className="text-sm font-medium white-80">
                                    &nbsp;(
                                    {
                                      data.matchSummary.bestBowler
                                        .battingStatsList[0].balls
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                              {data.matchSummary.bestBowler
                                .battingStatsList[1] && (
                                <div className="ph-1 text-base font-medium ">
                                  &
                                </div>
                              )}
                              {data.matchSummary.bestBowler.battingStatsList &&
                                data.matchSummary.bestBowler
                                  .battingStatsList[1] && (
                                  <div className="flex justify-center items-center py-1 bg-white-10 rounded-full m-1 px-4">
                                    {
                                      <span className="text-base font-medium">
                                        {
                                          data.matchSummary.bestBowler
                                            .battingStatsList[1].runs
                                        }
                                        {data.matchSummary.bestBowler
                                          .battingStatsList[1].isNotOut === true
                                          ? '*'
                                          : ''}
                                      </span>
                                    }
                                    {
                                      <span className="text-sm font-medium white-80 ">
                                        &nbsp;(
                                        {
                                          data.matchSummary.bestBowler
                                            .battingStatsList[1].balls
                                        }
                                        )
                                      </span>
                                    }
                                  </div>
                                )}
                            </div>

                            <div className="flex items-center mt-2">
                              {data.matchSummary.bestBowler
                                .bowlingStatsList && (
                                <div className="flex justify-center items-center bg-white-10 rounded-full py-2 px-4">
                                  <span className="text-base font-medium">
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].runsConceded
                                    }
                                  </span>
                                  <span className="text-sm font-medium white-80">
                                    &nbsp;(
                                    {
                                      data.matchSummary.bestBowler
                                        .bowlingStatsList[0].overs
                                    }
                                    )
                                  </span>
                                </div>
                              )}
                              {data.matchSummary.bestBowler
                                .bowlingStatsList[1] && (
                                <div className="ph-1 text-base font-medium ">
                                  &
                                </div>
                              )}
                              {data.matchSummary.bestBowler
                                .bowlingStatsList[1] && (
                                <div>
                                  <div className="flex justify-start items-center bg-white-10 rounded-full ma1 py-2 px-4">
                                    <span className="text-base font-medium">
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[1].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[1].runsConceded
                                      }
                                    </span>
                                    <span className="text-sm font-medium white-80 ">
                                      &nbsp;(
                                      {
                                        data.matchSummary.bestBowler
                                          .bowlingStatsList[1].overs
                                      }
                                      )
                                    </span>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                {/* ************************************************** Team score details ****************************************************************** */}

                <div className="w-52 ">
                  <div className="rounded m-2 ">
                    {data.matchSummary.inningOrder
                      .slice(
                        0,
                        props.matchType !== 'Test'
                          ? 2
                          : data.matchSummary.inningOrder.length,
                      )
                      .map((inning, i) => (
                        <div key={i} className="bg-white">
                          <div>
                            {/* Team Details */}

                            <div className="flex justify-between items-center p-2">
                              <div className="flex items-center">
                                <img
                                  alt=""
                                  src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                                  onError={(e) => {
                                    e.target.src = flagPlaceHolder
                                  }}
                                  className="ib w-16 h-1 shadow-4 mr-4"
                                />
                                <div className="text-sm font-semibold">
                                  {data.matchSummary[
                                    inning
                                  ].teamName.toUpperCase()}
                                </div>
                              </div>
                              <div>
                                <span className="text-sm font-bold darkRed">
                                  {
                                    data.matchSummary[inning].runs[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                  /
                                  {
                                    data.matchSummary[inning].wickets[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                </span>
                                <span className="text-xs font-semibold gray pl-2">
                                  {' '}
                                  (
                                  {
                                    data.matchSummary[inning].overs[
                                      i <= 1 ? 0 : 1
                                    ]
                                  }
                                  )
                                </span>
                              </div>
                            </div>

                            <div className="flex justify-between ">
                              {data.matchSummary[inning][
                                i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                              ].topBatsman &&
                                data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].topBatsman.battingStatsList && (
                                  <div className="flex w-full justify-between p-2 py-2">
                                    <span className="text-sm font-medium gray">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerName
                                      }
                                    </span>
                                    <span className="text-sm font-semibold black-80">
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.battingStatsList[0].runs
                                      }
                                      {data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.battingStatsList[0]
                                        .isNotOut === true
                                        ? '*'
                                        : ''}
                                    </span>
                                  </div>
                                )}
                              <div className="border-r-solid border-r-2 border-gray mt-1" />
                              {data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .topBowler &&
                                data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .topBowler.bowlingStatsList && (
                                  <div className="flex w-full justify-between p-2 py-2">
                                    <span className="text-sm font-medium gray">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerName
                                      }
                                    </span>
                                    <span className="text-sm font-semibold black-80">
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.bowlingStatsList[0].wickets
                                      }
                                      /
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.bowlingStatsList[0]
                                          .runsConceded
                                      }
                                    </span>
                                  </div>
                                )}
                            </div>

                            {props.matchType !== 'Test' && (
                              <div className="flex justify-between">
                                {data.matchSummary[inning][
                                  i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                                ].runnerBatsman &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman.battingStatsList && (
                                    <div className="flex w-full justify-between p-2 py-4">
                                      <span className="text-sm font-medium gray">
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.playerName
                                        }
                                      </span>
                                      <span className="text-sm font-semibold black-80">
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.battingStatsList[0]
                                            .runs
                                        }
                                        {data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.battingStatsList[0]
                                          .isNotOut === true
                                          ? '*'
                                          : ''}
                                      </span>
                                    </div>
                                  )}

                                <div className="border-r-2 border-r-solid border-gray mb-2" />

                                {data.matchSummary[
                                  inning === 'homeTeamData'
                                    ? 'awayTeamData'
                                    : 'homeTeamData'
                                ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                  .runnerBowler &&
                                  data.matchSummary[
                                    inning === 'homeTeamData'
                                      ? 'awayTeamData'
                                      : 'homeTeamData'
                                  ][
                                    i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                  ].runnerBowler.bowlingStatsList && (
                                    <div className="flex w-full justify-between p-2  py-4">
                                      <span className="text-sm font-medium gray">
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.playerName
                                        }
                                      </span>
                                      <span className="text-sm font-semibold black-80">
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.bowlingStatsList[0]
                                            .wickets
                                        }
                                        /
                                        {
                                          data.matchSummary[
                                            inning === 'homeTeamData'
                                              ? 'awayTeamData'
                                              : 'homeTeamData'
                                          ][
                                            i <= 1
                                              ? 'bowlerSummary1'
                                              : 'bowlerSummary2'
                                          ].runnerBowler.bowlingStatsList[0]
                                            .runsConceded
                                        }
                                      </span>
                                    </div>
                                  )}
                              </div>
                            )}
                            {((props.matchType !== 'Test' && i === 0) ||
                              (props.matchType === 'Test' &&
                                data.matchSummary.inningOrder.length >
                                  i + 1)) && (
                              <div className="border-b-2 border-b-solid border-gray pt-2"></div>
                            )}
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </>
    )
}
