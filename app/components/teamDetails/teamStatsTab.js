import React, { useState } from 'react'
const Playerfallback = '/pngsV2/playerph.png'
import { TEAM_STATS_V2 } from '../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Loading from '../loading'
const empty = '/svgs/Empty.svg'
import DataNotFound from '../commom/datanotfound'
import Tab from '../shared/Tab'

const TabData = [
  { name: 'TEST', value: 'test' },
  { name: 'ODI', value: 'odi' },
  { name: 'T20', value: 't20' },
]

export default function TeamStatsTab({ teamID }) {
  const [ActiveTab, setActiveTab] = useState(TabData[0])

  const matchType = {
    label: ['test', 'odi', 't20'],
    value: ['test', 'odi', 't20'],
  }

  const BestScores = [
    { label: 'mostRuns', value: 'Most Runs' },
    { label: 'mostWickets', value: 'Most Wickets' },
    { label: 'bestScore', value: 'Best Scores' },
    { label: 'bestFigures', value: 'Best Figures' },
  ]

  const { loading, error, data: teamDiscovery } = useQuery(TEAM_STATS_V2, {
    variables: { teamID: teamID },
    onCompleted: (data) => {
      if (data && data.teamStats) {
        setActiveTab(data.teamStats.hideTabs ? TabData[2] : TabData[0])
      }
    },
  })

  if (error) {
    return <div>{error}</div>
  }

  if (loading) {
    return <Loading />
  }
  if (teamDiscovery) {
    return (
      <>
        {teamDiscovery && teamDiscovery.teamStats ? (
          <div className="dark:bg-gray-8 bh-white mb-16 lg:mb-24 xl:mb-24 md:mb-24 dark:text-white">
            {teamDiscovery &&
              teamDiscovery.teamStats &&
              !teamDiscovery.teamStats.hideTabs && (
                <div className="flex items-center justify-center lg:w-6/12 dark:bg-gray-4 lg:ml-[20%] lg:rounded-full rounded-full mx-10 my-4">
                  <Tab
                    type="block"
                    data={TabData}
                    handleTabChange={(item) => setActiveTab(item)}
                    selectedTab={ActiveTab}
                  />
                </div>
              )}
            <div className="md:bg-white md:rounded-md md:p-3 md:shadow">
              <div className="flex pt-4 leading-6 text-xs font-semibold w-full md:w-full lg:w-full md:text-center md:justify-around xl:justify-around lg:justify-around">
                <div className="w-20 text-center">
                  <div className="font-medium text-sm lg:text-lg lg:font-bold text-black whitespace-normal uppercase dark:text-gray-2">
                    Matches
                  </div>
                  <div className="pt-2 dark:font-medium font-bold dark:text-green-4 text-basered text-xl">
                    {teamDiscovery &&
                      teamDiscovery.teamStats &&
                      teamDiscovery.teamStats[ActiveTab?.value] &&
                      teamDiscovery.teamStats[ActiveTab?.value].matchesPlayed}
                  </div>
                </div>
                <div className="dark:border-l-gray border-l-[#E2E2E2] border-l-2"></div>
                <div className="w-20 text-center">
                  <div className="font-medium text-sm lg:text-lg uppercase lg:font-bold text-black whitespace-normal dark:text-gray-2">
                    Won
                  </div>
                  <div className="pt-2 dark:font-medium font-bold dark:text-green-4 text-basered text-xl">
                    {teamDiscovery &&
                      teamDiscovery.teamStats &&
                      teamDiscovery.teamStats[ActiveTab?.value] &&
                      teamDiscovery.teamStats[ActiveTab?.value].matchesWon}{' '}
                  </div>
                </div>
                <div className="dark:border-l-gray border-l-[#E2E2E2] border-l-2"></div>
                <div className="w-20 text-center">
                  <div className="font-medium text-sm lg:text-lg uppercase lg:font-bold text-black whitespace-normal dark:text-gray-2">
                    Lost{' '}
                  </div>
                  <div className="pt-2 dark:font-medium font-bold dark:text-green-4 text-basered text-xl">
                    {teamDiscovery &&
                      teamDiscovery.teamStats &&
                      teamDiscovery.teamStats[ActiveTab?.value] &&
                      teamDiscovery.teamStats[ActiveTab?.value].matchesLost}
                  </div>
                </div>
                <div className="dark:border-l-gray border-l-[#E2E2E2] border-l-2"></div>
                <div className="w-20 text-center">
                  <div className="font-medium text-sm lg:text-lg uppercase lg:font-bold text-black whitespace-normal dark:text-gray-2">
                    Tied
                  </div>
                  <div className="pt-2 dark:font-medium font-bold dark:text-green-4 text-basered text-xl">
                    {teamDiscovery &&
                      teamDiscovery.teamStats &&
                      teamDiscovery.teamStats[ActiveTab?.value] &&
                      teamDiscovery.teamStats[ActiveTab?.value].matchesTied}
                  </div>
                </div>
                <div className="dark:border-l-gray border-l-[#E2E2E2] border-l-2"></div>
                <div className="w-20 text-center">
                  <div className="font-medium text-sm lg:text-lg uppercase lg:font-bold text-black whitespace-normal dark:text-gray-2">
                    W/L
                  </div>
                  <div className="pt-2 dark:font-medium font-bold dark:text-green-4 text-basered text-xl">
                    {teamDiscovery &&
                      teamDiscovery.teamStats &&
                      teamDiscovery.teamStats[ActiveTab?.value] &&
                      teamDiscovery.teamStats[ActiveTab?.value].winPercentage}
                  </div>
                </div>
              </div>
              <div className="py-2 border-b-black"></div>
              <div className="flex flex-col flex-wrap lg:flex-row md:flex-row p-2">
                {BestScores.map((card, i) => (
                  <div
                    className="w-full md:w-6/12 
                 "
                    key={i}
                  >
                    <div className="lg:mx-2 my-2 dark:bg-gray-4 overflow-hidden">
                      <div className="font-medium text-xs dark:text-white text-black dark:bg-gray-6 p-2 flex lg:mx-[1%] border border-solid border-gray-4 md:border-gray-11">
                        {card.value}
                      </div>
                      {/* <div className='py-2 ml-56  items-center  justify-center font-medium text-xs sm:text-sm lg:text-sm '>
                      {teamDiscovery &&
                        teamDiscovery.teamStats &&
                        teamDiscovery.teamStats[ActiveTab?.value] &&
                        teamDiscovery.teamStats[ActiveTab?.value][card.label] &&
                        teamDiscovery.teamStats[ActiveTab?.value][card.label].playerName}
                    </div> */}
                      <div className="flex lg:mx-2 md:mx-2 xl:mx-2 relative dark:border-basebg border border-solid border-gray-4 md:border-[#E2E2E2] ">
                        <div className="overflow-hidden md:m-4 m-2 object-cover object-top">
                          {/* <img
                          className="absolute h-5 w-8 rounded-sm border-solid ml-16 my-20"
                          src={`https://images.cricket.com/teams/${teamID}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        /> */}
                          {console.log(
                            teamDiscovery.teamStats[ActiveTab?.value][
                              card.label
                            ].playerID,
                            'lableeeeee',
                          )}
                          <img
                            className="md:w-32 md:pt-2 md:px-2 pr-6 md:h-48 object-cover object-top h-32"
                            src={`https://images.cricket.com/players/${
                              teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab?.value] &&
                              teamDiscovery.teamStats[ActiveTab?.value][
                                card.label
                              ] &&
                              teamDiscovery.teamStats[ActiveTab?.value][
                                card.label
                              ].playerID
                            }_headshot_safari.png`}
                            alt={Playerfallback}
                            loading="lazy"
                            onError={(evt) => {
                              evt.target.src = Playerfallback
                              evt.target.className =
                                'object-contain object-center dark:w-32 dark:pr-4 dark:h-32 h-48 w-24'
                            }}
                          />
                        </div>
                        {card.label === 'mostRuns' && (
                          <>
                            <div className="flex justify-between">
                              <div className="flex flex-col dark:py-4 md:mt-16 pr-2">
                                <div className="text-sm truncate md:py-2 dark:text-white text-black font-bold">
                                  {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].playerName}
                                </div>

                                <div className="py-2 flex items-center text-right justify-between">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].matchesPlayed !== '' ? (
                                    <span className="dark:text-gray-2 text-black text-xs uppercase">
                                      Mat :
                                    </span>
                                  ) : (
                                    ''
                                  )}
                                  <span className="dark:pl-16 pl-4 dark:text-green-6 text-basered text-xs font-medium text-right">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].matchesPlayed !== ''
                                      ? teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].matchesPlayed
                                      : ' '}
                                  </span>
                                </div>
                                <div className="py-2 flex items-center text-right justify-between">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].average !== '' ? (
                                    <span className="dark:text-gray-2 text-black text-xs uppercase">
                                      Avg :
                                    </span>
                                  ) : (
                                    ''
                                  )}
                                  <span className="dark:pl-16 pl-4 dark:text-green-6 text-basered text-xs font-medium text-right">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].average !== ''
                                      ? teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].average
                                      : ' '}
                                  </span>
                                </div>
                                <div className="py-2 lg:hidden md:hidden flex items-center text-right justify-between">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].runs !== '' ? (
                                    <span className="dark:text-gray-2 text-black text-xs uppercase">
                                      Runs :
                                    </span>
                                  ) : (
                                    ''
                                  )}
                                  <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].runs !== ''
                                      ? teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].runs
                                      : ' '}
                                  </span>
                                </div>
                              </div>
                              <div className="lg:flex md:flex hidden items-center text-right justify-between">
                                {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab?.value] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ].runs !== '' ? (
                                  <span className="dark:text-gray-2 text-black ml-24 text-xs uppercase">
                                    Runs :
                                  </span>
                                ) : (
                                  ''
                                )}
                                <span className="pl-1 text-basered text-xs font-medium text-right">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].runs !== ''
                                    ? teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].runs
                                    : ' '}
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {card.label === 'mostWickets' && (
                          <>
                            <div className="flex justify-around">
                              <div className="flex md:mt-16 flex-col dark:py-4">
                                <div className="text-sm md:py-2 dark:text-white truncate text-black font-bold">
                                  {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].playerName}
                                </div>
                                <div className="py-2 flex items-center text-right justify-between">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].matchesPlayed !== '' ? (
                                    <span className="dark:text-gray-2 text-black text-xs uppercase">
                                      Mat :
                                    </span>
                                  ) : (
                                    ''
                                  )}
                                  <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].matchesPlayed !== ''
                                      ? teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].matchesPlayed
                                      : ' '}
                                  </span>
                                </div>

                                {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].economy !== '' && (
                                    <div className="py-2 flex items-center text-right justify-between">
                                      {teamDiscovery &&
                                      teamDiscovery.teamStats &&
                                      teamDiscovery.teamStats[
                                        ActiveTab?.value
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].economy !== '' ? (
                                        <span className="dark:text-gray-2 text-black text-xs uppercase">
                                          Eco :
                                        </span>
                                      ) : (
                                        ''
                                      )}
                                      <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                        {teamDiscovery &&
                                        teamDiscovery.teamStats &&
                                        teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ] &&
                                        teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label] &&
                                        teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].economy !== ''
                                          ? teamDiscovery.teamStats[
                                              ActiveTab?.value
                                            ][card.label].economy
                                          : ' '}
                                      </span>
                                    </div>
                                  )}

                                {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].wicket !== '' && (
                                    <div className="py-2 flex lg:hidden md:hidden items-center text-right justify-between">
                                      {teamDiscovery &&
                                      teamDiscovery.teamStats &&
                                      teamDiscovery.teamStats[
                                        ActiveTab?.value
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].wicket !== '' ? (
                                        <span className="text-gray-2 text-xs uppercase">
                                          Wkts :
                                        </span>
                                      ) : (
                                        ''
                                      )}
                                      <span className="pl-16 text-green-6 text-xs font-medium text-right">
                                        {teamDiscovery &&
                                        teamDiscovery.teamStats &&
                                        teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ] &&
                                        teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label] &&
                                        teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].wicket !== ''
                                          ? teamDiscovery.teamStats[
                                              ActiveTab?.value
                                            ][card.label].wicket
                                          : ' '}
                                      </span>
                                    </div>
                                  )}
                              </div>
                              <div className="py-2 lg:flex mt-10 md:flex hidden items-center text-right justify-between">
                                {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab?.value] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ].wicket !== '' ? (
                                  <span className="dark:text-gray-2 text-black -mt-6 ml-24 text-xs uppercase">
                                    Wkts :
                                  </span>
                                ) : (
                                  ''
                                )}
                                <span className="pl-4 text-basered text-xs -mt-6 font-medium text-right">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].wicket !== ''
                                    ? teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].wicket
                                    : ' '}
                                </span>
                              </div>
                            </div>
                          </>
                        )}

                        {card.label === 'bestScore' && (
                          <>
                            <div className="flex flex-col dark:py-4 md:mt-16 md:pl-20 pr-2">
                              <div className="text-sm truncate md:py-2 dark:text-white text-black font-bold">
                                {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].playerName}
                              </div>

                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab?.value] &&
                              teamDiscovery.teamStats[ActiveTab?.value][
                                card.label
                              ] &&
                              teamDiscovery.teamStats[ActiveTab?.value][
                                card.label
                              ].matchesPlayed !== '' ? (
                                <div className="py-2 flex items-center text-right justify-between">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].matchesPlayed !== '' ? (
                                    <span className="dark:text-gray-2 text-black text-xs uppercase">
                                      Mat :
                                    </span>
                                  ) : (
                                    ''
                                  )}
                                  <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].matchesPlayed !== ''
                                      ? teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].matchesPlayed
                                      : ' '}
                                  </span>
                                </div>
                              ) : (
                                <div />
                              )}

                              {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab?.value] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ].average !== '' && (
                                  <div className="py-2 flex  items-center text-right justify-between">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].average !== '' ? (
                                      <span>Avg :</span>
                                    ) : (
                                      ''
                                    )}
                                    <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                      {teamDiscovery &&
                                      teamDiscovery.teamStats &&
                                      teamDiscovery.teamStats[
                                        ActiveTab?.value
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].average !== ''
                                        ? teamDiscovery.teamStats[
                                            ActiveTab?.value
                                          ][card.label].average
                                        : ' '}
                                    </span>
                                  </div>
                                )}

                              {
                                <div className="py-2 flex items-center text-xs lg:text-sm uppercase text-right justify-between">
                                  {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].runs !== '' ? (
                                    <span className="dark:text-gray-2 text-black text-xs uppercase">
                                      Best :
                                    </span>
                                  ) : (
                                    ''
                                  )}
                                  <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].runs !== ''
                                      ? teamDiscovery.teamStats[
                                          ActiveTab?.value
                                        ][card.label].runs
                                      : ' '}
                                  </span>
                                </div>
                              }
                            </div>
                          </>
                        )}
                        {card.label === 'bestFigures' && (
                          <>
                            <div className="flex flex-col dark:py-4 md:mt-16 md:pl-20 pr-2">
                              <div className="md:py-2 text-sm truncate dark:text-white text-black font-bold">
                                {teamDiscovery &&
                                  teamDiscovery.teamStats &&
                                  teamDiscovery.teamStats[ActiveTab?.value] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ] &&
                                  teamDiscovery.teamStats[ActiveTab?.value][
                                    card.label
                                  ].playerName}
                              </div>

                              {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab?.value] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ].matchesPlayed !== '' && (
                                  <div className="py-2 flex items-center text-right justify-between">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].matchesPlayed !== '' ? (
                                      <span className="dark:text-gray-2 text-black text-xs uppercase">
                                        Mat :
                                      </span>
                                    ) : (
                                      ''
                                    )}
                                    <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                      {teamDiscovery &&
                                      teamDiscovery.teamStats &&
                                      teamDiscovery.teamStats[
                                        ActiveTab?.value
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].matchesPlayed !== ''
                                        ? teamDiscovery.teamStats[
                                            ActiveTab?.value
                                          ][card.label].matchesPlayed
                                        : ' '}
                                    </span>
                                  </div>
                                )}

                              {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab?.value] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ].average !== '' && (
                                  <div className="py-2 flex items-center text-right justify-between">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].average !== '' ? (
                                      <span className="dark:text-gray-2 text-black text-xs uppercase">
                                        Avg :
                                      </span>
                                    ) : (
                                      ''
                                    )}
                                    <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                      {teamDiscovery &&
                                      teamDiscovery.teamStats &&
                                      teamDiscovery.teamStats[
                                        ActiveTab?.value
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].average !== ''
                                        ? teamDiscovery.teamStats[
                                            ActiveTab?.value
                                          ][card.label].average
                                        : ' '}
                                    </span>
                                  </div>
                                )}

                              {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab?.value] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ] &&
                                teamDiscovery.teamStats[ActiveTab?.value][
                                  card.label
                                ].runs !== '' && (
                                  <div className="py-2 flex items-center text-right justify-between">
                                    {teamDiscovery &&
                                    teamDiscovery.teamStats &&
                                    teamDiscovery.teamStats[ActiveTab?.value] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ] &&
                                    teamDiscovery.teamStats[ActiveTab?.value][
                                      card.label
                                    ].runs !== '' ? (
                                      <span className="dark:text-gray-2 text-black text-xs uppercase">
                                        Best:
                                      </span>
                                    ) : (
                                      ''
                                    )}
                                    <span className="pl-16 dark:text-green-6 text-basered text-xs font-medium text-right">
                                      {teamDiscovery &&
                                      teamDiscovery.teamStats &&
                                      teamDiscovery.teamStats[
                                        ActiveTab?.value
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ] &&
                                      teamDiscovery.teamStats[ActiveTab?.value][
                                        card.label
                                      ].runs !== ''
                                        ? teamDiscovery.teamStats[
                                            ActiveTab?.value
                                          ][card.label].runs
                                        : ' '}
                                    </span>
                                  </div>
                                )}
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          <DataNotFound />
        )}
      </>
    )
  }
}
