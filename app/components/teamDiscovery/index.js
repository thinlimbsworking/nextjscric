import React from 'react'
import TeamCard from './TeamCard'
function TeamDiscovery({ teams, status }) {
  const dis = {
    mens: "MEN'S",
    womens: "WOMEN'S",
  }
  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }
  return Object.keys(teams).map((key, i) => {
    return (
      <div key={i} className="my-2 self-center dark:text-white text-black">
        <strong className="ml-2 pt-2 text-sm md:block hidden">
          {dis[key] ? dis[key].toUpperCase() : key.toUpperCase()}
        </strong>
        <strong className="ml-2 pt-2 text-sm md:hidden">
          {dis[key]
            ? capitalizeFirstLetter(dis[key])
            : capitalizeFirstLetter(key)}
        </strong>
        <div className="flex h-1 w-10 bg-blue-8  ml-2 mb-4"></div>
        <div className="flex flex-col justify-between items-center">
          <div className="flex w-full  flex-row flex-wrap p-1">
            {teams[key].map((team, i) => (
              <TeamCard key={team.teamID} team={team} tab={status} />
            ))}
          </div>
        </div>
      </div>
    )
  })
}

export default TeamDiscovery
