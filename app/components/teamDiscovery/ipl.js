import React from 'react'
// import trophy from '../../public/svgs/trophy2.svg'
function ipl({ years }) {
  let yearComp = years.map((year, i) => (
    <div key={i} className=" flex text-center my-1">
      <span className="items-center text-xs dark:text-green text-basered">
        {year}
      </span>
    </div>
  ))
  return (
    <>
      <div className="flex flex-wrap h-12 justify-center items-center relative f5 font-semibold">
        <div className="text-xs">{yearComp ? yearComp : '-'}</div>
      </div>
    </>
  )
}

export default ipl
