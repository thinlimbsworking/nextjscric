import React from 'react'

function ranking({ team }) {
  return (
    <>
      <div className="w-fullflex mr-2 dark:text-white text-black">
        <p className="text-center w-full text-xs dark:mt-4 font-normal mb-2 ">
          ICC Ranking
        </p>
      </div>
      <div className="flex justify-around w-full center mr-2 border-b-gray">
        <div className="flex p-1 flex-col">
          <span className="block text-xs opacity-50 font-thin">TEST</span>

          <span className="block self-center oswald font-sm dark:text-green-6 text-basered">
            {team.testRanking}
          </span>
        </div>
        <div className="inline-block p-1 mb-1 relative border-l dark:border-l-gray-2"></div>

        <div className="flex p-1 flex-col">
          <span className="block text-xs font-thin opacity-50">ODI</span>
          <span className="block  self-center oswald text-sm dark:text-green-6 text-basered">
            {team.odiRanking}
          </span>
        </div>
        <div className="inline-block p-1 mb-1 relative border-l dark:border-l-gray-2 "></div>

        <div className="flex p-1 flex-col">
          <span className="block text-xs opacity-50 font-thin">T20</span>
          <span className="block self-center oswald text-sm dark:text-green-6 text-basered">
            {team.t20Ranking}
          </span>
        </div>
      </div>
    </>
  )
}

export default ranking
