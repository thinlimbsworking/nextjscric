'use client'
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import { TeamViewV2 } from '../api/services'
import Link from 'next/link'

// import teamImage from './../public/pngs/team-web.jpg';
// import ImageComponent from './Image';
// const images = {
//    upArrowBlack: '/svgs/upArrowBlack.svg',
//    backIconWhite: '/svgs/backIconWhite.svg',
//    downArrowBlack: '/svgs/downArrowBlack.svg',
//    EMPTY: '/svgs/Empty.svg',
//    BATSMAN: '/svgs/bat-red.svg',
//    BOWLER: '/svgs/bowler-red.svg',
//    KEEPER: '/svgs/wk-red.png',
//    ALL_ROUNDER: '/svgs/AllRounder-red.svg',
//    CLOSE: '/svgs/close.png',
//    LOCATION: '/svgs/location-icon.svg',
//    USER: '/svgs/user.svg',
//    TEAM_WEB: '/pngs/team-web.jpg'
// };
export default function TeamDetailLayout({
  teamName,
  teamID,
  teamTab,
  params,
  ...props
}) {
  let router = useRouter()
  let tab = params?.slug?.[2]

  const [selectedTab, setSelectedTab] = useState(tab)

  useEffect(() => {
    setSelectedTab(tab)
  }, [])
  let TeamTabs = ['form', 'schedule', 'stats', 'players', 'news']
  return (
    <div className="dark:text-white">
      <div className="fixed top-0 left-0 right-0 z-50 flex items-center p-2 bg-gray-8 white text-base font-semibold z-2 lg:hidden xl:hidden">
        {/* <img className='mr-2' onClick={() => window.history.back()} src={`/svgs/backIconWhite.svg`} alt='back icon' /> */}
        <div
          onClick={() => window.history.back()}
          className="outline-0 md:cursor-pointer mr-2 lg:hidden bg-gray rounded"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
        <span className="capitalize"> {teamName} </span>
      </div>

      <div className="overflow-hidden">
        <div className="">
          <div className="w-full md:bg-white md:p-4 md:rounded-md relative ">
            <img
              className="w-full dark:mt-12 md:rounded-md object-cover md:h-[24rem] object-top"
              onError={(evt) => (evt.target.src = '/pngs/team-web.jpg')}
              src={`https://images.cricket.com/teams/${teamID}_actionshot_safari.jpg?auto=compress&fit=crop&h=300&w=500&dpr=1`}
              loading="lazy"
            />
          </div>
        </div>
        <div className="shadow">
          <div className="flex overflow-x-scroll hidescroll border-black border-b-blue-9 lg:hidden md:hidden">
            {TeamTabs.map((tabName, i) => {
              return (
                <>
                  <Link
                    key={i}
                    {...TeamViewV2(teamName, teamID, tabName, teamTab)}
                    replace
                    passHref
                    title={tabName}
                    onClick={() => {
                      setSelectedTab(tabName)
                    }}
                    className={` ${
                      tabName === 'stats' ? 'w-1/5 md:w-1/4' : 'w-1/5 md:w-1/4'
                    } md:cursor-pointer whitespace-nowrap text-center py-2`}
                    style={{
                      letterSpacing: 0.6,
                      borderBottom:
                        tabName?.toLowerCase() === selectedTab?.toLowerCase()
                          ? '2px solid rgba(106, 145, 169, 1)'
                          : null,
                    }}
                  >
                    <div
                      className={`dark:text-xs lg:text-base md:text-base lg:font-medium md:font-medium py-1 font-medium flex-nowrap uppercase text-lg ${
                        tabName?.toLowerCase() === selectedTab?.toLowerCase()
                          ? 'text-blue-9'
                          : 'text-gray-2'
                      }`}
                    >
                      {tabName === 'career-stats' ? 'Career Stats' : tabName}
                    </div>
                  </Link>
                </>
              )
            })}
          </div>
          <div className="overflow-x-scroll hidescroll border-black border-b-basered hidden lg:flex md:flex">
            {TeamTabs.map((tabName, i) => {
              return (
                <>
                  <Link
                    key={i}
                    {...TeamViewV2(teamName, teamID, tabName, teamTab)}
                    replace
                    passHref
                    title={tabName}
                    onClick={() => {
                      setSelectedTab(tabName)
                    }}
                    className={` ${
                      tabName === 'stats' ? 'w-1/5 md:w-1/4' : 'w-1/5 md:w-1/4'
                    } md:cursor-pointer whitespace-nowrap text-center py-2`}
                    style={{
                      letterSpacing: 0.6,
                      borderBottom:
                        tabName?.toLowerCase() === selectedTab?.toLowerCase()
                          ? '2px solid #922133'
                          : null,
                    }}
                  >
                    <div
                      className={`text-xs uppercase mt-2 lg:font-medium md:font-medium py-1 font-semibold flex-nowrap ${
                        tabName?.toLowerCase() === selectedTab?.toLowerCase()
                          ? 'text-basered'
                          : 'text-black'
                      }`}
                    >
                      {tabName === 'career-stats' ? 'Career Stats' : tabName}
                    </div>
                  </Link>
                </>
              )
            })}
          </div>
        </div>
        {props.children}
      </div>
    </div>
  )
}
