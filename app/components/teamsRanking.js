'use client'
import { useQuery } from '@apollo/react-hooks'
import Link from 'next/link'
import React from 'react'
import { GET_ALL_RANKINGS } from '../../api/queries'
import DataNotFound from './commom/datanotfound'

const teamFallback = '/svgs/images/flag_empty.png'

const TeamBadge = ({ teamID, teamName }) => {
  return (
    <div className="flex flex-row items-center text-xs text-inherit dark:text-white font-medium text-left py-4 rounded-xl">
      <div className="h-6 w-10">
        <img
          alt="teamFlag"
          style={{ height: '100%', width: '100%' }}
          className="shadow-xl"
          src={`https://images.cricket.com/teams/${teamID}_flag_safari.png`}
          onError={(e) => {
            e.target.src = teamFallback
          }}
        />
      </div>
      {/* <span className="flex items-center">
        <img
          alt="teamFlag"
          className="mr-4 h-8 w-1 shadow-xl"
          src={`https://images.cricket.com/teams/${teamID}_flag_safari.png`}
          // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
        />
        {teamName?.toUpperCase()}
      </span> */}
      <Link
        href="/teams/[...slugs]"
        as={`/teams/${teamID}/${teamName
          ?.split(' ')
          ?.join('-')
          .toLowerCase()}/form`}
        passHref
        legacyBehavior
      >
        <span className="ml-2 md:cursor-pointer hover:underline hover:text-blue">
          {teamName?.toUpperCase()}
        </span>
      </Link>
    </div>
  )
}

const DisplayComponents = {
  TeamBadge,
}

const Table = ({ headers, data, category, className = '' }) => {
  if (!data?.length) {
    return <></>
  }
  return (
    <div class="flex flex-col">
      <div class="">
        <div class="py-2 inline-block ">
          <div class="overflow-hidden">
            {data && (
              <TeamCard
                category={category}
                teamRank={data[0][0]}
                teamName={data[0][1]?.teamName}
                teamRating={data[0][2]}
                teamID={data[0][1]?.teamID}
              />
            )}
            <table class="min-w-full border-2 border-solid border-gray-11">
              <thead class="bg-gray-200 border-b">
                <tr>
                  {headers?.map((column) => {
                    return (
                      <th
                        scope="col"
                        class="text-sm font-medium text-gray-900 px-4 py-4 text-left"
                      >
                        {column}
                      </th>
                    )
                  })}
                </tr>
              </thead>
              <tbody>
                {data?.slice(1).map((rowData) => {
                  return (
                    <tr class="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                      {rowData.map((column, index) => {
                        if (index > headers.length - 1) return
                        if (typeof column === 'object') {
                          return (
                            <td class="px-6 text-sm font-medium text-gray-900">
                              <DisplayComponents.TeamBadge {...column} />
                            </td>
                          )
                        }
                        return (
                          <td class="px-6 text-sm font-medium text-gray-900">
                            {column}
                          </td>
                        )
                      })}
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

const TeamCard = ({
  teamID,
  teamName,
  teamRank,
  teamRating,
  category = '',
}) => {
  return (
    <div className="bg-[#B1252B]">
      <div className="text-white flex py-0.5 px-1 items-center justify-start border-2 border-solid border-basered rounded-tl-md rounded-tr-md">
        {category}
      </div>

      <div className="flex bg-basered py-5 border-basered md:flex-row md:max-w-xl dark:border-gray-700 dark:bg-gray-800 text-white dark:hover:bg-gray-700 pb-2">
        <div class="flex w-full justify-between px-2">
          <div className="flex items-center justify-center mx-4">
            <p class="font-bold text-2xl">{teamRank}</p>
          </div>
          <div class="font-normal flex justify-center mx-6 text-white">
            <DisplayComponents.TeamBadge teamID={teamID} />
            <Link
              href="/teams/[...slugs]"
              as={`/teams/${teamID}/${teamName
                ?.split(' ')
                ?.join('-')
                ?.toLowerCase()}/form`}
              passHref
              legacyBehavior
            >
              <h5 class="text-xl font-normal md:cursor-pointer mt-3">
                {teamName.toUpperCase()}
              </h5>
            </Link>
          </div>
          <div className="flex justify-end">
            <span class="text-2xl font-bold tracking-tight ml-10 mt-2.5">
              {teamRating}
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default function TeamsRanking({ gender = 'men', format = 'Test' }) {
  const { data: rankingData } = useQuery(GET_ALL_RANKINGS)
  const tableHeader = ['Rank', 'Team', 'Rating']

  const tableData = React.useMemo(() => {
    try {
      if (rankingData) {
        return Object.entries(rankingData.getPlayerRankings).map(
          ([_, matchData]) => [matchData[gender].team][0],
        )
      }
    } catch (err) {}

    return []
  }, [rankingData, gender])

  if (tableData) {
    return (
      <div className="flex gap-3 mx-auto">
        {tableData?.map((match, index) => {
          const data = match?.rank?.map((team) => [
            team?.position,
            // <TeamBadge  teamID={player?.teamID} teamName={player?.teamName} />,
            {
              displayType: 'TeamBadge',
              teamID: team?.teamID,
              teamName: team?.teamName,
            },
            // team?.teamName,
            team?.Rating,
          ])
          if (!data?.length) {
            return <></>
          }
          return (
            <div className="flex flex-col w-full">
              <Table
                className=""
                headers={tableHeader}
                category={match?.matchType}
                data={data}
                // data={playerRankings[type?.label]?.rank}
              />
            </div>
          )
        })}
      </div>
    )
  }
  if (!rankingData) {
    return <DataNotFound />
  }

  return <></>
}

const typeOfPlayers = [
  {
    label: 'Team',
    key: 'team',
  },
]
