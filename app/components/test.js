import React, { useRef, useState, useEffect } from 'react'

import { Swiper, SwiperSlide } from 'swiper/react'

import {
  EffectCoverflow,
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Controller,
} from 'swiper'

export default function SwiperModule(props) {
  //
  const [swiper, updateSwiper] = useState(null)
  const bat = '/pngsV2/battericon.png'
  const ball = '/pngsV2/bowlericon.png'
  // console.log(p, 'iot')
  useEffect(() => {
    // props.setUrlTrack(false);

    if (swiper !== null) {
      try {
        swiper.params.navigation.prevEl = prevRef.current
        swiper.params.navigation.nextEl = nextRef.current
        swiper.navigation.init()
        swiper.navigation.update()

        swiper.on('slideChange', () => {
          props.setBowler(0)
          props.setTabIndex(swiper.realIndex)
        })
        swiper.on('click', () => {
          // ;
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        })
      } catch (e) {}
    }
  }, [swiper])
  const prevRef = useRef()
  const nextRef = useRef()

  //
  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)
  return (
    <div className="my-5 relative flex justify-center items-center">
      <div className="lg:block md:block" ref={prevRef}>
        <div
          style={{ zIndex: 1000 }}
          id="swiper-button-prev"
          className="white dn db-ns z-999 outline-0 cursor-pointer"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#38d925"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
      </div>
      <Swiper
        modules={[
          EffectCoverflow,
          Navigation,
          Pagination,
          Scrollbar,
          A11y,
          Controller,
        ]}
        navigation={{
          prevEl: prevRef?.current,
          nextEl: nextRef?.current,
        }}
        updateOnWindowResize
        observer
        observeParents
        onSwiper={(swiper) => {
          // Delay execution for the refs to be defined
          setTimeout(() => {
            // Override prevEl & nextEl now that refs are defined
            swiper.params.navigation.prevEl = navigationPrevRef.current
            swiper.params.navigation.nextEl = navigationNextRef.current

            // Re-init navigation
            // swiper.navigation.destroy();
            // swiper.navigation.init();
            // swiper.navigation.update();
          })
        }}
        onSlideChange={(swiper) => updateSwiper(swiper)}
        shouldSwiperUpdate={true}
        effect={'coverflow'}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={'2'}
        initialSlide={'0'}
        coverflowEffect={{
          slideShadows: true,
          rotate: 0,
          stretch: 0,
          depth: 600,
          modifier: 1,
        }}
        pagination={false}
        className="mySwiper"
      >
        {props.data &&
          props.data[props.toggle].mappings.map((item, index) => {
            return (
              <SwiperSlide key={index}>
                <div className="bg-gray lg:bg-white md:bg-white xl:bg-white h-48 flex flex-col items-center lg:border md:border xl:border lg:border-solid md:border-solid xl:border-solid  lg:border-gray-11 md:border-gray-11 xl:border-gray-11 justify-center rounded-lg">
                  <div className="flex relative items-center justify-center ">
                    <img
                      className="h-32 w-32 bg-gray-4 lg:bg-gray-2 object-top object-cover rounded-full"
                      src={`https://images.cricket.com/players/${item.batsmanId}_headshot_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngsV2/playerph.png')
                      }
                    />

                    {/* <img
                      className=" absolute h-6 w-6 left-0 bottom-0 object-fill"
                      src={item.role == 'Bowler' ? ball : bat}
                    /> */}

                    {item?.role === 'Batsman' ? (
                      <img
                        src="/svgs/whitebat.svg"
                        height="10px"
                        width="16px"
                        className="absolute h-5 w-5 left-2 bottom-0 bg-basebg border rounded object-fill"
                      />
                    ) : item?.role === 'Bowler' ? (
                      <img
                        src="/svgs/bowling.svg"
                        height="10px"
                        width="13px"
                        className="absolute h-5 w-5 left-2 bottom-0 bg-basebg border rounded object-fill"
                      />
                    ) : item?.role === 'Keeper' ? (
                      <img
                        src="/svgs/wicketKeeper.svg"
                        height="10px"
                        width="16px"
                        className="left-2 bottom-0 absolute h-5 w-5 bg-basebg md:h-5 object-fill border rounded md:border"
                      />
                    ) : (
                      <img
                        src="/svgs/allRounderWhite.svg"
                        height="10px"
                        width="16px"
                        className="h-5 w-5 left-2 bottom-0 absolute bg-basebg md:h-5 object-fill border rounded md:border"
                      />
                    )}

                    {/* <img
                      className=' absolute h-5 w-5 border   right-0 bottom-0 object-fill rounded-full'
                      src={`https://images.cricket.com/teams/${props.data[0].homeTeamID}_flag_safari.png`}
                    /> */}

                    <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center">
                      <img
                        className="h-5 w-5 rounded-full"
                        src={`https://images.cricket.com/teams/${
                          props.toggle === 0
                            ? props.homeTeamID
                            : props.awayTeamID
                        }_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/svgs/images/flag_empty.svg')
                        }
                      />
                    </div>
                  </div>
                  <div className="text-center px-2 leading-5 md:text-md md:text-black dark:text-xs font-semibold mt-2">
                    {item.name}
                  </div>
                </div>
              </SwiperSlide>
            )
          })}
      </Swiper>
      <div className=" text-xl  lg:block md:block" ref={nextRef}>
        <div
          style={{ zIndex: 1000 }}
          id="swiper-button-prev"
          className="white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer"
        >
          <svg width="30" viewBox="0 0 24 24">
            <path
              fill="#38d925"
              d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>
      </div>
    </div>
  )
}
