'use client'
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import Maintheme from './mainTheme'
import Subtheme from './subTheme'
import Link from 'next/link'

export default function Themdata1(props) {
  const router = useRouter()
  const [loading, setLoading] = useState(true)
  const [xdata, setxdata] = useState()
  const [state, setState] = useState(false)
  useEffect(() => {
    setLoading(false)
  }, [])

  const handlClick = (e, x) => {
    setxdata({ type: x, val: 10 })

    //  router.push(`theme/data/${x}`,'',{shallow:true})
    //  e.preventDefault();
  }
  // if(loading) return  <>its loading..</>
  return (
    <>
      <div>themdata1</div>
      <Link href={`theme/data/${'old'}`} passHref>
        {' '}
        <div onClick={(e) => handlClick(e, 'old')}>Old data</div>
      </Link>
      <Link href={`theme/data/${'new'}`} passHref>
        <div onClick={(e) => handlClick(e, 'new')}>New data</div>
      </Link>
      {xdata?.type === 'new' && <Maintheme xdata={xdata} setxdata={setxdata} />}
      {xdata?.type === 'old' && <Subtheme xdata={xdata} setxdata={setxdata} />}
    </>
  )
}
