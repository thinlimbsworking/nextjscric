import React from "react";
import { format, formatDistanceToNowStrict } from "date-fns";
import { HOME_UPCOMNG_SERIES } from "../api/queries";
import { useQuery } from "@apollo/react-hooks";
import { useRouter } from "next/navigation";
import Heading from "./shared/heading";
import Link from "next/link";

export default function UpcomingMatches(props) {
  const router = useRouter();
  const navigate = router.push;
  let {
    loading: loading,
    error: error,
    data,
  } = useQuery(HOME_UPCOMNG_SERIES, {});
  if (loading) data = {};
  if (props.error) error = props.error;
  if (data) {
    return (
      <>
       
        <div className=" text-white mb:mt-5 md:mt-1 p-1 mx-2  ">
          <div className="flex items-center justify-between mb-2 ">
            <div className="ml-2">
            <Heading heading={"Upcoming Series"} borderlineHide={true} />
            </div>
            <div className="flex justify-center rounded  bg-gray px-2 py-1.5  items-center  ">
              <Link href="/series/upcoming/all">
                <img className="w-6 h-6" src="/pngsV2/arrow.png" alt="" />
              </Link>
            </div>
          </div>

          <div className="flex overflow-scroll -mb-4 md:mb-0 ">
            {data &&
              data.getseriesforHomepage &&
              data.getseriesforHomepage.map((item, i) => {
                return (
                  <div className="">
                 
                    {item.seriesType == "Bilateral" && (
                      <div
                        key={i}
                        className=" w-44  mx-2  bg-gray   rounded-xl"
                      >
                        
                        {" "}
                        <Link
                          href={`/series/${item?.type}/${
                            item.tourID
                          }/${item.tourName
                            .replace(/[^a-zA-Z0-9]+/g, " ")
                            .split(" ")
                            .join("-")
                            .toLowerCase()}/matches`}
                        >
                          <div className="flex flex-col  h-40 items-center justify-center">
                            <div className="flex items-center justify-center  ">
                              <div className="z-10">
                                <img
                                  className="h-6 w-9 rounded  "
                                  src={`https://images.cricket.com/teams/${item.teamid[0]}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = "/pngsV2/flag_dark.png")
                                  }
                                  alt=""
                                />
                              </div>
                              <div>
                                <img
                                  className="h-6 w-6 mx-1"
                                  src="/pngsV2/bluecross.png"
                                  alt=""
                                />
                              </div>
                              <div className=" z-10">
                                <img
                                  className="h-6 w-9 rounded  "
                                  src={`https://images.cricket.com/teams/${item.teamid[1]}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = "/pngsV2/flag_dark.png")
                                  }
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="flex text-md mt-3 items-center justify-center">
                              {item.match}
                            </div>

                            <div className="font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1">
                              {format(
                                new Date(
                                  Number(item.seriesStartDate) - 19800000
                                ),
                                " MMM do"
                              )}
                              -{" "}
                              {format(
                                new Date(Number(item.seriesEndDate) - 19800000),
                                " MMM do"
                              )}
                            </div>
                          </div>
                        </Link>
                      </div>
                    )}

                    {item.seriesType == "Series" && (
                      <div
                        key={i}
                        className=" w-44  mx-2  bg-gray   rounded-xl"
                      >
                        <Link
                          className="w-full"
                          href={`/series/${item?.type}/${
                            item.tourID
                          }/${item.tourName
                            .replace(/[^a-zA-Z0-9]+/g, " ")
                            .split(" ")
                            .join("-")
                            .toLowerCase()}/matches`}
                        >
                          <div className="flex flex-col  h-40 items-center justify-center">
                            <div className="flex items-center justify-center  ">
                              <div className="z-10">
                                <img
                                  className=" object-cover  object-top rounded-md  "
                                  src={`${item.teamid[0]}?w=60&h=40&auto=compress,format&fit=crop&dpr=2`}
                                  onError={(evt) =>
                                    (evt.target.src = "/pngsV2/flag_dark.png")
                                  }
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1">
                              {format(
                                new Date(
                                  Number(item.seriesStartDate) - 19800000
                                ),
                                " MMM do"
                              )}
                              -{" "}
                              {format(
                                new Date(Number(item.seriesEndDate) - 19800000),
                                " MMM do"
                              )}
                            </div>
                          </div>
                        </Link>
                      </div>
                    )}

                    {item.seriesType.toLowerCase() == "tri-series" && (
                      <div
                        key={i}
                        className=" w-44  mx-2  bg-gray   rounded-xl"
                      >
                        {" "}
                        <Link
                          href={`/series/${item?.type}/${
                            item.tourID
                          }/${item.tourName
                            .replace(/[^a-zA-Z0-9]+/g, " ")
                            .split(" ")
                            .join("-")
                            .toLowerCase()}/matches`}
                        >
                          <div className="flex flex-col  h-40 items-center justify-center">
                            <div className="flex items-center justify-center  ">
                              <div className="z-10">
                                <img
                                  className="h-6 w-9 rounded  "
                                  src={`https://images.cricket.com/teams/${item.teamid[0]}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = "/pngsV2/flag_dark.png")
                                  }
                                  alt=""
                                />
                              </div>
                              <div>
                                <img
                                  className="h-6 w-6 mx-1"
                                  src="/pngsV2/bluecross.png"
                                  alt=""
                                />
                              </div>
                              <div className=" z-10">
                                <img
                                  className="h-6 w-9 rounded  "
                                  src={`https://images.cricket.com/teams/${item.teamid[1]}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = "/pngsV2/flag_dark.png")
                                  }
                                  alt=""
                                />
                              </div>
                            </div>
                            <div className="flex mt-2">
                              <img
                                className="h-6 w-9 rounded  "
                                src={`https://images.cricket.com/teams/${item.teamid[2]}_flag_safari.png`}
                                onError={(evt) =>
                                  (evt.target.src = "/pngsV2/flag_dark.png")
                                }
                                alt=""
                              />
                            </div>

                            <div className="flex text-sm mt-3 items-center justify-center">
                              {item.match}
                            </div>

                            <div className="font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1">
                              {format(
                                new Date(
                                  Number(item.seriesStartDate) - 19800000
                                ),
                                " MMM do"
                              )}
                              -{" "}
                              {format(
                                new Date(Number(item.seriesEndDate) - 19800000),
                                " MMM do"
                              )}
                            </div>
                          </div>
                        </Link>
                      </div>
                    )}
                    {item.seriesType == "Tournament" && (
                      <div
                        key={i}
                        className=" w-44  mx-2  bg-gray   rounded-xl"
                      >
                        <Link
                          className="w-full"
                          href={`/series/${item?.type}/${
                            item.tourID
                          }/${item.tourName
                            .replace(/[^a-zA-Z0-9]+/g, " ")
                            .split(" ")
                            .join("-")
                            .toLowerCase()}/matches`}
                        >
                          <div className="flex flex-col  h-40 items-center justify-center">
                            <div className="flex items-center justify-center  ">
                              <div className="z-10">
                                <img
                                  className=" object-cover  object-top rounded-md  "
                                  src={`${item.teamid[0]}?w=60&h=40&auto=compress,format&fit=crop&dpr=2`}
                                  onError={(evt) =>
                                    (evt.target.src = "/pngsV2/flag_dark.png")
                                  }
                                  alt=""
                                />
                              </div>
                            </div>

                            <div className="font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1">
                              {format(
                                new Date(
                                  Number(item.seriesStartDate) - 19800000
                                ),
                                " MMM do"
                              )}
                              -{" "}
                              {format(
                                new Date(Number(item.seriesEndDate) - 19800000),
                                " MMM do"
                              )}
                            </div>
                          </div>
                        </Link>
                      </div>
                    )}
                  </div>
                );
              })}
          </div>
        </div>
      </>
    );
  }
}
