import React, { useState } from 'react';
import { format, formatDistanceStrict } from 'date-fns';
import Link from 'next/link';
import { getVideoUrl } from '../../api/services';
import MetaDescriptor from '../MetaDescriptor';
import { VIDEOS } from '../../constant/MetaDescriptions';
import Heading from '../commom/heading';
import ImageWithFallback from '../commom/Image';

const VideosDetailPage = ({ data, type, ...props }) => {
  const [drop, setDrop] = useState(false);
  if (typeof window !== 'undefined') {
    window.scrollTo(0, 0);
  }
  return <>
    <MetaDescriptor
      section={VIDEOS({
        title: data?.getVideoByvideoID.title,
        type: type
      })}
    />

    {data && data.getVideoByvideoID && (
      <div className='text-white'>
        <div className=' flex justify-between items-center p-3 bg-gray-8 md:hidden lg:hidden'>
          <div className='flex items-center gap-2 md:hidden lg:hidden'>
            <div className='p-2 bg-gray rounded-md flex justify-center items-center cursor-pointer'>
              <img
                className="flex items-center justify-center h-4 w-4 rotate-180"
                onClick={() => window.history.back()}
                src='/svgsV2/RightSchevronWhite.svg'
                alt='backIconWhite'></img>
            </div>
            <div className='text-white'>Videos</div>
          </div>
        </div>
        {/* <div className='h44-l' /> */}

        <div className=' w-full'>
          <iframe
            className='w-full h-64 md:h-96 lg:h-96 '
            src={`https://www.youtube.com/embed/${data.getVideoByvideoID.videoYTId}?autoplay=1`}
            title='cricket'
            modestbranding='1'
            rel='0'
            frameBorder='0'
            fs=''
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen='1'></iframe>
          <div className='bg-gray-10 dark:bg-gray border-x border-b dark:border-none mb-4'>
            <div className='flex justify-column justify-between items-center py-2 text-black dark:text-white'>
              <div>
                <h1 className=' text-sm font-semibold px-2 py-1'> {data.getVideoByvideoID.title} </h1>

                {data.getVideoByvideoID.createdAt && (
                  <div className=' px-2 text-sm font-medium text-gray-2'>
                    {data.getVideoByvideoID.createdAt ? new Date().getTime() - Number(data.getVideoByvideoID.createdAt) > 86400000
                      ? format(
                        new Date(Number(data.getVideoByvideoID.createdAt)),
                        'dd MMM, yyyy',
                      )
                      : formatDistanceStrict(
                        new Date(Number(data.getVideoByvideoID.createdAt)),
                        new Date(),
                      ) + ' ago'
                      : ''}
                  </div>
                )}
              </div>
              <div className=' flex justify-center items-center pointer px-2'>
                <img
                  className=' pointer w-5'
                  src={drop ? '/svgs/up-arrow.png' : '/svgs/down-arrow.png'}
                  onClick={() => setDrop(!drop)}
                  alt=''
                />
              </div>
            </div>
            {drop && <h2 className='p-2 text-xs text-gray-2 dark:bg-gray'>{data.getVideoByvideoID.description}</h2>}
          </div>
          {data.getVideoByvideoID.relatedVideos && (
            <div className='flex flex-col gap-3 dark:bg-gray-8 dark:text-white px-1'>
              <div className='px-2 '>
                <Heading heading='Watch Related Videos' />
              </div>
              <div className='w-full  flex flex-wrap '>
                {data.getVideoByvideoID.relatedVideos.map((video, i) => (<div className='w-full  md:w-1/3 lg:w-1/3 p-2 md:pr-3 h-80  lg:pr-3 md:py-3 lg:py-3'>
                  <Link
                    key={i}
                    {...getVideoUrl(video, type)}
                    passHref
                  >
                    <div className='w-full border dark:border-none rounded-lg  h-full bg-white dark:bg-gray-4 '>
                      <div className=' h-3/4 relative rounded-t-lg'>
                        <ImageWithFallback
                          height={144}
                          width={144}
                          loading='lazy'
                          src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                          alt=''
                          className='lazyload w-full h-full object-cover object-top  rounded-t-lg'
                        />
                        <img className='absolute w-16 h-16  top-1/2  left-1/2  transform -translate-x-1/2 -translate-y-1/2  ' src='/pngsV2/videos_icon.png' alt='' />
                      </div>
                      <div className='p-2  text-black dark:text-white  rounded-b-lg flex flex-col justify-between'>
                        <div className='text-sm  font-semibold truncate'>{video.title}</div>

                        <div className='text-xs  pt-1 truncate '>{video.description}</div>
                        <div className='flex justify-between'>
                          {!!(video.author || video.authors?.length) && (
                            <div className=' text-xs text-gray-2 '>
                              By <span>{video.author || (video.authors && video.authors[0])}</span>
                            </div>
                          )}
                          {video.createdAt && (
                            <div className='text-xs text-gray-2 pt-2 flex justify-end '>
                              {video.createdAt
                                ? new Date().getTime() - Number(video.createdAt) > 86400000
                                  ? format(new Date(Number(video.createdAt)), 'dd MMM, yyyy')
                                  : formatDistanceStrict(new Date(Number(video.createdAt)), new Date()) + ' ago'
                                : ''}
                            </div>
                          )}
                        </div>
                        </div>
                      </div>
                  </Link>
                </div>
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    )}
  </>;
};

export default VideosDetailPage;
