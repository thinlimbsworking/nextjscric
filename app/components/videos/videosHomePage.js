import React, { useEffect, useState } from 'react';
import { format, formatDistanceStrict } from 'date-fns';
import { useRouter, useSearchParams } from 'next/navigation';
import Link from 'next/link';
import { getVideoUrl } from '../../api/services';
import Tab from '../shared/Tab';
import CommonDropdown from '../commom/commonDropdown';
import ImageWithFallback from '../commom/Image';

const VideosHomePage = ({
  error,
  data,
  seriesFilter,
  selectedDD,
  getPrimaryVideo,
  drop,
  setDrop,
  tabs,
  tab,
  handleVideoNavigation,
  ...props
}) => {
  const router = useRouter();
  const query = useSearchParams();

  return (
    <div className=' dark:text-white mx-auto '>
      <div className='flex items-center p-3 md:hidden lg:hidden'>
        <div className='w-8 h-8 rounded-md flex justify-center items-center cursor-pointer bg-gray-4 '>
          <img
            className="flex items-center justify-center h-4 w-4 rotate-180"
            onClick={() => window.history.back()}
            src='/svgsV2/RightSchevronWhite.svg'
            alt='backIconWhite' />
        </div>
        <div className='text-md font-semibold pl-2 white'>Videos</div>
      </div>


      <div className='w-full flex'>
        <div className='w-full  '>
          {seriesFilter?.length > 0 && <div className='px-3 md:px-0 lg:px-0'> <CommonDropdown options={seriesFilter} defaultSelected={selectedDD} variant='normal'
            onChange={item => { router.push(`/videos/${tab.value}?seriesID=${item.valueId}`) }}
          /></div>}

          {getPrimaryVideo && (
            <iframe
              className='w-full h-80 mt-3  hidden md:block lg:block'
              src={`https://www.youtube.com/embed/${getPrimaryVideo.videoYTId}`}
              title='cricket'
              modestbranding='1'
              rel='0'
              frameBorder='0'
              fs=''
              allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
              allowFullScreen='1'></iframe>
          )}

          {getPrimaryVideo && (
            <div className='w-full bg-gray-10 dark:bg-gray-4 border dark:border hidden md:block lg:block'>
              <div className=' justify-between items-center  hidden md:flex lg:flex p-3  '>
                <div>
                  <h1 className='text-xs font-semibold '> {getPrimaryVideo.title} </h1>
                  {getPrimaryVideo.createdAt && (
                    <div className='ph-2 text-xs font-medium text-gray-2'>
                      {getPrimaryVideo.createdAt
                        ? new Date().getTime() - Number(getPrimaryVideo.createdAt) > 86400000
                          ? format(new Date(Number(getPrimaryVideo.createdAt)), 'dd MMM, yyyy')
                          : formatDistanceStrict(new Date(Number(getPrimaryVideo.createdAt)), new Date()) + ' ago'
                        : ''}
                    </div>
                  )}
                </div>
                <div className='w-8 h-6 flex justify-center items-center cursor-pointer'>
                  <img
                    className='w-3 h-2 cursor-pointer '
                    src={drop ? '/svgs/up-arrow.png' : '/svgs/down-arrow.png'}
                    onClick={() => setDrop(!drop)}
                    alt=''
                  />
                </div>
              </div>
              {drop && <h3 className='p-3 font-medium tracking-wide text-xs dark:bg-gray-4 '>{getPrimaryVideo.description}</h3>}
            </div>
          )}


          <div className='w-full flex flex-col md:py-2 lg:py-2 md:mt-2 lg:mt-2 bg-white dark:bg-transparent rounded shadow-sm md:p-3 lg:p-3 '>
            {/* <div className='flex z-10  justify-between bg-gray-8 ' style={{ position: 'sticky', top: 0 }}>
            {tabs.map((tabName, i) => (
              (<Link
                href='/videos/[...slugs]'
                as={`/videos/${tabName.toLowerCase()}`}
                key={i}
                replace
                passHref
                className={`w-1/4 cursor-pointer  text-xs text-gray-2 px-3 py-2 capitalize  text-center ${tabName.toLowerCase() === tab.toLowerCase() ? ' border-b-2 border-blue-9 text-blue-8' : ''
                  }`}>

                {tabName.toLowerCase()}

              </Link>)
            ))}
          </div> */}

            {selectedDD && selectedDD.valueId === 'all' && <div className='w-full md:p-2 lg:p-2'>
              <div className='w-full md:hidden lg:hidden'>
                <Tab data={tabs} selectedTab={tab} type='line' handleTabChange={(item) => router.push(`/videos/${item.value}${selectedDD ? `?seriesID=${selectedDD.valueId}` : ''}`)} />
              </div>
              <div className='w-full hidden md:block lg:block'>
                <Tab data={tabs} selectedTab={tab} type='block' handleTabChange={(item) => router.push(`/videos/${item.value}${selectedDD ? `?seriesID=${selectedDD.valueId}` : ''}`)} />
              </div>
            </div>}


            {/* video list */}
            <div className='w-full flex flex-wrap px-1 md:px-0 lg:px-0 '>
              {data &&
                data.getVideos &&
                data.getVideos[tab.value].map((video, i) => (<div className='w-full md:w-1/2 lg:w-1/2 p-2'>
                  <Link key={i} href={getVideoUrl(video, tab.value).as} passHref legacyBehavior>

                    <div className='w-full cursor-pointer h-80 border dark:border-none rounded-lg  bg-white dark:bg-gray-4   '>
                      <a className='h-full  w-full ' onClick={() => handleVideoNavigation(tab, video.videoID)}>
                        <div className='z-0 h-3/4 contain relative  rounded-t-lg'>
                          <ImageWithFallback
                            height={144}
                            width={144}
                            loading='lazy'
                            src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                            alt=''
                            className=' w-full h-full mx-auto rounded-t-lg '
                          />
                          <img className='absolute w-16 h-16  top-1/2  left-1/2  transform -translate-x-1/2 -translate-y-1/2  ' src='/pngsV2/videos_icon.png' alt='' />
                        </div>
                        <div className=' p-2  flex flex-col justify-between dark:text-white  rounded-b-lg'>
                          <div className='mx-2 truncate text-sm font-semibold'>{video.title}</div>

                          <div className='mx-2 text-xs  pt-2 truncate'>{video.description}</div>
                          
                            {video.createdAt && (
                              <div className=' text-xs text-gray-2 pt-2 flex justify-end'>
                                {video.createdAt
                                  ? new Date().getTime() - Number(video.createdAt) > 86400000
                                    ? format(new Date(Number(video.createdAt)), 'dd MMM, yyyy')
                                    : formatDistanceStrict(new Date(Number(video.createdAt)), new Date()) + ' ago'
                                  : ''}
                              </div>
                            )}
                         
                        </div>
                      </a>
                    </div>
                  </Link>
                </div>
                ))}
              {data &&
                data.getVideosByCategories &&
                data.getVideosByCategories.map((video, i) => (<div className='w-full md:w-1/2 lg:w-1/2 p-2 md:pr-3  lg:pr-3 md:py-3 lg:py-3'>
                  <Link key={i} href={getVideoUrl(video, tab.value).as} passHref legacyBehavior>

                    <div className='w-full h-80 border dark:border-none rounded-lg bg-white  dark:bg-gray-4   '>
                      <a className='h-full  w-full ' onClick={() => handleVideoNavigation(tab, video.videoID)}>
                        <div className='z-0 h-3/4 contain  relative  rounded-t-lg'>
                          <ImageWithFallback
                            height={144}
                            width={144}
                            loading='lazy'
                            src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                            alt=''
                            className=' w-full h-full mx-auto rounded-t-lg object-cover object-top'
                          />
                          <img className='absolute w-16 h-16  top-1/2  left-1/2  transform -translate-x-1/2 -translate-y-1/2  ' src='/pngsV2/videos_icon.png' alt='' />
                        </div>
                        <div className=' p-2 h-1/4 flex flex-col justify-between dark:text-white  rounded-b-lg'>
                          <div className='mx-2  text-sm font-semibold'>{video.title}</div>

                          <div className='mx-3 text-xs  pt-2 truncate'>{video.description}</div>
                          <div className='flex justify-between'>
                            {!!(video.author || video.authors?.length) && (
                              <div className=' text-xs text-gray-2 '>
                                By <span>{video.author || (video.authors && video.authors[0])}</span>
                              </div>
                            )}
                          {video.createdAt && (
                            <div className=' text-xs text-gray-2 pt-2 flex justify-end'>
                              {video.createdAt
                                ? new Date().getTime() - Number(video.createdAt) > 86400000
                                  ? format(new Date(Number(video.createdAt)), 'dd MMM, yyyy')
                                  : formatDistanceStrict(new Date(Number(video.createdAt)), new Date()) + ' ago'
                                : ''}
                            </div>
                          )}
                          </div>
                        </div>
                      </a>
                    </div>
                  </Link>
                </div>
                ))}
            </div>
          </div>



        </div>

      </div>
    </div>
  );
};

export default VideosHomePage;

// This gets called on every request
// export async function getServerSideProps(context) {
// 	try {
// 		const getVideoPromise = axios.post(process.env.api, { query: GET_VIDEOS_AXIOS });
// 		const primaryVideoPromise = axios.post(process.env.api, { query: GET_PRIMARY_VIDEOS_AXIOS });

// 		let [{ data }, { data: primaryVideo }] = await Promise.all([
// 			getVideoPromise,
// 			primaryVideoPromise,
// 		]);
// 		return { props: { data, primaryVideo } };
// 	} catch (e) {
// 		return { props: { error: e } };
// 	}
// }
