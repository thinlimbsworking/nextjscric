import React, { useEffect, useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { LIVE_WAGON_DATA } from '../api/queries'
import * as d3 from 'd3'
import PieClass from './pieChart'
const minus = '/pngsV2/minus.png'
const plus = '/pngsV2/plus.png'

const ground = '/pngsV2/wagon.png'

export default function WagonWheel(props) {
  const [exp, setExp] = useState(false)

  var z1SquarLeg = 0
  var z2FineLeg = 0
  var z3ThirdMan = 0
  var z4Point = 0
  var z5Cover = 0
  var z6MidOff = 0
  var z7MidOn = 0
  var z8MidWicket = 0

  const { loading, error, data } = useQuery(LIVE_WAGON_DATA, {
    variables: { matchID: props.matchID },
  })

  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20,
    }))

  const [dataa, setData] = useState(generateData(0))
  const changeData = () => {
    setData(generateData())
  }

  useEffect(() => {
    setData(generateData())
    setExp(window.screen.width > 540 ? true : false)
  }, [!dataa])

  const getZadCo = (ball) => {
    if (props.battingStyle && props.battingStyle === 'LHB') {
      return 0 - (180 - parseInt(ball.zadval.split(',')[1]))
    }
    return 0 - ball.zadval.split(',')[1]
  }

  return data &&
    data.getWagonwheelLive &&
    data.getWagonwheelLive.matchBatsman ? (
    <div className="bg-gray mx-2 mt-5 rounded px-3 py-3 text-white">
      <div className="flex justify-between items-center">
        <h3 className="font-semibold text-lg tracking-wider">
          Player Wagon Wheel
        </h3>
        <div className="p-1 bg-gray-8 rounded-lg" onClick={() => setExp(!exp)}>
          <img className="w-6" src={exp ? minus : plus} />
        </div>
      </div>
      {exp && (
        <div>
          <div className="flex justify-between lg:justify-around md:justify-around items-center my-4">
            {data.getWagonwheelLive.matchBatsman.map((x, i) => (
              <div key={i}>
                <div className="text-center mb-3">
                  <div className="font-semibold text-sm tracking-wide">
                    {x.playerName}
                    {x.playerBattingNumber == '0' ? '*' : ''}
                  </div>
                  <div className="text-lg font-medium tracking-wider pt-1">
                    {x.playerMatchRuns}({x.playerMatchBalls})
                  </div>
                </div>
                <div className="flex items-center justify-center relative ">
                  <div
                    className={`absolute w-28 h-28 lg:w-56 md:w-56  z-2  z-2   flex items-center justify-center `}
                  >
                    <img
                      className="w-28 h-28  lg:w-56 md:w-56 lg:h-56 md:h-56 absolute "
                      src={ground}
                      alt=""
                    />

                    <div
                      className="flex h-1 z-2 border-white w-1 items-center justify-center absolute bg-red   rounded-full"
                      style={{
                        bottom: '58%',
                        left: '49%',
                        right: '0%',
                        transform: `rotate(0deg)`,
                        transformOrigin: '0 0',
                      }}
                    ></div>
                    <div className="w-24 h-24  lg:w-48 md:w-48 lg:h-48 md:h-48  absolute overflow-hidden  ">
                      {x.zad.map((item, index) => {
                        return (
                          <div
                            key={index}
                            className={`border  absolute ${
                              (item.runs === '6' && 'border-red w-56') ||
                              (item.runs === '5' && 'border-green w-56') ||
                              (item.runs === '4' && 'border-green w-56') ||
                              (item.runs === '3' && 'border-pink w-56') ||
                              (item.runs === '2' && 'border-blue w-56') ||
                              'border-white w-56'
                            } `}
                            style={{
                              bottom: '58%',
                              left: '53%',
                              right: '0%',
                              transform: `rotate(${getZadCo(item)}deg)`,
                              transformOrigin: '0 0',
                            }}
                          ></div>
                        )
                      })}
                    </div>
                  </div>
                  <div className="relative">
                    {/* height 5.5 rem */}
                    {/* window.screen.width > 975 ? window.screen.width/1.5 : window.screen.width+window.screen.width */}
                    <PieClass
                      data={dataa}
                      zoneRun={[
                        z2FineLeg,
                        z1SquarLeg,
                        z8MidWicket,
                        z7MidOn,
                        z6MidOff,
                        z5Cover,
                        z4Point,
                        z3ThirdMan,
                      ]}
                      width={window.screen.width > 975 ? 300 : 150}
                      height={window.screen.width > 975 ? 300 : 150}
                      battingStyle={x.battingStyle}
                      zadData={x.zad}
                      innerRadius={window.screen.width > 975 ? 57 * 2 : 57}
                      outerRadius={window.screen.width > 975 ? 75 * 2 : 75}
                    />
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="flex justify-between lg:justify-around md:justify-around items-center w-1/2 lg:w-full md:w-full px-2">
            <div className="flex items-center">
              <div className="h-2 w-2 bg-yellow rounded-full"></div>
              <div className="text-gray-2 text-xs font-medium pl-1">4 Runs</div>
            </div>
            <div className="flex item-center items-center">
              <div className="h-2 w-2 bg-blue rounded-full"></div>
              <div className="text-gray-2 text-xs font-medium pl-1">6 Runs</div>
            </div>
          </div>
        </div>
      )}
    </div>
  ) : (
    <></>
  )
}
