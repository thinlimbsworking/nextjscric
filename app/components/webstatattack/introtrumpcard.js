import React from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import CleverTap from 'clevertap-react';

const Player2Avatar = '/pngs/player2_avatar.png';
const Player1Avatar = '/pngs/player1_avatar.png';
const backIconWhite = '/svgs/backIconWhite.svg';
const timer = '/svgs/timer.svg';
const intromedal = '/svgs/introMedal.svg';
const share = '/svgs/share-line.png';

export default function IntroTrumpCard(props) {
  let router = useRouter();
  let navigate = router.push;

  const handleTrumpNavigation = (venueID) => {
    CleverTap.initialize('StatAttackStart', {
      Platform: localStorage.Platform
    });
    navigate('/stat-attack/[type]', '/stat-attack/play');
  };

  const handleShare = (e) => {
    if (navigator.share !== undefined && location.protocol == 'https:') {
      if (navigator.running == true) {
        return;
      }
      navigator.running = true;
      navigator
        .share({
          title: 'stat-attack',
          url: location.origin + '/stat-attack/start-game'
        })
        .then(() => {
          navigator.running = false;
        })
        .catch((error) => {
          navigator.running = false;
        });
    }
  };

  return (
    <div>
      <Head>
        <title>Stat Attack - Play The Cricket Card Game, Win High Score for Higher Stats</title>
        {/* <h1>STAT ATTACK</h1> */}
        <meta
          property='og:description'
          content='I challenge you to beat my score. Lets see how well you know T20 cricket statistics?'
        />
        <meta
          name='description'
          content='Cricket.com brings you Stat Attack, a game of cards. Pick one stat you think your player will rank higher than your opponent player. Win high score by picking higher stats.'
        />
        <meta itemprop='width' content='1280' />
        <meta itemprop='height' content='720' />
        <meta itemprop='url' content={'https://s3.ap-south-1.amazonaws.com/cdn.cricket.com/stacattacksquare.png'} />
        <meta
          property='og:image'
          itemprop='image'
          content={'https://s3.ap-south-1.amazonaws.com/cdn.cricket.com/stacattacksquare.png'}
        />
        <meta property='og:width' content='1280' />
        <meta property='og:height' content='720' />
        <meta property='og:image:width' content='1280' />
        <meta property='og:image:height' content='720' />
        <meta
          property='og:image:secure_url'
          content={'https://s3.ap-south-1.amazonaws.com/cdn.cricket.com/stacattacksquare.png'}
        />
        <meta property='og:title' content='I challenge you. Can you beat my highest score in Stat Attack?' />
        <meta property='og:url' content='https://www.cricket.com/stat-attack/start-game' />

        <meta property='og:type' content='STAT ATTACK' />
        <meta property='fb:app_id' content='631889693979290' />
        <meta property='og:site_name' content='cricket.com' />

        <meta name='twitter:card' content='summary' />
        <meta name='twitter:title' content='I challenge you. Can you beat my highest score in Stat Attack?' />
        <meta
          name='twitter:description'
          content='I challenge you to beat my score. Lets see how well you know T20 cricket statistics?'
        />
        <meta
          property='twitter:image'
          content={'https://s3.ap-south-1.amazonaws.com/cdn.cricket.com/stacattacksquare.png'}
        />
        <meta name='twitter:url' content='https://www.cricket.com/stat-attack/start-game' />

        <link rel='canonical' href='https://www.cricket.com/stat-attack/start-game' />
      </Head>

      <div className='md:hidden lg:hidden xl:hidden hidden'>
        <div className='bg-white black lg:w-52 md:w-full center h-3 f4 font-semibold flex justify-center items-center mt-2'>
          Please switch to Mobile (Potrait Mode) View
        </div>
      </div>
      <div className='md:hidden lg:hidden xl:hidden'>
        <div className='stat-attack-bg min-h-screen'>
          <div className='bg-navy flex items-center'>
            <div className='flex justify-between items-center bg-gray-4 w-full'>
            <div className='ml-2 w-4 h-2 flex justify-center items-center cursor-pointer'>
              <img className='' onClick={() => navigate('/')} src={backIconWhite} alt=''></img>
            </div>
              <div className='text-base font-semibold mt-4 w-10/12 text-white pb-4'>STAT ATTACK</div>
              <div className='mr-2 w-4 h-2 flex justify-center items-center'>
                <img src={share} onClick={handleShare} alt='' className='w-12'></img>
              </div>
            </div>
          </div>
          <div className='px-2 pt-3 pb-4 text-sm font-semibold text-center text-white'>
            Remember card games where you'd win on superior player stats? They're back!{' '}
          </div>

          <div className='flex flex-col justify-between items-center px-3 mt-3 min-vh-20'>
            <div className='avatar-container flex'>
              <img className='h-44 shadow-4 pr-3' src={Player2Avatar} alt='' />
              <img className='h-44 shadow-4' src={Player1Avatar} alt='' />
            </div>
            <div className='pt-5 pb-1 text-xs font-medium flex text-center items-center lh-title pl-2 text-white'>
              T20I stats of players will be pitted against each other. Pick one stat that will help you win over you
              opponents player card.
            </div>
          </div>
          <div className='rules-container flex flex-col items-center pt-8'>
            <div className='flex items-center px-8 w-full md:w-80 lg:w-80 xl:w-80 min-h-min flex-row-reverse justify-end pb-8'>
              <div className='text-sm font-medium lh-title pl-3 pr-2 text-white'>
                You'll have 15 seconds to select a specific player stat.
              </div>
              <div className='flex flex-col justify-center items-center'>
                <div>
                  <img className='shadow-4 w-16 md:w-4 lg:w-4 xl:w-4' src={timer} alt='' />
                </div>
                <div className='text-xs minor-score-bg pt-1 text-red'>15 Sec</div>
              </div>
            </div>

            <div className='flex justify-between w-full md:w-80 lg:w-80 xl:w-80 items-center px-8 min-h-min'>
              <div className='flex flex-col justify-center items-center'>
                <div>
                  <img className='shadow-4 w-16 md:w-4 lg:w-4 xl:w-4' src={intromedal} alt='' />
                </div>
                <div className='text-xs minor-score-bg pt-1 text-red'>Score</div>
              </div>
              <div className='text-sm font-medium flex items-center lh-title pl-3 text-white'>
                {' '}
                Your score will be calculated on the number of consecutive cards you triumph over.
              </div>
            </div>
          </div>
          <div className='mt-3 flex items-center'>
            <div
              className='stat-score-gr rounded-full text-center bg-red py-2 w-full mx-24 my-4 center'
              onClick={() => (localStorage.removeItem('totalScore'), handleTrumpNavigation())}>
              <div className='text-white text-base font-semibold'>START GAME</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
