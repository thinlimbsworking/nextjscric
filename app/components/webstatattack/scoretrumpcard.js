import React from 'react';
import { useRouter } from 'next/router';
import CleverTap from 'clevertap-react';
// import Head from "next/head";
import { scoreRangeText, between, getRandomString } from '../../constant/constants';

const backIconWhite = '/svgs/backIconWhite.svg';
const backgroundImg = '/svgs/myScoreBackground.svg';
const scoreMedal = '/svgs/scoreMedal.svg';
const share = '/svgs/share-line.png';
// import playerCard from "/pngs/PlayerCard.png";

export default function ScoreTrumpCard(props) {
  let router = useRouter();
  let navigate = router.push;

  const tempTotalScore = global.window ? JSON.parse(global.window.localStorage.getItem('totalScore')) : null;
  const tempHighestScore = global.window ? JSON.parse(global.window.localStorage.getItem('highestScore')) : null;

  if (tempTotalScore === null || tempTotalScore === undefined || tempTotalScore === 'null')
    global.window ? global.window.localStorage.setItem('totalScore', 0) : '';
  if (tempHighestScore === null || tempHighestScore === undefined || tempHighestScore === 'null')
    global.window
      ? global.window.localStorage.setItem('highestScore', global.window.localStorage.getItem('totalScore'))
      : '';

  if (
    global.window
      ? JSON.parse(global.window.localStorage.getItem('totalScore')) >
        JSON.parse(global.window.localStorage.getItem('highestScore'))
      : false
  )
    global.window.localStorage.setItem('highestScore', global.window.localStorage.getItem('totalScore'));
  const totalScore = global.window ? JSON.parse(global.window.localStorage.getItem('totalScore')) : '';
  const highestScore = global.window ? JSON.parse(global.window.localStorage.getItem('highestScore')) : '';
  const rangedValue = Object.keys(scoreRangeText).filter((item) => {
    const temp = item.split('-');
    return between(totalScore, parseInt(temp[0]), temp[1] !== 'infinite' ? parseInt(temp[1]) : 'infinite');
  });
  const statusText = scoreRangeText[rangedValue][getRandomString(0, scoreRangeText[rangedValue].length)];

  const handlePlayNavigation = (venueID) => {
    CleverTap.initialize('StatAttackTryAgain', {
      Score: totalScore ? totalScore : 0,
      Platform: global.window.localStorage.Platform
    });
    // navigate("/stat-attack/play", { replace: true })
    navigate('/stat-attack/[type]', '/stat-attack/play');
  };

  const handleShare = (e) => {
    if (navigator.share !== undefined && location.protocol == 'https:') {
      if (navigator.running == true) {
        return;
      }
      navigator.running = true;
      navigator
        .share({
          title: `Stat-Attack`,
          text: `Hey, I scored ${tempTotalScore} on Stat Attack. I challenge you to beat my score! Play here: `,
          url: location.origin + '/stat-attack/start-game'
        })
        .then(() => {
          navigator.running = false;
        })
        .catch((error) => {
          navigator.running = false;
        });
    }
  };

  return (
    <div>
      <div className='md:block lg:block xl:block hidden '>
        <div className=' text-white text-xl text-semibold h-full w-full  center  font-semibold flex justify-center items-center mt-2'>
          Please switch to Mobile Potrait Mode View
        </div>
      </div>
      <div className='md:hidden lg:hidden xl:hidden'>
        <div className='min-vh-full stat-attack-bg'>
          {/* <div className='bg-navy flex items-center'>
            <div className='w-5 h-5 flex justify-center items-center pointer'>
              <img className='' onClick={() => navigate(`/`)} src={backIconWhite}></img>
            </div>
            <div className='flex justify-between items-center w-full'>
              <div className='text-base font-semibold text-white mr-2'>STAT ATTACK</div>
              <div className='w-5 h-5 flex justify-center items-center'>
                <img
                  src={share}
                  onClick={() => {
                    handleShare();
                  }}
                  alt=''
                  className='w-12'></img>
              </div>
            </div>
          </div> */}
          <div className='bg-navy flex items-center'>
            <div className='flex justify-between items-center bg-gray-4 w-full'>
            <div className='ml-2 w-4 h-2 flex justify-center items-center cursor-pointer'>
              <img className='' onClick={() => navigate('/')} src={backIconWhite} alt=''></img>
            </div>
              <div className='text-base font-semibold mt-4 w-10/12 text-white pb-4'>STAT ATTACK</div>
              <div className='mr-2 w-4 h-2 flex justify-center items-center'>
                <img src={share} onClick={handleShare} alt='' className='w-42'></img>
              </div>
            </div>
          </div>
          <div className='w-full h-full'>
            <img className='w-full' src={backgroundImg} alt=''></img>
            <div className='flex items-center justify-center flex-col absolute w-full' style={{ top: '80px' }}>
              <div className=''>
                <img className='' src={scoreMedal} alt='' />
              </div>
              <div className='w-full flex flex-col items-center pt-3 text-center'>
                <div className='text-white w-80 font-bold text-sm i'>{statusText}</div>
                <div className='font-normal text-base text-white mt-1 pt-3 pb-3 flex items-center'>
                  <span className='pr-1 text-base'>High Score</span>
                  <span className='font-medium' style={{ color: '#E33632' }}>
                    {highestScore ? highestScore : '0'}
                  </span>
                </div>
                <div className='text-xl font-semibold text-white mt-1 pb-2'>Score</div>
                <div className='f1 font-semibold text-red' style={{ fontSize: '6.25rem' }}>
                  {' '}
                  {totalScore ? totalScore : '0'}
                </div>
              </div>
              <div
                className='flex items-center bg-red rounded-full justify-center px-6'
                style={{ top: '220px' }}
                onClick={() => (handlePlayNavigation(), global.window.localStorage.removeItem('totalScore'))}>
                <div className='text-white p-2 font-semibold text-base cdcgr text-center'>PLAY AGAIN</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
