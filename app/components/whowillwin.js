import React, { useState, useEffect } from 'react'
const pitch = '/pngsV2/pitch.png'
const star = '/pngsV2/Star.png'
const UserProfile = '/pngsV2/UserProfile.png'
import { useQuery, useMutation } from '@apollo/react-hooks'
import {
  HOME_SCREEN_FAN_POLL,
  HOME_SCREEN_FAN_POLL_MUTAION,
} from '../api/queries'
export default function WhoWIllWin(props) {
  const [fanPollID, setFanPollID] = useState('')
  const [pollArray, setPollArray] = useState([])
  const [isVoted, setIsVoted] = useState(false)
  const { loading, error, data, fetchMore } = useQuery(HOME_SCREEN_FAN_POLL, {
    variables: { matchID: props.matchID || '' },
    onCompleted: (data) => {},
  })

  // console.log("WhoWIllWinWhoWIllWinWhoWIllWin",data&&data.homeScreenFanPoll.options)
  const [getPoll] = useMutation(HOME_SCREEN_FAN_POLL_MUTAION, {
    onCompleted: (result) => {
      fetchMore({
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult) return prev
          return fetchMoreResult
        },
      })
    },
  })

  const getLocalValue = () => {
    let values
    let val
    try {
      values = JSON.parse(window.localStorage.getItem('uservote')) || []
      if (values) {
        // setFanPollID(values.pollid);
        val = values.find((str) => str === data.homeScreenFanPoll.pollID)
          ? true
          : false
      }
    } catch (e) {
      setIsVoted(false)
    }
    // console.log("outsixdde",val,isVoted)
    setPollArray(values)
    setIsVoted(val)
  }
  // console.log("ededededed",isVoted,pollArray)
  const storeLocally = (id, option) => {
    let a = []
    a = pollArray || []
    a.push(id)
    try {
      window.localStorage.setItem('uservote', JSON.stringify(a))
      // setFanPollID(vote.pollid);
      setPollArray(a)
      setIsVoted(true)
    } catch (e) {}
  }
  const updateVotes = (option, id) => {
    getPoll({
      variables: {
        matchID: props.matchID || '',
        teamId: option,
      },
    })
    storeLocally(id)
  }
  useEffect(() => {
    getLocalValue()
  }, [data && data.homeScreenFanPoll && data.homeScreenFanPoll.pollID])
  // console.log("propsprops",pollArray)

  // console.log("HOME_SCREEN_FAN_POLLHOME_SCREEN_FAN_POLLHOME_SCREEN_FAN_POLL", props.teamIDs&&props.teamIDs.matchScore[0].teamID,props.teamIDs.matchScore[1].teamID)
  return data && data.homeScreenFanPoll.options ? (
    <div className=" text-white   mt-3 mx-3 lg:mx-0 md:mx-0  ">
      {/* <img className='w-full ' src={pitch} /> */}

      <div className="flex  hidden w-full lg:block md:block  lg:flex-row justify-center items-center">
        <div className="lg:hidden md:hidden  flex items-center justify-between   mt-5  rounded-t-xl  bg-gray p-1">
          <div className="flex text-xs ml-3 font-bold text-gray ">
            Predict The Winner
          </div>
          <div className="flex text-xs mr-2 font-bold ">
            Total votes:{data.homeScreenFanPoll.totalVotes}{' '}
          </div>
        </div>

        <div className="rounded-b-lg  p-3 flex justify-evenly  ">
          <div className="flex flex-col bg-gray rounded-xl mr-1  items-center justify-center  w-1/4">
            <div className="flex items-center justify-around  w-full">
              <div className="flex flex-col items-center justify-center">
                {' '}
                <img
                  className=" w-10 rounded-md z-10 "
                  src={`https://images.cricket.com/teams/${data.homeScreenFanPoll.options[0].teamId}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                />
              </div>
              <div className="flex">
                {' '}
                <img className="w-5 h-5" src={star} />
              </div>

              <div className="flex">
                {' '}
                <img
                  className="w-10 rounded-md"
                  src={`https://images.cricket.com/teams/${data.homeScreenFanPoll.options[2].teamId}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                />
              </div>
            </div>

            {isVoted || props.matchStatus === 'completed' ? (
              <div className="pt-1 text-center">
                <div className="text-gray-2 text-sm font-medium">
                  Total votes:{data.homeScreenFanPoll.totalVotes}
                </div>
              </div>
            ) : (
              <div className="pt-1 text-center">
                <div className="text-xs font-normal tracking-wider">
                  WHO DO YOU THINK{' '}
                </div>
                <div className="text-xs font-bold tracking-wider">
                  WILL WIN?
                </div>
                {isVoted && (
                  <div className="text-gray-2 text-xs font-medium">
                    Total votes:{data.homeScreenFanPoll.totalVotes}
                  </div>
                )}
              </div>
            )}
          </div>

          <div
            className="bg-gray  h-28 w-1/4 mr-3  p-1  rounded-xl relative"
            onClick={() =>
              !isVoted &&
              props.matchStatus != 'completed' &&
              updateVotes(
                data.homeScreenFanPoll.options[0].teamId,
                data.homeScreenFanPoll.pollID,
              )
            }
          >
            <div
              className={`${
                parseFloat(data.homeScreenFanPoll.options[0].percentage) >=
                  Math.max(
                    data.homeScreenFanPoll.options[1].percentage,
                    data.homeScreenFanPoll.options[2].percentage,
                  ) && props.matchStatus != 'completed'
                  ? 'bg-green-6'
                  : 'bg-gray-3 '
              } absolute bottom-0 w-full ${
                data.homeScreenFanPoll.options[0].percentage == '100.00'
                  ? 'rounded-xl'
                  : 'rounded-b-xl'
              }`}
              style={{
                height:
                  isVoted || props.matchStatus == 'completed'
                    ? `${parseFloat(
                        data.homeScreenFanPoll.options[0].percentage,
                      )}%`
                    : '0%',
              }}
            ></div>

            <div className="absolute top-3 w-full">
              <div className="flex  justify-around items-center">
                {/* */}
                <div className="text-sm font-medium">
                  {' '}
                  {isVoted || props.matchStatus == 'completed'
                    ? data.homeScreenFanPoll.options[0].percentage + '%'
                    : ''}
                </div>
              </div>
            </div>
            <div className="absolute bottom-1 flex justify-around items-center flex-col w-full">
              <img
                className=" w-10 h-6 rounded rounded-xs shadow-xl"
                src={`https://images.cricket.com/teams/${data.homeScreenFanPoll.options[0].teamId}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="text-medium mt-1 font-semibold">
                {data.homeScreenFanPoll.options[0].teamShortName}
              </div>
            </div>
          </div>

          <div
            className="bg-gray  h-28 w-1/4 mr-3 rounded-xl relative"
            onClick={() =>
              !isVoted &&
              props.matchStatus != 'completed' &&
              updateVotes('0', data.homeScreenFanPoll.pollID)
            }
          >
            <div
              className={`${
                parseFloat(data.homeScreenFanPoll.options[1].percentage) >
                  Math.max(
                    parseFloat(data.homeScreenFanPoll.options[0].percentage),
                    parseFloat(data.homeScreenFanPoll.options[2].percentage),
                  ) && props.matchStatus != 'completed'
                  ? 'bg-green-6 '
                  : 'bg-gray-3 '
              } absolute bottom-0 w-full ${
                data.homeScreenFanPoll.options[1].percentage == '100.00'
                  ? 'rounded-xl'
                  : 'rounded-b-xl'
              }`}
              style={{
                height:
                  isVoted || props.matchStatus == 'completed'
                    ? `${parseFloat(
                        data.homeScreenFanPoll.options[1].percentage,
                      )}%`
                    : '0%',
              }}
            ></div>

            <div className="absolute top-3 w-full">
              <div className="flex justify-around items-center">
                <div className="text-sm font-medium ">
                  {' '}
                  {isVoted || props.matchStatus == 'completed'
                    ? data.homeScreenFanPoll.options[1].percentage + '%'
                    : ''}
                </div>
              </div>
            </div>
            <div className="absolute bottom-1 flex justify-around items-center flex-col w-full">
              <img
                className=" w-6 p-1 shadow-xl bg-gray rounded-full"
                src={star}
              />
              <div className="text-medium font-semibold mt-1">Tie</div>
            </div>
          </div>
          {/* {console.log('edededed', data.homeScreenFanPoll.options)} */}
          <div
            className="bg-gray  h-28 w-1/4 rounded-xl relative"
            onClick={() =>
              !isVoted &&
              props.matchStatus != 'completed' &&
              updateVotes(
                data.homeScreenFanPoll.options[2].teamId,
                data.homeScreenFanPoll.pollID,
              )
            }
          >
            <div
              className={`${
                parseFloat(data.homeScreenFanPoll.options[2].percentage) >
                  Math.max(
                    data.homeScreenFanPoll.options[0].percentage,
                    data.homeScreenFanPoll.options[1].percentage,
                  ) && props.matchStatus != 'completed'
                  ? 'bg-green-6 '
                  : 'bg-gray-3  '
              } absolute bottom-0 w-full ${
                data.homeScreenFanPoll.options[2].percentage == '100.00'
                  ? 'rounded-xl'
                  : 'rounded-b-xl'
              }`}
              style={{
                height:
                  isVoted || props.matchStatus == 'completed'
                    ? `${parseFloat(
                        data.homeScreenFanPoll.options[2].percentage,
                      )}%`
                    : '0%',
              }}
            ></div>

            <div className="absolute top-3 w-full">
              <div className="flex justify-around items-center">
                <div className="text-sm font-medium">
                  {' '}
                  {isVoted || props.matchStatus == 'completed'
                    ? data.homeScreenFanPoll.options[2].percentage + '%'
                    : ''}
                </div>
              </div>
            </div>
            <div className="absolute bottom-1 flex justify-around items-center flex-col w-full">
              <img
                className=" w-8 h-5 rounded rounded-xs shadow-xl"
                src={`https://images.cricket.com/teams/${data.homeScreenFanPoll.options[2].teamId}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="text-md font-semibold mt-1">
                {data.homeScreenFanPoll.options[2].teamShortName}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="flex flex-col lg:hidden md:hidden  justify-center items-center">
        <div className="lg:hidden md:hidden w-full flex items-center justify-between   border-b border-black  p-2  mt-5  rounded-t-lg  bg-gray p-1">
          <div className="flex text-xs ml-3 font-bold text-white ">
            Predict The Winner
          </div>
          <div className="flex text-xs mr-2 font-bold text-gray-2">
            Total votes:{data.homeScreenFanPoll.totalVotes}{' '}
          </div>
        </div>

        <div className="bg-gray rounded-b-lg  p-3 flex justify-evenly  w-full ">
          <div
            className="bg-basebg h-20 w-1/3 mr-3 rounded-xl relative flex items-center justify-center"
            onClick={() =>
              !isVoted &&
              props.matchStatus != 'completed' &&
              updateVotes(
                data.homeScreenFanPoll.options[0].teamId,
                data.homeScreenFanPoll.pollID,
              )
            }
          >
            <div
              className={`${
                parseFloat(data.homeScreenFanPoll.options[0].percentage) >=
                  Math.max(
                    data.homeScreenFanPoll.options[1].percentage,
                    data.homeScreenFanPoll.options[2].percentage,
                  ) && props.matchStatus != 'completed'
                  ? 'bg-green-6'
                  : 'bg-gray-3 '
              } absolute bottom-0 w-full ${
                data.homeScreenFanPoll.options[0].percentage == '100.00'
                  ? 'rounded-xl'
                  : 'rounded-b-xl'
              }`}
              style={{
                height:
                  isVoted || props.matchStatus == 'completed'
                    ? `${parseFloat(
                        data.homeScreenFanPoll.options[0].percentage,
                      )}%`
                    : '0%',
              }}
            ></div>

            <div className="absolute top-7 w-full left-1">
              <div className="flex  items-center">
                <div className="text-sm font-medium">
                  {' '}
                  {isVoted || props.matchStatus == 'completed'
                    ? data.homeScreenFanPoll.options[0].percentage + '%'
                    : ''}
                </div>
              </div>
            </div>
            <div className="absolute  left-5 flex  items-center flex-col w-full">
              <img
                className=" w-10 h-6 rounded rounded-xs shadow-xl"
                src={`https://images.cricket.com/teams/${data.homeScreenFanPoll.options[0].teamId}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="text-medium mt-1 font-semibold">
                {data.homeScreenFanPoll.options[0].teamShortName}
              </div>
            </div>
          </div>

          <div
            className="bg-basebg h-20 w-1/3 mr-3 rounded-xl relative flex items-center justify-center"
            onClick={() =>
              !isVoted &&
              props.matchStatus != 'completed' &&
              updateVotes('0', data.homeScreenFanPoll.pollID)
            }
          >
            <div
              className={`${
                parseFloat(data.homeScreenFanPoll.options[1].percentage) >
                  Math.max(
                    parseFloat(data.homeScreenFanPoll.options[0].percentage),
                    parseFloat(data.homeScreenFanPoll.options[2].percentage),
                  ) && props.matchStatus != 'completed'
                  ? 'bg-green-6 '
                  : 'bg-gray-3 '
              } absolute bottom-0 w-full ${
                data.homeScreenFanPoll.options[1].percentage == '100.00'
                  ? 'rounded-xl'
                  : 'rounded-b-xl'
              }`}
              style={{
                height:
                  isVoted || props.matchStatus == 'completed'
                    ? `${parseFloat(
                        data.homeScreenFanPoll.options[1].percentage,
                      )}%`
                    : '0%',
              }}
            ></div>

            <div className="absolute top-8 left-2 w-full">
              <div className="flex items-center">
                <div className="text-sm font-medium ">
                  {' '}
                  {isVoted || props.matchStatus == 'completed'
                    ? data.homeScreenFanPoll.options[1].percentage + '%'
                    : ''}
                </div>
              </div>
            </div>
            <div className="absolute  left-5 flex  items-center flex-col w-full">
              <img
                className=" w-6 p-1 shadow-xl bg-gray rounded-full"
                src={star}
              />
              <div className="text-medium font-semibold mt-1">Tie</div>
            </div>
          </div>
          {/* {console.log('edededed', data.homeScreenFanPoll.options)} */}
          <div
            className="bg-basebg h-20 w-1/3 mr-3 rounded-xl relative flex items-center justify-center"
            onClick={() =>
              !isVoted &&
              props.matchStatus != 'completed' &&
              updateVotes(
                data.homeScreenFanPoll.options[2].teamId,
                data.homeScreenFanPoll.pollID,
              )
            }
          >
            <div
              className={`${
                parseFloat(data.homeScreenFanPoll.options[2].percentage) >
                  Math.max(
                    data.homeScreenFanPoll.options[0].percentage,
                    data.homeScreenFanPoll.options[1].percentage,
                  ) && props.matchStatus != 'completed'
                  ? 'bg-green-6 '
                  : 'bg-gray-3  '
              } absolute bottom-0 w-full ${
                data.homeScreenFanPoll.options[2].percentage == '100.00'
                  ? 'rounded-xl'
                  : 'rounded-b-xl'
              }`}
              style={{
                height:
                  isVoted || props.matchStatus == 'completed'
                    ? `${parseFloat(
                        data.homeScreenFanPoll.options[2].percentage,
                      )}%`
                    : '0%',
              }}
            ></div>

            <div className="absolute top-7 left-2 w-full">
              <div className="flex  items-center">
                <div className="text-sm font-medium">
                  {' '}
                  {isVoted || props.matchStatus == 'completed'
                    ? data.homeScreenFanPoll.options[2].percentage + '%'
                    : ''}
                </div>
              </div>
            </div>
            <div className="absolute  left-5 flex  items-center flex-col w-full">
              <img
                className=" w-8 h-5 rounded rounded-xs shadow-xl"
                src={`https://images.cricket.com/teams/${data.homeScreenFanPoll.options[2].teamId}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="text-md font-semibold mt-1">
                {data.homeScreenFanPoll.options[2].teamShortName}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div className=""></div>
  )
}
