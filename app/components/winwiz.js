import React, { useState, useEffect } from 'react'

const Winwiz = ({ pollData, click, isAnimate }) => {
  const [ball, setBall] = useState(0)
  let mytime = 0

  isAnimate &&
    (mytime = setTimeout(() => {
      // ball === 5  ? clearTimeout(mytime) : setBall(ball + 1)
      ball === pollData.length - 1 ? clearTimeout(mytime) : setBall(ball + 1)
    }, 1000))

  useEffect(() => {
    setBall(0)
    clearTimeout(mytime)
    mytime = 0
  }, [])

  return (
    <div className="mt-3 poll">
      <div className="flex justify-center items-center rounded-lg bg-gray-3">
        <div
          style={{
            width: `${
              (pollData && pollData[ball] && pollData[ball].homeTeamPercent) ||
              0
            }%`,
          }}
          className={` dib br2 br--left poll rounded-s-lg bg-green h-3`}
        ></div>
        {pollData &&
        pollData[ball] &&
        pollData[ball].tiePercent &&
        pollData[ball].tiePercent > 0 ? (
          <div
            className=" dib poll h-3"
            style={{
              width: `${
                (pollData && pollData[ball] && pollData[ball].tiePercent) || 0
              }%`,
            }}
          />
        ) : (
          <div />
        )}
        <div
          style={{
            width: `${
              (pollData && pollData[ball] && pollData[ball].awayTeamPercent) ||
              0
            }%`,
          }}
          className={`dib poll dark:bg-white bg-[#B7B7B7] rounded-e-lg dib br2 br--right h-3`}
        ></div>
      </div>

      <div className="flex items-center justify-between mt-2 text-gray-2 text-xs">
        <div className="flex items-center">
          <div className="h-2 w-2 bg-green rounded-full"></div>
          <div className="px-1 font-medium">
            {pollData && pollData[ball] && pollData[ball].homeTeamShortName}
          </div>
          <div className={``}>
            (
            {(pollData && pollData[ball] && pollData[ball].homeTeamPercent) ||
              0}
            %)
          </div>
        </div>

        <div className="flex items-center">
          <div className="h-2 w-2 bg-gray-3 rounded-full"></div>
          <div className="px-1 font-medium">
            {'T20' === 'Test' ? 'Draw' : 'Tie'}
          </div>
          <div className="grey_6">
            ({(pollData && pollData[ball] && pollData[ball].tiePercent) || 0}%)
          </div>
        </div>

        <div className="flex items-center">
          <div className="h-2 w-2 dark:bg-white bg-[#B7B7B7] rounded-full"></div>
          <div className="px-1 font-medium">
            {pollData && pollData[ball] && pollData[ball].awayTeamShortName}
          </div>
          <div className="">
            (
            {(pollData && pollData[ball] && pollData[ball].awayTeamPercent) ||
              0}
            %)
          </div>
        </div>
      </div>
    </div>
  )
}
export default Winwiz
