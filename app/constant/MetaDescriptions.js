let MainUrl = process.env.SITEMAP_URL
  ? process.env.SITEMAP_URL
  : 'https://www.cricket.com'
export const CRICLYTICS_PHASE = (data, path, type, criclyticName) => {
  console.log(
    data,
    'DATA',
    path,
    'PATH',
    type,
    'TYPE',
    criclyticName,
    'criclytics33333333333',
  )
  let titleSlug, desSlug
  if (
    data.homeTeamName &&
    data.awayTeamName &&
    data.matchNumber &&
    data.seriesName
  ) {
    let matchName = `${data.homeTeamName} vs ${data.awayTeamName} ${data.matchNumber}`
    titleSlug = `${matchName} | ${data.seriesName}`
    desSlug = `${matchName}, ${data.seriesName}`
  } else {
    titleSlug = criclyticName.split('-').join(' ')
    desSlug = criclyticName.split('-').join(' ')
  }
  let url = `${MainUrl}${path}`
  if (data) {
    const metaSection = {
      'key-stats': {
        h1: 'Key Stats',
        title: `Key Stats - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Key Stats to determine which team statistically has the upper hand for ${desSlug} on Cricket.com`,
        url: url,
      },
      'match-stats': {
        h1: 'Match Stats',
        title: `Match Stats - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Key Stats to determine which team statistically has the upper hand for ${desSlug} on Cricket.com`,
        url: url,
      },
      'match-reel': {
        h1: 'Match Reel',
        title: `Match Reel - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Match Reel, a fast-tracked reel of how the fortunes of the teams shifted through the game for ${desSlug} on Cricket.com`,
        url: url,
      },
      'player-projection': {
        h1: 'Player Projection',
        title: `Player Projection - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Player Projection, pitch conditions, projected score/wickets for ${desSlug} on Cricket.com`,
        url: url,
      },
      'game-change-overs': {
        h1: 'Game Changing Overs',
        title: `Game Changing Overs - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Game Changing Overs, a collection of 4 overs which changed the game for ${desSlug} on Cricket.com`,
        url: url,
      },
      'phases-of-play': {
        h1: 'Phases of Play',
        title: `Phases of play - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Phases of Play, a breakdown of how the teams have fared across different phases of play for ${desSlug} on Cricket.com`,
        url: url,
      },
      'report-card': {
        h1: 'Report Card',
        title: `Player Impact - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Report Card, a comprehensive view of a player's impact on the game for ${desSlug} on Cricket.com`,
        url: url,
      },
      'player-matchup': {
        h1: 'Player MatchUp',
        title: `Player MatchUp - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Matchups, a head-to-head comparison between two players selected by you, against each other for ${desSlug} on Cricket.com`,
        url: url,
      },
      'score-projection': {
        h1: 'Team Score Projection',
        title: `Team Score Projection - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Team Score Projection, a projection of final scores for ${desSlug} on Cricket.com`,
        url: url,
      },
      'run-comparison': {
        h1: 'Run Comparison',
        title: `Run Comparison - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Team Score Projection, a projection of final scores for ${desSlug} on Cricket.com`,
        url: url,
      },
      'live-player-projection': {
        h1: 'Live Player Projection',
        title: `Live Player Projection - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Live Player Projection, a prediction of player's score/wickets for ${desSlug} on Cricket.com`,
        url: url,
      },
      'over-simulation': {
        h1: 'over-simulation',
        title: `Over-simulation - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Over Simulator, a simulation over by over for ${desSlug} on Cricket.com`,
        url: url,
      },
      'form-index': {
        h1: 'Form Index',
        title: `Form Index - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Final Four, a final four for ${desSlug} on Cricket.com`,
        url: url,
      },
      'most-valuable-player': {
        h1: 'Most Valuable Player',
        title: `Most Valuable Player - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Most Valuable Player , a most valuable player of ${desSlug} on Cricket.com`,
        url: url,
      },
      'partnership-predictor': {
        h1: 'Partnership Predictor',
        title: `Partnership  Predictor - ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Most Valuable Player , a most valuable player of ${desSlug} on Cricket.com`,
        url: url,
      },
      'scoring-charts': {
        h1: 'SCORING CHARTS',
        title: `Scoring charts- ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Most Valuable Player , a most valuable player of ${desSlug} on Cricket.com`,
        url: url,
      },
      'match-stat': {
        h1: 'MATCH STATS',
        title: `Match Stats- ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Most Valuable Player , a most valuable player of ${desSlug} on Cricket.com`,
        url: url,
      },
      'upcoming-matches': {
        h1: 'SCORING CHARTS',
        title: `Scoring charts- ${titleSlug} | Cricket.com`,
        description: `Check out Criclytics for Most Valuable Player , a most valuable player of ${desSlug} on Cricket.com`,
        url: url,
      },
    }
    return metaSection[type]
  } else {
    return {
      description:
        'Criclytics is the live cricket score predictor & the most powerful predictive cricket algorithm. It provides live cricket match prediction about who will win.',
      title: 'Criclytics: Today Match Prediction, Who Will Win | Cricket.com',
      h1: 'Criclytics',
      url: 'https://www.cricket.com/criclytics',
    }
  }
}

export const VIDEOS = (data, pageType) => {
  if (data && pageType === 'main') {
    const metaSection = {
      Latest: {
        title: 'Latest Cricket Videos | Cricket.com',
        h1: 'Cricket Latest Videos',
        description:
          'Watch latest cricket videos, match highlights, player interviews, match previews, press conferences, playlists, news and more videos on Cricket.com',
        url: 'https://www.cricket.com/videos/latest',
      },
      MatchRelated: {
        h1: 'Cricket Videos - MatchRelated',
        title: 'Match Analysis | Videos | Cricket.com',
        url: 'https://www.cricket.com/videos/matchrelated',
      },
      country: {
        h1: 'Cricket Latest Videos',
        title: 'Cricket Videos By Country | Cricket.com',
        url: 'https://www.cricket.com/videos/latest',
      },
      Fantasy: {
        h1: 'Cricket Latest Videos',
        title: 'Fantasy Cricket | Videos | Cricket.com',
        url: 'https://www.cricket.com/videos/fantasy',
      },
      News: {
        h1: 'Cricket Videos - Latest News',
        title: 'Latest News | Videos | Cricket.com',
        description:
          'Watch latest cricket news, videos, match highlights, player interviews, match previews, press conferences, playlists and more videos on Cricket.com',
        url: 'https://www.cricket.com/videos/news',
      },
      highlights: {
        h1: 'Cricket Videos - Highlights',
        title: 'Highlights | Videos | Cricket.com',
      },
      events: {
        h1: 'Cricket Videos - Events',
        title:
          'Watch cricket events, press conferences, match highlights, news, player interviews, match previews, playlists and more videos on Cricket.com',
        title: 'Events | Press Conferences | Videos  | Cricket.com',
      },
      interviews: {
        h1: 'Cricket Videos - Interviews',
        title: 'Player Interiews | Videos| Cricket.com',
      },
      playlist: {
        h1: 'Cricket Videos - Playlists',
        title: 'Playlists| Videos | Cricket.com',
      },
    }
    return metaSection[data]
  } else {
    return {
      h1: 'Cricket Latest Videos',
      title: `${data.title}| Cricket.com`,
      description:
        'Watch latest cricket videos, match highlights, player interviews, match previews, press conferences, playlists, news and more videos on Cricket.com',
      url: `https://www.cricket.com/videos/${data.type}`,
    }
  }
}

export const NEWS = {
  latest: {
    title: 'Latest Cricket News | Cricket Updates | Cricket.com',
    h1: 'Latest Cricket News',
    description:
      'Get the latest cricket news, match analysis, match related news, match coverage and recent updates of all international and domestic matches on Cricket.com',
    url: 'https://www.cricket.com/news/latest',
  },
  news: {
    h1: 'Cricket News',
    title: 'Latest Cricket News & Articles | Cricket Updates | Cricket.com',
    description:
      'Get the breaking cricket news, match related news, match analysis, match coverage and recent updates of all international and domestic matches on Cricket.com',
    url: 'https://www.cricket.com/news',
  },
  onthisday: {
    h1: 'IPL',
    title: 'IPL | Cricket News & Latest Cricket Updates | Cricket.com',
    description:
      'Get the articles about a brief cricket history and players performances which is happened On This Day on Cricket.com',
    url: 'https://www.cricket.com/videos/on-this-day',
  },
  matchrelated: {
    h1: 'Match Related',
    title: 'Match Related News | Latest Cricket Updates | Cricket.com',
    description:
      'Get the match related cricket news, pre-match & post-match analysis, fantasy preview, match report & latest updates of all cricket matches on Cricket.com',
    url: 'https://www.cricket.com/news/match-related',
  },
  features: {
    h1: 'Features',
    title: 'Features | Cricket News & Latest Cricket Updates | Cricket.com',
    description:
      'Get the latest cricket updates, articles about opinions on player performances, players interviews and experts interviews on Cricket.com',
    url: 'https://www.cricket.com/news/features',
  },
}

export const TEAMS = (data) => {
  if (!data)
    return {
      title: 'Cricket Teams - International, Domestic, T20 & IPL | Cricket.com',
      h1: 'Cricket Teams',
      description:
        'Check out the cricket teams of international, domestic, T20 leagues, IPL and their team schedule, player details, stats, latest news and updates on Cricket.com',
      url: 'https://www.cricket.com/teams',
    }
  return {
    title: `${data.countryName} National Cricket Team, Schedule, Stats & News | Cricket.com`,
    h1: `${data.countryName} National Cricket Team`,
    description: `Check out the ${data.countryName} national cricket team and match schedule, ${data.countryName} player details, stats, latest news, photos, videos & recent updates on Cricket.com`,
    url: 'https://www.cricket.com' + data.url,
  }
}

export const STADIUM = (data) => {
  if (!data)
    return {
      title: 'Cricket Stadiums | Cricket Grounds Stats & Details | Cricket.com',
      h1: 'Cricket Grounds Stats & Details',
      description:
        'Check out all the cricket stadiums profiles, international ODI, Test, T20 and IPL matches history, ground stats, news, venue photos and more on Cricket.com',
      url: 'https://www.cricket.com/stadiums',
    }
  return {
    title: data.title + ' | Cricket Grounds | Cricket.com',
    h1: data.title,
    description:
      data.description +
      ' Profile - Cricket ground details, ODI, Test & T20 matches history, statistics, news, venue photos and more on Cricket.com',
    url: 'https://cricket.com' + data.url,
  }
}

export const PLAYERS = (data) => {
  if (!data)
    return {
      h1: `Cricket Players`,
      title:
        'Cricket Players - Profile, Ranking, Stats, Career Info, News | Cricket.com',
      description:
        'Read about cricket players profile, biography, ODI, Test and T20 rankings, stats, records, career details, latest news and updates on Cricket.com',
      url: 'https://www.cricket.com/players',
    }
  return {
    h1: `${data.playerName}`,
    title:
      data.title +
      ' - Profile, Ranking, Stats, Career Info, News | Cricket.com',
    description: `Read about ${data.playerName}'s profile, biography, ODI, Test and T20 rankings, stats, records, career details, latest news and updates on Cricket.com`,
    url: 'https://cricket.com' + data.url,
  }
}
export const Archive = (data) => {
  if (!data) {
    return {
      h1: 'Cricket Archives, International ODI and Test Matches | Cricket.com',
      description:
        'Criclytics is the live cricket score predictor & the most powerful predictive cricket algorithm. It provides live cricket match prediction about who will win.',
      title:
        'Cricket Archives, International ODI and Test Matches | Cricket.com',
      url: 'https://www.cricket.com/archives',
    }
  } else {
    let { year } = data
    return {
      h1: `Cricket Archives of year ${year}, International ODI and Test Matches | Cricket.com`,
      description:
        'Criclytics is the live cricket score predictor & the most powerful predictive cricket algorithm. It provides live cricket match prediction about who will win.',
      title:
        'Cricket Archives, International ODI and Test Matches | Cricket.com',
      url: 'https://www.cricket.com/archives',
    }
  }
}

export const SERIES = (data, pageType) => {
  if (data.tabInfo && pageType === 'main') {
    const metaSection = {
      International: {
        title:
          'Cricket Series, International ODI and Test Matches | Cricket.com',
        h1: 'International Cricket Series',
        description:
          'Check out the schedule of current and upcoming cricket series and match results for all international ODI & Test matches at Cricket.com',
        url: 'https://www.cricket.com/videos/series/international',
      },
      Domestic: {
        title: 'Cricket Series, Domestic ODI and Test Matches | Cricket.com',
        h1: 'Domestic Cricket Series',
        description:
          'Check out the schedule of current and upcoming cricket series and match results for all domestic ODI & Test matches at Cricket.com',
        url: 'https://www.cricket.com/videos/series/domestic',
      },
    }
    return metaSection[data.tabInfo]
  } else if (pageType === 'detail') {
    return {
      title: `${data.name}, Match Schedule & Overview | Cricket.com`,
      description: `${data.name}, match schedule & overview, players & squad, stats & records, latest news, stadium details, photos and videos at Cricket.com`,
      url: `https://www.cricket.com/series/${data.tab}/${data.seriesId}/${data.url}-${data.type}`,
    }
  } else if (pageType === 'results') {
    return {
      title: `${data.name}, Match Schedule & Overview | Cricket.com`,
      description: `${data.name}, match schedule & overview, players & squad, stats & records, latest news, stadium details, photos and videos at Cricket.com`,
      url: `https://www.cricket.com/series/${data.tab}/${data.seriesId}/${data.url}-${data.type}`,
    }
  }
  return {
    title:
      'Cricket Series | International & Domestic ODI, Test, T20 Series | Cricket.com',
    h1: 'Cricket Series',
    description:
      'Check out the current & upcoming international and domestic cricket series, match schedules, squad, news, venues for all ODI, Test, T20 leagues on Cricket.com',
    url: 'https://www.cricket.com/series',
  }
}

export const CRICLYTICS = (data) => {
  if (!data) {
    return {
      h1: 'Criclytics',
      description:
        'Criclytics is the live cricket score predictor & the most powerful predictive cricket algorithm. It provides live cricket match prediction about who will win.',
      title: 'Criclytics: Today Match Prediction, Who Will Win | Cricket.com',
      url: 'https://www.cricket.com/criclytics',
    }
  }
}

export const ShcheduleHome = (tab, status, path) => {
  let url = `${MainUrl}${path}`
  let requiredStatus = status.split('-')[0]

  if (tab && status) {
    let metaTags = {
      all: {
        upcoming: {
          description:
            'Get the list of upcoming cricket schedules for all ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Upcoming ODI, Test, T20 Matches | Cricket.com',
          url: url,
        },
        live: {
          description:
            'Get the list of current cricket schedules for all ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Current ODI, Test, T20 Matches | Cricket.com',
          url: url,
        },
        completed: {
          description:
            'Get the cricket results of completed matches for all ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | ODI, Test, T20 Match Results | Cricket.com',
          url: url,
        },
      },
      international: {
        upcoming: {
          description:
            'Get the list of upcoming cricket schedules for all international ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Upcoming International ODI, Test, T20 Matches | Cricket.com',
          url: url,
        },
        live: {
          description:
            'Get the list of current cricket schedules for all international ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Current International ODI, Test, T20 Matches | Cricket.com',
          url: url,
        },
        completed: {
          description:
            'Get the cricket results of completed matches for all international ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | International ODI, Test, T20 Match Results | Cricket.com',
          url: url,
        },
      },
      domestic: {
        upcoming: {
          description:
            'Get the list of upcoming cricket schedules for all domestic ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Upcoming Domestic ODI, Test, T20 Matches | Cricket.com',
          url: url,
        },
        live: {
          description:
            'Get the list of current cricket schedules for all domestic ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Current Domestic ODI, Test, T20 Matches | Cricket.com',
          url: url,
        },
        completed: {
          description:
            'Get the cricket results of completed matches for all domestic ODI, Tests & T20 series along with complete match info & more on Cricket.com',
          title:
            'Cricket Schedule | Domestic ODI, Test, T20 Match Results | Cricket.com',
          url: url,
        },
      },
    }
    return metaTags[tab][requiredStatus]
  } else {
    return {
      description:
        'Get the cricket results of completed matches for all domestic ODI, Tests & T20 series along with complete match info & more on Cricket.com',
      title:
        'Cricket Schedule | Domestic ODI, Test, T20 Match Results | Cricket.com',
      url: MainUrl,
    }
  }
}

export const INDEX = (data) => {
  if (!data) {
    return {
      h1:
        'Live Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com',
      description:
        'Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com',
      title:
        'Live Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com',
      url: MainUrl,
      showScript: true,
    }
  }
}

export const FRCHome = (data) => {
  if (!data) {
    return {
      h1: 'Fantasy Research Center | Cricket.com',
      description:
        'Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com',
      title: 'Fantasy Research Center | Latest News | Cricket.com',
      url: MainUrl,
    }
  }
}

export const Schedule = (data, liveSore, path, tab, title, ...props) => {
  let status = data && data.matchStatus === 'completed' ? '' : 'live'
  let matchName = `${data && data.homeTeamName} vs ${
    data && data.awayTeamName
  } ${data && data.matchType ? data && data.matchType : ''} ${
    data && data.matchNumber ? data && data.matchNumber : ''
  }`
  let url = `${MainUrl}${path}`
  if (data) {
    const metaSection = {
      scorecard: {
        title: `${liveSore} Scorecard - ${matchName}, ${data.seriesName} | Cricket.com`,
        h1: `${matchName}, ${data.seriesName} - Scorecard`,
        description: `Catch live cricket score and full scorecard of ${matchName}, ${data.seriesName} on Cricket.com`,
        url: url,
      },
      highlights: {
        title: `Highlights - ${matchName}, ${data.seriesName} | Cricket.com`,
        h1: `${matchName}, ${data.seriesName} - Highlights`,
        description: `Get highlights of ${matchName}, ${data.seriesName} on Cricket.com`,
        url: url,
      },
      matchinfo: {
        title: `Match Information - ${matchName}, ${data.seriesName} | Cricket.com`,
        h1: `${matchName}, ${data.seriesName} - Match Info`,
        description: `Get full detailed match information of ${matchName}, ${data.seriesName} on Cricket.com`,
        url: url,
      },

      summary: {
        title: `Match Summary - ${matchName}, ${data.seriesName} | Cricket.com`,
        h1: `${matchName}, ${data.seriesName} - Summary`,
        description: `${matchName} cricket score, ball by ball coverage, match summary of the ${data.seriesName} on Cricket.com`,
        url: url,
      },
      articles: {
        title: `News - ${matchName}, ${data.seriesName} | Cricket.com`,
        h1: `${matchName}, ${data.seriesName} - Articles`,
        description: `Get latest cricket news and match related articles of ${matchName}, ${data.seriesName} on Cricket.com`,
        url: url,
      },
      videos: {
        title: `Videos -  ODI Cricket Videos & Streaming | Cricket.com`,
        h1: ` ${data.title} - Videos`,
        description: `
        Watch ${data.title} Cricket videos, match highlights, player interviews and match previews on Cricket.com. Get upcoming information of ${data.seriesName} Only ODI.
       `,
        url: url,
      },
      commentary: {
        h1: `${matchName}, ${data.seriesName} - Commentary`,
        title: `${status.toUpperCase()} Cricket Score - ${matchName}, ${
          data.seriesName
        } | Cricket.com`,
        description: `Catch ${matchName} ${status} cricket score, ball by ball coverage, match commentary of the ${data.seriesName} on Cricket.com`,
        url: url,
      },
      dugout: {
        h1: `${matchName}, ${data.seriesName} - Dugout`,
        title: `${status.toUpperCase()} Cricket Score - ${matchName}, ${
          data.seriesName
        } | Cricket.com`,
        description: `Scoring areas of  Players. Check out all Wagon Wheels at cricket.com. Download Now: https://cricketapp.app.link/downloadapp`,
        url: url,
      },
    }

    return metaSection[tab]
  } else {
    return {
      description:
        'Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com',
      title:
        'Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com',
      h1: 'Live Cricket Score',
      url: 'https://www.cricket.com/',
    }
  }
}

// series/international/2283/india-tour-of-ireland-2022/matches
export const SeriesView = (data, path, tab) => {
  let url = `${MainUrl}${path}`
  // India Tour of England, 2022 | IND VS. ENG Match Schedule, Scores & Results
  // awayTeamShortName:props.data&&props.data.matcheslist&&props.data.matcheslist[0].awayTeamShortName,
  //           homeTeamShortName:props.data&&props.data.matcheslist&&props.data.matcheslist[0].homeTeamShortName
  // {Match Name, Year} Squad & Players | {Match Name (using country code)}
  //Get latest India Tour of England, 2022 news on Cricket.com. Read breaking cricket news, match previews, reviews, and player interviews of India tour of England
  if (data) {
    const metaSection = {
      home: {
        h1: `${data.seriesName} - Overview`,
        title:
          data.seriesName == 'INDIA TOUR OF IRELAND 2022'
            ? 'IND vs IRELAND 2022, Squads Match Schedule & Results | Cricket.com'
            : `${data.seriesName}, ${
                data.awayTeamShortName ? data.awayTeamShortName : ''
              } ${data.awayTeamShortName ? 'vs' : ''}  ${
                data.homeTeamShortName ? data.homeTeamShortName : ''
              } Match Schedule, Scores & Results | Cricket.com`,
        description:
          data.seriesName == 'INDIA TOUR OF IRELAND 2022'
            ? 'Check India vs Ireland T20 2022, Match Schedule and other upcoming Cricket Matches on Cricket.com. Get the latest Cricket news, videos, match reports and live scores'
            : `Check ${data.seriesName}, Match Schedule and other upcoming Cricket Matches on Cricket.com.  Get the latest update, live scores, match schedule for ${data.awayTeamShortName} vs  ${data.homeTeamShortName} Match`,
        url: url,
      },
      matches: {
        h1: `${data.seriesName} - Matches`,
        title:
          data.seriesName == 'INDIA TOUR OF IRELAND 2022'
            ? 'IND vs IRELAND 2022, Squads Match Schedule & Results | Cricket.com'
            : `${data.seriesName}, ${data.awayTeamShortName} vs  ${data.homeTeamShortName} Match Schedule, Scores & Results | Cricket.com`,
        description:
          data.seriesName == 'INDIA TOUR OF IRELAND 2022'
            ? 'Check India vs Ireland T20 2022, Match Schedule and other upcoming Cricket Matches on Cricket.com. Get the latest Cricket news, videos, match reports and live scores'
            : `Check ${data.seriesName}, Match Schedule and other upcoming Cricket Matches on Cricket.com.  Get the latest update, live scores, match schedule for ${data.awayTeamShortName} vs  ${data.homeTeamShortName} Match`,
        url: url,
      },
      news: {
        h1: `${data.seriesName} - Highlights | ${data.awayTeamShortName} vs  ${data.homeTeamShortName} News `,
        title: `${data.seriesName}, News | Cricket.com`,
        description: `Get latest ${data.seriesName} news on Cricket.com.  Read breaking cricket news, match previews, reviews, and player interviews of ${data.seriesName}`,
        url: url,
      },
      'points-table': {
        h1: `${data.seriesName} - Points Table`,
        title: `${data.seriesName}, Point Table | Cricket.com`,
        description: `GGet cricket stats, player stats, batting stats, bowling stats, most runs, most wickets and all other stats of the ${data.seriesName} on Cricket.com`,
        url: url,
      },
      squads: {
        h1: `${data.seriesName} - Squad & Players |  ${data.awayTeamShortName} vs  ${data.homeTeamShortName} `,
        title:
          data.seriesName.toLowerCase() == 'india tour of ireland 2022'
            ? 'India T20I Squad & Players for India Tour Of Ireland 2022 | Cricket.com'
            : ` ${data.seriesName}  Squad & Players for  ${data.seriesName}| Cricket.com`,
        description:
          data.seriesName.toLowerCase() == 'india tour of ireland 2022'
            ? 'Check India T20I Squad and Keep up to date with the latest and complete list of cricket players for India Tour Of Ireland 2022 series at Cricket com'
            : `

        Check team  ${data.seriesName} and get all the latest information of cricket players for ${data.seriesName} series on Cricket.com

        
      `,
        url: url,
      },
      // Check out all the cricket match schedule, venue details, stadium and ground details of India tour of England, 2022 series on Cricket com. Read the detailed IND vs. ENG match info
      stadiums: {
        h1: `${data.seriesName} Stadiums & Venues | Cricket.com`,
        title:
          data.seriesName.toLowerCase() == 'india tour of ireland 2022'
            ? 'India Tour Of Ireland 2022 Stadiums & Venue | Cricket.com '
            : ` ${data.seriesName},  Stadiums & Venue | Cricket.com `,
        description:
          data.seriesName.toLowerCase() == 'india tour of ireland 2022'
            ? 'Check out all the cricket match schedule, venue details, stadium and ground details of India Tour Of Ireland 2022 series on Cricket com'
            : ` Check out all the cricket match schedule, venue details, stadium and ground details of  ${data.seriesName} series on Cricket com  Read the detailed   ${data.awayTeamShortName} vs  ${data.homeTeamShortName} match info`,
        url: url,
      },
      stats: {
        h1: `${data.seriesName} - Statistics`,
        title: `${data.seriesName}, Stats & Records | Cricket.com`,
        description: `Get cricket stats, player stats, batting stats, bowling stats, most runs, most wickets and all other stats of the ${data.seriesName} on Cricket.com`,
        url: url,
      },
    }

    return metaSection[tab]
  } else {
    return {
      h1: 'Cricket Series',
      description:
        'Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com',
      title:
        'Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com',
      url: 'https://www.cricket.com/',
    }
  }
}

export const APP_IPL_TEAMS = (data) => {
  let url = `${MainUrl}iplteams`
  if (!data) {
    return {
      title: 'Latest Cricket News & Articles | Cricket Updates | Cricket.com',
      description:
        'IPL Stats - Find the strengths and weakness of all your favourite IPL players and teams',
      url: url,
    }
  }
}

export const FRCView = (data, path, tab) => {
  let url = `${MainUrl}${path}`

  if (data) {
    let tabName =
      tab &&
      tab
        .split('-')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')
    let seriesSlug =
      data.seriesSlug &&
      data.seriesSlug
        .split('-')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')
    return {
      title: `FRC - ${
        tabName ? tabName : 'Team'
      } | ${seriesSlug} | Cricket.com`,
      h1: `Fantasy Research Center`,
      description: `Fantasy Team - ${
        tabName ? tabName : 'Team'
      } with players performances & projected points for ${seriesSlug} on Fantasy Research Center. `,
      url: url,
    }
  } else {
    const metaSection = {
      live: {
        title: `Fantasy Research Center | Live Matches | Cricket.com`,
        h1: `Live Fantasy Research Center`,
        description: `Create your fantasy team and watch fantasy previews for live matches and series on Fantasy Research Center. `,
        url: url,
      },
      upcoming: {
        title: `Fantasy Research Center | Upcoming Matches | Cricket.com`,
        h1: `Upcoming Fantasy Research Center`,
        description: `Create your fantasy team and watch fantasy previews for upcoming matches and series on Fantasy Research Center.`,
        url: url,
      },
      completed: {
        title: `Fantasy Research Center | Completed Matches - Results | Cricket.com`,
        h1: `Completed Fantasy Research Center`,
        description: `Watch fantasy previews and results for completed matches and series on Fantasy Research Center.`,
        url: url,
      },
    }
    return metaSection[tab]
  }
}
