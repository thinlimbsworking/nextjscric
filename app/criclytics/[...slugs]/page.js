'use client'

import React, { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import Link from 'next/link'
import CleverTap from 'clevertap-react'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { useQuery } from '@apollo/react-hooks'
import axios from 'axios'
import { format } from 'date-fns'
// components
import Loading from '../../components/loading'
import LiveSwiperCompleted from '../../components/crcltics/liveSwipeCrclytics/swipeliveCrcltics'
import { getCriclyticsUrl, GET_FRC_HOME_PAGE_AXIOS } from '../../api/queries'

import Finalfour from '../../components/crcltics/finalFour'
import Playerindex from '../../components/crcltics/mostValuePlayer'
import MetaDescriptor from '../../components/commom/metadecription'
import { CRICLYTICS_PHASE } from '../../constant/MetaDescriptions'
import SwiperCompleted from '../../components/crcltics/swipeCrclytics/swipeCompleted'
import { CRYCLYTICS_VIEW } from '../../constant/Links'
import { CRYCLYTICS_KEYS } from '../../constant/constants'
// import Urltracking from '../../components/Common/urltracking';
import GameChangingOver from '../../components/crcltics/swipeCrclytics/gameChanginOver'
import UpcomingSwipe from '../../components/crcltics/upcomingswipeCrclytics/swipeUpcoming'
import UpcomingMiniScoreCard from '../../components/crcltics/upcomingMiniScoreCard'
import CompleteComponent from '../../components/crcltics/swipeCrclytics/completed'
import UpcomingDesktop from '../../components/crcltics/upcomingswipeCrclytics/upcomingdesktop'
import LiveDsktop from '../../components/crcltics/liveSwipeCrclytics/livedesktop'
import {
  MATCH_DATA_FOR_SCORECARD,
  GET_KEY_MATCH_UPS_PHASE_STATS,
  CRICLYTICS_IPL,
  GET_PARTNERSHIP_BY_MATCH_ID,
} from '../../api/queries'
import {
  getCriclyticsScore,
  getCriclyticsUrlHome,
  getFrcSeriesTabUrl,
} from '../../api/services'

// import SwipeCompleted from "../"
const MatchUps = dynamic(() => import('../../components/matchups'), {
  loading: () => <Loading />,
})
const KeyStats = dynamic(() => import('../../components/crcltics/keyStats'), {
  loading: () => <Loading />,
})
const LivePlayerProjection = dynamic(
  () => import('../../components/crcltics/livePlayerProjection'),
  {
    loading: () => <Loading />,
  },
)
const LiveScorePredictorODI = dynamic(
  () => import('../../components/crcltics/liveScorePredictorODI'),
  {
    loading: () => <Loading />,
  },
)
const PlayerProjection = dynamic(
  () => import('../../components/crcltics/playerProjection'),
  {
    loading: () => <Loading />,
  },
)
const DeathOverSimulator = dynamic(
  () => import('../../components/crcltics/deathOverSimulator'),
  {
    loading: () => <Loading />,
  },
)
const MatchStats = dynamic(
  () => import('../../components/crcltics/matchStats'),
  {
    loading: () => <Loading />,
  },
)
const PhasesOfPlay = dynamic(
  () => import('../../components/crcltics/phaseOfPlay'),
  {
    loading: () => <Loading />,
  },
)

const ScoringChart = dynamic(
  () => import('../../components/crcltics/scoringchart.js'),
  {
    loading: () => <Loading />,
  },
)

const MatchRatings = dynamic(
  () => import('../../components/crcltics/matchRatings'),
  {
    loading: () => <Loading />,
  },
)
const GameChangingOvers = dynamic(
  () => import('../../components/crcltics/gameChangingOvers'),
  {
    loading: () => <Loading />,
  },
)
const PhasesOfPlayTest = dynamic(
  () => import('../../components/crcltics/phaseOfPlayTest'),
  {
    loading: () => <Loading />,
  },
)

const Partner = dynamic(() => import('../../components/crcltics/partner'), {
  loading: () => <Loading />,
})

const QualificationProb = dynamic(
  () => import('../../components/crcltics/qualificationProb'),
  {
    loading: () => <Loading />,
  },
)

const runImg = '/pngsV2/run-comparison.png'
const OverSimulatorImg = '/pngsV2/over-simulator.png'
const keyStatImg = '/pngsV2/over-simulator.png'
const formInd = '/pngsV2/form-index.png'
const matchupImg = '/pngsV2/player-matchup.png'
const scoreproj = '/pngsV2/score-projection.png'
const phaseofPlayImg = '/pngsV2/phases-of-play.png'
const reportImg = '/pngsV2/reportcard.png'
const gameChangingImg = '/pngsV2/game-changing.png'
const matchReelImg = '/pngsV2/match-reel.png'
const matchStatImg = '/pngsV2/match-stats.png'
/**
 * @description Individual Criclytics category view
 * @route /criclytics/matchID/matchName/tab
 * @example /criclytics/197726/kkr-vs-rcb-match-39-indian-premier-league-2020/key-stats
 * @param { Object } props
 */

// async function getMatchDataForScorecard(matchID) {
//   const res = await axios.post(process.env.API, {
//     query: MATCH_DATA_FOR_SCORECARD_AXIOS,
//     variables: { type: 'latest', page: 0, matchID: matchID },
//   })
//   return res
// }

// async function getKeyStats(matchID) {
//   const resKey = await axios.post(process.env.API, {
//     query: GET_KEY_MATCH_UPS_PHASE_STATS_AXIOS,
//     variables: { matchID: matchID },
//   })
//   return resKey.data
// }

// async function getIplData() {
//   const resIpl = await axios.post(process.env.API, {
//     query: CRICLYTICS_IPL_AXIOS,
//   })
//   return resIpl
// }

// async function getPartnershipData(matchID) {
//   const resPartner = await axios.post(process.env.API, {
//     query: GET_PARTNERSHIP_BY_MATCH_ID_AXIOS,
//     variables: { matchID: matchID },
//   })
//   return resPartner
// }

export default function CriclyticsSlider({ params }) {
  const routerTabName =
    params.slugs.length === 2 ? params.slugs[1] : params?.slugs[2]
  const tabName = routerTabName
  const router = useRouter()
  const pathname = usePathname()

  const currpath = sessionStorage.getItem('currentPath')
  const prevPath = sessionStorage.getItem('prevPath')

  const matchID = params?.slugs[0]
  const criclyticName = params?.slugs[1]

  const { loading, error, data: matchData } = useQuery(
    MATCH_DATA_FOR_SCORECARD,
    {
      variables: { type: 'latest', page: 0, matchID: matchID },
    },
  )
  const { data: getKeyStats } = useQuery(GET_KEY_MATCH_UPS_PHASE_STATS, {
    variables: { matchID: matchID },
  })
  const { data: getPartnershipData } = useQuery(GET_PARTNERSHIP_BY_MATCH_ID, {
    variables: { matchID: matchID },
  })
  const { data: iplData } = useQuery(CRICLYTICS_IPL)

  const [activeSwiper, setActiveSwiper] = useState()
  const [data, setMatchScorecardData] = useState()
  const [keyMatchStateData, setKeyMatchUps] = useState()
  // const [iplData, setIplData] = useState()
  const [partnershipDataMatchId, setPartnershipData] = useState()

  const getCriclyticdUrlHome = (match) => {
    let tab =
      match.matchStatus === 'completed'
        ? match.matchType !== 'Test'
          ? ''
          : ''
        : match.matchStatus === 'live' && match.isLiveCriclyticsAvailable
        ? ''
        : match.matchStatus === 'upcoming' ||
          (match.matchStatus === 'live' && !match.isLiveCriclyticsAvailable)
        ? ''
        : ''
    return getCriclyticsUrlHome(match, tab)
  }
  // useEffect(() => {
  //   let mounted = true
  //   getMatchDataForScorecard(matchID)
  //     .then((res) => {
  //       console.log(res?.data?.data, 'data90')
  //       if (mounted) {
  //         setMatchScorecardData(res?.data?.data)
  //       }
  //     })
  //     .catch((err) => console.log('err- - ', err))

  //   getKeyStats(matchID)
  //     .then((resKey) => {
  //       console.log(resKey?.data, 'data7878')
  //       if (mounted) {
  //         setKeyMatchUps(resKey?.data)
  //       }
  //     })
  //     .catch((err) => console.log('err- - ', err))
  //   getIplData()
  //     .then((res) => {
  //       if (mounted) {
  //         setIplData(res?.data)
  //       }
  //     })
  //     .catch((err) => console.log('err- - ', err))

  //   getPartnershipData(matchID)
  //     .then((res) => {
  //       if (mounted) {
  //         setPartnershipData(res?.data)
  //       }
  //     })
  //     .catch((err) => console.log(err, 'errorOccured'))
  //   return () => (mounted = false)
  // }, [])

  const indexTell = () => {
    if (routerTabName === 'player-impact') {
      return '0'
    }
    if (routerTabName === 'run-comparison') {
      return '1'
    }
    if (routerTabName === 'game-change-overs') {
      return '2'
    }
    if (routerTabName === 'match-reel') {
      return '3'
    }
    if (routerTabName === 'match-stat') {
      return '4'
    }
    if (routerTabName === 'phases-of-play') {
      return '5'
    }
  }
  // console.log('tabNametabNametabName', tabName)
  useEffect(() => {}, [activeSwiper])

  const NevSlider = () => {
    // LivePOP/ CompletedPOP/CompletedManhattan

    CleverTap.initialize('Criclytics', {
      Source: 'Slider',
      MatchID: matchData.miniScoreCard.data[0].matchID,
      SeriesID: matchData.miniScoreCard.data[0].seriesID,
      TeamAID: matchData.miniScoreCard.data[0].matchScore[0].teamID,
      TeamBID: matchData.miniScoreCard.data[0].matchScore[1].teamID,
      MatchFormat: data.miniScoreCard.data[0].matchType,
      MatchStartTime: format(
        Number(matchData.miniScoreCard.data[0].startDate),
        'do MMM yyyy, h:mm a',
      ),
      MatchStatus: matchData.miniScoreCard.data[0].matchStatus,
      CriclyticsEngine:
        matchData.miniScoreCard.data[0].matchStatus === 'live' &&
        routerTabName === 'team-score-projection'
          ? 'LiveTeam'
          : matchData.miniScoreCard.data[0].matchStatus === 'live' &&
            routerTabName === 'live-player-projection'
          ? 'LivePlayer'
          : matchData.miniScoreCard.data[0].matchStatus === 'live' &&
            routerTabName === 'match-ups'
          ? 'LiveMatchups'
          : matchData.miniScoreCard.data[0].matchStatus === 'live' &&
            routerTabName === 'over-simulator'
          ? 'LiveDOS'
          : matchData.miniScoreCard.data[0].matchStatus === 'live' &&
            routerTabName === 'phases-of-play'
          ? 'LivePOP'
          : matchData.miniScoreCard.data[0].matchStatus === 'upcoming' &&
            routerTabName === 'key-stats'
          ? 'ScheduledStats'
          : matchData.miniScoreCard.data[0].matchStatus === 'upcoming' &&
            routerTabName === 'player-projection'
          ? 'ScheduledPlayer'
          : matchData.miniScoreCard.data[0].matchStatus === 'upcoming' &&
            routerTabName === 'match-ups'
          ? 'ScheduledMatchups'
          : matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            routerTabName === 'match-stats'
          ? 'CompletedMS'
          : matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            routerTabName === 'phases-of-play'
          ? 'CompletedPOP'
          : matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            routerTabName === 'game-changing-overs'
          ? 'CompletedGCO'
          : matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            routerTabName === 'match-ratings'
          ? 'MatchRatings'
          : matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            routerTabName === 'match-reel'
          ? 'CompletedMR'
          : matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            routerTabName === 'scoring-charts'
          ? 'CompletedManhattan'
          : matchData.miniScoreCard.data[0].matchStatus === 'live' &&
            routerTabName === 'partnership-predictor'
          ? 'PartnershipPredictor'
          : '',
      Platform: localStorage ? localStorage.Platform : '',
    })
  }

  // dont dare to change function , if you really want please change then please check Constant/Link.js file.
  const getUrl = (criclyticName, tab, matchId) => {
    tab = tab.split(' ').join('-').toLowerCase()
    return { as: eval(CRYCLYTICS_VIEW.as), href: CRYCLYTICS_VIEW.href }
  }
  function resolveTab() {
    switch (routerTabName) {
      case CRYCLYTICS_KEYS.keyStatsKey:
        return (
          <KeyStats
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.projectionKey:
        return (
          <PlayerProjection
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.matchupKey:
        return (
          <MatchUps
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        )

      case CRYCLYTICS_KEYS.partnerShip:
        return (
          <Partner
            matchID={matchID}
            data={partnershipDataMatchId?.data}
            {...props}
          />
        )

      case CRYCLYTICS_KEYS.statsKey:
        return (
          <MatchStats
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        )

      case CRYCLYTICS_KEYS.scoringcharts:
        return (
          <ScoringChart
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        )

      case CRYCLYTICS_KEYS.phasesKey:
        return matchData.miniScoreCard.data[0].matchType === 'Test' ? (
          <PhasesOfPlayTest
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        ) : (
          <PhasesOfPlay
            matchData={matchData.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.gameChangingKey:
        return (
          <GameChangingOvers
            matchData={matchData?.miniScoreCard.data}
            matchID={matchID}
            data={keyMatchStateData?.data}
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.predictionKey:
        return (
          <LiveScorePredictorODI
            matchData={matchData?.miniScoreCard.data}
            matchID={matchID}
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.liveprojectionKey:
        return (
          <LivePlayerProjection
            isActive={true}
            matchData={matchData?.miniScoreCard.data}
            matchID={matchID}
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.reelKey:
        return (
          <LiveScorePredictorODI
            matchData={matchData?.miniScoreCard.data}
            path="matchreel"
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.rattingKey:
        return (
          <MatchRatings
            matchData={matchData?.miniScoreCard.data}
            path="matchratings"
            {...props}
          />
        )
      case CRYCLYTICS_KEYS.simulatorKey:
        return (
          <DeathOverSimulator
            isActive={true}
            matchData={matchData?.miniScoreCard?.data}
            matchID={matchID}
            {...props}
          />
        )
    }
  }

  const renderRecentSeriesTab = () => {
    switch (routerTabName) {
      case CRYCLYTICS_KEYS.finalFourKey:
        return (
          <>
            <MetaDescriptor
              section={CRICLYTICS_PHASE(
                {
                  seriesName:
                    iplData.featurematch[0].criclyticsButtonFlags
                      .featuredSeriesName,
                },
                router.asPath,
                routerTabName,
                criclyticName,
              )}
            />
            <Finalfour
              path="finalfour"
              browser={browser}
              series={
                iplData.data.featurematch[0].criclyticsButtonFlags
                  .featuredSeriesName
              }
            />
          </>
        )
      case CRYCLYTICS_KEYS.valuablePlayerKey:
        return (
          <>
            <MetaDescriptor
              section={CRICLYTICS_PHASE(
                { seriesName: criclyticName },
                router.asPath,
                routerTabName,
                criclyticName,
              )}
            />
            <Playerindex path="mostvaluableplayer" tourID={matchID} />
          </>
        )
    }
  }
  // if (error)
  //   return (
  //     <div className="w-100 vh-100 fw2 f7 gray flex flex-col  justify-center items-center">
  //       <span>Something isn't right!</span>
  //       <span>Please refresh and try again...</span>
  //     </div>
  //   )
  if (matchData && matchData.length === 0) return <Loading />
  else if (
    matchData &&
    matchData['miniScoreCard'] &&
    routerTabName !== 'final-four' &&
    routerTabName !== 'most-valuable-player'
  ) {
    return (
      <>
        {/* upcomig desktop sytart */}

        <div className="xl:pt-2 lg:pt-2 md:pt-2 hidden lg:block md:block ">
          {' '}
          {params?.slugs &&
            params?.slugs &&
            (matchData?.miniScoreCard.data[0].matchStatus === 'upcoming' ||
              (matchData.miniScoreCard.data[0].matchStatus == 'live' &&
                !matchData.miniScoreCard.data[0]
                  .isLiveCriclyticsAvailable)) && (
              <>
                <div className="flex w-full flex-wrap items-center justify-start gap-3 mx-3">
                  {[
                    {
                      tabName: <>Player Matchup</>,
                      key: 'player-matchup',
                      img: matchupImg,
                    },
                    {
                      tabName: <>Form Index</>,
                      key: 'form-index',
                      img: formInd,
                    },
                    {
                      tabName: <>Key Stats</>,
                      key: 'Key-Stat',
                      img: keyStatImg,
                    },
                  ].map((item, i) => {
                    return (
                      <Link
                        key={i}
                        {...getUrl(criclyticName, item.key, matchID)}
                        replace
                        passHref
                        legacyBehavior
                      >
                        <div
                          onClick={() => setActiveSwiper(indexTell())}
                          className={`${
                            tabName.toLowerCase() == item.key.toLowerCase()
                              ? 'border-b-3 border-b-green border-green border-r-2 border-r-[#707070] border-l-2 border-l-[#707070] border-t-[#707070] border-t-2'
                              : 'border-2 border-solid lg:border-[#E2E2E2]'
                          }  cursor-pointer my-4 rounded-xl h-32 w-32 dark:bg-gray bg-white mb-3 p-3`}
                        >
                          <div className="w-full flex items-center justify-center cursor-pointer">
                            {' '}
                            <img className="h-16 w-16" src={item.img} />{' '}
                          </div>
                          <div className=" w-full text-center dark:text-white text-black text-xs font-semibold pt-2 capitalize">
                            {item.tabName}
                          </div>
                        </div>
                      </Link>
                    )
                  })}
                </div>

                <div className="">
                  <UpcomingDesktop
                    activeIndex={activeSwiper && activeSwiper}
                    matchID={matchID}
                    keystat={keyMatchStateData?.getKeyStats}
                    data={
                      data &&
                      data.miniScoreCard &&
                      data.miniScoreCard.data &&
                      data.miniScoreCard.data[0]
                    }
                    matchType={
                      data &&
                      data.miniScoreCard &&
                      data.miniScoreCard.data &&
                      data.miniScoreCard.data[0].matchType
                    }
                    routerTabName={routerTabName}
                  />
                </div>
              </>
            )}
        </div>
        {/* upcomig desktop end */}

        {/* live MATch */}
        <div className="lg:pt-2 xl:pt-2 md:pt-2 hidden lg:block md:block ">
          {params?.slugs &&
          params?.slugs.length &&
          matchData.miniScoreCard.data[0].isLiveCriclyticsAvailable &&
          matchData.miniScoreCard.data[0].matchStatus === 'live' ? (
            matchData.miniScoreCard.data[0].matchType !== 'Test' ? (
              <div className="flex flex-wrap items-center justify-between lg:justify-start lg:gap-3 mx-3 ">
                {[
                  {
                    tabName: <>Over Simulation</>,
                    key: 'over-simulation',
                    img: OverSimulatorImg,
                  },
                  {
                    tabName: <>Run Comparison</>,
                    key: 'run-comparison',
                    img: runImg,
                  },
                  {
                    tabName: <>Score Projection</>,
                    key: 'score-projection',
                    img: scoreproj,
                  },
                  {
                    tabName: <>Player MatchUp</>,
                    key: 'player-matchUp',
                    img: matchupImg,
                  },

                  {
                    tabName: <>Form Index</>,
                    key: 'form-index',
                    img: formInd,
                  },
                  {
                    tabName: <>Phases of Play</>,
                    key: 'phases-of-play',
                    img: phaseofPlayImg,
                  },
                ].map((item, i) => {
                  return (
                    <Link
                      key={i}
                      {...getUrl(criclyticName, item.key, matchID)}
                      replace
                      passHref
                      legacyBehavior
                    >
                      <div
                        onClick={() => setActiveSwiper(indexTell())}
                        className={`${
                          tabName.toLowerCase() == item.key.toLowerCase()
                            ? 'border-b-3 border-b-green dark:border-b-green border-l-[#707070] border-r-[#707070] border-t-[#707070]'
                            : 'lg:border-[#E2E2E2] border-2 border-solid '
                        }  cursor-pointer rounded-xl h-32 w-32 dark:bg-gray bg-white mb-3 p-3  `}
                      >
                        <div className="w-full flex items-center justify-center cursor-pointer ">
                          {' '}
                          <img className="h-16 w-16" src={item.img} />{' '}
                        </div>
                        <div className="  w-full text-center text-black dark:text-white text-xs font-semibold pt-2 capitalize">
                          {item.tabName}
                        </div>
                      </div>
                    </Link>
                  )
                })}
                <LiveDsktop
                  activeIndex={activeSwiper && activeSwiper}
                  matchID={matchID}
                  keystat={keyMatchStateData?.getKeyStats}
                  data={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0]
                  }
                  matchType={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0].matchType
                  }
                  routerTabName={routerTabName}
                />
              </div>
            ) : (
              <div className="flex flex-wrap items-center justify-between lg:justify-start lg:gap-3 mx-3 ">
                {[
                  // {
                  //   tabName: <>Over Simulation</>,
                  //   key: 'over-simulation',
                  //   img: OverSimulatorImg,
                  // },
                  // {
                  //   tabName: <>Run Comparison</>,
                  //   key: 'run-comparison',
                  //   img: runImg,
                  // },
                  {
                    tabName: <>Score Projection</>,
                    key: 'score-projection',
                    img: scoreproj,
                  },
                  {
                    tabName: <>Player MatchUp</>,
                    key: 'player-matchUp',
                    img: matchupImg,
                  },

                  {
                    tabName: <>Form Index</>,
                    key: 'form-index',
                    img: formInd,
                  },
                  {
                    tabName: <>Phases of Play</>,
                    key: 'phases-of-play',
                    img: phaseofPlayImg,
                  },
                ].map((item, i) => {
                  return (
                    <Link
                      key={i}
                      {...getUrl(criclyticName, item.key, matchID)}
                      replace
                      passHref
                      legacyBehavior
                    >
                      <div
                        onClick={() => setActiveSwiper(indexTell())}
                        className={`${
                          tabName.toLowerCase() == item.key.toLowerCase()
                            ? 'border-b-3 border-b-green dark:border-b-green border-l-[#707070] border-r-[#707070] border-t-[#707070]'
                            : 'lg:border-[#E2E2E2] border-2 border-solid '
                        }  cursor-pointer rounded-xl h-32 w-32 dark:bg-gray bg-white mb-3 p-3  `}
                      >
                        <div className="w-full flex items-center justify-center cursor-pointer ">
                          {' '}
                          <img className="h-16 w-16" src={item.img} />{' '}
                        </div>
                        <div className="  w-full text-center text-black dark:text-white text-xs font-semibold pt-2 capitalize">
                          {item.tabName}
                        </div>
                      </div>
                    </Link>
                  )
                })}
                <LiveDsktop
                  activeIndex={activeSwiper && activeSwiper}
                  matchID={matchID}
                  keystat={keyMatchStateData?.getKeyStats}
                  data={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0]
                  }
                  matchType={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0].matchType
                  }
                  routerTabName={routerTabName}
                />
              </div>
            )
          ) : null}
        </div>

        {/* end sss match  */}
        <div className="md:pt-2 lg:pt-2 xl:pt-2 hidden lg:block md:block">
          {' '}
          {params?.slugs &&
            params?.slugs &&
            matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            (routerTabName !== 'final-four' ||
              routerTabName !== 'most-valuable-player') &&
            matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
            (matchData.miniScoreCard.data[0].matchType !== 'Test' ? (
              <>
                <div className="flex w-full  flex-wrap items-center justify-between mx-3 lg:justify-start lg:gap-3 cursor-pointer ">
                  {[
                    {
                      tabName: <>Players Impact</>,
                      key: 'report-card',
                      img: reportImg,
                    },
                    {
                      tabName: <>Run Comparison</>,
                      key: 'run-comparison',
                      img: runImg,
                    },
                    {
                      tabName: <>Game Changing Overs</>,
                      key: 'game-change-overs',
                      img: gameChangingImg,
                    },
                    {
                      tabName: <>Match Reel</>,
                      key: 'match-reel',
                      img: matchReelImg,
                    },

                    {
                      tabName: <>Match Stats</>,
                      img: matchStatImg,
                      key: 'match-stat',
                    },
                    {
                      tabName: <>Phases of Play</>,
                      key: 'Phases-of-Play',
                      img: phaseofPlayImg,
                    },
                  ].map((item, i) => {
                    return (
                      <Link
                        key={i}
                        {...getUrl(criclyticName, item.key, matchID)}
                        replace
                        passHref
                        legacyBehavior
                      >
                        <div
                          onClick={() => setActiveSwiper(indexTell())}
                          className={`${
                            tabName.toLowerCase() == item.key.toLowerCase()
                              ? 'border-b-3 border-green border-b-green border-r-2 border-r-[#707070] border-l-2 border-l-[#707070] border-t-[#707070] border-t-2'
                              : 'border-2 border-solid lg:border-[#E2E2E2]'
                          }  cursor-pointer  rounded-xl h-32 w-32 dark:bg-gray bg-white mb-3 p-3  `}
                        >
                          <div className="w-full flex items-center justify-center cursor-pointer">
                            {' '}
                            <img className="h-16 w-16" src={item.img} />{' '}
                          </div>
                          <div className=" text-black w-full text-center dark:text-white text-xs font-semibold pt-2 capitalize">
                            {item.tabName}
                          </div>
                        </div>
                      </Link>
                    )
                  })}
                </div>
                <CompleteComponent
                  activeIndex={activeSwiper && activeSwiper}
                  matchID={matchID}
                  keystat={keyMatchStateData?.getKeyStats}
                  data={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0]
                  }
                  matchType={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0].matchType
                  }
                  routerTabName={routerTabName}
                  params={params}
                />
              </>
            ) : (
              <>
                <div className="flex flex-wrap items-center justify-between mx-3 lg:gap-3 lg:justify-start ">
                  {[
                    // {
                    //   tabName: <>Players Impact</>,
                    //   key: 'report-card',
                    //   img: reportImg,
                    // },

                    {
                      tabName: <>Match Reel</>,
                      key: 'Match-Reel',
                      img: matchReelImg,
                    },

                    {
                      tabName: <>Phases of Play</>,
                      key: 'Phases-of-Play',
                      img: phaseofPlayImg,
                    },
                  ].map((item, i) => {
                    return (
                      <Link
                        key={i}
                        {...getUrl(criclyticName, item.key, matchID)}
                        replace
                        passHref
                        legacyBehavior
                      >
                        <div
                          onClick={() => setActiveSwiper(indexTell())}
                          className={`${
                            tabName.toLowerCase() == item.key.toLowerCase()
                              ? 'border-b-3 cursor-pointer border-b-green border-r-2 border-r-[#707070] border-l-2 border-l-[#707070] border-t-[#707070] border-t-2 border-green'
                              : 'border-2 border-solid lg:border-[#E2E2E2]'
                          }
                            rounded-xl h-32 w-32 dark:bg-gray bg-white mb-3 p-3 cursor-pointer `}
                        >
                          <div className="w-full flex items-center justify-center ">
                            {' '}
                            <img className="h-16 w-16" src={item.img} />{' '}
                          </div>
                          <div className="w-full text-center dark:text-white text-black text-xs font-semibold pt-2 capitalize">
                            {item.tabName}
                          </div>
                        </div>
                      </Link>
                    )
                  })}
                </div>
                <CompleteComponent
                  activeIndex={activeSwiper && activeSwiper}
                  matchID={matchID}
                  keystat={keyMatchStateData?.getKeyStats}
                  data={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0]
                  }
                  matchType={
                    matchData &&
                    matchData.miniScoreCard &&
                    matchData.miniScoreCard.data &&
                    matchData.miniScoreCard.data[0].matchType
                  }
                  routerTabName={routerTabName}
                  params={params}
                />
              </>
            ))}
        </div>
        {console.log('previouspateh', params, 'ppppppppp', prevPath)}
        <div className="md:mt-10 lg:hidden md:hidden">
          <>
            {params?.slugs?.length > 2 && prevPath !== '/' ? (
              <Link
                // {...getCriclyticdUrlHome(matchData?.miniScoreCard?.data?.[0])}
                href={prevPath}
                passHref
                legacyBehavior
              >
                <div className="fixed top-0 bg-basebg w-full px-3 py-2 z-10 ">
                  <div className="flex items-center justify-start ">
                    <div className="p-3 bg-gray rounded-md ">
                      <img
                        className="h-4 w-4 rotate-180"
                        src="/svgsV2/RightSchevronWhite.svg"
                        alt=""
                      />
                    </div>
                    <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
                      <div className="">Criclytics</div>
                      <span className="text-[8px] -mt-2">TM</span>
                    </div>{' '}
                  </div>
                </div>
              </Link>
            ) : prevPath === '/' ? (
              <Link href={'/'} passHref legacyBehavior>
                <div className="fixed top-0 bg-basebg w-full px-3 py-2 z-10 ">
                  <div className="flex items-center justify-start ">
                    <div className="p-3 bg-gray rounded-md">
                      <img
                        className="h-4 w-4 rotate-180"
                        src="/svgsV2/RightSchevronWhite.svg"
                        alt=""
                      />
                    </div>
                    <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
                      <div>Criclytics</div>
                      <span className="text-[8px] -mt-2">TM</span>
                    </div>
                  </div>
                </div>
              </Link>
            ) : prevPath === '/schedule' ? (
              <Link href={'/schedule/live-matches'} passHref legacyBehavior>
                <div className="fixed top-0 bg-basebg w-full px-3 py-2 z-10 ">
                  <div className="flex items-center justify-start ">
                    <div className="p-3 bg-gray rounded-md">
                      <img
                        className="h-4 w-4 rotate-180"
                        src="/svgsV2/RightSchevronWhite.svg"
                        alt=""
                      />
                    </div>
                    <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
                      <div>Criclytics</div>
                      <span className="text-[8px] -mt-2">TM</span>
                    </div>
                  </div>
                </div>
              </Link>
            ) : (
              <Link href={'/criclytics'} passHref legacyBehavior>
                <div className="fixed top-0 bg-basebg w-full px-3 py-2 z-10 ">
                  <div className="flex items-center justify-start ">
                    <div className="p-3 bg-gray rounded-md">
                      <img
                        className="h-4 w-4 rotate-180"
                        src="/svgsV2/RightSchevronWhite.svg"
                        alt=""
                      />
                    </div>
                    <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
                      <div>Criclytics</div>
                      <span className="text-[8px] -mt-2">TM</span>
                    </div>
                  </div>
                </div>
              </Link>
            )}
          </>

          <div>
            {/* {props.urlTrack ? <Urltracking PageName='Criclytics' /> : null} */}
            {console.log(matchData, 'matchDataaaa')}
            <div className="mt-3">
              {params?.slugs && params?.slugs.length == 2 && (
                <div className="mx-2 mb-4 ">
                  <UpcomingMiniScoreCard
                    winningTeamID={
                      matchData?.miniScoreCard?.data?.[0]?.winningTeamID
                    }
                    matchData={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0]
                    }
                    status={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0] &&
                      matchData.miniScoreCard.data[0].matchStatus
                    }
                    routerTabName={routerTabName}
                  />
                </div>
              )}
              {params?.slugs &&
              params?.slugs.length == 2 &&
              matchData.miniScoreCard.data[0].matchStatus === 'live' &&
              matchData.miniScoreCard.data[0].isLiveCriclyticsAvailable &&
              matchData.miniScoreCard.data[0].matchStatus === 'live' ? (
                matchData.miniScoreCard.data[0].matchType !== 'Test' ? (
                  <>
                    <div className="flex flex-wrap items-center lg:justify-start lg:gap-3 justify-between mx-3 ">
                      {[
                        {
                          tabName: <>Over Simulation</>,
                          key: 'over-simulation',
                          img: OverSimulatorImg,
                        },
                        {
                          tabName: <>Run Comparison</>,
                          key: 'run-comparison',
                          img: runImg,
                        },
                        {
                          tabName: <>Score Projection</>,
                          key: 'score-projection',
                          img: scoreproj,
                        },
                        {
                          tabName: <>Player MatchUp</>,
                          key: 'player-matchUp',
                          img: matchupImg,
                        },

                        {
                          tabName: <>Form Index</>,
                          key: 'form-index',
                          img: formInd,
                        },
                        {
                          tabName: <>Phases of Play</>,
                          key: 'phases-of-play',
                          img: phaseofPlayImg,
                        },
                      ].map((item, i) => {
                        return (
                          <Link
                            key={i}
                            {...getUrl(criclyticName, item.key, matchID)}
                            replace
                            passHref
                            legacyBehavior
                          >
                            <div className=" w-[46%] h-32  p-3  rounded-lg  bg-gray mb-3  ">
                              <div className="flex items-center justify-end">
                                {' '}
                                <img
                                  className="h-16 w-16"
                                  src={item.img}
                                />{' '}
                              </div>
                              <div className=" text-white text-sm font-semibold mt-2">
                                {' '}
                                {item.tabName}{' '}
                              </div>
                            </div>
                          </Link>
                        )
                      })}
                    </div>
                  </>
                ) : (
                  <>
                    <div className="flex flex-wrap items-center lg:justify-start lg:gap-3 justify-between mx-3 ">
                      {[
                        // {
                        //   tabName: <>Over Simulation</>,
                        //   key: 'over-simulation',
                        //   img: OverSimulatorImg,
                        // },
                        // {
                        //   tabName: <>Run Comparison</>,
                        //   key: 'run-comparison',
                        //   img: runImg,
                        // },
                        {
                          tabName: <>Score Projection</>,
                          key: 'score-projection',
                          img: scoreproj,
                        },
                        {
                          tabName: <>Player MatchUp</>,
                          key: 'player-matchUp',
                          img: matchupImg,
                        },

                        {
                          tabName: <>Form Index</>,
                          key: 'form-index',
                          img: formInd,
                        },
                        {
                          tabName: <>Phases of Play</>,
                          key: 'phases-of-play',
                          img: phaseofPlayImg,
                        },
                      ].map((item, i) => {
                        return (
                          <Link
                            key={i}
                            {...getUrl(criclyticName, item.key, matchID)}
                            replace
                            passHref
                            legacyBehavior
                          >
                            <div className=" w-[46%] h-32  p-3  rounded-lg  bg-gray mb-3  ">
                              <div className="flex items-center justify-end">
                                {' '}
                                <img
                                  className="h-16 w-16"
                                  src={item.img}
                                />{' '}
                              </div>
                              <div className=" text-white text-sm font-semibold mt-2">
                                {' '}
                                {item.tabName}{' '}
                              </div>
                            </div>
                          </Link>
                        )
                      })}
                    </div>
                  </>
                )
              ) : null}

              {/* {params?.slugs &&
                params?.slugs.length == 2 &&
                matchData.miniScoreCard.data[0].matchStatus === 'live' &&
                matchData.miniScoreCard.data[0].isLiveCriclyticsAvailable && (
                  <div className="flex flex-wrap items-center lg:justify-start lg:gap-3 justify-between mx-3 ">
                    {[
                      {
                        tabName: <>Over Simulation</>,
                        key: 'over-simulation',
                        img: OverSimulatorImg,
                      },
                      {
                        tabName: <>Run Comparison</>,
                        key: 'run-comparison',
                        img: runImg,
                      },
                      {
                        tabName: <>Score Projection</>,
                        key: 'score-projection',
                        img: scoreproj,
                      },
                      {
                        tabName: <>Player MatchUp</>,
                        key: 'player-matchUp',
                        img: matchupImg,
                      },

                      {
                        tabName: <>Form Index</>,
                        key: 'form-index',
                        img: formInd,
                      },
                      {
                        tabName: <>Phases of Play</>,
                        key: 'phases-of-play',
                        img: phaseofPlayImg,
                      },
                    ].map((item, i) => {
                      return (
                        <Link
                          key={i}
                          {...getUrl(criclyticName, item.key, matchID)}
                          replace
                          passHref
                          legacyBehavior
                        >
                          <div className=" w-[46%] h-32  p-3  rounded-lg  bg-gray mb-3  ">
                            <div className="flex items-center justify-end">
                              {' '}
                              <img className="h-16 w-16" src={item.img} />{' '}
                            </div>
                            <div className=" text-white text-sm font-semibold mt-2">
                              {' '}
                              {item.tabName}{' '}
                            </div>
                          </div>
                        </Link>
                      )
                    })}
                  </div>
                )} */}
              {/* -----------------Upcoming Block starts----------------- */}

              {params?.slugs &&
                params?.slugs.length == 2 &&
                (matchData.miniScoreCard.data[0].matchStatus === 'upcoming' ||
                  (matchData.miniScoreCard.data[0].matchStatus == 'live' &&
                    !matchData.miniScoreCard.data[0]
                      .isLiveCriclyticsAvailable)) && (
                  <div className="flex flex-wrap items-center justify-between mx-3 lg:justify-start lg:gap-3  ">
                    {[
                      {
                        tabName: <>Player Matchup</>,
                        key: 'Player Matchup',
                        img: matchupImg,
                      },
                      {
                        tabName: <>Form Index</>,
                        key: 'Form Index',
                        img: formInd,
                      },
                      {
                        tabName: <>Key Stats</>,
                        key: 'Key Stat',
                        img: keyStatImg,
                      },
                    ].map((item, i) => {
                      return (
                        <Link
                          key={i}
                          {...getUrl(criclyticName, item.key, matchID)}
                          replace
                          passHref
                          legacyBehavior
                        >
                          <div
                            onClick={() => setActiveSwiper(indexTell())}
                            className=" h-32 w-[46%]  rounded-lg  bg-gray mb-3 p-3  "
                          >
                            <div className="flex items-center justify-end">
                              <img className="h-20 w-20" src={item.img} />{' '}
                            </div>

                            <div className=" text-white text-sm font-semibold mt-2 capitalize">
                              {' '}
                              {item.tabName}{' '}
                            </div>
                          </div>
                        </Link>
                      )
                    })}
                  </div>
                )}

              {/* -----------------Upcoming Block Ends----------------- */}

              {/* -----------------------completed match criclytics------------------------- */}

              {params?.slugs &&
              params?.slugs.length == 2 &&
              matchData.miniScoreCard.data[0].matchStatus === 'completed' &&
              (routerTabName !== 'final-four' ||
                routerTabName !== 'most-valuable-player') &&
              matchData.miniScoreCard.data[0].matchStatus === 'completed' ? (
                matchData.miniScoreCard.data[0].matchType !== 'Test' ? (
                  <>
                    <div className="flex md:hidden flex-wrap items-center justify-around mx-3">
                      {[
                        {
                          tabName: <>Players Impact</>,
                          key: 'report card',
                          img: reportImg,
                        },
                        {
                          tabName: <>Run Comparison</>,
                          key: 'Run Comparison',
                          img: runImg,
                        },
                        {
                          tabName: <>Game Change Overs</>,
                          key: 'Game Change Overs',
                          img: gameChangingImg,
                        },
                        {
                          tabName: <>Match Reel</>,
                          key: 'Match-Reel',
                          img: matchReelImg,
                        },

                        {
                          tabName: <>Match Stat</>,
                          img: matchStatImg,
                          key: 'Match Stat',
                        },
                        {
                          tabName: <>Phases of Play</>,
                          key: 'Phases of Play',
                          img: phaseofPlayImg,
                        },
                      ].map((item, i) => {
                        return (
                          <Link
                            key={i}
                            {...getUrl(criclyticName, item.key, matchID)}
                            replace
                            passHref
                            legacyBehavior
                          >
                            <div
                              onClick={() => setActiveSwiper(indexTell())}
                              className="h-32 w-[46%]  rounded-lg  bg-gray mb-3 p-3  "
                            >
                              <div className="w-full flex items-center justify-end ">
                                {' '}
                                <img
                                  className="h-20 w-20"
                                  src={item.img}
                                />{' '}
                              </div>
                              <div className="  w-full text-white text-sm font-semibold pt-2 capitalize">
                                {' '}
                                {item.tabName}{' '}
                              </div>
                            </div>
                          </Link>
                        )
                      })}
                    </div>
                  </>
                ) : (
                  <>
                    <div className="flex flex-wrap items-center lg:justify-start justify-between mx-3 lg:gap-3  ">
                      {[
                        // {
                        //   tabName: <>Players Impact</>,
                        //   key: 'report card',
                        //   img: reportImg,
                        // },

                        {
                          tabName: <>Match Reel</>,
                          key: 'Match-Reel',
                          img: matchReelImg,
                        },

                        {
                          tabName: <>Phases of Play</>,
                          key: 'Phases of Play',
                          img: phaseofPlayImg,
                        },
                      ].map((item, i) => {
                        return (
                          <Link
                            key={i}
                            {...getUrl(criclyticName, item.key, matchID)}
                            replace
                            passHref
                            legacyBehavior
                          >
                            <div
                              onClick={() => setActiveSwiper(indexTell())}
                              className=" h-32 w-[46%]  rounded-lg  bg-gray mb-3 p-3   "
                            >
                              <div className="w-full flex items-center justify-end ">
                                {' '}
                                <img
                                  className="h-20 w-20"
                                  src={item.img}
                                />{' '}
                              </div>
                              <div className="  w-full text-white text-sm font-semibold pt-2 capitalize">
                                {' '}
                                {item.tabName}{' '}
                              </div>
                            </div>
                          </Link>
                        )
                      })}
                    </div>
                  </>
                )
              ) : null}
            </div>
            {/* -----------------------completed match criclytics Enddd------------------------- */}

            <MetaDescriptor
              section={CRICLYTICS_PHASE(
                matchData.miniScoreCard.data[0],
                router.asPath,
                routerTabName,
              )}
            />
            {routerTabName &&
              params?.slugs &&
              params?.slugs.length === 3 &&
              matchData.miniScoreCard.data[0].matchStatus === 'live' &&
              matchData.miniScoreCard.data[0].isLiveCriclyticsAvailable && (
                <div className="mt-14">
                  <LiveSwiperCompleted
                    activeIndex={activeSwiper && activeSwiper}
                    matchID={matchID}
                    keystat={keyMatchStateData?.getKeyStats}
                    data={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0]
                    }
                    matchType={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0].matchType
                    }
                    routerTabName={routerTabName}
                  />
                </div>
              )}

            {routerTabName &&
              params?.slugs &&
              params?.slugs.length === 3 &&
              (matchData.miniScoreCard.data[0].matchStatus === 'upcoming' ||
                (matchData.miniScoreCard.data[0].matchStatus === 'live' &&
                  !matchData.miniScoreCard.data[0]
                    .isLiveCriclyticsAvailable)) && (
                <div className="mt-14">
                  <UpcomingSwipe
                    activeIndex={activeSwiper && activeSwiper}
                    matchID={matchID}
                    keystat={keyMatchStateData?.getKeyStats}
                    data={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0]
                    }
                    matchType={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0].matchType
                    }
                    routerTabName={routerTabName}
                  />
                </div>
              )}

            {routerTabName &&
              params?.slugs &&
              params?.slugs.length === 3 &&
              matchData.miniScoreCard.data[0].matchStatus === 'completed' && (
                <div className="mt-14">
                  <SwiperCompleted
                    activeIndex={activeSwiper && activeSwiper}
                    matchID={matchID}
                    keystat={keyMatchStateData?.getKeyStats}
                    data={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0]
                    }
                    matchType={
                      matchData &&
                      matchData.miniScoreCard &&
                      matchData.miniScoreCard.data &&
                      matchData.miniScoreCard.data[0].matchType
                    }
                    routerTabName={routerTabName}
                    params={params}
                  />
                </div>
              )}
          </div>
        </div>
      </>
    )
  } else if (
    iplData &&
    (routerTabName === 'final-four' || routerTabName === 'most-valuable-player')
  ) {
    return (
      <div className="mw75-l center">
        {routerTabName && renderRecentSeriesTab()}
      </div>
    )
  } else if (routerTabName && routerTabName === 'qualification-probability') {
    return (
      <div className="flex w-full ">
        {' '}
        <QualificationProb matchID={matchID} />{' '}
      </div>
    )
  }
  return <></>
}

export const getStaticPaths = async () => {
  const HOMEPAGE = await axios.post(process.env.API, {
    query: GET_FRC_HOME_PAGE_AXIOS,
  })

  async function homePageAxios() {
    await new Promise((res) => setTimeout(res, 3000))

    const res = await axios.post(process.env.API, {
      query: GET_FRC_HOME_PAGE_AXIOS,
    })

    return res.data
  }

  // export default async function page({ params }) {
  //   const router = useRouter()
  //   console.log(router, 'home')

  //   console.log(params, 'opopop')

  //   return (
  //     <div>
  //       <h3 className="text-white">jkjkjk</h3>
  //     </div>
  //   )
  // }

  let data = HOMEPAGE.data.data

  let playerPaths =
    data && data.getFRCHomePage && data.getFRCHomePage.completedmatches
      ? data.getFRCHomePage.completedmatches.map((item) => {
          return {
            params: {
              id: [
                item.matchID.toString(),
                item.matchName
                  .replace(/[^a-zA-Z0-9]+/g, ' ')
                  .split(' ')
                  .join('-')
                  .toLowerCase(),
              ],
            },
          }
        })
      : []

  return {
    paths: [...playerPaths],
    fallback: true,
  }
}
