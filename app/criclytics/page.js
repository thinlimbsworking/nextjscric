'use client'
import React, { useEffect, useState } from 'react'
// import SwipeableViews from 'react-swipeable-views';
// import { autoPlay } from 'react-swipeable-views-utils';
import axios from 'axios'
import Link from 'next/link'
import { usePathname, useRouter } from 'next/navigation'
import { getCriclyticsUrlHome } from '../../api/services'
import CleverTap from 'clevertap-react'
import { format } from 'date-fns'
import { CRICLYTICS } from '../../constant/MetaDescriptions'
import DataNotFound from '../components/commom/datanotfound'
import MetaDescriptor from '../components/commom/metadecription'
import { useQuery } from '@apollo/react-hooks'
// import Urltracking from '../../components/Common/urltracking';

const location = './pngsV2/location.png'

import { CRICLYTICS_MATCHES, CRICLYTICS_MATCHES_AXIOS } from '../../api/queries'
import Scores from '../components/commom/score'
import Tab from '../components/shared/Tab'
import Heading from '../components/commom/heading'
import Loading from '../components/loading'
/**
 * @description Criclytics listing
 * @route /criclytics
 * @param { Object } props
 */
// async function getFeaturedMatches() {
//   const res = await axios.post(process.env.API, {
//     query: CRICLYTICS_MATCHES_AXIOS,
//     variables: { type: 'latest' },
//   })
//
//   return res.data.data
// }
const TabData = [
  { name: 'UPCOMING', value: 'upcoming' },
  { name: 'LIVE', value: 'live' },
  { name: 'COMPLETED', value: 'completed' },
]
export default function Criclytics({ browser, ...props }) {
  //
  const [cleverTapObject, updateClevertap] = useState(null)
  const [windowObject, updateWindowObject] = useState(null)

  const [currentTab, setCurrentTab] = useState(TabData[1])
  // const [currentTab, setCurrentTab] = useState(
  //   data?.getMatchesForCriclytics?.live?.length == 0 ? TabData[0] : TabData[1],
  //   )

  const { loading, error, data } = useQuery(CRICLYTICS_MATCHES, {
    variables: { type: 'latest' },
  })
  const pathname = usePathname()

  // useEffect(() => {
  //   // updateClevertap({ ...CleverTap })
  //   // updateWindowObject({ ...global.window })
  //   getFeaturedMatches()
  //     .then((res) => {
  //       setFeaturedMatches(res)
  //     })
  //     .catch((err) => )
  // }, [])
  useEffect(() => {
    if (data?.getMatchesForCriclytics?.live?.length == 0) {
      setCurrentTab(TabData[0])
    }
  }, [data])
  useEffect(() => {
    // if (cleverTapObject) {
    //   cleverTapObject?.event('Criclytics', {
    //     Source: 'CriclyticsHome',
    //     Platform: localStorage.Platform,
    //   })
    // }
  }, [cleverTapObject, windowObject])
  // const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
  //   const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
  const router = useRouter()
  const navigate = router.push
  const [index, setIndex] = useState(0)
  let localStorage = {}
  const handleCriclyticsNav = (card) => {
    CleverTap.initialize('Criclytics', {
      Source: 'CriclyticsHome',
      MatchID: card.matchID,
      SeriesID: card.seriesID,
      TeamAID: card.matchScore[0].teamID,
      TeamBID: card.matchScore[1].teamID,
      MatchFormat: card.matchType,
      MatchStartTime: format(Number(card.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: card.matchStatus,
      CriclyticsEngine:
        card.matchStatus === 'live'
          ? 'LiveTeam'
          : card.matchStatus === 'completed' && card.matchType !== 'Test'
          ? 'CompletedMS'
          : card.matchStatus === 'completed' && card.matchType === 'Test'
          ? 'CompletedMR'
          : card.matchStatus === 'upcoming'
          ? 'ScheduledStats'
          : '',
      Platform: localStorage.Platform,
    })
  }

  const getCriclyticdUrlHome = (match) => {
    let tab =
      match.matchStatus === 'completed'
        ? match.matchType !== 'Test'
          ? ''
          : ''
        : match.matchStatus === 'live' && match.isLiveCriclyticsAvailable
        ? ''
        : match.matchStatus === 'upcoming' ||
          (match.matchStatus === 'live' && !match.isLiveCriclyticsAvailable)
        ? ''
        : ''
    return getCriclyticsUrlHome(match, tab)
  }

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  const getCriclyticdUrlHomeDesktop = (match, subTab) => {
    let tab =
      match.matchStatus === 'completed'
        ? match.matchType !== 'Test'
          ? subTab
          : subTab
        : match.matchStatus === 'live' && match.isLiveCriclyticsAvailable
        ? subTab
        : match.matchStatus === 'upcoming' ||
          (match.matchStatus === 'live' && !match.isLiveCriclyticsAvailable)
        ? subTab
        : subTab
    return { ...getCriclyticsUrlHome(match, tab), query: { tab: subTab } }
  }
  console.log(pathname.length, 'pathname---------')
  const slectedCss =
    'flex  items-center justify-center rounded-3xl border-2 border-red bg-red text-black font-medium text-xs w-1/3 bg-gray-8 py-[6px]'
  const nonSlected =
    'flex items-center justify-center rounded-3xl bg-[#EEEEEE] py-[6px] text-black font-medium text-xs w-1/3 p-1 '
  if (loading)
    return (
      <>
        <Loading />
      </>
    )
  return (
    <div className="">
      <div className="flex items-center justify-start p-3 md:hidden z-50 lg:hidden fixed bg-gray-8 top-0 w-full">
        <div
          className="p-3 bg-gray rounded-md "
          // onClick={() => router.back()}
          onClick={() => router.push('/')}
        >
          <img
            className=" flex items-center justify-center h-4 w-4 rotate-180"
            src="/svgsV2/RightSchevronWhite.svg"
            alt=""
          />
        </div>

        <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
          <div className="">Criclytics</div>
          <span className="text-[8px] -mt-2">TM</span>
        </div>
      </div>

      <div
        className="flex flex-col md:flex-row w-full items-center justify-evenly dark:mt-16"
        style={{
          backgroundImage: 'url(/pngsV2/criclytics_back.png)',
          backgroundPosition: 'cover',
          backgroundRepeat: 'no-repeat',
          height: '100%',
          width: '100%',
          backgroundSize: 'cover',
        }}
      >
        {' '}
        <div className="lg:hidden xl:hidden md:hidden">
          <img
            className="h-36 w-36 mb:mt-7 md:mt-4 object-contain object-top"
            src="/pngsV2/cric_home.png"
            alt=""
          />
        </div>
        {/* <div className="hidden lg:block md:block">
          <img className="-mt-2" src="/pngsV2/criclytics_banner.png" alt="" />
        </div> */}
        <div className="mb:my-6 md:my-1 lg:hidden md:hidden">
          <div className="text-center w-full  text-2xl text-white font-semibold ">
            Criclytics Dashboard
          </div>

          <div className="pb-3 px-8 text-center white text-sm font-medium w-full">
            An accurate end-to-end analysis of teams, players and pitch based on
            their <br /> recent performances
          </div>
        </div>
      </div>

      <>
        <div className="lg:hidden md:hidden">
          {(data?.getMatchesForCriclytics?.criclyticsButtonFlags?.isFinalFour ||
            data?.getMatchesForCriclytics?.criclyticsButtonFlags
              ?.isPlayerIndex) && (
            <div>
              <div className="text-center py-2 bg-gray mb-3 mt-1 font-semibold text-xs text-gray-2 uppercase">
                {
                  data?.getMatchesForCriclytics?.criclyticsButtonFlags
                    ?.featuredSeriesName
                }
              </div>

              <div className="flex items-center justify-around">
                <Link
                  legacyBehavior
                  className="text-xs white font-medium md:pr-3"
                  href={`/criclytics/${
                    data?.getMatchesForCriclytics?.criclyticsButtonFlags?.tourID
                  }/${data?.getMatchesForCriclytics?.criclyticsButtonFlags?.seriesName
                    .replace(/[^a-zA-Z0-9]+/g, ' ')
                    .split(' ')
                    .join('-')
                    .toLowerCase()}/qualification-probability`}
                >
                  <div className="w-5/12 bg-gray p-3 text-xs text-white cursor-pointer rounded-lg flex items-center justify-center">
                    Qualification Probability
                    <img
                      className="w-8"
                      src="/pngsV2/qualification-probability.png"
                      alt=""
                    />
                  </div>
                </Link>

                <Link
                  className=" text-xs white font-medium md:pr-3"
                  legacyBehavior
                  href={`/criclytics/${
                    data?.getMatchesForCriclytics?.criclyticsButtonFlags?.tourID
                  }/${data?.getMatchesForCriclytics?.criclyticsButtonFlags?.seriesName
                    .replace(/[^a-zA-Z0-9]+/g, ' ')
                    .split(' ')
                    .join('-')
                    .toLowerCase()}/most-valuable-player`}
                >
                  <div className="flex w-5/12 text-xs bg-gray p-3 text-white rounded-lg items-center justify-center">
                    Most Valuable Player
                    <img
                      className="flex items-center justify-center w-8"
                      src="/pngsV2/most-valuable.png"
                      alt=""
                    />
                  </div>
                </Link>
              </div>
            </div>
          )}
        </div>
        <div className="hidden lg:block md:block">
          {(data?.getMatchesForCriclytics?.criclyticsButtonFlags?.isFinalFour ||
            data?.getMatchesForCriclytics?.criclyticsButtonFlags
              ?.isPlayerIndex) && (
            <div>
              <div className="mt-4 rounded shadow-sm flex justify-between items-center bg-white p-4">
                <Heading
                  heading={
                    data?.getMatchesForCriclytics?.criclyticsButtonFlags
                      ?.featuredSeriesName
                  }
                />

                <div className="">
                  <Link
                    legacyBehavior
                    className=""
                    href={`/criclytics/${
                      data?.getMatchesForCriclytics?.criclyticsButtonFlags
                        ?.tourID
                    }/${data?.getMatchesForCriclytics?.criclyticsButtonFlags?.seriesName
                      .replace(/[^a-zA-Z0-9]+/g, ' ')
                      .split(' ')
                      .join('-')
                      .toLowerCase()}/qualification-probability`}
                  >
                    <div className="inline-flex justify-center items-center py-2 px-4 border mr-3 cursor-pointer bg-slate-200 rounded">
                      <img
                        className="w-6 h-6"
                        src="/pngsV2/qualification.png"
                        alt=""
                      />
                      <span className="ml-2 text-sm">
                        Qualification Probability
                      </span>
                    </div>
                  </Link>

                  <Link
                    className=""
                    legacyBehavior
                    href={`/criclytics/${
                      data?.getMatchesForCriclytics?.criclyticsButtonFlags
                        ?.tourID
                    }/${data?.getMatchesForCriclytics?.criclyticsButtonFlags?.seriesName
                      .replace(/[^a-zA-Z0-9]+/g, ' ')
                      .split(' ')
                      .join('-')
                      .toLowerCase()}/most-valuable-player`}
                  >
                    <div className="inline-flex justify-center items-center py-2 px-4 border cursor-pointer bg-slate-200 rounded">
                      <img
                        className="flex items-center justify-center mr-1 w-6 h-6"
                        src="./pngsV2/mostvalue.png"
                        alt=""
                      />
                      <span className="text-sm">Most Valuable Player</span>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          )}
        </div>

        <div className="flex justify-start items-center mt-4 mb-2">
          <div className="flex justify-center dark:w-[1020px] dark:mx-2 dark:justify-between w-6/12 rounded-3xl capitalize tracking-wide p-1">
            <Tab
              type="block"
              data={TabData}
              handleTabChange={(item) => setCurrentTab(item)}
              selectedTab={currentTab}
            />
          </div>
        </div>

        <div className="">
          {currentTab?.value === 'upcoming' &&
            (data?.getMatchesForCriclytics?.upcoming?.filter(
              (x) =>
                x.isCricklyticsAvailable &&
                (x?.matchStatus === 'upcoming' ||
                  (x?.matchStatus == 'live' && !x.isLiveCriclyticsAvailable)),
            ).length > 0 ? (
              <div className="md:grid md:grid-cols-3 md:gap-3 md:p-4 md:mb-6 dark:border-none md:bg-white flex flex-wrap dark:mx-2 dark:gap-3 items-center">
                {data?.getMatchesForCriclytics?.upcoming
                  ?.filter(
                    (x) =>
                      x.isCricklyticsAvailable &&
                      (x?.matchStatus === 'upcoming' ||
                        (x?.matchStatus == 'live' &&
                          !x.isLiveCriclyticsAvailable)),
                  )
                  .map((team, t) => (
                    <>
                      {' '}
                      <Link
                        key={t}
                        {...getCriclyticdUrlHome(team)}
                        passHref
                        className="flex"
                        legacyBehavior
                      >
                        <div
                          key={t}
                          className="w-full lg:hidden md:hidden xl:hidden md:w-6/12 xl:w-6/12 lg:w-6/12 overflow-hidden cursor-pointer"
                          onClick={() => handleCriclyticsNav(team)}
                        >
                          <Scores data={team} from={'criclytics'} />
                        </div>
                      </Link>
                      <Link
                        key={t}
                        {...getCriclyticdUrlHomeDesktop(team, 'player-matchup')}
                        passHref
                        className="flex"
                        legacyBehavior
                      >
                        <div
                          key={t}
                          className="w-full overflow-hidden hidden md:block px-1 py-1.5 cursor-pointer"
                          onClick={() => handleCriclyticsNav(team)}
                        >
                          <Scores data={team} from={'criclytics'} />
                        </div>
                      </Link>
                    </>
                  ))}
              </div>
            ) : (
              <DataNotFound displayText={'No matches available'} />
            ))}
          {/* Live */}
          {currentTab.value === 'live' && (
            <div className="">
              {/* {data?.featurematch.filter((x) => x.isLiveCriclyticsAvailable && x.matchStatus === 'live').length >
                    0 && <div className='f5 fw5 black-80 white bg-navy pv3  pl2 dn db-l'>LIVE</div>} */}
              {}
              <div
                className={`${
                  data?.getMatchesForCriclytics?.live?.length == 0
                    ? 'flex items-center justify-center md:bg-white md:px-4 md:pt-6 md:pb-5 md:mb-6'
                    : 'md:grid md:grid-cols-3 md:gap-5 md:px-4 md:pt-6 md:pb-5 md:mb-6 dark:border-none md:bg-white flex flex-wrap dark:gap-2 items-center dark:mx-2'
                }`}
              >
                {data?.getMatchesForCriclytics?.live?.filter(
                  (x) =>
                    x.isLiveCriclyticsAvailable &&
                    x.matchStatus === 'live' &&
                    currentTab.value === 'live',
                ).length > 0 ? (
                  data?.getMatchesForCriclytics?.live
                    ?.filter(
                      (x) =>
                        x.isLiveCriclyticsAvailable &&
                        x.matchStatus === 'live' &&
                        currentTab.value === 'live',
                    )
                    .map((team, t) => (
                      <>
                        <div className="w-full hidden md:block cursor-pointer">
                          <Link
                            key={t}
                            {...getCriclyticdUrlHomeDesktop(
                              team,
                              'score-projection',
                            )}
                            passHref
                            className="flex"
                            legacyBehavior
                          >
                            <div className="">
                              <Scores data={team} from={'criclytics'} />
                            </div>
                          </Link>
                        </div>

                        <div className="w-full lg:hidden md:hidden overflow-hidden cursor-pointer">
                          <Link
                            key={t}
                            {...getCriclyticdUrlHome(team)}
                            passHref
                            className="flex "
                            legacyBehavior
                          >
                            <div className="mx-1">
                              <Scores data={team} from={'criclytics'} />
                            </div>
                          </Link>
                        </div>
                      </>
                    ))
                ) : (
                  <DataNotFound displayText={'No matches available'} />
                )}
              </div>
            </div>
          )}
          {/* md:gap-6 md:grid md:grid-cols-3 flex flex-wrap items-center
          dark:gap-3 dark:mx-2  */}
          {/* completed */}
          {currentTab.value === 'completed' && (
            <>
              <div
                className="md:grid md:grid-cols-3 md:gap-x-3 md:gap-y-5 md:px-4 md:pt-6 md:pb-8 md:mb-6 dark:border-none md:bg-white rounded-md shadow-sm flex flex-wrap items-center
            dark:gap-3 dark:mx-2"
              >
                {data?.getMatchesForCriclytics?.completed?.filter(
                  (x) =>
                    x.isCricklyticsAvailable && x.matchStatus === 'completed',
                ).length > 0 ? (
                  data?.getMatchesForCriclytics?.completed
                    ?.filter(
                      (x) =>
                        x.isCricklyticsAvailable &&
                        x.matchStatus === 'completed',
                    )
                    .map((team, t) => (
                      <>
                        <div className="w-full hidden md:block overflow-hidden cursor-pointer">
                          <Link
                            className="w-full"
                            key={t}
                            {...getCriclyticdUrlHomeDesktop(
                              team,
                              'report-card',
                            )}
                            passHref
                            legacyBehavior
                          >
                            <div className="mx-1">
                              <Scores data={team} from={'criclytics'} />
                            </div>
                          </Link>
                        </div>

                        <div className="w-full lg:hidden md:hidden overflow-hidden cursor-pointer">
                          <Link
                            className="w-full"
                            key={t}
                            {...getCriclyticdUrlHome(team)}
                            passHref
                            legacyBehavior
                          >
                            <div className="mx-1">
                              <Scores data={team} from={'criclytics'} />
                            </div>
                          </Link>
                        </div>
                      </>
                    ))
                ) : (
                  <DataNotFound displayText={'No matches available'} />
                )}
              </div>
            </>
          )}
        </div>
      </>

      <MetaDescriptor section={CRICLYTICS()} />
    </div>
  )
}
