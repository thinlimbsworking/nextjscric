'use client'
import axios from "axios";
import Link from "next/link";
import { exclusiveTabs } from "../../constant/constants";
import { useRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import { CDC_EXCLUSIVE_DATA_AXIOS } from '../../api/queries'
import Loading from "../../components/loading";
import Heading from "../../components/commom/heading";
import ImageWithFallback from "../../components/commom/Image";
import ExclusiveHome from "../../components/cdcexclusive/exclusiveHome";

let tout;

async function getData(type = 0, page = 0) {
    const { data } = await axios.post(process.env.API, {
        query: CDC_EXCLUSIVE_DATA_AXIOS,
        variables: { type, page },
    })
    return data.data.getCdcExclusive.data

}

export default function Page({ params }) {
    const excRef = useRef([])
    const scrollref = useRef();
    const [data, setData] = useState([]);
    const [scrolEnd, setscrolEnd] = useState(false);
    const [scrolStart, setScrolStart] = useState(true)
    const [scrollX, setscrollX] = useState(0);
  const [scrollX1, setscrollX1] = useState(0);
    const scrl = useRef();
    const [tab, setTab] = useState(exclusiveTabs[params.slug - 1])
   
    // const [data, setData] = useState([]);
    const [page, setPage] = useState(0)
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        setLoading(true);
        excRef.current[params.slug]?.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'center' })
        getData(tab.type, page).then(res => {
            setLoading(false);
            if(data.length && data.some(ele => ele.videoID === res[0]?.videoID && ele.articleID === res[0]?.articleID) ) {
                return;
            } else {
                setData(res)
            }
        })
    }, [])

    const slide = (shift) => {
        scrl.current?.scrollTo({
          left: scrl.current.scrollLeft + shift,
          behavior: "smooth",
        });
        setscrollX(scrollX + shift);
    
        if (
          Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
          scrl.current.offsetWidth
        ) {
          setscrolEnd(true);
        } else {
          setscrolEnd(false);
        }
        if(scrl.current.scrollLeft <= 0) setScrolStart(true)
            else setScrolStart(false)
      };
      const scrollCheck = () => {
        setscrollX1(scrl.current.scrollLeft);
        if (
          Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
          scrl.current.offsetWidth
        ) {
          setscrolEnd(true);
        } else {
          setscrolEnd(false);
        }
        if(scrl.current.scrollLeft <= 0) setScrolStart(true)
            else setScrolStart(false)
      };

   

    const handleScroll = async (evt) => {
        clearTimeout(tout)
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            tout = setTimeout(() => {
                setPage(p => p + 1);
                getData(tab.type, page + 1).then(res => {
                if(data.length && data.some(ele => ele.videoID === res[0]?.videoID && ele.articleID === res[0]?.articleID) ) {
                    return;
                } else {
                    setData(p => ([...p,...res]));
                }
                
            })
            }, 100); 
        }
    };  
    
    window.addEventListener('scroll', (e) => handleScroll(e))

    return <div className="w-full  p-2 md:p-0 lg:p-0  ">
         <div className=' hidden md:flex lg:flex   z-1 py-4 items-center justify-center text-white mb-3 '>
            <div className='w-full flex items-center '>
                {/* <span className='text-xl  font-semibold capitalize'>{'CRICKET.COM EXCLUSIVE'}</span> */}
                <Heading heading={'CRICKET.COM EXCLUSIVE'} />
            </div>
        </div>
        <div className='flex p-2 gap-2 items-center justify-start md:hidden lg:hidden'>
            <div className='p-2 rounded-md bg-gray-4'>
                <img className=' ' onClick={() => window.history.back()} src='/svgs/backIconWhite.svg' alt='back icon' />
            </div>
            <div className='text-base font-bold'>CRICKET.COM EXCLUSIVE</div>
        </div>
        <div className="w-full relative">
        <div className="w-full  flex gap-6  py-3 overflow-x-auto  border-b mb-3 px-3 md:px-4 lg:px-4" ref={scrl} onScroll={scrollCheck}>

            {/* router.push(`/exclusive/${ele.type}`) */}
            <img onClick={() => slide(-((scrl && scrl.current && scrl.current.offsetWidth) / 5 + 50) )} src='/svgs/red-left.svg' className={`z-20 cursor-pointer  black absolute w-6 h-6 -left-8 top-20 ${scrolStart ? 'opacity-70 blur-0' : ''} `}/>

            {
                exclusiveTabs.filter(ele => ele.type).map((ele, i) => <Link href={`/exclusive/${ele.type}`}><div ref={e => excRef.current[ele.type] = e} onClick={() => { setData([]); setPage(0); }} className={`min-w-[180px] md:min-w-[240px] lg:min-w-[240px]  rounded-lg cursor-pointer border ${ele.id === exclusiveTabs[params && params.slug - 1].id ? 'border-basered scale-110 dark:border-green ' : ''} bg-white dark:bg-gray p-2`}>
                    <ImageWithFallback height={100} width={100} loading='lazy' className="w-full h-32 " src={ele.src} />
                    <div className="w-full  text-center p-1 text-xs font-bold">{ele.name}</div>
                </div></Link>)
            }
                        <img onClick={() => slide(+((scrl && scrl.current && scrl.current.offsetWidth) / 5) + 50)} src='/svgs/red-right.svg' className={`z-20 cursor-pointer  black absolute w-6 h-6 -right-8 top-20 ${scrolEnd ? 'opacity-70 blur-0' : ''} `}/>

        </div>
        </div>
       
        <Heading heading={exclusiveTabs[params.slug - 1].name} />
        {
            loading ? <Loading/>
            : <ExclusiveHome data={data} getData={getData} setPage={setPage}/>
            
        }
        <div ref={scrollref}></div>

    </div>
}