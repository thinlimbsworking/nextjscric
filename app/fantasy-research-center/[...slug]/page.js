'use client'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import dynamic from 'next/dynamic'
import Loading from '../../components/loading'
import { usePathname, useSearchParams } from 'next/navigation'

import { FRCView } from '../../constant/MetaDescriptions'
import MetaDescriptor from '../../components/commom/metadecription'
import Urltracking from '../../components/commom/urltracking'

let token
const componentList = {
  fantasyDetail: dynamic(() => import('../../components/frc/fantasyDetails'), {
    loading: () => <Loading />,
  }),
  // expertTeams: dynamic(() => import('../../components/frc/ExpertTeams'), {
  //   loading: () => <Loading />
  // }),
  manualSelection: dynamic(
    () => import('../../components/frc/manualSelection'),
    {
      loading: () => <Loading />,
    },
  ),
  myTeams: dynamic(() => import('../../components/frc/myTeams'), {
    loading: () => <Loading />,
  }),

  fantasyTeam: dynamic(() => import('../../components/frc/algoSixTeams'), {
    loading: () => <Loading />,
  }),

  liveteams: dynamic(
    () => import('../../components/frc/liveCompletedMyTeams'),
    {
      loading: () => <Loading />,
    },
  ),

  // completedAndLiveFRCDetails: dynamic(() => import('../../components/FRC/CompletedAndLiveFRCDetails'), {
  //   loading: () => <Loading />
  // }),
  // completedAndLiveTeamSelect: dynamic(() => import('../../components/FRC/CompletedAndLiveFRCTeam'), {
  //   loading: () => <Loading />
  // }),

  StatHub: dynamic(() => import('../../components/frc/stathub'), {
    loading: () => <Loading />,
  }),
}

import {
  MATCH_DATA_INSIDE_FRC_AXIOS,
  GET_FRC_HOME_PAGE_AXIOS,
  GET_FRC_HOME_PAGE,
  GET_FANTASY_RESEARCH_AXIOS,
  GET_ALL_USER_TEAM_AXIOS,
  GET_ALL_USER_TEAM,
  MATCH_DATA_INSIDE_FRC,
  GET_FANTASY_RESEARCH
} from '../../api/queries'
import { getLangText } from '../../api/services'
import { words } from './../../constant/language'
import { useRouter } from 'next/navigation'
import Login from '../../login/page'
import { useQuery } from '@apollo/client'

function getComponentByUrl(subTab, matchID) {
  switch (subTab) {
    case 'my-teams':
      return {
        component: 'myTeams',
        request: {
          query: MATCH_DATA_INSIDE_FRC,
          variables: { matchID: matchID },
        },
      }
    case 'live-teams':
      return {
        component: 'liveteams',
        request: {
          query: MATCH_DATA_INSIDE_FRC,
          variables: { matchID: matchID },
        },
      }
    case 'fantasyTeam':
      return {
        component: 'fantasyTeam',
        request: {
          query: MATCH_DATA_INSIDE_FRC,
          variables: { matchID: matchID },
        },
      }

    case 'create-team':
      return {
        component: 'manualSelection',
        request: {
          query: GET_FANTASY_RESEARCH,
          variables: { matchID: matchID },
        },
      }

    case 'fantasy-stats/players':
      return {
        component: 'StatHub',
        request: {
          query: GET_FANTASY_RESEARCH,
          variables: { matchID: matchID },
        },
      }

    default:
      return {
        component: 'fantasyDetail',
        request: {
          query: MATCH_DATA_INSIDE_FRC,
          variables: { matchID: matchID },
        },
      }
  }
}


export default function Page(context) {
  const { params, searchParams } = context;
  let router = useRouter()
  const pathName = usePathname()
  const [urlData, setUrlData] = useState({})
  const [viewTeamStatsHub, setviewTeamStatsHub] = useState('')
  const [showlogin, setShowLogin] = useState(false)
  const [language, setlanguage] = useState()
  const [loadingUrl, setLoadingUrl] = useState(true)
 

  const [matchID, seriesSlug, subTab, fantasyTeam] = params.slug;
  let resolveComponent = getComponentByUrl(subTab, matchID)
  const {
    loading,
    data,
    error
  } = useQuery(resolveComponent.request.query, {
    variables: resolveComponent.request.variables,
  });

  const {
    loading: loading2,
    data: matchBasicDetail,
  } = useQuery(GET_FRC_HOME_PAGE);

  const {
    loading: loading1,
    data: matchData,
  } = useQuery(MATCH_DATA_INSIDE_FRC, {
    variables: {
      matchID,
    },
  });

  if (searchParams?.ffCode) {
    
    const { sdata } = useQuery(GET_ALL_USER_TEAM, {
      variables: {
        matchID,
        token: localStorage.getItem('tokenData')
      },
      onCompleted: (res) => {
        console.log('calling ffcode api - ',res.getUserAllFrcTeams?.teams.filter(ele => ele.ffCode === searchParams.ffCode)[0]);
        setUrlData(res.getUserAllFrcTeams?.teams.filter(ele => ele.ffCode === searchParams.ffCode)?.[0])
        setLoadingUrl(false)
      }
    })
  }


  useEffect(() => {
    // setLoading(true)
    // getData(params.slug, searchParams).then((res) => {
    //   setProps(res)
    //   res.selectedTeam && setUrlData(res.selectedTeam[0])
    //   setLoading(false)
    // })
    // updateWindowObject({ ...global.window });
    token = localStorage.getItem('tokenData')
    setlanguage(localStorage.getItem('FRClanguage'))
  }, [])

  if (loading || loading2 || loading1 || (searchParams?.ffCode && loadingUrl)) return <Loading />

  // let ComponentList=componentList['fantasyDetail']

  let ComponentList =
    fantasyTeam == 'players' ||
      fantasyTeam == 'teams' ||
      fantasyTeam == 'stadium'
      ? componentList['StatHub']
      : componentList[resolveComponent.component]

  
    return (
      <div className="text-white ">
        {seriesSlug && (
          <MetaDescriptor
            section={FRCView(
              {
                seriesSlug: seriesSlug.split('-').join(' ').toUpperCase(),
              },
              router.asPath,
              subTab,
            )}
          />
        )}

        {/* {urlTrack ? (
          <Urltracking PageName="Fantasy Research Center" />
        ) : null} */}
        {/* <Test/> */}

        {localStorage.getItem('tokenData') ? (
          <ComponentList
            data={data}
            matchBasicData={matchBasicDetail}
            urlData={urlData}
            setUrlData={setUrlData}
            matchData={matchData}
            // browser={browser}
            matchID={matchID}
            seriesSlug={seriesSlug}
            fantasyTeam={fantasyTeam}
            viewTeamStatsHub={viewTeamStatsHub}
            setviewTeamStatsHub={setviewTeamStatsHub}
            language={language}
            setlanguage={setlanguage}
          />
        ) : (
          (
            <Login
              navTo={router.asPath}
              modalText={'Login / Register to access Fantasy Research Center'}
              sucessPath={router.asPath}
              name={false}
              setShowLogin={setShowLogin}
              TeamAID={''}
              TeamBID={''}
              onCLose={'/slug'}
              matchID={pathName.split('/')[2]}
              ShowLoginSucess={false}
              entryRoute={'FRC'}
              direct={'true'}
            // fantasy-research-center/222197/sunrisers-hyderabad-vs-mumbai-indians/fantasyTeam
            />
          )
        )}
      </div>
    )
  
}

// export const getStaticPaths = async () => {

//   const HOMEPAGE = await axios.post(process.env.API, {
//       query: GET_FRC_HOME_PAGE_AXIOS
//     });

//    let data = HOMEPAGE.data.data

//    let playerPaths =data.getFRCHomePage.upcomingmatches.map((item) => {
//     return {
//       params: {
//         slugs: [ item.matchID.toString(),item.matchName.replace(/[^a-zA-Z0-9]+/g,' ')
//         .split(' ')
//         .join('-')
//         .toLowerCase()]
//       }
//     };
//   });
//   return {
//     paths: [...playerPaths],
//     fallback: true
//   };
// };
