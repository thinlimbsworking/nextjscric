'use client'
import React, { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import Login from '../login/page'
import FrcLayout from '../components/frc/frcLayout'
import ImageWithFallback from '../../components/commom/Image'
import Image from 'next/image'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/navigation'
import { getFrcSeriesHomeUrl } from '../../api/services'
import Link from 'next/link'
// import Login from '../login';
// import { navigate } from '@reach/router';
// import Router from 'next/router';
import CleverTap from 'clevertap-react'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import {
  GET_ARTICLES_BY_CATEGORIES_AXIOS,
  GET_FRC_HOME_PAGE_AXIOS,
  GET_FRC_HOME_PAGE,
  VIDEO_BY_CATEGORY_AXIOS,
} from '../api/queries'
import axios from 'axios'
import Countdown from 'react-countdown-now'
import { format } from 'date-fns'
const flagPlaceHolder = '/svgs/images/flag_empty.png'
import { FRCHome } from '../constant/MetaDescriptions'
import MetaDescriptor from '../components/MetaDescriptor'

const RightSchevronBlack = '/svgs/RightSchevronBlack.svg'
const VideoIcon = '/svgs/videoIcon.svg'
import { VIDEOS_VIEW } from '../constant/Links'
import LanguageModel from '../components/frc/languageModel'
import DataNotFound from '../components/commom/datanotfound'
import Heading from '../components/commom/heading'
import Loading from '../components/loading'
import Scores from '../components/commom/score'
import Tab from '../components/shared/Tab'
import SliderTab from '../components/shared/Slidertab'

// import Homevideo from '../../componentV2/frc/frchomevideo'
const location = './pngsV2/location.png'
const mom = '/pngsV2/MOM2.png'
const language_icon = '/pngsV2/lang.png'
let token,
  lang = 'ENG'

async function getData() {
  const HOMEPAGE = await axios.post(process.env.API, {
    query: GET_FRC_HOME_PAGE_AXIOS,
  })
  return HOMEPAGE.data.data
}
async function getVideos() {
  const HOMEPAGE = await axios.post(process.env.API, {
    query: VIDEO_BY_CATEGORY_AXIOS,
    variables: { type: 'fantasy' },
  })
  return HOMEPAGE.data.data
}
async function getArticles() {
  const HOMEPAGE = await axios.post(process.env.API, {
    query: GET_ARTICLES_BY_CATEGORIES_AXIOS,
    variables: { type: 'fantasy', page: 0 },
  })
  return HOMEPAGE.data.data
}

export default function Fantasyresearchcenter({ ...props }) {
  const router = useRouter()
  const [language, setlanguage] = useState(lang)
  // const [data, setData] = useState()
  const [fantasyVideos, setFantasyVideos] = useState([])
  const [fantasyPreviews, setFantasyPreviews] = useState([])
  const [loginPath, setLoginPath] = useState('')
  const [matchIDNAV, setmatchIDNAV] = useState('')

  const [showlogin, setShowLogin] = useState(false)
  const [navtoData, setnavToData] = useState('')
  const [tabName, setTabName] = useState({
    name: lang === 'ENG' ? 'UPCOMING' : 'आगामी',
    title: 'upcoming matches',
    value: 'upcomingmatches',
  })
  const [clicked, setClicked] = useState(false)

  const { loading, data, error } = useQuery(GET_FRC_HOME_PAGE, {
    variables: { type: 'fantasy' },
  })
  // ;
  // data = data.data;

  useEffect(() => {
    token = window.localStorage.getItem('tokenData')
    setTabName(matchTypes[0])
    // getData().then((res) => setData(res))
    // getVideos().then(res => setFantasyVideos(res.getVideosByType))
    // getArticles().then(res => setFantasyPreviews(res.getArticlesByCategories))
  }, [])

  let matchTypes = [
    {
      name: getLangText(language, words, 'upcoming'),
      title: 'upcoming matches',
      value: 'upcomingmatches',
    },
    {
      name: getLangText(language, words, 'live'),
      title: 'live matches',
      value: 'livematches',
    },
    {
      name: getLangText(language, words, 'completed'),
      title: 'completed matches',
      value: 'completedmatches',
    },
  ]

  const loginHandle = (item) => {
    setClicked(true)
    // alert("dd")
    if (
      localStorage.getItem('tokenData') &&
      localStorage.getItem('tokenData') !== 'null'
    ) {
      router.push(getFrcSeriesHomeUrl(item).as, getFrcSeriesHomeUrl(item).as)
    } else {
      let matchId = item.matchID
      setmatchIDNAV(item.matchID)
      let matchName = `${item.homeTeamShortName}-vs-${item.awayTeamShortName}-`.toLowerCase()
      let seriesSlug =
        matchName +
        item.matchName
          .replace(/[^a-zA-Z0-9]+/g, ' ')
          .split(' ')
          .join('-')
          .toLowerCase()

      const patHLogin = `/fantasy-research-center/${matchId}/${seriesSlug}`
      setnavToData(patHLogin)
      setShowLogin(true)
      setClicked(false)
    }
    // }
  }

  const getVideoView = (video) => {
    let type = 'fantasy'
    let videoId = video.videoID
    return {
      as: eval(VIDEOS_VIEW.as),
      href: VIDEOS_VIEW.href,
    }
  }

  const fantasyTrack = (match) => {
    CleverTap.initialize('Fantasy', {
      Source: 'FantasyCentreHome',
      MatchID: match.matchID,
      SeriesID: '',
      TeamAID: match.homeTeamID,
      TeamBID: match.awayTeamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.matchDateTimeGMT), 'd,y, h:mm a'),
      MatchStatus: match.matchStatus,
      Platform: localStorage ? localStorage.Platform : '',
    })
  }

  const arr = ['UPCOMING', 'LIVE', 'COMPLETED']

  const isEnglish = (lang) => {
    if (lang == 'ENG') return true
    else false
  }
  // const { error: replacementerror, loading, data } = useQuery(GET_FRC_HOME_PAGE);

  // if (replacementerror) {
  //
  // }
  // if (loading) {
  //

  // }

  const handleMonthHindiTag = (date1) => {
    let monthformat = format(Number(date1) - 19800000, 'MMMM')
    let do_year = format(Number(date1) - 19800000, 'do')
    let year = format(Number(date1) - 19800000, 'yyyy')
    let time = format(Number(date1) - 19800000, 'h:mm a')
    if (language === 'HIN') {
      return (
        getLangText(language, words, monthformat.toLowerCase()) +
        ' ' +
        do_year +
        ' ' +
        year +
        ', ' +
        time
      )
    } else {
      return monthformat + ' ' + do_year + ' ' + year + ', ' + time
    }
  }

  //   {`${format(
  //     Number(match.matchDateTimeGMT) - 19800000,
  //     `do ${getLangText(language,words,`MMMM`.toLowerCase())} yyyy, h:mm a`
  //   )}`}
  // }
  const handleMonthHindi = (date1) => {
    let monthformat = format(Number(date1) - 19800000, 'MMMM')

    let date = format(Number(date1) - 19800000, 'dd yyyy')
    //

    if (language === 'HIN') {
      return (
        getLangText(language, words, monthformat.toLowerCase()) + ' ' + date
      )
    } else {
      return monthformat + ' ' + date
    }
  }
  if (loading) return <Loading />
  if (clicked) return <Loading />
  if (data) {
    return (
      <>
        {showlogin && (
          <Login
            navTo={navtoData}
            modalText={'Login / Register to access Fantasy Research Center'}
            sucessPath={navtoData}
            name={false}
            setShowLogin={setShowLogin}
            TeamAID={''}
            TeamBID={''}
            onCLose={'/fantasy-research-center'}
            matchID={matchIDNAV}
            ShowLoginSucess={true}
            entryRoute={'FRC'}
          />
        )}

        <FrcLayout updateLanguage={setlanguage} lang={language}>
          <div className="flex items-center  my-3  ">
            <div className=" px-3 md:px-0 lg:px-0 w-full md:w-6/12 lg:w-6/12 ">
              <Tab
                type="block"
                data={matchTypes}
                selectedTab={tabName}
                handleTabChange={(item) => setTabName(item)}
              />
            </div>
          </div>
          <div className="px-3 md:px-0 lg:px-0 ">
            <div className="md:hidden lg:hidden my-3 flex flex-col text-white">
              <Heading heading={getLangText(language, words, tabName.title)} />
            </div>

            <div className="flex flex-wrap  md:p-4 lg:p-4 mb-6 dark:border-none md:bg-white rounded-md shadow-sm">
              {matchTypes
                .filter(
                  (val) =>
                    val.value.toLowerCase() === tabName.value.toLowerCase(),
                )
                .map((curr) =>
                  data &&
                  data.getFRCHomePage &&
                  data.getFRCHomePage[curr.value] &&
                  data.getFRCHomePage[curr.value].length > 0 ? (
                    data.getFRCHomePage[curr.value].map((match, i) => (
                      <div
                        className="w-full md:w-1/3 lg:w-1/3 cursor-pointer overflow-hidden py-2 md:p-2 lg:p-2"
                        onClick={() => (
                          loginHandle(match), fantasyTrack(match)
                        )}
                      >
                        {match && (
                          <Scores data={match} from="frc" lang={language} />
                        )}
                      </div>
                    ))
                  ) : (
                    <DataNotFound displayText={'Match not available'} />
                  ),
                )}
            </div>
          </div>
        </FrcLayout>
      </>
    )
  } else {
    ;<div></div>
  }
}
