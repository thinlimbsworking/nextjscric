'use client'
import React from 'react'
import { useState, useEffect, useRef } from 'react'
import { GET_FEEDS, UPDATE_FEED_LIKES, ASK_AN_EXPERT } from '../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { format, formatDistance, formatDistanceStrict } from 'date-fns'
import { TwitterTweetEmbed } from 'react-twitter-embed'

export default function Feeds() {
  let { loading, error, data, fetchMore } = useQuery(GET_FEEDS, {
    variables: {
      id: '',
      activeId: '',
      page: 0,
    },
    fetchPolicy: 'cache-first',
  })

  let scrl = useRef(null)
  let scrl1 = useRef(null)
  const [scrollWidth, setScrollWidth] = useState(0)

  const [scrollX, setscrollX] = useState(0)
  const [scrollX1, setscrollX1] = useState(0)
  const [askExpertQUestion, setAskExpertQuestion] = useState('')

  const [tabData, setTabData] = useState([])
  const [selectedTabData, setSelectedTabData] = useState('all')

  const [contenTheme, setContentTheme] = useState('selectMatch')
  const [scrolEnd, setscrolEnd] = useState(false)
  const [scrolEnd1, setscrolEnd1] = useState(0)
  const [use, setUse] = useState(0)
  const [value, setValue] = useState(0)
  // const [data,setData]=useState([])

  // console.log("evtevtevtevtevtevt",data)
  // console.log("evtevtevtevtevtevt",data)

  const slide = (shift) => {
    // scrl.current.scrollLeft += shift;

    scrl.current?.scrollTo({
      left: scrl.current.scrollLeft + shift,
      behavior: 'smooth',
    })

    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }
  const scrollCheck = () => {
    // alert(8);
    setscrollX1(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }

  useEffect(() => {
    if (!window.localStorage.getItem('myFeed')) {
      const myObject = [
        {
          name: 'john doe',
          feedID: 32,
          gender: 'male',
          profession: 'optician',
        },
      ]

      window.localStorage.setItem('myFeed', JSON.stringify(myObject))
    }
    console.log(JSON.parse(window.localStorage.getItem('myFeed')))
    const s = document.createElement('script')
    s.setAttribute('src', 'https://platform.twitter.com/widgets.js')
    s.setAttribute('async', 'true')
    document.head.appendChild(s)
  }, [])
  console.log('datadatadatadata12121', data && data.primaryFeeds)

  const func = (index) => {
    console.log(index)

    const s = document.createElement('script')
    s.setAttribute('src', 'https://platform.twitter.com/widgets.js')
    s.setAttribute('async', 'true')
    document.head.appendChild(s)
  }
  const handlePagination = () => {
    fetchMore({
      variables: {
        // seriesID: filter.valueId,
        page: Math.floor(data?.primaryFeeds?.length / 10),
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!previousResult) return previousResult

        let parsedPre = previousResult.primaryFeeds
        let newData = fetchMoreResult.primaryFeeds

        let removeDupli = [...parsedPre, ...newData]
        let uniq = new Set(
          removeDupli && removeDupli.map((e) => JSON.stringify(e)),
        )

        let res = Array.from(uniq).map((e) => JSON.parse(e))

        let feednewData = Object.assign({}, previousResult, {
          primaryFeeds: res,
        })

        return { ...feednewData }
      },
    })
  }
  const debounce = (fn, delay) => {
    let timeoutID
    return function (...args) {
      if (timeoutID) {
        clearTimeout(timeoutID)
      }
      timeoutID = setTimeout(() => {
        fn(...args)
      }, delay)
    }
  }

  const [UPDATE_FEED_LIKES_HANDLER, response] = useMutation(UPDATE_FEED_LIKES, {
    onCompleted: (data) => {
      console.log('response', data)

      let feedObj = JSON.parse(window.localStorage.getItem('myFeed'))
      console.log('23456789034567890-3456789034567890', data)
      const myObject = {
        name: 'john doe',
        feedID: data.updateFeedLikes.feedID,
        gender: 'male',
        profession: 'optician',
        response: '',
      }
      feedObj.push(myObject)

      window.localStorage.setItem('myFeed', JSON.stringify(feedObj))
    },
  })

  const [UPDATE_ASK_EXPERT, response2] = useMutation(ASK_AN_EXPERT, {
    onCompleted: (data) => {
      console.log('here is my answerdata', data)

      let feedObj = JSON.parse(window.localStorage.getItem('myFeed'))
      // alert(askExpertQUestion)

      const myObject = {
        name: ' doe',
        feedID: data.addExpertQuestion,
        gender: 'male',
        profession: 'optician',
        response: askExpertQUestion,
      }
      feedObj.push(myObject)

      window.localStorage.setItem('myFeed', JSON.stringify(feedObj))

      // setAskExpertQuestion("");
    },
  })

  const askExpert = (item) => {
    // let feedObj = JSON.parse(window.localStorage.getItem("myFeed"))

    // console.log(feedObj)
    // const found  = feedObj.find(({ feedID }) => feedID === id);

    // console.log('found',found)

    if (true)
      UPDATE_ASK_EXPERT({
        variables: {
          question: askExpertQUestion,
          feedID: item,
        },
      })
  }

  const handleClick = (id) => {
    // localStorage.setItem('feedArray', JSON.stringify(FlagArray))

    let feedObj = JSON.parse(window.localStorage.getItem('myFeed'))

    console.log(feedObj)
    const found = feedObj.find(({ feedID }) => feedID === id)
    // = feedObj.find(element => element.feedID == id);
    console.log('found', found)

    if (!found) {
      UPDATE_FEED_LIKES_HANDLER({
        variables: {
          id: id,
          like: true,
        },
      })
    }

    // console.log("7777",JSON.parse(feedObj));
  }

  const handleScroll = () => {
    setAskExpertQuestion('')
    //   console.log(parseInt(((scrl.current.getBoundingClientRect().y)*-1)/1000)/5, "datasize", (parseInt(((scrl.current.getBoundingClientRect().y)*-1)/1000)/5)>= data?.primaryFeeds?.length / 10)
    // scrl.current.style.overflow = "hidden"
    if (
      parseInt((scrl.current.getBoundingClientRect().y * -1) / 1000) / 5 >=
      data?.primaryFeeds?.length / 10
    ) {
      handlePagination()
    }

    function debounce(func, timeout = 300) {
      let timer
      return (...args) => {
        clearTimeout(timer)
        timer = setTimeout(() => {
          func.apply(this, args)
        }, timeout)
      }
    }
    function saveInput() {
      console.log('Saving data')
    }
    const scrollableHeight =
      scrl.current.scrollHeight - scrl.current.clientHeight

    const onScrollView = () => {
      if (
        scrl.current.getBoundingClientRect().y +
          scrl.current.getBoundingClientRect().height <
        800
      ) {
        alert(86)
        handlePagination()
      }
    }

    //   console.log("fhhh12h12",window.innerHeight + window.scrollY,document.body.offsetHeight,document.body.scrollHeight)
    // if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
    //     // setPagination((prev)=>prev + 1)

    //     // alert("end")
    //     console.log('checkkkkk')

    //     // alert(filter.valueLable)
    //     handlePagination()
    //   }
  }

  //   useEffect(() => {
  //     if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
  //         // setPagination((prev)=>prev + 1)

  //         // alert("end")
  //         console.log('checkkkkk')

  //         // alert(filter.valueLable)
  //         handlePagination()
  //       }
  //   }, []);

  //   onClick={debounce(likeHandle, 1000)}

  const checkResponse = (id) => {
    let feedObj = JSON.parse(window.localStorage.getItem('myFeed'))

    console.log(feedObj)
    const found = feedObj.find(({ feedID }) => feedID === id)

    return found ? true : false
  }

  const checkResponseExpert = (id) => {
    let feedObj = JSON.parse(window.localStorage.getItem('myFeed'))

    console.log(feedObj)
    const found = feedObj.find(({ feedID }) => feedID === id)

    return found?.response || ''
  }
  return (
    <div className="">
      <div className="example-wrapper ">
        <div
          className="container y-scroll y-mandatory "
          onScroll={handleScroll}
        >
          <div className="wrapper w-full" ref={scrl}>
            {data &&
              data.primaryFeeds.map((item, index) =>
                item.type.toLowerCase() == 'twitter'.toLowerCase() ? (
                  <div
                    className="element flex flex-col overflow-hidden  relative   w-full h-full  p-2"
                    key={index}
                    style={{ backgroundImage: `url(${item.background})` }}
                  >
                    <div className="flex  items-center p-2 ">
                      <div className=" flex items-center  w-9/12  ">
                        <img
                          className="h-12 w-12 rounded-full border-2"
                          src={`${item.featuredImage}?auto=compress&dpr=10&fit=crop&w=50&h=50`}
                          alt=""
                        />
                        <div className="ml-2 text xs font-bold text-white">
                          {' '}
                          {item.title}
                        </div>
                      </div>

                      <div className="flex ">
                        {' '}
                        {item?.createdAt
                          ? new Date().getTime() - Number(item?.createdAt) >
                            86400000
                            ? format(
                                new Date(Number(item?.createdAt)),
                                'dd MMM',
                              )
                            : formatDistanceStrict(
                                new Date(Number(item?.createdAt)),
                                new Date(),
                              ) + ' ago'
                          : ''}
                      </div>
                    </div>

                    <div className="relative">
                      <div
                        className="h-80"
                        dangerouslySetInnerHTML={{ __html: item.embeddedCode }}
                      />
                    </div>
                    {func()}
                    <svg
                      onClick={() => handleClick(item.feedID)}
                      className="absolute bottom-36 left-4 w-6 h-6  z-50"
                      xmlns="http://www.w3.org/2000/svg"
                      // fill={ checkResponse(item.feedID)?'red':'none'}

                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      fill={checkResponse(item.feedID) ? 'red' : 'none'}
                      stroke={
                        checkResponse(item.feedID) ? 'red' : 'currentColor'
                      }
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                      />
                    </svg>
                  </div>
                ) : item.type.toLowerCase() == 'image' ? (
                  <div
                    className="element flex flex-col p-2 overflow-hidden   h-screen  w-full relative"
                    key={index}
                    style={{ backgroundImage: `url(${item.background})` }}
                  >
                    <div className="flex  items-center p-2 ">
                      <div className=" flex items-center  w-9/12  ">
                        <img
                          className="h-12 w-12 rounded-full border-2"
                          src={`${item.featuredImage}?auto=compress&dpr=10&fit=crop&w=50&h=50`}
                          alt=""
                        />
                        <div className="ml-2 text xs font-bold text-white">
                          {' '}
                          {item.title}
                        </div>
                      </div>

                      <div className="flex ">
                        {' '}
                        {item?.createdAt
                          ? new Date().getTime() - Number(item?.createdAt) >
                            86400000
                            ? format(
                                new Date(Number(item?.createdAt)),
                                'dd MMM',
                              )
                            : formatDistanceStrict(
                                new Date(Number(item?.createdAt)),
                                new Date(),
                              ) + ' ago'
                          : ''}
                      </div>
                    </div>

                    <div className="p-2 flex  justify-center   relative">
                      <img
                        className="object-top object-cover  h-[500px] w-full"
                        src={`${item.image}?auto=compress&dpr=1&fit=crop&w=400&h=500'`}
                        alt=""
                      />
                    </div>
                    <svg
                      onClick={() => handleClick(item.feedID)}
                      className="absolute bottom-36 left-4 w-6 h-6 z-50"
                      xmlns="http://www.w3.org/2000/svg"
                      fill={checkResponse(item.feedID) ? 'red' : 'none'}
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke={
                        checkResponse(item.feedID) ? 'red' : 'currentColor'
                      }
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                      />
                    </svg>
                    {/* {func()} */}
                  </div>
                ) : item.type.toLowerCase() == 'expert' ? (
                  <div
                    className="element flex flex-col overflow-hidden iem  relative   w-full h-full  p-2"
                    key={index}
                    style={{ backgroundImage: `url(${item.background})` }}
                  >
                    <div className="flex  items-center p-2 ">
                      <div className=" flex items-center  w-9/12  ">
                        <img
                          className="h-12 w-12 rounded-full border-2"
                          src={`${item.featuredImage}?auto=compress&dpr=10&fit=crop&w=50&h=50`}
                          alt=""
                        />
                        <div className="ml-2 text xs font-bold text-white">
                          {' '}
                          {item.title}
                        </div>
                      </div>

                      <div className="flex ">
                        {' '}
                        {item?.createdAt
                          ? new Date().getTime() - Number(item?.createdAt) >
                            86400000
                            ? format(
                                new Date(Number(item?.createdAt)),
                                'dd MMM',
                              )
                            : formatDistanceStrict(
                                new Date(Number(item?.createdAt)),
                                new Date(),
                              ) + ' ago'
                          : ''}
                      </div>
                    </div>

                    <div className=" flex flex-col w-full  mt-6">
                      <img
                        className="w-full rounded-t-md object-top object-cover h-56"
                        src={`${item.image}`}
                        alt=""
                      />
                      <div className="flex flex-col items-center justify-center  w-full bg-gray-4 py-12 rounded-b-md ">
                        {item.question && (
                          <div className="flex  py-2 ">
                            {item.question || ''}
                          </div>
                        )}
                        <div className="flex text-sm py-2 ">
                          Ask The expert!
                        </div>
                        <div className="w-full  flex flex-col items-center justify-center py-2 px-6">
                          <div className="flex  items-center justify-center w-full">
                            {console.log(
                              'experttt ',
                              item.feedID,
                              checkResponseExpert(item.feedID),
                            )}
                            <input
                              maxLength={80}
                              value={
                                checkResponse(item.feedID)
                                  ? checkResponseExpert(item.feedID)
                                  : askExpertQUestion
                              }
                              onChange={(e) =>
                                setAskExpertQuestion(e.target.value)
                              }
                              // contentEditable={checkResponseExpert(item.feedID)!==""?true:false}
                              className="bg-gray-1  p-2 text-sm w-full rounded"
                              placeholder="Type somethings..."
                              type="text"
                              name=""
                              id=""
                            />
                          </div>
                          <div className="flex w-full items-end justify-end  text-xs text-light_gray pt-1">
                            {askExpertQUestion.length}/80 characters{' '}
                          </div>
                        </div>
                      </div>

                      <div className="flex  items-center justify-center mt-4">
                        <button
                          onClick={() => askExpert(item.feedID)}
                          className={` border   ${
                            checkResponse(item.feedID)
                              ? 'border-blue-2  text-blue '
                              : 'border-green  text-green '
                          }  p-2 text-xs w-40 rounded-md`}
                        >
                          {checkResponse(item.feedID) ? 'Sent' : 'Send'}
                        </button>
                      </div>
                    </div>

                    {/* {func()} */}
                  </div>
                ) : (
                  item.type.toLowerCase() == 'youtube' && (
                    <div
                      className="element flex flex-col p-2 overflow-hidden   h-screen  w-full relative"
                      key={index}
                      style={{ backgroundImage: `url(${item.background})` }}
                    >
                      <div className="flex  items-center p-2 ">
                        <div className=" ">
                          <img
                            className="h-12 w-12 rounded-full border-2"
                            src={`${item.featuredImage}?auto=compress&dpr=10&fit=crop&w=50&h=50`}
                            alt=""
                          />
                        </div>

                        <div className="ml-2 text xs font-bold text-white">
                          {' '}
                          {item.title}
                        </div>
                      </div>

                      <div className="p-2 flex  justify-center  ">
                        <iframe
                          width="853"
                          height="480"
                          src={`https://www.youtube.com/embed/${item.youtubeurlid}`}
                          frameBorder="0"
                          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;"
                          allowFullScreen
                          title="Embedded youtube"
                        />
                        {/* <iframe src={item.youtubeurl} title="W3Schools Free Online Web Tutorials"></iframe> */}
                      </div>
                      {/* {func()} */}
                      <svg
                        onClick={() => handleClick(item.feedID)}
                        className="absolute bottom-36 left-4 w-6 h-6  z-50"
                        xmlns="http://www.w3.org/2000/svg"
                        // fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        fill={checkResponse(item.feedID) ? 'red' : 'none'}
                        stroke={
                          checkResponse(item.feedID) ? 'red' : 'currentColor'
                        }
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                        />
                      </svg>
                    </div>
                  )
                ),
              )}
          </div>
        </div>
      </div>
    </div>
  )
}
