// export default function RootLayout({ children }) {
//   return (
//     <html>
//       <head></head>
//       <body>{children}</body>
//     </html>
//   )
// }

'use client'
import { useEffect, useRef, setState } from 'react'
import withData from './lib/lib'
import { useState, useReducer } from 'react'

import { ApolloProvider } from '@apollo/react-hooks'
import { ThemeProvider } from 'next-themes'
import Layouts from './components/layout'
import './styles/globals.css'
import 'swiper/css'
import 'swiper/css/effect-coverflow'
import { usePathname, useRouter } from 'next/navigation'

import 'swiper/css/pagination'
import ErrorBoundary from './components/shared/errorBoundry'

// const reloadScreen = (function () {
//   let tint;
//   let cnt = 0;
//   return (status) => {
//     if(status === 'on') {
//       clearInterval(tint)
//       if(cnt >= 5){
//       setTimeout(() => { window.location.href = window.location.href},100)
//     }
//     } else if(status === 'off') {
//         clearInterval(tint)
//         tint = setInterval(() => {
//           cnt++;
//         },60*1000)
//     }
//   }
// }());
// document.addEventListener('visibilitychange', () => {
//   if (document.visibilityState === 'visible') {
//     // console.log('came back to screen');
//     reloadScreen('on')
//   } else {
//     // console.log('went away from screen');
//     reloadScreen('off')
//   }
// })

export default function Layout({ children, params }) {
  // const [state, setStatePage] = useState("Active");

  // const onIdle = () => {
  //   setStatePage("Idle");

  //   setTimeout(() => {
  //     window.location = `${window.location.origin}${window.location.pathname}`;
  //   }, 30000);
  // };

  // const onActive = () => {
  //   setStatePage("Active");
  // };

  // const { getRemainingTime } = useIdleTimer({
  //   onIdle,
  //   onActive,

  //   timeout: 10_000,
  //   throttle: 500,
  // });

  // const timeout = useRef()
  // const [remainingTime, setRemainingTime] = useState(30)

  // useEffect(() => {
  //   if (timeout.current && remainingTime === 0) {

  //   } else {
  //     timeout.current = setTimeout(() => {
  //       setRemainingTime((t) => t - 1)
  //     }, 1000)
  //   }
  //   return () => {
  //     clearTimeout(timeout.current)
  //   }
  // }, [remainingTime])

  const router = useRouter()

  useEffect(() => storePathValues, [usePathname()])

  function storePathValues() {
    const storage = globalThis?.sessionStorage
    if (!storage) return
    // Set the previous path as the value of the current path.
    const prevPath = storage.getItem('currentPath')
    storage.setItem('prevPath', prevPath)
    // Set the current path value by looking at the browser's location object.
    storage.setItem('currentPath', globalThis.location.pathname)
  }

  const [multiplication, dispatch] = useReducer((state, action) => {
    return state * action
  }, 50)
  const [contenTabname, setContent] = useState('content')
  const baseUrl = 'https://m.devcdc.com/'
  // useEffect(() => {
  //
  //   if (window.innerWidth < 700) {
  //     window.location.href= window.location.href.replace(`${window.location.origin}/`, baseUrl);
  //       "https://m.devcdc.com/"
  //   }
  // }, [])

  // useEffect(() => {
  //   if (typeof window !== 'undefined') {
  //     let timerHere = null

  //     window?.document.addEventListener('visibilitychange', () => {
  //       if (window?.document.visibilityState === 'visible') {
  //         timerHere ? clearTimeout(timerHere) : null

  //         // console.log('came back to screen');
  //         // reloadScreen('on')
  //       } else {
  //         timerHere = setTimeout(() => {
  //           localStorage.setItem('lastpath', window.location.pathname)
  //           window.location = '/test'
  //         }, 2 * 60 * 1000)

  //         // console.log('went away from screen');
  //         // reloadScreen('off')
  //       }
  //     })
  //   }
  // })

  return (
    <html lang="en">
      <meta httpEquiv="Pragma" content="no-cache" />
      <meta
        httpEquiv="cache-control"
        content="no-cache,no-store, must-revalidate"
      />
      {/* <link rel='manifest' href='/manifest.webmanifest' /> */}
      <link
        rel="shortcut icon"
        href="https://images.cricket.com/icons/mainlogoico.ico"
      />
      <meta
        name="google-site-verification"
        content="0Hud9R7IwNS7E_rdKlYaIEOSTwx4rCKpZH7vUqc-Wx4"
      />
      <script async src="https://platform.twitter.com/widgets.js"></script>
      <script
        async
        src="https://platform.instagram.com/en_US/embeds.js"
      ></script>
      <script
        type="text/javascript"
        async=""
        src="https://www.google-analytics.com/analytics.js"
      ></script>
      <script
        type="text/javascript"
        async=""
        src="https://www.googletagmanager.com/gtag/js?id=G-CZEMGT65BD&amp;l=dataLayer&amp;cx=c"
      ></script>
      <script
        async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7891443491916247"
        crossorigin="anonymous"
      ></script>
      {/* <title>Cricket.com</title> */}
      <body className="w-full lg:bg-slate-100 md:bg-slate-100  bg-basebg  ">
        <ApolloProvider client={withData}>
          <ThemeProvider attribute="class" enableColorScheme={true}>
            <Layouts
              params={params}
              contenTabname={contenTabname}
              setContent={setContent}
              multiplication={multiplication}
            >
              <ErrorBoundary
                fallbackRender={() => (
                  <div className="w-full h-screen flex flex-col justify-center items-center">
                    <div className="flex justify-center items-center">
                      <img
                        className=" w-full h-60"
                        src="/pngsV2/empty.png"
                        alt=""
                      />
                    </div>

                    <div className="font-mnr font-semibold">
                      Please refresh and try again...
                    </div>
                  </div>
                )}
              >
                {children}
              </ErrorBoundary>
            </Layouts>
          </ThemeProvider>
        </ApolloProvider>

        <script
          defer
          src="https://www.googletagmanager.com/gtm.js?id=GTM-T852BBQ"
        ></script>

        <script async src="https://platform.twitter.com/widgets.js"></script>
        <script
          async
          src="https://platform.instagram.com/en_US/embeds.js"
        ></script>
      </body>
    </html>
  )
}
