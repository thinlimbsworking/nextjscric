'use client'
import React, { useEffect, useRef, useState } from 'react'
import dynamic from 'next/dynamic'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/navigation'
// import Loading from "../../../../components/Loading";
import axios from 'axios'
import Layout from '../../components/match-score/matchDetails/matchdetailLayout'
import {
  MATCH_DATA_FOR_SCORECARD_AXIOS,
  GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
  MATCH_SUMMARY_AXIOS,
  MATCH_DATA_FOR_SCORECARD,
  MATCH_DATA_TAB_WISE_AXIOS,
  MATCH_DATA_TAB_WISE,
} from '../../api/queries'
import Loading from '../../components/loading'

const componentList = {
  articles: dynamic(() =>
    import(`../../components/match-score/matchDetails/articlesTab`),
  ),
  scorecard: dynamic(() =>
    import(`../../components/match-score/matchDetails/scorecardTab`),
  ),
  commentary: dynamic(() =>
    import(`../../components/match-score/matchDetails/commentryTab`),
  ),
  matchinfo: dynamic(() =>
    import(`../../components/match-score/matchDetails/matchInfoTab`),
  ),
  summary: dynamic(() =>
    import('../../components/match-score/matchDetails/scoresummry'),
  ),
  highlights: dynamic(() =>
    import(`../../components/match-score/matchDetails/highlightsTab`),
  ),
  playing11: dynamic(() =>
    import(`../../components/match-score/matchDetails/playingXITab`),
  ),
}

/**
 * @description Live and Upcoming match view
 * @route /live-score/matchID/tab/matchName
 * @example /live-score/198323/summary/three-team-50-over-competition-2020
 * @param { String } status
 * @param { Object } matchData
 * @param { String } matchID
 * @param { Object } responseData
 * @param { Object } series
 * @param { Object } props
 */

// async function getServerData(id, status, series) {
//   let conditionalData = ''
//   // to check if live match is completed but url is indexed then make sure it wont crash on summary
//   let restrictedTab = ['summary']
//   if (restrictedTab.includes(status)) {
//     status = 'commentary'
//   }

//   const matchScoreCardData = await axios.post(process.env.API, {
//     query: MATCH_DATA_FOR_SCORECARD_AXIOS,
//     variables: { matchID: id },
//     pollInterval: 5000,
//   })
//   console.log(matchScoreCardData, 'hhhhhhhhhhhh---')
//   let newmatch = await axios.post(process.env.API, {
//     query: MATCH_DATA_TAB_WISE_AXIOS,
//     variables: { matchID: id },
//   })
//   switch (status) {
//     case 'highlights':
//     case 'articles':
//     case 'playing11':
//     case 'commentary':
//     case 'matchinfo':
//     case 'scorecard':
//       conditionalData = await axios.post(process.env.API, {
//         query: GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
//         variables: { type: 'matches', Id: id },
//       })
//       break
//     case 'summary':
//       conditionalData = await axios.post(process.env.API, {
//         query: MATCH_SUMMARY_AXIOS,
//         variables: { matchID: id, status: 'completed' },
//       })
//       break
//     default:
//       conditionalData = { data: [] }
//       break
//   }

//   const [
//     { data: matchData },
//     { data: conditionalResponse },
//   ] = await Promise.all([matchScoreCardData, conditionalData])

//   return {
//     data: matchData?.data,
//     matchID: id,
//     status,
//     series,
//     responseData: conditionalResponse ? conditionalResponse.data : [],
//     newData: newmatch?.data?.data,
//   }
// }

export default function MatchDetails({ params }) {
  const matchID = params?.slug?.[0]
  const id = params?.slug?.[0]
  const status = params?.slug?.[1]
  const series = params?.slug?.[2]
  let Component = componentList[status]
  let { loading, error, data } = useQuery(MATCH_DATA_FOR_SCORECARD, {
    variables: { matchID: matchID },
    pollInterval: 8000,
    fetchPolicy: 'network-only',
  })
  let { data: matchInfo } = useQuery(MATCH_DATA_TAB_WISE, {
    variables: { matchID: matchID },
  })
  // const run = useRef(0)

  // useEffect(() => {
  //   getServerData(id, status, series).then((res) => {
  //     setProps(res)
  //   })
  //   // run.current++
  // }, [windowSize])

  // const componentData = props?.responseData ? props?.responseData : []
  // useEffect(() => {
  //   window.scrollTo({ top: 0, behavior: 'smooth' })
  // }, [])
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])
  switch (status) {
    case 'highlights':
    case 'articles':
    case 'commentary':
    case 'matchinfo':
    case 'videos':
    case 'scorecard':
      // conditionalData = axios.post(process.env.API, {
      //   query: GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
      //   variables: { type: 'matches', Id: id },
      // })
      break
    case 'summary':
      // conditionalData = axios.post(process.env.API, {
      //   query: MATCH_SUMMARY_AXIOS,
      //   variables: { matchID: id, status: 'completed' },
      // })
      break
  }
  if (!data) {
    return <Loading />
  }
  if (data) {
    return (
      <Layout data={data} status={status} matchID={matchID} params={params}>
        {/* {props.urlTrack ? <Urltracking PageName="Live-score" /> : null} */}
        <Component
          // match={props?.data?.miniScoreCard?.data?.[0]}
          // matchType={props?.data?.miniScoreCard?.data?.[0]?.matchType}
          // status={props?.data?.miniScoreCard?.data?.[0]?.matchStatus}
          isDetails={true}
          // matchData={props?.data?.miniScoreCard?.data?.[0]}
          matchID={matchID}
          matchData={data?.miniScoreCard?.data?.[0]}
          matchStatus={data?.miniScoreCard?.data?.[0]?.matchStatus}
          data={matchInfo}
          pollDugout={true}
          newData={matchInfo}
          polling={true}
          params={params}
        />
      </Layout>
    )
  }
}
