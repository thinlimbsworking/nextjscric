'use client'
import React, { useEffect, useState, useRef } from 'react'
import Head from 'next/head'

import Getotp from '../../app/components/login/getotp'
import ConfirmOtp from '../../app/components/login/confirmotp'
import Profile from '../../app/components/login/profile'

import Merge from '../../components/login/merge'
import { useRouter } from 'next/navigation'
import CleverTap from 'clevertap-react'
// import { getItem, removeItem } from '../../services/authService'
// import CircleNews from '../../components/news/circle';
const IMAGES = {
  EMPTY: '/svgs/Empty.svg',
  backIconWhite: '/svgs/backIconWhite.svg',
}

export default function Login({ ...props }) {
  let router = useRouter()
  let navigate = router.push
  const [step, setStep] = useState('Getotp')
  const [number, setMobnum] = useState('')
  const [email, setEmail] = useState('')

  const [loginStatus, setLoginStatus] = useState()
  const [checkStat, setCheckStat] = useState()

  const [emailLogin, setEmailLogin] = useState(false)
  const [showMerge, setSHowMerge] = useState(false)

  const [cleverTapObject, updateClevertap] = useState(null)
  const [windowObject, updateWindowObject] = useState(null)

  const [showMergeMSG, setSHowMergeMSG] = useState('merge')

  const [remainingTimeMerge, setRemainingTimeMerge] = useState(5)

  const [session, setSession] = useState('')
  useEffect(() => {
    setLoginStatus(localStorage.getItem('tokenData') == null ? false : true)
  })

  const timeoutMerge = useRef()

  useEffect(() => {
    if (timeoutMerge.current && remainingTimeMerge === 0) {
      if (
        localStorage.getItem('emailotp') == 'true' &&
        localStorage.getItem('photp') == 'true' &&
        localStorage.getItem('mergeCheck') == 'false'
      ) {
        setSHowMerge(true)
      }
    } else {
      timeoutMerge.current = setTimeout(() => {
        setRemainingTimeMerge((t) => t - 1)
      }, 1000)
    }
    return () => {
      clearTimeout(timeoutMerge.current)
    }
  }, [remainingTimeMerge])

  useEffect(() => {
    updateClevertap({ ...CleverTap })
    updateWindowObject({ ...global.window })
  }, [])

  useEffect(() => {
    if (
      router.asPath == '/fantasy-research-center/upcoming' ||
      router.pathname == '/live-score/[...slugs]'
    ) {
      if (cleverTapObject) {
        cleverTapObject.event('Login', {
          Source:
            router.asPath == '/fantasy-research-center/upcoming'
              ? 'FRC'
              : 'scorecard',
          Platform: localStorage ? localStorage.Platform : '',
        })
      }
    }
  }, [cleverTapObject, windowObject])

  useEffect(() => {
    document.body.style.overflow = 'hidden'
    localStorage?.getItem('tokenData') == null ? null : null
    return () => {
      document.body.style.overflow = 'unset'
    }
  }, [])

  const signOut = () => {
    CleverTap.initialize('Signout', {
      Source: 'Profile',
      Platform: localStorage ? localStorage.Platform : '',
    })
    setCheckStat('2')
    setStep('Getotp')
    setLoginStatus(true)
    localStorage.removeItem('tokenData')
    localStorage.removeItem('userName')
    localStorage.removeItem('userNumber')
    localStorage.removeItem('userEmail')
    localStorage.removeItem('mergecheck')
    localStorage.removeItem('photp')
    localStorage.removeItem('emailotp')
  }

  return (
    <div
      className="flex justify-center items-center fixed absolute--fill z-50 bg-black-20 text-white"
      style={{ backdropFilter: 'blur(10px)' }}
    >
      <div className=" mx-3  w-full flex flex-col items-center justify-center lg:w-4/12 md:w-4/12 bg-gray rounded-lg pb-10  px-6">
        <div className="flex  justify-end mt-3  z-9999 w-full ">
          <img
            className="h-4 w-4 cursor-pointer br-100 "
            alt="close icon"
            src={'/pngs/closeIcon.png'}
            onClick={() => {
              if (props.onCLose !== '/slug') {
                props.onCLose === '/more' ||
                props.onCLose === '/fantasy-research-center' ||
                props.onCLose === '/match-score' ||
                props.onCLose === '/play-the-odd'
                  ? props.setShowLogin(false)
                  : (window.location = '/')
              } else {
                window.location = '/fantasy-research-center'
              }
            }}
          />
        </div>
        <div
          className={
            showMerge ? 'shadow-4 rounded-xl ' : '  shadow-4 w-100 rounded-xl'
          }
        >
          <div
            className={
              router.asPath !== '/login'
                ? ' hidescroll flex  justify-center items-center    py-3 '
                : 'hidescroll flex  justify-center items-center  px-2  py-3 '
            }
          >
            <>
              <title>Cricket Profile | Login | cricket.com</title>
              <meta
                name="description"
                content="Login at cricket.com and check out the Fantasy  Research Center. Create your fantasy team and watch fantasy previews for upcoming matches."
              />

              <meta
                property="og:title"
                content="Cricket Profile | Cricket.com"
              />
              <meta
                property="og:description"
                content="Login at cricket.com and check out the Fantasy  Research Center. Create your fantasy team and watch fantasy previews for upcoming matches."
              />
              <meta property="og:url" content="https://www.cricket.com/login" />

              <link rel="canonical" href="https://www.cricket.com/login" />
            </>

            {props.navTo == '/more' && (
              <div className="dn-l   fixed top-0 left-0 right-0 flex items-center pa3  white f5 fw6 z-2 dn-l">
                <Image
                  height={18}
                  width={18}
                  loading="lazy"
                  className="mr2 cursor-pointer"
                  onClick={() => {
                    props.navTo == '/more'
                      ? props.setShowLogin(false)
                      : navigate(props.navTo)
                  }}
                  src={IMAGES.backIconWhite}
                  alt="back icon"
                />
                <span className="fw6 f5 white">
                  {' '}
                  {props.name ? 'Fantasy Research Center' : 'Login'}{' '}
                </span>
              </div>
            )}

            {!loginStatus ? (
              <>
                {step == 'Getotp' && (
                  <>
                    <Getotp
                      setSHowMergeMSG={setSHowMergeMSG}
                      showMergeMSG={showMergeMSG}
                      onCLose={props.onCLose}
                      modalText={props.modalText}
                      setSession={setSession}
                      number={number}
                      session={session}
                      setShowLogin={props.setShowLogin}
                      setStep={setStep}
                      setMobnum={setMobnum}
                      setEmailLogin={setEmailLogin}
                      emailLogin={emailLogin}
                      email={email}
                      setEmail={setEmail}
                      setSHowMerge={setSHowMerge}
                      showMerge={showMerge}
                    />
                  </>
                )}
                {step == 'ConfirmOtp' && (
                  <>
                    <ConfirmOtp
                      setSHowMergeMSG={setSHowMergeMSG}
                      showMergeMSG={showMergeMSG}
                      navTo={props.navTo}
                      modalText={props.modalText}
                      setShowLogin={props.setShowLogin}
                      setSession={setSession}
                      session={session}
                      step={step}
                      setStep={setStep}
                      emailLogin={emailLogin}
                      ShowLoginSucess={props.ShowLoginSucess}
                      number={number}
                      email={email}
                      direct={props.direct}
                      setEmail={setEmail}
                      setSHowMerge={setSHowMerge}
                      showMerge={showMerge}
                    />
                  </>
                )}
              </>
            ) : (
              <>
                {!showMerge ? (
                  step == 'checkemail' || step == 'checkphone' ? (
                    <ConfirmOtp
                      setSHowMergeMSG={setSHowMergeMSG}
                      showMergeMSG={showMergeMSG}
                      navTo={props.navTo}
                      modalText={props.modalText}
                      setShowLogin={props.setShowLogin}
                      setSession={setSession}
                      session={session}
                      step={step}
                      setStep={setStep}
                      ShowLoginSucess={props.ShowLoginSucess}
                      number={number}
                      emailLogin={emailLogin}
                      email={email}
                      setEmail={setEmail}
                      setSHowMerge={setSHowMerge}
                      showMerge={showMerge}
                    />
                  ) : (
                    <Profile
                      setSHowMergeMSG={setSHowMergeMSG}
                      showMergeMSG={showMergeMSG}
                      onCLose={props.onCLose}
                      step={step}
                      setStep={setStep}
                      setShowLogin={props.setShowLogin}
                      setLoginStatus={setLoginStatus}
                      modalText={props.modalText}
                      setSession={setSession}
                      number={number}
                      session={session}
                      setEmail={setEmail}
                      setMobnum={setMobnum}
                      setEmailLogin={setEmailLogin}
                      emailLogin={emailLogin}
                      email={email}
                      setSHowMerge={setSHowMerge}
                      showMerge={showMerge}
                    />
                  )
                ) : (
                  <Merge
                    setSHowMergeMSG={setSHowMergeMSG}
                    showMergeMSG={showMergeMSG}
                    onCLose={props.onCLose}
                    step={step}
                    setStep={setStep}
                    setShowLogin={props.setShowLogin}
                    setLoginStatus={setLoginStatus}
                    modalText={props.modalText}
                    setSession={setSession}
                    number={number}
                    session={session}
                    setEmail={setEmail}
                    setMobnum={setMobnum}
                    setEmailLogin={setEmailLogin}
                    emailLogin={emailLogin}
                    email={email}
                    setSHowMerge={setSHowMerge}
                    showMerge={showMerge}
                  />
                )}
              </>
            )}
          </div>
          {loginStatus && (
            <div className="bottom-8 flex justify-center">
              <span
                onClick={signOut}
                className="cursor-pointer text-red text-center"
              >
                Sign Out
              </span>
            </div>
          )}
        </div>
      </div>

      {/* {loginStatus && (
        <div
          className="cursor-pointer text-red absolute bottom-56"
          onClick={signOut}
        >
          Sign Out
        </div>
      )} */}
    </div>
  )
}
