'use client'
import React, { useState, useEffect, useRef, Suspense } from 'react'
import Loading from '../../components/loading'
import dynamic from 'next/dynamic'
import { useRouter, useSearchParams } from 'next/navigation'
import axios from 'axios'
import Layout from '../../components/match-score/matchDetails/matchdetailLayout'
import {
  MATCH_DATA_FOR_SCORECARD_AXIOS,
  GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
  MATCH_SUMMARY_AXIOS,
  DUGOUT_AXIOS,
  MATCH_DATA_TAB_WISE_AXIOS,
  GET_SCORE_CARD_AXIOS,
  MATCH_DATA_FOR_SCORECARD,
} from '../../api/queries'
import MiniScoreCard from '../../components/match-score/matchDetails/miniScoreCard'
import { useQuery } from '@apollo/client'
// import Urltracking from '../../components/Common/urltracking';

const componentList = {
  articles: dynamic(() =>
    import(`../../components/match-score/matchDetails/articlesTab`),
  ),
  // highlights: dynamic(() => import(`../../components/match-score/matchDetails/highlightsTab`)),
  scorecard: dynamic(() =>
    import(`../../components/match-score/matchDetails/scorecardTab`),
  ),
  // live: dynamic(() => import(`../../components/match-score/matchDetails/matchLiveTab`)),
  commentary: dynamic(() =>
    import(`../../components/match-score/matchDetails/commentryTab`),
  ),
  matchinfo: dynamic(() =>
    import(`../../components/match-score/matchDetails/matchInfoTab`),
  ),
  summary: dynamic(() =>
    import('../../components/match-score/matchDetails/scoresummry'),
  ),
  videos: dynamic(() =>
    import('../../components/match-score/matchDetails/matchVideosTab'),
  ),
  highlights: dynamic(() =>
    import(`../../components/match-score/matchDetails/highlightsTab`),
  ),
  // playing11: dynamic(() =>
  //   import(`../../components/match-score/matchDetails/playingXITab`),
  // ),

  // dugout: dynamic(() => import('../../components/match-score/matchDetails/dugout'))
}

/**
 * @description Completed match view
 * @route /match-score/matchID/tab/matchName
 * @example /match-score/198323/summary/three-team-50-over-competition-2020
 * @param { String } status
 * @param { Object } data
 * @param { String } matchID
 * @param { Object } responseData
 * @param { Object } series
 * @param { Object } props
 */

// async function getServerData(id, status, series) {
//   try {
//     let conditionalData = ''
//     const matchScoreCardData = axios.post(process.env.API, {
//       query: MATCH_DATA_FOR_SCORECARD_AXIOS,
//       variables: { matchID: id },
//     })
//     let matchinfo = await axios.post(process.env.API, {
//       query: GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
//       variables: { type: 'matches', Id: id },
//     })
//     let newmatch = await axios.post(process.env.API, {
//       query: MATCH_DATA_TAB_WISE_AXIOS,
//       variables: { matchID: id },
//     })

//     switch (status) {
//       // case 'highlights':
//       case 'articles':
//       case 'commentary':
//       case 'matchinfo':
//       case 'videos':
//       case 'scorecard':
//         conditionalData = axios.post(process.env.API, {
//           query: GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
//           variables: { type: 'matches', Id: id },
//         })
//         break
//       case 'summary':
//         // conditionalData = axios.post(process.env.API, {
//         //   query: MATCH_SUMMARY_AXIOS,
//         //   variables: { matchID: id, status: 'completed' },
//         // })
//         break
//       default:
//         conditionalData = { data: [] }
//         break
//     }

//     const [
//       { data: matchData },
//       { data: conditionalResponse },
//     ] = await Promise.all([matchScoreCardData, conditionalData])
//     console.log(matchData, 'oooouu')
//     return {
//       data: matchData.data,
//       matchID: id,
//       status,
//       series,
//       responseData: conditionalResponse.data ? conditionalResponse.data : [],
//       matchInfo: matchinfo.data.data,
//       newData: newmatch.data.data,
//     }
//   } catch (e) {
//     console.error(e)
//     throw e
//   }
// }

export default function MatchDetails({ params }) {
  const [props, setProps] = useState()
  // const [mounted, setMounted] = useState(false)
  const matchID = params?.slug?.[0]
  const id = params?.slug?.[0]
  const status = params?.slug?.[1]
  const series = params?.slug?.[2]
  const router = useRouter()
  console.log(params, 'pp-->>')
  const Component = componentList[status]
  const { data } = useQuery(MATCH_DATA_FOR_SCORECARD, {
    variables: { matchID: id },
  })
  // setProps(data)
  // const run = useRef(0)
  // useEffect(() => {
  //   // setMounted(true)
  //   console.log('before API call')
  //   getServerData(id, status, series).then((res) => {
  //     setProps(res)
  //     console.log('after API call')
  //     // setMounted(false)
  //   })
  // }, [])
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])
  switch (status) {
    // case 'highlights':
    case 'articles':
    case 'commentary':
    case 'matchinfo':
    case 'videos':
    case 'scorecard':
      // conditionalData = axios.post(process.env.API, {
      //   query: GET_ARTICLES_MATCH_INFO_SCORECARD_AXIOS,
      //   variables: { type: 'matches', Id: id },
      // })
      break
    case 'summary':
      // conditionalData = axios.post(process.env.API, {
      //   query: MATCH_SUMMARY_AXIOS,
      //   variables: { matchID: id, status: 'completed' },
      // })
      break
  }
  const componentData = props?.responseData ? props.responseData : []

  if (router.isFallback) {
    return <Loading />
  }

  if (!componentData) {
    return <Loading />
  }

  if (data) {
    return (
      <>
        <Layout data={data} status={status} matchID={matchID} params={params}>
          {/* {props.urlTrack ? <Urltracking PageName='Match-Score' /> : null} */}

          <Component
            // match={props?.data?.miniScoreCard?.data[0]}
            // matchType={props?.data?.miniScoreCard?.data[0]?.matchType}
            // status={props?.data?.miniScoreCard?.data[0].matchStatus}
            isDetails={true}
            // matchData={props?.data?.miniScoreCard?.data[0]}
            matchID={matchID}
            matchStatus={data?.miniScoreCard?.data?.[0]?.matchStatus}
            // data={componentData}
            live={false}
            // matchInfo={props?.matchInfo}
            // newData={props?.newData}
            pollDugout={false}
            params={params}
            props={props}
          />
        </Layout>
      </>
    )
  }
}
