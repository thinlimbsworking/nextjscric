'use client'
import React, { useState } from 'react'
import version from '../../version.json'
import Link from 'next/link'
import AppDownload from '../components/commom/appdownload'
import axios from 'axios'
import BottomNavBar from '../components/commom/bottomnavbar'
import Login from '../login/page'
import CleverTap from 'clevertap-react'
import ImageWithFallback from '../components/commom/Image'

let seriesIcon = '/moreSvgs/seriesIcon.svg'
let playerIcon = '/moreSvgs/playerIcon.svg'
let teamIcon = '/moreSvgs/teamIcon.svg'
let arhiveIcon = '/svgs/archives.svg'
let winner = '/svgs/whoWasWinner.svg'

let stadiumIcon = '/moreSvgs/stadiumIcon.svg'
let videosIcon = '/moreSvgs/videosIcon.svg'
let rankingsIcon = '/moreSvgs/rankingsIcon.svg'
let recordsIcon = '/moreSvgs/recordsIcon.svg'
let statAttackMore = '/moreSvgs/stat-attack-more.svg'
let ProfileIcon = '/moreSvgs/menuProfile.svg'
let crictecIcon = '/moreSvgs/crictecIcon.svg'
let season_fantasy = '/svgs/morefantasy.svg'
import {
  SEASON_FANTASY_LOGIN,
  SEASON_FANTASY_LOGIN_AXIOS,
} from '../../api/queries'
import MetaDescriptor from '../../components/MetaDescriptor'
import { INDEX } from '../../constant/MetaDescriptions'
// import { Svg } from 'react-native-svg';
const playtheodds = '/svgs/mobileplayodd.svg'

export default function More() {
  const pages = [
    [
      // {
      //   asPath: '/season-fantasy',
      //   path: `/season-fantasy`,
      //   icon: season_fantasy,
      //   name: 'Season Fantasy',
      //   CTEventName: 'SeasonFantasy'
      // },
      {
        asPath: '/series/ongoing/all',
        path: '/series/ongoing/all',
        icon: seriesIcon,
        name: 'Series',
        CTEventName: 'SeriesHome',
      },
      // {
      //   path: '/archives/[slug]',
      //   asPath: `/archives/${new Date().getFullYear()}`,
      //   icon: arhiveIcon,
      //   name: 'Archives',
      //   CTEventName: 'archives'
      // }
    ],
    [
      {
        path: '/players/[slug]',
        asPath: '/players/all',
        icon: playerIcon,
        name: 'Players',
        CTEventName: 'PlayerDiscovery',
      },
      {
        path: '/teams/[slug]',
        asPath: '/teams/international',
        icon: teamIcon,
        name: 'Teams',
        CTEventName: 'TeamDiscovery',
      },
      {
        path: '/stadiums',
        asPath: '/stadiums',
        icon: stadiumIcon,
        name: 'Stadiums',
        CTEventName: 'StadiumsDiscovery',
      },
    ],
    [
      {
        path: '/videos/[type]',
        asPath: '/videos/latest',
        icon: videosIcon,
        name: 'Videos',
        CTEventName: 'VideosHome',
      },
    ],
    [
      {
        path: '/rankings',
        asPath: '/rankings',
        icon: rankingsIcon,
        name: 'Rankings',
        CTEventName: 'Rankings',
      },
      {
        path: '/records',
        asPath: '/records',
        icon: recordsIcon,
        name: 'Records',
        CTEventName: 'Records',
      },
    ],
    // [
      // {
      //   path: '/stat-attack/start-game',
      //   icon: statAttackMore,
      //   name: 'Stat Attack',
      //   CTEventName: 'StatAttackHome',
      // },
      // {
      //   path: '/who-was-the-winner',
      //   icon: winner,
      //   name: 'Who Was The Winner',
      //   CTEventName: 'StatAttackHome',
      // },
      // {
      //   path: '/play-the-odd',
      //   icon: winner,
      //   name: 'Play The Odds',
      //   CTEventName: 'PlayTheOdds',
      // },
    // ],
    [
      {
        path: '/login',
        asPath: '/login',
        icon: ProfileIcon,
        name: 'Profile',
        CTEventName: 'Login',
      },
      {
        path: '/about',
        asPath: '/about',
        icon: crictecIcon,
        name: 'About cricket.com',
      },
      // {
      //   asPath: '/playtheodds/login',
      //   path:'/playtheodds/login',
      //   icon: playtheodds,
      //   name: 'Play the odds',
      //   CTEventName: 'playtheodds'
      // },
    ],
  ]

  const handleNavigation = (page) => {
    if (page.path === '/xxxx') {
      setShowLogin(true)
    } else {
      //   if (page.CTEventName) {
      //     CleverTap.initialize(page.CTEventName, {
      //       Source: "More",
      //       Platform: global.window.localStorage.Platform,
      //     });
      //   }
    }
  }

  let osName =
    typeof window !== 'undefined' &&
    window.navigator.userAgent.indexOf('Android') > -1
      ? 'Android'
      : 'ios'
  const [show, setShowLogin] = useState(false)
  return (
    <div className="pb-10">
      {show && (
        <Login
          onCLose={'/more'}
          navTo={'/more'}
          entryRoute={'more'}
          sucessPath={'/more'}
          setShowLogin={setShowLogin}
          name={false}
          modalText="Login / Register to Cricket.com"
          ShowLoginSucess={true}
        />
      )}
      <div
        className="px-2 
      "
      >
        <div className="bg-gray-8 p-4 flex items-center justify-between text-white">
          <div className="text-md text-white font-semibold">More</div>

          <div className="db text-right  text-xs ">
            v{version.isDev ? version.dev.number : version.prod.number}
            {version.isDev ? ' - Staging' : ''}
          </div>
        </div>
        <div>
        {pages &&
          pages.map((tags, i) => (
           <div
              key={i}
              className="bg-gray-8 overflow-hidden shadow-4 text-white text-sm font-medium border-b border-b-gray-700 last:border-none mt-2"
            >
              {tags.map((page, i) =>
                page.asPath !== '/playtheodds/login' ? (
                  <React.Fragment key={i}>
                    <Link
                      key={i}
                      href={page.path != '/zzz' ? page.path : '/more'}
                      as={page.asPath != '/zzz' ? page.asPath : '/more'}
                      passHref
                    >
                      <div
                        className=" px-2 py-2 pl-1 cursor-pointer flex  justify-between items-center"
                        onClick={() => handleNavigation(page)}
                      >
                        <span className="flex justify-center items-center">
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            src={page.icon}
                            alt=""
                            className="mr-3 ml-1 "
                            style={{ width: 16, height: 16, fill: 'yellow' }}
                          />

                          <div className="cursor-pointer">{page.name}</div>
                        </span>

                        {/* <img className='w-2 ' width='10' alt='' src='/svgs/RightSchevronBlack.svg' /> */}
                        <svg width="30" viewBox="0 0 24 24">
                          <path
                            fill="#38d925"
                            d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                      
                    </Link>
                  </React.Fragment>
                ) : (
                  <div
                    className=" px-2 py-3 pl-1 cursor-pointer flex justify-between items-center"
                    onClick={async () => {
                      if (
                        global.window.localStorage.getItem('tokenData') !== null
                      ) {
                        const partnershipData = await axios.post(
                          'https://apiv2.cricket.com/cricket',
                          {
                            query: SEASON_FANTASY_LOGIN_AXIOS,
                            variables: {
                              token: global.window.localStorage.getItem(
                                'tokenData',
                              ),
                            },
                          },
                        )
                        if (
                          partnershipData.data.data.sessionFantasyToken.code ==
                          200
                        ) {
                          window.location.href = `http://playtheodds.cricket.com/?uniqueUserId=${partnershipData.data.data.sessionFantasyToken.message}`
                        }
                      } else {
                        window.location.href = '/playtheodds/login'
                      }
                    }}
                  >
                    <span className="flex justify-center items-center">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        src={page.icon}
                        alt=""
                        className="mr-3 ml-1 "
                        style={{ width: 16, height: 16, fill: 'yellow' }}
                      />
                      <div className="cursor-pointer  ">{page.name}</div>
                    </span>
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className="w-2 "
                      alt=""
                      src="/svgs/RightSchevronBlack.svg"
                    />
                  </div>
                ),
              )}
              <MetaDescriptor section={INDEX()} />
            </div>
          ))}
        </div>
        {osName === 'ios' ? (
          <AppDownload
            isAndroid={false}
            isMore={true}
            url="https://cricketapp.app.link/fhsnBX9Nf4"
          />
        ) : (
          <AppDownload
            isAndroid={true}
            isMore={true}
            url="https://cricketapp.app.link/fhsnBX9Nf4"
          />
        )}
        <div className="mt-4">
          <BottomNavBar />
        </div>
      </div>
    </div>
  )
}
