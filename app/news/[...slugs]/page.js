'use client'

import axios from 'axios'
import React, { useEffect, useRef, useState } from 'react'
import { ARTICLE_LIST_AXIOS, LIVE_BLOGGING_AXIOS } from '../../api/queries'
import { useRouter } from 'next/navigation'
import { useQuery } from '@apollo/react-hooks'
import AuthorBlog from '../../components/news/authorBlog'
import LiveBlog from '../../components/news/liveBlog'

async function getServerData(type) {
  if (type !== 'authors') {
    const res = await axios.post(process.env.API, {
      query: LIVE_BLOGGING_AXIOS,
      variables: { articleID: type },
    })
    return {
      data: res.data.data,
      type,
      articleSubType: 'Live',
    }
  }
  if (type == 'authors') {
    const res = await axios.post(process.env.API, {
      query: ARTICLE_LIST_AXIOS,
      variables: {
        type: 'news',
        page: 0,
      },
    })
    return {
      type: 'authors',
      data: res.data.data,
    }
  }
}

export default function Live({ params }) {
  const router = useRouter()
  const [props, setProps] = useState({})

  const type = params.slugs[3]

  // const run = useRef(0)
  useEffect(() => {
    getServerData(type).then((res) => setProps(res))
  }, [])

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  if (router.isFallback) {
    return <Loading />
  } else {
    return (
      <>
        {' '}
        {type == 'authors' ? (
          <AuthorBlog props={props} params={params} />
        ) : (
          <LiveBlog
            type={type}
            data={props.data}
            articleSubType={props.articleSubType}
            params={params}
          />
        )}
      </>
    )
  }
}

// Live.getInitialProps = async ({ query, ...context }) => {
// export async function getServerSideProps(context) {
//   if (context.params.slugs[0] !== 'authors') {
//     let type = context.params && context.params.slugs[3]

//     const { data } = await axios.post(process.env.API, {
//       query: LIVE_BLOGGING_AXIOS,
//       variables: { articleID: type },
//     })

//     if (data.errors) {
//       throw data.errors
//     }

//     return {
//       props: {
//         data: data,
//         type,

//         articleSubType: 'Live',
//       },
//     }
//   }
//   if (context.params.slugs[0] == 'authors') {
//     const { data } = await axios.post(process.env.API, {
//       query: ARTICLE_LIST_AXIOS,
//       variables: { type: 'news', page: 0 },
//     })

//     return {
//       props: { type: 'authors', data: data.data },
//     }
//   }
// }
