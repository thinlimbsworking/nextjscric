'use client'
import axios from 'axios'
import CleverTap from 'clevertap-react'
import dynamic from 'next/dynamic'
import { usePathname, useRouter } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import {
  ARTICLE_LIST_AXIOS,
  GET_ARTICLE_DETAILS,
  SERIES_NAME_FOR_ARTICLES,
} from '../../api/queries'
import {
  getArticleTabUrl,
  getLiveArticleUrl,
  getNewsUrl,
} from '../../api/services'
import { useQuery, useLazyQuery } from '@apollo/client'

import ImageWithFallback from '../../components/commom/Image'
import Urltracking from '../../components/commom/urltracking'
import Loading from '../../components/loading'
import CommonDropdown from '../../components/commom/commonDropdown'
import NewsSocialTracker from '../../components/news/newsSocialTracker'
import Heading from '../../components/shared/heading'
const backIconWhite = '/svgs/backIconWhite.svg'

const ground = '/svgs/groundImageWicket.png'

const NewsView = dynamic(() => import('../../components/article.js'), {
  // loading: () => <Loading />,
})

const NewsList = dynamic(() => import('../../components/newsdetailsLayout'), {
  // loading: () => <Loading />,
})

const LiveBlog = dynamic(() => import('../../components/liveblogging'), {
  // loading: () => <Loading />,
})
function dynamicImport(isListing) {
  if (isListing === 'liveBlog') {
    return LiveBlog
  } else {
    if (isListing) {
      return NewsList
    } else {
      return NewsView
    }
  }
}

async function getServerData(type) {
  let category = ['latest', 'news', 'on-this-day', 'match-related', 'features']
  if (category.includes(type)) {
    const res = await axios.post(process.env.API, {
      query: ARTICLE_LIST_AXIOS,
      variables: { type: type, page: 0 },
    })
    // console.log('artcllist -- ', res)
    return {
      data: res.data.data,
      type,
      isListing: true,
      articleSubType: 'NewsList',
    }
  } else {
    const res = await axios.post(process.env.API, {
      query: GET_ARTICLE_DETAILS,
      variables: { articleID: type },
    })
    return {
      data: res.data.data,
      isListing: false,
      articleSubType: 'Not Live',
    }
  }
}

export default function News({ params }) {
  const [desktopMode, setDesktopMode] = useState(false)
  const [articlesFilter, setArticlesFilter] = useState([
    { valueLable: 'ALL', valueId: '' },
  ])
  const [activeTabIndex, setActiveTabIndex] = useState(0)
  let filterTemp = []
  const [activefilter, setActivefilter] = useState({
    valueLable: 'All',
    valueId: '',
  })
  let pathname = usePathname()

  // console.log( 'checkingthis')
  const { loading, error, data: data2 } = useQuery(SERIES_NAME_FOR_ARTICLES, {
    onCompleted: (data) => {
      let dumTab = [{ valueLable: 'All', valueId: '' }]

      data &&
        data.getSeriesListForArticles.seriesListWithIds.map((item, index) => {
          dumTab.push({ valueId: item.tourID, valueLable: item.tourName })
        })
      setArticlesFilter(dumTab)
    },
  })

  useEffect(() => {
    if (window.innerWidth > 700) {
      setDesktopMode(true)
    }
  }, [activefilter])

  const router = useRouter()
  const [props, setProps] = useState({})
  const type = params.slug

  useEffect(() => {
    getServerData(type).then((res) => setProps(res))
    // run.current++
  }, [activefilter])

  const tab =
    type === 'on-this-day'
      ? 'onthisday'
      : type === 'match-related'
      ? 'matchrelated'
      : type
      ? type
      : 'news'
  const tabs = ['latest', 'news', 'matchrelated', 'features']
  const tabsConfig = {
    latest: 'latest',
    news: 'news',
    // IPL: 'ipl',
    'Match Related': 'matchrelated',
    features: 'features',
  }

  let data = props.data

  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'NewsHome',
      // MatchID: String(article.matchIDs),
      // SeriesID: String(article.seriesIDs),
      // PlayerID: String(article.playerIDs),
      ArticleType: article.type,
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: global.window ? global.window.localStorage.Platform : '',
    })
  }

  const getTabUrl = (tabName) => {
    let tab =
      tabName === 'IPL'
        ? 'ipl'
        : tabName === 'Match Related'
        ? 'match-related'
        : tabsConfig[tabName]

    return getArticleTabUrl(tab)
  }
  let Component = dynamicImport(
    props.articleSubType === 'NewsList'
      ? props.isListing
      : props.articleSubType === 'Not Live'
      ? props.isListing
      : 'liveBlog',
  )
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  if (loading)
    return (
      <div className="bg-white py-2 shadow-4">
        <Loading />
      </div>
    )
  if (data) {
    return (
      <div className="flex w-full">
        {pathname?.length <= 12 ? (
          <span className="text-lg fixed w-full z-10 lg:hidden md:hidden bg-gray-8 text-white lg:text-2xl flex items-center justify-start lg:py-5 lg:px-0 font-semibold p-3">
            <img
              src={backIconWhite}
              alt="back icon"
              className="bg-gray-8 rounded-md p-2 ga"
              onClick={() => window?.history?.back()}
            />
            <p>News & Articles</p>
          </span>
        ) : (
          ''
        )}

        <div
          className={`flex flex-col ${
            tab && tab.length < 10 ? 'md:w-8/12 w-full dark:mt-14' : 'w-full'
          } md:pt-4 `}
        >
          {tab && tab.length < 10 && (
            <div className="dark:mt-0.5">
              <CommonDropdown
                options={articlesFilter || []}
                setActiveTabIndex={setActiveTabIndex}
                activeTabIndex={activeTabIndex}
                isFixedHeight
                onChange={(option) => setActivefilter(option)}
                defaultSelected={activefilter}
                placeHolder={activefilter?.valueLable}
                showExpandIcon
                variant="outline"
                isNews
              />
            </div>
          )}

          {activefilter && (
            <Component
              key={activefilter.valueId || 'yyy'}
              filter={activefilter}
              tabs={tabs}
              getTabUrl={getTabUrl}
              handleCleverTab={handleCleverTab}
              getNewsUrl={getNewsUrl}
              tabsConfig={tabsConfig}
              News_Live_Tab_URL={getLiveArticleUrl}
              tab={tab}
              data={data}
              params={params}
            />
          )}
        </div>

        {tab && tab.length < 10 && desktopMode && (
          <div className="md:w-4/12 pt-4">
            <div className="flex-col text-black ml-4 mt-1 hidden md:block">
              <div className="hidden lg:block md:block xl:block">
                <Heading heading={'Social Tracker'} noPad />
              </div>
              <div className=" overflow-scroll h-[80rem] ">
                <NewsSocialTracker />
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}
