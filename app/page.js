'use client'
import { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
const ClientComponent = dynamic(() =>
  import('./clinentcomponent', { ssr: false }),
)
const HomeNewsVideo = dynamic(() =>
  import('./components/clinet/homePAge/homeNewsVideo', { ssr: false }),
)
const SideBarMain = dynamic(() =>
  import('./components/clinet/sideBar/sideBarMain', { ssr: false }),
)
// import  { Metadata } from 'next';
// import Datadigest from "./components/datadigest";

// dynamic(() => import('./TestimonialCard'), { ssr: false })
// const Datadigest =   dynamic(() => import("./components/datadigest", { ssr: false }) )

// import "swiper/css";
// import { Swiper, SwiperSlide } from 'swiper/react';

// import ServerComponent from "./servercomponent";
// import Main from './components/clinet/main'
// import MainServerComponet from './components/servercomponent/homepage'
// import NewsVideo from "./components/servercomponent/newVideo";
// const  HomePageMAin  =   dynamic(() => import("./components/servercomponent/hompage/homepageMain",{ssr:false}))
// const HomePageMAinBase = dynamic(() => import("./components/servercomponent/hompage/homePageMainbase",{ssr:false}))
// const HomePageCLinet = dynamic(() => import("./components/clinet/homePAge/homePageClinet",{ssr:false}))

// import HomePageCLinet from './components/clinet/homePAge/homePageClinet';
// const HomePageVideo = dynamic(() => import("./components/clinet/homePAge/homePageVideo",{ssr:false}))

// import HomePageVideo from './components/clinet/homePAge/homePageVideo';
// const Excluseive = dynamic(() => import('./components/clinet/homePAge/cricexclusive',{ssr:false}))
// const SeriesHome = dynamic(() => import('./components/clinet/homePAge/seriesHomePage',{ssr:false}))
// const OnthisDay = dynamic(() => import('./components/clinet/homePAge/onthisday',{ssr:false}))
// const QuizNews = dynamic(() => import('./components/clinet/homePAge/quizNews',{ssr:false}))

// const TwitterHomePage = dynamic(() => import('./components/clinet/homePAge/tweetHomePage',{ssr:false}))
// const SeriesHomeNews = dynamic(() => import('./components/clinet/homePAge/seriesHomeVideoArticle',{ssr:false}))

// import SideBarMain from './components/clinet/sideBar/sideBarMain'

// import Excluseive from './components/clinet/homePAge/cricexclusive';
// import SeriesHome from './components/clinet/homePAge/seriesHomePage';

// import OnthisDay from './components/clinet/homePAge/onthisday';
// import QuizNews from './components/clinet/homePAge/quizNews';

// import SideBarMain from './components/clinet/sideBar/sideBarMain'

// import TwitterHomePage from './components/clinet/homePAge/tweetHomePage'
// import Footer from './components/footer';

// import SeriesHomeNews from './components/clinet/homePAge/seriesHomeVideoArticle';

// export const metadata = {
//   title: 'Live Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com',

// };

export default function Page(props) {
  const [desktopMode, setDesktopMode] = useState(false)

  useEffect(() => {
    if (window.innerWidth > 700) {
      setDesktopMode(true)
    }
  }, [])

  // console.Console;
  return (
    <ClientComponent desktopMode={desktopMode}>
      <title>
        Live Cricket Score, Match Schedule & Predictions, Latest News |
        Cricket.com'
      </title>
      <link
        rel="shortcut icon"
        href="https://images.cricket.com/icons/mainlogoico.ico"
      />
      
      <div className="  hidden  lg:block md:block ">
        <div className="flex       bg-slate-100   ">
          {desktopMode && (
            <div className="flex  mx-auto max-w-7xl lg:px-24 md:px-24 item-center justify-start   text-black dark:bg-basebg   dark:text-white  ">
              <div className="flex flex-col w-8/12       ">
                <div className="w-full flex rounded-md shadow-sm  ">
                  <HomeNewsVideo />
                </div>

                {/*      
          <div className='w-full flex bg-white py-4 px-8 rounded-md mt-5 shadow-sm  '>  <HomePageCLinet /></div>
          <div className='w-full flex bg-white py-4 px-8 rounded-md   mt-5 shadow-sm'>  <HomePageVideo  /></div>
          <div className='w-full flex  bg-white pt-4 pb-5  px-8 rounded-md  mt-5 shadow-sm '>  <Excluseive /></div>
          <div className='w-full flex  bg-white pt-4 pb-3 px-8 rounded-md  mt-5 shadow-sm'>  <TwitterHomePage /></div>
          <div className='w-full flex  bg-white pt-4 pb-3 px-8 rounded-md  mt-5 shadow-sm '>  <OnthisDay /></div>
          <div className='w-full flex  bg-white pt-4 pb-3 px-8 rounded-md mt-5 shadow-sm'>  <QuizNews /> </div>
          <div className='w-full flex  bg-white pt-4 pb-3 px-8  rounded-md  mt-5 shadow-sm'>  <SeriesHome /></div>
          <div className='w-full flex bg-white py-4 px-8 rounded-md mt-5 shadow-sm '>  <SeriesHomeNews /></div>

          */}
              </div>
              <div className="flex flex-col w-4/12  justify-start  mt-5  ">
                <div className=" w-full   flex  flex-col   items-center  pl-8 ">
                  <SideBarMain />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </ClientComponent>
  )
}
