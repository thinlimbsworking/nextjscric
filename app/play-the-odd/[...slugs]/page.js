import React from 'react'
import dynamic from 'next/dynamic'
import Loading from '../../components/loading'

const COMPLIST = {
  contestDetail: dynamic(
    () => import('../../components/playodds/contestDetails'),
    {
      loading: () => <Loading />,
    },
  ),
  myContest: dynamic(() => import('../../components/playodds/myContest'), {
    loading: () => <Loading />,
  }),
  leaderboard: dynamic(() => import('../../components/playodds/leaderboard'), {
    loading: () => <Loading />,
  }),
  contestSummary: dynamic(
    () => import('../../components/playodds/contestSummary'),
    {
      loading: () => <Loading />,
    },
  ),
}

export default function PtoDetails({ params }) {
  const [page, matchID] = params.slugs

  const getDesiredComp = (page) => {
    switch (page) {
      case 'contest':
        return 'contestDetail'
      case 'mycontest':
        return 'myContest'
      case 'leaderboard':
        return 'leaderboard'
      case 'contestSummary':
        return 'contestSummary'
      default:
        return
    }
  }

  let Comp =
    COMPLIST[
      params.slugs.length == 3
        ? getDesiredComp('contestSummary')
        : params.slugs.length >= 4
        ? getDesiredComp('leaderboard')
        : getDesiredComp(page)
    ]
  // console.log('render',slugLevel);
  return <>
    <div className=' hidden md:flex lg:flex bg-basered  z-1 px-2 py-4 items-center justify-center text-white mb-3 '>
                <div className='w-full flex items-center justify-center pr-8'>
                    <span className='text-xl  font-semibold capitalize'>{'Play the Odds'}</span>
                </div>
      </div>
    <Comp matchID={matchID} props={params.slugs} />
  </>
}
