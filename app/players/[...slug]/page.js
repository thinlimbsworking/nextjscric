'use client'
import React, { useEffect, useState } from 'react'
import Loading from '../../components/loading'
import StatHubLayout from '../../components/shared/statHubLayout'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/navigation'
import Layout from '../../components/playersDetails/playerDetailsLayout'
import axios from 'axios'
import {
  PLAYER_DETAILS_AXIOS,
  NEWS_BY_ID_TYPE_AXIOS,
  PLAYER_CAREER_DETAILS_AXIOS,
} from '../../api/queries'
import Urltracking from '../../components/commom/urltracking'
import MetaDescriptor from '../../components/MetaDescriptor'
import { PLAYERS } from '../../constant/MetaDescriptions'

const componentList = {
  articles: dynamic(() =>
    import(`../../components/playersDetails/playerArticlesTab`),
  ),
  careerstats: dynamic(() =>
    import(`../../components/playersDetails/playerCareerStatsTab`),
  ),
  recent: dynamic(() =>
    import(`../../components/playersDetails/playerMatchTab`),
  ),
  fantasy: dynamic(() =>
    import(`../../components/playersDetails/playerFantasyTab`),
  ),
  playerbio: dynamic(() => import(`../../components/playersDetails/playerBio`)),
  compare: dynamic(() =>
    import(`../../components/playersDetails/playerCompare`),
  ),
}

// href={HEADER[menu.id].href} as={HEADER[menu.id].as}

/**
 * @route /players/playerID/playerName
 * @example /players/3993/virat-kohli
 * @param { String } playerID
 * @param { Object } playerNewsData
 */

async function getData(path) {
  const [playerID, playerSlug, tab, player2ID] = path
  var conditionalData = {}

  const playerNewData = await axios.post(process.env.API, {
    query: PLAYER_CAREER_DETAILS_AXIOS,
    variables: { playerID },
  })
  console.log('data sending to comp - ', playerNewData)

  let category = tab.split('-').join('')
  // console.log("category", category)
  switch (category) {
    case 'careerstats':
      conditionalData = { data: [] }
      break
    case 'articles':
      conditionalData = await axios.post(process.env.API, {
        query: NEWS_BY_ID_TYPE_AXIOS,
        variables: {
          Id: playerID,
          type: 'players',
        },
      })
      break
    case 'recent':
      conditionalData = { data: [] }
      break
    case 'fantasy':
      conditionalData = { data: [] }
      break
    case 'playerbio':
      conditionalData = { data: [] }
      break
    case 'compare':
      conditionalData = { data: [] }
      break
    default:
      conditionalData = { data: [] }
      break
  }

  const [
    { data: playerData },
    { data: conditionalResponse },
  ] = await Promise.all([playerNewData, conditionalData])
  console.log(
    'data sending to comp - ',
    playerData,
    ' -- ',
    conditionalResponse,
  )
  return {
    playerID,
    category: category,
    playerSlug: playerSlug,
    player2ID: player2ID ? player2ID : '',
    playerData: playerData.data,
    responseData:
      conditionalResponse && conditionalResponse.data
        ? conditionalResponse.data
        : [],
  }
}

export default function PlayerDetail({ params }) {
  // console.log("tdfuuhkj===========>", category)
  const [props, setProps] = useState()
  const [loading, setLoading] = useState(true)
  // const [Component, setComponent] = useState()
  let Component = componentList[params.slug[2].split('-').join('')]
  let router = useRouter()

  // if (router.isFallback) {
  //   return <Loading />;
  // } else {

  useEffect(() => {
    getData(params.slug).then((res) => {
      setProps(res)
      setLoading(false)
    })
  }, [])
  if (loading) return <Loading />
  return (
    <StatHubLayout currIndex={0}>
      {props && props.urlTrack ? <Urltracking PageName="Players" /> : null}
      {console.log('player slug - ',props.playerSlug)}
      <MetaDescriptor
        section={PLAYERS({
          title: props.playerSlug.replace('-', ' '),
          playerName: props.playerSlug.replace('-', ' '),
          url: router.asPath
        })}></MetaDescriptor>
      {props.category !== 'playerbio' && props.category !== 'compare' ? (
        <Layout
          data={props.playerData}
          tab={props.category}
          playerID={props.playerID}
          playerSlug={props.playerSlug}
        >
          {props.urlTrack ? <Urltracking PageName="Match-Score" /> : null}

          <Component
            playerRole={props?.playerData?.getPlayersProfileV2?.role}
            playerSlug={props.playerSlug}
            data={props.responseData}
            playerID={props.playerID}
          />
        </Layout>
      ) : (
        <Component
          playerRole={
            props.playerData?.getPlayersProfileV2?.role
              ? props.playerData.getPlayersProfileV2.role
              : null
          }
          playerDetils={props.playerData}
          playerSlug={props.playerSlug}
          playerID2={props.player2ID}
          playerID={props.playerID}
        />
      )}
    </StatHubLayout>
  )
}

// export async function  getServerSideProps (context)  {
//   const [playerID, playerSlug, tab, player2ID] = context.params.slugs;
//   var conditionalData = {};

//     const playerNewData = await axios.post(process.env.API, {
//       query: PLAYER_CAREER_DETAILS_AXIOS,
//       variables: { playerID }
//     });
//      console.log("playerDetails", playerNewData.data)

//     let category = tab.split('-').join('');
//     // console.log("category", category)
//     switch (category) {
//       case 'careerstats':
//         conditionalData = { data: [] };
//         break;
//       case 'articles':
//         conditionalData = await axios.post(process.env.API, {
//           query: NEWS_BY_ID_TYPE_AXIOS,
//           variables: {
//             Id: playerID,
//             type: 'players'
//           }
//         });
//         break;
//       case 'recent':
//         conditionalData = { data: [] };
//         break;
//       case 'fantasy':
//         conditionalData = { data: [] };
//         break;
//       case 'playerbio':
//         conditionalData = { data: [] };
//         break;
//       case 'compare':
//         conditionalData = { data: [] };
//         break;
//       default:
//         conditionalData = { data: [] };
//         break;
//     }

//     const [{ data: playerData }, { data: conditionalResponse }] = await Promise.all([playerNewData, conditionalData]);

//     return {
//         props: {
//       playerID,
//       category: category,
//       playerSlug: playerSlug,
//       player2ID: player2ID ? player2ID : '',
//       playerData: playerData.data,
//       responseData: conditionalResponse && conditionalResponse.data ? conditionalResponse.data : []}
//     };
//   }
