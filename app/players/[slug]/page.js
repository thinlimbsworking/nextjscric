'use client'
import React, { useState, useRef, useEffect } from 'react'
import {
    PLAYER_DISCOVERY_SEARCH,
    PLAYER_DISCOVERY_V2_AXIOS,
} from '../../api/queries'
//import Swiper from 'react-id-swiper';
import Link from 'next/link'
import { usePathname, useRouter } from 'next/navigation'

import axios from 'axios'
import { PLAYER_VIEW } from '../../constant/Links'
import { PLAYERS } from '../../constant/MetaDescriptions'
import MetaDescriptor from '../../components/MetaDescriptor'
import ImageWithFallback from '../../components/commom/Image'
import StatHubLayout from '../../components/shared/statHubLayout'
//import Urltracking from '../../components/Common/urltracking';
import DataNotFound from '../../components/commom/datanotfound'
const backIconWhite = '/svgsV2/RightSchevronWhite.svg'
const searchIcon = '/svgs/search.svg'
import Search from '../../components/search'
// import { trackScroll } from '../../components/Common/LazyImageLoader';
import PlayerDiscoveryGallery from '../../components/playersDetails/playerDiscoveryGallery'
import SliderCommon from '../../components/commom/slider'
// import useCleverTab from '../../components/customHooks/useCleverTab';
/**
 * @description Players listing
 * @route /players
 * @param { Object } props
 */

async function getData(param) {
    let TeamTab = param.toUpperCase() || 'ALL'
    let { data } = await axios.post(process.env.API, {
        query: PLAYER_DISCOVERY_V2_AXIOS,
        variables: { teamShortName: TeamTab.toUpperCase() },
    })
    data = data.data

    if (data && data.playerDiscoveryV2 && data.playerDiscoveryV2.length > 0) {
        let slicedData = data.playerDiscoveryV2
        var arr = []
        var temp = []
        for (let i = 0; i < slicedData.length; i++) {
            temp.push(slicedData[i])
            if (temp.length == 8) {
                arr.push(temp)
                temp = []
            }
        }
        if (slicedData.length % 8 !== 0) {
            arr.push(slicedData.slice(-slicedData.length % 8))
        }

        return { players: arr, TeamTab, page: 0 }
    }
    return { TeamTab, players: [], error: true }
}

export default function Players({ params }) {

  const currentPath=  usePathname()
 
    const [Page, setPage] = useState(0)
    var [teamsToFilter,setTeamsFilter] =useState ([
        'ALL',
        'IND',
        'NZ',
        'AUS',
        'ENG',
        'BAN',
        'SA',
        'WI',
        'PAK',
        'SL',
        'MI',
        'RCB',
        'CSK',
        'DC',
        'SRH',
        'KKR',
        'PBKS',
        'RR',
    ])


    useEffect(()=>{
if(true)
{
        let arr = [
            'ALL',
            'IND',
            'NZ',
            'AUS',
            'ENG',
            'BAN',
            'SA',
            'WI',
            'PAK',
            'SL',
            'MI',
            'RCB',
            'CSK',
            'DC',
            'SRH',
            'KKR',
            'PBKS',
            'RR',
        ];
     

        const fromIndex = teamsToFilter.indexOf(currentPath.substring(9, 20).toUpperCase()) // 👉️ 0
        const toIndex = 1;
        
        const element = arr.splice(fromIndex, 1)[0];
        arr.splice(toIndex, 0, element);
        setTeamsFilter(arr)
    }
       
    },[])
    function checkTeam() {
     
        
        return teamsToFilter.indexOf(currentPath.substring(9, 20).toUpperCase())
      }
    const [screenWidth, setscreenWidth] = useState(null)
    const [props, setProps] = useState()
    const [error, setError] = useState(false)
    const [players, setPlayers] = useState([])
    const [TeamTab, setTeamTab] = useState()
    const [scoreScreen, setScoreSreen] = useState({})
    const [inputValue, setInputValue] = useState('')
    // console.log("players",players)
    const [playerChunk, setPlayerChunk] = useState(players.slice(0, Page * 2 + 4))
    const wrapperRef = useRef(null)

    useEffect(() => {
        getData(params.slug).then((res) => {
            const { players, TeamTab, error, ...rest } = res
            setPlayers(players)
            setError(error)
            setTeamTab(TeamTab)
            setProps(rest)
        })
    }, [])

    useEffect(() => {
        let offSet = Page * 2 + 4
        let player = players.slice(0, offSet)
        setPlayerChunk(player)
    }, [Page, TeamTab])
    // console.log('playerChunk',playerChunk)
    useEffect(() => {
        if (
            typeof window !== 'undefined' &&
            window &&
            window.screen &&
            window.screen.width
        ) {
            setscreenWidth(window.screen.width > 975 ? 976 : window.screen.width)
        }
    }, [])
    useEffect(() => {
        function handleClickOutside(event) {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                setfocus(false)
            }
        }
        document.addEventListener('mousedown', handleClickOutside)
        return () => {
            document.removeEventListener('mousedown', handleClickOutside)
        }
    }, [wrapperRef])

    const serachStyle = {
        backgroundImage: `url(${searchIcon})`,
        backgroundRepeat: 'no-repeat',
        textIndent: '25px',
        backgroundPosition: '5px',
    }
    const paramss = {
        slidesPerView: 4,
        navigation: {
            nextEl: `#nbutton`,
            prevEl: `#pbutton`,
        },
        breakpoints: {
            700: {
                slidesPerView: 5,
            },
        },
    }

    const onSearchChange = async (value) => {
        if (value.length >= 3) {
            const { data } = await axios.post(process.env.API, {
                query: PLAYER_DISCOVERY_SEARCH,
                variables: { name: value },
            })
            return data &&
                data.data &&
                data.data.playerSearch &&
                data.data.playerSearch.length > 0
                ? [...data.data.playerSearch]
                : []
        }
        return null
    }

    const getSearchResult = (player, index) => {
        return (
            <div
                key={index}
                className=" cursor-pointer bg-white dark:bg-gray-8  border-b border-b-solid border-b-gray-2"
                onClick={() => {
                    // pushEvent('Players', {
                    //   Source: 'Search',
                    //   PlayerID: player.playerID,
                    //   Rank: player.playerRank
                    // });
                }}
            >
                <div className="py-2  ">
                    <Link {...goToPlayerProfile(player.name, player.playerID)}>
                        <li className=" text-left px-3 py-1   text-xs font-semibold cursor-pointer">
                            {player.name ? player.name : player.fullName}
                        </li>
                    </Link>
                </div>
                {/* <div className="divider w-full"></div> */}
            </div>
        )
    }
    const handleScroll = (evt) => {
        if (
            evt.target.scrollTop + evt.target.clientHeight >=
            0.8 * evt.target.scrollHeight
        ) {
            setPage((prevPage) => prevPage + 1)

            // handlePagination(true);
        }
    }

    // const handlePagination = (fetchMore) => {
    //   console.log("called fectchmp more",fetchMore)
    //  };

    const goToPlayerProfile = (name, playerID) => {
        let playerId = playerID
        let playerSlug = `${name && name.split(' ').join('-').toLowerCase()}`
        return {
            as: eval(PLAYER_VIEW.as),
            href: PLAYER_VIEW.href,
        }
    }

    const tabSwitch = (tag) => {
        tag = tag && tag.toLowerCase()
        return {
            as: `/players/${tag}/`,
            href: '/players/[slugs]',
        }
    }

    return (
        
        <StatHubLayout currIndex={0}>
            <div
                className="w-full h-[100vh] overflow-x-auto bg-white dark:bg-transparent "
                onScroll={(evt) => {
                    handleScroll(evt)
                }}
            >
                <MetaDescriptor section={PLAYERS()} />
                {props && props.urlTrack ? <Urltracking PageName="Players" /> : null}
                <div className="z-1 p-3  gap-2  md:hidden lg:hidden flex items-center">
                    {/* <Link href={'/'} as="/" passHref legacyBehavior> */}
                        <div onClick={() => window.history.back()} className="bg-gray p-2 rounded-md">
                            <ImageWithFallback
                                width={18}
                                height={18}
                                loading="lazy"
                                className="flex items-center justify-center h-4 w-4 rotate-180"
                                alt=""
                                src={backIconWhite}
                            />
                        </div>
                    {/* </Link> */}
                    <div className="white text-lg font-semibold pl-3 ">Players</div>
                </div>
                <div className="w-full p-3">
                    <div className=" w-full">
                        <Search
                            onSearchChange={onSearchChange}
                            getSearchResult={getSearchResult}
                            style={serachStyle}
                            className="w-full border border-gray-2 dark:border-none rounded-md dark:bg-gray  p-2 outline-none "
                            placeHolder="Search Player"
                        />
                    </div>
                </div>
                <div className="  w-full">
                    <div className="  relative w-full ">
                        <div className="  w-full">
                          
                            <SliderCommon
                          
                                data={teamsToFilter}
                                tabSwitch={tabSwitch}
                                event="playerDashboard"
                                tags={TeamTab}
                                setTeamTab={setTeamTab}
                            />
                            {/* <Swiper {...params}>
                {teamsToFilter.map((tag, i) => (
                  <div key={i} className={`flex flex-column w-20 `}>
                    <Link  {...tabSwitch(tag)} >
                      <div
                        className={` text-xs text-gray-2 border-b-2 border-gray-2 ${TeamTab === tag ? 'border-b-2 border-blue-6 text-blue-8' : ''
                          } cursor-pointer  flex items-center justify-center py-2 p py-2-m `}
                      >
                        {tag}
                      </div>
                    </Link>
                  </div>
                ))}
              </Swiper> */}

                            {/* <div className="absolute absolute--fill pt-1 flex justify-between items-center md:hidden lg:hidden">
                                <div
                                    id={`pbutton`}
                                    className="white cursor-pointer   outline-0  ">
                                    <svg width="30" focusable="false" viewBox="0 0 24 24">
                                        <path
                                            fill="#38d925"
                                            d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                                        ></path>
                                        <path fill="none" d="M0 0h24v24H0z"></path>
                                    </svg>
                                </div>
                                <div
                                    id={`nbutton`}
                                    className="white cursor-pointer   outline-0 ">
                                    <svg width="30" viewBox="0 0 24 24">
                                        <path
                                            fill="#38d925"
                                            d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                                        ></path>
                                        <path fill="none" d="M0 0h24v24H0z"></path>
                                    </svg>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
                {error ? (
                    <DataNotFound />
                ) : (
                    <PlayerDiscoveryGallery
                        Page={Page}
                        TeamTab={TeamTab}
                        screenWidth={screenWidth}
                        players={playerChunk}
                    />
                )}
            </div>
        </StatHubLayout>
    )
}

// export async function  getServerSideProps (context)  {
//   const query = context?.params;
//     let TeamTab = (query.slug ? query.slug : 'all')
//   let { data } = await axios.post(process.env.API, {
//     query: PLAYER_DISCOVERY_V2_AXIOS,
//     variables: { teamShortName: TeamTab.toUpperCase() },
//   });
//   data = data.data
// // console.log("data",data)

//   if (data && data.playerDiscoveryV2 && data.playerDiscoveryV2.length > 0) {
//     let slicedData = data.playerDiscoveryV2;
//     var arr = [];
//     var temp = [];
//     for (let i = 0; i < slicedData.length; i++) {
//       temp.push(slicedData[i]);
//       if (temp.length == 8) {
//         arr.push(temp);
//         temp = [];
//       }
//     }
//     if (slicedData.length % 8 !== 0) {
//       arr.push(slicedData.slice(-slicedData.length % 8));
//     }
//     // if(query.slug == 'all' || query.slug == 'ALL') arr = arr.slice(0,4);

//     return { props: {players:arr,TeamTab,page:0}}
//   }

//   return { props:{TeamTab,players:[],error:true}}

// };
