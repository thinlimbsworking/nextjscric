'use client'
import React, { useEffect, useRef, useState } from 'react'
import { RANKINGS_AXIOS } from '../../api/queries'
import Head from 'next/head'
import Link from 'next/link'
import axios from 'axios'
import Tab from '../components/shared/Tab'
import PlayersRanking from '../components/playersRanking'
import CommonDropdown from '../components/commom/commonDropdown'
import StatHubLayout from '../components/shared/statHubLayout'
import TeamsRanking from '../components/teamsRanking'
import Loading from '../components/loading'
/**
 * @description all type of rakings for odi, t20, test
 * @route /rankings
 * @param { Object } props
 */

async function getServerData() {
  const res = await axios.post(process.env.API, {
    query: RANKINGS_AXIOS,
  })
  return res
}

const FormatFilter = [
  { name: 'TEST', value: 'Test' },
  { name: 'ODI', value: 'ODI' },
  { name: 'T20', value: 'T20' },
]

const TabFilter = [
  { name: 'Teams', value: 'Teams' },
  { name: 'Players', value: 'Players' },
]

const GenderFilter = [
  { name: 'Men', value: 'men' },
  { name: 'Women', value: 'women' },
]

const Rankings = ({ params }) => {
  const [props, setProps] = useState()
  const [currentFormat, setCurrentFormat] = useState({
    name: 'ODI',
    value: 'ODI',
  })
  const [currentTab, setCurrentTab] = useState(TabFilter[0])
  const [currentGender, setCurrentGender] = useState(GenderFilter[0])

  const { loading, error, data } = { data: props && props?.data }
  // const run = useRef(0)
  useEffect(() => {
    getServerData().then((res) => {
      setProps(res?.data)
    })
    // run.current++
  }, [])

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  return (
    <StatHubLayout currIndex={3}>
      <div>
        <title>
          Cricket Rankings | Team & Player Rankings | ODI, Test, T20 Rankings |
          Cricket.com
        </title>
        <meta
          name="description"
          content="Check out the official ICC cricket rankings of international cricket teams & players for ODI, Test & T20 matches and women cricket teams rankings on Cricket.com"
        />
        <meta
          property="og:title"
          content="Cricket Rankings | Team & Player Rankings | ODI, Test, T20 Rankings | Cricket.com"
        />
        <meta
          property="og:description"
          content="Check out the official ICC cricket rankings of international cricket teams & players for ODI, Test & T20 matches and women cricket teams rankings on Cricket.com"
        />
        <meta property="og:url" content="https://www.cricket.com/rankings" />
        <meta
          name="twitter:title"
          content="Cricket Rankings | Team & Player Rankings | ODI, Test, T20 Rankings | Cricket.com"
        />
        <meta
          name="twitter:description"
          content="Check out the official ICC cricket rankings of international cricket teams & players for ODI, Test & T20 matches and women cricket teams rankings on Cricket.com"
        />
        <meta name="twitter:url" content="https://www.cricket.com/rankings" />
        <link rel="canonical" href="https://www.cricket.com/rankings" />
      </div>

      {error ? (
        <div></div>
      ) : loading ? (
        <div className="w-full h-screen font-extralight  m-0 text-xs gray flex justify-center items-center">
          <Loading />
        </div>
      ) : (
        <div className="overflow-hidden text-white">
          <div className="w-full sticky top-0 lg:hidden md:hidden xl:hidden bg-gray-8">
            <div className="p-4 flex items-center">
              {/* <img
              className='mr-2 lg:hidden p-2 bg-gray rounded-md'
              onClick={() => window.history.back()}
              src='/svgs/backIconWhite.svg'
              alt='back icon'
            /> */}

              <div
                onClick={() => window?.history?.back()}
                className=" outline-0 cursor-pointer mr-2 lg:hidden  bg-gray rounded"
              >
                <svg width="30" focusable="false" viewBox="0 0 24 24">
                  <path
                    fill="#ffff"
                    d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>

              {/* <div
            className="mr-2 lg:hidden p-1 bg-gray rounded">
            id='swiper-button-prev'
              <svg width="30" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#ffff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div> */}

              <span className="font-semibold lg:hidden md:hidden xl:hidden text-xl lg:text-xs white">
                {' '}
                Rankings{' '}
              </span>
            </div>

            {/* tabs for MWeb */}
            <div className="flex justify-between my-5 rounded-full w-4/5 mx-10 bg-gray ">
              <Tab
                data={FormatFilter?.filter((format) =>
                  currentGender?.valueId === 'women' && format === 'Test'
                    ? undefined
                    : format,
                )}
                type="outline"
                handleTabChange={(item) => setCurrentFormat(item)}
                selectedTab={currentFormat}
              />
            </div>
          </div>
          {/* <h1 className="text-xl lg:text-bold lg:hidden md:hidden xl:hidden ml-6 my-2">
            {' '}
            Rankings{' '}
          </h1> */}

          {/* Tabs for Web screen */}

          <div className="hidden md:flex lg:flex xl:flex gap-3 justify-between px-4 items-center py-4">
            <div className="w-5/12">
              {currentTab?.value === 'Players' && (
                <Tab
                  data={FormatFilter?.filter((format) =>
                    currentGender?.value === 'women' && format?.value === 'Test'
                      ? undefined
                      : format,
                  )}
                  type="block"
                  handleTabChange={(item) => setCurrentFormat(item)}
                  selectedTab={currentFormat}
                />
              )}
              {currentTab?.value !== 'Players' && (
                <Tab
                  data={GenderFilter}
                  type="line"
                  handleTabChange={(item) => setCurrentGender(item)}
                  selectedTab={currentGender}
                />
              )}
            </div>

            <div className="w-3/12">
              <Tab
                data={TabFilter}
                type="block"
                handleTabChange={(item) => setCurrentTab(item)}
                selectedTab={currentTab}
              />
            </div>
            {currentTab?.value == 'Players' && (
              <div className="w-2/12 ml-6 md:flex justify hidden">
                <Tab
                  data={GenderFilter?.filter((format) =>
                    currentFormat?.value === 'Test' ? undefined : format,
                  )}
                  type="line"
                  handleTabChange={(item) => setCurrentGender(item)}
                  selectedTab={currentGender}
                />
                {/* <CommonDropdown
                options={GenderFilter}
                onChange={(option) => setCurrentGender(option)}
                defaultSelected={currentGender}
                placeHolder={currentGender?.valueLable}
                showExpandIcon
                variant="outline"
              /> */}
              </div>
            )}
          </div>

          {currentTab?.value === 'Teams' ? (
            <>
              <div className="md:px-4 overflow-hidden md:hidden lg:hidden mx-2 lg:px-4 xl:px-4">
                <div className="dark:bg-gray bg-white rounded-t-2xl">
                  <div className=" flex justify-between items-center mx-4 ml-4 font-semibold text-xs">
                    <div className="py-4 text-left w-1/12 dark:text-white text-black">
                      RANK
                    </div>
                    <div className="py-4  w-9/12 text-left dark:text-white text-black">
                      TEAMS
                    </div>
                    <div className="font-normal  text-xs text-left"></div>
                  </div>
                  <div className=" bg-black/80 h-[1px] w-full"></div>
                </div>
                <div>
                  {data &&
                    data.getrankings[currentFormat?.value?.toLowerCase()].map(
                      (rank, i) => (
                        <Link
                          href="/teams/[...slugs]"
                          as={`/teams/${rank.teamID}/${rank.teamName
                            .split(' ')
                            .join('-')
                            .toLowerCase()}/form`}
                          key={i}
                          passHref
                          legacyBehavior
                        >
                          <div className="w-full px-2 text-black dark:text-white dark:bg-gray-6 bg-white my-4 uppercase rounded-xl dark:border-gray-6 border-[#E2E2E2] border-2 border-solid -mt-2 overflow-hidden ">
                            <div className="cursor-pointer flex justify-between items-center w-full">
                              <div className=" text-left pl-2 font-medium text-xl py-4 dark:text-green-6 text-black  uppercase opacity-1 w-16 h-12 content-center">
                                {rank.ranking}
                              </div>
                              <div className=" text-xs font-medium text-left py-4 w-80 lg:w-8/12 xl:w-8/12 sm:w-64 h-14 mx-6 rounded-xl m-auto">
                                <span className="flex items-center">
                                  <img
                                    alt="teamFlag"
                                    className="mr-4 h-8 w-16 shadow-xl"
                                    src={`https://images.cricket.com/teams/${rank.teamID}_flag_safari.png`}
                                    onError={(evt) =>
                                      (evt.target.src =
                                        '/svgs/images/flag_empty.svg')
                                    }
                                  />
                                  {rank.teamName.toUpperCase()}
                                </span>
                              </div>
                              {/* <div className='w-10 flex justify-end white z-999 outline-0 cursor-pointer'>
                      <img className='mr2 border ' alt='RightSchevron' src='/svgs/RightSchevronBlack.svg' />
                    </div> */}
                              <div
                                id="swiper-button-prev"
                                className="white p-2 lg:hidden md:hidden bg-gray-8 rounded-md outline-0 cursor-pointer w-10 flex justify-end mr-2"
                              >
                                <svg width="30" viewBox="0 0 24 24">
                                  <path
                                    fill="#38d925"
                                    d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                                  ></path>
                                  <path fill="none" d="M0 0h24v24H0z"></path>
                                </svg>
                              </div>
                              <div
                                id="swiper-button-prev"
                                className="white p-2 lg:flex md:flex hidden bg-basered rounded-md outline-0 cursor-pointer w-10  justify-end mr-2"
                              >
                                <svg width="30" viewBox="0 0 24 24">
                                  <path
                                    fill="#FFFF"
                                    d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                                  ></path>
                                  <path fill="none" d="M0 0h24v24H0z"></path>
                                </svg>
                              </div>
                            </div>
                            {/* <div className=" bg-black/80 h-[1px] w-full"></div> */}
                          </div>
                        </Link>
                      ),
                    )}
                </div>
              </div>

              <div className="lg:flex md:flex hidden">
                <TeamsRanking
                  format={FormatFilter}
                  gender={currentGender?.value}
                />
              </div>
            </>
          ) : (
            <PlayersRanking
              format={currentFormat?.value}
              gender={currentGender?.value}
            />
          )}
        </div>
      )}
    </StatHubLayout>
  )
}

// Rankings.Layout = Layout;
export default Rankings
