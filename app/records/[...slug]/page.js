'use client'
import React, { useEffect, useRef, useState } from 'react'
import { RECORDS_AXIOS } from '../../api/queries'
import axios from 'axios'
import Layout from '../../components/layout'
import Head from 'next/head'
import Urltracking from '../../components/commom/urltracking'
import Loading from '../../components/loading'
import ImageWithFallback from '../../components/commom/Image'

const PH = '/pngsV2/playerph.png'

async function getServerData(format, playertype, sortType) {
  const { data } = await axios.post(process.env.API, {
    query: RECORDS_AXIOS,
    variables: {
      format: format,
      playertype: playertype,
      sortType: keysObj[sortType],
    },
  })
  return { data: data.data, format, playertype, sortType }
}

const RecordDetails = ({ params }) => {
  const format = params?.slug?.[0]
  const playertype = params?.slug?.[1]
  const sortType = params?.slug?.[2]
  const [props, setProps] = useState()

  // const run = useRef(0)
  useEffect(() => {
    getServerData(format, playertype, sortType).then((res) => {
      setProps(res.data)
    })
    // run.current++
  }, [])

  const { error, loading, data } = { data: props }
  if (error)
    return (
      <div>
        <div className="w-full h-screen font-extralight text-xs gray flex flex-col justify-center items-center">
          <span>Something isn't right!</span>
          <span>Please refresh and try again...</span>
        </div>
      </div>
    )
  if (loading)
    return (
      <div>
        <Loading />
      </div>
    )
  if (error) {
    return <h2>Error</h2>
  }

  useEffect(() => {
    window.scrollTo({ top: 0, behaviour: 'smooth' })
  }, [])

  if (props?.getRecords) {
    return (
      <>
        <div className="dark:bg-gray-8 hidden md:block">
          {props.urlTrack ? <Urltracking PageName="Records" /> : null}
          <div>
            <title>Cricket Records | Cricket.com</title>
            <meta
              name="description"
              content="Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com"
            />
            <meta
              property="og:title"
              content="Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com"
            />
            <meta
              property="og:description"
              content="Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com"
            />
            <meta property="og:url" content="https://www.cricket.com/" />
            <meta
              name="twitter:title"
              content="Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com"
            />
            <meta
              name="twitter:description"
              content="Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com"
            />
            <meta name="twitter:url" content="https://www.cricket.com/" />
            <link rel="canonical" href="https://www.cricket.com/" />
          </div>
          <div className="mb-6 md:mt-1 mx-2 lg:max-w-5xl lg:mx-auto">
            {/* <div className='db dn-ns'>
              <div className='p-3 flex items-center'>
                <img
                  className='mr2 p-2 bg-gray rounded-md'
                  onClick={() => window.history.back()}
                  src='/svgs/backIconWhite.svg'
                  alt='back icon'
                />
                <span className='font-semibold white f35'> {props.sortType} </span>
              </div>
            </div> */}
            <div className="w-full sticky top-0 lg:hidden md:hidden xl:hidden">
              <div className="px-3 items-center flex">
                {/* <img
                  className='mr-2 lg:hidden p-2 bg-gray rounded-md'
                  onClick={() => window.history.back()}
                  src='/svgs/backIconWhite.svg'
                  alt='back icon'
                /> */}
                <div
                  onClick={() => window.history.back()}
                  className="outline-0 cursor-pointer mr-2 md:hidden lg:hidden xl:hidden bg-gray rounded"
                >
                  <svg width="30" focusable="false" viewBox="0 0 24 24">
                    <path
                      fill="#ffff"
                      d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                    ></path>
                    <path fill="none" d="M0 0h24v24H0z"></path>
                  </svg>
                </div>
                <span className="text-lg white font-semibold"> Records </span>
              </div>
              <div className="dark:text-white text-black p-3 my-0 flex items-center -mt-1">
                <span className="font-semibold f35">{params?.slug?.[2]} </span>
              </div>
              <div className="flex h-1 w-12 bg-blue-8 -mt-2 ml-3 mb-4"></div>
            </div>
            <h1 className="dark:font-semibold font-bold dark:text-sm lg:block md:block hidden text-lg py-2 pb-2 dark:px-4 px-3">
              {' '}
              {params?.slug?.[2]}{' '}
            </h1>
            <div className="h-1 w-12 bg-blue-8 -mt-2 ml-3 mb-4 hidden lg:flex md:flex"></div>
            <div className="ml-2">
              <div className="flex dark:py-2 py-4 dark:bg-gray-4 bg-gray-11 text-black dark:text-white shadow-4 justify-between items-center rounded-t-xl text-xl dark:text-xs">
                <div className="flex md:w-8/12 lg:w-8/12 xl:w-8/12 w-8/12">
                  <h2 className="text-xs mx-2 font-semibold">
                    {params?.slug?.[1] == 'batting' ? 'BATTER' : 'BOWLERS'}
                  </h2>
                </div>
                {recordKeys[params?.slug?.[2]].map((list, index) => (
                  <div className="flex items-center w-2/12 md:w-20 justify-center xl:w-20 dark:text-white text-black lg:w-20 text-center text-xs font-semibold">
                    {list.label}
                  </div>
                ))}
              </div>
              <div className="dark:bg-black/80 bg-gray-11 w-full " />
              {data &&
                data.getRecords &&
                data.getRecords[params?.slug?.[1]].map((item, index) => (
                  <div key={index}>
                    <div className="flex py-2 text-xs font-medium justify-between items-center bg-white dark:bg-gray">
                      <div className="flex w-8/12 ml-1 items-center break-words h-10">
                        {' '}
                        {/* <div className="flex items-center justify-between mx-2">
                        <img
                          className="h-10 w-10 bg-gray object-top object-cover rounded-full"
                          src={PH}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />
                      </div> */}
                        <div className="flex text-sm pl-1">{item.name}</div>{' '}
                      </div>
                      {recordKeys[params?.slug?.[2]].map(
                        (fields, fieldsIndex) => (
                          <div
                            key={fieldsIndex}
                            className={`flex items-center md:w-20 justify-center lg:w-20 xl:w-20 w-2/12 ${
                              fieldsIndex !== 2
                                ? 'dark:text-gray-2 text-[#555A64]'
                                : 'dark:text-green text-[#555A64]'
                            }`}
                          >
                            {item[fields.key] || '0'}
                          </div>
                        ),
                      )}
                    </div>
                    <div className="dark:bg-black/80 h-[1px] bg-gray-11" />
                  </div>
                ))}
            </div>
          </div>
        </div>
        <div className="text-white bg-gray-8 md:hidden md:pb-24 md:pt-12">
          {props.urlTrack ? <Urltracking PageName="Records" /> : null}

          <Head>
            <title>Cricket Records, Latest News | Cricket.com</title>
            <meta
              name="description"
              content="Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com"
            />
            <meta
              property="og:title"
              content="Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com"
            />
            <meta
              property="og:description"
              content="Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com"
            />
            <meta property="og:url" content="https://www.cricket.com/" />
            <meta
              name="twitter:title"
              content="Cricket Score, Match Schedule & Predictions, Latest News | Cricket.com"
            />
            <meta
              name="twitter:description"
              content="Get live cricket scores, scorecard updates, match schedule, predictions & results, stats, latest news & videos of all international, domestic & T20 series at Cricket.com"
            />
            <meta name="twitter:url" content="https://www.cricket.com/" />
            <link rel="canonical" href="https://www.cricket.com/" />
          </Head>
          <div className="mb-6 md:mt-1 mx-2 lg:max-w-5xl lg:mx-auto">
            {/* <div className='db dn-ns'>
            <div className='p-3 flex items-center'>
              <img
                className='mr2 p-2 bg-gray rounded-md'
                onClick={() => window.history.back()}
                src='/svgs/backIconWhite.svg'
                alt='back icon'
              />
              <span className='font-semibold white f35'> {props.sortType} </span>
            </div>
          </div> */}

            <div className="w-full top-0 lg:hidden">
              <div className="py-2.5 px-0.5 items-center flex">
                {/* <img
                className='mr-2 lg:hidden p-2 bg-gray rounded-md'
                onClick={() => window.history.back()}
                src='/svgs/backIconWhite.svg'
                alt='back icon'
              /> */}
                <div
                  onClick={() => window.history.back()}
                  className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
                >
                  <svg width="30" focusable="false" viewBox="0 0 24 24">
                    <path
                      fill="#ffff"
                      d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                    ></path>
                    <path fill="none" d="M0 0h24v24H0z"></path>
                  </svg>
                </div>
                <span className="text-lg white font-semibold"> Records </span>
              </div>

              <div className="md:hidden lg:hidden xl:hidden p-3 my-0 flex items-center -mt-1">
                <span className="font-semibold md:hidden lg:hidden xl:hidden f35">
                  {params?.slug?.[2]}{' '}
                </span>
              </div>
              <div className="flex h-1 w-12 bg-blue-8 -mt-2 ml-3 mb-4"></div>
            </div>

            <h1 className="font-semibold text-sm lg:text-sm hidden lg:hidden md:hidden xl:hidden py-2 pb-2 px-4">
              {' '}
              {params?.slug?.[2]}{' '}
            </h1>
            <div className="ml-2">
              <div className="flex py-2 bg-gray-4 shadow-4 justify-between items-center rounded-t-xl text-xs">
                <div className="flex md:w-8/12 lg:w-8/12 xl:w-8/12 w-8/12">
                  <h2 className="text-xs mx-2 font-semibold">
                    {params?.slug?.[1] == 'batting' ? 'BATSMEN' : 'BOWLERS'}
                  </h2>
                </div>
                {recordKeys[params?.slug?.[2]].map((list, index) => (
                  <div className="flex items-center w-2/12 md:w-20 justify-center xl:w-20 lg:w-20 text-center text-xs font-semibold">
                    {list.label}
                  </div>
                ))}
              </div>
              <div className="bg-black/80 w-full" />

              {data &&
                data.getRecords &&
                data.getRecords[params?.slug?.[1]].map((item, index) => (
                  <div key={index}>
                    <div className="flex py-2 text-xs font-medium justify-between items-center bg-gray">
                      <div className="flex w-8/12 items-center break-words">
                        {' '}
                        <div className="flex items-center justify-between mx-2">
                          <ImageWithFallback
                            fallbackSrc="/pngsV2/playerph.png"
                            className="h-10 w-10 bg-gray object-top object-cover rounded-full"
                            loading="lazy"
                            src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          />
                        </div>
                        <div>
                          <div className="flex">{item.name}</div>{' '}
                          <div className="text-gray-2 font-light">
                            {item.teamName}{' '}
                          </div>
                        </div>
                      </div>
                      {recordKeys[params?.slug?.[2]].map(
                        (fields, fieldsIndex) => (
                          <div
                            key={fieldsIndex}
                            className={`flex items-center md:w-20 justify-center lg:w-20 xl:w-20 w-2/12 ${
                              keysObj[params.slug?.[2]] === fields.key
                                ? 'text-green'
                                : 'text-gray-2'
                            }`}
                          >
                            {item[fields.key] || '0'}
                          </div>
                        ),
                      )}
                    </div>
                    <div className="bg-black/80 h-[1px]" />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default RecordDetails

const keysObj = {
  'Most-Runs': 'runs',
  'Best-Batting-Average': 'avg',
  'Best-Batting-Strike-Rate': 'strikeRate',
  'Most-Hundreds': 'centuries',
  'Most-Fifties': 'fifties',
  'Most-Fours': 'fours',
  'Most-Sixes': 'sixes',
  'Most-Wickets': 'wickets',
  'Best-Bowling-Average': 'avg',
  'Best-Bowling': 'economy',
  'Most-5-Wickets-Haul': 'fiveWickets',
  'Best-Economy': 'economy',
  'Best-Bowling-Strike-Rate': 'strikeRate',
}
const recordKeys = {
  'Most-Runs': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: 'AVG', key: 'avg' },
  ],
  'Highest-Scores': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: 'SCORE', key: 'highestScore' },
  ],
  'Best-Batting-Average': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: 'AVG', key: 'avg' },
  ],
  'Best-Batting-Strike-Rate': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: 'SR', key: 'strikeRate' },
  ],
  'Most-Hundreds': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: '100s', key: 'centuries' },
  ],
  'Most-Fifties': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: '50s', key: 'fifties' },
  ],
  'Most-Fours': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: '4s', key: 'fours' },
  ],
  'Most-Sixes': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'I', key: 'inningsPlayed' },
    { label: 'R', key: 'runs' },
    { label: '6s', key: 'sixes' },
  ],
  'Most-Wickets': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'O', key: 'overs' },
    { label: 'W', key: 'wickets' },
    { label: 'AVG', key: 'avg' },
  ],
  'Best-Bowling-Average': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'O', key: 'overs' },
    { label: 'W', key: 'wickets' },
    { label: 'AVG', key: 'avg' },
  ],
  'Best-Bowling': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'O', key: 'overs' },
    { label: 'W', key: 'wickets' },
    { label: 'ECO', key: 'economy' },
  ],
  'Most-5-Wickets-Haul': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'O', key: 'overs' },
    { label: 'W', key: 'wickets' },
    { label: '5W', key: 'fiveWickets' },
  ],
  'Best-Economy': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'O', key: 'overs' },
    { label: 'W', key: 'wickets' },
    { label: 'ECO', key: 'economy' },
  ],
  'Best-Bowling-Strike-Rate': [
    { label: 'M', key: 'matchesPlayed' },
    { label: 'O', key: 'overs' },
    { label: 'W', key: 'wickets' },
    { label: 'SR', key: 'strikeRate' },
  ],
}
