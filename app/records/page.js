'use client'
import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import Head from 'next/head'
// import { RECORD_VIEW } from "../../constant/Links";
import { getRecordUrl } from '../../api/services'
import Urltracking from '../../components/commom/urltracking'
import StatHubLayout from '../components/shared/statHubLayout'

const Records = () => {
  const [type, setType] = useState('batting')
  const [format, setFormat] = useState('test')

  const typeTabs = ['batting', 'bowling']
  const formatTabs = ['test', 'odi', 't20i']

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  return (
    <StatHubLayout currIndex={4}>
      {/* {props.urlTrack? <Urltracking PageName="Records"  />  :null} */}
      <div className="mb-4 center text-white">
        <div>
          {/* <h1>Records</h1> */}
          <title>
            Cricket Records | ODI, Test, T20, Batting, Bowling Records |
            Cricket.com
          </title>
          <meta
            name="description"
            content="Get all the cricket records, team records, batting records, bowling records, wicket keeper records & all other records for ODI, Test & T20 matches on Cricket.com"
          />
          <meta
            property="og:title"
            content="Cricket Records | ODI, Test, T20, Batting, Bowling Records | Cricket.com"
          />
          <meta
            property="og:description"
            content="Get all the cricket records, team records, batting records, bowling records, wicket keeper records & all other records for ODI, Test & T20 matches on Cricket.com"
          />
          <meta property="og:url" content="https://www.cricket.com/records" />
          <meta
            name="twitter:title"
            content="Cricket Records | ODI, Test, T20, Batting, Bowling Records | Cricket.com"
          />
          <meta
            name="twitter:description"
            content="Get all the cricket records, team records, batting records, bowling records, wicket-keeper records & all other records for ODI, Test & T20 matches on Cricket.com"
          />
          <meta name="twitter:url" content="https://www.cricket.com/records" />
          <link rel="canonical" href="https://www.cricket.com/records" />
        </div>

        {/* mobile */}
        <div className="block">
          <div className=" w-full sticky top-0 ">
            <div className="p-2.5 items-center flex">
              {/* <img
              className="mr-2 lg:hidden p-2 bg-gray rounded-md"
              onClick={() => window.history.back()}
              src="/svgs/backIconWhite.svg"
              alt="back icon"
            /> */}
              <div
                onClick={() => window.history.back()}
                className=" outline-0 cursor-pointer mr-2 lg:hidden  bg-gray rounded"
              >
                <svg width="30" focusable="false" viewBox="0 0 24 24">
                  <path
                    fill="#ffff"
                    d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
              <span className="text-lg text-black dark:text-white font-semibold lg:px-4">
                {' '}
                Records{' '}
              </span>
            </div>
            <div className="flex justify-between bg-gray-8 lg:hidden md:hidden items-center">
              {typeTabs.map((tabName, i) => (
                <span
                  key={i}
                  className={`cursor-pointer text-xs font-semibold flex-auto text-center opacity-100 font-mnr capitalize ${
                    type === tabName
                      ? ' text-blue-9 border-b-2 opacity-100  border-blue-9 border-solid  py-3  '
                      : ' text-gray-2 border-b-2  border-blue-6 border-solid  py-3'
                  }`}
                  onClick={() => setType(tabName)}
                >
                  {tabName}
                </span>
              ))}
            </div>
          </div>
        </div>

        {/* desktop */}
        {/* <h1 className='font-semibold f3 db-l dn pa3 pl0 pt-'>Records</h1> */}

        <div className="lg:hidden md:hidden py-2">
          <div className="my-2 py-0.5 mx-10 text-center flex justify-center items-center uppercase bg-gray-4 rounded-full">
            {formatTabs.map((tabName, i) => (
              <span
                key={i}
                className={`w-28 cursor-pointer text-xs py-1 m-1 ${
                  format === tabName
                    ? 'border-2 rounded-full bg-gray-8 border-green lg:border-green md:border-green lg:border-2'
                    : null
                }`}
                onClick={() => setFormat(tabName)}
              >
                {tabName}
              </span>
            ))}
          </div>
        </div>

        <div className="px-4">
          {/* Button Bar */}

          {/* desktop */}
          <div className="hidden lg:block md:block my-2">
            <div className="flex items-center justify-between">
              <div className="">
                <div className="z-1 py-2 w-full rounded-full">
                  {typeTabs.map((tabName, i) => (
                    <span
                      key={i}
                      className={`cursor-pointer text-xs font-semibold px-10 mx-1 my-0.5 py-2 text-center rounded-full uppercase ${
                        type === tabName
                          ? 'text-white bg-red-6 dark:basebg dark:border dark:border-green dark:text-green dark:bg-transparent'
                          : 'text-black bg-light_gray dark:bg-transparent '
                      }`}
                      onClick={() => setType(tabName)}
                    >
                      {tabName}
                    </span>
                  ))}
                </div>
              </div>
              <div className="hidden lg:flex md:flex">
                <div className="flex rounded-full uppercase py-1">
                  {formatTabs.map((tabName, i) => (
                    <span
                      key={i}
                      className={`cursor-pointer py-1.5 rounded-full mx-1 px-12 ${
                        format === tabName
                          ? 'text-white bg-red-6 dark:basebg dark:border dark:border-green dark:text-green dark:bg-transparent'
                          : 'text-black bg-light_gray dark:bg-transparent'
                      }`}
                      onClick={() => setFormat(tabName)}
                    >
                      <h2 className="text-xs font-semibold">{tabName}</h2>
                    </span>
                  ))}
                </div>
              </div>
            </div>
          </div>

          {/* TabBar */}

          {/* <div className='bg-black/80 h-[1px] w-full ' /> */}

          {/* List Section Starts */}
          {/* {[1,2,3].map((item, index) => ( */}
          {Object.keys(recordList[type]).map((item, index) => (
            <Link
              {...getRecordUrl(format, type, item)}
              passHref
              key={'index'}
              className="link lowercase"
            >
              <div className="flex p-4 justify-between mb-2 dark:bg-gray bg-white dark:text-white text-black dark:text-xs text-lg dark:font-medium font-bold rounded-md items-center">
                <div className=" flex items-center capitalize justify-between text-sm font-medium">
                  <p className="w-2 h-2 bg-green rounded-full lg:hidden md:hidden m-1 ml-0"></p>
                  {recordList[type][item].label}
                </div>
                {/* <img className='ml-4 p-2 bg-gray-4 rounded-md ' src='/svgs/RightSchevronBlack.svg' alt='RightSchevronBlack' /> */}
                <div className="ml-4 p-2 lg:hidden md:hidden bg-gray-4 rounded-md ">
                  {' '}
                  <svg width="18" viewBox="0 0 24 24">
                    <path
                      fill="#38d925"
                      d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                    ></path>
                    <path fill="none" d="M0 0h24v24H0z"></path>
                  </svg>
                </div>
                <div className="ml-4 p-2 lg:block md:block hidden bg-gray-4 rounded-md ">
                  {' '}
                  <svg width="18" viewBox="0 0 24 24">
                    <path
                      fill="#FFFF"
                      d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                    ></path>
                    <path fill="none" d="M0 0h24v24H0z"></path>
                  </svg>
                </div>
              </div>
              <div className="bg-black/80 lg:hidden md:hidden h-[1px] w-full" />
            </Link>
          ))}
          {/* List Section Closing */}
        </div>
      </div>
    </StatHubLayout>
  )
}
export default Records

// # sortType: batting: runs,strikeRate,centuries,fifties,fours,sixes,avg
// # sortType: bowling: wickets,bestBallInInnings,economy,avg,strikeRate,fiveWickets,runs

const recordList = {
  batting: {
    'Most-Runs': { label: 'Most Runs', key: 'runs' },
    'Best-Batting-Average': { label: 'Best Batting Average', key: 'avg' },
    'Best-Batting-Strike-Rate': {
      label: 'Best Batting Strike Rate',
      key: 'strikeRate',
    },
    'Most-Hundreds': { label: 'Most Hundreds', key: 'centuries' },
    'Most-Fifties': { label: 'Most Fifties', key: 'fifties' },
    'Most-Fours': { label: 'Most Fours', key: 'fours' },
    'Most-Sixes': { label: 'Most Sixes', key: 'sixes' },
  },
  bowling: {
    'Most-Wickets': { label: 'Most Wickets', key: 'wickets' },
    'Best-Bowling-Average': { label: 'Best Bowling Average', key: 'avg' },
    'Best-Bowling': { label: 'Best Bowling', key: 'bestBallInInnings' },
    'Most-5-Wickets-Haul': { label: 'Most 5 Wickets Haul', key: 'fiveWickets' },
    'Best-Economy': { label: 'Best Economy', key: 'economy' },
    'Best-Bowling-Strike-Rate': {
      label: 'Best Bowling Strike Rate',
      key: 'strikeRate',
    },
  },
}
