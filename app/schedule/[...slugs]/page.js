'use client'
import React, { useState, useEffect, useRef } from 'react'
import { useQuery } from '@apollo/react-hooks'
import {
  scheduleMatchView,
  getSeries,
  getScheduleTabUrl,
  newScheduleMatchView,
  getFilterTabUrlSeriesHome,
} from '../../api/services'
import {
  SCHEDULE,
  SCHEDULE_NEW,
  SCHEDULE_Axios,
  SCHEDULE_NEW_AXIOS,
} from '../../api/queries'
import Head from 'next/head'
import { usePathname } from 'next/navigation'
// import ScoreCard from '../../components/Common/s';
import Link from 'next/link'
import axios from 'axios'
import format from 'date-fns/format'
import CleverTap from 'clevertap-react'
import Scores from '../../components/commom/score'
import BottomNavBar from '../../components/commom/bottomnavbar'
// import ErrorBoundary, { useErrorHandler } from '../../components/ErrorBoundary';
import Urltracking from '../../components/commom/urltracking'
import MetaDescriptor from '../../components/MetaDescriptor'
import { ShcheduleHome } from '../../constant/MetaDescriptions'
import DataNotFound from '../../components/commom/datanotfound'
import Loading from '../../components/loading'
import CommonDropdown from '../../components/commom/commonDropdown'
import { tsvFormatValue } from 'd3'
const dropDownArrow = '../../public/pngsV2/allrounder-line-white.png'
/**
 * @description schedule listing
 * @route /schedule/matchStats/matchType
 * @example /schedule/international/upcoming-matches
 * @param { Object } props WIP
 */

async function getServerData(tab, status) {
  //   console.log('context.query.slugs', context.params)

  //   console.log(tab, status, 'ttttt')

  const res = await axios.post(process.env.API, {
    query: SCHEDULE_NEW_AXIOS,
    variables: { type: tab, status: status, page: 0 },
  })
  // console.log(res, 'data*')
  return { data: res.data.data, tab, status }
}

const options = [
  { valueId: 'All', valueLable: 'All' },
  {
    valueId: 'Players',
    valueLable: 'Players',
  },
  { valueId: 'Records', valueLable: 'Records' },
]

export default function Schedule({ params }) {
  const timer1 = (com) => {
    setTimeout(() => com, 1 * 1000)
  }
  const [props, setProps] = useState()
  let tab = params?.slugs?.[0]
  let status = params?.slugs?.[1]
  if (!status) {
    status = tab
    tab = 'all'
  }
  if (status == 'results') {
    status = 'completed'
  }
  const slectedCss =
    'flex  items-center justify-center rounded-3xl  border-2 border-green  white font-medium text-xs w-1/3 bg-gray-8 py-[6px]'
  const nonSlected =
    'flex items-center justify-center rounded-3xl  py-[6px] white font-medium text-xs w-1/3 p-1 '

  const pathName = usePathname()
  const wrapperRef = useRef(null)
  const [propLoading, setPropLoading] = useState(false)
  const [showDropDown, setShowDropDown] = useState(false)
  const tabs = ['all', 'international', 'domestic']
  const statusTabs = ['upcoming', 'live', 'Results']
  const [pagination, setPagination] = useState(0)
  const [refresh, setRefresh] = useState(false)
  const [windows, updateWindows] = useState()

  useEffect(() => {
    window.addEventListener('scroll', (event) => handleScroll(event))
    if (global.window) updateWindows(global.window)
  }, [global.window, pagination, status])

  // const isMounted = useRef(false)
  const count = useRef(0)
  // useEffect(() => {
  //   if (count.current !== 0) {
  //     window.addEventListener('scroll', handleScroll)
  //     getServerData(tab, status.split('-')[0]).then((res) => {
  //       setProps(res)
  //     })
  //   }
  //   count.current++
  // }, [])
  let { loading, error, data, fetchMore } = useQuery(SCHEDULE_NEW, {
    variables: {
      type: tab,
      status: status?.split('-')[0].toLowerCase(),
      page: 0,
    },
  })
  useEffect(() => {
    function handleClickOutside(event) {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setShowDropDown(false)
      }
    }

    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [wrapperRef])

  useEffect(() => {
    setRefresh(false)
  })

  useEffect(() => {
    setShowDropDown(false)
  }, [tab])

  window.addEventListener('scroll', () => handleScroll())

  let scrl = useRef(null)

  const handlePagination = (fetchMore) => {
    if (status.split('-')[0].toLowerCase() !== 'live') {
      fetchMore({
        variables: {
          type: tab,
          status: status.split('-')[0].toLowerCase(),
          page: pagination,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!fetchMoreResult) {
            return previousResult
          }
          let parsedPre = previousResult.newSchedule
          let newData = fetchMoreResult.newSchedule

          let removeDupli = [...parsedPre, ...newData]
          let uniq = new Set(
            removeDupli && removeDupli.map((e) => JSON.stringify(e)),
          )
          let res = Array.from(uniq).map((e) => JSON.parse(e))

          let resultSchedule = Object.assign({}, previousResult, {
            newSchedule: res,
            _typeName: 'newSchedule',
          })
          return { ...resultSchedule }
        },
      })
    }
    if (status.split('-')[0].toLowerCase() === 'live') {
      fetchMore({
        variables: {
          type: tab,
          status: 'live',
          page: pagination,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!fetchMoreResult) {
            return previousResult
          }
          let parsedPre = previousResult.newSchedule
          let newData = fetchMoreResult.newSchedule

          let removeDupli = [...parsedPre, ...newData]
          let uniq = new Set(
            removeDupli && removeDupli.map((e) => JSON.stringify(e)),
          )
          let res = Array.from(uniq).map((e) => Object.keys(JSON.parse(e)))

          let resultSchedule = Object.assign({}, previousResult, {
            newSchedule: res,
            _typeName: 'newSchedule',
          })
          return { ...resultSchedule }
        },
      })
    }
  }
  // const handleScroll = (evt) => {
  //   // console.log("evtevtevtevtevtevt",data)
  //   // console.log("evtevtevtevtevtevt",data)
  //   if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
  //     // setPagination((prev)=>prev + 1)

  //     // alert("end")
  //     console.log('checkkkkk', data, filter)

  //     // alert(filter.valueLable)
  //     handlePagination()
  //   }
  // }
  const handleScroll = async (evt) => {
    // if (true) {
    //   setPagination((prevPage) => prevPage + 1)
    //   handlePagination(fetchMore)
    // }
    if (
      window.innerHeight + window.scrollY >=
      document.body.offsetHeight - 200
    ) {
      // if (window.scrollY/1000>(pagination+1)) {
      setPagination(pagination + 1)

      handlePagination(fetchMore)
    }
  }
  const handleSeriesNavigation = (match) => {
    match = match.matches[0]

    // CleverTap &&
    //   CleverTap.initialize &&
    //   CleverTap.initialize('Series', {
    //     Source: 'Schedule',
    //     SeriesID: match.seriesID,
    //     TeamAID: match.matchScore[0].teamID,
    //     TeamBID: match.matchScore[1].teamID,
    //     Platform: global.window.localStorage
    //       ? global.window.localStorage.Platform
    //       : '',
    //   })
  }

  const handleNavigation = (match) => {
    match = match.matches[0]
    const event =
      match.matchStatus === 'upcoming' || match.matchStatus === 'live'
        ? 'CommentaryTab'
        : 'SummaryTab'
    CleverTap.initialize(event, {
      Source: 'Schedule',
      MatchID: match.matchID,
      SeriesID: match.seriesID,
      TeamAID: match.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: match.matchStatus,
      Platform: localStorage ? localStorage.Platform : '',
    })
    if (typeof window !== 'undefined') {
      localStorage['source'] = '/schedule'
    }
  }

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  function resolver(data) {
    return (
      <>
        <div className="hidden md:block" ref={scrl}>
          <div className="">
            <div className="w-full flex item-center justify-center flex-col">
              {data && data.newSchedule && data.newSchedule.length > 0 ? (
                data.newSchedule.map((x, i) => {
                  return (
                    <>
                      {' '}
                      {x.seriesView && (
                        <div className="w-full rounded md:cursor-pointer dark:m-1 md:mb-6 bg-white shadow-sm">
                          <div key={i} className="w-full mb-2">
                            <div className="w-full flex flex-col rounded-md md:cursor-pointer p-4">
                              <Link {...getSeries(x)} passHref legacyBehavior>
                                <div
                                  className="md:w-full dark:mx-2 flex justify-start items-center p-2 shadow-4 bg-gray md:bg-white md:border md:border-slate-200 rounded-lg hover:shadow"
                                  onClick={() => handleSeriesNavigation(x)}
                                >
                                  <div className="w-full flex justify between items-center">
                                    <div className="">
                                      <div
                                        className={`text-center text-white p-2 lg:hidden md:hidden text-xs font-semibold`}
                                        style={{
                                          background:
                                            x.matches[0].league ==
                                            'International'
                                              ? '#DE4727'
                                              : '#6A91A9',
                                        }}
                                      >
                                        {x.matches[0].league == 'International'
                                          ? 'INT'
                                          : 'DOM'}
                                      </div>
                                      <div
                                        className={`text-center rounded-md w-full px-3 text-white py-1 lg:block md:block hidden text-[0.6rem] font-semibold`}
                                        style={{
                                          background:
                                            x.matches[0].league ==
                                            'International'
                                              ? '#9B000D'
                                              : '#6A91A9',
                                        }}
                                      >
                                        {x.matches[0].league == 'International'
                                          ? 'INT'
                                          : 'DOM'}
                                      </div>
                                    </div>
                                    <div className="dark:text-xs text-sm dark:ml-4 ml-4 font-semibold dark:text-white text-slate-900">
                                      {x.seriesName}
                                    </div>
                                  </div>
                                  <div className="w-1/12 bg text-right flex items-end justify-end">
                                    <span className="dark:text-xs text-base text-black dark:text-white mx-1">
                                      {' '}
                                      &#x276F;{' '}
                                    </span>
                                  </div>
                                </div>
                              </Link>

                              {/* <div className='divider' /> */}
                              {/* {console.log('p')} */}
                              <div
                                className={
                                  x.matches && x.matches.length > 2
                                    ? 'w-full lg:mt-2 md:mt-2 xl:mt-2 flex pt-2'
                                    : x.matches && x.matches.length === 2
                                    ? 'lg:grid lg:grid-cols-3 lg:mt-2 md:mt-2 xl:mt-2 w-full  flex py-1 flex-row'
                                    : 'md:w-4/12 lg:mt-2 md:mt-2 xl:mt-2 flex py-1 flex-row'
                                }
                              >
                                {x.matches &&
                                  x.matches.map((item, index) => {
                                    return (
                                      <>
                                        {/* {console.log(item, 'oo')} */}
                                        <div
                                          className={
                                            x.matches.length > 1
                                              ? ' flex w-full'
                                              : ' flex w-full'
                                          }
                                        >
                                          <Link
                                            {...newScheduleMatchView(item)}
                                            passHref
                                            className={
                                              x.matches.length > 1
                                                ? 'md:w-[21rem] mr-4'
                                                : 'md:w-[21rem] mr-4'
                                            }
                                            onClick={() => handleNavigation(x)}
                                          >
                                            {false && (
                                              <div className="flex py-2">
                                                <div className=" flex items-center justify-start f7 text-white fw5 truncate w-90 ml1">
                                                  {(x.matchType == 'ODI' ||
                                                    x.matchType == 'T20' ||
                                                    x.matchType == 'T100' ||
                                                    x.matchType == 'Test') && (
                                                    <div
                                                      className="h1 w15 pa1
                                              f10 fw5 white flex items-center
                                            justify-center  br2 bg-gray"
                                                    >
                                                      {(x.matchType == 'ODI' ||
                                                        x.matchType == 'T20' ||
                                                        x.matchType == 'T100' ||
                                                        x.matchType ==
                                                          'Test') &&
                                                        x.matchType}
                                                    </div>
                                                  )}
                                                  <div
                                                    className={
                                                      x.matchType == 'ODI' ||
                                                      x.matchType == 'T100' ||
                                                      x.matchType == 'T20' ||
                                                      x.matchType == 'Test'
                                                        ? `flex ml2 white`
                                                        : 'flex white'
                                                    }
                                                  >
                                                    {item.matchNumber}{' '}
                                                  </div>
                                                </div>
                                                <div className="flex">
                                                  {item.matchStatus ===
                                                    'live' && (
                                                    <div className="flex items-center mr1 ba b--black br-pill pv05 ph2 bg-gray">
                                                      <span className="bg-green h04 w04 br-100 bor" />
                                                      <span className="f10 fw5 white pl1">
                                                        LIVE
                                                      </span>
                                                    </div>
                                                  )}
                                                </div>
                                              </div>
                                            )}
                                            {false && (
                                              <div className="flex items-center justify-between pb2">
                                                <div className="flex items-center f7 text-gray-2">
                                                  <img
                                                    alt="location"
                                                    src="/svgs/location-icon.svg"
                                                    className="ib w1 h1 f4"
                                                  />{' '}
                                                  {item.venue}
                                                </div>
                                              </div>
                                            )}
                                            <Scores
                                              hideSeriesName={true}
                                              featured={true}
                                              showpadding={false}
                                              data={item}
                                              shortName={true}
                                              from={'schedule'}
                                            />
                                          </Link>
                                        </div>

                                        {index == x.matches.length - 1 &&
                                          x.seriesAvailable && (
                                            <Link
                                              {...getSeries(item)}
                                              passHref
                                              legacyBehavior
                                            >
                                              <div className="w-80 dn-l items-center justify-center flex lg:hidden md:hidden">
                                                <div className="w-20 p-2 flex  justify-center rounded border-2 text-green border-green text-xs items-center font-bold">
                                                  {' '}
                                                  View ALL{' '}
                                                </div>
                                              </div>
                                            </Link>
                                          )}
                                      </>
                                    )
                                  })}
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                    </>
                  )
                })
              ) : (
                <div className="flex items-center justify-center bg-white shadow px-3 py-4 rounded-md">
                  <DataNotFound displayText={'No matches available'} />
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="mt3 md:hidden hidescroll pr1-ns mx-2">
          <div
            className="desktopScroll hidescroll"
            // onScroll={(evt) => handleScroll(evt)}
          >
            <div className="w-full flex justify-center flex-col items-center">
              {data && data.newSchedule && data.newSchedule.length > 0 ? (
                data.newSchedule.map((x, i) => {
                  return (
                    <>
                      {' '}
                      {x.seriesView && (
                        <div className="w-full rounded mr-1 ">
                          <div key={i} className="w-full">
                            <div className="w-full flex flex-col rounded-md p-1">
                              <Link {...getSeries(x)} passHref legacyBehavior>
                                <div
                                  className="w-full flex justify-start items-center p-2 shadow-4 bg-gray rounded-lg"
                                  onClick={() => handleSeriesNavigation(x)}
                                >
                                  <div className="flex items-center w-11/12">
                                    <div className="mb:w-1/5 md:w-2/12 ml1">
                                      <div
                                        className={`text-center br2 text-white  p-2  text-xs font-semibold `}
                                        style={{
                                          background:
                                            x.matches[0].league ==
                                            'International'
                                              ? '#DE4727'
                                              : '#6A91A9',
                                        }}
                                      >
                                        {x.matches[0].league == 'International'
                                          ? 'INT'
                                          : 'DOM'}
                                      </div>
                                    </div>
                                    <div className="text-xs ml-4 font-semibold md:ml-4 text-white">
                                      {x.seriesName}
                                    </div>
                                  </div>
                                  <div className="w-1/12 bg text-right flex items-end justify-end">
                                    <span className="text-xs text-white mx-1">
                                      {' '}
                                      &#x276F;{' '}
                                    </span>
                                  </div>
                                </div>
                              </Link>
                              {/* <div className='divider' /> */}
                              <div
                                className={
                                  x.matches && x.matches.length > 1
                                    ? 'w-full mt2 flex hidescroll overflow-scroll p-1'
                                    : 'w-full mt2 flex p-1 flex-row'
                                }
                              >
                                {x.matches &&
                                  x.matches.map((item, index) => {
                                    return (
                                      <>
                                        <div className={'w-full flex'}>
                                          <Link
                                            {...newScheduleMatchView(item)}
                                            passHref
                                            className={
                                              x.matches.length > 1
                                                ? 'md:w-96 py-2 mr-2 w-80'
                                                : 'pt-1 w-[23rem]'
                                            }
                                            onClick={() => handleNavigation(x)}
                                          >
                                            {false && (
                                              <div className=" flex  pv2 ">
                                                <div className=" flex items-center justify-start f7 text-white fw5 truncate w-90 ml1">
                                                  {(x.matchType == 'ODI' ||
                                                    x.matchType == 'T20' ||
                                                    x.matchType == 'T100' ||
                                                    x.matchType == 'Test') && (
                                                    <div
                                                      className="h1 w15 pa1
                                              f10 fw5 white flex items-center
                                            justify-center  br2 bg-gray"
                                                    >
                                                      {(x.matchType == 'ODI' ||
                                                        x.matchType == 'T20' ||
                                                        x.matchType == 'T100' ||
                                                        x.matchType ==
                                                          'Test') &&
                                                        x.matchType}
                                                    </div>
                                                  )}
                                                  <div
                                                    className={
                                                      x.matchType == 'ODI' ||
                                                      x.matchType == 'T100' ||
                                                      x.matchType == 'T20' ||
                                                      x.matchType == 'Test'
                                                        ? `flex ml2 white`
                                                        : 'flex white'
                                                    }
                                                  >
                                                    {item.matchNumber}{' '}
                                                  </div>
                                                </div>
                                                <div className="flex ">
                                                  {item.matchStatus ===
                                                    'live' && (
                                                    <div className="flex items-center mr1 ba b--black br-pill pv05 ph2 bg-gray">
                                                      <span className="bg-green h04 w04 br-100 bor" />
                                                      <span className="f10 fw5 white pl1">
                                                        LIVE
                                                      </span>
                                                    </div>
                                                  )}
                                                </div>
                                              </div>
                                            )}
                                            {false && (
                                              <div className="flex items-center justify-between  pb2">
                                                <div className="flex items-center f7 text-gray-2">
                                                  <img
                                                    alt="location"
                                                    src="/svgs/location-icon.svg"
                                                    className="ib w1 h1 f4"
                                                  />{' '}
                                                  {item.venue}
                                                </div>
                                              </div>
                                            )}
                                            {/* <Scores
                                              hideSeriesName={true}
                                              featured={true}
                                              showpadding={false}
                                              data={item}
                                              shortName={true}
                                            /> */}
                                            <Scores
                                              hideSeriesName={true}
                                              featured={true}
                                              showpadding={false}
                                              data={item}
                                              shortName={true}
                                              from={'schedule'}
                                            />
                                          </Link>
                                        </div>

                                        {index == x.matches.length - 1 &&
                                          x.seriesAvailable && (
                                            <Link
                                              {...getSeries(item)}
                                              passHref
                                              legacyBehavior
                                            >
                                              <div className="w-80 dn-l   items-center justify-center flex  lg:hidden md:hidden  ">
                                                <div className="w-20 p-2 flex  justify-center rounded border-2  text-green border-green text-xs items-center font-bold ">
                                                  {' '}
                                                  View ALL{' '}
                                                </div>
                                              </div>
                                            </Link>
                                          )}
                                      </>
                                    )
                                  })}
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                    </>
                  )
                })
              ) : (
                <DataNotFound displayText={'No matches available'} />
              )}
            </div>
          </div>
        </div>
      </>
    )
  }

  if (loading) {
    return (
      <div>
        <Loading />
      </div>
    )
  }
  if (data) {
    // clearTimeout(timer1)
    return data ? (
      <>
        <MetaDescriptor section={ShcheduleHome(tab, status, pathName)} />

        {/* {props.urlTrack ? <Urltracking PageName="Schedule" /> : null} */}
        {/* <div className="hidden md:flex lg:flex bg-basered z-1 px-2 py-4 items-center justify-center text-white">
          <div className="w-2/12 flex items-center justify-end pr-8">
            <span className="text-xl font-semibold capitalize">SCHEDULE</span>
          </div>
        </div> */}
        <div className="center relative">
          <div className="dark:bg-gray-8 top-0 w-full">
            <div className="flex items-center justify-start p-3 md:hidden">
              <div
                className="p-3 bg-gray rounded-md "
                onClick={() => window.history.back()}
              >
                <img
                  className="flex items-center justify-center h-4 w-4 rotate-180"
                  src="/svgsV2/RightSchevronWhite.svg"
                  alt=""
                />
              </div>

              <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
                <div className="">Schedule</div>
              </div>
            </div>

            {/* <h1 className="hidden md:flex lg:flex bg-basered text-lg font-bold z-1 px-2 py-4 items-center justify-center text-white">
              SCHEDULE
            </h1> */}

            {/* <div className='dn flex items-center  justify-evenly pb-8'>
               {statusTabs.map((tabName, i) => (
                 <Link key={i} {...getScheduleTabUrl(tab, tabName)} replace passHref>
                   <a
                     // onClick={() => setStatus(tabName.toLowerCase())}
                     className={`ba br2 pa2 tc w-4/12 text-center mr3 md:cursor-pointer  ${
                       (status == 'upcoming' ? 'upcoming' : status == 'live' ? 'live' : 'results') ===
                       tabName.toLowerCase()
                         ? 'text-blue-9  border-b-2 border-blue-9'
                         : 'border-b-2 text-gray border-gray '
                     }`}>
                     <h2 className='f6 fw5'>{tabName === 'Live' ? 'LIVE' : tabName.toUpperCase()}sss</h2>
                   </a>
                 </Link>
               ))}F
             </div> */}
            <div className="bg-gray-8 md:hidden">
              <div className="flex items-center justify-between mb-10">
                {statusTabs.map((tabName, i) => (
                  <Link
                    key={i}
                    {...getScheduleTabUrl(tab, tabName)}
                    replace
                    passHref
                    legacyBehavior
                  >
                    <div
                      className={`flex justify-center items-center p-2 md:cursor-pointer text-center text-xs font-semibold w-4/12 pb-2 ${
                        status?.split('-')[0].toLowerCase() ===
                          tabName.toLowerCase() ||
                        (status?.split('-')[0].toLowerCase() == 'completed' &&
                          tabName == 'Results')
                          ? 'text-blue-9 border-b-4 border-blue-9   '
                          : ' border-b-4 border-gray-2 text-white'
                      }`}
                    >
                      <div>
                        {' '}
                        {tabName === 'Live' ? 'LIVE' : tabName.toUpperCase()}
                      </div>

                      {tabName === 'Live' ? (
                        <div class="ml-2 bg-green h-1 w-1 rounded-full"></div>
                      ) : null}
                    </div>
                  </Link>
                ))}
              </div>
            </div>

            <div className="lg:flex md:flex justify-between w-full hidden my-4">
              <div className="w-5/12 border-none dark:bg-gray-4 rounded-full p-1 text-xs font-semibold flex md:gap-2 lg:gap-2 text-black dark:text-white">
                {statusTabs.map((tabName, i) => (
                  <Link
                    key={i}
                    {...getScheduleTabUrl(tab, tabName)}
                    replace
                    passHref
                    legacyBehavior
                  >
                    <div
                      className={`flex gap-3 rounded-full justify-center text-xs items-center px-1 py-2 md:cursor-pointer truncate w-full ${
                        status?.split('-')[0].toLowerCase() ===
                          tabName.toLowerCase() ||
                        (status?.split('-')[0].toLowerCase() === 'completed' &&
                          tabName === 'Results')
                          ? 'text-white bg-red-6 text-xs dark:basebg dark:border dark:border-green dark:text-green dark:bg-transparent'
                          : 'bg-light_gray text-xs dark:bg-transparent'
                      }`}
                    >
                      <div>
                        {tabName === 'Live' ? 'LIVE' : tabName.toUpperCase()}
                      </div>

                      {tabName === 'Live' ? (
                        <div class="ml-2 text-xs bg-green h-1 w-1 rounded-full"></div>
                      ) : null}
                    </div>
                  </Link>
                ))}
              </div>
              <div className="w-2/12 mr-3 mt-1 z-10">
                <CommonDropdown
                  variant="contained"
                  showExpandIcon="true"
                  isSchedule
                  defaultSelected={{
                    valueId: tab.toUpperCase(),
                    valueLable: tab.toUpperCase(),
                  }}
                  options={tabs?.map((tabName, i) => ({
                    valueId: tabName.toUpperCase(),
                    valueLable: tabName.toUpperCase(),
                    renderValue: (
                      <Link
                        key={i}
                        {...getScheduleTabUrl(tabName, status)}
                        replace
                        passHref
                        className="text-black text-bold truncate bg-white border-b-2 border-b-solid first:rounded-tr-xl first:rounded-tl-xl border-b-[#E2E2E2] text-center text-xs
                          w-full md:cursor-pointer p-1"
                      >
                        {tabName.toUpperCase()}
                      </Link>
                    ),
                  }))}
                />
              </div>
            </div>
            <div className="lg:hidden md:hidden -mt-4">
              <div className="mx-4 my-2 p-1 center text-center flex justify-between captilize bg-gray rounded-xl uppercase">
                {/* {console.log(tabs, 'tabs12')} */}
                {tabs.map((tabName, i) => (
                  <Link
                    key={i}
                    {...getScheduleTabUrl(tabName, status)}
                    replace
                    passHref
                    className={`text-white  text-xs ${
                      tabName === 'international' ? 'w-4/12' : 'w-4/12'
                    } md:cursor-pointer p-1 ${
                      tab === tabName
                        ? 'rounded-xl border-green border bg-gray-4 text-white font-semibold'
                        : null
                    }`}
                  >
                    {tabName}
                  </Link>
                ))}
              </div>
            </div>
            <div>
              <div className="hidden md:block flex-ns justify-between items-center relative pb1 pl3-m white ">
                {/* <div className="flex justify-center mt-4 mb-4">
                  <div className="flex justify-evenly w-11/12 bg-gray rounded-3xl capitalize tracking-wide p-1 ">
                    {tabs.map((tabName, i) => (
                      <Link
                        key={i}
                        {...getScheduleTabUrl(tabName, status)}
                        replace
                        passHref
                        className={`text-white  text-center  text-xs ${
                          tabName === 'international' ? 'w-4/12' : 'w-4/12'
                        } md:cursor-pointer p-1 ${
                          tab === tabName
                            ? 'rounded-xl border-green border bg-gray-4 text-white  font-semibold'
                            : null
                        }`}
                      >
                        {tabName}
                      </Link>
                    ))}
                  </div>
                </div> */}

                <div className="w-25 hidden " ref={wrapperRef}>
                  <div className="dropdown relative">
                    <div className="h-100 pt2 ph3 white flex items-center flex-col md:cursor-pointer bg-green">
                      <div className="flex items-center pb2 pt1">
                        <div className="nowrap f6 fw5">
                          {tab.toUpperCase()}{' '}
                        </div>
                        <img
                          className="pl2 w1 "
                          src={'/svgs/downArrowFilled.svg'}
                          alt="downArrowFilled"
                        />
                      </div>
                    </div>
                    <div className="ba bg-gray border-none absolute w-full dropdown-content z-99999 select-none">
                      {tabs.map((tabName, i) => (
                        <Link
                          key={i}
                          {...getScheduleTabUrl(tabName, status)}
                          replace
                          passHref
                          onClick={() => setShowDropDown((prev) => !prev)}
                          className="ph3 pv2 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30 "
                        >
                          <div>
                            {/* <img alt={menuA.icon} height='22' width='20' src={menuA.icon} /> */}
                          </div>
                          <div className="ma0 pb0 pt1 nowrap pl2  pb2 text-white fw5 f6">
                            {' '}
                            {tabName.toUpperCase()}
                          </div>
                        </Link>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-col ">
            {false
              ? errorHandler('Data not available')
              : data && resolver(data)}
            <div className="mt5 ">
              <BottomNavBar />
            </div>
          </div>
        </div>
      </>
    ) : (
      <div></div>
      // <DataNotFound />
    )
  } else {
    console.log('1212=====', <div></div>)
  }
}

// export const getStaticPaths = async () => {
//     // console.log("contextcontextcontextcontext",context)
//     // let [tab, status] = context.query.slugs;
//     const {data} = await axios.post(process.env.API, {
//         query: SCHEDULE_NEW_AXIOS,
//         variables: { type: 'all', status: 'completed', page: 0 }
//       });

//     //  let data = HOMEPAGE.data.data
//     // console.log("datadatadatadata",data.data)

// //   console.log("ssss",data.getFRCHomePage.completedmatches)
// //   console.log("datadatadatadata",data)
//      let playerPaths =data && data.data.newSchedule && data.data.newSchedule.map((item) => {
//       return {
//         params: {
//           slugs: [ item.seriesID.toString(),item.seriesName.replace(/[^a-zA-Z0-9]+/g,' ')
//           .split(' ')
//           .join('-')
//           .toLowerCase()]
//         }
//       };
//     });
// // let playerPaths=[]
//     return {
//       paths: [...playerPaths],
//       fallback: true
//     };
//   };
