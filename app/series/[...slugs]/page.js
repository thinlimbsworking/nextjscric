'use client'
import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import Tab from '../../components/shared/Tab';
import FilterPopUp from '../../components/series/filterpop';
import { usePathname, useRouter } from 'next/navigation';
import { SERIES } from '../../constant/MetaDescriptions';
import Loading from '../../components/loading';
import ImageWithFallback from '../../components/commom/Image';
import {
    getSeries,
    getFilterTabUrlSeries,
  } from '../../api/services'
// import SeriesBattingStats from '../../components/seriesDetails/seriesBattingStats'
// import SeriesView from '../../components/seriesDetails/seriesView';
import Urltracking from '../../components/commom/urltracking';
// import SeriesLayout from '../../components/series/serieslayout';
import DataNotFound from '../../components/commom/datanotfound';
import {
    SERIES_DETAILS_AXIOS,
    GET_ARTICLES_BY_ID_AND_TYPE_AXIOS,
    GET_POINTS_TABLE_AXIOS,
    TOP_GUNS_AXIOS,
    SERIES_VENUES_AXIOS,
    GET_SERIES_SQUADS_AXIOS,
    SERIES_DETAILS,
    GET_ARTICLES_BY_ID_AND_TYPE,
    GET_SERIES_SQUADS,
    GET_POINTS_TABLE,
    SERIES_VENUES,
    TOP_GUNS
} from '../../api/queries';

import { SeriesView as SeriesSeoView } from '../../constant/MetaDescriptions';
import MetaDescriptor from '../../components/MetaDescriptor';
import useCleverTab from '../../components/customHooks/useCleverTab';
import { data } from 'autoprefixer';
import Heading from '../../../components/commom/heading';
import { useLazyQuery, useQuery } from '@apollo/client';

function getQuery(tab, seriesID) {
    switch (tab) {
        case 'matches':
            return {
                comp: SERIES_DETAILS,
                variables: { seriesID: seriesID }
            }
        case 'news':
            return {
                comp: GET_ARTICLES_BY_ID_AND_TYPE,
                variables: { type: 'series', Id: seriesID }
            }
        case 'squads':
            return {
                comp: GET_SERIES_SQUADS,
                variables: { seriesID: seriesID }
            }
        case 'points-table':
            return {
                comp: GET_POINTS_TABLE,
                variables: { tourID: seriesID }
            }
        case 'stadiums':
            return {
                comp: SERIES_VENUES,
                variables: { seriesID: seriesID }
            }
        case 'stats':
            return {
                comp: TOP_GUNS,
                variables: { tourID: seriesID }
            }
        case 'home':
            return {
                comp: SERIES_DETAILS,
                variables: { seriesID: seriesID }
            }

        default:
            break;
    }

};



const SeriesBattingStats = dynamic(() => import('../../components/seriesDetails/seriesBattingStats'), {
    loading: () => <Loading />
})
const SeriesView = dynamic(() => import('../../components/seriesDetails/seriesView'), {
    loading: () => <Loading />
})
const SeriesLayout = dynamic(() => import('../../components/series/serieslayout'), {
    loading: () => <Loading />
})


const componentList = {
    matches: dynamic(() => import('../../components/seriesDetails/seriesMatches'), {
        loading: () => <Loading />
    }),
    news: dynamic(() => import('../../components/seriesDetails/seriesArticles'), {
        loading: () => <Loading />
    }),
    'points-table': dynamic(() => import('../../components/seriesDetails/pointsTable'), {
        loading: () => <Loading />
    }),
    squads: dynamic(() => import('../../components/seriesDetails/seriesSquad'), {
        loading: () => <Loading />
    }),
    stadiums: dynamic(() => import('../../components/seriesDetails/seriesVenues'), {
        loading: () => <Loading />
    }),
    stats: dynamic(() => import('../../components/seriesDetails/seriesStats'), {
        loading: () => <Loading />
    }),
    home: dynamic(() => import('../../components/seriesDetails/seriesHomePage'), {
        loading: () => <Loading />
    })
};
const homeTabs = [
    { name: 'UPCOMING', value: 'upcoming' },
    { name: 'ONGOING', value: 'ongoing' },
    { name: 'COMPLETED', value: 'completed' },
  ]
  const types = [
    { name: 'All', value: 'all' },
    { name: 'International', value: 'international' },
    { name: 'Domestic', value: 'domestic' },
    { name: 'T20 League', value: 't20league' },
    { name: 'Women', value: 'womens' },
  ]

/**
 * @description Multiple views for individual series
 * @route /series/tabName/seriesID/seriesName/currentSeriesTab
 * @example /series/domestic/1148/quadrangular-under-19-series-in-south-africa-2020/matches
 * @param { Object } props
 */




export default function SeriesDetails(context) {
    
    const router = useRouter();
    const asPath = usePathname();
    let [type, seriesID, seriesSlug, tab] = context.params.slugs;
    let query = getQuery(tab, seriesID);
    const [dataList, setDataList] = useState([])
    const [openFIlter, setOpenFilter] = useState(false)
    const [filterCall, setFilterCall] = useState(true)
    const [formatType, setFormatType] = useState([])
    const [countryName, setCountryName] = useState([])
    const [currType, setCurrType] = useState(types.find((ele) => ele.value === asPath?.split('/')[3]) || types[0])
    const [currTab, setTab] = useState(homeTabs.find((ele) => ele.value === type))
    let tabs = ['HOME', 'MATCHES', 'POINTS TABLE', 'SQUADS', 'STATS', 'NEWS', 'STADIUMS'];


    const { data, loading: loading2, error: error1 } = useQuery(SERIES_DETAILS, {
        variables: { seriesID },
        enabled: false
    })
    const { data: comp, loading: loading1, error } = useQuery(query?.comp || SERIES_DETAILS, {
        variables: query?.variables || {},
        enabled: false,
    })



    let Component = componentList[tab];




    const handleNavigation = (tab) => {
        // pushEvent(`Series${tab === 'POINTS TABLE'
        //         ? 'PointsTable'
        //         : tab === 'SQUADS'
        //             ? 'Squads'
        //             : tab === 'STATS'
        //                 ? 'Stats'
        //                 : tab === 'NEWS'
        //                     ? 'News'
        //                     : tab === 'STADIUMS'
        //                         ? 'Stadiums'
        //                         : ''
        //     }Home`, {
        //     Source: 'Series',
        // })

    };
    const handleShare = () => {
        if (window.navigator.share !== undefined) {
          window.navigator.share({
            title: `Hey, check out the upcoming series on cricket.com: ${window.location.href} | Download Now: https://cricketapp.app.link/downloadapp`,
            text: `Hey, check out the upcoming series on cricket.com: ${window.location.href} | Download Now: https://cricketapp.app.link/downloadapp`,
          })
        }
      }
    const getUrl = (tabName, type, seriesSlug, seriesID) => {
        let currentTab = tabName.split(' ').join('-').toLowerCase();
        return getSeries({ seriesSlug, seriesID, type }, currentTab);
    };


    return <div className='mt-3'> 




        {data?.matcheslist?.[0]?.seriesName && <div className=' hidden md:flex lg:flex   z-1 p-2 items-center justify-center dark:text-white mb-1 '>
            <div className='w-full flex items-center  pr-8'>
                {/* <span className='text-xl  font-semibold capitalize'>{data?.matcheslist?.[0]?.seriesName || 'SERIES'}</span> */}
                <Heading heading={data?.matcheslist?.[0]?.seriesName} />
            </div>
        </div>}
        {context.params.slugs?.length != 2 && context.params.slugs?.length != 3 && (
            <MetaDescriptor
                section={SeriesSeoView(
                    {
                        seriesName: seriesSlug?.split('-').join(' ').toUpperCase(),
                    },
                    asPath,
                    tab
                )}
            />
        )}
        {/* {context.params.slugs?.length == 4 && <MetaDescriptor
            section={SERIES(
                {
                    tabInfo: ''
                },
            )}
        />} */}
        {context.params.slugs?.length == 2 && (
            <MetaDescriptor
                section={SERIES({
                    tabInfo: tab
                })}
            />
        )}
        {/* {urlTrack ? <Urltracking PageName='Series' /> : null} */}

        {context.params.slugs?.length == 2 && (
            <>
            <MetaDescriptor
        section={SERIES({
          tabInfo: '',
        })}
      />
      {openFIlter && (
        <FilterPopUp
          filterCall={filterCall}
          setFilterCall={setFilterCall}
          setOpenFilter={setOpenFilter}
          formatType={formatType}
          setFormatType={setFormatType}
          countryName={countryName}
          setCountryName={setCountryName}
        />
      )}

      {/* {props.urlTrack ? <Urltracking PageName='Series' /> : null} */}

      <div className=" w-full bg-gray-8 ">
        <div className="flex items-center justify-between px-2 md:hidden lg:hidden">
          <div className="flex  gap-2 items-center justify-start">
            <div className="p-2 rounded-md bg-gray-4">
              <img
                class="flex items-center justify-center h-4 w-4 rotate-180"
                onClick={() => router.push('/')}
                src="/svgsV2/RightSchevronWhite.svg"
                alt="back icon"
              />
            </div>
            <div className="text-base font-bold">Series</div>
          </div>
          <div className="p-2 rounded-md bg-gray-4">
            <img
              className=" w-5 h-5  cursor-pointer"
              src="/svgs/share-line.png"
              onClick={() => handleShare()}
              alt="Share"
            />
          </div>
        </div>
      </div>

      <div className="  hidescroll ">
        {/* only desktop */}

        {/* M WEB NAV START */}
        <div className="w-full p-2 md:p-0 lg:p-0 flex flex-col-reverse md:flex-col lg:flex-col">
          <div className={`w-full flex justify-between `}>
            <div className="w-full md:w-1/3 lg:w-1/3 ">
              <Tab
                data={homeTabs}
                selectedTab={currTab}
                type="block"
                handleTabChange={(item) => {
                  setTab(item)
                  router.push(`/series/${item.value}/${currType.value}`)
                }}
              />
            </div>
            <div className=" w-1/3 hidden md:flex lg:flex h-full  justify-end gap-2  ">
              {!!(countryName.length || formatType.length) && (
                <span
                  className="w-24 text-xs font-bold cursor-pointer flex items-center justify-center gap-2  text-white bg-basered p-2 rounded-md bottom-0  "
                  onClick={() => {
                    setCountryName([])
                    setFormatType([])
                  }}
                >
                  CLEAR ALL
                </span>
              )}
              {!!(
                dataList?.length
              ) && (
                <div
                  onClick={() => setOpenFilter(true)}
                  className="w-28 cursor-pointer flex items-center justify-center gap-2  text-white bg-basered p-2 rounded-md bottom-0    "
                >
                  <img className="" src="/svgs/filter.svg" alt="user" />{' '}
                  <span className="text-xs  font-bold">FILTER</span>
                </div>
              )}
            </div>
          </div>
          <div className="w-full pb-3 md:py-3 lg:py-3 ">
            <Tab
              data={types}
              selectedTab={currType}
              type="line"
              handleTabChange={(item) => {
                setCurrType(item)
                router.push(getFilterTabUrlSeries(currTab.value, item.value).as)
              }}
            />
          </div>
        </div>
      </div>
            <SeriesLayout tab={currTab} formatType={formatType} countryName={countryName} currType={currType} setDataList={setDataList}/>
            <div className="flex fixed bottom-0  z-20 bg-basebg w-full justify-center py-2 gap-2 border-t md:hidden lg:hidden">
        {!!(dataList?.length) && (
          <div
            onClick={() => setOpenFilter(true)}
            className="w-32 h-10 cursor-pointer flex items-center justify-center gap-2 bg-gray-8 border-2 border-green py-2 px-3 rounded-md bottom-0    "
          >
            <img className="" src="/svgs/filter.svg" alt="user" />{' '}
            <span className="text-xs font-bold">FILTER</span>
          </div>
        )}
        {!!(countryName.length || formatType.length) && (
          <span
            className="w-32 h-10 cursor-pointer flex items-center justify-center bg-gray-8 border-2 border-green py-2 px-3 text-xs font-bold rounded-md bottom-0    "
            onClick={() => {
              setCountryName([])
              setFormatType([])
            }}
          >
            CLEAR ALL
          </span>
        )}
      </div>
            </>
        )}

        {context.params.slugs?.length !== 2 && (
            <>
                {(loading1 || loading2) ? <Loading /> : context.params.slugs?.length == 4 ? (
                    <>
                        {/* {console.log("data.matcheslistdata.matcheslistdata.matcheslist",data, ' --  ',props)} */}
                        {data.matcheslist?.length ? (
                            <div className='w-full dark:text-white'>
                                <div className='md:py-1 md:mb-3 lg:mb-3 lg:py-1 dark:sticky dark:top-0 dark:bg-basebg dark:z-10 ' >
                                    <div className='px-3 py-2 flex gap-2 items-center md:hidden lg:hidden '>
                                        <div className='p-2 bg-gray-4 rounded-md'>
                                            <ImageWithFallback
                                                height={18}
                                                width={18}
                                                loading='lazy'
                                                class="flex items-center justify-center h-4 w-4 rotate-180"
                                                onClick={() => window.history.back()}
                                                src='/svgsV2/RightSchevronWhite.svg'
                                                alt='back icon'
                                            />
                                        </div>
                                        <span className='text-base truncate text-center'>{data.matcheslist[0].seriesName}</span>
                                    </div>
                                    <div className='  flex justify-between items-center gap-2 dark:gap-0  dark:border-none rounded-full dark:rounded-none overflow-auto md:w-2/3 lg:w-2/3'>
                                        {
                                            tabs?.map((tabName, i) => {
                                               
                                                if (!data.matcheslist[0].isStatistics && tabName === 'STATS') {
                                                    return;
                                                }
                                                if (!data.matcheslist[0].isPoints && tabName === 'POINTS TABLE') {
                                                    return;
                                                }
                                                return (
                                                    (<Link
                                                        key={i}
                                                        {...getUrl(tabName, type, seriesSlug, seriesID)}
                                                        replace
                                                        passHref
                                                        className={`${tabName === 'HOME' ? 'hidden md:flex lg:flex' : ''} w-full flex justify-center items-center cursor-pointer font-semibold text-xs p-2 dark:text-sky-100 dark:border-b-2 border-b-sky-100 whitespace-nowrap rounded-full dark:rounded-none ${tab.toLowerCase() === tabName.replace(' ', '-').toLowerCase()
                                                            ? 'text-white bg-basered dark:bg-basebg dark:border-b-2 dark:text-sky-300 dark:border-sky-300 cursor-pointer '
                                                            : 'text-black bg-light_gray dark:bg-basebg dark:text-sky-100'
                                                            } `}>

                                                        <div

                                                            onClick={() => {
                                                                handleNavigation(tabName);
                                                                // navigate(`${tabName.toLowerCase().split(" ").join("")}`, { replace: true });
                                                            }}>
                                                            {tabName === 'NEWS' ? 'CONTENT' : tabName}
                                                        </div>

                                                    </Link>)
                                                );
                                            })}
                                    </div>
                                </div>


                                <Component
                                    seriesID={seriesID}
                                    seriesType={type}
                                    data={comp}
                                    slug={seriesSlug}
                                />
                            </div>
                        ) : (<DataNotFound />

                        )}
                    </>
                ) : context.params.slugs?.length == 6 ? (
                    <>
                        <SeriesBattingStats
                            seriesID={seriesID}
                            seriesType={type}
                            slug={seriesSlug}
                            type={context.params.slugs[5]}
                            format={context.params.slugs[4]}
                            data={comp}
                        />
                    </>
                ) : (
                    <>
                        <SeriesView
                            seriesID={seriesID}
                            seriesType={type}
                            slug={seriesSlug}
                            type={context.params.slugs[5]}
                            format={context.params.slugs[4]}
                            scoretype={context.params.slugs[6]}
                            data={comp}
                        />
                    </>
                )}
            </>
        )}
    </div>;
}





