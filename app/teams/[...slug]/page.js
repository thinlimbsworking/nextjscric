'use client'
import React, { useState, useEffect, useRef } from 'react'
import { useRouter } from 'next/navigation'
import Link from 'next/link'
import axios from 'axios'
import dynamic from 'next/dynamic'
import Layout from '../../components/teamdetaillayout'
import { TEAM_DETAILS_AXIOS, NEWS_BY_ID_TYPE_AXIOS } from '../../api/queries'

import { TEAMS } from '../../constant/MetaDescriptions'
import MetaDescriptor from '../../components/MetaDescriptor'
// import ErrorBoundary from '../../components/ErrorBoundary';
import Urltracking from '../../components/commom/urltracking'
import Loading from '../../components/loading'
import StatHubLayout from '../../components/shared/statHubLayout'

/**
 * @description individual team view
 * @route /teams/teamID/teamName
 * @param { String } teamID
 * @param { Array } articles
 */

const componentList = {
  stats: dynamic(() => import(`../../components/teamDetails/teamStatsTab`)),
  schedule: dynamic(() => import(`../../components/teamDetails/teamSchedule`)),
  form: dynamic(() => import(`../../components/teamDetails/teamForm`)),
  players: dynamic(() => import(`../../components/teamDetails/teamPlayers`)),
  news: dynamic(() => import(`../../components/teamDetails/teamNews`)),
}

async function getServerData(teamID, teamSlug, tab) {
  var conditionalData = {}

  switch (tab) {
    case 'stats':
      conditionalData = { data: [] }
      break
    case 'schedule':
      conditionalData = { data: [] }
      break
    case 'form':
      conditionalData = { data: [] }
      break
    case 'players':
      conditionalData = { data: [] }
      break
    case 'playerbio':
      conditionalData = { data: [] }
      break
    case 'news':
      conditionalData = await axios.post(process.env.API, {
        query: NEWS_BY_ID_TYPE_AXIOS,
        variables: {
          type: 'teams',
          Id: teamID,
        },
      })
      console.log('cd - - ', conditionalData)
      break
    default:
      conditionalData = { data: [] }
      break
  }

  const [{ data: conditionalResponse }] = await Promise.all([conditionalData])
  return {
    teamID: teamID,
    category: tab,
    teamName: teamSlug,
    teamTab: '',
    responseData:
      conditionalResponse && conditionalResponse.data
        ? conditionalResponse.data
        : [],
  }
}

export const Team = ({ params }) => {
  const router = useRouter()
  console.log(params, 'params')
  const [props, setProps] = useState({})

  const teamID = params?.slug?.[0]
  const teamSlug = params?.slug?.[1]
  const teamName = teamSlug
  const tab = params?.slug?.[2]
  const category = params?.slug?.[2]
  const Component = componentList[category]

  // const run = useRef(0)
  useEffect(() => {
    getServerData(teamID, teamSlug, tab).then((res) => setProps(res))
    // run.current++
  }, [])

  if (props.responseData)
    return (
      <StatHubLayout currIndex={1}>
        {props && props.urlTrack ? <Urltracking PageName="Teams" /> : null}

        <MetaDescriptor
        section={TEAMS({
          countryName: teamName?.replace('-', ' '),
          url: params.slug.join('/'),
        })}
      />
        <Layout
          category={category}
          teamName={teamName}
          teamID={teamID}
          teamTab={props.teamTab}
          params={params}
        >
          <Component
            teamID={teamID}
            teamName={teamName}
            data={props?.responseData}
          />
        </Layout>
      </StatHubLayout>
    )
}

export default Team
