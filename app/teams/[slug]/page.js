'use client'
import React, { useEffect, useRef, useState } from 'react'
import Link from 'next/link'
import axios from 'axios'
const searchIcon = '/svgs/teamSearch.svg'
const searchIconLarge = '/svgs/search.svg'
import TeamList from '../../components/teamDiscovery'
import Search from '../../components/search'
import { TeamDiscovery, TEAM_SEARCH } from '../../api/queries'
import { TEAM_TAB_VIEW, TEAM_VIEW } from '../../constant/Links'
import { TEAMS } from '../../constant/MetaDescriptions'
import MetaDescriptor from '../../components/MetaDescriptor'
import Urltracking from '../../components/commom/urltracking'
import Loading from '../../components/loading'
import StatHubLayout from '../../components/shared/statHubLayout'

async function getServerData(tab) {
  const statusTabs = {
    international: 'international',
    'other-league': 'otherLeagues',
  }

  let response = { data: {}, tab }
  let { data } = await axios.post(process.env.API, {
    query: TeamDiscovery,
  })

  data = data?.data?.teamDiscoveryV2[statusTabs[tab]]

  if (Array.isArray(data)) {
    response.data[tab] = data
  } else {
    response.data = data
  }
  return { data: response?.data, status: response?.tab, tab }
}

export default function Teams({ params }) {
  const [props, setProps] = useState()
  const [loading, setLoading] = useState(true)
  const tab = params?.slug
  // const status = params?.slug

  const statusTabs = [
    { tabName: 'international', type: 'international' },
    { tabName: 'Other Leagues', type: 'other-league' },
  ]

  // const run = useRef(0)
  useEffect(() => {
    getServerData(tab).then((res) => {
      setProps(res)
      setLoading(false)
    })
  }, [])
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [])

  const onSearchChange = async (value) => {
    try {
      if (value.length >= 3) {
        const data = await axios.post(process.env.API, {
          query: TEAM_SEARCH,
          variables: { name: value },
        })
        return data &&
          data.data &&
          data.data.data.teamSearch &&
          data.data.data.teamSearch.length > 0
          ? [...data.data.data.teamSearch]
          : []
      } else return null
    } catch (e) {}
  }

  const getTeamUrl = ({ name, teamID }) => {
    let teamName = name.split(' ').join('-').toLowerCase()
    let teamId = teamID
    return {
      as: eval(TEAM_VIEW.as),
      href: TEAM_VIEW.href,
    }
  }

  const getSearchResult = (team, index) => {
    return (
      <Link
        key={index}
        {...getTeamUrl({ name: team.name, teamID: team.teamID })}
        legacyBehavior
      >
        <div className="">
          <li className="text-left dark:text-white text-black pt-0.5 pb-4 px-3 font-normal text-xs border-b-2 border-b-solid dark:border-b-gray border-b-[#E2E2E2] dark:bg-gray-8 bg-white lg:text-sm md:text-sm cursor-pointer">
            {team.name}
          </li>

          <div className="divider"></div>
        </div>
      </Link>
    )
  }

  const getTabUrl = (tabName) => {
    return {
      as: eval(TEAM_TAB_VIEW.as),
      href: TEAM_TAB_VIEW.href,
    }
  }

  const styleDesktop = {
    backgroundImage: `url(${searchIconLarge})`,
    backgroundRepeat: 'no-repeat',
    textIndent: '25px',
    backgroundPosition: '5px',
  }

  const styleMobo = {
    backgroundImage: `url(${searchIcon})`,
    backgroundRepeat: 'no-repeat',
    textIndent: '25px',
    backgroundPosition: '5px',
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
  }
  if (loading) {
    return <Loading />
  }

  return (
    <StatHubLayout currIndex={1}>
      <div className="">
        {/* {props.urlTrack ? <Urltracking PageName="Teams" /> : null} */}
        <MetaDescriptor section={TEAMS()} />
        <div className="block lg:hidden md:hidden center">
          <div className="flex items-center p-2.5  white text-base font-semibold lg:hidden">
            <div
              onClick={() => window.history.back()}
              className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
            >
              <svg width="30" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#ffff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <h1 className="font-semibold text-lg">Teams</h1>
          </div>
          <div className="py-3 px-2 center lg:w-full md:w-full text-center">
            <Search
              placeHolder="Search Team"
              // onSubmitUrl={getTeamUrl}
              getSearchResult={getSearchResult}
              onSearchChange={onSearchChange}
              style={styleMobo}
              className="w-full lg:w-full md:w-full font-nomral text-xs lg:text-sm md:text-sm bg-gray-8 p-2 border-2 opacity-80 border-silver outline-0"
            />
          </div>
          <div className="flex justify-between my-2 rounded-full w-5/11 mx-10 bg-gray">
            {statusTabs.map(({ tabName, type }, i) => (
              <Link
                key={tabName}
                {...getTabUrl(type)}
                replace
                passHref
                className={`flex justify-center items-center oswald  white-50  px-2 py-1 cursor-pointer text-center flex-auto text-xs font-semibold ${
                  tab.toLowerCase() === type.toLowerCase()
                    ? 'z-1 rounded-full border-2 border-green-6 text-white '
                    : ''
                }`}
              >
                <div>
                  {' '}
                  {tabName === 'Live' ? 'LIVE' : tabName.toUpperCase()}
                </div>
                {tabName === 'Live' ? (
                  <div class="ml-2  h-4 w-4 border-xl"></div>
                ) : null}
              </Link>
            ))}
          </div>
        </div>
        <div>
          <div className="hidden lg:flex md:flex xl:flex pb-4 justify-between">
            <div className="py-4 pl-3 center items-center flex justify-center capitalize">
              <Search
                placeHolder="Search Your Team"
                getSearchResult={getSearchResult}
                onSearchChange={onSearchChange}
                style={styleDesktop}
                className="font-normal text-xs lg:text-sm md:text-sm p-2 border-2 opacity-80 text-black bg-white outline-0 w-[36rem]"
              />
            </div>
            <div className="w-full mt-5 mr-4">
              <div className="flex items-center justify-end gap-1">
                {statusTabs.map(({ tabName, type }, i) => (
                  <Link
                    key={i}
                    {...getTabUrl(type)}
                    passHref
                    className={`w-4/12 border py-1.5 px-1 rounded-full border-[#E2E2E2] border-solid cursor-pointer text-xs uppercase text-center  ${
                      tab.toLowerCase() === type.toLowerCase()
                        ? 'z-1 border-b bg-basered font-semibold text-white '
                        : 'font-medium bg-[#EEEEEE] text-black'
                    }`}
                  >
                    <span>{tabName}</span>
                  </Link>
                ))}
              </div>
            </div>
          </div>
          <TeamList teams={props?.data} status={tab} />
        </div>
      </div>
    </StatHubLayout>
  )
}
