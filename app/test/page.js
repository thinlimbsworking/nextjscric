'use client'
import { set } from 'date-fns'
import { usePathname, useRouter } from "next/navigation";

import React,{useEffect,useState} from 'react'


export default function Page() {


    const [counter,setCounter]=useState(0)
  const router = useRouter();



useEffect(() => {


  setTimeout(()=>{
 if (typeof window !== "undefined") {
       
 setCounter(counter+1)

        if(window?.document.visibilityState === "visible") {
            
          // console.log('came back to screen');
        //   window.location="/"
  let path=localStorage.getItem('lastpath')
          router.push(path||"/")
          // reloadScreen('on')
        } else {
  
          
  setTimeout(() => {
    


  window.location="/test"
  


    
  }, 60*1000)
  
         
         
        }
    
    }
  
},100)
  
  }, [counter])
  return ( <div>
       <title>
        Live Cricket Score, Match Schedule & Predictions, Latest News |
        Cricket.com'
      </title>
      <link
        rel="shortcut icon"
        href="https://images.cricket.com/icons/mainlogoico.ico"
      />
         <meta name="robots" content="noindex" />
    <div className='opacity-0'>
     
        
        Page {counter} </div>
        </div>
  )
}
