'use client'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { PREDICTION_LANDING_AXIOS } from '../../api/queries'
import Maintheme from '../../components/mainTheme'
import Themdata1 from '../../components/themdata1'
import Subtheme from '../../components/subTheme'

async function getData() {
  const res = await axios.post(process.env.API, {
    query: PREDICTION_LANDING_AXIOS,
    variables: { type: 'latest' },
  })
  return res.data.data.predictionHome
}
export default function Page({ params }) {
  // const [xdata, setxdata] = useState({});
  const [data, setData] = useState()

  useEffect(() => {
    getData()
      .then((res) => {
        setData(res)
      })
      .catch((err) => console.log('err- - ', err))
  }, [])
  return (
    <div>
      <Themdata1 />
    </div>
  )
}
