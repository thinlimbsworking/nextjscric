'use client'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Link from 'next/link'
import Image from 'next/image'
import Scores from '../components/commom/score'
import dynamic from 'next/dynamic'
import { getContestUrl } from '../api/services'
import { useRouter } from 'next/navigation'
import Heading from '../../components/commom/heading'
import Loading from '../../components/loading'

import Login from '../login/page'
import Tab from '../components/shared/Tab'
const coinIcon = '/pngs/coinIcon.png'
import { use } from 'react'
import {
  PREDICTION_LANDING_AXIOS,
  PREDICTION_PROFILE_AXIOS,
} from '../api/queries'

let tabData = [
  {
    title: 'FIRST',
    value: 'first',
  },
  {
    title: 'SECOND',
    value: 'second',
  },
  {
    title: 'THIRD',
    value: 'third',
  },
  {
    title: 'FOURTH',
    value: 'fourth',
  },
  {
    title: 'FIFTH',
    value: 'fifth',
  },
]


const leaderboardIcon = '/svgs/icons/leaderboardIcon.svg'
const backIcon = '/svgs/backIconWhite.svg'
const fantasyDp = '/svgs/mask_man.svg'

const Popup = dynamic(() => import('../components/playodds/popup'), {
  loading: () => <Loading />,
})
//   const Login = dynamic(() => import('../login/page'), {
//     loading: () => <Loading />,
//   })

async function getData() {
  const res = await axios.post(process.env.API, {
    query: PREDICTION_LANDING_AXIOS,
    variables: { type: 'latest' },
  })
  return res.data.data.predictionHome
}

async function getProfileData(token) {
  const res = await axios.post(process.env.API, {
    query: PREDICTION_PROFILE_AXIOS,
    variables: {
      token: token,
      year: '',
      month: '',
    },
  })

  return res.data.data
}

export default function Page() {
  let router = useRouter()
  const [ptopopup, setptopopup] = useState(false)
  const [profiledata, setProfileData] = useState()
  const [data, setData] = useState()
  const [token, setToken] = useState(null)
  const [showLogin, setShowLogin] = useState(false)

  const [navtoData, setNavtoData] = useState('')
  const [currId, setCurrId] = useState('')
  // let token = ''
  useEffect(() => {
    let t = localStorage.getItem('tokenData')
    setToken(t)
    console.log('t-- ', t)
    getProfileData(t)
      .then((res) => {
        setProfileData(res)
      })
      .catch((err) => console.log('err- - ', err))
    getData()
      .then((res) => {
        setData(res)
      })
      .catch((err) => console.log('err- - ', err))
  }, [])

  const loginHandle = (item) => {
    // alert("dd")
    console.log('matchhid = ', getContest(item))
    if (token) {
      router.push(getContest(item).as)
    } else {
      setCurrId(item)
      const patHLogin = `/play-the-odd/contest/${item}`
      setNavtoData(patHLogin)
      setShowLogin(true)
      //   setClicked(false);
    }
  }

  const getContest = (matchID, tabName) => {
    return getContestUrl(matchID, tabName)
  }
  if (showLogin)
    return (
      <Login
        navTo={navtoData}
        modalText={'Login / Register to Play the odds'}
        sucessPath={navtoData}
        name={false}
        setShowLogin={setShowLogin}
        TeamAID={''}
        TeamBID={''}
        onCLose={'/play-the-odd'}
        matchID={currId}
        ShowLoginSucess={true}
        entryRoute={'PTO'}
      />
    )
  // if (loading) return <Loading />
  return (
    <>
      <Popup showModal={ptopopup} setshowModal={() => setptopopup(!ptopopup)} />

      <div className={`w-full ${ptopopup ? 'opacity-10 fixed' : ''}`}>
        <div className="flex justify-between items-center w-full md:hidden lg:hidden">
          <div className="dark:bg-gray-8 flex gap-2 items-center p-2 rounded-md ">
            <div
              className="p-1  dark:bg-gray rounded-md"
              onClick={() => window.history.back()}
            >
              <svg width="30" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#ffff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <span className="text-base  font-bold">Play The Odds</span>
          </div>
          <button
            style={{ color: '#5F92AC' }}
            className="pr-2 underline"
            onClick={() => setptopopup(!ptopopup)}
          >
            How it works?
          </button>
        </div>
        <div className="w-full flex justify-around items-center p-3 my-2">
          <div
            className="flex w-42 items-center gap-2 bg-gray border-2 border-gray-3 p-2 rounded-md cursor-pointer"
            onClick={() => router.push(`/play-the-odd/leaderboard`)}
          >
            <Image
              height={22}
              width={22}
              src={leaderboardIcon}
              alt="leaderboard"
            />
            <span className="text-sm font-semibold ">Overall Leaderboard</span>
          </div>
          {token && (
            <div
              className="flex w-32 items-center gap-3 bg-gray border-2 border-gray-3 p-2 rounded-md cursor-pointer"
              onClick={() => router.push(`/play-the-odd/mycontest`)}
            >
              <Image
                height={22}
                width={22}
                src={fantasyDp}
                alt="leaderboard "
              />
              <div className="text-sm font-semibold  border-l border-l-gray-2 px-2 flex gap-2 items-center">
                <Image height={20} width={20} src={coinIcon} alt="coins" />
                <span className="text-green">
                  {profiledata?.predictionProfile?.totalEarnings}
                </span>
              </div>
            </div>
          )}
        </div>
        <div className="p-2">
          <Tab
            type="outline"
            data={tabData}
            selectedTab={{ title: 'FIRST', value: 'first' }}
            handleTabChange={(item) =>
              console.log('item coming in handleselect = ', item)
            }
          />
        </div>
        <div className="flex flex-col p-2 ">
          <Heading heading={`Featured Matches`} />
        </div>
        <div className="flex flex-wrap items-center justify-start my-4 pb-4 ">
          {data?.length ? (
            data.map((card) => (
              <div
                key={card.matchID}
                className="w-full md:w-1/2 lg:w-1/2 overflow-hidden  flex justify-center  items-center cursor-pointer "
              >
                {/* <Link className='w-full' {...getContest(card.matchID)} passHref > */}
                <div
                  className="-mb-4 w-full  cursor-pointer flex flex-col px-2"
                  onClick={() => loginHandle(card.matchID)}
                >
                  <div className=" w-full  cursor-pointer flex flex-col p-2">
                    {card && card.matchID ? (
                      <Scores featured={true} data={card} playTheOdds={true} />
                    ) : (
                      <></>
                    )}
                  </div>

                  {/* </Link> */}
                </div>
              </div>
            ))
          ) : (
            <></>
          )}
        </div>
      </div>
    </>
  )

  {
    /* {name&& name.data.predictionHome.map((item,index)=>{return(<div>{index} <Scores  featured={true} data={item} playTheOdds={true} /> {console.log("ppppp",item)}</div>)})} */
  }
}
