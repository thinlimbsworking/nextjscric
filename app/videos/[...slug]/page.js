'use client'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import dynamic from 'next/dynamic'
import CleverTap from 'clevertap-react'
import {
  GET_VIDEO_BY_ID_AXIOS,
  GET_VIDEOS_AXIOS,
  GET_PRIMARY_VIDEOS_AXIOS,
  SERIES_NAME_FOR_ARTICLES_AXIOS,
  GET_VIDEOS_BY_CATEGORIES_AXIOS
} from '../../api/queries'
import Loading from '../../components/loading'
import MetaDescriptor from '../../components/MetaDescriptor'
import { VIDEOS } from '../../constant/MetaDescriptions'
import Urltracking from '../../components/commom/urltracking'
import Heading from '../../../components/commom/heading'
import ExclusiveTabs from '../../components/shared/exclusiveTabs'

const VideosHomepage = dynamic(
  () => import('../../components/videos/videosHomePage'),
  {
    loading: () => <Loading />,
  },
)

const VideosDetails = dynamic(
  () => import('../../components/videos/videosDetailPage'),
  {
    loading: () => <Loading />,
  },
)

const dynamicImport = (isHomepage) => {
  if (isHomepage) {
    return VideosHomepage
  } else {
    return VideosDetails
  }
}

/**
 * @description individual view view section
 * @route /video/any-video-id/....
 * @param { Object } error
 * @param { Bool } isHomePage
 * @param { Object } primaryVideo
 * @param { isHomePage } Bool
 */
async function getDDList() {
  const { data } = await axios.post(process.env.API, {
    query: SERIES_NAME_FOR_ARTICLES_AXIOS
  })
  return data?.data.getSeriesListForArticles.seriesListWithIds;
}
async function getData(params, searchParams, tab) {
  const type = params.slug;
  const primaryVideoPromise = axios.post(process.env.API, {
    query: GET_PRIMARY_VIDEOS_AXIOS,
  })
  if (searchParams && searchParams.seriesID && searchParams.seriesID !== 'all') {
    const categoryVideos = axios.post(process.env.API, {
      query: GET_VIDEOS_BY_CATEGORIES_AXIOS,
      variables: { type: tab, page: 0, seriesID: searchParams.seriesID }
    })
    const [{ data }, { data: primaryVideo }] = await Promise.all([
      categoryVideos,
      primaryVideoPromise,
    ])
    return {
      data: data.data,
      primaryVideo: primaryVideo.data.getPrimaryVideo,
    }

  }
  const spliter = parseInt(type[0].split('-').slice(-1)[0])
  if (Number.isNaN(spliter)) {
    const getVideoPromise = axios.post(process.env.API, {
      query: GET_VIDEOS_AXIOS,
    })

    const [{ data: data }, { data: primaryVideo }] = await Promise.all([
      getVideoPromise,
      primaryVideoPromise,
    ])
    return {
      data: data.data,
      primaryVideo: primaryVideo.data.getPrimaryVideo,
      type,
    }
  } else {
    const [videoID] = params.slug
    const { data } = await axios.post(process.env.API, {
      query: GET_VIDEO_BY_ID_AXIOS,
      variables: { videoID: videoID },
    })
    return { data: data.data, videoID }
  }
}

const VideosSection = ({ params, searchParams }) => {
  const [type] = params.slug
  const spliter = parseInt(type.split('-').slice(-1)[0])
  const isHomePage = Number.isNaN(spliter) ? true : false
  const [loading, setLoading] = useState(false)
  const [seriesFilter, setSeriesFilter] = useState([]);
  const [selectedDD, setSelectedDD] = useState()
  const [props, setProps] = useState({})
  const { data } = { data: props.data }
  const [drop, setDrop] = useState(false)

  const tabs = [
    { name: 'Latest', value: 'Latest' },
    { name: 'Fantasy', value: 'Fantasy' },
    { name: 'Match', value: 'MatchRelated' },
    { name: 'News', value: 'News' },
  ]
  let temp =
    type === 'matchrelated'
      ? 'MatchRelated'
      : type.replace(/\b\w/g, (l) => l.toUpperCase())
  const tab = tabs.filter((ele) => ele.value === temp)[0]

  let Component = dynamicImport(isHomePage)

  useEffect(() => {
    getDDList().then((res) => {
      setSeriesFilter([{ valueLable: 'All', valueId: 'all' }, ...res.filter(ele => ele.videosFlag).map(ele => ({ valueLable: ele.tourName, valueId: ele.tourID }))])
    })
  }, [])

  useEffect(() => {
    setSelectedDD(seriesFilter.find(ele => ele.valueId === searchParams.seriesID) || seriesFilter[0])
  }, [seriesFilter, params])

  useEffect(() => {
    setLoading(true)

    getData(params, searchParams, tab?.value).then((res) => {
      setLoading(false)
      setProps(res)
    })

  }, [searchParams])



  if (typeof window !== 'undefined') {
    window.scrollTo(0, 0)
  }

  const handleVideoNavigation = (type, videoID) => {
    CleverTap.initialize('Videos', {
      Source: 'VideosHome',
      VideoID: videoID,
      Platform:
        global && global.window && global.window.localStorage
          ? global.window.localStorage.Platform
          : '',
    })
  }



  const handleShare = (e) => {
    if (window.navigator.share !== undefined) {
      window.navigator
        .share({
          title: props.videoTitle,
          url: props.location.href,
        })
        .then((res) => {
          CleverTap.initialize('Share', {
            Source: 'Fantasy',
            Platform: global.window.localStorage
              ? global.window.localStorage.Platform
              : '',
          })
        })
        .catch((error) => console.log(error))
    }
  }
  // if (loading) return <Loading />

  if (props.error)
    return (
      <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center">
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    )
  else
    return (
      <>
        {tab && <MetaDescriptor section={VIDEOS(tab.value, 'main')} />}
        {props.urlTrack ? <Urltracking PageName="Videos" /> : null}
        <div className=" hidden md:flex lg:flex   z-1 px-2 py-2 items-center  dark:text-white mb-3 ">

        </div>
        <div className='w-full flex'>
          <div className={`w-full ${isHomePage ? 'md:w-9/12 lg:w-9/12 md:pr-6 lg:pr-6' : ''}`}>
            {
              loading ? <Loading />
                : (!searchParams?.seriesID || selectedDD) &&
                <Component
                  tabs={tabs}
                  seriesFilter={seriesFilter}
                  handleShare={handleShare}
                  tab={tab}
                  VIDEOS={VIDEOS}
                  drop={drop}
                  selectedDD={selectedDD}
                  setDrop={setDrop}
                  data={props.data}
                  getPrimaryVideo={props.primaryVideo}
                  handleVideoNavigation={handleVideoNavigation}
                />
            }
          </div>
          {isHomePage && <div className='hidden md:block lg:block w-3/12 '>

            <ExclusiveTabs />

          </div>}
        </div>


      </>
    )

}

export default VideosSection
