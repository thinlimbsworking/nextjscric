import Head from 'next/head';

const MetaDescriptor = ({ section, children, ...props }) => {
{console.log("sectionsection",section)}
 
  return (
    <Head>
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      <title>{section && section.title ? section.title : 'Cricket.com'}</title>
      {/* <h1 className='dn'>{section && section.h1 ? section.h1 : 'Cricket.com'}</h1> */}
      <meta name='twitter:title' content={section && section.title ? section.title : 'Cricket.com'} />
      <meta property='og:title' content={section && section.title ? section.title : 'Cricket.com'} />
      <meta name='description' content={section && section.description ? section.description : 'Cricket.com'} />
      <meta property='og:description' content={section && section.description ? section.description : 'Cricket.com'} />
      <meta property='og:url' content={section && section.url ? section.url : 'https://www.cricket.com'} />
      <meta name='twitter:description' content={section && section.description ? section.description : 'Cricket.com'} />
      <meta name='twitter:url' content={section && section.url ? section.url : 'https://www.cricket.com'} />
      <meta name='og:image' content={section && section.img ? section.img : ''} />
      {children}
      <link rel='canonical' href={section && section.url ? section.url : 'https://www.cricket.com'} />
    </Head>
  );
};

export default MetaDescriptor;
