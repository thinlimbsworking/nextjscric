import React from 'react'

export default function Heading(props) {
  return (
    <div className=''>
        
        <div className=' font-semibold text-base tracking-wide capitalize '>{props.heading} </div>
       {props.subHeading && <div className='  text-xs  tracking-wide  '>{props.subHeading} </div>}


        <div className='bg-blue-8 h-1 rounded-md mt-1 w-16'></div>
        </div>
  )
}
