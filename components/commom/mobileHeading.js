import React from 'react'

export default function MobileHeader(props) {
  return (
      <div className='flex items-center justify-start p-2 lg:hidden md:hidden fixed bg-gray-8 w-full z-50'>
    <div className='p-3 bg-gray rounded-md' onClick={props.onClick?props.onClick:() => window.history.back()}>
      <img
        className=' flex items-center justify-center h-4 w-4 rotate-180'
        src='/svgsV2/RightSchevronWhite.svg'
        alt=''
      />
    </div>

    <div className='flex justify-center items-center text-lg font-semibold text-white pl-3'>
      <div className='text-sm'>{props.title}</div>
    
    </div>
  </div>
  )
}
