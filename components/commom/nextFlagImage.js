import React, { useState } from 'react'
import Image from 'next/image'

export default function NextImageFlag(props) {
  
const darkFlag='/pngsV2/flag_dark.png'

const [imgSrc,setImgSrc]=useState(props.src)

  // const [error,setError]=useState()

  return (<Image 
 className={props.className}
    height={props.height} width={props.width} src={imgSrc}
    onError={(evt) =>setImgSrc(darkFlag)}
    /> 
  )
}