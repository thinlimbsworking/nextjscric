import React from 'react';

const Polling = ({ pollData }) => {
  return (
    <div className='mt1  w-full'>
      <div className='flex flex-row justify-between f7 fw6 w-full'>
        <div className={`${'text-green'}`}>{pollData.teamsWinProbability.homeTeamPercentage}%</div>
        {pollData.teamsWinProbability.tiePercentage && parseInt(pollData.teamsWinProbability.tiePercentage) > 0 ? (
          <div className=' text-gray-3'>{pollData.teamsWinProbability.tiePercentage}%</div>
        ) : (
          <div />
        )}
        <div className={`${'text-white'}`}>{pollData.teamsWinProbability.awayTeamPercentage}%</div>
      </div>
      <div className='flex pv1 justify-center items-center w-full'>
        <div
          style={{ width: `${pollData.teamsWinProbability.homeTeamPercentage}%`, height: 4 }}
          className={` dib br2 br--left ${'bg-green'}`}></div>
        {pollData.teamsWinProbability.tiePercentage && parseInt(pollData.teamsWinProbability.tiePercentage) > 0 ? (
          <div
            className=' dib bg-gray-3'
            style={{ width: `${pollData.teamsWinProbability.tiePercentage}%`, height: 4 }}
          />
        ) : (
          <div />
        )}
        <div
          style={{ width: `${pollData.teamsWinProbability.awayTeamPercentage}%`, height: 4 }}
          className={`dib ${'bg-white'} dib br2 br--right`}></div>
      </div>
      <div className='flex flex-row justify-between w-full f7 text-white/80 mt-1'>
        <div className='fw6 f7 '>{pollData.teamsWinProbability.homeTeamShortName}</div>
        {pollData.teamsWinProbability.tiePercentage && parseInt(pollData.teamsWinProbability.tiePercentage) > 0 ? (
          <div className='fw6 f7 '>{pollData.matchType === 'Test' ? 'Draw' : 'Tie'}</div>
        ) : (
          <div />
        )}
        <div className='fw6 f7 '>{pollData.teamsWinProbability.awayTeamShortName}</div>
      </div>
    </div>
  );
};

export default Polling;
