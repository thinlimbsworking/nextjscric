import React from "react";
const PostSuccessModal = ({Msg}) =>{
  return (
    <div className="border-none outline-0 outline-none w-full h-full flex flex-col justify-center items-center gap-2 focus:outline-none text-center">
      <img src="/gif/success.gif" className="h-80  " />
      <span className="text-white text-xl ">{Msg}</span>
    </div>
  )
}
export default PostSuccessModal
