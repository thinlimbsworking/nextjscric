import CleverTap from "clevertap-react";
import { format } from "date-fns";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Countdown from "react-countdown-now";
import Login from "../../pages/login/index";
// import { useErrorHandler } from '../../components/ErrorBoundary';
import {
  getCriclyticsScore, getFrcSeriesTabUrl
} from "../../api/services";
import { PLAYER_VIEW } from "../../constant/Links";
import Predictionbar from "../playodds/predictionDetails";
const AbandonedIcon = "/svgs/images/Abandoned_Icon.png";
const flagPlaceHolder = "/pngsV2/flag_dark.png";
const ManOfMatch = "/pngsV2/MOM2.png";
const crycLyticsLogo = "/pngsV2/CriclyticsIcon.png";
const frc = "/pngsV2/FantasyStatsIcon.png";
const location = "/pngsV2/location.png";

export default function Scores({
  data,
  hideFantasyCriTab,
  isPredictedMatch,
  ...props
}) {
  const { featured, showCriclytics, isDetails } = props;
  const [showlogin, setShowLogin] = useState(false);
  const [loginPath, setLoginPath] = useState("");
    // const handleError = useErrorHandler();
    // if (!data) {
    //   handleError('Data not found');
    // }
  // const { data: predictionMatchData } = useQuery(PREDICTION_PROFILE_MATCH, {
  //   variables: {
  //     matchID: data?.matchID,
  //     token:
  //       "BEARER eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InBobnVtYmVyIjoiIiwiZW1haWwiOiJpdHNwcmVtMjE5M0BnbWFpbC5jb20iLCJfaWQiOiI2MzZhMmRkOGVmYzI4Yjg2ZmFjNGEwODYiLCJ1c2VySUQiOiI2MzZhMmRkOGVmYzI4Yjg2ZmFjNGEwODYifSwiaWF0IjoxNjY4MDU5Nzk5LCJleHAiOjE2OTk1OTU3OTl9.ymKIX6r9e0CB8zMRRHBg-kcfbm4fC12MgpBXKAkQKC4",
  //   },
  // });
  console.log(data,'Indians')

  let router = useRouter();
  let navigate = router.push;
  const handleCriclyticsNav = () => {
    CleverTap.initialize("Criclytics", {
      Source: "MatchScorecard",
      MatchID: data.matchID,
      SeriesID: data.seriesID,
      TeamAID: data.matchScore[0].teamID,
      TeamBID: data.matchScore[1].teamID,
      MatchFormat: data.matchType,
      MatchStartTime: format(Number(data.startDate), "d,y, h:mm a"),
      MatchStatus: data.matchStatus,
      CriclyticsEngine:
        data.matchStatus === "live"
          ? "LiveTeam"
          : data.matchStatus === "completed" && data.matchType !== "Test"
          ? "CompletedMS"
          : data.matchStatus === "completed" && data.matchType === "Test"
          ? "CompletedMR"
          : data.matchStatus === "upcoming"
          ? "ScheduledStats"
          : "",
      Platform: localStorage ? localStorage.Platform : "",
    });
  };
  const criclyticsUrl = () => {
    let tab =
      data.matchStatus === "upcoming" ||
      (data.matchStatus === "live" && !data.isLiveCriclyticsAvailable)
        ? "key-stats"
        : data.matchStatus === "live" && data.isLiveCriclyticsAvailable === true
        ? "team-score-projection"
        : data.matchStatus === "completed" && data.matchType !== "Test"
        ? "match-stats"
        : data.matchStatus === "completed" && data.matchType === "Test"
        ? "match-reel"
        : "key-stats";
    return getCriclyticsScore(data, tab);
  };
  const handleFantasyNav = () => {
    CleverTap.initialize("Fantasy", {
      Source: "MatchScorecard",
      MatchID: data.matchID,
      SeriesID: data.seriesID,
      TeamAID: data.matchScore[0].teamID,
      TeamBID: data.matchScore[1].teamID,
      MatchFormat: data.matchType,
      MatchStatus:
        data.matchStatus === "live"
          ? "Live"
          : data.matchStatus === "upcoming"
          ? "Scheduled"
          : data.matchStatus === "completed"
          ? "Completed"
          : "",
      Platform: localStorage.Platform,
    });
  };
  const getUrl = (player) => {
    let playerSlug = `${player.name.split(" ").join("-").toLowerCase()}`;
    let playerId = player.id;
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href,
    };
  };
  const handlePlayerNav = (playerName, playerID) => {
    CleverTap.initialize("Players", {
      Source: "MatchScorecard",
      PlayerID: playerID,
      Platform: localStorage.Platform,
    });
    let { as, href } = getUrl({ name: playerName, id: playerID });
    navigate(href, as);
  };
  const redirectToFile = (match, isLiveCriclyticsAvailable) => {
    let matchProjection = {};
    let tab = "";
    matchProjection.matchID = match.matchID;
    let matchName = `${match.homeTeamName}-vs-${match.awayTeamName}-${match.matchNumber}-${match.seriesName}`;
    matchProjection.seriesSlug = matchName
      .replace(/[^a-zA-Z0-9]+/g, " ")
      .split(" ")
      .join("-")
      .toLowerCase();
    return getFrcSeriesTabUrl(matchProjection, tab);
  };
  const handleNav = (data, status) => {
    const { href, as } = redirectToFile(data);

    if (data && data.loginEnable) {
      if (localStorage.getItem("tokenData") !== null) {
        navigate(href, as);
      } else {
        setLoginPath(as);
        setShowLogin(true);
      }
    } else {
      navigate(href, as);
    }
  };
  return (
    <>
      {showlogin && (
        <Login
          navTo={loginPath}
          modalText={"Login / Register to access Fantasy Research Center"}
          sucessPath={loginPath}
          name={false}
          onCLose={"/match-score"}
          setShowLogin={setShowLogin}
          TeamAID={data ? data.matchScore[0].teamID : ""}
          TeamBID={data ? data.matchScore[1].teamID : ""}
          matchID={data ? data.matchID : ""}
          ShowLoginSucess={true}
          entryRoute={"Scorecard"}
        />
      )}
      {data.matchStatus == "completed" && (
        <div
          className="bg-gray rounded-lg p-3   w-full"
          style={{
            height: props.playTheOdds || props.from === "frc" ? 275 : 320,
          }}
        >
          <div className="font-semibold  text-md tracking-wide text-white">
            {data.seriesName || data.matchName}
          </div>
          <div className="flex flex-col justify-between gap-2 mt-2 ">
            <div className="flex items-center gap-2">
              <div className="text-xs text-gray-2 font-semibold  uppercase">
                {data.matchNumber}
              </div>
              {data.matchResult === "Match Cancelled" || data.isAbandoned ? (
                <div className=" text-red-5 text-xs font-semibold rounded-full bg-red-5/30 border-red-5 border-2 p-1 px-2 flex  uppercase  justify-center items-center">
                  <div className="pr-1 text-red-5 text-xs"> x</div>{" "}
                  {data.isAbandoned ? "ABANDONED" : "Cancelled"}
                </div>
              ) : (
                <div className=" text-gray-7 text-xs font-semibold rounded-full border-gray-7 border-2 px-2 py-0.5 flex  uppercase  justify-center items-center bg-gray-7/20">
                  <div className="w-1.5 h-1.5 bg-white rounded-full mr-1  "></div>{" "}
                  {data.matchStatus}{" "}
                </div>
              )}
            </div>
            <div className="flex items-center gap-1">
              <img src={location} className="w-3" />
              <div className="text-xs text-gray-2 font-semibold pl-1">
                {data.venue || data.city}
              </div>
            </div>
            {/* <div className=''>
                      <img src={Bell} alt='' />
                    </div> */}
          </div>

          <div className="mt-3 bg-gray-4 flex flex-col justify-center gap-2 rounded p-3 ">
            <div className="flex justify-between items-center ">
              <div className="flex items-center  text-white text-md font-semibold">
                <img
                  className="w-10 h-6  rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                />{" "}
                {data.matchScore[0].teamShortName}
              </div>
              <div
                className={`justify-center items-center flex border-2 font-semibold ${
                  data.matchResult === "Match Cancelled" || data.isAbandoned
                    ? "border-gray-3 text-gray-3"
                    : "border-green text-green"
                }  rounded-full  uppercase text-xs px-3   py-0.5 `}
              >
                {data.matchType}
              </div>
              <div>
                <div className="flex items-center  text-white text-md font-semibold">
                  {" "}
                  {data.matchScore[1].teamShortName}
                  <img
                    className="w-10 h-6  rounded-sm ml-2"
                    src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = "/pngsV2/flag_dark.png")
                    }
                  />{" "}
                </div>
              </div>
            </div>
            <div className="flex justify-between items-center ">
              <div className="flex flex-col justify-center">
                {data.matchScore &&
                  data.matchScore[0] &&
                  data.matchScore[0].teamScore &&
                  data.matchScore[0].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      <p
                        className={`${
                          data.winningTeamID === team.teamID
                            ? "text-green"
                            : "text-white"
                        } text-sm font-semibold `}
                      >
                        {team.runsScored}/{team.wickets}
                      </p>
                      {team.overs && (
                        <p className="text-xs  font-medium  text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
              <div className="flex flex-col justify-center">
                {data.matchScore &&
                  data.matchScore[1] &&
                  data.matchScore[1].teamScore &&
                  data.matchScore[1].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      {team.runsScored && (
                        <p
                          className={`${
                            data.winningTeamID === team.teamID
                              ? "text-green"
                              : "text-white"
                          } text-sm font-semibold `}
                        >
                          {team.runsScored}/{team.wickets || 0}
                        </p>
                      )}
                      {team.overs && (
                        <p className="text-xs  font-medium text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
            </div>
          </div>

          {data.matchResult === "Match Cancelled" || data.isAbandoned ? (
            <div className=" bg-gray-4 text-center my-2 rounded py-2  text-xs font-semibold ">
              {data.statusMessage}
            </div>
          ) : (
            <div className=" bg-gray-4 text-center my-2 rounded py-2 text-green text-xs font-semibold ">
              {data.matchResult || data.statusMessage}
            </div>
          )}

          {props?.from !== "frc" &&
            (!isPredictedMatch ? (
              <div className="flex justify-start items-center gap-3">
                {data.matchResult !== "Match Cancelled" && !data.isAbandoned ? (
                  <div class="flex relative  items-center ">
                    {data && console.log("----", data)}
                    <img
                      class="h-14 w-14 bg-gray-4 object-cover object-top rounded-full p-1"
                      src={`https://images.cricket.com/players/${
                        data && data.playerID
                      }_headshot_safari.png`}
                      onError={(evt) => (evt.target.src = "/pngsV2/MOM2.png")}
                    />

                    <img
                      class=" absolute h-5 w-5   right-0 bottom-0 object-fill rounded border-2"
                      src={`https://images.cricket.com/teams/${
                        data && data.winningTeamID
                      }_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = "/pngsV2/flag_dark.png")
                      }
                    />
                  </div>
                ) : (
                  <img
                    className="h-16 w-16 bg-gray-4 rounded-full p-2"
                    src={ManOfMatch}
                  />
                )}
                {data.matchResult == "Match Cancelled" ? (
                  <div className="text-white text-sm font-semibold ">
                    Match Cancelled
                  </div>
                ) : data.isAbandoned ? (
                  <div className="text-gray-2 text-sm font-semibold">
                    Match Abandoned
                  </div>
                ) : (
                  <div className="flex flex-col">
                    <div className="text-white text-sm font-semibold ">
                      {(data.playerOfTheMatchdDetails &&
                        data.playerOfTheMatchdDetails.playerName) ||
                        "TBA"}
                    </div>
                    <div className=" capitalize text-gray-2 text-xs font-medium">
                      player Of The Match
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <div className="flex flex-col justify-between -mt-2">
                {!props.playTheOdds && (
                  <Predictionbar
                    matchID={data?.matchID}
                    

                  />
                )}
              </div>
            
            ))}
        </div>
      )}

      {/* ----------------------completed match block ends-------------------- */}

      {/*------------------------------ upcoming card block starts--------------------------------------------- */}

      {/* {console.log("datadatdata",item)} */}
      {(data.matchStatus === "upcoming" ||
        (data.matchStatus == "live" && !data.isLiveCriclyticsAvailable)) && (
        <div
          className=" rounded-lg p-3 bg-gray  w-full"
          style={{
            height: props.playTheOdds || props.from === "frc" ? 275 : 320,
          }}
        >
          {props.hideSeriesName !== true && (
            <div className="font-semibold  text-md tracking-wide text-white ">
              {data.seriesName || data.matchName}
            </div>
          )}
          <div className="flex flex-col justify-between gap-2 mt-2">
            <div className="flex items-center gap-2">
              <div className="text-xs text-gray-2 font-semibold  uppercase">
                {data.matchNumber}
              </div>
              <div
                className={` font-semibold text-xs  rounded-full ${
                  data.matchStatus == "live" && !data.isLiveCriclyticsAvailable
                    ? "border-red-5 text-white bg-red-5 "
                    : " text-blue-9 border-blue-9 bg-blue-9/20"
                }  border-2 px-2 py-0.5 flex  uppercase  justify-center items-center`}
              >
                <div
                  className={`w-1.5 h-1.5 ${
                    data.matchStatus == "live" &&
                    !data.isLiveCriclyticsAvailable
                      ? "bg-white"
                      : "  bg-blue-9"
                  }  rounded-full mr-1  `}
                ></div>{" "}
                {data.matchStatus}{" "}
              </div>
            </div>
            <div className="flex items-center gap-1">
              <img src={location} className="w-3" />
              <div className="text-xs text-gray-2 font-semibold pl-1">
                {data.venue || data.city}
              </div>
            </div>
            {/* <div className=''>
                      <img src={Bell} alt='' />
                    </div> */}
          </div>
          <div className="mt-3 bg-gray-4 flex flex-col justify-center gap-2 rounded p-3 ">
            <div className="flex justify-between items-center ">
              <div className="flex items-center  text-white text-md font-semibold">
                <img
                  className="w-10 h-6 rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                />
                {data.matchScore[0].teamShortName}
              </div>

              <div className="justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 ">
                {data.matchType}
              </div>
              <div className="flex items-center  text-white text-md font-semibold">
                {data.matchScore[1].teamShortName}
                <img
                  className="w-10 h-6  rounded-sm ml-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                />
              </div>
            </div>

            <div className="flex justify-between items-center ">
              <div className="flex flex-col justify-center">
                {data.matchScore &&
                  data.matchScore[0] &&
                  data.matchScore[0].teamScore &&
                  data.matchScore[0].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      <p
                        className={`${
                          data.winningTeamID === team.teamID
                            ? "text-green"
                            : "text-white"
                        } text-sm font-semibold `}
                      >
                        {team.runsScored}/{team.wickets}
                      </p>
                      {team.overs && (
                        <p className="text-xs  font-medium  text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
              <div className="flex flex-col justify-center">
                {data.matchScore &&
                  data.matchScore[1] &&
                  data.matchScore[1].teamScore &&
                  data.matchScore[1].teamScore.map((team, id) => (
                    <div key={id} className="flex items-center gap-1">
                      {team.runsScored && (
                        <p
                          className={`${
                            data.winningTeamID === team.teamID
                              ? "text-green"
                              : "text-white"
                          } text-sm font-semibold `}
                        >
                          {team.runsScored}/{team.wickets || 0}
                        </p>
                      )}
                      {team.overs && (
                        <p className="text-xs  font-medium text-white">
                          ({team.overs})
                        </p>
                      )}
                      {team.declared && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          d
                        </p>
                      )}
                      {team.folowOn && (
                        <p className="text-sm text-green  pl-1 font-semibold">
                          f
                        </p>
                      )}
                    </div>
                  ))}
              </div>
            </div>
          </div>
          <div className="flex bg-gray-4 justify-center items-center my-3 rounded py-2 text-white">
            {data.toss ? (
              <p className="text-sm font-medium text-center ">{data.toss}</p>
            ) : (
              <div className="flex items-center">
                <Countdown
                  date={new Date(Number(data.startDate) - 19800000 - 1800000)}
                  renderer={(props) => (
                    <span className="text-sm font-semibold">
                      {`${
                        props.days > 0
                          ? format(Number(data.startDate), "d LLLL, hh:mm a ")
                          : Number(props.hours || 0) +
                            " hrs " +
                            props.minutes +
                            " mins to toss"
                      }`}
                    </span>
                  )}
                />
              </div>
            )}
          </div>

          {props?.from !== "frc" &&
            !props.playTheOdds &&
            (data.teamsWinProbability && !isPredictedMatch ? (
              <div className="">
                <div className="text-gray-2 text-xs my-2 font-medium">
                  WIN PERCENTAGE
                </div>
                <div className=" flex bg-gray-3 rounded-full  h-3 overflow-hidden ">
                  <div
                    className={`${"bg-green"}  h-3 rounded-l-lg`}
                    style={{
                      width: data.teamsWinProbability.homeTeamPercentage + "%",
                    }}
                  />
                  <div
                    className={` bg-gray-3 h-3   `}
                    style={{
                      width: data.teamsWinProbability.tiePercentage + "%",
                    }}
                  />
                  <div
                    className={`${"bg-white"}  h-3 rounded-r-lg  `}
                    style={{
                      width: data.teamsWinProbability.awayTeamPercentage + "%",
                    }}
                  />
                </div>
              </div>
            ) : (
              !props.playTheOdds &&
               (
                <Predictionbar
                  matchID={data?.matchID}
                  

                />
              )
            ))}

          {/* {!props.playTheOdds && data.teamsWinProbability && (
            <div className="flex mt-3 justify-between items-center text-gray-2">
              <div className=" flex items-center text-xs font-medium">
                <p className={`w-2 h-2 ${"bg-green"} rounded-full m-1  `}></p>
                {data.teamsWinProbability &&
                  data.teamsWinProbability.homeTeamShortName}{" "}
                (
                {data.teamsWinProbability &&
                  data.teamsWinProbability.homeTeamPercentage}
                %)
              </div>
              {data.teamsWinProbability &&
                data.teamsWinProbability.tiePercentage && (
                  <div className="flex items-center text-xs font-medium">
                    <p className="w-2 h-2 bg-gray-3 rounded-full m-1  "></p>
                    {data.matchType != "Test" ? "Tie" : "DRAW"} (
                    {(data.teamsWinProbability &&
                      data.teamsWinProbability.tiePercentage) ||
                      0}
                    %)
                  </div>
                )}
              <div className="flex items-center text-xs font-medium">
                <p className={`w-2 h-2 ${"bg-white"} rounded-full m-1  `}></p>
                {data.teamsWinProbability &&
                  data.teamsWinProbability.awayTeamShortName}{" "}
                (
                {data.teamsWinProbability &&
                  data.teamsWinProbability.awayTeamPercentage}
                %)
              </div>
            </div>
          )} */}

          {props.playTheOdds && (
            <div className="text-xs text-white p-2 ">
              {(data.totalParticipants || 0) + " participants"}
            </div>
          )}
        </div>
      )}

      {/* -------------------------------------------end of upcoming match card------------------------------------ */}

      {/* --------------------------------------live match card----------------------------------- */}

      {data.matchStatus == "live" && data.isLiveCriclyticsAvailable && (
        <div
          className="bg-gray rounded-lg p-3 w-full"
          style={{
            height: props.playTheOdds || props.from === "frc" ? 275 : 320,
          }}
        >
          <div className="font-semibold  text-md tracking-wide text-white">
            {data.seriesName || data.matchName}
          </div>
          <div className="flex flex-col justify-between gap-2 mt-2 ">
            <div className="flex items-center">
              <div className="text-xs text-gray-2 font-semibold  uppercase">
                {data.matchNumber}
              </div>
              <div className=" font-semibold text-xs  rounded-full bg-red-5 border-red-5 border-2 px-1 py-0.5 flex  uppercase text-white justify-center items-center">
                <div className="w-1.5 h-1.5  bg-white rounded-full mr-1 "></div>{" "}
                {data.matchStatus}{" "}
              </div>
            </div>
            <div className="flex items-center">
              <img src={location} className="w-3" />
              <div className="text-xs text-gray-2 font-semibold pl-1">
                {data.venue || data.city}
              </div>
            </div>
            {/* <div className=''>
                      <img src={Bell} alt='' />
                    </div> */}
          </div>
          <div className=" bg-gray-4  my-3 rounded p-3 ">
            <div className="flex justify-between items-center">
              <div className="">
                <div className="flex items-center  text-white text-md font-semibold">
                  <img
                    className="w-10 h-6 rounded-sm mr-2"
                    src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = "/pngsV2/flag_dark.png")
                    }
                  />{" "}
                  {data.matchScore[0].teamShortName}
                </div>
              </div>
              <div className="justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 ">
                {data.matchType}
              </div>
              <div>
                <div className="flex items-center  text-white text-md font-semibold">
                  {" "}
                  {data.matchScore[1].teamShortName}
                  <img
                    className="w-10 h-6  rounded-sm ml-2"
                    src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = "/pngsV2/flag_dark.png")
                    }
                  />{" "}
                </div>
              </div>
            </div>

            {data.matchScore &&
              data.matchScore[0] &&
              data.matchScore[0].teamScore.length > 0 && (
                <div className="flex justify-between items-center pt-2">
                  <div className="flex flex-col justify-center">
                    {data.matchScore &&
                      data.matchScore[0] &&
                      data.matchScore[0].teamScore &&
                      data.matchScore[0].teamScore.map((team, id) => (
                        <div key={id} className="flex items-center">
                          <p
                            className={`${
                              data.winningTeamID === team.teamID
                                ? "text-green"
                                : "text-white"
                            } text-sm font-semibold pb-1`}
                          >
                            {team.runsScored}/{team.wickets}
                          </p>
                          {team.overs && (
                            <p className="text-xs pl-1 font-medium text-white">
                              ({team.overs})
                            </p>
                          )}
                          {team.declared && (
                            <p className="text-sm text-green  pl-1 font-semibold">
                              d
                            </p>
                          )}
                          {team.folowOn && (
                            <p className="text-sm text-green  pl-1 font-semibold">
                              f
                            </p>
                          )}
                        </div>
                      ))}
                  </div>
                  <div className="flex flex-col justify-center">
                    {data.matchScore &&
                    data.matchScore[1] &&
                    data.matchScore[1].teamScore.length > 0 ? (
                      data.matchScore[1].teamScore.map((team, id) => (
                        <div key={id} className="flex items-center">
                          {team.runsScored && (
                            <p
                              className={`${
                                data.winningTeamID === team.teamID
                                  ? "text-green"
                                  : "text-white"
                              } text-sm font-semibold pb-1`}
                            >
                              {team.runsScored}/{team.wickets}
                            </p>
                          )}
                          {team.overs && (
                            <p className="text-xs pl-1 font-medium text-white">
                              ({team.overs})
                            </p>
                          )}
                          {team.declared && (
                            <p className="text-sm text-green  pl-1 font-semibold">
                              d
                            </p>
                          )}
                          {team.folowOn && (
                            <p className="text-sm text-green  pl-1 font-semibold">
                              f
                            </p>
                          )}
                        </div>
                      ))
                    ) : (
                      <div className="text-gray-2 text-xs font-medium ">
                        Yet to bat
                      </div>
                    )}
                  </div>
                </div>
              )}
          </div>
          <div className="flex bg-gray-4 justify-center items-center my-3 rounded p-2 text-white text-sm font-medium text-center">
            {data.statusMessage}
          </div>
          {console.log(props, "propssssssss")}
          {(props?.from !== "frc" || !props.playTheOdds) &&
            (data.teamsWinProbability && !isPredictedMatch ? (
              <>
                {data.teamsWinProbability && (
                  <div className="">
                    <div className="text-gray-2 text-xs my-2 font-medium">
                      WIN PERCENTAGE
                    </div>
                    <div className=" flex bg-gray-3 rounded-full  h-3 ">
                      <div
                        className={`${
                          parseInt(
                            data.teamsWinProbability.homeTeamPercentage
                          ) >
                          parseInt(data.teamsWinProbability.awayTeamPercentage)
                            ? "bg-green"
                            : "bg-white"
                        }  h-3 rounded-l-lg`}
                        style={{
                          width:
                            data.teamsWinProbability.homeTeamPercentage + "%",
                        }}
                      />
                      <div
                        className={` bg-gray-3 h-3   `}
                        style={{
                          width: data.teamsWinProbability.tiePercentage + "%",
                        }}
                      />
                      <div
                        className={`${
                          parseInt(
                            data.teamsWinProbability.homeTeamPercentage
                          ) <
                          parseInt(data.teamsWinProbability.awayTeamPercentage)
                            ? "bg-green"
                            : "bg-white"
                        }  h-3 rounded-r-lg  `}
                        style={{
                          width:
                            data.teamsWinProbability.awayTeamPercentage + "%",
                        }}
                      />
                    </div>
                  </div>
                )}
                {data.teamsWinProbability && (
                  <div className="flex mt-3 justify-between items-center text-gray-2">
                    <div className=" flex items-center text-xs font-medium">
                      <p
                        className={`w-1.5 h-1.5 ${
                          parseInt(
                            data.teamsWinProbability.homeTeamPercentage
                          ) >
                          parseInt(data.teamsWinProbability.awayTeamPercentage)
                            ? "bg-green"
                            : "bg-white"
                        } rounded-full m-1  `}
                      ></p>
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.homeTeamShortName}{" "}
                      (
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.homeTeamPercentage}
                      %)
                    </div>
                    {data.teamsWinProbability && (
                      <div className="flex items-center text-xs font-medium">
                        <p className="w-1.5 h-1.5 bg-gray-3 rounded-full m-1  "></p>
                        {data.matchType != "Test" ? "Tie" : "DRAW"} (
                        {(data.teamsWinProbability &&
                          data.teamsWinProbability.tiePercentage) ||
                          0}
                        %)
                      </div>
                    )}
                    <div className="flex items-center text-xs font-medium">
                      <p
                        className={`w-1.5 h-1.5 ${
                          parseInt(
                            data.teamsWinProbability.homeTeamPercentage
                          ) <
                          parseInt(data.teamsWinProbability.awayTeamPercentage)
                            ? "bg-green"
                            : "bg-white"
                        } rounded-full m-1  `}
                      ></p>
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.awayTeamShortName}{" "}
                      (
                      {data.teamsWinProbability &&
                        data.teamsWinProbability.awayTeamPercentage}
                      %)
                    </div>
                  </div>
                )}
              </>
            ) : (
              <div className="-mt-4">
                {props?.from !=='frc' && !props.playTheOdds  && (
                  <Predictionbar
                    matchID={data?.matchID}
                  />
                )}
              </div>
            ))}
        </div>
      )}
      {isDetails && <div className="h-[1px] mt-2  w-full"></div>}
      {!hideFantasyCriTab && !props.a23 && (
        <div className="flex justify-center items-center m-2 py-2">
          {showCriclytics &&
          data?.isCricklyticsAvailable &&
          data?.isFantasyAvailable ? (
            <div className="bg-gray-8 w-[1px] h-9 my-1" style={{}}></div>
          ) : null}
          {showCriclytics && data.isCricklyticsAvailable ? (
            <Link
              {...criclyticsUrl()}
              passHref
              onClick={handleCriclyticsNav}
              className="flex flex-auto justify-center items-center pv2 pointer w-1/2 mx-1 bg-gray p-2 rounded-md"
            >
              <div className=" font-bold tracking-wide text-white text-xs mx-2">
                Criclytics <br /> Dashboard
              </div>
              <img
                src={crycLyticsLogo}
                alt="Criclytics"
                className="w-8 pr2"
              ></img>
            </Link>
          ) : null}
          {data?.isFantasyAvailable && (
            <div
              className="flex w-1/2 items-center justify-center bg-gray p-2 rounded-md"
              onClick={() => handleNav(data, data.matchStatus)}
            >
              <div
                className="flex justify-center items-center pv2 pointer"
                onClick={() => (
                  handleFantasyNav(),
                  redirectToFile(data, data?.isLiveCriclyticsAvailable)
                )}
              >
                <div className=" font-bold tracking-wide mx-2 text-white text-xs">
                  Fantasy <br /> Centre{" "}
                </div>
                <img src={frc} className="w-8 pr2" alt="frc"></img>
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
}