

import React, { useState } from "react";

const Tooltip = ({delay,content,className='',direction,children}) => {
  let timeout;
  const [active, setActive] = useState(false);

  const showTip = () => {
    timeout = setTimeout(() => {
      setActive(true);
    }, delay || 400);
  };

  const hideTip = () => {
    clearInterval(timeout);
    setActive(false);
  };

  return (
    <div
      className={`relative ${className}`}
      // onMouseEnter={showTip}
      // onMouseLeave={hideTip}
    >
      {children}
      {active && (
        <div className={`left-1/2 border-2 bg-gray-2 text-xs text-white border-solid border-gray-2 ${direction || "top-0"}`}>
          {content}
        </div>
      )}
    </div>
  );
};

export default Tooltip;
















