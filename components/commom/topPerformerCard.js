import React from 'react'
import { MATCH_SUMMARY, MATCH_DATA_TAB_WISE } from '../../api/queries'
import { useQuery } from '@apollo/react-hooks'

export default function Topperform({
  matchID,
  data,
  status,
  matchSummary,
  ...props
}) {
  if (matchID) {
    const { loading, error, data: matchSummaryData } = useQuery(MATCH_SUMMARY, {
      variables: { matchID, status },
    })

    const tabwise = useQuery(MATCH_DATA_TAB_WISE, {
      variables: { matchID },
    })

    matchSummary = tabwise?.data?.getMatchCardTabWiseByMatchID?.matchSummary

    data = matchSummaryData
  }

  return (
    <div>
      {true && data && data.matchSummary && data.matchSummary && (
        <div className="white bg-gray-8">
          <div className="text-white p-3 text-base font-semibold">
            MATCH SUMMARY
          </div>
          <div className="bg-gray mx-3 pb-3 mb-3">
            <div className="flex">
              <div className=" w-6/12 flex flex-col p-2 items-center  ">
                <div className="text-gray-2 font-semibold mb-1 text-xs">
                  TOP BATTER
                </div>
                <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                  <div className="overflow-hidden w-full h-full rounded-full ">
                    <img
                      className=" object-top object-contain w-24   "
                      style={{ objectPosition: '0 0%' }}
                      src={`https://images.cricket.com/players/${matchSummary?.topPerformer.batsman.playerID}_headshot.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngs/fallbackprojection.png')
                      }
                      alt=""
                    ></img>
                  </div>
                </div>

                {/* matchSummary.topPerformer.bowler */}

                <div className="w-full -mt-10 bg-gray-4 text-center  rounded-md px-2">
                  <div className="text-sm  font-semibold mt-10  truncate text-white ">
                    {matchSummary?.topPerformer.batsman.playerName}
                  </div>
                  {data.matchSummary.bestBatsman.battingStatsList &&
                  props.matchType !== 'Test' ? (
                    <div>
                      <div className="flex  flex-col items-center py-1">
                        {matchSummary?.topPerformer.bowler && (
                          <div className=" text-sm  font-semibold  truncate text-white">
                            <span className="f5 fw5 mr1">
                              {
                                matchSummary?.topPerformer.batsman
                                  .playerMatchRuns
                              }
                              {/* {data.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''} */}
                            </span>
                            <span className="f6">
                              {' '}
                              (
                              {
                                matchSummary?.topPerformer.batsman
                                  .playerMatchBalls
                              }
                              )
                            </span>
                          </div>
                        )}
                        <div className="flex items-center justify-between mt-2">
                          <div className="mr-1">
                            <span className=" text-sm  font-semibold  truncate text-white ">
                              4s{' '}
                            </span>
                            <span className=" text-sm  font-semibold  truncate text-white">
                              {` `}
                              {
                                matchSummary?.topPerformer.batsman
                                  .playerMatchFours
                              }
                            </span>
                          </div>
                          <div className="ml-1 border-l">
                            <span className="f text-sm  font-semibold  truncate text-white ml-2">
                              6s
                            </span>
                            <span className=" text-sm  font-semibold  truncate text-white">
                              {` `}
                              {
                                matchSummary?.topPerformer.batsman
                                  .playerMatchSixes
                              }
                            </span>
                          </div>
                        </div>
                      </div>
                      {false && data.matchSummary.bestBatsman.bowlingStatsList && (
                        <div className="flex  items-center pv1">
                          <div className="ba bg-silver br2 pv1 fw5 b--black  ph2">
                            <span className="f5 fw5 mr1 nowrap">
                              {
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].wickets
                              }
                              /
                              {
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].runsConceded
                              }
                            </span>
                            <span className="f7 ">
                              (
                              {
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].overs
                              }
                              )
                            </span>
                          </div>
                          <div className="pl2 ">
                            <span className="fw4 f6 gray">Eco</span>
                            <span className="fw6 pl1 f7 white">
                              {data.matchSummary.bestBatsman.bowlingStatsList &&
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].economyRate}
                            </span>
                          </div>
                        </div>
                      )}
                    </div>
                  ) : (
                    <div>
                      <div className="flex  items-center pv1">
                        {matchSummary?.topPerformer.bowler && (
                          <div className="ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap ph2">
                            <span className="f5 fw5 mr1">
                              {
                                matchSummary?.topPerformer.bowler
                                  .playerRunsConceeded
                              }
                              {/* {matchSummary.topPerformer.bowler.isNotOut === true ? '*' : ''} */}
                            </span>
                            <span className="f6">
                              ({' '}
                              {
                                matchSummary.topPerformer.bowler
                                  .playerOversBowled
                              }{' '}
                              )
                            </span>
                          </div>
                        )}
                        {matchSummary?.topPerformer.bowler && (
                          <div className="ba bg-frc-yellow black nowrap fw5 br2 pv1 b--black  ml2 ph2">
                            {matchSummary?.topPerformer.bowler && (
                              <span className="f5 fw5">
                                {
                                  matchSummary?.topPerformer.bowler
                                    .playerRunsConceeded
                                }
                              </span>
                            )}
                            {matchSummary?.topPerformer.bowler && (
                              <span className="f7">{`(${matchSummary?.topPerformer.bowler.playerOversBowled})`}</span>
                            )}
                          </div>
                        )}
                      </div>
                      {data.matchSummary.bestBatsman.bowlingStatsList && (
                        <div className="flex  items-center pv1">
                          <div className="ba bg-silver br2 pv1 fw5 b--black  ph2">
                            {' '}
                            <span className="f5 fw5">
                              {
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].wickets
                              }
                              /
                              {
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].runsConceded
                              }
                            </span>
                            <span className="f7 pl2">
                              (
                              {
                                data.matchSummary.bestBatsman
                                  .bowlingStatsList[0].overs
                              }
                              )
                            </span>
                          </div>
                          {data.matchSummary.bestBatsman
                            .bowlingStatsList[1] && (
                            <div className="ph1">&</div>
                          )}
                          {data.matchSummary.bestBatsman
                            .bowlingStatsList[1] && (
                            <div className="ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap  ph2">
                              <span className="f5 fw5">
                                {
                                  data.matchSummary.bestBatsman
                                    .bowlingStatsList[1].wickets
                                }
                                /
                                {
                                  data.matchSummary.bestBatsman
                                    .bowlingStatsList[1].runsConceded
                                }
                              </span>
                              <span className="f7 pl2">
                                (
                                {
                                  data.matchSummary.bestBatsman
                                    .bowlingStatsList[1].overs
                                }
                                )
                              </span>
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  )}
                </div>
              </div>

              {matchSummary?.topPerformer && props.matchType !== 'Test' ? (
                <div className="w-6/12 flex flex-col p-2 items-center">
                  <div className="text-gray-2 font-semibold mb-1 text-xs">
                    TOP BOWLER
                  </div>
                  <div className="relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center ">
                    <div className="overflow-hidden w-full h-full rounded-full ">
                      <img
                        className=" object-top object-contain w-24  rounded-full  "
                        src={`https://images.cricket.com/players/${matchSummary?.topPerformer.bowler.playerID}_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngs/fallbackprojection.png')
                        }
                        alt=""
                      ></img>
                    </div>
                  </div>

                  <div className="w-full -mt-10 bg-gray-4 text-center  rounded-md px-2">
                    <div className="text-base  font-semibold mt-10  text-white  truncate">
                      {'sss' === 'HIN'
                        ? data.matchSummary.bestBowler.playerNameHindi
                        : matchSummary?.topPerformer.bowler.playerName}
                    </div>
                    <div className="flex flex-col  items-center justify-between text-white ">
                      {data.matchSummary.bestBowler && (
                        <div className="text-base">
                          <span className=" font-bold text-white">
                            {
                              matchSummary?.topPerformer.bowler
                                .playerWicketsTaken
                            }
                            /
                            {
                              matchSummary?.topPerformer.bowler
                                .playerWicketsTaken.playerRunsConceeded
                            }
                          </span>
                          <span className="f7  text-xs">
                            (
                            {
                              data.matchSummary.bestBowler.bowlingStatsList[0]
                                .overs
                            }
                            )
                          </span>
                        </div>
                      )}
                    </div>
                    <div className="text-center   pb-2">
                      <span className="text-white text-xs font-semibold">
                        Eco :
                      </span>
                      <span className="text-white text-xs font-semibold ml-1">
                        {matchSummary &&
                          matchSummary?.topPerformer.bowler.playerOversBowled &&
                          matchSummary.topPerformer.bowler.playerEconomyRate}
                      </span>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="flex w-100 items-center justify-center pv2 ">
                  <div className="w-40 flex items-end justify-end">
                    {' '}
                    <img
                      className="h4 h45-l"
                      src={`https://images.cricket.com/players/${data.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = '/pngs/fallbackprojection.png')
                      }
                      alt=""
                    />
                  </div>
                  <div className="w-60  flex lh-copy flex-column  items-start justify-start ">
                    <div className="f6 fw5">
                      {'sss' === 'HIN'
                        ? data.matchSummary.bestBowler.playerNameHindi
                        : data.matchSummary.bestBowler.playerName}
                    </div>
                    <div className="flex   ">
                      <div className="">
                        {data.matchSummary.bestBowler &&
                          data.matchSummary.bestBowler.battingStatsList &&
                          data.matchSummary.bestBowler.battingStatsList.length >
                            0 && (
                            <div className="ba bg-frc-yellow br2 pv1 tc b--black black ph2">
                              {' '}
                              <span className="f6 fw5">
                                {
                                  data.matchSummary.bestBowler
                                    .battingStatsList[0].runs
                                }
                                {data.matchSummary.bestBowler
                                  .battingStatsList[0].isNotOut === true
                                  ? '*'
                                  : ''}
                              </span>
                              <span className="f7 ">
                                &nbsp;(
                                {
                                  data.matchSummary.bestBowler
                                    .battingStatsList[0].balls
                                }
                                )
                              </span>
                            </div>
                          )}
                        {data.matchSummary.bestBowler.bowlingStatsList && (
                          <div className="ba mv2  bg-silver br2 tc pv1 b--black  ph2">
                            {' '}
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  .wickets
                              }
                              /
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  .runsConceded
                              }
                            </span>
                            {/* <span>&nbsp;</span> */}
                            <span className="f7 ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[0]
                                  .overs
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                      <div className="">
                        {data.matchSummary.bestBowler.battingStatsList[1] && (
                          <div className="pa1 f7">&</div>
                        )}
                        {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                          <div className="mv3 ph1  f7">&</div>
                        )}
                      </div>
                      <div className="">
                        {data.matchSummary.bestBowler &&
                          data.matchSummary.bestBowler.battingStatsList &&
                          data.matchSummary.bestBowler.battingStatsList.length >
                            0 &&
                          data.matchSummary.bestBowler.battingStatsList[1] && (
                            <div className="ba bg-frc-yellow  tc br2 pv1 b--black black  ph2">
                              {data.matchSummary.bestBowler
                                .battingStatsList[1] && (
                                <span className="f6 fw5">
                                  {
                                    data.matchSummary.bestBowler
                                      .battingStatsList[1].runs
                                  }
                                  {data.matchSummary.bestBowler
                                    .battingStatsList[1].isNotOut === true
                                    ? '*'
                                    : ''}
                                </span>
                              )}
                              {data.matchSummary.bestBowler
                                .battingStatsList[1] && (
                                <span className="f7 ">
                                  &nbsp;(
                                  {
                                    data.matchSummary.bestBowler
                                      .battingStatsList[1].balls
                                  }
                                  )
                                </span>
                              )}
                            </div>
                          )}
                        {data.matchSummary.bestBowler.bowlingStatsList[1] && (
                          <div className="ba mv2 bg-silver br2 pv1 b--black  tc ph2">
                            {' '}
                            <span className="f6 fw5">
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .wickets
                              }
                              /
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .runsConceded
                              }
                            </span>
                            <span className="f7 ">
                              &nbsp;(
                              {
                                data.matchSummary.bestBowler.bowlingStatsList[1]
                                  .overs
                              }
                              )
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="flex flex-col items-center justify-center mx-3  ">
              {matchSummary?.innings.map((item, index) => {
                return (
                  <>
                    <div className="w-full flex  items-center justify-between my-2 bg-gray-4 p-2">
                      <div className="flex items-center justify-center">
                        <img
                          className=" w-12 h-8 rounded  object-cover object-top"
                          src={`https://images.cricket.com/teams/${item.score.battingTeamID}_flag_safari.png`}
                          alt=""
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        />
                        <span className="ml-2 text-white text-xs font-semibold">
                          {item.score.battingTeamName}
                        </span>
                      </div>
                      <div className="flex text-base font-bold text-white ">
                        {' '}
                        {item.score.runsScored}/{item.score.wickets}{' '}
                        <span className="text-sm ml-1">
                          {'('}
                          {item.score.overs} {')'}
                        </span>{' '}
                      </div>
                    </div>
                    <div className="w-full flex flex-row  items-center justify-start my-2 ">
                      <div className=" flex-col flex items-center justify-start w-6/12  text-xs text-gray-2 border-r border-black">
                        <div className="flex items-center justify-start w-full py-2 font-bold">
                          BATTER
                        </div>

                        {item.battingList.map((itemInner, indexInner) => {
                          return (
                            <>
                              <div className="flex items-center  justify-start w-full text-sm text-white truncate py-1">
                                {itemInner.playerName}
                              </div>
                              <div className="flex items-center justify-start w-full text-sm dark:text-white text-black py-1 ">
                                {itemInner.playerMatchRuns} {'('}
                                {itemInner.playerMatchBalls}
                                {')'}{' '}
                              </div>
                            </>
                          )
                        })}
                      </div>

                      <div className="flex flex-col items-center justify-end w-6/12 text-gray-2 text-xs">
                        <div className="flex items-center justify-end py-2 font-bold w-full ">
                          BOWLER
                        </div>

                        {item.bowlingList.map((itemInner, indexInner) => {
                          return (
                            <>
                              <div className="flex items-center  justify-end w-full text-sm text-white truncate  py-1">
                                {itemInner.playerName}
                              </div>
                              <div className="flex items-center justify-end w-full text-sm text-white py-1">
                                {itemInner.playerWicketsTaken}/
                                {itemInner.playerRunsConceeded}{' '}
                              </div>
                            </>
                          )
                        })}
                      </div>
                    </div>
                  </>
                )
              })}{' '}
            </div>

            {false && (
              <>
                {data.matchSummary.inningOrder
                  .slice(
                    0,
                    props.matchType !== 'Test'
                      ? 2
                      : data.matchSummary.inningOrder.length,
                  )
                  .map((inning, i) => (
                    <div key={i}>
                      {/* <div className='h-solid-divider-light mh3 mv2 '></div> */}
                      <div>
                        {/* {console.log("data.matchSummary", data.matchSummary)} */}
                        <div className="bg-gray-4 m-2 rounded-md p-2">
                          <div className="flex ph3 items-center justify-between">
                            <div className="flex items-center">
                              <img
                                className=" w-10 h-10 rounded-full  object-cover object-top"
                                src={`https://images.cricket.com/teams/${data.matchSummary[inning].teamID}_flag_safari.png`}
                                alt=""
                                onError={(evt) =>
                                  (evt.target.src = flagPlaceHolder)
                                }
                              />
                              <div className="pl2 f6  fw5">
                                {data.matchSummary[
                                  inning
                                ].teamShortName.toUpperCase()}
                              </div>
                            </div>
                            <div className="flex items-center  justify-center">
                              <div className="text-white font-semibold  oswald f3 fw6">
                                {data.matchSummary[inning].runs[i <= 1 ? 0 : 1]}
                                /
                                {
                                  data.matchSummary[inning].wickets[
                                    i <= 1 ? 0 : 1
                                  ]
                                }
                              </div>
                              <div className="text-white font-semibold  ml-1">
                                (
                                {
                                  data.matchSummary[inning].overs[
                                    i <= 1 ? 0 : 1
                                  ]
                                }
                                )
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="ph3 pv2 flex">
                          {data.matchSummary[inning][
                            i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                          ].topBatsman &&
                            data.matchSummary[inning][
                              i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'
                            ].topBatsman.battingStatsList && (
                              <div className="w-50 ph2">
                                <div className="text-gray-2 uppercase text-xs font-semibold">
                                  Batter
                                </div>
                                <div className="flex items-center moon-gray justify-between pv1">
                                  {'sss' === 'HIN' ? (
                                    <div className="truncate w-75 text-white font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerNameHindi
                                      }
                                    </div>
                                  ) : (
                                    <div className="truncate w-75 text-white font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].topBatsman.playerName
                                      }
                                    </div>
                                  )}
                                  <div className="text-white font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary[inning][
                                        i <= 1
                                          ? 'batsmanSummary1'
                                          : 'batsmanSummary2'
                                      ].topBatsman.battingStatsList[0].runs
                                    }
                                    {data.matchSummary[inning][
                                      i <= 1
                                        ? 'batsmanSummary1'
                                        : 'batsmanSummary2'
                                    ].topBatsman.battingStatsList[0]
                                      .isNotOut === true
                                      ? '*'
                                      : ''}
                                  </div>
                                </div>
                                {props.matchType !== 'Test' &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman &&
                                  data.matchSummary[inning][
                                    i <= 1
                                      ? 'batsmanSummary1'
                                      : 'batsmanSummary2'
                                  ].runnerBatsman.battingStatsList && (
                                    <div className="flex moon-gray items-center justify-between pv1 ">
                                      {'sss' === 'HIN' ? (
                                        <div className="truncate w-75 text-white font-semibold">
                                          {' '}
                                          {
                                            data.matchSummary[inning][
                                              i <= 1
                                                ? 'batsmanSummary1'
                                                : 'batsmanSummary2'
                                            ].runnerBatsman.playerNameHindi
                                          }
                                        </div>
                                      ) : (
                                        <div className="truncate w-75 text-white font-semibold">
                                          {' '}
                                          {
                                            data.matchSummary[inning][
                                              i <= 1
                                                ? 'batsmanSummary1'
                                                : 'batsmanSummary2'
                                            ].runnerBatsman.playerName
                                          }
                                        </div>
                                      )}
                                      <div className="text-white font-semibold">
                                        {' '}
                                        {
                                          data.matchSummary[inning][
                                            i <= 1
                                              ? 'batsmanSummary1'
                                              : 'batsmanSummary2'
                                          ].runnerBatsman.battingStatsList[0]
                                            .runs
                                        }
                                        {data.matchSummary[inning][
                                          i <= 1
                                            ? 'batsmanSummary1'
                                            : 'batsmanSummary2'
                                        ].runnerBatsman.battingStatsList[0]
                                          .isNotOut === true
                                          ? '*'
                                          : ''}
                                      </div>
                                    </div>
                                  )}
                              </div>
                            )}
                          <div className="bl  b--white-20"></div>

                          <div className="w-50 ph2 center">
                            <div className="text-gray-2 uppercase text-xs font-semibold">
                              Bowler
                            </div>
                            {data.matchSummary[
                              inning === 'homeTeamData'
                                ? 'awayTeamData'
                                : 'homeTeamData'
                            ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                              .topBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .topBowler.bowlingStatsList && (
                                <div className="flex moon-gray items-center justify-between pv1 ">
                                  {'sss' === 'HIN' ? (
                                    <div className="truncate w-75 text-white font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerNameHindi
                                      }
                                    </div>
                                  ) : (
                                    <div className="truncate w-75 text-white font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].topBowler.playerName
                                      }
                                    </div>
                                  )}
                                  <div className="text-white font-semibold">
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].topBowler.bowlingStatsList[0]
                                        .runsConceded
                                    }
                                  </div>
                                </div>
                              )}
                            {props.matchType !== 'Test' &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler &&
                              data.matchSummary[
                                inning === 'homeTeamData'
                                  ? 'awayTeamData'
                                  : 'homeTeamData'
                              ][i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2']
                                .runnerBowler.bowlingStatsList && (
                                <div className="flex moon-gray items-center justify-between pv1  ">
                                  {'sss' === 'HIN' ? (
                                    <div className="truncate w-75 text-white font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.playerNameHindi
                                      }
                                    </div>
                                  ) : (
                                    <div className="truncate w-75 text-white font-semibold">
                                      {' '}
                                      {
                                        data.matchSummary[
                                          inning === 'homeTeamData'
                                            ? 'awayTeamData'
                                            : 'homeTeamData'
                                        ][
                                          i <= 1
                                            ? 'bowlerSummary1'
                                            : 'bowlerSummary2'
                                        ].runnerBowler.playerName
                                      }
                                    </div>
                                  )}
                                  <div className="text-white font-semibold">
                                    {' '}
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0].wickets
                                    }
                                    /
                                    {
                                      data.matchSummary[
                                        inning === 'homeTeamData'
                                          ? 'awayTeamData'
                                          : 'homeTeamData'
                                      ][
                                        i <= 1
                                          ? 'bowlerSummary1'
                                          : 'bowlerSummary2'
                                      ].runnerBowler.bowlingStatsList[0]
                                        .runsConceded
                                    }
                                  </div>
                                </div>
                              )}
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
              </>
            )}
          </div>
        </div>
      )}
    </div>
  )
}
