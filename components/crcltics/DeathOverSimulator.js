import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { DEATH_OVER_SIMULATOR } from '../../api/queries';
import CleverTap from 'clevertap-react';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const information = '/svgs/information.svg';
const doubleArrow = '/svgs/doubleArrow.svg';
const ground = '/svgs/groundImageWicket.png';
export default function DeathOverSimulator({ browser, isActive, ...props }) {
  const { loading, error, data } = useQuery(DEATH_OVER_SIMULATOR, {
    variables: { matchID: props.matchData[0].matchID },
    fetchPolicy: 'network-only'
  });
  const [isNext3, setIsNext3] = useState(false);
  const [isNext5, setIsNext5] = useState(false);
  const [count, setCount] = useState(0);
  const [infoToggle, setInfoToggle] = useState(false);
  const fallBackRuns = [{ overNumber: '', runs: '10', wickets: '0' }];
  useEffect(() => {
    if (data) {
      CleverTap.initialize('Criclytics', {
        Source: 'Slider',
        MatchID: props.matchData[0].matchID,
        MatchFormat: props.matchData[0].matchType,
        MatchStatus: 'Live',
        CriclyticsEngine: 'LiveDOS',
        Platform: localStorage.Platform
      });
    }
  }, []);
  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };
  if (error)
    return (
      <div className='bg-white w-100 mt2 shadow-4 pa4'>
        <img
          className='w45-m h45-m w4 h4 w5-l h5-l  '
          style={{ margin: 'auto', display: 'block' }}
          src={ground}
          alt='loading...'
        />
        <div className='f5 fw5 tc pt2'>Data Not Available</div>
      </div>
    );
  if (loading || !data) return <div>Loading.. </div>;
  else
    return (
      <div>
        {(data && data.getOverSimulator === null) ||
        (data.getOverSimulator.overSimulatorData &&
          data.getOverSimulator.overSimulatorData.inningNo === '2' &&
          data.getOverSimulator.phaseOfInningsData.length === 0) ||
        data.getOverSimulator.phaseOfInningsData.length === 0 ? (
          <div className='bg-white w-100 mt2 shadow-4 pa4'>
            <img
              className='w45-m h45-m w4 h4 w5-l h5-l  '
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt='loading...'
            />
            <div className='f5 fw5 tc pt2'>Data Not Available</div>
          </div>
        ) : (
          <div>
            {}
            <div className='bg-white mt2 shadow-4   mw75-l'>
              <div className='flex justify-between items-center pa2'>
                <h1 className='f7 f5-l fw5 grey_10 ttu'>Over Simulator</h1>
                <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
              </div>
              <div className='outline light-gray' />
              {infoToggle && (
                <div className='bg-white'>
                  <div className='f7 grey_8 pa2'>Forecast of where the match is headed, in the next few overs.</div>
                  <div className='outline light-gray' />
                </div>
              )}
              <div className='flex justify-between pa2 items-center'>
                <div className='pa2 flex justify-start items-center'>
                  <img
                    className='w2 h13 shadow-4'
                    src={`https://images.cricket.com/teams/${data.getOverSimulator.overSimulatorData.currentTeamId}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    alt=''
                  />
                  <div className='f6 pl2 fw5'>{data.getOverSimulator.overSimulatorData.currentTeamShortName}</div>
                </div>
                <div className='pa2 flex justify-end items-center'>
                  <div>
                    <span className='f6 fw6'>
                      {data.getOverSimulator.overSimulatorData.currentScore
                        ? data.getOverSimulator.overSimulatorData.currentScore
                        : 0}
                      /
                      {data.getOverSimulator.overSimulatorData.currentWickets
                        ? data.getOverSimulator.overSimulatorData.currentWickets
                        : 0}
                    </span>
                    <span className='pl1 f8 gray'>({data.getOverSimulator.overSimulatorData.currentOvers})</span>
                  </div>
                  <div className='flex items-center ba b--black-40 br-pill pv1 ph2 ml2'>
                    <span className='bg-green h04 w04 br-100' />
                    <span className='f10 fw6 pl1'>LIVE</span>
                  </div>
                </div>
              </div>
              <div className='f7 pl1 '> Runs </div>
              <div className='mt2 flex flex-row ph1 pl2-l '>
                <div
                  className=''
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    height: Math.max.apply(
                      Math,
                      [
                        ...data.getOverSimulator.overSimulatorData.predictedOversArray,
                        ...data.getOverSimulator.phaseOfInningsData,
                        ...fallBackRuns
                      ].map((o) => {
                        return o.runs * 8 + 24;
                      })
                    )
                  }}>
                  {[1, 2, 3, 4, 5].map((val, i) => (
                    <div
                      key={i}
                      className='f8 mr1 flex'
                      style={{
                        height: Math.max.apply(
                          Math,
                          [
                            ...data.getOverSimulator.overSimulatorData.predictedOversArray,
                            ...data.getOverSimulator.phaseOfInningsData,
                            ...fallBackRuns
                          ].map((o) => {
                            return (o.runs * 8 + 5) / 5;
                          })
                        )
                      }}>
                      {data.getOverSimulator.phaseOfInningsData.length > 0
                        ? Math.max.apply(
                            Math,
                            [
                              ...data.getOverSimulator.overSimulatorData.predictedOversArray,
                              ...data.getOverSimulator.phaseOfInningsData,
                              ...fallBackRuns
                            ].map((o) => {
                              return Math.floor((o.runs / 5) * (5 - i));
                            })
                          )
                        : 0}
                    </div>
                  ))}
                </div>
                <div
                  className='pb3 center pr3 relative '
                  style={{ width: window.innerWidth > 965 ? 950 : window.innerWidth - 26 }}>
                  <div
                    className='bl bb overflow-hidden flex  '
                    style={{
                      flexDirection: 'row',
                      height: Math.max.apply(
                        Math,
                        [
                          ...data.getOverSimulator.overSimulatorData.predictedOversArray,
                          ...data.getOverSimulator.phaseOfInningsData,
                          ...fallBackRuns
                        ].map((o) => {
                          return o.runs * 8 + 24;
                        })
                      )
                    }}>
                    {data.getOverSimulator.phaseOfInningsData.map((x, i) => {
                      return (
                        <div
                          className=''
                          style={{
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                            display: 'flex',
                            flexDirection: 'column'
                          }}
                          key={Math.random()}>
                          {[...Array(Number(x.wickets))].map((x) => (
                            <div
                              key={Math.random()}
                              style={{
                                width: 5,
                                height: 5,
                                borderRadius: 5,
                                backgroundColor: '#d44030',
                                marginBottom: 5
                              }}
                            />
                          ))}
                          <div
                            className='br'
                            style={{
                              width:
                              data.getOverSimulator.overSimulatorData.format == 'T10'?
                                      window.innerWidth > 965
                                    ? (950 * 0.97) / 10
                                    : (window.innerWidth * 0.8) / 10
                              :
                                data.getOverSimulator.overSimulatorData.format == 'T20'
                                  ? window.innerWidth > 965
                                    ? (950 * 0.97) / 20
                                    : (window.innerWidth - 42) / 20
                                  : window.innerWidth > 965
                                  ? (950 * 0.97) / 50
                                  : (window.innerWidth * 0.78) / 50,
                              alignSelf: 'stretch',
                              border: '0 1px 0 0 ',
                              height: x.runs * 8,
                              borderColor: 'white',
                              backgroundColor: '#C06C84'
                            }}
                          />
                        </div>
                      );
                    })}
                    {(isNext3 || isNext5) &&
                      data.getOverSimulator.overSimulatorData.predictedOversArray.map((x, i) => {
                        return (
                          i < count && (
                            <div
                              key={Math.random()}
                              style={{
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                                display: 'flex',
                                flexDirection: 'column'
                              }}>
                              {[...Array(Number(x.wickets))].map((x) => (
                                <div
                                  key={Math.random()}
                                  style={{
                                    width: 5,
                                    height: 5,
                                    borderRadius: 5,
                                    backgroundColor: '#d44030',
                                    marginBottom: 5
                                  }}
                                />
                              ))}
                              <div
                                className='br'
                                style={{
                                  width:
                                  data.getOverSimulator.overSimulatorData.format == 'T10'?
                                      window.innerWidth > 965
                                    ? (950 * 0.97) / 10
                                    : (window.innerWidth * 0.8) / 10
                              :
                                    data.getOverSimulator.overSimulatorData.format == 'T20'
                                      ? window.innerWidth > 965
                                        ? (950 * 0.97) / 20
                                        : (window.innerWidth - 42) / 20
                                      : window.innerWidth > 965
                                      ? (950 * 0.97) / 50
                                      : (window.innerWidth * 0.78) / 50,
                                  height: x.runs * 8,
                                  border: '0 1px 0 0 ',
                                  borderColor: 'white',
                                  backgroundColor: '#163567'
                                }}
                              />
                            </div>
                          )
                        );
                      })}
                  </div>
                  <div className='flex flex-row relative pl1 '>
                    {}
                    <div className=' absolute f8' style={{ left: -10, bottom: 4 }}>
                      {' '}
                      0{' '}
                    </div>
                    {(data.getOverSimulator.overSimulatorData.format == 'T10'?[1]:data.getOverSimulator.overSimulatorData.format == 'T20' ? [1, 2] : [1, 2, 3, 4, 5]).map(
                      (val, i) => (
                        <div
                          key={i}
                          className='flex  justify-end pt1 f8'
                          style={{
                            width:
                              (window.innerWidth > 950 ? 940 : window.innerWidth - 26) /
                              (data.getOverSimulator.overSimulatorData.format == 'T10'? 1 :data.getOverSimulator.overSimulatorData.format == 'T20' ? 2 : 5)
                          }}>
                          {(i + 1) * 10}
                        </div>
                      )
                    )}
                  </div>
                  <div className='f8 absolute bg-white vertical-text' style={{ bottom: 34, right: -6 }}>
                    Overs
                  </div>
                </div>
              </div>
              {data.getOverSimulator.overSimulatorData.threeFlag === false ? (
                data.getOverSimulator.overSimulatorData.inningNo === 1 ? (
                  <div className='bg-light-gray tc f8 f7-ns pa2 fw1'>
                    Over Simulation will commence once match resumes
                  </div>
                ) : data.getOverSimulator.overSimulatorData.inningNo === 2 ? (
                  <div className='bg-light-gray tc f8 f7-ns pa2 fw1'>Over simulator not available for Super Over</div>
                ) : (
                  <div />
                )
              ) : (
                <div className='flex justify-center items-center'>
                  <div
                    className={`ba b--darkRed flex items-center ma2 br-pill ph3 pv2 cursor-pointer ${
                      isNext3 || !data.getOverSimulator.overSimulatorData.threeFlag ? 'cdcgr' : 'bg-white'
                    }`}
                    onClick={() =>
                      (!isNext3 || isNext5) ?
                      (data.getOverSimulator.overSimulatorData.threeFlag)&&
                      (setIsNext3(true), setCount(3), setIsNext5(false)):setIsNext3(false)
                    }>
                    <span
                      className={`${
                        isNext3 || !data.getOverSimulator.overSimulatorData.threeFlag ? 'white' : 'darkRed'
                      } f6 fw6`}>
                      NEXT 3
                    </span>
                    <div
                      className={` ${
                        isNext3 || !data.getOverSimulator.overSimulatorData.threeFlag ? 'white' : 'darkRed'
                      }  flex items-center pl2`}>
                      &#9654;&#9654;
                    </div>
                  </div>
                  <div
                    className={`ba b--darkRed flex items-center ma2 br-pill ph3 pv2 cursor-pointer ${
                      isNext5 || !data.getOverSimulator.overSimulatorData.fiveFlag ? 'cdcgr' : 'bg-white'
                    }`}
                    onClick={() => (
                      isNext5 || !data.getOverSimulator.overSimulatorData.fiveFlag ? setIsNext5(false): setIsNext5(true),
                      setCount(5),
                      setIsNext3(false)
                    )}>
                    <span
                      className={`${
                        isNext5 || !data.getOverSimulator.overSimulatorData.fiveFlag ? 'white' : 'darkRed'
                      } f6 fw6`}>
                      NEXT 5
                    </span>
                    <div
                      className={`${
                        isNext5 || !data.getOverSimulator.overSimulatorData.fiveFlag ? 'white' : 'darkRed '
                      } flex items-center pl2`}>
                      &#9654;&#9654;
                    </div>
                  </div>
                </div>
              )}
              {(isNext3 || isNext5) && (
                <div>
                  <div className='pv2 flex justify-center items-center'>
                    <div className='br-pill pv1 ph3 tc f7 fw5 truncate ttu' style={{ backgroundColor: '#F16A3633' }}>
                      Projected Score End of Over{' '}
                      {isNext3 && !isNext5
                        ? data.getOverSimulator.overSimulatorData.nextThreeOvers
                        : data.getOverSimulator.overSimulatorData.nextFiveOvers}
                    </div>
                  </div>
                  <div className='pb2 tc'>
                    <div className='f3 fw5 darkRed'>
                      {isNext3 && !isNext5
                        ? data.getOverSimulator.overSimulatorData.nextThreeScore +
                          '/' +
                          data.getOverSimulator.overSimulatorData.nextThreeWickets
                        : data.getOverSimulator.overSimulatorData.nextFiveScore +
                          '/' +
                          data.getOverSimulator.overSimulatorData.nextFiveWickets}
                    </div>
                    <div className='f7 fw4 darkRed'>
                      (
                      {isNext3 && !isNext5
                        ? data.getOverSimulator.overSimulatorData.nextThreeOvers
                        : data.getOverSimulator.overSimulatorData.nextFiveOvers}
                      )
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    );
}
