import React, { useState, useEffect, useRef } from 'react';

import { useRouter } from 'next/router';




import SwiperCore, { EffectCoverflow, Navigation, Pagination, Scrollbar } from 'swiper';

// install Swiper modules
SwiperCore.use([EffectCoverflow, Pagination, Navigation, Scrollbar]);
import Matchups from './matchUps';

function HomePage({ browser, ...props }) {

  const router = useRouter();


 
 
  return <div className='mx-2 max-vh-90 overflow-scroll md:mt-14'>
    <div className=''>
          <div className='text-md  text-white tracking-wide font-bold '>Player MatchUp</div>
          <div className='text-xs text-white pt-1 font-medium tracking-wide'>
            {' '}
            Select a batter and see how they compare against opposing bowlers{' '}
          </div>
        </div>

      <div className='  mt-4  text-white  text-sm font-semibold '>Batter Vs Bowler Performance</div>
      <div className='w-12 h-1 bg-blue-8 mt-2'></div>
  <Matchups matchID={props.matchID} data={props.data} />
 </div>;
}


export default HomePage;

