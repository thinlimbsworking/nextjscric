import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { GET_KEY_STATS } from '../../api/queries';
import DataNotFound from '../commom/datanotfound';
const fallbackProjection = '/pngs/fallbackprojection.png';
const location = '/svgs/location-icon-color.png';
const information = '/svgs/information.svg';

const empty = '/svgs/Empty.svg';
export default function KeyStats({ data, browser, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false);


  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  if (!data)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  else
    return (<>
      { data.getKeyStats.head2headStats ?<div>
        <div className='flex justify-between items-center pa2 ph3-l bg-white mt2 shadow-4'>
          <h1 className='f7 f5-l fw5 grey_10 ttu'>Key Stats</h1>
          <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
        </div>
        <div className='outline light-gray' />
        {infoToggle && (
          <div className='bg-white'>
            <div className='f7 grey_8 pa2'>
              Just the right stats to help you determine which team statistically has the upper hand.
            </div>
            <div className='outline light-gray ' />
          </div>
        )}
        {/* Head to head */}
        { data &&  data.getKeyStats && data.getKeyStats.head2headStats && 
        data.getKeyStats.head2headStats.head2Head.totalMatches &&
        data.getKeyStats.head2headStats.head2Head.totalMatches !== 0 ? (
          <div className='bg-white  shadow-4'>
            <div className='pa2 ph3-l flex justify-between items-center '>
              <div className='f6 fw4 f5-l fw5-l ttu'>Head To Head</div>
              <div className='silver i f6 fw5'>
                {props.matchData[0].matchType === 'Test' ? '*last 10 Matches' : '*last 3 years'}
              </div>
            </div>
            <div className='outline light-gray ' />
            <div className='pa3'>
              <div className='w-100 flex justify-center tc fw6 f6 white bg-grey_5'>
                <div
                  className='pv2'
                  style={{
                    width:
                      data.getKeyStats.head2headStats.head2Head.teamA *
                        (100 /
                          (data.getKeyStats.head2headStats.head2Head.teamA +
                            data.getKeyStats.head2headStats.head2Head.teamB +
                            data.getKeyStats.head2headStats.head2Head.noResult)) +
                      '%',
                    background: `${data.getKeyStats.head2headStats.teamA.color === null}`
                      ? '#3BBBA2'
                      : '{data.getKeyStats.head2headStats.teamA.color}'
                  }}>
                  {data.getKeyStats.head2headStats.head2Head.teamA}
                </div>
                <div
                  className='pv2'
                  style={{
                    width:
                      data.getKeyStats.head2headStats.head2Head.teamB *
                        (100 /
                          (data.getKeyStats.head2headStats.head2Head.teamA +
                            data.getKeyStats.head2headStats.head2Head.teamB +
                            data.getKeyStats.head2headStats.head2Head.noResult)) +
                      '%',
                    background: `${data.getKeyStats.head2headStats.teamB.color === null}`
                      ? '#D9CAA1'
                      : '{data.getKeyStats.head2headStats.teamB.color}'
                  }}>
                  {data.getKeyStats.head2headStats.head2Head.teamB}
                </div>
                <div
                  className='pv2'
                  style={{
                    width:
                      data.getKeyStats.head2headStats.head2Head.noResult *
                        (100 /
                          (data.getKeyStats.head2headStats.head2Head.teamA +
                            data.getKeyStats.head2headStats.head2Head.teamB +
                            data.getKeyStats.head2headStats.head2Head.noResult)) +
                      '%',
                    background: '#d8d8d8'
                  }}>
                  {data.getKeyStats.head2headStats.head2Head.noResult}
                </div>
              </div>

              {/* line separator */}
      
              <div className='mt3 flex justify-between items-center'>
                <span className='w-34 w-40-l h01 bg-grey_5_1' />
                <span className='ph2 w-25 w-10-l f7 tc fw5'>
                  {data.getKeyStats.head2headStats.head2Head.totalMatches} Matches
                </span>
                <span className='w-34 w-40-l h01 bg-grey_5_1' />
              </div>

              {/* countary name and label */}
              <div className='flex justify-between mt3 f9'>
                <div className='w-30'>
                  <div
                    className='w2-3 h02 mb2'
                    style={{
                      background: `${data.getKeyStats.head2headStats.teamA.color === null}`
                        ? '#3BBBA2'
                        : '{data.getKeyStats.head2headStats.teamA.color}'
                    }}
                  />
                  <div className='f7 fw4'>{data.getKeyStats.head2headStats.teamA.teamShortName}</div>
                </div>
                <div className='w-30'>
                  <div
                    className='w2-3 h02 mb2 center'
                    style={{
                      background: `${data.getKeyStats.head2headStats.teamB.color === null}`
                        ? '#D9CAA1'
                        : '{data.getKeyStats.head2headStats.teamB.color}'
                    }}
                  />
                  <div className='f7 fw4 tc'>{data.getKeyStats.head2headStats.teamB.teamShortName}</div>
                </div>
                <div className='w-30 pl3'>
                  <div className='w2-3 h02 mb2 bg-grey_5' />
                  <div className='f7 fw4 '>
                    {props.matchData[0].matchType === 'Test' ? 'No Result/Draw' : 'No Result/Tie'}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ):null}
        {/* End of Head to head */}
        {/* Stadium Details */}

        {data.getKeyStats.head2headStats.venueStatsData.avgFirstInningScore ||
        data.getKeyStats.head2headStats.venueStatsData.firstBattingWinPercent ||
        data.getKeyStats.head2headStats.venueStatsData.highestScoreChased ||
        data.getKeyStats.head2headStats.venueStatsData.paceWicketPercent ||
        data.getKeyStats.head2headStats.venueStatsData.spinWicketPercent ? (
          <div className='mv2 mv0-l  bg-white shadow-4'>
            <div className='flex justify-between items-center pa2'>
              <span className='f6 fw4 f5-l fw5-l'>Stadium Details</span>
              <span className='silver i f6 fw5'>
                {props.matchData[0].matchType === 'Test' ? '*last 3 years' : '*last 3 years'}
              </span>
            </div>
            <div className='outline light-gray' />
            <div className='pa3 flex items-center'>
              <img className='h13 w1' src={location} alt='' />
              <span className='pl1 f7 f6-l fw5 gray'>{data.getKeyStats.head2headStats.venueStatsData.venueName}</span>
            </div>
            <div className='flex justify-between pa2 pv2-l ph5-l items-center mb2 '>
              <div className='tc'>
                <div className='f8 f6-l fw4'> 1st Batting Average Score </div>
                <div className='pt1 f4 fw5'>
                  {data.getKeyStats.head2headStats.venueStatsData.avgFirstInningScore
                    ? data.getKeyStats.head2headStats.venueStatsData.avgFirstInningScore
                    : '--'}{' '}
                </div>
              </div>
              <div className='v-solid-divider' />
              <div className='tc'>
                <div className='f8 f6-l fw4'> 1st Batting win Percentage</div>
                <div className='f4 fw5 pt1'>
                  {' '}
                  {data.getKeyStats.head2headStats.venueStatsData.firstBattingWinPercent
                    ? data.getKeyStats.head2headStats.venueStatsData.firstBattingWinPercent + '%'
                    : '--'}{' '}
                </div>
              </div>
              <div className='v-solid-divider' />
              <div className='tc'>
                <div className='f8 f6-l fw4'> Highest Score Chased </div>
                <div className='pt1 f4 fw5'>
                  {' '}
                  {data.getKeyStats.head2headStats.venueStatsData.highestScoreChased
                    ? data.getKeyStats.head2headStats.venueStatsData.highestScoreChased
                    : '--'}{' '}
                </div>
              </div>
            </div>

            <div className='outline light-gray' />
            <div className=' pa2 f6 fw4 f5-l'>Wickets Split</div>
            <div className='flex justify-around items-center pb2'>
              <div className='tc w-25-l'>
                <div className='f8 f6-l fw4'>Pace </div>
                <div className='f4 fw5 pt2'>
                  {data.getKeyStats.head2headStats.venueStatsData.paceWicketPercent
                    ? data.getKeyStats.head2headStats.venueStatsData.paceWicketPercent
                    : '--'}
                  %{' '}
                </div>
              </div>
              <div className='v-solid-divider mv1' />
              <div className='tc w-25-l'>
                <div className='f8 f6-l fw4'>Spin </div>
                <div className='f4 fw5 pt2'>
                  {data.getKeyStats.head2headStats.venueStatsData.spinWicketPercent
                    ? data.getKeyStats.head2headStats.venueStatsData.spinWicketPercent
                    : '--'}
                  %{' '}
                </div>
              </div>
            </div>
          </div>
        ) : (
          ''
        )}

        {/* End Of Stadium Details */}

        {/* Top Runner Scored */}
        { (data && data.getKeyStats &&data.getKeyStats.topRunScorer1 ||  data && data.getKeyStats &&data.getKeyStats.topRunScorer2) &&   <div className='mv2 mv0-l bg-white shadow-4'>
          <div className='pa2 f6 fw4 f5-l fw5-l ph3-l'>Top Run Scorer in Last 5 Matches</div>
          <div className='divider-dark' />

          <div className='w-100 flex justify-between   justify-around-l items-center pv2'>
         
          { data && data.getKeyStats &&data.getKeyStats.topRunScorer1 &&   <div className='ph2 w-50 w-25-l'>
              <div className='f7 fw5 gray tc pv1'>{data && data.getKeyStats && data.getKeyStats.topRunScorer1 && data.getKeyStats.topRunScorer1.fullName}
              
               </div>
              <div className='w-100 flex justify-start items-center'>
                <div className='br-50 w2-3 h2-3 w2-4-l h2-4-l overflow-hidden bg-white ba b--light-gray'>
                  <img
                    className='contain'
                    src={data && data.getKeyStats && data.getKeyStats.topRunScorer1 ? ` https://images.cricket.com/players/${data.getKeyStats.topRunScorer1.playerID}_headshot_safari.png`:fallbackProjection}
                    alt=''
                    onError={(evt) => (evt.target.src = fallbackProjection)}
                  />
                </div>
                <span className='f7 fw5 ml2'>{data && data.getKeyStats && data.getKeyStats.topRunScorer1 && data.getKeyStats.topRunScorer1.playerName}</span>
              </div>
              <div className='flex justify-between items-center pv2'>
                <span className='f7 gray fw5'>Runs</span>
                <span className='f7 fw6'>{ data && data.getKeyStats && data.getKeyStats && data.getKeyStats.topRunScorer1&& data.getKeyStats.topRunScorer1.playerRuns}</span>
              </div>
              <div className='flex justify-between items-center pt1 pb2'>
                <span className='f7 gray fw5'>Average</span>
                <span className='f7 fw6 '>{data && data.getKeyStats && data.getKeyStats && data.getKeyStats.topRunScorer1&& data.getKeyStats.topRunScorer1.average}</span>
              </div>
            </div>
      }

            <div className='v-solid-divider mv2 ' />

            { data && data.getKeyStats &&data.getKeyStats.topRunScorer2 && <div className='w-50 w-25-l ph2'>
              <div className='f7 fw4 gray tc pv1'> {data && data.getKeyStats && data.getKeyStats && data.getKeyStats.topRunScorer2&& data.getKeyStats.topRunScorer2.fullName} </div>
              <div className='w-100 flex justify-start items-center'>
                <div className='br-50 w2-3 h2-3 w2-4-l h2-4-l overflow-hidden bg-white ba b--light-gray'>
                  <img
                    className='contain'
                    src={data && data.getKeyStats && data.getKeyStats.topRunScorer2 ?`https://images.cricket.com/players/${data.getKeyStats.topRunScorer2.playerID}_headshot_safari.png`:fallbackProjection}
                    alt=''
                    onError={(evt) => (evt.target.src = fallbackProjection)}
                  />
                </div>
                <span className='f7 fw5 ml2'>{data.getKeyStats.topRunScorer2.playerName}</span>
              </div>
              <div className='flex justify-between items-center pv2'>
                <span className='f7 gray fw5'>Runs</span>
                <span className='f7 fw6 '>{data.getKeyStats.topRunScorer2.playerRuns}</span>
              </div>
              <div className='flex justify-between items-center pt1 pb2'>
                <span className='f7 gray fw5 '>Average</span>
                <span className='f7 fw6 '>{data.getKeyStats.topRunScorer2.average}</span>
              </div>
            </div>
}
          </div>
        </div>
}

     {  (data && data.getKeyStats &&data.getKeyStats.topRunScorer1 ||  data && data.getKeyStats &&data.getKeyStats.topRunScorer2) ? <div className='bg-white shadow-4 mv2 mt0-l'>
          <div className='pa2 f6 fw4 f5-l fw5-l ph3-l'>Top Wicket Taker in Last 5 Matches</div>
          <div className='divider-dark' />

          <div className='w-100 flex justify-between justify-around-l items-center pv2'>
            <div className='ph2 w-50 w-25-l'>
              <div className='f7 fw5 gray tc pv1'> {data && data.getKeyStats.topWicketTaker1 && data.getKeyStats.topWicketTaker1.fullName} </div>

              <div className='w-100 flex justify-start items-center'>
                <div className='br-50 w2-3 h2-3 w2-4-l h2-4-l overflow-hidden bg-white ba b--light-gray'>
                  <img
                    className='contain'
                    src={data && data.getKeyStats && data.getKeyStats.topWicketTaker1 ?`https://images.cricket.com/players/${  data.getKeyStats.topWicketTaker1.playerID}_headshot_safari.png`:fallbackProjection}
                    alt=''
                    onError={(evt) => (evt.target.src = fallbackProjection)}
                  />
                </div>
                <span className='f7 fw5 ml2'>{data && data.getKeyStats && data.getKeyStats.topWicketTaker1 && data.getKeyStats.topWicketTaker1.playerName}</span>
              </div>
              <div className='flex justify-between  items-center pv2'>
                <span className='f7 gray fw5'>Wickets</span>
                <span className='f7 fw6'>{data && data.getKeyStats && data.getKeyStats.topWicketTaker1 && data.getKeyStats.topWicketTaker1.totalWickets}</span>
              </div>
              <div className='flex justify-between items-center pt1 pb2'>
                <span className='f7 gray fw5'>Average</span>
                <span className='f7 fw6'>{data && data.getKeyStats && data.getKeyStats.topWicketTaker1 && data.getKeyStats.topWicketTaker1.average}</span>
              </div>
            </div>

            <div className='v-solid-divider mv2 ' />

            {data && data.getKeyStats && data.getKeyStats.topWicketTaker2 && <div className='ph2 w-50 w-25-l'>
              <div className='f7 fw4 gray tc pv1'> {data && data.getKeyStats && data.getKeyStats.topWicketTaker2 && data.getKeyStats.topWicketTaker2.fullName} </div>

              <div className='w-100 flex justify-start items-center'>
                <div className='br-50 w2-3 w2-4-l h2-4-l h2-3 overflow-hidden ba b--light-gray bg-white'>
                  <img
                    className='contain'
                    src={ data && data.getKeyStats && data.getKeyStats.topWicketTaker2 && `https://images.cricket.com/players/${data.getKeyStats.topWicketTaker2.playerID}_headshot_safari.png`}
                    alt=''
                    onError={(evt) => (evt.target.src = fallbackProjection)}
                  />
                </div>
                <span className='f7 fw5 ml2'>{data.getKeyStats.topWicketTaker2.playerName}</span>
              </div>
              <div className='flex justify-between items-center pv2'>
                <span className='f7 gray fw5'>Wickets</span>
                <span className='f7 fw6'>{data.getKeyStats.topWicketTaker2.totalWickets}</span>
              </div>
              <div className='flex justify-between items-center pt1 pb2'>
                <span className='f7 gray fw5'>Average</span>
                <span className='f7 fw6'>{data.getKeyStats.topWicketTaker2.average}</span>
              </div>
            </div>}
          </div>
        </div>:<DataNotFound/>}
        {/* End of Top Wicket Taker */}
      </div>:<div className="">
      <div className='flex justify-between items-center pa2 ph3-l bg-white mt2 shadow-4'>
          <h1 className='f7 f5-l fw5 grey_10 ttu'>Key Stats</h1>
          <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
        </div>
        <div className="bg-white flex mt2 flex-column items-center justify-center fw5 tc pv4">
         <div className="w5  h5">
         <img className="h-100 w-100" src={empty} alt=""/>
         </div>
         <div>Key Stats data not available</div></div>
      </div>}
      </>
    );
}
