import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { LIVE_SCORE_PREDICTOR } from '../../api/queries';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const ground = '/svgs/groundImageWicket.png';



export default function LiveScorePredictor({ browser, ...props }) {
 
  const { loading, error, data } = useQuery(LIVE_SCORE_PREDICTOR, {
    variables: { matchId: props.matchData[0].matchID, matchType: props.matchData[0].matchType },
    pollInterval: 8000
  });

  if (error)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  if (loading || !data) return <div>Loading.. </div>;
  else
    return (
      <div>
        <div className='mh3'>
          <p className='f4 mb3 fw7 '>Live Score Predictor</p>
          <p className='fw4 f6 gray lh-copy'>Is it going to be a high-scoring affair or a low-scoring bummer?</p>
        </div>

        {data.liveScorePredictor.liveScores.length > 0 ? (
          <div>
            <div className='bg-white'>
              <div className='divider' />
              <div className='pa3'>
            
                <div className='fw4 f7 mb2'>Who’s winning?</div>
                <div className='divider mb3' />

                {data.liveScorePredictor.liveScores.map((inning, i) => (
                  <div key={i} className=''>
                    <div className='flex flex-row justify-between f7 fw6'>
                      <div
                        className={`${inning.homeTeamPercentage > inning.awayTeamPercentage ? 'green_10' : 'red_10'}`}>
                        {inning.homeTeamPercentage}%
                      </div>
                      {inning.tiePercentage && inning.tiePercentage > 0 ? (
                        <div className='  grey_6'>{inning.tiePercentage}%</div>
                      ) : (
                        <div />
                      )}
                      <div
                        className={`${inning.homeTeamPercentage > inning.awayTeamPercentage ? 'red_10' : 'green_10'}`}>
                        {inning.awayTeamPercentage}%
                      </div>
                    </div>
                    <div className='flex pv1 justify-center items-center'>
                      <div
                        style={{ width: `${inning.homeTeamPercentage}%`, height: 4 }}
                        className={` dib br2 br--left ${
                          inning.homeTeamPercentage > inning.awayTeamPercentage ? 'bg-green_10' : 'bg-red_10'
                        }`}></div>
                      {inning.tiePercentage && inning.tiePercentage > 0 ? (
                        <div className=' dib bg-grey_2' style={{ width: `${inning.tiePercentage}%`, height: 4 }} />
                      ) : (
                        <div />
                      )}
                      <div
                        style={{ width: `${inning.awayTeamPercentage}%`, height: 4 }}
                        className={`dib ${
                          inning.homeTeamPercentage > inning.awayTeamPercentage ? 'bg-red_10' : 'bg-green_10'
                        } dib br2 br--right`}></div>
                    </div>
                    <div className='flex flex-row justify-between f7'>
                      <div className='fw6 f7 grey_8'>{inning.homeTeamShortName}</div>
                      {inning.tiePercentage && inning.tiePercentage > 0 ? (
                        <div className='fw6 f7 grey_8'>Draw</div>
                      ) : (
                        <div />
                      )}
                      <div className='fw6 f7 grey_8'>{inning.awayTeamShortName}</div>
                    </div>
                  </div>
                ))}
              
              </div>
              <div className='divider' />
            </div>
            <div>
              <div className='divider' />
              <div className='flex justify-between items-center pa2 ph3'>
                <span className='f8 fw5 w-20'>LIVE</span>
                <span className='f8 fw5'>TEAM</span>
                <span className='f8 fw5 w-20 tr '>PROJECTED</span>
              </div>
              <div className='divider' />
            </div>
         
            {data.liveScorePredictor.liveScores.map((inning, i) => (
              <div key={i}>
                <p className='fw2 f7 gray lh-copy pl2 mv0 pv1'>INNINGS 1</p>
                <div className='bg-white pa3 flex justify-between items-center'>
                  <div className='tl pv2 w-20'>
                    <p className='oswald f4 fw2 ma0'>
                      {inning.inningNo === 1
                        ? inning.currentScore + '/' + inning.currentWickets
                        : inning.predictedScore + (inning.predictedWicket !== 10 ? '/' + inning.predictedWicket : '')}
                    </p>
                    <p className='f7 fw5 gray ma0 mt2'>
                      ({inning.inningNo === 1 ? inning.overNo : inning.predictedOver})
                    </p>
                  </div>

                  <div className='flex justify-between items-center w-50 mb3 dib'>
                    <div className='bg-orange br-50' style={{ height: 10, width: 10 }} />
                    <div className='tc ' style={{ height: 1, width: '95%', borderBottom: '1px dashed gray' }}>
                      <img
                        className='ib w2-5 h2 nt3 shadow-4'
                        src={`https://images.cricket.com/teams/${inning.inningIds[0]}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        alt=''
                      />
                      <p className='ma0 mt2 dark-gray f6 fw6'>
                        {inning.inningIds[0] === inning.team1Id ? inning.team1ShortName : inning.team2ShortName}
                      </p>
                    </div>
                    <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                  </div>

                  <div className='tr pv2 w-20'>
                    <p className='oswald f4 fw2 red_10 ma0 cdc'>
                      {inning.inningNo === 1
                        ? inning.predictedScore + (inning.predictedWicket !== 10 ? '/' + inning.predictedWicket : '')
                        : '--'}
                    </p>
                    {inning.inningNo === 1 ? (
                      <p className='f6 fw5 gray red_10 ma0  mt2'>({inning.predictedOver})</p>
                    ) : (
                      ''
                    )}
                  </div>
                </div>
                <div className='divider' />
              </div>
            ))}
          
            {data.liveScorePredictor.liveScores.map((inning, i) => (
              <div key={i}>
               
                <div className='bg-white pa3 flex justify-between items-center'>
                  <div className='tl pv2 w-20'>
                    <p className='oswald f4 fw2 ma0'>
                      {inning.inningNo === 2
                        ? inning.currentScore + '/' + inning.currentWickets
                        : inning.inningNo > 2
                        ? inning.secondPredictedScore +
                          (inning.secondPredictedWicket !== 10 ? '/' + inning.secondPredictedWicket : '')
                        : '--'}
                    </p>
                    <p className='f7 fw5 gray ma0 mt2'>
                      {inning.inningNo === 2
                        ? `(${inning.overNo})`
                        : inning.inningNo > 2
                        ? `(${inning.secondPredictedOVer})`
                        : ''}
                    </p>
                  </div>

                  <div className='flex justify-between items-center w-50 mb3'>
                    <div className='bg-orange br-50' style={{ height: 10, width: 10 }} />
                    <div className='tc ' style={{ height: 1, width: '95%', borderBottom: '1px dashed gray' }}>
                      <img
                        className='ib w2-5 h2 nt3 shadow-4'
                        src={`https://images.cricket.com/teams/${inning.inningIds[1]}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        alt=''
                      />
                      <p className='ma0 mt2 dark-gray f6 fw6'>
                        {inning.inningIds[1] === inning.team1Id ? inning.team1ShortName : inning.team2ShortName}
                      </p>
                    </div>
                    <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                  </div>

                  <div className='tr pv2 w-20'>
                    <p className='oswald f4 fw2 red_10 ma0 cdc'>
                      {inning.inningNo <= 2
                        ? inning.secondPredictedScore +
                          (inning.secondPredictedWicket !== 10 ? '/' + inning.secondPredictedWicket : '')
                        : '--'}
                    </p>
                    {inning.inningNo <= 2 ? (
                      <p className='f6 fw5 gray ma0 red_10 mt2'>({inning.secondPredictedOVer})</p>
                    ) : (
                      ''
                    )}
                  </div>
                </div>
                <div className='divider' />
              </div>
            ))}
            {/* 3rd innings */}
            {data.liveScorePredictor.liveScores[data.liveScorePredictor.liveScores.length - 1].inningIds.length >= 3 &&
              data.liveScorePredictor.liveScores.map((inning, i) => (
                <div key={i}>
                  <p className='fw2 f7 gray   lh-copy pl2 mv0 pv1'>INNINGS 2</p>
                  <div className='bg-white pa3 flex justify-between items-center'>
                    <div className='tl pv2 w-20'>
                      <p className='oswald f4 fw2 ma0'>
                        {inning.inningNo === 3
                          ? inning.currentScore + '/' + inning.currentWickets
                          : inning.inningNo > 3
                          ? inning.thirdPredictedScore +
                            (inning.thirdPredictedWicket !== 10 ? '/' + inning.thirdPredictedWicket : '')
                          : '--'}
                      </p>
                      <p className='f7 fw5 gray ma0 mt2'>
                        {inning.inningNo === 3
                          ? `(${inning.overNo})`
                          : inning.inningNo > 3
                          ? `(${inning.thirdPredictedOver})`
                          : ''}
                      </p>
                    </div>

                    <div className='flex justify-between items-center w-50 mb3'>
                      <div className='bg-orange br-50' style={{ height: 10, width: 10 }} />
                      <div className='tc ' style={{ height: 1, width: '95%', borderBottom: '1px dashed gray' }}>
                        <img
                          className='ib w2-5 h2 nt3 shadow-4'
                          src={`https://images.cricket.com/teams/${inning.inningIds[2]}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          alt=''
                        />
                        <p className='ma0 mt2 dark-gray f6 fw6'>
                          {inning.inningIds[2] === inning.team1Id ? inning.team1ShortName : inning.team2ShortName}
                        </p>
                      </div>
                      <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                    </div>

                    <div className='tr pv2  w-20'>
                      <p className='oswald f4 fw2 red_10 ma0 cdc'>
                        {inning.inningNo <= 3
                          ? inning.thirdPredictedScore > 0
                            ? inning.thirdPredictedScore +
                              (inning.thirdPredictedWicket !== 10 ? '/' + inning.thirdPredictedWicket : '')
                            : '--'
                          : '--'}
                      </p>
                      {inning.inningNo <= 3 ? (
                        <p className='f6 fw5 gray ma0 red_10 mt2'>
                          ({inning.thirdPredictedScore > 0 && inning.thirdPredictedOver})
                        </p>
                      ) : (
                        ''
                      )}
                      
                    </div>
                  </div>
                  <div className='divider' />
                </div>
              ))}
            {/* 4th innings */}
            {data.liveScorePredictor.liveScores[data.liveScorePredictor.liveScores.length - 1].inningIds.length === 4 &&
              data.liveScorePredictor.liveScores.map((inning, i) => (
                <div key={i}>
                
                  <div className='bg-white pa3 flex  justify-between items-center'>
                    <div className='tl pv2 w-20'>
                      <p className='oswald f4 fw2 ma0'>
                        {inning.inningNo === 4 ? inning.currentScore + '/' + inning.currentWickets : '--'}
                      </p>
                      <p className='f7 fw5 gray ma0 mt2'>{inning.inningNo === 4 ? `(${inning.overNo})` : ''}</p>
                    </div>

                    <div className='flex justify-between items-center w-50 mb3'>
                      <div className='bg-orange br-50' style={{ height: 10, width: 10 }} />
                      <div className='tc ' style={{ height: 1, width: '95%', borderBottom: '1px dashed gray' }}>
                        <img
                          className='ib w2-5 h2 nt3 shadow-4'
                          src={`https://images.cricket.com/teams/${inning.inningIds[3]}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                          alt=''
                        />
                        <p className='ma0 mt2 dark-gray f6 fw6'>
                          {inning.inningIds[3] === inning.team1Id ? inning.team1ShortName : inning.team2ShortName}
                        </p>
                      </div>
                      <div className='bg-green br-50' style={{ height: 10, width: 10 }} />
                    </div>

                    <div className='tr pv2 w-20'>
                      <p className='oswald red_10 f4 fw2 ma0 cdc'>
                        {inning.inningNo <= 4
                          ? inning.fourthPredictedScore > 0
                            ? inning.fourthPredictedScore +
                              (inning.fourthPredictedWicket !== 10 ? '/' + inning.fourthPredictedWicket : '')
                            : '--'
                          : '--'}
                      </p>
                      {inning.inningNo <= 4 ? (
                        <p className='f6 fw5 gray ma0 red_10 mt2'>
                          ({inning.fourthPredictedScore > 0 && inning.fourthPredictedOver})
                        </p>
                      ) : (
                        ''
                      )}
                    </div>
                  </div>
                  <div className='divider' />
                </div>
              ))}
            <div className='pa3 ph7-ns'>
              <div className='tc gray bg-orange_2 mv2 pv1 ph3 br4 f7'>
                <span className='fw2 grey_10'>Projection: </span>
                {data.liveScorePredictor.liveScores[0].predictvizMarginView.result === 'tie'
                  ? 'Match to end in a Draw'
                  : `${
                      data.liveScorePredictor.liveScores[0].predictvizMarginView.winnerTeamId ===
                      data.liveScorePredictor.liveScores[0].team1Id
                        ? data.liveScorePredictor.liveScores[0].team1ShortName
                        : data.liveScorePredictor.liveScores[0].team2ShortName
                    } to ${data.liveScorePredictor.liveScores[0].predictvizMarginView.result} by ${
                      data.liveScorePredictor.liveScores[0].predictvizMarginView.runs
                        ? `${data.liveScorePredictor.liveScores[0].predictvizMarginView.runs} ${
                            data.liveScorePredictor.liveScores[0].predictvizMarginView.runs == 1 ? 'run' : 'runs'
                          }`
                        : `${data.liveScorePredictor.liveScores[0].predictvizMarginView.wickets.split('.')[0]} ${
                            data.liveScorePredictor.liveScores[0].predictvizMarginView.wickets.split('.')[0] == 1
                              ? 'wicket'
                              : 'wickets'
                          }`
                    }`}
              </div>
            </div>
          </div>
        ) : (
          <div className='ma4 pb' style={{ margin: 'auto', display: 'block' }}>
            <img
              className='w45-m h45-m w4 h4 w5-l h5-l'
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt='loading...'
            />
          </div>
        )}
      </div>
    );
}
