import React, { useRef, useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
import { format } from 'date-fns';
import Countdown from 'react-countdown-now';
// import PlayerImpact from './playerimpact'
// import RunComparison from './runComparison'
// import GameChangingOver from './gameChanginOver'
// import MatchStat from './matchStat'
// import MatchReel from './matchReel'
import PhasesOfPlay from '../swipeCrclytics/phasesOfPlay';
// import ReportCard from './matchRating'
import FormIndex from './formIndex';
// import MatchUps from '../HomeScreens/matchups'
import MatchUps from '../../matchups';
import RunComparison from '../swipeCrclytics/runComparison';
import ScoringChartCompleted from '../overprojection';
import OverSimulationLive from '../swipeCrclytics/overSimulationLive';
import MatchReel from '../swipeCrclytics/matchReel.js';
// componentV2/swipeCrclytics/matchReel.js
import { EffectCoverflow, Pagination } from 'swiper';
// import Homepage from '..'
export default function index(props) {
  // console.log('propspropspropspropspropspropsprops', props);
  const [paggination, setPaggination] = useState(0);

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  const [swiper, updateSwiper] = useState(null);
  const [currentIndex, updateCurrentIndex] = useState(0);
  const [componentArray, setComponentArray] = useState([]);
  const [didSLideCHange,setSLideChange]=useState(false)
  const [contentArray, setContentArray] = useState([]);


  const router = useRouter();

  const routerTabName = router.query.id.slice(-1)[0];
console.log("router.query.id", router.query.id)
  const indexTell = () => {
    if (routerTabName === 'over-simulation') {
      return '0';
    }
    if (routerTabName === 'run-comparison') {
      return '1';
    }
    if (routerTabName === 'score-projection') {
      return '2';
    }
    if (routerTabName === 'player-matchup') {
      return '3';
    }
    if (routerTabName === 'form-index') {
      return '4';
    }
    if (routerTabName === 'phases-of-play') {
      return '5';
    }
  };

  const Bell = './pngsV2/bell.png';
  const location = './pngsV2/location.png';

  const swiperRef = useRef(null);
  {
    console.log('dd', props.data.matchStatus);
  }
  const components = [
    <OverSimulationLive matchID={props.matchID} matchType={props.matchType} />,
    <RunComparison matchID={props.matchID} matchType={props.matchType} />,
    <MatchReel matchID={props.matchID} matchType={props.matchType} matchStatus={props.data.matchStatus} />,
    <MatchUps matchID={props.matchID} data={props.keystat} matchData={props.data} />,
    <FormIndex matchID={props.matchID} matchType={props.matchType} />,
    <PhasesOfPlay matchID={props.matchID} matchType={props.matchType} />
  ];
  const content = [
    {
      routerTabName: 'over-simulation',
      heading: 'over simulation',
      subHeading: 'Check the simulated team scores based on historical data of past overs.'
    },
    {
      routerTabName: 'run-comparison',
      heading: 'Run Comparison',
      subHeading:'Statistical visualization to depict score progression and fall of wickets against the overs bowled during an innings'
    },
    {
      routerTabName: 'score-projection',
      heading: 'Score Projection',
      subHeading: 'A prediction of the teams final scores based on historical data and our powerful AI'
    },
    {
      routerTabName: 'player-matchup',
      heading: 'Player MatchUp',
      subHeading: 'Select a batter and see how they perform against opposing bowlers'
    },

    {
      routerTabName: 'form-index',
      heading: 'Form Index',
      subHeading: 'An accurate analysis of players from both teams based on their form from recent matches '
    },
    {
      routerTabName: 'phases-of-play',
      heading: 'Phases Of Play',
      subHeading: 'Check how the teams performed in a session by session breakdown'
    }
  ];
  useEffect(() => {
    // parseInt(indexTell());

    const newArrayMAtch = components.splice(parseInt(indexTell()), 6);
    const finalArray = [...newArrayMAtch, ...components];

    setComponentArray(finalArray);
  }, []);



  useEffect(() => {
  //  alert(parseInt(indexTell()));
//  alert(parseInt(indexTell()))
    const newCOntent = content.splice(parseInt(indexTell()), 6);
    console.log("1111111",content)
    console.log("22222",newCOntent)

    const finalContent = [...newCOntent, ...content];

    setContentArray(finalContent);
  }, []);
  // alert(8)


  useEffect(() => {
    setPaggination(parseInt(indexTell()));
    updateCurrentIndex(indexTell());
    if (swiper !== null) {
      try {
        swiper.on('slideChange', () => {
        
         
          setPaggination(swiper.realIndex);
          updateCurrentIndex(swiper.realIndex);
          alert(8)
          // props.updateCurrentIndex(swiper.realIndex);
        });
        swiper.on('click', (swipeData) => {
          console.log(currentIndex);
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        });
      } catch (e) {
        console.log(e);
      }
    }
  }, []);


  
  return (

   
    <div className=' pb-10 mt-16 px-2 pt-2'>
      {console.log("contentArraycontentArraycontentArraycontentArraycontentArray",contentArray,didSLideCHange)}
       {/* {contentArray.length>0&& router.query.id.length>2&&(
        <div className='flex  flex-col justify-between  p-3  capitalize'>
          <div className='text-lg  text-white font-bold '> {contentArray&& contentArray[didSLideCHange? parseInt(paggination):parseInt(paggination)].heading} </div>
          <div className='text-xs text-white font-medium pt-1'>  {contentArray&& contentArray[didSLideCHange? parseInt(paggination):parseInt(paggination)].subHeading} </div>
          <div className='w-12 h-1 bg-blue-8 mt-2'></div>
        </div>
      )} */}

      <Swiper
        // navigation={{
        //   prevEl: navigationPrevRef.current,
        //   nextEl: navigationNextRef.current
        // }}

        shouldSwiperUpdate={true}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={'1'}
        initialSlide={1}
        onSlideChange={(swiper) =>(setPaggination(swiper.realIndex))}
        className='mySwiper   '>
        {componentArray.map((component, i) => {
          return <SwiperSlide>{component}</SwiperSlide>;
        })}
      </Swiper>
      {/* <Add index={props.activeIndex} /> */}
      <div className='h-10 w-full'></div>
      <div className='flex justify-center items-center left-0 right-0 z-max p-2 bg-basebg  fixed bottom-0'>
        {[1, 2, 3, 4, 5, 6].map((item, key) => (
          <div
            className={`w-1.5 h-1.5 base flex ${
              parseInt(paggination) === key ? 'bg-green-3' : 'bg-blue-4 '
            } rounded-full m-2  `}>
            {' '}
          </div>
        ))}
      </div>
    </div>
  );
}
