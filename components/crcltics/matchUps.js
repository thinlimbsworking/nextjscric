import React, { useState, useEffect } from 'react';
import CleverTap from 'clevertap-react';

const matchupPlayer1 = '/pngs/fallbackprojection.png';
const cricket = '/pngs/c.png';
const thunder = '/pngs/thunder.png';
const BatIcon = '/svgs/bat.svg';
const ballIcon = '/svgs/ball.svg';
const playerAvatar = '/pngs/fallbackprojection.png';
const information = '/svgs/information.svg';
const pencil_white = '/svgs/pencil-edit.svg';
const plusRed = '/svgs/plus-red.png';
const ground = '/svgs/groundImageWicket.png';
const batsmen = '/pngs/bat_icon.png';
const bowler = '/pngs/ball_icon.png';
const pencil = '/svgs/pencil.png';

export default function MatchUps({ data, matchData, matchID, browser, ...props }) {
  console.log("datadatadatadatadata",props.data)

  const [list1Flag, setList1Flag] = useState(false);
  const [list2Flag, setList2Flag] = useState(false);
  const [initialPage, setInitialPage] = useState(true);
  const [Player1, setPlayer1] = useState('');
  const [Player2, setPlayer2] = useState('');
  const [matchupPage, setMatchupPage] = useState(true);
  const [infoToggle, setInfoToggle] = useState(false);

  let player1Filter = [];
  let player2Filter = [];



  useEffect(() => {
    setPlayer1(data&&data.matchupsById&&data.matchupsById.matchUpData[0] && data.matchupsById.matchUpData[0].player1);
    setPlayer2(data&&data.matchupsById&&data.matchupsById.matchUpData[0] && data.matchupsById.matchUpData[0].player2);
  }, []);



  if (data && data.matchupsById.matchUpData.length !== 0) {
    player1Filter = Array.from(new Set(data.matchupsById.matchUpData.map((s) => s.player1))).map((id) => {
      return {
        player1: id,
        player1Name: data.matchupsById.matchUpData.find((s) => s.player1 === id).player1Name
      };
    });

    player2Filter = Array.from(new Set(data.matchupsById.matchUpData.map((s) => s.player2))).map((id) => {
      return {
        player2: id,
        player2Name: data.matchupsById.matchUpData.find((s) => s.player2 === id).player2Name
      };
    });
  }

  const selectPlayerList1 = (playerId) => {
    setPlayer1(playerId);
    setInitialPage(false);
    setList1Flag(false);
    setList2Flag(true);
    setMatchupPage(false);
  };

  const selectPlayerList2 = (playerId) => {
    setPlayer2(playerId);
    setMatchupPage(true);
    setInitialPage(false);
    setList1Flag(false);
    setList2Flag(false);
  };

  const setToInitialState = () => {
    setInitialPage(true);
    setList1Flag(false);
    setList2Flag(false);
    setMatchupPage(false);
  };
  const afterClickSelect = () => {
    setList1Flag(true);
    setInitialPage(false);
    setList2Flag(false);
    setMatchupPage(false);
    setPlayer1('');
    setPlayer2('');
  };

  const pencilClicked = () => {
    setInitialPage(false);
    setList1Flag(true);
    setList2Flag(false);
    setMatchupPage(false);
  };

  const setPlayer1OnPlayer2 = (playerId) => {
    setPlayer1(playerId);
    setInitialPage(false);
    setList1Flag(false);
    setList2Flag(false);
    setMatchupPage(true);
  };
  const plusClicked = () => {
    setInitialPage(false);
    setList1Flag(false);
    setList2Flag(true);
    setMatchupPage(false);
  };
  const showPlayerList1 = (playerId) => {
    setPlayer2(playerId);
    setInitialPage(false);
    setList1Flag(true);
    setList2Flag(false);
    setMatchupPage(true);
  };

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  if (!data)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn’t right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  else
    return (
      <div className='mt2'>
        <div className='flex justify-between items-center pa2 ph3-l bg-white '>
          <div className='f7 f5-l fw4 grey_10 ttu'>PLAYER MATCH-UPS</div>
          <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
        </div>
        <div className='outline light-gray' />

        {infoToggle && (
          <div className='f7 grey_8 pa2 bg-white'>
            A direct head-to-head comparison between bat and ball, pitting two players selected by you, against each
            other.
          </div>
        )}
        {data && data.matchupsById.matchUpData.length !== 0 ? (
          <div className=''>
            <div className='bg-navy '>
              <img className='absolute contain z-1 pt2 pl4-l pl2 ' height='150' alt={cricket} src={cricket} />
              <div className='flex justify-between items-center relative mv1 h43 mh4-l'>
                <div>
                  <img className='absolute top-0 left-0 pl1 pt2 z-1 h1 w1' src={BatIcon} alt='' />
                  <img
                    className='absolute z-1 bw0  top-0 left-1  pt2'
                    height='135'
                    width='100'
                    alt=''
                    src={`https://images.cricket.com/players/${Player1}_headshot_safari.png`}
                    onError={(e) => {
                      e.target.src = playerAvatar;
                    }}
                  />
                  <div
                    className='absolute pv2 left-0 bottom-0 shadow-4 z-1 mt2 w-49 w-25-l flex justify-around items-center '
                    style={{ backgroundColor: '#273049' }}
                    onClick={() => (pencilClicked(), setPlayer1(''))}>
                    <span className='ph1 truncate f7 fw5 white'>
                      {Player1 != '' ? player1Filter.find((id) => id.player1 === Player1).player1Name : 'Batsman'}
                    </span>
                    <img src={pencil_white} alt='' className='h07' />
                  </div>
                </div>
                <div>
                  <img src={thunder} className='absolute center left-0 right-0 top-0 bottom-0' />
                  <div className='white f4 fw6'>Vs</div>
                </div>
                <div className=''>
                  <img className='absolute contain z-1 pt2 top-0 right-0' height='150' alt={cricket} src={cricket} />
                  <img className='absolute top-0 right-0 pr1 pt2 z-1 h1 w1' src={ballIcon} alt='' />
                  <img
                    className='absolute z-1 bw0  top-0 right-1  pt2 flip-horizontally'
                    height='135'
                    width='100'
                    alt=''
                    src={`https://images.cricket.com/players/${Player2}_headshot_safari.png`}
                    onError={(e) => {
                      e.target.src = playerAvatar;
                    }}
                  />
                  <div
                    className='absolute  pv2 shadow-4 bottom-0 white z-1 mt2 right-0 w-49 w-25-l flex justify-around items-center'
                    style={{ backgroundColor: '#273049' }}
                    onClick={() => (plusClicked(), setPlayer2(''))}>
                    <img src={pencil_white} alt='' className='h07' />
                    <span className='ph1 truncate f7 fw5 white'>
                      {Player2 !== '' ? player2Filter.find((id) => id.player2 === Player2).player2Name : 'Bowler'}
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div className='bg-white pb2'>
              {/* Filter player1 based on when player2 is not set*/}

              {!initialPage && list1Flag && !list2Flag && Player2 === '' ? (
                <div className='flex-l justify-auto-l flex-wrap-l'>
                  {player1Filter.map((player, i) => (
                    <div
                      key={i}
                      className='cursor-pointer pa1-l ph2 w-20-l ma3-l h4-l ba-l br3-l b--black-60 shadow-4-l grow '
                      onClick={() => selectPlayerList1(player.player1)}>
                      <div className='flex justify-between ph1'>
                        <div className='flex items-center dn-l pv2'>
                          <div className='cover overflow-hidden bg-top mr2 avatarShadow  br-100 w2-3 h2-3'>
                            <img
                              className=' '
                              src={`https://images.cricket.com/players/${player.player1}_headshot_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = matchupPlayer1)}
                            />
                          </div>
                          <div className='f6 grey_10 fw5'>{player.player1Name}</div>
                        </div>
                        <div className='flex items-center justify-center dn-l'>
                          <img className='w13 h13' src={batsmen} alt={player.player1Role} />
                        </div>
                      </div>
                      <div className='dn db-l flex justify-center'>
                        <div className='br-50 w2-6 h2-6 overflow-hidden bg-white ba b--grey_7 dn db-l tc center mt3'>
                          <img
                            className='contain'
                            src={`https://images.cricket.com/players/${player.player1}_headshot_safari.png`}
                            alt=''
                            onError={(evt) => (evt.target.src = matchupPlayer1)}
                          />
                        </div>
                        <div className='f6 tc black pt2 fw3 dn db-l'>{player.player1Name}</div>
                      </div>

                      <div className='divider dn-l' />
                    </div>
                  ))}
                </div>
              ) : (
                ''
              )}
              {/* Filter player1 based on when player2 is set*/}
              {!initialPage && list1Flag && !list2Flag && Player2 !== '' ? (
                <div className='flex-l justify-auto-l flex-wrap-l'>
                  {data.matchupsById.matchUpData
                    .filter((x, i) => x.player2 === Player2)
                    .map((player, j) => (
                      <div
                        key={j}
                        className=' cursor-pointer pa1-l ph2 w-20-l ma3-l h4-l ba-l br3-l b--black-60 shadow-4-l grow'
                        onClick={() => setPlayer1OnPlayer2(player.player1)}>
                        <div className='flex justify-between ph1'>
                          <div className='flex items-center dn-l pv2'>
                            <div className='cover overflow-hidden bg-top mr2 avatarShadow br-100 w2-3 h2-3'>
                              <img
                                className=' '
                                src={`https://images.cricket.com/players/${player.player1}_headshot_safari.png`}
                                alt=''
                                onError={(evt) => (evt.target.src = matchupPlayer1)}
                              />
                            </div>
                            <div className='f6 grey_10 fw5'>{player.player1Name}</div>
                          </div>
                          <div className='flex items-center justify-center dn-l'>
                            <img className='w13 h13' src={batsmen} alt={player.player1Role} />
                          </div>
                        </div>
                        <div className='dn db-l flex justify-center'>
                          <div className='br-50 w2-6 h2-6 overflow-hidden bg-white ba b--grey_7 dn db-l tc center mt3'>
                            <img
                              className='contain'
                              src={`https://images.cricket.com/players/${player.player1}_headshot_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = matchupPlayer1)}
                            />
                          </div>
                          <div className='f6 tc black pt2 fw3 dn db-l'>{player.player1Name}</div>
                        </div>
                        <div className='divider dn-l' />
                      </div>
                    ))}
                </div>
              ) : (
                ''
              )}

              {/* Filter player2 based on when player1 is set*/}

              {!initialPage && list2Flag && !list1Flag && Player1 !== '' ? (
                <div className='overflow-scroll flex-l justify-auto-l flex-wrap-l'>
                  {data.matchupsById.matchUpData
                    .filter((id) => id.player1 === Player1)
                    .map((player, i) => (
                      <div
                        key={i}
                        className=' cursor-pointer pa1-l ph2 w-20-l ma3-l h4-l ba-l br3-l b--black-60 shadow-4-l grow'
                        onClick={() => selectPlayerList2(player.player2)}>
                        <div className='flex justify-between ph1'>
                          <div className='flex items-center pv2 dn-l'>
                            <div className='cover overflow-hidden bg-top mr2 avatarShadow bg-grey_10 br-100 w2-3 h2-3'>
                              <img
                                className=' '
                                src={`https://images.cricket.com/players/${player.player2}_headshot_safari.png`}
                                alt=''
                                onError={(evt) => (evt.target.src = matchupPlayer1)}
                              />
                            </div>
                            <div className='f6 grey_10 fw5'>{player.player2Name}</div>
                          </div>
                          <div className='flex items-center justify-center dn-l'>
                            <img className='w13 h13' src={bowler} alt={player.player2Role} />
                          </div>
                        </div>
                        <div className='dn db-l flex justify-center'>
                          <div className='br-50 w2-6 h2-6 overflow-hidden bg-white ba b--grey_7 dn db-l tc center mt3'>
                            <img
                              className='contain'
                              src={`
																https://images.cricket.com/players/${player.player2}_headshot_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = matchupPlayer1)}
                            />
                          </div>
                          <div className='f6 tc black pt2 fw3 dn db-l'>{player.player2Name}</div>
                        </div>
                        <div className='divider dn-l' />
                      </div>
                    ))}
                </div>
              ) : (
                ''
              )}
              {/* Filter player2 based on when player1 is not set*/}
              {!initialPage && list2Flag && !list1Flag && Player1 === '' ? (
                <div className='overflow-scroll flex-l justify-auto-l flex-wrap-l'>
                  {player2Filter.map((player, i) => (
                    <div
                      key={i}
                      className='cursor-pointer pa1-l ph2 w-20-l ma3-l h4-l ba-l br3-l b--black-60 shadow-4-l grow'
                      onClick={() => showPlayerList1(player.player2)}>
                      <div className='flex justify-between ph1'>
                        <div className='flex items-center pv2 dn-l'>
                          <div className='cover overflow-hidden bg-top mr2 avatarShadow bg-grey_10 br-100 w2-3 h2-3'>
                            <img
                              className=' '
                              src={`https://images.cricket.com/players/${player.player2}_headshot_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = matchupPlayer1)}
                            />
                          </div>
                          <div className='f6 grey_10 fw5'>{player.player2Name}</div>
                        </div>
                        <div className='flex items-center justify-center dn-l'>
                          <img className='w13 h13' src={bowler} alt={player.player2Role} />
                        </div>
                      </div>
                      <div className='dn db-l flex justify-center'>
                        <div className='br-50 w2-6 h2-6 overflow-hidden bg-white ba b--grey_7 dn db-l tc center mt3'>
                          <img
                            className='contain'
                            src={`https://images.cricket.com/players/${player.player2}_headshot_safari.png`}
                            alt=''
                            onError={(evt) => (evt.target.src = matchupPlayer1)}
                          />
                        </div>
                        <div className='f6 tc black pt2 fw3 dn db-l'>{player.player2Name}</div>
                      </div>
                      <div className='divider dn-l' />
                    </div>
                  ))}
                </div>
              ) : (
                ''
              )}

              {/* show matchups List when both player1 and player2 are both set */}

              {Player1 !== '' && Player2 !== '' && matchupPage
                ? data.matchupsById.matchUpData
                    .filter((player, i) => player.player1 === Player1 && player.player2 === Player2)
                    .map((matchup, j) => (
                      <div key={j} className='bg-white'>
                        <div className='gray f8 f6-l fw2 pa2'>Last 5 Years</div>
                        <div className='flex justify-between ph2 ph6-l'>
                          <div className='f6 shadow-4 w-30 pv3 tc'>
                            <div className='f2-5 fw5' style={{ color: '#C63935' }}>
                              {matchup.ballsFaced}
                            </div>
                            <div className='f7 fw4 pt1'>Balls Bowled</div>
                          </div>
                          <div className='shadow-4  pv3 f6 w-30 tc'>
                            <div className='f2-5 fw5' style={{ color: '#C63935' }}>
                              {matchup.Dismissals}
                            </div>
                            <div className='f7 fw4 pt1'>Dismissals</div>
                          </div>
                          <div className='shadow-4 pv3 f6 w-30 tc'>
                            <div className='f2-5 fw5' style={{ color: '#C63935' }}>
                              {matchup.runsScored}
                            </div>
                            <div className='f7 fw4 pt1'>Runs</div>
                          </div>
                        </div>
                        <div className='flex justify-between items-center pa3 f6 ph6-l'>
                          <div>Batting S R</div>
                          <div className='oswald'>
                            {matchup.battingSR === 'NA' ? matchup.battingSR : parseFloat(matchup.battingSR)}
                          </div>
                        </div>
                        <div className='divider' />
                        <div className='flex justify-between items-center pa3 f6 ph6-l'>
                          <div>Bowling S R</div>
                          <div className='oswald'>
                            {matchup.bowlingSR === 'NA' ? matchup.bowlingSR : parseFloat(matchup.bowlingSR)}
                          </div>
                        </div>
                      </div>
                    ))
                : ''}
              {matchupPage && (
                <div
                  className='fw6 f6 cdcgr pa3 tc w-70 white tracked cursor-pointer ttu center br-pill mv2  '
                  onClick={afterClickSelect}>
                  Other matchups
                </div>
              )}
            </div>
          </div>
        ) : (
          <div className='bg-white w-100 shadow-4 pa4'>
            <img
              className='w45-m h45-m w4 h4 w5-l h5-l  '
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt='loading...'
            />
            <div className='f5 fw5 tc pt2'>Data Not Available</div>
          </div>
        )}
      </div>
    );
}
