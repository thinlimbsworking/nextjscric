import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { PLAYER_INDEX } from '../../api/queries'
import DataNotFound from '../../components/commom/datanotfound'
export default function PlayerIndex({ browser, ...props }) {
  const { error, loading, data } = useQuery(PLAYER_INDEX)

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0)
    }
  }, [])

  if (error)
    return (
      <div className="   mt-5">
        <DataNotFound />
      </div>
    )
  if (loading)
    return (
      <div className="   mt-5">
        <DataNotFound />
      </div>
    )
  else
    return (
      <div className=" ">
        <div className="flex items-center justify-start p-3 fixed bg-basebg w-full top-0 z-10">
          <div
            className="p-3 bg-gray rounded-md "
            onClick={() => window.history.back()}
          >
            <img
              className=" flex items-center justify-center h-4 w-4 rotate-180"
              src="/svgsV2/RightSchevronWhite.svg"
              alt=""
            />
          </div>
          <div className="flex justify-center items-center text-lg font-semibold text-white pl-3">
            <div className="">Criclytics</div>
            <span className="text-[8px] -mt-2">TM</span>
          </div>{' '}
        </div>
        <div className="mt-14  mx-1 p-2 ">
          <div className="flex flex-column justify-start items-start ">
            <div className="flex text-lg white font-bold">
              Most Valuable Players
            </div>
            <div className="flex w-full text-sm white font-medium mt-1 leading-4 tracking-wide text-white/80">
              The batsmen who made short work of the hude target, the bowlers
              who acted as partnership-breakers - here's a list of players whose
              consistent performances lit up the series.
            </div>
          </div>
          <div className="bg-blue-8 w-12 h-1 mt-2"></div>

          <div className="flex  mt-3 mb-2 justify-between items-center font-semibold  ">
            <div className=" text-gray-2 uppercase  text-xs">Player</div>

            <div className="uppercase text-gray-2 text-xs">Rank</div>
          </div>

          {data &&
          data.playerIndex &&
          data.playerIndex.playerObject.length > 0 ? (
            <div className="">
              {data.playerIndex.playerObject.map((player, i) => (
                <>
                  <div
                    key={i}
                    className={`flex  mb-[2px] p-2  justify-between items-center ${
                      i < 3 ? 'bg-gray rounded' : 'border-gray border-b'
                    } `}
                  >
                    <div className="flex justify-start items-center">
                      <div className=" relative bg-basebg  rounded-full">
                        <img
                          className="h-12 w-12  object-top object-cover rounded-full "
                          src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />
                      </div>
                      <div className=" ml-2">
                        <div className="flex items-center  justify-start text-white  text-sm font-semibold">
                          <div className=" ">{player.playerName} </div>
                          <div className="font-medium text-xs pl-2 text-white/80">
                            ({player.playerTeamName})
                          </div>
                        </div>
                        <div className="font-medium  text-white text-xs mt-1">
                          {player.totalPoints} Points{' '}
                        </div>
                      </div>
                    </div>

                    <div
                      className={`  ${
                        i < 3 ? 'bg-basebg' : 'bg-gray'
                      }  rounded flex justify-center items-center text-white w-11 h-10 font-semibold`}
                    >
                      {' '}
                      {i + 1}{' '}
                    </div>
                  </div>
                </>
              ))}
            </div>
          ) : (
            <div className=" flex  mt-5">
              <DataNotFound />
            </div>
          )}
        </div>
      </div>
    )
}
