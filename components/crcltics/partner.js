import React, { useState } from 'react';

import PartnetshipScore from './partnetshipscore';
import { useQuery } from '@apollo/react-hooks';
import { GET_PARTNERSHIP_BY_MATCH_ID } from '../../api/queries';
const FLAG = '/svgs/i.png';
const information = '/svgs/information.svg';
export default function Partner(props) {
  const ground = '/svgs/groundImageWicket.png';

  const [infoToggle, setInfoToggle] = useState(false);

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  return props.data &&
    props.data.getpartnerShipPrediction &&
    props.data.getpartnerShipPrediction.partnerShipData &&
    props.data.getpartnerShipPrediction.partnerShipData.length !== 0 ? (
    <div className='bg-white'>
      <div className='mt2 mb3'>
        <div className='flex justify-between items-center pa2 ph3-l bg-white '>
          <h1 className='f7 f5-l fw5 grey_10 ttu'>partnership predictor</h1>
          <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
        </div>

        <div className='outline light-gray' />
        {infoToggle && (
          <div className='f7 grey_8 pa2 bg-white'>
            The pair in the middle determine how far the team can go. But how far can the pair go? Check it out.
          </div>
        )}
        <div className='flex ml2 justify-between items-center pa2 ph3-l bg-white '>
          <div>
            {' '}
            <img
              src={`https://images.cricket.com/teams/${props.data.getpartnerShipPrediction.teamID}_flag_safari.png`}
              alt=''
              className='h1 w20 shadow-4 mt1'
            />
            <span className='ml2 f5 fw5'>{props.data.getpartnerShipPrediction.teamShortName}</span>
            <span className='ml3 f5 fw5'>
              {props.data.getpartnerShipPrediction.currentRuns}/{props.data.getpartnerShipPrediction.currentWickets}{' '}
              <span className='f7 fw3'>{`(${props.data.getpartnerShipPrediction.currentOver})`}</span>{' '}
            </span>
          </div>

          <div
            className='inline-flex ph2 pv1 items-center 
         ba b--black-40 br-pill  right-1 top-1'>
            <span className='bg-green h04 w04 br-100 mr1'></span>
            <h2 className='f8 fw5 black-40 '>LIVE</h2>
          </div>
        </div>

        {props.data &&
          props.data.getpartnerShipPrediction &&
          props.data.getpartnerShipPrediction.partnerShipData &&
          props.data.getpartnerShipPrediction.partnerShipData.map((item, index) => {
            return (
              <>
                {index >= props.data.getpartnerShipPrediction.partnerShipData.length - 1 ? (
                  <PartnetshipScore
                    index={index}
                    prediction={props.data.getpartnerShipPrediction.prediction}
                    active={true}
                    data={item}
                  />
                ) : (
                  <PartnetshipScore
                    index={index}
                    active={false}
                    data={item}
                    prediction={props.data.getpartnerShipPrediction.prediction}
                  />
                )}
              </>
            );
          })}
      </div>
    </div>
  ) : (
    <div className='bg-white w-100 mt2 shadow-4 pa4'>
      <img
        className='w45-m h45-m w4 h4 w5-l h5-l  '
        style={{ margin: 'auto', display: 'block' }}
        src={ground}
        alt='loading...'
      />
      <div className='f5 fw5 tc pt2'>Data Not Available</div>
    </div>
  );
}
