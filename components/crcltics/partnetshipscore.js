import React from 'react';

export default function Partnetshipscore(props) {
  return (
    <div>
      {!props.active && (
        <div className='flex flex-column pa3 '>
          <div className='flex w-80'>
            <div className='w-50  fw3 f7 f6-l ml3 '>{props.data.player1Name}</div>
            <div className='w-50 tr  fw3 f7 f6-l mr-4 ml0-l'> {props.data.player2Name} </div>
          </div>

          <div className='flex w-100  items-center  nt2 nt3-l'>
            <b className='mr2 f7 nt2'>{props.index + 1}</b>{' '}
            <div className='h1 relative bg-yellow w-40 tr  ' style={{ background: '#A1A4AC' }}>
              <div
                className='absolute h1  right-0  
         w-40'
                style={{ background: '#D87774', width: `${props.data.player1Percentage}%` }}>
                <div className='absolute right-0 mr2 white'>{props.data.player1Runs}</div>
              </div>
            </div>
            <div className='relative bg-yellow w-40 tr h1  ' style={{ background: '#A1A4AC' }}>
              <div
                className='absolute  h1  tl 
        '
                style={{ background: '#5B5F65', width: `${props.data.player2Percentage}%` }}>
                <div className='ml2 white'>{props.data.player2Runs}</div>
              </div>
            </div>
            <div
              className=' flex items-center justify-center
              h2-4-l ml1   ml4-l f6 f5-l tc ba w-20 w-12-l  pv2  '>
              <div className='fw5'>{props.data.totalRuns} </div>
              <div className='f8 mt1'>{`(${props.data.totalBalls})`} </div>
            </div>
          </div>
        </div>
      )}

      {props.active && (
        <div className='flex flex-column pa3  shadow-1'>
          <div className='flex w-80'>
            <div className='w-50  fw3 f7 f6-l ml3 '>{props.data.player1Name}</div>
            <div className='w-50 tr  fw3 f7 f6-l mr-4 ml0-l'> {props.data.player2Name} </div>
          </div>

          <div className='flex w-100  items-center nt2 nt3-l '>
            <b className='f7'>{props.index + 1}</b>{' '}
            <div className='relative bg-yellow w-40 tr ml1 h1' style={{ background: '#A1A4AC' }}>
              <div
                className='absolute   right-0  
         bg-pink  h1'
                style={{ background: '#C63935', width: `${props.data.player1Percentage}%` }}>
                <span className='absolute right-0 mr2 white'>{props.data.player1Runs}</span>
              </div>
            </div>
            <div className='relative bg-yellow w-40 tr h1 ' style={{ background: '#A1A4AC' }}>
              <div
                className='absolute h1  tl 
        '
                style={{ background: '#172132', width: `${props.data.player2Percentage}%` }}>
                <span className=' white ml2'>{props.data.player2Runs}</span>
              </div>
            </div>
            {/* <div className="  h2-4-l ml1   ml4-l pa2 f6 f5-l tc ba b--green w-20 w-12-l pa "> 
          {props.data.totalRuns} *
           <span className="f7 ">{`(${props.data.totalBalls})`}</span>  
           <span class=" bg-green h04 w04 br-100 mr1 absolute  nt1  ml3-l  "></span>
        
         
      
      

           </div> */}
            <div
              className=' flex items-center ba b--green justify-center
              h2-4-l ml1  ml4-l f6 f5-l tc ba w-20 w-12-l  pv2  '>
              <div className='fw5'>{props.data.totalRuns} * </div>
              <div className='f8 mt1'>
                {`(${props.data.totalBalls})`}
                <span class=' bg-green h04 w04 br-100 mr1 absolute  nt1  ml3-l  '></span>{' '}
              </div>
            </div>
          </div>

          <div className='flex items-center justify-center flex-column'>
            <div
              className=' pa3-l pa2  br4 f6 fw6 tc bg-orange_2_1 
               mt3 '>
              PARTNERSHIP PREDICTED
            </div>
            <div className='mt2 red_10 fw6 f4'>{props.prediction}</div>
            <div className='m1'>Runs</div>
          </div>
        </div>
      )}
    </div>
  );
}
