import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
const information = '/svgs/information.svg';
import { PLAYER_INDEX } from '../../api/queries';
const ground = '/svgs/groundImageWicket.png';
export default function PlayerIndex({ browser, ...props }) {
  const { error, loading, data } = useQuery(PLAYER_INDEX);
  const [infoToggle, setInfoToggle] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0);
    }
  }, []);
  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  if (error) return <div></div>;
  if (loading) return <div></div>;
  else
    return (
      <div className='mw75-l center'>
        <div className='bg-white mv2 shadow-4'>
          <div className='flex justify-between items-center pa2'>
            <div className='f7 f5-l fw4 grey_10 ttu'>Most Valuable Players</div>
            <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
          </div>
          <div className='outline light-gray' />
          {infoToggle && (
            <div className='bg-white'>
              <div className='f7 grey_8 pa2'>
                The batsmen who made short work of the huge target, the bowlers who acted as partnership-breakers -
                here's a list of players whose consistent performances lit up the series.{' '}
              </div>
              <div className='outline light-gray' />
            </div>
          )}
           {data && data.playerIndex&& data.playerIndex.playerObject.length > 0 ? (
            <div className='flex-l flex-wrap'>
              {data.playerIndex.playerObject.map((player, i) => (
                <div
                  key={i}
                  className='shadow-5 flex justify-between bg-white bl bw3 br2 w-95 center mv3 ph2 pt2 w-45-l'
                  style={{ borderLeftColor: `${player.teamColor}` }}>
                  <div className='f4 fw7 w-10 flex justify-center items-center'>{i + 1}</div>
                  <div className='w-20'>
                    <img
                      className=''
                      height='100'
                      alt=''
                      src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                      onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                    />
                  </div>
                  <div className='w-50 flex items-center ph1'>
                    <div className=''>
                      <div className='fw6 f5 pv1'>{player.playerName}</div>
                      <div className='gray fw5 f7 pv1'>{player.playerTeamName}</div>
                    </div>
                  </div>
                  <div className='w-20 flex items-center ph1'>
                    <div className=''>
                      <div className='fw5 f6 pv1'>{player.totalPoints}</div>
                      <div className='gray fw5 f7 pv1'>Points</div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : (
              <div className=' bg-white pv3'>
                <div>
                  <img
                    className='w45-m h45-m w4 h4 w5-l h5-l'
                    style={{ margin: 'auto', display: 'block' }}
                    src={ground}
                    alt='loading...'
                  />
                </div>
                <div className='tc pv2 f5 fw5'>Data Not Available</div>
              </div>
            )}
        </div>
      </div>
    );
}
