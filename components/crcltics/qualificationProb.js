import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import { QUALIFICATION_PROBABILITY } from '../../api/queries';
const play = '/pngsV2/playcric.png';
const pause = '/pngsV2/pausebutton.png';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const ground = '/svgs/groundImageWicket.png';
const Handle = Slider.Handle;
import DataNotFound from '../commom/datanotfound';

const handle = ({ value, dragging, index, ...restProps }) => {
  return (
    // <Tooltip prefixCls='rc-slider-tooltip' overlay={value + 1} visible={dragging} placement='top' key={index}>
      <Handle value={value} {...restProps} />
    // </Tooltip>
  );
};
export default function QualificationProb({ series, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false);
  const [index, setindex] = useState(0);
  const [match, setMatchNo] = useState(0);
  const [click, setclick] = useState(false);
  const [button, setbutton] = useState(false);
  const { error, loading, data } = useQuery(QUALIFICATION_PROBABILITY);
  // console.log("data.getQualificationProbability",data.getQualificationProbability)
  useEffect(() => {
    if (data && data.getQualificationProbability && data.getQualificationProbability.probabilityArray) {
      setMatchNo(data.getQualificationProbability.probabilityArray[index].length - 1);
    }
    if (typeof window !== 'undefined') {
      window.scrollTo(0, 0);
    }
  }, [data]);
  let mytime = 0;
  click &&
    (mytime = setTimeout(function () {
      match === data.getQualificationProbability.probabilityArray[index].length - 1
        ? (clearTimeout(mytime), setclick(!click), setbutton(false))
        : setMatchNo(match + 1);
    }, 1000));
 
  if (error || loading || !data)
    return (
      <div className=' flex  mt-5'>
      <DataNotFound />
    </div>
    );
 
  else
    return (
      <div className='w-full '>
        <div className='flex items-center justify-start p-3 fixed bg-basebg w-full top-0'>
          <div className='p-3 bg-gray rounded-md ' onClick={() => window.history.back()}>
            <img
              className=' flex items-center justify-center h-4 w-4 rotate-180'
              src='/svgsV2/RightSchevronWhite.svg'
              alt=''
            />
          </div>

          <div className='text-lg font-semibold text-white pl-3'>Criclytics</div>
        </div>
        <div className='mx-3 mt-16'>
          <div className=''>
            <div className='flex text-lg white font-bold'>Qualification Probability</div>
            <div className='flex w-full text-sm  font-medium mt-1 leading-4 tracking-wide text-white/80'>
              The changes in qualification fortunes, across the tenure of the tournament, presented as a snapshot.
            </div>
          </div>
          <div className='bg-blue-8 w-12 h-1 mt-2'></div>
        </div>

        <div className='flex justify-center items-center mt-5 text-white font-semibold text-md '>
          <div className=''>Match</div>
          <div className='pl-1'>{match}</div>
        </div>

        <div className='flex mx-3 mt-3 rounded-lg  bg-gray p-3'>
            {data.getQualificationProbability ? (
              <div className=' w-full  '>
                  {data.getQualificationProbability.probabilityArray.length > 1 && (
                      <div className='flex  w-full items-center justify-center'>
                        {data &&
                          data.getQualificationProbability &&
                          data.getQualificationProbability.probabilityArray.length > 0 &&
                          data.getQualificationProbability.probabilityArray.map((group, i) => (
                            <>
                              <div
                                className={`w-1/2 py-2 ${i === index ? 'border-b border-green' : ''} border`}
                                key={i}
                                onClick={() => {
                                  setindex(i);
                                  setMatchNo(data.getQualificationProbability.probabilityArray[index].length - 1);
                                }}>
                                <div className=' flex  items-center justify-center  w-100'>
                                  <div className={` f6 ${i === index ? 'black fw6' : 'gray fw5'}`}>Group {i + 1} </div>
                                </div>
                              </div>
                            </>
                          ))}
                      </div>
                  )}
                  {/* <div className='dn db-l pa2 '>
                    <div className=' flex items-center justify-between'>
                      <img
                        alt='pause'
                        src={button ? pause : play}
                        className='h2-4 w2-4'
                        onClick={() => (
                          setclick(!click),
                          match === data.getQualificationProbability.probabilityArray[index].length - 1
                            ? setMatchNo(0)
                            : '',
                          setbutton(!button)
                        )}
                      />
                      <div className='w-90 flex justify-between items-center pl2'>
                        <div className='f9 f7-l nowrap '>Match 0</div>
                        <div className='w-80 mh4'>
                          <Slider
                            className='slider-main'
                            max={data.getQualificationProbability.probabilityArray[index].length - 1}
                            min={0}
                            step={1}
                            value={match}
                            onChange={(val) => {
                              if (
                                val !== data.getQualificationProbability.probabilityArray[index].length &&
                                val <= data.getQualificationProbability.probabilityArray[index].length
                              ) {
                                setbutton(false);
                                setclick(false);
                                setMatchNo(val);
                              }
                            }}
                            handle={handle}
                            handleStyle={[
                              {
                                backgroundColor: 'white',
                                border: 'white',
                                width: '18px',
                                height: '18px',
                                marginTop: '-8px',
                                boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
                              }
                            ]}
                            trackStyle={[{ backgroundColor: '#a70e13', height: '5px' }]}
                            railStyle={{
                              backgroundColor: '#e8ebf3',
                              height: '5px'
                            }}
                          />
                        </div>
                        <div className='f9 f7-l nowrap'>
                          Match {data.getQualificationProbability.probabilityArray[index].length - 1}
                        </div>
                      </div>
                    </div>
                  </div> */}

                  <div className='rounded-xl bg-basebg p-2 text-white'>
                    {data &&
                      data.getQualificationProbability &&
                      data.getQualificationProbability.qpTeamList.length > 0 &&
                      data.getQualificationProbability.qpTeamList[index].map((tname, i) => (
                        <div key={i} className='flex py-2 justify-between items-center '>
                          <div className='text-xs font-semibold w-2/12'>{tname.teamName}</div>
                          <div className='flex w-8/12 items-center '>
                            {/* {console.log("matchmatch",match)} */}
                            <div className={`${i%2 !== 0 ? 'bg-blue-8':'bg-green'} rounded  poll h-1.5 `}
                              style={{
                                width: `${
                                  data &&
                                  data.getQualificationProbability &&
                                  data.getQualificationProbability.probabilityArray.length > 0 &&
                                  data.getQualificationProbability.probabilityArray[index] &&
                                  data.getQualificationProbability.probabilityArray[index][match] &&
                                  data.getQualificationProbability.probabilityArray[index][match][i] &&
                                  data.getQualificationProbability.probabilityArray[index][match][i]
                                }%`,
                                
                              }}
                             ></div>
                               </div>
                            <div className='text-right w-2/12' >
                              <span className={`${i%2 !== 0 ? 'text-blue-8':'text-white'} text-sm font-medium `}>
                                {data &&
                                data.getQualificationProbability &&
                                data.getQualificationProbability.probabilityArray.length > 0 &&
                                data.getQualificationProbability.probabilityArray[index] &&
                                data.getQualificationProbability.probabilityArray[index][match] &&
                                data.getQualificationProbability.probabilityArray[index][match][i] &&
                                data &&
                                data.getQualificationProbability.probabilityArray[index][match][i] === 100
                                  ? 'Q'
                                  : data &&
                                    data.getQualificationProbability &&
                                    data.getQualificationProbability.probabilityArray.length > 0 &&
                                    data.getQualificationProbability.probabilityArray[index] &&
                                    (data.getQualificationProbability.probabilityArray[index][match] &&
                                    data.getQualificationProbability.probabilityArray[index][match][i] &&
                                    data.getQualificationProbability.probabilityArray[index][match][i]
                                      ? data.getQualificationProbability.probabilityArray[index][match][i]
                                      : 0) + '%'}
                              </span>
                            </div>
                         
                        </div>
                      ))}
                  </div>
                  {/* <div className='flex justify-end pv2 ph3 f8 fw6 white'>{data.getQualificationProbability.text}</div> */}
                  <div className='  flex  w-full  items-center mt-3 '>
                    <div className='flex w-2/12 '>
                      <img
                        alt='pause'
                        src={button ? pause : play}
                        className='h-10 w-10'
                        onClick={() => (
                          setclick(!click),
                          match === data.getQualificationProbability.probabilityArray[0].length - 1
                            ? setMatchNo(0)
                            : '',
                          setbutton(!button)
                        )}
                      />
                    </div>

                    <div className='w-10/12 flex h-10 bg-basebg justify-center items-center rounded-md'>
                      <div className='w-full mx-2'>
                        <Slider
                          className='slider-main w-full'
                          max={data.getQualificationProbability.probabilityArray[0].length - 1}
                          min={0}
                          step={1}
                          value={match}
                          onChange={(val) => {
                            if (
                              val !== data.getQualificationProbability.probabilityArray[0].length &&
                              val <= data.getQualificationProbability.probabilityArray[0].length
                            ) {
                              setbutton(false);
                              setclick(false);
                              setMatchNo(val);
                            }
                          }}
                          handle={handle}
                          handleStyle={[
                            {
                              backgroundColor: 'white',
                              border: 'white',
                              width: '18px',
                              height: '18px',
                              marginTop: '-7px',
                              marginLeft:'1px'
                             
                            }
                          ]}
                          trackStyle={[{ backgroundColor: '#8C98B0', height: '5px' }]}
                          railStyle={{
                            backgroundColor: '#2B323F',
                            height: '5px'
                          }}
                        />
                      </div>
                    </div>
                  </div>
                  <div className='flex justify-between items-center text-xs font-medium text-gray-2 mt-2'>
                    <div className='w-2/12'></div>
                    <div className='w-10/12 flex justify-between items-center'>

                    <div>Match 0</div>
                    <div>Match {data.getQualificationProbability.probabilityArray[0].length -1}</div>
                    </div>

                  </div>
              </div>
            ) : (
              <div className='bg-gray pv3 '>
                <div>
                  <img
                    className='w45-m h45-m w4 h4 w5-l h5-l'
                    style={{ margin: 'auto', display: 'block' }}
                    src={ground}
                    alt='loading...'
                  />
                </div>
                <div className='tc pv2 f5 fw5 white'>Data Not Available</div>
              </div>
            )}
        </div>
      </div>
    );
}
