import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { MATCH_RATINGS } from '../../../api/queries'
import CleverTap from 'clevertap-react'
import DataNotavaible from '../../commom/datanotfound'
import Heading from '../../commom/heading'
const location = '/svgs/location-icon-color.png'
const information = '/svgs/information.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const ground = '/svgs/groundImageWicket.png'
const playerAvatar = '/placeHodlers/playerAvatar.png'
const bat = '/pngsV2/battericon.png'
const ball = '/pngsV2/bowlericon.png'
export default function MatchRating({ browser, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false)
  const [showRatings, setShowRatings] = useState(true)
  const [indexNo, setindexNo] = useState(0)
  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }

  const { error, loading, data } = useQuery(MATCH_RATINGS, {
    variables: { matchID: props.matchID },
  })
  useEffect(() => {
    // if (data && data.matchRatings) {
    //   CleverTap.initialize('Criclytics', {
    //     Source: 'Slider',
    //     MatchID: props.matchID,
    //     TeamAID: data.matchRatings.team1ID || '',
    //     TeamBID: data.matchRatings.team2ID || '',
    //     MatchFormat: props.matchData[0].matchType,
    //     MatchStatus: 'Completed',
    //     CriclyticsEngine: 'CompletedMR',
    //     Platform: localStorage.Platform
    //   });
    // }
  }, [])
  if (error) return <div></div>
  if (loading) return <div></div>
  else
    return (
      <div className="mx-3 pb-7  overflow-y-auto md:max-h-[60rem] hidescroll">
        <div className="mb-5">
          <Heading
            heading={'Report Card'}
            subHeading={' A comprehensive view of a players impact on the game'}
          />
          {/* <div className='text-md  text-white tracking-wide font-bold '> Report Card </div>
              <div className='text-xs text-white pt-1 font-medium tracking-wide'>
                {' '}
                A comprehensive view of a player's impact on the game{' '}
              </div>
              <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        </div>
        {data && data.matchRatings && data.matchRatings.finalArray ? (
          <div className=" ">
            <div className="md:flex flex-wrap justify-around">
              {data.matchRatings.finalArray.map((plr, y) => (
                <div className="my-2 rounded-md bg-gray-4 md:w-5/12 " key={y}>
                  <div
                    className={`flex items-center justify-start shadow-4 ${
                      showRatings && indexNo === y ? ' p-2' : 'p-2'
                    }`}
                    onClick={() => (setShowRatings(true), setindexNo(y))}
                  >
                    <div className="w-2/12 flex justify-start">
                      <div className="flex relative items-center justify-center  ">
                        <img
                          className="h-14 w-14 bg-basebg  object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${plr.playerID}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />

                        <img
                          className=" absolute h-4 w-4  border-1  left-0 bottom-0"
                          src={
                            plr.playerRole == 'Batsman'
                              ? bat
                              : plr.playerRole == 'Bowler'
                              ? ball
                              : ball
                          }
                        />
                        <div className="w-4 h-4  absolute right-0 bottom-0 flex justify-center items-center  rounded-full border-[1px]">
                          <img
                            className=" rounded-full "
                            src={`https://images.cricket.com/teams/${plr.playerTeamID}_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/pngsV2/flag_dark.png')
                            }
                          />
                        </div>
                      </div>
                    </div>
                    <div className="w-8/12 px-2 ">
                      <div className="text-sm font-medium text-white flex items-center ">
                        {plr.playerName}
                      </div>
                      {
                        <div className=" text-gray-2 text-xs">
                          <div className="flex  items-center  ">
                            <div className=" ">Batting Contribution</div>
                            <div className="pl-1 font-medium">
                              - {plr.battingPoints}
                            </div>
                          </div>
                          <div className="flex   items-center  ">
                            <div className="  ">Bowling Contribution</div>
                            <div className="pl-1 font-medium">
                              - {plr.bowlingPoints}
                            </div>
                          </div>
                        </div>
                      }
                    </div>

                    <div className="w-2/12  flex   items-center justify-end ">
                      <div className="bg-basebg rounded-xl text-md font-medium flex items-center p-2 justify-center">
                        {plr.totalPoints}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <div className="  py-3 mt-2">
            <DataNotavaible />
          </div>
        )}
      </div>
    )
}
