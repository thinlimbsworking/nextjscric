import React, { useState, useEffect } from 'react';
// import Svg, {Defs, G, Image, LinearGradient, Path, Stop, Text, TextPath} from "react-native-svg";
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
const flagPlaceHolder = '/pngsV2/flag_dark.png';

const ground = '/svgs/groundImageWicket.png';
import DataNotFound from '../../commom/datanotfound';
const d3 = {
  scale,
  shape
};
import { useQuery } from '@apollo/react-hooks';
const information = '/svgs/information.svg';

import { GET_SESSION_WINNER } from '../../../api/queries';
import Heading from '../../commom/heading';

export default function PieChart({ browser, ...props }) {
  const [batteam, setBatting] = useState([]);

  const [infoToggle, setInfoToggle] = useState(false);
  const [windowObject, updateWindowObject] = useState(null);
  const [firstSelect, SetFirstSelect] = useState(false);
  const [activePieIndex, SetactivePieIndex] = useState(0);
  const [activeSession, SetactiveSession] = useState(0);
  const [firstindex, SetfirstIndex] = useState(0);

  useEffect(() => {
    updateWindowObject({ ...global.window });
  }, [firstindex]);
  const { loading, error, data } = useQuery(GET_SESSION_WINNER, {
    variables: { matchID: props.matchID },
    onCompleted: (data) => {
      if (data && data.getSessionWinner && data.getSessionWinner.data && data.getSessionWinner.data.length) {
        setBatting([
          data.getSessionWinner.data.length > 0 ? data.getSessionWinner.data[0].sessions[0].scores[0].teamName : '',
          data.getSessionWinner.data.length > 1 ? data.getSessionWinner.data[1].sessions[0].scores[0].teamName : ''
        ]);

        for (let i = 0; i < data.getSessionWinner.data.length; i++) {
          let res = data.getSessionWinner.data[i].sessions.filter((o) => o.session)[0];
          if (res) {
            SetactiveSession(res.session - 1);
            SetactivePieIndex(data.getSessionWinner.data.length - 1);
            SetFirstSelect(data.getSessionWinner.data.length - 1);

            break;
          }
        }
      }
    }
  });

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  var overArr =
    props.matchType == 'T10'
      ? ['1-3', '4-7', '8-10']
      : props.matchType == 'T20'
      ? ['1-6', '7-15', '16-20']
      : ['1-10', '11-40', '41-50'];

  if (
    error ||
    (!loading &&
      (!data || !data.getSessionWinner || !data.getSessionWinner.data || data.getSessionWinner.data.length == 0))
  )
    return (
      <>
        <div className='mx-3 mb-4'>
          <Heading heading={'Phases Of Play'} subHeading={'Check how the teams performed in a session by session breakdown'} />
          {/* <div className='text-md  text-white tracking-wide font-bold '> Phases Of Play </div>
          <div className='text-xs text-white pt-1 font-medium tracking-wide'>
            {' '}
            Check how the teams performed in a session by session breakdown{' '}
          </div>
          <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        </div>
     <DataNotFound/>
</>    );
  else if (loading) return <div></div>;
  else {
    const scr = data.getSessionWinner.data[activePieIndex].sessions[activeSession].scores;
    return ( <>
    
        <div className='mx-3 mb-4'>
          <div className='text-md  text-white tracking-wide font-bold '> Phases Of Play </div>
          <div className='text-xs text-white pt-1 font-medium tracking-wide'>
            {' '}
            Check how the teams performed in a session by session breakdown{' '}
          </div>
          <div className='w-12 h-1 bg-blue-8 mt-2'></div>
        </div>
      <div className={`md:mt-14  mt-5  mw75-l center text-white `}>
        {/* <div className={`f6 fw5 pa2`}>PHASES OF PLAY</div> */}

      

        <div className='md:flex m-2 bg-gray rounded'>
          <div className='w-full lg:w-6/12 md:w-6/12 '>
            <div className={`pb2`}>
              <div className='p-2 text-base font-semibold tracking-wide capitalize '>Phases of Play</div>
            </div>
            {/* ---------------------------------------------TEAMS FLAG START------------------------------------------------------------------ */}
            {/* ------------------TEAMA----------------------- */}
            <div className=' flex justify-between items-center '>
              <div className='flex flex-col items-center w-1/3 '>
                <div className='  bg-basebg flex items-center justify-center w-20 h-14  rounded'>
                  <img
                    alt=''
                    className='w-8 h-5 rounded-sm  '
                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    src={`https://images.cricket.com/teams/${data.getSessionWinner.teamAId}_flag.png`}
                  />

                  <div className='pl-2 font-semibold text-md'>{data.getSessionWinner.teamAWon}</div>
                </div>

                <div className={`text-gray-2 font-semibold text-sm pt-1 `}>{data.getSessionWinner.teamA}</div>
              </div>

              {/* SHARED */}

              <div className='flex flex-col items-center w-1/3 '>
                <div className='  bg-basebg flex items-center justify-center h-14 w-20  rounded'>
                  <div clasName=''>
                    <img
                      alt=''
                      className='w-7 h-4 '
                      onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      src={`https://images.cricket.com/teams/${data.getSessionWinner.teamAId}_flag.png`}
                    />
                    <img
                      alt=''
                      className='w-7 h-4 absolute -mt-2 ml-2 '
                      onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      src={`https://images.cricket.com/teams/${data.getSessionWinner.teamBId}_flag.png`}
                    />
                  </div>

                  <div className='pl-4 font-semibold text-md'>{data.getSessionWinner.shared}</div>
                </div>

                <div className={`text-gray-2 font-semibold text-xs pt-1 uppercase`}>shared</div>
              </div>

              {/*--------------------TEAMB-------------------*/}

              <div className='flex flex-col items-center w-1/3 '>
                <div className='  bg-basebg flex items-center justify-center w-20 h-14  rounded'>
                  <img
                    alt=''
                    className='w-8 h-5 rounded-sm  '
                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    src={`https://images.cricket.com/teams/${data.getSessionWinner.teamBId}_flag.png`}
                  />

                  <div className='pl-2 font-semibold text-md'>{data.getSessionWinner.teamBWon}</div>
                </div>

                <div className={`text-gray-2 font-semibold text-sm pt-1 `}>{data.getSessionWinner.teamB}</div>
              </div>
            </div>
            {/* -------------------------------------------TEAMS FLAG END-------------------------------------------------------------------- */}
            <div className='h-[2px] bg-gray-8 mt-3 mx-3'></div>
            <div className='flex    justify-center items-center mt-3 mb-2'>
              <div className='w-20 '></div>
              <div className='flex justify-center   w-80 text-gray-2 '>
                {overArr.map((session, i) => (
                  <div key={i} className={` w-1/3 ml-1  text-center font-medium text-xs `}>
                    {' '}
                    {session}{' '}
                  </div>
                ))}
              </div>
            </div>

            <div className=' '>
              {data &&
                data.getSessionWinner &&
                data.getSessionWinner.data &&
                data.getSessionWinner.data.map((day, i) => (
                  <div className='flex   justify-center items-center w-100'>
                    {day &&
                      day.sessions &&
                      day.sessions.map((session, j) => (
                        <>
                          {j == 0 && (
                            <div className={` ml-2  text-gray-2 text-xs font-semibold tc w-20 `}>
                              {batteam.length > 0 ? batteam[i] : null}
                            </div>
                          )}

                          <div
                            key={j}
                            onClick={
                              session.teamId != null
                                ? () => (SetFirstSelect(true), SetactivePieIndex(i), SetactiveSession(j))
                                : null
                            }
                            className={` w-1/4 h-8 m-2 bg-gray-8 
                           ${
                             firstSelect
                               ? i === activePieIndex && j == activeSession
                                 ? ' border-green border-2 '
                                 : ''
                               : activePieIndex === i && j == 0
                               ? 'border-green border-2'
                               : ''
                           }  flex   shadow-4 justify-center items-center  br2 mh2 fw6 f6-l f8 tc`}>
                            {session.teamId != 'None' && session.teamId !== null ? (
                              <img
                                alt=''
                                className='w-8   h-5 '
                                onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                src={`https://images.cricket.com/teams/${session.teamId}_flag.png`}
                              />
                            ) : session.teamId !== null ? (
                              <div clasName='flex p-1 w-100  items-center justify-center'>
                                <img
                                  alt=''
                                  className='w-8 h-5 '
                                  onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  src={`https://images.cricket.com/teams/${data.getSessionWinner.teamAId}_flag.png`}
                                />
                                <img
                                  alt=''
                                  className='w-8 h-5 absolute -mt-3 ml-2 '
                                  onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  src={`https://images.cricket.com/teams/${data.getSessionWinner.teamBId}_flag.png`}
                                />
                              </div>
                            ) : (
                              ''
                            )}
                          </div>
                        </>
                      ))}
                  </div>
                ))}
            </div>
          </div>
          <div className='h-[2px] bg-gray-8 mt-3 mx-3'></div>
          {/* -------------------------------------------SESSION DATA ON DISPLAY BELOW PIECHART---------------------------------------------------------------------------------------- */}
          <div className=' lg:hidden md:hidden   '>
            {/* <div className={`f5 fw5 flex justify-end mr2 mb2 dn-l`}>{`Day - ${activePieIndex + 1}`}</div> */}

            <div className='flex justify-between p-3 px-4 text-gray-2'>
              <div className={`text-xs font-semibold`}>
                {' '}
                {batteam[activePieIndex]} BAT {overArr[activeSession]} OVS{' '}
              </div>
              <div
                className={`text-xs font-semibold`}>{`SESSION : ${data.getSessionWinner.data[activePieIndex].sessions[activeSession].teamWon}`}</div>
            </div>
            <div className='h-[2px] bg-gray-8 mb-3 mx-3'></div>

            {data.getSessionWinner.data[firstSelect ? activePieIndex : firstindex].sessions[activeSession].scores.map(
              (score, i) =>
                ((scr[0].inningNo === scr[1].inningNo && i === 0) || scr[0].inningNo !== scr[1].inningNo) && (
                  <div key={i} className='flex justify-between items-start pt-2 ph3 '>
                    <div className='w-1/4'>
                      <div className={`text-md font-semibold ml-2`}>{`${
                        scr[0].inningNo === scr[1].inningNo ? scr[0].startScore : scr[i].startScore
                      }`}</div>
                    </div>
                    <div className='w-1/2'>
                      <div className='flex justify-between items-center pt-2'>
                        <div className='bg-black h-2 w-2 rounded-full' />
                        {Array.apply(null, { length: 15 })
                          .map(Number.call, Number)
                          .map((x) => (
                            <div
                              key={x}
                              className='bg-black'
                              style={{
                                width: 9,
                                height: 1.2
                              }}
                            />
                          ))}
                        <div className='bg-black h-2 w-2 rounded-full' />
                      </div>
                      <div className={`flex justify-center items-center `}>
                        <img
                          alt=''
                          className='w-10 h-10 shadow-4'
                          style={{ marginTop: '-0.9rem' }}
                          src={`https://images.cricket.com/teams/${
                            scr[0].inningNo === scr[1].inningNo ? scr[0].teamId : scr[i].teamId
                          }_flag.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        />
                      </div>
                      <div className={`text-sm font-semibold pt-1 text-center `}>
                        {scr[0].inningNo === scr[1].inningNo ? scr[0].teamName : scr[i].teamName}
                      </div>
                    </div>
                    <div className='w-1/4 text-right mr-2'>
                      <div className={`text-md font-semibold ml-2`}>{`${
                        scr[0].inningNo === scr[1].inningNo ? scr[1].endScore : scr[i].endScore
                      }`}</div>
                    </div>
                  </div>
                )
            )}
            <div className='flex justify-between pb-2 px-3 text-gray-2 text-sm font-semibold'>
              <p className={``}>Start</p>
              <p className={``}>End</p>
            </div>
            {/* score[0].endScore !== "" ? score[0].endScore: score[1].endScore */}
          </div>
          {/* desktop */}
          <div className='hidden lg:block md:block flex flex-col items-center  justify-center w-6/12 '>


          <div className='   '>
            {/* <div className={`f5 fw5 flex justify-end mr2 mb2 dn-l`}>{`Day - ${activePieIndex + 1}`}</div> */}

            <div className='flex justify-between p-3 px-4 text-gray-2'>
              <div className={`text-xs font-semibold`}>
                {' '}
                {batteam[activePieIndex]} BAT {overArr[activeSession]} OVS{' '}
              </div>
              <div
                className={`text-xs font-semibold`}>{`SESSION : ${data.getSessionWinner.data[activePieIndex].sessions[activeSession].teamWon}`}</div>
            </div>
            <div className='h-[2px] bg-gray-8 mb-3 mx-3'></div>

            {data.getSessionWinner.data[firstSelect ? activePieIndex : firstindex].sessions[activeSession].scores.map(
              (score, i) =>
                ((scr[0].inningNo === scr[1].inningNo && i === 0) || scr[0].inningNo !== scr[1].inningNo) && (
                  <div key={i} className='flex justify-between items-start pt-2 ph3 '>
                    <div className='w-1/4'>
                      <div className={`text-md font-semibold ml-2`}>{`${
                        scr[0].inningNo === scr[1].inningNo ? scr[0].startScore : scr[i].startScore
                      }`}</div>
                    </div>
                    <div className='w-1/2'>
                      <div className='flex justify-between items-center pt-2'>
                        <div className='bg-black h-2 w-2 rounded-full' />
                        {Array.apply(null, { length: 15 })
                          .map(Number.call, Number)
                          .map((x) => (
                            <div
                              key={x}
                              className='bg-black'
                              style={{
                                width: 9,
                                height: 1.2
                              }}
                            />
                          ))}
                        <div className='bg-black h-2 w-2 rounded-full' />
                      </div>
                      <div className={`flex justify-center items-center `}>
                        <img
                          alt=''
                          className='w-10 h-10 shadow-4'
                          style={{ marginTop: '-0.9rem' }}
                          src={`https://images.cricket.com/teams/${
                            scr[0].inningNo === scr[1].inningNo ? scr[0].teamId : scr[i].teamId
                          }_flag.png`}
                          onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        />
                      </div>
                      <div className={`text-sm font-semibold pt-1 text-center `}>
                        {scr[0].inningNo === scr[1].inningNo ? scr[0].teamName : scr[i].teamName}
                      </div>
                    </div>
                    <div className='w-1/4 text-right mr-2'>
                      <div className={`text-md font-semibold ml-2`}>{`${
                        scr[0].inningNo === scr[1].inningNo ? scr[1].endScore : scr[i].endScore
                      }`}</div>
                    </div>
                  </div>
                )
            )}
            <div className='flex justify-between pb-2 px-3 text-gray-2 text-sm font-semibold'>
              <p className={``}>Start</p>
              <p className={``}>End</p>
            </div>
            {/* score[0].endScore !== "" ? score[0].endScore: score[1].endScore */}
          </div>

            {false&& data.getSessionWinner.data[firstSelect ? activePieIndex : firstindex].sessions.map(
              (x, i) =>
                x.day && (
                  <div>
                    <div className={`f5 fw5 flex justify-end mr2 my-2 dn-l`}>{`Day - ${activePieIndex + 1}`}</div>
                    <div className='flex justify-between items-center  p-2 bg-basebg my-2'>
                      <div className={`text-xs font-semibold`}>
                        {' '}
                        {batteam.length > 0 ? batteam[activePieIndex] : null} BAT {overArr[i]} OVS{' '}
                      </div>
                      <div className={`text-xs font-semibold`}>{`SESSION : ${x.teamWon}`}</div>
                    </div>
                    <div className='flex justify-between ph2'>
                      <p className={` text-xs font-semibold `}>Start</p>
                      <p className={` text-xs font-semibold` }>End</p>
                    </div>
                    {x.scores.map(
                      (score, j) =>
                        ((x.scores[0].inningNo === x.scores[1].inningNo && j === 0) ||
                          x.scores[0].inningNo !== x.scores[1].inningNo) && (
                          <div key={j} className='flex justify-between pa2 ph3'>
                            <div className='w-25'>
                              <div className={`f5 fw6 pt2`}>{`${
                                x.scores[0].inningNo === x.scores[1].inningNo
                                  ? x.scores[0].startScore
                                  : score.startScore
                              }`}</div>
                            </div>
                            <div className='w-50'>
                              <div className='flex justify-between items-center pv2'>
                                <div
                                  className='bg-green_10 mr1'
                                  style={{
                                    height: 5,
                                    width: 5,
                                    borderRadius: 3
                                  }}
                                />
                                {Array.apply(null, { length: 15 })
                                  .map(Number.call, Number)
                                  .map((x) => (
                                    <div
                                      key={x}
                                      className='bg-grey_5'
                                      style={{
                                        width: 6,
                                        height: 1,
                                        borderRadius: 2,
                                        marginRight: 1
                                      }}
                                    />
                                  ))}
                                <div
                                  className='bg-red_10 ml1'
                                  style={{
                                    height: 5,
                                    width: 5,
                                    borderRadius: 3
                                  }}
                                />
                              </div>
                              <div className={`flex justify-center items-center `}>
                                <img
                                  alt=''
                                  className='w-10 h-10 shadow-4'
                                  style={{ marginTop: '-1.3rem' }}
                                  onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                  src={`https://images.cricket.com/teams/${
                                    x.scores[0].inningNo === x.scores[1].inningNo ? x.scores[1].teamId : score.teamId
                                  }_flag.png`}
                                />
                              </div>
                              <div className={`f6 fw5 flex justify-center pt1 `}>
                                {x.scores[0].inningNo === x.scores[1].inningNo ? x.scores[1].teamName : score.teamName}
                              </div>
                            </div>
                            <div className='w-25 tr'>
                              <div className={`f5 fw6 pt2`}>{`${
                                x.scores[0].inningNo === x.scores[1].inningNo ? x.scores[1].endScore : score.endScore
                              }`}</div>
                            </div>
                          </div>
                        )
                    )}
                  </div>
                )
            )}
          </div>
        </div>
      </div>
  </>  );
  }
}
