import React, { useState } from 'react'
import WormChart from '../wormChart'
import ScoringChartCompleted from '../scoringcharComplete'
import Heading from '../../commom/heading'
export default function runComparisonCompleted(props) {
  const [toggle2, setToggle2] = useState('worm')
  return (
    <div>
      <div className="mx-3">
        <Heading
          heading={'RUN COMPARSION'}
          subHeading={
            'Statistical visualization to depict score progression and fall of wickets against the overs bowled during an innings'
          }
        />
        {/* <div className='text-md  text-white tracking-wide font-bold '>RUN COMPARSION </div>
        <div className='text-xs text-white pt-1 font-medium tracking-wide'>
          {' '}
          Compare team scores and measure their performance with every over{' '}
        </div>
        <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
      </div>
      <div className="mt-5 mx-1 bg-gray py-2 text-white  ">
        {/* <div className='text-lg font-semibold tracking-wide px-2'>Run Comparison </div> */}
        <div className="flex rounded-3xl items-center justify-between text-sm mt-3 bg-gray-8 mx-3 lg:mx-20 md:mx-20">
          <div
            className={`w-1/2 text-center rounded-3xl py-1.5  ${
              toggle2 === 'worm'
                ? `border-2  border-green bg-gray-8 shadow-sm shadow-green font-medium`
                : ''
            }`}
            onClick={() => setToggle2('worm')}
          >
            Worm Chart
          </div>
          <div
            className={`w-1/2 text-center rounded-3xl py-1.5  ${
              toggle2 === 'scoring'
                ? `border-2  border-green bg-gray-8 shadow-sm shadow-green font-medium`
                : ''
            }`}
            onClick={() => setToggle2('scoring')}
          >
            Scoring Chart
          </div>
        </div>
        {toggle2 == 'worm' ? (
          <WormChart
            cricltics={true}
            matchType={props.matchType}
            matchID={props.matchID}
          />
        ) : (
          <ScoringChartCompleted
            cricltics={true}
            matchType={props.matchType}
            matchID={props.matchID}
          />
        )}
      </div>
    </div>
  )
}
