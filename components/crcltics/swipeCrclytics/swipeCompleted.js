import React, { useRef, useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'

import GameChangingOver from './gameChanginOver'
import MatchStat from './matchStat'
import MatchReel from './matchReel'
import PhasesOfPlay from './phasesOfPlay'
import ReportCard from './matchRating'
import RunComparisonCompleted from './runComparisonCompleted'
import { EffectCoverflow, Pagination } from 'swiper'
export default function index(props) {
  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)
  const [swiper, updateSwiper] = useState(null)
  const [currentIndex, updateCurrentIndex] = useState(0)
  const [componentArray, setComponentArray] = useState([])

  const router = useRouter()

  const routerTabName = router.query.id.slice(-1)[0]

  const indexTell = () => {
    if (routerTabName === 'report-card') {
      return '0'
    }
    if (routerTabName === 'run-comparison') {
      return '1'
    }
    if (routerTabName === 'game-change-overs') {
      return '2'
    }
    if (routerTabName === 'match-reel') {
      return '3'
    }
    if (routerTabName === 'match-stat') {
      return '4'
    }
    if (routerTabName === 'phases-of-play') {
      return '5'
    }
  }

  const [paggination, setPaggination] = useState(0)

  const swiperRef = useRef(null)

  const components = [
    <ReportCard matchID={props.matchID} />,
    <RunComparisonCompleted
      matchID={props.matchID}
      matchType={props.matchType}
    />,
    <GameChangingOver />,
    <MatchReel
      matchID={props.matchID}
      matchType={props.matchType}
      matchStatus={props.data.matchStatus}
    />,
    <MatchStat
      matchID={props.matchID}
      data={props.keystat}
      matchData={props.data}
    />,
    <PhasesOfPlay matchID={props.matchID} />,
  ]
  const componentsTest = [
    <ReportCard matchID={props.matchID} />,
    <MatchReel
      matchID={props.matchID}
      matchType={props.matchType}
      matchStatus={props.data.matchStatus}
    />,
    <PhasesOfPlay matchID={props.matchID} />,
  ]

  useEffect(() => {
    parseInt(indexTell())

    const newArrayMAtch =
      props.matchType !== 'Test'
        ? components.splice(parseInt(indexTell()), 6)
        : componentsTest.splice(parseInt(indexTell()), 3)
    const finalArray =
      props.matchType !== 'Test'
        ? [...newArrayMAtch, ...components]
        : [...newArrayMAtch, ...componentsTest]
    setComponentArray(finalArray)
  }, [])
  useEffect(() => {
    setPaggination(parseInt(indexTell()))
    updateCurrentIndex(indexTell())
    if (swiper !== null) {
      try {
        swiper.on('slideChange', () => {
          setPaggination(swiper.realIndex)
          updateCurrentIndex(swiper.realIndex)

          // props.updateCurrentIndex(swiper.realIndex);
        })
        swiper.on('click', (swipeData) => {
          // console.log(currentIndex);
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        })
      } catch (e) {
        console.log(e)
      }
    }
  }, [])

  // const content = [
  //   { heading: 'Report Card', subHeading: 'A comprehensive view of a player’s impact on the game' },
  //   { heading: 'Run Comparison', subHeading: 'Compare team scores and measure their performance with every over.' },
  //   { heading: 'Game Changing Overs', subHeading: 'Forecast of where the match is headed, in the next few overs' },
  //   { heading: 'Match Reel', subHeading: 'An over by over breakdown of projected scores during the match' },

  //   { heading: 'Match Stats', subHeading: 'An overview of all the important statistics from the match' },
  //   { heading: 'Phases Of Play', subHeading: 'Check how the teams performed in a session by session breakdown' }
  // ];
  const content = [
    {
      routerTabName: 'report-card',
      heading: 'Report Card',
      subHeading: 'A comprehensive view of a player’s impact on the game.',
    },
    {
      routerTabName: 'run-comparison',
      heading: 'Run Comparison',
      subHeading:
        'Statistical visualization to depict score progression and fall of wickets against the overs bowled during an innings',
    },
    {
      routerTabName: 'game-change-overs',
      heading: 'Game Changing Overs',
      subHeading:
        'A collection of four overs which according to Criclytics changed the game,with a detailed breakdown of what happened in each ball of these overs.',
    },
    {
      routerTabName: 'match-reel',
      heading: 'Match Reel',
      subHeading:
        'A fast-tracked reel of how the fortunes of the teams shifted through the game.',
    },

    {
      routerTabName: 'match-stat',
      heading: 'Match Stats',
      subHeading: 'An overview of all the important statistics from the match',
    },
    {
      routerTabName: 'phases-of-play',
      heading: 'Phases Of Play',
      subHeading:
        'Check how the teams performed in a session by session breakdown',
    },
  ]

  return (
    <div className="text-white mt-16 px-2 pt-2 ">
      {/* {
        <div className='flex  flex-column justify-between  p-3'>
          <div className='text-base  text-white font-bold '> {content[parseInt(paggination)].heading} </div>
          <div className='text-xs text-white'> {content[parseInt(paggination)].subHeading} </div>
        </div>
      } */}

      <Swiper
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current,
        }}
        shouldSwiperUpdate={true}
        effect={'coverflow'}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        onSlideChange={(swiper) => setPaggination(swiper.realIndex)}
        slidesPerView={'1'}
        initialSlide={1}
        coverflowEffect={{
          slideShadows: true,
          rotate: 0,
          stretch: 0,
          depth: 600,
          modifier: 0,
        }}
        pagination={false}
        modules={[EffectCoverflow, Pagination]}
        className="mySwiper "
      >
        {componentArray.map((component, i) => {
          return (
            <SwiperSlide>
              {/* {router.query.id.length>2&&<div className='m-3 md:mt-14 m w-full'>
        <div className='text-md  text-white tracking-wide font-bold '> {content[paggination].heading}</div>

        <div className='text-xs  text-white tracking-wide '> {content[paggination].subHeading}</div>
        <div className='w-12 h-1  bg-blue-8 mt-3'></div>
      </div>
      } */}

              {component}
            </SwiperSlide>
          )
        })}
      </Swiper>
      <div className="h-10 w-full "></div>

      {props.matchType !== 'Test' ? (
        <div className="flex justify-center items-center left-0 right-0 z-max p-2 bg-basebg  fixed bottom-0">
          {[1, 2, 3, 4, 5, 6].map((item, key) => (
            <div
              className={`w-1.5 h-1.5 base ${
                parseInt(paggination) === key ? 'bg-green-3' : 'bg-blue-4 '
              } rounded-full m-2  `}
            ></div>
          ))}
        </div>
      ) : (
        <div className="flex justify-center items-center left-0 right-0 z-max p-2 bg-basebg  fixed bottom-0">
          {[1, 2, 3].map((item, key) => (
            <div
              className={`w-1.5 h-1.5 base ${
                parseInt(paggination) === key ? 'bg-green-3' : 'bg-blue-4 '
              } rounded-full m-2  `}
            >
              {' '}
            </div>
          ))}
        </div>
      )}
      {/* <Add index={props.activeIndex} /> */}
    </div>
  )
}

// const Add = (props) => {
//   return (
//     <Swiper
//       spaceBetween={50}
//       centeredSlides={true}
//       slidesPerView={1}
//       initialSlide={props.index ? props.index : 3}
//       onSlideChange={() => console.log('slide change')}
//       onSwiper={(swiper) => {
//         // alert(8)
//       }}>
//       <SwiperSlide>Slide 1</SwiperSlide>
//       <SwiperSlide>Slide 2</SwiperSlide>
//       <SwiperSlide>Slide 3</SwiperSlide>
//       <SwiperSlide>Slide 4</SwiperSlide>
//       <SwiperSlide>Slide 5</SwiperSlide>
//       <SwiperSlide>Slide 6</SwiperSlide>
//       <SwiperSlide>Slide 7</SwiperSlide>
//     </Swiper>
//   );
// };
