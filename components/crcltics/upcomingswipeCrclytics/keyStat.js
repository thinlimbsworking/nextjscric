import React from 'react'

export default function KeyStat(props) {
  const Pitch = '/pngsV2/keystatpitch.png'
  const Location = '/pngsV2/statlocation.png'

  return (
    <div className="">
      <div className="flex text-2xl font-semibold mx-3 text-white">
        Key Stats
      </div>
      <div className="flex text-xs text-white mx-3 mt-2 tracking-wide mb-5 lg:mb-10 md:lb-10">
        Just the right stats to help you determine which team statistically has
        the upper hand.
      </div>
      {props.data.getKeyStats.head2headStats.head2Head.totalMatches > 0 && (
        <div>
          <div className="flex items-center justify-between   m-2 mx-3">
            <div className="text-white text-base  font-bold">Head To Head</div>
            <div className="text-xs text-gray-2 font-medium">*Last 3 Years</div>
          </div>
          <div className="h-1  w-12 bg-blue-8  mx-3" />

          <div className="flex flex-col mx-3 mt-4 rounded p-2 dark:bg-gray bg-white">
            <div className="m-1 relative">
              <div className="py-2 flex justify-center">
                <div className="bg-gray-3 h-[1px] w-full absolute"></div>
                <div className="text-gray-3 text-xs font-medium tc absolute top-0 bg-gray px-2 uppercase">
                  {props.data.getKeyStats.head2headStats.head2Head.totalMatches}{' '}
                  Matches{' '}
                </div>
              </div>

              <div className="w-full flex bg-gray-3 rounded-full overflow-hidden  h-3  mt-2">
                <div
                  className={`${'bg-green'}  h-3 rounded-l-lg`}
                  style={{
                    width:
                      props.data.getKeyStats.head2headStats.head2Head.teamA *
                        (100 /
                          (props.data.getKeyStats.head2headStats.head2Head
                            .teamA +
                            props.data.getKeyStats.head2headStats.head2Head
                              .teamB +
                            props.data.getKeyStats.head2headStats.head2Head
                              .noResult)) +
                      '%',
                  }}
                />
                <div
                  className={` bg-gray-3 h-3  `}
                  style={{
                    width:
                      props.data.getKeyStats.head2headStats.head2Head.noResult *
                        (100 /
                          (props.data.getKeyStats.head2headStats.head2Head
                            .teamA +
                            props.data.getKeyStats.head2headStats.head2Head
                              .teamB +
                            props.data.getKeyStats.head2headStats.head2Head
                              .noResult)) +
                      '%',
                  }}
                />
                <div
                  className={`rounded-r-lg  ${'bg-white'}  h-3   `}
                  style={{
                    width:
                      props.data.getKeyStats.head2headStats.head2Head.teamB *
                        (100 /
                          (props.data.getKeyStats.head2headStats.head2Head
                            .teamA +
                            props.data.getKeyStats.head2headStats.head2Head
                              .teamB +
                            props.data.getKeyStats.head2headStats.head2Head
                              .noResult)) +
                      '%',
                  }}
                />
              </div>
            </div>

            <div className="flex mt-2 mx-1 justify-between items-center text-gray-2">
              <div className=" flex items-center text-xs font-medium">
                <p className={`w-2 h-2 ${'bg-green'} rounded-full m-1  `}></p>
                {props.data.getKeyStats.head2headStats &&
                  props.data.getKeyStats.head2headStats.teamA.teamShortName}
                (
                {props.data.getKeyStats.head2headStats.head2Head.teamA &&
                  props.data.getKeyStats.head2headStats.head2Head.teamA}
                )
              </div>
              {true && (
                <div className="flex items-center text-xs font-medium">
                  <p className="w-2 h-2 bg-gray-3 rounded-full m-1  "></p>
                  TIE (
                  {props.data.getKeyStats.head2headStats.head2Head.noResult ||
                    0}
                  )
                </div>
              )}
              <div className="flex items-center text-xs font-medium">
                <p className={`w-2 h-2 ${'bg-white'} rounded-full m-1  `}></p>
                {props.data.getKeyStats.head2headStats &&
                  props.data.getKeyStats.head2headStats.teamB.teamShortName}
                (
                {props.data.getKeyStats.head2headStats.head2Head.teamB &&
                  props.data.getKeyStats.head2headStats.head2Head.teamB}
                )
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="flex flex-col items-start   m-3 mt-4">
        <div className="text-white text-base  font-bold">Pitch Behaviour</div>
        <div className="h-1 my-2 w-12 bg-blue-8" />

        <div className="text-white text-xs flex items-center justify-center mt-1 font-medium">
          {' '}
          <img className="h-8 w-8 mr-2" src={Location} alt="" />{' '}
          {props.data.getKeyStats.head2headStats.venueStatsData.venueName}
        </div>
      </div>

      <div className=" lg:flex md:flex ">
        <div className="flex flex-col lg:w-6/12 md:w-6/12">
          <div className="flex  items-center  justify-start p-2 mx-1">
            <div className="w-6/12  bg-gray white mx-[1px] p-3 rounded-l-lg">
              <div className="text-[10px] font-medium text-gray-2">
                {' '}
                OVERALL
              </div>
              <div className="text-xs dark:text-white text-black font-bold pt-1">
                {console.log(
                  'props.data.getKeyStats.head2headStats.v',
                  props.data.getKeyStats,
                )}
                {props.data.getKeyStats.head2headStats.venueStatsData.overall}
              </div>
            </div>
            <div className="w-6/12  bg-gray white mx-[1px] p-3 rounded-r-lg">
              <div className="text-[10px] font-medium text-gray-2 uppercase">
                {' '}
                Best suited for
              </div>
              <div className="text-xs dark:text-white text-black font-bold pt-1">
                {' '}
                {
                  props.data.getKeyStats.head2headStats.venueStatsData
                    .bestSuited
                }
              </div>
            </div>
          </div>

          <img
            className="flex  -mt-5 h-48 lg:h-24 md:h-24 w-full"
            src={Pitch}
          />
        </div>

        <div className="-mt-6 lg:w-6/12 md:w-6/12 lg:mt-6 md:mt-2">
          <div className="bg-gray mx-3 p-3 rounded-lg ">
            <div className="flex justify-between">
              <div className="w-6/12 pl-2">
                <div className="flex text-gray-2 text-[10px] font-semibold">
                  {' '}
                  1ST BATTING AVG. SCORE
                </div>
                <div className="flex text-white text-xs font-bold mt-1  ">
                  {
                    props.data.getKeyStats.head2headStats.venueStatsData
                      .avgFirstInningScore
                  }
                </div>
              </div>
              <div className="w-6/12 pl-2 ">
                <div className="flex text-gray-2 text-[10px] font-semibold">
                  {' '}
                  HIGHEST SCORE CHASED
                </div>
                <div className="flex text-white text-xs font-bold mt-1 ">
                  {
                    props.data.getKeyStats.head2headStats.venueStatsData
                      .highestScoreChased
                  }
                </div>
              </div>
            </div>
            <div className="pl-2 mt-5">
              <div className="flex text-gray-2 text-[10px] font-semibold">
                {' '}
                WICKET SPLIT
              </div>
              <div className=" flex justify-between ">
                <div className="flex text-white text-xs font-bold mt-1 w-6/12 ">
                  <p className="text-gray-2 pr-1 font-medium"> PACE</p>{' '}
                  {
                    props.data.getKeyStats.head2headStats.venueStatsData
                      .paceWicketPercent
                  }
                  %
                </div>
                <div className="flex text-white text-xs font-bold mt-1 w-6/12">
                  <p className="text-gray-2 pr-1 font-medium"> SPIN</p>{' '}
                  {
                    props.data.getKeyStats.head2headStats.venueStatsData
                      .spinWicketPercent
                  }
                  %
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
