import React, { useRef, useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
import { format } from 'date-fns';
import Countdown from 'react-countdown-now';

import PhasesOfPlay from './keyStat';
// import ReportCard from './matchRating'
// import FormIndex from './keyStat'
import MatchUps from '../../matchups';
// import MatchUps from './keyStat'
import FormIndex from '../liveSwipeCrclytics/formIndex';
import KeyStat from './keyStat';

import { EffectCoverflow, Pagination } from 'swiper';
// import Homepage from '..'
export default function index(props) {

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  const [swiper, updateSwiper] = useState(null);
  const [currentIndex, updateCurrentIndex] = useState(0);
  const [componentArray, setComponentArray] = useState([]);

  const router = useRouter();

  const routerTabName = router.query.id.slice(-1)[0];
  const [paggination, setPaggination] = useState(0);

  const indexTell = () => {
    if (routerTabName === 'key-stat') {
      return '0';
    }
    if (routerTabName === 'player-matchup') {
      return '1';
    }

    if (routerTabName === 'form-index') {
      return '2';
    }
  };
  const navArr = ['player-matchup', 'key-stat', 'form-index'];

  

  const swiperRef = useRef(null);

  const components = [
    <KeyStat data={props.keystat} matchID={props.matchID} />,
    <MatchUps data={props.keystat} matchID={props.matchID} />,
    <FormIndexComponent data={props.keystat} matchID={props.matchID} />
  ];

  useEffect(() => {
    parseInt(indexTell());

    const newArrayMAtch = components.splice(parseInt(indexTell()), 2);
    const finalArray = [...newArrayMAtch, ...components];

    setComponentArray(finalArray);
  }, []);

  useEffect(() => {
    // alert(currentIndex)

    // updateCurrentIndex(indexTell())
    if (swiper !== null) {
      try {
        swiper.on('slideChange', () => {
          updateCurrentIndex(swiper.realIndex);

          // props.updateCurrentIndex(swiper.realIndex);
        });
        swiper.on('click', (swipeData) => {
          console.log(currentIndex);
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        });
      } catch (e) {
        console.log(e);
      }
    }
  }, [swiper, currentIndex]);

  const handleNav = (index) => {
    updateCurrentIndex(index);

    router.push('/criclytics/[...id]', `210480/mi-vs-pbks-match-23-indian-premier-league-2022/${navArr[index]}`);
  };

  return (
    <div className=''>
      <div className='flex justify-center items-center overflow-scroll'>
        <Swiper
          navigation={{
            prevEl: navigationPrevRef.current,
            nextEl: navigationNextRef.current
          }}
          onSwiper={(swiper) => {
            // Delay execution for the refs to be defined
          }}
          shouldSwiperUpdate={true}
          // onSlideChange={(swiper) => handleNav(swiper.realIndex)}
          grabCursor={true}
          centeredSlides={true}
          loop={true}
          slidesPerView={'1'}
          initialSlide={1}
          onSlideChange={(swiper) => setPaggination(swiper.realIndex)}
          pagination={false}
          className='mySwiper '>
          {componentArray.map((component, i) => {
            return (
              <SwiperSlide>
                {i}
                {component}
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
      {/* <div className='h-10 w-full'></div> */}

      <div className='flex justify-center items-center left-0 right-0 z-max p-2 bg-basebg  fixed bottom-0'>
        {[1, 2, 3].map((item, key) => (
          <div
            className={`w-1.5 h-1.5 base ${
              parseInt(paggination) === key ? 'bg-green-3' : 'bg-blue-4 '
            } rounded-full m-2  `}>
            {' '}
          </div>
        ))}
      </div>
      {/* <Add index={props.activeIndex} /> */}
    </div>
  );
}

const FormIndexComponent = (props) => {
  return (
    <div className='flex flex-column '>
      <FormIndex data={props.keystat} matchID={props.matchID} />
    </div>
  );
};
