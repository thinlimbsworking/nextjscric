import React from 'react';

const logo = '/gif/logo.gif';

export default function Loading() {
  return (
    <div
      className={`container flex justify-center items-center top-0 bottom-0 h-100 w-100 left-0 right-0 fixed absolute--fill z-9999`}
     >
      <img src={logo} alt='' width='75' height='75' className='bg-white  br3 pa3' />
    </div>
  );
}
