import React from 'react';
import { useRouter } from 'next/router';

import Link from 'next/link';
export default function Footer(props) {
  let router = useRouter();
  return (
    <div className={` z-50 bg-gray text-gray-2 flex justify-center  w-full bottom-0 ${router.asPath==='/season-fantasy'?'nt1':''} py-5`} st>
      <div className='flex justify-center items-center w-full'  >
        <div className='text-xs font-medium'>
          <img src={'/svgs/cricket-logo.svg'} alt='logo' />
          <div className='pl-2 pt-1'> Cricket like never before </div>
        </div>
        {/* <div className= ml3'>
        <a className='ph1'  href='https://www.cricbattle.com/' target='_blank'> <img className="w4" src={'/pngs/cricBattle.png'} alt='logo' />
        </a> 
        </div> */}
    
        <div className='flex items-center text-sm font-medium'>
          <div className=' flex'>
            
              <a href='/about/terms-and-conditions'>
                <div className='px-2 cursor-pointer  hover:text-orange '> Term of use </div>
              </a>
       
            {/* <div className="f7 ph2 cursor-pointer  hover-orange > Feedback </div> */}
            <Link href='/about/privacy-policy' passHref>

              <div className=' px-2 cursor-pointer  hover:text-orange '> Privacy Policy </div>

            </Link>
            <Link href='/about/cookies-policy' passHref>

              <div className=' px-2 cursor-pointer  hover:text-orange '> Cookies Policy </div>

            </Link>
          </div>
        </div>

        <div className='flex flex-col text-xs'>
          <div className='flex justify-center items-center'>
            <div className='pr-3'> Follow us on </div>
            <div className='flex'>
              <a className='px-1' rel='noopener noreferrer' href='https://www.facebook.com/Cricketcom/' target='_blank'>
                <img className='h-5' src={'/svgs/facebook.svg'} alt='Facebook' />
              </a>
              <a className='px-1' rel='noopener noreferrer' href='https://twitter.com/weRcricket' target='_blank'>
                <img className='h-5' src={'/svgs/twitter.svg'} alt='Twitter' />
              </a>
              <a
                className='px-1'
                rel='noopener noreferrer'
                href='https://www.linkedin.com/company/cricketcom'
                target='_blank'>
                <img className='h-5' src={'/svgs/linkedin.svg'} alt='Linkedin' />
              </a>
            </div>
          </div>
          <div className='text-xs mt-2 px-2 text-gray-2'> @ 2022 cricket.com | All rights reserved</div>
        </div>
      </div>
    </div>
  );
}
