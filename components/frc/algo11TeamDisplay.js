import React, { useState, useEffect } from 'react'
import html2canvas from 'html2canvas'
import Link from 'next/link'
import ImageWithFallback from '../commom/Image'
// import backIcon from '../../public/svgs/back_dark_black.svg'
// import stadium from '../../public/pngs/frc_stadium.png'
// import Countdown from 'react-countdown-now';
// import { format } from 'date-fns';
// import { useQuery, useMutation } from '@apollo/react-hooks';

// import { CopyToClipboard } from 'react-copy-to-clipboard';
// import { CREATE_AND_UPDATE_USER_TEAM } from '../../api/queries';
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'

const editIcon = '/svgs/icons/editteam.svg'
const downloadIcon = '/svgs/icons/downloadteam.svg'

export default function Algo11TeamDisplay({
  setclose,
  myTeam,
  lang,
  ...props
}) {
  console.log('props getting in algo ', props)
  const playerPlaceholder = '/pngs/fallbackprojection.png'
  const flagPlaceHolder = '/svgs/images/flag_empty.svg'
  const [credits, setcredits] = useState(null)

  let playerCategories = [
    {
      name: 'BATTERS',
      value: 'BATSMAN',
    },
    {
      name: 'WICKET_KEEPERS',
      value: 'KEEPER',
    },
    {
      name: 'ALL_ROUNDERS',
      value: 'ALL_ROUNDER',
    },
    {
      name: 'BOWLERS',
      value: 'BOWLER',
    },
  ]

  useEffect(() => {
    let a = myTeam?.data?.reduce((total, currentValue) => {
      return parseFloat(total) + parseFloat(currentValue.credits)
    }, 0)

    setcredits(a)
  }, [])

  const downloadImage = () => {
    html2canvas(document.getElementById('image-part'), {
      useCORS: true,
      allowTaint: true,
    }).then(function (canvas) {
      var anchorTag = document.createElement('a')
      document.body.appendChild(anchorTag)
      //document.getElementById("previewImg").appendChild(canvas);
      anchorTag.download = `${props.teamName}.jpg`
      anchorTag.href = canvas.toDataURL()
      anchorTag.click()
      anchorTag.remove()
    })
  }

  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="flex flex-col bg-basebg" id="image-part">
          <div className="flex items-center justify-around w-full">
            <div className="w-full p-2 flex justify-between items-center">
              <div className="flex items-center gap-2">
                <div
                  onClick={() => setclose(false)}
                  className="p-2 cursor-pointer bg-gray rounded-md w-8 h-8"
                >
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/svgs/empty.svg"
                    className=" flex items-center justify-center h-4 w-4 rotate-180"
                    src="/svgsV2/RightSchevronWhite.svg"
                    alt="right"
                  />
                </div>
                <span className="text-base font-semibold text-white">
                  {props.teamName}
                </span>
              </div>
              <div className="flex gap-1 ">
                {lang === 'HIN' ? 'क्रेडिट' : 'Credits'}
                <div className=" mx-2 px-2 bg-gray flex items-center justify-center rounded-md text-green ">
                  {credits ? credits : 0}
                </div>
              </div>
            </div>
          </div>

          <div className="flex  mx-2 ">
            <div className="w-full items-center">
              <div className="w-full text-center  white">
                <div className="bb b--white-20  my-3"></div>

                {playerCategories.map((category) => (
                  <div className="center text-center">
                    <div className="bb b--white-20 mb2 mt-1"></div>
                    <div className="text-center   uppercase oswald f5 fw4 p-2">
                      {getLangText(lang, words, category.name)}
                    </div>
                    <div className="w-full  flex flex-wrap   justify-center  items-center">
                      {myTeam.data
                        .filter(
                          (player) => player.player_role === category.value,
                        )
                        .map((item, index) => {
                          return (
                            <div className="w-1/3 md:w-32 lg:w-32 p-1 h-32">
                              <div className=" bg-gray my-2 h-full p-2   border-r-2 border-gray-4   w-full  rounded-md ">
                                <div className="flex w-full justify-center">
                                  <div className="flex flex-col items-center ">
                                    <div className="relative flex justify-center mb-2 ">
                                      <ImageWithFallback
                                        height={35}
                                        width={35}
                                        loading="lazy"
                                        fallbackSrc="/pngsV2/playerph.png"
                                        className="h-12 w-12 bg-gray-4 rounded-full  object-cover object-top"
                                        src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                                        // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                                      />
                                      <ImageWithFallback
                                        height={18}
                                        width={18}
                                        loading="lazy"
                                        fallbackSrc="/svgs/empty.svg"
                                        className="w-3 h-3 drop-shadow-lg absolute left-0 bottom-0 object-center flex justify-center rounded-full ba"
                                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                        // onError={(evt) => (evt.target.src = '/svgs/empty.svg')}
                                      />
                                      {(item.captain == '1' ||
                                        item.vice_captain == '1') && (
                                        <div className="w-3 h-3 absolute right-0 bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium">
                                          {item.captain == '1'
                                            ? 'C'
                                            : item.vice_captain == '1'
                                            ? 'VC'
                                            : ''}
                                        </div>
                                      )}
                                    </div>
                                    <p
                                      className="text-xs font-medium text-center w-full wordWrapLine "
                                      style={{ wordWrap: 'break-word' }}
                                    >
                                      {lang === 'HIN'
                                        ? item.playerNameHindi
                                        : item.playerName}
                                    </p>
                                    <p className=" text-center text-gray-2 text-xs">
                                      credit{' '}
                                      <span className="text-green">
                                        {' '}
                                        {item.credits}
                                      </span>{' '}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="mt-2 p-2 w-full  text-center flex justify-center gap-3">
          <div
            onClick={downloadImage}
            className="flex border cursor-pointer border-green bg-gray-4  text-xs items-center justify-center gap-1 p-2 rounded text-green"
          >
            {' '}
            <ImageWithFallback
              height={18}
              width={18}
              loading="lazy"
              fallbackSrc="/pngsV2/flag_dark.png"
              src={downloadIcon}
              alt={'download team'}
            />{' '}
            <span> {getLangText(lang, words, 'downloadTeam')} </span>
          </div>

          {props.calledBy === 'myTeams' && (
            <Link
              href={{
                pathname: '/fantasy-research-center/[...slugs]',
              }}
              as={`/fantasy-research-center/${props.matchID}/${
                props.seriesSlug
              }/${
                props.leagueType === 'statsHub'
                  ? 'fantasy-stats/players'
                  : 'create-team'
              }`}
              passHref
              legacyBehavior
            >
              <div
                onClick={() => {
                  props.handleEditTeam()
                }}
                className="flex border cursor-pointer border-green bg-gray-4  text-xs items-center justify-center gap-1 p-2 rounded text-green"
              >
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/pngsV2/flag_dark.png"
                  src={editIcon}
                  alt={'edit team'}
                />{' '}
                <span> {lang === 'HIN' ? 'टीम एडिट करें' : 'EDIT TEAM'}</span>
              </div>
            </Link>
          )}
        </div>
      </div>
    </>
  )
}
