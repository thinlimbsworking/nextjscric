import React from 'react'



export default function FRCPerformanceIndicator(props) {

   // const [maxyellow,setmaxyellow]=useState(Math.max(props.data.overallRPI,props.data.stadiumRPI,props.data.teamRPI))

   let maxRPI = Math.max(props.data.overallRPI, props.data.stadiumRPI, props.data.teamRPI)
   let maxWPI = Math.max(props.data.overallWPI, props.data.stadiumWPI, props.data.teamWPI)
   //  console.log("maxyellow",maxRPI)
   // console.log("maxyellow",maxWPI)
   return (
      <div className="flex  flex-col  mt-3  pb-3 justify-center items-center">
         {/* <svg   width="100%" viewBox={`0 0 ${width} ${height}`}  style={{
          height: "100%",
          marginRight: "0px",
          marginLeft: "0px",
        }} ref={svgRef}></svg> */}
         <div className=" bg-gray-8 rounded  w-11/12 ">
            <div className="flex items-center pt-3 justify-between">
               <div className="w-6/12 flex items-center">
                  {(props.playerRole === "KEEPER" || props.playerRole === "ALL_ROUNDER" || props.playerRole === "BATSMAN") &&
                     <>


                     </>}
               </div>
               <div className="w-50 flex items-center">
                  {/* {(props.playerRole === "ALL_ROUNDER" || props.playerRole === "BOWLER") && <>
                     <div className="w-20 flex justify-end">
                        <div className="w1 h1 br2 bg-gray "></div>
                     </div>
                     <div className="fw6 w-80 pl1  f8">{props.wickets}</div>
                  </>} */}
               </div></div>
            <div className="pv4 ph2">
               <div
                  className='relative    mt2 white w-100   '
                  style={{
                     height: 12 * 20
                  }}
               >
                  {props.data.overallRPI && (props.playerRole === "KEEPER" || props.playerRole === "ALL_ROUNDER" || props.playerRole === "BATSMAN") && 
                  <div className=" absolute bottom-0 w-100 border-t border-b "
                   style={{ height: props.data.overallRPI ? parseFloat(props.data.overallRPI) / maxRPI * 200 : 0, borderTopStyle: "dotted" }}></div>}
                  {props.data.overallWPI && (props.playerRole === "ALL_ROUNDER" || props.playerRole === "BOWLER") && <div className=" absolute bottom-0 w-100  " 
                  style={{ height: props.data.overallWPI ? parseFloat(props.data.overallWPI) / maxWPI * 100 : 0, borderTopStyle: "dotted" }}></div>}
                  <div className=" mt-6 w-100 border-b border-t  border-dotted  " >
                     <div className="flex w-100 items-end">

                        <div className="flex w-1/3 relative items-end justify-center">
                           {props.data.overallRPI && (props.playerRole === "KEEPER" || props.playerRole === "ALL_ROUNDER" || props.playerRole === "BATSMAN") && 
                           <div className="bg-green   flex justify-center relative rounded text-gray-2 mx-2" style={{ width: 10, height: parseFloat(props.data.overallRPI) / maxRPI * 200 }}>
                              <div className="absolute tc  fw6 text-xs " style={{ top: -21 }}>{props.data.overallRPI ? props.data.overallRPI : 0}</div></div>}

                           {props.data.overallWPI && (props.playerRole === "ALL_ROUNDER" || props.playerRole === "BOWLER") && <div className="bg-gray  flex justify-center relative rounded br--top" style={{ width: 10, height: props.data.overallWPI ? parseFloat(props.data.overallWPI) / maxWPI * 100 : 0 }}>
                              <div className="absolute  fw6 text-xs text-xs" style={{ top: -21 }}>{props.data.overallWPI ? parseFloat(props.data.overallWPI) : 0}</div></div>}
                           <div className="absolute tc fw4 text-xs text-center truncate gray h2 -bottom-5" >{props.lang === 'HIN' ? 'संपूर्ण' : 'Overall'}</div>
                        </div>


                        <div className="flex w-1/3 relative items-end justify-center">
                           {props.data.teamRPI && (props.playerRole === "KEEPER" || props.playerRole === "ALL_ROUNDER" || props.playerRole === "BATSMAN") && 
                           <div className="bg-green   flex justify-center relative rounded br--top mx-2" 
                           style={{ width: 10, height: props.data.teamRPI ? parseFloat(props.data.teamRPI) / maxRPI * 200 : 0 }}><div className="absolute fw6 text-xs "
                            style={{ top: -21 }}>{props.data.teamRPI ? parseFloat(props.data.teamRPI) : 0}</div></div>}
                           {props.data.teamWPI && (props.playerRole === "ALL_ROUNDER" || props.playerRole === "BOWLER") &&
                            <div className="bg-gray  flex justify-center relative rounded br--top" style={{ width: 10, height: props.data.teamWPI ? parseFloat(props.data.teamWPI) / maxWPI * 100 : 0 * 2 }}><div className="absolute  fw6 text-xs" style={{ top: -21 }}>{props.data.teamWPI ? props.data.teamWPI : 0}</div></div>}
                           <div className="absolute tc fw4 text-xs text-center truncate gray h2 -bottom-5 ">v {props.data.vsTeam}</div>
                        </div>

                        <div className="flex w-1/3 relative items-end justify-center">
                           {props.data.stadiumRPI && (props.playerRole === "KEEPER" || props.playerRole === "ALL_ROUNDER" || props.playerRole === "BATSMAN") && <div className="bg-green  flex justify-center relative rounded br--top" style={{ width: 10, height: props.data.stadiumRPI ? parseFloat(props.data.stadiumRPI) / maxRPI * 200 : 0 }}><div className="absolute fw6 text-xs " style={{ top: -21 }}>{props.data.stadiumRPI ? parseFloat(props.data.stadiumRPI) : 0}</div></div>}
                           {props.data.stadiumWPI && (props.playerRole === "ALL_ROUNDER" || props.playerRole === "BOWLER") && <div className="bg-gray   flex justify-center relative br2 br--top" style={{ width: 10, height: props.data.stadiumWPI ? parseFloat(props.data.stadiumWPI) / maxWPI * 100 : 0 }}><div className="absolute fw6 text-xs " style={{ top: -21 }}>{props.data.stadiumWPI ? parseFloat(props.data.stadiumWPI) : 0}</div></div>}
                           <div className="absolute tc fw4 text-xs text-center truncate gray h2 -bottom-5" > {props.lang === 'HIN' ? props.data.atStadiumHindi + ' में' : 'at ' + props.data.atStadium}</div>
                        </div>


                     </div>
                  </div>

                  {/* <div className="f9 absolute bottom--1 right-0 left--1 mr1 rotate-90">RUNS</div> */}
               </div>


            </div>

         </div>
         <div className='flex items-center justify-evenly w-full'>  
          <div className="mt-2 text-xs pl-1  text-left  font-bold flex items-center justify-start text-left text-gray-2">
             <div className='w-2 h-2 bg-green mr-1'></div> {props.runs}</div> 
             <div className="mt-2 text-xs pl-1  text-left  font-bold flex items-center justify-center text-left text-gray-2 ">
             <div className='w-2 h-2 bg-green mr-1 text-gray-2'></div> {props.wickets}</div></div>
       
      </div>

   )
}
