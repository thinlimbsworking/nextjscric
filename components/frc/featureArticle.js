import React,{useState} from 'react';
import { GET_ARTICLES_BY_CATEGORIES } from '../../api/queries';
import { useQuery } from '@apollo/react-hooks';
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import format from 'date-fns/format';
import { getNewsUrl ,getLiveArticleUrl} from '../../api/services';
//import { Swiper, SwiperSlide } from 'swiper/react';
//import { EffectCoverflow, Pagination } from 'swiper';
import { useRouter } from 'next/router';
import ImageWithFallback from '../commom/Image';

export default function FeaturedArticlesFRC({ browser, ...props }) {
  const [currentIndex, updateCurrentIndex] = useState(0);
  const router = useRouter();
  const navigate = router.push;
  const { loading, error, data } = useQuery(GET_ARTICLES_BY_CATEGORIES, {
    variables: { type: 'fantasy', page: 0 }
  });

  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'FantasyResearch',
      // MatchID: String(article.matchIDs),
      // SeriesID: String(article.seriesIDs),
      // PlayerID: String(article.playerIDs),
      ArticleType: article.type,
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };

  const newsHomeNav = () => {
    CleverTap.initialize('NewsHome', {
      Source: 'Homepage',
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };
  if (error) return <div></div>;
  if (loading) return <div></div>;
  else
    return (
      <div className='p-4'>
       <div className='flex justify-between items-center'>
          <div className='white capitalize  font-bold '>      {props.language?  <h2 className='text-base'>{props.language==='HIN'?"फ़ैंटसी सुझाव":"FANTASY PREVIEWS"}</h2>:<h2 className='text-base'>{props.title}</h2>}
          <div className='flex h-1 w-22 bg-blue-8 mt-1' />
          </div>
          <div
            className='flex  justify-center rounded border-2  text-green border-green text-xs px-2 py-1 items-center font-bold '
            onClick={() => navigate(`/news/latest`)}>
            VIEW ALL
          </div>
        </div>
        {/* <div className='flex items-center justify-between  cursor-pointer pa2 bb b--white-10'>
          
          {props.language?  <h2 className='f7 fw6 white'>{props.language==='HIN'?"फ़ैंटसी सुझाव":"FANTASY PREVIEWS"}</h2>:<h2 className='f7 fw6 white'>{props.title}</h2>}
        </div> */}
        
        <div className='flex flex-wrap cursor-pointer '>
          {false && data &&
            data.getArticlesByCategories.length > 0 &&
            data.getArticlesByCategories.slice(0, 2).map((article, i) => (
              <div key={i} className="w-49-l">


              {article.type != 'live-blogs' && article.type != 'live-blog' ? <Link
                key={i}
                {...getNewsUrl(article)}
                passHref
                className='w-100 ph2 pv2 flex justify-between'
                onClick={() => {
                  handleCleverTab(article);
                }}>

                <div className='br1 flex relative cover bg-top w-2/5' style={{ height: '7rem' }}>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  className=' object-cover object-top h-28 w-36 rounded'
                  src={`${article.sm_image_safari}?auto=compress&dpr=2&w=144&h=144`}
                />
                  <div className=' center f7 absolute   ph2  bottom-0 cdcgr white   pa1'>
                    {article && article.type.split('_').splice(-1)[0].toUpperCase()}
                  </div>
                </div>
                <div className='pl2 mb:w-3/5 md:w-4/5'>
                  <div className='f7 white pb1 fw6 lh-title text' style={{ WebkitLineClamp: 2, height: '2.1rem' }}>
                    {article && (article.title || '')}
                  </div>
                  <div className=' f8 text-white/80 lh-title mt1 text' style={{ WebkitLineClamp: 2 }}>
                    {article && (article.description.substr(0, 70) || '')}
                  </div>
                  <div className=''>
                    {/* <img src={'/svgs/user.svg'} alt='user' className='o-50' height='10' /> */}
                    <span className='fw4 f7 white mt2 i'>{article && (article.author || '')}</span>
                  </div>
                  <div className='f8 fw4 white'>{format(parseInt(article.createdAt), 'dd MMM yyyy')}</div>
                </div>

              </Link>:<Link
                key={i}
                {...getLiveArticleUrl(article)}
                passHref
                className='w-100 w-50-ns ph2 pv2 flex justify-between'
                onClick={() => {
                  handleCleverTab(article);
                }}>

                <div className='br1 flex relative cover bg-top w-2/5' style={{ height: '7rem' }}>
                  <ImageWithFallback height={18} width={18} loading='lazy' className='object-cover' src={article.sm_image_safari} alt='' />
                  <div className=' dib br3 f7 absolute flex  ph2  bottom-0 cdcgr white br--bottom br--left pa1'>
                    {article && article.type.split('_').splice(-1)[0].toUpperCase()}
                  </div>
                </div>
                <div className='pl2 mb:w-3/5 md:w-4/5'>
                  <div className='f7  pb1 fw6 lh-title text' style={{ WebkitLineClamp: 2, height: '2.1rem' }}>
                    {article && (article.title || '')}
                  </div>
                  <div className='fw5 f8 text-white/80 lh-title mt1 text' style={{ WebkitLineClamp: 2 }}>
                    {article && (article.description.substr(0, 70) || '')}
                  </div>
                  <div className='mt2'>
                    <ImageWithFallback height={18} width={18} loading='lazy' src={'/svgs/user.svg'} alt='user' className='o-50'  />
                    <span className='fw6 f8 gray mt2 pl1'>{article && (article.author || '')}</span>
                  </div>
                  <div className='f8 fw4 mt1'>{format(parseInt(article.createdAt), 'dd MMM yyyy')}</div>
                </div>

              </Link>}
              </div>
            ))}

{/* 
<Swiper
          slidesPerView={'auto'}
          spaceBetween={10}
          onSlideChange={(swiper) => updateCurrentIndex(swiper.realIndex)}
          modules={[Pagination]}
          className='mySwiper'>
          {data &&
            data.getArticlesByCategories.length > 0 &&
            data.getArticlesByCategories.map((item,i) => {
              return (
                item.type !== 'wvideos' && (
                  <SwiperSlide key={i} className=' w-60 bg-gray h-56  rounded-xl p-2  mr-4 my-2'>
                    <div className=' cardUpdate w-100 h-100 '>
                      <div className='flex relative items-center justify-center w-full '>
                        <img className=' h-28 w-full rounded-lg object-cover ' src={item.featureThumbnail} alt='' />
                      </div>
                      <div className=' overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left tracking-wider truncate text-white'>
                        {item.title || ''}
                      </div>
                      <div className=' my-1 text-xs font-thin truncate text-left tracking-wide text-gray-2 '>
                        {item.description || ''}
                      </div>
                      <div className=' text-gray-2 text-xs text-left truncate '>{item.author}</div>
                      <div className='text-gray-2 text-xs text-left mt-1'>
                        {format(new Date(Number(item.createdAt) - 19800000), 'dd MMM yyyy')}
                      </div>
                    </div>
                  </SwiperSlide>
                )
              );
            })}
        </Swiper> */}

        <div className='flex justify-start '>
          {data &&
            data.getArticlesByCategories.map((item, key) => (
              <div key={key} className={`w-2 h-2 ${currentIndex == key ? 'bg-blue-8' : 'bg-blue-4 '} rounded-full m-1 `}></div>
            ))}
        </div>
        </div>
      </div>
    );
}


