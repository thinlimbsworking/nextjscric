import React,{useState} from 'react';
import { GET_ARTICLES_BY_CATEGORIES } from '../../api/queries';
import { useQuery } from '@apollo/react-hooks';
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import format from 'date-fns/format';
import { getNewsUrl ,getLiveArticleUrl} from '../../api/services';
// import { Swiper, SwiperSlide } from 'swiper/react';
// import { EffectCoverflow, Pagination } from 'swiper';
import { useRouter } from 'next/router';

export default function FeaturedArticlesFRC({ browser, ...props }) {
  const [currentIndex, updateCurrentIndex] = useState(0);
  const router = useRouter();
  const navigate = router.push;


  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'FantasyResearch',
      // MatchID: String(article.matchIDs),
      // SeriesID: String(article.seriesIDs),
      // PlayerID: String(article.playerIDs),
      ArticleType: article.type,
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };

  const newsHomeNav = () => {
    CleverTap.initialize('NewsHome', {
      Source: 'Homepage',
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };
 
    return (
      <div className='p-4'>
       <div className='flex justify-between items-center'>
          <div className='white capitalize  font-bold '>      {props.language?  <h2 className='text-base'>{props.language==='HIN'?"फ़ैंटसी सुझाव":"FANTASY PREVIEWS"}</h2>:<h2 className='text-base'>{props.title}</h2>}
          <div className='flex h-1 w-22 bg-blue-8 mt-1' />
          </div>
          {/* <div
            className='flex curspor-pointer justify-center rounded border-2  text-green border-green text-xs px-2 py-1 items-center font-bold '
            onClick={() => navigate(`news/latest`)}>
            VIEW ALL
          </div> */}
        </div>
        {/* <div className='flex items-center justify-between  cursor-pointer pa2 bb b--white-10'>
          
          {props.language?  <h2 className='f7 fw6 white'>{props.language==='HIN'?"फ़ैंटसी सुझाव":"FANTASY PREVIEWS"}</h2>:<h2 className='f7 fw6 white'>{props.title}</h2>}
        </div> */}
        
        <div className='flex flex-wrap cursor-pointer '>
        


{/* <Swiper
          slidesPerView={'auto'}
          spaceBetween={10}
          onSlideChange={(swiper) => updateCurrentIndex(swiper.realIndex)}
          modules={[Pagination]}
          className='mySwiper'>
          {props.data &&
            props.data.getVideosByType.length > 0 &&
            props.data.getVideosByType.slice(0,6).map((item,i) => {
              return (
                item.type !== 'wvideos' && (
                  <SwiperSlide key={i} className=' w-60 bg-gray h-56  rounded-xl p-2  mr-4 my-2'>
                    <div className=' cardUpdate w-100 h-100 '>
                      <div className='flex relative items-center justify-center w-full '>
                        <img className=' h-28 w-full rounded-lg object-cover '  src={`https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg`} alt='' />
                      </div>
                      <div className=' overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left tracking-wider  text-white'>
                        {item.title || ''}
                      </div>
                      <div className=' my-1 text-xs font-thin  text-left tracking-wide text-gray-2 '>
                        {item.description || ''}
                      </div>
                     
                    
                    </div>
                  </SwiperSlide>
                )
              );
            })}
        </Swiper> */}

        <div className='flex justify-start '>
          {props.data &&
             props.data.getVideosByType.slice(0,6).map((item, key) => (
              <div key={key} className={`w-2 h-2 ${currentIndex == key ? 'bg-blue-8' : 'bg-blue-4 '} rounded-full m-1 `}></div>
            ))}
        </div>
        </div>
      </div>
    );
}




// import React, { useState,useRef,useEffect } from 'react';
// import Link from 'next/link';
// import { useQuery } from '@apollo/react-hooks';
// import CleverTap from 'clevertap-react';

// import { VIDEOS_VIEW } from '../../constant/Links';
// import { GET_VIDEO_POSITIONS } from '../../api/queries';
// import { useRouter } from 'next/router';
// import { Swiper, SwiperSlide } from 'swiper/react';
// import { EffectCoverflow, Navigation, Pagination, Scrollbar, A11y, Controller ,FreeMode} from 'swiper';
// export default function VideoHome({ ...props }) {
//     console.log("propspropspropspropsprops",props)

//     const [swiper, setSwiper] = useState();
//     const prevRef = useRef();
//     const nextRef = useRef();
  
//     useEffect(() => {
//       if (swiper) {
//         console.log("Swiper instance:", swiper);
//         swiper.params.navigation.prevEl = prevRef.current;
//         swiper.params.navigation.nextEl = nextRef.current;
//         swiper.navigation.init();
//         swiper.navigation.update();
//       }
//     }, [swiper]);
//   const router = useRouter();
//   const navigate = router.push;
  
//   const [currentIndex, updateCurrentIndex] = useState(0);

//   const getVideoView = (video) => {
//     let type = 'featured-videos';
//     let videoId = video.videoID;
//     return {
//       as: eval(VIDEOS_VIEW.as),
//       href: VIDEOS_VIEW.href
//     };
//   };
//   const handleVideoNavigation = (videoID) => {
//     CleverTap.initialize('Videos', {
//       Source: 'Homepage',
//       VideoID: videoID,
//       Platform: localStorage ? localStorage.Platform : ''
//     });
//   };

//   const handleNavigation = () => {
//     CleverTap.initialize('VideosHome', {
//       Source: 'Homepage',
//       Platform: localStorage ? localStorage.Platform : ''
//     });
//   };


//     return (
//       <div className=' text-white p-4 '>
//         <div className='flex justify-between items-center '>
//           <div className='font-semibold text-lg'>Featured Videos</div>
//           <div
//             className='flex  justify-center rounded border-2  text-green border-green text-xs px-2 py-1 items-center font-bold '
//             onClick={() => navigate(`videos/latest`)}>
//             VIEW ALL
//           </div>
//         </div>
//         <div className='flex justify-start mt-1 ml-12 '>
//           {props.data.getVideosByType &&
//            props.data.getVideosByType.slice(0,6).map((item, key) => (
//               <div key={key} className={`w-2 h-2 ${currentIndex == key ? 'bg-blue-8' : 'bg-blue-4 '} rounded-full m-1 `}></div>
//             ))}
//         </div>
//         <div className='flex items-center justify-between '>
//         <div className=" mx-2 hidden lg:block md:block" ref={prevRef}  >
//        <div
//           style={{ zIndex: 1000 }}
        
//           id='swiper-button-prev'
//           className='outline-0 cursor-pointer'>
//           <svg width='30' focusable='false' viewBox='0 0 24 24'>
//             <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
//             <path fill='none' d='M0 0h24v24H0z'></path>
//           </svg>
//         </div>
     
//         </div>


//         <Swiper
//  className="external-buttons "
// modules={[EffectCoverflow,Navigation, Pagination, Scrollbar, A11y, Controller]}
                  
//                     navigation={{
//                       prevEl: prevRef?.current,
//                       nextEl: nextRef?.current
//                     }}
//                     updateOnWindowResize
//                     observer
//                     observeParents
//                     onSwiper={setSwiper}
                   
                    
//                     onSlideChange={(swiper) => updateCurrentIndex(swiper.realIndex)}
//                       shouldSwiperUpdate={true}
//                       loop={true}
                   
//                         grabCursor={true}
//                         centeredSlides={true}
//                         slidesPerView={'3'}
                     
// >

   
//           {props.data.getVideosByType &&
//             props.data.getVideosByType.slice(0,6).map((item, i) => (
//               <SwiperSlide
//               className=' w-60 bg-gray h-56  rounded-xl p-2  mr-4 my-2'
               
//                 onClick={() => navigate(`videos/${item.videoID}`)}
//                 key={i}>
//                <div className='  cardUpdate w-100 h-100 '>
//                <div className='flex relative items-center justify-center w-full '>
//                     <img
//                     className=' h-28 w-full rounded-lg object-cover '
//                       src={`https://img.youtube.com/vi/${item.videoYTId}/mqdefault.jpg`}
//                       alt=''
//                     />
//                     <img className='z-0 right-0 bottom-0 w-10 h-10 absolute' src='/pngsV2/videos_icon.png' alt='' />
//                   </div>
//                   <div className=' overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left tracking-wider truncate text-white'>
//                     {item.title}
//                   </div>
//                   <div className=' text-gray-2 text-xs text-left  '>{item.description}</div>
//                 </div>
//               </SwiperSlide>
//             ))}
//         </Swiper>
//         <div className=" text-2xl hidden lg:block md:block" ref={nextRef}>
//       <div
//           style={{ zIndex: 1000 }}
        
//           id='swiper-button-prev'
//           className='white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer'>
//             <svg width='30' viewBox='0 0 24 24'>
//             <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
//             <path fill='none' d='M0 0h24v24H0z'></path>
//           </svg>
//         </div>
//         </div>
//         </div>

     
//       </div>
//     );
// }

// {/* <SwiperSlide key={i} className=' w-60 bg-gray h-56  rounded-xl p-2  mr-4 my-2'>
// <div className=' cardUpdate w-100 h-100 '>
//   <div className='flex relative items-center justify-center w-full '>
//     <img className=' h-28 w-full rounded-lg object-cover ' src={item.featureThumbnail} alt='' />
//   </div>
//   <div className=' overflow-hidden mt-2 text-xs font-semibold text-ellipsis text-left tracking-wider truncate text-white'>
//     {item.title || ''}
//   </div>
//   <div className=' my-1 text-xs font-thin truncate text-left tracking-wide text-gray-2 '>
//     {item.description || ''}
//   </div>
//   <div className=' text-gray-2 text-xs text-left truncate '>{item.author}</div>
//   <div className='text-gray-2 text-xs text-left mt-1'>
//     {format(new Date(Number(item.createdAt) - 19800000), 'dd MMM yyyy')}
//   </div>
// </div>
// </SwiperSlide> */}