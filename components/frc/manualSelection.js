import React, { useState } from 'react';
import ChoosePlayer from './choosePlayer';
import Countdown from 'react-countdown-now';
import LoadingTeam from './loadingTeam.js';
import ChooseTeam from './chooseTeam.js';
import ChooseLogic from './chooseLogic';
import { format } from 'date-fns';
import { words } from '../../constant/language';
import { getLangText } from '../../api/services';
import ImageWithFallback from '../commom/Image';

const flagPlaceHolder = '/pngsV2/flag_dark.png';
const langIcon = '/pngsV2/lang.png';
const leftarrow = '/svgsV2/RightSchevronWhite.svg';

const leagueTypes = [
  {
    title: 'GRAND_LEAGUE',
    ref: 'grand_league',
    desc: 'WBFCL',
    imgSrc: '/pngsV2/frc_gl.png'
  },
  {
    title: 'SMALL_LEAGUE',
    ref: 'small_league',
    desc: 'FTFC',
    imgSrc: '/pngsV2/frc_sl.png'
  },
  {
    title: 'ONE_V_ONE',
    ref: 'h2h',
    desc: 'AHDAYR',
    imgSrc: '/pngsV2/frc_ovo.png'
  }
]
export default function ManualSelection(props) {
  console.log('manulaselection props -- ',props);
  let FantasymatchID = props.matchID;
  let lang = props.language;
  const data =
    props.matchBasicData &&
    [
      ...props.matchBasicData.getFRCHomePage.completedmatches,
      ...props.matchBasicData.getFRCHomePage.livematches,
      ...props.matchBasicData.getFRCHomePage.upcomingmatches
    ].filter((x) => x.matchID === FantasymatchID);

  const Main = '/pngs/ban1.png';
  const Empty = '/svgs/rightarrow.svg';
  const backIcon = '/svgs/back_dark_black.svg';
  const activeText = ' text-green tc  ';
  const inactiveText = 'text-gray-2 tc ';
  const stadium = '/pngs/frc_stadium.png';

  const chooseLeageText = '  border-y-2 border-r-2 border-green';
  const unChooseLeageText = ' ';
  // const [currenFlag, setCurrentFlag] = useState('choose League');
  
  const [currenFlag, setCurrentFlag] = useState(props.urlData.ffCode ? 'choose Team' :'choose League');
  const [leagueType, setLeagueType] = useState(props.urlData.ffCode ? props.urlData.leagueType : '');
  const [chooseLogic, setChooseLogic] = useState(props.urlData.ffCode ? props.urlData.selectCriteria : '');
  const [playerID, setPlayerID] = useState([]);
  const [players, setPlayers] = useState([]);
  const [homeTeamNo, setHomeTeamNo] = useState(0);
  const [awayTeamNo, setAwayTeamNo] = useState(0);
  const [totalCredits, setTotalCredits] = useState(
    props.urlData.ffCode ? props.urlData.team.reduce((pl, i) => parseFloat(pl.credits) + parseFloat(i)) : 0
  );
  const [buildTeam, setBuildTeam] = useState(
    props.urlData.ffCode
      ? {
          batsman: props.urlData.team.filter((pl) => pl.player_role == 'BATSMAN'),
          bowler: props.urlData.team.filter((pl) => pl.player_role == 'BOWLER'),
          keeper: props.urlData.team.filter((pl) => pl.player_role == 'KEEPER'),
          all_rounder: props.urlData.team.filter((pl) => pl.player_role == 'ALL_ROUNDER')
        }
      : {}
  );
  console.log('currflag is => ',currenFlag);
  const handleChange = (type) => {
    const index = leagueType.indexOf(type);
    if (index > -1) {
      leagueType.splice(index, 1);

      setLeagueType([...leagueType]);
    } else {
      setLeagueType([...leagueType, type]);
    }
  };
  // let month={
  //   "january":["january","जनवरी"],
  //   "february":["february","फ़रवरी"],
  //   "march":["march","मार्च"],
  //   "april":["april","अप्रैल"],
  //   "may":["may","मई"],
  //   "june":["june","जून"],
  //   "july":["july","जुलाई"],
  //   "august":["august","अगस्त"],
  //   'September':["september","सितंबर"],
  //   "october":["october","अक्टूबर"],
  //   "november":["november","नवंबर"],
  //   "december":["december","दिसंबर"]
  // }

  const handleMonthHindiTag = () => {
    let monthformat = format(Number(data[0].matchDateTimeGMT) - 19800000, 'MMMM');

    let date = format(Number(data[0].matchDateTimeGMT) - 19800000, 'do');
    let year = format(Number(data[0].matchDateTimeGMT) - 19800000, 'yyyy');
    //  console.log("monthformat",monthformat)

    if (lang === 'HIN') {
      return getLangText(lang, words, monthformat.toLowerCase()) + ' ' + date + ' ' + year;
    } else {
      return monthformat + ' ' + date + ' ' + year;
    }
  };
  const handleMonthHindi = () => {
    let monthformat = format(Number(data[0].matchDateTimeGMT) - 19800000, 'MMMM');

    let date = format(Number(data[0].matchDateTimeGMT) - 19800000, 'do');
    // console.log("monthformat",monthformat)

    if (lang === 'HIN') {
      return getLangText(lang, words, monthformat.toLowerCase()) + ' ' + date;
    } else {
      return date + ' ' + monthformat;
    }
  };
  return (
    <div className='bg-basebg mw75-l center text-white m-3'>
      {currenFlag !== 'Loading' && (
        <div className=''>
          <div className='flex items-center justify-between pb-5'>
            <div className='flex items-center '>
              <div className='bg-gray p-3 rounded'>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  className='w-4 h-4  rotate-180'
                  onClick={() =>
                    currenFlag == 'choose players'
                      ? setCurrentFlag('choose League')
                      : currenFlag == 'choose logic'
                      ? setCurrentFlag('choose players')
                      : currenFlag == 'choose Team' && !props.urlData.ffCode
                      ? setCurrentFlag('choose logic')
                      : window.history.back()
                  }
                  alt=''
                  src={leftarrow}
                />
              </div>
              <div className='text-lg font-semibold pl-3 capitalize'>{getLangText(lang, words, 'BYT')}</div>
            </div>
            {/* <img className='w-10 h-10' src={langIcon} /> */}
          </div>

          <div className='  p-3 rounded flex items-center  bg-gray '>
            <div className='w-full'>
              {/* {console.log('ssss', data[0])} */}
              <div>
                {currenFlag === 'choose League' && (
                  <div className='text-sm font-medium'>
                    {lang === 'HIN'
                      ? data[0].matchNameHindi.split(',')[1] + ' ' + data[0].matchNameHindi.split(',')[2]
                      : data[0].tourName}
                  </div>
                )}
                {currenFlag === 'choose League' && (
                  <div className='text-sm font-semibold text-gray-2 pt-1 mb-2'>
                    {lang === 'HIN'
                      ? data[0].matchNameHindi.split(',')[1] + ' ' + data[0].matchNameHindi.split(',')[0]
                      : data[0].matchNumber}
                  </div>
                )}

                <div className='bg-gray-4 rounded p-3 '>
                  <div className='flex items-center justify-between '>
                    <div className='flex  items-center'>
                      <imImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder}g
                        alt=''
                        src={`https://images.cricket.com/teams/${data[0].homeTeamID}_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        className='h-5 w-8 shadow-4'
                      />
                      <div className='pl-2 text-sm font-semibold '>{data[0].homeTeamShortName}</div>
                    </div>
                    <div className='border-green border-2 rounded-full px-2 py-0.5 text-xs font-medium  text-green bg-gray-8'>
                      {data[0].matchType}
                    </div>
                    <div className='flex items-center'>
                      <div className=' pr-2 text-sm font-semibold'>{data[0].awayTeamShortName}</div>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder}
                        alt=''
                        src={`https://images.cricket.com/teams/${data[0].awayTeamID}_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        className='h-5 w-8 shadow-4'
                      />
                    </div>
                  </div>
                  <div className='flex mt-3 justify-center items-center text-xs font-medium'>
                    <div className=''> {handleMonthHindi()} </div>
                    <div className=''> ,{format(Number(data[0].matchDateTimeGMT) - 19800000, 'h:mm a')} IST</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='center mt-5 text-xs font-medium leading-4 px-2'>
            <div className='flex justify-around items-center  '>
              <div className={currenFlag == 'choose League' ? activeText : (currenFlag == 'choose players' || currenFlag =='choose logic' || currenFlag =='choose Team' ) ? 'text-green opacity-40' :inactiveText}>
                {lang == 'HIN' ? getLangText(lang, words, 'League') : getLangText(lang, words, 'Choose')} <br />{' '}
                {lang == 'HIN' ? getLangText(lang, words, 'Choose') : getLangText(lang, words, 'League')}
              </div>
              <div className={currenFlag == 'choose players' ? activeText : ((currenFlag == 'choose logic' || currenFlag == 'choose Team' ) ? 'text-green opacity-40' : inactiveText)}>
                {lang == 'HIN' ? getLangText(lang, words, 'PLAYERS') : getLangText(lang, words, 'Choose')} <br />{' '}
                {lang == 'ENG' ? getLangText(lang, words, 'PLAYERS') : getLangText(lang, words, 'Choose')}
              </div>

              <div className={currenFlag == 'choose logic' ? activeText :( currenFlag == 'choose Team' ) ? 'text-green opacity-40' : inactiveText}>
                {lang == 'HIN' ? getLangText(lang, words, 'LOGIC') : getLangText(lang, words, 'Choose')} <br />{' '}
                {lang == 'ENG' ? getLangText(lang, words, 'LOGIC') : getLangText(lang, words, 'Choose')}
              </div>

              <div className={currenFlag == 'choose Team' ? activeText : inactiveText}>
                {lang == 'HIN' ? getLangText(lang, words, 'TEAM') : getLangText(lang, words, 'Choose')} <br />{' '}
                {lang == 'ENG' ? getLangText(lang, words, 'TEAM') : getLangText(lang, words, 'Choose')}
              </div>
            </div>
            <div className='w-100 h-1 bg-gray mt-3 rounded-full flex justify-start'>
              <span
                className={`bg-green rounded-full ${
                  currenFlag == 'choose League'
                    ? 'w-1/4'
                    : currenFlag == 'choose players'
                    ? 'w-1/2'
                    : currenFlag == 'choose logic'
                    ? 'w-3/4'
                    : currenFlag == 'choose Team'
                    ? 'w-full'
                    : ''
                }`}></span>
            </div>
          </div>
        </div>
      )}

      {currenFlag == 'choose League' && (
        <>
          <div className='text-md font-semibold mt-4 tracking-wide'>
            {getLangText(lang, words, 'Your league')} {getLangText(lang, words, 'your preference')}
          </div>
          <div className='bg-blue-8 w-16 h-1.5 mt-1 rounded'></div>
          <div className='py-2 text-gray-2 text-xs font-base'>{getLangText(lang, words, 'SYJBCAL')}</div>

          <div className='mt-3'>

            {
              leagueTypes.map(league => (
                <div
                className={`mb-3 rounded-xl bg-gray  cursor-pointer flex items-center overflow-hidden h-20 ${
                  leagueType === league.ref ? chooseLeageText : unChooseLeageText
                }`}
                onClick={() => setLeagueType(league.ref)}>
                <div className='bg-green w-1.5 h-full '></div>
                <div className='px-3 flex items-center'>
                  <ImageWithFallback height={18} width={18} loading='lazy'
                     className={`w-16 h-16`} src={league.imgSrc} alt='' />
                  <div className=' pl-2 cursor-pointer'>
                    <div className='text-sm font-semibold'>{getLangText(lang, words, league.title)}</div>
                    <div className='h-1 rounded mt-1 w-12 bg-blue-8 '></div>
                    <div className='mt-2 text-xs  '>{getLangText(lang, words, league.desc)}</div>
                  </div>
                </div>
              </div>
              ))
            }

       
          </div>

          <div className='flex items-center mt-4 justify-end min-vh-10 mh3 cursor-pointer'>
            <div
              onClick={() => (leagueType.length > 0 ? setCurrentFlag('choose players') : '')}
              className={
                leagueType.length > 0
                  ? 'border-2 border-green bg-gray-8 tc flex item-center justify-center text-green py-1 px-3 rounded uppercase text-sm font-medium'
                  : ' border-2 border-gray-2 bg-gray-8 tc flex item-center justify-center text-gray-2 py-1 px-3 rounded uppercase text-sm font-medium'
              }>
              {getLangText(lang, words, 'PROCEED')}
            </div>
          </div>
        </>
      )}

      {currenFlag === 'choose players' && (
        <ChoosePlayer
          language={lang}
          playerID={playerID}
          setPlayerID={setPlayerID}
          leagueType={leagueType}
          setCurrentFlag={setCurrentFlag}
          setPlayers={setPlayers}
          players={players}
          rules={props.data.getPlayerSelectionComposition}
          FantasymatchID={FantasymatchID}
          teamDetails={data}
          setHomeTeamNo={setHomeTeamNo}
          setAwayTeamNo={setAwayTeamNo}
          totalCredits={totalCredits}
          setTotalCredits={setTotalCredits}
        />
      )}
      {currenFlag == 'choose logic' && (
        <ChooseLogic
          language={lang}
          chooseLogic={chooseLogic}
          setChooseLogic={setChooseLogic}
          leagueType={leagueType}
          setCurrentFlag={setCurrentFlag}
          homeTeamNo={homeTeamNo}
          awayTeamNo={awayTeamNo}
          homeTeamID={data[0].homeTeamID}
          FantasymatchID={FantasymatchID}
          awayTeamID={data[0].awayTeamID}
          totalCredits={totalCredits}
          matchType={data[0].matchType}
        />
      )}
      {currenFlag === 'Loading' && (
        <LoadingTeam
          language={lang}
          setCurrentFlag={setCurrentFlag}
          playerID={playerID}
          setPlayerID={setPlayerID}
          chooseLogic={chooseLogic}
          leagueType={leagueType}
          FantasymatchID={FantasymatchID}
          setBuildTeam={setBuildTeam}
        />
      )}
      {currenFlag === 'choose Team' && (
        <ChooseTeam
          language={lang}
          playerID={playerID}
          setPlayerID={setPlayerID}
          chooseLogic={chooseLogic}
          leagueType={leagueType}
          setCurrentFlag={setCurrentFlag}
          FantasymatchID={FantasymatchID}
          teamDetails={data}
          buildTeam={buildTeam}
          ffcode={props.urlData.ffCode ? props.urlData.ffCode : ''}
          matchType={data[0].matchType}
          editTeamName={props.urlData.ffCode ? props.urlData.teamName : ''}
        />
      )}
    </div>
  );
}
