import React, { useState } from 'react'
import ImageWithFallback from '../commom/Image'
// const frc_allround='/svgs/frc_allround.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
export default function MatchUpsModel(props) {
  // alert(8)
  const [type, setType] = useState('Wicket Keeper')

  let filterRole = { Bowler: 'bowler', Batsman: 'batsman' }
  // console.log("Props model", props)
  return (
    <div
      className="flex  justify-center items-center fixed absolute--fill z-9999 bg-basebg/30 overflow-y-scroll"
      style={{ backdropFilter: 'blur(4px)' }}
    >
      <div className=" bg-gray-4 br3 shadow-4  ba b--white-20 relative  min-vh-90 pt2 mh2  w-100 w-50-l">
        <div className="w-100  flex  flex-row ">
          <div className="flex w-80 justify-start f5 fw6 white ph2 items-center ttu">
            {props.lang === 'HIN' ? 'वर्तमान चयन' : 'current selection'}
          </div>
          <div
            className="flex w-20 justify-end items-center white fw6 ph3 pv2 cursor-pointer"
            onClick={() => {
              props.closeNewModelV2()
            }}
          >
            X
          </div>
        </div>
        <div className="w-100">
          <div className="flex  items-center mv2    justify-between  bg-white-10 ba b--yellow ph2 pv1 br2  ma2 ">
            <div className="flex items-center">
              <div className="w2 h2 br-100  ba b--white-20 bg-gray overflow-hidden ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/pngs/fallbackprojection.png"
                  className="w-100 h-100 object-cover object-top"
                  src={`https://images.cricket.com/players/${props.Player2Obj.player2}_headshot_safari.png`}
                  alt=""
                />
              </div>
              <div className="ttu f5 oswald fw6 ttu ph2">
                {props.lang === 'HIN'
                  ? props.Player2Obj.player2HindiName
                  : props.Player2Obj.player2Name}
              </div>
            </div>
            <div className="flex items-center">
              <div className="ttu f5 fw6 oswald gray ttu ph2">{''}</div>
              <div className="w15 h15 br-100 overflow-hidden ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc={flagPlaceHolder}
                  className="w-100 h-100 object-cover"
                  src={`https://images.cricket.com/teams/${props.Player2Obj.player2Team}_flag_safari.png`}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
        <div className="mh2 mt4 mb2 bb b--white-20"></div>
        <div className=" f5 fw6 white ph2  ttu">
          {props.lang === 'HIN' ? 'खिलाड़ी बदलें' : 'CHANGE PLAYER'}
        </div>

        <div
          className=" overflow-y-scroll overflow-hidden hidescroll "
          style={{ maxHeight: '57vh' }}
        >
          {props.playersList
            .filter(
              (player) =>
                player.player1 === props.RecentPlayer.playerID &&
                player.player2 !== props.Player2Obj.player2,
            )
            .map((player, i) => (
              <div
                className="w-100"
                key={i}
                onClick={() => props.choosePlayer(player, props.SelectionType)}
              >
                <div className="flex  items-center mv2  ba b--gray justify-between  bg-white-10  ph2 pv1 br2  ma2 ">
                  <div className="flex items-center">
                    <div className="w2 h2 br-100  ba b--white-20 bg-gray overflow-hidden ">
                      {/* <img className="w-100 h-100 object-cover object-top" src={`https://images.cricket.com/players/${props.RecentPlayer.playerID===player.player1?player.player2:player.player1}_headshot_safari.png`} alt="" onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}></img> */}
                    </div>
                    {props.lang === 'HIN' ? (
                      <div className="ttu f5 oswald fw6 ttu ph2">
                        {props.RecentPlayer.playerID === player.player1
                          ? player.player2HindiName
                          : player.player1HindiName}
                      </div>
                    ) : (
                      <div className="ttu f5 oswald fw6 ttu ph2">
                        {props.RecentPlayer.playerID === player.player1
                          ? player.player2Name
                          : player.player1Name}
                      </div>
                    )}
                  </div>
                  <div className="flex items-center">
                    <div className="ttu f5 fw6 oswald gray ttu ph2">{''}</div>
                    <div className="w15 h15 br-100 overflow-hidden ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="w-100 h-100 object-cover"
                        src={`https://images.cricket.com/teams/${
                          props.RecentPlayer.playerID === player.player1
                            ? player.player2Team
                            : player.player1Team
                        }_flag_safari.png`}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  )
}
