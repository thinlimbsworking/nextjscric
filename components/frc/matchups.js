import React, { useEffect, useState } from 'react'
import SwiperModule from '../test'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING } from '../../api/queries'
import ImageWithFallback from '../commom/Image'
export default function Matchups(props) {
  const [toggle, setToggle] = useState(0)
  const [tabIndex, setTabIndex] = useState(0)
  const [bowler, setBowler] = useState(0)
  //  console.log("MatchupsMatchupsMatchupsMatchups",props)

  const { loading, error, data } = useQuery(
    HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING,
    {
      variables: { matchID: props.matchID },
    },
  )
  const capsuleCss = {
    Strong: ' text-green  text-sm  font-bold text-center',
    Average: ' text-yellow  text-sm  font-bold text-center',
    Weak: ' text-red  text-sm  font-bold text-center',
  }

  return data &&
    data.HomeScreenPlayerMatchupsUpcomming &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 ? (
    <>
      <div className="rounded mx-2 text-white lg:hidden md:hidden">
        <div className=" uppercase text-left">Matchups</div>
        <div className="flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded"></div>

        {/* <div className='flex w-full justify-between items-center mt-5 '>
        <div className=' w-3/12 text-gray-2 text-xs flex justify-between items-center  '> <span className='h-1 w-1 bg-green rounded-full'></span> BATTING</div>

        <div className='bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-9/12 p-1'>
          <div
            className={`w-1/2 text-center rounded-3xl py-2  ${toggle === 0 ? `border-2  border-green bg-gray-8` : ''}`}
            onClick={() => (setTabIndex(0), setToggle(0))}>
            {data &&
              data.HomeScreenPlayerMatchupsUpcomming &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamShortName}
          </div>
          <div
            className={`w-1/2 text-center rounded-3xl py-2 ${toggle === 1 ? `border-2  border-green bg-gray-8` : ''}`}
            onClick={() => (setTabIndex(0), setToggle(1))}>
            {data && data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamShortName}
          </div>
        </div>
      </div> */}

        {/* <SwiperModule
        toggle={toggle}
        homeTeamID={data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamID}
        awayTeamID={data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamID}
        tabIndex={tabIndex}
        setTabIndex={setTabIndex}
        data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
      /> */}
        <div className="flex   justify-center items-center">
          <div
            className={`flex   w-5/12 justify-center items-center bg-gray h-40 relative rounded-xl mb-3 `}
          >
            <div className="flex flex-col relative items-center justify-center  ">
              <div className="flex relative items-center justify-center ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/pngsV2/playerph.png"
                  className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                  src={`https://images.cricket.com/players/${
                    props.RecentPlayer && props.RecentPlayer.playerID
                  }_headshot_safari.png`}
                  // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                />

                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc="/svgs/images/flag_empty.svg"
                  className=" absolute h-6 w-6  border  left-0 bottom-0 object-fill"
                  src={'/pngsV2/bowlericon.png'}
                />

                <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    fallbackSrc="/pngsV2/playerph.png"
                    className="  h-5 w-5  rounded-full"
                    src={`https://images.cricket.com/teams/${
                      props.RecentPlayer && props.RecentPlayer.teamID
                    }_flag_safari.png`}
                    // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                  />
                </div>
              </div>

              <div className=" text-center  text-xs font-semibold mt-2 ">
                {props.RecentPlayer && props.RecentPlayer.name}
              </div>
            </div>
          </div>
        </div>

        <div className="flex bg-gray items-center justify-between rounded py-3">
          <div className="flex w-1/3 flex-col  items-center justify-center  ">
            <div className="text-xs font-medium uppercase text-gray-2">
              Balls{' '}
            </div>
            <div className="uppercase text-xl font-semibold  ">
              {props.ballFaced}
            </div>
          </div>
          <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
            <div className="uppercase text-xs font-medium text-gray-2">
              RUNS{' '}
            </div>
            <div className="uppercase text-xl font-semibold">
              {/* {data &&
              data.HomeScreenPlayerMatchupsUpcomming &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[tabIndex].bowlers[bowler].runsScored} */}
              {props.runs}
            </div>
          </div>
          <div className="flex flex-col  w-1/3  items-center justify-center">
            <div className="uppercase text-xs font-medium text-gray-2">
              WICKETS{' '}
            </div>
            <div className="uppercase text-xl font-semibold">
              {props.wickets}
            </div>
          </div>
        </div>
        {console.log('=>>>>>>>>>>', props.RecentPlayer)}

        <div className="  flex flex-wrap justify-center  mt-4 items-center">
          {props.playersList &&
            props.playersList
              .filter(
                (player) => player.player1 === props.RecentPlayer.playerID,
              )
              .map((item, i) => {
                return (
                  <div
                    key={i}
                    className={`flex w-5/12 my-2 mx-3 justify-center bg-gray h-40 relative rounded-xl ${
                      i === bowler ? 'border-green border' : ''
                    }`}
                    onClick={() => setBowler(i)}
                  >
                    <div className="flex flex-col relative items-center justify-center ">
                      <div className="flex relative items-center justify-center ">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc="/pngsV2/playerph.png"
                          className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${item.player2}_headshot_safari.png`}
                          // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                        />

                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc="/pngsV2/playerph.png"
                          className=" absolute h-6 w-6    left-0 bottom-0 object-fill"
                          src={'/pngsV2/bowlericon.png'}
                        />

                        <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            fallbackSrc="/svgs/images/flag_empty.svg"
                            className="  h-5 w-5  rounded-full"
                            src={`https://images.cricket.com/teams/${
                              toggle === 0
                                ? data.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[1].awayTeamID
                                : data.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[0].homeTeamID
                            }_flag_safari.png`}
                            // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                          />
                        </div>
                      </div>

                      <div className=" text-center  text-xs font-semibold mt-2 ">
                        {item.player2Name}
                      </div>
                      <div className={`mt-2  ${capsuleCss[item.label]}`}>
                        {item.label}
                      </div>
                    </div>
                  </div>
                )
              })}
        </div>
      </div>

      {/* desktop */}
      <div className="mt-5 rounded mx-2 text-white hidden lg:block md:block  ">
        {/* 
     <div className='flex w-full justify-between items-center mt-5  my-3'>
       <div className=' w-6/12 text-gray-2 text-xs flex  items-center  '><span className='mx-2 h-2 w-2 bg-green rounded-full'></span> BATTING</div>

       <div className='bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-4/12 '>
         <div
           className={`w-1/2 text-center rounded-3xl py-1  ${toggle === 0 ? `border-2  border-green bg-gray-8` : ''}`}
           onClick={() => (setTabIndex(0), setToggle(0))}>
           {data &&
             data.HomeScreenPlayerMatchupsUpcomming &&
             data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
             data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamShortName}
         </div>
         <div
           className={`w-1/2 text-center rounded-3xl py-1 ${toggle === 1 ? `border-2  border-green bg-gray-8` : ''}`}
           onClick={() => (setTabIndex(0), setToggle(1))}>
           {data && data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamShortName}
         </div>
       </div>
     </div> */}
        <div className="flex  items-center justify-center">
          <div className="w-6/12">
            {' '}
            <div className="flex   justify-center items-center">
              <div
                className={`flex   w-10/12 justify-center items-center bg-gray h-40 relative rounded-xl mb-3 `}
              >
                <div className="flex flex-col relative items-center justify-center  ">
                  <div className="flex relative items-center justify-center ">
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      fallbackSrc="/pngsV2/playerph.png"
                      className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                      src={`https://images.cricket.com/players/${
                        props.RecentPlayer && props.RecentPlayer.playerID
                      }_headshot_safari.png`}
                      // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                    />

                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      fallbackSrc="/pngsV2/playerph.png"
                      className=" absolute h-6 w-6  border  left-0 bottom-0 object-fill"
                      src={'/pngsV2/bowlericon.png'}
                    />

                    <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc="/svgs/images/flag_empty.svg"
                        className="  h-5 w-5  rounded-full"
                        src={`https://images.cricket.com/teams/${
                          props.RecentPlayer && props.RecentPlayer.teamID
                        }_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />
                    </div>
                  </div>

                  <div className=" text-center  text-xs font-semibold mt-2 ">
                    {props.RecentPlayer && props.RecentPlayer.name}
                  </div>
                </div>
              </div>
            </div>
            {/* <SwiperModule
       toggle={toggle}
       homeTeamID={data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamID}
       awayTeamID={data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamID}
       tabIndex={tabIndex}
       setTabIndex={setTabIndex}
       data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
     /> */}
          </div>

          <div className=" w-6/12 bg-gray flex items-center justify-center rounded  h-28">
            <div className="flex w-1/3 flex-col  items-center justify-center  ">
              <div className="text-xs font-medium uppercase text-gray-2">
                Balls{' '}
              </div>
              <div className="uppercase text-xl font-semibold  ">
                {props.ballFaced}
              </div>
            </div>
            <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
              <div className="uppercase text-xs font-medium text-gray-2">
                RUNS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].runsScored}
              </div>
            </div>
            <div className="flex flex-col  w-1/3  items-center justify-center">
              <div className="uppercase text-xs font-medium text-gray-2">
                WICKETS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].wicket}{' '}
              </div>
            </div>
          </div>
        </div>

        <div className="  flex flex-wrap justify-center  mt-4 items-center">
          {props.playersList &&
            props.playersList
              .filter(
                (player) => player.player1 === props.RecentPlayer.playerID,
              )
              .map((item, i) => {
                return (
                  <div
                    key={i}
                    className={`flex w-5/12 my-2 mx-3 justify-center bg-gray h-40 relative rounded-xl ${
                      i === bowler ? 'border-green border' : ''
                    }`}
                    onClick={() => setBowler(i)}
                  >
                    <div className="flex flex-col relative items-center justify-center ">
                      <div className="flex relative items-center justify-center ">
                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc="/pngsV2/playerph.png"
                          className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${item.player2}_headshot_safari.png`}
                          // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                        />

                        <ImageWithFallback
                          height={18}
                          width={18}
                          loading="lazy"
                          fallbackSrc="/svgs/images/flag_empty.svg"
                          className=" absolute h-6 w-6  border  left-0 bottom-0 object-fill"
                          src={'/pngsV2/bowlericon.png'}
                        />

                        <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            fallbackSrc="/svgs/images/flag_empty.svg"
                            className="  h-5 w-5  rounded-full"
                            src={`https://images.cricket.com/teams/${
                              toggle === 0
                                ? data.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[1].awayTeamID
                                : data.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[0].homeTeamID
                            }_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/svgs/images/flag_empty.svg')
                            }
                          />
                        </div>
                      </div>

                      <div className=" text-center  text-xs font-semibold mt-2 ">
                        {item.player2Name}
                      </div>
                      <div className={`mt-2  ${capsuleCss[item.label]}`}>
                        {item.label}
                      </div>
                    </div>
                  </div>
                )
              })}
        </div>
      </div>
    </>
  ) : (
    <></>
  )
}
