import React, { useState, useEffect, useRef, Suspense, lazy } from 'react'
import Countdown from 'react-countdown-now'
import Link from 'next/link'
import { getLangText } from '../../api/services'
import { words } from '../../constant/language'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { GET_ALL_USER_TEAM, EDIT_MY_FANTASY_TEAM_NAME } from '../../api/queries'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import TeamSlider from '../commom/teamslider'
import Loading from '../loading'
import CleverTap from 'clevertap-react'
const backIcon = '/svgs/back_dark_black.svg'
const stadium = '/pngs/frc_stadium.png'
// import div from 'react-id-div';
import { getFrcSeriesTabUrl } from '../../api/services'
const flagPlaceHolder = '/pngsV2/flag_dark.png'
// import { navigate } from '@reach/router'
const playerPlaceholder = '/pngs/fallbackprojection.png'
import { format } from 'date-fns'
import { useRouter } from 'next/router'
import DataNotFound from '../commom/datanotfound'
import Algo11TeamDisplay from './algo11TeamDisplay'
import ImageWithFallback from '../commom/Image'

const Modal = lazy(() => import('./editTeamNameModal'))
const editTeam = '/svgs/editTeam.svg'
const copyCode = '/svgs/copyCode.svg'
const addTeam = '/svgs/addTeam.svg'

const Empty = '/svgs/Empty.svg'

export default function MyTeams(props) {
  let scrl = useRef(null)
  const [scrollX, setscrollX] = useState(0)
  const [scrolEnd, setscrolEnd] = useState(false)

  const [close, setclose] = useState(false)

  //Slide click
  const slide = (shift) => {
    scrl.current.scrollLeft += shift
    setscrollX(scrollX + shift)

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }
  const scrollCheck = () => {
    setscrollX(scrl.current.scrollLeft)
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true)
    } else {
      setscrolEnd(false)
    }
  }
  ;<style jsx>{`
    -webkit-scrollbar {
      -webkit-appearance: none !important;
      width: 2px !important;
      border-radius: !important;
      height: 4px !important;
      background-color: #2b323f !important;
    }
    ::-webkit-scrollbar:hover {
      -webkit-appearance: none;
      width: 2px !important;
      border-radius: 2px !important;
      height: 4px !important;
      background-color: #2b323f !important;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px !important;
      background-color: #6a91a9 !important;
    }
  `}</style>

  const lang = props.language
  // console.log("lang my saved team",lang)
  let router = useRouter()
  let FantasymatchID = props.matchID
  // const teamDetails = (props.matchBasicData && [...props.matchBasicData.getFRCHomePage.completedmatches, ...props.matchBasicData.getFRCHomePage.livematches, ...props.matchBasicData.getFRCHomePage.upcomingmatches].filter((x) => x.matchID === FantasymatchID)).length > 0 ?
  //   [...props.matchBasicData.getFRCHomePage.completedmatches, ...props.matchBasicData.getFRCHomePage.livematches, ...props.matchBasicData.getFRCHomePage.upcomingmatches].filter((x) => x.matchID === FantasymatchID) :
  //   [{
  //     ...props.data.miniScoreCard.data[0],
  //     awayTeamShortName: props.data.miniScoreCard.data[0].awayTeamName,
  //     homeTeamShortName: props.data.miniScoreCard.data[0].homeTeamName,
  //     MatchName: props.data.miniScoreCard.data[0].matchName
  //   }]

  const teamDetails = {
    ...props.data.miniScoreCard?.data[0],
    awayTeamShortName: props.data.miniScoreCard?.data[0].awayTeamName,
    homeTeamShortName: props.data.miniScoreCard?.data[0].homeTeamName,
    MatchName: props.data.miniScoreCard?.data[0].matchName,
  }

  // console.log("teamDetails",teamDetails)
  const [click, setclick] = useState(false)
  const [click2, setclick2] = useState(false)

  const [index, setIndex] = useState()
  const [toggle, setToggle] = useState(false)
  const [windows, setLocalStorage] = useState({})
  const [newToken, setToken] = useState('')
  const [copyAllCodes, setCopyAllCodes] = useState(false)
  const [teamCode, setTeamCode] = useState('')
  const [teamCodeClick, setteamCodeClick] = useState()
  const [myTeam, setmyTeam] = useState(null)
  const [selectedTeam, setSelectedTeam] = useState({})
  const [editNameModal, seteditNameModal] = useState(false)
  const [currenteditTeam, setCurrentEditTeam] = useState(null)
  const [name, setName] = useState('')

  useEffect(() => {
    console.log('propsssssssss', props)
    if (global.window) {
      setLocalStorage({ ...global.window })
      setToken(global.window.localStorage.getItem('tokenData'))
    }
  }, [global.window])

  const { loading, error, data: allUserTeamData, refetch } = useQuery(
    GET_ALL_USER_TEAM,
    {
      variables: { matchID: FantasymatchID, token: newToken },
      // fetchPolicy:'no-cache',
      // pollInterval:5000,
      onError: (err) => {
        console.log(err, 'error')
      },
      onCompleted: (res) => {
        console.log('myteam sres - ', res)
      },
    },
  )

  React.useEffect(() => {
    if (!loading) {
      let sortedData =
        allUserTeamData &&
        allUserTeamData.getUserAllFrcTeams &&
        allUserTeamData.getUserAllFrcTeams.teams &&
        allUserTeamData.getUserAllFrcTeams.teams.map((teams) => {
          //   let batsman=player.team.batsman?player.team.batsman:[]
          //   let all_rounder=player.team.all_rounder?player.team.all_rounder:[]
          //   let keeper=player.team.keeper?player.team.keeper:[]
          //   let bowler=player.team.bowler?player.team.bowler:[]
          // let arrayMerge=[...batsman,...all_rounder,...keeper,...bowler];
          // let arrayMerge=[...team.batsman, ...team.all_rounder, ...team.keeper, ...team.bowler];
          let captain = teams.team.filter((player) => player.captain === '1')
          // console.log("captain",captain)
          let viceCaptain = teams.team.filter(
            (player) => player.vice_captain === '1',
          )
          //  console.log("viceCaptain",viceCaptain)
          let rem_player = teams.team.filter(
            (player) => player.vice_captain !== '1' && player.captain !== '1',
          )
          // console.log("rem_player",rem_player)

          return {
            ffCode: teams.ffCode,
            leagueType: teams.leagueType,
            selectCriteria: teams.selectCriteria,
            teamName: teams.teamName,
            team: [...captain, ...viceCaptain, ...rem_player],
          }
        })

      let saveTeamData = {
        ffCode: allUserTeamData.getUserAllFrcTeams.ffCode,
        matchID: allUserTeamData.getUserAllFrcTeams.matchID,
        teams: sortedData,
      }

      setmyTeam(saveTeamData)
    }
  }, [allUserTeamData, loading])

  const [updateTeamname] = useMutation(EDIT_MY_FANTASY_TEAM_NAME, {
    onCompleted: (data) => {
      if (data) {
        console.log('teamNameEditSuccessFully', data)
        if (data.editMyFantasyTeamName) {
          refetch({ matchID: props.matchID, token: newToken })
        }
      }
    },
  })

  const handleEditTeam = (item) => {
    props.setUrlData(item)
    props.setviewTeamStatsHub(
      item.leagueType === 'statsHub' ? 'viewstatshubteam' : '',
    )
  }

  const fantasyTrack = () => {
    CleverTap.initialize('FantasyBYT', {
      Source: 'NewTeam',
      MatchID: props.matchID,
      SeriesID: props.seriesID || '',
      TeamAID: teamDetails.homeTeamID,
      TeamBID: teamDetails.awayTeamID,
      MatchFormat: teamDetails.matchType,
      MatchStartTime: format(
        Number(teamDetails.matchDateTimeGMT),
        'd,y, h:mm a',
      ),
      MatchStatus: teamDetails.matchStatus,
      Platform:
        windows && windows.localStorage ? windows.localStorage.Platform : '',
    })
  }

  if (loading) {
    return (
      <div>
        <Loading />
      </div>
    )
  }
  if (error) {
    return (
      <div>
        <DataNotFound />
      </div>
    )
  }

  console.log('algo team passing datas - ', selectedTeam)
  if (close) {
    return (
      <Algo11TeamDisplay
        setclose={setclose}
        handleEditTeam={() => handleEditTeam(selectedTeam)}
        myTeam={{ data: selectedTeam.team }}
        leagueType={selectedTeam.leagueType}
        teamName={selectedTeam.teamName}
        lang={lang}
        matchID={props.matchID}
        seriesSlug={props.seriesSlug}
        calledBy={'myTeams'}
      />
    )
  } else
    return newToken ? (
      <div className="mw75-l  center bg-basebg min-vh-100">
        <div className=" z-1 ph2 h2-4 lg:hidden md:hidden  mt-2 mx-2 flex items-center">
          <div className="p-2 bg-gray rounded-md ">
            <ImageWithFallback
              height={18}
              width={18}
              loading="lazy"
              onClick={() =>
                router.push(
                  `/fantasy-research-center/${router.query.slugs[0]}/${router.query.slugs[1]}`,
                )
              }
              className=" flex items-center justify-center h-4 w-4 rotate-180"
              src="/svgsV2/RightSchevronWhite.svg"
              alt=""
            />
          </div>

          {/* <img className="w1" onClick={() => router.push(`/fantasy-research-center/${router.query.slugs[0]}/${router.query.slugs[1]}`)} alt='' src={backIcon} /> */}
          <div className="black f6 fw7 pl-3  ttu tc">
            {lang === 'HIN' ? 'माई सेव टीम' : 'My Saved teams'}
          </div>
        </div>
        <div className="flex p-3 rounded border bg-gray  flex-col  items-center justify-between border-green m-4">
          <div className="flex w-full  items-center justify-between ">
            <div className="flex  items-center justify-center ">
              <div>
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc={flagPlaceHolder}
                  className="h-7 w-12 object-cover mx-2"
                  alt=""
                  src={`https://images.cricket.com/teams/${teamDetails.homeTeamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                />
              </div>
              <div className="items-center justify-center">
                {teamDetails.homeTeamShortName}
              </div>
            </div>
            <div className="border flex items-center justify-center  border-green bg-basebg rounded-2xl px-2 h-8 w-16 text-center text-green">
              {teamDetails.matchType}
            </div>
            <div className="flex  items-center">
              <div className="oswald f5 f3-l f3-m fw5 white pr-2">
                {teamDetails.awayTeamShortName}
              </div>
              <div>
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  fallbackSrc={flagPlaceHolder}
                  alt=""
                  src={`https://images.cricket.com/teams/${teamDetails.awayTeamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                  className="h-7 w-12 object-cover"
                />
              </div>
            </div>
          </div>
          <div className="mt-2 text-xs  ">
            {' '}
            {console.log(
              'matchdate = ',
              new Date(Number(teamDetails.matchDateTimeGMT) - 19800000),
            )}
            {new Date(Number(teamDetails.matchDateTimeGMT) - 19800000) -
              new Date().getTime() <
            84600000 ? (
              <Countdown
                date={new Date(Number(teamDetails.matchDateTimeGMT) - 19800000)}
                renderer={(props) => (
                  <div className="pa1 ph2 br2 f8 f7-l f7-m tc fw6 white bg-darkRed truncate flex">
                    <div className="pr-1"> {`Starts in `} </div>
                    <div>{`${
                      props.days !== 0
                        ? props.days * 24 + Number(props.hours)
                        : props.hours
                    } h ${props.minutes} m ${props.seconds} s `}</div>
                  </div>
                )}
              />
            ) : (
              <div className="pa1 ph2 br2 f8 f7-l f7-m fw6 white bg-darkRed truncate">
                <div>
                  {`${
                    teamDetails?.matchDateTimeGMT &&
                    format(
                      Number(teamDetails?.matchDateTimeGMT) - 19800000,
                      'do MMMM yyyy',
                    )
                  }`}
                </div>
                <div>
                  {`${
                    teamDetails?.matchDateTimeGMT &&
                    format(
                      Number(teamDetails?.matchDateTimeGMT) - 19800000,
                      'h:mm a',
                    )
                  }`}
                </div>
              </div>
            )}
          </div>
        </div>

        {myTeam && myTeam && myTeam.teams ? (
          <div className="p-3  ">
            {myTeam &&
              myTeam.teams &&
              myTeam.teams.map((item, index) => {
                return (
                  <div className="flex items-center flex-col ">
                    <div className="flex justify-between w-full ">
                      <div className="flex  flex-col text-base font-bold">
                        <div className="flex space-x-3">
                          <div> {item.teamName || `My Team ${index + 1}`} </div>
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            src="/pngs/editpencil.png"
                            role="button"
                            className="h-6 w-6"
                            onClick={() => {
                              seteditNameModal((prev) => !prev)
                              setCurrentEditTeam(item)
                            }}
                          />
                        </div>

                        <div className="flex h-1 w-16 bg-blue-8 mt-1 " />
                      </div>
                      <div
                        className="flex border cursor-pointer border-green bg-gray-4  text-xs items-center justify-center p-1 rounded text-green"
                        onClick={() => {
                          setSelectedTeam(item)
                          setclose(true)
                        }}
                      >
                        {getLangText(lang, words, 'view_team')}
                      </div>
                    </div>

                    <div className="flex  w-full items-center">
                      <div
                        className="flex z-50   hidden items-center "
                        onClick={() => slide(-50)}
                      >
                        <svg width="30" focusable="false" viewBox="0 0 24 24">
                          <path
                            fill="#38d925"
                            d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                      <div className=" w-full bg-gray-4   my-3 rounded-md">
                        <div className="">
                          <div className="p-2 flex items-center justify-between">
                            <div className="text-xs font-medium">
                              Match {index},
                              {props &&
                                props.data &&
                                props.data.miniScoreCard?.data[0].seriesName}
                            </div>
                            <div
                              className="text-white cursor-pointer text-xs p-2 bg-basebg  items-center justify-center rounded  font-medium"
                              onClick={() => handleEditTeam(item)}
                            >
                              <Link
                                href={{
                                  pathname:
                                    '/fantasy-research-center/[...slugs]',
                                }}
                                as={`/fantasy-research-center/${
                                  props.matchID
                                }/${props.seriesSlug}/${
                                  item.leagueType === 'statsHub'
                                    ? 'fantasy-stats/players'
                                    : 'create-team'
                                }`}
                                passHref
                                legacyBehavior
                              >
                                <div className="ttu  flex f6 fw6 ph3 bg-dark-gray  pa2 br2 db ">
                                  <div className="pl1">
                                    {lang === 'HIN'
                                      ? 'टीम एडिट करें'
                                      : 'EDIT TEAM'}
                                  </div>
                                </div>
                              </Link>
                            </div>
                          </div>

                          <div className="flex  rounded-lg p-1 ">
                            <div
                              className="flex bg-gray   overflow-x-auto "
                              ref={scrl}
                              onScroll={scrollCheck}
                            >
                              {item.team.map((item, y) => {
                                return (
                                  <div className=" mr-2  border-r-2 border-gray-4   p-2 px-4  w-32">
                                    <div className="flex w-full justify-center items-center">
                                      <div className="flex flex-col items-center ">
                                        <div className="relative mb-2">
                                          <ImageWithFallback
                                            height={33}
                                            width={33}
                                            loading="lazy"
                                            fallbackSrc="/pngsV2/playerph.png"
                                            className="h-16 w-16 bg-gray-4 rounded-full  object-cover object-top"
                                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                                            // onError={(evt) =>
                                            //   (evt.target.src =
                                            //     "/pngsV2/playerph.png")
                                            // }
                                          />
                                          <ImageWithFallback
                                            height={18}
                                            width={18}
                                            loading="lazy"
                                            fallbackSrc="/pngsV2/playerph.png"
                                            className="w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba"
                                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                            // onError={(evt) =>
                                            //   (evt.target.src =
                                            //     "/pngsV2/flag_dark.png")
                                            // }
                                          />
                                          {(item.captain == '1' ||
                                            item.vice_captain == '1') && (
                                            <div className="w-5 border text-green bg-gray-4 h-5 absolute text-xs right-[-4px] bottom-0 rounded-md ba flex justify-center items-center text-xs font-medium">
                                              {item.captain == '1'
                                                ? 'C'
                                                : item.vice_captain == '1'
                                                ? 'VC'
                                                : ''}
                                            </div>
                                          )}
                                        </div>
                                        <p
                                          className="text-xs text-center font-medium leading-5 w-full wordWrapLine"
                                          style={{ wordWrap: 'break-word' }}
                                        >
                                          {item.playerName}
                                        </p>
                                        <p className="text-gray-2 text-center text-xs uppercase">
                                          {getLangText(
                                            lang,
                                            words,
                                            item.player_role,
                                          )}
                                        </p>
                                      </div>
                                      {/* <div className='bg-black w-[1px] h-10'></div> */}
                                    </div>
                                  </div>
                                )
                              })}

                              {/* bowler */}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div
                        className=" hidden  flex items-center justify-center"
                        onClick={() => slide(+50)}
                      >
                        <svg width="30" viewBox="0 0 24 24">
                          <path
                            fill="#38d925"
                            d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                    </div>
                  </div>
                )
              })}
          </div>
        ) : (
          <></>
        )}
        <Suspense fallback={<div>Loading...</div>}>
          {editNameModal && (
            <Modal
              title={getLangText(lang, words, 'teamNameChangeModalTitle')}
              submitMsg={getLangText(lang, words, 'teamNameChangeSuccessMsg')}
              placeholderText={getLangText(
                lang,
                words,
                'temNameChangeModalPlaceholder',
              )}
              showModal={editNameModal}
              setshowModal={seteditNameModal}
              errorText={getLangText(lang, words, 'PickDifferentTeamName')}
              name={name}
              setName={setName}
              submitCallback={() => {
                return updateTeamname({
                  variables: {
                    matchID: props.matchID,
                    newTeamName: name,
                    oldTeamName: currenteditTeam.teamName,
                    token: newToken,
                  },
                })
              }}
              submitButtonText={getLangText(
                lang,
                words,
                'teamNameChangeModalSubmitButton',
              )}
            />
          )}
        </Suspense>

        {/* <TeamSlider data={myTeam && myTeam}  /> */}

        <div className=" bottom-0  lg:hidden md:hidden fixed z-999 left-0  right-0  ">
          <div className=" bg-basebg  py-2 ">
            <div className="flex justify-center items-center white fw6 ">
              <Link
                {...getFrcSeriesTabUrl(props, 'create-team')}
                passHref
                legacyBehavior
              >
                <div
                  className="dib border-green uppercase text-xs p-1 border-2  text-green ml-2 w-5/12 w-20-l db pa2 flex br3 ba bg-white-10 b--white-20 justify-center items-center f6 "
                  onClick={() => {
                    props.setUrlData({}), fantasyTrack()
                  }}
                >
                  <ImageWithFallback
                    height={18}
                    width={18}
                    loading="lazy"
                    className="w-3 mx-1 "
                    src={addTeam}
                  />
                  <span className="ttu">
                    {getLangText(lang, words, 'build_new_teams')}
                  </span>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="hidden md:block lg:block ">
          <div className=" bg-basebg w-100 w-full text-center  shadow-4 h-33 flex items-center">
            <div className="w-full">
              <div className="flex justify-center items-center text-white  ">
                <Link
                  {...getFrcSeriesTabUrl(props, 'create-team')}
                  passHref
                  legacyBehavior
                >
                  <div
                    className="cursor-pointer border-green uppercase text-xs p-1 border-2  text-green ml-2 w-5/12 w-20-l db pa2 flex br3 ba bg-white-10 b--white-20 justify-center items-center f6 "
                    onClick={() => {
                      props.setUrlData({}), fantasyTrack()
                    }}
                  >
                    <ImageWithFallback
                      height={18}
                      width={18}
                      loading="lazy"
                      className="w-3 mx-1 "
                      src={addTeam}
                    />
                    <span className="ttu">
                      {getLangText(lang, words, 'build_new_teams')}
                    </span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        {click && (
          <div
            className="flex justify-center items-center fixed absolute--fill z-9999  overflow-y-scroll"
            style={{ backdropFilter: 'blur(10px)' }}
          >
            <div className="bg-gray border m-3 w-90  w-25-ns relative white pb-3">
              <ImageWithFallback
                height={18}
                width={18}
                loading="lazy"
                className="h-3 -mt-6 w08 pa2 bg-white-20 right-0 top--2 absolute br-100 cursor-pointer"
                onClick={() => setclick(false)}
                src={'/svgs/close.png'}
                alt="close icon"
              />{' '}
              <div className="flex justify-center items-center p-2 ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  src="/pngs/A23.png"
                  className="w-16 h-16"
                  alt=""
                />
              </div>
              <div className=" flex flex-col items-center justify-center">
                <div className="flex pa2 flex-row f7 ph2 ">
                  <div className="br-100 mt1 w04 h04 bg-white"></div>
                  <div className="fw2 f7 ml2 text-white text-base  px-8 ">
                    {' '}
                    . {getLangText(lang, words, 'Open_ff_message')}
                  </div>
                </div>
                <div className="flex justify-center items-center mt3 uppercase font-bold">
                  {getLangText(lang, words, 'team_code')}
                </div>
                <div className="flex justify-center items-center mt3">
                  <div className="gray br2 bw1 ph2 ba b--white-10 bg-basebg p-2">
                    <div className="ttu pa2 text-orange flex justify-center items-center orange f4 fw2">
                      {myTeam && myTeam.ffCode}
                    </div>
                  </div>
                </div>
                <div
                  className={`tc pv2  text-green bg-basebg ${
                    copyAllCodes ? 'bg-basebg' : 'bg-darkRed'
                  } mt-2 border border-green white tc w-60 f5 fw6 cursor-pointer center br2 mt3 mb2 ttu cursor-pointer flex justify-center`}
                >
                  <CopyToClipboard
                    text={myTeam && myTeam.ffCode}
                    onCopy={() => {
                      setCopyAllCodes(true)
                    }}
                  >
                    {lang === 'HIN' ? (
                      <div>{`${
                        copyAllCodes
                          ? getLangText(lang, words, 'copied')
                          : getLangText(lang, words, 'Copy Code')
                      }`}</div>
                    ) : (
                      <div>{`${copyAllCodes ? 'Copied' : 'Copy Code'}`}</div>
                    )}
                    {/* <div>{`${copyAllCodes ? 'Copied' : 'Copy Code'}`}</div> */}
                  </CopyToClipboard>
                </div>
              </div>
            </div>
          </div>
        )}

        {click && (
          <div
            className="flex justify-center items-center fixed absolute--fill z-9999  overflow-y-scroll"
            style={{ backdropFilter: 'blur(10px)' }}
          >
            <div className="bg-gray border m-3 w-90  w-25-ns relative white pb-3">
              <ImageWithFallback
                height={18}
                width={18}
                loading="lazy"
                className="h-3 -mt-6 w08 pa2 bg-white-20 right-0 top--2 absolute br-100 cursor-pointer"
                onClick={() => setclick(false)}
                src={'/svgs/close.png'}
                alt="close icon"
              />{' '}
              <div className="flex justify-center items-center p-2 ">
                <ImageWithFallback
                  height={18}
                  width={18}
                  loading="lazy"
                  src="/pngs/A23.png"
                  className="w-16 h-16"
                  alt=""
                />
              </div>
              <div className=" flex flex-col items-center justify-center">
                <div className="flex pa2 flex-row f7 ph2 ">
                  <div className="br-100 mt1 w04 h04 bg-white"></div>
                  <div className="fw2 f7 ml2 text-white text-base  px-8 ">
                    {' '}
                    . {getLangText(lang, words, 'Open_ff_message')}
                  </div>
                </div>
                <div className="flex justify-center items-center mt3 uppercase font-bold">
                  {getLangText(lang, words, 'team_code')}
                </div>
                <div className="flex justify-center items-center mt3">
                  <div className="gray br2 bw1 ph2 ba b--white-10 bg-basebg p-2">
                    <div className="ttu pa2 text-orange flex justify-center items-center orange f4 fw2">
                      {myTeam && myTeam.ffCode}
                    </div>
                  </div>
                </div>
                <div
                  className={`tc pv2  text-green bg-basebg ${
                    copyAllCodes ? 'bg-basebg' : 'bg-darkRed'
                  } mt-2 border border-green white tc w-60 f5 fw6 cursor-pointer center br2 mt3 mb2 ttu cursor-pointer flex justify-center`}
                >
                  <CopyToClipboard
                    text={myTeam && myTeam.ffCode}
                    onCopy={() => {
                      setCopyAllCodes(true)
                    }}
                  >
                    {lang === 'HIN' ? (
                      <div>{`${
                        copyAllCodes
                          ? getLangText(lang, words, 'copied')
                          : getLangText(lang, words, 'Copy Code')
                      }`}</div>
                    ) : (
                      <div>{`${copyAllCodes ? 'Copied' : 'Copy Code'}`}</div>
                    )}
                    {/* <div>{`${copyAllCodes ? 'Copied' : 'Copy Code'}`}</div> */}
                  </CopyToClipboard>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    ) : (
      <div className="bg-basebg w-100 min-vh-100 mw75-l center">
        <div className="bg-gold z-1 ph2 h2-4  flex items-center mb4">
          <ImageWithFallback
            height={18}
            width={18}
            loading="lazy"
            className="w1"
            onClick={() => window.history.back()}
            alt=""
            src={backIcon}
          />
          <div className="black f6 fw7 pl3 ttu tc">My Teams</div>
        </div>
        <ImageWithFallback
          height={18}
          width={18}
          loading="lazy"
          className="w45-m h45-m w4 h4 w5-l h5-l"
          style={{ margin: 'auto', display: 'block' }}
          src={'/svgs/groundImageWicket.png'}
          alt="loading..."
        />
        <div className="f5 fw5 f3-l tc pt2 white pt4-l">No Saved Teams</div>
      </div>
    )
}
