import React, { useState } from 'react'

import { useRouter } from 'next/router'
import { useQuery, useMutation } from '@apollo/react-hooks'
import ImageWithFallback from '../commom/Image'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
const allRounder = '/pngs/allRounder-white-solid.png'
const ball = '/pngs/ball-white-solid.png'
const bat = '/pngs/bat-white-solid.png'
const wicket = '/pngs/wicketKeeper-white-solid.png'
import { getLangText } from '../../api/services'
import { words } from './../../constant/language'
import { CREATE_AND_UPDATE_USER_TEAM } from '../../api/queries'
const saveTeam = '/svgs/saveTeam.svg'
import { getStatshubFrcUrl } from '../../api/services'
import Link from 'next/link'
const playerPlaceholder = '/pngs/fallbackprojection.png'
//playerList,  ffCode={ffCode} viceCaptain={viceCaptain} captain={captain} teamName={teamName}
export default function playerStatsChooseCaptains({
  matchID,
  playerScores,
  changeComponentState,
  categoryPlayer,
  ...props
}) {
  let lang = props.language ? props.language : 'ENG'
  let router = useRouter()
  // console.log("matchID captainvc",matchID)
  // console.log("matchID playerScores",playerScores)
  // console.log("matchID changeComponentState",changeComponentState)
  // console.log("matchID categoryPlayer",categoryPlayer)

  const [captain, setcaptain] = useState(props.captain ? props.captain : null)
  const [vice_captain, setvice_captain] = useState(
    props.viceCaptain ? props.viceCaptain : null,
  )
  const [Submit, setSubmit] = useState(false)
  // console.log("captain",captain)
  // console.log("vice capt",vice_captain)
  const [teamName, setTeamName] = useState(props.teamName ? props.teamName : '')
  const [playerList, setplayerList] = useState([...props.playerList])
  // console.log("playerList",playerList)
  const checkCaptains = (id, type) => {
    if (type === 'captain') {
      if (id !== vice_captain) {
        setcaptain(id)
      }
    }
    if (type === 'vice_captain') {
      if (id !== captain) {
        setvice_captain(id)
      }
    }
  }

  const [matchId, matchSlug, pageCategory, stahubCategory] = router.query.slugs
  // console.log("router.query.slugs",router.query.slugs) ;

  const [createUserTeam] = useMutation(CREATE_AND_UPDATE_USER_TEAM)

  const updateTeamDetails = async () => {
    let finalList = playerList.map((player) => {
      return {
        ...player,
        captain: player.playerId === captain ? '1' : '0',
        vice_captain: player.playerId === vice_captain ? '1' : '0',
      }
    })
    // console.log("finalList",finalList)
    await createUserTeam({
      variables: {
        matchID: matchID,
        token: global.window.localStorage.getItem('tokenData'),
        team: {
          teamName: props.teamName ? props.teamName : teamName,
          leagueType: 'statsHub',
          selectCriteria: 'projected_points',
          ffCode: props.ffCode ? props.ffCode : '',
          team: finalList,
          isAlogo11: false,
        },
      },
    })
    //api call here
    setSubmit(false)
  }

  return (
    <div className="white pb3">
      <div className="font-medium pv2-l capitalize text-xs">
        {lang === 'HIN'
          ? 'अपना कप्तान और उप कप्तान चुनें'
          : 'CHOOSE YOUR CAPTAIN AND VICE CAPTAIN'}
      </div>
      <div className="">
        <div className="mt3">
          {playerList
            .filter((pl) => pl.player_role === 'KEEPER')
            .map((player, i) => (
              <div
                key={i}
                className=" py-2  flex items-center  border-t border-b mt-2 "
              >
                <div className="flex items-center w-60 w-50-l">
                  <div className=" w-30  w-40-l flex items-center justify-end ml-2">
                    <div className=" relative overflow-hidden    ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={playerPlaceholder}
                        className="rounded-full h-16 w-16 h3-l w3-l h3-m w3-m   
                            object-cover object-top"
                        src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                        alt=""
                      />
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5    bg-gray-4  "
                        style={{ left: 2 }}
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      />
                    </div>
                  </div>
                  <div className="pl2 w-70 w-50-l justify-start">
                    <div className="fw6 f8 f6-l f6-m">
                      {lang === 'HIN'
                        ? player.playerNameHindi
                        : player.playerName}
                    </div>
                    <div className="mt1">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-2 h-2"
                        src={wicket}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                <div className="w-20 w-30-l   flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center  h-7 w-7 rounded-full  ${
                      player.playerId === captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    }  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className="w-20 w-30-l flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center   ${
                      player.playerId === vice_captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    } 
                      justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m  h-7 w-7 rounded-full fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>
                <div></div>
              </div>
            ))}
        </div>

        <div className="mt3">
          {playerList
            .filter((pl) => pl.player_role === 'BATSMAN')
            .map((player, i) => (
              <div
                key={i}
                className="py-2  flex items-center  border-t border-b"
                mt-2
              >
                <div className="flex items-center w-60 w-50-l">
                  <div className=" w-30  w-40-l flex items-center justify-end ml-2">
                    <div className=" relative overflow-hidden    ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={playerPlaceholder}
                        className="rounded-full h-16 w-16 h3-l w3-l h3-m w3-m    object-cover object-top"
                        src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                        alt=""
                      />
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5    bg-gray-4 "
                        style={{ left: 2 }}
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      />
                    </div>
                  </div>
                  <div className="pl2 w-70 w-50-l justify-start">
                    <div className="fw6 f8 f6-l f6-m">
                      {lang === 'HIN'
                        ? player.playerNameHindi
                        : player.playerName}
                    </div>
                    <div className="mt1">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-2 h-2"
                        src={bat}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                <div className="w-20 w-30-l  flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center  h-7 w-7 rounded-full  ${
                      player.playerId === captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    }  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m  fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className="w-20 w-30-l flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center h-7 w-7 rounded-full   ${
                      player.playerId === vice_captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    }  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m  fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>
                <div></div>
              </div>
            ))}
        </div>
        <div className="mt3">
          {playerList
            .filter((pl) => pl.player_role === 'ALL_ROUNDER')
            .map((player, i) => (
              <div
                key={i}
                className="py-2  flex items-center  border-t border-b mt-2 "
              >
                <div className="flex items-center w-60 w-50-l">
                  <div className=" w-30  w-40-l flex items-center justify-end ml-2">
                    <div className=" relative overflow-hidden    ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={playerPlaceholder}
                        className=" rounded-full h-16 w-16 h3-l w3-l h3-m w3-m    object-cover object-top"
                        src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                        alt=""
                      />
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5 bg-gray-4 "
                        style={{ left: 2 }}
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      />
                    </div>
                  </div>
                  <div className="pl2 w-70 w-50-l justify-start">
                    <div className="fw6 f8 f6-l f6-m">
                      {lang === 'HIN'
                        ? player.playerNameHindi
                        : player.playerName}
                    </div>
                    <div className="mt1">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-2 h-2"
                        src={allRounder}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                <div className="w-20 w-30-l  flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center  h-7 w-7 rounded-full  ${
                      player.playerId === captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    }  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m  fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className="w-20 w-30-l flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center h-7 w-7 rounded-full  ${
                      player.playerId === vice_captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    }  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m  fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>
                <div></div>
              </div>
            ))}
        </div>
        <div className="mt3">
          {playerList
            .filter((pl) => pl.player_role === 'BOWLER')
            .map((player, i) => (
              <div
                key={i}
                className="py-2  flex items-center  border-t border-b mt-2"
              >
                <div className="flex items-center w-60 w-50-l">
                  <div className=" w-30  w-40-l flex items-center justify-end ml-2">
                    <div className=" relative overflow-hidden    ">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={playerPlaceholder}
                        className=" rounded-full h-16 w-16 h3-l w3-l h3-m w3-m    object-cover object-top"
                        src={`https://images.cricket.com/players/${player.playerId}_headshot.png`}
                        alt=""
                      />
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        fallbackSrc={flagPlaceHolder}
                        className="absolute bottom-0 object-cover   ba b--black h08 rounded-full h-5 w-5 bg-gray-4"
                        style={{ left: 2 }}
                        src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`}
                        alt=""
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                      />
                    </div>
                  </div>
                  <div className="pl2 w-70 w-50-l justify-start">
                    <div className="fw6 f8 f6-l f6-m">
                      {lang === 'HIN'
                        ? player.playerNameHindi
                        : player.playerName}
                    </div>
                    <div className="mt1">
                      <ImageWithFallback
                        height={18}
                        width={18}
                        loading="lazy"
                        className="w-2 h-2"
                        src={ball}
                        alt=""
                      />
                    </div>
                  </div>
                </div>
                <div className="w-20 w-30-l  flex items-center justify-center cursor-pointer">
                  <div
                    className={`flex items-center   ${
                      player.playerId === captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    }  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m fw6 f7 h-7 w-7 rounded-full`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'captain')
                    }}
                  >
                    C
                  </div>
                </div>
                <div className="w-20 w-30-l flex items-center justify-center cursor-pointer ">
                  <div
                    className={`flex items-center   ${
                      player.playerId === vice_captain
                        ? 'bg-green text-white border  baorder-green'
                        : 'border border-white'
                    } h-7 w-7 rounded-full  justify-center br-100 w2 h2 w2-3-l h2-3-l w2-3-m h2-3-m fw6 f7`}
                    onClick={() => {
                      checkCaptains(player.playerId, 'vice_captain')
                    }}
                  >
                    VC
                  </div>
                </div>
                <div></div>
              </div>
            ))}
        </div>
      </div>
      {captain && vice_captain ? (
        <Link
          {...getStatshubFrcUrl(matchId, matchSlug, 'my-teams')}
          passHref
          legacyBehavior
        >
          <div className=" mt-3 flex items-center justify-center ">
            <div
              className={`dib db ${
                captain && vice_captain ? 'bg-green' : 'bg-white-30'
              } pa2 br2 ttu white f6 fw6 center flex justify-center w-40 w-25-l shadow-4 cursor-pointer`}
              onClick={() =>
                captain && vice_captain ? updateTeamDetails() : ''
              }
            >
              <ImageWithFallback
                height={18}
                width={18}
                loading="lazy"
                src={saveTeam}
              />
              <span className="pl1">
                {lang === 'HIN' ? 'सेव टीम' : 'Save team'}{' '}
              </span>
            </div>
          </div>
        </Link>
      ) : (
        <div className=" mt-3  flex items-center justify-center">
          <div
            className={`dib db bg-white-30 pa2 br2 ttu white f6 fw6 center flex justify-center w-40 w-25-l shadow-4 cursor-pointer`}
          >
            <ImageWithFallback
              height={18}
              width={18}
              loading="lazy"
              src={saveTeam}
            />
            <span className="pl1">
              {lang === 'HIN' ? 'सेव टीम' : 'Save team'}
            </span>
          </div>
        </div>
      )}
      {/* {
             Submit &&  
        <div className="flex justify-center items-center fixed absolute--fill z-9999 bg-black-70 cursor-pointer" style={{ backdropFilter: "blur(10px)" }} >
          <div className="w-90 mw6-l relative">
            <img src={'/svgs/close.png'} className="absolute right-0 top--2" onClick={() => setSubmit(false)} />
            <div className="bg-black br3 shadow-4  ba b--white-20 max-vh-80  white pa2 pv4" >
              <div className="fw6 f4 tc pb2">Name your team</div>
              <input className="w-80 br2 h2 tc flex justify-center center bg-white-30 ba b--yellow white ttu mv4 f6 fw5" placeholder="Enter Your Team Name" value={teamName} onChange={(e) => setTeamName(e.target.value)}></input>
              <Link {...getStatshubFrcUrl(matchId,matchSlug,'my-teams')} passHref>
                <div className={`f5 fw6 white ${teamName.length>0 ? "bg-green":"bg-white-30"} db flex justify-center center w-40 items-center pa2 br2 dib`} onClick={() => (teamName.length>0 ? (updateTeamDetails()):"")}>
                <div className="pl2">SAVE</div></div>
              </Link>
            </div>
          </div>
        </div>
      
          } */}
    </div>
  )
}
