import React,{useState} from 'react'
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ImageWithFallback from '../commom/Image';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const playerPlaceholder='/pngs/fallbackprojection.png';
const editTeam = '/svgs/editTeam.svg';
const copyCode = '/svgs/copyCode.svg';
import {getLangText} from '../../api/services'
import { words } from '../../constant/language'
export default function PlayerStatsHubViewTeam({listID,matchID,matchStatus,homeTeamID,credit,categoryPlayer,awayTeamID,...props}) {
  const lang = props.language
   const [playerList,setplayerList]=useState([...props.playerList])
   const [teamCode, setTeamCode] = useState(props.ffCode)
   const [toggle, setToggle] = useState(false);
   const [teamCodeClick, setteamCodeClick] = useState(false)
  //  console.log("PlayerStatsHubViewTeam",props)
   return (
      <div>
      {<div className="bg-black min-vh-70 mh2 pb4">
        {/* <div className="tc white f4 fw5 mt2 pv2">Your superteam </div>
        <div className="yellow-frc f8 fw5 tc lh-copy pb2">Your superteam has been assembled. You can still <br />make changes to this team here and proceed <br />to the next step.</div> */}

        <div className=" mt2  pa2">
         
        { <div className="center tc white">
           
            <div className="tc ttu oswald f5 fw4 pb2">{getLangText(lang,words,"WICKET_KEEPERS")}</div>
            <div className="w-100  flex flex-wrap justify-around   items-center">
              {playerList.filter(pl=>pl.player_role==='KEEPER').map((item, index) => {
                return (
                  <div key={index} className={`ba b--white-20 br3 w-30 w-15-l tc cursor-pointer mb2 relative h4 flex bg-white-10 items-center justify-center mb3-l `}>
                    {/* {item.isMyPick && <img className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />} */}
                    {/* <img className="w1 absolute right-0 top-0 mr2 mt2 " src={`${item.isMyPick ? "/svgs/change_inActive.png" : "/svgs/change_active.svg"}`} /> */}
                    {(item.captain==="1"||item.vice_captain==="1") &&<div className="w1 absolute right-0 top-0 mr2 mt2 "  >
                       <div className="w1 h1 bg-frc-yellow black flex items-center justify-center br-100  fw6 f8">{item.captain==="1"? "c":"vc"}</div>
                    </div>}
                    <div className="">
                      <div className="relative flex justify-center items--center w-24 h-24 center">
                        <div className="overflow-hidden rounded-full b--white-20 w-24 h-24 bg-mid-gray">
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" />
                        </div>
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 rounded-full w07  ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt=""  />
                      </div>
                      <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                        <div className="oswald "> {item.playerName}</div>
                        <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang,words,"Credits")}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>}

          {/* battingList.length > 0 */}
          { <div className="center tc white">
            {/* <div className="bb b--white-20  mv3"></div> */}
            <div className="bb b--white-20 mb2 mt1"></div>
            <div className="tc ttu oswald f5 fw4 pb2">{getLangText(lang,words,"BATTERS")}</div>
            <div className="w-100  flex flex-wrap justify-around   items-center">
              {playerList.filter(pl=>pl.player_role==='BATSMAN').map((item, index) => {
                return (
                  <div key={index} className={`ba b--white-20 br3 w-30 w-15-l tc cursor-pointer mb2 relative h4 flex bg-white-10 items-center justify-center mb3-l `}>
                    {/* {item.isMyPick && <img className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />} */}
                    {/* <img className="w1 absolute right-0 top-0 mr2 mt2 " src={`${item.isMyPick ? "/svgs/change_inActive.png" : "/svgs/change_active.svg"}`} /> */}
                    {(item.captain==="1"||item.vice_captain==="1") &&<div className="w1 absolute right-0 top-0 mr2 mt2 "  >
                       <div className="w1 h1 bg-frc-yellow black flex items-center justify-center br-100  fw6 f8">{item.captain==="1"? "c":"vc"}</div>
                    </div>}
                    <div className="">
                      <div className="relative flex justify-center items--center w2-6 h2-6 center">
                        <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt="" />
                        </div>
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                      </div>
                      <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                        <div className="oswald "> {item.playerName}</div>
                        <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang,words,"Credits")}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>}

          { <div className="center tc white">
            <div className="bb b--white-20  mb2 mt1"></div>
            <div className="tc ttu oswald f5 fw4 pb2">{getLangText(lang,words,"ALL_ROUNDERS")}</div>
            <div className="w-100  flex flex-wrap justify-around   items-center">
              {playerList.filter(pl=>pl.player_role==='ALL_ROUNDER').map((item, index) => {
                return (
                  <div key={index} className={`ba b--white-20 br3 w-30 w-15-l tc cursor-pointer mb2 relative h4 flex bg-white-10 items-center justify-center mb3-l `}>
                    {/* {item.isMyPick && <img className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />} */}
                    {(item.captain==="1"||item.vice_captain==="1") &&<div className="w1 absolute right-0 top-0 mr2 mt2 "  >
                       <div className="w1 h1 bg-frc-yellow black flex items-center justify-center br-100  fw6 f8">{item.captain==="1"? "c":"vc"}</div>
                    </div>}
                    <div className="">
                      <div className="relative flex justify-center items--center w2-6 h2-6 center">
                        <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt=""  />
                        </div>
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt="" />
                      </div>
                      <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                        <div className="oswald "> {item.playerName}</div>
                        <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang,words,"Credits")}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>}


          { <div className="center tc white">
            <div className="bb b--white-20  mb2 mt1"></div>
            <div className="tc ttu oswald f5 fw4 pb2">{getLangText(lang,words,"BOWLERS")}</div>
            <div className="w-100  flex flex-wrap justify-around   items-center">
              {playerList.filter(pl=>pl.player_role==='BOWLER').map((item, index) => {
                return (
                  <div key={index} className={`ba b--white-20 br3 w-30 w-15-l tc cursor-pointer mb2 relative h4 flex bg-white-10 items-center justify-center mb3-l `}>
                    {/* {item.isMyPick && <img className=" absolute left-0 top-0 w2" src={'/svgs/your_pick.png'} />} */}
                    {/* <img className="w1 absolute right-0 top-0 mr2 mt2 " src={`${item.isMyPick ? "/svgs/change_inActive.png" : "/svgs/change_active.svg"}`} /> */}
                    {(item.captain==="1"||item.vice_captain==="1") &&<div className="w1 absolute right-0 top-0 mr2 mt2 "  >
                       <div className="w1 h1 bg-frc-yellow black flex items-center justify-center br-100  fw6 f8">{item.captain==="1"? "c":"vc"}</div>
                    </div>}
                    <div className="">
                      <div className="relative flex justify-center items--center w2-6 h2-6 center">
                        <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray">
                          <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${item.playerId}_headshot.png`} alt=""  />
                        </div>
                        <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder} className="absolute bottom-0 right-0 mt1 top-2 left-0 h07 br-100 w07 ba b--black-20 bg-gray" src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`} alt=""  />
                      </div>
                      <div className=" f8 f7-l  justify-center pl1 items--center pt1">
                        <div className="oswald "> {item.playerName}</div>
                        <div className="mt1 fw5 f8-l f10 pt1"> {getLangText(lang,words,"Credits")}: <span className="ph1 black fw5 br2 bg-yellow oswald">{item.credits}</span></div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>}


     
        </div>
        <div className="flex items-center justify-center w-100 " >
                <div className="flex w-50 w-30-l items-center justify-center" >
                <div className="ttu  flex f6 fw6 ph3 bg-green  pa2 br2 db " onClick={()=>setToggle(true)}>
                  <div>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                       className="" src={copyCode} />
                  </div>
                  <div className="pl1">{getLangText(lang,words,"Copy Code")}</div></div>
                  </div>
                   
                <div className="w-50 w-30-l flex items-center justify-center"  >
                
                  <div className="ttu  flex f6 fw6 ph3 bg-dark-gray  pa2 br2 db " onClick={()=>props.changeComponentState('BYTPlayerStathubState')}>
                    <div>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        className="" src={editTeam} />
                    </div>
                    <div className="pl1 ttu">{getLangText(lang,words,"Edit Team")}</div></div>
                   
                    </div>
                     
              </div>


              {toggle && (
                <div className='flex justify-center white items-center fixed absolute--fill z-9999 bg-black-70  overflow-y-scroll' style={{ backdropFilter: 'blur(10px)' }}>
                  <div className='pb3 bg-white-10 w-90  w-25-ns relative'>
                    <ImageWithFallback height={18} width={18} loading='lazy'
                      
                      className='h08 w08 pa2 bg-white-20 right-0 top--2 absolute br-100 cursor-pointer'
                      onClick={() => setToggle(false)}
                      src={'/svgs/close.png'}
                      alt='close icon'
                    />                    <div className='flex justify-center items-center pa2 bg-black-20'>
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        src='/pngs/A23.png' className='w-12' alt='' />
                    </div>
                    <div className=''>
                      {/* <div className='flex pa2 f7 mt3 ph2'>
                        <div className='br-100 mt1 w04 h04 bg-white'></div>
                        <div className='fw2 f7 ml2 w-90'>{getLangText(lang,words,"copy_activate_code")}</div>
                      </div> */}
                      <div className='flex pa2 flex-row f7 ph2 '>
                        <div className='br-100 mt1 w04 h04 bg-white'></div>
                        <div className='fw2 f7 ml2 w-90'> {getLangText(lang,words,"Open_ff_message")}</div>
                      </div>
                      <div className='flex justify-center items-center mt3'>{getLangText(lang,words,"team_code")}</div>
                      <div className='flex justify-center items-center mt3'>
                        <div className='gray br2 bw1 ph2 ba b--white-10'>
                          <div className='ttu pa2 flex justify-center items-center orange f4 fw2'>{teamCode}</div>
                        </div>
                      </div>
                      <div className={`tc pv2 ${teamCodeClick ? "bg-gray" : "bg-darkRed"} white tc w-80 f5 fw6 cursor-pointer center br2 mt3 ttu cursor-pointer`}>
                        <CopyToClipboard
                          text={teamCode}
                          onCopy={() => {
                            setteamCodeClick(true)
                          }}>
                          <div>{`${teamCodeClick ? 'Copied' : 'Copy Code'}`}</div>
                        </CopyToClipboard>
                      </div>
                    </div>
                  </div>
                </div>
              )}
       
      </div>}
     
  



    {/* #################################---for captain and vice captain ---------------################################### */}

      

    </div>

   )
}
