import React,{useState} from 'react'
import { words } from '../../constant/language'
export default function RecentPlayerBattingType({RecentPlayer,battingType,language}) {
    let lang=language
   
   const getLangText = (lang,keys,key) => {
    //   console.log("keys[key]",keys[key])
      let [english,hindi] = keys[key]
      switch(lang){
        case 'HIN':
          return hindi
        case 'ENG':
          return english
        default:
          return english
      }
        }
    let newArray= (battingType === "WICKETS"?[...RecentPlayer.battingDetails.battingType.filter(ar=>ar.wickets!==null)]:battingType === "AVERAGE"?[...RecentPlayer.battingDetails.battingType.filter(ar=>ar.battingAvg!==null)]:[...RecentPlayer.battingDetails.battingType.filter(ar=>ar.economyRate!==null)]).sort(function (a, b) {
        return battingType === "WICKETS" ?(a.wickets - b.wickets):battingType === "AVERAGE"?(a.battingAvg - b.battingAvg):(a.economyRate - b.economyRate)
    })
    
    let min = newArray[0]
    // console.log("battingType min",battingType,"min", min)
    let max = newArray[newArray.length - 1]
    // console.log("battingType max",battingType,"max", max)

  
    const typeCheck=(currentObj)=>{
        if(battingType === "WICKETS"){
           if((currentObj.wickets===min.wickets)||(currentObj.wickets===max.wickets)){
               return 'o-100'
           }else{
               return 'o-50'
           }
        }

        else if(battingType === "AVERAGE"){
            if((currentObj.battingAvg===min.battingAvg)||(currentObj.battingAvg===max.battingAvg)){
                return 'o-100'
            }else{
                return 'o-50'
            }
         }
         else{
            if((currentObj.economyRate===min.economyRate)||(currentObj.economyRate===max.economyRate)){
                return 'o-100'
            }else{
                return 'o-50'
            }
         }
    }
   return (
     <div className="">
            {RecentPlayer && RecentPlayer.battingDetails &&
                              RecentPlayer.battingDetails.battingType && RecentPlayer.battingDetails.battingType.length > 0 &&
                              (battingType === "WICKETS"?[...RecentPlayer.battingDetails.battingType.filter(ar=>ar.wickets!==null)]:battingType === "AVERAGE"?[...RecentPlayer.battingDetails.battingType.filter(ar=>ar.battingAvg!==null)]:[...RecentPlayer.battingDetails.battingType.filter(ar=>ar.economyRate!==null)]).map((compare, i) =>
                                 <div className="px-3 py-2" key={i}>
                                    <div className="flex py-2 items-center justify-between">
                                       <div className="fw4 f8 text-xs"><span className="gray">v</span><span className="white ttu pl1"> {getLangText(lang,words,compare.types.toUpperCase())}</span></div>
                                       <div className="fw7 white f8 text-xs">{battingType === "WICKETS" ? Number(compare.wickets) : battingType === "AVERAGE" ? Number(compare.battingAvg) : Number(compare.economyRate)}</div>
                                    </div>

                                    <div
                                       className={`  br2  relative items-center w-100 z-0 `}
                                       style={{ height: 6, backgroundColor: "#333a46" }}>
                                       <div
                                          className={`absolute h-1 rounded  br--left left-0 bg-green text-xs  ${typeCheck(compare)}`}
                                          style={{
                                             width: `${battingType === "WICKETS" ? Number(compare.wickets) / Number(RecentPlayer.battingDetails.maxWickets) * 100 : battingType === "AVERAGE" ? Number(compare.battingAvg) / Number(RecentPlayer.battingDetails.maxAvg) * 100 : Number(compare.economyRate) / Number(RecentPlayer.battingDetails.maxEconomy) * 100}%`,


                                             zIndex: -1
                                          }}></div>
                                    </div>
                                 </div>
                              )}
     </div>

   )
}
