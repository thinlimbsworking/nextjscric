import React, { useState, useEffect, useRef } from 'react'
import {getLangText} from '../../api/services'
import { words } from '../../constant/language'
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import { format } from 'date-fns';
import { STAT_HUB_TEAM, MATCH_SUMMARY } from '../../api/queries';
import Loading from '../loading';
import DataNotFound from '../commom/datanotfound';
const yellowDownArrow = '/svgs/yellowDownArrow.svg';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const location = '/svgs/location-icon-color.png';
const rightArrow = '/svgs/yellowRightArrow.svg';
const downArrow = '/svgs/downArrowWhite.svg';
const upArrow = '/svgs/upArrowWhite.svg';

export default function StatHubTeam(props) {
   // const lang = 'ENG'
   const lang = props.language
   const [teamCategory, setTeamCategory] = useState('overAllRecord');
   const [hide, setHide] = useState(false);
   const [recentMatch, setRecentMatch] = useState('lastFiveH2H');
   const [playerRecord, setPlayerRecord] = useState('BATTING');
   const [showDropDown, setShowDropDown] = useState(false);
   const tabs = ['wicket', 'runs', 'Average'];
   const [RecentMatchIndex, setRecentMatchIndex] = useState({});
   const [dropDown, setDropDown] = useState(false);
   const [type, setType] = useState('Average');
   const teamTab = ['overAllRecord', 'battingFirst', 'battingSecond'];
   const RecentMatchTab = ['HEAD TO HEAD', 'DC', 'MI'];
   const playerRecordTab = ['BATTING', 'BOWLING'];
   const [battingType, setBattingType] = useState("battingAverage")
   const [bowlingType, setBowlingType] = useState("totalWickets")
   const battingTab = ['totalRuns', 'battingAverage', 'battingStrikeRate', 'fifties', 'hundreds', 'totalFours', 'totalSixes'];
   const bowlingTab = ['totalWickets', 'bowlingStrikeRate', 'economy', 'threeWickets', 'fiveWickets']
   const [viewAll, setViewAll] = useState(false)
   const { loading, error, data: TeamStatHub } = useQuery(STAT_HUB_TEAM, {
      variables: { matchID: props.matchID }
   });


   const [getSummary, { error: scoreError, loading: scoreLoading, data: summaryData }] = useLazyQuery(
      MATCH_SUMMARY
   );

   const wrapperRef = useRef(null);
   useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
         if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setShowDropDown(false);
         }
      }

      document.addEventListener('mousedown', handleClickOutside);
      return () => {
         document.removeEventListener('mousedown', handleClickOutside);
      };
   }, [wrapperRef]);




 const isEnglish = (lang) => {
    if(lang === 'ENG') return true
    else return false
 }

   const handlecssHomeTeam = (team, HomeTeam, awayTeam) => {

      if (team === "lowestScore" || team === "avgWickets") {
         if (awayTeam > HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      } else {
         if (awayTeam < HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      }
   }

   const handlecssAwayTeam = (team, HomeTeam, awayTeam) => {

      if (team === "lowestScore" || team === "avgWickets") {
         if (awayTeam < HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      } else {
         if (awayTeam > HomeTeam) {
            return "text-green"
         } else {
            return "text-gray-2"
         }
      }
   }
   const getSummaryData = async (matchID, i) => {
      if (!RecentMatchIndex[i]) {

         await getSummary({ variables: { matchID: matchID, status: "completed" } })
      }
      setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });


   }
   // if (error)
   // return (
   //   <div className='w-100 vh-100 white fw2 f7 gray flex flex-column  min-vh-100 bg-black justify-center items-center'>
   //     <span>Something isn't right!</span>
   //     <span>Please refresh and try again...</span>
   //   </div>
   // );
   if (loading) return <Loading />;
   else 

      return (
         <>
            {TeamStatHub && TeamStatHub.teamHub ? <div className='tt  '>
               <div className='lg:flex md:flex  justify-between mx-3 lg:mx-0 md:mx-0 '>

               {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords ?
                <div className="mt-3 bg-gray p-2 rounded-xl lg:w-6/12 md:w-6/12 mr-1" >
                  <div className='flex   items-center justify-center p-1 bg-gray-8 rounded-3xl ' >
                     {teamTab.map((team, i) => (
                        <div
                           onClick={() => setTeamCategory(team)}
                           className={`${teamCategory === team ? 'border rounded-2xl border-green black p-1 text-white' : 'p-1 text-white'
                              }  w-4/12 text-center `}
                           key={i}>
                           {team === 'overAllRecord' ? (
                              <div className='text-center text-xs p-1 '>{getLangText(lang,words,'OVERALL').toUpperCase()}</div>
                           ) : team === 'battingFirst' ? (
                              <div className='text-center text-xs p-1'>
                                 {getLangText(lang,words,'BATTING').toUpperCase()} 1<sup>st</sup>
                              </div>
                           ) : (
                              <div className='text-center text-xs p-1'>
                                 {getLangText(lang,words,'BATTING').toUpperCase()} 2<sup>nd</sup>
                              </div>
                           )}
                        </div>
                     ))}
                  </div>

                  {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div>
                     <div className='bg-gray-4 mt-2 rounded-md white    py-2 flex  items-center justify-between'>
                        <div className='flex justify-center items-center w-4/12'>
                           <img
                              className='flex items-center justify-center w-10 h-6 '
                              src={`https://images.cricket.com/teams/${TeamStatHub.teamHub.teamRecords.homeTeamID}_flag_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = flagPlaceHolder)}
                           />
                           <div className='pl2 fw5 oswald f4 ml-2 text-xs font-bold '>{TeamStatHub.teamHub.teamRecords.homeTeamShortName}</div>
                        </div>
                        <div className="w-4/12 flex items-center justify-center">
                           <div
                              className='border border-green w-8/12  p-1 text-xs flex text-green bg-gray-8 font-semiBold  items-center justify-center rounded-xl'
                           >
                              {props.matchType}
                           </div>
                           {/* <div className='h-solid-divider-light  center flex  items-center justify-center relative  w3'>
                        <div
                           className='w2 h2 absolute  bg-frc-yellow flex black mont-semiBold f6  f6-l fw6 items-center justify-center br-100'
                           style={{ left: 15 }}>
                           VS
                        </div>
                     </div> */}
                        </div>

                        <div className='flex items-center justify-center w-4/12 justify-center'>
                           <div className='pr-2 oswald text-white text-xs font-bold'>{TeamStatHub.teamHub.teamRecords.awayTeamShortName}</div>
                           <img
                              className=' flex items-center justify-center  w-10 h-6  '
                              src={`https://images.cricket.com/teams/${TeamStatHub.teamHub.teamRecords.awayTeamID}_flag_safari.png`}
                              alt=''
                              onError={(evt) => (evt.target.src = flagPlaceHolder)}
                           />
                        </div>
                     </div>
                     <div className="oswald lh-copy ttu">
                        {(TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'] !== null) &&
                           ["wins", "avgTeamRuns", "highestScore", "lowestScore", "avgWickets"].map((team, i) => <div key={i} >

                              <div className='flex  items-center justify-center pv1  f4 '>
                                 <div className={`w-4/12 flex ${handlecssHomeTeam(team, TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory] && TeamStatHub.teamHub.teamRecords[teamCategory]['homeData'] !== null &&
                                    TeamStatHub.teamHub.teamRecords[teamCategory]['homeData'][team], TeamStatHub &&
                                    TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory] && TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'] !== null && TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'][team])} items-center justify-center  f3 oswald fw6`}>{TeamStatHub


                                       && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory] && TeamStatHub.teamHub.teamRecords[teamCategory]['homeData'] !== null && TeamStatHub.teamHub.teamRecords[teamCategory]['homeData'][team]}




                                 </div>
                                 <div className='w-4/12  border-x flex items-center justify-center  p-1 m-1 '>
                                    {team === "wins" ? <div className="oswald text-center text-base text-gray-2 ">{getLangText(lang,words,'WINS')}</div> 
                                    : team === "avgTeamRuns" ? <div className="oswald text-center text-base text-gray-2">{getLangText(lang,words,'AVERAGE')} 
                                     <br />{getLangText(lang,words,'Score')} </div> : team === "avgWickets" ? 
                                     <div className="oswald text-center text-base text-gray-2">{getLangText(lang,words,'AVG.WICKETS')}
                                      <br />{getLangText(lang,words,'LOST/INN')}</div> :
                                       team === "highestScore" ? 
                                       <div className="oswald    text-base  text-center text-gray-2">
                                          {getLangText(lang,words,'HIGHEST')} <br />{getLangText(lang,words,'Score')} </div> : team === "lowestScore" ? <div className="oswald text-center text-base text-gray-2">{getLangText(lang,words,'LOWEST')}<br />{getLangText(lang,words,'Score')} </div> : team === "teamRuns" ? <div className="oswald f5  f4-l fw6  tc text-gray-2">{getLangText(lang,words,'RUNS')}</div> : <div className="oswald f5 f4-l  fw6  tc text-gray-2">{getLangText(lang,words,'TOTAL')} <br />{getLangText(lang,words,'MATCHES')}</div>}
                                 </div>
                                 <div className={`w-4/12  flex items-center justify-center ${handlecssAwayTeam(team,
                                    TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory] && TeamStatHub.teamHub.teamRecords[teamCategory]['homeData'] !== null
                                    && TeamStatHub.teamHub.teamRecords[teamCategory]['homeData'][team],

                                    TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'] !== null &&
                                    TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'][team])} f3 oswald fw6`}>


                                    {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'] !== null && TeamStatHub.teamHub.teamRecords[teamCategory]['awayData'][team]}</div>

                              </div>
                             
                           </div>)
                        }
                     </div>
                     {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div className="text-gray-2 mt-2 text-xs">
                 {lang==='ENG'?<span>{`* ${TeamStatHub.teamHub.compType} data from last 3 years`}</span>
                 : <span>{`* पिछले 3 वर्षों के  ${TeamStatHub.teamHub.compType} डेटा`}</span>}
                  </div>}
                  </div>}
               </div>:<DataNotFound/>}
           
               
 

               <div className='bg-gray py-4 mt-2 rounded-xl lg:w-6/12 md:w-6/12 ml-1'>
                  { TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div>
                     <div className='text-base ml-3  font-bold text-white uppercase  py-2'> 
                     {getLangText(lang,words,'RECENT MATCHES')}</div>
                     <div className='bg-blue-8 w-16 rounded h-1 ml-2 mb-2' />

                     <div className='flex items-center justify-center bg-gray-8 rounded-full  p-2 m-2 '>

                        <div
                           onClick={() => {
                              setRecentMatch("lastFiveH2H");
                              if (recentMatch !== "lastFiveH2H") {
                                 setRecentMatchIndex({})
                              }
                           }}
                           className={`${recentMatch === "lastFiveH2H" ? 'border  border-green rounded-full p-2 text-white  ' : ''
                              }  w-4/12  text-center captilize text-xs rounded-full`}
                        >
                           {getLangText(lang,words,'HEAD TO HEAD')} 
                        </div>
                        <div
                           onClick={() => {
                              setRecentMatch("lastFiveHome");
                              if (recentMatch !== "lastFiveHome") {
                                 setRecentMatchIndex({})
                              }
                           }}
                           className={`${recentMatch === "lastFiveHome" ? 'border border-green rounded-full p-2 text-white  ' : ''
                              }  w-4/12  text-center text-xs rounded-full`}
                        >
                           {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.homeTeamShortName}
                        </div>
                        <div
                           onClick={() => {
                              setRecentMatch("lastFiveAway");
                              if (recentMatch !== "lastFiveAway") {
                                 setRecentMatchIndex({})
                              }
                           }}
                           className={`${recentMatch === "lastFiveAway" ? 'border border-green rounded-full p-2 text-white  ' : ''
                              }  w-4/12  text-center text-xs rounded-full`}
                        >
                           {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.awayTeamShortName}
                        </div>

                        {/* style={{borderBottomColor: "#777"}} */}
                     </div>
                     <>
                        <div className='   '>
                           {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub[recentMatch] && TeamStatHub.teamHub[recentMatch].length > 0 ? TeamStatHub.teamHub[recentMatch].map((team, i) => (
                              <>

                                 <div
                                    className={i == 0 ? 'flex  cursor-pointer    items-center py-2  justify-between bg-gray-8 mt-3 mx-3 ' :
                                       'flex    mt2 cursor-pointer items-center py-2  justify-between bg-gray-8 mx-3 mt-3'}
                                    key={i}
                                    onClick={() => {
                                       getSummaryData(team.matchID, i)
                                       // setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });
                                    }}
                                   >
                                    <div className='flex items-center w-10/12 '>
                                       <div className=' w-1/4 tc flex justify-center items-center'>
                                          <img
                                             className='h-8 w-12 object-cover'
                                             src={team.winnerTeam === team.awayTeam ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png` :
                                                team.winnerTeam === team.homeTeam ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png` : flagPlaceHolder
                                             }
                                             alt=''
                                             onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                                          />
                                       </div>
                                       <div className='  w-70'>
                                          <div className='flex items-center pv1'>
                                             <div className={`ba  pv1 border ${team.winnerTeam === team.homeTeam ? ' border-green text-gray-2 ' : 'border-gray-2 white'}  br2   ph2  f6 fw5 text-xs p-1`}>{team.homeTeam}</div>
                                             <div className='ph2 moon-gray mx-1'> vs</div>
                                             <div className={`ba  br2 border  pt-2 ${team.winnerTeam === team.awayTeam ? 'border-green text-gray-2 ' : 'border-gray-2 white'} pv1    ph2 f6 fw5 text-xs p-1`}>{team.awayTeam}</div>
                                          </div>
                                          <div className='text-xs text-white tracked mt-2'>{isEnglish(lang) ? team.matchResult : team.matchResultHindi}</div>
                                          <div className='flex items-center  f7 moon-gray lh-title '>
                                             <div className="tracked text-gray-2 text-xs">{format(+team.matchDate, 'MMM dd, yyyy. ')}</div>
                                             <img className='w-5 px-1 ' src={location}></img>
                                             <div className='text-gray-2 font-semiBold text-xs'>{isEnglish(lang) ? team.venue : team.venueHindi}</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div
                                       className=' flex items-center justify-end mr-2'
                                    >
                                     
                                       <img src={RecentMatchIndex[i] ? yellowDownArrow : rightArrow} />
                                    </div>
                                 </div>
                                 {RecentMatchIndex[i] && summaryData && summaryData.matchSummary && (
                                    <div className='white bg-gray-8 mx-3 pb-3'>
                                        <div className='bg-gray mx-3 pb-3 mb-3'>
                                       {/* <div className='f6 silver ph3 pv2 fw7'>MATCH SUMMARY</div> */}
                                       <div className='flex'>
                                     
                                       <div className=' w-6/12 flex flex-col p-2 items-center  '>
                                          <div className='text-gray-2 font-semibold mb-1 text-xs'>TOP BATTER</div>
                                          <div className='relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center '>
                                             <div className='overflow-hidden w-full h-full rounded-full '>
                                             <img
                                                   className=' object-top object-contain w-24   '
                                                   style={{ objectPosition: '0 0%' }}
                                                   src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBatsman.playerID}_headshot.png`}
                                                   onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                   alt=''></img>
                                                
                                             </div>


                                          </div>



                                         
                                          <div className='w-full -mt-10 bg-gray-4 text-center  rounded-md px-2'>
                                             <div className="text-base  font-semibold mt-10  truncate">{lang==="HIN"?summaryData.matchSummary.bestBatsman.playerNameHindi
                                             :summaryData.matchSummary.bestBatsman.playerName}</div>
                                             {summaryData.matchSummary.bestBatsman.battingStatsList && team.matchType !== 'Test' ?
                                                <div>
                                                   <div className='flex  flex-col items-center py-1'>
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[0] &&
                                                       <div className=' font-semibold'><span className='f5 fw5 mr1'>
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[0].runs}
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                      </span>
                                                         <span className='f6'>( {summaryData.matchSummary.bestBatsman.battingStatsList[0].balls} )</span></div>}
                                                         <div className='flex items-center justify-between mt-2'>
                                                      <div className='mr-1'>
                                                         <span className='text-xs text-white '>4s </span>
                                                         <span className='text-xs text-white'>{` `}{summaryData.matchSummary.bestBatsman.battingStatsList[0].fours}</span>
                                                      </div>
                                                      <div className='ml-1 border-l'>
                                                         <span className='fw4 f6 gray ml-2'>6s</span>
                                                         <span className='fw6 f6 white'>{` `}{summaryData.matchSummary.bestBatsman.battingStatsList[0].sixes}</span>
                                                      </div>
                                                      </div>
                                                   </div>
                                                   {false && summaryData.matchSummary.bestBatsman.bowlingStatsList && <div className='flex  items-center pv1'>
                                                      <div className='ba bg-silver br2 pv1 fw5 b--black  ph2'><span className='f5 fw5 mr1 nowrap'>
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].wickets}/
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].runsConceded}
                                                      </span>
                                                         <span className='f7 '>({summaryData.matchSummary.bestBatsman.bowlingStatsList[0].overs})</span></div>
                                                      <div className='pl2 '>
                                                         <span className='fw4 f6 gray'>Eco</span>
                                                         <span className='fw6 pl1 f7 white'>{ summaryData.matchSummary.bestBatsman.bowlingStatsList&&summaryData.matchSummary.bestBatsman.bowlingStatsList[0].economyRate}</span>
                                                      </div>
                                                   </div>}
                                                </div> : <div>
                                                   <div className='flex  items-center pv1'>
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[0] && <div className='ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap ph2'>
                                                         <span className='f5 fw5 mr1'>
                                                            {summaryData.matchSummary.bestBatsman.battingStatsList[0].runs}
                                                            {summaryData.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                         </span>
                                                         <span className='f6'>( {summaryData.matchSummary.bestBatsman.battingStatsList[0].balls} )</span>
                                                      </div>}
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[1] && <div className='ba bg-frc-yellow black nowrap fw5 br2 pv1 b--black  ml2 ph2'>
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[1] && <span className='f5 fw5'>{summaryData.matchSummary.bestBatsman.battingStatsList[1].runs}</span>}
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[1] && (
                                                            <span className='f7'>{`(${summaryData.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                                                         )}
                                                      </div>}


                                                   </div>
                                                   {summaryData.matchSummary.bestBatsman.bowlingStatsList && <div className='flex  items-center pv1'>
                                                      <div className='ba bg-silver br2 pv1 fw5 b--black  ph2'> <span className='f5 fw5'>
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].wickets}/
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].runsConceded}
                                                      </span>
                                                         <span className='f7 pl2'>
                                                            ({summaryData.matchSummary.bestBatsman.bowlingStatsList[0].overs})
                                                         </span></div>
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[1] && <div className='ph1'>&</div>}
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[1] && (
                                                         <div className='ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap  ph2'>
                                                            <span className='f5 fw5'>
                                                               {summaryData.matchSummary.bestBatsman.bowlingStatsList[1].wickets}/
                                                               {summaryData.matchSummary.bestBatsman.bowlingStatsList[1].runsConceded}
                                                            </span>
                                                            <span className='f7 pl2'>
                                                               ({summaryData.matchSummary.bestBatsman.bowlingStatsList[1].overs})
                                                            </span>
                                                         </div>
                                                      )}
                                                   </div>}
                                                </div>}
                                          </div>
                                       </div>
                                        
                                       {summaryData.matchSummary.bestBowler.bowlingStatsList && team.matchType !== 'Test' ? 
                                       <div className='w-6/12 flex flex-col p-2 items-center'>
                                          <div className='text-gray-2 font-semibold mb-1 text-xs'>TOP BOWLER</div>
                                          <div className='relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center '>
                                             <div className='overflow-hidden w-full h-full rounded-full '>
                                             <img
                                                   className=' object-top object-contain w-24  rounded-full  '
                                                  
                                                   src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                                onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                   alt=''></img>
                                                
                                             </div>


                                          </div>

                                          <div className='w-full -mt-10 bg-gray-4 text-center  rounded-md px-2'>
                                             <div className="text-base  font-semibold mt-10 truncate">{lang==='HIN'?summaryData.matchSummary.bestBowler.playerNameHindi:summaryData.matchSummary.bestBowler.playerName}</div>
                                             <div className='flex flex-col  items-center justify-between '>
                                                {summaryData.matchSummary.bestBowler.battingStatsList &&
                                                   (summaryData.matchSummary.bestBowler.battingStatsList[0].runs !== 0 ||
                                                      summaryData.matchSummary.bestBowler.battingStatsList[0].balls !== 0) && <div className='ba bg-frc-yellow br2 pv1 b--black  black ph2'> <span className='f5 fw5 mr1 nowrap'>
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[0].runs}
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                      </span>
                                                      <span className='f7 '>({summaryData.matchSummary.bestBowler.battingStatsList[0].balls})</span></div>}
                                                {summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className="f6 fw6 ph2">&</div>}
                                                {summaryData.matchSummary.bestBowler.bowlingStatsList &&
                                                   <div className=''><span className=' font-bold text-white'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      <span className='f7 '>({summaryData.matchSummary.bestBowler.bowlingStatsList[0].overs})</span></div>}
                                             </div>
                                             <div className='text-center mt-2  pb-2'>
                                                         <span className='text-white text-xs'>Eco :</span>
                                                         <span className='text-white text-base font-semibold ml-1'>{ summaryData.matchSummary.bestBatsman.bowlingStatsList&&summaryData.matchSummary.bestBatsman.bowlingStatsList[0].economyRate}</span>
                                                      </div>
                                          </div>
                                       </div> : 
                                       <div className='flex w-100 items-center justify-center pv2 '>
                                          <div className='w-40 flex items-end justify-end'>
                                             {' '}
                                             <img
                                                className='h4 h45-l'
                                                src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                                onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                alt=''
                                             />
                                          </div>
                                          <div className='w-60  flex lh-copy flex-column  items-start justify-start '>
                                             <div className="f6 fw5">{lang==='HIN'?summaryData.matchSummary.bestBowler.playerNameHindi:summaryData.matchSummary.bestBowler.playerName}</div>
                                             <div className="flex   ">
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler && summaryData.matchSummary.bestBowler.battingStatsList && summaryData.matchSummary.bestBowler.battingStatsList.length > 0 && <div className='ba bg-frc-yellow br2 pv1 tc b--black black ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].runs}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                   </span>
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.battingStatsList[0].balls})</span></div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList && <div className='ba mv2  bg-silver br2 tc pv1 b--black  ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      {/* <span>&nbsp;</span> */}
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.bowlingStatsList[0].overs})</span></div>}
                                                </div>
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className='pa1 f7'>&</div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[1] && <div className='mv3 ph1  f7'>&</div>}
                                                </div>
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler && summaryData.matchSummary.bestBowler.battingStatsList && summaryData.matchSummary.bestBowler.battingStatsList.length > 0 && summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className='ba bg-frc-yellow  tc br2 pv1 b--black black  ph2'>{summaryData.matchSummary.bestBowler.battingStatsList[1] && (
                                                      <span className='f6 fw5'>
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[1].runs}
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[1].isNotOut === true ? '*' : ''}
                                                      </span>
                                                   )}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[1] && (
                                                         <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.battingStatsList[1].balls})</span>
                                                      )}</div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[1] && <div className='ba mv2 bg-silver br2 pv1 b--black  tc ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[1].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[1].runsConceded}
                                                   </span>
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.bowlingStatsList[1].overs})</span></div>}
                                                </div>
                                             </div>
                                          </div>
                                       </div>}
                                       </div>
                        
                                       {summaryData.matchSummary.inningOrder
                                          .slice(0, team.matchType !== 'Test' ? 2 : summaryData.matchSummary.inningOrder.length).map((inning, i) => <div key={i}>
                                             <div className='h-solid-divider-light mh3 mv2 '></div>
                                             <div>
                                                <div className='bg-gray-4 m-2 rounded-md p-2'>
                                                   <div className='flex ph3 items-center justify-between'>
                                                      <div className='flex items-center'>
                                                         <img
                                                            className=' w-10 h-10 rounded-full  object-cover objetc-top'
                                                            src={`https://images.cricket.com/teams/${summaryData.matchSummary[inning].teamID}_flag_safari.png`}
                                                            alt=''
                                                            onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                                         />
                                                         <div className='pl2 f6  fw5'>{summaryData.matchSummary[inning].teamShortName.toUpperCase()}</div>
                                                      </div>
                                                      <div className='flex items-center  justify-center'>
                                                         <div className='text-white font-semibold  oswald f3 fw6'>
                                                            {summaryData.matchSummary[inning].runs[i <= 1 ? 0 : 1]}/
                                                            {summaryData.matchSummary[inning].wickets[i <= 1 ? 0 : 1]}

                                                         </div>
                                                         <div className='text-white font-semibold  ml-1'>({summaryData.matchSummary[inning].overs[i <= 1 ? 0 : 1]})</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div className='px-2 pv2 flex w-full '>
                                                   {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman &&
                                                      summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                         .battingStatsList && <div className='flex w-6/12 flex-col ph2'>
                                                             <div className='text-gray-2 uppercase text-xs font-semibold'>Batter</div>
                                                         <div className='flex items-center moon-gray justify-between pv1'>
                                                           
                                                           {lang==='HIN'? <div className="truncate w-75 text-white font-semibold text-xs text-xs"> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .playerNameHindi
                                                            }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs"> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .playerName
                                                            }</div>}
                                                            <div className='text-white font-semibold'> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .battingStatsList[0].runs
                                                            }
                                                               {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .battingStatsList[0].isNotOut === true
                                                                  ? '*'
                                                                  : ''}</div>
                                                         </div>
                                                         {team.matchType !== 'Test' && summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].runnerBatsman &&
                                                            summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].runnerBatsman
                                                               .battingStatsList && <div className='flex moon-gray items-center justify-between pv1 '>
                                                               {lang==='HIN'?<div className="truncate w-75 text-white font-semibold text-xs"> {
                                                                  summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.playerNameHindi
                                                               }</div>:<div className="truncate w-75 text-white font-semibold text-xs"> {
                                                                  summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.playerName
                                                               }</div>}
                                                               <div className='text-white font-semibold'> {
                                                                  summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.battingStatsList[0].runs
                                                               }
                                                                  {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.battingStatsList[0].isNotOut === true
                                                                     ? '*'
                                                                     : ''}</div>
                                                            </div>}
                                                      </div>}
                                                   <div className='bl  b--white-20'></div>
                                                  


                                                   <div className=' flex w-6/12 flex-col px-2'>
                                                   <div className='text-gray-2 uppercase text-xs font-semibold'>Bowler</div>
                                                      {summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                         i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                      ].topBowler &&
                                                         summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                            i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                         ].topBowler.bowlingStatsList && <div className='flex moon-gray items-center justify-between pv1 '>
                                                            {lang==='HIN'?<div className="truncate w-75 text-white font-semibold text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.playerNameHindi
                                                            }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.playerName
                                                            }</div>}
                                                            <div className='text-white font-semibold'>{
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.bowlingStatsList[0].wickets
                                                            }/
                                                               {
                                                                  summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                     i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                                  ].topBowler.bowlingStatsList[0].runsConceded
                                                               }</div>
                                                         </div>}
                                                      {team.matchType !== 'Test' && summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                         i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                      ].runnerBowler &&
                                                         summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                            i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                         ].runnerBowler.bowlingStatsList && <div className='flex moon-gray items-center justify-between pv1  '>
                                                            {lang==='HIN'?<div className="truncate w-75 text-white font-semibold text-xs text-xs" > {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.playerNameHindi
                                                            }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.playerName
                                                            }</div>}
                                                            <div className='text-white font-semibold'> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.bowlingStatsList[0].wickets
                                                            }/
                                                               {
                                                                  summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                     i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                                  ].runnerBowler.bowlingStatsList[0].runsConceded
                                                               }</div>
                                                         </div>}
                                                   </div>
                                                </div>
                                             </div></div>)}

                                            


                                   
                                     
                                                </div>
                                    </div>
                                 )}
                                 
                              </>
                           )) : <div className="white f5 fw5 pv2  tc ">No match found</div>}
                           
                        </div>
                     </>
                  </div>}
               </div>
               </div>
               {TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.playerRecordBatting && TeamStatHub.teamHub.playerRecordBatting.totalRuns && TeamStatHub.teamHub.playerRecordBatting.totalRuns.length > 0 && <div>
                  {/* <div className='bb b--white-20 mt3 mb2'></div> */}


                  <div className='bg-gray py-4 mt-2 rounded-xl mx-3'>
                     <div className='flex items-center justify-between'>
                     <div className='text-base ml-3  font-bold text-white uppercase  py-2 '>{getLangText(lang,words,'PLAYER RECORDS')}</div>
                     {(playerRecord === 'BATTING' && TeamStatHub.teamHub.playerRecordBatting[battingType].length > 0) || (playerRecord === 'BOWLING' &&
                      TeamStatHub.teamHub.playerRecordBowling[bowlingType].length > 0) ?
                              <div>
                                 {!viewAll && <div className='flex  mr-4 bg-gray-8 uppercase flex-col  items-center justify-center' onClick={() => { setViewAll(true) }}>
                                    <div className='border  border-green  p-2  rounded text-green text-xs  font-semibold'>{getLangText(lang,words,'View All')}</div>

                                 </div>}

                              </div> : <></>}
                     </div>

                     
                     <div className='bg-blue-8 w-16 rounded h-1 ml-2 mb-2'></div>
                     {false && TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div className="silver f8 f7-l f7-m pv1 ">
                        {lang==='HIN'?
                        <span>{` सभी ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.homeTeamShortName} vs ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.awayTeamShortName} मैचों से`}</span>
                        :<span>{`* From matches involving  ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.homeTeamShortName} and ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.awayTeamShortName}`}</span>}
                        </div>}



<div className='flex items-center justify-center'>
                     <div className='flex items-center bg-gray-4  w-10/12 justify-center rounded-full  bg-gray-8 m-2'>
                        {playerRecordTab.map((team, i) => (
                           <div
                              onClick={() => { setPlayerRecord(team); setViewAll(false) }}
                              className={`${playerRecord === team ? ' border border-green pv1 ' : ' white pv1 '
                                 }    px-3  capitalize  cursor-pointer w-6/12 rounded-full text-center p-1 `}

                              key={i}>
                              {getLangText(lang,words,team)}
                           </div>
                        ))}
                        {/* style={{borderBottomColor: "#777"}} */}
                     </div>
                     </div>
                     
                     <>
                        <div className=' bg-gray pt-2  m-2  white rounded text-xs f6-l   ' >
                           {playerRecord === "BATTING" ? <div className={`  relative  w-40 ml2     mt2 `} ref={wrapperRef}>
                              <div
                                 className={`h-10   ${showDropDown ? 'br--top' : ''} rounded  text-white border flex items-center justify-center flex-column cursor-pointer   bg-gray-4`}
                                 onClick={() => {
                                    setShowDropDown((prev) => !prev);
                                 }}>
                                 <div className='flex text-center  pa1 items-center justify-center '>
                                    <div className='w-80  f6 fw5 ttc  ml2 pa1 text-white'>{battingType === "battingAverage" ?getLangText(lang,words,'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang,words,'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang,words,'RUNS') : "6s"} </div>

                                    <div className='w-20 tc  bl b--white-20 flex items-center justify-center'>
                                       <img className='w1  ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                    </div>
                                 </div>
                              </div>
                              {showDropDown && (
                                 <div
                                    className='  text-white border absolute w-full h-40 overflow-y-scroll z-99999 overflow-hidden bg-gray-4' style={{ bottom: 0, left: 0, top: 33 }}>
                                    {battingTab.map((tabName, i) => (
                                       <div
                                          key={i}
                                          onClick={() => {
                                             setShowDropDown((prev) => !prev), setBattingType(tabName); setViewAll(false)
                                          }}
                                          className='p-2 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30'>
                                          <div className='pl2 nowrap white ttc  pb2 black fw5 f6'>{tabName === "battingAverage" ? <div>{getLangText(lang,words,'AVERAGE')}</div> : tabName === "battingStrikeRate" ? <div>{getLangText(lang,words,'S/R')}</div> : tabName === "fifties" ? <div>50s</div> : tabName === "hundreds" ? <div>100s</div> : tabName === "totalFours" ? <div>4s</div> : tabName === "totalRuns" ? <div>{getLangText(lang,words,'RUNS')}</div> : "6s"}</div>
                                       </div>
                                    ))}
                                 </div>
                              )}
                           </div> : <div className={` relative  w-40 ml2     mt2`} ref={wrapperRef}>
                              <div
                                 className={`  ${showDropDown ? 'br--top' : ''} rounded border  flex items-center flex-column cursor-pointer   bg-gray-4`}
                                 onClick={() => {
                                    setShowDropDown((prev) => !prev);
                                 }}>
                                 <div className='flex w-full  p-1 items-center justify-center '>
                                    <div className='w-8/12  text-center text-xs font-semibold ttc ml-2 p-1'>{bowlingType === "bowlingStrikeRate" ? getLangText(lang,words,'S/R') : bowlingType === "economy" ? getLangText(lang,words,'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" :getLangText(lang,words,'wickets')} </div>

                                    <div className=' text-center w-4/12 text-center bl b--white-20 flex items-center justify-center'>
                                       <img className='w1  ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                    </div>
                                 </div>
                              </div>
                              {showDropDown && (
                                 <div
                                    className='  border   rounded w-full h-40   absolute w-fukk  overflow-y-scroll z-99999 overflow-hidden bg-gray-4' style={{ bottom: 0, left: 0, top: 33 }}>
                                    {bowlingTab.map((tabName, i) => (
                                       <div
                                          key={i}
                                          onClick={() => {
                                             setShowDropDown((prev) => !prev); setBowlingType(tabName); setViewAll(false)
                                          }}
                                          className='ph2 pv1 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30 '>
                                          <div className='ma0 pb0  nowrap white  pb2 black ttc fw5 f6'> {tabName === "bowlingStrikeRate" ? <div>{getLangText(lang,words,'S/R')}</div> : tabName === "economy" ? <div>{getLangText(lang,words,'ECONOMY')}</div> : tabName === "fiveWickets" ? <div>5-W</div> : tabName === "threeWickets" ? <div>3-W</div> :getLangText(lang,words,'wickets')}</div>
                                       </div>
                                    ))}
                                 </div>
                              )}
                           </div>}
                           {(playerRecord === 'BATTING' && TeamStatHub.teamHub.playerRecordBatting[battingType].length > 0) || (playerRecord === 'BOWLING' && TeamStatHub.teamHub.playerRecordBowling[bowlingType].length > 0) ?
                              <div>
                                 {TeamStatHub && TeamStatHub.teamHub && (playerRecord === 'BATTING' ? TeamStatHub.teamHub.playerRecordBatting[battingType] : TeamStatHub.teamHub.playerRecordBowling[bowlingType]).map((item, index) => {
                                    return (
                                       <div key={index} className={`${!viewAll && index > 5 ? 'dn' : 'db'}`}>
                                          <div className='flex       justify-between pv2 mv2 bg-gray-4 rounded-xl m-2 p-2 '>
                                             <div className='flex  w-80  justify-start items-center'>
                                                <div className=' flex  w-30 items-center  bg-gray h-12 w-12 rounded-full   justify-center '>
                                                   <img className='h-12 w-12 rounded-full bw1  bg-dark-gray object-cover object-top ' src={`https://images.cricket.com/players/${item.playerID}_headshot.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} alt='' />
                                                </div>

                                                <div className=' w-70 ph2 pl-3'>
                                                   <div className='flex  flex-col items-start justify-between'>
                                                      <div className='  text-xs font-semibold'>{isEnglish(lang) ? item.playerName : item.playerNameHindi}</div>
                                                      <div className="w-30 flex items-end justify-end">
                                                         <div className='border mt-2  rounded  border border-green text-gray-2   px-2' >{item.shortName}</div>
                                                      </div>
                                                   </div>
                                                   <div className=' flex pt1  justify-between mt-2'>

                                                      <div className="w-50  flex items-start justify-start mx-2 ">
                                                         <div className='bg-gray-8   rounded    text-center text-gray-2  captilize px-2 py-1' >{item.totalMatches} {getLangText(lang,words,'matches')}</div>

                                                      </div>
                                                      {playerRecord === 'BATTING' && battingType !== "totalRuns" && 
                                                      <div className=" w-50 flex items-center  justify-center"> 
                                                      <div className='  bg-gray-8   text-white rounded  py-1 px-2'>{item.playerRuns} {getLangText(lang,words,'RUNS')}</div></div>}
                                                      {playerRecord === 'BOWLING' && bowlingType !== "totalWickets" && <div className=" w-50 flex items-end justify-end"> 
                                                      <div className='bg-gray-8   text-white rounded  py-1 px-2'>{item.totalWickets} {getLangText(lang,words,'WICKETS')}</div></div>}
                                                   </div>
                                                </div>
                                             </div>

                                             <div className='w-20 tc bg-gray-8 rounded m-2 p-2 flex    items-center justify-center'>
                                                                                               <div className=' text-base text-green oswald  flex  flex-col  items-center justify-center'>
                                                                                               <div className="fw5 ttc text-gray-2 f6-l text-xs">{playerRecord === 'BATTING' ? (battingType === "battingAverage" ?getLangText(lang,words,'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang,words,'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang,words,'RUNS') : "6s") : (bowlingType === "bowlingStrikeRate" ? getLangText(lang,words,'S/R') : 
                                                bowlingType === "economy" ? getLangText(lang,words,'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" : getLangText(lang,words,'wickets'))}</div>
 
                                                   {playerRecord === 'BATTING' ? TeamStatHub.teamHub.playerRecordBatting[battingType][index][battingType === "totalRuns" ? "playerRuns" : battingType] : TeamStatHub.teamHub.playerRecordBowling[bowlingType][index][bowlingType]} </div>
                                             </div>
                                          </div>
                                       </div >
                                    );
                                 })}
                              </div> : <div className="white  tc pv3 ph2 fw5 f6 f5-l f5-m">Data not available</div>
                           }
                       

                        </div>

                     </>

                  </div>
               </div>}

            </div> : <div className="w br2 ba  b--white-20 ">
            <DataNotFound />
              </div>}
         </>

      );
}

// WebkitLineClamp: 2