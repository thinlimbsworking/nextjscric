import React from 'react'
import { HOME_PAGE_GET_ALGO11 } from '../../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks'
const arrow = '/pngsV2/fantasy_Icon.png'
import { useRouter } from 'next/router'
import TeamSlider from '../commom/teamslider'
import ImageWithFallback from '../commom/Image'
export default function BuildYourTeam({
  algoteam,
  getLangText,
  lang,
  algo11,
  words,
  ...props
}) {
  console.log('algoteam in buildteam - ', algoteam)
  const router = useRouter()
  const navigate = router.push
  const { loading, error, data } = useQuery(HOME_PAGE_GET_ALGO11, {
    variables: { matchID: props.matchID },
  })
  // console.log("matchIDmatchIDmatchIDmatchIDmatchIDmatchID====>>>",data&&data.homePageGetAlgo11)
  const getUpdatedTime = (updateTime) => {
    let currentTime = new Date().getTime()
    let distance = currentTime - updateTime
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    let sec = Math.floor((distance % (1000 * 60)) / 1000)

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago'
    return result
  }
  ;<style jsx>{`
    ::-webkit-scrollbar {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height: 4px;
      background-color: #2b323f;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px;
      background-color: #6a91a9;
    }
  `}</style>
  return algoteam && algoteam.length > 0 ? (
    <div className=" border-blue-4 w-4/5 h-96 border-y-3 border-r-3 rounded-r-3xl mt-5 relative text-white px-4  py-3">
      <div className="flex justify-between items-center ">
        <div>
          <div className="font-semibold text-lg">
            {getLangText(lang, words, 'Cricket.com teams')}
          </div>
          <div className="w-14 bg-blue-8 h-1 rounded-full mt-1"></div>
        </div>
        {/* <img className='w-10' src={arrow} /> */}
      </div>
      <div className="pt-4">
        {/* <p className='font-semibold text-sm'>{getLangText(lang, words, 'Cricket.com teams')} </p> */}
        <p className="text-sm font-light pt-1">
          {getLangText(
            lang,
            words,
            'A few of our best and curated suggestions',
          )}
        </p>
      </div>
      {/* {console.log("ddede",data.homePageGetAlgo11)} */}
      <p className="pt-2 text-xs text-gray-2 font-light">
        *Last Updated {getUpdatedTime(algo11.getAlgo11.timestamp)}
      </p>
      <div className="absolute right-2 top-5">{props.comp}</div>
      <div className="absolute w-full bg-gray-4 shadow-md shadow-gray-4 my-3 rounded-md">
        <div className="">
          <div className="flex justify-center items-center p-2">
            <p className="text-xs font-medium">PROJECTED SCORE :</p>
            <p className="text-green pl-1 text-sm font-medium">
              {' '}
              {data && data.homePageGetAlgo11.totalProjectedPoints}
            </p>
          </div>
          {/* <TeamSlider data={data &&
                data.homePageGetAlgo11 &&
                data.homePageGetAlgo11.batsman &&
                data.homePageGetAlgo11.batsman} /> */}
          <div className="bg-gray rounded-lg p-1 overflow-x-auto w-full ">
            <div className="scrollmenu flex  overflow-x-scroll ">
              {algoteam &&
                algoteam.length > 0 &&
                algoteam.map((item, y) => (
                  <div className="  mr-2 border-r-2 border-gray-4 p-2 px-4 w-32 ">
                    <div className="flex w-full justify-center">
                      <div className="flex flex-col items-center ">
                        <div className="relative mb-2  flex justify-center">
                          <ImageWithFallback
                            height={28}
                            width={28}
                            loading="lazy"
                            fallbackSrc="/pngsV2/playerph.png"
                            className="h-12 w-12 bg-gray-4 rounded-full  object-cover object-top"
                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                            // onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
                          />
                          <ImageWithFallback
                            height={18}
                            width={18}
                            loading="lazy"
                            fallbackSrc="/svgs/empty.svg"
                            className="w-3 h-3 drop-shadow-lg absolute left-0 bottom-0 object-center flex justify-center rounded-full ba"
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            // onError={(evt) => (evt.target.src = '/svgs/empty.svg')}
                          />
                          {(item.captain == '1' ||
                            item.vice_captain == '1') && (
                            <div className="w-3 h-3 absolute right-[-4px] bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium">
                              {item.captain == '1'
                                ? 'C'
                                : item.vice_captain == '1'
                                ? 'VC'
                                : ''}
                            </div>
                          )}
                        </div>
                        <p
                          className="text-xs font-medium  w-full truncate "
                          style={{ wordWrap: 'break-word' }}
                        >
                          {lang === 'HIN'
                            ? item.playerNameHindi
                            : item.playerName}
                        </p>
                        <p className="text-gray-2 text-xs uppercase text-center">
                          {item.player_role}
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <></>
  )
}
