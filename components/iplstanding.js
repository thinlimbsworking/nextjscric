import React from 'react';
import { HOME_PAGE_POINTS_TABLE_QUEY } from '../api/queries';
import { useQuery } from '@apollo/react-hooks';
import { useRouter } from 'next/router';

export default function Iplstanding(props) {
  const router = useRouter();
  const navigate = router.push;

  let { loading: loading, error: error, data } = useQuery(HOME_PAGE_POINTS_TABLE_QUEY, {});
  if (error) return <div></div>;
  if (loading) return <div></div>;
  else
    return data && data.topFourTeams && data.topFourTeams.teams && data.topFourTeams.teams[0] ? (
      <div className='lp:mt-0 mt-5 text-white px-1 mx-2'>
        <div className='flex justify-between items-center'>
          <div className=' font-semibold text-lg tracking-wider '> IPL Team Standings</div>
          <div
            className='rounded border-2  text-green border-green text-xs px-2 py-1.5 font-semibold bg-gray-8'
            onClick={() =>
              navigate(
                'series/[...slugs]',
                `series/domestic/${data.topFourTeams.tourID}/${data.topFourTeams.seriesName
                  .replace(/[^a-zA-Z0-9]+/g, ' ')
                  .split(' ')
                  .join('-')
                  .toLowerCase()}/points-table`
              ).then(() => window.scrollTo(0, 0))
            }>
            VIEW ALL
          </div>
        </div>
        <div className='flex flex-col bg-gray rounded-lg  mt-3'>
          <div className='flex justify-between bg-gray-4 m-1 rounded-t-lg px-2 py-3 text-white font-bold text-sm'>
            <div className=' flex items-center justify-start w-4/12 '> TEAMS </div>
            <div className=' flex items-center  w-8/12  justify-between'>
              <div className='w-1/4 text-center'>P</div>
              <div className='w-1/4 text-center'>W</div>
              <div className='w-1/4 text-center'>PTS</div>
              <div className='w-1/4 text-center'>QL%</div>
            </div>
          </div>

          {data &&
            data.topFourTeams &&
            data.topFourTeams.teams &&
            data.topFourTeams.teams[0].map((item, index) => {
              return (
                index < 5 && (
                  <div key={index} className='flex   my-3 px-2 '>
                    <div className=' flex items-center justify-start w-4/12  text-gray-2 font-semibold text-sm  '>
                      <img
                        className='h-5 w-7  rounded-md mr-2'
                        src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                        alt=''
                      />
                      {item.teamName}
                    </div>
                    <div className=' flex items-center  w-8/12  justify-between text-gray-2 font-semibold'>
                      <div className=' w-1/4 text-center'>{item.matchesPlayed}</div>
                      <div className=' w-1/4 text-center'>{item.wins}</div>
                      <div className='text-white text-bold w-1/4 text-center '>{item.points}</div>

                      <div className=' w-1/4 text-center'>{item.qp}%</div>
                    </div>
                  </div>
                )
              );
            })}
        </div>
      </div>
    ) : (
      <></>
    );
}
