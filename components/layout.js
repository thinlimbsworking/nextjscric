import Header from './header';
import Footer from './footer';
import React, { useEffect, useState } from 'react';
// import Footer from './footer';
 //import BottomNavBar from '../components/BottomNavBar';

 import BottomNavBar from '../components/commom/bottomnavbar';
// import Loading from '../components/Loading';
import { useRouter } from 'next/router';

const Layout = ({ children, isLoading, ...props }) => {
  let router = useRouter();
  const [screenWidth, setscreenWidth] = useState();

  useEffect(() => {
    if (typeof window !== 'undefined' && window && window.screen && window.screen.width) {
      setscreenWidth(window.screen.width > 975 ? 976 : window.screen.width);
    }
  }, []);
  return (
    <>
      <Header />
     
      <div className={`bg-basebg px-0  md:px-28 lg:px-52 xl:px-60    md:pt-24 lg:pt-24 md:pb-24 lg:pb-24  lg:h-screen md:h-screen overflow-auto`}>
            {children}
      </div>

      {/* {console.log("router.asPathrouter.asPathrouter.asPathrouter.asPath", router.asPath)} */}
      {((!router.asPath.includes('/criclytics/')  && !router.asPath.includes('match-score') && !router.asPath.includes('/series/')
      && !router.asPath.includes('create-team')  && !router.asPath.includes('/fantasy-research-center')
      &&!router.asPath.includes('live-score')) && (
     (router.asPath.includes('/news/')?router.asPath.length<22?true:false:true)


       
      )) 
      
      
      && <div className=' w-full z-50 md:hidden'>
        <BottomNavBar />
       
      </div>
      }
      <div className="hidden md:block lg:block">
        <Footer screenWidth={screenWidth} /></div>
    </>
  );
};

export default Layout;







