import React, { useEffect, useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { LAST_12_BALLS } from '../api/queries';
import Heading from '../components/commom/heading'
export default function Liveballs(props) {
  const [toggle, setToggle] = useState('LIVE');
  const { loading, error, data } = useQuery(LAST_12_BALLS, {
    variables: { matchID:  props.matchID,
      innings: props.matchData.currentinningsNo, 
    }
  });

  console.log("propspropsprops1212121212",props,data)
  return (
    data &&
          data.last12Balls && data.last12Balls.length>0 ? 
          <div className=' mt-5 mx-2 rounded text-white'>
         {!props.hideTitle&& <Heading heading={'Last 12 Balls'} />}
   
      <div className='flex justify-between scrollmenu mt-3'>
        {data &&
          data.last12Balls && 
          data.last12Balls.map((overs, i) => (
            <div className='p-1 bg-gray  rounded mr-1 lg:w-6/12 md:w-6/12 flex flex-col lg:flex-row md:flex-row py-3 items-center ' key={i}>
               <div className='hidden lg:block md:block text-gray-2 font-medium text-sm text-left mt-1 w-20 ml-2 '>Over {overs.overNumber}</div>
              <div className='flex justify-between  w-full'>
                {overs.over.map((balls, y) => (
                  <div className='w-full flex mt-1' key={y}>
                    {/* <div className='h-5 w-5 text-xs flex items-center justify-center m-1 rounded-full bg-green '> </div> */}
                      {balls.isBall
                        ? (
                            balls.isWicket ==="true" ? (
                              <div
                                key={i}
                                className={`bg-red text-black justify-center flex items-center mx-1 h-6 w-6 text-xs font-semibold rounded-full `}>
                                W
                              </div>
                            ) : (
                              balls.runs && (
                                <div
                                  key={i}
                                  className={`h-5 w-5 lg:w-8 lg:h-8 rounded-full justify-center flex items-center text-black ${
                                    balls.runs === '6' ? 'bg-green' : balls.runs === '4' ? 'bg-green' : 'bg-gray-2'
                                  } white f7 fw6 mx-1`}>
                                  {balls.type === 'no ball'
                                    ? 'nb'
                                    : balls.type === 'leg bye'
                                    ? 'lb'
                                    : balls.type === 'bye'
                                    ? 'b'
                                    : ''}
                                  {balls.type === 'no ball' || balls.type === 'leg bye' || balls.type === 'bye'
                                    ? balls.runs >= 1
                                      ? balls.runs
                                      : ''
                                    : balls.runs}
                                </div>
                              )
                            )
                          )
                        : 
                        
                            <div
                              key={y}
                              className={`justify-center flex items-center mx-1 h-6 w-6 text-xs font-normal bg-gray-3 rounded-full
                              `}>
                              {balls.type === 'no ball'
                                ? 'nb'
                                : balls.type === 'leg bye'
                                ? 'lb'
                                : balls.type === 'bye'
                                ? 'b'
                                : balls.type === 'wide'
                                ? 'wd'
                                : ''}
                              {balls.type === 'no ball' || balls.type === 'leg bye' || balls.type === 'bye' || balls.type === 'wide'
                                ? balls.runs >= 1
                                  ? balls.runs
                                  : ''
                                : balls.runs}
                            </div>
                          
                          }
                   
                  </div>
                ))}
              </div>
              <div className='w-full  lg:hidden md:hidden pt-2 text-gray-2 font-medium text-xs text-left mt-1'>Over {overs.overNumber}</div>
            </div>
          ))}
      </div>
    </div>:<></>
  );
}
// ${ data.getcriclyticsCommonApi.lasttenball.length > 6 ? '' : 'mh2' }
