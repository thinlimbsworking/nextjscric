import React from 'react';
const arrow = '/pngsV2/arrow.png';
const icon = '/pngsV2/liveUpdateIcon.png';
import {GET_HOME_PAGE_LIVE_COMMENTS} from '../api/queries'
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useRouter } from 'next/router';

export default function LiveUpdates(props) {
  const router = useRouter();
  const navigate = router.push;
  const { loading, error, data } = useQuery(GET_HOME_PAGE_LIVE_COMMENTS, {
		// variables: { matchID: props.matchID},
		variables: { matchID: props.matchID||''},

	
	  });


  const redirectToFile = () => {
    
        navigate(
          `news/live-blog/${props.matchID}/${data.getHomePageLiveComments.matchSlug.toLowerCase()}/${data.getHomePageLiveComments.articleID.toLowerCase()}`
        )
  };
  return (
    data && data.getHomePageLiveComments &&  data.getHomePageLiveComments.articleID ? <div className='bg-gray rounded-md p-3 mx-1 text-white mt-3'>
      <div className='flex justify-between items-center'>
        <div className='text-xl font-semibold'>Live Updates</div>
        <img className='bg-basebg p-2 w-12 rounded-lg' src={arrow} onClick={()=>redirectToFile()} />
      </div>
      <div className='bg-gray-4 rounded-lg p-3 my-4'>
        <div className='text-xs text-left text-gray-2 font-semibold'>{data.getHomePageLiveComments.matchName}</div>
        <div className='timelineLiveBlog mt-3 w-full '>
          {data.getHomePageLiveComments.comments.map((x, y) => (
            y<=2 && <div key={y} className={` ${y === 0 ? '' : 'pt-3'} w-100 flex items-center `}>
              <div
                className={`${
                  y == 0 ? 'w-6 h-6 -ml-1.5' : 'w-3 h-3'
                } relative bg-blue rounded-full flex justify-center items-center `}>
                {y === 0 && <img className='w-4 h-4' src={icon} />}
              </div>
              <div className={`${y == 0 ? ' text-sm' : 'text-gray-2  pl-1 text-sm'} ml-3 w-11/12 font-normal leading-5`}>
               <div className='font-semibold'>{x.currentScore}</div>
               <div className='pt-1'>{x.title}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>:<></>
  );
}
