import React, { useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { set } from 'date-fns';

const URL ='https://apiv2.cricket.com/cricket' ?'https://apiv2.cricket.com/cricket' : 'https://apiv2.cricket.com/cricket';

export default function Getotp(props) {
  // console.log("/slug/slug/slug",props)
  let router = useRouter();

  const Empty = '/svgs/Empty.svg';
  const login = '/pngs/bigind.png';

  const re = /^[0-9\b]+$/;

  const  disbleButton= ' br2 fw5 f7 p-2   mt-2 text-center cursor-pointer white mt2 bg-gray border rounded border-gray-2 text-gray-2  text-xs w-6/12 ttu';
  const enbaleButton  = ' br2 fw5 f7 p-2 mt-2  text-center cursor-pointer white mt2 bg-gray h2 w-6/12 ttu rounded border border-green text-green bg-black';

  const [mobNum, setMobNum] = useState('');
  const [firstEmail, setFirstEmail] = useState(false);

  // const [email, setEmail] = useState('');
  const re2 =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

  const populate = (value) => {
    if (!value.includes('@')) {
      props.setEmail(value);
    } else {
      if (!firstEmail) {
        setFirstEmail('true');
        props.setEmail(`${props.email}@gmail.com`);
      } else {
        props.setEmail(value);
      }
    }

    setErrorMsg('');
  };
  const [errorMsj, setErrorMsg] = useState('');

  const handlePress = () => {
    // step == 'checkemail' ||step=='checkphone'
    if (mobNum.length === 10 || props.email.length > 3) {
      axios
        .post(`https://apiv2.cricket.com/cricket/services/getotp`, {
          email: props.email,
          checkphone: props.emailLogin ? false : true,

          phnumber: mobNum
        })
        .then((res) => {
          const persons = res.data.Details;
          const persons2 = res.data.Status;

          if (res.data.code == 200) {
            props.setMobnum(mobNum);
            props.setStep('ConfirmOtp');
            props.setSession(res.data.Details);
          } else {
            setErrorMsg(res.data.Details);
          }
        })
        .catch((error) => {
          console.log('error', error);
        });
    }
  };

  const handleKeyPress = (e) => {
    if (e.keyCode == 13 && mobNum.length === 10) {
      handlePress();
    }
  };
  const handleChange = (value) => {
    setErrorMsg('');
    value === '' || re.test(value) ? setMobNum(value) : null;
  };

  return (
    <>
      {
        <div className='text-white'>
          {!router.asPath.includes('/login')&& <div className='flex justify-end right-0 z-9999 ' style={{ top: '-30px' }}>
            <img
              className='h-5 w-5  cursor-pointer br-100 '
              alt='close icon'
              src={'/pngs/closeIcon.png'}
              onClick={() => {
                if (props.onCLose !== '/slug') {
                  props.onCLose === '/more' ||
                  props.onCLose === '/fantasy-research-center' ||
                  props.onCLose === '/match-score'
                    ? props.setShowLogin(false)
                    : (window.location = '/');
                } else {
                  window.location = '/fantasy-research-center';
                }
              }}
            />
          </div>}

          <div
            className={
              props.modalText ? 'p-8 text-base w-full mt4 font-medium  text-center' : 'w-full p-3  text-center text-base font-medium tl mt4   center pa1 ml3'
            }>
           Login / Signup
          </div>

          {!props.emailLogin ? (
            <div className='p-4 text-center  flex flex-col items-center justify-center'>
              <div className='border-b pb-2 border-gray-2 flex items-center flex-row b--black'>
                <span>
                  <img className='w-8 h-5 mx-2' src={login} alt='' />
                </span>
                <input
                  id='name'
                  value='+91'
                  className='w-8 white mx-1 bg-gray'
                  placeholder='Mobile number'
                  type='text'
                  aria-describedby='name-desc'
                />

                <input
                  id='name'
                  pattern='[0-9]*'
                  type='tel'
                  onKeyDown={handleKeyPress}
                  value={mobNum}
                  maxLength={10}
                  className=' w-10/12 lg:w-8/12 md:w-8/12 text-white bg-gray'
                  onChange={(e) => {
                    handleChange(e.target.value), setErrorMsg('');
                  }}
                  placeholder='Mobile number'
                  aria-describedby='name-desc'
                />
              </div>

              <div className=' justify-center mt-3 flex flex-col items-center text-white my-4'>
                <div
                  onClick={mobNum.length == 10 ? handlePress : null}
                  className={mobNum.length == 10 ? enbaleButton : disbleButton}>
                  GET OTP
                </div>

                <div className='pt-2 f7 f6-l tc text-xs my-4'>
                  <span className='text-xs -mr-1'>or</span>
                <span className='text-xs font-bold cursor-pointer ml-2 mr-1 text-red' onClick={() => props.setEmailLogin(true)}>
                    Click Here
                  </span>
                  to login with your email.
                </div>
              </div>
            </div>
          ) : (
            <div className='p-3 center w-100'>
              <div className='w-full border-b pb-2 border-gray-2  flex items-center flex-row  b--black '>
                <input
                  id='name'
                  onKeyDown={handleKeyPress}
                  value={props.email}
                  className=' w-10/12 lg:w-11/12 md:w-11/12   white  mb2 bg-gray '
                  onChange={(e) => {
                    populate(e.target.value);
                  }}
                  placeholder='Email'
                  type='text'
                  aria-describedby='name-desc'
                />
              </div>

              <div className='flex justify-center  mt-3 flex-col items-center '>
                <div
                  onClick={re2.test(String(props.email).toLowerCase()) ? handlePress : null}
                  className={re2.test(String(props.email).toLowerCase()) ? enbaleButton : disbleButton}>
                  verify
                </div>

                <div className='mt-2  text-xs  f6-l text-center'>
                  <div className='mb-2'>or</div>
                  Login via Mobile number ? {' '}
                  <span
                    className='text-xs font-bold cursor-pointer    ml-2 text-red'
                    onClick={() => {
                      props.setEmailLogin(false), setErrorMsg('');
                    }}>
                    Click Here
                  </span>
                </div>
              </div>
            </div>
          )}

          {errorMsj !== '' && <div className='  f7 fw6  tc text-red text-xs center mt2 '>{errorMsj}</div>}

          {/* {false && (
            <>
              <div className='flex p-3 mb2 mt4'>
                <div className=' mh2 '>
                  <img src={'/svgs/expert.svg'} alt='' />
                </div>
                <div className=' w-80 tl'>
                  <div className='f8  fw6 darkRed lh-copy'>EXPERT TEAM</div>
                  <div className='f8  fw5  black lh-copy'>
                    Use our algorithm-generated ready-to-use teams to build your perfect fantasy team.
                  </div>
                </div>
              </div>

              <div className='flex p-3 mb2 mt4'>
                <div className=' w-80 tr '>
                  <div className='f8  fw6  darkRed lh-copy'>POINTS PROJECTIONS</div>
                  <div className='f8  fw5  black  lh-copy'>
                    Points projections for each player to help you to choose your best XI.
                  </div>
                </div>
                <div className=' mh2'>
                  <img src={'/svgs/pointproj.svg'} alt='' />
                </div>
              </div>
              <div className='flex p-3 mb2 mt4'>
                <div className=' mh2  '>
                  <img src={'/svgs/export.svg'} alt='' />
                </div>
                <div className=' w-80 tl'>
                  <div className='f8  fw6  darkRed lh-copy'>EXPORTS YOUR TEAM</div>
                  <div className='f8  fw5  black lh-copy'>
                    Exporting your saved teams to FanFight is now just a tap away
                  </div>
                </div>
              </div>
            </>
          )} */}
        </div>
      }
    </>
  );
}
