import React, { useState } from 'react';

import { LOGIN_OTP_MUTATION } from '../../api/queries';
import { useMutation } from '@apollo/react-hooks';

export default function DesktopOtp(props) {
  const { data } = props;
  const re = /^[0-9\b]+$/;

  const enbaleButton = 'ba br2 fw5 f7 pa2 ph3 tc cursor-pointer white mt2 cdcgr h2 w-50';
  const disbleButton = 'ba br2 fw5 f7 pa2 ph3 tc cursor-pointer white mt2 bg-gray h2 w-50';

  const [mobNum, setMobNum] = useState('');
  const [errorMsj, setErrorMsg] = useState('');
  const [LoginOtp, response] = useMutation(LOGIN_OTP_MUTATION, {
    onCompleted: (responseOtp) => {
    
      if (responseOtp.login.code === 200) {
        props.setStep('ConfirmOtp');
      }

      if (responseOtp.login.code == 202) {
        setErrorMsg(responseOtp.login.message);
      }
    }
  });

  const handlePress = () => {
    if (mobNum.length == 10) {
      LoginOtp({ variables: { type: 'mobile', number: mobNum, email: '' } });

      props.setMobnum(mobNum);

     
    }
  };

  return (
    <>
      <div className='ma3 nt6  mw75-l center '>
        <div className='bg-white br2'>
          <div className='pt2 ph3 relative'>
            <div class='mw9 center ph3-ns'>
              <div className='cf ph2-ns'>
                <div className='fl w-100 w-50-ns pa2'>
                  <div className=''>
                    <div class='pa4 black-80 center '>
                      <div class='measure '>
                        <input
                          id='name'
                          value={mobNum}
                          maxLength={10}
                          class=' w-100 border-input mb2 mt4 ba b--black-05 '
                          onChange={(e) =>
                            e.target.value === '' || re.test(e.target.value) ? setMobNum(e.target.value) : null
                          }
                          placeholder='Mobile number'
                          type='text'
                          aria-describedby='name-desc'
                        />
                      </div>
                      <div className='flex justify-center  mt3'>
                        <div
                          onClick={mobNum.length == 10 ? handlePress : null}
                          className={mobNum.length == 10 ? enbaleButton : disbleButton}>
                          GET OTP
                        </div>
                      </div>

                      {errorMsj !== '' && <div className='  f7 fw6  tc red center mt2 '>{errorMsj}</div>}
                    </div>
                  </div>
                </div>

                {/* 2nd div */}
                <div className='fl w-100 w-50-ns pa2'>
                  <div className=' pv4'>
                    <div class='flex pa2 mb2 '>
                      <div class=' w-10   '>
                        <img src={require('../../public/svgs/expertTeam.svg')} alt='' />
                      </div>
                      <div class=' w-80 tl'>
                        <div class='f8  fw5  orange lh-copy'>EXPERT TEAM</div>
                        <div class='f8  fw5  black lh-copy'>
                          {' '}
                          Use our algorithm-generated ready-to-use teams to build your perfect fantasy team.
                        </div>
                      </div>
                    </div>

                    <div class='flex pa2 mb2 '>
                      <div class=' w-80 tr '>
                        <div class='f8  fw5  orange lh-copy'>POINTS PROJECTIONS</div>
                        <div class='f8  fw5  black  lh-copy'>
                          Points projections for each player to help you to choose your best XI.
                        </div>
                      </div>
                      <div class=' w-10 ma1 '>
                        <img src={require('../../public/svgs/pointproj.svg')} alt='' />
                      </div>
                    </div>
                    <div class='flex pa2 mb2 '>
                      <div class=' w-10  '>
                        <img src={require('../../public/svgs/exportTeam.svg')} alt='' />
                      </div>
                      <div class=' w-80 tl'>
                        <div class='f8  fw5  orange lh-copy'>EXPORTS YOUR TEAM</div>
                        <div class='f8  fw5  black lh-copy'>
                          Exporting your saved teams to FanFight is now just a tap away
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
