import React, { useEffect, useRef, useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import CleverTap from 'clevertap-react';
import { Head } from 'react-static';
import backIconWhite from '../../public/svgs/backIconWhite.svg';
import { UPDATE_PROFILE } from '../../api/queries';

// import Facebook  from '../../public/svgs/facebook.svg'
export default function ProfileDesktop(props) {
  const [userName, setUserName] = useState(
    localStorage.getItem('userName') == null ? 'kunal' : localStorage.getItem('userName')
  );
  const token = localStorage.getItem('tokenData');

  const [updateProfile, response] = useMutation(UPDATE_PROFILE, {
    onCompleted: (responseProfile) => {
      if (responseProfile.updateProfileInfo.code == 200) {
        localStorage.setItem('userName', userName);
      }
    }
  });
  const updateName = () => {
    updateProfile({
      variables: {
        username: userName
      },
      context: {
        headers: {...headers, 
          accesstoken: 'BEARER ' + token
        
      }
    }
    });
  };

  const signOut = () => {
    props.setLoginStatus(false);

    localStorage.clear();
    CleverTap.initialize('Signout', {
      Source: 'Profile',
      Platform: localStorage ? localStorage.Platform : ''
    });
  };

  return (
    <div className='ma3 nt6  mw75-l center '>
      <div className='bg-white br2'>
        <div className='pt2 ph3 relative'>
          <div class='mw9 center ph3-ns'>
            <div className='cf ph2-ns'>
              <div className='fl w-100 w-100-ns pa2'>
                <div class='flex mt4 justify-center w-100'>
                  <img className='h4 pa2' src={require('../../public/svgs/loginteam.svg')} alt='' srcset='' />
                </div>

                <div class='flex flex-column  justify-center ph4  ma2 w-100 '>
                  <div className='measure center'>
                    <div className=' ml pa2'>
                      {' '}
                      <img src={require('../../public/moreSvgs/playerIcon.svg')} className='w-10 h1 ' alt='' />
                      <input
                        value={userName}
                        onBlur={updateName}
                        onChange={(e) => setUserName(e.target.value)}
                        id='name'
                        class='border-input input-reset pa2 mb2 ba b--black-05  w-80'
                        type='text'
                        aria-describedby='name-desc'
                      />
                      <img
                        src={require('../../public/svgs/pencil-edit-button.svg')}
                        className='w-10 h1 '
                        alt=''
                      />
                    </div>

                    <div className=' ml pa2'>
                      {' '}
                      <img src={require('../../public/svgs/phone.svg')} className='w-10 h1 ' alt='' />
                      <input
                        id='name'
                        value={localStorage.getItem('userNumber') == null ? '' : localStorage.getItem('userNumber')}
                        class='border-input input-reset   pa2 mb2 ba b--black-05   w-80'
                        type='text'
                        aria-describedby='name-desc'
                      />
                      <img src={require('../../public/svgs/tickgreen.svg')} className='w-10 h1 ' alt='' />
                    </div>
                  </div>
                </div>

                <div className='pa2  f8 fw6  tc   black center pa1'>Love CRICKET.COM? Share With Friends</div>
                <div className='pa2   tc   center pa1'>
                  <a target='_blank' href={'https://www.facebook.com/sharer/sharer.php?u=http://cricket.com'}>
                    <img src={require('../../public/svgs/fb.svg')} className='w-10 h2 ma2' alt='' />
                  </a>

                  <a target='_blank' href={'https://twitter.com/intent/tweet?url=http://cricket.com'}>
                    <img src={require('../../public/svgs/colorTwiter.svg')} className='w-10 h2 ma2' alt='' />
                  </a>

                  <a target='_blank' href='https://api.whatsapp.com/send?text=http://cricket.com'>
                    <img src={require('../../public/svgs/whatsapp.svg')} className='w-10 h2 ma2' alt='' />
                  </a>
                </div>

                <div className='pa2  f7 fw6  tc   red center pa1' onClick={signOut}>
                  SIGN OUT
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
