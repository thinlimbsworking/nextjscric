import React, { useState, useEffect } from 'react';
import { GET_HIGHLIGHTS } from '../../../api/queries';
import { useQuery } from '@apollo/react-hooks';
import Loading from '../../commom/datanotfound';
import DataNotFound from '../../commom/datanotfound';
const empty = '/svgs/Empty.svg';


// fliter.png
export default function HighlightsTab(props) {
  console.log("matchDatamatchDatamatchDatamatchDatamatchData",props)


  const { matchData } = props;

  const [Team, setTeam] = useState({});
  const [click ,setclick] = useState(false)
  const [activeFilter, setActiveFilter] = useState('All');
  const [ActiveArray, setActiveArray] = useState([]);
  const [innings, setInnings] = useState(
    matchData.matchType !== 'Test' && matchData.currentinningsNo > 2
      ? 2
      : matchData.currentinningsNo
  );
  const inningObject = [];
  const { loading, error, data } = useQuery(GET_HIGHLIGHTS, {
    variables: { matchID: props.matchID, innings: String(innings), type: '', page: 1 }
  });
  

  const filterTabs = ['All', 'Fours', 'Sixes', 'Wickets'];
  useEffect(() => {
    matchData.matchScore&& matchData.matchScore.map((y,j) => y.teamScore.map((x, i) => matchData.matchType !== 'Test' ? (i === 0 ? inningObject.push(x):""):inningObject.push(x)));
    const sortedInning = inningObject.sort((a, b) => {
      return a.inning - b.inning;
    });

    setActiveArray(sortedInning);
    setTeam({ teamID: matchData.currentInningteamID, inning: matchData.currentinningsNo });
    // setTeam({ teamID: ActiveArray[ActiveArray.length-1].teamID, inning: ActiveArray[ActiveArray.length-1].inning });

  }, []);
  if (error)
    return ( <DataNotFound />
    );
  if (loading) return <Loading />;
  else
    return (
      <div className='text-white '>
       
        { data.getHighlights.HighlightBall.length !== 0 ? (
          <div>
            {  matchData.matchType === 'Test' && (
              <div className='py-2 my-2  tc center relative flex items-center justify-center'>
                <div className=''>
                  {ActiveArray.map((team, i) => (
                    <span
                      key={i}
                      className={`black-20  cursor-pointer text-xs font-bold  p-2 px-3  text-center border   ${
                        (team.teamID === Team.teamID && team.inning == Team.inning)
                          ? 'bg-cdc white'
                          : 'bg-gray white'
                      }`}
                      onClick={() => {
                        setInnings(i + 1);
                        setTeam({ teamID: team.teamID, inning: team.inning });
                        setActiveFilter('All');
                      }}>
                      {team.inning === 1 || team.inning === 2
                        ? `${team.battingTeamShortName} 1`
                        : `${team.battingTeamShortName} 2`}
                    </span>
                  ))}
                </div>
              </div>
            )}
            {matchData.matchType !== 'Test' && (
              <div className='py-2 my-2  tc center relative flex items-center justify-center'>
                <div className='flex  items-center justify-center bg-gray w-6/12   rounded-full '>
                  {ActiveArray.map(
                    (team, i) =>
                      i <= 1 && (
                        <span
                          key={i}
                          className={`black-20 text-xs  text-center  w-6/12 rounded-full cursor-pointer p-2  ${
                            ((i === 1 && click ===  false) || (team.teamID === Team.teamID && team.inning == Team.inning))
                              ? 'border bg-basebg  border-green '
                              : ' white bg-gray'
                          }`}
                          onClick={() => {
                            setInnings(i + 1);
                            setclick(true),
                            setTeam({ teamID: team.teamID, inning: team.inning });
                            setActiveFilter('All');
                          }}>
                          {team.battingTeamShortName}{' '}
                        </span>
                      )
                  )}
                </div>
              </div>
            )}
            
            { Team && (
              <div>
                <div className='  flex justify-center items-center flex-col'>
                  <div className='flex m-1 justify-center items-center relative w-28 h-28'>
                    <img
                      className='w-28 h-28 absolute'
                      src={
                        activeFilter === 'Wickets' ? '/svgs/groundImageWicket.png' : '/svgs/ground.png'
                      }
                      alt=''
                    />
                    {console.log("data",data.getHighlights)}
                    <div className='w-28 h-28  absolute  overflow-hidden rounded-full '>
                      {data.getHighlights.HighlightBall.filter((x) =>
                        activeFilter === 'All'
                          ? x
                          : activeFilter === 'Fours'
                          ? x.runs === '4'
                          : activeFilter === 'Sixes'
                          ? x.runs === '6'
                          : null
                      ).map((x, i) => (
                        <div
                          key={i}
                          className={`border-[.2px]   overflow-hidden absolute ${
                            x.runs === '0' ? 'hidden' : x.runs === '6' ? ' border-blue' : ' border-white'
                          } `}
                          style={{
                            bottom: '55%',
                            left: '50%',
                            right: '0%',
                            transform: `rotate(${x.zad.split(',')[1]}deg)`,
                            transformOrigin: '0 0'
                          }}></div>
                      ))}
                    </div>
                  </div>
               
                  {activeFilter === 'All' ? (
                    <div className='f7 text-gray-2 tc py-2 font-medium text-xs'>
                      <span>Total </span>
                      <span>
                        <span>4's: </span>
                        <span className=' mr-2 font-bold text-white text-sm '>{data.getHighlights.totalCount.totalFours}</span>
                      </span>
                      <span>
                        <span>6's: </span>
                        <span className=' mr-2 font-bold text-white text-sm '>{data.getHighlights.totalCount.totalSix}</span>
                      </span>
                      <span>
                        <span>Wickets: </span>
                        <span className=' mr-2  font-bold text-white text-sm'>{data.getHighlights.totalCount.totalWickets}</span>
                      </span>
                    </div>
                  ) : (
                    ''
                  )}
                  {activeFilter === 'Fours' ? (
                    <div className='f7 text-gray-2  font-medium tc py-2'>
                      <span>
                        <span>Fours: </span>
                        <span className=' mr2 '>{data.getHighlights.totalCount.totalFours}</span>
                      </span>
                    </div>
                  ) : (
                    ''
                  )}
                  {activeFilter === 'Sixes' ? (
                    <div className='f7 text-gray-2  font-medium tc py-2'>
                      <span>
                        <span>Sixes: </span>
                        <span className=' mr2 '>{data.getHighlights.totalCount.totalSix}</span>
                      </span>
                    </div>
                  ) : (
                    ''
                  )}
                  {activeFilter === 'Wickets' ? (
                    <div className='f7 tc py-2 text-gray-2  font-medium'>
                      <span>
                        <span>Wickets: </span>
                        <span className=' mr2 '>{data.getHighlights.totalCount.totalWickets}</span>
                      </span>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              
              
                <div className='fixed w-full bottom-0 bg-gray-2 p-2 flex justify-center  items-center white f8 font-medium pa2'>
              
                  {filterTabs.map((value, key) => (
                    <div
                      key={key}
                      className={` cursor-pointer w-16 mx-2 py-2 flex justify-center rounded ${
                        activeFilter === value ? 'bg-basebg border-2 border-green' : 'bg-gray-4 '
                      }`}
                      onClick={() => setActiveFilter(value)}>
                      {value=="Fours"?'4s':''}
                      {value=="Sixes"?'6s':''}

                      {value=="Wickets"?'Wkts':''}

                      {value=="All"?'All':''}

                    </div>
                  ))}
                  <div onClick={()=>props.setShowHigh(!props.showHigh)} className='w-5 h-5 flex items-center justify-center border rounded-full'>X</div>
                </div>

                <div className='overflow-y-scroll '>
                  {data.getHighlights.HighlightBall.filter((x) =>
                    activeFilter === 'All'
                      ? x
                      : activeFilter === 'Fours'
                      ? x.runs === '4'
                      : activeFilter === 'Sixes'
                      ? x.runs === '6'
                      : activeFilter === 'Wickets'
                      ? x.wicket === true
                      : null
                  ).map((x, i) => (
                    <div key={i}>
                    <div  className=' flex   p-2   justify-center items-center '>

                    <div className=' flex-col flex justify-center items-center bg-gray   py-2  w-2/12 mx-1 overflow-x-scroll overflow-y-hidden hidescroll  '>
                 
                    <div key={i} className='text-center w-full flex items-center justify-center border-b border-black pb-2'>
                      {x.runs.includes('W') ? (
                        <div className='flex justify-center items-center  black w-8 h-8 rounded-full bg-red white text-xs font-bold'>
                          W
                        </div>
                      ) : (
                        <>
                    { activeFilter!=="Wickets"  ? <div
                          className={` flex justify-center   items-center text-xs font-bold black w-8 h-8 rounded-full ${
                            parseInt(x.runs) === 4
                              ? 'bg-green white b--green'
                              : parseInt(x.runs) === 6
                              ? 'bg-green white b--green text-xs font-bold'
                              : 'bg-red text-white text-xs font-bold '
                              } `}>
                        
                    {x.runs=='0'?'W':x.runs}
                        </div>:<div
                          className={` flex justify-center   items-center bg-red-4  text-xs font-bold  black w-8 h-8 rounded-full 
                           `}>
                       {x.runs=='0'?'W':x.runs}
                        </div>}
                        </>

                      )}
                     
                    </div>
                    {true&&  <div
                        className={`flex w-full   justify-center items-center text-xs font-bold fblack  text-center `}
                          
                      >
                      
                       
                       <p className='text-center w-full white f7  fw5 py-1'>{x.over} </p>
                      </div> }
                  
                </div>



                     
                      <div className='f8 ml1 text-xs w-10/12'> {x.commentary} </div>
                      
                    </div>

                    {i==data.getHighlights.HighlightBall.filter((x) =>
                    activeFilter === 'All'
                      ? x
                      : activeFilter === 'Fours'
                      ? x.runs === '4'
                      : activeFilter === 'Sixes'
                      ? x.runs === '6'
                      : activeFilter === 'Wickets'
                      ? x.wicket === true
                      : null
                  ).length-1&& <div className='h-20 '> </div>}
                    </div>
                  ))}
                </div>

                {/* <div
                          className={` flex justify-center items-center text-xs font-bold font-bold black w-8 h-8 rounded-full ${
                            parseInt(ball.runs) === 4
                              ? 'bg-green white b--green'
                              : parseInt(ball.runs) === 6
                              ? 'bg-green white b--green text-xs font-bold'
                              : 'bg-gray-2 text-black text-xs font-bold '
                          } `}>
                          {getRuns(ball.runs)}
                        </div> */}

              </div>
            )}
          </div>
        ) : (
          <DataNotFound />
        )}
      </div>
    );
}
