import React, { useState } from "react";
// import Ballbyball from "./BallbyballTab";
// import Overbyover from "./OverbyoverTab";
import Commentary from "./commentry";
// import OverbyOverV2 from "./OverbyOverV2";
// import ProbablePlayingXITab from "../../components/MatchDetails/ProbablePlayingXITab";
// import MatchLiveTab from "../../components/MatchDetails/MatchLiveTab";
export default function CommentryTab(props) {
  const [tab, setTab] = useState("BALL BY BALL");

  // const commentryTab = ["BALL BY BALL", "OVER BY OVER"];
  const [isRefresh, setisRefresh] = useState(false);

  return (
    <div className="">
      <div className="overflow-scroll hidescroll">


        <div>
          {/* {props.matchData.matchStatus === 'live' &&  props.matchData.isLiveCriclyticsAvailable ? <MatchLiveTab matchData={props.matchData} path="live"/>:null}

        {props.matchData.matchStatus === 'upcoming' &&  props.matchData.probable11Status?
          <ProbablePlayingXITab matchData={props.matchData} path="probableplaying11"/>:null} */}
          <Commentary
            matchType="completed"
            matchData={props.matchData}
            matchID={props.matchID}
            getId={(val) => setisRefresh(val)}
          />

        </div>
      </div>
    </div>
  );
}
