import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { MINI_SCORE_CARD } from "../../../api/queries";
// import Ballbyball from "./BallbyballTab";
// import Overbyover from "./overbyoverTab";
// import Quickbytes from "../Home/quickBytes";
// import Commentary from "./commentary";
// import OverbyOverV2 from "./overbyOverV2";
export default function ScorecardTab(props) {
  const [tab, setTab] = useState("BALL BY BALL");



  const [isRefresh, setisRefresh] = useState(false);

  const { loading, error, data } = useQuery(MINI_SCORE_CARD, {
    variables: { matchID: props.matchData.matchID },
  });


  if (error)
    return (
     <></>
    );
  if (loading || !data) return <p>Loading...</p>;
  else
    return (
      <>
        
          <div className=" bg-gray-4 text-white">
            <div className="">
              <table className=" w-100">
                <thead
                  className="bg-gray "
                >
                  <tr className="">
                    <th className="w-50 pv3   text-xs font-semibold tl pl-3">BATTER</th>
                    <th className=" w-10  text-xs font-semibold tr ph2">R</th>
                    <th className=" w-10  text-xs font-semibold tr ph2">B</th>
                    <th className=" w-10  text-xs font-semibold tr ph2">4s</th>
                    <th className=" w-10  text-xs font-semibold tr ph2">6s</th>
                    <th className=" w-10  text-xs font-semibold tr ph2 ">SR</th>
                  </tr>
                </thead>
                <tbody>
                  {data.miniScoreCard.batting.map((batsmen, i) => (
                    <tr key={i}>
                      <td className="b--black-10 tl fw5 f7 pv3 pl-2">
                        
                        {batsmen.playerName}
                        {batsmen.playerOnStrike === true ? "*" : ""}
                      </td>
                      <td className=" text-xs font-medium ph2 tr">
                        
                        {batsmen.runs}
                      </td>
                      <td className=" text-xs font-medium ph2 tr">
                        
                        {batsmen.playerMatchBalls}
                      </td>
                      <td className=" text-xs font-medium ph2 tr">
                        
                        {batsmen.fours}
                      </td>
                      <td className=" text-xs font-medium ph2 tr">
                        
                        {batsmen.sixes}
                      </td>
                      <td className=" text-xs font-medium ph2 tr">
                        
                        {batsmen.playerMatchStrikeRate}
                      </td>
                    </tr>
                  ))}
                </tbody>



              </table>

              {data.miniScoreCard.bowling &&
                data.miniScoreCard.bowling.length > 0 && (
                  <table className="" >
                    <thead
                      className="bg-gray"
                    >
                      <tr className="">
                        <th className="w-50 pv3 text-xs font-semibold tl pl-3">
                          BOWLER
                        </th>
                        <th className="w-10 text-xs font-semibold tr ph2">0</th>
                        <th className="w-10 text-xs font-semibold tr ph2">M</th>
                        <th className="w-10 text-xs font-semibold tr ph2">R</th>
                        <th className="w-10 text-xs font-semibold tr ph2">W</th>
                        <th className="w-10 text-xs font-semibold tr ph2">ER</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.miniScoreCard.bowling.map((bowler, i) => (
                        <tr key={i}>
                          <td className="  tl fw5 f7 pv3 pl-3 text-xs font-medium">
                            
                            {bowler.playerName}
                          </td>
                          <td className="  text-xs font-medium  ph2 tr">
                            
                            {bowler.overs}
                          </td>
                          <td className="  text-xs font-medium ph2 tr">
                            
                            {bowler.maiden}
                          </td>
                          <td className="  text-xs font-medium ph2 tr">
                            
                            {bowler.RunsConceeded}
                          </td>
                          <td className="  text-xs font-medium ph2 tr">
                            
                            {bowler.wickets}
                          </td>
                          <td className="  text-xs font-medium ph2 tr">
                            
                            {bowler.economy}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                )}
              <table className=" w-100 ph2" style={{ borderSpacing: 0 }}>
                <thead
                  className="bg-gray "
                >
                  <tr className=" flex  justify-between  items-center px-2">
                    <th className={`pv3  tl w-20  w-25-l fw5 f7`}>
                      P'SHIP:
                      <span className=" fw6">
                       {' '} {data.miniScoreCard.partnership}
                      </span>
                    </th>
                    {props.matchData.matchType==='Test' && data.miniScoreCard.oversRemaining!="0.0" &&<th className="pv3 w-30 tl w-25-l fw5 f7">
                      OVS REM:
                      <span className=" fw6">
                        {data.miniScoreCard.oversRemaining}
                      </span>
                    </th>}
                    <th className={`fw5 pv3  tr w-25 w-25-l f7`}>
                      CRR :
                      <span className=" fw6">
                       {' '} {data.miniScoreCard.runRate}
                      </span>
                    </th>
                    {data.miniScoreCard.rRunRate !== "" && (
                      <th className={` w-25 fw5  w-25-l tr f7`}>
                        Req RR:
                        <span className=" fw6">
                          {data.miniScoreCard.rRunRate} 
                        </span>
                      </th>
                    )}
                  </tr>

                  
                  {(data.miniScoreCard.reviewDetails[0].review!=="" || data.miniScoreCard.reviewDetails[1].review!=="") &&<tr className=" flex bb bt b--black-10 pa2  items-center ">
                  <th className="fw5  tr  flex b--black-10 f7">
                        <div className="text-gray-2">REVIEWS REM: &nbsp; </div>
                        <div className=" flex  ">
                          {data.miniScoreCard.reviewDetails.map((review,i)=><div className="">
                               {review.teamName.toUpperCase()} {review.review || '0'}  {i===data.miniScoreCard.reviewDetails.length-1?'': <span className="mr1">{' , '}</span>} 
                          </div>)} 
                        </div>
                      </th>
                  </tr>}
                </thead>
              </table>
            </div>
            {/* <div className="bg-white mt3 pt0 br2">
              <Quickbytes matchID={props.matchID} />
            </div> */}


          </div>
        
      </>
    );
}
