import React from 'react'
import Scores from '../../commom/score'
import Liveballs from '../../liveballs'
const AbandonedIcon = '/svgs/images/Abandoned_Icon.png'
const ManOfMatch = '/svgs/Man_of_the_match.svg'
const backIconWhite = '/svgs/backIconWhite.svg'

export default function MiniScoreCard({ data, browser, ...props }) {
  return data.miniScoreCard &&
    data.miniScoreCard.data.length &&
    data.miniScoreCard.data[0].matchID ? (
    <>
      {/* <div className="h43 pt3 ph3 mb3 truncate" style={{ background: "linear-gradient(to left, rgb(186, 82, 34), rgb(167, 15, 20))" }} >
         <img className="mr3" onClick={() => window.history.back()} src={backIconWhite} alt="back icon" />
         <span className="white fw6 f4 "> {data.miniScoreCard.data[0].homeTeamName} VS {data.miniScoreCard.data[0].awayTeamName}, {data.miniScoreCard.data[0].matchNumber}</span>
      </div> */}
      {!props.a23 && (
        <div className="py-1  flex items-center bg-gray-8 text-white md:pt-5">
          <img
            className="p-3 md:hidden "
            onClick={() => window.history.back()}
            src={backIconWhite}
            alt="back icon"
          />
          <h1 className="text-xl font-semibold  uppercase  ">
            {' '}
            {data.miniScoreCard.data[0].homeTeamName} VS{' '}
            {data.miniScoreCard.data[0].awayTeamName},{' '}
            {data.miniScoreCard.data[0].matchNumber}
          </h1>
        </div>
      )}

      <div className="w-full">
        <div className="bg-gray-4  shadow-1">
          <div className="p-2 relative bg-gray-8">
            {data.miniScoreCard.data[0].matchStatus === 'live' && (
              <div className="inline-flex px-2 py-0.5 items-center rounded-full  border border-white/80  absolute right-2 top-3 text-white">
                <span className="bg-green h04 w04 rounded-full mr1" />
                <h2 className="font-medium text-[10px]">LIVE</h2>
              </div>
            )}

            <Scores
              isDetails
              featured={true}
              showCriclytics={true}
              data={data.miniScoreCard.data[0]}
              browser={browser}
              a23={props.a23}
            />
          </div>
        </div>
      </div>
      <Liveballs
        hideTitle={true}
        matchID={data && data.miniScoreCard.data[0].matchID}
        matchData={data && data.miniScoreCard.data[0]}
        currentinningsNo={data && data.miniScoreCard.data[0].currentinningsNo}
      />
    </>
  ) : (
    <></>
  )
}
