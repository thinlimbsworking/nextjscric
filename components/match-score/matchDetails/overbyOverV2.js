import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { GET_OVER_BY_OVER } from '../../../api/queries';
function OverbyOverV2({ matchID, matchData, refresh }) {
  const [newInnings, setNewInnings] = useState(parseInt(matchData.currentinningsNo));
  const { loading, error, data, fetchMore, refetch } = useQuery(GET_OVER_BY_OVER, {
    variables: {
      matchId: matchID,
      innings: parseInt(matchData.currentinningsNo)
    },
    onCompleted: (data) => {},
    context: { clientName: 'commentary' }
  });
  useEffect(() => {
    if (refresh) {
      refetch();
    }
  }, [refresh]);
  const innSummary = (ball) => {
    return (
      <div className='ba b--navy'>
        <div className='bg-navy white flex pa3 justify-between items-center'>
          <span className='f6 fw5'>{ball.score.battingTeamName.toUpperCase()}</span>
          <div className='flex items-center'>
            <span className='f6 fw5 ph1'>
              {ball.score.runsScored}/{ball.score.wickets}
            </span>
            <span className='f6 fw5'>({ball.score.overs})</span>
          </div>
        </div>
        <div className='flex justify-between items-center'>
          <div className=' w-100 '>
            {ball.battingList.map((player, i) => (
              <div key={i} className='flex justify-between pa2 pv3 bb w-100'>
                <span className='f7 fw5 truncate w-80 '>{player.playerName}</span>
                <span className='f7 fw5 truncate'>{player.playerMatchRuns}</span>
              </div>
            ))}
          </div>
          <div className='v-divider flex w-10 ' />
          <div className='w-100 '>
            {ball.bowlingList.map((player, i) => (
              <div key={i} className='flex justify-between pa2 pv3 bb w-100'>
                <span className='f7 fw5 truncate w-80 '>{player.playerName}</span>
                <span className='f7 fw5 truncate'>
                  {player.playerWicketsTaken}/{player.playerRunsConceeded}
                </span>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  };
  const overArr = (ovr) => {
    return (
      <div>
        <div className=' flex bg-light-title items-center  pa2'>
          <div className='pa1 br2' style={{ background: 'rgb(114, 118, 130)', minWidth: '50px' }}>
            <p className='tc ma0 fw7 white f4 pa2'>{~~ovr[0].split(':')[0].split('.')[0] + 1}</p>
            <div className='divider' />
            <p className='tc ma0 white f8 fw5 pa1'>over </p>
          </div>
          <div className='flex ml2 overflow-x-scroll hidescroll ' style={{ overflowY: 'hidden' }}>
            {ovr.map((ball, i) => {
              const [bl, run] = ball.split(':');
              return (
                <div key={i}>
                  <div key={i} className='tc ml2 '>
                    <div
                      className={`ba flex justify-center items-center f7 fw4 black w2 h2 br-100 ${
                        run.includes('4') || run.includes('5')
                          ? 'bg-green white'
                          : run.includes('6')
                          ? 'bg-blue white'
                          : run.includes('W') && !run.includes('wd')
                          ? 'bg-cdc white'
                          : ''
                      }`}>
                      {run === '0W' ? 'W' : run}
                    </div>
                    <p className='f8 black-30 pt1 ma0 fw6'>{bl}</p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  };
  const handleScroll = (evt) => {
    if (evt.target.scrollTop + evt.target.clientHeight >= 0.8 * evt.target.scrollHeight) {
      if (newInnings !== 1) {
        setNewInnings((prev) => parseInt(prev - 1));
        fetchMore({
          variables: { innings: newInnings - 1 },
          updateQuery: (prev, { fetchMoreResult }) => {
            if (!fetchMoreResult) return prev;
            let parsedPre = JSON.parse(prev.getOverbyOver.overbyover);
            let newData = JSON.parse(fetchMoreResult.getOverbyOver.overbyover);
            return Object.assign({}, prev, {
              getOverbyOver: {
                overbyover: JSON.stringify([...parsedPre, ...newData])
              }
            });
          }
        });
      }
    }
  };
  return (
    <div className=' max-vh-80' style={{ overflowY: 'scroll' }} onScroll={(evt) => handleScroll(evt)}>
      {data && data.getOverbyOver && data.getOverbyOver.overbyover
        ? JSON.parse(data.getOverbyOver.overbyover).map((ov) =>
            Array.isArray(JSON.parse(ov)) ? (
              <div>{overArr(JSON.parse(ov))}</div>
            ) : (
              <div>{innSummary(JSON.parse(ov))}</div>
            )
          )
        : ''}
    </div>
  );
}
export default OverbyOverV2;
