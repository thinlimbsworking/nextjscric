import React, { useState } from 'react';
import { OVER_BY_OVER } from '../../../api/queries';
import { useQuery } from '@apollo/react-hooks';
const ground = '/svgs/backIconWhite.svg';

export default function Overbyover(props) {
  const {
    matchID,
    matchData: { currentDay: day, currentSession: session, currentinningsNo: innings, matchType }
  } = props;
  
  const [newDay, setNewDay] = useState(() => (session === 1 ? day - 1 : day));
  const [newSession, setNewSession] = useState(() => (session === 1 ? 3 : session));
  const [newInnings, setNewInnings] = useState(innings);

  const { loading, error, data, fetchMore } = useQuery(OVER_BY_OVER, {
    variables: { matchID, day: day, session: session, matchType, innings: innings }
  });
  
  const handlePaginationTest = (fetchMore) => {
    fetchMore({
      variables: { matchID, day: newDay, session: newSession, innings: newInnings, matchType },

      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult;
        }
        if (fetchMoreResult.getBallByBall[0].inningsSummary.length === 0) {
          let resultedBallByBall = Object.assign({}, previousResult, {
            overByOver: [...previousResult.overByOver[0].oversdata, ...fetchMoreResult.overByOver[0].oversdata]
          });

          return resultedBallByBall;
        } else {
          setNewInnings((prev) => (prev === '1' ? '1' : (prev - 1).toString()));
          let resultedBallByBall = Object.assign({}, previousResult, {
            overByOver: [...previousResult.overByOver, ...fetchMoreResult.overByOver]
          });

          return resultedBallByBall;
        }
      }
    });
  };

  const handlePaginationODI = (fetchMore) => {
    if (data.overByOver.length === 1) {
      fetchMore({
        variables: { matchID, matchType, day: 1, session: 1, innings: '1' },

        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!fetchMoreResult) {
            return previousResult;
          }
          let resultedOverByOver = Object.assign({}, previousResult, {
            overByOver: [...previousResult.overByOver, ...fetchMoreResult.overByOver]
          });

          return resultedOverByOver;
        }
      });
    }
  };

  const handleScroll = (evt) => {
    if (evt.target.scrollTop + evt.target.clientHeight >= 0.8 * evt.target.scrollHeight) {
      if (matchType === 'Test') {
        if (newSession === 1) {
          setNewSession(3);
          setNewDay((prev) => prev - 1);

          handlePaginationTest(fetchMore);
        } else {
          setNewSession((prev) => prev - 1);
          handlePaginationTest(fetchMore);
        }
      } else {
        if (newInnings !== '1') {
          setNewInnings((prev) => String(prev - 1));
          handlePaginationODI(fetchMore);
        }
      }
    }
  };

  if (error)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  if (loading || !data) return <div>Loading...</div>;
  else
    return (
      <div className=' max-vh-80' style={{ overflowY: 'scroll' }} onScroll={(evt) => handleScroll(evt)}>
     
        {data.overByOver.map((data, i) => (
          <React.Fragment key={i}>
            {data.inningsSummary &&
              data.inningsSummary.map((inning, i) => (
                <div key={i} className='ba ma2 b--navy'>
                  <div className='bg-navy white flex pa3 justify-between items-center'>
                    <span className='f6 fw5'>{inning.score.battingTeamName.toUpperCase()}</span>
                    <div className='flex items-center'>
                      <span className='f6 fw5 ph1'>
                        {inning.score.runsScored}/{inning.score.wickets}
                      </span>
                      <span className='f6 fw5'>({inning.score.overs})</span>
                    </div>
                  </div>

                  <div className='flex justify-between  items-center'>
                    <div className=' w-100 '>
                      {inning.battingList.map((player, i) => (
                        <div key={i} className='flex justify-between pa2 pv3 bb w-100'>
                          <span className='f7 fw5 truncate w-80 '>{player.playerName}</span>
                          <span className='f7 fw5 truncate'>{player.playerMatchRuns}</span>
                        </div>
                      ))}
                    </div>

                    <div className='v-divider flex w-10 ' />

                    <div className='w-100 '>
                      {inning.bowlingList.map((player, i) => (
                        <div key={i} className='flex justify-between pa2 pv3 bb w-100'>
                          <span className='f7 fw5 truncate w-80 '>{player.playerName}</span>
                          <span className='f7 fw5 truncate'>
                            {player.playerWicketsTaken}/{player.playerRunsConceeded}
                          </span>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              ))}

            {data.oversdata.map((over, i) => (
              <React.Fragment key={i}>
                <div className='divider' />

                <div className=' flex bg-light-title items-center  pa2'>
                  <div className='pa1 br2' style={{ background: 'rgb(114, 118, 130)', minWidth: '50px' }}>
                    <p className='tc ma0 fw7 white f4 pa2'>{over.overNumber}</p>
                    <div className='divider' />
                    <p className='tc ma0 white f8 fw5 pa1'>over </p>
                  </div>

                  <div className='flex ml2 overflow-x-scroll hidescroll ' style={{ overflowY: 'hidden' }}>
                    {over.balls.map((ball, i) => (
                      <div key={i}>
                        <div key={i} className='tc ml2 '>
                          {ball.wicket ? (
                            <div className='flex justify-center items-center ba f6 fw7 black w2 h2 br-100 bg-cdc white b--cdc'>
                              W
                            </div>
                          ) : (
                            <div
                              className={`ba flex justify-center items-center f7 fw4 black w2 h2 br-100 ${
                                ball.runs === '4'
                                  ? 'bg-green white b--green'
                                  : ball.runs === '6'
                                  ? 'bg-blue white b--blue'
                                  : 'bg-white '
                              } `}>
                              {ball.type === 'no ball'
                                ? 'nb'
                                : ball.type === 'leg bye'
                                ? 'lb'
                                : ball.type === 'bye'
                                ? 'b'
                                : ball.type === 'wide'
                                ? 'wd'
                                : ''}
                              {ball.runs}
                            </div>
                          )}
                          <p className='f8 black-30 pt1 ma0 fw6'>{ball.over}</p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </React.Fragment>
            ))}
          </React.Fragment>
        ))}

        <div className='divider' />
      </div>
    );
}
