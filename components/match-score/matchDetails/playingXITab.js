import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { PROBABLE_PLAYING_XI } from '../../../api/queries';
// import FanFightCard from '../FanFightCard';
import Loading from '../../commom/loading';
// import { navigate } from '@reach/router';
import Link from 'next/link';
import { getPlayerUrl } from '../../../api/services';
import CleverTap from 'clevertap-react';
export default function PlayingXITab(props) {
  const { loading, error, data } = useQuery(PROBABLE_PLAYING_XI, { variables: { matchID: props.matchData.matchID } });

  const clevertab =(id)=>{
    CleverTap.initialize('Players', {
      Source: 'CommentaryTab',
      PlayerID:id,
      Platform: localStorage.Platform
    });

  }
  if (error)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
       
      </div>
    );
  if (loading) return <Loading />;
  else
    return (
      <React.Fragment>
        <div className='text-white pb-3'>
          <div className='text-sm font-semibold f6-ns  p-2 bg-gray'>
            PLAYING XI
          </div>
         
      
          {data.probablePlaying11.homeTeamPP11 && (
            <div className='p-1 justify-center'>
              <span className='f7 fw6'> {data.probablePlaying11.homeTeamShortName.toUpperCase()} :</span>
              {data.probablePlaying11.homeTeamPP11.map((player, i) => (
                (<Link
                  key={i}
                  {...getPlayerUrl({ ...player })}
                  onClick={()=>clevertab(player.playerID)}
                  passHref
                  className='text-xs pl-1 cursor-pointer'>

                  {`${player.playerName}${player.captain ? ' (C)' : ''}${player.keeper ? ' (Wk)' : ''}${
                    i === 10 ? '.' : ','
                  }`}

                </Link>)
              ))}
            </div>
          )}

          {data.probablePlaying11.awayTeamPP11 && (
            <div className='pv2 justify-center'>
              <span className='f7 fw6'> {data.probablePlaying11.awayTeamShortName.toUpperCase()} :</span>
              {data.probablePlaying11.awayTeamPP11.map((player, i) => (
                (<Link
                  key={i}
                  {...getPlayerUrl({ ...player })}
                  onClick={()=>clevertab(player.playerID)}
                  passHref
                  className='text-xs pl-1 cursor-pointer'>

                  {`${player.playerName}${player.captain ? ' (C)' : ''}${player.keeper ? ' (Wk)' : ''}${
                    i === 10 ? '.' : ','
                  }`}

                </Link>)
              ))}
            </div>
          )}
        </div>

        {/* <FanFightCard /> */}
      </React.Fragment>
    );
}
