import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { PROBABLE_PLAYING_XI } from '../../../api/queries';
import FanFightCard from './fanfightcard';
import Loading from '../../commom/datanotfound';
import Link from 'next/link';
import { getPlayerUrl } from '../../../api/services';

export default function ProbablePlayingXITab(props) {
  const { loading, error, data } = useQuery(PROBABLE_PLAYING_XI, {
    variables: { matchID: props.matchData.matchID }
  });

  if (error) return <div className=''></div>;
  if (loading) return <Loading />;
  else
    return (
      <React.Fragment>
        {
          <div className='text-white pb-3'>
            <div className='p-1 text-sm font-semibold bg-gray' >
              PROBABLE XI
            </div>
            {data.probablePlaying11.homeTeamPP11 && (
              <div className=' justify-center p-1'>
                <span className='text-sm font-semibold'> {data.probablePlaying11.homeTeamShortName.toUpperCase()} :</span>

                {data.probablePlaying11.homeTeamPP11.map((player, i) =>
                  player.playerName ? (
                    (<Link key={i} {...getPlayerUrl({ ...player })} passHref>

                      <span className='text-xs pl-1  cursor-pointer'>
                        {`${player.playerName}${player.captain ? ' (C)' : ''}${player.keeper ? ' (Wk)' : ''}${
                          i === 10 ? '.' : ','
                        }`}
                      </span>

                    </Link>)
                  ) : (
                    ''
                  )
                )}
              </div>
            )}

            {data.probablePlaying11.awayTeamPP11 && (
              <div className='p-1 justify-center'>
                <span className='f7 fw6'> {data.probablePlaying11.awayTeamShortName.toUpperCase()} :</span>
                {data.probablePlaying11.awayTeamPP11.map((player, i) =>
                  player.playerName ? (
                    (<Link
                      key={i}
                      {...getPlayerUrl({ ...player })}
                      passHref
                      className='text-xs pl-1 cursor-pointer'>

                      {`${player.playerName}${player.captain ? ' (C)' : ''}${player.keeper ? ' (Wk)' : ''}${
                        i === 10 ? '.' : ','
                      }`}

                    </Link>)
                  ) : (
                    ''
                  )
                )}
              </div>
            )}
          </div>
        }

        {/* <FanFightCard /> */}
      </React.Fragment>
    );
}
//data.probablePlaying11.playing11Status === true ?
