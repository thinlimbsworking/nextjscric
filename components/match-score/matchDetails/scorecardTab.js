import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import * as d3 from 'd3';

import { PLAYER_VIEW } from '../../../constant/Links';
import CleverTap from 'clevertap-react';
import Runwheel from './runwheel'
import PieClass from '../../piechart'
const upArrowWhite = '/svgs/upArrowWhite.svg';
const ManOfMatch = '/svgs/Man_of_the_match.svg';
const upwarrow = '/pngsV2/dwarrow.png'
export default function ScorecardTab({ data, ...props }) {
  console.log("newDatanewDatanewDatanewData222", data)
  let router = useRouter();
  let navigate = router.push;
  const [card, setCard] = useState(3);
  const [showWheel, setShowWheel] = useState(98);

  const [exp, setExp] = useState(false);

  var z1SquarLeg = 0;
  var z2FineLeg = 0;
  var z3ThirdMan = 0;
  var z4Point = 0;
  var z5Cover = 0;
  var z6MidOff = 0;
  var z7MidOn = 0;
  var z8MidWicket = 0

  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20
    }));
  const [dataa, setData] = useState(generateData(0));
  const [teamShow, setTeamShow] = useState(1);
  

  

  const changeData = () => {
    setData(generateData());
  };

  useEffect(() => {
    setData(generateData());
    setExp(window.screen.width > 540 ? true : false)
  }, [!dataa]);



  useEffect(() => {
    setCard(data.getScoreCard.fullScoreCard && data.getScoreCard.fullScoreCard.length - 1);
    setTeamShow(data && data.getScoreCard.fullScoreCard && data.getScoreCard.fullScoreCard.length - 1)
  }, []);

  const handlePlayerNavigation = (playerName, playerID) => {
    CleverTap.initialize('Players', {
      Source: 'ScorecardTab',
      playerID: playerID,
      Platform: localStorage.Platform
    });
  };
  const getPlayerUrl = (playerName, playerID) => {
    let playerSlug = `${playerName ? playerName.split(' ').join('-').toLowerCase() : 'N/A'}`;
    let playerId = playerID;
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href
    };
  };

  if (!data)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  else
    return (
      <div className='w-full'>
        {/* {false && props.matchData.matchResult !== 'Match Abandoned' && props.matchData.matchStatus === 'completed' ? (
          <div className='flex  justify-between items-center'>
            <div className=' '>
              <Link {...getPlayerUrl(props.matchData.playerOfTheMatch, props.matchData.playerID)} passHref>
                <a
                  className='flex  pv2 ph2 items-center'
                  onClick={() => {
                    handlePlayerNavigation(props.matchData.playerOfTheMatch, props.matchData.playerID);
                  }}>
                  <div className='w-10 h-10 cursor-pointer bg-gray br-100 overflow-hidden  object-cover object-top'>
                    <img
                      src={`${` https://images.cricket.com/players/${props.matchData.playerID}_headshot_safari.png`}`}
                      onError={(e) => {
                        e.target.src = ManOfMatch;
                      }}
                      alt=''
                    />
                  </div>
                  <div className='flex-column  justify-between pl2 '>
                    <div className='text-xs font-semibold'>
                      {props.matchData.playerOfTheMatch ? props.matchData.playerOfTheMatch : 'TBA'}
                    </div>
                    <div className='text-xs text-gray-2'> Man of the Match </div>
                  </div>
                </a>
              </Link>
            </div>
          </div>
        ) : null} */}

        <div className='flex  flex-col w-full'>
          {console.log("data.getScoreCard.fullScoreCard", data.getScoreCard)}
          {data && data.getScoreCard && data.getScoreCard.fullScoreCard && data.getScoreCard.fullScoreCard.map((item, index) => {
            return (<div className={`flex flex-col w-full items-center justify-between border-b py-2 border-gray   `}>
<div className='flex  w-full '>

<div className='flex w-full  '>
              <div className={`flex p-2 w-full `}>
                <img className='h-6 w-10 rounded' src={`https://images.cricket.com/teams/${item.battingTeamID}_flag_safari.png`}  
                
                
                alt="" srcset="" />
                <span className='ml-2 text-white'>{item.battingTeamShortName}</span>

              </div>
              <div className='flex items-center justify-center'>


                <span className='text-white'> {item.total.runsScored}
                  {item.total.wickets && item.total.wickets !== '10' ? `/${item.total.wickets}` : null}
                  {item.total.overs ? `(${item.total.overs})` : ''}</span>
                <img  onClick={()=>setTeamShow(index==teamShow?4:index)} className="cursor-pointer mx-2 rounded-xl bg-gray h-10 w-10 p-1" src={teamShow==index?`/pngsV2/minus.png`:'/pngsV2/plus.png'} alt="" /></div>
                </div>








        
               <div className='flex flex-col  items-start justify-center  m-2  '>

          </div>
        </div>
    {index===teamShow && <>
{/* batter */}

        { <>

<div className='mt-4 mx-4  w-full bg-gray-4 rounded-t-lg'>
  <div className='flex items-center  justify-between p-2 py-3'>
    <div className='text-sm font-semibold w-5/12 text-blue font-bold'>BATTER</div>
    <div className='flex items-center justify-between w-6/12 text-sm font-semibold   '>
      <div className='w-2/12 text-center text-xs text-white '>R</div>
      <div className='w-2/12 text-center text-xs text-white '>B</div>
      <div className='w-2/12 text-center text-xs text-white '>4s</div>
      <div className='w-1/12 text-center text-xs text-white '>6s</div>

      <div className='w-3/12 text-center text-xs text-white '>S/R</div>



    </div>
    <div className='flex items-center justify-between w-1/12 text-sm font-semibold '></div>
  </div>
</div>
{data.getScoreCard.fullScoreCard[teamShow].batting.map((item, index) => {
  return (<div className=' w-full bg-gray '>
    <div className='flex  flex-col items-center  justify-between p-2 py-3'>
      <div className='flex w-full '>
        <div className='text-sm font-semibold w-5/12 text-white  '>{item.playerName}</div>
        <div className='flex items-center justify-between w-6/12 text-sm font-semibold '>
          <div className='w-2/12 text-center text-xs font-semibold text-white'>{item.playerMatchRuns}</div>
          <div className='w-2/12 text-center text-xs font-semibold text-white'>{item.playerMatchBalls}</div>
          <div className='w-2/12 text-center text-xs font-semibold text-white'>{item.playerMatchFours}</div>
          <div className='w-1/12 text-center text-xs font-semibold text-white'>{item.playerMatchSixes}</div>
          <div className='w-3/12 text-center text-xs font-semibold text-white'>{item.playerMatchStrikeRate}</div>




        </div>
        {item.playerMatchRuns!=0 &&   <div className='flex items-center justify-between w-1/12 ml-3 text-sm font-semibold '>
          <img className={`h-2 w-4 ${index === showWheel && showWheel ? "rotate-180" : ""}`} onClick={() => item.playerMatchRuns!=0?setShowWheel(index):null} src={upwarrow} alt="" />

        </div> }
      
        
        
      </div>
      <div className='flex w-full  text-gray-2 text-xs items-start justify-start'>{item.playerHowOut}</div>

{console.log('runsjjjjjjjjjjjjjjj',item.playerMatchRuns)}
      {showWheel===index && <div className='flex w-full  h-48 bg-gray  mt-5  '>

        <div className=' flex w-6/12    flex justify-center items-center flex-col '>
          <div className='flex m-1  justify-center items-center relative w-28 h-28'>
            <img
              className='w-28 h-28 absolute'
              src={
                'ssss' === 'Wickets' ? '/svgs/groundImageWicket.png' : '/pngsV2/wagon.png'
              }
              alt=''
            />
            <div className='flex m-1  justify-center items-center relative w-28 h-28'>

              {/* {console.log("data",data.getHighlights)} */}
              <div className='w-28 h-28  absolute  overflow-hidden rounded-full '>
                {item.zad && item.zad.map((x, i) => (
                  <div
                    key={i}
                    className={`border-[.2px]   overflow-hidden absolute ${x.runs === '0' ? 'hidden' :
                      (x.runs === '6' && 'border-red w-56') ||
                      (x.runs === '5' && 'border-green w-56') ||
                      (x.runs === '4' && 'border-yellow w-56') ||
                      (x.runs === '2' && 'border-blue-2 w-56') ||
                      (x.runs === '3' && 'border-pink-500 w-56') || 'border-white w-56'



                      } `}
                    style={{
                      bottom: '55%',
                      left: '50%',
                      right: '0%',
                      transform: `rotate(${x.zadval.split(',')[1]}deg)`,
                      transformOrigin: '0 0'
                    }}></div>
                ))}
              </div>
              <div className='relative border rounded-full border-gray-3'>
                {/* height 5.5 rem */}
                {/* window.screen.width > 975 ? window.screen.width/1.5 : window.screen.width+window.screen.width */}
                <PieClass
                  data={dataa}
                  zoneRun={[z2FineLeg, z1SquarLeg, z8MidWicket, z7MidOn, z6MidOff, z5Cover, z4Point, z3ThirdMan]}
                  width={window.screen.width > 975 ? 300 : 150}
                  height={window.screen.width > 975 ? 300 : 150}
                  battingStyle={item.battingStyle}
                  zadData={item.zad ? item.zad : []}
                  innerRadius={window.screen.width > 975 ? 57 * 2 : 57}
                  outerRadius={window.screen.width > 975 ? 75 * 2 : 75}
                />
              </div>
            </div>

          </div>
        </div>

        <div className='flex flex-col items-center justify-center flex-col w-6/12  '>
          <div className='flex flex-col items-center justify-center   w-10/12'>
            <div className='flex   border-white w-full'>

              <div className='w-6/12 text-center  border-r'>
                <div className='flex w-full text-xs text-gray-2'>
                  35%
                  <br />
                  OFF SIDE


                </div>


              </div>

              <div className='w-6/12 text-center  text-xs text-white'>13 RUNS </div>

            </div>
            <div className='flex   border-white w-full items-center'>

              <div className='w-6/12 text-center  border-r mt-2'>
                <div className='flex w-full text-xs text-gray-2'>
                  35%
                  <br />
                  LEG SIDE


                </div>


              </div>

              <div className='w-6/12 text-center  text-xs text-white'>13 RUNS </div>

            </div>
          </div>
          <div className='flex flex-wrap items-center justify-center'>
            <div className='flex items-center justify-center ml-2 text-xs py-2 '>
              <div className='w-2 h-2 mx-1 bg-white rounded-full'></div> <span className='text-white'>1 Run</span>
            </div>
            <div className='flex items-center justify-center ml-2 text-xs py-2 '>
              <div className='w-2 h-2 mx-1 bg-blue rounded-full'></div> <span className='text-white'>2 Run</span>
            </div>
            <div className='flex items-center justify-center ml-2 text-xs py-2 '>
              <div className='w-2 h-2 mx-1 bg-pink-500 rounded-full'></div> <span className='text-white'>3 Run</span>
            </div>
            <div className='flex items-center justify-center ml-2 text-xs py-2 '>
              <div className='w-2 h-2 mx-1 bg-yellow rounded-full'></div> <span className='text-white'>4 Run</span>
            </div>
            <div className='flex items-center justify-center ml-2 text-xs py-2 '>
              <div className='w-2 h-2 mx-1 bg-red rounded-full'></div> <span className='text-white'>6 Run</span>
            </div>


          </div>






        </div>


      </div>
      }


    </div>

  </div>)
})

}
<div className=' flex justify-between bg-gray-4 py-3 px-2 border-b border-gray  w-full'>
  <span className='text-xs text-white font-bold '>Extras</span>
  <span className='text-xs text-gray-2 font-bold'><span className='text-white font-bold'>{data.getScoreCard.fullScoreCard[teamShow].extras.totalExtras}</span> {` (b ${data.getScoreCard.fullScoreCard[teamShow].extras.byes}, lb ${data.getScoreCard.fullScoreCard[teamShow].extras.legByes}, nb ${data.getScoreCard.fullScoreCard[teamShow].extras.noBalls}, wd ${data.getScoreCard.fullScoreCard[teamShow].extras.wides})`}</span>
</div>
<div className='w-full flex justify-between pv3 ph2 bg-gray-4 py-2 items-center'>
  <span className='w-4/12  text-xs text-white font-bold ml-2'>Total: {data.getScoreCard.fullScoreCard[teamShow].total.runsScored &&
    `${data.getScoreCard.fullScoreCard[teamShow].total.runsScored}/${data.getScoreCard.fullScoreCard[teamShow].total.wickets}`} </span>

  <span className='w-4/12  text-xs text-white font-bold ml-2 text-gray-2'>Overs: {data.getScoreCard.fullScoreCard[teamShow].total.runsScored &&
    data.getScoreCard.fullScoreCard[teamShow].total.overs} </span>
  <span className=' w-4/12  text-xs text-left text-white font-bold ml-2 text-gray-2'>RR: {data.getScoreCard.fullScoreCard[teamShow].total.runRate} </span>


</div>


<div className='w-full py-2 flex bg-gray items-center rounded-b-md-'>
  <span className=' w-3/12  text-white text-xs ml-2'>
    Did Not Bat
  </span>
  <span className='text-xs text-gray-2'>
    {data.getScoreCard.fullScoreCard[teamShow].batting
      .filter((e) => e.playerHowOut === '')
      .map((batsmen, i) => ' ' + batsmen.playerName)
      .toString()}
  </span>
</div>
</>}
{/* bowler */}
       { <div className='flex w-full flex-col  items-start justify-center  m-2  '>
          <div className='mt-4 w-full bg-gray-4 rounded-t-lg'>
            <div className='flex items-center  justify-between p-2 py-3'>
              <div className='text-sm font-semibold w-5/12 text-blue font-bold uppercase'>Bowler</div>
              <div className='flex items-center justify-between w-6/12 text-sm font-semibold   '>
                <div className='w-2/12 text-center text-xs text-white '>O</div>
                <div className='w-2/12 text-center text-xs text-white '>M</div>
                <div className='w-2/12 text-center text-xs text-white '>R</div>
                <div className='w-1/12 text-center text-xs text-white '>W</div>

                <div className='w-3/12 text-center text-xs text-white '>ECO</div>



              </div>
              <div className='flex items-center justify-between w-1/12 text-sm font-semibold '></div>
            </div>
          </div>
          {data.getScoreCard.fullScoreCard[teamShow].bowling.map((item, index) => {
            return (<div className=' w-full bg-gray '>
              <div className='flex flex-col items-center  justify-between p-2 py-3'>
                <div className='flex w-full'>
                  <div className='text-sm font-semibold w-5/12 text-white  '>{item.playerName}</div>
                  <div className='flex items-center justify-between w-6/12 text-sm font-semibold '>
                    <div className='w-2/12 text-center text-xs font-semibold text-white'>{item.playerOversBowled}</div>
                    <div className='w-2/12 text-center text-xs font-semibold text-white'>{item.playerMaidensBowled}</div>
                    <div className='w-2/12 text-center text-xs font-semibold text-white'>{item.playerRunsConceeded}</div>
                    <div className='w-1/12 text-center text-xs font-semibold text-white'>{item.playerWicketsTaken}</div>
                    <div className='w-3/12 text-center text-xs font-semibold text-white'>{item.playerEconomyRate}</div>




                  </div>
                </div>
              </div>
            </div>
            )
          })}

        </div>
          }

        {/* FOW */}



       { <div className='flex w-full flex-col  items-start justify-center  m-2  '>
          <div className='flex items-center justify-center text-gray-3 font-bold w-full text-sm'>FALL OF WICKETS</div>
          <div className='mt-4 w-full bg-gray-4 rounded-t-lg'>
            <div className='flex items-center  justify-center w-full p-2 py-3'>
              <div className='text-sm flex items-center  justify-start  font-semibold w-4/12 text-blue font-bold uppercase'>Score </div>
              <div className='text-sm flex items-center  justify-center  font-semibold w-4/12 text-blue font-bold uppercase'>Player</div>

              <div className='text-sm flex items-center  justify-end  font-semibold w-4/12 text-blue font-bold uppercase'>Over</div>



            </div>
          </div>
          {data.getScoreCard.fullScoreCard[teamShow].fow.map((item, index) => {
            return (   <div className=' w-full bg-gray '>
            <div className='flex items-center  justify-center w-full p-2 py-3'>
              <div className='text-xs flex items-center  justify-start  font-semibold w-3/12 text-white font-bold uppercase'>   {item.runs}/{item.order}</div>
              <div className=' flex items-center  justify-center  w-6/12 text-gray-2 font-bold uppercase text-xs'>{item.playerName}</div>

              <div className='text-xs flex items-center  justify-end  font-semibold w-3/12 text-white font-bold uppercase'>{item.over_ball}</div>



            </div>
          </div>
            )
          })}

                </div>
          }

                
</>}   </div>)
          })}



        </div>


        {
          false && <>
            {
              data && data.getScoreCard.fullScoreCard !== null ? (
                data.getScoreCard.fullScoreCard.map((ex, i) => (
                  <div className='flex items-center justify-center' key={i}>
                    <div
                      className={`flex justify-between  ${i === card ? 'cdcgr' : 'bg-gray'
                        } `}
                      onClick={() => setCard((prev) => (prev === i ? 5 : i))}>
                      <span className='f6 fw6 '>
                        {ex.battingTeamShortName}
                        {props.matchData.matchType === 'Test' && (i <= 1 ? '1st Inn' : '2nd Inn')}
                      </span>
                      <div className='flex items-center'>
                        <span className='f6 fw6 mh2'>
                          {ex.total.runsScored}
                          {ex.total.wickets && ex.total.wickets !== '10' ? `/${ex.total.wickets}` : null}
                          {ex.total.overs ? `(${ex.total.overs})` : ''}
                        </span>
                        <img
                          className={`w1 h1 cursor-pointer ${i === card ? '' : 'rotate-180'}  `}
                          src={upArrowWhite}
                          alt=''
                        />
                      </div>
                    </div>

                    {/* batsman details */}
                    <div className={`${card === i ? 'flex  flex-col' : 'hidden'}`}>
                      <div className=''>
                        <div className='text-gray-2 bg-gray  font-semibold flex justify-between items-center w-full py-3 px-2'>
                          <div className='  f8 w-50'>BATTER</div>
                          <div className='  f8 tr ph2 w-10'>R</div>
                          <div className='  f8 tr ph2 w-10'>B</div>
                          <div className='  f8 tr ph2 w-10'>4s</div>
                          <div className='  f8 tr ph2 w-10'>6s</div>
                          <div className='  f8 tr ph2 tr w-10'>SR</div>
                        </div>
                        <div>
                          {ex.batting.map(
                            (batsmen, i) =>
                              batsmen.playerHowOut && (
                                <div>
                                  <div key={i} className='flex justify-between items-center p-2'>
                                    <Link
                                      {...getPlayerUrl(batsmen.playerName, batsmen.playerID)}
                                      passHref
                                      className='w-50'>

                                      <div
                                        className=' tl fw5 f7  cursor-pointer'
                                        onClick={() => handlePlayerNavigation(batsmen.playerName, batsmen.playerID)}>
                                        <span className='tl font-medium f7'> {batsmen.playerName} </span>
                                        <span className='pl2 fw6 f7'>
                                          {' '}
                                          {batsmen.isCaptain && batsmen.isKeeper
                                            ? '(c) & (Wk)'
                                            : batsmen.isCaptain
                                              ? '(c)'
                                              : batsmen.isKeeper
                                                ? '(Wk)'
                                                : ''}
                                        </span>
                                        <br />
                                        <p
                                          className={`f8 mb0 ${batsmen.playerDismissalInfo === 'not out' ? 'red font-medium' : 'text-gray-2'
                                            }`}>
                                          {batsmen.playerHowOut}
                                        </p>
                                      </div>

                                    </Link>
                                    <div className=' f8 ph2 tr w-10'>{batsmen.playerMatchRuns}</div>
                                    <div className=' f8 ph2 tr w-10'>{batsmen.playerMatchBalls}</div>
                                    <div className=' f8 ph2 tr w-10'>{batsmen.playerMatchFours}</div>
                                    <div className=' f8 ph2 tr w-10'>{batsmen.playerMatchSixes}</div>
                                    <div className=' f8 ph2 tr w-10'>{batsmen.playerMatchStrikeRate}</div>
                                  </div>
                                  <div className='h-[1px] w-full bg-black/80'></div>
                                </div>
                              )
                          )}
                        </div>
                      </div>
                      <div className='w-100 flex justify-between pv3 ph2'>
                        <span className='f7 fw5'>Extras</span>
                        <span className='f7 fw5'>{`${ex.extras.totalExtras} (b ${ex.extras.byes}, lb ${ex.extras.legByes}, nb ${ex.extras.noBalls}, wd ${ex.extras.wides})`}</span>
                      </div>
                      <div className='w-100 flex justify-between pv3 ph2 bg-gray-4 items-center'>
                        <span className='f7 white fw5'>Total</span>
                        <span className='f7 white fw5'>
                          {ex.total.runsScored &&
                            `${ex.total.runsScored}/${ex.total.wickets} ( ${ex.total.overs} overs RR ${ex.total.runRate}  ) `}
                        </span>
                      </div>
                      {ex.batting.filter((e) => e.playerHowOut === '').length > 0 && (
                        <div className='w-100  f6 pv3 ph2'>
                          <span className=' fw5'>
                            {props.matchData.matchStatus === 'live' ? 'Yet To Bat' : 'Did Not Bat'}:
                          </span>
                          <span className='f6 text-gray-2'>
                            {ex.batting
                              .filter((e) => e.playerHowOut === '')
                              .map((batsmen, i) => ' ' + batsmen.playerName)
                              .toString()}
                          </span>
                        </div>
                      )}

                      {/* Bowlers Details */}

                      <div className=''>
                        <div className='bg-gray text-gray-2 flex items-center justify-center p-2 font-semibold'>
                          <div className='  f8 tl w-50'>BOWLER</div>
                          <div className='  f8 tr w-10'>0</div>
                          <div className='  f8 tr w-10'>M</div>
                          <div className='  f8 tr w-10'>R</div>
                          <div className='  f8 tr w-10'>W</div>
                          <div className='  f8 tr w-10'>Econ</div>
                        </div>
                        <div>
                          {ex.bowling.map((bowler, i) => (
                            <div>
                              <div key={i} className='w-full justify-between items-center flex p-2'>
                                <Link
                                  {...getPlayerUrl(bowler.playerName, bowler.playerID)}
                                  passHref
                                  className='w-50'>

                                  <div
                                    className=' tl fw5 f7  cursor-pointer'
                                    onClick={() => handlePlayerNavigation(bowler.playerName, bowler.playerID)}>
                                    <span className='tl fw5 f7'> {bowler.playerName} </span>
                                    <span className='pl2 fw6 f7'> {bowler.isCaptain ? '(c)' : ''}</span>
                                    <br />
                                  </div>

                                </Link>
                                <div className=' f8 tr w-10'>{bowler.playerOversBowled}</div>
                                <div className=' f8 tr w-10'>{bowler.playerMaidensBowled}</div>
                                <div className=' f8 tr w-10'>{bowler.playerRunsConceeded}</div>
                                <div className=' f8 tr w-10'>{bowler.playerWicketsTaken}</div>
                                <div className=' f8 tr w-10'>{bowler.playerEconomyRate}</div>
                              </div>
                              <div className='h-[1px] w-full bg-black/80'></div>
                            </div>
                          ))}
                        </div>
                      </div>
                      {ex.fow && ex.fow.length > 0 && (
                        <div className=''>
                          <div className='p-2  bg-gray-4'>
                            <span className='f7 fw5'>Fall of Wickets</span>
                          </div>
                          <div className='flex justify-between p-2 items-center'>
                            <span className='f7 fw6 fw5'>Score</span>
                            <span className='f7 fw6 fw5'> Player</span>
                            <span className='f7 fw6 fw5'> Over </span>
                          </div>

                          {ex.fow.map((fow, i) => (
                            <div>
                              <div key={i} className='flex justify-between p-2 '>
                                <span className='f8 fw5'>
                                  {fow.runs}/{fow.order}
                                </span>
                                <div className='flex items-center'>
                                  <span className='f8 fw5'> {fow.playerName} </span>
                                  <span className=' ml2  fw6 f7'>
                                    {' '}
                                    {fow.isCaptain && fow.isKeeper
                                      ? '(c) & (Wk)'
                                      : fow.isCaptain
                                        ? '(c)'
                                        : fow.isKeeper
                                          ? '(Wk)'
                                          : ''}
                                  </span>
                                </div>
                                {/* <span className='f8 fw5'> {fow.playerName} </span> */}
                                <span className='f8 fw5'> {fow.over_ball} </span>
                              </div>
                              <div className='h-[1px] w-full bg-black/80'></div>
                            </div>
                          ))}
                        </div>
                      )}
                    </div>
                  </div>
                ))
              ) : (
                <div className='bg-gray font-semibold text-sm p-2 mt2  tc text-white'>
                  Scorecard will begin as soon as the match commences
                </div>
              )
            }
          </>
        }
      </div >
    );
}
