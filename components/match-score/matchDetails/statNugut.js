import React, { useState } from 'react';

export default function StatNugut(props) {
  const [currentTabBat, setcurrentTabBat] = useState(0);
  const [currentTabBol, setcurrentTabBol] = useState(false);

  // ['1-6', '7-15', '16-20'] : ['1-10', '11-40', '41-50']

  return (
    <div>
      {props.data && (
        <>
          <div className=' mt2 flex  flex-column  bg-gray-4  shadow-4  pa1 text-white'>
            <div className='flex w-100 bb b--black pa2 f7 fw5 ttu'>
              <div className='w-50'>Stat Nuggets</div>
              <div className='w-50 tr text-gray-2'>*last 5 years</div>
            </div>

            <div className='w-100 flex '>
              <div className='flex   flex-column  justify-start  w-100    pv2'>
                <div className='flex w-100 items-center justify-center  b--black    '>
                  {props.data.batsman.map((item, index) => {
                    return (
                      <div
                        onClick={() => (setcurrentTabBat(index), setcurrentTabBol(false))}
                        className={`w-33 f7  pa1 fw6 lh-copy     ${
                          currentTabBat == index && !currentTabBol
                            ? index == 0
                              ? 'ba b--red'
                              : 'ba b--red'
                            : index == 0
                            ? 'bt bl bb b--black '
                            : currentTabBol
                            ? ' bb bt  bl b--black'
                            : 'br bb bt b--black  '
                        }  `}>
                        <div className='flex  w-100 pa1'>
                          <div className={`${index == currentTabBat && !currentTabBol ? 'red_10' : ''} flex w-60 `}>
                            {' '}
                            {item.playerName}
                          </div>
                          <div className='flex w-40 '>
                            <img
                              className='w1'
                              src={`/svgs/${
                                currentTabBat == index && !currentTabBol ? 'batred.svg' : 'notactivebat.svg'
                              }`}
                              alt=''
                              srcset=''
                            />
                          </div>
                        </div>
                      </div>
                    );
                  })}

                  {props.data.bowler.map((item, index) => {
                    return (
                      <div
                        className={` w-33 f7  pa1 fw6 lh-copy  ${
                          currentTabBol ? ' ba b--red' : 'bb bt br  b--black'
                        }  `}
                        onClick={() => setcurrentTabBol(true)}>
                        {' '}
                        <div className='flex  w-100 pa1'>
                          <div className={`flex w-60 ${currentTabBol ? 'red_10' : ''} `}>{item.playerName}</div>
                          <div className='flex w-40 '>
                            <img
                              className='w1'
                              src={currentTabBol ? `/svgs/about.svg` : `/svgs/bowlnotselect.svg`}
                              alt=''
                              srcset=''
                            />
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>

            {currentTabBol == false ? (
              <div className='flex flex-column'>
                <div className='flex w-100 bb b--black pa2 f7 fw5 ttu'>SR in phases</div>

                <div className='w-100 flex items-center justify-center tc fw5 f7 pa2'>
                  <div className='w-20 br b--black'>
                    <div className='fw6'> {(props.data && props.data.batsman[currentTabBat].powerplay) || '-'} </div>{' '}
                    <div className='mt1 fw5 f8'>{props.matchType == 'ODI' ? '1-10' : '1-6 ov'} </div>{' '}
                  </div>
                  <div className='w-20 br b--black'>
                    <div className='fw6'> {(props.data && props.data.batsman[currentTabBat].middle_overs) || '-'}</div>{' '}
                    <div className='mt1 fw5 f8'> {props.matchType == 'ODI' ? '11-40' : ' 7-15 ov'} </div>{' '}
                  </div>

                  <div className='w-20 br b--black'>
                    <div className='fw6'> {(props.data && props.data.batsman[currentTabBat].death_overs) || '-'}</div>{' '}
                    <div className='mt1 fw5 f8'> {props.matchType == 'ODI' ? '41-50' : '16-20 ov'} </div>
                  </div>

                  <div className='w-20 br b--black'>
                    <div className='fw6'>
                      {' '}
                      {(props.data && props.data.batsman[currentTabBat].against_spinner) || '-'}
                    </div>{' '}
                    <div className='mt1 fw5 f8'> Vs SPIN </div>
                  </div>

                  <div className='w-20 b--black'>
                    <div className='fw6'> {(props.data && props.data.batsman[currentTabBat].against_pacer) || '-'}</div>{' '}
                    <div className='mt1 fw5 f8'> Vs PACE </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className='flex flex-column'>
                <div className='flex w-100 bb b--black pa2 f7 fw5 ttu'>ECO in phases</div>

                <div className='w-100 flex items-center justify-center tc fw5 f7 pa2'>
                  <div className='w-20 br b--black'>
                    <div className='fw6'> {(props.data && props.data.bowler[0].powerplay) || '-'}</div>{' '}
                    <div className='mt1 fw5 f8'>{props.matchType == 'ODI' ? '1-10' : '1-6 ov'} </div>{' '}
                  </div>
                  <div className='w-20 br b--black'>
                    <div className='fw6'> {(props.data && props.data.bowler[0].middle_overs) || '-'}</div>{' '}
                    <div className='mt1 fw5 f8'> {props.matchType == 'ODI' ? '11-40' : ' 7-15 ov'} </div>{' '}
                  </div>

                  <div className='w-20 br b--black'>
                    <div className='fw6'> {(props.data && props.data.bowler[0].death_overs) || '-'}</div>{' '}
                    <div className='mt1 fw5 f8'> {props.matchType == 'ODI' ? '41-50' : '16-20 ov'} </div>
                  </div>

                  <div className='w-20 br b--black'>
                    <div className='fw6'>
                      {' '}
                      {(props.data && props.data.bowler[0].against_left_handed_batsman) || '-'}
                    </div>{' '}
                    <div className='mt1 fw5 f8'> Vs RHB </div>
                  </div>

                  <div className='w-20 b--black'>
                    <div className='fw6'>
                      {' '}
                      {(props.data && props.data.bowler[0].against_right_handed_batsman) || '-'}
                    </div>{' '}
                    <div className='mt1 fw5 f8'> Vs LHB </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
}
