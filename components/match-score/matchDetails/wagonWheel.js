import React from 'react';
import * as d3 from 'd3';

const WagonWheel = (props) => {
  const wagonPos = {
    x: 0,
    y: -180
  };
  let { wagon, selectedWagonPoint } = props;
  const rad = d3.radialLine();

  return (
    <div className='pv3 flex justify-center'>
      <div className='w45 h45 relative br-100 overflow-hidden'>
        <div className='relative'>
          <img className='z-1' src={require('../../public/svgs/ground.png')} alt='' />

          <svg width={200} height={200} className='absolute top-0 left-0'>
            <g transform='translate(100,100)'>
              {[1, 2, 3, 4, 5, 6].map((ball, index) => (
                <path
                  d={rad([
                    [100, 10],
                    [35 * index * (Math.PI / 90), 90]
                  ])}
                  fill='none'
                  strokeWidth={1}
                  stroke={index === 1 ? 'red' : `#4a90e2`}
                />
              ))}
            </g>
          </svg>
        </div>
        <div
          style={{
            width: 4,
            height: 4,
            borderRadius: '50%',
            border: '1px solid white',
            background: 'red',
            position: 'absolute',
            top: '43%',
            left: '48.5%'
          }}
        />
      </div>
    </div>
  );
};

export default WagonWheel;

const pageStyle = {
  lineStyle: {
    position: 'absolute',
    left: '1%', //"50%",
    top: '2%', //"43%",
    height: 'inherit',
    width: 'inherit'
  }
};
