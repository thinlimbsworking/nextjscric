import React, { useEffect, useState } from 'react'
import SwiperModule from './test'
import { useRouter } from 'next/router'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING } from '../api/queries'
import Heading from './commom/heading'
import DataNotFound from './commom/datanotfound'
export default function Matchups(props) {
  const [toggle, setToggle] = useState(0)
  const [tabIndex, setTabIndex] = useState(0)
  const [bowler, setBowler] = useState(0)
  //  console.log("MatchupsMatchupsMatchupsMatchups",props.teamIDs)

  const router = useRouter()
  const { loading, error, data } = useQuery(
    HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING,
    {
      variables: { matchID: props.matchID },
    },
  )
  const capsuleCss = {
    Strong: ' text-green  text-sm  font-bold text-center',
    Average: ' text-yellow  text-sm  font-bold text-center',
    Weak: ' text-red  text-sm  font-bold text-center',
  }
  // alert(888)
  return data &&
    data.HomeScreenPlayerMatchupsUpcomming &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
    data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 ? (
    <>
      <div className="w-fl lg:mt-14 md:mt-14 px-1 lg:px-0 md:px-0 ">
        {/* <div className='text-md  text-white tracking-wide font-bold '> Player MatchUp</div>
        <div className='text-xs  text-white tracking-wide font-bold '>  Select a batter and see how they perform against opposing bowlers</div>
       
      <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        <Heading
          heading={'Player MatchUp'}
          subHeading={
            'Select a batter and see how they perform against opposing bowlers'
          }
        />
      </div>
      <div className="mt-5 rounded mx-2 text-white lg:hidden md:hidden">
        <div className="flex w-full justify-between items-center mt-5 ">
          <div className=" w-3/12 text-gray-2 text-xs flex justify-between items-center  ">
            {' '}
            <span className="h-1 w-1 bg-green rounded-full"></span> BATTING
          </div>

          <div className="bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-9/12 p-1">
            <div
              className={`w-1/2 text-center rounded-3xl py-2  ${
                toggle === 0 ? `border-2  border-green bg-gray-8` : ''
              }`}
              onClick={() => (setTabIndex(0), setToggle(0))}
            >
              {data &&
                data.HomeScreenPlayerMatchupsUpcomming &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
                  .homeTeamShortName}
            </div>
            <div
              className={`w-1/2 text-center rounded-3xl py-2 ${
                toggle === 1 ? `border-2  border-green bg-gray-8` : ''
              }`}
              onClick={() => (setTabIndex(0), setToggle(1))}
            >
              {data &&
                data.HomeScreenPlayerMatchupsUpcomming.preMatch[1]
                  .awayTeamShortName}
            </div>
          </div>
        </div>

        <SwiperModule
          toggle={toggle}
          homeTeamID={
            data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamID
          }
          awayTeamID={
            data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamID
          }
          tabIndex={tabIndex}
          setTabIndex={setTabIndex}
          data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
        />

        {false && (
          <div className="bg-gray h-52 flex flex-col items-center justify-center rounded-lg ">
            <div className="flex relative items-center justify-center ">
              <img
                className="h-36 w-36 bg-gray-4 object-top object-cover rounded-full"
                src={`https://images.cricket.com/players/${item.batsmanId}_headshot_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/playerph.png')}
              />

              <img
                className=" absolute h-6 w-6 left-0 bottom-0 object-fill"
                src={item.role == 'Bowler' ? ball : bat}
              />
              {/* <img
                      className=' absolute h-5 w-5 border   right-0 bottom-0 object-fill rounded-full'
                      src={`https://images.cricket.com/teams/${props.data[0].homeTeamID}_flag_safari.png`}
                    /> */}

              <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center">
                <img
                  className="  h-5 w-5  rounded-full"
                  src={`https://images.cricket.com/teams/${
                    props.toggle === 0 ? props.homeTeamID : props.awayTeamID
                  }_flag_safari.png`}
                  onError={(evt) =>
                    (evt.target.src = '/svgs/images/flag_empty.svg')
                  }
                />
              </div>
            </div>
            <div className=" text-center text-xs font-semibold mt-2 ">
              {item.name}
            </div>
          </div>
        )}
        {true && (
          <div className="flex bg-gray items-center justify-between rounded py-3">
            <div className="flex w-1/3 flex-col  items-center justify-center  ">
              <div className="text-xs font-medium uppercase text-gray-2">
                Balls{' '}
              </div>
              <div className="uppercase text-xl font-semibold  ">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].ballsFaced}
              </div>
            </div>
            <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
              <div className="uppercase text-xs font-medium text-gray-2">
                RUNS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].runsScored}
              </div>
            </div>
            <div className="flex flex-col  w-1/3  items-center justify-center">
              <div className="uppercase text-xs font-medium text-gray-2">
                WICKETS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].wicket}{' '}
              </div>
            </div>
          </div>
        )}

        {true && (
          <div className="  flex flex-wrap justify-center  mt-4 items-center">
            {data &&
              data.HomeScreenPlayerMatchupsUpcomming &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 &&
              data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[
                tabIndex
              ].bowlers.map((item, i) => {
                return (
                  <div
                    key={i}
                    className={`flex w-5/12 my-2 mx-3 justify-center bg-gray h-40 relative rounded-xl ${
                      i === bowler ? 'border-green border' : ''
                    }`}
                    onClick={() => setBowler(i)}
                  >
                    <div className="flex flex-col relative items-center justify-center ">
                      <div className="flex relative items-center justify-center ">
                        <img
                          className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                          src={`https://images.cricket.com/players/${item.bowlerId}_headshot_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/pngsV2/playerph.png')
                          }
                        />

                        <img
                          className=" absolute h-6 w-6    left-0 bottom-0 object-fill"
                          src={'/pngsV2/bowlericon.png'}
                        />

                        <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                          <img
                            className="  h-5 w-5  rounded-full"
                            src={`https://images.cricket.com/teams/${
                              toggle === 0
                                ? data.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[1].awayTeamID
                                : data.HomeScreenPlayerMatchupsUpcomming
                                    .preMatch[0].homeTeamID
                            }_flag_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = '/svgs/images/flag_empty.svg')
                            }
                          />
                        </div>
                      </div>

                      <div className=" text-center  text-xs font-semibold mt-2 ">
                        {item.name}
                      </div>
                      <div className={`mt-2  ${capsuleCss[item.label]}`}>
                        {item.label}
                      </div>
                    </div>
                  </div>
                )
              })}
          </div>
        )}
      </div>

      {/* desktop */}
      <div className="mt-5 rounded  text-white hidden lg:block md:block  ">
        {router.asPath === '/' && (
          <div className="flex w-full justify-between items-center mt-5  my-3">
            <div className=" w-6/12 text-gray-2 text-xs flex  items-center  ">
              <span className="mx-2 h-2 w-2 bg-green rounded-full"></span>{' '}
              BATTING
            </div>

            <div className="bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-4/12 ">
              <div
                className={`w-1/2 text-center rounded-3xl py-1  ${
                  toggle === 0 ? `border-2  border-green bg-gray-8` : ''
                }`}
                onClick={() => (setTabIndex(0), setToggle(0))}
              >
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
                    .homeTeamShortName}
              </div>
              <div
                className={`w-1/2 text-center rounded-3xl py-1 ${
                  toggle === 1 ? `border-2  border-green bg-gray-8` : ''
                }`}
                onClick={() => (setTabIndex(0), setToggle(1))}
              >
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[1]
                    .awayTeamShortName}
              </div>
            </div>
          </div>
        )}

        {router.asPath !== '/' && (
          <div className="flex w-full justify-between items-center mt-5  my-3">
            <div className=" w-6/12 text-gray-2 text-xs flex  items-center  ">
              <span className="mx-2 h-2 w-2 bg-green rounded-full"></span>{' '}
              BATTING
            </div>

            <div className="bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-4/12 ">
              <div
                className={`w-1/2 text-center rounded-3xl py-1  ${
                  toggle === 0 ? `border-2  border-green bg-gray-8` : ''
                }`}
                onClick={() => (setTabIndex(0), setToggle(0))}
              >
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[0]
                    .homeTeamShortName}
              </div>
              <div
                className={`w-1/2 text-center rounded-3xl py-1 ${
                  toggle === 1 ? `border-2  border-green bg-gray-8` : ''
                }`}
                onClick={() => (setTabIndex(0), setToggle(1))}
              >
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[1]
                    .awayTeamShortName}
              </div>
            </div>
          </div>
        )}
        <div className="flex  items-center justify-center">
          <div className="w-6/12">
            <SwiperModule
              toggle={toggle}
              homeTeamID={
                data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamID
              }
              awayTeamID={
                data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamID
              }
              tabIndex={tabIndex}
              setTabIndex={setTabIndex}
              data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
            />
          </div>

          <div className=" w-6/12 bg-gray flex items-center justify-center rounded  h-28">
            <div className="flex w-1/3 flex-col  items-center justify-center  ">
              <div className="text-xs font-medium uppercase text-gray-2">
                Balls{' '}
              </div>
              <div className="uppercase text-xl font-semibold  ">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].ballsFaced}
              </div>
            </div>
            <div className="flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] ">
              <div className="uppercase text-xs font-medium text-gray-2">
                RUNS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].runsScored}
              </div>
            </div>
            <div className="flex flex-col  w-1/3  items-center justify-center">
              <div className="uppercase text-xs font-medium text-gray-2">
                WICKETS{' '}
              </div>
              <div className="uppercase text-xl font-semibold">
                {data &&
                  data.HomeScreenPlayerMatchupsUpcomming &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] &&
                  data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]
                    .mappings[tabIndex].bowlers[bowler].wicket}{' '}
              </div>
            </div>
          </div>
        </div>

        <div className="  flex flex-wrap justify-center  mt-4 items-center">
          {data &&
            data.HomeScreenPlayerMatchupsUpcomming &&
            data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
            data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 &&
            data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[
              tabIndex
            ].bowlers.map((item, i) => {
              return (
                <div
                  key={i}
                  className={`flex w-2/12 my-2 mx-3 justify-center bg-gray h-40 relative rounded-xl ${
                    i === bowler ? 'border-green border' : ''
                  }`}
                  onClick={() => setBowler(i)}
                >
                  <div className="flex flex-col relative items-center justify-center ">
                    <div className="flex relative items-center justify-center ">
                      <img
                        className="h-20 w-20 bg-gray-4 object-top object-cover rounded-full"
                        src={`https://images.cricket.com/players/${item.bowlerId}_headshot_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = '/pngsV2/playerph.png')
                        }
                      />

                      <img
                        className=" absolute h-6 w-6    left-0 bottom-0 object-fill"
                        src={'/pngsV2/bowlericon.png'}
                      />

                      <div className="absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center ">
                        <img
                          className="  h-5 w-5  rounded-full"
                          src={`https://images.cricket.com/teams/${
                            toggle === 0
                              ? data.HomeScreenPlayerMatchupsUpcomming
                                  .preMatch[1].awayTeamID
                              : data.HomeScreenPlayerMatchupsUpcomming
                                  .preMatch[0].homeTeamID
                          }_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = '/svgs/images/flag_empty.svg')
                          }
                        />
                      </div>
                    </div>

                    <div className=" text-center  text-xs font-semibold mt-2 ">
                      {item.name}
                    </div>
                    <div className={`mt-2  ${capsuleCss[item.label]}`}>
                      {item.label}
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      </div>
    </>
  ) : (
    <DataNotFound />
  )
}
