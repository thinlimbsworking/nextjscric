import React,{useState}from 'react'
import * as d3 from 'd3'
export default function LineChart({data,color}) {
  


  var margin = {top:0, right: 0, bottom: 0, left: 0},
  width = 50 - margin.left - margin.right,
  height = 25 - margin.top - margin.bottom;
 

  const svgRef = React.useRef();
 React.useEffect( () => {
  
  var x = d3.scaleLinear().range([0, width]);
var y = d3.scaleLinear().range([height,1]);

// Define the axes
var xAxis = d3.axisBottom().scale(x);
var yAxis = d3.axisLeft().scale(y);

// create a line based on the data
var line = d3.line()
		.x(function(d) { return x(d.a); })
		.y(function(d) { return y(d.b); }).curve(d3.curveCardinal);

// Add the svg canvas
var svg = d3.select(svgRef.current)
     .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
   
x.domain(d3.extent(data, function(d) { return d.a; }));
y.domain([
		d3.min(data, function(d) { return d.b ; }), 
		d3.max(data, function(d) { return d.b ; })
	]);

// draw the line created above
svg.append("path").data([data])
		.style('fill', 'none')
		.style('stroke', color)
		.style('stroke-width', '4px')
		.attr("d", line);
return()=>{
  d3.selectAll("svg > *").remove();
}

  },[svgRef,data]);
  return (

<div className="" style={{height:"15px"}}>
<svg ref={svgRef} height="100%" width="100%" preserveAspectRatio="xMidYMid meet" viewBox="0 0 60 30"></svg>
 

</div>
  )
}




// var data = [
// 	{a: 0.1,  b: 0.3763460891},
//     {a: 0.2,  b: 0.350773831},
//     {a: 0.3, b: 0.3605610392},
//     {a: 0.4, b: 0.5402252989},
//     {a: 0.5, b: 0.9898870068},
//     {a: 0.6, b: 0.5073363549},
//     {a: 0.7, b: 0.0535673934},
//   {a: 0.8, b: 0.3408754297},
//   {a: 0.9, b: 0.0677684236},
//   {a: 1, b: 0.8358393322}
// ];

// Parse the date / time
// var parseDate = d3.timeParse("%Y");

// force types
// function type(dataArray) {
// 	dataArray.forEach(function(d) {
// 		d.a=d.a;
// 		d.b;
// 	});
// 	return dataArray;
// }
// data = type(data);

// Set the dimensions of the canvas / graph
// var margin = {top: 30, right: 20, bottom: 30, left: 50},
//     width = 500 - margin.left - margin.right,
//     height = 250 - margin.top - margin.bottom;


// Set the scales ranges
// var x = d3.scaleLinear().range([0, width]);
// var y = d3.scaleLinear().range([height,1]);

// // Define the axes
// var xAxis = d3.axisBottom().scale(x);
// var yAxis = d3.axisLeft().scale(y);

// // create a line based on the data
// var line = d3.line()
// 		.x(function(d) { return x(d.a); })
// 		.y(function(d) { return y(d.b); }).curve(d3.curveCardinal);

// // Add the svg canvas
// var svg = d3.select("body")
//     .append("svg")
//         .attr("width", width + margin.left + margin.right)
//         .attr("height", height + margin.top + margin.bottom)
//     .append("g")
//         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// // set the domain range from the data
// x.domain(d3.extent(data, function(d) { return d.a; }));
// y.domain([
// 		d3.min(data, function(d) { return d.b ; }), 
// 		d3.max(data, function(d) { return d.b ; })
// 	]);

// // draw the line created above
// svg.append("path").data([data])
// 		.style('fill', 'none')
// 		.style('stroke', 'steelblue')
// 		.style('stroke-width', '2px')
// 		.attr("d", line);

// // Add the X Axis
// svg.append("g")
// 		.attr("transform", "translate(0," + height + ")")
// 		.call(xAxis);

// // Add the Y Axis
// svg.append("g")
// 		.call(yAxis);