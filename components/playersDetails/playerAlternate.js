import React, { useState } from 'react';
import backgroundImage from '../../public/svgs/backgroundSearch.svg';
import flagPlaceHolder from '../../public/svgs/images/flag_empty.svg';
const allRounder = '/pngs/allRounder-white-solid.png';
const ball = '/pngs/ball-white-solid.png';
const bat = '/pngs/bat-white-solid.png';
const wicket = '/pngs/wicketKeeper-white-solid.png';
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component';
function PlayerAlternate({
  flag,
  skills,
  player,
  marginRight,
  marginTop,
  BackgroundHeight,
  MediumDeviceHeight,
  PlayerPositionTop,
  ScreenHeight,
  ScreenWidth,
  animate,
  actionShot,
  url,
  ...props
}) {
  const [isError, setisError] = useState(false);
  const bgColors = [
    '#1B6DC7',
    '#3A1E64',
    '#3FA8D1',
    '#3EA870',
    '#BF5A5A',
    '#B85ABF',
    '#725ABF',
    '#BFBF5A',
    '#5ABF5D',
    '#5A89BF',
    '#BF5AB8'
  ];
  const delayArr = [
    '1000ms',
    '1500ms',
    '2000ms',
    '2500ms',
    '300ms',
    '3500ms',
    '4000ms',
    '4500ms',
    '5000ms',
    '5500ms',
    '4700ms',
    '6000ms'
  ];

  // const random=delayArr[Math.floor(Math.random()*delayArr.length)]
  // console.log("random",random)
  // const pulsate = `@keyframes pulsate {
  //   0% { transform: scale(1) translate(0,0px);  }
  //   100% { transform:  scale(1.2) translate(0,20px)}

  // }`
  
  return (
    <div
      key={player.ID}
      className={`w-full text-center  ${isError ? '' : 'bg-gray'} cursor-pointer  relative  flex `}
      style={{
        width: ScreenWidth,
        height: ScreenHeight,
        marginRight: marginRight,
        marginTop: marginTop,
        overflow: 'hidden'
      }}>
      {isError ? (
        <div
          className=' w-full flex flex-column items-center relative justify-center   h-full '
          style={{ backgroundColor: bgColors[Math.floor(Math.random() * bgColors.length)] }}>
          {/* <div className="tl absolute left-0 white top-0 fw3 truncate f7 pt-1 pl1 w-90">{player.BP?player.BP:"Best Score"}</div> */}
          {/* <div className="flex items-center justify-center ">
          <div> */}
          {/* <div className="white fw5 f5">{player.V ? player.V : 0}</div> */}
          <div className='absolute right-0 top-0  pr-1 pt-1'>
            <div style={{ width: '1.5rem' }} className=' h-2   shadow-3'>
              <img
                className='h-full object-cover w-full'
                src={`https://images.cricket.com/teams/${flag}_flag_safari.png`}
                alt=''
                onError={(evt) => (evt.target.src = flagPlaceHolder)}
              />
            </div>
          </div>

          <div className='white ph2 fw5 f6 text-center'>{player.PN}</div>
          <div className='pt-1'>
            <img
              className='w-2 h-2'
              src={
                skills === 'Batsman'
                  ? bat
                  : skills === 'Bowler'
                  ? ball
                  : skills === 'Wicket Keeper'
                  ? wicket
                  : allRounder
              }
              alt=''
            />
          </div>
          {/* {player.ON && <div className="white  truncate w-90"><span className="fw3 f7">vs</span><span className="fw3 pl2 f7">{player.ON }</span></div>} */}
          {/* </div>
         </div> */}
          {/* <div className="absoute flex items-center justify-center white absolute bottom-0 w-full  f8 truncate fw5   h1" style={{backgroundColor:"rgba(0, 0, 0, 0.6)"}}
          ><div className="f8 ph2  white fw5 truncate">{player.PN}</div></div> */}
        </div>
      ) : (
        <div className='h-full flex'>
          <div
            className={`mx-auto w-full  `}
            style={{
              backgroundImage: `url(${backgroundImage})`,
              backgroundPosition: 'mx-auto top',
              backgroundRepeat: 'no-repeat',
              display: 'flex',
              justifyContent: 'mx-auto',
              animationDelay: `${animate ? delayArr[Math.floor(Math.random() * delayArr.length)] : ''}`,
              animationDirection: `${animate ? 'alternate' : ''}`,
              animationDuration: `${animate ? '9000ms' : ''}`,
              animationIterationCount: `${animate ? 'infinite' : ''}`,
              animationTimingFunction: `${animate ? 'ease-in-out' : ''}`,
              animationName: `${animate ? 'pulsate' : ''}`,
              // animation: `${animate?'9000ms pulsate infinite alternate ease-in-out':''}`,
              height: BackgroundHeight,
              backgroundSize: 'contain'
            }}>
            {/* <LazyLoadImage  className={`absolute   object-cover object-top ${actionShot?'w-full':''} ${MediumDeviceHeight !== "" ? MediumDeviceHeight : ""}`} style={{ top: PlayerPositionTop }} src={actionShot?`https://images.cricket.com/players/${player.ID}_actionshot_safari.jpg`:`https://images.cricket.com/players/${player.ID}_headshot_safari.png`} alt={actionShot?`https://images.cricket.com/players/${player.ID}_actionshot_safari.jpg`:`https://images.cricket.com/players/${player.ID}_headshot_safari.png`} onError={() => {setisError(true);}} /> */}
            <LazyLoadImage
              className={`absolute  object-cover object-top  ${actionShot ? 'w-full' : ''} ${
                MediumDeviceHeight !== '' ? MediumDeviceHeight : ''
              }`}
              style={{ top: PlayerPositionTop }}
              // src={actionShot ? `${url}` : `${url}`}
              src={url}
              alt=''
              onError={() => {
                setisError(true);
              }}
            />
            {/* <img  src={url}/> */}
            {/* {console.log("->>>>>>>>",imh)} */}
          </div>
          <div className='absolute right-0 top-0  pr-1 pt-1'>
            <div style={{ width: '1.5rem' }} className=' h1  object-cover shadow-3'>
              <img
                className='h-full object-cover w-full'
                src={`https://images.cricket.com/teams/${flag}_flag_safari.png`}
                alt=''
                onError={(evt) => (evt.target.src = flagPlaceHolder)}
              />
            </div>
          </div>
          <div
            className='absoute flex items-center  white absolute bottom-0 w-full  f8 truncate fw5    h1'
            style={{ backgroundColor: 'rgba(0, 0, 0, 0.6)' }}>
            <div className='f8 w-80  white pl-2  white  truncate'>{player.PN}</div>
            <div className='w-20 flex items-center justify-center'>
              <img
                className='h-4 w-4'
                src={
                  skills === 'Batsman'
                    ? bat
                    : skills === 'Bowler'
                    ? ball
                    : skills === 'Wicket Keeper'
                    ? wicket
                    : allRounder
                }
                alt=''
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

const p = React.memo(PlayerAlternate);
export default p;
