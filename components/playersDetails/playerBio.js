import React from 'react';
import parse from 'html-react-parser';
let  backIconWhite = '/svgs/backIconWhite.svg';


export default function PlayerBio({ playerDetils }) {

  let playerbio = playerDetils.getPlayersProfileV2;
  return (
    <div className='md:mt-28 lg:mt-24 center text-white pb-4'>
      <div className=' p-2 flex gap-2 items-center md:hidden'>
        <img className=' p-2 bg-gray rounded-md' onClick={() => window.history.back()} src={backIconWhite} alt='back icon' />
        <span className='text-base font-bold'>Player Bio</span>
      </div>
      <div className='bg-gray-4 rounded-md m-2'>
        <div className='w-full h-full hidescroll overflow-y-auto'>
          <div className=' p-2 text-xs font-bold '>{playerbio.name}</div>
          <div className='bg-gray p-2 text-xs rounded-md'>{parse(playerbio.description)}</div>
        </div>
      </div>
    </div>
  );
}
