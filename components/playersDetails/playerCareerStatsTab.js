import React, { useState, useRef, useEffect } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { useRouter } from 'next/router'
import { PLAYER_CAREER_STATS_DATA } from '../../api/queries'
import Loading from '../Loading'
import Link from 'next/link'
//import Swiper from 'react-id-swiper';
// import Playerfallback from '../../public/pngs/fallbackprojection.png';
const Playerfallback = '/pngsV2/playerph.png'
import SliderCommon from '../commom/slider'
import Compare from '../../public/svgs/compare.svg'
import CleverTap from 'clevertap-react'
import { getPlayerUrl } from '../../api/services'
import { PLAYER_COMPARE_VIEW } from '../../constant/Links'
import LineChart from './lineChart'
const empty = '/svgs/Empty.svg'
import Imgix from 'react-imgix'

export default function PlayerCareerStatsTab({
  playerID,
  playerSlug,
  playerRole,
}) {
  let router = useRouter()
  const [ActiveTab, setActiveTab] = useState(
    playerRole && playerRole === 'Bowler' ? 'bowlingStats' : 'battingStats',
  )
  const [swiper, updateSwiper] = useState(null)
  const [currentIndex, updateCurrentIndex] = useState(0)
  let WicketKeeper = ['battingStats', 'fieldingStats']
  let bowlingStyle = ['battingStats', 'bowlingStats']

  useEffect(() => {
    if (swiper !== null || swiper != undefined) {
      try {
        // swiper.lazy.load()
        // console.log("swiper",swiper)
        swiper.on('slideChange', () => {
          updateCurrentIndex(swiper.realIndex)
        })
      } catch (e) {
        console.log(e)
      }
    }
  }, [swiper])

  const { loading, error, data: playerData } = useQuery(
    PLAYER_CAREER_STATS_DATA,
    {
      variables: { playerID: playerID },
    },
  )

  const playerRoleFields = {
    battingStats: {
      label: [
        'Format',
        'Matches',
        'Innings',
        'Rec. Form',
        'Runs',
        'Bat S/R',
        'Average',
        '50s/100s',
        '4s/6s',
      ],
      value: [
        'format',
        'matches',
        'innings',
        'recForm',
        'runs',
        'strikeRate',
        'average',
        'hundredsfifties',
        'foursix',
      ],
    },
    bowlingStats: {
      label: [
        'Format',
        'Matches',
        'Innings',
        'Rec. Form',
        'Balls',
        'Wickets',
        'Economy',
        'Bowl S/R',
        'Average',
        '5w',
      ],
      value: [
        'format',
        'matches',
        'innings',
        'recForm',
        'ballsBowled',
        'wickets',
        'economyRate',
        'strikeRate',
        'average',
        'fivetenWicketHauls',
      ],
    },
    fieldingStats: {
      label: ['Format', 'Catches', 'Stumpings', 'Run Outs'],
      value: ['format', 'catches', 'stupms', 'runOuts'],
    },
  }

  const params1 = {
    preloadImages: true,
    updateOnImagesReady: true,
    loadPrevNext: true,
    loadPrevNextAmount: 3,
    effect: 'coverflow',
    loop: true,
    setWrapperSize: true,
    slidesPerView: 3,
    // setWrapperSize: false,
    centeredSlides: true,
    loopedSlides: 3,
    coverflowEffect: {
      rotate: 10,
      stretch: 0,
      depth: 50,
      slideShadows: false,
      modifier: 1,
      observer: true,
      observeParents: true,
    },
    // breakpoints: { 640: { slidesPerView: , spaceBetween: 0 } },
    getSwiper: updateSwiper,
    navigation: {
      nextEl: '#nbutton',
      prevEl: '#pbutton',
    },
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      // 320: {
      //    slidesPerView: 3,
      //    spaceBetween: 20,
      //    coverflowEffect: {
      //       rotate: 10,
      //       stretch: 0,
      //       depth: 50,
      //       slideShadows: false,
      //       modifier: 1
      //    }
      // },
      // when window width is >= 480px
      // when window width is >= 640px
      760: {
        preloadImages: true,
        updateOnImagesReady: true,
        loadPrevNext: true,
        loadPrevNextAmount: 5,
        loopedSlides: 5,
        loop: true,
        observer: true,
        observeParents: true,
        slidesPerView: 5,
        spaceBetween: 40,
        coverflowEffect: {
          rotate: 0,
          stretch: 0,
          depth: 20,
          slideShadows: false,
          modifier: 1,
        },
      },
    },
  }

  const handlePlayerCompare = (playerID1, playerSlug, playerID2) => {
    return { as: eval(PLAYER_COMPARE_VIEW.as), href: PLAYER_COMPARE_VIEW.href }
  }

  const handleCompare = () => {
    CleverTap.initialize('PlayersCompare', {
      Source: 'Players',
      Platform: global.window.localStorage
        ? global.window.localStorage.Platform
        : '',
    })
    let playerID2 = playerData.getPlayersProfileV2.similarplayers.filter(
      (id) => id.playerID !== playerID,
    )[currentIndex].playerID
    let playerID1 = playerID
    router.push(
      handlePlayerCompare(playerID1, playerSlug, playerID2).href,
      handlePlayerCompare(playerID1, playerSlug, playerID2).as,
    )
  }

  if (error) return <div></div>
  else if (loading) {
    return <Loading />
  } else
    return (
      <>
        {playerData.getPlayersProfileV2 ? (
          <div className=" ">
            <div className="flex items-center justify-center  w-min mx-auto rounded-full  my-2  p-1 bg-gray md:bg-gray-8 lg:bg-gray-4 gap-2">
              {(playerData.getPlayersProfileV2.role === 'Wicket Keeper'
                ? WicketKeeper
                : bowlingStyle
              ).map((categ, i) => (
                <div
                  key={i}
                  className=" flex  itemse-center justify-center "
                  onClick={() => {
                    setActiveTab(categ)
                  }}
                >
                  <div
                    className={`p-2 w-32 text-xs uppercase ${
                      ActiveTab === categ
                        ? 'bg-gray-8 border-2 border-green-6'
                        : 'bg-gray '
                    } text-center rounded-full cursor-pointer`}
                  >
                    {categ === 'battingStats'
                      ? 'BATTING STATS'
                      : categ === 'fieldingStats'
                      ? 'KEEPING STATS'
                      : 'BOWLING STATS'}
                  </div>
                </div>
              ))}
            </div>
            {/* mobile */}
            <div className=" lg:hidden md:hidden m-2 rounded-md ">
              <div className="flex hidescroll rounded-md">
                <div>
                  {playerRoleFields[ActiveTab].label.map((x, i) => (
                    <div
                      key={i}
                      className={`border-b shadow-3 ${
                        i === 0 ? 'bg-gray-4' : 'bg-gray'
                      }
                        } fw6 flex items-center justify-center border-black   h-9 f6`}
                      style={{
                        width: `${
                          playerData.getPlayersProfileV2[ActiveTab].length < 4
                            ? '105px'
                            : '90px'
                        }`,
                      }}
                    >
                      {x}
                    </div>
                  ))}
                </div>
                <div className="flex  overflow-scroll">
                  {playerData.getPlayersProfileV2[ActiveTab].map((item, j) => (
                    <div
                      key={j}
                      className=""
                      style={{
                        width: `${
                          playerData.getPlayersProfileV2[ActiveTab].length < 4
                            ? '105px'
                            : '90px'
                        }`,
                      }}
                    >
                      {playerRoleFields[ActiveTab].value.map((value, k) => (
                        <div
                          key={k}
                          className={`border-b ${
                            k === 0 ? 'bg-gray-4' : 'bg-gray'
                          }
                            } border-black overflow h-9  text-cente`}
                          style={{
                            width: `${
                              playerData.getPlayersProfileV2[ActiveTab].length <
                              4
                                ? '105px'
                                : '90px'
                            }`,
                          }}
                        >
                          {value === 'recForm' &&
                          item['recForm'] &&
                          item['recForm'].length !== 0 ? (
                            <LineChart
                              color={item['color']}
                              data={item['recForm']}
                            />
                          ) : value !== 'recForm' && item[value] !== '' ? (
                            item[value]
                          ) : value === 'recForm' &&
                            item['recForm'] &&
                            item['recForm'].length === 0 ? (
                            '-'
                          ) : (
                            '-'
                          )}
                        </div>
                      ))}
                    </div>
                  ))}
                </div>
                {/* desktop */}
              </div>
            </div>
            <div className="hidden  md:flex lg:flex flex-wrap overflow-scroll">
              <div
                className={`${
                  playerData.getPlayersProfileV2[ActiveTab].length < 4
                    ? 'w-4/12'
                    : 'w-1/5'
                }`}
              >
                {playerRoleFields[ActiveTab].label.map((x, i) => (
                  <div
                    key={i}
                    className={`border-b ${i === 0 ? 'bg-gray-4' : 'bg-gray'}
                    fw6 text-center border-black h-9  text-center  flex items-center justify-center f6 w-full`}
                  >
                    {x}
                  </div>
                ))}
              </div>

              <div
                className={`flex ${
                  playerData.getPlayersProfileV2[ActiveTab].length < 4
                    ? 'w-8/12'
                    : 'w-4/5'
                } flex-between`}
              >
                {playerData.getPlayersProfileV2[ActiveTab].map((item, j) => (
                  <div
                    key={j}
                    className={` ${
                      playerData.getPlayersProfileV2[ActiveTab].length < 4
                        ? 'w-2/5'
                        : 'w-1/5'
                    }`}
                  >
                    {playerRoleFields[ActiveTab].value.map((value, k) => (
                      <div
                        key={k}
                        className={`border-b ${
                          k === 0 ? 'bg-gray-4' : 'bg-gray'
                        }
                         border-black h-9   flex items-center justify-center  w-full text-center f6`}
                      >
                        {value === 'recForm' &&
                        item['recForm'] &&
                        item['recForm'].length !== 0 ? (
                          <LineChart
                            color={item['color']}
                            data={item['recForm']}
                          />
                        ) : value !== 'recForm' && item[value] !== '' ? (
                          item[value]
                        ) : value === 'recForm' &&
                          item['recForm'] &&
                          item['recForm'].length === 0 ? (
                          '-'
                        ) : (
                          '-'
                        )}
                      </div>
                    ))}
                  </div>
                ))}
              </div>
            </div>

            {playerData &&
              playerData.getPlayersProfileV2 &&
              playerData.getPlayersProfileV2.similarplayers &&
              playerData.getPlayersProfileV2.similarplayers.filter(
                (id) => id.playerID !== playerID,
              ).length > 0 && (
                <div className="bg-gray-4  mt3 mb3 mx-2 rounded-md">
                  <div className="flex justify-between pt-1">
                    <div className="fw6  f5-l f6  p-2">SIMILAR PLAYERS</div>
                    <div
                      className={` mx-2 mt-2`}
                      onClick={() => handleCompare()}
                    >
                      <span className="px-4 py-2 rounded-md text-green-6 border-2 border-green-6 text-xs font-bold">
                        COMPARE
                      </span>
                    </div>
                  </div>
                  {/* <div className='bb b--black'>indian player</div> */}
                  {/* mobile */}

                  {/* desktop */}
                  <div className=" relative  bg-gray mt-2 rounded-md ">
                    <SliderCommon
                      data={
                        playerData?.getPlayersProfileV2?.similarplayers || []
                      }
                      event="players"
                      source="SimilarPlayers"
                    />

                    {/* <Swiper {...params1}> */}
                    {/* {playerData &&
                        playerData.getPlayersProfileV2 &&
                        playerData.getPlayersProfileV2.similarplayers &&
                        playerData.getPlayersProfileV2.similarplayers
                          .filter((id) => id.playerID !== playerID)
                          .map((player, i) => (
                            <div
                              key={player.playerID}
                              className='flex   flex-column items-center cursor-pointer '
                              onClick={() => {
                                CleverTap.initialize('Players', {
                                  Source: 'SimilarPlayers',
                                  PlayerID: player.playerID,
                                  Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
                                });
                              }}>
                              <Link {...getPlayerUrl(player)} passHref>
                                <a>
                                  <div className='relative '>
                                    <div
                                      className={`rounded-full  ${currentIndex === i ? 'w4 h4 ' : 'w35 h35 mt3'
                                        }   overflow-hidden flex justify-center items-top  bg-gray-2 `}>

                                      {player.headShotImage !== '' ? (
                                        <Imgix
                                          src={`${player.headShotImage}?fit=crop&crop=face`}
                                          width={currentIndex === i ? 127 : 94}
                                          height={currentIndex === i ? 183 : 160}
                                        />
                                      ) : (
                                        <img
                                          className='  '
                                          // style={{ height: currentIndex === i ? 183 : 160, width: currentIndex === i ? 127 : 94 }}
                                          src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                          alt={''}
                                          onError={(evt) => (evt.target.src = Playerfallback)}
                                        />
                                      )}
                                    </div>
                                    <div className='absolute fw5 dark-green f7 ' style={{ left: -4, bottom: -5 }}>
                                      {Math.round(player.similarity.toString())}%
                                    </div>
                                  </div>
                                  <div className='f7 pt-2   tc'>{player.playerName}</div>
                                </a>
                              </Link>
                            </div>
                          ))}  */}
                    {/* </Swiper> */}
                    <div className="absolute absolute--fill flex justify-between items-center md:hidden lg:hidden">
                      <div
                        id={`pbutton`}
                        className="white cursor-pointer  outline-0  mt4 hidden md:block lg:block"
                      >
                        <svg width="44" focusable="false" viewBox="0 0 24 24">
                          <path
                            fill="#38d925"
                            d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                      <div
                        id={`nbutton`}
                        className="white cursor-pointer hidden md:block lg:block outline-0  mt4"
                      >
                        <svg width="44" viewBox="0 0 24 24">
                          <path
                            fill="#38d925"
                            d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                    </div>
                  </div>

                  {/* <div className='flex items-center justify-center w-full py-2 cursor-pointer'>
                    <div className={`br-pill  bg-darkRed ph-3 py-2 flex `} onClick={() => handleCompare()}>
                      <img src={Compare} alt={Compare} />
                      <span className='pl-3 white fw5 f6'>Compare</span>
                    </div>
                  </div> */}
                </div>
              )}
          </div>
        ) : (
          <div className="bg-white flex flex-column items-center justify-center fw5 text-centerpv4">
            <div className="w-8 h-8">
              <img className="h-100 w-full" src={empty} alt="" />
            </div>
            <div>Player Stats data not available</div>
          </div>
        )}
      </>
    )
}
