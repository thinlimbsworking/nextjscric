import React, { useState, useEffect } from 'react';
import differenceInYears from 'date-fns/differenceInYears';
import Link from 'next/link';
import parse from 'html-react-parser';
import { useRouter } from 'next/router';
import { getPlayerUrlBio, PlayerMatchView } from '../../api/services';

import { PLAYER_VIEW_BIO, HEADER } from '../../constant/Links';
import { useQuery } from '@apollo/react-hooks';
import MetaDescriptor from '../MetaDescriptor';
// import Loading from '.';
import { PLAYER_CAREERSTATS_DETAILS_GQL } from '../../api/queries';
import Imgix, { Picture, Source } from 'react-imgix';
import { Schedule } from '../../constant/MetaDescriptions';


let whitebat =  '/svgs/whitebat.svg';
let wicketKeeper = '/svgs/wicketKeeper.svg';
let bowler = '/svgs/bowling.svg';
let  allRounder = '/svgs/allRounderWhite.svg';
let PlayerBackground = '/svgs/PlayerBackground.svg';
let backIconWhite = '/svgs/backIconWhite.svg';

let flagPlaceHolder = '/svgs/flag_empty.svg';
let Playerfallback = '../../public/pngsV2/playerPH.png';
let background = '/pngsV2/player-role-tile-black.png';

export default function PlayerDetailsLayout({ data, tab, playerID, ...props }) {
  let router = useRouter();
  const [playerName, setPlayerName] = useState(router.asPath.split('/')[3]);
  const [swiper, updateSwiper] = useState(null);
  const [selectedTab, setSelectedTab] = useState(router.asPath.split('/')[4]);
  const [currentIndex, updateCurrentIndex] = useState(0);
  //   const [playerDetails,setplayerDetails]=useState(null)
  //   const { loading, error, data: playerDetailsPlayer } = useQuery(PLAYER_CAREERSTATS_DETAILS_GQL, {
  //     variables: { playerID: playerID },onCompleted:(data)=>{
  //       if(data && data.getPlayersProfileV2){
  //         setplayerDetails(data.getPlayersProfileV2)
  //       }

  //     }
  //  });
  // console.log("playerDetailsplayerDetails",playerDetailsPlayer)
  let playerDetails = data && data.getPlayersProfileV2;

  useEffect(() => {
    setPlayerName(router.asPath.split('/')[3]);
    setSelectedTab(router.asPath.split('/')[4]);
  }, []);
  const handlePlayerBio = (playerID, playerName) => {
    return { as: eval(PLAYER_VIEW_BIO.as), href: PLAYER_VIEW_BIO.href };
  };

  const handleBioNav = (playerID, playerName) => {
    router.push(handlePlayerBio(playerID, playerName).href, handlePlayerBio(playerID, playerName).as);
  };

  let PlayerTabs = ['career-stats', 'recent', 'fantasy', 'articles'];
  let date = new Date();

  //   const handleBack=()=>{
  //     return { as: eval(HEADER['players'].as), href: HEADER['players'].href };
  //   }
  // const handleBckPlayer=()=>{
  //   router.push(handleBack().href, handleBack().as);

  // }
  // if (error) return <div></div>;
  // else if (loading) {
  //    return (
  //       <Loading />
  //    );
  // }
  // else
  return <>
    <MetaDescriptor section={Schedule(data && data.miniScoreCard && data.miniScoreCard.data[0], router.asPath, status)} />
    <div className=' mx-auto hidescroll text-white '>
      <div className='sticky top-0 z-40 bg-gray-8 md:hidden lg:hidden'>
        <div className='flex items-center p-1'>
          <div className='bg-gray-4 p-2 rounded-md'>
            <img
              className='w-5 h-5'
              onClick={() => window.history.back()}
              src='/svgs/backIconWhite.svg'
              alt='back icon'
            /> </div>
          <div className='text-white m-2 text-xl font-bold'>{playerDetails && playerDetails.name}</div>
        </div>
        <div
          className='m-2 mt-0 rounded-md relative overflow-hidden shadow-1 '
          style={{
            backgroundImage: `url(${PlayerBackground})`,
            //   backgroundPosition: 'center',
            backgroundSize: 'cover'
          }}>
          <div className='absolute   left-0 right-0 w-full' style={{ bottom: -10 }}>
            <div className='absolute w-full flex flex-col  justify-center p-2 text-xs'>
              <div className='' style={{ width: '37%' }}>
                <div className='font-bold'>Batting Style</div>
                <div className=' truncate '>{playerDetails && playerDetails.battingStyle}</div>
              </div>
              <div className='mt-1 w-60'>
                <div className='font-bold'>Bowling Style</div>
                <div className=''>{playerDetails && playerDetails.bowlingStyle}</div>
              </div>
            </div>

            <img className='w-full' src={background} />
          </div>

          <div className='flex flex-col '>
              
            <div className='flex flex-row mt-2'>
              <div className='w-50  flex flex-col px-2 gap-2'>
                {/* <div className="h3 absolute bottom-0 right-0 left-0 bg-green z-1" style={{background: "linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5))"}}></div> */}
                <div className='text-xl text-bold  font-bold'>{playerDetails && playerDetails.name }</div>
                <div className='flex  items-center gap-2'>
                  <img
                    alt=''
                    src={`https://images.cricket.com/teams/${playerDetails && playerDetails.teamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                    className='h-4 w-6 shadow-4'
                  />
                  <div className=' text-sm font-semibold white uppercase'>{playerDetails && playerDetails.birthPlace}</div>
                </div>

                <div className='flex  gap-2 '>
                  <div className='p-2 bg-gray-8 rounded-md'>
                  <img
                    className=''
                    src={
                      playerDetails &&
                      playerDetails.role &&
                      (playerDetails.role === 'Batsman'
                        ? whitebat
                        : playerDetails.role === 'Bowler'
                          ? bowler
                          : playerDetails.role === 'Wicket Keeper'
                            ? wicketKeeper
                            : allRounder)
                    }
                    alt={
                      playerDetails &&
                      playerDetails.role &&
                      (playerDetails.role === 'Batsman'
                        ? whitebat
                        : playerDetails.role === 'Bowler'
                          ? bowler
                          : playerDetails.role === 'Wicket Keeper'
                            ? wicketKeeper
                            : allRounder)
                    }
                  />
                  </div>
                  <div className='white flex gap-2 items-center'>
                    <span className='text-xs'>
                      {playerDetails && playerDetails.dob
                        ? differenceInYears(
                          new Date(`${date.getUTCFullYear()}`, `${date.getMonth()}`, `${date.getDay()}`),
                          new Date(...playerDetails.dob.split('/').reverse())
                        ) + ' YEARS'
                        : '--'}
                    </span>
                  </div>
                </div>
               
              </div>
              <div className='w-50 flex  justify-end '>
                <div className=' overflow-hidden flex justify-center items-center object-cover'>
                  {/* <img
                    className='  center pt-3 '
                    style={{ height: 250, width: 180 }}
                    src={playerDetails.headShotImage !=="" ? `${playerDetails.headShotImage}?fit=crop&crop=faces&w=180&h=250&auto=compress&q=30`:`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                    alt={Playerfallback}
                    onError={(evt) => (evt.target.src = Playerfallback)}
                  /> */}
                  {playerDetails.headShotImage !== '' ? (
                    <Imgix src={`${playerDetails.headShotImage}?fit=crop&crop=face`} width={180} height={250} />
                  ) : (
                    <img
                      className='  center pt-3 '
                      style={{ height: 250, width: 180 }}
                      src={`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                      alt={Playerfallback}
                      onError={(evt) => (evt.target.src = Playerfallback)}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='  '>
          <div className='flex justify-center  overflow-x-scroll z-999 hidescroll  bg-gray-4 w-full text-center'>
            {PlayerTabs.map((tabName, i) => {
              return (
                (<Link
                  key={i}
                  {...PlayerMatchView(playerName, playerID, tabName)}
                  replace
                  passHref
                  title={tabName}
                  onClick={() => {
                    setSelectedTab(tabName);
                  }}
                  className={` w-1/4 p-2 cursor-pointer nowrap  text-center text-xs font-semibold capitalize`}
                  style={{
                    paddingBottom: '.8rem',
                    paddingTop: '0.8rem',
                    color: tabName.toLowerCase() === selectedTab.toLowerCase() ? '#6A91A9' : null,
                    borderBottom:
                      tabName.toLowerCase() === selectedTab.toLowerCase() ? '2px solid #6A91A9' : null
                  }}>

                  <div
                    className={`flex-nowrap  text-center `}>
                    {tabName === 'career-stats' ? 'Career Stats' : tabName}
                  </div>

                </Link>)
              );
            })}
          </div>
        </div>
      </div>

      {/* desktop view */}

      <div className='hidden md:block lg:block '>
        <div
          className=' hidden md:block lg:block shadow-2 flex-l flex-m w-full flex-col   '
          style={{
            backgroundImage: `url(${PlayerBackground})`,
            backgroundSize: 'cover'
          }}>
          <div className='hidden md:block lg:block'>
            <img
              className='w-2  p-2'
              onClick={() => window.history.back()}
              src='/svgs/backIconWhite.svg'
              alt='back icon'
            />
          </div>
          <div className='flex w-full pt-3 justify-center items-center'>
            <div className='w-50 flex  justify-end pt-5 '>
              <div className='w-40 pt-3 p-2'>
                <div className='text-xl font-bold text-white'>{playerDetails && playerDetails.name}</div>
                <div className='flex items-center gap-2 pt-3 '>
                  <img
                    className='w-5'
                    src={
                      playerDetails &&
                      (playerDetails.role === 'Batsman'
                        ? whitebat
                        : playerDetails.role === 'Bowler'
                          ? bowler
                          : playerDetails.role === 'Wicket Keeper'
                            ? wicketKeeper
                            : allRounder)
                    }
                    alt={
                      playerDetails &&
                      (playerDetails.role === 'Batsman'
                        ? whitebat
                        : playerDetails.role === 'Bowler'
                          ? bowler
                          : playerDetails.role === 'Wicket Keeper'
                            ? wicketKeeper
                            : allRounder)
                    }
                  />
                  <div className='text-white'>
                    <span className='text-xs'>
                      {playerDetails && playerDetails.dob
                        ? differenceInYears(
                          new Date(`${date.getUTCFullYear()}`, `${date.getMonth()}`, `${date.getDay()}`),
                          new Date(...playerDetails.dob.split('/').reverse())
                        ) + ' YEARS'
                        : '--'}
                    </span>
                  </div>
                </div>
                <div className='flex items-center gap-2 mt-2'>
                  <img
                    alt=''
                    src={`https://images.cricket.com/teams/${playerDetails && playerDetails.teamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                    className='h-4 w-8 shadow-4'
                  />
                  <div className=' text-base uppercase '>{playerDetails && playerDetails.birthPlace}</div>
                </div>
                <div className='pt-3'>
                  <div className='fw5  white f6'>Batting Style</div>
                  <div className='f7  white pt-2 truncate '>{playerDetails && playerDetails.battingStyle}</div>
                </div>
                <div className='pt-3 '>
                  <div className='fw5 white f6'>Bowling Style</div>
                  <div className='f7 pt-2 white '>{playerDetails && playerDetails.bowlingStyle}</div>
                </div>
              </div>
            </div>
            <div className='w-50 flex justify-start'>
              <div className='  overflow-hidden flex  items-center object-cover '>
                {/* <img
                  className='  center '
                  style={{ height: 320, width: 220 }}
                  src={playerDetails.headShotImage !=="" ? `${playerDetails.headShotImage}?fit=crop&crop=faces&w=220&h=320&auto=compress&q=30`:`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                  alt={Playerfallback}
                  onError={(evt) => (evt.target.src = Playerfallback)}
                /> */}
                {/* <img
                  className='  center '
                  src={playerDetails.headShotImage !=="" ? `${playerDetails.headShotImage}?fit=crop&crop=faces&w=220&h=320`:`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                  alt={Playerfallback}
                  onError={(evt) => (evt.target.src = Playerfallback)}
                /> */}
                {playerDetails.headShotImage !== '' ? (
                  <Imgix src={`${playerDetails.headShotImage}?fit=crop&crop=face`} width={220} height={320} />
                ) : (
                  <img
                    className='  center '
                    style={{ height: 320, width: 220 }}
                    src={`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                    alt={Playerfallback}
                    onError={(evt) => (evt.target.src = Playerfallback)}
                  />
                )}

                {/* <picture>
                  <img srcSet={`${playerDetails.headShotImage}?fit=crop&crop=face&w=220&h=320&dpr=1 1x`,
                  `${playerDetails.headShotImage}?fit=crop&crop=face&w=220&h=320&dpr=2 2x`,
                  `${playerDetails.headShotImage}?fit=crop&crop=face&w=220&h=320&dpr=3 3x`
                }
                src={`${playerDetails.headShotImage}?w=220&h=320`}
                ></img>
                </picture> */}
                {/* <img srcSet={}/> */}
              </div>
            </div>
          </div>
        </div>
        <div className=' shadow-4   '>
          <div className='flex  overflow-x-scroll z-999 hidescroll   bg-gray text-xs font-semibold capitalize'>
            {PlayerTabs.map((tabName, i) => {
              return <>
                <Link
                  key={i}
                  {...PlayerMatchView(playerName, playerID, tabName)}
                  replace
                  passHref
                  title={tabName}
                  onClick={() => {
                    setSelectedTab(tabName);
                  }}
                  className={` ${tabName === 'career-stats' ? 'w-25' : 'w-25'
                    } cursor-pointer nowrap px-3 w-1/4 text-center `}
                  style={{
                    paddingBottom: '.8rem',
                    paddingTop: '0.8rem',

                    color: tabName.toLowerCase() === selectedTab.toLowerCase() ? '#6A91A9' : null,
                    borderBottom:
                      tabName.toLowerCase() === selectedTab.toLowerCase() ? '2px solid #6A91A9' : null
                  }}>

                  <div
                    className={`f6 flex-nowrap ttext-center`}>
                    {tabName === 'career-stats' ? 'Career Stats' : tabName}
                  </div>

                </Link>
              </>;
            })}
          </div>
        </div>
      </div>

      {props.children}
      {selectedTab === 'career-stats' && playerDetails && playerDetails.description && (
        <div className='bg-gray-4 mx-2 mt-2 rounded-md md:mb-4 lg:mb-4'>
          <div className='text-xs font-bold p-2'>PLAYER BIO</div>
          <div className='bg-gray mt-1 rounded-md'>
            <div className='text-xs  p-2 '>About</div>
            <div className='text-ellipsis overflow-hidden text-xs p-2  text-gray-2'>
              {parse(
                playerDetails && playerDetails.description !== null ? playerDetails.description.slice(0, 350) : '--'
              )}
            </div>
            <div
              // players/4912/kemar-andre-jamal-roach/undefined/playerbio
              className=' text-xs font-bold text-right p-2 cursor-pointer'
              onClick={() => handleBioNav(playerID, playerName)}>
              {playerDetails && playerDetails.description !== null ? 'More...' : ' '}
            </div>
          </div>
        </div>
      )}
    </div>
  </>;
}
