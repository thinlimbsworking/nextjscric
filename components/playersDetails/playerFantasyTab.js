import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useRouter } from 'next/router';
import { PLAYER_FANTASY_MATCH } from '../../api/queries';
import Loading from '../Loading';
const empty = '/svgs/Empty.svg';
import DataNotFound from '../commom/datanotfound';
export default function PlayerFantasyTab({ playerID }) {
  let router = useRouter();
  const [format, setFormat] = useState('all');
  const [playerid, setPlayerid] = useState(router.asPath.split('/')[2]);

  useEffect(() => {
    let playerid = router.asPath.split('/')[2];
    setPlayerid(playerid);
  }, []);

  const {
    loading,
    error,
    data: playerFantasyData
  } = useQuery(PLAYER_FANTASY_MATCH, {
    variables: { playerID: playerID },
    onCompleted: (data) => {}
  });
  let category = ['all', 'test', 'odi', 't20', 't20Domestic'];

  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return (
      <>
        <div className='flex  bg-gray h-12 w-min rounded-full my-2 mx-auto px-2 py-3 gap-2 justify-between items-center md:bg-gray-8'>
          {category.map((y, i) => (
            <div
              key={i}
              className={`w-14 h-8 p-1 text-xs font-bold  ${
                format === y ? 'border-2 border-green-6 text-green-6 bg-gray-8' : 'white  bg-gray'
              } text-center cursor-pointer flex items-center justify-center rounded-full cursor-pointer`}
              onClick={() => {
                setFormat(y);
              }}>
              {y === 'test'
                ? 'Test'
                : y === 'odi'
                ? 'ODI'
                : y === 't20'
                ? 'T20I'
                : y === 't20Domestic'
                ? 'T20s'
                : 'ALL'}
            </div>
          ))}
        </div>
        <div className='m-2'>
          {
            <div className='flex w-full h-10 item-center justify-between text-xs p-2 bg-gray-4  lh-copy rounded-t-md '>
              <div className=' w-3/12  flex justify-start items-center  '>Opposition</div>
              <div className='w-3/12  flex items-center justify-center '>Batting</div>
              <div className='w-3/12 flex items-center justify-center '>Bowling</div>
              <div className='w-2/12  flex items-center justify-center '>Points</div>
              <div className='w-1/12 flex items-center justify-center '>DTA</div>
            </div>
          }

          {playerFantasyData &&
          playerFantasyData.getPlayerFantasyMatches &&
          playerFantasyData &&
          playerFantasyData.getPlayerFantasyMatches[format].length > 0 ? (
            playerFantasyData.getPlayerFantasyMatches[format].map((fantasy, i) => (
              <div
                key={i}
                className={`flex bg-gray  w-full bb b--black p-2 items-center h-8  text-xs ${i === playerFantasyData.getPlayerFantasyMatches[format].length-1 ? 'rounded-b-md':''} `}>
                {
                  <div className='w-3/12  flex items-center justify-start  '>
                    <span className='fw4'>{fantasy.opposition.split('vs')[0]} vs</span>
                    <span className='fw6 pl-1'>{fantasy.opposition.split('vs')[1]}</span>
                  </div>
                }
                {
                  <div className='w-3/12 text-center '>
                    <div className='fw4 '>
                      {fantasy.battingStats.split('&')[0]}
                      {fantasy.battingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.battingStats.split('&')[1]}</div>
                  </div>
                }
                {
                  <div className='w-3/12  text-center fw4'>
                    <div>
                      {fantasy.bowlingStats.split('&')[0]}
                      {fantasy.bowlingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.bowlingStats.split('&')[1]}</div>
                  </div>
                }
                {<div className='w-2/12   flex items-center justify-center  fw6'>{fantasy.points}</div>}
                {
                  <div className='w-1/12   flex items-center justify-center'>
                    <div className={`${fantasy.isDreamTeam ? 'bg-green' : 'bg-red'}  rounded-md p-1`}></div>
                  </div>
                }
              </div>
            ))
          ) : (
            <DataNotFound/>
          )}
        </div>
      </>
    );
}
