import React from "react";
import { useState } from "react";
import Tooltip from "../commom/tooltip";
import Foldericon from "./Foldericon";
import Rewardmodal from "./rewardmodal";
import ImageWithFallback from "../commom/Image";
export default function Addcoinsmodal({
  showpopup,
  setshowpopup,
  submitCallback,
  profileDetails,
}) {
  console.log("profiledetails", profileDetails);
const [showreward,setshowreward]=useState(false)
  return (
    showpopup && (
      <div className="top-0 lg:top-[6.5%] lg:py-20 lg:max-w-[73%] z-[9999999999] rounded-tl-xl pb-8 rounded-tr-xl mx-1 h-auto w-full border-gray-6 border-solid border-2 fixed bg-gray-6 text-white border-t-4">
        <div
          className="flex items-end
                text-lg border-b-gray border-b-2 border-b-solid pb-4
             text-white mt-6 justify-between"
        >
          <div className="flex-col items-start justify-start pl-3 text-white lg:flex lg:item-start lg:justify-start">
            {profileDetails?.name}

            <p className="items-start justify-start">
              Level -
              <span className="text-green pl-1">{profileDetails?.level}</span>
            </p>
          </div>

          <button className="mx-4 flex gap-2 border-gray-3 px-3 py-2 text-sm border-2 border-solid" onClick={()=>setshowreward(!showreward)}>
            <Foldericon />
            Rewards
          </button>
        </div>
        <h2 className="mt-6 ml-4 lg:flex lg:ml-4 lg:mt-3 lg:gap-2">My Wallet</h2>

        <div className="border-2 w-11/12 p-3 mb-5 rounded-tr-xl rounded-tl-xl rounded-bl-xl rounded-br-xl m-auto border-solid border-gray-1 bg-basebg flex justify-between items-center">
          <h4>Coins</h4>
          <span className={`flex gap-1 ${profileDetails?.totalEarnings<100?'text-red':'text-green'}`}>
            <ImageWithFallback src="/pngsV2/coin.png" alt="" width={26} height={12} />

            {profileDetails?.totalEarnings}
          </span>
        </div>

        <div className="flex items-center justify-between">
          <button
            disabled={profileDetails?.totalEarnings > 100}
            onClick={() => {
              submitCallback();
              setshowpopup();
            }}
            className={`relative bottom-4 border-green text-xs text-green px-5 rounded-md py-2 bg-basebg border-2 disabled:text-gray-1 disabled:border-gray border-solid left-1/2 -translate-x-1/2 mt-4`}
          >
            ADD COINS
          </button>
        <Tooltip content="Enjoy free reload of coins. Add Coins will be enabled when coins are below 100" direction='bottom' className="flex-row text-green">
          <button className="fixed -mt-6 mr-3 right-4 lg:hidden" style={{color: "white"}}></button>
          </Tooltip>
        </div>

        <br />
        {!showreward && <button
          onClick={setshowpopup}
          className="absolute left-1/2 -translate-x-1/2 mt-2 lg:mt-8"
        >
          <ImageWithFallback src="/svgs/up-arrow.png" width={20} height={12}/>
        </button>}
        
        <Rewardmodal showmodal={showreward} setshowmodal={()=>setshowreward(!showreward)}/>
      </div>
    )
  );
}
