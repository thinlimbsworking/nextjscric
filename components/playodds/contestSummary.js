import React from "react";
import { clsx } from "clsx";
import { useRouter } from "next/router";
import {
  PREDICTION_PROFILE,
  PREDICTION_PROFILE_MATCH,
} from "../../api/queries";
import { useQuery } from "@apollo/react-hooks";

import Rightprediction from "./rightprediction";
import ImageWithFallback from "../commom/Image";
const leaderboardIcon = "/svgs/icons/leaderboardIcon.svg";

export default function contestSummary({ props }) {
  let router = useRouter();
  console.log(props, "hiiProp");
  const token = localStorage.getItem("tokenData");

  const { data: profiledata } = useQuery(PREDICTION_PROFILE, {
    variables: {
      year: "",
      month: "",
      token: token
    },
    onCompleted: (data) => {
      console.log(data, "profiledataaaaa");
    },
  });

  const { data } = useQuery(PREDICTION_PROFILE_MATCH, {
    variables: {
      matchID: props?.matchID,
      token:token
    },
  });
  console.log(data, "dataofthe match");
  const matchPredictions = React.useMemo(
    () =>
      (
        data?.predictionProfileMatch &&
        Object.keys(data.predictionProfileMatch).filter(
          (key) =>
            typeof data.predictionProfileMatch[key] !== "number" &&
            data?.predictionProfileMatch[key]
        )
      )?.map((key) => data?.predictionProfileMatch[key]) || [],
    [data]
  );

  console.log(matchPredictions, "matchPred");
  console.log(profiledata, "matchDetailsData");
  return (
    <>
      {/* flex fixed justify-start bg-gray-8 items-center w-full md:hidden lg:hidden pb-4  */}
      <div className="flex fixed justify-start z-[999999] items-center w-full mt-1 lg:hidden">
        <div
          onClick={() => window.history.back()}
          className="outline-0 cursor-pointer mr-2 rounded bg-gray ml-1"
        >
          <svg width="30" focusable="false" viewBox="0 0 24 24">
            <path
              fill="#ffff"
              d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
            ></path>
            <path fill="none" d="M0 0h24v24H0z"></path>
          </svg>
        </div>

        {profiledata?.predictionProfile?.matches.map((item) => (
          <div className="text-white text-lg gap-8 font-bold hover:break-all">
            <span className="mr-1">
              {item?.matchDetails?.matchID === props?.matchID &&
                item?.matchDetails?.matchNumber}
            </span>

            {item?.matchDetails?.matchID === props?.matchID &&
              item?.matchDetails?.homeTeamName}
            {item?.matchDetails?.matchID === props?.matchID && (
              <>
                <span> VS </span>
                {item?.matchDetails?.awayTeamName}
              </>
            )}
          </div>
        ))}
      </div>
      <div className="fixed bg-gray-8 lg:-mt-4 z-[99999] w-full lg:hidden">
        <div className="p-2 item-start justify-start flex mt-12 lg:mt-16">
          <div
            className="flex gap-2 bg-gray border-2 border-gray-3 p-2 rounded-md cursor-pointer"
            onClick={() =>
              router.push(
                `/play-the-odd/myContest/${props.matchID}/matchSummary/leaderboard`
              )
            }
          >
            <ImageWithFallback src={leaderboardIcon} width={16} height={5} alt="leaderboard" />
            <span className="text-sm font-semibold text-white pr-5">
              Match Leaderboard
            </span>
          </div>
        </div>
      </div>

      {/* larger screen leaderboard button */}

      <div className="lg:relative  hidden lg:block md:block ">
        <div className="p-2 item-start justify-start flex">
          <div
            className="flex gap-2 bg-gray border-2 border-gray-3 p-2 rounded-md cursor-pointer"
            onClick={() =>
              router.push(
                `/play-the-odd/myContest/${props.matchID}/matchSummary/leaderboard`
              )
            }
          >
            <ImageWithFallback  src={leaderboardIcon} width={16} height={1} alt="leaderboard" />
            <span className="text-sm font-semibold text-white pr-5">
              Match Leaderboard
            </span>
          </div>
        </div>
      </div>
      <Rightprediction
        predictionDetails={data?.predictionProfileMatch}
        profiledata={profiledata}
        props={props}
      />
      <div className="bg-gray-6 flex border-2 text-white border-gray gap-6 border-solid rounded-md py-4 px-2 m-2 flex-col mb-10">
        {matchPredictions?.map((prediction) => (
          <div
            className={clsx("flex flex-col bg-basebg p-3 rounded-md gap-3", {
              ["border-red border-2"]: prediction?.status === "LOSS",
              ["border-green border-2"]: prediction?.status === "WIN",
            })}
          >
            <p> {prediction.question}</p>
            <div className="flex-col bg-gray p-2 rounded-md">
              <p
                className={clsx({
                  ["text-green"]: prediction?.status === "WIN",
                })}
              >
                {prediction.selectedName}
              </p>
              <div className="flex pt-2.5 text-xs justify-between">
                <div className="flex justify-center items-center">
                  <span className="w-24 lg:mr-10 lg:text-lg text-gray-2 pl-2">
                    Your
                    <br /> bet
                  </span>
                  <ImageWithFallback
                    src="/pngsV2/coin.png"
                    width={18}
                    height={5}
                    className="mr-1"
                  />
                  <span className="pt-1">{prediction?.betCoins}</span>
                </div>

                <div className="border-r-2 border-r-gray-2 lg:w-12 border-r-solid"></div>
               
                  <div className="flex items-center justify-center lg:justify-evenly">
                    {prediction?.status === "NEW" && (
                      <span className="mr-8 ml-5 lg:mr-24 lg:text-lg text-gray-2">
                        Potential
                        <br /> Earning
                      </span>
                    )}
                    {prediction?.status === "WIN" && (
                      <span className="mr-12 lg:mr-24 lg:text-lg text-white">
                        You
                        <br /> Won
                      </span>
                    )}
                    {prediction?.status === "LOSS" && (
                      <span className="mr-12 lg:mr-24 text-gray-2 lg:text-lg">
                        You
                        <br /> Won
                      </span>
                    )}

                    <ImageWithFallback
                      src="/pngsV2/coin.png"
                      width={18}
                      height={5}
                    />
                    <span
                      className={`pt-1 px-2 ${
                        prediction?.status === "WIN"
                          ? "text-green font-bold text-lg"
                          : prediction?.status === "LOSS"
                          ? "text-red"
                          : "text-white"
                      }`}
                    >
                      {prediction?.winCoins}
                    </span>
                  </div>
                
              </div>
            </div>
            {prediction?.status === "NEW" && (
              <p className="text-right text-gray-2">
                Result Yet to be declared
              </p>
            )}
            {prediction?.status === "ABANDONED" && (
              <p className="text-right text-gray-2">
                Bet Cancelled
              </p>
            )}
            {prediction?.status === "LOSS" && (
              <>
                <div className="flex flex-col bg-basebg rounded-md gap-3">
                  <div className="flex justify-between bg-gray p-2 rounded-md">
                    <p className="text-green">{prediction.correctName}</p>
                  </div>
                </div>
                {/* <p className="text-right flex items-center gap-1 justify-end text-gray-2">
                  Loss <img src="/pngsV2/coin.png" width="22px" />
                  <span className="text-red">{prediction?.betCoins}</span>
                </p> */}
              </>
            )}
            {/* {prediction?.status === "WIN" && (
              <p className="text-right flex items-center gap-1 justify-end text-gray-2">
                WIN <img src="/pngsV2/coin.png" width="22px" />
                <span className="text-white">{prediction?.winCoins}</span>
              </p>
            )} */}
          </div>
        ))}
      </div>
    </>
  );
}
