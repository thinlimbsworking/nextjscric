import { useQuery } from "@apollo/react-hooks";
import { useMutation } from "@apollo/react-hooks";
import React from "react";
import { useState } from "react";
import {
  UPDATE_WALLET_COINS,
  PREDICTION_PROFILE,
  PREDICTION_PROFILE_STREAK,
  PREDICTION_PROFILE_MATCH,
  GET_PREURL,
} from "../../api/queries";
import Scores from "../commom/score";
import Addcoinsmodal from "./addcoinsmodal";
import Profilecard from "./profilecard";
import Streakcard from "./streakcard";
import Heading from "../commom/heading";
import { useRouter } from "next/router";
import { getContestUrl } from "../../api/services";
import Link from "next/link";
import axios from "axios";
import Monthfilter from "./monthfilter";
import ImageWithFallback from "../commom/Image";

export default function MyContest(props) {
  console.log(props, "propsss");
  let router = useRouter();
  const token = localStorage.getItem("tokenData");

  const [preUrl, setpreUrl] = useState("");
  const [path, setpath] = useState("");
  const [photoUrl, setphotoUrl] = useState("");

  const [showFilter, setshowFilter] = useState(false);
  const [selectedYear, setSelectedYear] = useState("");
  const [selectedMonth, setSelectedMonth] = useState("");

  const getcontestSummary = (matchID) => {
    return getContestUrl(matchID);
  };

  const [updatecoins] = useMutation(UPDATE_WALLET_COINS, {
    onCompleted: (data) => {
      if (data) {
        console.log(data, "dataaaaaa");
      }
    },
  });

  const [imgupload] = useMutation(GET_PREURL, {
    variables: {
      token: token,
    },
  });

  const [displaypopup, setdisplaypopup] = useState(false);
  const { data } = useQuery(PREDICTION_PROFILE_MATCH, {
    variables: {
      matchID: props?.matchID,
      token: token,
    },
  });
  console.log(
    "datatatadatadatadatdatadtadtadtadtadtadtadtadtadatdatdatadtadatat",
    data
  );

  const { data: profileDetails, refetch: fetchProfileDetails } = useQuery(
    PREDICTION_PROFILE,
    {
      variables: {
        year: selectedYear,
        month: selectedMonth,
        token: token,
      },
      onCompleted: (data) => {
        console.log(data, "profiledataaaaa");
      },
    }
  );

  const { data: streakData } = useQuery(PREDICTION_PROFILE_STREAK, {
    variables: {
      token: token,
    },
  });

  const [showcoinsmodal, setshowcoinsmodal] = useState(false);
  console.log("klklklkl", profileDetails);

  // const [images, setImages] = useState([]);

  async function handleImageUpload(e) {
    const image = e.target.files[0];
    if (image) {
      console.log(image, "image");
      const uploadEndPoints = await imgupload();
      if (uploadEndPoints) {
        console.log(uploadEndPoints, "endpoints");
        const conifg = {
          method: "PUT",
          url: uploadEndPoints?.data?.getPresignUrl?.preurl,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "image/png",
          },
          data: image,
        };
        axios(conifg)
          .then(function (response) {
            console.log(JSON.stringify(response.data), "Identifier");
            const imgUrl =
              "https://api.devcdc.com/cricket/services/updateImgix";
            axios(imgUrl, {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                url: uploadEndPoints?.data?.getPresignUrl?.preurl,
                path: uploadEndPoints?.data?.getPresignUrl?.path,
                photoUrl: uploadEndPoints?.data?.getPresignUrl?.photoUrl,
                token: token,
              }),
            })
              .then((res) => res.json())
              .then((response) => {
                console.log("response came", response);
                // imageUploadCB(response, res);
                // setisLoading(false);
                // handleModalClose(false);
              })
              .catch((err) => {
                console.log("error came", err);
                // handleModalClose(false);
                // setisLoading(false);
              });
            // console.log(profileRes.json().then(res => res), "afteriage");
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
  }

  return (
    <>
      <div className={`${displaypopup ? "opacity-25" : ""}`}>
        <div className="flex w-full justify-between items-center fixed lg:hidden top-0 z-50 bg-gray-8 lg:relative">
          <div className="bg-gray-8 flex items-center p-2 rounded-md">
            <div
              onClick={() => window.history.back()}
              className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
            >
              <svg width="30" focusable="false" viewBox="0 0 24 24">
                <path
                  fill="#ffff"
                  d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                ></path>
                <path fill="none" d="M0 0h24v24H0z"></path>
              </svg>
            </div>
            <span className="text-base text-white font-bold lg:hidden">
              Profile
            </span>
          </div>
          <button
            style={{ color: "#6ED54A" }}
            className="pr-1 border-2 border-solid border-gray-3 pl-2 rounded-md py-1 mr-2 lg:mr-4 mt-1 sm:z-50"
            onClick={() => setshowcoinsmodal(!showcoinsmodal)}
          >
            <span className="flex gap-2">
              <ImageWithFallback
                className="w-6 h-6"
                height={22}
                width={22}
                src="/pngsV2/wallet.png"
              />
              <div className="border-r-gray-2 border-r-2 border-r-solid"></div>
              <ImageWithFallback
                width={20}
                height={2}
                src="/pngsV2/coin.png"
                alt=""
              />
              {profileDetails &&
                profileDetails?.predictionProfile?.totalEarnings}
            </span>
          </button>

          <Addcoinsmodal
            showpopup={showcoinsmodal}
            profileDetails={profileDetails?.predictionProfile}
            setshowpopup={() => setshowcoinsmodal(!showcoinsmodal)}
            submitCallback={() => {
              return updatecoins({
                variables: {
                  token: token,
                },
              }).then(() => fetchProfileDetails());
            }}
          />
        </div>

        <div className="text-end hidden lg:block md:block">
          <Addcoinsmodal
            showpopup={showcoinsmodal}
            profileDetails={profileDetails?.predictionProfile}
            setshowpopup={() => setshowcoinsmodal(!showcoinsmodal)}
            submitCallback={() => {
              return updatecoins({
                variables: {
                  token: token,
                },
              }).then(() => fetchProfileDetails());
            }}
          />
          <button
            style={{ color: "#6ED54A" }}
            className="pr-2 border-2 border-solid border-gray-3 pl-3 rounded-md py-1 mr-2 mt-1"
            onClick={() => setshowcoinsmodal(!showcoinsmodal)}
          >
            <span className="flex gap-2">
              <ImageWithFallback
                className="w-6 h-6"
                width={22}
                height={2}
                src="/pngsV2/wallet.png"
              />
              <div className="border-r-gray-2 border-r-2 border-r-solid"></div>
              <ImageWithFallback
                width={20}
                height={2}
                src="/pngsV2/coin.png"
                alt=""
              />
              {profileDetails &&
                profileDetails?.predictionProfile?.totalEarnings}
            </span>
          </button>
        </div>

        <Profilecard
          profileinfo={profileDetails?.predictionProfile}
          handleclick={() => setdisplaypopup(!displaypopup)}
        />
        <Streakcard streakInfo={streakData?.contestStreak} />
        <div
          className={`${
            showcoinsmodal
              ? "flex flex-col items-start opacity-25 justify-start px-3 pb-4"
              : "flex flex-col items-start justify-start px-3 pb-4"
          }`}
        >
          <div className="flex items-end justify-between w-full">
            <Heading heading={"My Contests"} />

            <ImageWithFallback
              width={22}
              height={22}
              className="lg:sticky w-8"
              src="/pngsV2/filter.png"
              role="button"
              onClick={() => setshowFilter(!showFilter)}
            />
            {showFilter && (
              <Monthfilter
                filterShow={showFilter}
                setfilterShow={() => setshowFilter(!showFilter)}
                selectedMonth={selectedMonth}
                selectedYear={selectedYear}
                setMonth={setSelectedMonth}
                setYear={setSelectedYear}
              />
            )}
          </div>

          <div
            className={`w-full mt-3 flex-wrap lg:flex md:flex ${
              showFilter ? "opacity-25 overflow-hidden" : ""
            }`}
          >
            {profileDetails?.predictionProfile?.matches.map(
              ({ matchDetails: card }) => (
                <div
                  key={card.matchID}
                  className="w-full md:w-1/2 lg:w-1/2 overflow-hidden flex justify-center  items-center cursor-pointer "
                >
                  {console.log(
                    getcontestSummary(card.matchID),
                    "kkkkkkkkkkkkkkkk"
                  )}
                  {console.log(card, "card")};
                  {data?.predictionProfileMatch.isCancalled === true ? (
                    ""
                  ) : (
                    <Link
                      className="w-full"
                      {...getcontestSummary(card.matchID)}
                      as={`${getcontestSummary(card.matchID).as}/matchSummary`}
                      href={`${
                        getcontestSummary(card.matchID).href
                      }/matchSummary`}
                      passHref
                    >
                      <div className="-mb-4 w-full cursor-pointer flex flex-col px-2">
                        {card && card.matchID ? (
                          <Scores data={card} isPredictedMatch />
                        ) : (
                          <></>
                        )}
                      </div>
                    </Link>
                  )}
                </div>
              )
            )}
          </div>
        </div>
        {/* <ImageUpload
        setshowupload={() => setdisplaypopup()}
        showupload={displaypopup}
      /> */}

        {/* <ImageUpload/> */}
      </div>
      {displaypopup && (
        <div className="bottom-0 w-full border-gray-6 border-solid border-2 fixed bg-gray-6 z-[9999] text-white border-t-4 h-16">
          <div>
            <div className="flex items-center justify-evenly mt-3">
              {/* { <input type="file" className="bg-gray text-green" multiple accept="image/*" onChange={handleImageUpload}/>} */}
              {/* <span>
                    <input
                      type="file"
                      className="my-2 bg-gray-700 w-full border-b-gray-600 border-b-2 outline-none text-white-500"
                      onChange={handleFileSelector}
                      
                    />  

                    Gallery 
                  </span>  */}
              <label class="block">
                <span class="sr-only">Choose profile photo</span>
                <input
                  type="file"
                  className=" w-full text-sm file:mr-4 file:py-2 file:px-4 file:text-sm file:font-semibold file:text-green file:bg-gray"
                  onChange={handleImageUpload}
                />
              </label>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
