import React, { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import Rulebook from "./rulebook";
import Image from "next/image";

import onboard1 from "../../public/pngsV2/onboard1.png";
import onboard2 from "../../public/pngsV2/onboard2.png";
import onboard3 from "../../public/pngsV2/onboard3.png";
import ImageWithFallback from "../commom/Image";

const Popup = ({ showModal, setshowModal }) => {
  const [rulebookshow, setrulebookshow] = useState(false);
  return (
    <>
      {showModal && (
        <div
          className="bottom-0 rounded-tl-xl rounded-tr-xl mx-1 border-gray-6 border-solid border-2 fixed bg-gray-6 z-[200] text-white border-t-4"
          style={{ height: "37.5rem", width: "100%" }}
        >
          {!rulebookshow ? (
            <div
              className={`flex items-end ${
                rulebookshow ? "justify-between" : "justify-end"
              }  text-gray-3 underline mr-1`}
            >
              <button onClick={() => setrulebookshow(!rulebookshow)}>
                Rule Book
              </button>
            </div>
          ) : (
            ""
          )}

          {rulebookshow && (
            <div>
              <div>
                {rulebookshow && (
                 <div
                 onClick={() => setrulebookshow(!rulebookshow)}
                 className="outline-0 cursor-pointer mr-2 lg:hidden rounded"
               >
                 <svg width="30" focusable="false" viewBox="0 0 24 24">
                   <path
                     fill="#ffff"
                     d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                   ></path>
                   <path fill="none" d="M0 0h24v24H0z"></path>
                 </svg>
               </div>
                )}
              </div>

              <div className="flex items-center justify-center w-full">
                <h1 className="text-green text-xl pb-2">Rule Book</h1>
              </div>
            </div>
          )}

          {!rulebookshow && <SwiperPopup setshowModal={setshowModal} />}
          {rulebookshow && (
            <Rulebook
              showrulebook={rulebookshow}
              setshowrulebook={() => setrulebookshow(!rulebookshow)}
            />
          )}
        </div>
      )}
    </>
  );
};

export default Popup;

const SwiperPopup = ({ setshowModal }) => {
  const [indexSwiper, setIndex] = useState(0);

  return (
    <>
      <Swiper
        onSlideChange={(swiper) => setIndex(swiper.realIndex)}
        modules={[Pagination]}
        className="mySwiper"
      >
        <SwiperSlide className="text-center pt-2">
          Test your cricket knowledge by making predictions on match events
          <ImageWithFallback
            src={onboard1}
            height={430}
            width={305}
            className="mx-auto"
          />
        </SwiperSlide>
        <SwiperSlide className="text-center pt-2">
          Come back to check the results and see your earnings
          <ImageWithFallback
            src={onboard2}
            height={430}
            width={305}
            className="mx-auto"
          />
        </SwiperSlide>
        <SwiperSlide className="text-center pt-2">
          Maintain your streak by playing on consecutive featured match days
          <ImageWithFallback
            src={onboard3}
            alt=""
            height={430}
            width={305}
            className="mx-auto"
          />
        </SwiperSlide>
      </Swiper>
      <div className="absolute w-full bottom-0 top-3/4">
        <div className="flex items-center justify-center">
          {[1, 2, 3].map((item, index) => (
            <div
              className={`h-2 w-2 m-2 ${
                index == indexSwiper ? "bg-blue-8" : "bg-blue-4"
              } rounded-full`}
            />
          ))}
        </div>
        <h2 className="text-center">Are you smarter than other cricket fans? Play the odds and find out</h2>
        <div className="flex items-center justify-center mt-4">
          <button
            onClick={() => setshowModal((prev) => !prev)}
            className="border-2 flex border-solid px-4 py-1 bg-basebg rounded-md border-green"
          >
              Play the odds
            <Image src="/pngsV2/path.png"  className="mt-1 ml-2" alt="" height={1} width={10} onClick={()=>setshowModal((prev)=>!prev)}/>
          
            
            
          </button>
        </div>
      </div>
    </>
  );
};
