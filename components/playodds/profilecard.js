import React from "react";
import ImageWithFallback from "../commom/Image";
const Playerfallback = "/pngsV2/playerPH.png";

export default function Profilecard({ profileinfo, handleclick }) {
  console.info(profileinfo, "whats the info???????????????");
  return (
    <div>
      <div className="flex relative bg-gray mx-4 lg:mt-6 mt-14">
        <div className="border-r-solid border-r-2 border-r-gray-2 my-3">
          <div className="w-24 h-24 ml-8 rounded-full overflow-hidden m-4 object-cover object-top">
            <ImageWithFallback
              className="absolute h-6 w-7 border-solid ml-16 my-16"
              src="/pngsV2/editpencil.png"
              // onError={(evt) => (evt.target.src = Playerfallback)}
              fallbackSrc={Playerfallback}
              onClick={handleclick}
              height={18}
              width={22}
            />
            <ImageWithFallback
              className="rounded-full w-full lg:w-32 md:w-32"
              width={150}
              height={150}
              src={profileinfo?.photo}
              alt={Playerfallback}
              // onError={(evt) => (evt.target.src = Playerfallback)}
              fallbackSrc={Playerfallback}
            />
          </div>
        </div>
        <div className="flex items-center justify-between">
          <span className="text-xl text-white p-6">
            {profileinfo?.name}
            <p className="items-start justify-start text-lg">
              Level -
              <span className="text-green pl-1">{profileinfo?.level}</span>
            </p>
            <div
              className="items-start text-sm flex  justify-start pt-3"
              style={{ color: "#8C98B0" }}
            >
              Total Earnings-
              <span className="text-green pl-1 flex gap-1">
                <ImageWithFallback src="/pngsV2/coin.png" className="h-5 w-5" width={2} height={2}/>
                {profileinfo?.totalEarnings}
              </span>
            </div>
            <p
              className="items-start text-sm justify-start"
              style={{ color: "#8C98B0" }}
            >
              Contest Entered -
              <span className="text-green pl-1">
                {profileinfo?.contestEntered}
              </span>
            </p>
          </span>
        </div>
        <div />
        <div className="absolute bottom-0"></div>
      </div>
    </div>
  );
}
