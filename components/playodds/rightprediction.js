import React from "react";
import Predictionbar from "./predictionDetails";

export default function Rightprediction({
  predictionDetails,
  props,
  profiledata,
}) {
  return (
    <div className="pt-3">
      <div className="bg-gray-6 lg:mt-3 mt-28 flex border-2 border-gray border-solid rounded-md py-4 px-2 m-2 flex-col">
        <div className="flex items-center justify-between pb-1">
          <div>
            {profiledata?.predictionProfile?.matches.map((item) => (
              <>
                {item?.matchDetails?.matchID === props?.matchID && (
                  <span className="text-xs text-white">
                    {item?.matchDetails?.totalParticipants || 0}
                  </span>
                )}
                {item?.matchDetails?.matchID === props?.matchID && (
                  <span className="text-xs text-white"> Participants </span>
                )}
              </>
            ))}
          </div>

          <h2 className="text-white text-xs uppercase">
            MY RANK{" "}
            <span className="text-lg font-bold pl-1">
              # {predictionDetails?.rank}
            </span>
          </h2>
        </div>
        <hr className="border-gray-2 w-full" />

        <Predictionbar matchID={props?.matchID} isContestSummary={true}/>
      </div>
    </div>
  );
}
