import React from "react";
import { useState } from "react";
import Tooltip from "../commom/tooltip";
import Fireicon from "./Fireicon";
import Matchespopup from "./matchespopup";


export default function Streakcard({ streakInfo }) {
  const [showthePopup,setshowthePopup]=useState(false)
  console.log(streakInfo,'streakkkkkkkkkkkkkkkkkkkkkkkkkk');
  return (
    <div>


      {/* < data /> */}
      <div className="flex relative bg-gray flex-col mx-4 my-4 p-4 h-auto">
        <div className="flex justify-between">
          <span className="text-white">Streak - {streakInfo?.streak}</span>
          <h4 className="text-green">
            Highest Streak - {streakInfo?.heighestStreak}
          </h4>
        </div>
        <hr className="w-3/4 absolute top-[70%] left-[12%] lg:left-[14%] lg:w-[73%] m-auto border-gray-8 border-1"/>

        
        <div className="flex-row flex justify-evenly" onClick={() => setshowthePopup(!showthePopup)}>
          {(streakInfo?.matchesData || Array.from(Array(7).keys()))?.map((items, index) => (
            <Tooltip className="absolute text-white">
              <div className="w-8 h-8 mt-4 rounded-full border-2 border-solid border-gray z-30 bg-basebg">
                {streakInfo?.continousStreak > index && (
                  <Fireicon className="relative top-[10%] m-auto flex justify-center items-center"/>
                )}
              </div>
            </Tooltip>
          ))}
        </div>
      <Matchespopup showMatches={showthePopup} setshowMatches={()=>setshowthePopup(!showthePopup)} streakData={streakInfo?.matchesData}/>

      </div>
    </div>
  );
}
