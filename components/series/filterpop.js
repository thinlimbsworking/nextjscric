import React, { useEffect, useState } from 'react';
import CleverTap from 'clevertap-react';
const RightSchevronBlack = '/svgs/RightSchevronWhite.svg';

const FilterPopUp = (props) => {




  // useEffect(() => {

    

    
  //     return()=>{
        
  //     }
  //   },[])


  const [localCountryName,setLocalCountryName]=useState(props.countryName)
  const [format1, setFormat1] = useState(true);
  const [formatTypeLocal,setFormatTypeLocal]=useState(props.formatType)
  const[changHandle,setHandleChange]=useState(false)
  const [formatClick,setFormtClick]=useState(false)
  const [countryClick,setCountryclick]=useState(false)


  useEffect(() => {}, [format1, []]);

  const teamList = [
    { id: '4', name: 'IND' },
    { id: '1', name: 'AUS' },
    { id: '5', name: 'NZ' },
    { id: '3', name: 'ENG' },
    { id: '7', name: 'SA' },
    { id: '6', name: 'PAK' },
    { id: '2', name: 'BAN' },
    { id: '8', name: 'SL' },
    { id: '9', name: 'WI' }
  ];
  let teamFlag = [
    { sortName: 'nz', id: 5 },
    { sortName: 'ind', id: 5 },
    { sortName: 'aus', id: 5 },
    { sortName: 'nz', id: 5 },
    { sortName: 'nz', id: 5 },
    { sortName: 'nz', id: 5 },
    { sortName: 'nz', id: 5 },
    { sortName: 'nz', id: 5 },
    { sortName: 'nz', id: 5 }
  ];

  const handleCounty = (name) => {
    setCountryclick(true)
    setHandleChange(true)
    const index = localCountryName.indexOf(name);

    // countryName, setCountryName
    if (localCountryName.length == 0) {
      let ar = [];
      ar.push(name);
     
      setLocalCountryName(ar)
      setFormat1(!format1);
      return;
    }

    if (localCountryName.length > 0 && index < 0) {
     
    

      let ar = localCountryName;
      ar.push(name);

      // props.setCountryName(ar);
      setLocalCountryName(ar)
    
      setFormat1(!format1);
      return;
    }
    if (localCountryName.length > 0 && index >= 0) {
      
    
      let ar = localCountryName;

      ar.splice(index, 1);
      // props.setCountryName(ar);
      setLocalCountryName(ar)
     
      setFormat1(!format1);

      return;
    }
  };

  const handleFormat = (name) => {
    if(!props.singleFormate){
    setFormtClick(true)
    setHandleChange(true)
    const index = formatTypeLocal.indexOf(name);

    if (formatTypeLocal.length == 0) {
      let ar = [];
      ar.push(name);
  
      // props.setFormatType(ar);
      setFormatTypeLocal(ar)
      setFormat1(!format1);
      return;
    }

    if (formatTypeLocal.length > 0 && index < 0) {
    

      let ar = formatTypeLocal;
      ar.push(name);

      // props.setFormatType(ar);
      setFormatTypeLocal(ar)

     
      setFormat1(!format1);
      return;
    }

    if (formatTypeLocal.length > 0 && index >= 0) {
     
      let ar = formatTypeLocal;

      ar.splice(index, 1);
      // props.setFormatType(ar);
      setFormatTypeLocal(ar)

    
      setFormat1(!format1);

      return;
    }
  } else {
    // props.setFormatType(name);
    // setFormat1(!format1);
    setHandleChange(true)
    setFormatTypeLocal(name)
  }
  };

  const handleClose=()=>{
if(countryClick)
{
    CleverTap.initialize('Filter', {
      Source: 'SeriesHome',
      Type:'Country',
    
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });

  }
if(formatClick)
{
    CleverTap.initialize('Filter', {
      Source: 'SeriesHome',
      Type:'Format',
     
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  }

    props.setFormatType(formatTypeLocal);
    props.setCountryName(localCountryName);
    props.setOpenFilter(false)
  }

  const verifyMatch = (item) => {
    if(!props.singleFormate)
      return formatTypeLocal.indexOf(item.name) >= 0 
    else  {
      return formatTypeLocal ? formatTypeLocal ===  item.name : false
    }
  }
  let format = props.formate ? props.formate : [{ name: 'ODI' }, { name: 'T20' }, { name: 'Tests' }];
  return (
    <div
      className='flex  justify-center items-center fixed top-0 left-0 right-0 bottom-0 z-40 bg-black-20 text-white mb-16 '
      style={{ backdropFilter: 'blur(10px)' }}>
      <div className='flex self-end md:self-center'>
        <div className='flex flex-col text-xs items-center relative bg-gray-8 '>
          <div className='flex justify-between items-center  w-full border-b border-b-black'>
            <div className=' flex  text-base font-bold p-2 '>FILTERS </div>
            <div onClick={() => props.setOpenFilter(false)} className=' flex justify-end items-end cursor-pointer text-xs text-gray-2 p-2'>
              <span></span>
              CLOSE
            </div>
          </div>

          <div className='flex w-full flex-col'>
            <div className=' text-xs p-2'> Country</div>
            <div className='flex w-full items-center justify-center  flex-wrap'>
              {teamList.map((item, index) => {
                return (
                  <div
                    className={`w-28 gap-2 bg-gray-4 p-2 m-2 flex items-center justify-center cursor-pointer rounded-md  ${
                      localCountryName.indexOf(item.name) >= 0 ? 'border-2 border-green-3' : ''
                    }`}
                    onClick={() => handleCounty(item.name)}>
                    <img
                      className='h-4 w15'
                      src={`https://images.cricket.com/teams/${item.id}_flag_safari.png`}
                      alt=''
                    />
                    <span className=' ml1 ttu f7 fw5'> {item.name}</span>
                  </div>
                );
              })}
            </div>
          </div>

          <div className='flex w-full flex-col mb-3'>
            <div className=' tetx-xs p-2'> Format</div>
            <div className='flex w-full items-center'>
              {format.map((item, index) => {
              
                return (
                  <div
                    className={`w-28 bg-gray-4 p-2 m-2 flex items-center justify-center rounded-md cursor-pointer  ${
                      verifyMatch(item) ? 'border-2 border-green-3' : ''
                    }`}
                    onClick={() => handleFormat(item.name)}>
                  
                    <span className=' text-xs '> {item.name.toUpperCase()}</span>
                  </div>
                );
              })}
            </div>
            <div className='flex  w-full items-center  justify-center'>
              <div
                onClick={() =>handleClose()}
                className={`w-20 p-2 mt-2 rounded-full text-xs  bg-gray-8 flex   items-center  justify-center border-2 border-green-3 ${
                  (localCountryName.length || formatTypeLocal.length) ? ' text-white' : 'text-gray-2'
                } `}>
                DONE
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FilterPopUp;



// import React, { useEffect, useState } from 'react';
// import CleverTap from 'clevertap-react';
// const RightSchevronBlack = '/svgs/RightSchevronWhite.svg';

// const FilterPopUp = (props) => {


//   const handleCleverTap = (typeName) => {
//     CleverTap.initialize('Filter', {
//       Source: 'SeriesHome',
//       // SeriesID: match.seriesID,
//       type:typeName,
//       Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
//     });
//   };
//   const [format1, setFormat1] = useState(true);

//   useEffect(() => {
//   }
    
//     , [format1, []]);

//   const teamList = [
//     { id: '4', name: 'IND' },
//     { id: '1', name: 'AUS' },
//     { id: '5', name: 'NZ' },
//     { id: '3', name: 'ENG' },
//     { id: '7', name: 'SA' },
//     { id: '6', name: 'PAK' },
//     { id: '2', name: 'BAN' },
//     { id: '8', name: 'SL' },
//     { id: '9', name: 'WI' }
//   ];
//   console.log('propspropspropspropsprops', props);
//   let teamFlag = [
//     { sortName: 'nz', id: 5 },
//     { sortName: 'ind', id: 5 },
//     { sortName: 'aus', id: 5 },
//     { sortName: 'nz', id: 5 },
//     { sortName: 'nz', id: 5 },
//     { sortName: 'nz', id: 5 },
//     { sortName: 'nz', id: 5 },
//     { sortName: 'nz', id: 5 },
//     { sortName: 'nz', id: 5 }
//   ];

//   const handleCounty = (name) => {
//     // props.setFilterCall(false)
//     const index = props.countryName.indexOf(name);

//     // countryName, setCountryName
//     if (props.countryName.length == 0) {
//       let ar = [];
//       ar.push(name);
//       console.log('array', ar);
//       props.setCountryName(ar);
//       setFormat1(!format1);
//       return;
//     }

//     if (props.countryName.length > 0 && index < 0) {
//       console.log('push', name);

//       let ar = props.countryName;
//       ar.push(name);

//       props.setCountryName(ar);
//       console.log('pushh', props.countryName);
//       setFormat1(!format1);
//       return;
//     }
//     if (props.countryName.length > 0 && index >= 0) {
//       console.log('pop', name);
//       let ar = props.countryName;

//       ar.splice(index, 1);
//       props.setCountryName(ar);
//       console.log('popp', props.formatType);
//       setFormat1(!format1);

//       return;
//     }
//   };

//   const handleFormat = (name) => {
//     // props.setFilterCall(false)
//     const index = props.formatType.indexOf(name);
//     console.log('ssssss', index, name);

//     if (props.formatType.length == 0) {
//       let ar = [];
//       ar.push(name);
//       console.log('array', ar);
//       props.setFormatType(ar);
//       setFormat1(!format1);
//       return;
//     }

//     if (props.formatType.length > 0 && index < 0) {
//       console.log('push', name);

//       let ar = props.formatType;
//       ar.push(name);

//       props.setFormatType(ar);
//       console.log('pushh', props.formatType);
//       setFormat1(!format1);
//       return;
//     }

//     if (props.formatType.length > 0 && index >= 0) {
//       console.log('pop', name);
//       let ar = props.formatType;

//       ar.splice(index, 1);
//       props.setFormatType(ar);
//       console.log('popp', props.formatType);
//       setFormat1(!format1);

//       return;
//     }
//   };

//   let format = [{ name: 'ODI' }, { name: 'T20' }, { name: 'TEST' }];
//   return (
//     <div
//       className='flex  justify-center items-center fixed absolute--fill z-max bg-black-20'
//       style={{ backdropFilter: 'blur(10px)' }}>
//       <div className='flex self-end self-center-l'>
//         <div className='flex flex-col fw6 items-center relative bg-white '>
//           <div className='flex  w-full bb b--light-gray'>
//             <div className=' flex w-80  f6 fw5  pa2 '>FILTERS </div>
//             <div onClick={() => props.setOpenFilter(false)} className=' flex justify-end items-end   f6 fw5 gray pa2'>
//               <span></span>
//               CLOSE
//             </div>
//           </div>

//           <div className='flex w-full flex-col'>
//             <div className=' f6 fw5  pa2'> Country</div>
//             <div className='flex w-full items-center justify-center  flex-wrap'>
//               {teamList.map((item, index) => {
//                 return (
//                   <div
//                     className={`w-25  bg-light-gray pa2 ma2 flex items-center justify-center br-pill  ${
//                       props.countryName.indexOf(item.name) >= 0 ? 'ba b--red' : ''
//                     }`}
//                     onClick={() => handleCounty(item.name)}>
//                     <img
//                       className='h1 w15'
//                       src={`https://images.cricket.com/teams/${item.id}_flag_safari.png`}
//                       alt=''
//                     />
//                     <span className=' ml1 ttu f7 fw5'> {item.name}</span>
//                   </div>
//                 );
//               })}
//             </div>
//           </div>

//           <div className='flex w-full flex-col mb3'>
//             <div className=' f6 fw5  pa2'> Format</div>
//             <div className='flex w-full items-center justify-center  flex-wrap'>
//               {format.map((item, index) => {
//                 return (
//                   <div
//                     className={`w-25  bg-light-gray pa2 ma2 flex items-center justify-center br-pill  ${
//                       props.formatType.indexOf(item.name) >= 0 ? 'ba b--red' : ''
//                     }`}
//                     onClick={() => handleFormat(item.name)}>
//                     {console.log(' props.formatType.indexOf(item.name)', props.formatType.indexOf(item.name))}
//                     <span className='black ml1 ttu f7 fw5'> {item.name}</span>
//                   </div>
//                 );
//               })}
//             </div>
//             <div className='flex  w-full items-center  justify-center'>
//               <div
//                 onClick={() => props.setOpenFilter(false)}
//                 className={`w-20 fw5 lh-copy  ma2 br-pill flex   items-center  justify-center ${
//                   props.formatType.length > 0 || props.countryName.length > 0 ? '0-100 filter' : 'o-50 bg-red_10'
//                 }    white bg-light-gray pa2    f8  `}>
//                 DONE
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default FilterPopUp;
