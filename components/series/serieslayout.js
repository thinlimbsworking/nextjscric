import React, { useState, useEffect } from 'react';
import CleverTap from 'clevertap-react';
import axios from 'axios';
import Link from 'next/link';
import {
  getSeries,
  getSeriesTabUrl,
  getSeriesNew,
  getTabUrlSeries,
  getFilterTabUrlSeries,
  getFilterTabUrlSeriesHome
} from '../../api/services';
import { SERIES_AXIOS, SERIES_PRIORITY_AXIOS, SERIES_PRIORITY } from '../../api/queries';
import Loading from '../../components/Loading';
import { SERIES } from '../../constant/MetaDescriptions';
//import Urltracking from '../../components/Common/urltracking';
import { useQuery } from '@apollo/react-hooks';
import { format } from 'date-fns';
import DataNotFound from '../commom/datanotfound';
import { useRouter } from 'next/router';
//import ErrorBoundary, { useErrorHandler } from '../../components/ErrorBoundary';
import MetaDescriptor from '../../components/MetaDescriptor';
import FilterPopUp from '../../components/series/filterpop';
/**
 * @description series listing with multiple tab view ex - international,domestic
 * @route /series/seriesTab
 * @param { Object } props
 * clearfliter.svg
 */
export default function Series(props) {
  const handleShare = () => {
    if (window.navigator.share !== undefined) {
      window.navigator.share({
        title: `Hey, check out the upcoming series on cricket.com: ${window.location.href} | Download Now: https://cricketapp.app.link/downloadapp`,
        text: `Hey, check out the upcoming series on cricket.com: ${window.location.href} | Download Now: https://cricketapp.app.link/downloadapp`
      });
    }
  };

  let router = useRouter();
  let page = 0;
  const [tab, setTab] = useState(props?.tab?.toLowerCase());
  const [currType, setCurrType] = useState('International')
  const [openFIlter, setOpenFilter] = useState(false);
  const [formatType, setFormatType] = useState([]);
  const [countryName, setCountryName] = useState([]);
  const [filterCall, setFilterCall] = useState(true);

  const tabs = ['upcoming', 'ongoing', 'completed'];

  const types = ['International','Domestic','T20League','Women'];

  const monthsArr = [
    { January: 'jan' },
    { February: 'Feb' },
    { March: 'Mar' },
    { April: 'Apr' },
    { May: 'May' },
    { Jun: 'Jun' },
    { July: 'Jul' },
    { August: 'Aug' },
    { September: 'Sept' },
    { October: 'Oct' },
    { November: 'Nov' },
    { December: 'Dec' }
  ];

  const [currentMonth, setCurrentMonth] = useState(new Date().getMonth());
  const [pagination, setPagination] = useState(1);

  const [refresh, setRefresh] = useState(false);

  let [filterTAB, setfilterTAB] = useState(router.query.slugs[1]);

  var [currentTab, filterTab] = router.query.slugs;

  let apiTab = currentTab == 'ongoing' ? 'live' : currentTab;

  // let page = router.query.page ? router.query.page : 0;

  // alert(filterCall)
  var { loading, error, data, fetchMore=function(){} } = useQuery(SERIES_PRIORITY, {
    variables: {
      tab: apiTab,
      filter: router.query.slugs[1] == 'women' ? 'womens' : router.query.slugs[1],
      country: countryName.toString(),
      format: formatType.toString() == 'T100' ? 'T100' : formatType.toString().toLocaleLowerCase(),
      page: page
    },
    fetchPolicy: 'network-only'
  });

  if (loading)
    if (data && !data.seriesPriority) {
      console.log('');
    }
  useEffect(() => {
    setRefresh(true);
    console.log('currtype = ',currType);
  }, [filterTAB]);

  const ScrollToCurrentMonth = () => {
    var elem = document.getElementById('active');
    elem.scrollIntoView();
  };

  const handleSeriesNavigation = (match) => {
    CleverTap.initialize('Series', {
      Source: 'SeriesHome',
      SeriesID: match.seriesID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };
  const timeConvert = (timestamp) => {
    // var xx = new Date();
    // return  xx.setTime(timestamp*1000)

    var newDate = new Date();
    newDate.setTime(timestamp);
    let dateString = newDate.getDate();

    return dateString;
  };

  const MonthData = (team) => {
    switch (team) {
      case 'January':
        return 'Jan';
      case 'February':
        return 'Feb';
      case 'March':
        return 'Mar';
      case 'April':
        return 'Apr';
      case 'May':
        return 'May';
      case 'Jun':
        return 'Jun';
      case 'July':
        return 'Jul';
      case 'August':
        return 'Aug';
      case 'September':
        return 'Sept';
      case 'October':
        return 'Oct';
      case 'November':
        return 'NOV';
      default:
        return 'Dec';
    }
  };

  const borderColor = (seriesName) => {
    let team = seriesName.split(' ')[0];
    switch (team) {
      case 'England':
        return 'b--black-90';
      case 'India':
        return 'b--blue';
      case 'Sri':
        return 'b--dark-green';
      case 'Afghanistan':
        return 'b--dark-green';
      case 'Pakistan':
        return 'b--dark-green';
      case 'South':
        return 'b--dark-green';
      case 'Australia':
        return 'b--yellow';
      case "Women's":
        return 'b--dark-pink';
      case 'New':
        return 'b--black-90';
      case 'West':
        return 'b--dark-red';
      case 'ICC':
        return 'b--blue';
      default:
        return 'b--black-50';
    }
  };

  let borderColorTeam = {
    all: 'b--inter',
    international: 'b--inter',
    domestic: 'completed'
  };
  const handleScroll = async (evt) => {
    if (evt.target.scrollTop + evt.target.clientHeight >= evt.target.scrollHeight) {
      setPagination((prevPage) => prevPage + 1);
      handlePagination(fetchMore);
    }
  };

  const handlePagination = (fetchMore) => {
    fetchMore({
      variables: {
        tab: apiTab,
        filter: router.query.slugs[1] == 'women' ? 'womens' : router.query.slugs[1],
        country: countryName.toString(),
        format: formatType.toString() == 'T100' ? 'T100' : formatType.toString().toLocaleLowerCase(),

        page: pagination
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult;
        }

        let Priority = Object.assign({}, previousResult, {
          seriesPriority: [...previousResult.seriesPriority, ...fetchMoreResult.seriesPriority]
        });

        return { ...Priority };
      }
    });
  };

  return (
    <div className=' mx-auto text-white'>
      <MetaDescriptor
        section={SERIES({
          tabInfo: ''
        })}
      />
      {openFIlter && (
        <FilterPopUp
          filterCall={filterCall}
          setFilterCall={setFilterCall}
          setOpenFilter={setOpenFilter}
          formatType={formatType}
          setFormatType={setFormatType}
          countryName={countryName}
          setCountryName={setCountryName}
        />
      )}

      {/* {props.urlTrack ? <Urltracking PageName='Series' /> : null} */}

      <div className='fixed w-full bg-gray-8 md:static'>
        <div className='flex items-center justify-between p-2 md:hidden lg:hidden'>
          <div className='flex  gap-2 items-center justify-start'>
            <div className='p-2 rounded-md bg-gray-4'>
              <img className=' ' onClick={() => window.history.back()} src='/svgs/backIconWhite.svg' alt='back icon' />
            </div>
            <div className='text-base font-bold'>Series</div>
          </div>
        <div className='p-2 rounded-md bg-gray-4'>
          <img className=' w-5 h-5  cursor-pointer' src='/svgs/share-line.png' onClick={() => handleShare()} alt='Share' />
        </div>
        </div>


        <div className='flex justify-between items-center '>
        {formatType.length == 0 && countryName.length == 0 &&  types.map((type) => (
              
              <Link
                {...getFilterTabUrlSeries(currentTab, type.toLowerCase())}
                replace
                passHref
                legacyBehavior>
                  <div onClick={e => setCurrType(type)}
                    className={` border-b-2 w-1/4 text-center border-b-gray-2 text-xs p-2 text-gray-2 cursor-pointer ${
                     type === currType ? 'text-blue-8  border-b-blue-6' : ''
                    } `}>
                    {type}
                  </div>
              </Link>
            )) }
        </div>
      </div>

      <div className='  hidescroll '>
        {/* only desktop */}

        
        
        

        {/* M WEB NAV START */}
        <div className=''>
          <div
            className={
              formatType.length == 0 && countryName.length == 0
                ? 'flex items-center justify-center w-full  md:hidden lg:hidden  '
                : 'flex items-center justify-end w-full  md:hidden lg:hidden '
            }>
            






              
            

            {(formatType.length != 0 || countryName.length != 0) && (
              <div className=' w-35  flex justify-center items-center '>
                <div className='flex  bg-gray rounded-md  '>
                  <div className='flex justify-center items-center'>
                    <img
                      className=''
                      // onClick={() => window.history.back()}
                      src='/svgs/clearfliter.svg'
                      alt='back icon'
                    />
                  </div>
                  <div
                    className='flex ml-1 justify-center items-center text-base font-bold bg-red-2 '
                    onClick={() => (setFormatType([]), setCountryName([]))}>
                    {' '}
                    CLEAR FILTER
                  </div>
                </div>
              </div>
            )}
          </div>
        <div className={`w-min flex  p-1  mx-auto justify-between items-center  gap-2 bg-gray-4 rounded-full md:bg-gray-8 lg:bg-gray-8  md:gap-16 lg:gap-8 md:mt-4 lg:mt-4 ${!!(countryName.length || formatType.length) ? 'mt-12' : 'mt-24'}`}>
          {tabs.map((tabName, i) => (
            
            (<Link
              key={i}
              href = {`/series/${tabName}/international`}
              replace
              passHref
              className={` text-xs font-bold rounded-full text-center p-2 cursor-pointer ${
                (props.tab.toLowerCase() == 'live' ? 'ongoing' : props.tab.toLowerCase()) === tabName.toLowerCase()
                  ? 'bg-gray-8 border-2 border-green-3'
                  : null
              }`}
              onClick={() => {setTab(tabName);setCurrType('International')}}>

              {tabName.toUpperCase()}

            </Link>)
          ))}
        </div>
        </div>
        
        
      </div>
      

      {error ? (
        <div className='w-full h-100 text-base font-bold gray flex flex-col  justify-center items-center'>
          <DataNotFound />
        </div>
      ) : loading ? (
        <Loading />
      ) : (
        <div 
          className='w-full mt-2 max-h-[66vh] min-h-[66vh] md:max-h-[58vh] md:min-h-[58vh] lg:max-h-[58vh] lg:min-h-[58vh] overflow-y-scroll hidescroll border-b-2 border-b-gray-4 p-2 md:mb-24 lg:mb-16 md:border-none lg:border-none'
          onScroll={(evt) => handleScroll(evt)}>
          {/* <div className='flex w-96 center   justify-center items-center flex-col flex-wrap-l  flex-row-l p-1 md:w-full'> */}
            {data && data.seriesPriority.length > 0 ? (
              data.seriesPriority.map((item, i) => {
                return (
                  <Link {...getSeriesNew(item)} key={i} passHref legacyBehavior>
                    <div
                      className={` flex h-30 m-2 bg-gray-4 rounded-md`}>
                      <div className='w-full h-24 py-2  flex justify-between items-center  flex-row'>
                        <div
                          className={`w-1/5 flex flex-col text-xs text-white font-bold justify-center  p-2  `}>
                          <div className='w-40 center '>
                            {' '}
                            {format(Number(item.seriesStartDate) - 19800000, 'MMM do')}{' '}
                          </div>
                          <div className='text-gray-2 w-40 center'> To</div>
                          <div className='w-40 center'>
                            {' '}
                            {format(Number(item.seriesEndDate) - 19800000, 'MMM do')}{' '}
                          </div>
                        </div>
                        <div className='border-r-2 border-r-gray-2 h-16 ml-4'></div>
                        <div className='w-4/5  p-2 '>
                          <div className=' text-xs font-bold '> {item.tourName} </div>
                          <div className='w-full py-2  mt1 flex flex-wrap items-end justify-start'>
                            <div className='flex text-xs  gap-2'>
                              {item.Testcount && (
                                <div className='flex rounded-full px-2 py-1 border-2 border-blue-8 bg-gray-8'>
                                  <span className='font-bold'>
                                    {item.Testcount}
                                  </span>
                                  <span className='ml-1'>
                                    Test
                                  </span>
                                </div>
                              )}{' '}
                              {item.Odicount && (
                               <div className='flex rounded-full px-2 py-1 border-2 border-red-9 bg-gray-8'>
                               <span className='font-bold'>
                                 {item.Odicount}
                               </span>
                               <span className='ml-1'>
                                 ODIs
                               </span>
                             </div>
                              )}
                              {item.T20count && (
                                <div className='flex rounded-full px-2 py-1 border-2 border-orange-5 bg-gray-8'>
                                <span className='font-bold'>
                                  {item.T20count}
                                </span>
                                <span className='ml-1'>
                                  T20s
                                </span>
                              </div>
                              )}
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </Link>
                );
              })
            ) : (
              <div className='bg-gray w-full mt2  p-4'>
                <img className='h-4 w-5  ' src='/pngsV2/datanotfound.png' alt='loading...' />
                <div className='f5 fw5 text-centerwhite pt-2'>No Series Found</div>
              </div>
            )}
          {/* </div> */}

          {/* {false &&
            data &&
            data.listseries.map((series, i) => (
              <div
                key={i}
                className={`${i === 0 ? 'pt4 pt0-l' : ''} `}
                id={`${series.month.indexOf(monthsArr[currentMonth]) > -1 ? 'active' : ''}`}>
                <div key={i} className=''>
                  <div className='ph-2 pv1 mv2-ns f6 fw5 mb2 tl center '> {series.month} </div>

                  <div className='center w-full flex flex-wrap items-center'>
                    {series.series.map((match, i) => (
                      <Link {...getSeries(match)} key={i} passHref>
                        <a
                          key={i}
                          onClick={() => handleSeriesNavigation(match)}
                          className={`cursor-pointer br3 bl bw3 ${borderColor(
                            match.seriesName
                          )} mv2 w-full w-50-ns pr3-ns mb3-ns`}>
                          <div className='flex items-center bg-gray br3 b--right '>
                            <div className='ml3 py-3 h-30 w-60'>
                              <span className='f6  fw6 w-full'>{match.seriesName}</span>
                              <div className='pt2 flex items-center'>
                                {match.Odicount ? (
                                  <div className='flex items-center mr1'>
                                    <span className='fw6 bigBalls f7 bg-gray-2 white'>{`${match.Odicount}`} </span>
                                    <span className='py-3  f7 fw5 fw4 mh2  '> ODIs </span>
                                  </div>
                                ) : (
                                  ''
                                )}
                                {match.T20count ? (
                                  <div className='flex items-center mr1'>
                                    <span className='fw6 bigBalls f7 bg-gray-2 white'>{`${match.T20count} `} </span>
                                    <span className='py-3  f7 fw5 fw4 mh2  '> T20s </span>
                                  </div>
                                ) : (
                                  ''
                                )}
                                {match.Testcount ? (
                                  <div className='flex items-center mr1'>
                                    <span className='fw6 bigBalls f7 bg-gray-2 white'>{`${match.Testcount} `}</span>
                                    <span className='py-3  f7 fw5 fw4 mh2 '> Tests </span>
                                  </div>
                                ) : (
                                  ''
                                )}
                              </div>
                            </div>

                            <div className='w-40  mr3-ns mr2 flex justify-center items-center text-centerbr-pill ba b--gray'>
                              <span className=' pv1  br4 f8 navy '>{match.startEndDate} </span>
                            </div>
                          </div>
                        </a>
                      </Link>
                    ))}
                  </div>
                </div>
              </div>
            ))} */}

         
        
        </div>
        
      )} 
     
      <div className='flex w-full items-center justify-center mt-4 gap-2  '> 
            { !!(data && data.seriesPriority && data?.seriesPriority.length) && <div
              onClick={() => setOpenFilter(true)}
              className='w-32 cursor-pointer flex items-center justify-center gap-2 bg-gray-8 border-2 border-green py-2 px-3 rounded-md bottom-0    '>
              <img className='' src='/svgs/filter.svg' alt='user' /> <span className='text-xs font-bold'>FILTER</span>
            </div> }
            {
              !!(countryName.length || formatType.length) && <span className='cursor-pointer font-semibold text-sm text-white border-b-2 border-white'
              onClick={() => {setCountryName([]); setFormatType([])}}>
             Clear all
            </span>
            }
          </div> 
    </div>
  );
}
