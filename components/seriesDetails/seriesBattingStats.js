import React, { useState, useEffect } from 'react';
//import { useQuery } from '@apollo/react-hooks';

//import { navigate } from '@reach/router';
// import { TOP_GUNS } from '../../api/queries'
import { seriesViewStatBest } from '../../api/services';
import { useRouter } from 'next/router';
import CleverTap from 'clevertap-react';

import  backIconBlack from '../../public/svgs/backIconwhite.svg';
const RightSchevronBlack = '/svgs/RightSchevronBlack.svg';
const fallback = '/pngsV2/playerPH.png';
export default function SeriesBattingStats(props) {
  const { type, format, data, seriesID, slug, seriesName, seriesType } = props;

  const [value, setValue] = useState({});
  const [open, setOpen] = useState(false);
  const router = useRouter();
  

  let cricketType = type === 'Fielding' ? 'fielding' : type;

  const handleViewNavigation = (types) => {
    CleverTap.initialize('SeriesStats', {
      Source: `Series${cricketType}Stats`,
      StatsType: types.toLowerCase(),
      Platform: localStorage.Platform
    });

    let sendData = { seriesType, seriesID, slug, type, types, format };

  
    router.push(seriesViewStatBest(sendData).href, seriesViewStatBest(sendData).as);

   
  };

  const text = {
    Highest_Score: 'HIGHEST SCORE',
    Most_Runs: 'MOST RUNS',
    Most_Fifties: 'MOST FIFTIES',
    Most_Hundreds: 'MOST HUNDREDS',
    Most_4s: 'MOST 4S',
    Most_6s: 'MOST 6S',
    Best_Strike_Rate: 'BEST STRIKE RATE',
    Best_Batting_Average: 'BEST BATTING AVERGAE',
    Most_Wickets: 'MOST WICKETS',
    Best_figures: 'BEST FIGURES',
    Five_Wicket_Haul: '5 WICKETS HAUL',
    Three_Wicket_Haul: '3 WICKETS HAUL',
    Best_Bowling_Strike_Rate: 'BEST BOWLING STRIKE RATE',
    Best_Bowling_Average: 'BEST BOWLING AVERAGE',
    Best_Economy_Rates: 'BEST ECONOMY RATES',
    Most_Catches: 'MOST CATCHES',
    Most_Run_Outs: 'MOST RUN OUTS',
    Most_Dismissals: 'MOST DISMISSALS'
  };

  const scores = {
    Highest_Score: 'highest_score',
    Most_Runs: 'runs_scored',
    Most_Fifties: 'fifties',
    Most_Hundreds: 'hundred',
    Most_4s: 'fours',
    Most_6s: 'sixes',
    Best_Strike_Rate: 'batting_strike_rate',
    Best_Batting_Average: 'average',
    Most_Wickets: 'wickets',
    Best_figures: 'best_bowling_figures',
    Five_Wicket_Haul: 'five_wickets_haul',
    Three_Wicket_Haul: 'three_wickets_haul',
    Best_Bowling_Strike_Rate: 'bowling_strike_rate',
    Best_Bowling_Average: 'best_bowling_average',
    Best_Economy_Rates: 'economy',
    Most_Catches: 'most_catches',
    Most_Run_Outs: 'most_run_outs',
    Most_Dismissals: 'most_dismissals'
  };

  return (
    <div className=' pb2 mw75-l center text-white'>
      <div className=' flex justify-between dn-l items-center'>
        <div className='w2-5 h2-4 flex justify-center items-center cursor-pointer'>
          <img className=' dn-ns cursor-pointer' onClick={() => window.history.back()} src={backIconBlack} alt='' />
        </div>
        <span className='f5 fw5  mv0 flex-auto white pl2 truncate'>{`Series ${type} Stats`}</span>
      </div>
      <h1 className='f5 fw6 mv0 flex-auto pt3 dn db-l pa2 truncate'>{`Series ${type} Stats`}</h1>

      <div className='ph1 flex flex-wrap'>
        {/* mobiel */}

        {data &&
          data.getStatsResolver[format] &&
          data.getStatsResolver[format][cricketType] &&
          Object.keys(data.getStatsResolver[format][cricketType]).map(
            (types, i) =>
              data.getStatsResolver[format][cricketType][types] &&
              data.getStatsResolver[format][cricketType][types].length > 0 && (
                <div key={i} className={` ${i === 0 || i === 3 || i === 6 ? 'w-100  w-50-ns ph1' : ' ph1  w-50'}`}>
                  <div
                    className='bg-gray-4 mt2 mt3-ns  br2 shadow-4'
                    onClick={() => {
                      handleViewNavigation(types);
                    }}>
                    <div className='flex items-center justify-between p-2'>
                      <h2 className='fw6 f6'>{text[types]}</h2>
                      <div>
                        <img src={RightSchevronBlack} alt=''></img>
                      </div>
                    </div>

                    <div className='bg-black/80 h-[1px]'></div>
                    <div
                      className={`  ${i === 0 || i === 3 || i === 6
                        ? 'flex  ph2 pt2  flex-row justify-center  items-center'
                        : 'pt2-ns flex pv2 pb0-ns ph2-ns flex-row-ns justify-center flex-column items-center'
                        } `}>
                      <div className=' h4 overflow-hidden'>
                        <img
                          alt=''
                          className='h43'
                          src={`https://images.cricket.com/players/${data.getStatsResolver[format][cricketType][types][0].player_id}_headshot_safari.png`}
                          onError={(evt) => (evt.target.src = fallback)}></img>
                      </div>
                      <div className='pl2 pt1 pb1 pl0-ns'>
                        <div
                          className={`${i === 0 || i === 3 || i === 6
                            ? 'flex  items-baseline'
                            : 'items-baseline flex justify-center'
                            } `}>
                          <div className='darkRed  fw6 f4'>
                            {data.getStatsResolver[format][cricketType][types][0][scores[types]]}
                          </div>
                          <div className='darkRed fw2 pl1  f7'>
                            {types === 'Most_Wickets'
                              ? 'Wickets'
                              : types === 'Most_Runs'
                                ? 'Runs'
                                : types === 'Most_Fifties'
                                  ? 'Fifties'
                                  : types === 'Most_Hundreds'
                                    ? 'Hundreds'
                                    : types === 'Most_Catches'
                                      ? 'Catches'
                                      : types === 'Highest_Score'
                                        ? `(${data.getStatsResolver[format][cricketType][types][0].balls_faced})`
                                        : ''}
                          </div>
                        </div>
                        <div
                          className={` ${i === 0 || i === 3 || i === 6
                            ? ' items-baseline '
                            : 'flex-column flex-row-ns items-center justify-center '
                            } flex    `}>
                          <div
                            className={` ${i === 0 || i === 3 || i === 6 ? 'fw4 truncate f6 pr1  ' : 'tc pr1-ns fw4 f6'
                              }`}>
                            {data.getStatsResolver[format][cricketType][types][0].player_name}{' '}
                          </div>
                          <div className='text-gray-2 font-semibold tc f7'>
                            {data.getStatsResolver[format][cricketType][types][0].team_short_name}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
          )}
      </div>
    </div>
  );
}
