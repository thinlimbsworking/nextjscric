import React, { useState } from 'react';
// import { PLAYER_VIEW } from '../../api/services';
import { useRouter } from 'next/router';
import { PLAYER_VIEW } from '../../constant/Links';

// import downArrow from '../../public/svgs/downArrow.svg'
// const Empty = "/svgs/Empty.svg";
// import flagPlaceHolder from "`../../public/pngsV2/flag_dark.png";

export default function SeriesSquad({ browser, ...props }) {
  const router = useRouter();

  const [status, setStatus] = useState(
    props &&
      props.data &&
      props.data.getSeriesSquads &&
      props.data.getSeriesSquads.displayFormats.length > 0 &&
      props.data.getSeriesSquads.displayFormats[0]
  );
  const [card, setCard] = useState(
    props &&
      props.data &&
      props.data.getSeriesSquads &&
      props.data.getSeriesSquads.squadData.length > 0 &&
      props.data.getSeriesSquads.squadData.length - 1
  );
  const [format, setFormat] = useState('');
  const [index, setIndex] = useState(
    props && props.data && props.data.getSeriesSquads && props.data.getSeriesSquads.isNewSeries
      ? props.data.getSeriesSquads.squadData[props.data.getSeriesSquads.squadData.length - 1].squad.findIndex(
          (x) => x.matchType === props.data.getSeriesSquads.displayFormats[0]
        )
      : 0
  );
  let localStorage = {};
  let data = props.data;

  const images = {
    BATSMAN: '/pngsV2/bat-line-white.png',
    BOWLER: '/pngsV2/ball-line-white.png',
    KEEPER: '/pngsV2/keeper-line-white.png',
    ALL_ROUNDER: '/pngsV2/keeper-line-white.png',
    NAV_ARROW: '/svgs/RightSchevronWhite.svg'
  };
  // player/name/id
  const handlePlayer = (playerId, playerSlug) => {
    return {
      as: eval(PLAYER_VIEW.as),
      href: PLAYER_VIEW.href
    };
  };

  const handlePlayerNav = (playerID, playerName) => {
    router.push(
      handlePlayer(playerID, playerName.split(' ').join('-')).href,
      handlePlayer(playerID, playerName.split(' ').join('-')).as
    );
  };

  const fallbackProjection = '/pngsV2/playerPH.png';
  // if (error) return <div></div>;
  // if (loading)
  // 	return (
  // 		<div className='w-full vh-100 fw2 f7 gray flex justify-center items-center'>LOADING...</div>
  // 	);
  return (
    <>
      <div className='md:hidden lg:hidden'>
        {data.getSeriesSquads.isNewSeries &&
          data.getSeriesSquads.displayFormats.length > 1 &&
          data.getSeriesSquads.displayFormats.length !== data.getSeriesSquads.disableFormats.length && (
            <div className='flex items-center justify-center  '>
              <div className='w-min flex justify-center p-1 rounded-full mt-3 bg-gray-4'>
                {data.getSeriesSquads.displayFormats.map((tabName, i) => (
                  <span
                    key={i}
                    className={`w-24 text-sm font-semibold cursor-pointer px-2 py-1 text-center  rounded-full   ${
                      status === tabName ? 'bg-gray-8 border-2 border-green-3' : ''
                    }`}
                    onClick={() => {
                      data.getSeriesSquads.disableFormats.includes(tabName)
                        ? ''
                        : (setStatus(tabName),
                          data.getSeriesSquads.squadData.map((x, i) =>
                            x.squad.map((team, y) => (team.matchType === tabName ? setIndex(y) : ''))
                          ));
                    }}>
                    {tabName.toUpperCase()}
                  </span>
                ))}
              </div>
              
            </div>
          )}
          
        <div className=' m-2 '>
          {data && data.getSeriesSquads && data.getSeriesSquads.squadData.length > 0 ? (
            <div>
              {data.getSeriesSquads.squadData.map((team, i) => (
                <div key={i}>
                  <div
                    className={`bg-gray-4 mt-3 rounded-md flex justify-between p-3 cursor-pointer  ${
                      i === card ? 'rounded-none' : ''
                    } `}
                    onClick={() => setCard((prev) => (prev === i ? '' : i))}>
                    <div className='flex justify-start gap-2 items-center bg-gray-8 rounded-md p-2 w-5/6'>
                      <img
                        className='h-4 w-7'
                        src={`https://images.cricket.com/teams/${team.teamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = '/pngsV2/flag_dark.png';
                        }}
                        alt=''
                      />
                      <span className='text-xs font-bold'>{team.name}</span>
                    </div>
                    <div className='flex items-center p-2 rounded-md bg-gray-8'>
                      <img
                        className={`w-3 h-3 cursor-pointer text-white ${i === card ? 'rotate-90': ''}`}
                        src={images.NAV_ARROW}
                        alt={i === card ? '/pngsV2/uwarrow.png' : '/pngsV2/dwarrow.png'}
                      />
                    </div>
                  </div>
                  <div className={`p-2 bg-gray-4 ${card === i ? 'block ' : 'hidden'} `}>
                    {team.squad[index] ? (
                      team.squad[index].playersArray.map((player, i) => (
                        <div key={i} onClick={() => handlePlayerNav(player.playerID, player.playerName)}>
                          <div className='flex  justify-center items-center p-2 cursor-pointer border-b-2 border-b-gray-2'>
                            <div className='w-4/5 flex gap-2 items-center justify-start'>
                              <img
                                className='h-10 w-10 bg-gray-8 object-cover object-top  rounded-full'
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                                alt=''
                              />
                              <span className='text-xs font-semibold'>
                                {player.playerName}{' '}
                                <span className=''>
                                  {player.iswk && player.iscap
                                    ? '(c) & (Wk)'
                                    : player.iswk === true
                                    ? '(Wk)'
                                    : player.iscap === true
                                    ? '(c)'
                                    : null}
                                </span>{' '}
                              </span>
                            </div>
                            <div className='w-1/5 flex items-end justify-end '>
                              <div className=' rounded-md p-1 border-2 border-gray-8'>
                              <img className='w-5 h-5 ' src={images[player.playerRole]} alt='' />
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className='w-full p-3 bg-gray-4 shadow-4  flex justify-center item-center'>
                        <p className='gray text-base'>Squads not yet announced</p>
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <div className='flex flex-col justify-center items-center'>
              <div>
                <img className='pl-3 w-8 h-8' src='/svgs/Empty.svg' alt='' />
              </div>
              <div className='gray text-base'>No Squads found</div>
            </div>
          )}
        </div>
      </div>
      {/* this is for Web  */}
      <div className='hidden  md:block lg:block'>
        {data.getSeriesSquads.isNewSeries &&
          data.getSeriesSquads.displayFormats.length > 1 &&
          data.getSeriesSquads.displayFormats.length !== data.getSeriesSquads.disableFormats.length && (
            <div className='flex items-center justify-center text-white '>
              <div className='z-1  w-50 flex justify-center m-3 w-min rounded-full  gap-8'>
                {data.getSeriesSquads.displayFormats.map((tabName, i) => (
                  <span
                    key={i}
                    className={`cursor-pointer text-xs font-bold p-2 px-8 text-center rounded-full ${
                      status === tabName ? 'bg-gray-8 border-2 border-green-3' : ''
                    }`}
                    onClick={() => {
                      data.getSeriesSquads.disableFormats.includes(tabName)
                        ? ''
                        : (setStatus(tabName),
                          data.getSeriesSquads.squadData.map((x, i) =>
                            x.squad.map((team, y) => (team.matchType === tabName ? setIndex(y) : ''))
                          ));
                    }}>
                    {tabName.toUpperCase()}
                  </span>
                ))}
              </div>
             
            </div>
          )}
        <div className='bg-black/80 h-[1px]' />
        <div className=' bg-gray-4'>
          {data && data.getSeriesSquads && data.getSeriesSquads.squadData.length > 0 ? (
            <div className='flex w-full '>
              <div class='w-1/4 '>
                {data.getSeriesSquads.squadData.map((team, i) => (
                  <div
                    key={i}
                    className={`flex  gap-2 cursor-pointer ${card === i ? 'bg-gray-8 ' : ''}  p-2`}
                    onClick={() => setCard(i)}>
                    <div className='flex justify-center items-center'>
                      <img
                        className='h-5 w-8'
                        src={`https://images.cricket.com/teams/${team.teamID}_flag_safari.png`}
                        onError={(e) => {
                          e.target.src = '/pngsV2/flag_dark.png';
                        }}
                        alt=''
                      />
                    </div>
                    <div>
                      <span className=' text-sm font-semibold'>{team.name}</span>
                    </div>
                  </div>
                ))}
              </div>
              <div className='bg-black/80 w-[1px]'></div>
              <div className='w-3/4'>
                {data.getSeriesSquads.squadData
                  .filter((x, i) => i === card)
                  .map((team, i) => (
                    <div key={i} className=''>
                      <div className='  '>
                        {team.squad[index] ? (
                          team.squad[index].playersArray.map((player, i) => (
                            <div
                              key={i}
                              className=''
                              onClick={() => handlePlayerNav(player.playerID, player.playerName)}>
                              <div className='flex  justify-center items-center p-2 cursor-pointer'>
                                <div className='w-4/5 flex items-center justify-start gap-2'>
                                  <img
                                    className='h-12 w-12 bg-gray-8 object-cover object-top rounded-full'
                                    src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                    onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                                    alt=''
                                  />
                                  <span className='f6 text-sm'>
                                    {player.playerName}{' '}
                                    <span className=''>
                                      {player.iswk && player.iscap
                                        ? '(c) & (Wk)'
                                        : player.iswk === true
                                        ? '(Wk)'
                                        : player.iscap === true
                                        ? '(c)'
                                        : null}
                                    </span>{' '}
                                  </span>
                                </div>
                                <div className='w-1/5 flex items-end justify-end '>
                                  <span className='rounded-md border-2 border-gray-8 p-1'>
                                  <img className='w-5 h-5' src={images[player.playerRole]} alt='' />
                                  </span>
                                </div>
                              </div>
                            </div>
                          ))
                        ) : (
                          <div className='w-full pa4 bg-gray-4     flex justify-center item-center'>
                            <p className='white f7'>Squads not yet announced</p>
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          ) : (
            <div className='flex flex-col justify-center items-center'>
              <div>
                <img className='w5 pl3 h5' src='/svgs/Empty.svg' alt='' />
              </div>
              <div className='gray f6 pb1'>No Squads found</div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}
