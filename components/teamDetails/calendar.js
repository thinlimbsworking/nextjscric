import React, { useEffect, useState } from 'react';
import { TEAM_SCHEDULE } from '../../api/queries';
import { useQuery } from '@apollo/react-hooks';

const Calendar = (props) => {
  const [currentYear, setNewYear] = useState(new Date().getFullYear());
  const [currentMonth, setNewMonth] = useState(new Date().getMonth());

  useEffect(() => {
    cal.list();
  }, []);

  useEffect(() => {
    cal.list();
  }, [currentMonth]);

  function forwardCalendar() {
    if (currentMonth == 11) {
      setNewMonth(0);
      setNewYear(currentYear + 1);
      refetchSchedule(currentYear + 1, 0, 1);
    } else {
      setNewMonth(currentMonth + 1);
      refetchSchedule(currentYear, currentMonth + 1, 1);
    }
  }

  function backwardCalendar() {
    if (currentMonth == 0) {
      setNewMonth(11);
      setNewYear(currentYear - 1);
      refetchSchedule(currentYear - 1, 11, 1);
    } else {
      setNewMonth(currentMonth - 1);
      refetchSchedule(currentYear, currentMonth - 1, 1);
    }
    cal.list();
  }

  const refetchSchedule = (cy, cm, d) => {
    refetch({
      startDate: `${cy}-${cm + 1}-${d}`
    });
  };

  // ====== Calendar Calculation ===
  var cal = {
    /* [PROPERTIES] */
    mName: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], // Month Names
    data: null, // Events for the selected period
    sDay: 0, // Current selected day
    sMth: 0, // Current selected month
    sYear: 0, // Current selected year
    sMon: false, // Week start on Monday?

    /* [FUNCTIONS] */
    list() {
      // cal.list() : draw the calendar for the given month

      // BASIC CALCULATIONS
      // Note - Jan is 0 & Dec is 11 in JS.
      // Note - Sun is 0 & Sat is 6
      cal.sMth = currentMonth; // selected month
      cal.sYear = currentYear; // selected year
      var daysInMth = new Date(cal.sYear, cal.sMth + 1, 0).getDate(), // number of days in selected month
        startDay = new Date(cal.sYear, cal.sMth, 1).getDay(), // first day of the month
        endDay = new Date(cal.sYear, cal.sMth, daysInMth).getDay(); // last day of the month

      // LOAD DATA FROM LOCALSTORAGE
      cal.data = localStorage.getItem('cal-' + cal.sMth + '-' + cal.sYear);
      if (cal.data == null) {
        localStorage.setItem('cal-' + cal.sMth + '-' + cal.sYear, '{}');
        cal.data = {};
      } else {
        cal.data = JSON.parse(cal.data);
      }

      // DRAWING CALCULATIONS
      // Determine the number of blank squares before start of month
      var squares = [];
      if (cal.sMon && startDay != 1) {
        var blanks = startDay == 0 ? 7 : startDay;
        for (var i = 1; i < blanks; i++) {
          squares.push('b');
        }
      }
      if (!cal.sMon && startDay != 0) {
        for (var i = 0; i < startDay; i++) {
          squares.push('b');
        }
      }

      // Populate the days of the month
      // console.log("CAl: ", cal)
      for (var i = 1; i <= daysInMth; i++) {
        // if () {

        // }

        squares.push(i);
      }

      // Determine the number of blank squares after end of month
      if (cal.sMon && endDay != 0) {
        var blanks = endDay == 6 ? 1 : 7 - endDay;
        for (var i = 0; i < blanks; i++) {
          squares.push('b');
        }
      }
      if (!cal.sMon && endDay != 6) {
        var blanks = endDay == 0 ? 6 : 6 - endDay;
        for (var i = 0; i < blanks; i++) {
          squares.push('b');
        }
      }

      // DRAW HTML
      // Container & Table
      var container = document.getElementById('cal-container'),
        cTable = document.createElement('table');
      cTable.id = 'calendar';
      if (container) {
        container.innerHTML = '';
        container.appendChild(cTable);
      }

      // First row - Days
      var cRow = document.createElement('tr'),
        cCell = null,
        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
      if (cal.sMon) {
        days.push(days.shift());
      }
      for (var d of days) {
        cCell = document.createElement('td');
        // console.log("innerHTML 2: ", cCell)
        cCell.innerHTML = d;
        cRow.appendChild(cCell);
      }
      cRow.classList.add('head');
      cTable.appendChild(cRow);

      // Days in Month
      var total = squares.length;
      // console.log("SQUARES: ", squares, cal)
      cRow = document.createElement('tr');
      cRow.classList.add('day');
      for (var i = 0; i < total; i++) {
        cCell = document.createElement('td');
        if (squares[i] == 'b') {
          cCell.classList.add('blank');
        } else {
          // console.log("innerHTML 2: ", cCell)
          cCell.innerHTML = "<div class='dd'>" + squares[i] + '</div>';

          for (let match of (data && data.teamSchedule.matches) || []) {
            if (
              `${new Date(+match.startDate).getFullYear()}-${new Date(+match.startDate).getMonth()}-${new Date(
                +match.startDate
              ).getDate()}` == `${cal.sYear}-${cal.sMth}-${squares[i]}`
            ) {
              cCell.setAttribute('class', `cal-${match.format}`);
              cCell.innerHTML += `<div class="evt schedule-${match.format}">${match.statusStr}</div>`;
              cCell.onclick = (e) => props.getScorecard(e, match.matchID);
            }
          }
        }
        cRow.appendChild(cCell);
        if (i != 0 && (i + 1) % 7 == 0) {
          cTable.appendChild(cRow);
          cRow = document.createElement('tr');
          cRow.classList.add('day');
        }
      }
    }
  };

  // === Calendar calculation closing

  let { error, loading, data, refetch } = useQuery(TEAM_SCHEDULE, {
    variables: {
      // teamID: "4", startDate: `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`
      teamID: props.teamID,
      startDate: `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()}`
    }
  });

  if (data) {
    cal.list();
  }

  return (
    <div id='page-body'>
      {/* <!-- [CALENDAR] --> */}
      <div className='bg-white mb3'>
        {/* Calendar title */}
        <div className='flex justify-between items-center pa3 fw5 f7 grey_10'>
          <div>SCHEDULE</div>
          <div className='flex w35 justify-between'>
            <img onClick={backwardCalendar} src='/svgs/leftArrowOrange.svg' alt='' />
            {/* <div className="mh3">AUG - 2019</div> */}
            <div className=''>{`${cal.mName[currentMonth]} - ${currentYear}`}</div>
            <img onClick={forwardCalendar} src='/svgs/rightArrowOrange.svg' alt='' />
          </div>
        </div>

        <div id='cal-container' className='bg-white w-100' />

        {/* label indicator */}
        <div className='flex justify-center items-center pv3 grey_8'>
          <div className='bg-blue_10 bg-blue_10 w04 h04 br-100 mr1' />
          <div className='f8 mr2'>TEST</div>
          <div className='bg-red_Orange w04 h04 br-100 mr1' />
          <div className='f8 mr2'>ODI</div>
          <div className='bg-green_10 w04 h04 br-100 mr1' />
          <div className='f8 mr2'>T20</div>
        </div>
      </div>
    </div>
  );
};

export default Calendar;
