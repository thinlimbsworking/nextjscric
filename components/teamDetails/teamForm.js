import React, { useState } from 'react'
import { TEAM_RECENT_FORMAT_V2 } from '../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import { scheduleMatchView } from '../../api/services'
import Loading from '../Loading'
import Link from 'next/link'
const empty = '/svgs/Empty.svg'
import { format } from 'date-fns'
import Countdown from 'react-countdown-now'
import downarrow from '../../public/svgs/caret-down.svg'
import uparrow from '../../public/svgs/caret-up.svg'
import TeamScorecardSummary from './teamScorecardSummary'
import TeamScorecard from './teamScorecard'
import DataNotFound from '../../components/commom/datanotfound'
import Scores from '../commom/score'
import Topperform from '../commom/topPerformerCard'
// import Score from '../../components/commom/score';

export default function TeamForm({ teamID }) {
  const [ActiveTab, setActiveTab] = useState('test')
  const [RecentMatchIndex, setRecentMatchIndex] = useState({})

  // const [Index, setIndex] = useState(0)
  // const [show, setShow] = useState(false)
  const matchType = {
    label: ['test', 'odi', 't20'],
    value: ['test', 'odi', 't20'],
  }
  const { loading, error, data: teamSchedule } = useQuery(
    TEAM_RECENT_FORMAT_V2,
    {
      variables: { teamID: teamID },
      onCompleted: (data) => {
        if (data && data.teamRecentForm) {
          setActiveTab(
            data.teamRecentForm.hideTabs
              ? 't20'
              : data.teamRecentForm.test &&
                data.teamRecentForm.test.scoreCard &&
                data.teamRecentForm.test.scoreCard.length !== 0
              ? 'test'
              : data.teamRecentForm.odi &&
                data.teamRecentForm.odi.scoreCard &&
                data.teamRecentForm.odi.scoreCard.length !== 0
              ? 'odi'
              : 't20',
          )
        }
      },
    },
  )

  function getSeriesView(match) {
    let currentTab =
      match.matchStatus === 'upcoming' &&
      match.playing11Status === false &&
      match.probable11Status === true
        ? 'probableplaying11'
        : match.matchStatus === 'upcoming' &&
          match.playing11Status === true &&
          match.probable11Status === false
        ? 'playing11'
        : match.matchStatus === 'upcoming' &&
          match.playing11Status === true &&
          match.probable11Status === true
        ? 'playing11'
        : (match.matchStatus === 'upcoming' || match.matchStatus === null) &&
          match.playing11Status === false &&
          match.probable11Status === false
        ? 'matchInfo'
        : match.matchStatus === 'completed'
        ? 'scorecard'
        : 'live'
    return scheduleMatchView(match)
  }
  const changeRecentIndex = (index) => {
    setRecentMatchIndex({ [`${index}`]: !RecentMatchIndex[`${index}`] })
  }
  // let array = [5, 6, 7, 8, 9, 5]
  if (error) return <div></div>
  else if (loading) {
    return <Loading />
  } else
    return (
      <>
        {teamSchedule && teamSchedule.teamRecentForm ? (
          <div className="bg-gray-8 py-2 text-white ">
            {teamSchedule &&
              teamSchedule.teamRecentForm &&
              teamSchedule &&
              !teamSchedule.teamRecentForm.hideTabs && (
                // <div className='flex items-center justify-center flex-wrap md:flex-row bg-gray-4 static py-2 m-8'>
                //   {matchType.label.map((categ, i) => (
                //     <div
                //       key={i}
                //       className=' flex mx-2  items-center  justify-center'
                //       onClick={() => {
                //         setActiveTab(categ);
                //         setRecentMatchIndex({});
                //       }}>
                //       <div
                //         className={`  text-center text-xs font-medium uppercase lg:px-4 ${
                //           ActiveTab === categ ? ' white border-2 border-green px-4 justify-center items-center' : 'bg-gray-6 black'
                //         }  rounded-full cursor-pointer`}>
                //         {matchType.value[i]}
                //       </div>
                //     </div>
                //   ))}
                // </div>
                <div className="flex items-center justify-center bg-gray-4 sm:bg-gray-4   md:bg-gray-8 py-1 rounded-full mx-10 my-4   ">
                  {matchType.label.map((categ, i) => (
                    <div
                      key={i}
                      className=" flex mx-2  rounded-full items-center bg-gray  justify-center"
                      onClick={() => {
                        setActiveTab(categ)
                      }}
                    >
                      <div
                        className={` px-8 pt-2 pb-2 uppercase  rounded-full text-xs fot-medium bg-gray
                     ttu ph4-l ${
                       ActiveTab === categ
                         ? 'rounded-full bg-gray-8 shadow-4 px-8 font-medium border-green border-2'
                         : 'bg-gray-4 '
                     }  rounded-full cursor-pointer`}
                      >
                        {matchType.value[i]}
                      </div>
                    </div>
                  ))}
                </div>
              )}

            <div className="bg-gray py-2 mx-2 ">
              {teamSchedule &&
                teamSchedule.teamRecentForm &&
                teamSchedule.teamRecentForm[ActiveTab].scoreCard.length > 0 && (
                  <div className="font-normal bg-gray text-center text-xs ">
                    {/* <span className='text-green flex'>*</span> */}
                    <div className="flex text-white px-2 text-md font-semibold md:items-center md:justify-center xl:justify-center xl:items-center lg:items-center lg:justify-center">
                      <span> Last 5 Results </span>
                    </div>
                  </div>
                )}
              <div className="flex items-center justify-center">
                <div className="flex w-5/12 md:hidden lg:hidden xl:hidden">
                  <img
                    className="h-5 w-6 border-2 rounded-md"
                    src={`https://images.cricket.com/teams/${
                      teamSchedule &&
                      teamSchedule.teamRecentForm &&
                      teamSchedule.teamRecentForm[ActiveTab] &&
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard &&
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard[0] &&
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard[0]
                        .matchScore[0].teamID
                    }_flag_safari.png`}
                    onError={(evt) =>
                      (evt.target.src = '/pngsV2/flag_dark.png')
                    }
                  />

                  <div className="px-2">
                    {teamSchedule &&
                      teamSchedule.teamRecentForm &&
                      teamSchedule.teamRecentForm[ActiveTab] &&
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard &&
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard[0] &&
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard[0]
                        .matchScore[0].teamShortName}
                  </div>
                </div>

                {teamSchedule &&
                  teamSchedule.teamRecentForm &&
                  teamSchedule.teamRecentForm[ActiveTab].winLost.map(
                    (ball, i) => (
                      <div
                        key={i}
                        className="w-10 flex items-center justify-center"
                      >
                        <div
                          className={`flex justify-center lg:justify-around md:justify-around xl:justify-around items-center h-6 w-6 rounded-xl   ${
                            ball === 'W'
                              ? 'bg-green rounded-full'
                              : ball === 'L'
                              ? 'bg-red'
                              : 'bg-gray-2'
                          } text-center white  p-1 rounded-full text-xs font-semibold my-2`}
                        >
                          {ball}
                        </div>
                      </div>
                    ),
                  )}
              </div>
            </div>

            <div className="hidden md:flex-row">
              <div>
                {teamSchedule &&
                teamSchedule.teamRecentForm &&
                teamSchedule.teamRecentForm[ActiveTab].scoreCard.length > 0 ? (
                  [
                    ...Array(
                      teamSchedule.teamRecentForm[ActiveTab].scoreCard.length,
                    ),
                  ].map((x, i) => (
                    <>
                      {i % 2 === 0 && (
                        <div className="flex flex-col px-2 w-full flex-wrap xl:block lg:block md:block ">
                          <div className={`w-full py-2 lg:w-52 md:w-52`}>
                            <TeamScorecard
                              data={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i]
                              }
                              RecentMatchIndex={RecentMatchIndex}
                              changeRecentIndex={changeRecentIndex}
                              i={i}
                            />
                          </div>

                          {i + 1 <=
                            teamSchedule.teamRecentForm[ActiveTab].scoreCard
                              .length -
                              1 && (
                            <div className={`w-full py-2 lg:w-52 md:w-52 `}>
                              <TeamScorecard
                                data={
                                  teamSchedule.teamRecentForm[ActiveTab]
                                    .scoreCard[i + 1]
                                }
                                RecentMatchIndex={RecentMatchIndex}
                                changeRecentIndex={changeRecentIndex}
                                i={i + 1}
                              />
                            </div>
                          )}
                        </div>
                      )}
                      {RecentMatchIndex[i] && (
                        <div key={i} className="px-2 w-full">
                          {
                            <TeamScorecardSummary
                              matchID={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i].matchID
                              }
                              status={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i].matchStatus
                              }
                              matchType={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i].matchType
                              }
                            />
                          }
                        </div>
                      )}
                    </>
                  ))
                ) : (
                  <div className="bg-gray-4 font-medium text-center text-sm   flex flex-column items-center justify-center py-4 text-white ">
                    <div className="w-5 flex items-center justify-center ml-2 h-5">
                      <img
                        className=" md:w-44 md:h-44 w-4 h-4 lg:w-5 lg:h-5"
                        src={'./pngsV2/datanotfound.png'}
                        alt=""
                      />
                    </div>
                    <div>Team data not available</div>
                  </div>
                )}
              </div>
            </div>
            <div className="block mt-8">
              <div className="flex flex-wrap w-full">
                {teamSchedule &&
                  teamSchedule.teamRecentForm &&
                  teamSchedule.teamRecentForm[ActiveTab].scoreCard.length > 0 &&
                  teamSchedule.teamRecentForm[ActiveTab].scoreCard.map(
                    (x, i) => (
                      <>
                        {
                          <div className="flex flex-col w-full md:w-1/2 lg:w-1/2">
                            <div className={`w-full flex flex-col px-2`}>
                              <TeamScorecard
                                data={
                                  teamSchedule.teamRecentForm[ActiveTab]
                                    .scoreCard[i]
                                }
                                RecentMatchIndex={RecentMatchIndex}
                                changeRecentIndex={changeRecentIndex}
                                i={i}
                              />

                              {RecentMatchIndex[i] && (
                                <div className="w-full">
                                  {
                                    // <TeamScorecardSummary
                                    //   matchID={
                                    //     teamSchedule.teamRecentForm[ActiveTab]
                                    //       .scoreCard[i].matchID
                                    //   }
                                    //   status={
                                    //     teamSchedule.teamRecentForm[ActiveTab]
                                    //       .scoreCard[i].matchStatus
                                    //   }
                                    //   matchType={
                                    //     teamSchedule.teamRecentForm[ActiveTab]
                                    //       .scoreCard[i].matchType
                                    //   }
                                    // />
                                    <Topperform
                                      matchID={
                                        teamSchedule.teamRecentForm[ActiveTab]
                                          .scoreCard[i].matchID
                                      }
                                      status={
                                        teamSchedule.teamRecentForm[ActiveTab]
                                          .scoreCard[i].matchStatus
                                      }
                                    />
                                  }
                                </div>
                              )}
                            </div>

                            {/* {i + 1 <=
                            teamSchedule.teamRecentForm[ActiveTab].scoreCard
                              .length -
                              1 && (
                            <div className={`w-full py-2 `}>
                              <TeamScorecard
                                data={
                                  teamSchedule.teamRecentForm[ActiveTab]
                                    .scoreCard[i + 1]
                                }
                                RecentMatchIndex={RecentMatchIndex}
                                changeRecentIndex={changeRecentIndex}
                                i={i + 1}
                              />
                            </div>
                          )} */}
                          </div>
                        }
                        {/* {RecentMatchIndex[i] && (
                        <div className="  w-full   ">
                          {
                            <TeamScorecardSummary
                              matchID={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i].matchID
                              }
                              status={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i].matchStatus
                              }
                              matchType={
                                teamSchedule.teamRecentForm[ActiveTab]
                                  .scoreCard[i].matchType
                              }
                            />
                          }
                        </div>
                      )} */}
                      </>
                    ),
                  )}
              </div>
            </div>

            {/* {teamSchedule && teamSchedule.teamRecentForm && teamSchedule.teamRecentForm[ActiveTab].scoreCard.length > 0 && [...Array(teamSchedule.teamRecentForm[ActiveTab].scoreCard.length)].map((x, i) =>
               <>
                  {i % 2 === 0 && <div className="flex flex-column flex-row-l flex-row-m"><div className={`w-full w-50-l w-50-m    h4 ${i % 2 === 0 ? 'bg-red' : 'bg-green'}`} onClick={() => setRecentMatchIndex({ [`${array[i]}`]: !RecentMatchIndex[`${array[i]}`] })}> {array[i]}</div>
                     {i + 1 <= array.length - 1 && <div className={`w-full w-50-l w-50-m h4 bg-green`} onClick={() => setRecentMatchIndex({ [`${array[i + 1]}`]: !RecentMatchIndex[`${array[i + 1]}`] })}> {array[i + 1]}</div>}</div>}
                  {RecentMatchIndex[array[i]] && <div className="     w-full  ">{
                     <div>{array[i]}</div>
                  }</div>}
               </>

            )} */}
          </div>
        ) : (
          <DataNotFound />
        )}
      </>
    )
}
