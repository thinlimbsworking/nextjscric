import React, { useState } from 'react';
const Playerfallback = '/pngsV2/playerPH.png';
import { TEAM_STATS_V2 } from '../../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Loading from '../Loading';
const empty = '/svgs/Empty.svg';
import DataNotFound from '../commom/datanotfound';



export default function TeamStatsTab({ teamID }) {
  const [ActiveTab, setActiveTab] = useState('test');

  const matchType = {
    label: ['test', 'odi', 't20'],
    value: ['test', 'odi', 't20']
  };

  const BestScores = [
    { label: 'mostRuns', value: 'Most Runs' },
    { label: 'mostWickets', value: 'Most Wickets' },
    { label: 'bestScore', value: 'Best Scores' },
    { label: 'bestFigures', value: 'Best Figures' }
  ];

  const {
    loading,
    error,
    data: teamDiscovery
  } = useQuery(TEAM_STATS_V2, {
    variables: { teamID: teamID },
    onCompleted: (data) => {
      if (data && data.teamStats) {
        setActiveTab(data.teamStats.hideTabs ? 't20' : 'test');
      }
    }
  });

  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return (
      // flex items-center justify-center bg-gray rounded-full  mx-10 my-4
      <>
        {teamDiscovery && teamDiscovery.teamStats ? (
          <div className='bg-gray-8 mb-16 lg:mb-24 xl:mb-24 md:mb-24 text-white'>
            {teamDiscovery && teamDiscovery.teamStats && !teamDiscovery.teamStats.hideTabs && (
              <div className='flex items-center justify-center bg-gray-4 sm:bg-gray-4 md:bg-gray-8 py-1 rounded-full mx-10 my-4'>
                {matchType.label.map((categ, i) => (
                  <div
                    key={i}
                    className='flex mx-2 rounded-full items-center bg-gray justify-center'
                    onClick={() => {
                      setActiveTab(categ);
                    }}>
                    <div
                      className={`px-8 pt-2 pb-2 uppercase rounded-full text-xs fot-medium bg-basebg
                       ttu ph4-l ${
                         ActiveTab === categ
                           ? 'rounded-full bg-basebg shadow-4 px-8 font-medium border-green border-2'
                           : 'bg-gray-4 '
                       }  rounded-full cursor-pointer`}>
                      {matchType.value[i]}
                    </div>
                  </div>
                ))}
              </div>
            )}
            

            <div className='flex pt-4 leading-6 text-xs font-semibold w-full md:w-full lg:w-full md:text-center md:justify-around xl:justify-around lg:justify-around'>
              <div className='w-20 text-center'>
                <div className='font-medium text-sm lg:text-sm whitespace-normal text-gray-2'>Matches</div>
                <div className='pt-2 font-medium text-green-4 text-xl'>
                  {teamDiscovery &&
                    teamDiscovery.teamStats &&
                    teamDiscovery.teamStats[ActiveTab] &&
                    teamDiscovery.teamStats[ActiveTab].matchesPlayed}
                </div>
              </div>
              <div className='border-l-gray border-l-2'></div>
              <div className='w-20 text-center'>
                <div className='font-medium text-sm lg:text-sm whitespace-normal text-gray-2'>Won</div>
                <div className='pt-2 font-medium text-green-4 text-xl'>
                  {teamDiscovery &&
                    teamDiscovery.teamStats &&
                    teamDiscovery.teamStats[ActiveTab] &&
                    teamDiscovery.teamStats[ActiveTab].matchesWon}{' '}
                </div>
              </div>
              <div className='border-l-gray border-l-2'></div>
              <div className='w-20 text-center'>
                <div className='font-medium text-sm lg:text-sm whitespace-normal text-gray-2'>Lost </div>
                <div className='pt-2 font-medium text-green-4 text-xl'>
                  {teamDiscovery &&
                    teamDiscovery.teamStats &&
                    teamDiscovery.teamStats[ActiveTab] &&
                    teamDiscovery.teamStats[ActiveTab].matchesLost}
                </div>
              </div>
              <div className='border-l-gray border-l-2'></div>
              <div className='w-20 text-center'>
                <div className='font-medium text-sm lg:text-sm whitespace-normal text-gray-2'>Tied</div>
                <div className='pt-2 font-medium text-green-4 text-xl'>
                  {teamDiscovery &&
                    teamDiscovery.teamStats &&
                    teamDiscovery.teamStats[ActiveTab] &&
                    teamDiscovery.teamStats[ActiveTab].matchesTied}
                </div>
              </div>
              <div className='border-l-gray border-l-2'></div>
              <div className='w-20 text-center'>
                <div className='font-medium text-sm lg:text-sm whitespace-normal text-gray-2'>W/L</div>
                <div className='pt-2 font-medium text-green-4 text-xl'>
                  {teamDiscovery &&
                    teamDiscovery.teamStats &&
                    teamDiscovery.teamStats[ActiveTab] &&
                    teamDiscovery.teamStats[ActiveTab].winPercentage}
                </div>
              </div>
            </div>
            <div className='py-2 border-b-black'></div>
            <div className='flex flex-col flex-wrap lg:flex-row md:flex-row p-2 '>
              {BestScores.map((card, i) => (
                <div
                  className='w-full md:w-6/12  
                 '
                  key={i}>
                  <div className='lg:mx-2 my-2 bg-gray-4 bgshadow-xl overflow-hidden'>
                    <div className='font-medium text-xs text-white bg-gray-6 p-2 flex'>{card.value}</div>
                    {/* <div className='py-2 ml-56  items-center  justify-center font-medium text-xs sm:text-sm lg:text-sm '>
                      {teamDiscovery &&
                        teamDiscovery.teamStats &&
                        teamDiscovery.teamStats[ActiveTab] &&
                        teamDiscovery.teamStats[ActiveTab][card.label] &&
                        teamDiscovery.teamStats[ActiveTab][card.label].playerName}
                    </div> */}
                    <div className='flex lg:mx-2 relative'>
                      <div className='w-24 h-24 rounded-full overflow-hidden m-4 object-cover object-top'>
                      <img
                          className='absolute h-5 w-8 rounded-sm border-solid ml-16 my-20'
                          src={`https://images.cricket.com/teams/${teamID}_flag_safari.png`}
                          onError={(evt) => (evt.target.src =Playerfallback)}
                        />
                        <img
                          className='bg-gray-8 rounded-full w-96 lg:w-32 md:w-32 pt-2 px-2 h-32'
                          src={`https://images.cricket.com/players/${
                            teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].playerID
                          }_headshot_safari.png`}
                          alt={Playerfallback}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />

                        
                      </div>

                      {card.label === 'mostRuns' && (
                        <div className='flex flex-col py-4 pl-20 pr-2'>
                          <div className='text-sm'>
                            {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].playerName}
                          </div>

                          <div className='py-2 flex items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Mat :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed
                                : ' '}
                            </span>
                          </div>
                          <div className=' py-2 flex items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].average !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Avg :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].average !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].average
                                : ' '}
                            </span>
                          </div>
                          <div className=' py-2flex items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].runs !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Runs :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].runs !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].runs
                                : ' '}
                            </span>
                          </div>
                        </div>
                      )}
                      {card.label === 'mostWickets' && (
                        <div className='flex flex-col  py-4 pl-20 pr-2'>
                          <div className='text-sm'>
                            {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].playerName}
                          </div>
                          <div className='py-2 flex items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Mat :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed
                                : ' '}
                            </span>
                          </div>


                          {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].economy !== '' && (
                          <div className='py-2 flex items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].economy !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Eco :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].economy !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].economy
                                : ' '}
                            </span>
                          </div>)}
                          

                          {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].wicket !== '' && (<div className='py-2 flex  items-center  text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].wicket !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Wkts :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].wicket !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].wicket
                                : ' '}
                            </span>
                          </div>)}
                          
                        </div>
                      )}



                      {card.label === 'bestScore' && (
                        <div className='flex flex-col py-4 pl-20 pr-2'>
                          <div className='text-sm'>
                            {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].playerName}
                          </div>


                          {teamDiscovery &&
                          teamDiscovery.teamStats &&
                          teamDiscovery.teamStats[ActiveTab] &&
                          teamDiscovery.teamStats[ActiveTab][card.label] &&
                          teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== '' ? (
                            <div className=' py-2 flex  items-center text-right justify-between'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== '' ? (
                                <span className='text-gray-2 text-xs uppercase'>Mat :</span>
                              ) : (
                                ''
                              )}
                              <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                                {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab] &&
                                teamDiscovery.teamStats[ActiveTab][card.label] &&
                                teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== ''
                                  ? teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed
                                  : ' '}
                              </span>
                            </div>
                          ) : (
                            <div />
                          )}

                          {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].average !== '' && (<div className='py-2 flex  items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].average !== '' ? (
                              <span>Avg :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].average !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].average
                                : ' '}
                            </span>
                          </div>) }
                          
                          {
                            <div className='py-2 flex items-center text-xs lg:text-sm uppercase text-right justify-between'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].runs !== '' ? (
                                <span className='text-gray-2 text-xs uppercase'>Best :</span>
                              ) : (
                                ''
                              )}
                              <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                                {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab] &&
                                teamDiscovery.teamStats[ActiveTab][card.label] &&
                                teamDiscovery.teamStats[ActiveTab][card.label].runs !== ''
                                  ? teamDiscovery.teamStats[ActiveTab][card.label].runs
                                  : ' '}
                              </span>
                            </div>
                          }
                        </div>
                      )}
                      {card.label === 'bestFigures' && (
                        <div className='flex flex-col  py-4 pl-20 pr-2'>
                          <div className='text-sm'>
                            {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].playerName}
                          </div>


                          {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== '' && (<div className='py-2 flex items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Mat :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].matchesPlayed
                                : ' '}
                            </span>
                          </div>)}


                          
                          {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].average !== '' && (<div className='py-2 flex  items-center text-right justify-between'>
                            {teamDiscovery &&
                            teamDiscovery.teamStats &&
                            teamDiscovery.teamStats[ActiveTab] &&
                            teamDiscovery.teamStats[ActiveTab][card.label] &&
                            teamDiscovery.teamStats[ActiveTab][card.label].average !== '' ? (
                              <span className='text-gray-2 text-xs uppercase'>Avg :</span>
                            ) : (
                              ''
                            )}
                            <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].average !== ''
                                ? teamDiscovery.teamStats[ActiveTab][card.label].average
                                : ' '}
                            </span>
                          </div>)}

                          
                          
                            {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].runs !== '' && (<div className='py-2 flex  items-center text-right justify-between'>
                              {teamDiscovery &&
                              teamDiscovery.teamStats &&
                              teamDiscovery.teamStats[ActiveTab] &&
                              teamDiscovery.teamStats[ActiveTab][card.label] &&
                              teamDiscovery.teamStats[ActiveTab][card.label].runs !== '' ? (
                                <span className='text-gray-2 text-xs uppercase'>Best:</span>
                              ) : (
                                ''
                              )}
                              <span className='pl-16 text-green-6 text-xs font-medium text-right'>
                                {teamDiscovery &&
                                teamDiscovery.teamStats &&
                                teamDiscovery.teamStats[ActiveTab] &&
                                teamDiscovery.teamStats[ActiveTab][card.label] &&
                                teamDiscovery.teamStats[ActiveTab][card.label].runs !== ''
                                  ? teamDiscovery.teamStats[ActiveTab][card.label].runs
                                  : ' '}
                              </span>
                            </div>)}
                            
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <DataNotFound />
        )}
      </>
    );
}
