import React, { useEffect, useState } from 'react';

const Calender = () => {
  const [currentYear, setNewYear] = useState(new Date().getFullYear());
  const [currentMonth, setNewMonth] = useState(new Date().getMonth());

  useEffect(() => {
    cal.list();
  }, []);

  useEffect(() => {
    cal.list();
  }, [currentMonth]);

  function forwardCalendar() {
    if (currentMonth == 11) {
      setNewMonth(0);
      setNewYear(currentYear + 1);
    } else {
      setNewMonth(currentMonth + 1);
    }
  }
  function backwardCalendar() {
    if (currentMonth == 0) {
      setNewMonth(11);
      setNewYear(currentYear - 1);
    } else {
      setNewMonth(currentMonth - 1);
    }
    cal.list();
  }

  // ====== Calendar Calculation ===
  var cal = {
    /* [PROPERTIES] */
    mName: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], // Month Names
    data: null, // Events for the selected period
    sDay: 0, // Current selected day
    sMth: 0, // Current selected month
    sYear: 0, // Current selected year
    sMon: false, // Week start on Monday?

    /* [FUNCTIONS] */
    list() {
      // cal.list() : draw the calendar for the given month

      // BASIC CALCULATIONS
      // Note - Jan is 0 & Dec is 11 in JS.
      // Note - Sun is 0 & Sat is 6
      cal.sMth = currentMonth; // selected month
      cal.sYear = currentYear; // selected year
      var daysInMth = new Date(cal.sYear, cal.sMth + 1, 0).getDate(), // number of days in selected month
        startDay = new Date(cal.sYear, cal.sMth, 1).getDay(), // first day of the month
        endDay = new Date(cal.sYear, cal.sMth, daysInMth).getDay(); // last day of the month

      // LOAD DATA FROM LOCALSTORAGE
      cal.data = localStorage.getItem('cal-' + cal.sMth + '-' + cal.sYear);
      if (cal.data == null) {
        localStorage.setItem('cal-' + cal.sMth + '-' + cal.sYear, '{}');
        cal.data = {};
      } else {
        cal.data = JSON.parse(cal.data);
      }

      // DRAWING CALCULATIONS
      // Determine the number of blank squares before start of month
      var squares = [];
      if (cal.sMon && startDay != 1) {
        var blanks = startDay == 0 ? 7 : startDay;
        for (var i = 1; i < blanks; i++) {
          squares.push('b');
        }
      }
      if (!cal.sMon && startDay != 0) {
        for (var i = 0; i < startDay; i++) {
          squares.push('b');
        }
      }

      // Populate the days of the month
      for (var i = 1; i <= daysInMth; i++) {
        squares.push(i);
      }

      // Determine the number of blank squares after end of month
      if (cal.sMon && endDay != 0) {
        var blanks = endDay == 6 ? 1 : 7 - endDay;
        for (var i = 0; i < blanks; i++) {
          squares.push('b');
        }
      }
      if (!cal.sMon && endDay != 6) {
        var blanks = endDay == 0 ? 6 : 6 - endDay;
        for (var i = 0; i < blanks; i++) {
          squares.push('b');
        }
      }

      // DRAW HTML
      // Container & Table
      var container = document.getElementById('cal-container'),
        cTable = document.createElement('table');
      cTable.id = 'calendar';
      container.innerHTML = '';
      container.appendChild(cTable);

      // First row - Days
      var cRow = document.createElement('tr'),
        cCell = null,
        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
      if (cal.sMon) {
        days.push(days.shift());
      }
      for (var d of days) {
        cCell = document.createElement('td');
        cCell.innerHTML = d;
        cRow.appendChild(cCell);
      }
      cRow.classList.add('head');
      cTable.appendChild(cRow);

      // Days in Month
      var total = squares.length;
      cRow = document.createElement('tr');
      cRow.classList.add('day');
      for (var i = 0; i < total; i++) {
        cCell = document.createElement('td');
        if (squares[i] == 'b') {
          cCell.classList.add('blank');
        } else {
          cCell.innerHTML = "<div class='dd'>" + squares[i] + '</div>';
          if (cal.data[squares[i]]) {
            cCell.innerHTML += "<div class='evt'>" + cal.data[squares[i]] + '</div>';
          }
        }
        cRow.appendChild(cCell);
        if (i != 0 && (i + 1) % 7 == 0) {
          cTable.appendChild(cRow);
          cRow = document.createElement('tr');
          cRow.classList.add('day');
        }
      }
    }
  };

  // === Calendar calculation closing

  return (
    <div id='page-body'>
      {/* <!-- [CALENDAR] --> */}
      <div className='bg-white mb3'>
        {/* Calendar title */}
        <div className='flex justify-between items-center pa3 fw5 f7 grey_10'>
          <h2>SCHEDULE</h2>
          <div className='flex w35 justify-between'>
            <img onClick={backwardCalendar} src='/svgs/leftArrowOrange.svg' alt='' />
            {/* <div className="mh3">AUG - 2019</div> */}
            <div className=''>{`${cal.mName[currentMonth]} - ${currentYear}`}</div>
            <img onClick={forwardCalendar} src='/svgs/rightArrowOrange.svg' alt='' />
          </div>
        </div>

        <div id='cal-container' className='bg-white w-100' />

        {/* label indicator */}
        <div className='flex justify-center items-center pv3 grey_8'>
          <div className='bg-blue_10 w04 h04 br-100 mr1' />
          <div className='f8 mr2'>TEST</div>
          <div className='bg-red_Orange w04 h04 br-100 mr1' />
          <div className='f8 mr2'>ODI</div>
          <div className='bg-green_10 w04 h04 br-100 mr1' />
          <div className='f8 mr2'>T20</div>
        </div>
      </div>
    </div>
  );
};

export default Calender;
