import React from 'react';
import { format, formatDistanceToNowStrict } from 'date-fns';
import { SERIES_HOME_CARD_QUERY } from '../api/queries';
import { useQuery } from '@apollo/react-hooks';
import { useRouter } from 'next/router';


export default function UpcomingMatches(props) {
  const router = useRouter();
  const navigate = router.push;
  let { loading:loading, error:error, data } = useQuery(SERIES_HOME_CARD_QUERY, {
   
  });
  if (loading) data = {}
  if (props.error) error = props.error;
  return (
    <div className=' text-white mb:mt-5 md:mt-1 p-1 mx-2 '>
          <div className='flex items-center justify-between'>
            <div className='font-semibold text-lg tracking-wider capitalize '> Upcoming Matches</div>
            <div className='flex justify-center rounded  bg-gray-4 p-1  items-center  '
            onClick={() =>
              navigate(
                'series/[...slugs]',
                `series/domestic/${data.SeriesHomeCard.matchesData[0].tourID}/${data.SeriesHomeCard.matchesData[0].seriesName
                  .replace(/[^a-zA-Z0-9]+/g, ' ')
                  .split(' ')
                  .join('-')
                  .toLowerCase()}/matches`
              )
            }
            >
              <img className='w-8 h-8' src='/pngsV2/arrow.png' alt='' />
            </div>
          </div>
          <div className='flex overscroll-scroll flex flex-wrap-row'>
            {data&& data.SeriesHomeCard &&
              data.SeriesHomeCard.matchesData.map((item,i) => {
                return (
                  <div key={i} className=' w-40 lg:w-96 md:w-96  mx-2  bg-gray   rounded-xl' onClick={() =>
                      navigate(
                        'series/[...slugs]',
                        `series/domestic/${data.SeriesHomeCard.matchesData[0].tourID}/${data.SeriesHomeCard.matchesData[0].seriesName
                          .replace(/[^a-zA-Z0-9]+/g, ' ')
                          .split(' ')
                          .join('-')
                          .toLowerCase()}/matches`
                      )
                    }>
                      <div className='flex flex-col  h-40 items-center justify-center'>
                        <div className='flex items-center justify-center  '>
                          <div className='z-10'>
                            <img
                              className='h-6 w-9 rounded  '
                              src={`https://images.cricket.com/teams/${item.homeTeamID}_flag_safari.png`}
                                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                              alt=''
                            />
                          </div>
                          <div>
                            <img className='h-6 w-6 mx-1' src='/pngsV2/bluecross.png' alt='' />
                          </div>
                          <div className=' z-10'>
                            <img
                              className='h-6 w-9 rounded  '
                              src={`https://images.cricket.com/teams/${item.awayTeamID}_flag_safari.png`}
                                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                              alt=''
                            />
                          </div>
                        </div>

                        <div className='flex text-md mt-3 items-center justify-center'>
                          <div className='mr-2  font-semibold'>{item.homeTeamShortName} </div>
                          <div className='text-xs font-bold '> VS</div>{' '}
                          <div className='ml-2  font-semibold'>{item.awayTeamShortName} </div>
                        </div>
                        {/* <div className='text-xs font-medium mt-2 text-ellipsis overflow-hidden px-2 w-full text-gray-2'>
                          {item.seriesName}
                        </div> */}

                        <div className='font-semibold text-sm mt-2 items-center  justify-center text-yellow rounded-full  px-2 py-1'>
                          {new Date(Number(item.matchDateTimeIST) - 19800000).getTime() - new Date().getTime() > 88200000 ? (
                            `${formatDistanceToNowStrict(new Date(Number(item.matchDateTimeIST) - 19800000), {
                              unit: 'day'
                            })} to go`
                          ) : new Date(Number(item.matchDateTimeIST) - 19800000).getDate() != new Date().getDate() ? (
                            <div className={``}>
                              {format(new Date(Number(item.matchDateTimeIST) - 19800000), ' dd MMM yy , h:mm a ')}
                            </div>
                          ) : (
                            'Today'
                          )}
                        </div>
                      </div>
                    </div>
                );
              })}
          </div>
        </div>
  );
}
