import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { GET_TRUMP_CARDS } from '../../api/queries';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import DataNotFound from '../commom/datanotfound';
import axios from 'axios';

const CricketFlipImage = '/svgs/images/cricket-flip-card.png';
const backIconWhite = '/svgs/backIconWhite.svg';
const fallbackProjection = '/pngs/fallbackprojection.png';
const cricket = '/svgs/images/icon-128x128.png';
const share = '/svgs/share-line.png';

const cardsData = [
  {
    name: 'Matches',
    value: 'matches'
  },
  {
    name: 'Runs',
    value: 'runs'
  },
  {
    name: '50s',
    value: 'fifties'
  },
  {
    name: '100s',
    value: 'hundreds'
  },
  {
    name: 'Batting avg',
    value: 'average'
  },
  {
    name: 'Wickets',
    value: 'wickets'
  },
  {
    name: 'Bowling avg',
    value: 'bowlingAverage'
  },
  {
    name: 'Eco. Rate',
    value: 'economyRate'
  },
]

var a = 0;
var tout = 0;

export default function PlayTrumpCard(props) {
  let router = useRouter();
  let navigate = router.push;

  const [select, setselect] = useState(false);
 
  const [count, setcount] = useState(15);
  //const [data, setData] = useState([]);


  const [stats, setStats] = useState({
    matches: false,
    runs: false,
    average: false,
    hundreds: false,
    fifties: false,
    wickets: false,
    bowlingAverage: false,
    economyRate: false
  })

  const { loading, error, data, fetchMore } = useQuery(GET_TRUMP_CARDS, {
    fetchPolicy: 'network-only'
  });


  // useEffect(() => {
  //   if (count === 15) {
  //     axios.post('https://apiv2.cricket.com/cricket', {
  //       query: GET_TRUMP_CARDS,
  //       variables: {}
  //     }).then(({ data }) => {
  //       console.log('getting carddat = ', data.data);
  //       setData(data.data);
  //     }).catch(err => console.log('err occur - ', err))
  //   }
  // }, [count])

  const callNextData = (fetchMore) => {
    fetchMore({
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult;
        }
        let result = fetchMoreResult;
        return { ...result };
      }
    });
  };

  if (typeof window !== 'undefined') {
    window.onbeforeunload = () => {
      global.window && global.window.localStorage.key('totalScore')
        ? global.window.localStorage.clear('totalScore')
        : '';
    };
  }
  useEffect(() => {
    if (data) {
      clearInterval(a);
      handleCountdownTimer();
    }
    return () => {
      clearInterval(a);
    };
  }, [data,count]);

  // useEffect(() => {
  //  if(count === 0) {
  //   handleCountdownTimer();
  //  }
  // }, [count]);

  const handleShare = (e) => {
    if (navigator.share !== undefined && location.protocol == 'https:') {
      if (navigator.running == true) {
        return;
      }
      navigator.running = true;
      navigator
        .share({
          title: 'stat-attack',
          url: location.origin + '/stat-attack/start-game'
        })
        .then(() => {
          navigator.running = false;
        })
        .catch((error) => {
          navigator.running = false;
          console.log(error);
        });
    }
  };
  const handleCountdownTimer = () => {
    clearTimeout(tout)
    clearInterval(a);
    a = setInterval(() => {
      if(count === 0) {
          clearTimeout(tout)
          navToScore();
          clearInterval(a);
          setcount(15);
      } else {
        setcount(prev => prev-1)
      }
    }, 1000);
  };


  const handleStat = val => {
    setStats(prev => ({ ...prev, [val]: true }))
  }


  const handleTimeout = (fun,timer) => {
    clearInterval(a);
    tout = setTimeout(() => {
      fun();
    },timer)
  }


  function nextCard() {
    setselect(false);
    setcount(15);
    callNextData(fetchMore);
    setStats({
      matches: false,
      runs: false,
      average: false,
      hundreds: false,
      fifties: false,
      wickets: false,
      bowlingAverage: false,
      economyRate: false
    })
  }

  function add() {
    var totalscore = global.window.localStorage.getItem('totalScore');
    totalscore = totalscore ? totalscore : 0;
    global.window.localStorage.setItem('totalScore', parseInt(totalscore) + 1);
    handleTimeout(nextCard,3000);
  }

  function navToScore() {
    navigate('/stat-attack/[type]', '/stat-attack/score');
  }

  function wrong() {
    handleTimeout(navToScore,5000)
  }

  if(data && !data.getTrumpCards.length) return <div className='w-full h-[100vh] flex flex-col gap-5 items-center justify-center'><DataNotFound/></div>
  return (
    <div className="">
      <div className='md:block lg:block xl:block hidden '>
        <div className=' text-white text-xl text-semibold h-full w-full  center  font-semibold flex justify-center items-center mt-2'>
          Please switch to Mobile Potrait Mode View
        </div>
      </div>
      <div className='md:hidden lg:hidden xl:hidden'>
        <div className='stat-attack-bg min-vh-full'>
          {/* <div className='bg-navy flex items-center'>
            <div className='w-5 h-4 flex justify-center items-center cursor-pointer ml-2'>
              <img className='' onClick={() => navigate('/')} src={backIconWhite} alt=''></img>
            </div>
            <div className='flex justify-between items-center w-full'>
              <div className='text-base font-semibold text-white ml-2'>STAT ATTACK</div>
              <div className='w-6 h-6 flex justify-center items-center mr-2 my-2'>
                <img
                  src={share}
                  onClick={() => {
                    handleShare();
                  }}
                  alt=''
                  className='w-13'></img>
              </div>
            </div>
          </div> */}
          <div className='bg-navy flex items-center'>
            <div className='flex justify-between items-center bg-gray-4 w-full'>
              <div className='ml-2 w-4 h-2 flex justify-center items-center cursor-pointer'>
                <img className='' onClick={() => navigate('/')} src={backIconWhite} alt=''></img>
              </div>
              <div className='text-base font-semibold mt-4 w-10/12 text-white pb-4'>STAT ATTACK</div>
              <div className='mr-2 w-4 h-2 flex justify-center items-center'>
                <img src={share} onClick={handleShare} alt='' className='w-12'></img>
              </div>
            </div>
          </div>
          <div className='flex justify-between items-center p-2'>
            <div className='text-white w-42 flex justify-between items-center'>
              <div className='text-base font-medium mr-1 nowrap'>Your Score</div>
              <div className='stat-score-gr text-sm font-semibold p-3 ml-8 border-2 bg-red'>
                {' '}
                {global.window && global.window.localStorage.getItem('totalScore')
                  ? global.window.localStorage.getItem('totalScore')
                  : '0'}{' '}
              </div>
            </div>
            <div className='w-42 flex justify-center items-center'>
              <CircularProgressBar strokeWidth='5' sqSize='50' percentage={100 - (count / 15) * 100} value={count} />
            </div>
          </div>

          {data && (
            <div className='mt-2 flex w-full'>
              <div className='w-48' style={{ border: '5px solid #222c32', borderRadius: 4 }}>
                <div className='shadow-4 bg-white-10 border-solid border-2 bg-navy'>
                  <div className='bg-navy flex justify-center items-center'>
                    <img
                      className='h-44 p-1'
                      src={`${global.window && global.window.localStorage.browser === 'Safari'
                        ? `https://images.cricket.com/players/${data && data.getTrumpCards[0].playerID
                        }_headshot_safari.png`
                        : `https://images.cricket.com/players/${data?.getTrumpCards && data.getTrumpCards[0] && data.getTrumpCards[0].playerID}_headshot.png`
                        }`}
                      alt=''
                      onError={(evt) => (evt.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className='bg-white flex items-center justify-center text-center f6 font-semibold h-4 border-2'>
                    {' '}
                    {data?.getTrumpCards && data.getTrumpCards[0] && data.getTrumpCards[0].name}{' '}
                  </div>
                </div>
                <div className='shadow-4 p-0.5' style={{ backgroundColor: '#08141A' }}>

                  {

                    cardsData.map(card => (
                      <div
                        className={`bg-white mb-2 border-2 p-2 text-center flex justify-between shadow-4 ${select && stats[card.value] &&
                          (parseInt(data && data.getTrumpCards[0] && data.getTrumpCards[0][card.value]) >= parseInt(data && data.getTrumpCards[1] && data.getTrumpCards[1][card.value])
                            ? 'bg-green-3 text-white'
                            : 'bg-red text-white')
                          } `}
                        onClick={() => !select && (setselect(true), handleStat(card.value))}

                      >
                        <span className='f7'>{card.name}</span>
                        <span className='f6 font-semibold'> {data?.getTrumpCards && data.getTrumpCards[0] && data.getTrumpCards[0][card.value]} </span>
                      </div>
                    ))

                  }

                </div>
              </div>

              <div className='w-48' style={{ border: '5px solid #222c32' }}>
                <div className='shadow-4 bg-white-10 border-solid border-2 bg-navy'>
                  <div className='bg-navy flex justify-center items-center border-2 border-t-solid'>
                    <img
                      className='h-44 flip-horizontally p-1'
                      src={`${global.window && global.window.localStorage.browser === 'Safari'
                        ? `https://images.cricket.com/players/${data && data.getTrumpCards[1].playerID
                        }_headshot_safari.png`
                        : `https://images.cricket.com/players/${data?.getTrumpCards && data.getTrumpCards[1] && data.getTrumpCards[1].playerID}_headshot.png`
                        }`}
                      alt=''
                      onError={(evt) => (evt.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className='bg-white f6 font-semibold h-4 flex items-center justify-center text-center border-2'>
                    {' '}
                    {data?.getTrumpCards && data.getTrumpCards[1] && data.getTrumpCards[1].name}{' '}
                  </div>
                </div>

                <div
                  className='shadow-4 bg-white-10'
                  style={{
                    perspective: '1000px',
                    height: 285,
                    borderBottomLeftRadius: '10px',
                    borderBottomRightRadius: '10px',
                    backgroundColor: '#08141A'
                  }}>

                  <div
                    className={`absolute w-full h-full ${select ? 'card-flip' : ''}`}
                    style={{
                      transition: 'transform 0.8s',
                      transformStyle: 'preserve-3d'
                    }}>
                    {!select ? <div
                      className='absolute h-full w-full flex items-center justify-center card-flip Safari'
                      style={{
                        backfaceVisibility: 'hidden',
                        WebkitBackfaceVisibility: 'hidden'
                      }}>
                      <div
                        className='flex justify-center items-center w-full h-full'
                        style={{
                          backgroundImage: `url(${CricketFlipImage})`,
                          backgroundPosition: 'center',
                          backgroundSize: 'cover'
                        }}></div>
                    </div>
                      : (
                        <div
                          className='absolute w-full card-flip Safari px-1'
                          style={{
                            //backfaceVisibility: 'hidden',
                            //transform: 'rotateY(180deg)',
                            WebkitBackfaceVisibility: 'hidden'
                          }}>

                          {

                            cardsData.map(card => (
                              <div
                                className={`bg-white mb-2 border-2 p-2 text-center flex justify-between shadow-4 ${stats[card.value] &&
                                  (parseInt(data && data.getTrumpCards[0] && data.getTrumpCards[0][card.value]) >=
                                    parseInt(data && data.getTrumpCards[1] && data.getTrumpCards[1][card.value])
                                    ? (add(), 'bg-green-3 text-white')
                                    : (wrong(), 'bg-red text-white'))
                                  } `}>
                                <span className='f7'>{[card.name]}</span>
                                <span className={`f6 font-semibold`}> {data && data.getTrumpCards[1] && data.getTrumpCards[1][card.value]} </span>
                              </div>
                            ))

                          }                   
                      
                        </div>
                      )}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

const CircularProgressBar = (props) => {
  const sqSize = props.sqSize;
  // SVG centers the stroke width on the radius, subtract out so circle fits in square
  const radius = (props.sqSize - props.strokeWidth) / 2;
  // Enclose cicle in a circumscribing square
  const viewBox = `0 0 ${sqSize} ${sqSize}`;
  // Arc length at 100% coverage is the circle circumference
  const dashArray = radius * Math.PI * 2;
  // Scale 100% coverage overlay with the actual percent
  const dashOffset = dashArray - (dashArray * (100 - props.percentage)) / 100;

  return (
    <svg width={props.sqSize} height={props.sqSize} viewBox={viewBox}>
      <circle
        style={{
          fill: 'white',
          stroke: 'white',
          strokeLinecap: 'round',
          strokeLinejoin: 'round'
        }}
        className='circle-background'
        cx={props.sqSize / 2}
        cy={props.sqSize / 2}
        r={radius}
        strokeWidth={`${props.strokeWidth}px`}
      />

      <circle
        className='circle-progress'
        cx={props.sqSize / 2}
        cy={props.sqSize / 2}
        r={radius}
        strokeWidth={`${props.strokeWidth}px`}
        // Start progress marker at 12 O'Clock
        transform={`rotate(-90 ${props.sqSize / 2} ${props.sqSize / 2})`}
        style={{
          strokeDasharray: dashArray,
          strokeDashoffset: dashOffset,
          fill: 'none',
          stroke: '#a80d0d',
          transition: 'stroke-dashoffset 1s linear 0s',
          strokeLinecap: 'round',
          strokeLinejoin: 'round'
        }}
      />
      <text
        style={{
          fontSize: 17,
          fontWeight: 'bold'
        }}
        className='circle-text'
        x='50%'
        y='50%'
        dy='.3em'
        textAnchor='middle'>
        {`${props.value}`}
      </text>
    </svg>
  );
};
