import React, { useState,useEffect } from 'react';
const backIcon = '/svgs/backIconWhite.svg'
const flagplaceholder = '/svgs/flag_empty.svg';
import ScorecardSummary from '../../components/HomeCriclytics/scorecardsummary'
const shareIcon = '/svgs/share-line.png';
import { useRouter } from 'next/router';
const close = '/svgs/close.png'

let uparrow = "/svgs/caret-up.svg";
export default function Result({ correct, data, setShowResult, props }) {
  let router = useRouter();

  const [openModel, setOpenModel] = useState(false)
  const [index, setIndex] = useState()
  const [scored, setScored] = useState(correct && correct.filter(x => x === true).length)
  const [Internet, setInternet] = useState(true)

  
  const handleShare = (e) => {
    if (navigator.share !== undefined) {
      window.navigator
        .share({
          title: `Who Was the Winner`,
          text: `Hey, I scored ${scored} on Who Was the Winner. Guess the Winner of these historic games at `,
          url: window.location.origin + '/who-was-the-winner'
        })
        .then(() => {
        })
        .catch((error) => console.log(error));
    }
  };

  const shareMsg = () => {

    if (global.window && global.window['ReactNativeWebView'])
      global.window.ReactNativeWebView.postMessage(`share,${scored}`);

  }
  return (
    <div className="bg-black min-h-screen">

      {/* <div className="flex items-center bg-white-10 text-white p-3" style={{backgroundColor:'rgb(26,26,26)'}}>
        <img className="w-1" onClick={() => setShowResult(false)} alt='' src={backIcon} />
        <div className="text-base font-semibold pl-3">Who Was the Winner</div>
      </div> */}
      <div className="px-2 h-8 py-6 flex justify-between items-center text-white" style={{backgroundColor:'rgb(26,26,26)'}}>
          <div className="flex items-center">
            <img className="w-4" onClick={() => handleBack()} alt='' src={backIcon} />
            <div className="f5 font-semibold pl-2">Who Was the Winner</div>
          </div>
          <img className="w-4" onClick={() => { handleShare();shareMsg() }} src={shareIcon} />
        </div>

      <div className="mt-3 text-white mx-auto px-2">
        <div className="py-2 border-2 border-gray-1 flex justify-center center" style={{backgroundColor:'rgb(26,26,26)'}}>
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((x, i) => (
            <div key={i} className="" style={{ width: "9%" }}>
              <div className={`h-2 ${(correct[i] ? "bg-green" : "bg-red")} border-2 border-solid border-black flex`} > </div>
            </div>
          ))
          }
        </div>
        <div className="mx-auto flex justify-center mt-4">
          <div className="rounded-full w-44 h-44 shadow-4 relative" >
            <div className="w-80 h-80 rounded-full bg-white-10 z-1 relative" style={{ top: "12px", left: "12px" }}>
              <div className="w-80 h-80 rounded-full bg-white-10 z-1 relative" style={{ top: "10px", left: "10px" }}>
                <div className="text-center absolute top-1">
                  <img className="w-14 mx-8" src={'/pngs/crown.png'} />
                  <div className="text-sm font-medium pt-1">Your Score</div>
                  <div className="text-green f2 font-semibold pt-1">{scored}</div>
                </div>
              </div>
            </div>
          </div>
          {/* {correct.filter(x => x === true).length} */}
        </div>
        <div className="bg-red uppercase text-sm font-semibold p-2 cursor-pointer mt-3 flex rounded-lg justify-center w-42 mx-28" onClick={() => window.location.reload()}>Play Again</div>

        <div className='mt-3'>
          <div className="flex justify-center items-center cursor-pointer mx-auto" onClick={() => { handleShare(); shareMsg()}}>
            <div className="bg-blue rounded-full flex justify-center items-center">
              <img src={shareIcon} alt='' className='w-8 p-2'></img>
            </div>
            <div className="text-sm font-medium pl-2">Share</div>
          </div>
        </div>

        <div className="text-white pb-3 pt-2">
          {
            data.map((x, i) => (
              <div key={i} className="mt-2 text-black" style={{backgroundColor:'rgb(26,26,26)'}} onClick={() => (global.window && global.window.navigator.onLine ? (setOpenModel(!openModel), setIndex(i)) : setInternet(false))} >
                <div className="flex justify-between text-white">
                  <div className="bg-gray-400 py-1 flex justify-center items-center text-xs w-8">Q{i + 1}</div>
                  <div className="bg-red-50 text-black text-sm font-medium mt-2 py-1 rounded-full flex items-center px-2">{x.result}</div>
                  {correct[i] ? <div className="bg-green py-1 px-2 flex items-center">&#x2713;</div> :
                    <div className="bg-red py-1 px-2 flex items-center">&#10005;</div>}
                </div>

                <div className="p-2 px-3 mt-2 flex justify-between text-white">
                  <div>
                    <div className="flex items-center">
                      <img className='h-5 w-10 shadow-4' src={`https://images.cricket.com/teams/${x.firstInningsTeamID}_flag_safari.png`} onError={(evt) => (evt.target.src = flagplaceholder)} />
                      <div className="text-xs pl-2 font-medium">{x.firstInningsShortName}</div>
                      <div className="text-sm font-medium pl-3" >{x.firstInningsScore}</div>
                    </div>
                    <div className="text-sm pr-1 text-right">({x.firstInningsOver})</div>
                  </div>


                  <div>
                    <div className="flex items-center">
                      <img className='h-5 w-10 shadow-4' src={`https://images.cricket.com/teams/${x.secondInningsTeamID}_flag_safari.png`} onError={(evt) => (evt.target.src = flagplaceholder)} />
                      <div className="text-xs pl-2 font-medium">{x.secondInningsShortName}</div>
                      <div className="text-sm font-medium pl-3" >{x.secondInningsScore}</div>
                    </div>
                    <div className="text-sm pr-2 text-right">({x.secondInningsOver})</div>
                  </div>

                </div>
                <img className={`flex justify-center mx-auto w-8 p-2  ${index === i && openModel ? "rotate-180" : ""}`} src={uparrow}/>
                {openModel && i === index && <ScorecardSummary matchID={x.matchID} status={"completed"} noURL={"false"} />}
              </div>
            ))
          }
        </div>

      </div>
      {!Internet && <div className="flex justify-center items-center fixed absolute--fill z-9999 bg-black-10 overflow-y-scroll " style={{ backdropFilter: "blur(10px)" }} >
        <div className="w-full lg:max-w-lg ">
          <div className="bg-black shadow-4 border-solid border-2 border-white relative p-2 py-3" >
            <img className="w-1 h-1 absolute right-0 bg-white-10" style={{ top: "-22px" }} src={'/svgs/close.png'} onClick={() => (setInternet(true))} />
            <div className="center text-center text-white">
              <img src={"/svgs/images/icon-128x128.png"} alt="" className="w-2-5 pb2" />
              <div className="white text-base font-semibold">No Internet Connection</div>
              <div className="pt-3 text-xs font-normal lh-title">We're facing trouble reaching our  servers.<br></br>Please reconnect. </div>
              {/* <div className="bb b--white-20 mv2 bw-1"></div> */}
              {/* <div className="text-base font-semibold pt-1" onClick={()=> router.push("/")} >TRY AGAIN</div> */}
            </div>


          </div>
        </div>
      </div>}
    </div>
  )
}