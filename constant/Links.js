export const SERIES_DETAILS = {
  as: '`/series/${type}/${seriesId}/${seriesName}/${currentTab}`',
  href: '/series/[...slugs]'
};



export const TEAM_TAB_VIEW = {
  as: '`/teams/${tabName}`',
  href: '/teams/[team]'
};

export const SERIES_DETAILS_View = {
  as: '`/series/${seriesType}/${seriesId}/${seriesName}/stats/${type2}/${types}`',
  href: '/series/[...slugs]'
};

export const SERIES_DETAILS_View_BEST = {
  as: '`/series/${seriesType}/${seriesId}/${seriesName}/stats/${format}/${type2}/${types}`',
  href: '/series/[...slugs]'
};
export const CRYCLYTICS_VIEW_SCORE = {
  as: '`/criclytics/${matchID}/${criclyticName}`',
  href: '/criclytics/[...id]'
};
export const SERIES_TAB_SWITCH = {
  as: '`/series/${tab}`',
  href: '/series/[slug]'
};

export const TEAM_VIEW = {
  as: '`/teams/${teamId}/${teamName}/form`',
  href: '/teams/[...slugs]'
};

export const TEAM_TAB_V2 = {
  as: '`/teams/${teamId}/${teamName}/${tabName}`',
  href: '/teams/[...slugs]'
};

export const IPL_TEAM_VIEW = {
  as: '`/iplteams/${teamId}/${teamName}`',
  href: '/iplteams/[...slugs]'
};

export const IPL_AUCTION_VIEW= {
  as: '`/ipl-2022/${teamId}/${teamName}`',
  href: '/ipl-2022/[...slugs]'
};

export const IPL_PLAYER_AUCTION_VIEW = {
  as: '`/ipl-2022/players/${playerID}/${playerName}`',
  href: '/ipl-2022/[...slugs]'
};





export const IPL_PLAYER_VIEW = {
  as: '`/iplteams/${players}/${playerName}/${playerID}`',
  href: '/iplteams/[...slugs]'
};

//
export const APP_IPL_TEAM_VIEW = {
  as: '`/appiplteams/${teamId}/${teamName}/`',
  href: '/appiplteams/[...slugs]'
};
export const APP_IPL_PLAYER_VIEW = {
  as: '`/appiplteams/players/${playerName}/${playerID}`',
  href: '/appiplteams/[...slugs]'
};





//
// ex - completed and inernational domestic
export const SCHEDULE_TAB_STATUS_SWITCH = {
  as: '`/schedule/${pathWillBe}`',
  href: '/schedule/[...slugs]'
};
export const GET_LIEV_STREAM = {
  as: '`/livestream/${pathWillBe}`',
  href: '/livestream/[slug]'
};




export const SERIES_TAB_BASE_URL = {
  as: '`/series/${tab}`',
  href: '/series/[slugs]'
};


export const SERIES_FILTER_TAB_BASE_URL = {
  as: '`/series/${tab}/${filterTab}`',
  href: '/series/[...slugs]'
};

export const SERIES_HOME = {
  as: '`/series/${filterTab}`',
  href: '/series/[slug]'
};

export const MATCH_DETAIL_LAYOUT = {
  as: '`/${matchStatus}/${matchID}/${currentTab}/${seriesName}`',
  href: '`/${matchStatus}/[...slugs]`'
};

export const LIVE_SCORE_ARTICLE = {
  as: '`/${matchStatus}/${matchID}/${currentTab}/${seriesName}`',
  href: '`/${matchStatus}/[...slugs]`'
};

export const ARTICLE_VIEW = {
  as: '`/news/${articleId}`',
  href: '/news/[type]'
};

export const FEATURED_ARTICLE_VIEW = {
  as: '`/news/${tab}/${articleId}`',
  href: '/news/[...slugs]`'
};

export const ARTICLE_TAB_SWITCH = {
  as: '`/news/${tab}`',
  href: '/news/[type]'
};

export const FANTASY_TEAM_VIEW = {
  as: '`/${switchView}/${matchId}/${index}`',
  href: '`/${switchView}/[...slugs]`'
};

export const CRYCLYTICS_VIEW = {
  as: '`/criclytics/${matchID}/${criclyticName}/${tab}`',
  href: '/criclytics/[...id]'
};


export const News_Live_Tab_URL = {
  as: '`/news/live-blog/${matchId}/${matchName}/${articleID}`',
  href: '/news/[...slugs]'
};



export const PLAYER_VIEW = {
  as: '`/players/${playerId}/${playerSlug}/career-stats`',
  href: '/players/[...slugs]'
};

export const PLAYER_VIEW_BIO = {
  as: '`/players/${playerID}/${playerName}/playerbio`',
  href: '/players/[...slugs]'
};
export const PLAYER_COMPARE_VIEW = {
  as: '`/players/${playerID1}/${playerSlug}/compare/${playerID2}`',
  href: '/players/[...slugs]'
};

export const PLAYER_TABS = {
  as: '`/players/${PlayerId}/${PlayerName}/${tabName}`',
  href: '/players/[...slugs]'
};



export const RECORD_VIEW = {
  as: '`/records/${format}/${type}/${item}`',
  href: '/records/[...slugs]'
};
// http://localhost:3001/news/authors/vishal

export const AUTHOR_NEWS_VIEW = {
  as: '`/authors/${authorName}/${authorID}`',
  href: '/authors/[...slugs]'
};



export const STADIUM_VIEW = {
  as: '`/stadiums/${stadiumId}/${stadiumSlug}`',
  href: '/stadiums/[...slugs]'
};


export const AUCTION_VIEW = {
  as: '`/ipl-2022/${tabName}`',
  href: '/ipl-2022/[...slugs]'
};


export const VIDEOS_VIEW = {
  as: '`/videos/${videoId}`',
  href: '/videos/[...slugs]'
};

export const FRC_SERIES_VIEW = {
  as: '`/fantasy-research-center/${matchId}/${seriesSlug}`',
  href: '/fantasy-research-center/[...slugs]'
};

export const FRC_SERIES_CATEGORY_VIEW = {
  as: '`/fantasy-research-center/${matchId}/${seriesSlug}/${tab}`',
  href: '/fantasy-research-center/[...slugs]'
};

export const FRC_SERIES_TEAM_VIEW = {
  as: '`/fantasy-research-center/${matchId}/${seriesSlug}/${tab}/${fantasyTeam}`',
  href: '/fantasy-research-center/[...slugs]'
};

export const STAT_ATTACK = {
  as: '`/stat-attack/start-game',
  href: '/stat-attack/start-game'
};

export const APP_STAT_ATTACK = {
  as: '`/app-stat-attack/intro',
  href: '/app-stat-attack/intro'
};
export const PLAY_THE_ODDS_CONTEST = {
  as: '`/play-the-odd/contest/${matchID}`',
  href: '/play-the-odd/[...slug]'
}

export const MY_CONTEST={
  as: '`/play-the-odd/contest/${matchID}`',
  href: '/play-the-odd/[...slug]'
}

export const PLAY_THE_ODDS_CONTEST_LEADERBOARD = {
  as: '`/play-the-odd/${matchID}/leaderboard`',
  href: '/play-the-odd/[...slug]'
}






export const HEADER = {

  

  'playtheodds': {
    as: '/playtheodds/login',
    href: '/playtheodds/login'
  },

  'season-fantasy': {
    as: '/season-fantasy',
    href: '/season-fantasy'
  },

  'fantasy-research-center': {
    as: '/fantasy-research-center',
    href: '/fantasy-research-center'
  },
  criclytics: {
    as: '/criclytics',
    href: '/criclytics'
  },
  schedule: {
    as: '/schedule/live-matches',
    href: '/schedule/[...slugs]'
  },
  series: {
    as: '/series/ongoing',
    href: '/series/[slug]'
  },
  news: {
    as: '/news/latest',
    href: '/news/[type]'
  },
  players: {
    as: '/players/all',
    href: '/players/all'
  },
  teams: {
    as: '/teams/international',
    href: '/teams/international'
  },
  stadiums: {
    as: '/stadiums',
    href: '/stadiums'
  },
  rankings: {
    as: '/rankings',
    href: '/rankings'
  },
  records: {
    as: '/records',
    href: '/records'
  },
  videos: {
    as: '/videos/latest',
    href: '/videos/[...slugs]'
  },
  profile: {
    as: '/profile',
    href: '/profile'
  }
};



