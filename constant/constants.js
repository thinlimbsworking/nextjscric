export const scoreRangeText = {
  '0-5': [
    'Slogging from the get-go? Patience, Afridi!',
    'The key is to get your eye in. Bad balls will come.',
    'Next time, see off the new ball maybe?',
    'Jeez, relax a little, Viru! What was that?',
    'Even Gayle takes an over to get his eye in.'
  ],
  '6-10': [
    'A flying start followed by a rash shot.',
    'Three words for you pal: Practice, practice, practice.',
    'Just the polar opposite of responsible batting.'
  ],
  '11-20': ['Unlucky there! Fancy another go at it?', 'Out off a full-toss! Where’s your head?'],
  '21-30': ['You did the hard part, then threw it away!', 'Looked good for a big score there.'],
  '31-infinite': [
    'What a gem of an innings, you little beauty!',
    'Class and swagger written all over it. Well done mate!'
  ]
};

export const between = (x, min, max) => {
  if (max === 'infinite') return x >= min;
  else return x >= min && x <= max;
};

export const getRandomString = (min, max) => Math.floor(Math.random() * (max - min)) + min;

export const CRYCLYTICS_KEYS = {
  keyStatsKey: 'key-stats',
  projectionKey: 'player-projection',
  matchupKey: 'match-ups',
  statsKey: 'match-stats',
  phasesKey: 'phases-of-play',
  gameChangingKey: 'game-changing-overs',
  predictionKey: 'team-score-projection',
  liveprojectionKey: 'live-player-projection',
  reelKey: 'match-reel',
  rattingKey: 'report-card',
  simulatorKey: 'over-simulator',
  finalFourKey: 'final-four',
  valuablePlayerKey: 'most-valuable-player',
  partnerShip:'partnership-predictor',
  scoringcharts:'scoring-charts'
};

export const CRYCLYTICS_COMPLETED = [
  'match-stats',
  'phases-of-play',
  'game-changing-overs',
  'match-reel',
  'report-card'
];

export const CRYCLYTICS_UPCOMING = ['player-projection', 'match-ups', 'key-stats'];

export const CRYCLYTICS_LIVE = ['match-ups', 'team-score-projection', 'live-player-projection', 'over-simulator','partner'];

export const validSiteMapPaths = [
  'cricket',
  'articles',
  'criclytics',
  'fantasy',
  'stadiums',
  'players',
  'teams',
  'series',
  'live-match-scorecard',
  'live-match-commentary',
  'live-match-highlights',
  'live-match-matchinfo',
  'live-match-articles',
  'live-match-videos',
  'completed-match-scorecard',
  'completed-match-commentary',
  'completed-match-highlights',
  'completed-match-matchinfo',
  'completed-match-articles',
  'completed-match-videos',
  'completed-match-summary',
  'abandoned-match-matchinfo',
  'abandoned-match-articles',
  'upcoming-match-commentary',
  'upcoming-match-matchinfo',
  'upcoming-match-articles',
  'upcoming-match-videos'
];

export const indexSiteMapSlugs = [
  { slug: '/criclytics', priority: 1 },
  { slug: '/schedule/upcoming-matches', priority: 1 },
  { slug: '/schedule/live-matches', priority: 1 },
  { slug: '/schedule/results', priority: 1 },
  { slug: '/schedule/international/results', priority: 1 },
  { slug: '/schedule/international/live-matches', priority: 1 },
  { slug: '/schedule/international/upcoming-matches', priority: 1 },
  { slug: '/schedule/domestic/results', priority: 1 },
  { slug: '/schedule/domestic/live-matches', priority: 1 },
  { slug: '/schedule/domestic/upcoming-matches', priority: 1 },
  { slug: '/series', priority: 1 },
  { slug: '/series/international', priority: 1 },
  { slug: '/series/domestic', priority: 1 },
  { slug: '/news/latest', priority: 1 },
  { slug: '/news', priority: 1 },
  { slug: '/news/on-this-day', priority: 1 },
  { slug: '/news/match-related', priority: 1 },
  { slug: '/news/features', priority: 1 },
  { slug: '/players', priority: 1 },
  { slug: '/teams', priority: 1 },
  { slug: '/videos/latest', priority: 1 },
  { slug: '/videos/fantasy', priority: 1 },
  { slug: '/videos/matchrelated', priority: 1 },
  { slug: '/videos/news', priority: 1 },
  { slug: '/stadiums', priority: 1 },
  { slug: '/rankings', priority: 1 },
  { slug: '/records', priority: 1 },
  { slug: '/fantasy-research-center/upcoming', priority: 1 },
  { slug: '/fantasy-research-center/live', priority: 1 },
  { slug: '/fantasy-research-center/completed', priority: 1 },
  { slug: '/ipl-2021/teams', priority: 1 },
  { slug: '/ipl-2021/players', priority: 1 },
  { slug: '/ipl-2021/commentary', priority: 1 },
  { slug: '/news/live-blog',priority:1}
];

export const SERIES_TABS = ['squads', 'matches', 'news', 'stadiums', 'stats', 'points-table'];
