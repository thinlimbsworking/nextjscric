import React, { useState } from 'react'
import Heading from '../../shared/heading'
import { TEAM_ANALYSIS_HOMEPAGE } from '../../../api/queries'
import { useQuery } from '@apollo/react-hooks'
// import { useState } from 'react/cjs/react.production.min';

export default function TeamAnalysis(props) {
  const [showList, setSHowList] = useState(0)

  // prgram to check if a number is a float or integer value

  function checkNumber(x) {
    // check if the passed value is a number
    if (typeof x == 'number' && !isNaN(x)) {
      if (Number.isInteger(x)) {
        return 'number'
      } else {
        return 'float'
      }
    } else {
    }
  }

  const lableObject = [
    {
      lable: 'Overall',
      key: 'overallRating',
    },

    {
      lable: 'Vs Spin Bowling',
      key: 'battingVSSpin',
    },
    {
      lable: 'Batting Depth',
      key: 'battingDepth',
    },
    {
      lable: 'Pace Bowling',
      key: 'paceBowling',
    },
    {
      lable: 'Vs Pace Bowling',
      key: 'battingVSPace',
    },
    {
      lable: 'Spin Bowling',
      key: 'spinBowling',
    },
  ]

  let { loading, error, data } = useQuery(TEAM_ANALYSIS_HOMEPAGE, {
    variables: { matchID: props.matchID },
  })
  if (error) return <div></div>
  if (loading) return <div></div>
  if (data) {
  
    return (
      <div className="flex w-full  flex-col  p-4 ">
        <div className="flex bg-gray-4  w-full rounded-md items-center justify-evenly p-1">
          <div className=" font-semibold lg:text-xl  capitalize   font-ttb p-1  text-left w-full ml-2">
            Team Analysis{' '}
          </div>
          <div
            className={`pr-2  flex items-end justify-end  w-full text-right  `}
            onClick={() => setSHowList(showList == 0 ? 5 : 0)}
          >
            {' '}
            <img className="h-5" src={'/pngsV2/upArrowWhite.png'} />{' '}
          </div>
        </div>

        <div className="flex flex-col items-center justify-center bg-gray">
          <div className="flex items-center justify-center   w-full border-b-2 border-gray-4 ">
            <div className="w-4/12 flex items-center justify-center"></div>
            <div className="w-4/12 flex items-center justify-center border-r border-l border-gray-4 text-xs">
              {data.getTeamAnalysis.teamA.teamShortName}
            </div>

            <div className="w-4/12 flex items-center justify-center  text-xs">
              {data.getTeamAnalysis.teamB.teamShortName}
            </div>
          </div>
          <div className="flex items-center justify-center flex-col w-full">
            {[1, 2, 3, 4, 5, 6].map((item, index) => {
              return (
                showList >= index && (
                  <div className="flex text-xs items-center border-b-2 border-gray-4 justify-center   w-full ">
                    <div className="w-4/12 flex items-center justify-start text-md  font-sans pl-2  ">
                      {lableObject[index].lable}{' '}
                    </div>
                    <div className="w-4/12 flex items-center justify-center border-r border-l border-gray-4">
                      {/* {parseFloat(data.getTeamAnalysis.teamA[lableObject[index].key])} */}
                      {[1, 2, 3, 4, 5].map((num, innerKey) =>
                        data.getTeamAnalysis.teamA[item.key] > innerKey &&
                        data.getTeamAnalysis.teamA[item.key] < innerKey + 1 ? (
                          'half'
                        ) : data.getTeamAnalysis.teamA[item.key] > innerKey ? (
                          <img
                            className="mx-1 h-3 w-4"
                            src={'/pngsV2/re_fstarwhite.png'}
                          />
                        ) : (
                          <img
                            className="mx-1 h-3 w-4"
                            src={'/pngsV2/re_fstarblack.png'}
                          />
                        ),
                      )}
                    </div>

                    <div className="w-4/12 flex items-center justify-center ">
                      {lableObject.map((item, key) =>
                        showList || key == 0 ? (
                          <div key={item.key}>
                            <div></div>
                            <div className="flex justify-evenly py-2 border-x border-gray-8 dark:border-basebg">
                              {[1, 2, 3, 4, 5].map((num, innerKey) =>
                                data.getTeamAnalysis.teamB[item.key] >
                                  innerKey &&
                                data.getTeamAnalysis.teamB[item.key] <
                                  innerKey + 1 ? (
                                  <img
                                    className="mx-1 h-3 w-4"
                                    src={'/pngsV2/re_hstarwhite.png'}
                                  />
                                ) : data.getTeamAnalysis.teamB[item.key] >
                                  innerKey ? (
                                  <img
                                    className="mx-1 h-3 w-4"
                                    src={'/pngsV2/re_fstarwhite.png'}
                                  />
                                ) : (
                                  <img
                                    className="mx-1 h-3 w-4"
                                    src={'/pngsV2/re_fstarblack.png'}
                                  />
                                ),
                              )}
                            </div>

                            {/* <div  className='flex justify-evenly'>
                  {[1, 2, 3, 4, 5].map((num, innerKey) =>
                    data.getTeamAnalysis.teamB[item.key] > innerKey &&
                    data.getTeamAnalysis.teamB[item.key] < innerKey + 1 ? (
                      "half"
                      ) : (
                        data.getTeamAnalysis.teamB[item.key] > innerKey?'fW':'EB'
                      ),
                  )}
                </div> */}
                          </div>
                        ) : (
                          <div></div>
                        ),
                      )}

                      {/* {false && [1,2,3,4,5].map((item,index)=>{return( 
      parseFloat(data.getTeamAnalysis.teamA[lableObject[index].key])>=item ?<>
   
   <img className='m-1' src={'/pngsV2/re_fstarwhite.png'} />
   </>:<><img className='m-1' src={'/pngsV2/re_hstarwhite.png'} /></>)}
   )
} */}
                    </div>
                  </div>
                )
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}
