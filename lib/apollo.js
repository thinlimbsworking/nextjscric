import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  gql,
  ApolloLink,
  HttpLink,
} from '@apollo/client'
import fetch from 'isomorphic-unfetch'

// const client = new ApolloClient({
// 	uri: 'https://apiv2.cricket.com/cricket',
// 	cache: new InMemoryCache(),
//   });

const client = new ApolloClient({
  link: ApolloLink.split(
    (operation) => operation.getContext().clientName === 'commentary', // Routes the query to the proper client
    new HttpLink({
      uri: 'https://apiv2.cricket.com/cricket/commentary',
      // uri: 'https://api.devcdc.com/cricket/commentary',

      // uri: 'https://5d9b410e168d.ngrok.io',
      fetch,
    }),
    new HttpLink({
      uri: 'https://apiv2.cricket.com/cricket',
      // uri: 'https://api.devcdc.com/cricket',
      // uri: 'https://5d9b410e168d.ngrok.io',

      fetch,
    }),
  ),
  cache: new InMemoryCache({
    addTypename: false,
  }),
})
export default client
