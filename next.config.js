// module.exports={
//   images:{
//     formats: ['image/avif', 'image/webp'],
//   domains:['img.youtube.com','images-devcdc.imgix.net','images.cricket.com','ayushi-upload-image.s3.ap-south-1.amazonaws.com','lh3.googleusercontent.com']

//   }
// }

module.exports = {
  images: {
    // loader:"imgix",
    // path:'https://images-cricketcom.imgix.net',
    // formats: ['image/avif', 'image/webp'],

    // deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],

    domains: [
      'img.youtube.com',
      'images-devcdc.imgix.net',
      'images.cricket.com',

      'images-cricketcom.imgix.net',
      'https://images-cricketcom.imgix.net',
      'lh3.googleusercontent.com',
      'ayushi-upload-image.s3.ap-south-1.amazonaws.com',
    ],
  },
  compiler: {
    removeConsole: process.env.NODE_ENV === 'production',
  },
  reactStrictMode: true,
  env: {
    API: 'https://apiV2.cricket.com/cricket',
    CLEVERTAP_ID: 'TEST-94W-RR9-965Z',
    // API: 'https://api.devcdc.com/cricket',
  },
  // distDir: 'build',
  reactStrictMode: true,
  experimental: { appDir: true },
}

// import('next').NextConfig
// const nextConfig = {
//   output: 'standalone',
//   images: {
//     formats: ['image/avif', 'image/webp'],
//     domains: [
//       'img.youtube.com',
//       'images-devcdc.imgix.net',
//       'images.cricket.com',
//       'ayushi-upload-image.s3.ap-south-1.amazonaws.com',
//       'lh3.googleusercontent.com',
//     ],
//   },
// }
// module.exports = nextConfig
