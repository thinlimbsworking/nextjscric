import React from 'react';
import { HOME_PAGE_GET_ALGO11 } from '../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useRef, useState} from 'react'
const arrow = '/pngsV2/fantasy_Icon.png';
import { useRouter } from 'next/navigation';

export default function BuildYourTeam(props) {
  let scrl = useRef(null);
  const [scrollX, setscrollX] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);

  //Slide click
  const slide = (shift) => {
    scrl.current.scrollLeft += shift;
    setscrollX(scrollX + shift);

    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  const scrollCheck = () => {
    setscrollX(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  
 
  const firstItemRef = useRef(null);
  const router = useRouter();
  const navigate = router.push;
  const { loading, error, data } = useQuery(HOME_PAGE_GET_ALGO11, {
    variables: { matchID: props.matchID }
  });
  // console.log("matchIDmatchIDmatchIDmatchIDmatchIDmatchID====>>>",data&&data.homePageGetAlgo11)
  const getUpdatedTime = (updateTime) => {

    let currentTime = new Date().getTime();
    let distance = currentTime - updateTime;
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let sec = Math.floor((distance % (1000 * 60)) / 1000);

    let result =
      minutes > 0
        ? (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
          minutes +
          (minutes > 0 ? ' mins ' : ' min ') +
          ' ago'
        : (sec > 0 ? sec + ' secs' : ' sec') + ' ago';
    return result;
  }; <style jsx>{`
       
  -webkit-scrollbar {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height:4px;
      background-color:#2B323F
    }
    ::-webkit-scrollbar:hover {
      -webkit-appearance: none;
      width: 2px;
      border-radius: 2px;
      height:4px;
      background-color:#2B323F
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 2px;
      background-color: #6A91A9;
      
    }
`}</style>

  return data && data.homePageGetAlgo11.batsman ? (
    <div className=' items-center justify-center border-blue-4 h-96 border-y-3 border-r-3 rounded-r-3xl mt-5 relative text-white px-4  py-3  absolute'>
      <div className='flex justify-start items-start '>
        <div>
          <div className='font-semibold text-lg capitalize'>Fantasy Centre</div>
          <div className='w-14 bg-blue-8 h-1 rounded-full mt-1'></div>
        </div>
        {/* <img className='w-10' src={arrow} /> */}
      </div>
      <div className='pt-4'>
        <div className='font-semibold text-sm'>Cricket.com Teams </div>
        <div className='text-sm font-light pt-1'>Pick from our expert curated teams</div>
      </div>
      {/* {console.log("ddede",data.homePageGetAlgo11)} */}
      <div className='pt-2 text-xs text-gray-2 font-light'>
        *Last Updated {getUpdatedTime(data.homePageGetAlgo11.timestamp)}
      </div>

      <div className='absolute right-3 top-5  ' onClick={()=>navigate(`fantasy-research-center/${props.matchID}/${props.matchName.toLowerCase()}`)}>
        <div className='text-green bg-gray-8 border-green border p-1 px-2 text-sm font-medium rounded-md shadow-green shadow-sm '>
          Build Team
        </div>
      </div>
      <div className='flex justify-center w-10/12 ml-10'>

      <div className="flex  items-center justify-center" onClick={() => slide(-50)}>
          <svg width='30' focusable='false' viewBox='0 0 24 24'>
            <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
            <path fill='none' d='M0 0h24v24H0z'></path>
          </svg>
        </div>
      <div className=' w-full bg-gray-4   my-3 rounded-md'>
        <div className=''>
          <div className='p-2 flex items-center justify-center'>
            <div className='text-xs font-medium'>PROJECTED SCORE :</div>
            <div className='text-green pl-1 text-sm font-medium'> {data.homePageGetAlgo11.totalProjectedPoints}</div>
          </div>
        
    
          <div className='flex  rounded-lg p-1 overflow-x-auto w-full   overflow-x-auto'  ref={scrl} onScroll={scrollCheck}>
        
          <div className="flex bg-gray  ">
              {data &&
                data.homePageGetAlgo11 &&
                data.homePageGetAlgo11.batsman &&
                data.homePageGetAlgo11.batsman.map((item, y) => (
                  <div className=' mr-2  border-r-2 border-gray-4   p-2 px-4  w-32' >
                    <div className='flex w-full justify-center items-center'>
                      <div className='flex flex-col items-center '>
                        <div className='relative mb-2'>
                        
                          <img
                            className='h-16 w-16 bg-gray-4= rounded-full  object-cover object-top'
                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <img
                            className='w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                          />
                          {(item.captain == '1' || item.vice_captain == '1') && (
                            <div className='w-5 h-5 absolute right-[-4px] bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium'>
                              {item.captain == '1' ? 'C' : item.vice_captain == '1' ? 'VC' : ''}
                            </div>
                          )}
                        </div>
                        <p
                          className='text-xs text-center font-medium leading-5 w-full wordWrapLine'
                          style={{ wordWrap: 'break-word' }}>
                          {item.playerName}
                        </p>
                        <p className='text-gray-2 text-xs uppercase'>Batter</p>
                      </div>
                      {/* <div className='bg-black w-[1px] h-10'></div> */}
                    </div>
                  </div>
                ))}
                {data &&
                data.homePageGetAlgo11 &&
                data.homePageGetAlgo11.keeper &&
                data.homePageGetAlgo11.keeper.map((item, y) => (
                  <div className=' mr-2  border-r-2 border-gray-4   p-2 px-4  w-32' >
                    <div className='flex flex-col items-center '>
                      <div className='relative mb-3 '>
                      <img
                            className='h-16 w-16 bg-gray-4 rounded-full  object-cover object-top'
                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <img
                            className='w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                          />
                          {(item.captain == '1' || item.vice_captain == '1') && (
                            <div className='w-5 h-5 absolute right-[-4px] bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium'>
                              {item.captain == '1' ? 'C' : item.vice_captain == '1' ? 'VC' : ''}
                            </div>
                          )}
                      </div>
                      <p className='text-xs text-center font-medium leading-5'>{item.playerName}</p>
                      <p className='text-gray-2 text-xs uppercase'>Keeper</p>
                    </div>
                  </div>
                ))}
              {data &&
                data.homePageGetAlgo11 &&
                data.homePageGetAlgo11.bowler &&
                data.homePageGetAlgo11.bowler.map((item, y) => (
                  <div className=' mr-2  border-r-2 border-gray-4   p-2 px-4  w-32' >
                    <div className='flex flex-col items-center'>
                      <div className='relative mb-3 '>
                      <img
                            className='h-16 w-16 bg-gray-4 rounded-full  object-cover object-top'
                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <img
                            className='w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                          />
                          {(item.captain == '1' || item.vice_captain == '1') && (
                            <div className='w-5 h-5 absolute right-[-4px] bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium'>
                              {item.captain == '1' ? 'C' : item.vice_captain == '1' ? 'VC' : ''}
                            </div>
                          )}
                      </div>
                      <p className='text-xs font-medium leading-5 text-center w-full wordWrapLine'>{item.playerName}</p>
                      <p className='text-gray-2 text-xs uppercase'>Bowler</p>
                    </div>
                  </div>
                ))}
              {data &&
                data.homePageGetAlgo11 &&
                data.homePageGetAlgo11.bowler &&
                data.homePageGetAlgo11.all_rounder.map((item, y) => (
                  <div className=' mr-2  border-r-2 border-gray-4   p-2 px-4  w-32' >
                    <div className='flex flex-col items-center '>
                      <div className='relative mb-3 '>
                      <img
                            className='h-16 w-16 bg-gray-4 rounded-full  object-cover object-top'
                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <img
                            className='w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba'
                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                            onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                          />
                          {(item.captain == '1' || item.vice_captain == '1') && (
                            <div className='w-5 h-5 absolute right-[-4px] bottom-0 rounded-full ba flex justify-center items-center text-xs font-medium'>
                              {item.captain == '1' ? 'C' : item.vice_captain == '1' ? 'VC' : ''}
                            </div>
                          )}
                      </div>
                      <p className='text-xs font-medium text-center leading-5'>{item.playerName}</p>
                      <p className='text-gray-2 text-xs uppercase'>All rounder</p>
                    </div>
                  </div>
                ))}
              

              {/* bowler */}
            </div>
          
          </div>
             
       
          
        </div>
    
       
      </div>
      <div className=" flex items-center justify-center" onClick={() => slide(+50)}>
       <svg width='30' viewBox='0 0 24 24'>
            <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
            <path fill='none' d='M0 0h24v24H0z'></path>
          </svg>
        </div>
    </div>
    </div>
  ) : (
    <></>
  );
}


