import React from 'react'
const Dnf= '/pngsV2/empty.png'
export default function DataNotFound() {
  return (<div className='flex w-full flex-col items-center justify-center '>
        <div>   <img className='' src={Dnf} alt="" /></div>
     
<div className='text-center text-white'>Data not available</div>
    </div>
  )
}
