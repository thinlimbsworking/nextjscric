import React, { useState } from 'react';
import Winwiz from '../winwiz';
import DATA from '../commom/datanotfound';
import Heading from '../commom/heading';

const information = '/svgs/information.svg';
const ground = '/svgs/groundImageWicket.png';
const down_arrow = '/svgs/down-arrow.png';
const up_arrow = '/svgs/up-arrow.png';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';

export default function GameChangingOvers({ data, browser, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false);
  const [click, setClick] = useState(true);
  const [overno, setOver] = useState(0);
  const [showCommentary, setshowCommentary] = useState(false);
  const [deskCommentary, setDeskCommentary] = useState(0);

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };
// console.log("olddd",data)
  return (
    <div>
     
      <div className='mx-3'>
      <Heading heading={'Game Changing Overs'} subHeading={'Forecast of where the match is headed, in the next few overs'} />

        {/* <div className='text-md  text-white tracking-wide font-bold uppercase '>Game Changing Overs</div>
        <div className='text-xs text-white pt-1 font-medium tracking-wide'>
          {' '}
          Forecast of where the match is headed, in the next few overs{' '}
        </div>
        <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
      </div>
      {data && data.gameChangingOvers.matchID ? (
        <div>
          <div className='flex justify-between items-center pa2 ph3-l bg-white mv2-l mt2 shadow-4'>
            <h1 className='f7 f5-l fw5 grey_10 ttu'>Game changing overs</h1>
            <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1 h13-l w13-l' />
          </div>
          <div className='outline light-gray' />
          {infoToggle && (
            <div className='bg-white'>
              <div className='f7 grey_8 pa2'>
                A collection of four overs which according to Criclytics changed the game, with a detailed breakdown of
                what happened in each ball of these overs
              </div>
              <div className='outline light-gray' />
            </div>
          )}
          <div className='flex-l justify-between'>
            <div className='w-49-l'>
              {data.gameChangingOvers.overs.map((over, i) => (
                <div key={i} className='mb2 br2 cursor-pointer'>
                  <div className='bg-white shadow-4'>
                    <div
                      key={i}
                      className={`pa2 pa3-l ${overno === i || !click ? 'o-100' : 'o-80'} ${
                        overno === i ? 'ba  b--red bw1' : ''
                      }`}
                      onClick={() => (setClick(true), setOver(i), setDeskCommentary(i), setshowCommentary(false))}>
                      <div className='flex items-center'>
                        <div className='pa1 br2 ' style={{ background: '#141B2F', minWidth: '50px' }}>
                          <p className='tc ma0 fw7 white f4 ph2 pv1'>{over.overNo || ''}</p>
                          <div className='divider' />
                          <p className='tc ma0 white f8 fw5 pa1'>over </p>
                        </div>
                        <div className='flex  overflow-x-scroll overflow-y-hidden hidescroll pv2-l'>
                          {over.commentaryOver.map((ball, i) => (
                            <div key={i} className='tc ml2 '>
                              {ball.wicket ? (
                                <div className='flex justify-center items-center ba f6 fw7 black w2 h2 br-100 bg-cdc white b--cdc'>
                                  W
                                </div>
                              ) : (
                                <div
                                  className={`ba flex justify-center items-center f6 fw7 black w2 h2 br-100 ${
                                    ball.runs === '4'
                                      ? 'bg-green white b--green'
                                      : ball.runs === '6'
                                      ? 'bg-blue white b--blue'
                                      : 'bg-white '
                                  } `}>
                                  {ball.type === 'no ball'
                                    ? 'nb'
                                    : ball.type === 'leg bye'
                                    ? 'lb'
                                    : ball.type === 'bye'
                                    ? 'b'
                                    : ball.type === 'wide'
                                    ? 'wd'
                                    : ''}
                                  {ball.type === 'no ball' ||
                                  ball.type === 'leg bye' ||
                                  ball.type === 'bye' ||
                                  ball.type === 'wide'
                                    ? ball.runs >= 1
                                      ? ball.runs
                                      : ''
                                    : ball.runs}
                                </div>
                              )}
                              <p className='f8 black-30 pt1 ma0 fw6'>{ball.over}</p>
                            </div>
                          ))}
                        </div>
                      </div>
                      <Winwiz
                        click={click}
                        isAnimate={i === overno}
                        pollData={over.lastBallPredictionData}
                        teamName={over.teamShortName}
                        score={over.score}
                      />
                    </div>
                    {click && i === overno && (
                      <div className='flex justify-center pv1 dn-l'>
                        <img
                          alt=''
                          src={showCommentary ? up_arrow : down_arrow}
                          className='pa2 shadow-4 bg-white w1'
                          onClick={() => setshowCommentary(!showCommentary)}
                        />
                      </div>
                    )}
                  </div>
                  <div className='dn-l'>
                    {click && i === overno && showCommentary && (
                      <div className='bg-white'>
                        <div className='bg-navy pa2 flex justify-between items-center white'>
                          <div className='flex justify-between items-center'>
                            <img
                              className='w2 h1-3 shadow-4'
                              src={`https://images.cricket.com/teams/${over.teamID}_flag_safari.png`}
                              onError={(evt) => (evt.target.src = flagPlaceHolder)}
                              alt=''
                            />
                            <div className=' f5 fw5 ph2'>{over.teamShortName}</div>
                          </div>
                          <div className='f6 fw4'> INN {over.inningNo} </div>
                        </div>
                        {over.commentary.reverse().map((x, z) => (
                          <div key={z}>
                            <div className='flex justify-start items-start pa2 black'>
                              <div
                                className={`pa1 br2 ba ${
                                  x.type === 'wicket'
                                    ? 'bg-cdc white'
                                    : x.type === 'four'
                                    ? 'bg-green white b--green'
                                    : x.type === 'six'
                                    ? 'bg-blue white b--blue'
                                    : ''
                                }`}
                                style={{ minWidth: '50px' }}>
                                <p className='tc ma0 fw7 f4 ph2 pv1'>{x.wicket ? 'W' : x.runs}</p>
                                <div className='divider' />
                                <p className='tc ma0 f8 fw5 pa1'>{x.over}</p>
                              </div>
                              <div className='f6 pl2'>
                                <span className='fw6'>{x.bowlerName}</span>
                                <span> to </span>
                                <span className='fw6'>{x.batsmanName}</span>
                                <span> {x.comment}</span>
                              </div>
                            </div>
                            <div className='divider' />
                          </div>
                        ))}
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
            <div className='dn db-l w-49-l'>
              {
                <div className='bg-white'>
                  <div className='bg-navy pa2 flex justify-between items-center white'>
                    <div className='flex justify-between items-center'>
                      <img
                        className='w2 h1-3 shadow-4'
                        src={`https://images.cricket.com/teams/${data.gameChangingOvers.overs[deskCommentary].teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        alt=''
                      />
                      <div className=' f5 fw5 ph2'>{data.gameChangingOvers.overs[deskCommentary].teamShortName}</div>
                    </div>
                    <div className='f6 fw4'> INN {data.gameChangingOvers.overs[deskCommentary].inningNo} </div>
                  </div>
                  {data.gameChangingOvers.overs[deskCommentary].commentary.map((x, z) => (
                    <div key={z} className='flex justify-start items-start pa2 black'>
                      <div
                        className={`pa1 br2 ba ${
                          x.type === 'wicket'
                            ? 'bg-cdc white'
                            : x.type === 'four'
                            ? 'bg-green white b--green'
                            : x.type === 'six'
                            ? 'bg-blue white b--blue'
                            : ''
                        }`}
                        style={{ minWidth: '50px' }}>
                        <p className='tc ma0 fw7 f4 ph2 pv1'>{x.wicket ? 'W' : x.runs}</p>
                        <div className='divider' />
                        <p className='tc ma0 f8 fw5 pa1'>{x.over}</p>
                      </div>
                      <div className='f6 pl2'>
                        <span className='fw6'>{x.bowlerName}</span>
                        <span> to </span>
                        <span className='fw6'>{x.batsmanName}</span>
                        <span> {x.comment}</span>
                      </div>
                    </div>
                  ))}
                </div>
              }
            </div>
          </div>
        </div>
      ) : (
        <div className='h-screen w-100 mt2 shadow-4 pa4'>
          <DATA />
        </div>
      )}
    </div>
  );
}
