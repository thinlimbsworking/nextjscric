import React, { useEffect, useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { CRICLYTICS_FORM_INDEX } from '../../../api/queries';
import DataNotFound from '../../commom/datanotfound';
import Heading from '../../commom/heading';

export default function formIndex(props) {
  const [toggle, setToggle] = useState('');
  const [teamKey, setTeamKey] = useState('teamOne');
  const { loading, error, data } = useQuery(CRICLYTICS_FORM_INDEX, {
    // variables: { matchID: props.matchID},
    variables: { matchID: props.matchID },
    onCompleted: (data) => {
      setToggle((data && data.getFormIndex && data.getFormIndex.teamOne && data.getFormIndex.teamOne.name) || []);
    }
  });

  const capsuleCss = {
    verygood: 'h-2 w-2 rounded-full bg-green',
    good: 'h-2 w-2 rounded-full bg-green-3',
    neutral: 'h-2 w-2 rounded-full bg-yellow',
    bad: 'h-2 w-2 rounded-full bg-orange-3',
    verybad: 'h-2 w-2 rounded-full bg-red'
  };
  const capsuleCssColor = {
    verygood: ' text-green text-xs font-bold',
    good: ' text-xs  text-green-4  font-bold',
    neutral: ' text-xs text-yellow  font-bold',
    bad: ' text-orange-3  text-xs  font-bold',
    verybad: 'text-red text-xs font-bold'
  };

  return (
    <div className='md:mt-12  w-full'>
     
      <div className='mx-3 mt-3'>
      <Heading heading={'Form Index'} subHeading={'An accurate analysis of players from both teams based on their form from recent matches'} />
      {/* <div className='  m w-full'>
        <div className='text-md  text-white tracking-wide font-bold '>  Form Index</div>
        <div className='text-md  text-white tracking-wide font-bold '>   An accurate analysis of players from both teams based on their form from recent matches</div>
       
      <div className='w-12 h-1 bg-blue-8 mt-2'></div>
      </div> */}
        <div className='  mt-4  text-white  text-sm font-semibold '>Current Team Form</div>
        <div className='w-12 h-1 bg-blue-8 mt-2'></div>
      </div>

<div className='hidden lg:block md:block'>
      {data && data.getFormIndex && data.getFormIndex.teamOne ?
       <div className='flex flex-col items-center justify-center text-sm     '>
         <div className='flex w-6/12 items-center justify-center  bg-gray-4 rounded-3xl'>
            <div
              className={`w-1/2 text-center text-white cursor-pointer rounded-3xl py-1.5 text-xs ${
                toggle === data.getFormIndex.teamOne.name ? `border-2  border-green` : ''
              }`}
              onClick={() => (setToggle(data.getFormIndex.teamOne.name), setTeamKey('teamOne'))}>
              {data.getFormIndex.teamOne.name}
            </div>
            <div
              className={`w-1/2 text-center rounded-3xl text-white cursor-pointer py-1.5 text-xs  ${
                toggle === data.getFormIndex.teamTwo.name ? `border-2  border-green` : ''
              }`}
              onClick={() => (setToggle(data.getFormIndex.teamTwo.name), setTeamKey('teamTwo'))}>
              {data.getFormIndex.teamTwo.name}
            </div>
            </div>
          
          <div className='flex justify-between pt-3'>
            <div className='flex items-center'>
              <p className='h-2 w-2 m-1 rounded-full bg-green'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Very Good</p>
            </div>
            <div className='flex items-center justify-center'>
              <p className='h-2 w-2 m-1 rounded-full bg-green-3'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Good</p>
            </div>
            <div className='flex items-center justify-center'>
              <p className='h-2 w-2 m-1 rounded-full bg-yellow'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Neutral</p>
            </div>

            <div className='flex items-center'>
              <p className='h-2 w-2 m-1 rounded-full bg-orange-3'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Bad</p>
            </div>
            <div className='flex items-center'>
              <p className='h-2 w-2 m-1 rounded-full bg-red'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Very Bad</p>
            </div>
          </div>
          </div>:''}
           </div>

      {data && data.getFormIndex && data.getFormIndex.teamOne ? (
        <div className=' mx-1 md:mt-0 mt-5 rounded px-3 py-3 text-white '>
          <div className='lg:hidden md:hidden bg-gray-4 rounded-3xl  flex items-center justify-between text-sm  '>
            <div
              className={`w-1/2 text-center rounded-3xl py-1.5 text-xs ${
                toggle === data.getFormIndex.teamOne.name ? `border-2  border-green` : ''
              }`}
              onClick={() => (setToggle(data.getFormIndex.teamOne.name), setTeamKey('teamOne'))}>
              {data.getFormIndex.teamOne.name}
            </div>
            <div
              className={`w-1/2 text-center rounded-3xl py-1.5 text-xs  ${
                toggle === data.getFormIndex.teamTwo.name ? `border-2  border-green` : ''
              }`}
              onClick={() => (setToggle(data.getFormIndex.teamTwo.name), setTeamKey('teamTwo'))}>
              {data.getFormIndex.teamTwo.name}
            </div>
          </div>
          <div className='flex justify-between pt-3 lg:hidden md:hidden'>
            <div className='flex items-center'>
              <p className='h-2 w-2 m-1 rounded-full bg-green'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Very Good</p>
            </div>
            <div className='flex items-center justify-center'>
              <p className='h-2 w-2 rounded-full bg-green-3'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Good</p>
            </div>
            <div className='flex items-center justify-center'>
              <p className='h-2 w-2 rounded-full bg-yellow'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Neutral</p>
            </div>

            <div className='flex items-center'>
              <p className='h-2 w-2 rounded-full bg-orange-3'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Bad</p>
            </div>
            <div className='flex items-center'>
              <p className='h-2 w-2 rounded-full bg-red'></p>
              <p className='text-gray-2 text-xs font-normal pl-1.5'>Very Bad</p>
            </div>
          </div>
          <div className='flex flex-col'>
            <div className='flex flex-col'>
          {
            <div>
              {data.getFormIndex.teamOne.batsmen && <div className='flex gray-2 text-xs mt3 uppercase'> BATTERS</div>}
              {teamKey == 'teamOne'
                ? data.getFormIndex.teamOne.batsmen &&
                  data.getFormIndex.teamOne.batsmen.map((player, y) => {
                    return (
                      <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                        <div className='flex items-center justify-between'>
                          <img
                            className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                            src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <div className='pl-3'>
                            <p className='text-sm font-medium '>{player.playerName}</p>
                            <p className='text-gray-2 text-[11px] font-medium '>
                              BAT SR: {player.BatSR} | BAT AVG: {player.BatAvg}
                            </p>
                          </div>
                        </div>
                        <div>
                          <div className='flex items-center'>
                            <p
                              className={`${
                                capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs mr-1`}>
                              {player.OverallForm}
                            </p>
                            <p
                              className={`${
                                capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs`}></p>
                          </div>
                        </div>
                      </div>
                    );
                  })
                : data.getFormIndex.teamTwo.batsmen &&
                  data.getFormIndex.teamTwo.batsmen.map((player, y) => {
                    return (
                      <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                        <div className='flex items-center justify-between'>
                          <img
                            className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                            src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <div className='pl-3'>
                            <p className='text-sm font-medium '>{player.playerName}</p>
                            <p className='text-gray-2 text-[11px] font-medium '>
                              BAT SR: {player.BatSR} | BAT AVG: {player.BatAvg}
                            </p>
                          </div>
                        </div>
                        <div>
                          <div className='flex items-center'>
                            <p
                              className={`${
                                capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs mr-1`}>
                              {player.OverallForm}
                            </p>
                            <p
                              className={`${
                                capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs`}></p>
                          </div>
                        </div>
                      </div>
                    );
                  })}
            </div>
          }
          {
            <div>
              {teamKey == 'teamOne' ? (
                <>
                  {data.getFormIndex.teamOne.keepers && (
                    <div className='flex gray-2 text-xs mt3 uppercase'>WICKETS KEEPER</div>
                  )}
                  {data.getFormIndex.teamOne.keepers &&
                    data.getFormIndex.teamOne.keepers.map((player, y) => {
                      return (
                        <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                          <div className='flex items-center justify-between'>
                            <img
                              className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                            />
                            <div className='pl-3'>
                              <p className='text-sm font-medium pb-1'>{player.playerName}</p>
                              <p className='text-gray-2 text-[11px] font-medium '>
                                BAT SR: {player.BatSR} | BAT AVG: {player.BatAvg}
                              </p>
                            </div>
                          </div>
                          <div>
                            <div className='flex items-center'>
                              <p
                                className={`${
                                  capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs mr-1`}>
                                {player.OverallForm}
                              </p>
                              <p
                                className={`${
                                  capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs`}></p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </>
              ) : (
                <>
                  {data.getFormIndex.teamTwo.keepers && (
                    <div className='flex gray-2 text-xs mt3 uppercase'>WICKETS KEEPER</div>
                  )}

                  {data.getFormIndex.teamTwo.keepers &&
                    data.getFormIndex.teamTwo.keepers.map((player, y) => {return ( <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                          <div className='flex items-center justify-between'>
                            <img
                              className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                            />
                            <div className='pl-3'>
                              <p className='text-sm font-medium pb-1'>{player.playerName}</p>
                              <p className='text-gray-2 text-[11px] font-medium '>
                                BAT SR: {player.BatSR} | BAT AVG: {player.BatAvg}
                              </p>
                            </div>
                          </div>
                          <div>
                            <div className='flex items-center'>
                              <p
                                className={`${
                                  capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs mr-1`}>
                                {player.OverallForm}
                              </p>
                              <p
                                className={`${
                                  capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs`}></p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </>
              )}
            </div>
          }
          </div>
          <div className='flex flex-col'>
          {
            <div>
              {teamKey == 'teamOne' ? (
                <>
                  {data.getFormIndex.teamOne.allRounders && <div className='flex gray-2 text-xs mt3'>ALL ROUNDER</div>}
                  {data.getFormIndex.teamOne.allRounders &&
                    data.getFormIndex.teamOne.allRounders.map((player, y) => {
                      return (
                        <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                          <div className='flex items-center justify-between'>
                            <img
                              className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                            />
                            <div className='pl-3'>
                              <p className='text-sm font-medium pb-1'>{player.playerName}</p>
                              <p className='text-gray-2 text-[11px] font-medium '>
                                BAT SR: {player.BatSR} | BAT AVG: {player.BatAvg}
                              </p>
                            </div>
                          </div>
                          <div>
                            <div className='flex items-center'>
                              <p
                                className={`${
                                  capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs mr-1`}>
                                {player.OverallForm}
                              </p>
                              <p
                                className={`${
                                  capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs`}></p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </>
              ) : (
                <>
                  {' '}
                  {data.getFormIndex.teamTwo.allRounders && <div className='flex gray-2 text-xs mt3'>ALL ROUNDER</div>}
                  {data.getFormIndex.teamTwo.allRounders &&
                    data.getFormIndex.teamTwo.allRounders.map((player, y) => {
                      return (
                        <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                          <div className='flex items-center justify-between'>
                            <img
                              className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                              src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                            />
                            <div className='pl-3'>
                              <p className='text-sm font-medium pb-1'>{player.playerName} </p>
                              <p className='text-gray-2 text-[11px] font-medium '>
                                BAT SR: {player.BatSR} | BAT AVG: {player.BatAvg}
                              </p>
                            </div>
                          </div>
                          <div>
                            <div className='flex items-center'>
                              <p
                                className={`${
                                  capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs mr-1`}>
                                {player.OverallForm}
                              </p>
                              <p
                                className={`${
                                  capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                                } uppercase text-xs`}></p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </>
              )}
            </div>
          }
          {
            <div>
              <div className='flex gray-2 text-xs mt3 uppercase'> bowlers</div>
              {teamKey == 'teamOne'
                ? data.getFormIndex.teamOne.bowlers.map((player, y) => {
                    return (
                      <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                        <div className='flex items-center justify-between'>
                          <img
                            className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                            src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <div className='pl-3'>
                            <p className='text-sm font-medium '>{player.playerName}</p>
                            <p className='text-gray-2 text-[11px] font-medium'>
                              BOWL SR:
                              {player.BowlSR} | BOWL AVG: {player.BowlAvg}
                            </p>
                          </div>
                        </div>
                        <div>
                          <div className='flex items-center'>
                            <p
                              className={`${
                                capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs mr-1`}>
                              {player.OverallForm}
                            </p>
                            <p
                              className={`${
                                capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs`}></p>
                          </div>
                        </div>
                      </div>
                    );
                  })
                : data.getFormIndex.teamTwo.bowlers.map((player, y) => {
                    return (
                      <div className='bg-gray-4 rounded-lg mt-2 flex justify-between items-center p-2'>
                        <div className='flex items-center justify-between'>
                          <img
                            className='h-12 w-12 bg-gray object-top object-cover rounded-full'
                            src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/playerPH.png')}
                          />
                          <div className='pl-3'>
                            <p className='text-sm font-medium pb-1'>{player.playerName}</p>
                            <p className='text-gray-2 text-[11px] font-medium'>
                              BOWL SR:
                              {player.BowlSR} | BOWL AVG: {player.BowlAvg}
                            </p>
                          </div>
                        </div>
                        <div>
                          <div className='flex items-center'>
                            <p
                              className={`${
                                capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs mr-1`}>
                              {player.OverallForm}
                            </p>
                            <p
                              className={`${
                                capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]
                              } uppercase text-xs`}></p>
                          </div>
                          {/* <div className={`${capsuleCssColor[player.OverallForm.split(' ').join('').toLowerCase()]} uppercase text-xs`}>
                {player.OverallForm.toLowerCase()==='none'?'NOT AVAIBLE':player.OverallForm} 
                </div>
                <div className={`${capsuleCss[player.OverallForm.split(' ').join('').toLowerCase()]} h-2 w-2`}>
                  </div> */}
                        </div>
                      </div>
                    );
                  })}
            </div>
          }
          </div>
          </div>
        </div>
      ) : (
        <div className=' flex  mt-5'>
          <DataNotFound />
        </div>
      )}
    </div>
  );
}
