import React, { useEffect, useState } from 'react';
import SwiperModule from './swiperModule';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING } from '../../api/queries';
export default function Matchups(props) {
  const [toggle, setToggle] = useState(0);
  const [tabIndex, setTabIndex] = useState(0);
  const [bowler,setBowler] = useState(0)
  // console.log("MatchupsMatchupsMatchupsMatchups",props.teamIDs[toggle].teamID)

  const { loading, error, data } = useQuery(HOME_SCREEN_PLAYER_MATCHUPS_UPCOMING, {
    variables: { matchID: props.matchID}
  });
  const capsuleCss = {
    Strong: 'bg-green-2 border text-green rounded-3xl py-1 px-2 text-sm border-green font-bold text-center',
    Average: 'bg-yellow-2 border text-yellow rounded-3xl py-1 px-2 text-sm border-yellow font-bold text-center',
    Weak: 'bg-red-8 border text-red rounded-3xl py-1 px-3 text-sm border-red font-bold text-center'
  };

  return (
    data &&  data.HomeScreenPlayerMatchupsUpcomming && data.HomeScreenPlayerMatchupsUpcomming.preMatch ?<div className='mt-8 rounded mx-1 text-white'>
      <div className='flex justify-between items-center'>
        <div className='font-semibold text-xl tracking-wider '>Player MatchUp</div>
      </div>
      <div className=' text-gray-2    text-xs pr-3 font-medium tracking-wide'>
      Select a batter and see how they perform against opposing bowlers

      </div>

      <div className='flex w-full justify-between items-center mt-5 '>
        <div className=' w-3/12 text-gray-2 text-xs flex justify-between items-center  '>BATTING</div>

        <div className='bg-gray-4 rounded-3xl flex items-center justify-between text-sm  w-9/12 p-1'>
          <div
            className={`w-1/2 text-center rounded-3xl py-2  ${toggle === 0 ? `border-2  border-green bg-gray-8` : ''}`}
            onClick={() => (setTabIndex(0), setToggle(0))}>
            {data && data.HomeScreenPlayerMatchupsUpcomming.preMatch[0].homeTeamShortName}
          </div>
          <div
            className={`w-1/2 text-center rounded-3xl py-2 ${toggle === 1 ? `border-2  border-green bg-gray-8` : ''}`}
            onClick={() => (setTabIndex(0), setToggle(1))}>
            {data && data.HomeScreenPlayerMatchupsUpcomming.preMatch[1].awayTeamShortName}
          </div>
        </div>
      </div>

      <SwiperModule
        toggle={toggle}
        tabIndex={tabIndex}
        setTabIndex={setTabIndex}
        data={data && data.HomeScreenPlayerMatchupsUpcomming.preMatch}
      />

      <div className='flex bg-gray items-center justify-between rounded py-3'>
        <div className='flex w-1/3 flex-col  items-center justify-center  '>
          <div className='text-xs font-medium uppercase text-gray-2'>Balls </div>
          <div className='uppercase text-xl font-semibold  '>{data && data.HomeScreenPlayerMatchupsUpcomming&& data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] && data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[tabIndex].bowlers[bowler].ballsFaced}</div>
        </div>
        <div className='flex flex-col  w-1/3 items-center justify-center border-r-[1px] border-l-[1px] '>
          <div className='uppercase text-xs font-medium text-gray-2'>RUNS </div>
          <div className='uppercase text-xl font-semibold'>{  data && data.HomeScreenPlayerMatchupsUpcomming&& data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle] && data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[tabIndex].bowlers[bowler].runsScored}</div>
        </div>
        <div className='flex flex-col  w-1/3  items-center justify-center'>
          <div className='uppercase text-xs font-medium text-gray-2'>WICKETS </div>
          <div className='uppercase text-xl font-semibold'>{ data && data.HomeScreenPlayerMatchupsUpcomming&&data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle]&& data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[tabIndex].bowlers[bowler].wicket} </div>
        </div>
      </div>

      <div className='  flex flex-wrap justify-center  mt-4 items-center'>
        {data &&
          data.HomeScreenPlayerMatchupsUpcomming &&
          data.HomeScreenPlayerMatchupsUpcomming.preMatch &&
          data.HomeScreenPlayerMatchupsUpcomming.preMatch.length > 0 &&
          data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].mappings[tabIndex].bowlers.map((item,i) => {
            return (
              <div key={i} className='flex w-5/12 my-3 mx-3 justify-center bg-gray h-44 relative rounded-xl' onClick={()=>setBowler(i)}>
                <div className='flex flex-col relative items-center justify-center '>
                  <div className='flex relative items-center justify-center '>
                    <img
                      className='h-20 w-20 bg-gray-4 object-top object-cover rounded-full'
                      src={`https://images.cricket.com/players/${item.bowlerId}_headshot_safari.png`}
                    />

                    <img
                      className=' absolute h-6 w-6  border  left-0 bottom-0 object-fill'
                      src={'./pngsV2/bowlericon.png'}
                    />

                    <div className='absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center '>
                      <img
                        className='  h-5 w-5  rounded-full'
                        src={`https://images.cricket.com/teams/${data.HomeScreenPlayerMatchupsUpcomming.preMatch[toggle].homeTeamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />
                    </div>
                  </div>

                  <div className=' text-center  uppercase text-xs font-semibold mt-2 '>{item.name}</div>
                </div>

                <div className={`flex w-full items-center justify-center  absolute bottom-0 `}>
                  <div className={`absolute  ${capsuleCss[item.label]}`}>{item.label}</div>
                </div>
              </div>
            );
          })}
      </div>
    </div>:<></>
  );
}
