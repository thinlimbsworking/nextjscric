import React, { useRef, useState, useEffect } from 'react';

import { Swiper, SwiperSlide } from 'swiper/react';

import { EffectCoverflow, Pagination } from 'swiper';


export default function SwiperModule(props) {
  const [swiper, updateSwiper] = useState(null);

  useEffect(() => {
    // props.setUrlTrack(false);

    if (swiper !== null) {
      try {
        swiper.on('slideChange', () => {
          props.setTabIndex(swiper.realIndex);
        });
        swiper.on('click', () => {
          console.log(swiper);
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        });
      } catch (e) {
        console.log(e);
      }
    }
  }, [swiper]);

  // console.log("SwiperModule",props&&props.data&&props.data[0].mappings)
  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  return (
    <div className='my-5 relative flex justify-center items-center'>
      <Swiper
        navigation={{
          prevEl: navigationPrevRef.current,
          nextEl: navigationNextRef.current
        }}
        onSwiper={(swiper) => {
          // Delay execution for the refs to be defined
          setTimeout(() => {
            // Override prevEl & nextEl now that refs are defined
            // swiper.params.navigation.prevEl = navigationPrevRef.current;
            // swiper.params.navigation.nextEl = navigationNextRef.current;

            // Re-init navigation
            // swiper.navigation.destroy();
            // swiper.navigation.init();
            // swiper.navigation.update();
          });
        }}
        onSlideChange={(swiper) => updateSwiper(swiper)}
        shouldSwiperUpdate={true}
        effect={'coverflow'}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={'2'}
        initialSlide={'0'}
        coverflowEffect={{
          slideShadows: true,
          rotate: 0,
          stretch: 0,
          depth: 600,
          modifier: 0
        }}
        pagination={false}
        modules={[EffectCoverflow, Pagination]}
        className='mySwiper'>
        {props.data &&
          props.data[props.toggle].mappings.map((item) => {
            return (
              <SwiperSlide>
                <div className='bg-gray h-44 flex flex-col items-center justify-center rounded-lg '>
                  <div className='flex relative items-center justify-center '>
                    <img
                      className='h-20 w-20 bg-gray-4 object-top object-cover rounded-full'
                      src={`https://images.cricket.com/players/${item.batsmanId}_headshot_safari.png`}
                    />

                    <img className=' absolute h-6 w-6 left-0 bottom-0 object-fill' src={'./pngsV2/battericon.png'} />

                    {/* <img
                      className=' absolute h-5 w-5 border   right-0 bottom-0 object-fill rounded-full'
                      src={`https://images.cricket.com/teams/${props.data[0].homeTeamID}_flag_safari.png`}
                    /> */}

                    <div className='absolute w-6 h-6 bg-white right-0 bottom-0  rounded-full flex justify-center items-center'>
                      <img
                        className='  h-5 w-5  rounded-full'
                        src={`https://images.cricket.com/teams/${props.data[0].homeTeamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />
                    </div>
                  </div>
                  <div className=' text-center  uppercase text-xs font-semibold mt-2 '>{item.name}</div>
                </div>
              </SwiperSlide>
            );
          })}
      </Swiper>
      <div className='absolute absolute--fill flex justify-between items-center'>
        <div
          style={{ zIndex: 1000 }}
          ref={navigationPrevRef}
          id='swiper-button-prev'
          className='white cursor-pointer dn db-ns z-999 outline-0'>
          <svg width='30' focusable='false' viewBox='0 0 24 24'>
            <path fill='green' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
            <path fill='none' d='M0 0h24v24H0z'></path>
          </svg>
        </div>
        <div
          style={{ zIndex: 1000 }}
          ref={navigationNextRef}
          id='swiper-button-next'
          className='white cursor-pointer dn db-ns z-999 outline-0'>
          <svg width='30' viewBox='0 0 24 24'>
            <path fill='green' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
            <path fill='none' d='M0 0h24v24H0z'></path>
          </svg>
        </div>
      </div>
    </div>
  );
}
