import React, { useState, useEffect } from 'react'
const ground = '/svgs/groundImageWicket.png'
const information = '/svgs/information.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg'
export default function MatchStats({ data, browser, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false)
  const [innerWidth, updateInnerWidth] = useState(0)

  useEffect(() => {
    updateInnerWidth(window.innerWidth)
  }, [])

  const localStorage = {}

  const statsArr = [
    { key: 'highestBattingScore', value: 'Highest Score' },
    { key: 'highestPartnership', value: 'Highest Partnership' },
    { key: 'totalFours', value: '4s' },
    { key: 'totalSix', value: '6s' },
    { key: 'runsScoredInBoundaries', value: 'Runs Scored in Boundaries' },
    { key: 'totalDotBalls', value: 'Number of Dot Balls Faced' },
    { key: 'highestWickets', value: 'Best Bowling' },
    { key: 'extras', value: 'Extras Conceded' },
    { key: 'runRate', value: 'Run Rate in Inning' },
    { key: 'runRateInPowerplay1_6', value: 'Run Rate - Powerplay' }, // for T20
    { key: 'runRateInPowerplay1_10', value: 'Run Rate - Powerplay' }, // For ODI
    { key: 'runRateInPowerplay11_40', value: 'Run Rate in Mid-Over' }, // For ODI
    { key: 'runRateDeathOver', value: 'Run Rate - Death Over' },
  ]

  const toggleInfo = () => {
    setInfoToggle(true)
    setTimeout(() => {
      setInfoToggle(false)
    }, 6000)
  }
  if (!data)
    return (
      <div>
        <div className=" bg-white pv3 mt2">
          <div>
            <img
              className="w45-m h45-m w4 h4 w5-l h5-l"
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt="loading..."
            />
          </div>
          <div className="tc pv2 f5 fw5">Data Not Available</div>
        </div>
      </div>
    )
  else
    return (
      <div>
        {data && data.getmatchStats && data.getmatchStats.matchStatsArray ? (
          <div className="bg-white mv2 mt0-l shadow-4 ">
            <div className="flex justify-between items-center pa2 ph3-l ">
              <h1 className="f7 f5-l fw5 grey_10 ttu">Match Stats</h1>
              <img
                src={information}
                onClick={() => toggleInfo()}
                alt=""
                className="h1 w1"
              />
            </div>
            <div className="outline light-gray" />

            {infoToggle && (
              <div>
                <div className="f7 grey_8 pa2 bg-white">
                  An overview of all the important statistics from the match.
                </div>
                <div className="outline light-gray" />
              </div>
            )}

            <div className="flex items-center justify-between pa2">
              {data.getmatchStats.matchStatsArray.map((flag, i) => (
                <div
                  key={i}
                  className={`flex  items-center ${
                    i === 1 ? 'flex-row-reverse' : ''
                  }`}
                >
                  <img
                    className="h1 w15 shadow-4"
                    src={`https://images.cricket.com/teams/${flag.teamId}_flag_safari.png`}
                    onError={(e) => {
                      e.target.src = flagPlaceHolder
                    }}
                    alt=""
                  />
                  <span className={` fw6 f5-l f6 ${i === 0 ? 'pl1' : 'pr1'} `}>
                    {flag.shortName}{' '}
                  </span>
                </div>
              ))}
            </div>
            <div className="divider"></div>
            {statsArr.map(
              (category, j) =>
                data.getmatchStats.matchStatsArray[0] &&
                data.getmatchStats.matchStatsArray[1] &&
                data.getmatchStats.matchStatsArray[0][category.key] &&
                data.getmatchStats.matchStatsArray[1][category.key] &&
                data.getmatchStats.matchStatsArray[0][category.key].value &&
                data.getmatchStats.matchStatsArray[1][category.key].value && (
                  <div key={j} className="pv2">
                    <div className="flex fw5 items-center ph2 pv1 justify-between">
                      <div className="f6 fw6">
                        {
                          data.getmatchStats.matchStatsArray[0][category.key]
                            .value
                        }
                      </div>
                      <div className="f7 fw6 gray">{category.value}</div>
                      <div className="f6 fw6">
                        {
                          data.getmatchStats.matchStatsArray[1][category.key]
                            .value
                        }
                      </div>
                    </div>
                    <div className="flex items-center justify-between">
                      <div
                        className={`bg-light-gray br2 ma1 ma2-l relative items-center w-50 z-0 ${
                          (
                            category.key === 'extras'
                              ? parseFloat(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].value,
                                ) <
                                parseFloat(
                                  data.getmatchStats.matchStatsArray[1][
                                    category.key
                                  ].value,
                                )
                              : parseFloat(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].value,
                                ) >
                                  parseFloat(
                                    data.getmatchStats.matchStatsArray[1][
                                      category.key
                                    ].value,
                                  ) ||
                                parseFloat(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].value,
                                ) ===
                                  parseFloat(
                                    data.getmatchStats.matchStatsArray[1][
                                      category.key
                                    ].value,
                                  )
                          )
                            ? 'o-100'
                            : 'o-50'
                        }`}
                        style={{ height: 6 }}
                      >
                        <div
                          className={`absolute h-100 br2 right-0 bg-dark-red dn-l`}
                          style={{
                            width:
                              ((innerWidth - 26) *
                                data.getmatchStats.matchStatsArray[0][
                                  category.key
                                ].percent) /
                              200,
                            zIndex: -1,
                          }}
                        ></div>
                        <div
                          className={`absolute h-100 br2 right-0 bg-dark-red db-l dn`}
                          style={{
                            width:
                              ((innerWidth > 960 ? 950 : innerWidth) *
                                data.getmatchStats.matchStatsArray[0][
                                  category.key
                                ].percent) /
                              200,
                            zIndex: -1,
                          }}
                        ></div>
                      </div>
                      <div
                        className={`bg-light-gray br2 ma1 ma2-l relative items-center w-50 z-0 ${
                          (
                            category.key === 'extras'
                              ? parseFloat(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].value,
                                ) >
                                parseFloat(
                                  data.getmatchStats.matchStatsArray[1][
                                    category.key
                                  ].value,
                                )
                              : parseFloat(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].value,
                                ) <
                                  parseFloat(
                                    data.getmatchStats.matchStatsArray[1][
                                      category.key
                                    ].value,
                                  ) ||
                                parseFloat(
                                  data.getmatchStats.matchStatsArray[0][
                                    category.key
                                  ].value,
                                ) ===
                                  parseFloat(
                                    data.getmatchStats.matchStatsArray[1][
                                      category.key
                                    ].value,
                                  )
                          )
                            ? 'o-100'
                            : 'o-50'
                        }`}
                        style={{ height: 6 }}
                      >
                        <div
                          className={`absolute h-100 br2 left-0 criclytic-bg dn-l`}
                          style={{
                            width:
                              ((innerWidth && global.window.innerWidth - 26) *
                                data.getmatchStats.matchStatsArray[1][
                                  category.key
                                ].percent) /
                              200,
                            zIndex: -1,
                          }}
                        ></div>
                        <div
                          className={`absolute h-100 br2 left-0 criclytic-bg db-l dn`}
                          style={{
                            width:
                              ((innerWidth > 960 ? 950 : innerWidth) *
                                data.getmatchStats.matchStatsArray[1][
                                  category.key
                                ].percent) /
                              200,

                            zIndex: -1,
                          }}
                        ></div>
                      </div>
                    </div>
                  </div>
                ),
            )}
          </div>
        ) : (
          <div className=" bg-white pv3 mt2">
            <div>
              <img
                className="w45-m h45-m w4 h4 w5-l h5-l"
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt="loading..."
              />
            </div>
            <div className="tc pv2 f5 fw5">Data Not Available</div>
          </div>
        )}
      </div>
    )
}
