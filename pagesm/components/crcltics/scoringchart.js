import React, { useState, useEffect } from 'react';
import { DUGOUT } from '../../api/queries';

const information = '/svgs/information.svg';
const ground = '/svgs/groundImageWicket.png';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';

export default function ScoringChart({ data, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false);
  const [innerWidth, updateInnerWidth] = useState(0);

  useEffect(() => {
    updateInnerWidth(window.innerWidth);
  }, []);

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };

  let powerPlay = data && data.getPhaseOfInningsResolver && data.getPhaseOfInningsResolver.format == 'T10'?3: data && data.getPhaseOfInningsResolver && data.getPhaseOfInningsResolver.format == 'ODI' ? 10 : data.getPhaseOfInningsResolver && data.getPhaseOfInningsResolver.format == 'T100'?5: 6;

  if (!data)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  else
    return (
      <>
        {data && data.getPhaseOfInningsResolver && data.getPhaseOfInningsResolver.format !== 'Test' ? (
          <div className='mt2 mt0-l'>
            <div className='flex justify-between items-center pa2 bg-white'>
              <h1 className='f7 f5-l fw5 grey_10 ttu'>scoring charts</h1>
              <img src={information} onClick={() => toggleInfo()} alt='' className='h1 w1' />
            </div>
            <div className='outline light-gray' />

            {infoToggle && (
              <div>
                <div className='f7 grey_8 pa2 bg-white'>
                The Manhattan graphs show a breakdown of how the teams
                          have fared through the course of their innings.
                  
                </div>
                <div className='outline light-gray' />
              </div>
            )}

            {data.getPhaseOfInningsResolver.inningsPhase.map((inning, key) => (
              <div key={key} className='bg-white mb2'>
                <div className='flex justify-between items-center pa2 bg-white shadow-4'>
                  <div className='flex items-center'>
                    <img
                      alt=''
                      className='h13 w2-2 shadow-4'
                      src={`https://images.cricket.com/teams/${inning.teamID}_flag.png`}
                      onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    />
                    <span className='ml2 f6 fw5 fw6-l f5-l'>{inning.teamShortName || ''}</span>
                  </div>
                  <div className='fw6 f5'>
                    {inning.totalRuns || 0}
                    {inning.totalWickets ? `/${inning.totalWickets}` : ''}{' '}
                    <span className='fw4'>({inning.data.length})</span>{' '}
                  </div>
                </div>

                <div className='pa3 bg-white'>
                  <div className='flex justify-between items-end'>
                    <div
                      className=' bl mt2 relative'
                      style={{
                        width: 1,
                        height: Math.max.apply(
                          Math,
                          inning.data.map((o) => {
                            return o.runs * 8 + 24;
                          })
                        )
                      }}>
                      <div className='absolute right-0 mr1'>
                        {[1, 2, 3, 4, 5].map((val, i) => (
                          <div
                            key={i}
                            className='f8 flex items-end'
                            style={{
                              height: Math.max.apply(
                                Math,
                                inning.data.map((o) => {
                                  return (o.runs * 8 + 5) / 5;
                                })
                              )
                            }}>
                            {Math.max.apply(
                              Math,
                              inning.data.map((o) => {
                                return Math.floor((o.runs / 5) * (5 - i));
                              })
                            )}
                            
                          </div>
                          
                        ))}
                        
                         
                      </div>
                      <div className="f9 absolute bottom--1 right-0 left--1 mr1 rotate-90">RUNS</div>
                    </div>
                    

                    
                    {inning.data.map((over, i) => (
                      <div key={i}>
                        {Array.apply(null, { length: Number(over.wickets) }).map((x, i) => (
                          <div key={i} className='br-50 bg-darkRed  mb2 center ma1 w03 h03 w04-l h04-l' />
                        ))}
                        <div
                          style={{
                            height: Number(over.runs) * 7,
                            backgroundColor:
                              i < powerPlay
                                ? '#A5B2D8'
                                : (i < 40 && 'ODI' === data.getPhaseOfInningsResolver.format) ||
                                  (i < 15 && ('T20' === data.getPhaseOfInningsResolver.format || 'T100' === data.getPhaseOfInningsResolver.format))||(i < 7 && 'T10' === data.getPhaseOfInningsResolver.format)
                                ? '#63729F'
                                : '#A5B2D8',
                            width:
                            data.getPhaseOfInningsResolver.format == 'T10'
                                ? innerWidth > 976
                                  ? (950 * 0.70) / 10
                                  : innerWidth > 900
                                  ? (innerWidth - 750 * 0.82) / 10
                                  : (innerWidth * 0.50) / 10 :
                             (data.getPhaseOfInningsResolver.format == 'T20'  || 'T100' === data.getPhaseOfInningsResolver.format)
                                ? innerWidth > 976
                                  ? (950 * 0.82) / 20
                                  : innerWidth > 900
                                  ? (innerWidth - 750 * 0.82) / 20
                                  : (innerWidth * 0.82) / 20
                                : innerWidth > 976
                                ? (950 * 0.9) / 50
                                : innerWidth > 900
                                ? (innerWidth - 750 * 0.95) / 50
                                : (innerWidth * 0.78) / 50
                          }}
                        />
                      </div>
                    ))}
                  </div>

                  <div className='bg-navy pa2'>
                    <div className='flex justify-between items-center white-70 w-100  '>
                      <div className='f6 fw5 w-33 w-20-l tc'>
                        1 - {inning.data.length >= powerPlay ? powerPlay : inning.data.length}
                      </div>
                      { (data.getPhaseOfInningsResolver.format == 'T20'  || 'T100' === data.getPhaseOfInningsResolver.format) && inning.data.length > powerPlay && (
                        <div className='f6 fw5 w-33 w-20-l tc'>
                          {powerPlay + 1} - {inning.data.length >= 15 ? 15 : inning.data.length}
                        </div>
                      )}
                      { (data.getPhaseOfInningsResolver.format == 'T20'  || 'T100' === data.getPhaseOfInningsResolver.format) && inning.data.length > 15 && (
                        <div className='f6 fw5 w-33 w-20-l tc'>16 - {inning.data.length}</div>
                      )}

                      {data.getPhaseOfInningsResolver.format === 'T10' && inning.data.length > powerPlay && (
                        <div className='f6 fw5 w-33 w-20-l tc'>
                          {powerPlay + 1} - {inning.data.length >= 7 ? 7 : inning.data.length}
                        </div>
                      )}
                      {data.getPhaseOfInningsResolver.format === 'T10' && inning.data.length > 7 && (
                        <div className='f6 fw5 w-33 w-20-l tc'> 8 - {inning.data.length}</div>
                      )}


                      {'ODI' === data.getPhaseOfInningsResolver.format && inning.data.length > powerPlay && (
                        <div className='ma0 fw5 f6 w-33 w-20-l tc'>
                          {powerPlay + 1} - {inning.data.length >= 40 ? 40 : inning.data.length}
                        </div>
                      )}
                      {'ODI' === data.getPhaseOfInningsResolver.format && (
                        <div className='ma0 fw5 f6 w-33 w-20-l tc'>
                          {inning.data.length > 40 ? 41 + ' - ' + inning.data.length : ''}
                        </div>
                      )}
                    </div>
                    <div className='flex justify-between items-center pt1'>
                      <div className='white f6 fw5 w-33 w-20-l tc'>
                        {inning.data
                          .slice(0, inning.data.length >= powerPlay ? powerPlay : inning.data.length)
                          .map((x) => Number(x.runs))
                          .reduce((a, x) => a + x, 0)}
                        /
                        {inning.data
                          .slice(0, powerPlay)
                          .map((x) => Number(x.wickets))
                          .reduce((a, x) => a + x, 0)}
                      </div>
                      { (data.getPhaseOfInningsResolver.format == 'T20'  || 'T100' === data.getPhaseOfInningsResolver.format) && inning.data.length > powerPlay && (
                        <p className='ma0 white fw5 f6 w-33 w-20-l tc'>
                          {inning.data
                            .slice(powerPlay, inning.data.length > 15 ? 15 : inning.data.length)
                            .map((x) => Number(x.runs))
                            .reduce((a, x) => a + x, 0)}
                          /
                          {inning.data
                            .slice(powerPlay, inning.data.length > 15 ? 15 : inning.data.length)
                            .map((x) => Number(x.wickets))
                            .reduce((a, x) => a + x, 0)}
                        </p>
                      )}
                      { (data.getPhaseOfInningsResolver.format == 'T20'  || 'T100' === data.getPhaseOfInningsResolver.format) && inning.data.length > 15 && (
                        <p className='ma0 white fw5 f6 w-33 w-20-l tc'>
                          {inning.data
                            .slice(15, inning.data.length)
                            .map((x) => Number(x.runs))
                            .reduce((a, x) => a + x, 0)}
                          /
                          {inning.data
                            .slice(15, inning.data.length)
                            .map((x) => Number(x.wickets))
                            .reduce((a, x) => a + x, 0)}
                        </p>
                      )}


                      {data.getPhaseOfInningsResolver.format === 'T10' && inning.data.length > powerPlay && (
                        <p className='ma0 white fw5 f6 w-33 w-20-l tc'>
                          {inning.data
                            .slice(powerPlay, inning.data.length > 7 ? 7 : inning.data.length)
                            .map((x) => Number(x.runs))
                            .reduce((a, x) => a + x, 0)}
                          /
                          {inning.data
                            .slice(powerPlay, inning.data.length > 7 ? 7 : inning.data.length)
                            .map((x) => Number(x.wickets))
                            .reduce((a, x) => a + x, 0)}
                        </p>
                      )}
                      {'T10' === data.getPhaseOfInningsResolver.format && inning.data.length > 7 && (
                        <p className='ma0 white fw5 f6 w-33 w-20-l tc'>
                          {inning.data
                            .slice(7, inning.data.length)
                            .map((x) => Number(x.runs))
                            .reduce((a, x) => a + x, 0)}
                          /
                          {inning.data
                            .slice(7, inning.data.length)
                            .map((x) => Number(x.wickets))
                            .reduce((a, x) => a + x, 0)}
                        </p>
                      )}





                      {'ODI' === data.getPhaseOfInningsResolver.format && inning.data.length > powerPlay && (
                        <p className='ma0 white fw5 f6 w-33 w-20-l tc'>
                          {inning.data
                            .slice(powerPlay, inning.data.length < 40 ? inning.data.length : 40)
                            .map((x) => Number(x.runs))
                            .reduce((a, x) => a + x, 0)}
                          /
                          {inning.data
                            .slice(powerPlay, inning.data.length < 40 ? inning.data.length : 40)
                            .map((x) => Number(x.wickets))
                            .reduce((a, x) => a + x, 0)}
                        </p>
                      )}
                      {'ODI' === data.getPhaseOfInningsResolver.format && (
                        <p className='ma0 white fw5 f6 w-33 w-20-l tc'>
                          {inning.data.length > 40
                            ? inning.data
                                .slice(40, inning.data.length)
                                .map((x) => Number(x.runs))
                                .reduce((a, x) => a + x, 0) +
                              '/' +
                              inning.data
                                .slice(40, inning.data.length)
                                .map((x) => Number(x.wickets))
                                .reduce((a, x) => a + x, 0)
                            : ''}
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div className=' bg-white pv3 mt2'>
            <div>
              <img
                className='w45-m h45-m w4 h4 w5-l h5-l'
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt='loading...'
              />
            </div>
            <div className='tc pv2 f5 fw5'>Data Not Available</div>
          </div>
        )}
      </>
    );
}
