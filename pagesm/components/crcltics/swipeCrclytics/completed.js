import React, { useRef, useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'

import GameChangingOver from './gameChanginOver'
import MatchStat from './matchStat'
import MatchReel from './matchReel'
import PhasesOfPlay from './phasesOfPlay'
import ReportCard from './matchRating'
import RunComparisonCompleted from './runComparisonCompleted'
import { EffectCoverflow, Pagination } from 'swiper'
export default function index(props) {
  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)
  const [swiper, updateSwiper] = useState(null)
  const [currentIndex, updateCurrentIndex] = useState(0)
  const [componentArray, setComponentArray] = useState([])

  const router = useRouter()

  const routerTabName = router.query.id.slice(-1)[0]

  const indexTell = () => {
    if (routerTabName === 'report-card') {
      return '0'
    }
    if (routerTabName === 'run-comparison') {
      return '1'
    }
    if (routerTabName === 'game-change-overs') {
      return '2'
    }
    if (routerTabName === 'match-reel') {
      return '3'
    }
    if (routerTabName === 'match-stat') {
      return '4'
    }
    if (routerTabName === 'phases-of-play') {
      return '5'
    }
  }

  const [paggination, setPaggination] = useState(0)

  const swiperRef = useRef(null)

  const components = [
    <ReportCard matchID={props.matchID} />,
    <RunComparisonCompleted
      matchID={props.matchID}
      matchType={props.matchType}
    />,
    <GameChangingOver />,
    <MatchReel
      matchID={props.matchID}
      matchType={props.matchType}
      matchStatus={props.data.matchStatus}
    />,
    <MatchStat
      matchID={props.matchID}
      data={props.keystat}
      matchData={props.data}
    />,
    <PhasesOfPlay matchID={props.matchID} />,
  ]
  const componentsTest = [
    <ReportCard matchID={props.matchID} />,
    <MatchReel
      matchID={props.matchID}
      matchType={props.matchType}
      matchStatus={props.data.matchStatus}
    />,
    <PhasesOfPlay matchID={props.matchID} />,
  ]

  useEffect(() => {
    parseInt(indexTell())

    const newArrayMAtch =
      props.matchType !== 'Test'
        ? components.splice(parseInt(indexTell()), 6)
        : componentsTest.splice(parseInt(indexTell()), 3)
    const finalArray =
      props.matchType !== 'Test'
        ? [...newArrayMAtch, ...components]
        : [...newArrayMAtch, ...componentsTest]
    setComponentArray(finalArray)
  }, [])
  useEffect(() => {
    setPaggination(parseInt(indexTell()))
    updateCurrentIndex(indexTell())
    if (swiper !== null) {
      try {
        swiper.on('slideChange', () => {
          setPaggination(swiper.realIndex)
          updateCurrentIndex(swiper.realIndex)

          // props.updateCurrentIndex(swiper.realIndex);
        })
        swiper.on('click', (swipeData) => {
          // console.log(currentIndex);
          // data.featurematch.length > 0 &&
          //   data.featurematch[0].displayFeatureMatchScoreCard &&
          //   handleNavigation(data.featurematch[swiper.realIndex]);
        })
      } catch (e) {
        console.log(e)
      }
    }
  }, [])

  const content = [
    {
      routerTabName: 'report-card',
      heading: 'Report Card',
      subHeading: 'A comprehensive view of a player’s impact on the game.',
    },
    {
      routerTabName: 'run-comparison',
      heading: 'Run Comparison',
      subHeading:
        'Statistical visualization to depict score progression and fall of wickets against the overs bowled during an innings',
    },
    {
      routerTabName: 'game-change-overs',
      heading: 'Game Changing Overs',
      subHeading:
        'A collection of four overs which according to Criclytics changed the game,with a detailed breakdown of what happened in each ball of these overs.',
    },
    {
      routerTabName: 'match-reel',
      heading: 'Match Reel',
      subHeading:
        'A fast-tracked reel of how the fortunes of the teams shifted through the game.',
    },

    {
      routerTabName: 'match-stat',
      heading: 'Match Stats',
      subHeading: 'An overview of all the important statistics from the match',
    },
    {
      routerTabName: 'phases-of-play',
      heading: 'Phases Of Play',
      subHeading:
        'Check how the teams performed in a session by session breakdown',
    },
  ]

  return (
    <div
      className="text-white  overflow-scroll max-h-fit overflow-scroll w-full lg:mt-10"
      style={{ maxHeight: '70vh' }}
    >
      {/* {router.query.id.length>2&&<div className='m-3 md:mt-14 m w-full'>
        <div className='text-xl  text-white tracking-wide font-bold '> {content[indexTell()].heading}</div>
   
       
        <div className='text-md  text-white tracking-wide '> {content[indexTell()].subHeading}</div>
        <div className='w-12 h-1  bg-blue-8 mt-3'></div>
      </div>
      } */}

      {components[indexTell()]}

      {/* {componentArray.map((component, i) => {
          return i==parseInt(indexTell())&& <> {indexTell()}{component}</>;
        })} */}
    </div>
  )
}
