import React, { useState, useEffect } from 'react';
const ground = '/svgs/groundImageWicket.png';
const information = '/svgs/information.svg';
import DataNotFound from '../../commom/datanotfound';
import Heading from '../../commom/heading';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
export default function MatchStats({ data, browser, ...props }) {
  const [infoToggle, setInfoToggle] = useState(false);

  const [innerWidth, updateInnerWidth] = useState(0);

  useEffect(() => {
    updateInnerWidth(window.innerWidth);
  }, []);

  const localStorage = {};

  const statsArr = [
    { key: 'highestBattingScore', value: 'Highest Score' },
    { key: 'highestPartnership', value: 'Highest Partnership' },
    { key: 'totalFours', value: '4s' },
    { key: 'totalSix', value: '6s' },
    { key: 'runsScoredInBoundaries', value: 'Runs Scored in Boundaries' },
    { key: 'totalDotBalls', value: 'Number of Dot Balls Faced' },
    { key: 'highestWickets', value: 'Best Bowling' },
    { key: 'extras', value: 'Extras Conceded' },
    { key: 'runRate', value: 'Run Rate in Inning' },
    { key: 'runRateInPowerplay1_6', value: 'Run Rate - Powerplay' }, // for T20
    { key: 'runRateInPowerplay1_10', value: 'Run Rate - Powerplay' }, // For ODI
    { key: 'runRateInPowerplay11_40', value: 'Run Rate in Mid-Over' }, // For ODI
    { key: 'runRateDeathOver', value: 'Run Rate - Death Over' }
  ];

  const toggleInfo = () => {
    setInfoToggle(true);
    setTimeout(() => {
      setInfoToggle(false);
    }, 6000);
  };
  if (!data)
    return (
      <div>
        <div className=' pv3 mt2'>
          <div>
            <img
              className='w45-m h45-m w4 h4 w5-l h5-l'
              style={{ margin: 'auto', display: 'block' }}
              src={ground}
              alt='loading...'
            />
          </div>
          <div className='tc pv2 f5 white fw5'>Data Not Available</div>
        </div>
      </div>
    );
  else
    return (
      <div className='mx-3' >
       
        <div className='mb-3'>
          <Heading heading={'Match Stats'} subHeading={'An overview of all the important statistics from the match'} />
          {/* <div className='text-md  text-white tracking-wide font-bold '> Match Stats </div>
          <div className='text-base text-white pt-1 font-medium tracking-wide'>
            {' '}
            An overview of all the important statistics from the match{' '}
          </div>
          <div className='w-12 h-1 bg-blue-8 mt-2'></div> */}
        </div>

        {data && data.getmatchStats && data.getmatchStats.matchStatsArray ? (
          <div className=' bg-gray rounded-xl p-3 ' >
            {/*  */}

            {/* {false && (
              <div className='flex flex-column bg-gray-4  m-3 rounded  my-5 '>
                <div className='flex justify-between items-center'>
                  <div className=''>
                    <div className='flex items-center  text-white text-md font-semibold'>
                      <img
                        className='w-10 h-6 shadow-2xl border-2 border-white rounded-sm mr-2'
                        src={`https://images.cricket.com/teams/${props.matchData.firstInningsTeamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />{' '}
                      {props.matchData.homeTeamName}
                    </div>
                  </div>
                  <div className='justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 '>
                    {props.matchData.matchType}
                  </div>
                  <div>
                    <div className='flex items-center  text-white text-md font-semibold'>
                      {' '}
                      {props.matchData.awayTeamName}
                      <img
                        className='w-10 h-6 shadow-2xl border-2 border-white rounded-sm ml-2'
                        src={`https://images.cricket.com/teams/${props.matchData.secondInningsTeamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />{' '}
                    </div>
                  </div>
                </div>

                {props.matchData.matchScore &&
                  props.matchData.matchScore[0] &&
                  props.matchData.matchScore[0].teamScore.length > 0 && (
                    <div className='flex justify-between items-center pt-3'>
                      <div className=''>
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[0] &&
                          props.matchData.matchScore[0].teamScore &&
                          props.matchData.matchScore[0].teamScore.map((team, id) => (
                            <div key={id} className='flex items-center'>
                              <p
                                className={`${
                                  team.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                } text-sm font-semibold pb-1`}>
                                {team.runsScored}/{team.wickets}
                              </p>
                              {team.overs && <p className='text-xs pl-1 font-medium'>({team.overs})</p>}
                            </div>
                          ))}
                      </div>
                      <div className=''>
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[1] &&
                          props.matchData.matchScore[1].teamScore &&
                          props.matchData.matchScore[1].teamScore.map((team, id) => (
                            <div key={id} className='flex items-center'>
                              <p
                                className={`${
                                  team.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                } text-sm font-semibold pb-1`}>
                                {team.runsScored}/{team.wickets}
                              </p>
                              {team.overs && <p className='text-xs pl-1 font-medium'>({team.overs})</p>}
                            </div>
                          ))}
                      </div>
                    </div>
                  )}
              </div>
            )} */}

            <div className='  text-md font-semibold lg:hidden md:hidden'>Match Stats</div>
            <div className='flex my-3 items-center justify-center'>
              <div className='flex  w-full flex-col bg-gray-4  rounded-xl  p-3'>
                <div className='flex justify-between items-center'>
                  <div className=''>
                    <div className='flex items-center  text-white text-base lg:text-xl md:text-xl  font-semibold'>
                      <img
                        className='w-12 h-10 lg:w-16 md:w-16  border-2 border-white rounded mr-2'
                        src={`https://images.cricket.com/teams/${props.matchData.matchScore[0].teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />{' '}
                      {props.matchData.matchScore[0].teamShortName}
                    </div>
                  </div>
                  <div className='text-base border font-semibold border-green rounded-full text-green uppercase text-xs px-2 bg-gray-8  py-1  lg:py-2  md:py-2 text-center lg:w-3/12 md:w-3/12'>
                    {props.matchData.matchType}
                  </div>
                  <div>
                    <div className='flex items-center  text-white  lg:text-xl md:text-xl text-base font-semibold'>
                      {' '}
                      {props.matchData.matchScore[1].teamShortName}
                      <img
                        className='w-12 h-10 lg:w-16 md:w-16 border-2 border-white rounded ml-2'
                        src={`https://images.cricket.com/teams/${props.matchData.matchScore[1].teamID}_flag_safari.png`}
                        onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                      />{' '}
                    </div>
                  </div>
                </div>

                {props.matchData.matchScore &&
                  props.matchData.matchScore[0] &&
                  props.matchData.matchScore[0].teamScore.length > 0 && (
                    <div className='flex justify-between items-center pt-3'>
                      <div className=''>
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[0] &&
                          props.matchData.matchScore[0].teamScore &&
                          props.matchData.matchScore[0].teamScore.map((team, id) => (
                            <div key={id} className='flex items-center'>
                              <p
                                className={`${
                                  team.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                } text-base lg:text-xl md:text-xl font-semibold pb-1`}>
                                {team.runsScored}/{team.wickets}
                              </p>
                              {team.overs && <p className='text-base lg:text-xl md:text-xl pl-1 font-medium'>({team.overs})</p>}
                            </div>
                          ))}
                      </div>
                      <div className=''>
                        {props.matchData.matchScore &&
                          props.matchData.matchScore[1] &&
                          props.matchData.matchScore[1].teamScore &&
                          props.matchData.matchScore[1].teamScore.map((team, id) => (
                            <div key={id} className='flex items-center'>
                              <p
                                className={`${
                                  team.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                }  font-semibold pb-1 text-base lg:text-xl md:text-xl`}>
                                {team.runsScored}/{team.wickets}
                              </p>
                              {team.overs && <p className='text-base lg:text-xl md:text-xl pl-1 font-medium'>({team.overs})</p>}
                            </div>
                          ))}
                      </div>
                    </div>
                  )}
              </div>
            </div>

            {statsArr.map(
              (category, j) =>
                data.getmatchStats.matchStatsArray[0] &&
                data.getmatchStats.matchStatsArray[1] &&
                data.getmatchStats.matchStatsArray[0][category.key] &&
                data.getmatchStats.matchStatsArray[1][category.key] &&
                data.getmatchStats.matchStatsArray[0][category.key].value &&
                data.getmatchStats.matchStatsArray[1][category.key].value && (
                  <>
                    <div key={j} className='my-2'>
                      <div className='text-center text-[11px] font-medium uppercase text-gray-2'>{category.value} </div>
                      <div className='flex items-center justify-between w-full '>
                        <div className='text-xs text-white font-semibold  '>
                          {data.getmatchStats.matchStatsArray[0][category.key].value}
                        </div>
                        <div
                          className={`bg-gray-3 flex rounded mx-1 relative items-center w-4/5 z-0  o-100 `}
                          style={{ height: 5 }}>
                          <div
                            className={`absolute rounded-l left-0 bg-green `}
                            style={{
                              width: `${
                                (parseInt(data.getmatchStats.matchStatsArray[0][category.key].percent) * 100) /
                                (parseInt(data.getmatchStats.matchStatsArray[0][category.key].percent) +
                                  parseInt(data.getmatchStats.matchStatsArray[1][category.key].percent))
                              }%`,
                              zIndex: -1,
                              height: 5
                            }}></div>
                        </div>

                        <div className='text-xs text-white font-semibold'>
                          {' '}
                          {data.getmatchStats.matchStatsArray[1][category.key].value}
                        </div>
                      </div>
                    </div>
                  </>
                )
            )}
          </div>
        ) : (
          <>s</>
        )}

      <div className='h-56  hidden lg:block md:block '></div>
       
      </div>
    );
}
