import React from 'react'
import Heading from '../../commom/heading';
import ScoringChartCompleted from '../overprojection';

export default function overSimulationLive(props) {
  return (
    <div className='w-full'>
         <div className=' mx-2 md:mt-14 '>
           <Heading heading={'over simulation'} 
           subHeading={'Forecast of where the match is headed in the next few overs'} />
        
      </div>
            <ScoringChartCompleted matchID={props.matchID} matchType={props.matchType} />

    </div>
    
  )
}
