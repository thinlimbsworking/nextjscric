import React, { useRef, useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'
import { format } from 'date-fns'
import Countdown from 'react-countdown-now'

import PhasesOfPlay from './keyStat'
// import ReportCard from './matchRating'
// import FormIndex from './keyStat'
import MatchUps from '../../matchups'

// import MatchUps from './keyStat'
import FormIndex from '../liveSwipeCrclytics/formIndex'
import KeyStat from './keyStat'

import { EffectCoverflow, Pagination } from 'swiper'
// import Homepage from '..'
export default function index(props) {
  // alert(8)
  const navigationPrevRef = React.useRef(null)
  const navigationNextRef = React.useRef(null)
  const [swiper, updateSwiper] = useState(null)
  const [currentIndex, updateCurrentIndex] = useState(0)
  const [componentArray, setComponentArray] = useState([])

  const router = useRouter()

  const routerTabName = router.query.id.slice(-1)[0]
  const [paggination, setPaggination] = useState(0)

  const indexTell = () => {
    if (routerTabName === 'key-stat') {
      return '0'
    }
    if (routerTabName === 'player-matchup') {
      return '1'
    }

    if (routerTabName === 'form-index') {
      return '2'
    }
  }
  const navArr = ['player-matchup', 'key-stat', 'form-index']

  const swiperRef = useRef(null)

  const components = [
    <KeyStat data={props.keystat} matchID={props.matchID} />,
    <MatchUps data={props.keystat} matchID={props.matchID} />,
    <FormIndexComponent data={props.keystat} matchID={props.matchID} />,
  ]

  //   useEffect(() => {
  //     parseInt(indexTell());

  //     const newArrayMAtch = components.splice(parseInt(indexTell()), 2);
  //     const finalArray = [...newArrayMAtch, ...components];

  //     setComponentArray(finalArray);
  //   }, []);

  //   useEffect(() => {
  //     // alert(currentIndex)

  //     // updateCurrentIndex(indexTell())
  //     if (swiper !== null) {
  //       try {
  //         swiper.on('slideChange', () => {
  //           updateCurrentIndex(swiper.realIndex);

  //           // props.updateCurrentIndex(swiper.realIndex);
  //         });
  //         swiper.on('click', (swipeData) => {
  //           console.log(currentIndex);
  //           // data.featurematch.length > 0 &&
  //           //   data.featurematch[0].displayFeatureMatchScoreCard &&
  //           //   handleNavigation(data.featurematch[swiper.realIndex]);
  //         });
  //       } catch (e) {
  //         console.log(e);
  //       }
  //     }
  //   }, [swiper, currentIndex]);

  const handleNav = (index) => {
    updateCurrentIndex(index)

    router.push(
      '/criclytics/[...id]',
      `210480/mi-vs-pbks-match-23-indian-premier-league-2022/${navArr[index]}`,
    )
  }
  const content = [
    {
      routerTabName: 'over-simulation',
      heading: 'over simulation',
      subHeading:
        'Check the simulated team scores based on historical data of past overs.',
    },
    {
      routerTabName: 'run-comparison',
      heading: 'Run Comparison',
      subHeading:
        'Statistical visualization to depict score progression and fall of wickets against the overs bowled during an innings',
    },
    {
      routerTabName: 'score-projection',
      heading: 'Score Projection',
      subHeading:
        'A prediction of the teams final scores based on historical data and our powerful AI',
    },
    {
      routerTabName: 'player-matchup',
      heading: 'Player MatchUp',
      subHeading:
        'Select a batter and see how they perform against opposing bowlers',
    },

    {
      routerTabName: 'form-index',
      heading: 'Form Index',
      subHeading:
        'An accurate analysis of players from both teams based on their form from recent matches ',
    },
    {
      routerTabName: 'phases-of-play',
      heading: 'Phases Of Play',
      subHeading:
        'Check how the teams performed in a session by session breakdown',
    },
  ]

  return (
    <>
      {/* {router.query.id.length>2&&<div className='m-3 md:mt-14 m w-full'>
        <div className='text-md  text-white tracking-wide font-bold '> {content[indexTell()].heading}</div>
        <div className='text-md  text-white tracking-wide font-bold '> {content[indexTell()].subHeading}</div>
       
       {indexTell()!=='4'&& <div className='w-12 h-1 bg-blue-8 mt-2'></div>}
      </div>
      } */}
      <div
        className="max-h-fit overflow-scroll w-full "
        style={{ maxHeight: '65vh' }}
      >
        {components[indexTell()]}
      </div>
    </>
  )
}

const FormIndexComponent = (props) => {
  return (
    <div className="flex flex-col ">
      <FormIndex data={props.keystat} matchID={props.matchID} />
    </div>
  )
}
