import React, { useEffect, useState } from 'react'
import { getToken } from '../../services/authService';

export function useAuth(...params) {
    const [token, setToken] = useState('');
    useEffect(() => {
        let datas = [];
        params.forEach(param => {
            let val = getToken(param)
            datas.push(val ? val : '')
        })
        setToken(datas);
    },[])
    return token;
}

//    let [token, username, language] = useAuth('token','username','lang')
