import React, { useState, useEffect, useRef } from "react";
import Countdown from "react-countdown-now";
import Link from "next/link";
import ImageWithFallback from "../commom/Image";
import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  GET_FANTASY_SIX_TEAMS,
  CREATE_AND_UPDATE_USER_TEAM,
} from "../../api/queries";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Loading from "../loading";
import CleverTap from "clevertap-react";
const backIcon = "/svgs/back_dark_black.svg";
const stadium = "/pngs/frc_stadium.png";
// import Swiper from 'react-id-swiper';
const flagPlaceHolder = "/svgs/images/flag_empty.svg";
import fanfight from "../../public/svgs/fanfight_logo.svg";
import { getFrcSeriesTabUrl } from "../../api/services";

const playerPlaceholder = "/pngs/fallbackprojection.png";
import { format } from "date-fns";
import { useRouter } from "next/router";
import Algo11TeamDisplay from "./algo11TeamDisplay.js";
import { getLangText } from "../../api/services";
import { words } from "../../constant/language";
import Heading from "../commom/heading";
import MobileHeader from "../commom/mobileHeading";

const Empty = "/svgs/Empty.svg";

export default function AlgoSixTeams(props) {
  let scrl = useRef(null);

  let router = useRouter();
  let lang = props.language;
  // console.log("props algo11",props)
  let FantasymatchID = props.matchID;

  const [click, setclick] = useState(false);
  const [index, setIndex] = useState();
  const [toggle, setToggle] = useState(false);
  const [windows, setLocalStorage] = useState({});
  const [newToken, setToken] = useState("");
  const [copyAllCodes, setCopyAllCodes] = useState(false);
  const [teamCode, setTeamCode] = useState("");
  const [teamCodeClick, setteamCodeClick] = useState(false);
  const [myTeam, setMyTeam] = useState({});
  

  const [algo11Code, setalgo11Code] = useState("");
  // console.log("algo11Codealgo11Code", algo11Code)
  const [sixteam, setsixteam] = useState(null);
  // console.log("sixteam",sixteam)
  // console.log("algo11Code",algo11Code)

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem("tokenData"));
    }
  }, [global.window]);

  const { loading, error, data } = useQuery(GET_FANTASY_SIX_TEAMS, {
    variables: { matchID: FantasymatchID, token: newToken },
    onCompleted: (data) => {
      // console.log("data algosix",data)
      // let arrayMerge=[...algo11.getAlgo11.batsman, ...algo11.getAlgo11.all_rounder, ...algo11.getAlgo11.keeper, ...algo11.getAlgo11.bowler];
      // let captain=arrayMerge.filter(player=>player.captain==="1")
      // let viceCaptain=arrayMerge.filter(player=>player.vice_captain==="1")
      // let rem_player=arrayMerge.filter(player=>player.vice_captain!=="1" &&player.captain!=="1" )
      // setalgoTeam(algo11.getAlgo11.batsman && [...captain, ...viceCaptain,...rem_player]);
      if (data) {
        let sortedData = data.getFantasySixTeams.map((team) => {
          let batsman = team.batsman ? team.batsman : [];
          let all_rounder = team.all_rounder ? team.all_rounder : [];
          let keeper = team.keeper ? team.keeper : [];
          let bowler = team.bowler ? team.bowler : [];
          let arrayMerge = [...batsman, ...all_rounder, ...keeper, ...bowler];
          // let arrayMerge=[...team.batsman, ...team.all_rounder, ...team.keeper, ...team.bowler];
          let captain = arrayMerge.filter((player) => player.captain === "1");
          // console.log("captain",captain)
          let viceCaptain = arrayMerge.filter(
            (player) => player.vice_captain === "1"
          );
          //  console.log("viceCaptain",viceCaptain)
          let rem_player = arrayMerge.filter(
            (player) => player.vice_captain !== "1" && player.captain !== "1"
          );
          // console.log("rem_player",rem_player)

          return {
            data: [...captain, ...viceCaptain, ...rem_player],
            teamtotalpoints: team.teamtotalpoints,
            timestamp: team.timestamp,
            totalProjectedPoints: team.totalProjectedPoints,
            fantasy_teamName: team.fantasy_teamName,
          };
        });
        //  console.log("sortedData",sortedData)
        setsixteam(sortedData);
      }
    },
  });

  const [getAlgo11Code] = useMutation(CREATE_AND_UPDATE_USER_TEAM, {
    onCompleted: (data) => {
      console.log("===>>>>>>>>", data.createAndUpdateUserTeam.ffCode);
      setalgo11Code(data.createAndUpdateUserTeam.ffCode);
    },
  });
  const getAlgo11TeamDetails = (a, b) => {
    // console.log("a",a)
    // console.log("b",b)
    getAlgo11Code({
      variables: {
        matchID: FantasymatchID,
        token: newToken,
        team: {
          teamName: a,
          leagueType: "",
          selectCriteria: "",
          ffCode: "",
          team: b,
          isAlogo11: true,
        },
      },
    });
  };
  const fantasyTrack = () => {
    CleverTap.initialize("FantasyBYT", {
      Source: "NewTeam",
      MatchID: props.matchID,
      SeriesID: seriesID || "",
      TeamAID: data[0].homeTeamID,
      TeamBID: data[0].awayTeamID,
      MatchFormat: data[0].matchType,
      MatchStartTime: format(Number(data[0].matchDateTimeGMT), "d,y, h:mm a"),
      MatchStatus: data[0].matchStatus,
      Platform:
        windows && windows.localStorage ? windows.localStorage.Platform : "",
    });
  };

  const getUpdatedTime = (updateTime) => {
    // let uT=1618808000000;
    let currentTime = new Date().getTime();
    let distance = currentTime - updateTime;
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

    let result =
      (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
      minutes +
      (minutes > 1 ? " mins " : " min ") +
      (lang === "HIN" ? "पहले" : "ago");
    return result;
  };

  if (loading) {
    <div></div>;
  }

  const scrollCheck = () => {
    setscrollX(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };

  return !click ? (
    <div className="  min-vh-100">
      {/* <div className=' z-1 px-2 h2-4  flex items-center'>
        <img className='w1' onClick={() => window.history.back()} alt='' src={backIcon} />
        <div className='black f6 fw7 pl3 uppercase tc'>{getLangText(lang, words, 'Cricket.com teams')}</div>
      </div> */}
      <MobileHeader title={getLangText(lang, words, "Cricket.com teams")} />

      {true ? (
        <div className="p-3  ">
          {!click &&
            sixteam &&
            sixteam.length > 0 &&
            sixteam.map((item, index) => {
              return (
                <div className="flex items-center flex-col">
                  {console.log("itemitemitemsixteamsixteam", sixteam)}

                  <div className="flex items-center justify-around w-full lg:my-3 md:my-3 xl:my-3">
                    <div className="w-full md:mt-4 xl:mt-4 lg:mt-4 mt-16 mb-4">
                      <div className="flex">
                        <Heading heading={item.fantasy_teamName} />
                      </div>
                    </div>
                    <div className="w-full flex  items-center justify-end">
                      <div
                        className="border cursor-pointer rounded-md text-xs border-green w-6/12 text-center p-2 text-green font-bold"
                        onClick={() => (
                          setToggle(true),
                          setteamCodeClick(false),
                          setalgo11Code(""),
                          getAlgo11TeamDetails(item.fantasy_teamName, item.data)
                        )}
                      >
                        COPY CODE
                      </div>
                    </div>
                  </div>

                  {/* {lang === 'HIN' ? 'फ़ैंटसी अंक' : 'Fantasy Points'}: {item.teamtotalpoints} */}
                  <div className="flex w-full justify-around items-center  p-2 bg-gray-4  rounded-t">
                    <div className="flex w-full  justify-start items-center ">
                      {lang === "HIN" ? "अनुमानित अंक " : "Projected Points "}{" "}
                      {"  :  "}{" "}
                      <span className="text-green font-semibold ">
                        {" "}
                        {"  "} {item.totalProjectedPoints}
                      </span>
                    </div>
                    <div
                      className="flex cursor-pointer w-full items-center justify-end text-xs uppercase"
                      onClick={() => (
                        setclick(true), setMyTeam(item), setalgo11Code("")
                      )}
                    >
                      {" "}
                      View Team
                    </div>
                  </div>

                  <div className="flex  w-full items-center">
                    <div className=" w-full   mb-3 ">
                      <div className="">
                        <div className="flex  rounded-lg  ">
                          <div
                            className={`flex bg-gray  p-1 ${
                              index == 0
                                ? " border-2  border-green rounded-md"
                                : ""
                            }  overflow-x-auto `}
                          >
                            {true &&
                              item &&
                              item.data.map((item, y) => {
                                return (
                                  <div className=" mr-2  border-r-2 border-gray-4   p-2 px-4  w-32">
                                    <div className="flex w-full justify-center items-center">
                                      <div className="flex flex-col items-center justify-center ">
                                        <div className="relative mb-2">
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                            fallbackSrc = '/pngsV2/playerPH.png'
                                            className="h-12 w-12 bg-basebg rounded-full  object-cover object-top"
                                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                                            // onError={(evt) =>
                                            //   (evt.target.src =
                                            //     "/pngsV2/playerPH.png")
                                            // }
                                          />
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                            fallbackSrc = '/pngsV2/flag_dark.png'
                                            className="w-4 h-4  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba cursor-pointer"
                                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                            // onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
                                            // onError={(evt) =>
                                            //   (evt.target.src =
                                            //     "/pngsV2/flag_dark.png")
                                            // }
                                          />
                                          {(item.captain == "1" ||
                                            item.vice_captain == "1") && (
                                            <div className="w-5 border text-green bg-gray-4 h-5 absolute  right-[-4px] bottom-0 rounded-md ba flex justify-center items-center text-xs font-medium">
                                              {item.captain == "1"
                                                ? "C"
                                                : item.vice_captain == "1"
                                                ? "VC"
                                                : ""}
                                            </div>
                                          )}
                                        </div>
                                        <p
                                          className="text-xs text-center font-medium leading-5 w-full wordWrapLine"
                                          style={{ wordWrap: "break-word" }}
                                        >
                                          {item.playerName}
                                        </p>
                                        <p className="text-gray-2 text-xs uppercase">
                                          {item.player_role}
                                        </p>
                                      </div>
                                      {/* <div className='bg-black w-[1px] h-10'></div> */}
                                    </div>

                                    {toggle && (
                                      <>
                                        <div
                                          className="flex justify-center items-center fixed absolute--fill z-9999  overflow-y-scroll"
                                          style={{
                                            backdropFilter: "blur(10px)",
                                          }}
                                        >
                                          <div className="bg-gray border m-3 w-90  w-25-ns relative white pb-3">
                                            <ImageWithFallback height={18} width={18} loading='lazy'
                                              fallbackSrc = '/pngsV2/playerPH.png'
                                              className="h-3 -mt-6 w08 pa2 bg-white-20 right-0 top--2 absolute rounded-full cursor-pointer"
                                              onClick={() => setToggle(false)}
                                              src={"/svgs/close.png"}
                                              alt="close icon"
                                            />{" "}
                                            <div className="flex justify-center items-center p-2 ">
                                              <ImageWithFallback height={18} width={18} loading='lazy'
                                                fallbackSrc = '/pngsV2/playerPH.png'
                                                src="/pngs/A23.png"
                                                className="w-16 h-16"
                                                alt="a23"
                                              />
                                            </div>
                                            <div className=" flex flex-col items-center justify-center">
                                              <div className="flex pa2 flex-row f7 px-2 ">
                                                <div className="rounded-full mt1 w04 h04 bg-white"></div>
                                                <div className="fw2 f7 ml-2 text-white text-base  px-8 ">
                                                  {" "}
                                                  .{" "}
                                                  {getLangText(
                                                    lang,
                                                    words,
                                                    "Open_ff_message"
                                                  )}
                                                </div>
                                              </div>
                                              <div className="flex justify-center items-center mt3 uppercase font-bold">
                                                {getLangText(
                                                  lang,
                                                  words,
                                                  "team_code"
                                                )}
                                              </div>
                                              <div className="flex justify-center items-center mt3">
                                                <div className="gray br2 bw1 px-2 ba b--white-10 bg-basebg p-2">
                                                  <div className="uppercase pa2 text-orange flex justify-center items-center orange f4 fw2">
                                                    {algo11Code}
                                                  </div>
                                                </div>
                                              </div>
                                              <div
                                                className={`text-center pv2  text-green bg-basebg ${
                                                  copyAllCodes
                                                    ? "bg-basebg"
                                                    : "bg-darkRed"
                                                } mt-2 border border-green white text-center w-60 f5 fw6 cursor-pointer center br2 mt3 mb2 uppercase cursor-pointer flex justify-center`}
                                              >
                                                <CopyToClipboard
                                                  text={algo11Code}
                                                  onCopy={() => {
                                                    setteamCodeClick(true);
                                                  }}
                                                >
                                                  {lang === "HIN" ? (
                                                    <div>{`${
                                                      teamCodeClick
                                                        ? getLangText(
                                                            lang,
                                                            words,
                                                            "copied"
                                                          )
                                                        : getLangText(
                                                            lang,
                                                            words,
                                                            "Copy Code"
                                                          )
                                                    }`}</div>
                                                  ) : (
                                                    <div>{`${
                                                      teamCodeClick
                                                        ? "Copied"
                                                        : "Copy Code"
                                                    }`}</div>
                                                  )}
                                                  {/* <div>{`${teamCodeClick ? 'Copied' : 'Copy Code'}`}</div> */}
                                                </CopyToClipboard>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </>
                                    )}
                                  </div>
                                );
                              })}

                            {/* bowler */}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className=" hidden  flex items-center justify-center"
                      onClick={() => slide(+50)}
                    >
                      <svg width="30" viewBox="0 0 24 24">
                        <path
                          fill="#38d925"
                          d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                        ></path>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                      </svg>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      ) : (
        <></>
      )}

      {false ? (
        <div className="p-3  ">
          {!click &&
            sixteam &&
            sixteam.length > 0 &&
            sixteam.map((item, index) => {
              return (
                <div className="flex items-center flex-col ">
                  {console.log("itemitemitemsixteamsixteam", item)}
                  <div>
                    {lang === "HIN" ? "फ़ैंटसी अंक" : "Fantasy Points"}:{" "}
                    {item.teamtotalpoints}
                    <div className="flex">1</div>
                    <div
                      className="flex bg-red"
                      onClick={() => (
                        setToggle(true),
                        setteamCodeClick(false),
                        setalgo11Code(""),
                        getAlgo11TeamDetails(item.fantasy_teamName, item.data)
                      )}
                    >
                      2
                    </div>
                  </div>

                  <div className="flex  w-full items-center">
                    <div className=" w-full bg-gray-4   my-3 rounded-md">
                      <div className="">
                        <div className="flex  rounded-lg p-1 ">
                          <div className="flex bg-gray   overflow-x-auto ">
                            {true &&
                              item &&
                              item.data.map((item, y) => {
                                return (
                                  <div className=" mr-2  border-r-2 border-gray-4   p-2 px-4  w-32">
                                    <div className="flex w-full justify-center items-center">
                                      <div className="flex flex-col items-center ">
                                        <div className="relative mb-2">
                                          <ImageWithFallback height={33} width={33} loading='lazy'
                                            fallbackSrc = '/pngsV2/playerPH.png'
                                            className="h-16 w-16 bg-gray-4 rounded-full  object-cover object-top"
                                            src={`https://images.cricket.com/players/${item.playerId}_headshot_safari.png`}
                                            // onError={(evt) =>
                                            //   (evt.target.src =
                                            //     "/pngsV2/playerPH.png")
                                            // }
                                          />
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                            fallbackSrc = '/svgs/images/flag_empty.svg'
                                            className="w-5 h-5  absolute left-[-4px] bottom-0 object-center flex justify-center rounded-full ba cursor-pointer"
                                            src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                            // onError={(evt) =>
                                            //   (evt.target.src =
                                            //     "/svgs/images/flag_empty.svg")
                                            // }
                                          />
                                          {(item.captain == "1" ||
                                            item.vice_captain == "1") && (
                                            <div className="w-5 border text-green bg-gray-4 h-5 absolute text-xs right-[-4px] bottom-0 rounded-md ba flex justify-center items-center text-xs font-medium">
                                              {item.captain == "1"
                                                ? "C"
                                                : item.vice_captain == "1"
                                                ? "VC"
                                                : ""}
                                            </div>
                                          )}
                                        </div>
                                        <p
                                          className="text-xs text-center font-medium leading-5 w-full wordWrapLine"
                                          style={{ wordWrap: "break-word" }}
                                        >
                                          {item.playerName}
                                        </p>
                                        <p className="text-gray-2 text-xs uppercase">
                                          {item.player_role}
                                        </p>
                                      </div>
                                      {/* <div className='bg-black w-[1px] h-10'></div> */}
                                    </div>
                                  </div>
                                );
                              })}

                            {/* bowler */}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className=" hidden  flex items-center justify-center"
                      onClick={() => slide(+50)}
                    >
                      <svg width="30" viewBox="0 0 24 24">
                        <path
                          fill="#38d925"
                          d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                        ></path>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                      </svg>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      ) : (
        <></>
      )}

      {/* {console.log(" data && data.getFantasySixTeams", data && data.getFantasySixTeams)} */}
      {false &&
        !click &&
        sixteam &&
        sixteam.length > 0 &&
        sixteam.map((team, i) => {
          const params = {
            slidesPerView: 4,
            navigation: {
              nextEl: `#nbutton${i}`,
              prevEl: `#pbutton${i}`,
            },
            spaceBetween: 10,
            breakpoints: { 640: { slidesPerView: 8, spaceBetween: 20 } },
          };
          // onClick={() => navigate(`/fantasy-research-center/${props.matchID}/${props.seriesSlug}/create-team`)}
          return (
            <div key={i} className="m-2 mt-3 ">
              <div className="flex justify-between pb-2 items-center ">
                <div className="f7 fw5 white-50 pb-1 uppercase">
                  {team.fantasy_teamName}
                </div>
                {i === 0 && (
                  <div className="flex justify-center items-center ">
                    {props &&
                      props.data &&
                      props.data.miniScoreCard &&
                      props.data.miniScoreCard.data[0] &&
                      props.data.miniScoreCard.data[0].matchStatus ===
                        "upcoming" &&
                      team.timestamp && (
                        <div className="gray br2 bw1 px-2 ba pv1 white  b--white-30  f7">
                          {`  ${getLangText(
                            lang,
                            words,
                            "Last Updated"
                          )}: ${getUpdatedTime(team.timestamp)}`}{" "}
                        </div>
                      )}
                    {/* {data && data[0] && (data[0].matchStatus==="live"|| data[0].matchStatus==="completed")&& algo11.getAlgo11.teamtotalpoints &&  <div className="flex item-center  justify-center"><div className='white  f7'>{`Fantasy Points`} </div><div className="yellow-frc fw5 f6  pl1 oswald">{algo11.getAlgo11.teamtotalpoints}</div></div>} */}
                  </div>
                )}
              </div>
              <div
                className={`px-2 bg-white-10 ${
                  i === 0 ? "ba b--gold bw1" : "ba b--white-20"
                } br2 relative white pb-3`}
              >
                <div className="pt-1">
                  {props &&
                    props.data &&
                    props.data.miniScoreCard &&
                    props.data.miniScoreCard.data[0] &&
                    props.data.miniScoreCard.data[0].matchStatus ===
                      "upcoming" &&
                    team.totalProjectedPoints && (
                      <div className="text-right mb2 f6 flex items-center justify-end">
                        <div className="f8">
                          {getLangText(lang, words, "Projected_Points")}
                        </div>
                        <div className="yellow-frc fw5 f6  pl-1 oswald">
                          {team.totalProjectedPoints}
                        </div>
                        {/* <div className="ba br3 pa1 b--white-30 bg-black f7 ml2">{team.totalProjectedPoints}</div> */}
                      </div>
                    )}
                  {props &&
                    props.data &&
                    props.data.miniScoreCard &&
                    props.data.miniScoreCard.data[0] &&
                    (props.data.miniScoreCard.data[0].matchStatus === "live" ||
                      props.data.miniScoreCard.data[0].matchStatus ===
                        "completed") &&
                    team.teamtotalpoints && (
                      <div className="text-right mb2 f6 flex items-center justify-end">
                        <div className="f8">
                          {lang === "HIN" ? "फ़ैंटसी अंक" : "Fantasy Points"}
                        </div>
                        <div className="yellow-frc fw5 f6  pl1 oswald">
                          {team.teamtotalpoints}
                        </div>
                      </div>
                    )}

                  <div className="pb-2">
                    <div className="flex justify-between mx-1">
                      <div {...params}>
                        {team.data.map((x, y) => (
                          <div key={y} className="">
                            <div className=" relative flex justify-center">
                              <div className="overflow-hidden rounded-full b--white-20 ba w2-6 h2-6 bg-mid-gray relative">
                                <ImageWithFallback height={18} width={18} loading='lazy'
                                  fallbackSrc = {playerPlaceholder}
                                  className=""
                                  src={`https://images.cricket.com/players/${x.playerId}_headshot_safari.png`}
                                  // onError={(e) =>
                                  //   (e.target.src = playerPlaceholder)
                                  // }
                                />
                              </div>
                              <div className="absolute w1 ml-1 h1 left-0 bottom-0 mb1 rounded-full ba b--black  bg-gray ml3-l overflow-hidden">
                                <ImageWithFallback height={18} width={18} loading='lazy'
                                  fallbackSrc = {playerPlaceholder}
                                  className="w-100 h-100 object-cover"
                                  src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                                  // onError={(evt) =>
                                  //   (evt.target.src = flagPlaceHolder)
                                  // }
                                />
                              </div>
                              {(x.captain == "1" || x.vice_captain == "1") && (
                                <div className="absolute w1 h1 mr-1 right-0 bottom-0 mb-1 text-center black f10 flex items-center mr3-l fw6 justify-center rounded-full ba b--white  bg-gold">
                                  {x.captain === "1" ? "C" : "VC"}
                                </div>
                              )}
                            </div>
                            <div className="oswald f8 py-1 text-center px-1">
                              {lang === "HIN"
                                ? x.playerNameHindi
                                : x.playerName}
                            </div>
                            <div className="f10 gray text-center pt-1 uppercase">
                              {getLangText(lang, words, x.player_role)}
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="absolute absolute--fill flex justify-between items-center">
                      <div
                        id={`pbutton${i}`}
                        className="white cursor-pointer z-999 outline-0 nl2"
                      >
                        <svg width="30" focusable="false" viewBox="0 0 24 24">
                          <path
                            fill="#EEBA04"
                            d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                      <div
                        id={`nbutton${i}`}
                        className="white cursor-pointer z-999 outline-0 nr2"
                      >
                        <svg width="30" viewBox="0 0 24 24">
                          <path
                            fill="#EEBA04"
                            d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"
                          ></path>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="absolute left-0 right-0 z-1 flex justify-around center items-center cursor-pointer">
                  <div
                    className="uppercase  px-3 cursor-pointer bg-gray ba b--black pa2 br2 db "
                    onClick={() => (
                      setclick(true), setMyTeam(team), setalgo11Code("")
                    )}
                  >
                    {lang === "HIN" ? "टीम देखें" : "View Team"}
                  </div>
                  {props.data.miniScoreCard.data[0].matchStatus ===
                    "upcoming" && (
                    <div
                      className="uppercase f6 fw6 px-3 bg-green pa2 br2 db "
                      onClick={() => (
                        setToggle(true),
                        setteamCodeClick(false),
                        setalgo11Code(""),
                        getAlgo11TeamDetails(team.fantasy_teamName, team.data)
                      )}
                    >
                      {lang === "HIN" ? "कॉपी कोड" : "Copy Code"}
                    </div>
                  )}
                </div>

                {toggle && (
                  <>
                    <div
                      className="flex justify-center items-center fixed absolute--fill z-9999  overflow-y-scroll"
                      style={{ backdropFilter: "blur(10px)" }}
                    >
                      <div className="bg-gray border m-3 w-90  w-25-ns relative white pb-3">
                        <ImageWithFallback height={18} width={18} loading='lazy'
                          fallbackSrc = {playerPlaceholder}
                          className="h-3 -mt-6 w08 pa2 bg-white-20 right-0 top--2 absolute rounded-full cursor-pointer"
                          onClick={() => setToggle(false)}
                          src={"/svgs/close.png"}
                          alt="close icon"
                        />{" "}
                        <div className="flex justify-center items-center p-2 ">
                          <ImageWithFallback height={18} width={18} loading='lazy'
                            fallbackSrc = {playerPlaceholder}
                            src="/pngs/A23.png"
                            className="w-16 h-16"
                            alt=""
                          />
                        </div>
                        <div className=" flex flex-col items-center justify-center">
                          <div className="flex pa2 flex-row f7 px-2 ">
                            <div className="rounded-full mt1 w04 h04 bg-white"></div>
                            <div className="fw2 f7 ml2 text-white text-base  px-8 ">
                              {" "}
                              . {getLangText(lang, words, "Open_ff_message")}
                            </div>
                          </div>
                          <div className="flex justify-center items-center mt3 uppercase font-bold">
                            {getLangText(lang, words, "team_code")}
                          </div>
                          <div className="flex justify-center items-center mt3">
                            <div className="gray br2 bw1 px-2 ba b--white-10 bg-basebg p-2">
                              <div className="uppercase pa2 text-orange flex justify-center items-center orange f4 fw2">
                                {algo11Code}
                              </div>
                            </div>
                          </div>
                          <div
                            className={`text-center pv2  text-green bg-basebg ${
                              copyAllCodes ? "bg-basebg" : "bg-darkRed"
                            } mt-2 border border-green white text-center w-60 f5 fw6 cursor-pointer center br2 mt3 mb2 uppercase cursor-pointer flex justify-center`}
                          >
                            <CopyToClipboard
                              text={algo11Code}
                              onCopy={() => {
                                setteamCodeClick(true);
                              }}
                            >
                              {lang === "HIN" ? (
                                <div>{`${
                                  teamCodeClick
                                    ? getLangText(lang, words, "copied")
                                    : getLangText(lang, words, "Copy Code")
                                }`}</div>
                              ) : (
                                <div>{`${
                                  teamCodeClick ? "Copied" : "Copy Code"
                                }`}</div>
                              )}
                              {/* <div>{`${teamCodeClick ? 'Copied' : 'Copy Code'}`}</div> */}
                            </CopyToClipboard>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
              <div className="bb mx-2 b--white-20 pb4"></div>
            </div>
          );
        })}
    </div>
  ) : (
    click && (
      <Algo11TeamDisplay
        setclose={setclick}
        code={algo11Code}
        setMyTeam={setMyTeam}
        myTeam={myTeam}
        data={props.data.miniScoreCard?.data[0]}
        lang={lang}
      />
    )
  );
}
