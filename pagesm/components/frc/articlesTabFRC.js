import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { GET_ARTICLES_BY_ID_AND_TYPE } from '../../api/queries';
import ArticleCard from '../ArticleCard';
import Link from 'next/link';
import { ARTICLE_VIEW,News_Live_Tab_URL } from '../../constant/Links';
import CleverTap from 'clevertap-react';

export default function ArticlesTabFRC(matchID,props) {
  
  const { loading, error, data } = useQuery(GET_ARTICLES_BY_ID_AND_TYPE, {
    variables: { type: 'matches', Id: props.matchID || matchID }
  }); //Id: props.matchID

  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      // MatchID: String(article.matchIDs),
      // SeriesID: String(article.seriesIDs),
      // PlayerID: String(article.playerIDs),
      ArticleType: article.type,
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: localStorage ? localStorage.Platform : ''
    });
  };
  const getNewsUrl = (article) => {


    if(article.type!=="live-blog")
    {
     
    let articleId = article.articleID;
    let tab = article.type;
    return {
      as: eval(ARTICLE_VIEW.as),
      href: ARTICLE_VIEW.href
    };

  }

    if(article.type==="live-blog")
    {
     
    
    
      let matchId=article.matchIDs[0].matchID
      let articleID = article.articleID;
      let matchName = article.matchIDs[0].matchSlug
    
      
      return {
        as: eval(News_Live_Tab_URL.as),
        href: News_Live_Tab_URL.href
      };
    }


  };
  if (error) return <div></div>;
  if (loading) return <div></div>;
  else
    return data.getArticlesByIdAndType.length !== 0 ? (
      <div className='bg-white mv2 shadow-4'>
        <div className='flex items-center justify-between ph3'>
          <span className='f6 fw6 gray'>FANTASY PREVIEWS </span>
          <div className='h2-3 flex items-center'></div>
        </div>
        <div className='divider' />

        <div className='flex-ns w-100-ns bg-white'>
          <div className='flex-ns w-100 bg-white-ns mt3-ns'>
            <Link
              {...getNewsUrl(data.getArticlesByIdAndType[0])}
              passHref
              onClick={() => {
                handleCleverTab(data.getArticlesByIdAndType[0]);
              }}>

              <ArticleCard article={data.getArticlesByIdAndType[0]} />

            </Link>
          </div>
        </div>
      </div>
    ) : null;
}
