import React, { useState } from "react";
import CleverTap from "clevertap-react";
import { getLangText } from "../../api/services";
import { words } from "./../../constant/language";
import ImageWithFallback from "../commom/Image";

export default function chooseLogic({
  chooseLogic,
  setChooseLogic,
  players,
  setCurrentFlag,
  setPlayers,
  homeTeamNo,
  awayTeamNo,
  FantasymatchID,
  homeTeamID,
  awayTeamID,
  matchType,
  ...props
}) {
  

  const logicTypes = [
    {
      title: 'Projected_Points',
      ref: 'projected_points',
      imgSrc: '/pngsV2/frc_pp.png',
      desc: 'OROPWC'
    },
    {
      title: 'SELECTION_PERCENTAGE',
      ref: 'selection_percent',
      imgSrc: '/pngsV2/frc_sp.png',
      desc: 'PWWPBMU'
    },
    {
      title: 'Fantasy points',
      ref: 'fantasy_points',
      imgSrc: '/pngsV2/frc_fp.png',
      desc: 'PWTMFPSF'
    },
    {
      title: 'DreamTeam_Appearance',
      ref: 'dream_team_appearances',
      imgSrc: '/pngsV2/frc_dta.png',
      desc: 'PWTMAITB'
    }
  ]

  let lang = props.language;
  const fantasyTrack = () => {
    CleverTap.initialize("FantasyGeneratedTeam", {
      MatchID: FantasymatchID,
      SeriesID: "",
      TeamAID: homeTeamID,
      TeamBID: awayTeamID,
      MatchFormat: matchType,
      PlayersSelected: parseInt(homeTeamNo) + parseInt(awayTeamNo),
      LogicSelected: chooseLogic,
      LeagueSelected: props.leagueType,
      Platform:
        global && global.window && global.window.localStorage
          ? global.window.localStorage.Platform
          : "",
    });
  };
  const chooseLeageText = "border-y-2 border-r-2 border-green";
  const unChooseLeageText = "";

  return (
    <div className="text-white pb-4 mt-5">
      <div className="text-lg font-semibold">
        {" "}
        {getLangText(lang, words, "Add_method")}{" "}
        {getLangText(lang, words, "toym")}
      </div>
      <div className="bg-blue-8 w-15 rounded h-1 mt-1"></div>
      <div className="text-gray-2 text-xs font-normal mt-2">
        {" "}
        {getLangText(lang, words, "COOTFM")} <br />{" "}
        {getLangText(lang, words, "tocyt")}{" "}
      </div>

      <div className="flex-l flex-wrap-l justify-between-l mt-4 ">
        {/* <div
          className={`mb2 ba br2 b--white-30  ma2-l cursor-pointer ${chooseLogic === 'projected_points' ? chooseLeageText : unChooseLeageText}`}
          onClick={() => setChooseLogic('projected_points')}>
          <div className={`w2-3 h2-3 pa1 flex justify-center items-center br-100 ${chooseLogic === 'projected_points' ? "bg-yellow" : "bg-white-10"}`}>
            <img className={``} src={'/pngsV2/frc_pp.png'} alt='' />
          </div>
          <div className='w-80 flex flex-column justify-start items-center pl2'>
            <div className='f6 fw6 ttu tl w-100'>{getLangText(lang,words,'Projected_Points')}</div>
            <div className='f8 fw5 w-100 pv1 o-80  '>{getLangText(lang,words,'OROPWC')}</div>
          </div>
        </div> */}


        {
          logicTypes.map(logic => (
            <div
            className={`mb-3 rounded-xl bg-gray  cursor-pointer flex items-center overflow-hidden h-20 ${chooseLogic === logic.ref
                ? chooseLeageText
                : unChooseLeageText
              }`}
            onClick={() => setChooseLogic(logic.ref)}>
            <div className="bg-blue-8 w-1.5 h-full "></div>
            <div className="px-3 flex items-center">
              <ImageWithFallback height={18} width={18} loading='lazy' className={`w-16 h-16`} src={logic.imgSrc} alt="" />
              <div className=" pl-2 cursor-pointer">
                <div className="text-sm font-semibold">
                  {getLangText(lang, words, logic.title)}
                </div>
                <div className="h-1 rounded mt-1 w-12 bg-blue-8 "></div>
                <div className="mt-2 text-xs  ">
                  {getLangText(lang, words, logic.desc)}
                </div>
              </div>
            </div>
          </div>
          ))
        }

        


        <div className="h37 dn-l"></div>
        <div className=" bottom-0 fixed  z-999 left-0  right-0 dn-l lg:hidden md:hidden">
          <div className=" bg-basebg rounded-t-xl border-t-2 border-gray-2  flex items-center">
            <div className="w-full p-3 flex justify-between items-center">
              <div className="  flex items-center justify-between w-4/6">
                <div className=" bg-gray-4 p-2 rounded-md text-lg font-semibold justify-center  flex items-center flex-col h-[80px]">
                  {" "}
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc = "/pngsV2/flag_dark.png"
                    className="w-8 h-5 mb-1"
                    src={`https://images.cricket.com/teams/${homeTeamID}_flag_safari.png`}
                    alt=""
                    // onError={(evt) =>
                    //   (evt.target.src = "/pngsV2/flag_dark.png")}
                  />{" "}
                  {homeTeamNo}
                </div>
                <div className=" flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-lg font-semibold  h-[80px]">
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc = "/pngsV2/flag_dark.png"
                    className="w-8 h-5 mb-1"
                    src={`https://images.cricket.com/teams/${awayTeamID}_flag_safari.png`}
                    alt=""
                    // onError={(evt) =>
                    //   (evt.target.src = "/pngsV2/flag_dark.png")}
                  />{" "}
                  {awayTeamNo}
                </div>

                <div className=" flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-sm font-semibold  h-[80px] ">
                  <span className="text-xs font-medium text-gray-2">
                    {getLangText(lang, words, "Players")}
                  </span>
                  <div className="text-green">
                    <span className="text-3xl font-semibold">
                      {parseInt(homeTeamNo) + parseInt(awayTeamNo)}
                    </span>
                    <span className="text-md font-medium">/11</span>
                  </div>
                </div>
                <div className=" flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-sm font-semibold  h-[80px]">
                  <span className="text-xs font-medium text-gray-2 text-center">
                    {getLangText(lang, words, "Credits")} <br></br> Left
                  </span>

                  <span className="text-3xl font-semibold text-green">
                    {100 - props.totalCredits}
                  </span>
                </div>
              </div>
              <div className="flex items-center  center justify-end w-1/3 uppercase text-sm font-semibold">
                <div
                  onClick={() =>
                    chooseLogic.length > 0
                      ? (setCurrentFlag("Loading"), fantasyTrack())
                      : ""
                  }
                  className={
                    chooseLogic.length > 0
                      ? " border-green border-2 rounded-lg bg-gray-8 tc px-3 py-1 text-green  "
                      : " border-gray-2 border-2 rounded-lg bg-gray-8 tc px-3 py-1 text-gray-2 "
                  }
                >
                  {getLangText(lang, words, "PROCEED")}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="hidden lg:block md:block pt-4 w-full">
          <div className="bg-gray-8 text-center shadow-4 flex items-center ">
            <div className="w-full mt-2">
              <div className="flex-wrap flex justify-around items-center f1 yellow-frc  fw6 ">
                <div className="w-4/12 flex justify-center items-center f2 oswald">
                  {" "}
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc = "/pngsV2/flag_dark.png"
                    className="w-10 h-10 mr-1"
                    src={`https://images.cricket.com/teams/${homeTeamID}_flag_safari.png`}
                    alt=""
                    // onError={(evt) =>
                    //   (evt.target.src = "/pngsV2/flag_dark.png")
                    // }
                  />{" "}
                  {homeTeamNo}
                </div>
                <div className="w-4/12  flex justify-center items-center f2 oswald">
                  <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc = "/pngsV2/flag_dark.png"
                    className="w-10 h-10 mr-1"
                    src={`https://images.cricket.com/teams/${awayTeamID}_flag_safari.png`}
                    alt=""
                    // onError={(evt) =>
                    //   (evt.target.src = "/pngsV2/flag_dark.png")
                    // }
                  />{" "}
                  {awayTeamNo}
                </div>
                <div className="w-4/12 flex justify-center items-center white oswald f8 f7-l uppercase text-right">
                  {getLangText(lang, words, "TOTAL")} <br />{" "}
                  {getLangText(lang, words, "SELECTED")} <br />{" "}
                  {getLangText(lang, words, "Players")}{" "}
                  <span className="yellow-frc oswald pl-2 f2">
                    {parseInt(homeTeamNo) + parseInt(awayTeamNo)}{" "}
                  </span>{" "}
                </div>
              </div>
              <div className="flex items-center center w-full justify-center cursor-pointer mt-4">
                <div
                  onClick={() =>
                    chooseLogic.length > 0
                      ? (setCurrentFlag("Loading"), fantasyTrack())
                      : ""
                  }
                  className={
                    chooseLogic.length > 0
                      ? "w-20 border-2 border-gray bg-green text-center flex item-center justify-center text-white p-2 uppercase f6 font-medium"
                      : "w-20 border-2 border-gray bg-gray text-center flex item-center justify-center text-white p-2 uppercase f6 font-medium"
                  }
                >
                  {getLangText(lang, words, "PROCEED")}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
