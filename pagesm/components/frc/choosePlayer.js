import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_SQUAD_FOR_BUILD_TEAM } from '../../api/queries';
import { getLangText } from '../../api/services';
import { words } from './../../constant/language';
import ImageWithFallback from '../commom/Image';

const playerPlaceholder = '/pngsV2/playerPH.png';

// setPlayers={setPlayers} players={players} setBat={setBat} bat={bat} setBol={setBol} bol={bol} all={all} setAll={setAll}
export default function ChoosePlayer({
  playerID,
  setPlayerID,
  players,
  totalCredits,
  setTotalCredits,
  setCurrentFlag,
  setPlayers,
  rules,
  FantasymatchID,
  teamDetails,
  setAwayTeamNo,
  setHomeTeamNo,
  ...props
}) {
  const lang = props.language;

  const [windows, setLocalStorage] = useState({});
  const [newToken, setToken] = useState('');
  const flagPlaceHolder = '/pngsV2/flag_dark.png';

  const playerCategories = [
    {
      title: 'BATTERS',
      value: 'batsman'
    },
    {
      title: 'BOWLERS',
      value: 'bowler'
    },
    {
      title: 'ALL_ROUNDERS',
      value: 'all_rounder'
    },
    {
      title: 'WICKET_KEEPERS',
      value: 'keeper'
    },
  ]

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem('tokenData'));
    }
  }, [global.window]);

  const { loading, error, data } = useQuery(GET_SQUAD_FOR_BUILD_TEAM, {
    variables: { matchID: FantasymatchID, token: newToken }

    // onCompleted: (data) => {
    //   setArticleCount(data.getArticlesLikes.articlesCount);
    // }
  });

  useEffect(() => {
    setPlayerID(players.map((x, y) => x.playerId)),
      setHomeTeamNo(players.filter((x) => x.teamName === teamDetails[0].homeTeamShortName).length),
      setAwayTeamNo(players.filter((x) => x.teamName === teamDetails[0].awayTeamShortName).length);
  }, [players]);

  if (loading && !data) {
    <div className='bg-black min-vh-70'></div>;
  }
  const notSelctPlayer = ' flex  justify-center items-center w-1/3 ';
  const selctPlayer = 'flex  justify-center items-center w-1/3 ';
  const [errMsg, seterrMsg] = useState('');
  const [isError, setisError] = useState(false);
  const [playerType, setPlayerType] = useState('');
  const [batsmanCount, setBatsmanCount] = useState(0);
  const [allRounderCount, setAllRounderCount] = useState(0);
  const [bowlerCount, setBowlerCount] = useState(0);
  const [keeperCount, setKeeperCount] = useState(0);
  const [playersArray, setPlayersArray] = useState([]);

  let maxKeeper = 0;
  let maxBatsman = 0;
  let maxBowler = 0;
  let maxAllRounder = 0;

  const handlePress = (type) => {
    players.filter((x, y) => x.playerId === type.playerId).length === 0
      ? players.length <= 4 ||
        (players.length === 5 && totalCredits + parseFloat(type.credits) <= 58) ||
        (players.length === 6 && totalCredits + parseFloat(type.credits) <= 66)
        ? (type.player_role === 'KEEPER' && keeperCount < maxKeeper) ||
          (type.player_role === 'BATSMAN' && batsmanCount < maxBatsman) ||
          (type.player_role === 'BOWLER' && bowlerCount < maxBowler) ||
          (type.player_role === 'ALL_ROUNDER' && allRounderCount < maxAllRounder)
          ? (setPlayers([...players, type]), addition(type.player_role, type.credits))
          : errResolver(type.credits, type.player_role)
        : errResolver(type.credits, type.player_role)
      : (setPlayers(players.filter((o) => o.playerId != type.playerId)), deletion(type.player_role, type.credits));
  };
  const deletion = (role, credits) => {
    if (role === 'KEEPER') {
      setKeeperCount(keeperCount - 1);
      setTotalCredits(totalCredits - parseFloat(credits));
    } else if (role === 'BATSMAN') {
      setBatsmanCount(batsmanCount - 1);
      setTotalCredits(totalCredits - parseFloat(credits));
    } else if (role === 'BOWLER') {
      setBowlerCount(bowlerCount - 1);
      setTotalCredits(totalCredits - parseFloat(credits));
    } else if (role === 'ALL_ROUNDER') {
      setAllRounderCount(allRounderCount - 1);
      setTotalCredits(totalCredits - parseFloat(credits));
    }
  };

  const addition = (role, credits) => {
    if (role === 'KEEPER') {
      setKeeperCount(keeperCount + 1);
      setTotalCredits(totalCredits + parseFloat(credits));
    } else if (role === 'BATSMAN') {
      setBatsmanCount(batsmanCount + 1);
      setTotalCredits(totalCredits + parseFloat(credits));
    } else if (role === 'BOWLER') {
      setBowlerCount(bowlerCount + 1);
      setTotalCredits(totalCredits + parseFloat(credits));
    } else if (role === 'ALL_ROUNDER') {
      setAllRounderCount(allRounderCount + 1);
      setTotalCredits(totalCredits + parseFloat(credits));
    }
  };

  const showError = (msg) => {
    seterrMsg(msg);
    setisError(true);
    setTimeout(() => {
      setisError(false);
    }, 2000);
  };
  const errResolver = (credit, playerType) => {
    if (players.length === 7) {
      const errMsg = lang === 'HIN' ? 'आप अधिकतम 7 खिलाड़ियों का चयन कर सकते हैं' : 'Cannot select more than 7 players';
      showError(errMsg);
    } else if (totalCredits + parseFloat(credit) > 58 && players.length === 5) {
      const errMsg =
        lang === 'HIN'
          ? 'कुल क्रेडिट 6 खिलाड़ियों के लिए 58 से अधिक नहीं हो सकते '
          : 'Total credits cannot exceed 58 for 6 players';
      showError(errMsg);
    } else if (totalCredits + parseFloat(credit) > 66 && players.length === 6) {
      const errMsg =
        lang === 'HIN'
          ? 'कुल क्रेडिट 7 खिलाड़ियों के लिए 66 से अधिक नहीं हो सकते'
          : 'Total credits cannot exceed 66 for 7 players';
      showError(errMsg);
    } else if (playerType === 'KEEPER' && players.length != 7) {
      const errMsg =
        lang === 'HIN'
          ? `${maxKeeper} KEEPER से अधिक का चयन नहीं कर सकते`
          : `Cannot Select more than ${maxKeeper} KEEPER`;
      showError(errMsg);
    } else if (playerType === 'BATSMAN' && players.length != 7) {
      const errMsg =
        lang === 'HIN'
          ? `${maxBatsman} BATSMAN से अधिक का चयन नहीं कर सकते`
          : `Cannot Select more than ${maxBatsman} BATSMAN`;
      showError(errMsg);
    } else if (playerType === 'BOWLER' && players.length != 7) {
      const errMsg =
        lang === 'HIN'
          ? `${maxBowler} BOWLER से अधिक का चयन नहीं कर सकते`
          : `Cannot Select more than ${maxBowler} BOWLER`;
      showError(errMsg);
    } else if (playerType === 'ALL_ROUNDER' && players.length != 7) {
      const errMsg =
        lang === 'HIN'
          ? `${maxAllRounder} ALL ROUNDER से अधिक का चयन नहीं कर सकते`
          : `Cannot Select more than ${maxAllRounder} ALL ROUNDER`;
      showError(errMsg);
    }
  };

  rules &&
    rules.players &&
    rules.players.map(function (ele) {
      if (ele.role === 'KEEPER') {
        maxKeeper = ele.max;
      } else if (ele.role === 'BATSMAN') {
        maxBatsman = ele.max;
      } else if (ele.role === 'ALL_ROUNDER') {
        maxAllRounder = ele.max;
      } else if (ele.role === 'BOWLER') {
        maxBowler = ele.max;
      }
    });

  return data ? (
    <div className='white relative '>
      <div className=' flex justify-between my-4 p-2'>
        <div className='w-3/4'>
          <div className=' text-lg font-semibold'> {getLangText(lang, words, 'BYST')} </div>
          <div className='h-1 w-15 bg-blue-8 rounded my-1 mb-2'></div>
          <div className='text-xs font-medium text-gray-2'>
            {getLangText(lang, words, 'Pick-1-7')}.{getLangText(lang, words, 'TROYTW')}
          </div>
        </div>
        <div>
          <div className='bg-gray p-2 rounded-xl'>
            <div className='text-xs text-center'>
              Total <br></br> {getLangText(lang, words, 'Credits')}
            </div>
            <div className='text-green text-lg font-semibold text-center'>100</div>
          </div>
        </div>
      </div>



      {
        playerCategories.map(category => {
          return (
            <div className='bg-gray-4 mt-3 rounded-xl'>
              <div className='text-sm font-semibold p-3'>{getLangText(lang, words, category.title)} </div>
              <div className=' flex flex-wrap justify-around items-center bg-gray rounded-xl px-2 pb-4'>
                {
                  data?.getSquadForBuildTeam?.[category.value]?.map((item, index) => {
                    return (
                      <div
                        key={index}
                        onClick={() => {
                          handlePress(item);
                        }}
                        className={`cursor-pointer pt-3  flex  justify-center items-center w-1/3`}>
                        <div>
                          <div className='relative bg-gray-8 w-20 h-20 rounded-full'>
                            <div
                              className={`overflow-hidden w-full h-full rounded-full  ${players.filter((x) => x.playerId === item.playerId).length > 0 ? 'border-2 border-green' : ''
                                }`}>
                              <ImageWithFallback height={40} width={40} loading='lazy'
                                fallbackSrc = {playerPlaceholder}
                                className='object-top object-contain w-20  '
                                src={`https://images.cricket.com/players/${item.playerId}_headshot.png`}
                                alt=''
                                // onError={(e) => (e.target.src = playerPlaceholder)}
                              />
                            </div>
                            <div className='absolute bottom-0 right-0 border rounded border-white'>
                              <ImageWithFallback height={22} width={22} loading='lazy'
                                fallbackSrc = {playerPlaceholder}
                                className='w-6 h-4 rounded'
                                src={`https://images.cricket.com/teams/${item.teamID}_flag_safari.png`}
                                // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                alt=''
                              />
                            </div>
                          </div>
                          <div className=' flex justify-center flex-col items-center pt-3'>
                            <div className='text-xs font-semibold'>
                              {' '}
                              {lang === 'HIN' ? item.playerNameHindi : item.playerName}{' '}
                            </div>
                            <div className='mt-1 text-xs text-gray-2 font-medium'>
                              {' '}
                              {getLangText(lang, words, 'Credits')}:{' '}
                              <span className='text-green font-semibold'>{item.credits}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>

          )
        })
      }

      
      <div className='h-24  h2-l'></div>


      <div className=' bottom-0 fixed  z-999 left-0  right-0 lg:hidden md:hidden'>
        {isError && (
          <div className='flex'>
            <div className='bg-frc-yellow f6 white fw1 flex justify-center items-center pa2 w-100 mw75-l center'>
              <div className='ba br-100 bw1 pa2 h2 w2 white flex justify-center items-center fw5'> ! </div>
              <div className='pl2 f6 fw5'>{errMsg}</div>
            </div>
          </div>
        )}
        <div className='   bg-basebg rounded-t-xl border-t-2 border-gray-2  flex items-center'>
          <div className='w-full p-3 flex justify-between items-center'>
            <div className='  flex items-center justify-between w-4/6'>
              <div className=' bg-gray-4 p-2 rounded-md text-lg font-semibold justify-center  flex items-center flex-col h-[80px]'>
                {' '}
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = "/pngsV2/flag_dark.png"
                  className='w-8 h-5 mb-1'
                  src={`https://images.cricket.com/teams/${teamDetails[0].homeTeamID}_flag_safari.png`}
                  alt=''
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                />{' '}
                {players.filter((x) => x.teamName === teamDetails[0].homeTeamShortName).length}
              </div>
              <div className=' flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-lg font-semibold  h-[80px]'>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = "/pngsV2/flag_dark.png"
                  className='w-8 h-5 mb-1'
                  src={`https://images.cricket.com/teams/${teamDetails[0].awayTeamID}_flag_safari.png`}
                  alt=''
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                />{' '}
                {players.filter((x) => x.teamName === teamDetails[0].awayTeamShortName).length}
              </div>

              <div className=' flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-sm font-semibold  h-[80px] '>
                <span className='text-xs font-medium text-gray-2'>{getLangText(lang, words, 'Players')}</span>
                <div className='text-green'>
                  <span className='text-3xl font-semibold'>{players.length}</span>
                  <span className='text-md font-medium'>/11</span>
                </div>
              </div>
              <div className=' flex items-center justify-center flex-col bg-gray-4 rounded-md p-2 text-sm font-semibold  h-[80px]'>
                <span className='text-xs font-medium text-gray-2 text-center'>{getLangText(lang, words, 'Credits')} <br></br> Left</span>

                <span className='text-3xl font-semibold text-green'>{100 - totalCredits}</span>
              </div>
            </div>
            <div className='flex items-center  center justify-end w-1/3 uppercase text-sm font-semibold'>
              <div
                onClick={() => (players.length >= 1 && players.length <= 7 ? setCurrentFlag('choose logic') : '')}
                className={
                  players.length >= 1 && players.length <= 7
                    ? ' border-green border-2 rounded-lg bg-gray-8 tc px-3 py-1 text-green  '
                    : ' border-gray-2 border-2 rounded-lg bg-gray-8 tc px-3 py-1 text-gray-2 '
                }>
                {getLangText(lang, words, 'PROCEED')}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='hidden lg:block md:block'>
        {isError && (
          <div className='flex'>
            <div className='bg-frc-yellow f6 white fw1 flex justify-center items-center pa2 w-100 mw75-l center'>
              <div className='ba br-100 bw1 pa2 h2 w2 white flex justify-center items-center fw5'> ! </div>
              <div className='pl2 f6 fw5'>{errMsg}</div>
            </div>
          </div>
        )}
        <div>
          <div className='w-full '>
            <div className=' flex-wrap flex justify-around items-center '>
              <div className='w-25 flex justify-center gap-2 items-center '>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = "/pngsV2/flag_dark.png"
                  className='w-16 h-10 mr-1 mr1'
                  src={`https://images.cricket.com/teams/${teamDetails[0].homeTeamID}_flag_safari.png`}
                  alt=''
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                />
                <span>{players.filter((x) => x.teamName === teamDetails[0].homeTeamShortName).length}</span>
              </div>
              <div className='w-25 flex justify-center gap-2 items-center '>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = "/pngsV2/flag_dark.png"
                  className='w-16 h-10 mr-1 mr1'
                  src={`https://images.cricket.com/teams/${teamDetails[0].awayTeamID}_flag_safari.png`}
                  alt=''
                  // onError={(evt) =>
                  //   (evt.target.src = "/pngsV2/flag_dark.png")}
                />{' '}
                <span>{players.filter((x) => x.teamName === teamDetails[0].awayTeamShortName).length}</span>
              </div>
              <div className='w-25 flex justify-center items-center white oswald f8 f7-l ttu tr '>
                {getLangText(lang, words, 'TOTAL')} <br /> {getLangText(lang, words, 'SELECTED')} <br />{' '}
                {getLangText(lang, words, 'Players')}
                <span className='yellow-frc oswald pl-3 f2'>{players.length} </span>{' '}
              </div>
            </div>
            <div className='flex items-center w-full justify-center mt-4 cursor-pointer'>
              <div
                onClick={() => (players.length >= 1 && players.length <= 7 ? setCurrentFlag('choose logic') : '')}
                className={
                  players.length >= 1 && players.length <= 7
                    ? 'w-30  border-2 border-gray rounded-sm border-solid bg-green text-center flex item-center justify-center text-white p-2 uppercase font-mmedium'
                    : 'w-30  border-2 border-gray rounded-sm border-solid bg-dark-gray text-center flex item-center justify-center text-white p-2 uppercase text-sm font-medium'
                }>
                {getLangText(lang, words, 'PROCEED')}
              </div>
            </div>
          </div>
        </div>
        <div className='h37  h4'></div>
      </div>

    </div>
  ) : (
    <div></div>
  );
}
