import React, { useState } from 'react'
// const frc_allround='/svgs/frc_allround.svg'
import white_allRounder from '../../public/pngs/allRounder-white-solid.png'
import white_ball from '../../public/pngs/ball-white-solid.png'
import white_bat from '../../public/pngs/bat-white-solid.png'
import white_wicket from '../../public/pngs/wicketKeeper-white-solid.png'
import black_bat from '../../public/svgs/black_bat.svg'
import black_allRounder from '../../public/svgs/black_allRounder.svg'
import black_ball from '../../public/svgs/black_ball.svg'
import black_keeper from '../../public/svgs/black_keeper.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import { getLangText } from '../../api/services'
import { words } from './../../constant/language'
import ImageWithFallback from '../commom/Image'
export default function CompareModelPlayerHub(props) {
  const [type, setType] = useState(props.Player2Obj.playerSkill)

  let filterRole = {
    "Bowler": "bowler",
    "Batsman": "batsman"
  }
  // console.log("Props model", props)
  return (
    <div
      className='flex justify-center items-center fixed absolute--fill z-9999 w-full  overflow-y-scroll '
      style={{ backdropFilter: 'blur(1px)' }}>

      <div className=' rounded  bg-gray  border relative w-11/12 md:w-5/12 lg:w-5/12 '>
        <div className='  flex  flex-row  items-center justify-between w-full mt-2'>
          <div className='flex  justify-start   px-2 items-center  text-base uppercase  font-semibold'>{props.lang === "HIN" ? 'वर्तमान चयन' : 'current selection'}</div>
          <div className='flex  justify-end items-center  mr-2 text-base font-semibold' onClick={() => { props.closeNewModelV2() }}
          >X</div>
        </div>
        <div className='flex w-12 h-1 lg:w-18 bg-blue-8 mt-1 rounded my-3 ml-2 '></div>
        {/* <div className=" ">
              <div className="flex  items-center mv2    justify-between  bg-white-10 ba b--yellow ph2 pv1 br2 ph3-l  mv2 mh2 mh3-l mh3-m">
                <div className="flex items-center">
                  <div className="w2 h2 w2-6-l h2-6-l w2-6-m h2-6-m br-100  ba b--white-20 bg-gray overflow-hidden ">
                    <img className="w-100 h-100 object-cover object-top" src={`https://images.cricket.com/players/${ props.Player2Obj.playerID}_headshot_safari.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}  alt=""></img>
                  </div>
                
                  <div className="ttu f5 f4-l f4-m oswald fw6 ttu ph2">{props.lang==="HIN" ? props.RecentPlayer.playerNameHindi: props.Player2Obj.name}</div>
                </div>
                <div className="flex items-center">
                <div className="ttu f5 f4-l f4-m fw6 oswald white-40   ttu ph2">{ props.Player2Obj.teamShortName}</div>
                  <div className="w15 w2-l h2-l  w2-m h2-m h15 br-100 overflow-hidden ">
                    <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${ props.Player2Obj.teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                  </div>   
                </div>
              </div>
            </div> */}
        {/* <div className="mh2 mt4 mb2 bb b--white-20"></div> */}
        {/* <div className=" f5 fw6 white ph2  ttu">{props.lang==="HIN"?'खिलाड़ी बदलें':'CHANGE PLAYER'}</div> */}
        <div className={`flex p-2 items-center justify-between bg-gray-8 mx-3 rounded-full  `}>
          {<div className={` flex justify-center white w-2/12  ${type === 'KEEPER' ? 'bg-gray-8 p-1 rounded-3xl border border-green ' : ''}`}>
            <div className="cursor-pointer flex flex-column items-center " onClick={() => { setType('KEEPER') }} >
            <div className="text-xs " > <div className={` 
             flex p-2 text-white items-center justify-center    `}>
            {getLangText(props.lang, words, "WK")}
            </div>
            </div>
            {/* <div className={`fw6 f7 tc gray pt1`} >{getLangText(props.lang, words, "WK")}</div> */}
          </div></div>}
          <div className={`${props.SelectionType === 'matchups' ? '' : ' '}  flex justify-center white w-3/12   ${type === 'BATSMAN' ? 'bg-gray-8 p-2 rounded-3xl border border-green ' : ''} `}>
            <div className="cursor-pointer flex flex-column items-center " onClick={() => { setType('BATSMAN') }}>
            <div className="text-xs">
              <div className={`  flex text-white items-center text-xs justify-center }   `}>
              {getLangText(props.lang, words, "BATTERS")}
              </div>
              </div>

            {/* <div className="fw6 f7 tc gray pt1" >{getLangText(props.lang, words, "BATTERS")}</div> */}
          </div></div>

         
          
          <div className={`${props.SelectionType === 'matchups' ? '' : ' '}  flex justify-center white w-3/12  ${type === 'BOWLER' ? 'bg-gray-8 p-2 rounded-3xl border border-green ' : ''} `}>
            <div className="cursor-pointer flex flex-column items-center" onClick={() => { setType('BOWLER') }}>
            <div className="text-xs"> 
            <div className={`  flex text-white items-center justify-center   `}>
            {getLangText(props.lang, words, "BOWLERS")}
              </div></div>
            {/* <div className="fw6 f7 tc gray pt1" >{getLangText(props.lang, words, "BOWLERS")}</div> */}
          </div></div>
          <div className={` flex justify-center white w-4/12 p-1   ${type === 'ALL_ROUNDER' ? 'bg-gray-8 p-2 rounded-3xl border border-green ' : ''}`}><div className="cursor-pointer flex flex-column items-center" onClick={() => { setType('ALL_ROUNDER') }}>
            <div className="text-xs"> 
            <div className={` flex text-white items-center justify-center    `}>
            {getLangText(props.lang, words, "ARS")}
              </div>
              </div>
          
          </div></div>
        </div>
        <div className=' overflow-y-scroll overflow-hidden hidescroll ' style={{ maxHeight: "57vh" }}>


          {(props.playersList.filter(player => player.playerID !== props.RecentPlayer.playerID && player.playerSkill === type && player.playerID !== props.Player2Obj.playerID)).map((player, i) =>
            <div className=" mt-2" key={i} onClick={() => props.choosePlayer(player, props.SelectionType)}>
              <div className="flex  items-center m-2  justify-between border-b border-gray-4  py-2 ">
                <div className="flex items-center ">
                  <div className="flex items-center justify-center w-14 h-14 rounded-full bg-gray-8  overflow-hidden ">
                    <ImageWithFallback height={18} width={18} loading='lazy' className="w-14 h-14  object-cover object-top  rounded-full" 
                    src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`} 
                    alt="" onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} />
                  </div>
                  <div className="text-xs font-semibold ml-2">{props.lang === "HIN" ? player.playerNameHindi : player.name}</div>
                </div>
                <div className="flex items-center">
                  <div className="text-white text-xs font-semibold mr-2">{player.teamShortName}</div>
                  <div className="w15 h15 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                    <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="w-10 h-6 object-top object-cover" src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`} alt="" />
                  </div>

                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>

  )
}
