import React, { useState } from "react";
import PostSuccessModal from "../commom/postSuccessModal";

const Modal = ({
  title,
  showModal,
  submitButtonText,
  placeholderText,
  setshowModal,
  errorText,
  setName,
  name,
  submitMsg,
  submitCallback,
}) => {
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  return (
    <>
      {showModal && (
        <div className="grid fixed top-0 left-0 place-items-center w-screen bg-gray-800 h-screen z-20 overflow-hidden">
          <div className={`${success?'relative flex flex-col w-80 md:w-3/12 lg:3/12 xl:3/12':'border-2 border-gray-300 rounded-md shadow-lg relative flex flex-col w-80 md:w-3/12 lg:3/12 xl:3/12 bg-gray-700  outline-none focus:outline-none'}`}>
            <div>
              {!success ? (
                <>
                  <div className="flex items-center justify-center p-3 border-solid text-gray-300">
                    <h3 className="text-xl pt-5">{title}</h3>
                    <button
                      className="text-white float-right mr-6 md:mr-96 xl:mr-96 lg:mr-96 fixed md:right-72 xl:right-72 lg:right-72 right-3 px-2 -mt-24"
                      onClick={() => setshowModal((prev) => !prev)}
                    >
                      X
                    </button>
                  </div>
                  <div className="relative p-6 flex-auto">
                    <span>
                      <input
                        type="text"
                        value={name}
                        id="name"
                        placeholder={placeholderText}
                        className="my-2 bg-gray-700 w-full border-b-gray-600 border-b-2 outline-none text-white-500"
                        //  bg-gray  px-2 pb-1 border-b border-black ml-2 mt-4 w-full
                        onChange={(e) => setName(e.target.value)}
                      />
                      {error && (
                        <p className="text-red-500">
                          {errorText}
                        </p>
                      )}
                    
                    </span>
                      <div className="flex items-center justify-center p-6 border-solid">
                        <button
                          className={`${name.length ? 'cursor-pointer rounded-md font-bold text-green-500 uppercase text-sm px-8 py-2 border-green-500 border-2 border-solid' : 'cursor-pointer text-gray bg-gray-800 rounded-md font-bold uppercase text-sm px-8 py-2 border-gray border-2 border-solid'}`}
                          type="button"
                          disabled={name.length===0}
                           onClick={async () => {
                            if (name.length===0) {
                              return;
                              
                            }
                            const res = await submitCallback();
                            if(res.data && !res.data.editMyFantasyTeamName){
                              setError(true)
                            } else  {
                              setSuccess(!success);
                              setName("")
                              setTimeout(() => setshowModal(!showModal), 2000);
                            }
                           
                          }}
                        >
                          {submitButtonText}
                        </button>
                      </div>
                  </div>
                </>
              ) : (
                <PostSuccessModal Msg={submitMsg}/>
              )}
            </div>
            <div />
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;
