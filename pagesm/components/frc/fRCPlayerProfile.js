import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { FANTASY_PLAYER_RECORDS } from '../../api/queries';
const fallbackProjection = '/pngs/fallbackprojection.png';
import ImageWithFallback from '../commom/Image';

export default function FRCPlayerProfile(props) {
  const [imgError, setImgError] = useState(false);
  const { loading, error, data } = useQuery(FANTASY_PLAYER_RECORDS, {
    variables: { playerID: props.playerId }
  });

  if (loading) return <div></div>;
  if (error)
    return (
      <div className='relative br3 '>
        <div className='cdcgr white shadow-4 h3 br3 flex justify-center items-center fw6'>No Player Data Found</div>
      </div>
    );
  else
    return (
      <div>
        <div className=''>
          <div className='br3'>
            <div className={`w-100 ${imgError && 'flex justify-center pt2 bg-grey_5_1'}`}>
             
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = {fallbackProjection}
                  className='w-100 h4'
                  src={`https://images.cricket.com/players/${data.fantasyPlayersRecords.playerID}_actionshot.jpg`}
                  alt=''
                  // onError={(evt) => (evt.target.src = setImgError(true))}
                />
              
            </div>
            <div className='ph3'>
              <div className='f6 pt1 fw5'> {data.fantasyPlayersRecords.playerName}</div>
              <div className='flex justify-between'>
                <div className='pv2 w-50'>
                  <div className='f7 f6-ns fw6 orange  '>Batting Style</div>
                  <div className='f7 f6-ns fw3'> {data.fantasyPlayersRecords.battingStyle} </div>
                </div>

                <div className='pv2 w-50'>
                  <div className='f7 f6-ns fw6 orange '>Bowling Style</div>
                  <div className='f7 f6-ns fw3 '> {data.fantasyPlayersRecords.bowlingStyle} </div>
                </div>
              </div>
              <div className='flex justify-between'>
                <div className='pv2 w-50'>
                  <div className='f7 f6-ns fw4 gray'>Selected by</div>
                  {data.fantasyPlayersRecords.SelectionPercentage === '' ? (
                    '--'
                  ) : (
                    <div className='f7 f6-ns fw6'>{data.fantasyPlayersRecords.SelectionPercentage}</div>
                  )}
                </div>

                <div className='pv2 w-50'>
                  <div className='f7 f6-ns fw4 gray'>Credits</div>
                  {data.fantasyPlayersRecords.playerCredits === '' ? (
                    '--'
                  ) : (
                    <div className='f7 f6-ns fw6 '> {data.fantasyPlayersRecords.playerCredits} </div>
                  )}
                </div>
              </div>
              <div className='flex justify-between'>
                <div className='pv2 w-50'>
                  <div className='f7 f6-ns fw4 gray'>Strength</div>
                  <div className=''>
                    {/* <span className="f7 fw3 pr2">Avg</span> */}
                    {data.fantasyPlayersRecords.strength === '' ? (
                      '--'
                    ) : (
                      <span className='f7 fw5 pr1'>{data.fantasyPlayersRecords.strength}</span>
                    )}
                    {/* <span className="f7 fw3">vs WI</span> */}
                  </div>
                </div>

                <div className='pv2 w-50'>
                  <div className='f7 f6-ns fw4 gray'>Weakness</div>
                  <div className=''>
                    {data.fantasyPlayersRecords.weakness === '' ? (
                      '--'
                    ) : (
                      <span className='f7 fw5 pr2'>{data.fantasyPlayersRecords.weakness}</span>
                    )}
                    {/* <span className="f6 fw6 gray">53.50</span> */}
                  </div>
                </div>
              </div>
              <div className='divider mt2' />
              <div className='w-100'>
                <div className='flex justify-between pv2 fw5'>
                  <div className='w-25 f7 center tc'>Last 5 Matches</div>
                  <div className='v-divider' />
                  <div className='w-30 f7 center tc'>Total Fantasy Points</div>
                  <div className='v-divider' />
                  <div className='w-20 f7 tc'>Runs Scored</div>
                  <div className='v-divider' />
                  <div className='w-20 f7 center tc'>Wickets Taken</div>
                </div>
                <div className='divider' />
                {data.fantasyPlayersRecords.matchesData.map((x, y) => (
                  <div key={y}>
                    <div className='flex justify-between items-center pv2'>
                      <div className='w-25 center tc'>
                        <div className='f6 fw3'>vs {x.againstTeam}</div>
                        <div>
                          <span className='f7 pr1 fw5'>{x.matchType.split(' - ')[0]}</span>
                          <span className='red f7 fw5'>{x.matchType.split(' - ')[1]}</span>
                        </div>
                      </div>
                      <div className='w-30 f6 fw2 center tc'>{x.TotalFantasyPoint}</div>
                      {x.playerRun === 'dnb' ? (
                        <div className='w-20 f6 fw2 tc'>--</div>
                      ) : (
                        <div className='w-20 f6 fw2 tc'>{x.playerRun}</div>
                      )}
                      {x.playerwickets === 'dnb' ? (
                        <div className='w-20 f6 fw2 tc'>--</div>
                      ) : (
                        <div className='w-20 f6 fw2 center tc'>{x.playerwickets}</div>
                      )}
                    </div>
                    {y !== data.fantasyPlayersRecords.matchesData.length - 1 && <div className='divider' />}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}
