import { useQuery, useMutation } from '@apollo/react-hooks';
import React, { useState, useEffect } from 'react';
import Countdown from 'react-countdown-now';
import { format } from 'date-fns';
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import backIcon from '../../public/svgs/back_dark_black.svg';
import stadium from '../../public/pngs/frc_stadium.png';
const flagPlaceHolder = '/svgs/images/flag_empty.png';
import stats_hub_star from '../../public/svgs/statHubStart.svg';
import buildyourTeamArrow from '../../public/svgs/buildYourTeamArrow.svg';
import saveTeam from '../../public/svgs/saveTeamYellowicon.svg';
import rightArrow from '../../public/svgs/upArrowWhite.svg';
// import Swiper from 'react-id-swiper';
import { getFrcSeriesTabUrl } from '../../api/services';
import FeaturedArticlesFRC from './featureArtcileFrc';
import ScrollBar from './pegScroll'
import { GET_ALGO_11, GET_ARTICLES_BY_ID_AND_TYPE } from '../../api/queries';
const playerPlaceholder = '/pngs/fallbackprojection.png';
import { ARTICLE_VIEW, News_Live_Tab_URL } from '../../constant/Links';
 import ArticleFRC from './featureArtcileFrc';
import { useRouter } from 'next/router';
import { words } from '../../constant/language';
// import BYTNew from '../../public/svgs/BYTNew.png';
import BYTNewMobile from '../../public/svgs/BYTNewMobile.png';
// import FantasyStatsNew from '../../public/svgs/newfanstat.svg';
// import language_icon from '../../public/svgs/language_icon.svg';
import LanguageModel from './languageModel';
// import FRCVideoIcon from '../../public/pngsV2/frcbanner.png';
import BuildYourTeam from './suggestTeam'
import HindiFRCVideoBanner from '../../public/HindiFRCVideoBanner.png';
import PlayVideoModel from './playVideoModel';
import Loading from '../loading';
import DataNotFound from '../commom/datanotfound';
import ImageWithFallback from '../commom/Image';
const BYTNew = '/pngsV2/bytnew.png'

const language_icon = '/pngsV2/lang.png'
export default function FantasyDetails(props) {
  console.log("props.dataprops.dataprops.data", props)
  const FRCVideoIcon = '/pngsV2/frcbanner.png'
  const FantasyStatsNew = '/svgs/newfanstat.svg'
  const [language, setlanguage] = useState(
    typeof window !== 'undefined' && global.window.localStorage.getItem('FRClanguage')
      ? global.window.localStorage.getItem('FRClanguage')
      : 'ENG'
  );
  // console.log("languagelanguage", props.language)
  // console.log("props",props)
  let router = useRouter();
  let lang = props.language;
  // console.log("dkjgdlkhflj",lang)
  const [algoteam, setalgoTeam] = useState(null);
  let FantasymatchID = props.matchID;
  const [newToken, setToken] = useState('');
  const [windows, setLocalStorage] = useState({});
  const [algo11Code, setalgo11Code] = useState('');
  const [toggle, setToggle] = useState(false);
  const [click, setclick] = useState(false);
  const [openAlgo11, setOpenalgo11] = useState(false);
  const [languagemodel, setlanguagemodel] = useState(false);
  const [load, setLoad] = useState(false);
  const [playvideomodel, setplayvideo] = useState(false);
  const data =
  {
    ...props?.data?.miniScoreCard?.data[0],
    awayTeamShortName: props?.data?.miniScoreCard?.data[0]?.awayTeamName,
    homeTeamShortName: props?.data?.miniScoreCard?.data[0]?.homeTeamName,
    MatchName: props?.data?.miniScoreCard?.data[0]?.matchName
  }

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem('tokenData'));
    }
  }, [global.window]);


  const {
    loading: loadingArticle,
    error: errorArticle,
    data: articleData
  } = useQuery(GET_ARTICLES_BY_ID_AND_TYPE, {
    variables: { type: 'matches', Id: FantasymatchID }
  });
  // console.log(data,'dataaa')

  let seriesID =
    props &&
    props.data &&
    props.data.miniScoreCard && props.data.miniScoreCard.data &&
    props.data.miniScoreCard.data &&
    props.data.miniScoreCard.data.seriesID;

  const fantasyTrack = (event, source) => {
    CleverTap.initialize(event, {
      Source: source,
      MatchID: props.matchID,
      SeriesID: seriesID || '',
      TeamAID: data?.homeTeamID,
      TeamBID: data?.awayTeamID,
      MatchFormat: data?.matchType,
      MatchStartTime: data?.matchDateTimeGMT && format( Number(data?.matchDateTimeGMT), 'd,y, h:mm a'),
      MatchStatus: data.matchStatus,
      Platform: windows && windows.localStorage ? windows.localStorage.Platform : ''
    });
  };

  const {
    loading,
    error,
    data: algo11
  } = useQuery(GET_ALGO_11, {
    variables: { matchID: FantasymatchID, token: newToken },
    // fetchPolicy: 'network-only',
    
    onCompleted: (algo11) => {
      console.log('myalgo11 -> ',algo11);
      if (algo11 && algo11.getAlgo11.saveTeams) {
        setalgoTeam([...(algo11.getAlgo11.batsman || []), ...(algo11.getAlgo11.all_rounder || []), ...(algo11.getAlgo11.bowler || []), ...(algo11.getAlgo11.keeper || [])]);
      } 
    }
    
  });



  const getUpdatedTime = (updateTime) => {
    // let uT=1618808000000;
    let currentTime = new Date().getTime();
    let distance = currentTime - updateTime;
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

    let result =
      (hours > 0 ? hours + (hours > 1 ? ' hrs ' : ' hr ') : '') +
      minutes +
      (minutes > 1 ? ' mins ' : ' min ') +
      (lang === 'HIN' ? 'पहले' : 'ago');
    return result;
  };

  const getLangText = (lang, keys, key) => {
    // console.log("keys[key]",keys[key])
    let [english, hindi] = keys[key];
    switch (lang) {
      case 'HIN':
        return hindi;
      case 'ENG':
        return english;
      default:
        return english;
    }
  };
  const getNewsUrl = (article) => {
    if (article.type !== 'live-blog') {
      let articleId = article.articleID;
      let tab = article.type;
      return {
        as: eval(ARTICLE_VIEW.as),
        href: ARTICLE_VIEW.href
      };
    }

    if (article.type === 'live-blog') {
      let matchId = article.matchIDs[0].matchID;
      let articleID = article.articleID;
      let matchName = article.matchIDs[0].matchSlug;

      return {
        as: eval(News_Live_Tab_URL.as),
        href: News_Live_Tab_URL.href
      };
    }
  };
  if (loading) {
    <Loading/>
  }
  if (error) {
    <DataNotFound/>
  }
  const params = {
    slidesPerView: 4,
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },

    navigation: {
      nextEl: '#nbutton',
      prevEl: '#pbutton'
    },
    spaceBetween: 5,
    breakpoints: { 650: { slidesPerView: 8, spaceBetween: 20 } }
  };

  const handleMonthHindi = () => {
    let monthformat = format(Number(data.matchDateTimeGMT) - 19800000, 'MMMM');

    let date = format(Number(data.matchDateTimeGMT) - 19800000, 'do');
    //  console.log("monthformat",monthformat)

    if (lang === 'HIN') {
      return getLangText(lang, words, monthformat.toLowerCase()) + ' ' + date;
    } else {
      return monthformat + ' ' + date;
    }
  };
  console.log("algoteamalgoteam", algoteam)
  if(load) return <Loading/>
  return (
    // : openAlgo11 &&
    // <Algo11TeamDisplay setOpenalgo11={setOpenalgo11} data={data} setMyTeam={""} myTeam={algo11.getAlgo11} totalPoints={algo11.getAlgo11.teamtotalpoints} algo11codee={algo11Code} />
    <div className=' bg-basebg '>

      <div className=' flex items-center justify-between text-white mt-3 mx-4 lg:hidden md:hidden '>
        <div className='flex items-center justify-center '>  <div className='p-2 bg-gray rounded-md ' onClick={() => window.history.back()}>
          <ImageWithFallback height={18} width={18} loading='lazy'
            className=' flex items-center justify-center h-4 w-4 rotate-180'
            src='/svgsV2/RightSchevronWhite.svg'
            alt=''
          />
        </div>
          <span className='ml-2 font-bold capitalize'>{getLangText(lang, words, 'fantasy_research_center')}</span>
        </div>

        <div className="cursor-pointer" onClick={() => {
          lang === 'HIN'
            ? (props.setlanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG'))
            : (props.setlanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'));
        }}>
          <ImageWithFallback height={18} width={18} loading='lazy' className='h-10 w-10 ' src={language_icon} />
        </div>
      </div>



      {/* <div className=' flex bg-red items-center justify-center w-full hidden lg:block md:block'>
        <div className='flex items-center'>
          <img className='w1 dn-l' onClick={() => router.push(`/fantasy-research-center`)} alt='' src={backIcon} />
          <div className='black f6 fw7 pl3 ttu'>{getLangText(lang, words, 'fantasy_research_center')}</div>
        </div>
        <div
          className='flex cursor-pointer'
          onClick={() => {
            lang === 'HIN'
              ? (props.setlanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG'))
              : (props.setlanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'));
          }}>
          <img className='h-10 w-10 ' src={language_icon} />
        </div>
      
      </div> */}

      {data && data && !data.isLiveCriclyticsAvailable &&
        <div className='flex mx-4 items-center justify-center'>
          <div className='flex flex-col mt-5 bg-gray border border-green  rounded  p-2 w-full    '>
            <div className='flex items-center justify-between '>
              <div className='flex  items-center'>
                {/* {console.log("datadatadata",data)} */}
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = "/pngsV2/flag_dark.png"
                  alt=''
                  src={`https://images.cricket.com/teams/${data.homeTeamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                  className='h-6 w-10 rounded shadow-4 mx-1'
                />
                <div className='oswald f4 fw5  white pl2'>{data.homeTeamShortName}</div>
              </div>
              <div className='flex  items-center justify-center'>
                <div className='flex  items-center justify-center border border-green rounded-full h-8 w-16 bg-basebg'> {data.matchType}</div>
              </div>

              <div className='flex items-center'>
                <div className='oswald f4 fw5 white pr2'>{data.awayTeamShortName}</div>
                <ImageWithFallback height={18} width={18} loading='lazy'
                  fallbackSrc = "/pngsV2/flag_dark.png"
                  alt=''
                  src={`https://images.cricket.com/teams/${data.awayTeamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                  className='h-6 w-10 rounded  shadow-4 mx-1'
                />
              </div>


            </div>
            <div className='flex items-center justify-center text-center text-xs font-bold mt-2'> {data && data && data.matchDateTimeGMT && format(Number(data.matchDateTimeGMT) - 19800000, 'h:mm a')} IST</div>
          </div>
        </div>}

      {/* { data && data && !data.isLiveCriclyticsAvailable ? (
        <div className='relative  tc  flex items-center mb-2 border-green border '>
          <img className='  w-10 h-10 h45-l  h4-m' style={{ borderWidth: '1px' }} src={stadium} />
          <div className='absolute flex item-center justify-center  flex-column  left-0 right-0 tc '>
            <div className='f7 fw6 white px-2  f5-l  mt-2'>
              {lang === 'HIN' ? data.matchNameHindi : data.matchName}
            </div>
            <div className='flex justify-center  ph2 items-center oswald f6 ttu fw6 white mt-2'>
              <div className='f6'> {handleMonthHindi()} </div>
              <div className='px-1'>|</div>
              <div className='f6'> {format(Number(data.matchDateTimeGMT) - 19800000, 'h:mm a')} IST</div>
            </div>
            <div className=' pt-2 px-2 ph7-l'>
              <div className='flex items-center justify-around '>
                <div className='flex  items-center'>
                  <img
                    alt=''
                    src={`https://images.cricket.com/teams/${data.homeTeamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    className='h10 w-10 rounded  shadow-4'
                  />
                  <div className='oswald f4 fw5  white pl2'>{data.homeTeamShortName}</div>
                </div>
                <div className='yellow-frc f6 mh1 fw7 br-100  flex items-center justify-center'>VS</div>
                <div className='flex items-center'>
                  <div className='oswald f4 fw5 white pr2'>{data.awayTeamShortName}</div>
                  <img
                    alt=''
                    src={`https://images.cricket.com/teams/${data.awayTeamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    className='h1-3 w2 br2 shadow-4'
                  />
                </div>
              </div>
              <div>
                <div className=' '>
                  {new Date(Number(data.matchDateTimeGMT) - 19800000) - new Date().getTime() < 84600000 ? (
                    <Countdown
                      date={new Date(Number(data.matchDateTimeGMT) - 19800000)}
                      renderer={(props) => (
                        <span className='pa1 ph2 br2 f8 fw6 white bg-darkRed truncate'>
                          {`Starts in ${props.days !== 0 ? props.days * 24 + Number(props.hours) : props.hours} h ${
                            props.minutes
                          } m ${props.seconds} s `}
                        </span>
                      )}
                    />
                  ) : (
                    <span className='pa1 ph2 br2 f8 fw6 white bg-darkRed truncate'>{`${format(
                      Number(data.matchDateTimeGMT) - 19800000,
                      'do MMMM yyyy, h:mm a'
                    )}`}</span>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className='bg-white-10 shadow-4 ba b--frc-yellow ma2 br2 pa3 '>
          {data &&
            data &&
            data.matchScore.map((x, z) => (
              <div key={z} className='flex items-center   justify-evenly pv2'>
                <span className='flex w-30'>
                  <img
                    alt={x.teamShortName}
                    src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                    onError={(evt) => (evt.target.src = flagPlaceHolder)}
                    className='h1 w15 shadow-4'
                  />
                  <span className='mh2 f6 fw6'> {x.teamShortName} </span>
                </span>
                <div className='w-70 flex items-center tracked-tight '>
                  {x.teamScore &&
                    x.teamScore.map((score, i) => (
                      <span key={i} className={`flex justify-center items-start f6 `}>
                        <span className={``}>
                          {score.runsScored} {score.wickets && score.wickets !== '10' ? `/ ${score.wickets}` : null}
                        </span>

                        {((data.matchType === 'Test' &&
                          data.matchStatus !== 'completed' &&
                          score.inning == data.currentinningsNo) ||
                          data.matchType !== 'Test') && (
                          <div className='ml2'>{score.overs ? `(${score.overs})` : null}</div>
                        )}
                        {score.declared ? <span className='darkRed fw6 oswald f6 ml1'>d</span> : ''}
                        {score.folowOn ? <span className='darkRed fw6 f6 oswald ml1'> f/o</span> : ''}
                        {x.teamScore.length > 1 && i === 0 && <div className='mh2 fw5'>&</div>}
                      </span>
                    ))}
                </div>
              </div>
            ))}
          <div className='mt2'>
            {lang === 'HIN' ? (
              <div className='ph2 pv1 f8 fw5 white tc w-60-l bg-white-20 br4  truncate '>
                {data &&
                  data &&
                  (data.isLiveCriclyticsAvailable && data.statusMessageHindi
                    ? data.statusMessageHindi
                    : data.toss)}
              </div>
            ) : (
              <div className='ph2 pv1 f8 fw5 white tc w-60-l bg-white-20 br4  truncate '>
                {data &&
                  data &&
                  (data.isLiveCriclyticsAvailable && data.statusMessage ? data.statusMessage : data.toss)}
              </div>
            )}
          </div>
        </div>
      )}  */}

      <div className='hidden lg:block md:block'>
        <div className='flex  items-center justify-between  mx-4 mt-2 '>
          <div> <div className='flex items-center'>
            {/* <ImageWithFallback height={18} width={18} loading='lazy' className='w1 dn-l' onClick={() => router.push(`/fantasy-research-center`)} alt='' src={backIcon} /> */}
            <div className='black f6 fw7 pl3 ttu text-xl font-bold uppercase'>{getLangText(lang, words, 'fantasy_research_center')}</div>
          </div></div>
         <div
            className='flex cursor-pointer'
            onClick={() => {
              lang === 'HIN'
                ? (props.setlanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG'))
                : (props.setlanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'));
            }}>
            <ImageWithFallback height={18} width={18} loading='lazy' className='h-10 w-10 ' src={language_icon} />
          </div>



        </div>
      </div>
      <div className='flex items-center justify-center mx-2 mt-3'>
        <div className=' w-full' onClick={() => setplayvideo(true)}>
          <ImageWithFallback height={148} width={148} loading='lazy' className='w-full cursor-pointer object-contain object-top ' src={FRCVideoIcon} alt="fantasy-research-center" />
        </div>
      </div>
      {/* desktop */}
      
      {/* mobile */}
      <div className=' cursor-pointer  mx-4  lg:flex md:flex items-center justify-between  '>
        <Link
          {...getFrcSeriesTabUrl(props, 'fantasy-stats/players')}
          passHref
          legacyBehavior>
          <div
            className='relative  mt-3  lg:w-6/12 md:w-6/12   flex items-start shadow-2xl bg-gray-4  rounded-lg'
            onClick={() => {
              props.setUrlData({}), fantasyTrack('FantasyStatsPlayer', '');
            }}

          >
            <div className='absolute   ml-3   text-white'>
              <div className='text-white text-base mt-3  font-semibold capitalize'> {getLangText(lang, words, 'FS')} </div>
              <div className='flex h-1 w-10 bg-blue-8 mt-2 rounded-lg' />
              <div className='text-white text-xs mt-3 '>{getLangText(lang, words, 'osst')} </div>
            </div>
            <ImageWithFallback height={18} width={18} loading='lazy' className='h-90 w-full' src={FantasyStatsNew} />
          </div>
        </Link>



        {data && data && !data.isLiveCriclyticsAvailable ? (
          <Link {...getFrcSeriesTabUrl(props, 'create-team')} passHref legacyBehavior>
            <div
              className='relative cursor-pointer  mt-3  lg:w-6/12 md:w-6/12 lg:ml-3 md:ml-3 flex items-start shadow-2xl bg-gray-4  rounded-lg '
              onClick={() => (props.setUrlData({}), fantasyTrack('FantasyBYT', 'NewTeam'))}

            >
              <div className='absolute   ml-3   white'>
                <div className='text-white text-base mt-3  font-semibold capitalize'> {getLangText(lang, words, 'BYT')}  </div>
                <div className='flex h-1 w-10 bg-blue-8 mt-2 rounded-lg' />
                <div className=' text-xs mt-3  text-white'>{getLangText(lang, words, 'BYT_Description')} </div>
              </div>
              <ImageWithFallback height={108} width={108} loading='lazy' className='h-90 w-full' src={BYTNew} />
            </div>
          </Link>
        ) : (
          <div
            className='relative cursor-pointer overflow-hidden mt-3 lg:mt-0 md:mt-0 lg:w-6/12 md:w-6/12  flex items-center rounded opacity-60 '
            style={{

              // background: 'linear-gradient(230deg, rgba(144,144,88,.5), rgba(0,0,0,0) 100%)',
              // borderLeftWidth: 'thick'
            }}>
            <div
              className='relative   flex items-start shadow-2xl bg-gray-4  mt-2'
              onClick={() => (props.setUrlData({}), fantasyTrack('FantasyBYT', 'NewTeam'))}

            >
              <div className='absolute   ml-3   white'>
                <div className='text-white text-base mt-3  font-semibold capitalize'> {getLangText(lang, words, 'BYT')}  </div>
                <div className='flex h-1 w-10 bg-blue-8 mt-2 rounded-lg' />
                <div className='text-white text-xs mt-3 '>{getLangText(lang, words, 'BYT_Description')} </div>
              </div>
              <ImageWithFallback height={108} width={108} loading='lazy' className='h-90 w-full' src={BYTNew} />
            </div>
          </div>
        )}
      </div>
      {console.log('algo11 teams = ', algo11?.getAlgo11)}
      
      {algo11 && algo11?.getAlgo11.saveTeams ? (
        data && data && !data.isLiveCriclyticsAvailable ? (
          <Link {...getFrcSeriesTabUrl(props, 'my-teams')} passHref legacyBehavior>
            <div
              className='p-2 mx-4 rounded mt-3 bg-gray  cursor-pointer flex items-center justify-between  '
              onClick={() => {fantasyTrack('FantasyMyTeams', '');setLoad(true)}}>
              <div className='flex items-center'>

                <div className='text-white flex capitalize font-semibold text-base'>{getLangText(lang, words, 'Saved_Teams')}</div>
              </div>
              <div className='flex items-center '>
                  <div className='flex items-center  justify-center'>
                    {' '}
                    <div className='text-white  text-2xl'> {">"} </div>
                  </div>
               
              </div>
            </div>
          </Link>
        ) : (
          <Link {...getFrcSeriesTabUrl(props, 'live-teams')} passHref legacyBehavior>
            <div
              className='p-2 bg-red  mt-2 bg-white-10 flex items-center justify-between mh2 mh4-l mh4-m br2 ba b--frc-yellow cursor-pointer'
              onClick={() => fantasyTrack('FantasyMyTeams', '')}>
              <div className='flex items-center'>
                <ImageWithFallback height={18} width={18} loading='lazy' className='w15 h15' src={saveTeam} />
                <div className='f7 fw6 pl2 ttu'>{getLangText(lang, words, 'Saved_Teams')}</div>
              </div>
              <div className='flex items-center '>
                {algo11 && algo11.getAlgo11.saveTeamCount ? (
                  <div className='flex items-center justify-center w1 h1 br-100 bg-frc-yellow'>
                    {' '}
                    <div className='f8 black fw6'>{algo11 && algo11.getAlgo11.saveTeamCount}</div>
                  </div>
                ) : (
                  <></>
                )}

                <div className='white cursor-pointer z-999 outline-0 mt1'>
                  <svg width='25' viewBox='0 0 24 24'>
                    <path fill='#EEBA04' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                    <path fill='none' d='M0 0h24v24H0z'></path>
                  </svg>
                </div>
              </div>
            </div>
          </Link>
        )
      ) : (
        ''
      )}



      
      <BuildYourTeam  comp={<Link {...getFrcSeriesTabUrl(props, 'fantasyTeam')} passHref legacyBehavior>
        <div onClick={() => setLoad(true)}
          className='flex cursor-pointer  justify-center rounded border-2  mt-2 text-green border-green text-xs px-2 py-1 items-center font-bold '>
          <div className='capitalize f6 fw7'>{getLangText(lang, words, 'view all teams')}</div>
        </div>
      </Link>} algoteam={algoteam} matchID={FantasymatchID} algo11={algo11} lang={lang} getLangText={getLangText} words={words} />

      <div className='mt-3'>
        <FeaturedArticlesFRC title='FANTASY PREVIEWS' language={props.language} /></div>

      <div className='h-20'></div>
      {/* <div className=''>
        {articleData && articleData.getArticlesByIdAndType.length !== 0 ? (
          <div className='ma2 shadow-4 bg-white-10'>
            <div className='flex items-center justify-between ph2 bb b--white-20'>
              <span className='f7 fw6 white'>{getLangText(lang, words, 'FR')} </span>
              <div className='h2-3 flex items-center'></div>
            </div>

            <div className='flex-ns w-50-l w-50-m w-100 white'>
              <div className='flex-ns w-100 white mt3-ns'>
                <Link {...getNewsUrl(articleData.getArticlesByIdAndType[0])} passHref>
                  <a>
                    <ArticleFRC article={articleData.getArticlesByIdAndType[0]} isdark={true} />
                  </a>
                </Link>
              </div>
            </div>
          </div>
        ) : null}
      </div> */}

      {languagemodel && (
        <LanguageModel
          setlanguagemodel={setlanguagemodel}
          language={props.language ? props.language : 'english'}
          setlanguage={props.setlanguage}
          languagemodel={languagemodel}
        />
      )}

      {playvideomodel && <PlayVideoModel setplayvideo={setplayvideo} playvideomodel={playvideomodel} />}
    </div>
  );
}
