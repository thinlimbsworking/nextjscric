import React, { useEffect,useRef } from 'react'
import CleverTap from 'clevertap-react';
import language_icon from '../../public/svgs/language_icon.svg'
import close from '../../public/pngs/closeIcon.png'
import ImageWithFallback from '../commom/Image';
export default function LanguageModel(props) {

    const ref = useRef()
    //   useEffect(() => {
    //     props.callScroll(true);
    //     return () => {
    //       props.callScroll(false);
    //     };
    //   }, []);

    useEffect(() => {
        const checkIfClickedOutside = e => {
          // If the menu is open and the clicked target is not within the menu,
          // then close the menu
          if (props.languagemodel &&ref &&  ref.current && !ref.current.contains(e.target)) {
            props.setlanguagemodel(false)
          }
        }
        document.addEventListener("mousedown", checkIfClickedOutside)
        return () => {
          // Cleanup the event listener
          document.removeEventListener("mousedown", checkIfClickedOutside)
        }
      }, [props.languagemodel])


    return (
        <div
        // flex justify-center items-center fixed absolute--fill z-9999 bg-black-70 overflow-y-scroll
            className='flex justify-center items-center fixed absolute--fill z-9999 bg-basebg/70 overflow-y-scroll'  style={{ backdropFilter: 'blur(4px)' }}
           >

            <div className=' bg-gray br3 shadow-4  pa2 ba b--black relative  min-vh-20 pt2 mh2  w-100 w-50-l' ref={ref}>

              <ImageWithFallback height={18} width={18} loading='lazy' className='w-4 h-4 right-2 absolute top-[-20px]' src={close} onClick={()=>props.setlanguagemodel(false)}/>

                <div className="f5 fw6 white pa2">CHOOSE LANGUAGE</div>
                <div className="flex items-center justify-between">
                    <div className="w-50">
                        <div className={` br2 mh2 tc pv1 ph2 cursor-pointer ${props.language==='ENG'?'bg-frc-yellow black':'bg-white-20 white'}`} onClick={()=>{props.setlanguage('ENG');localStorage.setItem('FRClanguage', 'ENG'); props.setlanguagemodel(false);
                        CleverTap.initialize('LanguageChange', {
                            Language: "English",
                            Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
                          });
                    }}>
                            <div className=" fw6 pv1 f5" >ENGLISH</div>
                            <div className={` fw4  pv1 f8 ${props.language==='ENG'?'black':'white'}`}>English</div>
                        </div>
                    </div>
                    <div className="w-50">
                        <div className={` br2 mh2 tc pv1 ph2 cursor-pointer ${props.language==='HIN'?'bg-frc-yellow black':'bg-white-20 white'}`}  onClick={()=>{props.setlanguage('HIN');localStorage.setItem('FRClanguage', 'HIN'); props.setlanguagemodel(false);
                      CleverTap.initialize('LanguageChange', {
                        Language: "Hindi",
                        Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
                      });
                    }}>
                            <div className=" fw6 pv1 f5">हिंदी</div>
                            <div className={` fw4 pv1 f8`}>Hindi</div>
                        </div>
                    </div>
                </div>
                <div className="pt2 ph2">
                <div className=" white-70 f8 f6-l f6-m  fw3" >
                 
                <span className="" style={{ verticalAlign: 'middle',display: 'inline-block'}}>{props.language==='HIN'?'भाषा बदलने के लिए, पेज के ऊपरी दाएं कोने पर स्थित':'To change language, tap on the'}</span>
                <span className="ph1"  style={{ verticalAlign: 'middle'}}> 
                <svg width='20' height='20' viewBox='0 0 20 20'>
                        <g transform="translate(-2 -2)">
                            <g transform="translate(2 2)">
                                <rect class="a" width="20" height="20" />
                                <path fill="white" d="M4.325,10.99,2.5,7.851H3.548V2.5h9.434V3.981H4.969v3.87H6.017ZM15.2,9.8H13.5l3.152-7.3h1.263l3.123,7.3H19.309l-.65-1.6H15.83Zm1.171-2.963h1.756l-.882-2.3ZM6.891,14.307h.886v2.406a1.423,1.423,0,0,1-.673.152,1.438,1.438,0,0,1-.736-.217,1.6,1.6,0,0,0-.336-.224,1.973,1.973,0,0,0,.6-1.485,1.894,1.894,0,0,0-1.952-2.051,2.016,2.016,0,0,0-1.756.976l-.055.092.952,1.064.1-.183a.792.792,0,0,1,.73-.494c.377,0,.613.277.613.722a.754.754,0,0,1-.781.863H4.011v1.452H4.69c.572,0,.861.258.861.767a.777.777,0,0,1-.8.863A1.36,1.36,0,0,1,3.6,18.228l-.1-.18L2.5,19.186l.06.092a2.586,2.586,0,0,0,2.184,1.194,2.183,2.183,0,0,0,2.211-2.293,1.384,1.384,0,0,0,.829.023V21.04H9.223V14.307H10.6V21.04h1.438V14.307h1.1V12.941H6.891Zm10.63.742H18.57v4.333H13.823V21.04H19.99V15.049h1.05l
                                        -1.827-3.527Z" transform="translate(-1.986 -1.986)" />
                            </g>
                        </g>
                    </svg>
                    </span>
                    <span >{props.language==='HIN'?'आइकन पर टैप करें !':'icon on the top right corner of the page.'}</span>
                    <br></br>
                    <span >{props.language==='HIN'?'भाषा परिवर्तन केवल फ़ैंटसी सेंटर के लिए है':'Language conversion is only for Fantasy Centre.'}</span>

                    {/* <span className="pl1 dn db-l db-m ">corner of the page</span> */}

                   
                </div>

               {/* <div className=" white-70 f8 flex items-center  dn-l dn-m  fw3"> corner of the page</div> */}
               </div>

            </div>


        </div>

    )
}
