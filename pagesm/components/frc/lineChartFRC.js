import React, { useEffect, useState } from 'react'
import * as d3 from 'd3'
// [
//   { a: 1, b: 19, isDta: false },
//   { a: 2, b: 0, isDta: false },
//   { a: 3, b: 65, isDta: true },
//   { a: 4, b: 92, isDta: true },
//   { a: 5, b: 43, isDta: false },
// ]
export default function LineChartFRC({
  data,
  strokeClr,
  height = 150,
  width = 200,
  margin = 10,
}) {
  // const margin = {top: 30, right: 20, bottom: 30, left: 80},

  //   width = screenWidth - margin.left - margin.right,
  //   height = screenHeight  - margin.top - margin.bottom;

  // const h = height - 2 * margin,
  //   w = width - 2 * margin;
  //number formatter

  // height= height - 2 * margin,
  // width = width - 2 * margin;

  const line = d3
    .line()
    //.curve(d3.curveNatural)
    .x((d, i) => x(i))
    .y((d) => y(d.points))

  const x = d3
    .scaleLinear()
    .domain([0, data.length - 1]) //domain: [min,max] of a[0,120][1,5]
    .range([margin, width])

  // x(2)
  //y scale
  // const maxVal = Math.ceil(d3.max(data, d => d.points) / 10) * 10;
  const maxVal = Math.ceil(Math.max(...data.map((o) => o.points)) / 10) * 10
  const y = d3
    .scaleLinear()
    .domain([0, maxVal]) // domain [0,max] of b (start from 0)
    .range([height, margin])

  //line generator: each point is [x(d.a), y(d.b)] where d is a row in data
  // and x, y are scales (e.g. x(10) returns pixel value of 10 scaled by x)
  const points = d3
    .line()
    .x((d, i) => x(i))
    .y((d) => y(d.points)) //curve line

  //   const yAxis = d3.axisLeft(y).ticks(3);
  //   console.log(yAxis());
  // console.log(d3.ticks(3).tickValues([d3.range]));

  //const xAxis = d3.axisBottom(x).ticks(data.length);
  // const yAxis = d3.axisLeft(yScale).ticks(height / 50, yFormat);
  return (
    <svg height={height + 50} width={width + 60}>
      <g transform="translate(25,25)">
        <rect
          height={height + 10}
          width={width + 20}
          fill={'#1F2634'}
          style={{ borderRadius: 10, overflow: 'hidden' }}
          transform="translate(3,-9)"
        />
        <g transform="translate(-7,1)">
          {y.ticks(5).map((tick) => (
            <g key={tick} transform="translate(-5,0)">
              <line
                x1="10"
                y1={y(tick)}
                x2={width + 30}
                y2={y(tick)}
                //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                stroke={'#2B323F'}
                strokeWidth="0.6"
              />
              <text
                x={0}
                y={y(tick)}
                fontSize="10"
                fill="#8C98B0"
                fillOpacity={tick == 0 ? 0 : 1}
                fontFamily="Manrope-Medium"
                textAnchor="middle"
              >
                {tick}
              </text>
            </g>
          ))}
          {x.ticks(5).map((tick) => (
            <g key={tick} transform="translate(10,0)">
              <line
                x1={x(tick)}
                y1="0"
                x2={x(tick)}
                y2={height}
                //   strokeDasharray={tick === 0 ? [0, 0] : [1, 1]}
                stroke={'#2B323F'}
                strokeWidth="0.6"
              />
            </g>
          ))}
        </g>
        <g>
          <path
            transform="translate(4,0)"
            id="line"
            d={line(data)}
            fill="none"
            stroke={strokeClr}
            strokeWidth="2"
          />
          {data.map((val, i) => (
            <g key={i}>
              <circle
                cx={x(i)}
                cy={y(val.points)}
                fill={'#38D926'}
                r={val.dta_flag ? 5 : 3}
              />
              {/* <text
                originY={y(val.points)}
                originX={x(i)}
                textAnchor={'middle'}
                fontFamily="Montserrat-Regular"
                fontSize={6}
                alignmentBaseline={'middle'}
                fontWeight="700"
                x={x(i)}
                y={y(val.points)}
                fill={val.dta_flag? "#000":"none"}>
                DTA
              </text> */}
            </g>
          ))}
        </g>
      </g>
      {/* <div
        style={{
          color: '#8C98B0',
          fontFamily: 'Manrope-Medium',
          fontSize: 10,
          position: 'absolute',
          left: 5,
          top: height,
          transform: [{rotate: '270deg'}],
        }}>
        Runs
      </div> */}
      ​
      {/* <Text style={tw(`font-mnm`)}
        x={height + 4}
        y={-15}
        fontSize="10"
        fill="#8C98B0"
        // fontFamily="Manrope-Medium"
        transform="rotate(90)"
        textAnchor="start">
        Runs
      </Text> */}
    </svg>
  )
}
