import React, { useState, useEffect } from 'react';
import Countdown from 'react-countdown-now';
import Link from 'next/link';

import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_USER_FRC_TEAM_LIVE_POINTS } from '../../api/queries';
import ImageWithFallback from '../commom/Image';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Loading from '../loading';
import CleverTap from 'clevertap-react';
const backIcon = '/svgs/back_dark_black.svg';
const stadium = '/pngs/frc_stadium.png'
// import Swiper from 'react-id-swiper';
import fanfight from '../../public/svgs/fanfight_logo.svg'
import { getFrcSeriesTabUrl } from '../../api/services';
// import { navigate } from '@reach/router'
const playerPlaceholder = '/pngs/fallbackprojection.png';
import LiveCompletedMyTeamDetails from './liveCompletedMyTeamDetails.js'
import LiveCompletedStatshubTeamDetails from './liveCompletedStatshubTeamDetails'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';

const Empty = '/svgs/Empty.svg';


export default function LiveCompletedMyTeams(props) {


  let FantasymatchID = props.matchID;
  const teamDetails = (props.matchBasicData && [...props.matchBasicData.getFRCHomePage.completedmatches, ...props.matchBasicData.getFRCHomePage.livematches, ...props.matchBasicData.getFRCHomePage.upcomingmatches].filter((x) => x.matchID === FantasymatchID)).length >0 ?
  [...props.matchBasicData.getFRCHomePage.completedmatches, ...props.matchBasicData.getFRCHomePage.livematches, ...props.matchBasicData.getFRCHomePage.upcomingmatches].filter((x) => x.matchID === FantasymatchID):
  [{...props.data.miniScoreCard.data[0],
    awayTeamShortName: props.data.miniScoreCard.data[0].awayTeamName,
    homeTeamShortName: props.data.miniScoreCard.data[0].homeTeamName,
    MatchName: props.data.miniScoreCard.data[0].matchName}]
  const [click, setclick] = useState('MyTeamList');
  const [index, setIndex] = useState()
  const [toggle, setToggle] = useState(false);
  const [windows, setLocalStorage] = useState({});
  const [newToken, setToken] = useState('')
  const [myTeam,setMyTeam] = useState({})
// console.log("clickclick",click)
  // useEffect(() => {
  //   if (global.window) {
  //     setLocalStorage({ ...global.window });
  //     setToken(global.window.localStorage.getItem('tokenData'))
  //   }
  // }, [global.window]);

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem('tokenData'))
    }
  }, [global.window]);

  const { loading, error, data } = useQuery(GET_USER_FRC_TEAM_LIVE_POINTS, {
    variables: { matchID: FantasymatchID, token: newToken }

  });


  if (loading) {
    <div></div>
  }


  return (
    data && data.getUserFrcTeamLivePoints && !error? <div className='mw75-l center bg-black min-vh-100'>
      <div className="bg-gold z-1 ph2 h2-4  flex items-center">
        <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="w1" onClick={() => window.history.back()} alt='' src={backIcon} />
        <div className="black f6 fw7 pl3 ttu tc">My Saved teams</div>
      </div>



      <div className="bg-white-10 shadow-4 ba b--frc-yellow ma2 br2 pa3 white">
        {teamDetails && teamDetails[0] && teamDetails[0].matchScore.map((x, i) => (
          <div key={i} className='flex items-center   justify-evenly pv2'>
            <span className='flex w-30'>
              <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder}
                alt={x.teamShortName}
                src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                className='h1 w15 shadow-4'
              />
              <span className='mh2 f6 fw6'> {x.teamShortName} </span>
            </span>
            <div className='w-70 flex items-center tracked-tight '>
              {x.teamScore &&
                x.teamScore.map((score, i) => (
                  <span key={i} className={`flex justify-center items-start f6 `}>

                    <span className={``}>
                      {score.runsScored}{' '}
                      {score.wickets && score.wickets !== '10' ? `/ ${score.wickets}` : null}

                    </span>

                    {((teamDetails[0].matchType === 'Test' &&
                      teamDetails[0].matchStatus !== 'completed' &&
                      score.inning == teamDetails[0].currentinningsNo) ||
                      teamDetails[0].matchType !== 'Test') && (
                        <div className='ml2'>{score.overs ? `(${score.overs})` : null}</div>
                      )}
                    {score.declared ? <span className='darkRed fw6 oswald f6 ml1'>d</span> : ''}
                    {score.folowOn ? <span className='darkRed fw6 f6 oswald ml1'> f/o</span> : ''}
                    {x.teamScore.length > 1 && i === 0 && <div className='mh2 fw5'>&</div>}
                  </span>
                ))}
            </div>

          </div>
        ))}
        <div className='pv1 white bg-white-20 br4 ph2 '>
          <span className='f8 fw5 tl truncate'>{teamDetails && teamDetails[0] && (teamDetails[0].isLiveCriclyticsAvailable && teamDetails[0].statusMessage ? teamDetails[0].statusMessage : teamDetails[0].toss)}</span>
        </div>
      </div>
      {click === "MyTeamList" && <div>
      <div className="f5 fw5 white-50 ph2 mb2 mt3 ttu">SAVED TEAMS</div>
      
        {data && data.getUserFrcTeamLivePoints.map((team, i) => {
          const params = {
            slidesPerView: 4,
            navigation: {
              nextEl: `#nbutton${i}`,
              prevEl: `#pbutton${i}`
            },
            spaceBetween: 10,
            breakpoints: { 400: { slidesPerView: 8, spaceBetween: 20 } }
          };
          // onClick={() => navigate(`/fantasy-research-center/${props.matchID}/${props.seriesSlug}/create-team`)}
          return (

            <div key={i} className="ma2" >
              {console.log("team.leagueTyp",team.leagueType)}
              <div className="ph2 bg-white-10 ba b--white-20 br2 relative white pb3" onClick={() => {setMyTeam(team), setclick("openTeamDetails") }}>
                <div>
                  <div className="pb2 pt2 flex justify-between items-center">
                    <div className="f8 fw6 ttu">{team.teamName}</div>
                    <div className="flex items-center">

                      <div className="f10 ">FANTASY POINTS</div>
                      <div className="gold oswald pl2">{team.teamtotalpoints}</div>
                    </div>
                  </div>
                  <div className="pb1">
                    <div className="flex justify-between mh1">
                      <div {...params}>
                        {team.team.map((x, y) => <div key={y} className="">

                          <div className=" relative flex justify-center">
                            <div className="overflow-hidden br-100 b--white-20 ba w2-6 h2-6 bg-mid-gray relative">
                              <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder} className="" src={`https://images.cricket.com/players/${x.playerId}_headshot_safari.png`} />
                            </div>
                            <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder} className="absolute w1 ml1 h1 left-0 bottom-0 mb1 br-100 ba b--black  bg-gray ml3-l" src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`} />
                            {(x.captain == "1" || x.vice_captain == "1") && <div className="absolute w1 h1 mr1 right-0 bottom-0 mb1 tc black f10 flex items-center mr3-l fw6 justify-center br-100 ba b--white  bg-gold">{x.captain === "1" ? "C" : "VC"}</div>}
                          </div>
                          <div className="oswald f8 pv1 tc ph1">{x.playerName}</div>
                          <div className="f10 gray tc pt1 ttc">{x.player_role}</div>
                        </div>)}
                      </div>
                    </div>
                    <div className='absolute absolute--fill flex justify-between items-center'>
                      <div id={`pbutton${i}`} className='white cursor-pointer z-999 outline-0 nl2'>
                        <svg width='30' focusable='false' viewBox='0 0 24 24'>
                          <path fill='#EEBA04' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                          <path fill='none' d='M0 0h24v24H0z'></path>
                        </svg>
                      </div>
                      <div id={`nbutton${i}`} className='white cursor-pointer z-999 outline-0 nr2'>
                        <svg width='30' viewBox='0 0 24 24'>
                          <path fill='#EEBA04' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                          <path fill='none' d='M0 0h24v24H0z'></path>
                        </svg>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              {data && data.getUserFrcTeamLivePoints.length !== i + 1 && <div className="bb mh2 b--white-20 pb4"></div>}
            </div>
          )
        }
        )}
      </div>}
      
      {click == "openTeamDetails" &&
        <LiveCompletedMyTeamDetails  setMyTeam={setMyTeam} myTeam={myTeam} islive={teamDetails[0].isLiveCriclyticsAvailable} />
      }
      {/* {click == "openStatshubTeamDetails" &&
        <LiveCompletedStatshubTeamDetails  setMyTeam={setMyTeam} myTeam={myTeam} islive={teamDetails[0].isLiveCriclyticsAvailable} />
      } */}


    </div> : <div className='bg-black w-100 min-vh-100 mw75-l center'>
      <div className="bg-gold z-1 ph2 h2-4  flex items-center mb4">
        <ImageWithFallback height={18} width={18} loading='lazy' className="w1" onClick={() => window.history.back()} alt='' src={backIcon} />
        <div className="black f6 fw7 pl3 ttu tc">My Teams</div>
      </div>
      <ImageWithFallback height={18} width={18} loading='lazy'
        className='w45-m h45-m w4 h4 w5-l h5-l'
        style={{ margin: 'auto', display: 'block' }}
        src={'/svgs/groundImageWicket.png'}
        alt='loading...'
      />
      <div className='f5 fw5 f3-l tc pt2 white pt4-l'>No Saved Teams</div>
    </div>
  );
}
