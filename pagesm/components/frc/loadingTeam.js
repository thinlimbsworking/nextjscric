import React, { useState, useEffect } from 'react';
import { GET_FRC_TEAM } from '../../api/queries';
import { useQuery, useMutation, useLazyQuery } from '@apollo/react-hooks';
import { getLangText } from '../../api/services';
import { words } from '../../constant/language';

export default function LoadingTeam({
  setCurrentFlag,
  playerID,
  chooseLogic,
  leagueType,
  FantasymatchID,
  setBuildTeam,
  ...props
}) {
  const lang = props.language;
  const [rotate, setRoate] = useState('circle');
  const [windows, setLocalStorage] = useState({});
  const [newToken, setToken] = useState('');

  useEffect(() => {
    if (global.window) {
      setLocalStorage({ ...global.window });
      setToken(global.window.localStorage.getItem('tokenData'));
    }
  }, [global.window]);

  const { loading, error, data } = useQuery(GET_FRC_TEAM, {
    variables: {
      matchID: FantasymatchID,
      playerIds: playerID,
      leagueType: leagueType,
      selectCriteria: chooseLogic,
      token: newToken
    },
    onCompleted: (data) => {
      setBuildTeam(data.getFrcTeam);
      setTimeout(() => {
        setRoate('added');
      }, 1000);
      setTimeout(() => {
        setCurrentFlag('choose Team');
      }, 3000);
    }
  });

  return (
    <div className='h-screen flex justify-center items-center'>
      {rotate === 'circle' && (
        <div className=''>
          <img src={'/gif/frc_loader.gif'} />

          <div className='text-xs font-bold  text-center fw3 lh-copy tc white mt3'>
            {getLangText(lang, words, 'Calculating and')} <br /> {getLangText(lang, words, 'adding players')}{' '}
          </div>
        </div>
      )}
      {rotate === 'added' && (
        <div className='tc'>
          <img src={`/gif/start-green.png`} />
          <div className='f3 fw3 text-center lh-copy tc white mt5'>
            {getLangText(lang, words, 'Players_Added')} <br /> {getLangText(lang, words, 'Successfully')}
          </div>
        </div>
      )}
    </div>
  );
}
