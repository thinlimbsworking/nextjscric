
  import React ,{useState}from 'react';
  const flagPlaceHolder = '/svgs/images/flag_empty.svg';
//   import Swiper from 'react-id-swiper';
  import { Swiper, SwiperSlide } from 'swiper/react';
  import ImageWithFallback from '../commom/Image';
  const playerPlaceholder = '/pngs/fallbackprojection.png';
  const ProgressPagination = ({algoteam,getLangText,lang,words,...props}) => {
  
  

  
    return (
        <div className="border-t-lg">
             <style jsx>{`
       
        ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 2px;
            border-radius: 2px;
            height:4px;
            background-color:#2B323F
          }
          ::-webkit-scrollbar-thumb {
            border-radius: 2px;
            background-color: #6A91A9;
            
          }
      `}</style>
      
          
     
            <div className=' flex justify-between mx-1 overflow-x-scroll bg-gray-4 py-3'  >
        
  {algoteam &&
                    algoteam.length > 0 &&
                    algoteam.map((x, y) => (
                      <div key={y} className='  ' >
                        <div className=' relative  flex justify-center border-r border-black  p-3  '>
                          <div className='overflow-hidden rounded-full  bg-gray w-20 h-20  relative'>
                            <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {playerPlaceholder}
                              className='object-top object-cover'
                              src={`https://images.cricket.com/players/${x.playerId}_headshot_safari.png`}
                              // onError={(e) => (e.target.src = playerPlaceholder)}
                            />
                          </div>
                          <div className='absolute w1 ml1 h1 left-2 bottom-2 mb1 br-100 ba b--black  bg-gray ml3-l overflow-hidden'>
                            <ImageWithFallback height={18} width={18} loading='lazy'
                                fallbackSrc = {flagPlaceHolder}
                              className='w-100 h-100 object-cover'
                              src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                              // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                            />
                          </div>

                          {(x.captain == '1' || x.vice_captain == '1') && (
                            <div className='absolute w1 h1 mr1 right-1 bottom-2 mb1 tc black f10 flex items-center mr3-l fw6 justify-center br-100 ba b--white  bg-gold'>
                              {x.captain === '1' ? 'C' : 'VC'}
                            </div>
                          )}
                        </div>
                        <div className='text-xs text-white text-center'>{lang === 'HIN' ? x.playerNameHindi : x.playerName}</div>
                        <div className='text-xs text-gray-2 text-center'>{getLangText(lang, words, x.player_role)}</div>
                      </div>
                    ))}

    

                  </div>
                  </div>
                  )
                }
    
  

  export default ProgressPagination;
    