import React,{useState} from 'react'
import ImageWithFallback from '../commom/Image'
// const frc_allround='/svgs/frc_allround.svg'
import  white_allRounder from '../../public/pngs/allRounder-white-solid.png'
import  white_ball from '../../public/pngs/ball-white-solid.png'
import  white_bat from '../../public/pngs/bat-white-solid.png'
import  white_wicket from '../../public/pngs/wicketKeeper-white-solid.png'
import black_bat from '../../public/svgs/black_bat.svg'
import black_allRounder from '../../public/svgs/black_allRounder.svg'
import black_ball from '../../public/svgs/black_ball.svg'
import black_keeper from '../../public/svgs/black_keeper.svg'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import {getLangText} from '../../api/services'
import { words } from '../../constant/language'
export default function PlayerModelV2(props) {
  const [type,setType]=useState(props.SelectionType==="compare"?props.Player2 && props.Player2.playerSkill:props.RecentPlayer && props.RecentPlayer.playerSkill)

  let filterRole={"Bowler":"bowler",
    "Batsman":"batsman" }
  // console.log("Props model", props)
   return (
      <div
      className='flex justify-center items-center fixed absolute--fill z-9999 bg-black-70 overflow-y-scroll'
      style={{ backdropFilter: 'blur(4px)' }}>

      <div className=' rounded  bg-gray  border relative w-11/12 md:w-5/12 lg:w-5/12 '>
        <div className=' py-2  flex  items-center justify-between '>
          <div className='flex  justify-start   px-2 items-center  text-base uppercase  font-semibold'>{props.lang==="HIN"?'वर्तमान चयन':'current selection'}</div>
          <div className='flex mr-3 justify-end items-center white fw6 ph3 pv2 cursor-pointer' onClick={()=>{props.closeNewModelV2()}}
            >X</div>
        </div>
        {/* <div className="w-100">
                <div className="flex  items-center mv2    justify-between  bg-white-10 ba b--yellow ph2 pv1 br2 ph3-l  mv2 mh2 mh3-l mh3-m">
                  <div className="flex items-center">
                    <div className="w2 h2 w2-6-l h2-6-l w2-6-m h2-6-m br-100  ba b--white-20 bg-gray overflow-hidden ">
                      <img className="w-100 h-100 object-cover object-top" src={`https://images.cricket.com/players/${props.SelectionType==="compare"?props.Player2 && props.Player2.playerID:props.RecentPlayer && props.RecentPlayer.playerID}_headshot_safari.png`} onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}  alt=""></img>
                    </div>
                    {props.lang==="HIN"?<div className="ttu f5 f4-l f4-m oswald fw6 ttu ph2">{ props.SelectionType==="compare"?props.Player2 && props.Player2.playerNameHindi : props.RecentPlayer && props.RecentPlayer.playerNameHindi}</div>:
                    <div className="ttu f5 f4-l f4-m oswald fw6 ttu ph2">{ props.SelectionType==="compare"?props.Player2 && props.Player2.name : props.RecentPlayer && props.RecentPlayer.name}</div>
                    }
                  </div>
                  <div className="flex items-center">
                  <div className="ttu f5 f4-l f4-m fw6 oswald white-40   ttu ph2">{props.SelectionType==="compare"?props.Player2 && props.Player2.teamShortName :props.RecentPlayer && props.RecentPlayer.teamShortName}</div>
                    <div className="w15 w2-l h2-l  w2-m h2-m h15 br-100 overflow-hidden ">
                      <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${props.SelectionType==="compare"?props.Player2 && props.Player2.teamID :props.RecentPlayer && props.RecentPlayer.teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                    </div>   
                  </div>
                </div>
              </div> */}
              {/* <div className="mh2 mt4 mb2 bb b--white-20"></div> */}
              {/* <div className=" f5 fw6 white ph2  ttu">3{props.lang==="HIN"?'खिलाड़ी बदलें':'CHANGE PLAYER'}</div> */}

              <div className={`flex p-2 items-center justify-between bg-gray-8 mx-3 rounded-full  `}>
          {<div className={` flex justify-center white w-2/12  ${type === 'KEEPER' ? 'bg-gray-8 p-1 rounded-3xl border border-green ' : ''}`}>
            <div className="cursor-pointer flex flex-column items-center " onClick={() => { setType('KEEPER') }} >
            <div className="text-xs " > <div className={` 
             flex p-2 text-white items-center justify-center    `}>
            {getLangText(props.lang, words, "WK")}
            </div>
            </div>
            {/* <div className={`fw6 f7 tc gray pt1`} >{getLangText(props.lang, words, "WK")}</div> */}
          </div></div>}
          <div className={`${props.SelectionType === 'matchups' ? '' : ' '}  flex justify-center white w-3/12   ${type === 'BATSMAN' ? 'bg-gray-8 p-2 rounded-3xl border border-green ' : ''} `}>
            <div className="cursor-pointer flex flex-column items-center " onClick={() => { setType('BATSMAN') }}>
            <div className="text-xs">
              <div className={`  flex text-white items-center text-xs justify-center }   `}>
              {getLangText(props.lang, words, "BATTERS")}
              </div>
              </div>

            {/* <div className="fw6 f7 tc gray pt1" >{getLangText(props.lang, words, "BATTERS")}</div> */}
          </div></div>

         
          
          <div className={`${props.SelectionType === 'matchups' ? '' : ' '}  flex justify-center white w-3/12  ${type === 'BOWLER' ? 'bg-gray-8 p-2 rounded-3xl border border-green ' : ''} `}>
            <div className="cursor-pointer flex flex-column items-center" onClick={() => { setType('BOWLER') }}>
            <div className="text-xs"> 
            <div className={`  flex text-white items-center justify-center   `}>
            {getLangText(props.lang, words, "BOWLERS")}
              </div></div>
            {/* <div className="fw6 f7 tc gray pt1" >{getLangText(props.lang, words, "BOWLERS")}</div> */}
          </div></div>
          <div className={` flex justify-center white w-4/12 p-1   ${type === 'ALL_ROUNDER' ? 'bg-gray-8 p-2 rounded-3xl border border-green ' : ''}`}><div className="cursor-pointer flex flex-column items-center" onClick={() => { setType('ALL_ROUNDER') }}>
            <div className="text-xs"> 
            <div className={` flex text-white items-center justify-center    `}>
            {getLangText(props.lang, words, "ARS")}
              </div>
              </div>
          
          </div></div>
        </div>
              {/* <div className="flex pv2 items-center justify-between w-100">
             {<div className="w-25 flex justify-center white"><div className="cursor-pointer flex flex-column items-center " onClick={()=>{setType('KEEPER')}} >
                <div className="fw4 f8 gray" > <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='KEEPER'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='KEEPER'?black_keeper:white_wicket}/></div></div>
                  <div className={`fw6 f7 gray  tc pt1`} >{getLangText(props.lang,words,"WK")}</div>
                </div></div>}
                <div className={`${props.SelectionType==='matchups'?'w-50':'w-25 ' }  flex justify-center white`}><div className="cursor-pointer flex flex-column items-center " onClick={()=>{setType('BATSMAN')}}>
                <div className="fw4 f8 gray"> <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='BATSMAN'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='BATSMAN'?black_bat:white_bat}/></div></div>
                  <div className="fw6 f7 tc gray pt1" >{getLangText(props.lang,words,"BATTERS")}</div>
                </div></div>
                {<div className="w-25 flex justify-center white"><div className="cursor-pointer flex flex-column items-center" onClick={()=>{setType('ALL_ROUNDER')}}>
                 <div className="fw4 f8 gray"> <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='ALL_ROUNDER'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='ALL_ROUNDER'?black_allRounder:white_allRounder}/></div></div>
                  <div className="fw6 f7 gray tc pt1"  >{getLangText(props.lang,words,"ARS")}</div>
                </div></div>}
                <div className={`${props.SelectionType==='matchups'?'w-50':'w-25 ' }  flex justify-center white`}><div className="cursor-pointer flex flex-column items-center" onClick={()=>{setType('BOWLER')}}>
                <div className="fw4 f8 gray"> <div className={` w3 h2-3  w35-l h2-5-l w35-m h2-5-m br2 flex items-center justify-center ${type==='BOWLER'?'bg-frc-yellow':'bg-white-30'}   `}><img className="h1 h13-l  h13-m" src={type==='BOWLER'?black_ball:white_ball}/></div></div>
                  <div className="fw6 f7 gray tc pt1" >{getLangText(props.lang,words,"BOWLERS")}</div>
                </div></div>
                </div> */}
                <div className=' overflow-y-scroll overflow-hidden hidescroll ' style={{maxHeight:"57vh"}}>

               
                {(props.playersList.filter(player=>props.Player2 && props.Player2.playerID ?(player.playerID!==props.RecentPlayer.playerID  &&  player.playerSkill===type &&  player.playerID!==props.Player2.playerID):player.playerID!==props.RecentPlayer.playerID  &&  player.playerSkill===type)).map((player,i)=>
                  <div className="w-100" key={i} onClick={()=>props.choosePlayer(player,props.SelectionType)}>
               
               
               <div className="flex  items-center m-2  justify-between border-b border-gray-4  py-2 ">
                <div className="flex items-center ">
                  <div className="flex items-center justify-center w-14 h-14 rounded-full bg-gray-8  overflow-hidden ">
                    <ImageWithFallback height={18} width={18} loading='lazy'
                    fallbackSrc = '/pngs/fallbackprojection.png' className="w-14 h-14  object-cover object-top  rounded-full" 
                    src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`} 
                    alt="" />
                  </div>
                  <div className="text-xs font-semibold ml-2">{props.lang === "HIN" ? player.playerNameHindi : player.name}</div>
                </div>
                <div className="flex items-center">
                  <div className="text-white text-xs font-semibold mr-2">{player.teamShortName}</div>
                  <div className="w15 h15 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                    <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder} className="w-10 h-6 object-top object-cover" src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`} alt=""/>
                  </div>

                </div>
              </div>
{/*                
                <div className="flex  items-center mv2 cursor-pointer ba b--gray justify-between  bg-white-10  ph2  ph3-l pv1 br2  mv2 mh2 mh3-l mh3-m">
                  <div className="flex items-center">
                    <div className="w2 h2 w2-6-l h2-6-l w2-6-m h2-6-m br-100  ba b--white-20 bg-gray overflow-hidden ">
                      <img className="w-100 h-100 object-cover object-top" src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`} alt="" onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}></img>
                    </div>
                    <div className="ttu f5 f4-l f4-m oswald fw6 ttu ph2">{props.lang==="ENG"?player.name:player.playerNameHindi}</div>
                  </div>
                  <div className="flex items-center">
                  <div className="ttu f5 f4-l f4-m fw6 oswald white-40 ttu ph2">{player.teamShortName}</div>
                    <div className="w15 h15 w2-l h2-l w2-m h2-m br-100 overflow-hidden ">
                      <img className="w-100 h-100 object-cover" src={`https://images.cricket.com/teams/${player.teamID}_flag_safari.png`} alt="" onError={(evt) => (evt.target.src = flagPlaceHolder)}></img>
                    </div>
                   
                  </div>
                </div> */}
              </div>
                )}
                </div>
      </div>
    </div>

   )
}
