import React,{useState} from 'react'
import { words } from '../../constant/language'
export default function RecentPlayerBowlingType({RecentPlayer,bowlingType,language}) {
    let lang=language
   
   const getLangText = (lang,keys,key) => {
    //   console.log("keys[key]",keys[key])
      let [english,hindi] = keys[key]
      switch(lang){
        case 'HIN':
          return hindi
        case 'ENG':
          return english
        default:
          return english
      }
        }
    let newArray= (bowlingType === "S/R"?[...RecentPlayer.bowlingDetails.bowlingType.filter(ar=>ar.bowlingSR!==null)]:bowlingType === "AVERAGE"?[...RecentPlayer.bowlingDetails.bowlingType.filter(ar=>ar.bowlingAvg!==null)]:[...RecentPlayer.bowlingDetails.bowlingType.filter(ar=>ar.Dismissals!==null)]).sort(function (a, b) {
        return bowlingType === "S/R" ?(a.bowlingSR - b.bowlingSR):bowlingType === "AVERAGE"?(a.bowlingAvg - b.bowlingAvg):(a.Dismissals - b.Dismissals)
    })
    
    let min = newArray[0]
    // console.log("bowlingType min",bowlingType,"min", min)
    let max = newArray[newArray.length - 1]
    // console.log("bowlingType max",bowlingType,"max", max)


    const typeCheck=(currentObj)=>{
        if(bowlingType === "S/R"){
           if((currentObj.bowlingSR===min.bowlingSR)||(currentObj.bowlingSR===max.bowlingSR)){
               return 'o-100'
           }else{
               return 'o-50'
           }
        }

        else if(bowlingType === "AVERAGE"){
            if((currentObj.bowlingAvg===min.bowlingAvg)||(currentObj.bowlingAvg===max.bowlingAvg)){
                return 'o-100'
            }else{
                return 'o-50'
            }
         }
         else{
            if((currentObj.Dismissals===min.Dismissals)||(currentObj.Dismissals===max.Dismissals)){
                return 'o-100'
            }else{
                return 'o-50'
            }
         }
    }
   return (
     <div className="">
      
           {RecentPlayer && RecentPlayer.bowlingDetails &&
                           RecentPlayer.bowlingDetails.bowlingType && RecentPlayer.bowlingDetails.bowlingType.length > 0 &&  (bowlingType === "S/R"?[...RecentPlayer.bowlingDetails.bowlingType.filter(ar=>ar.bowlingSR!==null)]:bowlingType === "AVERAGE"?[...RecentPlayer.bowlingDetails.bowlingType.filter(ar=>ar.bowlingAvg!==null)]:[...RecentPlayer.bowlingDetails.bowlingType.filter(ar=>ar.Dismissals!==null)]).map((compare, i) =>
                              <div className="ph3 pv2" key={i}>
                                 <div className="flex  pv2 items-center justify-between mt-4">
                                    <div className=" text-xs font-semibold"><span className="gray text-xs">v </span>
                                    <span className="white ttu pl1">{getLangText(lang,words,compare.types.toUpperCase())}</span></div>
                                    <div className="text-xs font-semibold">
                                      {bowlingType === "S/R" ? Number(compare.bowlingSR) : bowlingType === "AVERAGE" ? Number(compare.bowlingAvg) : Number(compare.Dismissals)}</div>
                                 </div>
                                 {/* {console.log("dismissal", Number(compare.Dismissals) / Number(RecentPlayer.bowlingDetails.maxDismissals))}
                                 {console.log("maxx", (bowlingType === "S/R" ? Number(compare.bowlingSR) : bowlingType === "AVERAGE" ? Number(compare.bowlingAvg) : Number(compare.Dismissals) / bowlingType === "S/R" ? Number(RecentPlayer.bowlingDetails.maxSR) : bowlingType === "AVERAGE" ? Number(RecentPlayer.bowlingDetails.maxAvg) : Number(RecentPlayer.bowlingDetails.maxDismissals)) * 100)} */}
                                 <div
                                    className={`  br2  my-2 rounded relative items-center w-100 z-0 `}
                                    style={{ height: 6, backgroundColor: "#333a46" }}>
                                    <div
                                       className={`absolute h-1 br2  rounded br--left left-0 bg-green  
                                       ${typeCheck(compare)}

                                       
                                       `}
                                       style={{
                                          width: `${bowlingType === "DISMISSALS" ? Number(compare.Dismissals) / Number(RecentPlayer.bowlingDetails.maxDismissals) * 100 : bowlingType === "AVERAGE" ? Number(compare.bowlingAvg) / Number(RecentPlayer.bowlingDetails.maxAvg) * 100 : Number(compare.bowlingSR) / Number(RecentPlayer.bowlingDetails.maxSR) * 100}%`,


                                          zIndex: -1
                                       }}></div>
                                 </div>
                              </div>
                           )}
     </div>

   )
}
