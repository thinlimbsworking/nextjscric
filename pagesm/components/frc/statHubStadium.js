import React, { useState, useEffect, useRef } from 'react'
import { STAT_HUB_STADIUM, MATCH_SUMMARY } from '../../api/queries';
import { format } from 'date-fns';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
const location = '/pngsV2/location.png';
const rightArrow = '/svgs/yellowRightArrow.svg';
const downArrow = '/svgs/downArrowWhite.svg';
const upArrow = '/svgs/upArrowWhite.svg';
const yellowDownArrow = '/svgs/yellowDownArrow.svg';
import { words } from '../../constant/language'
import DataNotFound from '../commom/datanotfound'
import Loading from '../Loading';
import axios from 'axios';
import Heading from '../commom/heading';
import ImageWithFallback from '../commom/Image';
const playerPlaceholder = '/pngs/fallbackprojection.png';
export default function StatHubStadium(props) {
   let lang=props.language?props.language:'ENG'
   const { loading, error, data: StadiumStatHub } = useQuery(STAT_HUB_STADIUM, {
      variables: { matchID: props.matchID }
   });

   const [getSummary, { error: scoreError, loading: scoreLoading, data: summaryData }] = useLazyQuery(
      MATCH_SUMMARY
   );


   const [viewAll, setViewAll] = useState(false)
   const [playerRecord, setPlayerRecord] = useState("BATTING")
   const [showDropDown, setShowDropDown] = useState(false);
   const [battingType, setBattingType] = useState("battingAverage")
   const [bowlingType, setBowlingType] = useState("totalWickets")
   const battingTab = ['totalRuns', 'battingAverage', 'battingStrikeRate', 'fifties', 'hundreds', 'totalFours', 'totalSixes'];
   const bowlingTab = ['totalWickets', 'bowlingStrikeRate', 'economy', 'threeWickets', 'fiveWickets']

   const [RecentMatchIndex, setRecentMatchIndex] = useState({})
   const [dropDown, setDropDown] = useState(false)
   const [type, setType] = useState("battingStrikeRate")
   // const [bowlingType, setBowlingType] = useState("bowlingStrikeRate")
   const RecentMatchTab = ['HEAD TO HEAD', 'DC', 'MI'];
   const playerRecordTab = ['BATTING', 'BOWLING'];


   const getLangText = (lang,keys,key) => {
           let [english,hindi] = keys[key]
          switch(lang){
            case 'HIN':
              return hindi
            case 'ENG':
              return english
            default:
              return english
          }
        }
   const getSummaryData = async (matchID, i) => {

      if (!RecentMatchIndex[i]) {
         await getSummary({ variables: { matchID: matchID, status: "completed" } })
      }
      setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });


   }
   const wrapperRef = useRef(null);
   useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
         if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setShowDropDown(false);
         }
      }

      document.addEventListener('mousedown', handleClickOutside);
      return () => {
         document.removeEventListener('mousedown', handleClickOutside);
      };
   }, [wrapperRef]);

   const isEnglish = (lang) => {
      if (lang == 'ENG') return true;
      else false;
    };
   
   if (loading) return <Loading />;
   else
      return (
         <>
         {StadiumStatHub && StadiumStatHub.stadiumHub ?<div className="pb-2 mx-3 ">

            <div className="  py-2 text-center bg-basebg  rounded   ">
               <div className='flex flex-col items-center justify-center '>    
                         <ImageWithFallback height={18} width={18} loading='lazy'
                         className="h-14 py-2 w-10" src={location}/>
               <div className="text-white px-3 font-semibold  ">{StadiumStatHub && StadiumStatHub.stadiumHub && isEnglish(lang) ? StadiumStatHub.stadiumHub.venueName : StadiumStatHub.stadiumHub.venueNameInHindi}</div>
               
               </div>
 
               {/* <div className="bb bw3 bg-white-20 br2 b--white-20 w-15 center mv2"></div> */}

               <div className='bg-gray my-2 rounded-md'>
               {StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.characteristics && <div className="lh-copy">
            
                  <div className="text-left text-base ml-2 py-2 uppercase">   <Heading  heading={getLangText(lang,words,'CHARACTERISTICS')} /></div>
               
                  <div className="flex w-100 flex-wrap items-center pl2 pl4-l  pl4-m justify-start-l f7  lh-copy ">
                     {lang==='HIN'?
                     StadiumStatHub && StadiumStatHub.stadiumHub && Object.keys(StadiumStatHub.stadiumHub.characteristicsInHindi).map((character, i) =>
                     <div className={`  ${character==="weather" && StadiumStatHub.stadiumHub.characteristicsInHindi["weather"]==="अनुपलब्ध"?'':' black  bg-green p-1 m-1 capatilize rounded-xl '} `} key={i}>
                       
                        {(character==="weather" && StadiumStatHub.stadiumHub.characteristicsInHindi["weather"]==="अनुपलब्ध")?'':character==="weather" && 
                        StadiumStatHub.stadiumHub.characteristicsInHindi["weather"]!=="अनुपलब्ध"?"मौसम : " +
                         StadiumStatHub.stadiumHub.characteristicsInHindi[character]:StadiumStatHub.stadiumHub.characteristicsInHindi[character]}</div>
                  )
                     :StadiumStatHub && StadiumStatHub.stadiumHub && Object.keys(StadiumStatHub.stadiumHub.characteristics).map((character, i) =>
                        <div className={`  ${character==="weather" && StadiumStatHub.stadiumHub.characteristics["weather"]==="N/A"?'':' black bg-green p-1 capatilize rounded-xl m-1 text-xs text-gray '} `} key={i}>


                           {(character==="weather" && StadiumStatHub.stadiumHub.characteristics["weather"]==="N/A")?'':
                           character==="weather" && StadiumStatHub.stadiumHub.characteristics["weather"]!=="N/A"?
                           "weather : " + StadiumStatHub.stadiumHub.characteristics[character]:StadiumStatHub.stadiumHub.characteristics[character]}</div>
                     )}

                  </div>
               </div>}
               <div className="flex mt-2 lh-copy white f8  fw6    py-2">
                  <div className="w-3/12 border-r ">
                     <div className="text-gray-2 font-semibold  mt-2 text-xs  ">{lang==='HIN'?'मैचों की':'Number of'  }<br/> {lang==='HIN'?'संख्या':'matches'}</div>
                     <div className=" text-white font-bold text-xl  ">{StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.groundStats && StadiumStatHub.stadiumHub.groundStats.totalMatches || 0}</div>
                  </div>
                  <div className="bl b--white-20"></div>
                  <div className="w-3/12 border-r ">
                     <div className="text-gray-2 font-semibold  mt-2 text-xs ">{lang==='HIN'?'पहली पारी':'Avg 1st' }<br/> {lang==='HIN'?'का औसत':'Innings Total'}</div>
                     <div className="  text-white font-bold text-xl   ">{StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.groundStats && StadiumStatHub.stadiumHub.groundStats.avgFirstInningScore}</div>
                  </div>
                  <div className="bl b--white-20"></div>
                  <div className="w-3/12 border-r">
                     <div className="text-gray-2 font-semibold  mt-2 text-xs ">{lang==='HIN'?'पहले बैटिंग करते':'Won by team' }<br/>{lang==='HIN'?'हुए जीत':'batting 1st'}</div>
                     {/* <div className=" text-white font-bold text-xl tracked ">18/38</div> */}
                     <div className="  text-white font-bold text-xl  tracked  ">{StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.groundStats && StadiumStatHub.stadiumHub.groundStats.firstBattingWinPercent}%</div>
                  </div>
                  <div className="bl  b--white-20"></div>
                  <div className="w-3/12 ">
                     <div className="text-gray-2 font-semibold  mt-2 text-xs ">{lang==='HIN'?'विकेट विभाजन:':'Wickets split'} <br/> {lang==='HIN'?'पेस बनाम स्पिन ':'Pace-Spin'}</div>
                     <div className="  text-white font-bold text-xl  tracked  ">{StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.groundStats && StadiumStatHub.stadiumHub.groundStats.pacerWickets}-{StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.groundStats && StadiumStatHub.stadiumHub.groundStats.spinnerWickets}</div>
                     {StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.groundStats && ( StadiumStatHub.stadiumHub.groundStats.paceWicketPercent!=="" &&  StadiumStatHub.stadiumHub.groundStats.spinWicketPercent!=="") && <div className="pt2 fw5  white f5-l tracked  f6 oswald">{ Math.round(StadiumStatHub.stadiumHub.groundStats.paceWicketPercent)}%-{ Math.round( StadiumStatHub.stadiumHub.groundStats.spinWicketPercent)}%</div>}
                     {/* <div className="f6-l f7 fw5">49%</div> */}
                  </div>
                  
               </div>
               {StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.characteristics && <div className="text-left text-xs p-2">
            {lang==='HIN'?<span>{`*  पिछले 3 वर्षों के ${ StadiumStatHub.stadiumHub.compType}  डेटा`}</span>:
               <span>{`* ${ StadiumStatHub.stadiumHub.compType} data from last 3 years`}</span>}
               </div>}
               </div>
            </div>
           
            {StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.recentMatches && <div>
            <div className="mv2">
               <div className="h-solid-divider-light"></div>
            </div>

            <div className='bg-gray py-2 rounded-md' >


<div className='bg-gray   rounded-xl'>
   
                  { true&& <div>
                   
                    
                     <div className='text-base ml-2  font-bold text-white uppercase  py-2'> 
                     
                     <Heading heading={getLangText(lang,words,'RECENT MATCHES')} /></div>
                   

                   
                     <>
                        <div className='   '>
                           { StadiumStatHub&&StadiumStatHub.stadiumHub&&StadiumStatHub.stadiumHub.recentMatches&&StadiumStatHub.stadiumHub.recentMatches.length > 0 ? StadiumStatHub.stadiumHub.recentMatches.map((team, i) => (
                              <>

                                 <div
                                    className={i == 0 ? 'flex  cursor-pointer    items-center py-2  justify-between bg-gray-8 mt-3 mx-3 ' :
                                       'flex    mt2 cursor-pointer items-center py-2  justify-between bg-gray-8 mx-3 mt-3'}
                                    key={i}
                                    onClick={() => {
                                       getSummaryData(team.matchID, i)
                                       // setRecentMatchIndex({ [`${i}`]: !RecentMatchIndex[`${i}`] });
                                    }}
                                   >
                                    <div className='flex items-center w-10/12 '>
                                       <div className=' w-1/4 tc flex justify-center items-center'>
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                             fallbackSrc = {flagPlaceHolder}
                                             className='h-8 w-12 object-cover'
                                             src={team.winnerTeam === team.awayTeam ? `https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png` :
                                                team.winnerTeam === team.homeTeam ? `https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png` : flagPlaceHolder
                                             }
                                             alt=''
                                             // onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                                          />
                                       </div>
                                       <div className='  w-70'>
                                          <div className='flex items-center pv1'>
                                             <div className={`ba  pv1 border text-xs p-1 ${team.winnerTeam === team.homeTeam ? ' border-green text-gray-2 ' : 'b--gray white'}  br2   ph2  f6 fw5`}>{team.homeTeam}</div>
                                             <div className='ph2 moon-gray mx-1'>vs</div>
                                             <div className={`ba  br2 border text-xs p-1 ${team.winnerTeam === team.awayTeam ? 'border-green text-gray-2 ' : 'b--gray white'} pv1    ph2 f6 fw5`}>{team.awayTeam}</div>
                                          </div>
                                          <div className='text-xs text-white tracked mt-2'>{isEnglish(lang) ? team.matchResult : team.matchResultHindi}</div>
                                          <div className='flex items-center  f7 moon-gray lh-title '>
                                             <div className="tracked text-gray-2  text-xs">{format(+team.matchDate, 'MMM dd, yyyy. ')}</div>
                                             <ImageWithFallback height={18} width={18} loading='lazy'
                                                className='w-5 px-1 ' src={location}/>
                                             <div className='text-gray-2 font-semiBold text-xs'>{isEnglish(lang) ? team.venue : team.venueHindi}</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div
                                       className=' flex items-center justify-end mr-2'
                                    >
                                                  {RecentMatchIndex[i] ?   <svg width='30' focusable='false' viewBox='0 0 24 24'>
//             <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
//             <path fill='none' d='M0 0h24v24H0z'></path>
//           </svg>: <svg width='30' viewBox='0 0 24 24'>
//             <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
//             <path fill='none' d='M0 0h24v24H0z'></path>
//           </svg>}
                                    
                                    </div>
                                 </div>
                                 {RecentMatchIndex[i] && summaryData && summaryData.matchSummary && (
                                    <div className='white bg-gray-8 mx-3 pb-3'>
                                        <div className='bg-gray mx-3 pb-3 mb-3'>
                                       {/* <div className='f6 silver ph3 pv2 fw7'>MATCH SUMMARY</div> */}
                                       <div className='flex'>
                                     
                                       <div className=' w-6/12 flex flex-col p-2 items-center  '>
                                          <div className='text-gray-2 font-semibold mb-1 text-xs'>TOP BATTER</div>
                                          <div className='relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center '>
                                             <div className='overflow-hidden w-full h-full rounded-full '>
                                             <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder}
                                                   className=' object-top object-contain w-24   '
                                                   style={{ objectPosition: '0 0%' }}
                                                   src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBatsman.playerID}_headshot.png`}
                                                   // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                   alt=''/>
                                                
                                             </div>


                                          </div>



                                         
                                          <div className='w-full -mt-10 bg-gray-4 text-center  rounded-md px-2'>
                                             <div className="text-base  font-semibold mt-10  truncate">{lang==="HIN"?summaryData.matchSummary.bestBatsman.playerNameHindi
                                             :summaryData.matchSummary.bestBatsman.playerName}</div>
                                             {summaryData.matchSummary.bestBatsman.battingStatsList && team.matchType !== 'Test' ?
                                                <div>
                                                   <div className='flex  flex-col items-center py-1'>
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[0] &&
                                                       <div className=' font-semibold'><span className='f5 fw5 mr1'>
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[0].runs}
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                      </span>
                                                         <span className='f6'>( {summaryData.matchSummary.bestBatsman.battingStatsList[0].balls} )</span></div>}
                                                         <div className='flex items-center justify-between mt-2'>
                                                      <div className='mr-1'>
                                                         <span className='text-xs text-white '>4s </span>
                                                         <span className='text-xs text-white'>{` `}{summaryData.matchSummary.bestBatsman.battingStatsList[0].fours}</span>
                                                      </div>
                                                      <div className='ml-1 border-l'>
                                                         <span className='fw4 f6 gray ml-2'>6s</span>
                                                         <span className='fw6 f6 white'>{` `}{summaryData.matchSummary.bestBatsman.battingStatsList[0].sixes}</span>
                                                      </div>
                                                      </div>
                                                   </div>
                                                   {false && summaryData.matchSummary.bestBatsman.bowlingStatsList && <div className='flex  items-center pv1'>
                                                      <div className='ba bg-silver br2 pv1 fw5 b--black  ph2'><span className='f5 fw5 mr1 nowrap'>
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].wickets}/
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].runsConceded}
                                                      </span>
                                                         <span className='f7 '>({summaryData.matchSummary.bestBatsman.bowlingStatsList[0].overs})</span></div>
                                                      <div className='pl2 '>
                                                         <span className='fw4 f6 gray'>Eco</span>
                                                         <span className='fw6 pl1 f7 white'>{ summaryData.matchSummary.bestBatsman.bowlingStatsList&&summaryData.matchSummary.bestBatsman.bowlingStatsList[0].economyRate}</span>
                                                      </div>
                                                   </div>}
                                                </div> : <div>
                                                   <div className='flex  items-center pv1'>
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[0] && <div className='ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap ph2'>
                                                         <span className='f5 fw5 mr1'>
                                                            {summaryData.matchSummary.bestBatsman.battingStatsList[0].runs}
                                                            {summaryData.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                         </span>
                                                         <span className='f6'>( {summaryData.matchSummary.bestBatsman.battingStatsList[0].balls} )</span>
                                                      </div>}
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[1] && <div className='ba bg-frc-yellow black nowrap fw5 br2 pv1 b--black  ml2 ph2'>
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[1] && <span className='f5 fw5'>{summaryData.matchSummary.bestBatsman.battingStatsList[1].runs}</span>}
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[1] && (
                                                            <span className='f7'>{`(${summaryData.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                                                         )}
                                                      </div>}


                                                   </div>
                                                   {summaryData.matchSummary.bestBatsman.bowlingStatsList && <div className='flex  items-center pv1'>
                                                      <div className='ba bg-silver br2 pv1 fw5 b--black  ph2'> <span className='f5 fw5'>
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].wickets}/
                                                         {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].runsConceded}
                                                      </span>
                                                         <span className='f7 pl2'>
                                                            ({summaryData.matchSummary.bestBatsman.bowlingStatsList[0].overs})
                                                         </span></div>
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[1] && <div className='ph1'>&</div>}
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[1] && (
                                                         <div className='ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap  ph2'>
                                                            <span className='f5 fw5'>
                                                               {summaryData.matchSummary.bestBatsman.bowlingStatsList[1].wickets}/
                                                               {summaryData.matchSummary.bestBatsman.bowlingStatsList[1].runsConceded}
                                                            </span>
                                                            <span className='f7 pl2'>
                                                               ({summaryData.matchSummary.bestBatsman.bowlingStatsList[1].overs})
                                                            </span>
                                                         </div>
                                                      )}
                                                   </div>}
                                                </div>}
                                          </div>
                                       </div>
                                        
                                       {summaryData.matchSummary.bestBowler.bowlingStatsList && team.matchType !== 'Test' ? 
                                       <div className='w-6/12 flex flex-col p-2 items-center'>
                                          <div className='text-gray-2 font-semibold mb-1 text-xs'>TOP BOWLER</div>
                                          <div className='relative bg-gray-8 w-24 h-24 rounded-full flex items-center justify-center '>
                                             <div className='overflow-hidden w-full h-full rounded-full '>
                                             <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder}
                                                   className=' object-top object-contain w-24  rounded-full  '
                                                  
                                                   src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                                // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                   alt=''/>
                                                
                                             </div>


                                          </div>

                                          <div className='w-full -mt-10 bg-gray-4 text-center  rounded-md px-2'>
                                             <div className="text-base  font-semibold mt-10 truncate">{lang==='HIN'?summaryData.matchSummary.bestBowler.playerNameHindi:summaryData.matchSummary.bestBowler.playerName}</div>
                                             <div className='flex flex-col  items-center justify-between '>
                                                {summaryData.matchSummary.bestBowler.battingStatsList &&
                                                   (summaryData.matchSummary.bestBowler.battingStatsList[0].runs !== 0 ||
                                                      summaryData.matchSummary.bestBowler.battingStatsList[0].balls !== 0) && <div className='ba bg-frc-yellow br2 pv1 b--black  black ph2'> <span className='f5 fw5 mr1 nowrap'>
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[0].runs}
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                      </span>
                                                      <span className='f7 '>({summaryData.matchSummary.bestBowler.battingStatsList[0].balls})</span></div>}
                                                {summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className="f6 fw6 ph2">&</div>}
                                                {summaryData.matchSummary.bestBowler.bowlingStatsList &&
                                                   <div className=''><span className=' font-bold text-white'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      <span className='f7 '>({summaryData.matchSummary.bestBowler.bowlingStatsList[0].overs})</span></div>}
                                             </div>
                                             <div className='text-center mt-2  pb-2'>
                                                         <span className='text-white text-xs'>Eco :</span>
                                                         <span className='text-white text-base font-semibold ml-1'>{ summaryData.matchSummary.bestBatsman.bowlingStatsList&&summaryData.matchSummary.bestBatsman.bowlingStatsList[0].economyRate}</span>
                                                      </div>
                                          </div>
                                       </div> : 
                                       <div className='flex w-100 items-center justify-center pv2 '>
                                          <div className='w-40 flex items-end justify-end'>
                                             {' '}
                                             <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder}
                                                className='h4 h45-l'
                                                src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                                // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                alt=''
                                             />
                                          </div>
                                          <div className='w-60  flex lh-copy flex-column  items-start justify-start '>
                                             <div className="f6 fw5">{lang==='HIN'?summaryData.matchSummary.bestBowler.playerNameHindi:summaryData.matchSummary.bestBowler.playerName}</div>
                                             <div className="flex   ">
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler && summaryData.matchSummary.bestBowler.battingStatsList && summaryData.matchSummary.bestBowler.battingStatsList.length > 0 && <div className='ba bg-frc-yellow br2 pv1 tc b--black black ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].runs}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                   </span>
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.battingStatsList[0].balls})</span></div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList && <div className='ba mv2  bg-silver br2 tc pv1 b--black  ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      {/* <span>&nbsp;</span> */}
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.bowlingStatsList[0].overs})</span></div>}
                                                </div>
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className='pa1 f7'>&</div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[1] && <div className='mv3 ph1  f7'>&</div>}
                                                </div>
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler && summaryData.matchSummary.bestBowler.battingStatsList && summaryData.matchSummary.bestBowler.battingStatsList.length > 0 && summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className='ba bg-frc-yellow  tc br2 pv1 b--black black  ph2'>{summaryData.matchSummary.bestBowler.battingStatsList[1] && (
                                                      <span className='f6 fw5'>
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[1].runs}
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[1].isNotOut === true ? '*' : ''}
                                                      </span>
                                                   )}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[1] && (
                                                         <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.battingStatsList[1].balls})</span>
                                                      )}</div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[1] && <div className='ba mv2 bg-silver br2 pv1 b--black  tc ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[1].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[1].runsConceded}
                                                   </span>
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.bowlingStatsList[1].overs})</span></div>}
                                                </div>
                                             </div>
                                          </div>
                                       </div>}
                                       </div>
                        
                                       {summaryData.matchSummary.inningOrder
                                          .slice(0, team.matchType !== 'Test' ? 2 : summaryData.matchSummary.inningOrder.length).map((inning, i) => <div key={i}>
                                             <div className='h-solid-divider-light mh3 mv2 '></div>
                                             <div>
                                                <div className='bg-gray-4 m-2 rounded-md p-2'>
                                                   <div className='flex ph3 items-center justify-between'>
                                                      <div className='flex items-center'>
                                                         <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {flagPlaceHolder}
                                                            className=' w-10 h-10 rounded-full  object-cover objetc-top'
                                                            src={`https://images.cricket.com/teams/${summaryData.matchSummary[inning].teamID}_flag_safari.png`}
                                                            alt=''
                                                            // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                                         />
                                                         <div className='pl2 f6  fw5'>{summaryData.matchSummary[inning].teamShortName.toUpperCase()}</div>
                                                      </div>
                                                      <div className='flex items-center  justify-center'>
                                                         <div className='text-white font-semibold  oswald f3 fw6'>
                                                            {summaryData.matchSummary[inning].runs[i <= 1 ? 0 : 1]}/
                                                            {summaryData.matchSummary[inning].wickets[i <= 1 ? 0 : 1]}

                                                         </div>
                                                         <div className='text-white font-semibold  ml-1'>({summaryData.matchSummary[inning].overs[i <= 1 ? 0 : 1]})</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div className='ph3 pv2 flex'>
                                                   {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman &&
                                                      summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                         .battingStatsList && <div className='w-5/12 ph2'>
                                                             <div className='text-gray-2 uppercase text-xs font-semibold'>Batter</div>
                                                         <div className='flex items-center moon-gray justify-between pv1'>
                                                           
                                                           {lang==='HIN'? <div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .playerNameHindi
                                                            }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .playerName
                                                            }</div>}
                                                            <div className='text-white font-semibold'> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .battingStatsList[0].runs
                                                            }
                                                               {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                                  .battingStatsList[0].isNotOut === true
                                                                  ? '*'
                                                                  : ''}</div>
                                                         </div>
                                                         {team.matchType !== 'Test' && summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].runnerBatsman &&
                                                            summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].runnerBatsman
                                                               .battingStatsList && <div className='flex moon-gray items-center justify-between pv1 '>
                                                               {lang==='HIN'?<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                                  summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.playerNameHindi
                                                               }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                                  summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.playerName
                                                               }</div>}
                                                               <div className='text-white font-semibold'> {
                                                                  summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.battingStatsList[0].runs
                                                               }
                                                                  {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                     .runnerBatsman.battingStatsList[0].isNotOut === true
                                                                     ? '*'
                                                                     : ''}</div>
                                                            </div>}
                                                      </div>}
                                                   <div className='bl  b--white-20'></div>
                                                  


                                                   <div className='w-5/12 ph2 center'>
                                                   <div className='text-gray-2 uppercase text-xs font-semibold'>Bowler</div>
                                                      {summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                         i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                      ].topBowler &&
                                                         summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                            i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                         ].topBowler.bowlingStatsList && <div className='flex moon-gray items-center justify-between pv1 '>
                                                            {lang==='HIN'?<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.playerNameHindi
                                                            }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.playerName
                                                            }</div>}
                                                            <div className='text-white font-semibold'>{
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.bowlingStatsList[0].wickets
                                                            }/
                                                               {
                                                                  summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                     i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                                  ].topBowler.bowlingStatsList[0].runsConceded
                                                               }</div>
                                                         </div>}
                                                      {team.matchType !== 'Test' && summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                         i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                      ].runnerBowler &&
                                                         summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                            i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                         ].runnerBowler.bowlingStatsList && <div className='flex moon-gray items-center justify-between pv1  '>
                                                            {lang==='HIN'?<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.playerNameHindi
                                                            }</div>:<div className="truncate w-75 text-white font-semibold text-xs text-xs text-xs"> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.playerName
                                                            }</div>}
                                                            <div className='text-white font-semibold'> {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.bowlingStatsList[0].wickets
                                                            }/
                                                               {
                                                                  summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                     i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                                  ].runnerBowler.bowlingStatsList[0].runsConceded
                                                               }</div>
                                                         </div>}
                                                   </div>
                                                </div>
                                             </div></div>)}

                                            


                                   
                                     
                                                </div>
                                    </div>
                                 )}
                                 
                              </>
                           )) : <div className="white f5 fw5 pv2  tc ">No match found</div>}
                           
                        </div>
                     </>
                  </div>}
               </div>
               
            {false&&<div >
                  <div className='f5  fw6 white mb2  ttu mv4-l pl2'>{getLangText(lang,words,'RECENT MATCHES')}</div>
                  <div className='bg-blue-8 w-16 rounded h-1 ml-2 mb-2'></div>

                  <>
                     <div className='   white br2  f7 f6-l ba b--white-20  '>
                         {StadiumStatHub.stadiumHub.recentMatches.map((team, i) => (
                           <>
                              <div
                                 className={i == 0 ? 'flex cursor-pointer   mt3  items-center pv2  justify-between' :
                                    'flex    mt2  cursor-pointer items-center pv2  justify-between'}
                                 key={i}
                                 onClick={() => {
                                    getSummaryData(team.matchID, i)

                                 }}
                               >
                                 <div className='flex items-center w-80 justify-center'>
                                    <div className=' w-30 tc flex justify-center items-center'>
                                       <ImageWithFallback height={18} width={18} loading='lazy'
                                          fallbackSrc = {flagPlaceHolder}
                                          className=' w2-6 h2-6  br-100  object-cover'
                                          src={ team.winnerTeam === team.awayTeam?`https://images.cricket.com/teams/${team.awayTeamID}_flag_safari.png`:
                                          team.winnerTeam === team.homeTeam?`https://images.cricket.com/teams/${team.homeTeamID}_flag_safari.png`:flagPlaceHolder
                                          }
                                          alt=''
                                          // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                       /> 
                                    </div>
                                    <div className='  w-70'>
                                       <div className='flex items-center pv1'>
                                          <div className={`ba  ${team.winnerTeam === team.homeTeam ? 'bg-frc-yellow black' : 'b--gray white'} pv1  br2   ph2  f6 fw5`}>{team.homeTeam}</div>
                                          <div className='ph2 moon-gray'>vs</div>
                                          <div className={`ba  br2 pv1    ${team.winnerTeam === team.awayTeam ? 'bg-frc-yellow black' : 'b--gray white'}  ph2 f6 fw5`}>{team.awayTeam}</div>
                                       </div>
                                       {}
                                       <div className='f7 fw6  lh-copy tracked'>{isEnglish(lang) ? team.matchResult : team.matchResultHindi }</div>
                                       <div className='flex items-center  f7 moon-gray lh-title '>
                                          <div className="tracked "> {format(+team.matchDate, 'dd MMM, yyyy. ')}|</div>
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                                className='w08 ph1 ' src={location}/>
                                          <div>{isEnglish(lang) ? team.venue : team.venueHindi }</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div
                                    className='w-20 tc'
                                 >
                     
                                    <ImageWithFallback height={18} width={18} loading='lazy'
                                           src={RecentMatchIndex[i] ? yellowDownArrow : rightArrow} />
                                 </div>
                              </div>

                              {RecentMatchIndex[i] && summaryData && summaryData.matchSummary && (
                                 <div className='white'>
                                    <div className='h-solid-divider-light mh3 '></div>
                                    <div className='f6 silver ph3 pv2 fw7'>MATCH SUMMARY</div>
                                    {/* <div className='h4 w4 flex br-100 bg-gray  overflow-hidden object-cover object-top items-center   ' >
                      <img className=' '   src={`https://images.cricket.com/players/${4332}_headshot_safari.png`}
                          onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')} />
                    </div> */}
                                    <div className='w-100 flex ph3 pv2 items-center '>
                                       <div className='w-40 flex items-end justify-end'>
                                          <div className=' br-100 bg-dark-gray overflow-hidden' style={{width:"7rem",height:"7rem"}}>
                                             <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder}
                                                className='h-100  w-100 object-cover '
                                                style={{ objectPosition: '0 0%' }}
                                                src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBatsman.playerID}_headshot.png`}
                                                // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                                alt=''/>
                                          </div>
                                       </div>
                                       <div className='w-60 pl3 lh-copy'>
                                          <div className="f5 fw6 ">{lang==='HIN'?summaryData.matchSummary.bestBatsman.playerNameHindi:summaryData.matchSummary.bestBatsman.playerName}</div>
                                          {summaryData.matchSummary.bestBatsman.battingStatsList && team.matchType !== 'Test' ?
                                             <div>
                                                <div className='flex  items-center pv1'>
                                                   {summaryData.matchSummary.bestBatsman.battingStatsList[0] && <div className='ba bg-frc-yellow black fw5 br2 nowrap pv1 b--black  ph2'><span className='f5 fw5 mr1'>
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[0].runs}
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                   </span>
                                                      <span className='f6'>( {summaryData.matchSummary.bestBatsman.battingStatsList[0].balls} )</span></div>}
                                                   <div className='pl2'>
                                                      <span className='fw4 f6 gray'>4s </span>
                                                      <span className='fw6 f7 white'>{` `}{summaryData.matchSummary.bestBatsman.battingStatsList[0].fours}</span>
                                                   </div>
                                                   <div className='pl2'>
                                                      <span className='fw4 f6 gray'>6s</span>
                                                      <span className='fw6 f6 white'>{` `}{summaryData.matchSummary.bestBatsman.battingStatsList[0].sixes}</span>
                                                   </div>
                                                </div>
                                                {summaryData.matchSummary.bestBatsman.bowlingStatsList && <div className='flex  items-center pv1'>
                                                   <div className='ba bg-silver  br2 pv1 fw5 b--black  ph2'><span className='f5 fw5 mr1 nowrap'>
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      <span className='f7 '>({summaryData.matchSummary.bestBatsman.bowlingStatsList[0].overs})</span></div>
                                                   <div className='pl2'>
                                                      <span className='fw4 f6 gray'>Eco</span>
                                                      <span className='fw6 pl1 f7 white'>{summaryData.matchSummary.bestBatsman.bowlingStatsList[0].economyRate}</span>
                                                   </div>
                                                </div>}
                                             </div> : <div>
                                                <div className='flex  items-center pv1'>
                                                   {summaryData.matchSummary.bestBatsman.battingStatsList[0] && <div className='ba bg-frc-yellow black fw5 nowrap br2 pv1 b--black  nowrap ph2'>
                                                      <span className='f5 fw5 mr1'>
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[0].runs}
                                                         {summaryData.matchSummary.bestBatsman.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                      </span>
                                                      <span className='f6'>( {summaryData.matchSummary.bestBatsman.battingStatsList[0].balls} )</span>
                                                   </div>}
                                                   {summaryData.matchSummary.bestBatsman.battingStatsList[1] && <div className='ba bg-frc-yellow black fw5 br2 nowrap pv1 b--black  ml2 ph2'>
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[1] && <span className='f5 fw5'>{data.matchSummary.bestBatsman.battingStatsList[1].runs}</span>}
                                                      {summaryData.matchSummary.bestBatsman.battingStatsList[1] && (
                                                         <span className='f7'>{`(${summaryData.matchSummary.bestBatsman.battingStatsList[1].balls})`}</span>
                                                      )}
                                                   </div>}


                                                </div>
                                                {summaryData.matchSummary.bestBatsman.bowlingStatsList && <div className='flex  items-center pv1'>
                                                   <div className='ba bg-silver br2 pv1 fw5 b--black  ph2'> <span className='f5 fw5'>
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBatsman.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      <span className='f7 pl2'>
                                                         ({summaryData.matchSummary.bestBatsman.bowlingStatsList[0].overs})
                                                         </span></div>
                                                   {summaryData.matchSummary.bestBatsman.bowlingStatsList[1] && <div className='ph1'>&</div>}
                                                   {summaryData.matchSummary.bestBatsman.bowlingStatsList[1] && (
                                                      <div className='ba bg-frc-yellow black fw5 br2 pv1 b--black nowrap   ph2'>
                                                         <span className='f5 fw5'>
                                                            {summaryData.matchSummary.bestBatsman.bowlingStatsList[1].wickets}/
                                                               {summaryData.matchSummary.bestBatsman.bowlingStatsList[1].runsConceded}
                                                         </span>
                                                         <span className='f7 pl2'>
                                                            ({summaryData.matchSummary.bestBatsman.bowlingStatsList[1].overs})
                                                </span>
                                                      </div>
                                                   )}
                                                </div>}
                                             </div>}
                                       </div>
                                    </div>
                                    { summaryData.matchSummary.inningOrder
                                       .slice(0, team.matchType !== 'Test' ? 2 : summaryData.matchSummary.inningOrder.length).map((inning, i) => <div key={i}><div className='h-solid-divider-light mh3 mv2 '></div>
                                          <div>
                                             <div>
                                                <div className='flex ph3 items-center justify-between'>
                                                   <div className='flex items-center'>
                                                      <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {flagPlaceHolder}
                                                         className='  w-10 h-10 rounded-full  object-cover objetc-top'
                                                         src={`https://images.cricket.com/teams/${summaryData.matchSummary[inning].teamID}_flag_safari.png`}
                                                         alt=''
                                                         // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                                      />
                                                      <div className='pl2 f6  fw5'>{summaryData.matchSummary[inning].teamShortName.toUpperCase()}</div>
                                                   </div>
                                                   <div className='flex items-center  justify-center'>
                                                      <div className='yellow-frc  oswald f3 fw6'>
                                                         {summaryData.matchSummary[inning].runs[i <= 1 ? 0 : 1]}/
                                                             {summaryData.matchSummary[inning].wickets[i <= 1 ? 0 : 1]}

                                                      </div>
                                                      <div className='f6 fw5 gray'>({summaryData.matchSummary[inning].overs[i <= 1 ? 0 : 1]})</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div className='ph3 pv2 flex'>
                                                {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman &&
                                                   summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                      .battingStatsList && <div className='w-50 ph2'>
                                                      <div className='flex items-center moon-gray justify-between pv1'>
                                                        {lang==='HIN'? <div className="truncate w-75"> {
                                                            summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                               .playerNameHindi
                                                         }</div>: <div className="truncate w-75"> {
                                                            summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                               .playerName
                                                         }</div>}
                                                         <div> {
                                                            summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                               .battingStatsList[0].runs
                                                         }
                                                            {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].topBatsman
                                                               .battingStatsList[0].isNotOut === true
                                                               ? '*'
                                                               : ''}</div>
                                                      </div>
                                                      {team.matchType !== 'Test' && summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].runnerBatsman &&
                                                         summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2'].runnerBatsman
                                                            .battingStatsList && <div className='flex moon-gray items-center justify-between pv1 '>
                                                           {lang==='HIN'?
                                                           <div className="truncate w-75"> {
                                                            summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                               .runnerBatsman.playerNameHindi
                                                         }</div>
                                                           : <div className="truncate w-75"> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                  .runnerBatsman.playerName
                                                            }</div>}
                                                            <div> {
                                                               summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                  .runnerBatsman.battingStatsList[0].runs
                                                            }
                                                               {summaryData.matchSummary[inning][i <= 1 ? 'batsmanSummary1' : 'batsmanSummary2']
                                                                  .runnerBatsman.battingStatsList[0].isNotOut === true
                                                                  ? '*'
                                                                  : ''}</div>
                                                         </div>}
                                                   </div>}
                                                <div className='bl b--white-20'></div>

                                                <div className='w-50 ph2 center'>
                                                   {summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                      i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                   ].topBowler &&
                                                      summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                         i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                      ].topBowler.bowlingStatsList && <div className='flex moon-gray items-center justify-between pv1 '>
                                                        {lang==='HIN'?
                                                        <div className="truncate w-75"> {
                                                         summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                            i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                         ].topBowler.playerNameHindi
                                                      }</div>
                                                        : <div className="truncate w-75"> {
                                                            summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                               i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                            ].topBowler.playerName
                                                         }</div>}
                                                         <div>{
                                                            summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                               i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                            ].topBowler.bowlingStatsList[0].wickets
                                                         }/
                                                           {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].topBowler.bowlingStatsList[0].runsConceded
                                                            }</div>
                                                      </div>}
                                                   {team.matchType !== 'Test' && summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                      i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                   ].runnerBowler &&
                                                      summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                         i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                      ].runnerBowler.bowlingStatsList && <div className='flex moon-gray items-center justify-between pv1  '>
                                                         {lang==='HIN'?<div className="truncate w-75"> {
                                                            summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                               i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                            ].runnerBowler.playerNameHindi
                                                         }</div>
                                                         :<div className="truncate w-75"> {
                                                            summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                               i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                            ].runnerBowler.playerName
                                                         }</div>}
                                                         <div> {
                                                            summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                               i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                            ].runnerBowler.bowlingStatsList[0].wickets
                                                         }
                                  /
                                  {
                                                               summaryData.matchSummary[inning === 'homeTeamData' ? 'awayTeamData' : 'homeTeamData'][
                                                                  i <= 1 ? 'bowlerSummary1' : 'bowlerSummary2'
                                                               ].runnerBowler.bowlingStatsList[0].runsConceded
                                                            }</div>
                                                      </div>}
                                                </div>
                                             </div>
                                          </div></div>)}


                                    <div className='h-solid-divider-light mh3 mv2 '></div>
                                    {summaryData.matchSummary.bestBowler.bowlingStatsList && team.matchType !== 'Test' ? <div className='flex w-100 items-center justify-center pv2'>
                                       <div className='w-40 flex items-end justify-end'>
                                          {' '}
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder}
                                             className='h4 h45-l'
                                             src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                             // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                             alt=''
                                          />
                                       </div>
                                       <div className='w-60 flex lh-copy flex-column items-start justify-start pl2'>
                                          <div className="f6 fw5">{lang==='HIN'?summaryData.matchSummary.bestBowler.playerNameHindi:summaryData.matchSummary.bestBowler.playerName}</div>
                                          <div className='flex  items-center justify-between '>
                                             {summaryData.matchSummary.bestBowler.battingStatsList &&
                                                (summaryData.matchSummary.bestBowler.battingStatsList[0].runs !== 0 ||
                                                   summaryData.matchSummary.bestBowler.battingStatsList[0].balls !== 0) && <div className='ba bg-frc-yellow br2 pv1 b--black black ph2'> <span className='f5 fw5 mr1 nowrap'>
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].runs}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                   </span>
                                                   <span className='f7 '>({summaryData.matchSummary.bestBowler.battingStatsList[0].balls})</span></div>}
                                             {summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className="f6 fw6 ph2">&</div>}
                                             {summaryData.matchSummary.bestBowler.bowlingStatsList &&
                                                <div className='ba  bg-silver br2 pv1 b--black  ph2'><span className='f5 fw5 mr1 nowrap'>
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[0].wickets}/
                                                       {summaryData.matchSummary.bestBowler.bowlingStatsList[0].runsConceded}
                                                </span>
                                                   <span className='f7 '>({summaryData.matchSummary.bestBowler.bowlingStatsList[0].overs})</span></div>}
                                          </div>
                                       </div>
                                    </div> : <div className='flex w-100 items-center justify-center pv2'>
                                       <div className='w-40 flex items-end justify-end'>
                                          {' '}
                                          <ImageWithFallback height={18} width={18} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder}
                                             className='h4 h45-l'
                                             src={`https://images.cricket.com/players/${summaryData.matchSummary.bestBowler.playerID}_headshot_safari.png`}
                                             // onError={(evt) => (evt.target.src = '/pngs/fallbackprojection.png')}
                                             alt=''
                                          />
                                       </div>
                                       <div className='w-60  flex lh-copy flex-column  items-start justify-start '>
                                             <div className="f6 fw5">{summaryData.matchSummary.bestBowler.playerName}</div>
                                             <div className="flex   ">
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler && summaryData.matchSummary.bestBowler.battingStatsList && summaryData.matchSummary.bestBowler.battingStatsList.length > 0 && <div className='ba bg-frc-yellow br2 pv1 tc b--black black ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].runs}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[0].isNotOut === true ? '*' : ''}
                                                   </span>
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.battingStatsList[0].balls})</span></div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList && <div className='ba mv2  bg-silver br2 tc pv1 b--black  ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[0].runsConceded}
                                                   </span>
                                                      {/* <span>&nbsp;</span> */}
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.bowlingStatsList[0].overs})</span></div>}
                                                </div>
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className='pa1 f7'>&</div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[1] && <div className='mv3 ph1  f7'>&</div>}
                                                </div>
                                                <div className=''>
                                                   {summaryData.matchSummary.bestBowler && summaryData.matchSummary.bestBowler.battingStatsList && summaryData.matchSummary.bestBowler.battingStatsList.length > 0 && summaryData.matchSummary.bestBowler.battingStatsList[1] && <div className='ba bg-frc-yellow  tc br2 pv1 b--black black  ph2'>{summaryData.matchSummary.bestBowler.battingStatsList[1] && (
                                                      <span className='f6 fw5'>
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[1].runs}
                                                         {summaryData.matchSummary.bestBowler.battingStatsList[1].isNotOut === true ? '*' : ''}
                                                      </span>
                                                   )}
                                                      {summaryData.matchSummary.bestBowler.battingStatsList[1] && (
                                                         <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.battingStatsList[1].balls})</span>
                                                      )}</div>}
                                                   {summaryData.matchSummary.bestBowler.bowlingStatsList[1] && <div className='ba mv2 bg-silver br2 pv1 b--black  tc ph2'> <span className='f6 fw5'>
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[1].wickets}/
                                                      {summaryData.matchSummary.bestBowler.bowlingStatsList[1].runsConceded}
                                                   </span>
                                                      <span className='f7 '>&nbsp;({summaryData.matchSummary.bestBowler.bowlingStatsList[1].overs})</span></div>}
                                                </div>
                                             </div>
                                          </div>
                                    </div>}
                                 </div>
                              )}
                           </>
                        ))}
                     </div>
                  </>
               </div>}
            </div>
            </div>}

{/* start */}
{ StadiumStatHub && StadiumStatHub.stadiumHub &&  StadiumStatHub.stadiumHub.playerRecordBatting && StadiumStatHub.stadiumHub.playerRecordBatting.totalRuns && StadiumStatHub.stadiumHub.playerRecordBatting.totalRuns.length>0 &&   <div>
                  {/* <div className='bb b--white-20 mt3 mb2'></div> */}


                  <div className='bg-gray  py-2  mt-3 rounded-xl'>
                     <div className='flex items-center justify-between'>
                     <div className='text-base ml-2  font-bold'>
                        
                        < Heading heading= {getLangText(lang,words,'PLAYER RECORDS')}  />
                        </div>
                 
                     {(playerRecord === 'BATTING' && StadiumStatHub.stadiumHub.playerRecordBatting[battingType].length > 0) || (playerRecord === 'BOWLING' && StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType].length > 0)  ?
                              <div>
                                 {!viewAll && <div className='flex  mr-4 bg-gray-8 uppercase flex-col  items-center justify-center' onClick={() => { setViewAll(true) }}>
                                    <div className='border  border-green  p-2  rounded text-green text-xs  font-semibold'>{getLangText(lang,words,'View All')}</div>

                                 </div>}

                              </div> : <></>}
                     </div>

                     
                   
                     {false && TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && <div className="silver f8 f7-l f7-m pv1 ">
                        {lang==='HIN'?
                        <span>{` सभी ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.homeTeamShortName} vs ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.awayTeamShortName} मैचों से`}</span>
                        :<span>{`* From matches involving  ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.homeTeamShortName} and ${TeamStatHub && TeamStatHub.teamHub && TeamStatHub.teamHub.teamRecords && TeamStatHub.teamHub.teamRecords.awayTeamShortName}`}</span>}
                        </div>}




                     <div className='flex items-center justify-center rounded-full  bg-gray-8 m-2 '>
                        {playerRecordTab.map((team, i) => (
                           <div
                              onClick={() => { setPlayerRecord(team); setViewAll(false) }}
                              className={`${playerRecord === team ? ' border border-green pv1 ' : ' white pv1 '
                                 }    px-3  capitalize  cursor-pointer w-6/12 rounded-full text-center p-1 `}

                              key={i}>
                              {getLangText(lang,words,team)}
                           </div>
                        ))}
                        {/* style={{borderBottomColor: "#777"}} */}
                     </div>
                     
                     <>
                        <div className=' bg-gray pt-2   white rounded text-xs f6-l   ' >
                           {playerRecord === "BATTING" ? <div className={`  relative  w-40 ml2     mt2 `} ref={wrapperRef}>
                              <div
                                 className={`h-100  ${showDropDown ? 'br--top' : ''} rounded  text-white border flex items-center justify-center flex-column cursor-pointer   bg-gray-4`}
                                 onClick={() => {
                                    setShowDropDown((prev) => !prev);
                                 }}>
                                 <div className='flex w-100 pa1 items-center justify-center '>
                                    <div className='  f6 fw5 ttc  ml2 pa1'>{battingType === "battingAverage" ?getLangText(lang,words,'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang,words,'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang,words,'RUNS') : "6s"} </div>

                                    <div className='bg-red tc  bl b--white-20 flex items-center justify-center'>
                                       <ImageWithFallback height={18} width={18} loading='lazy'
                                          className='w-2 ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                    </div>
                                 </div>
                              </div>
                              {showDropDown && (
                                 <div
                                    className='  text-white border absolute w-full h-32 p-2 overflow-y-scroll z-99999 overflow-hidden bg-gray-4' style={{ bottom: 0, left: 0, top: 33 }}>
                                    {battingTab.map((tabName, i) => (
                                       <div
                                          key={i}
                                          onClick={() => {
                                             setShowDropDown((prev) => !prev), setBattingType(tabName); setViewAll(false)
                                          }}
                                          className='p-2 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30'>
                                          <div className='pl2 nowrap white ttc  pb2 black fw5 f6'>{tabName === "battingAverage" ? <div>{getLangText(lang,words,'AVERAGE')}</div> : tabName === "battingStrikeRate" ? <div>{getLangText(lang,words,'S/R')}</div> : tabName === "fifties" ? <div>50s</div> : tabName === "hundreds" ? <div>100s</div> : tabName === "totalFours" ? <div>4s</div> : tabName === "totalRuns" ? <div>{getLangText(lang,words,'RUNS')}</div> : "6s"}</div>
                                       </div>
                                    ))}
                                 </div>
                              )}
                           </div> : <div className={` relative  w-40 ml2     mt2`} ref={wrapperRef}>
                              <div
                                 className={`  ${showDropDown ? 'br--top' : ''} rounded border  flex items-center flex-column cursor-pointer   bg-gray-4`}
                                 onClick={() => {
                                    setShowDropDown((prev) => !prev);
                                 }}>
                                 <div className='flex w-full  p-1 items-center justify-center '>
                                    <div className='  text-center text-xs font-semibold ttc p-2 '>{bowlingType === "bowlingStrikeRate" ? getLangText(lang,words,'S/R') : bowlingType === "economy" ? getLangText(lang,words,'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" :getLangText(lang,words,'wickets')} </div>

                                    <div className=' text-center   text-center bl b--white-20 flex items-center justify-center'>
                                       <ImageWithFallback height={18} width={18} loading='lazy'
                                           className='w-2 ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                    </div>
                                 </div>
                              </div>
                              {showDropDown && (
                                 <div
                                    className='  border   rounded w-full h-40   absolute   overflow-y-scroll z-99999 overflow-hidden bg-gray-4' style={{ bottom: 0, left: 0, top: 33 }}>
                                    {bowlingTab.map((tabName, i) => (
                                       <div
                                          key={i}
                                          onClick={() => {
                                             setShowDropDown((prev) => !prev); setBowlingType(tabName); setViewAll(false)
                                          }}
                                          className='ph2 pv1 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30 '>
                                          <div className='ma0 pb0  nowrap white  pb2 black ttc fw5 f6'> {tabName === "bowlingStrikeRate" ? <div>{getLangText(lang,words,'S/R')}</div> : tabName === "economy" ? <div>{getLangText(lang,words,'ECONOMY')}</div> : tabName === "fiveWickets" ? <div>5-W</div> : tabName === "threeWickets" ? <div>3-W</div> :getLangText(lang,words,'wickets')}</div>
                                       </div>
                                    ))}
                                 </div>
                              )}
                           </div>}

                           
                           {(playerRecord === 'BATTING' && StadiumStatHub.stadiumHub.playerRecordBatting[battingType].length > 0) || (playerRecord === 'BOWLING' && StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType].length > 0) ?
                              <div>
                     {StadiumStatHub && StadiumStatHub.stadiumHub && (playerRecord === 'BATTING' ? StadiumStatHub.stadiumHub.playerRecordBatting[battingType] : StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType]).map((item, index) =>{
                                    return (
                                       <div key={index} className={`${!viewAll && index > 5 ? 'dn' : 'db'}`}>
                                          <div className='flex       justify-between py-2 mv2 bg-gray-4 rounded-xl m-2  '>
                                             <div className='flex  w-full    justify-start items-center'>
                                                <div className=' flex  w-3/12  items-center    justify-center '>
                                                   <ImageWithFallback height={28} width={28} loading='lazy'
                                                   fallbackSrc = {playerPlaceholder} className='h-12 w-12  rounded-full bw1  bg-gray object-cover object-top ' src={`https://images.cricket.com/players/${item.playerID}_headshot.png`}  alt='' />
                                                </div>

                                                <div className='  w-9/12 flex-col flex items-start justify-center'>
                                                   <div className='flex  flex-col items-start justify-between'>
                                                      <div className='  text-xs font-semibold'>{isEnglish(lang) ? item.playerName : item.playerNameHindi}</div>
                                                      <div className="w-30 flex items-end justify-end">
                                                         <div className='border mt-2  rounded  border border-green text-gray-2   px-2' >{item.shortName}</div>
                                                      </div>
                                                   </div>
                                                   <div className=' flex pt1  justify-between mt-2'>

                                                      <div className="  flex items-center bg-basebg justify-start mx-2 rounded px-2 ">
                                                         <div className=' rounded-full bw1 bg-dark-gray object-cover object-top ' >{item.totalMatches} {getLangText(lang,words,'matches')}</div>

                                                      </div>
                                                      {playerRecord === 'BATTING' && battingType !== "totalRuns" && 
                                                      <div className="  flex items-center  justify-center"> 
                                                      <div className='  bg-gray-8   text-white rounded  py-1 px-2'>{item.playerRuns} {getLangText(lang,words,'RUNS')}</div></div>}
                                                      {playerRecord === 'BOWLING' && bowlingType !== "totalWickets" && <div className=" w-50 flex items-end justify-end"> 
                                                      <div className='bg-gray-8   text-white rounded  py-1 px-2'>{item.totalWickets} {getLangText(lang,words,'WICKETS')}</div></div>}
                                                   </div>
                                                </div>
                                             </div>

                                             <div className='w-20 tc bg-gray-8 rounded m-2 p-2 flex    items-center justify-center'>
                                                                                               <div className=' text-base text-green oswald  flex  flex-col  items-center justify-center'>
                                                                                               <div className="fw5 ttc text-gray-2 f6-l text-xs">{playerRecord === 'BATTING' ? (battingType === "battingAverage" ?getLangText(lang,words,'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang,words,'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang,words,'RUNS') : "6s") : (bowlingType === "bowlingStrikeRate" ? getLangText(lang,words,'S/R') : 
                                                bowlingType === "economy" ? getLangText(lang,words,'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" : getLangText(lang,words,'wickets'))}</div>
 
                                                   {/* {playerRecord === 'BATTING' ? TeamStatHub.teamHub.playerRecordBatting[battingType][index][battingType === "totalRuns" ? "playerRuns" : battingType] : TeamStatHub.teamHub.playerRecordBowling[bowlingType][index][bowlingType]}  */}
                                                   
                                                   {playerRecord === 'BATTING' ? StadiumStatHub && StadiumStatHub.stadiumHub.playerRecordBatting[battingType][index][battingType === "totalRuns" ? "playerRuns" : battingType] : StadiumStatHub && StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType][index][bowlingType]}
                                                   </div>
                                             </div>
                                          </div>
                                       </div >
                                    );
                                 })}
                              </div> : <div className="white  tc pv3 ph2 fw5 f6 f5-l f5-m">Data not available</div>
                           }
                       

                        </div>

                     </>

                  </div>
               </div>}
{/* end */}
            { false&& StadiumStatHub && StadiumStatHub.stadiumHub &&  StadiumStatHub.stadiumHub.playerRecordBatting && StadiumStatHub.stadiumHub.playerRecordBatting.totalRuns && StadiumStatHub.stadiumHub.playerRecordBatting.totalRuns.length>0 &&  <div>
            <div className='h-01 bg-gray mt3 mb2'></div>


            <div>
               <div className='f5  fw6 white pt1 ttu pl2'>{getLangText(lang,words,'PLAYER RECORDS')}</div>
               {StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.characteristics && <div className="silver f8 f7-l pl2 f7-m flex "><div>*</div><div className="pl1">{lang==='HIN'?`${StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.venueName} में खेले गए मैचों से`:`From matches in ${StadiumStatHub && StadiumStatHub.stadiumHub && StadiumStatHub.stadiumHub.venueName}`}</div></div>}

               <div className='flex items-center justify-start pt2  '>
                  {playerRecordTab.map((team, i) => (
                     <div
                        onClick={() => { setPlayerRecord(team); setViewAll(false) }}
                        className={`${playerRecord === team ? 'bg-frc-yellow black pv1 ' : 'bg-dark-gray white pv1 '
                           }  f6 f6-l  br--top mh2 ttu ph3  tc cursor-pointer mh5-l b--black  fw6 br2`}
                        key={i}>
                           
                        {getLangText(lang,words,team)}
                     </div>
                  ))}
                  {/* style={{borderBottomColor: "#777"}} */}
               </div>
               <>
                  <div className=' bg-black pt2   white br2  f7 f6-l ba b--white-20  ' >
                     {playerRecord === 'BATTING' ? <div className={`  relative  w-40 ml2     mt2 `} ref={wrapperRef}>
                        <div
                           className={`h-100  ${showDropDown ? 'br--top' : ''} br2 b--yellow white ba flex items-center justify-center flex-column cursor-pointer   bg-dark-gray`}
                           onClick={() => {
                              setShowDropDown((prev) => !prev);
                           }}>
                           <div className='flex w-100 pa1 items-center justify-center '>
                              <div className='w ttc f6 fw5 ttc ml2 pa1'>{battingType === "battingAverage" ? getLangText(lang,words,'AVERAGE'): battingType === "battingStrikeRate" ? getLangText(lang,words,'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? "Runs" : "6s"} </div>

                              <div className='w-20 tc  bl b--white-20 flex items-center justify-center'>
                                 <ImageWithFallback height={18} width={18} loading='lazy'
                                     className='w-2  ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                              </div>
                           </div>
                        </div>
                        {showDropDown && (
                           <div
                              className='  b--yellow bl bb br bt  br2 br--bottom white absolute w-100 h4 overflow-y-scroll z-99999 overflow-hidden bg-dark-gray' style={{ bottom: 0, left: 0, top: 33 }}>
                              {battingTab.map((tabName, i) => (
                                 <div
                                    key={i}
                                    onClick={() => {
                                       setShowDropDown((prev) => !prev), setBattingType(tabName); setViewAll(false)
                                    }}
                                    className='p-2 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30'>
                                    <div className='pl2 nowrap white ttc  pb2 black fw5 f6'>{tabName === "battingAverage" ? <div> {getLangText(lang,words,'AVERAGE')}</div> : tabName === "battingStrikeRate" ? <div>{getLangText(lang,words,'S/R')}</div> : tabName === "fifties" ? <div>50s</div> : tabName === "hundreds" ? <div>100s</div> : tabName === "totalFours" ? <div>4s</div> : tabName === "totalRuns" ? <div>{lang==='HIN'?'रन':'Runs'}</div> : "6s"}</div>
                                 </div>
                              ))}
                           </div>
                        )}
                     </div> :
                        <div className={` relative  w-40 ml2     mt2`} ref={wrapperRef}>
                           <div
                              className={`h-100  ${showDropDown ? 'br--top' : ''} br2 b--yellow white ba flex items-center flex-column cursor-pointer   bg-dark-gray`}
                              onClick={() => {
                                 setShowDropDown((prev) => !prev);
                              }}>
                              <div className='flex w-100 pa1 items-center justify-center '>
                                 <div className='w-80  f6 fw5 ttc  ml2 pa1'>{bowlingType === "bowlingStrikeRate" ? getLangText(lang,words,'S/R') : bowlingType === "economy" ?getLangText(lang,words,'ECONOMY'): bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" : getLangText(lang,words,'wickets')} </div>

                                 <div className='w-20 tc  bl b--white-20 flex items-center justify-center'>
                                    <ImageWithFallback height={18} width={18} loading='lazy'
                                      className='w-2 ' src={showDropDown ? upArrow : downArrow} alt='dropdown' />
                                 </div>
                              </div>
                           </div>
                           {showDropDown && (
                              <div
                                 className='  b--yellow bl bb br bt  br2 br--bottom white absolute w-100 h4 overflow-y-scroll z-99999 overflow-hidden bg-dark-gray' style={{ bottom: 0, left: 0, top: 33 }}>
                                 {bowlingTab.map((tabName, i) => (
                                    <div
                                       key={i}
                                       onClick={() => {
                                          setShowDropDown((prev) => !prev); setBowlingType(tabName); setViewAll(false)
                                       }}
                                       className='ph2 pv1 white-80 flex items-center justify-start cursor-pointer hover-bg-black-30 '>
                                       <div className='ma0 pb0  nowrap white  pb2 black ttc fw5 f6'> {tabName === "bowlingStrikeRate" ? <div>{ getLangText(lang,words,'S/R')}</div> : tabName === "economy" ? <div>{getLangText(lang,words,'ECONOMY')}</div> : tabName === "fiveWickets" ? <div>5-W</div> : tabName === "threeWickets" ? <div>3-W</div> : getLangText(lang,words,'wickets')}</div>
                                    </div>
                                 ))}
                              </div>
                           )}
                        </div>}
                        {(playerRecord === 'BATTING' && StadiumStatHub.stadiumHub.playerRecordBatting[battingType].length > 0) || (playerRecord === 'BOWLING' && StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType].length > 0) ?
                              <div>
                     {StadiumStatHub && StadiumStatHub.stadiumHub && (playerRecord === 'BATTING' ? StadiumStatHub.stadiumHub.playerRecordBatting[battingType] : StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType]).map((item, index) =>
                        <div key={index} className={`${!viewAll && index > 5 ? 'dn' : 'db'}`}>
                        <div className='flex       justify-between pv2 mv2 bg-gray-4 rounded-xl m-2 p-2 '>
                           <div className='flex  w-80  justify-start items-center'>
                              <div className=' flex  w-30 items-center  bg-gray h-12 w-12 rounded-full   justify-center '>
                                 <ImageWithFallback height={28} width={28} loading='lazy'
                                    fallbackSrc = {playerPlaceholder} className='h-12 w-12 rounded-full bw1  bg-dark-gray object-cover object-top ' src={`https://images.cricket.com/players/${item.playerID}_headshot.png`}  alt='' />
                              </div>

                              <div className=' w-70 ph2 pl-3'>
                                 <div className='flex  flex-col items-start justify-between'>
                                    <div className='  text-xs font-semibold'>{isEnglish(lang) ? item.playerName : item.playerNameHindi}</div>
                                    <div className="w-30 flex items-end justify-end">
                                       <div className='border mt-2  rounded  border border-green text-gray-2   px-2' >{item.shortName}</div>
                                    </div>
                                 </div>
                                 <div className=' flex pt1  justify-between mt-2'>

                                    <div className="w-50  flex items-start justify-start mx-2 ">
                                       <div className='bg-gray-8   rounded    text-center text-gray-2  captilize px-2 py-1' >{item.totalMatches} {getLangText(lang,words,'matches')}</div>

                                    </div>
                                    {playerRecord === 'BATTING' && battingType !== "totalRuns" && 
                                    <div className=" w-50 flex items-center  justify-center"> 
                                    <div className='  bg-gray-8   text-white rounded  py-1 px-2'>{item.playerRuns} {getLangText(lang,words,'RUNS')}</div></div>}
                                    {playerRecord === 'BOWLING' && bowlingType !== "totalWickets" && <div className=" w-50 flex items-end justify-end"> 
                                    <div className='bg-gray-8   text-white rounded  py-1 px-2'>{item.totalWickets} {getLangText(lang,words,'WICKETS')}</div></div>}
                                 </div>
                              </div>
                           </div>

                           <div className='w-20 tc bg-gray-8 rounded m-2 p-2 flex    items-center justify-center'>
                                                                             <div className=' text-base text-green oswald  flex  flex-col  items-center justify-center'>
                                                                             <div className="fw5 ttc text-gray-2 f6-l text-xs">{playerRecord === 'BATTING' ? (battingType === "battingAverage" ?getLangText(lang,words,'AVERAGE') : battingType === "battingStrikeRate" ? getLangText(lang,words,'S/R') : battingType === "fifties" ? "50s" : battingType === "hundreds" ? "100s" : battingType === "totalFours" ? "4s" : battingType === "totalRuns" ? getLangText(lang,words,'RUNS') : "6s") : (bowlingType === "bowlingStrikeRate" ? getLangText(lang,words,'S/R') : 
                              bowlingType === "economy" ? getLangText(lang,words,'ECONOMY') : bowlingType === "fiveWickets" ? "5-W" : bowlingType === "threeWickets" ? "3-W" : getLangText(lang,words,'wickets'))}</div>

                                 {playerRecord === 'BATTING' ? TeamStatHub.teamHub.playerRecordBatting[battingType][index][battingType === "totalRuns" ? "playerRuns" : battingType] : TeamStatHub.teamHub.playerRecordBowling[bowlingType][index][bowlingType]} </div>
                           </div>
                        </div>
                     </div >

                     )}
                      </div> : <div className="white  tc pv3 ph2 fw5 f6 f5-l f5-m">Data not available</div>
                           }

                    {(playerRecord === 'BATTING' && StadiumStatHub.stadiumHub.playerRecordBatting[battingType].length > 0) || (playerRecord === 'BOWLING' && StadiumStatHub.stadiumHub.playerRecordBowling[bowlingType].length > 0) ?
                              <div>
                                 {!viewAll && <div className='flex  flex-column mv4 cursor-pointer items-center justify-center' onClick={() => { setViewAll(true) }}>
                                    <div className='ba bg-dark-gray  pv2 ph4 b--black br2 white  f6 fw6'>{getLangText(lang,words,'View All')}</div>

                                 </div>}

                              </div> : <></>}
                    
                  </div>
                  {/* <div className="silver f7 tc mv2">*All records from DC vs MI games since Jan 2015</div> */}
               </>
             
            </div>
            </div>}
            
         </div>:
         <div className="w-100 br2 ba  b--white-20 ">
      <DataNotFound />
         </div>}
         </>

      )
}