import React, { useState, useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import { GET_FRC_SELECTION_COMPOSITION } from '../../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { format } from 'date-fns';
import Countdown from 'react-countdown-now';
import StatHubTeam from './statHubTeam';
import Link from 'next/link';
import StatHubStadium from './statHubStadium';
import PlayerStathubBYT from './playerStathubBYT'
import PlayerStatHub from './playerStatsHubV2'
import PlayerStatsChooseCaptains from './playerStatsChooseCaptains'
import PlayerStatsHubViewTeam from './playerStatsHubViewTeam'
const flagPlaceHolder = '/svgs/images/flag_empty.png';
import backIcon from '../../public/svgs/back_dark_black.svg'
import { words } from './../../constant/language'
const Main = '/pngs/ban1.png';
const Empty = '/svgs/rightarrow.svg';
// const backIconWhite = '/svgs/backIconWhite.svg';
const backIconWhite = '/pngs/arrow-left.png';
import CleverTap from 'clevertap-react';
import ImageWithFallback from '../commom/Image';


export default function stathub(props) {
  console.log("STATHUB",props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType)

  const getVideoView = (video) => {
    let type = 'fantasy';
    let videoId = video.videoID;
    return {
      as: eval(VIDEOS_VIEW.as),
      href: VIDEOS_VIEW.href
    };
  };
  // console.log("props language statshub n",props)
  let lang=props.language?props.language:'ENG'
  let router = useRouter();
  const[matchId,matchSlug,pageCategory,stahubCategory]=router.query.slugs
  // console.log("props statshubdatedfwffa",props)
// console.log("props statshubdata",props && props.data.miniScoreCard.data[0] && props.data.miniScoreCard.data[0] && props.data.miniScoreCard.data[0].isLiveCriclyticsAvailable )
  const createList = (team) => {
    let list = team && team.map((player) => {
      return player.playerId
    })
    // console.log("createList", list)
    return list.length > 0 ? list : []
  }

  const createCategoryPlayers = (team) => {

    let category = {
      AR: [...team.filter(pl => pl.player_role === "ALL_ROUNDER")],
      BAT: [...team.filter(pl => pl.player_role === "BATSMAN")],
      BOWL: [...team.filter(pl => pl.player_role === "BOWLER")],
      WK: [...team.filter(pl => pl.player_role === "KEEPER")]
    }
    // console.log('catogory', category)
    return { ...category }
  }
  const getLangText = (lang,keys,key) => {
    
    let [english,hindi] = keys[key]
    switch(lang){
      case 'HIN':
        return hindi
      case 'ENG':
        return english
      default:
        return english
    }
      }
  const calculateCredits = (team) => {
    let sum = team && team.reduce((pl, i) => { return parseFloat(pl) + parseFloat(i.credits) }, 0)

    
    return 100 - sum
  }

  
  // const [playerList, setplayerList] = useState([])

  
  const [credit, setcredit] = useState(props.urlData.team ? calculateCredits(props.urlData.team) : 100)
  // console.log("outer",credit)
  const [categoryPlayer, setcategoryPlayer] = useState(props && props.urlData && props.urlData.team ? createCategoryPlayers(props.urlData.team) : { AR: [], BAT: [], BOWL: [], WK: [] })
  // console.log(categoryPlayer",categoryPlayer)
  const [totlaPlayer, settotalPlayer] = useState(11)
  const [teamName, setteamName] = useState(props && props.urlData && props.urlData.teamName ? props.urlData.teamName : "")
  // console.log("outer teamName",teamName)
  const [ffCode, setffCode] = useState(props && props.urlData && props.urlData.ffCode ? props.urlData.ffCode : "")
  const [captain, setcaptain] = useState(props && props.urlData && props.urlData.team ? props.urlData.team.filter(pl => pl.captain === "1")[0].playerId : null)
  const [viceCaptain, setviceCaptain] = useState(props && props.urlData && props.urlData.team ? props.urlData.team.filter(pl => pl.vice_captain === "1")[0].playerId : null)
  const [listID, setlistID] = useState(props && props.urlData && props.urlData.leagueType && props.urlData.leagueType === "statsHub" ? createList(props.urlData.team) : [])
  // console.log("outer listID",listID)
  const [recentplayer,setrecentplayer]=useState(null)
  // console.log("outer recentplayer",recentplayer)
  const [config, setconfig] = useState({
    WK: {
      filter: 'KEEPER',
      fullName: 'Wicket-Keeper',
      limit: '1-2',
    },
    BAT: {
      filter: 'BATSMAN',
      fullName: 'Batsmen',
      limit: '3 - 6',
    },
    AR: {
      filter: 'ALL_ROUNDER',
      fullName: 'All-Rounders',
      limit: '1 - 4',
    },
    BOWL: {
      filter: 'BOWLER',
      fullName: 'Bowlers',
      limit: '3 - 6',
    },
  })


  // console.log("config",config)
  const [matchConfig, setmatchConfig] = useState({
    maxCredits: 100.0,
    maxPlayers: 11,
    WK_max: 2,
    WK_min: 1,
    AR_max: 4,
    AR_min: 1,
    BOWL_max: 6,
    BOWL_min: 3,
    BAT_max: 6,
    BAT_min: 3,
  })
 
  const [componentType, setcomponentType] = useState(((props.viewTeamStatsHub=="" )&& (props.urlData.leagueType === "statsHub")) ? 'BYTPlayerStathubState' :((props.viewTeamStatsHub==="viewstatshubteam") &&  (props.urlData.leagueType === "statsHub"))?'viewTeamStatsHub': stahubCategory==="teams"?"TeamState":stahubCategory==="stadium"?"StadiumState" :'PlayerState')
  // console.log("outer componentType",componentType)
  // console.log("(props &&( props.viewTeamSta",(props.viewTeamStatsHub==="viewstatshubteam" ))
  const [BYTList, setBYTList] = useState(props && props.urlData && props.urlData.team ? [...props.urlData.team] : [])
  // console.log(" outer BYTList",BYTList)
  let FantasymatchID = props.matchID;
  const data = (props.matchBasicData && [...props.matchBasicData.getFRCHomePage.completedmatches, ...props.matchBasicData.getFRCHomePage.livematches, ...props.matchBasicData.getFRCHomePage.upcomingmatches].filter((x) => x.matchID === FantasymatchID).length > 0 ?
    [...props.matchBasicData.getFRCHomePage.completedmatches, ...props.matchBasicData.getFRCHomePage.livematches, ...props.matchBasicData.getFRCHomePage.upcomingmatches].filter((x) => x.matchID === FantasymatchID) :
    [{
      ...props.data.miniScoreCard.data[0],
      awayTeamShortName: props.data.miniScoreCard.data[0].awayTeamName,
      homeTeamShortName: props.data.miniScoreCard.data[0].homeTeamName,
      MatchName: props.data.miniScoreCard.data[0].matchName
    }]
  )
// console.log("datadatadatadatadata",data)
  let seriesID = props && props.data && props.data.miniScoreCard &&  props.data.miniScoreCard.data &&props.data.miniScoreCard.data[0] && props.data.miniScoreCard.data[0].seriesID

const setViceCaptainCatainComponent=(state)=>{
        if(state==="BYTPlayerStathubState"){
          if(props && props.urlData && props.urlData.team){
          let value=  calculateCredits(props.urlData.team)
          // console.log("value",value)
          setcredit(value)
          setcomponentType(state)
          }
        }
}
  const setComponentState = (statename) => {
    // console.log("statename", statename)
    setcomponentType(statename)
  }
  // let config = {
  //   WK: {
  //     filter: 'KEEPER',
  //     fullName: 'Wicket-Keeper',
  //     limit: '1-2',
  //   },
  //   BAT: {
  //     filter: 'BATSMAN',
  //     fullName: 'Batsmen',
  //     limit: '3 - 6',
  //   },
  //   AR: {
  //     filter: 'ALL_ROUNDER',
  //     fullName: 'All-Rounders',
  //     limit: '1 - 4',
  //   },
  //   BOWL: {
  //     filter: 'BOWLER',
  //     fullName: 'Bowlers',
  //     limit: '3 - 6',
  //   },
  // };
  // let matchConfig = {
  //   maxCredits: 100.0,
  //   maxPlayers: 11,
  //   WK_max: 2,
  //   WK_min: 1,
  //   AR_max: 4,
  //   AR_min: 1,
  //   BOWL_max: 6,
  //   BOWL_min: 3,
  //   BAT_max: 6,
  //   BAT_min: 3,
  // }
// variables: { matchID: '204162' }
  const { loading: getFRCCompositionLoading, error: getFRCCompositionError, data: getFRCComposition } = useQuery(GET_FRC_SELECTION_COMPOSITION, {
    onCompleted: (data) => {
      if (data && data.getPlayerSelectionComposition) {
        setcredit(data.getPlayerSelectionComposition.credits)
        settotalPlayer(data.getPlayerSelectionComposition.totalPlayersCount)

        let matchConfiguration = {
          ...matchConfig,
          WK_max: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'KEEPER',
          )[0].max,
          WK_min: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'KEEPER',
          )[0].min,
          AR_max: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'ALL_ROUNDER',
          )[0].max,
          AR_min: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'ALL_ROUNDER',
          )[0].min,
          BOWL_max: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'BOWLER',
          )[0].max,
          BOWL_min: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'BOWLER',
          )[0].min,
          BAT_max: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'BATSMAN',
          )[0].max,
          BAT_min: data.getPlayerSelectionComposition.players.filter(
            o => o.role == 'BATSMAN',
          )[0].min,
        };
        setmatchConfig(matchConfiguration)

        let configuration = {
          WK: {
            filter: 'KEEPER',
            fullName: 'Wicket-Keeper',
            limit: `${data.getPlayerSelectionComposition.players.filter(
              o => o.role == 'KEEPER',
            )[0].min
              } ${data.getPlayerSelectionComposition.players.filter(
                o => o.role == 'KEEPER',
              )[0].max > 1
                ? `- ${data.getPlayerSelectionComposition.players.filter(
                  o => o.role == 'KEEPER',
                )[0].max
                }`
                : ''
              } `
          },
          BAT: {
            filter: 'BATSMAN',
            fullName: 'Batsmen',
            limit: `${data.getPlayerSelectionComposition.players.filter(
              o => o.role == 'BATSMAN',
            )[0].min
              } - ${data.getPlayerSelectionComposition.players.filter(
                o => o.role == 'BATSMAN',
              )[0].max
              }`,
          },
          AR: {
            filter: 'ALL_ROUNDER',
            fullName: 'All-Rounders',
            AR: `${data.getPlayerSelectionComposition.players.filter(
              o => o.role == 'ALL_ROUNDER',
            )[0].min
              } - ${data.getPlayerSelectionComposition.players.filter(
                o => o.role == 'ALL_ROUNDER',
              )[0].max
              }`
          },
          BOWL: {
            filter: 'BOWLER',
            fullName: 'Bowlers',

            BOWL: `${data.getPlayerSelectionComposition.players.filter(
              o => o.role == 'BOWLER',
            )[0].min
              } - ${data.getPlayerSelectionComposition.players.filter(
                o => o.role == 'BOWLER',
              )[0].max
              }`
          }
        }
        setconfig({ ...configuration })
      }

    }
  });

  // console.log("getFRCComposition",getFRCComposition && getFRCComposition.getPlayerSelectionComposition)

  // const wrapperRef = useRef(null);
  // useEffect(() => {
  //   /**
  //    * Alert if clicked on outside of element
  //    */
  //   function handleClickOutside(event) {
  //     if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
  //       setShowDropDown(false);
  //     }
  //   }

  //   document.addEventListener('mousedown', handleClickOutside);
  //   return () => {
  //     document.removeEventListener('mousedown', handleClickOutside);
  //   };
  // }, [wrapperRef]);


  const tabs = ['all', 'international', 'domestic'];
  const [showDropDown, setShowDropDown] = useState(false);
  const activeTab = 'flex mt-2   item-center  justify-center  w-1/3  text-blue-9 border-b-2 border-blue-9  text-xs font-semibold pb-2';
  const inActiveTab = 'flex  mt-2   item-center  justify-center w-1/3  text-gray-2  border-b-2 border-gray-2  font-semibold text-xs pb-2';
  const [matchID, matchName, screen, tabname] = router.query.slugs;

  const handleNavTab = (type) => {
    CleverTap.initialize(`${type === "players" ? "FantasyStatsPlayer" : type === "teams" ? "FantasyStatsTeam" : "FantasyStatsStadium"}`, {
      MatchID: props.matchID,
      SeriesID: seriesID,
      TeamAID: data[0].homeTeamID,
      TeamBID: data[0].awayTeamID,
      MatchFormat: data[0].matchType,
      MatchStartTime: format(Number(data[0].matchDateTimeGMT), 'd,y, h:mm a'),
      MatchStatus: data[0].matchStatus,
      Platform: localStorage ? localStorage.Platform : ''
    });
    router.push('/fantasy-research-center/[...slugs]', `/fantasy-research-center/${matchID}/${matchName}/${screen}/${type}`);

  };

  // const [statsTab, setStatsTab] = useState('battingRecord');

  const playerScores = (players, creditScores, categoryList, TeamIDList, type,RecentPlayer) => {
    // setcategoryPlayer({...categoryList})
    // setcredit(creditScores)
    //  setplayerList([...playerList])
    // setlistID([...TeamIDList])
    // console.log("playerListplayerList",players)
    // console.log("creditScores",creditScores)
    if (type === 'chooseCaptains') {
      setcategoryPlayer({ ...categoryList })
      setcredit(creditScores)
      setBYTList([...players])
      setlistID([...TeamIDList])
    }
    if (type === "BYTPlayerStathubState") {
      setcredit(creditScores)
      setlistID([...TeamIDList])
      setcategoryPlayer({ ...categoryList })
      setrecentplayer(RecentPlayer)
    }

    if(type==="maintainState"){
      // console.log("maintainState creditScores",creditScores)
    setcredit(creditScores)
    setlistID([...TeamIDList])
    setcategoryPlayer({ ...categoryList })
    setrecentplayer(RecentPlayer)
    }


  }

  const handleMonthHindi=()=>{
    let monthformat= format(Number(data[0].matchDateTimeGMT) - 19800000, 'MMMM')
 
    let date=format(Number(data[0].matchDateTimeGMT) - 19800000, 'do')
    let year=format(Number(data[0].matchDateTimeGMT) - 19800000, 'yyyy')
    //  console.log("monthformat",monthformat)
  
    if(lang==='HIN'){
      return  getLangText(lang,words,monthformat.toLowerCase())+' '+date+' '+year
    }else{
     return monthformat+' '+date+' '+year
    }
   }

  const language_icon = '/pngs/iconlang.png';
  const [language, setlanguage] = useState(
    typeof window !== 'undefined' && global.window.localStorage.getItem('FRClanguage')
      ? global.window.localStorage.getItem('FRClanguage')
      : 'ENG'
  );

  return (
    <div className='mw75-l pb3 min-vh-100 bg-basebg center white  hidescoll  '>

<div className=' z-1 ph2 h2-4 flex items-center justify-between text-white capitalize mt-3 mx-2 lg:mx-4 md:mx-4 lg:hidden md:hidden'>
          <div className='flex items-center justify-center'>  
          {/* <div className='p-2 bg-gray rounded-md ' onClick={() => window.history.back()}>
            <img
              className=' flex items-center justify-center h-4 w-4 rotate-180'
              src='/svgsV2/RightSchevronWhite.svg'
              alt=''
            />
          </div> */}
          <div
                onClick={() => window && window.history && window.history.back()}
                className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
              >
                <svg width="30" focusable="false" viewBox="0 0 24 24">
                  <path
                    fill="#ffff"
                    d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
                  ></path>
                  <path fill="none" d="M0 0h24v24H0z"></path>
                </svg>
              </div>
          
           <span className='ml-2 font-bold  '> { lang==='HIN'?
      (componentType === "BYTPlayerStathubState" || componentType === "chooseCaptains") ? 'अपनी टीम बनाएं' : componentType === "viewTeamStatsHub"? props.urlData.teamName:'फ़ैंटसी आँकड़े'
        :(componentType === "BYTPlayerStathubState" || componentType === "chooseCaptains") ? 'build your team' : componentType === "viewTeamStatsHub"? props.urlData.teamName:'Fanatsy Stats'}
        </span>
        </div>

          {/* <div className="cursor-pointer" onClick={() => {
            language === 'HIN' ? (setlanguage('ENG'), localStorage.setItem('FRClanguage', 'ENG')) :
              (setlanguage('HIN'), localStorage.setItem('FRClanguage', 'HIN'))
          }}>
            <img className='h2 w2' src={language_icon} />
          </div> */}
        </div>

      {/* <div className="bg-gold z-1 ph2 h2-4 flex items-center justify-between">
     <div className="flex items-center w-50 justify-start">
        <img className="w1 " onClick={() => componentType === "chooseCaptains" ? setcomponentType('BYTPlayerStathubState') : (Object.entries(props.urlData).length===0  && componentType === "BYTPlayerStathubState") ? setcomponentType('PlayerState') : router.push('/fantasy-research-center/[...slugs]', `${matchID}/${matchName}`)} alt='' src={backIcon} />
        { lang==='HIN'?
        <div className="black f6 fw7 pl3 ttu">{(componentType === "BYTPlayerStathubState" || componentType === "chooseCaptains") ? 'अपनी टीम बनाएं' : componentType === "viewTeamStatsHub"? props.urlData.teamName:'फ़ैंटसी आँकड़े'}</div>
        :<div className="black f6 fw7 pl3 ttu">{(componentType === "BYTPlayerStathubState" || componentType === "chooseCaptains") ? 'build your team' : componentType === "viewTeamStatsHub"? props.urlData.teamName:'FANTASY STATS'}</div>}
        </div>
       {componentType === "viewTeamStatsHub" && <div className="flex items-center w-50 justify-end">
         <div className="fw6 f6 black">Credit</div>
          <div className="flex items-center ml2 fw6 f6 justify-center br2 pv2 ph2 bg-basebg white ">{props.urlData.team&& 100 - calculateCredits(props.urlData.team) }</div>
       </div>}
       
      </div> */}

      {(componentType !== "chooseCaptains" && componentType !== "viewTeamStatsHub")? data && data[0] && !data[0].isLiveCriclyticsAvailable ?
        <div className="bg-gray-4 border border-green flex  m-3 rounded-md justify-center">
          <div className="w-full  h-20 mt2 relative ">
            {/* <img className="br4 ba b--frc-yellow  h-100 w-100  " style={{ borderWidth: "1px" }} src={Main} /> */}
            <div className="absolute top-0  left-0 right-0  flex justify-center items-center tc">
              <div className=" w-full  ">
                <div className="w-full flex items-center justify-between mt-3  ">
                  <div className="flex w-4/12  items-center justify-center ">
                   
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                        alt=''
                        src={`https://images.cricket.com/teams/${data[0].homeTeamID}_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        className='h-6 mx-1 rounded w-10 object-cover  object-top'
                      />
                    
                    <p className="oswald f5 f3-l f3-m  fw5 white pl2">{data[0].homeTeamShortName}</p>
                  </div>
                  <div className="w-4/12 flex items-center justify-center ">
                    <div className=" w-6/12 border-green border text-xs font-bold rounded-full p-1 flex items-center justify-center">
                   {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType}
                    </div>
                  </div>
                  <div className="flex items-center justify-center text-center w-4/12 ">
                    <p className="oswald text-white ">{data[0].awayTeamShortName}</p>
                    
                      <ImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}
                        alt=''
                        src={`https://images.cricket.com/teams/${data[0].awayTeamID}_flag_safari.png`}
                        // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                        className='h-6 mx-1 rounded w-10 object-cover object-top'
                      />
                  
                  </div>
                </div>
                <div className="flex items-center justify-center">
                  <div className="dib">
                    {new Date(Number(data[0].matchDateTimeGMT) - 19800000) - new Date().getTime() < 84600000 ? (
                      <Countdown
                        date={new Date(Number(data[0].matchDateTimeGMT) - 19800000)}
                        renderer={(props) => (
                          <div className='py-0.5  rounded text-xs  text-center font-bold text-white truncate flex'>
                            <div> {`Starts in `}{' '}</div>
                            <div>{`${props.days !== 0 ? props.days * 24 + Number(props.hours) : props.hours} h ${props.minutes
                              } m ${props.seconds
                              } s `}</div>
                          </div>
                        )}
                      />) : <div className='pa1 ph2 br2 f8 f7-l f7-m fw6 white bg-darkRed truncate'>
                        <div>{handleMonthHindi()}
                      </div>
                      <div className='flex justify-center items-center font-bold text-xs'>{`${format(
                        Number(data[0].matchDateTimeGMT) - 19800000,
                        'h:mm a'
                      )}`}
                      </div>
                      </div>}


                  </div>
                </div>
              </div>
            </div>
          </div>
        

        </div> : <div className="bg-gray border border-green m-2 rounded-md p-3 flex flex-col ">
       
          <div className='flex items-center justify-between'>
          {data && data[0] && data[0].matchScore.map((x, i) => (
            <>
             {i!==0&&<div className='flex text-center  items-center justify-center text-xs font-bold w-2/12 text-green border-green border p-1 rounded-xl bg-gray-8'>{data[0].matchType}</div>}
            <div key={i} className='flex items-center   justify-between py-2'>
              
              <div className='flex '>
                <imImageWithFallback height={18} width={18} loading='lazy'
                        fallbackSrc = {flagPlaceHolder}g
                  alt={x.teamShortName} 
                  src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                  // onError={(evt) => (evt.target.src = flagPlaceHolder)}
                  className='h-10 w-10 '
                />
                
                <span className='text-white font-semibold'> {x.teamShortName} </span>
              </div>
              
              
              {/* <div className='w-8/12 flex items-center tracked-tight '>
                {x.teamScore &&
                  x.teamScore.map((score, i) => (
                    <span key={i} className={`flex justify-center items-start f6 `}>

                      <span className={``}>
                        {score.runsScored}{' '}
                        {score.wickets && score.wickets !== '10' ? `/ ${score.wickets}` : null}

                      </span>

                      {((data[0].matchType === 'Test' &&
                        data[0].matchStatus !== 'completed' &&
                        score.inning == data[0].currentinningsNo) ||
                        data[0].matchType !== 'Test') && (
                          <div className='ml2'>{score.overs ? `(${score.overs})` : null}</div>
                        )}
                      {score.declared ? <span className='darkRed fw6 oswald f6 ml1'>d</span> : ''}
                      {score.folowOn ? <span className='darkRed fw6 f6 oswald ml1'> f/o</span> : ''}
                      {x.teamScore.length > 1 && i === 0 && <div className='mh2 fw5'>&</div>}
                    </span>
                  ))}
              </div> */}

            </div>
           
            </>
          ))}
          </div>
           <div className=' py-1 text-center'>
          {lang==='HIN'?<span className='text-white text-xs font-semibold truncate  '>{data && data[0] && (data[0].isLiveCriclyticsAvailable && data[0].statusMessageHindi ? data[0].statusMessageHindi : data[0].toss)}</span>:
            <span className='text-white text-xs font-semibold truncate '>{data && data[0] && (data[0].isLiveCriclyticsAvailable && data[0].statusMessage ? data[0].statusMessage : data[0].toss)}</span>}
          </div>
        </div> : <></>
        
        
        }
       
       

      {/* </div> */}
   
      {(componentType !== "BYTPlayerStathubState" && componentType !== "chooseCaptains" && componentType !== "viewTeamStatsHub") && 
      <div className='      mt-2  '>
        <div className=' flex white flex-row mt-2  items-center justify-center  '>
        
          <div
            className={tabname == 'players' ? activeTab : inActiveTab}
            onClick={() => {
              tabname !== 'players' ? handleNavTab('players') : null;
              setcomponentType('PlayerState')

            }}>
           {getLangText(lang,words,'PLAYERS')}
          </div>
          <div
            className={tabname == 'teams' ? activeTab : inActiveTab}
            onClick={() => {
              tabname !== 'teams' ? handleNavTab('teams') : null;
              setcomponentType('TeamState')
            }
            }>
            {lang==='HIN'?'टीमें':'TEAMS'}
          </div>
          <div
            className={tabname == 'stadium' ? activeTab : inActiveTab}
            onClick={() => {
              tabname !== 'stadium' ? handleNavTab('stadium') : null;
              setcomponentType('StadiumState')
            }}>
            {getLangText(lang,words,'STADIUM')}
          </div>
        </div>
      </div>}
{data && data[0]&&<>
      <div className='bg-basebg  mt-2  '>
        {tabname == 'players' && componentType === "PlayerState" && (
          <PlayerStatHub matchTypeData={props.matchTypeData} matchID={matchID} seriesID={seriesID} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus=  {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} changeComponentState={setComponentState} 
           showHideFRC={props.data && props.data.miniScoreCard &&props.data.miniScoreCard.data && props.data.miniScoreCard.data[0] && props.data.miniScoreCard.data[0] && props.data.miniScoreCard.data[0].isLiveCriclyticsAvailable?false:true}
            matchType={props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} playerScores={playerScores} teamList={listID} credit={credit} categoryPlayer={categoryPlayer} frcComposition_matchConfig={matchConfig} frcComposition_config={config} recentplayer={recentplayer} language={props.language}
          />
        )}
      </div>

      <div className='  ph2   mw75-l center-l   '>
        {tabname == 'teams' && componentType === "TeamState" && <StatHubTeam  matchTypeData={props.matchTypeData} matchID={matchID} seriesID={seriesID} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus=  {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType}
          matchType={props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} language={props.language}/>}
      </div>
      {tabname == 'stadium' && componentType === "StadiumState" && (
        <StatHubStadium  matchTypeData={props.matchTypeData} matchID={matchID} seriesID={seriesID} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus=  {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType}
          matchType={props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} language={props.language}/>
      )}
      <div className='  ph2   mw75-l center-l   '>
        {componentType === "BYTPlayerStathubState" && <div className="white"><PlayerStathubBYT  matchTypeData={props.matchTypeData} matchID={matchID} listID={listID}
          credit={credit} categoryPlayer={categoryPlayer} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus=  {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} changeComponentState={setComponentState} playerScores={playerScores} playerList={BYTList}
          frcComposition_matchConfig={matchConfig} frcComposition_config={config} awayTeamName={data[0].awayTeamShortName} homeTeamName={data[0].homeTeamShortName} ffCode={ffCode} recentplayer={recentplayer} setcaptain={setcaptain} setviceCaptain={setviceCaptain} captain={captain} viceCaptain={viceCaptain} language={props.language}
        /></div>}
      </div>
      <div className='    mw75-l center-l   '>
        {componentType === "chooseCaptains" && <PlayerStatsChooseCaptains  matchTypeData={props.matchTypeData} matchID={matchID}  ffCode={ffCode} viceCaptain={viceCaptain} captain={captain} teamName={teamName}
           categoryPlayer={categoryPlayer} changeComponentState={setComponentState} language={props.language} playerScores={playerScores} playerList={BYTList} />}
      </div>

      {/*  <div className=' ma3-l   mw75-l center-l   '>
        {componentType === "chooseCaptains" && <PlayerStatsChooseCaptains matchID={matchID}  ffCode={ffCode} viceCaptain={viceCaptain} captain={captain} teamName={teamName}
          credit={credit} categoryPlayer={categoryPlayer} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus=  {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} changeComponentState={setComponentState} playerScores={playerScores} playerList={BYTList} />}
      </div> */}
      <div className='    mw75-l center-l   '>
        {componentType === "viewTeamStatsHub" && <PlayerStatsHubViewTeam  matchTypeData={props.matchTypeData} matchID={matchID} listID={listID}
          credit={credit} categoryPlayer={categoryPlayer} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus={props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} changeComponentState={setViceCaptainCatainComponent} playerList={BYTList}
          frcComposition_matchConfig={matchConfig} language={props.language} frcComposition_config={config} ffCode={ffCode} />}
      </div>
      </>}
     {/* <PlayerStatsChooseCaptains matchID={matchID} teamList={listID} ffCode={ffCode} viceCaptain={viceCaptain} captain={captain} teamName={teamName}
          credit={credit} categoryPlayer={categoryPlayer} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus=  {props.data.miniScoreCard&& props.data.miniScoreCard.data[0]&&props.data.miniScoreCard.data[0].matchType} changeComponentState={setComponentState} playerScores={playerScores} playerList={BYTList} /> */}

    </div>

  );
}

