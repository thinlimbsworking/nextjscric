import React, { useState, useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import { GET_FRC_SELECTION_COMPOSITION,A23playerStatsByID } from '../../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { format } from 'date-fns';
import Countdown from 'react-countdown-now';
import StatHubTeam from './statHubTeam';
import StatHubStadium from './statHubStadium';

import PlayerStatHub from './playerStatsHubV3'
import PlayerStatsChooseCaptains from './playerStatsChooseCaptains'
import PlayerStatsHubViewTeam from './playerStatsHubViewTeam'
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import backIcon from '../../public/svgs/back_dark_black.svg'
import { words } from './../../constant/language'
const Main = '/pngs/ban1.png';
const Empty = '/svgs/rightarrow.svg';
// const backIconWhite = '/svgs/backIconWhite.svg';
const backIconWhite = '/pngs/arrow-left.png';
import CleverTap from 'clevertap-react';


export default function stathub(props) {

  console.log("444555",props)

  let lang=props.language?props.language:'ENG'
  let router = useRouter();
  const[matchId,matchSlug,pageCategory,stahubCategory]=router.query.slugs

  const createList = (team) => {
    let list = team && team.map((player,key) => {
      return player.playerId
    })
 
    return list.length > 0 ? list : []
  }

  const createCategoryPlayers = (team) => {

    let category = {
      AR: [...team.filter(pl => pl.player_role === "ALL_ROUNDER")],
      BAT: [...team.filter(pl => pl.player_role === "BATSMAN")],
      BOWL: [...team.filter(pl => pl.player_role === "BOWLER")],
      WK: [...team.filter(pl => pl.player_role === "KEEPER")]
    }

    return { ...category }
  }
  const getLangText = (lang,keys,key) => {
    
    let [english,hindi] = keys[key]
    switch(lang){
      case 'HIN':
        return hindi
      case 'ENG':
        return english
      default:
        return english
    }
      }


  const [categoryPlayer, setcategoryPlayer] = useState(props && props.urlData && props.urlData.team ? createCategoryPlayers(props.urlData.team) : { AR: [], BAT: [], BOWL: [], WK: [] })
  
  const [listID, setlistID] = useState(props && props.urlData && props.urlData.leagueType && props.urlData.leagueType === "statsHub" ? createList(props.urlData.team) : [])
  // console.log("outer listID",listID)
  const [recentplayer,setrecentplayer]=useState(null)






  
  const [matchID, matchName, screen, tabname] = router.query.slugs;



 


  return (
    <div className='mw75-l pb3 min-vh-100 bg-black center white  hidescoll  '>
     <div className='ph2 mw75-l center-l'>
      
          <PlayerStatHub 
          mainData={props.A23playerStatsByID}
          
          matchID={matchID}
        
           

            
             teamList={listID} 
            
             categoryPlayer={categoryPlayer} 
            
           
             recentplayer={recentplayer} 
             language={props.language}
          />
        
      </div>
  
    </div>

  );
  }