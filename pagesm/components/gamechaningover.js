import React, { useState } from 'react';
// import Winwiz from '../Criclytics/winwiz';
import Winwiz from './winwiz';
const information = '/svgs/information.svg';
const ground = '/svgs/groundImageWicket.png';
const down_arrow = '/svgs/down-arrow.png';
const up_arrow = '/svgs/up-arrow.png';
const flagPlaceHolder = '/svgs/images/flag_empty.svg';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GAME_CHANGING_OVERS } from '../api/queries';
import DataNotFound from './commom/datanotfound';
export default function GameChangingOvers(props) {
  const [index, setIndex] = useState(0);
  const [click, setClick] = useState(true);
  const [overno, setOver] = useState(0);
  const [showCommentary, setshowCommentary] = useState(false);
  const [len, setLen] = useState(false);
  const { loading, error, data } = useQuery(GAME_CHANGING_OVERS, {
    // variables: { matchID: props.matchID},
    variables: { matchID: props.matchID }
  });

  return data && data.gameChangingOvers && data.gameChangingOvers.overs && data.gameChangingOvers.overs.length>0 ? (
    <div className='bg-gray  lg:mx-2  md:mx-2 mt-5  px-3 pb-4 text-white rounded-xl'>
      <div className='font-semibold text-lg tracking-wide pt-2'>Game Changing Overs</div>
      

      <div className='text-gray-2 text-xs font-medium  mt-1'>OVER IMPACT</div>

      <div className='mb-2 rounded cursor-pointer py-3'>
        <Winwiz
          click={click}
          isAnimate={overno}
          pollData={data &&data.gameChangingOvers&&data.gameChangingOvers.overs &&data.gameChangingOvers.overs[index].lastBallPredictionData}
        />
      </div>

      <div className='bg-gray-4 p-3 rounded'>
        <div className='flex justify-between items-center text-gray-2 text-xs font-medium'>
          <div>Over {data.gameChangingOvers.overs[index].overNo}</div>
          <div className=''>
            {data.gameChangingOvers.overs[index].teamShortName}
            {' : '}
            {data.gameChangingOvers.overs[index].score}
          </div>
        </div>
        <div className='flex overflow-x-scroll text-black justify-between mt-2 font-semibold scrollmenu text-sm'>
          {data &&
            data.gameChangingOvers.overs[index].commentaryOver.map((ball, i) => (
              <div key={i} className='mx-1 '>
                {ball.wicket ? (
                  <div className='flex justify-center items-center bg-red rounded-full h-8 w-8 shadow-inner shadow-red'>
                    W
                  </div>
                ) : (
                  <div
                    className={`flex justify-center items-center rounded-full h-8 w-8 shadow-inner ${
                      ball.runs === '4' ? 'bg-green' : ball.runs === '6' ? 'bg-blue ' : 'bg-gray-2 '
                    } `}>
                    {ball.type === 'no ball'
                      ? 'nb'
                      : ball.type === 'leg bye'
                      ? 'lb'
                      : ball.type === 'bye'
                      ? 'b'
                      : ball.type === 'wide'
                      ? 'wd'
                      : ''}
                    {ball.type === 'no ball' || ball.type === 'leg bye' || ball.type === 'bye' || ball.type === 'wide'
                      ? ball.runs >= 1
                        ? ball.runs
                        : ''
                      : ball.runs}
                  </div>
                )}
                <div className=' text-gray-2 pt-2 text-xs text-center'>{ball.over}</div>
              </div>
            ))}
        </div>
      </div>
      <div className='flex justify-between items-center'>
        {data &&
          data.gameChangingOvers.overs.map((over, i) => (
            <div className='w-1/4  flex justify-center items-center'>
              <div
                key={i}
                className={`bg-gray-4 py-2 rounded-lg shadow-sm px-2 mt-3 text-center text-white w-full mx-1  ${
                  index === i ? 'border-2 border-green' : 'border-2 border-gray-4'
                }`}
                onClick={() => (setOver(over.overNo), setClick(true), setIndex(i))}>
                <div className='font-semibold'>{over.overNo}</div>
                <div className='font-medium text-xs text-gray-2 '>over</div>
              </div>
            </div>
          ))}
      </div>
      <div className='relative mt-4 flex justify-between mx-4 '>
        {data.gameChangingOvers.overs.map((over, i) => (
          <div className='bg-gray-1 z-10 '>
            {/* {i==0 && <div className='text-gray-2 text-xs font-medium'>INN {over.inningNo}</div>} */}
            {i >= 1 && data.gameChangingOvers.overs[i - 1].inningNo === over.inningNo ? (
              ''
            ) : (
              <div className='text-gray-2 text-xs px-1 font-medium'>INN {over.inningNo}</div>
            )}
          </div>
        ))}
        <div className={`bg-gray-2 h-[1px] w-full absolute bottom-2 `}></div>
      </div>
    </div>
  ) : (
    <div className='bg-gray  mx-2 mt-5 flex flex-col  px-3 pb-4 text-white rounded-xl hidden lg:block md:block'>
       <div className='font-semibold text-lg tracking-wide pt-2'>Game Changing Overs</div>

<div className='text-gray-2 text-xs font-medium mt-4'>OVER IMPACT</div>
      <DataNotFound />

    </div>
  );
}
