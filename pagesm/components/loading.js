import React from 'react';

// const logo = '/gif/logo.gif';

export default function Loading() {
  return (
    <div
      className={` flex justify-center items-center top-0 bottom-0 h-100 w-100 left-0 right-0 fixed absolute--fill z-9999 m-auto `}
      style={{ backgroundColor: 'rgba(0, 0, 0, 0.050)', backdropFilter: 'blur(3px)' }}>
    <img className='h-10 w-10'  src="/pngsV2/cricket.png" />
    </div>
  );
}
