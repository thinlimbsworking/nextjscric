import React from 'react';
const Empty = '/svgs/Empty.svg';

export default function FantasyTab(props) {
  return (
    <div>
      <div className='flex justify-center items-center mt4 mb2 w-100'>
        <img src={Empty} alt='' />
      </div>
      <div className='pb2 gray f6 fw5 tc'>Your one-stop shop for Fantasy insights.</div>
      <div className='pb4 gray f7 fw5 tc'>Coming soon...</div>
    </div>
  );
}
