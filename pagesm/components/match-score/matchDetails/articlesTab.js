import React from 'react';
import Loading from '../../commom/loading';
import ArticleCard from './articlecard';
import { ARTICLE_VIEW, News_Live_Tab_URL } from '../../../constant/Links';
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import Router from 'next/router';

const RightSchevronBlack = '/svgs/RightSchevronBlack.svg';

export default function ArticlesTab({ data, ...props }) {


  console.log("kkkkkk",props)

  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'ArticlesTab',
      ArticleType:
        article.type === 'match_report'
          ? 'MatchReport'
          : article.type === 'news'
          ? 'News'
          : article.type === 'fantasy'
          ? 'Fantasy'
          : article.type === 'interview'
          ? 'Interview'
          : 'Others',
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: localStorage ? localStorage.Platform : ''
    });
  };
  const getNewsUrl = (article) => {
    let articleId = article.articleID;
    let tab = 'latest';
    return {
      as: eval(ARTICLE_VIEW.as),
      href: ARTICLE_VIEW.href
    };
  };

  const navigateMatchDetailBlog = (article) => {
    ///news/live-blog/${matchId}/${matchName}/${articleID}
    let matchId = props.matchID;
    let name = `${props.match.homeTeamName ? props.match.homeTeamName : props.match.homeTeamShortName}-vs-${
      props.match.awayTeamName ? props.match.awayTeamName : props.match.awayTeamShortName
    }-${props.match.matchNumber ? props.match.matchNumber.split(' ').join('-') : ''}-`;

    let matchName =
      name.toLowerCase() +
      (props.match.seriesName
        ? props.match.seriesName
            .replace(/[^a-zA-Z0-9]+/g, ' ')
            .split(' ')
            .join('-')
            .toLowerCase()
        : '');
    let articleID = article.articleID;
    return {
      as: eval(News_Live_Tab_URL.as),
      href: News_Live_Tab_URL.href
    };
  };

  const newsHomeNav = () => {
    CleverTap.initialize('NewsHome', {
      Source: 'Homepage',
      Platform: global.window && global.window.localStorage.Platform
    });
    Router.push(`/news/[type]`, '/news/latest');
  };

  if (!data)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  if (data.length === 0) return <Loading />;
  else
    return data.getArticlesByIdAndType.length ? (
      <div>
        {data.getArticlesByIdAndType.map((article, i) => (
          <>
            {article.type != 'live-blogs' && article.type != 'live-blog' ? (
              (<Link {...getNewsUrl(article)} passHref>

                <div
                  key={i}
                  onClick={() => {
                    handleCleverTab(article);
                  }}>
                  <ArticleCard article={article} />
                </div>

              </Link>)
            ) : (
              (<Link {...navigateMatchDetailBlog(article)} passHref>

                <div
                  key={i}
                  onClick={() => {
                    handleCleverTab(article);
                  }}>
                  <ArticleCard article={article} />
                </div>

              </Link>)
            )}
          </>
        ))}
      </div>
    ) : (
      <div>
        <div className='bg-gray-4 text-xs font-semibold py-3 my-2 text-white tc'>
          <div>No articles available for this match.</div>
          <div>Do check out other stories.</div>
        </div>
        <div>
          <div className='flex justify-between items-center p-2.5 bg-gray text-white'>
            <div className='f7 fw6'>RECENT NEWS</div>
            <img className='cursor-pointer' alt={RightSchevronBlack} src={RightSchevronBlack} onClick={() => newsHomeNav()} />
          </div>
          <div className='h-[1px] bg-black w-full' />
          {data &&
            data.getArticleByPostitions &&
            data.getArticleByPostitions.map(
              (article, i) =>
                article && (
                  <>
                    {article.type != 'live-blogs' && article.type != 'live-blog' ? (
                      (<Link {...getNewsUrl(article)} passHref>

                        <div
                          key={i}
                          onClick={() => {
                            handleCleverTab(article);
                          }}>
                          <ArticleCard article={article} />
                        </div>

                      </Link>)
                    ) : (
                      (<Link {...navigateMatchDetailBlog(article)} passHref>

                        <div
                          key={i}
                          onClick={() => {
                            handleCleverTab(article);
                          }}>
                          <ArticleCard article={article} />
                        </div>

                      </Link>)
                    )}
                  </>
                )
            )}
        </div>
      </div>
    );
}
