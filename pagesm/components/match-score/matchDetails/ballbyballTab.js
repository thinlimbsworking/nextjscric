import React, { useState } from 'react'
import { BALL_BY_BALL } from '../../../api/queries'
import { useQuery } from '@apollo/react-hooks'
import BallByBallRenderer from './BallByBallRenderer'
export default function BallbyballTab(props) {

   const { matchID, matchData: { currentDay: day, currentSession: session, currentinningsNo: innings, matchType } } = props

   const [newDay, setNewDay] = useState(() => session === 1 ? day - 1 : day)
   const [newSession, setNewSession] = useState(() => session === 1 ? 3 : session - 1)
   const [newInnings, setNewInnings] = useState(innings)

   const { loading, error, data, fetchMore } = useQuery(BALL_BY_BALL, { variables: { matchID, day, session, matchType, innings }, })
   const handlePaginationTest = (fetchMore) => {

      fetchMore({

         variables: { matchID, day: newDay, session: newSession, innings: newInnings, matchType },

         updateQuery: (previousResult, { fetchMoreResult }) => {
            if (!fetchMoreResult) {
               return previousResult;
            }
            if (fetchMoreResult.getBallByBall[0].inningsSummary.length === 0) {
               let resultedBallByBall = Object.assign({}, previousResult = {
                  getBallByBall: [...previousResult.getBallByBall, ...fetchMoreResult.getBallByBall]
               });
               return resultedBallByBall
            }
            else {
               setNewInnings(prev => prev === "1" ? "1" : (prev - 1).toString())
               let resultedBallByBall = Object.assign({}, previousResult, {
                  getBallByBall: [...previousResult.getBallByBall, ...fetchMoreResult.getBallByBall]
               });
               return resultedBallByBall
            }

         },
      })
   }

   const handlePaginationODI = (fetchMore) => {

      if (newInnings !== 1) {
         fetchMore({

            variables: { matchID, matchType, day: 1, session: 1, innings: "1", },

            updateQuery: (previousResult, { fetchMoreResult }) => {
               if (!fetchMoreResult) {
                  return previousResult;
               }
               let resultedBallByBall = Object.assign({}, previousResult, {
                  getBallByBall: [...previousResult.getBallByBall, ...fetchMoreResult.getBallByBall]
               });

               return resultedBallByBall
            },
         })
      }
   }

   const handleScroll = (evt) => {

      if (evt.target.scrollTop + evt.target.clientHeight >= evt.target.scrollHeight) {

         if (matchType === "Test" && newDay !== 0) {
            if (newSession === 1) {
               setNewSession(3)
               setNewDay(prev => prev - 1)
               setNewInnings(prev => String(prev - 1))

               handlePaginationTest(fetchMore)
            }
            else {
               setNewSession(prev => prev - 1)
               setNewInnings(prev => String(prev - 1))

               handlePaginationTest(fetchMore)
            }
         }
         if (matchType !== "Test") {
            if (newInnings !== "1") {
               setNewInnings(prev => String(prev - 1))
               handlePaginationODI(fetchMore)
            }
         }
      }


   }

   if (error) return <div className="w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center"><span>Something isn't right!</span><span>Please refresh and try again...</span></div>
   if (loading || !data) return <div>loading...</div>
   else return (
      <div className=" mh2  max-vh-80" style={{ overflowY: "scroll" }} onScroll={evt => handleScroll(evt)}>

         {data.getBallByBall.map((ball, i) => <BallByBallRenderer key={i} inningsSummary={ball.inningsSummary} overs={ball.overs} />)}

      </div>
   )
}
