import React from 'react';

export default function Bowler_Dug(props) {
  const playerAvatar = '/placeHodlers/playerAvatar.png';

  const matchBowler = props.data && props.data[0];

  return (
    <div>
      {props.data && props.data && (
        <div className=' mt2 flex  flex-column  bg-gray-4 text-white  shadow-4 '>
          <div className='flex w-100 bb b--light-gray pa2 f7 fw5'>
            {props.status == 'mid' ? 'TOP BOWLER' : 'BOWLER'}{' '}
          </div>

          {props.data.map((item) => {
            return (
              <div className='w-100 flex pa2'>
                <div className='flex ma1  flex-column  justify-start  w-100   shadow-4  pv2'>
                  <div className='flex'>
                    <div className='w-30   relative'>
                      <div className=''>
                        <img
                          class='absolute cover z-0  '
                          width='100'
                          height='100'
                          alt='/svgs/newwhite.png'
                          src='/svgs/newwhite.svg'
                        />

                        <img
                          className=' h37 object-cover z-0 ph1  bw0'
                          alt=''
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          onError={(evt) => (evt.target.src = playerAvatar)}
                        />
                      </div>
                    </div>
                    <div className='w-70 flex flex-column items-center justify-center pa1'>
                      <div
                        className='w-100 items-center justify-center  white pa1 f6'
                        style={{ background: props.bgColor ? props.bgColor : 'red_10' }}>
                        {item.playerName}
                      </div>
                      <div className='flex w-100 ba b--silver pa1'>
                        <div className='w-40  br ml1 f7 fw5'>
                          {item.playerWicketsTaken}/{item.playerRunsConceeded} {'('}
                          {item.playerOversBowled}
                          {')'}
                        </div>
                        <div className='w-60 flex justify-center item-center f7 fw5'>
                          Eco Rate <b className='red_10 f7 fw7 ml1'>{item.playerEconomyRate}</b>
                        </div>
                      </div>

                      <div className='flex w-100 b--silver mt2'>
                        <div className='flex overflow-x-scroll z-1 hidescroll pt2 mb1 '>
                          {props.data &&
                            props.data[0] &&
                            matchBowler &&
                            matchBowler.over &&
                            matchBowler.over.map((x, i) => (
                              <div class='tc mr-2 '>
                                <React.Fragment key={i}>
                                  {x.isBall ? (
                                    x.wicket ? (
                                      <div
                                        key={i}
                                        className={`ba  mt1 flex justify-center items-center f6 fw6 black w2 h2 br-100 bg-red_10 white ${''}`}>
                                        W
                                      </div>
                                    ) : (
                                      x.runs && (
                                        <div
                                          key={i}
                                          className={`ba flex justify-center items-center f6 fw6 black w2 h2 br-100 white  ${''} ${
                                            x.runs === '6' ? 'bg-blue' : x.runs === '4' ? 'bg-green' : 'bg-blue_grey'
                                          } white f7 fw6 mv1`}>
                                          {x.type === 'no ball'
                                            ? 'nb'
                                            : x.type === 'leg bye'
                                            ? 'lb'
                                            : x.type === 'bye'
                                            ? 'b'
                                            : ''}
                                          {x.type === 'no ball' || x.type === 'leg bye' || x.type === 'bye'
                                            ? x.runs >= 1
                                              ? x.runs
                                              : ''
                                            : x.runs}
                                        </div>
                                      )
                                    )
                                  ) : (
                                    <div
                                      key={i}
                                      className={` bg-blue_grey   flex justify-center items-center f6 fw6 black w2 h2 br-100 white b--blue ${''} ${
                                        x.type == 'wide' ||
                                        x.type === 'no ball' ||
                                        x.type === 'leg bye' ||
                                        x.type === 'bye'
                                          ? 'mt1'
                                          : ''
                                      }`}>
                                      {x.type === 'no ball'
                                        ? 'nb'
                                        : x.type === 'leg bye'
                                        ? 'lb'
                                        : x.type === 'bye'
                                        ? 'b'
                                        : x.type === 'wide'
                                        ? 'wd'
                                        : ''}
                                      {x.type === 'no ball' ||
                                      x.type === 'leg bye' ||
                                      x.type === 'bye' ||
                                      x.type === 'wide'
                                        ? x.runs >= 1
                                          ? x.runs
                                          : ''
                                        : x.runs}
                                    </div>
                                  )}
                                  {x.isLastBall ? <div style={{ width: 2 }} className=' h2 bg-black-50' /> : null}
                                </React.Fragment>
                              </div>
                            ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}
