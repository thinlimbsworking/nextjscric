import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import * as d3 from 'd3';
import PieClass from './try';

export default function MAIn(props) {
  const ground = '/svgs/cricketGroundNew.svg';

  var z1SquarLeg = 0;
  var z2FineLeg = 0;
  var z3ThirdMan = 0;
  var z4Point = 0;
  var z5Cover = 0;
  var z6MidOff = 0;
  var z7MidOn = 0;
  var z8MidWicket = 0;

  // props.zad.map((item,index)=>{

  // console.log(item,"itemitemitem",props.NamePlayer,"zadindex",props.zad)
  // if(parseInt(item.zadval.split(',')[0])==1)
  // {

  // console.log("parseInt(item.zadval.split(',')[0])==1)",parseInt(item.zadval.split(',')[0]))
  //   // console.log( "item",item,"zone",(parseInt(item.zadval.split(',')[0])),"run",z1SquarLeg)
  //   z1SquarLeg=z1SquarLeg+parseInt(item.zadval.split(',')[1])
  // }
  // if(parseInt(item.zadval.split(',')[0])==2)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // if(parseInt(item.zadval.split(',')[0])==3)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // if(parseInt(item.zadval.split(',')[0])==4)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // if(parseInt(item.zadval.split(',')[0])==5)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // if(parseInt(item.zadval.split(',')[0])==6)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // if(parseInt(item.zadval.split(',')[0])==7)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // if(parseInt(item.zadval.split(',')[0])==8)
  // {
  //   // console.log(parseInt(item.zadval.split(',')[0]))
  // }

  // }

  // )

  console.log("",props)

  const generateData = (value, length = 5) =>
    d3.range(8).map((item, index) => ({
      date: index,
      value: 20
    }));

  const [data, setData] = useState(generateData(0));
  const changeData = () => {
    setData(generateData());
  };

  useEffect(() => {
    setData(generateData());
  }, [!data]);
  const getZadCo = (ball) => {
    if (props.battingStyle && props.battingStyle === 'LHB') {
      return 0 - (180 - parseInt(ball.zadval.split(',')[1]));
    }
    return 0 - ball.zadval.split(',')[1];
  };

  var isSafari = window && window['safari'] ? true : false;

  // Chrome 1 - 71
  var isChrome = window && window.chrome ? true : false;

  // {`absolute w77 h77  z-2   flex items-center justify-center   bw-01 ${isChrome?'nt1':''}`}
  return (
    <div className='flex items-center justify-center '>
      <div
        className={`absolute w55 h55 z-2  z-2   flex items-center justify-center  bw-01   bw-01 ${
          isChrome ? 'nt1' : ''
        } `}>
        <img className='w55 h55 absolute' src={ground} alt='' />

        <div
          className='flex h02 z-2 ba b--white w02 items-center justify-center absolute bg-red  bb-white  br-100'
          style={{
            bottom: '58%',
            left: '49%',
            right: '0%',

            transform: `rotate(0deg)`,
            transformOrigin: '0 0'
          }}></div>
        <div
          className='w55 h55 absolute 
                   overflow-hidden br-100 ba wheel bw-01'>
          {props.zad.map((item) => {
            return (
              <div
                className={`bb  absolute ${
                  item.runs === '0'
                    ? 'dn'
                    : (item.runs === '6' && 'b--red w5') ||
                      (item.runs === '1' && 'b--white w13') ||
                      (item.runs === '2' && 'b--blue w2') ||
                      (item.runs === '3' && 'b--purple w2-4') ||
                      (item.runs === '5' && 'b--green') ||
                      (item.runs === '4' && 'b--yellow')
                } `}
                style={{
                  bottom: '58%',
                  left: '53%',
                  right: '0%',

                  transform: `rotate(${getZadCo(item)}deg)`,
                  transformOrigin: '0 0'
                }}></div>
            );
          })}
        </div>
      </div>
      <div className='relative'>
        {/* height 5.5 rem */}
        <PieClass
          data={data}
          zoneRun={[z2FineLeg, z1SquarLeg, z8MidWicket, z7MidOn, z6MidOff, z5Cover, z4Point, z3ThirdMan]}
          width={120}
          height={120}
          battingStyle={props.battingStyle}
          zadData={props.zad}
          innerRadius={43}
          outerRadius={60}
        />
      </div>
    </div>
  );
}
