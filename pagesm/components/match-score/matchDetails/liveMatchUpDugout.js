import React from 'react';

export default function Bowler_Dug(props) {
  return (
    <div>
      <div className=' mt2 flex  flex-column  bg-gray-4  shadow-4 '>
        <div className='flex w-100 bb b--black pa2 f7 fw5 '> LIVE MATCHUP</div>

        <div className='w-100 flex pa2'>
          <div className='flex flex-column  justify-start  w-100   shadow-4  pv2'>
            <div className='flex w-100 items-center justify-center bb b--black pa1'>
              <div className='w-40 f7  pa1 fw6 lh-copy  '> {props.bowlerName} </div>
              <div className='w-25 text-xs tc pa1 font-medium br bl b--black'>Balls Faced </div>

              <div className='w-25 text-xs tc pa1 font-medium br   b--black'>Runs Scored </div>

              <div className='w-25 text-xs tc pa1 font-medium   b--black'>Boundaries </div>
            </div>

            {props.data &&
              props.data.map((item, index) => {
                return (
                  <div className='flex w-100 items-center justify-center pa1'>
                    <div className='w-40  pa1 text-xs font-semibold lh-copy'>Vs {item.playerName}</div>
                    <div className='w-25 text-xs tc  pa1 font-semibold oswald br bl b--black'>{item.ballfaced} </div>

                    <div className='w-25 text-xs tc pa1 font-semibold br oswald  b--black'>{item.runsscored} </div>

                    <div className='w-25 text-xs tc pa1 font-semibold oswald  b--black'>{item.boundaries}</div>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
}
