import React, { useState } from 'react';
import MiniScoreCard from '../matchDetails/miniScoreCard';
import Link from 'next/link';
import { scheduleMatchView } from '../../../api/services';
import CleverTap from 'clevertap-react';
import format from 'date-fns/format';
import MetaDescriptor from '../../MetaDescriptor';
import { Schedule } from '../../../constant/MetaDescriptions';
import { useRouter } from 'next/router';
const MatchLayout = ({ status, browser, routerPath, ...props }) => {
  const [tab, setTab] = useState(status|| props.status);
  let router = useRouter();
  const tabs = [
    'Live',
    'Summary',
    // 'Playing11',
    // 'probablePlaying11',
    'Commentary',
    'Scorecard',
    // 'Dugout',
    // 'Highlights',
    'MatchInfo',
    'Articles',
    // 'Videos'
  ];

  const completedTabs = [
    'Summary',
    'Commentary',
    // 'Dugout',
    'Scorecard',
    // 'Highlights',
    // 'MatchInfo',
    'Articles',
    // 'Videos'
  ];
  

  const liveTabs = ['Commentary', 'Scorecard',  'Articles', 'MatchInfo', ];

  const scheduledTabs = ['Commentary', 'Scorecard', 'Highlights', 'Articles', 'MatchInfo', 'Videos'];
  const abondonedTabs = ['Commentary', 'Scorecard', 'Articles', 'Videos', 'MatchInfo'];
  const CancelledTabs = ['Commentary', 'Articles', 'Videos', 'MatchInfo'];

  const { error, data } = { error: null, data: props.data };
  const handleNavigation = (tabName) => {
    CleverTap.initialize(`${tabName}Tab`, {
      source: tabName == 'Dugout' ? 'Dugout' : tabName === 'Commentary' || tabName === 'Summary' ? 'Homepage' : '',
      MatchID: data.miniScoreCard.data[0].matchID,
      SeriesID: data.miniScoreCard.data[0].seriesID,
      TeamAID: data.miniScoreCard.data[0].matchScore[0].teamID,
      TeamBID: data.miniScoreCard.data[0].matchScore[1].teamID,
      MatchFormat: data.miniScoreCard.data[0].matchType,
      MatchStartTime: format(Number(data.miniScoreCard.data[0].startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: data.miniScoreCard.data[0].matchStatus,
      Platform: global.window.localStorage.Platform
    });
  };
  
  return <>
    <MetaDescriptor section={Schedule(data.miniScoreCard.data[0], data.miniScoreCard.liveScoreUrl, router.asPath, status)} />

    {error ? (
      <div></div>
    ) : (
      <div className='w-full mx-auto'>
        <MiniScoreCard data={data} browser={browser} />
        
        {/* {alert( data.miniScoreCard.data[0].matchType)} */}
        <div className='bg-basebg pb-1'>
          <div className='flex overflow-x-scroll z-999 hidescroll pt-2 text-white font-semibold text-xs bg-basebg pb-1 mb-2'>
            {tabs
              .filter((tab) =>
                data.miniScoreCard.data[0].isAbandoned
                  ? abondonedTabs.includes(tab)
                  : data.miniScoreCard.data[0].matchResult === 'Match Cancelled'
                  ? CancelledTabs.includes(tab)
                  : data.miniScoreCard.data[0].matchStatus === 'completed'
                  ? completedTabs.includes(tab)
                  : data.miniScoreCard.data[0].matchStatus === 'upcoming'
                  ? scheduledTabs.includes(tab)
                  : liveTabs.includes(tab)
              )
              .map((tabName, i) => {
                return <>
                  {(tabName != 'Dugout'
                    ? true
                    : (data.miniScoreCard.data[0].matchType == 'ODI' ||
                        data.miniScoreCard.data[0].matchType == 'T20' ||
                        data.miniScoreCard.data[0].matchType == 'T100') &&
                      data.miniScoreCard.isDisplayDugout &&
                      data.miniScoreCard.data[0].isLiveCriclyticsAvailable) && (
                    (<Link
                      key={i}
                      {...scheduleMatchView(data.miniScoreCard.data[0], tabName)}
                      replace
                      passHref
                      title={tabName}
                      onClick={() => {
                        setTab(tabName);
                        // handleNavigation(tabName);
                      }}
                      className={`${tabName == 'Dugout' ? 'hidden' : ''} cursor-pointer p-2 border-b-2 border-gray-2 text-gray-2  w-full  text-center ${
                        tabName.toLowerCase() ===  tab.toLowerCase() ? 'border-blue-9 border-b-2 text-blue-9' : ''
                      }`}>

                      <div className=''> {tabName === 'probablePlaying11' ? 'Probable Playing11' : tabName}</div>

                    </Link>)
                  )}
                </>;
              })}
          </div>
         
          {props.children}
        </div>
      </div>
    )}
  </>;
};

export default MatchLayout;
