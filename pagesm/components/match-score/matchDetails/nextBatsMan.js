import React from 'react';

export default function NextBatsMan(props) {
  const playerAvatar = '/pngsV2/playerPH.png';

  return (
    <div className='w-100 mt2 flex  flex-column  bg-gray-4  shadow-4  pa1 overflow-x-scroll text-white' >
      <div className='flex w-100 bb b--black pa2 f7 fw5 ttu'>
        <div className='w-50 ttu'>Next BATTER</div>
      </div>

      <div className='w-100'>
       
          <div className='flex'>
            <div className='overflow-x-scroll hidescroll p-2'>
              <div className='flex'>
                {props.data &&
                  props.data.map((item) => (
                    <div className='flex justify-center  items-center flex-column ma1 p-2 cursor-pointer   shadow-4'>
                      <div className='w4    flex justify-center '>
                        <img
                          className='w3 h3 cursor-pointer br-100 overflow-hidden object-top object-cover border-black   ba'
                          src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                          onError={(evt) => (evt.target.src = playerAvatar)}
                        />
                      </div>
                      <div className='w-100 tc text-xs mt2 font-semibold tc'>{item.playerName}</div>

                      <div className='flex w-100 items-center justify-center oswald text-sm mt2 fw6  tc text-gray-2'>
                        <div className='flex w-50 items-center justify-center'> Avg</div>
                        <div className='flex w-50 items-center justify-center'> SR</div>
                      </div>

                      <div className='flex w-100 items-center justify-center text-sm oswald  fw5 pa1 tc '>
                        <div className='flex w-50 items-center justify-center fw5 tc'>
                          {' '}
                          {item.playerMatchAvg || '-'}
                        </div>
                        <div className='flex w-50 items-center justify-center fw5 tc'>
                          {item.playerMatchStrikeRate || '-'}
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            </div>
        </div>
      </div>
    </div>
  );
}
