import React from 'react';
const playerAvatar = '/pngsV2/playerPH.png';

export default function NextBatsMan(props) {
  return (
    <div className=' w-100 mt1 flex   flex-column   hidescroll  pa1 overflow-y-scroll shadow-4 bg-gray-4 text-white '>
      <div className='w-100 flex '>
        <div className='flex ma1  flex-column  justify-start  w-100    '>
          <div className='flex'>
            <div className='overflow-x-scroll  hidescroll ph3-l ph2'>
              <div className='flex w-100'>
                {props.swipeData.map((item, index) => {
                  return (
                    item.playerID == props.idKey && (
                      <div
                        onClick={() => props.setPopWHeelIndex(index)}
                        className='flex ba  justify-end items-center flex-column ma2  cursor-pointer   shadow-5'
                        style={
                          props.teamColor == ''
                            ? item.playerID == props.idKey
                              ? { border: '1px solid   #B70000' }
                              : { borderColor: 'black' }
                            : item.playerID === props.idKey
                            ? { borderColor: ' #B70000' }
                            : { border: '1px solid ' }
                        }>
                        <div className='w44  h37 items-center  flex justify-center '>
                          <img
                            className='w33  mv2 h33 br-100 ba bg-gray b--black object-cover object-top bottom-0 '
                            src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = playerAvatar)}
                          />
                        </div>
                        <div className='flex items-center justify-center f7 mb1 fw5 pa1 tc'>{item.playerName}</div>

                        <div
                          className=' h2-3  white flex w-100 items-center justify-center f5 oswald  fw5  tc  '
                          style={
                            props.teamColor == ''
                              ? item.playerID === props.idKey
                                ? { background: ' #B70000' }
                                : { background: '#2B323F', color: '', borderTop: '0.5px solid  #B70000' }
                              : item.playerID === props.idKey
                              ? { background: ' #B70000' }
                              : { background: '#2B323F', color: ' ', borderTop: '0.5px solid  #B70000' }
                          }>
                          <div className='flex  items-center justify-center  tc f5 oswald'>
                            {' '}
                            {item.playerMatchRuns}
                            <span className='f7 ml1 white-50'>
                              {item.playerMatchBalls} {item.isNotOut && '*'}{' '}
                            </span>{' '}
                          </div>
                        </div>
                      </div>
                    )
                  );
                })}

                {props.swipeData.map((item, index) => {
                  return (
                    item.playerID !== props.idKey && (
                      <div
                        onClick={() => props.setPopWHeelIndex(index)}
                        className='flex ba  justify-end items-center flex-column ma2  cursor-pointer   shadow-5'
                        style={
                          props.teamColor == ''
                            ? item.playerID == props.idKey
                              ? { border: '1px solid  #B70000' }
                              : { borderColor: 'lightgray' }
                            : item.playerID === props.idKey
                            ? { borderColor: '#B70000' }
                            : { border: '1px solid rgb(0 0 0 / 50%)' }
                        }>
                        <div className='w44  h37 items-center  flex justify-center '>
                          <img
                            className='w33  mv2 h33 br-100 ba bg-gray b--gray object-top object-cover bottom-0 '
                            src={`https://images.cricket.com/players/${item.playerID}_headshot_safari.png`}
                            onError={(evt) => (evt.target.src = playerAvatar)}
                          />
                        </div>
                        <div className='flex items-center justify-center f7 mb1 fw5 pa1 tc'>{item.playerName}</div>

                        <div
                          className=' h2-3  white flex w-100 items-center justify-center f5 oswald  fw5  tc  '
                          style={
                            props.teamColor == ''
                              ? item.playerID === props.idKey
                                ? { background: '#B70000' }
                                : { background: '#2B323F', color: '', borderTop: '0.5px solid black' }
                              : item.playerID === props.idKey
                              ? { background: '#B70000' }
                              : { background: '#2B323F', color: ' ', borderTop: '0.5px solid rgb(0 0 0 / 50%' }
                          }>
                          <div className='flex  items-center justify-center  tc red_10 f5 oswald'>
                            {' '}
                            {item.playerMatchRuns}
                            <span className='f7 ml1 '>
                              {item.isNotOut && '*'} {item.playerMatchBalls}{' '}
                            </span>{' '}
                          </div>
                        </div>
                      </div>
                    )
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

// <div
//  className=' flex ba justify-center items-center flex-column ma2  cursor-pointer  w37  shadow-4'
// >
//  <div className='    flex justify-center '>

//   <div className=" mt2 pa1">

//       <img
//      className='w3 h3 cursor-pointer br-100 overflow-hidden object-top  bg-gray   '
//      src="https://images.cricket.com/players/4276_headshot_safari.png"
//    /></div>

//  </div>
//  <div className='flex items-center justify-center f7 mt2 fw6 pa1 tc'>
//  Jofra  Archer
//  </div>

//       <div className='ba h2-3 bg-red_10 white flex w-100 items-center justify-center f5 oswald  fw5 pa1 tc black'>
//  <div className="flex  items-center justify-center  tc"> 53.50</div>

//       </div>

// </div>
