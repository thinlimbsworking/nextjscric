import React from "react";
import * as d3 from "d3";

const Arc = ({ data, index, createArc, colors, format ,Runs,zadData,battingStyle}) => (
  <g key={index} className="arc">
    <path className="arc" d={createArc(data)} fill={'#eabf69'} />
    <text
      transform={`translate(${createArc.centroid(data)})`}
      textAnchor="middle"
      alignmentBaseline="middle"
      fill="black"
      fontSize="9"
    >
      {getScores(index,zadData,battingStyle)}
    </text>
  </g>
);


const getScores = (index,zadData,battingStyle) => {
  let indexMap = [2, 1, 8, 7, 6, 5, 4, 3];
  if(battingStyle === "LHB") {
    indexMap = [3,4,5,6,7,8,1,2];
  }
  // console.log(data.filter(dt => dt.zadval.split(“,”)[0] == indexMap[index]),data.filter(dt => dt.zadval.split(“,”)[0] == 8).map(x => Number(x.runs)).reduce((a,x) => a+x,0));
  const zoneTotal = zadData.filter(dt => dt.zadval.split(",")[0] == indexMap[index]).map(x => Number(x.runs)).reduce((a,x) => a+x,0)
  return `${zoneTotal}`
}

const Pie = props => {

 

  // console.log()
  const createPie = d3
    .pie()
    .value(d => d.value)
    .sort(null);
  const createArc = d3
    .arc()
    .innerRadius(props.innerRadius)
    .outerRadius(props.outerRadius).padAngle(.015);
  const colors = d3.scaleOrdinal(d3.schemeCategory10);
  const format = d3.format(".4f");
  const data = createPie(props.data);

  return (
    <svg width={props.width} height={props.height}>
      <g transform={`translate(${props.outerRadius} ${props.outerRadius})`}>
        {data.map((d, i) => (
          <Arc
            key={i}
            zadData={props.zadData}
            battingStyle={props.battingStyle}
            data={d}
            index={i}
            createArc={createArc}
            colors={colors}
        
          />
        ))}
      </g>
    </svg>
  );
};

export default Pie;
