import React from 'react';
import { STADIUM_VIEW, PLAYER_VIEW, SERIES_DETAILS } from '../../../constant/Links';
import Link from 'next/link';

export default function VenuDeatils(props) {
    console.log("matchInfomatchInfo",props.data.venueID)
  const Pitch = '/pngsV2/keystatpitch.png';
  const Location = '/pngsV2/statlocation.png';
  const getStadiumUrl = (matchInfo) => {
     
    let stadiumId = matchInfo.venueID;
    let stadiumSlug = matchInfo.venue
      .replace(/[^a-zA-Z0-9]+/g, ' ')
      .split(' ')
      .join('-')
      .toLowerCase();
    return {
      as: eval(STADIUM_VIEW.as),
      href: STADIUM_VIEW.href
    };
  };
  let matchInfo={
      venueID:props.data.venueID,
      venue:props.data.venue

  }
  console.log("matchInfomatchInfomatchInfomatchInfo",matchInfo)
  return (
    <div className=''>
     
     
      <div className='flex flex-col items-start   m-3 mt-4'>
        <div className=' flex items-center justify-between w-full'>
        <div className='text-white text-base  font-bold'>Venue Details
        <br />
        <div className='h-1  w-12 bg-blue-8 mt-1' />        </div>
        <Link {...getStadiumUrl(matchInfo)} passHref legacyBehavior> 
        <div className='text-white bg-gray rounded p-1 mt-2'>
         <img className='h-8 w-8' src="/pngsV2/arrow.png" alt="" /> 
        </div>
        </Link>
        </div>
      

        <div className='text-white text-xs flex items-center justify-center mt-1 font-medium'>
          {' '}
          <img className='h-8 w-8 mr-2' src={Location} alt='' />{' '}
          {props.venueName}
        </div>
      </div>

      <div className=' lg:flex md:flex '>
        <div className='flex flex-col lg:w-6/12 md:w-6/12'>
        <div className='flex  items-center  justify-start p-2 mx-1'>
          <div className='w-6/12  bg-gray white mx-[1px] p-3 rounded-l-lg'>
            <div className='text-[10px] font-medium text-gray-2'> OVERALL</div>
            <div className='text-xs text-white font-bold pt-1'>
          
              {props.venueStatsData.overall} 
            </div>
          </div>
          <div className='w-6/12  bg-gray white mx-[1px] p-3 rounded-r-lg'>
            <div className='text-[10px] font-medium text-gray-2 uppercase'> Best suited for</div>
            <div className='text-xs text-white font-bold pt-1'>
              {' '}
              {props.venueStatsData.bestSuited  || 'NA'}
            </div>
          </div>
        </div>

        <img className='flex  -mt-5 h-48 lg:h-24 md:h-24 w-full' src={Pitch} />
        </div>

        <div className='-mt-6 lg:w-6/12 md:w-6/12 lg:mt-6 md:mt-2'>
          <div className='bg-gray mx-3 p-3 rounded-lg '>
            <div className='flex justify-between'>
              <div className='w-6/12 pl-2'>
                <div className='flex text-gray-2 text-[10px] font-semibold'> 1ST BATTING AVG. SCORE</div>
                <div className='flex text-white text-xs font-bold mt-1  '>
                  {props.venueStatsData.avgFirstInningScore}
                </div>
              </div>
              <div className='w-6/12 pl-2 '>
                <div className='flex text-gray-2 text-[10px] font-semibold'> HIGHEST SCORE CHASED</div>
                <div className='flex text-white text-xs font-bold mt-1 '>
                  {props.venueStatsData.highestScoreChased}
                </div>
              </div>
            </div>
            <div className='pl-2 mt-5'>
              <div className='flex text-gray-2 text-[10px] font-semibold'> WICKET SPLIT</div>
              <div className=' flex justify-between '>
                <div className='flex text-white text-xs font-bold mt-1 w-6/12 '>
                  <p className='text-gray-2 pr-1 font-medium'> PACE</p>{' '}
                  {props.venueStatsData.paceWicketPercent}%
                </div>
                <div className='flex text-white text-xs font-bold mt-1 w-6/12'>
                  <p className='text-gray-2 pr-1 font-medium'> SPIN</p>{' '}
                  {props.venueStatsData.spinWicketPercent}%
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
