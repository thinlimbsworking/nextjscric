import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import ArticleCard from '../ArticleCard';
import axios from 'axios'
import { useQuery } from '@apollo/react-hooks';
import { AUTHOR_DATA,AUTHOR_DATA2 } from '../../api/queries';
import { NEWS } from '../../constant/MetaDescriptions';
import MetaDescriptor from '../MetaDescriptor';
import { getNewsUrl, getArticleTabUrl, getLiveArticleUrl } from '../../api/services';
import { useRouter } from 'next/router';

const NewsDetailsLayout = (props) => {


let router = useRouter();

let authorName=router.query.slugs[1].replace("-", "_")



  // 2021-07-01
  const monthsArr = [{month:'JAN',date:'-01-31',key:0}, {month:'FEB',date:'-02-28',key:1}, {month:'MAR',date:'-03-31',key:2}, {month:'APR',date:'-04-30',key:3}, {month:'MAY',date:'-05-31',key:4}, {month:'JUN',date:'-06-30',key:5}, {month:'JUL',date:'-07-31',key:6}, {month:'AUG',date:'-08-31',key:7}, {month:'SEP',date:'-09-30',key:8}, {month:'OCT',date:'-10-31',key:9}, {month:'NOV',date:'-11-30',key:10}, {month:'DEC',date:'-12-31',key:11}];
  const [showYears, setShowYears] = useState(false);
  const[dateChange,setDateChange]=useState(false)
  const [selectYear, setSelectYear] = useState(new Date().getFullYear());
  const [selectMonth, setSelectMonth] = useState(monthsArr[parseInt(new Date().getMonth())]);
  const[apiMonth,setApiMonth]=useState("")
  const[apiYear,setApiYear]=useState("")

  const[typeName,setTypeName]=useState({title:"Type",value:''})
const [showTypes,setShowTypes]=useState(false)
  let tab = 'new';

  const [pagination, setPagination] = useState(1);
  const [windows, updateWindows] = useState();
  useEffect(() => {
    if (global.window) updateWindows(global.window);3

    return()=>{
      document.body.style.overflow = 'unset';
    }
  }, [global.window]);

  let { loading, error, data, fetchMore } = useQuery(AUTHOR_DATA, {
    variables: { authorID: authorName, page: 0 ,type:typeName.value,fromDate:dateChange? `${apiYear}${apiMonth.date}`:''},
  
    fetchPolicy:'no-cache'
  }
  // return() document.body.style.overflow = 'unset';
 
  
 

  );
  // const  aaa=async()=>{
  //   alert(6)
  // let ata2  =await  axios.post(process.env.API, {
  //   query: AUTHOR_DATA2,
  //   variables: { authorID: authorName, page: 0 ,type:typeName.value,fromDate:dateChange? `${apiYear}${apiMonth.date}`:''},
  // })

  // console.log("ata2ata2ata2ata2ata2",ata2)
  // }
  if (loading) {return <div></div>  }

  const handleScroll = (evt) => {
    if (evt.target.scrollTop + evt.target.clientHeight >= 0.9 * evt.target.scrollHeight) {
      setPagination((prevPage) => prevPage + 1);

      handlePagination(fetchMore);
    }
  };
  const handlePagination = (fetchMore) => {
    fetchMore({
      variables: {  authorID: authorName, page: pagination },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult;
        }
        let resultedArticle = Object.assign({}, previousResult, {
          getAuthorsArticles: [...previousResult.getAuthorsArticles, ...fetchMoreResult.getAuthorsArticles]
        });
        return { ...resultedArticle };
      }
    });
  };

  const typeArr=[
    {title: 'News', value: 'news'},
    {title: 'Match Related', value: 'matchrelated'},
    {title: 'Features', value: 'features'},
    {title: 'On This Day', value: 'on_this_day'},
  ]
  if(data){

  

  const stopBackGroundScroll=()=>{

    document.body.style.overflow = 'hidden';
  }
  const onBackGroundScroll=()=>{
    document.body.style.overflow = 'unset';
  }



  
  var  authorData= data && data.getAuthorsArticles&& data.getAuthorsArticles.author
  return <>
    <MetaDescriptor section={NEWS[tab]} />
  
    <div className='lg:max-w-5xl center lg:pt-1'>
      <div className='bg-navy z-1 px-2 h-2  lg:hidden md:hidden flex items-center'>
        {/* onClick={() =>router.push('/fantasy-research-center/[...slugs]', `${matchID}/${matchName}`)} */}
        <div className='w-5' onClick={() => window.history.back()}>
          <img className='w-1 lg:hidden' alt='' src='/svgs/backIconWhite.svg' />
        </div>
        <div className='white text-sm fw7 pl3 '>Authors</div>
      </div>
{ authorData  ?<>
      <div className='w-full flex bg-white pa3-l'>
        <div className='w-25 flex  items justify-center  pa2'>
          <img
            className='br-100 h33 w33 object-cover '
            src={authorData && authorData.pic||'https://devcdc.com/svgs/Man-of-the-match.svg'}
            alt=''
          />
        </div>

        <div className='w-75  flex-col items-start justify-start p-2'>
          <div className='text-base font-medium w-full flex'>{authorData && authorData.userName} </div>
          <div className='text-sm font-medium w-full flex mt-1'> {authorData && authorData.twtID &&<img src="/pngs/tweet.png" alt="" />}{" "} <span className="ml-1 gray">{authorData && authorData.twtID}</span></div>
          <div className='text-xs gray'>
          {authorData && authorData.bio}
          </div>

          <div className='flex w-full mt-2 items-start justify-start'>
            <div className='w-28 lg:w-20 mx-1'>
              <div className={`border-solid border-2 p-1 flex items-center justify-center rounded-full   w-full h-12
               ${(apiMonth=="" || apiYear=="")?'':'border-red'} `}>
                <span className={`text-xs font-medium ml-1 flex items-center justify-center  cursor-pointer `}
                 onClick={() =>( setShowTypes(false),setShowYears(true),stopBackGroundScroll())}>
                {(apiMonth=="" || apiYear=="")?'MM/YY':`${apiMonth.month}/${apiYear}`  }
            
                 <img className='mx-1 ' src={`${(apiMonth=="" || apiYear=="")?'/svgs/calander.svg':'/svgs/redcalander.svg'}`}
                
                 alt='' />
                </span>
              </div>

              {showYears && (
                <div className='w-48 lg:w-14 z-2 p-1 flex items-start justify-start flex-col  fixed bg-white shadow-5'>
                  <div className='flex text-xs gray py-1'> Years</div>
                  <div className='flex w-full flex-wrap items-center justify-center'>
                    {[new Date().getFullYear(), new Date().getFullYear()-1, new Date().getFullYear()-2].map((item) => {
                      return (
                        <div
                        onClick={()=>( (item==new Date().getFullYear()&&selectMonth.key>new Date().getMonth() )   ?(setSelectMonth(monthsArr[new Date().getMonth()]), setSelectYear(item)):setSelectYear(item))}


                        // item==new Date().getFullYear()&&selectMonth.month==  )?(     setSelectYear(item)):null)}
                          className={`cursor-pointer text-sm font-medium w-24 border-solid border-2 mx-1 h-1 black rounded-full bg-white flex items-center justify-center 
${selectYear == item ? 'border-red' : 'border-white'}
`}>
                          {item}
                        </div>
                      );
                    })}
                  </div>

                  <div className='flex text-xs gray py-1'> Month 
</div>
                  <div className='flex w-full flex-wrap items-center justify-center'>
                    
                    {monthsArr.map((item,index) => {
                      return (
                        <div

                        onClick={()=>( ( selectYear==new Date().getFullYear()&& index>new Date().getMonth()) 
                            ?null: setSelectMonth(item))}
                       
                        // onClick={()=>
                          
                          
                        //   setSelectMonth(item)}
                          className={`cursor-pointer text-xs font-medium w-24 border-solid border-2 m-1 h-1 rounded-full black bg-near-white flex items-center justify-center
                          ${( selectYear==new Date().getFullYear()&& index>new Date().getMonth())
                            ?'o-50': ''}
                          
                          
                          ${
                            selectMonth.month == item.month ? 'border-red' : 'border-white'
                          } `}>
                          {item.month}   
                        </div>
                      );
                    })}
                  </div>

                  <div className="flex w-full items-center justify-center py-2 cursor-pointer"
                   onClick={()=> (onBackGroundScroll(),setDateChange(true),setApiMonth(selectMonth),setApiYear(selectYear),setShowYears(false))}>
                      <div className=" w-30 white  flex items-center justify-center bg-darkRed text-xs p-1 rounded-full white">Done</div></div>
                </div>
              )}
            </div>
           <div className='w-44 mx-1  lg:w-20 relative'  >
           
              <div
              onClick={()=>(setShowTypes(true),setShowYears(false))}
              className={`${typeName.title!="Type"?'darkRed ba b--darkRed  w-full  w-full-l':'ba w-70  w-full-l  '}  pa1 cursor-pointer flex items-center justify-center br-pill    h13`}
               >
                <span className={`text-sm font-semibold ml-1 flex items-center justify-center `}>
                {typeName.title} <img className='mx-1 h-6 ' src='/svgs/downArrow.svg' alt='' />
                </span>
              </div>


              {showTypes && (
                <div className='w-full  border-black br-2 -mt-4 z-2 py-1 flex items-start 
                justify-start flex-col  absolute bg-white shadow-5'>
                  <div className={`w-full  text-xs  p-1 flex items-center cursor-pointer  justify-center  `} 
                    onClick={()=> (setTypeName({title:"Type",value:''}),setShowTypes(false))}
                  > Type <img className='mx-1 h-6 ' src='/svgs/downArrow.svg' alt='' /></div>
                  
                  {typeArr.map((item)=>{return(<div className={`w-full flex flex-col text-xs   text-center  } 
                   p-1 border-silver black font-medium flex items-center justify-center`} 
                  
                  onClick={()=> (setTypeName(item),setShowTypes(false))}>
                    <div className="pb-1">  {item.title}</div>
                   
                    {item.title!=="On This Day"?  <div className="h-2 w-full divider"></div>:'' }
                  
                  </div>
                  )}) }
                  
                  </div>
                )  }
            </div>

            {(apiMonth!=="" || apiYear!==""||typeName.title!=="Type" )  && <div className='w-28 lg:w-20 mx-1'>
              <div className='br-2 border-solid cursor-pointer w-full p-1 flex items-center justify-center rounded-full  border-red  h-12'  onClick={()=>(setDateChange(false),setApiMonth(""),setApiYear(""),setTypeName({title:"Type",value:''}))}>
                <span className='text-xs font-medium ml-1 flex items-center justify-center' >
                  <img
                  className='m '
                  // onClick={() => window.history.back()}
                  src='/svgs/clearfliter.svg'
                  alt='back icon'
                /> <span className={`ml-1 text-sm font-semibold flex items-center justify-center ${(apiMonth!="" || apiYear!="")?'darkRed':''}`}>CLEAR</span> 
                </span>
              </div>
            </div>}
           
          </div>
        </div>
      </div>

      <div className='w-full'>
        {error ? (
          <div className='w-full h-screen font-extralight text-xs gray flex flex-col  justify-center items-center'>
            <span>Something isn't right!</span>
            <span>Please refresh and try again...</span>
          </div>
        ) : (
          <div
            className='w-full flex flex-wrap min-h-fit  max-h-full hidescroll overflow-y-scroll  bg-white'
            onScroll={(evt) => handleScroll(evt)}>
            {(data && data.getAuthorsArticles &&  data.getAuthorsArticles.articles.length>0)    ? (
              data.getAuthorsArticles.articles.map((article, key) => (
                <>
            <Link
              key={key}
              {...getNewsUrl(article, tab)}
              passHref
              className='w-full w-full-l cursor-pointer'>

              <ArticleCard isFeatured={false} article={article} 
              authorPic={authorData && authorData.pic}
              
              />
              <div className='divider' />

            </Link>
              
                  {/* {article.type != 'live-blog' && article.type != 'live-blogs' ? (
                    <Link key={key} {...getNewsUrl(article, tab)} passHref>
                      <a className='w-full w-full-l cursor-pointer' key={key}>
                        <ArticleCard isFeatured={false} article={article} />
                        <div className='divider' />
                      </a>
                    </Link>
                  ) : (
                    <Link key={key} {...props.News_Live_Tab_URL(article, tab)} passHref>
                      <a
                        className='w-full w-full-l cursor-pointer'
                        key={key}
                        onClick={() => {
                          props.handleCleverTab(article);
                        }}>
                        <ArticleCard
                          isFeatured={
                            windows
                              ? windows.localStorage.Platform === 'Web'
                                ? key === 0 || key === 1
                                : key === 0
                              : ''
                          }
                          article={article}
                        />
                        <div className='divider' />
                       
                      </a>
                    </Link>
                  )} */}

                </>
              ))
            ) : (
              <div className='pt-4 center'>
                {' '}
                {/* {...props.News_Live_Tab_URL(article, tab)} */}
                <div className='flex justify-center items-center'>
                  <img className='w-44' src='/svgs/Empty.svg' alt='' />
                </div>
                <div className='gray text-sm text-center mr-4'>No Articles found</div>
              </div>
            )}
          </div>
        )}
      </div>
      </>:<div className='pt-4 center'>
                {' '}
                {/* {...props.News_Live_Tab_URL(article, tab)} */}
                <div className='flex justify-center items-center'>
                  <img className='w-44' src='/svgs/Empty.svg' alt='' />
                </div>
                <div className='gray text-sm text-center mr-4'>No Data Found</div>
              </div>}
    </div>
  </>;
}
}

export default NewsDetailsLayout;
