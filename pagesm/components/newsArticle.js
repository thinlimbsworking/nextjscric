import React, { useState,useRef ,useEffect} from 'react';
import { format, formatDistanceToNowStrict } from 'date-fns';
import { useRouter } from 'next/navigation';
import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectCoverflow, Navigation, Pagination, Scrollbar, A11y, Controller ,FreeMode} from 'swiper';
import Heading from './commom/heading';
export default function News({data,...props}) {
    console.log("useEffectuseEffectuseEffect",data)
    const [swiper, setSwiper] = useState();
    const prevRef = useRef();
    const nextRef = useRef();
    const deviceType = () => {
      const ua = navigator.userAgent;
      if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
          return "tablet";
      }
      else if (/Mobile|Android|iP(hone|od)|IEMobile|BlackBerry|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(ua)) {
          return "mobile";
      }
      return "desktop";
  };
  
  
  const [currentIndex, updateCurrentIndex] = useState(0);
  const router = useRouter();
  const navigate = router.push;
  return (
    <div className=' text-white  mt-5  mx-3  '>
    <div className='flex justify-between items-center '>
      <Heading heading={'Featured Article'}/>
     
      <div
        className='flex  justify-center rounded border-2  text-green border-green text-xs px-2 py-1 items-center font-bold '
        onClick={() => navigate(`videos/latest`)}>
        VIEW ALL
      </div>
    </div>

    <div className='flex items-center justify-center mt-4'>
    <div className=" mx-2 hidden lg:block md:block" ref={prevRef}  >
   <div
      style={{ zIndex: 1000 }}
    
      id='swiper-button-prev'
      className='white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer'>
      <svg width='30' focusable='false' viewBox='0 0 24 24'>
        <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
        <path fill='none' d='M0 0h24v24H0z'></path>
      </svg>
    </div>
 
    </div>




{data &&
        data.getArticleByPostitions.map((item,i) => {return( 
        <div className='   pb-4 bg-gray cardUpdate  rounded-xl  m-1'>
        <div className='flex relative items-center justify-center w-full '>
                    <img className='h-32   w-full rounded-lg object-conatin object-top ' src={item.featureThumbnail} alt='' />
                  </div>
                  <div className=' px-2 overflow-hidden mt-2 text-sm font-semibold text-ellipsis text-left tracking-wider truncate'>
                    {item.title || ''}
                  </div>
                  <div className=' my-1 px-2 text-xs font-thin truncate text-left tracking-wide '>
                    {item.description || ''}
                  </div>
                  <div className='px-2 text-gray-2 text-xs text-left truncate '>{item.author}</div>
                  <div className='px-2 text-gray-2 text-xs text-left mt-1'>
                    {format(new Date(Number(item.createdAt) - 19800000), 'dd MMM yyyy')}
                  </div>
                </div>
        
        )})}
    
    <div className=" text-2xl hidden lg:block md:block" ref={nextRef}>
  <div
      style={{ zIndex: 1000 }}
    
      id='swiper-button-prev'
      className='white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer'>
        <svg width='30' viewBox='0 0 24 24'>
        <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
        <path fill='none' d='M0 0h24v24H0z'></path>
      </svg>
    </div>
    </div>
    </div>

    <div className='flex justify-start mt-1  '>
      {data &&
        data.getArticleByPostitions.map((item, key) => (
          <div key={key} className={`w-2 h-2 ${currentIndex == key ? 'bg-blue-8' : 'bg-blue-4 '} rounded-full m-1 `}></div>
        ))}
    </div>
  </div>
  );
}



