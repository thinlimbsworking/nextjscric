import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import ArticleCard from './articleCard';
import BottomNavBar from './BottomNavBar';
import { useQuery } from '@apollo/react-hooks';
import { ARTICLE_LIST } from '../api/queries';
import { NEWS } from '../constant/MetaDescriptions';
import MetaDescriptor from './MetaDescriptor';
import Mobile from '../components/commom/mobileHeading'

const NewsDetailsLayout = ({ tabs, tabsConfig, tab, ...props }) => {
 
  const [pagination, setPagination] = useState(1);
  const [windows, updateWindows] = useState();
  useEffect(() => {
    if (global.window) updateWindows(global.window);
  }, [global.window]);

  let { loading, error, data, fetchMore } = useQuery(ARTICLE_LIST, {
    variables: { type: tab, page: 0 },
    notifyOnNetworkStatusChange: false
  });
  if (loading) data = props.data.data;

  const handleScroll = (evt) => {
    if (evt.target.scrollTop + evt.target.clientHeight >= 0.7 * evt.target.scrollHeight) {
      setPagination((prevPage) => prevPage + 1);
      
      handlePagination(fetchMore);
    }
  };

  const handlePagination = (fetchMore) => {
    fetchMore({
      variables: { type: tab, page: pagination },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previousResult;
        }
        let resultedArticle = Object.assign({}, previousResult, {
          getArticles: [...previousResult.getArticles, ...fetchMoreResult.getArticles]
        });
        return { ...resultedArticle };
      }
    });
  };

  return <>
    <MetaDescriptor section={NEWS[tab]} />
    <div className=' text-white bg-basebg lg:pt-24 xl:pt-24 md:pt-24 pt-20'>
      <div className='fixed w-full z-10 bg-gray-8 xl:top-16 lg:top-16 md:top-16 top-0'>

      <h1 className='text-lg lg:text-2xl lg:py-5 lg:px-0 font-semibold p-3'>News & Articles </h1>
      <div className='md:hidden '>
        <div className='flex overflow-x-scroll hidescroll sticky justify-between scrollmenu top-4'>
          <div>
            {tabs.map((tabName, i) => (
              (<Link
                key={i}
                {...props.getTabUrl(tabName)}
                replace
                passHref
                className={`text-white text-sm pt-2 nowrap inline-block mx-2 py-1 mb-1 text-center capitalize ${
                  tabsConfig[tabName] === tab && 'border-b-2 border-green-6'
                }`}>

                {tabName}

              </Link>)
            ))}
          </div>
        </div>
      </div>
      <div className='items-center py-4 justify-start scrollmenu hidden lg:block md:block'>
        <div className='flex justify-start' >
          {tabs.map((tabName, i) => (
            <>
              <Link
                key={i}
                {...props.getTabUrl(tabName)}
                replace
                passHref
                className={`rounded flex items-center justify-center  text-white text-sm  py-2  mr-4 pointer w-36 ${tabsConfig[tabName] === tab ? 'border-green-6 rounded-lg text-green border-2 bg-gray-4' : 'text-white'}`}>

                {tabName.toUpperCase()}

              </Link>
            </>
          ))}
        </div>
      </div>
      </div>
      {/* <mobileHeading title={'News & Articles'}/> */}
    {/* <h1 className='text-lg lg:text-2xl lg:py-5 lg:px-0 font-semibold p-3'>News & Articles </h1> */}

{/* --------------------------------for mobile screen----------------------- */}
      {/* <div className='md:hidden '>
        <div className='flex overflow-x-scroll hidescroll sticky justify-between scrollmenu top-4'>
          <div>
            {tabs.map((tabName, i) => (
              <Link key={i} {...props.getTabUrl(tabName)} replace passHref>
                <a
                  key={i}
                  className={`text-white text-sm pt-2 nowrap inline-block mx-2 py-1 mb-1 text-center capitalize ${
                    tabsConfig[tabName] === tab && 'border-b-2 border-green-6'
                  }`}>
                  {tabName}
                </a>
              </Link>
            ))}
          </div>
        </div>
      </div> */}


      {/* <div className='items-center py-4 justify-start scrollmenu hidden lg:block md:block'>
        <div className='flex justify-start' >
          {tabs.map((tabName, i) => (
            <>
              <Link key={i} {...props.getTabUrl(tabName)} replace passHref>
                <a
                 
                  className={`rounded flex items-center justify-center  text-white text-sm  py-2  mr-4 pointer w-36 ${tabsConfig[tabName] === tab ? 'border-green-6 rounded-lg text-green border-2 bg-gray-4' : 'text-white'}`}
                 >
                  {tabName.toUpperCase()}
                </a>
              </Link>
            </>
          ))}
        </div>
      </div> */}

      <div className='w-full'>
        {error ? (
          <div className='w-full h-screen text-sm font-semibold text-gray-2 flex flex-col  justify-center items-center'>
            <span>Something isn't right!</span>
            <span>Please refresh and try again...</span>
          </div>
        ) : (

       
          <div
            className=' max-h-[800px] bg-basebg'
            // style={{max}}
            onScroll={(evt) => handleScroll(evt)}>
            {data && data.getArticles.length !== 0 ? (
              data.getArticles.map((article, key) => (
                <>
                  {article.type != 'live-blog' &&  article.type != 'live-blogs' ? (
                    (<Link
                      key={key}
                      {...props.getNewsUrl(article, tab)}
                      passHref
                      className='w-full lg:w-full cursor-pointer'
                      onClick={() => {
                        props.handleCleverTab(article);
                      }}>

                      <ArticleCard
                        isFeatured={
                          windows
                            ? windows.localStorage.Platform === 'Web'
                              ? key === 0 || key === 1
                              : key === 0
                            : ''
                        }
                        article={article}
                      />

                    </Link>)
                  ) : (
                    (<Link
                      key={key}
                      {...props.News_Live_Tab_URL(article, tab)}
                      passHref
                      className='w-full lg:w-full cursor-pointer'
                      onClick={() => {
                        props.handleCleverTab(article);
                      }}>

                      <ArticleCard
                        isFeatured={
                          windows
                            ? windows.localStorage.Platform === 'Web'
                              ? key === 0 || key === 1
                              : key === 0
                            : ''
                        }
                        article={article}
                      />
                      {/* <div className='divider' /> */}

                    </Link>)
                  )}
                </>
              ))
            ) : (
              <div className='pt-4 center'>   {/* {...props.News_Live_Tab_URL(article, tab)} */}
                <div className='flex justify-center items-center'>
                  <img className='w-44' src='/svgs/Empty.svg' alt='' />
                </div>
                <div className='gray font-semibold text-center mr-4'>No Articles found</div>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
    {/* <div className='mt-5'>
      <BottomNavBar />
    </div> */}
  </>;
};

export default NewsDetailsLayout;
