import React, { useEffect, useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { HOME_OVER_SIMULATOR } from '../api/queries';

export default function OverProjection(props) {
  const [screenWidth, setScreenWidth] = useState(0);
  const [maxRun, setMaxRun] = useState(10);
  const [oversArr, setOverArr] = useState([]);
  const [oversTab, setoversTab] = useState(0);
  const [yRange, setYRange] = useState();

  const { loading, error, data } = useQuery(HOME_OVER_SIMULATOR, { variables: { matchID: props.matchID },
    pollInterval: 8000,
    onCompleted: (data) => {
      let a = Math.max.apply(
        Math,
        data.homePageOverSimulator.phaseOfInningsData.map((o) => {
          return o.runs;
        })
      );
      setoversTab(data.homePageOverSimulator.phaseOfInningsData.length > 25 ? 1 : 0);
      setOverArr(
        props.matchType === 'T20'
          ? [4, 8, 12, 16, 20]
          : props.matchType === 'T10'
          ? [2, 4, 6, 8, 10]
          : data.homePageOverSimulator.phaseOfInningsData.length > 22 &&
            data.homePageOverSimulator.phaseOfInningsData.length < 26
          ? [4, 8, 12, 16, 20, 24, 28]
          : data.homePageOverSimulator.phaseOfInningsData.length > 25
          ? [30, 35, 40, 45, 50]
          : [5, 10, 15, 20, 25]
      );
      setYRange(
        a < 8 ? 10 : a <= 13 ? 15 : a <= 18 ? 20 : a <= 23 ? 25 : a <= 28 ? 30 : a <= 33 ? 35 : a <= 38 ? 40 : 45
      );
      setMaxRun(a);
    }
  });
  useEffect(() => {
    // setScreenWidth(window.innerWidth);
    if (typeof window !== 'undefined' && window && window.screen && window.screen.width) {
      setScreenWidth(window.screen.width > 975 ? window.screen.width/1.5 : window.screen.width+window.screen.width);
    }
  });

if(loading){

}
if(error){}

useEffect(() => {
  console.log()
  return () => {
   console.log()
  }
});
// useEffect(()=>{
//  return(
//    alert(88)
//  ) 
// })

if(data)
  return data && data.homePageOverSimulator && data.homePageOverSimulator.phaseOfInningsData ? (
    <div className='text-white bg-gray rounded-md mt-3 mx-1 py-3 px-2 ' style={{ width: screenWidth/2 - 8 }}>
      <div className='text-md font-semibold px-2 mb-4'>Next 3 Overs Projection</div>

      {data.homePageOverSimulator.phaseOfInningsData.length > 25 && props.matchType == 'ODI' && (
        <div
          className='flex justify-between'
          style={{
            width: screenWidth/2 - 48
          }}>
          <div style={{ width: 22 }}></div>
          <div
            className={`bg-gray-4 rounded-t flex py-2 justify-between text-sm font-semibold `}
            style={{
              width: screenWidth/2 - 75
            }}>
            <div
              className={`w-1/2 pb-2 text-center ${oversTab == 0 ? 'text-blue border-b-2 border-blue' : 'text-gray-2'}`}
              onClick={() => (setoversTab(0), setOverArr([5, 10, 15, 20, 25]))}>
              1-25 Overs
            </div>
            <div
              className={`w-1/2 pb-2 text-center ${oversTab == 1 ? 'text-blue border-b-2 border-blue' : 'text-gray-2'}`}
              onClick={() => (setoversTab(1), setOverArr([30, 35, 40, 45, 50]))}>
              26-50 Overs
            </div>
          </div>
        </div>
      )}
      <div
        className='flex justify-between  '
        style={{
          width: screenWidth/2 - 48,
          height: 280
        }}>
        <div className='flex flex-col-reverse text-gray-2 text-xs font-medium' style={{ width: 22 }}>
          {[1, 2, 3, 4, 5].map((val, i) => (
            <div
              key={i}
              className=''
              style={{
                height: 280 / 5
              }}>
              <div className='flex justify-end '>{(yRange / 5) * (i + 1)}</div>
              <div className={`${i === 0 ? '' : ''} -rotate-90 flex justify-end mt-2 ml-3`}>{i === 0 ? 'Runs' : ''}</div>
            </div>
          ))}
        </div>
        <div
          className=''
          style={{
            width: screenWidth/2 - 75
          }}>
          <div
            className='rounded-b bg-gray-4 flex items-end justify-start '
            style={{
              width: screenWidth/2 - 75,
              height: 280,
              paddingBottom: 0
            }}>
            {data &&
              data.homePageOverSimulator.phaseOfInningsData.map(
                (over, i) =>
                  (props.matchType == 'ODI' ? oversTab === 0 && i < 25 : true) && (
                    <div
                      key={i}
                      className={` ${over.isCurrentOver ? 'bg-green' : 'bg-gray-3'} rounded relative`}
                      style={{
                        width:
                          props.matchType == 'T10'
                            ? (screenWidth/2 - 95) / 10
                            : props.matchType == 'T20'
                            ? (screenWidth/2 - 110) / 20
                            : oversArr.length === 7
                            ? (screenWidth/2 - 131) / 36
                            : (screenWidth/2 - 125) / 25,
                        height: (275 / 5) * (over.runs / (yRange / 5)),
                        // height:(over.runs/yRange)*275,
                        marginRight: 2
                      }}>
                      <div className='flex-col '>
                        {Array.apply(null, { length: Number(over.wickets) }).map((x, i) => (
                          <div key={i} className='bg-red flex rounded-full -mt-5 w-2 h-2 m-auto' />
                        ))}
                      </div>
                    </div>
                  )
              )}
            {data &&
              data.homePageOverSimulator.phaseOfInningsData.map(
                (over, i) =>
                  (props.matchType == 'ODI' ? oversTab === 1 && i >= 25 : false) && (
                    <div
                      key={i}
                      className={` ${over.isCurrentOver ? 'bg-green' : 'bg-gray-3'} rounded relative`}
                      style={{
                        width: (screenWidth - 125) / 25,
                        height: (275 / 5) * (over.runs / (yRange / 5)),
                        marginRight: 2
                      }}>
                      <div className='flex-col '>
                        {Array.apply(null, { length: Number(over.wickets) }).map((x, i) => (
                          <div key={i} className='bg-red flex rounded-full -mt-5 w-2 h-2 m-auto' />
                        ))}
                      </div>
                    </div>
                  )
              )}
            {/* oversArr.length === 7 && */}
            {/* (props.matchType== 'ODI' ? oversTab === 1 && i>=25:false) && */}
            {((props.matchType === 'T20' && data.homePageOverSimulator.phaseOfInningsData.length < 20) ||
              (props.matchType === 'T10' && data.homePageOverSimulator.phaseOfInningsData.length < 10) ||
              (props.matchType === 'ODI' &&
                oversTab === 0 &&
                data.homePageOverSimulator.phaseOfInningsData.length <= 25) ||
              (props.matchType === 'ODI' && oversTab == 1)) &&
              data &&
              data.homePageOverSimulator.overSimulatorData.predictedOversArray.map((over, i) => (
                <div
                  key={i}
                  className={` bg-green rounded relative`}
                  style={{
                    width:
                      props.matchType == 'T10'
                        ? (screenWidth - 95) / 10
                        : props.matchType == 'T20'
                        ? (screenWidth - 110) / 20
                        : oversArr.length === 7
                        ? (screenWidth - 131) / 36
                        : (screenWidth - 125) / 25,
                    height: (275 / 5) * (over.runs / (yRange / 5)),
                    marginRight: 2
                  }}>
                  <div className='flex-col '>
                    {Array.apply(null, { length: Number(over.wickets) }).map((x, i) => (
                      <div key={i} className='bg-red flex rounded-full -mt-5 w-2 h-2 m-auto' />
                    ))}
                  </div>
                </div>
              ))}
          </div>

          <div className='pt-1 flex text-gray-2 text-xs justify-between font-medium relative ' style={{}}>
            {oversArr.map((val, i) => (
              <div
                key={i}
                className=' flex text-end justify-end    '
                style={{
                  width:
                    props.matchType == 'T10'
                      ? (screenWidth - 20) / 5
                      : props.matchType == 'T20'
                      ? (screenWidth - 40) / 5
                      : oversArr.length === 7
                      ? (screenWidth - 56) / 7
                      : (screenWidth - 50) / 5
                }}>
                <div className={`${i === 0 ? 'absolute left-0' : ''} text-[11px]`}>{i === 0 ? 'Overs' : ''}</div>
                <div className='text-end'>{val}</div>
              </div>
            ))}
          </div>
        </div>
      </div>

      <div className='flex justify-between items-center mt-12 px-2 font-medium'>
        <div className='flex items-center'>
          <div className='w-2 h-2 rounded-full bg-gray-3'></div>
          <div className='text-gray-2 text-sm pl-1'>Actual Runs</div>
        </div>
        <div className='flex items-center'>
          <div className='w-2 h-2 rounded-full bg-green'></div>
          <div className='text-gray-2 text-sm pl-1'>Projected Runs</div>
        </div>
        <div className='flex items-center'>
          <div className='w-2 h-2 rounded-full bg-green'></div>
          <div className='text-gray-2 text-sm pl-1'>Current</div>
        </div>
      </div>
      {data.homePageOverSimulator.overSimulatorData.currentTeamShortName && (
        <div className='bg-gray-4 rounded-lg p-2 mt-4 text-center text-sm font-semibold'>
          Total Projected Score {data.homePageOverSimulator.overSimulatorData.currentTeamShortName}:{' '}
          {data.homePageOverSimulator.overSimulatorData.nextThreeScore}
          {data.homePageOverSimulator.overSimulatorData.nextThreeWicket ? '/' : ''}
          {data.homePageOverSimulator.overSimulatorData.nextThreeWicket}(
          {data.homePageOverSimulator.overSimulatorData.nextThreeOvers})
        </div>
      )}
    </div>
  ) : (
    <></>
  );
}
