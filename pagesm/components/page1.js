import React, { useEffect, useState } from 'react';
import ClevertapReact from 'clevertap-react';
import { ApolloProvider } from '@apollo/react-hooks';

import Router from 'next/router';
// import ErrorBoundary from '../components/ErrorBoundary';

// import Layout from '../components/Layout';
import withData from '../lib/apollo';


const MyApp = ({ Component, pageProps, apollo, ...props }) => {
  const [isLoading, updateLoadingStatus] = useState(false);
  const [urlTrack, setUrlTrack] = useState(true);

  useEffect(() => {
    ClevertapReact.initialize(process.env.CLEVERTAP_ID);
    localStorage['Platform'] = window.innerWidth > 550 ? 'Web' : 'Mweb';

    if(process.env.BUILD_TYPE == 'PROD') {
      if (typeof window !== 'undefined') {
        window.dataLayer = window.dataLayer || [];
        function gtag() {
          dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'GTM-T852BBQ');
        // gtag('config', 'UA-191876470-1');
        // gtag('config', 'GTM-MMNNHLW');
      }
    }
    Router.events.on('routeChangeStart', () => {
      updateLoadingStatus(true);
    });
    Router.events.on('routeChangeComplete', () => {
      setUrlTrack(false);
      updateLoadingStatus(false);
    });
    Router.events.on('routeChangeError', () => {
      updateLoadingStatus(false);
    });
  }, []);
  return (
    <ApolloProvider client={apollo}>
      
        
          <Component {...pageProps} urlTrack={urlTrack} setUrlTrack={setUrlTrack} />
       
      
    </ApolloProvider>
  );
};

// Wraps all components in the tree with the data provider
export default withData(MyApp);
