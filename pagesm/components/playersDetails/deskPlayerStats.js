import React from 'react';
import batsmenIcon from '../../public/svgs/images/playerBatsmen.svg';
import bowlerIcon from '../../public/svgs/images/playerBowler.svg';
import HrLine from '../Common/HrLine';

const DeskPlayerStats = ({ stats }) => {
  return (
    <div className='flex justify-between grey_10'>
      <div className='bg-white br2 flex flex-column w-third mb3 justify-around'>
        <div className='flex pa3 justify-between'>
          <div className='flex flex-column justify-between w-50 '>
            <p className='f5 ma0 red_Orange fw6'>Matches</p>
            <p className='f3 fw6 ma0 mt2'>77</p>
          </div>
          <div className='flex flex-column justify-between w-50'>
            <p className='f5 ma0 red_Orange fw6'>Innings</p>
            <p className='f3 fw6 ma0 mt2'>77</p>
          </div>
        </div>
        <HrLine />
        <div className='flex pa3 justify-between'>
          <div className='w-50 fw6'>
            <p className='ma0 red_Orange f5'>Debut</p>
            <p className='ma0 grey_10 f3 mt2'>WI v IND</p>
          </div>
          <div className='w-50 fw3'>
            <p className='ma0 grey_10 f7'>20 Jun 2011</p>
            <p className='ma0 grey_10 f7 mt3'>4 (10) |15 (54)</p>
          </div>
        </div>
        <HrLine />
        <div className='flex pa3 justify-between'>
          <div className='w-50 fw6'>
            <p className='ma0 red_Orange f5'>Recent</p>
            <p className='ma0 grey_10 f3 mt2'>WI v IND</p>
          </div>
          <div className='w-50 fw3'>
            <p className='ma0 grey_10 f7'>20 Jun 2011</p>
            <p className='ma0 grey_10 f7 mt3'>4 (10) |15 (54)</p>
          </div>
        </div>
      </div>
      <div className='bg-white br2 flex flex-column w-third mh3 mb3 justify-around'>
        <div className='flex ph3 pt3 flex-column'>
          <div className='flex items-end'>
            <span className='fw6 mr2'>BATTING</span>
            <img src={batsmenIcon} alt='batsmen_icon' />
          </div>
          <div className='f5 fw6 flex justify-between'>
            <div>
              <p className='ma0 mt3'>Runs</p>
              <p className='f3 ma0 mt1'>6676</p>
            </div>
            <div
              style={{
                width: '6.3rem',
                height: '6.3rem',
                boxShadow: '0 4px 8px 0 rgba(250, 148, 65, 0.32)'
              }}
              className='f6 fw4 white br-100 bg-red_Orange tc justify-center items-center flex flex-column'>
              <div className='ph2'>Highest Score</div>
              <div className='fw6 mt1' style={{ fontSize: '1.1rem' }}>
                123
              </div>
            </div>
          </div>
          <div className='f5 fw6 flex mt2 pb4'>
            <div className='mr4'>
              <p className='ma0 mt3'>Average</p>
              <p className='f3 ma0 mt1'>56.45</p>
            </div>
            <div className='relative'>
              <div
                style={{
                  width: '5rem',
                  height: '5rem',
                  boxShadow: '0 4px 8px 0 rgba(250, 148, 65, 0.32)'
                }}
                className='f6 fw4 white  br-100 bg-red_Orange tc justify-center items-center flex flex-column'>
                <div>100s</div>
                <div className='fw6 mt1' style={{ fontSize: '1.1rem' }}>
                  23
                </div>
              </div>
              <div
                style={{
                  width: '4rem',
                  height: '4rem',
                  top: '54%',
                  right: '-2rem',
                  boxShadow: '0 4px 8px 0 rgba(250, 148, 65, 0.32)'
                }}
                className='f6 fw4 white absolute br-100 bg-red_Orange tc justify-center items-center flex flex-column'>
                <div>50s</div>
                <div className='fw6 mt1' style={{ fontSize: '1.1rem' }}>
                  23
                </div>
              </div>
            </div>
          </div>
          <HrLine />
          <div className='bg-gradientGrey flex justify-between f7 fw4' style={{ minHeight: '4rem' }}>
            <div className='mv3 ph2 tc' style={{ borderRight: '2px solid #e3e5ea' }}>
              <div>SR</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
            <div className='mv3 ph2 tc' style={{ borderRight: '2px solid #e3e5ea' }}>
              <div>Balls Faced</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
            <div className='mv3 ph2 tc' style={{ borderRight: '2px solid #e3e5ea' }}>
              <div>4s / 6s</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
            <div className='mv3 ph2 tc'>
              <div>Not Outs</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
          </div>
        </div>
      </div>
      <div className='bg-white br2 flex flex-column w-third mb3 justify-around'>
        <div className='flex ph3 pt3 flex-column'>
          <div className='flex items-end'>
            <span className='fw6 mr2'>BOWLING</span>
            <img src={bowlerIcon} alt='batsmen_icon' />
          </div>
          <div className='f5 fw6 flex justify-between'>
            <div>
              <p className='ma0 mt3'>Wickets</p>
              <p className='f3 ma0 mt1'>66</p>
            </div>
            <div
              style={{
                width: '6.3rem',
                height: '6.3rem',
                boxShadow: '0 4px 8px 0 rgba(250, 148, 65, 0.32)'
              }}
              className='f6 fw4 white br-100 bg-red_Orange tc justify-center items-center flex flex-column'>
              <div className='ph2'>Best Bowling</div>
              <div className='fw6 mt1' style={{ fontSize: '1.1rem' }}>
                12-9
              </div>
            </div>
          </div>
          <div className='f5 fw6 flex mt2 pb4'>
            <div className='mr4'>
              <p className='ma0 mt3'>Economy</p>
              <p className='f3 ma0 mt1'>56.45</p>
            </div>
            <div className='relative'>
              <div
                style={{
                  width: '5rem',
                  height: '5rem',
                  boxShadow: '0 4px 8px 0 rgba(250, 148, 65, 0.32)'
                }}
                className='f6 fw4 white  br-100 bg-red_Orange tc justify-center items-center flex flex-column'>
                <div>5W</div>
                <div className='fw6 mt1' style={{ fontSize: '1.1rem' }}>
                  23
                </div>
              </div>
              <div
                style={{
                  width: '4rem',
                  height: '4rem',
                  top: '54%',
                  right: '-2rem',
                  boxShadow: '0 4px 8px 0 rgba(250, 148, 65, 0.32)'
                }}
                className='f6 fw4 white absolute br-100 bg-red_Orange tc justify-center items-center flex flex-column'>
                <div>10W</div>
                <div className='fw6 mt1' style={{ fontSize: '1.1rem' }}>
                  23
                </div>
              </div>
            </div>
          </div>
          <HrLine />
          <div className='bg-gradientGrey flex justify-between f7 fw4' style={{ minHeight: '4rem' }}>
            <div
              className='mv3 ph2 tc'
              style={{
                borderRight: '2px solid #e3e5ea',
                width: '25%'
              }}>
              <div>Avg</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
            <div
              className='mv3 ph2 tc'
              style={{
                borderRight: '2px solid #e3e5ea',
                width: '25%'
              }}>
              <div>S/R</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
            <div
              className='mv3 ph2 tc'
              style={{
                borderRight: '2px solid #e3e5ea',
                width: '25%'
              }}>
              <div>Balls</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
            <div className='mv3 ph2 tc' style={{ width: '25%' }}>
              <div>Runs</div>
              <div className='fw6 mt1'>124.5</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeskPlayerStats;
