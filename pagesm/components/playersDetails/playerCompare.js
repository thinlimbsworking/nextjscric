import React, { useState, useRef, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useRouter } from 'next/router';
import { PLAYER_COMPARISION } from '../../api/queries';
import Loading from '../Loading';
const PlayerBackground ='/svgs/playerWhiteBackground.svg';
const Playerfallback = '/pngs/fallbackprojection.png';
const  whitebat = '/svgs/whitebat.svg';
const wicketKeeper ='/svgs/wicketKeeper.svg';
import CleverTap from 'clevertap-react';
const bowler = '/public/svgs/bowling.svg';
const allRounder = '/svgs/allRounderWhite.svg';
const compare2 = '/svgs/compare2.svg';
const swapIcon = '/pngsV2/swap.png'
import SliderCommon from '../commom/slider'
//import Swiper from 'react-id-swiper';
import { PLAYER_COMPARE_VIEW, PLAYER_VIEW } from '../../constant/Links';
import LineChart from './lineChart';
import Imgix from 'react-imgix';

export default function PlayerCompare({ playerID, playerSlug, playerID2, playerRole }) {
  const [DefaultImage, setDefaultImage] = useState(false);
  const [types, setTypes] = useState('Test');
  const [style, setstyle] = useState(playerRole && playerRole === 'Bowler' ? 'bowlingStyle' : 'battingStyle');
  const [swap, setSwap] = useState(true);
  let router = useRouter();
  const {
    loading,
    error,
    data: playerCompare
  } = useQuery(PLAYER_COMPARISION, {
    variables: { playerID1: playerID, playerID2: playerID2 }
  });

  let category = [
    { label: 'Test', value: 'Test' },
    { label: 'ODI', value: 'ODI' },
    { label: 'T20I', value: 'T20I' },
    { label: 'T20s', value: 'T20s' },
    { label: 'FC', value: 'FC' },
    { label: 'listA', value: 'List A' }
  ];
  let categoryBattingStats = [
    { label: 'battingStyle', value: 'BATTING STATS' },
    { label: 'bowlingStyle', value: 'BOWLING STATS' }
  ];

  const params = {
    preloadImages: true,
    slidesPerView: 3,
    navigation: {
      nextEl: `#nbutton`,
      prevEl: `#pbutton`
    },
    breakpoints: { 640: { slidesPerView: 6, spaceBetween: 0 } }
  };

  let BattingProperties = [
    { label: 'matches', value: 'Matches' },
    { label: 'innings', value: 'Innings' },
    { label: 'recForm', value: 'Rec. Form' },
    { label: 'runs', value: 'Runs' },
    { label: 'strikeRate', value: 'Strike Rate' },
    { label: 'average', value: 'Average' },
    { label: 'hundredsfifties', value: '50s/100s' },
    { label: 'foursix', value: '4s/6s' }
  ];

  let BowlingProperties = [
    { label: 'matches', value: 'Matches' },
    { label: 'innings', value: 'Innings' },
    { label: 'recForm', value: 'Rec Form' },
    { label: 'ballsBowled', value: 'Balls per Innings' },
    { label: 'wickets', value: 'Wickets' },
    { label: 'strikeRate', value: 'Bowl Strike Rate' },
    { label: 'average', value: 'Bowl Average' },
    { label: 'fivetenWicketHauls', value: '5w' }
  ];

  const handlePlayerCompare = (playerID1, playerSlug, playerID2) => {
    return { as: eval(PLAYER_COMPARE_VIEW.as), href: PLAYER_COMPARE_VIEW.href };
  };

  const handleCompare = (Player2ID) => {
    CleverTap.initialize('PlayersCompare', {
      Source: 'PlayersCompare',
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
    setDefaultImage(false);
    let playerID2 = Player2ID;
    let playerID1 = playerID;
    router.push(
      handlePlayerCompare(playerID1, playerSlug, playerID2).href,
      handlePlayerCompare(playerID1, playerSlug, playerID2).as
    );
  };
  const handlePlayerBack = (playerId, playerSlug) => {
    return { as: eval(PLAYER_VIEW.as), href: PLAYER_VIEW.href };
  };

  const handlePlayerBackNavigation = () => {
    let playerId = playerID;
    router.push(handlePlayerBack(playerId, playerSlug).href, handlePlayerBack(playerId, playerSlug).as);
  };
  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return (
      <>
        <div className='bg-gray-8 flex gap-2 items-center p-2 rounded-md md:hidden lg:hidden'>
          <div className='p-2  bg-gray rounded-md'>
            <img
              className='w-5 h-5'
              onClick={() => window.history.back()}
              src='/svgs/backIconWhite.svg'
              alt='back icon'
            />
          </div>
          <span className='text-base text-white font-bold'>Players</span>
        </div>
        {playerCompare && playerCompare.getPlayersComarisionV2 ? (
          <div className=' center hidescroll pb-2 '>
            <div
              className=' nt0 nt1-l relative overflow-hidden  '>
              
              {/* <div
                className='white absolute h2-3 flex  items-center bottom-0 w-100 bg-red'
                style={{
                  backgroundImage: 'linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.9))'
                }}>
                <div className='w-100 absolute   flex  '>
                  <div className='white  w-50   flex justify-center '>
                    <div className='w-90 w-50-l w-60-m flex justify-center items-center '>
                      <div className='w-20 tc'>
                        <img
                          className='w13 '
                          src={
                            playerCompare && playerCompare.getPlayersComarisionV2.role1 === 'Batsman'
                              ? whitebat
                              : playerCompare.getPlayersComarisionV2.role1 === 'Bowler'
                                ? bowler
                                : playerCompare.getPlayersComarisionV2.role1 === 'Wicket Keeper'
                                  ? wicketKeeper
                                  : allRounder
                          }
                          alt={
                            playerCompare &&
                              playerCompare.getPlayersComarisionV2 &&
                              playerCompare.getPlayersComarisionV2.role1 === 'Batsman'
                              ? whitebat
                              : playerCompare.getPlayersComarisionV2.role1 === 'Bowler'
                                ? bowler
                                : playerCompare.getPlayersComarisionV2.role1 === 'Wicket Keeper'
                                  ? wicketKeeper
                                  : allRounder
                          }
                        />
                      </div>
                      <div className='pl2 w-80 fw5  line-clamp   f6-l f7'>
                        {playerCompare &&
                          playerCompare.getPlayersComarisionV2 &&
                          playerCompare.getPlayersComarisionV2.player1}
                      </div>
                    </div>
                  </div>
                  <div className='white  w-50   flex justify-center '>
                    <div className='w-90 w-50-l w-60-m flex justify-center'>
                      <div className='w-20 tc'>
                        <img
                          className='w13 '
                          src={
                            playerCompare &&
                              playerCompare.getPlayersComarisionV2 &&
                              playerCompare.getPlayersComarisionV2.role2 === 'Batsman'
                              ? whitebat
                              : playerCompare.getPlayersComarisionV2.role2 === 'Bowler'
                                ? bowler
                                : playerCompare.getPlayersComarisionV2.role2 === 'Wicket Keeper'
                                  ? wicketKeeper
                                  : allRounder
                          }
                          alt={
                            playerCompare &&
                              playerCompare.getPlayersComarisionV2 &&
                              playerCompare.getPlayersComarisionV2.role2 === 'Batsman'
                              ? whitebat
                              : playerCompare.getPlayersComarisionV2.role2 === 'Bowler'
                                ? bowler
                                : playerCompare.getPlayersComarisionV2.role2 === 'Wicket Keeper'
                                  ? wicketKeeper
                                  : allRounder
                          }
                        />
                      </div>
                      <div className='flex w-80 items-center justify-between '>
                        <div className='  fw5  line-clamp  pa1  f7 f6-l'>
                          {playerCompare &&
                            playerCompare.getPlayersComarisionV2 &&
                            playerCompare.getPlayersComarisionV2.player2}
                        </div>
                        <img onClick={() => setDefaultImage(true)} className='w15 ' src={compare2} alt={compare2} />
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
              <div className='flex flex-col '>

                
                {/* <div className='flex text-base font-bold text-white -mt-2 p-2 mx-2'>Compare</div> */}
                <div className='flex mx-2 gap-1 justify-center'>
                  
                  <div className='w-1/2 flex flex-col items-center  py-4 bg-gray rounded-md'>
                    <div className=' overflow-hidden flex justify-center   bg-gray-8 w-32 h-32  rounded-full'>
                      {/* <img
                        className='nb1  center  w43 h47 h5-l'
                        src={playerCompare.getPlayersComarisionV2.headShotImagePlayer1 ? `${playerCompare.getPlayersComarisionV2.headShotImagePlayer1}?fit=crop&crop=faces&w=156&h=224&auto=compress&q=30`  : `https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                        alt={Playerfallback}
                        onError={(evt) => (evt.target.src = Playerfallback)}
                      /> */}
                      
                        
                      {playerCompare.getPlayersComarisionV2.headShotImagePlayer1 !== '' ? (
                        <Imgix
                          src={`${playerCompare.getPlayersComarisionV2.headShotImagePlayer1}?fit=crop&crop=face`}
                          width={150}
                          height={224}
                        /> 
                      ) : (
                        <img
                          className=' object-top object-cover'
                          style={{ height: 224, width: 150 }}
                          src={`https://images.cricket.com/players/${playerID}_headshot_safari.png`}
                          alt={Playerfallback}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                      )}
                     
                    </div>
                    <span className='text-base font-bold text-white mt-2 w-2/3 m-auto text-center'>{playerCompare.getPlayersComarisionV2.player1.toUpperCase()}</span>
                  </div>
                  <div className='flex items-center'><img className='w-6' src='/pngsV2/Star.png' /></div>
                  <div className='w-1/2 flex flex-col items-center relative  py-4 bg-gray rounded-md'>

                  <img
                        className='w-6 cursor-pointer h-6 absolute top-2 right-2'
                        src={swapIcon}
                        alt={'swap'}
                        //onError={(evt) => (evt.target.src = Playerfallback)}
                        onClick={e => setSwap(p => !p)}
                      />

                    <div className=' overflow-hidden flex justify-center   bg-gray-8 w-32 h-32  rounded-full'>
                      {/* <img
                        className='w-5 h-5 absolute top-1 right-1'
                        src={swapIcon}
                        alt={'swap'}
                        onError={(evt) => (evt.target.src = Playerfallback)}
                      /> */}

                      {playerCompare.getPlayersComarisionV2.headShotImagePlayer2 !== '' ? (
                        <Imgix
                          src={`${playerCompare.getPlayersComarisionV2.headShotImagePlayer2}?fit=crop&crop=face`}
                          width={150}
                          height={224}
                        /> 
                      ) : (
                        <img
                          className='object-top object-cover'
                          style={{ height: 224, width: 150 }}
                          src={`https://images.cricket.com/players/${playerID2}_headshot_safari.png`}
                          alt={Playerfallback}
                          onError={(evt) => (evt.target.src = Playerfallback)}
                        />
                      )}
                     
                    </div>
                    <span className='text-base font-bold text-white mt-2 w-2/3 m-auto text-center'>{playerCompare.getPlayersComarisionV2.player2.toUpperCase()}</span>
                  </div>



                  
                </div>
              </div>
            </div>
            {DefaultImage ? (
              <></>
            ) : (
              <div className='mt-2 '>
                {swap && <div className='flex pt-2 justify-between'>
                  {category.map((y, i) => (
                    <div
                      key={i}
                      className={`w-1/${category.length} p-2 text-xs text-gray-2 border-b-2 border-gray-2 ${types === y.label ? 'border-blue-6 text-blue-8' : ''
                    } text-xs text-gray-2  text-center flex  justify-center cursor-pointer`}
                      onClick={() => {
                        setTypes(y.label);
                      }}>
                      {y.value}
                    </div>
                  ))}
                </div>}
                {swap && <div className='flex w-min mx-auto  bg-gray-4  rounded-full my-4 p-1 justify-center gap-1 md:gap-8 lg:gap-8 md:bg-gray-8 lg:bg-gray-8'>
                  {categoryBattingStats.map((y, i) => (
                    <div
                      key={i}
                      className={`w-32 text-white text-xs font-bold p-2 ${style === y.label ? 'bg-gray-8 text-green-3 border-2 border-green-3' : ''
                        }  cursor-pointer text-center   flex items-center justify-center rounded-full cursor-pointer`}
                      onClick={() => {
                        setstyle(y.label);
                      }}>
                      {y.value}
                    </div>
                  ))}
                </div>}
                {/* playerCompare && playerCompare.getPlayersComarisionV2 && playerCompare.getPlayersComarisionV2[types] */}
                {swap && <div className='bg-gray-4 m-2 rounded-md'>
                  {(style === 'battingStyle' ? BattingProperties : BowlingProperties).map((x, i) => (
                    <div className='' key={i}>
                      <div className='flex w-100 items-center py-2 jusitfy-center'>
                        <div className='w-1/4 text-center text-base font-bold text-white'>
                          {playerCompare &&
                            playerCompare.getPlayersComarisionV2 &&
                            (x.label === 'recForm' &&
                              playerCompare.getPlayersComarisionV2[style][types][0][x.label].length !== 0 ? (
                              <LineChart
                                color={playerCompare.getPlayersComarisionV2[style][types][0].color}
                                data={playerCompare.getPlayersComarisionV2[style][types][0][x.label]}
                              />
                            ) : x.label !== 'recForm' &&
                              playerCompare.getPlayersComarisionV2[style][types][0][x.label] &&
                              playerCompare.getPlayersComarisionV2[style][types][0][x.label] !== '' ? (
                              playerCompare.getPlayersComarisionV2[style][types][0][x.label]
                            ) : x.label === 'recForm' &&
                              playerCompare.getPlayersComarisionV2[style][types][0][x.label].length === 0 ? (
                              '--'
                            ) : (
                              '--'
                            ))}
                        </div>
                        <div className=' w-2/4'>
                          <div className='text-center border-x-gray-2 padding-2' style={{borderLeftWidth:'1px', borderRightWidth:'1px'}}>
                            <div className='text-xs p-2 text-gray-2'>
                              {x.value}
                            </div>
                          </div>
                        </div>
                        <div className='w-1/4 text-center text-base text-white font-bold'>
                          {playerCompare &&
                            playerCompare.getPlayersComarisionV2 &&
                            (x.label === 'recForm' &&
                              playerCompare.getPlayersComarisionV2[style][types][1][x.label].length !== 0 ? (
                              <LineChart
                                color={playerCompare.getPlayersComarisionV2[style][types][1].color}
                                data={playerCompare.getPlayersComarisionV2[style][types][1][x.label]}
                              />
                            ) : x.label !== 'recForm' &&
                              playerCompare.getPlayersComarisionV2[style][types][1][x.label] &&
                              playerCompare.getPlayersComarisionV2[style][types][1][x.label] !== '' ? (
                              playerCompare.getPlayersComarisionV2[style][types][1][x.label]
                            ) : x.label === 'recForm' &&
                              playerCompare.getPlayersComarisionV2[style][types][1][x.label].length === 0 ? (
                              '--'
                            ) : (
                              '--'
                            ))}
                        </div>
                      </div>
                    </div>
                  ))}
                </div> }
              </div>
            )}
            <div className='bg-gray-4 m-2'>
              <div className='text-xs text-white p-2 bg-gray-4 rounded-t-md'>OTHER PLAYERS TO COMPARE</div>
              {/* mobile */}

              {/* desktop */}
              <div className='relative  bg-gray rounded-md pt-1'>

              <SliderCommon data={playerCompare?.getPlayersComarisionV2?.otherSimilarPlayers || [] } event='PlayersCompare' source='PlayersCompare' handleCompare={handleCompare} playerID2={playerID2} />

                {/* <Swiper {...params}> */}
                  {/* {playerCompare &&
                    playerCompare.getPlayersComarisionV2 &&
                    playerCompare.getPlayersComarisionV2.otherSimilarPlayers &&
                    playerCompare.getPlayersComarisionV2.otherSimilarPlayers.map((player, i) => (
                      <div
                        key={i}
                        className='flex py-3 flex-col items-center cursor-pointer '
                        onClick={() => handleCompare(player.playerID)}>
                        <div className='relative'>
                          <div className=' rounded-full  w-20 h-20 overflow-hidden '>
                            

                            {player.headShotImage !== '' ? (
                              <Imgix src={`${player.headShotImage}?fit=crop&crop=face`} width={94} height={120} />
                            ) : (
                              <img
                                className='object-cover object-top bg-gray-8 '
                                src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                                alt={Playerfallback}
                                onError={(evt) => (evt.target.src = Playerfallback)}
                              />
                            )}
                           
                          </div>

                          <div className='absolute bg-white rounded-full w-8 h-8 flex items-center justify-center ' style={{ bottom: -13, left: 25 }}>
                            <img className='w-4 ' src='/pngsV2/swap.png' alt={compare2} />
                          </div>
                        </div>
                        <span className='text-xs mt-4 text-center text-white'>{player.playerName}</span>
                      </div>
                    ))} */}
                {/* </Swiper> */}
                <div className='absolute absolute--fill flex justify-between items-center md:hidden lg:hidden mt-4 '>
                  <div id={`pbutton`} className='white cursor-pointer z-999 outline-0  '>
                    <svg width='44' focusable='false' viewBox='0 0 24 24'>
                      <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                      <path fill='none' d='M0 0h24v24H0z'></path>
                    </svg>
                  </div>
                  <div id={`nbutton`} className='white cursor-pointer z-999 outline-0  '>
                    <svg width='44' viewBox='0 0 24 24'>
                      <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                      <path fill='none' d='M0 0h24v24H0z'></path>
                    </svg>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className='bg-white py-3 text-center text-base'>Data not available</div>
        )}
      </>
    );
}
