import React from 'react';
import parse from 'html-react-parser';
import backIconBlack from '../../public/svgs/backIconBlack.svg';

export default function PlayerDescription(props) {
  return (
    <div className=' '>
      <div className='flex ma3 items-center'>
        <img className='mr2' onClick={props.close} src={backIconBlack} alt='back icon' />
        <div className='f3 fw6'>Player Bio</div>
      </div>
      <div className='bg-white mh3 br2 overflow-y-scroll  ph2  '>
        <div className='bg-title pa3 mh3'>PLayer anme</div>
        <div className='f6 gray bg-yellow'>{parse(props.description)}</div>
      </div>
    </div>
  );
}
