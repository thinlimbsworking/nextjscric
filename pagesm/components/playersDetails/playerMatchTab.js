import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useRouter } from 'next/router';
import Loading from '../Loading';
import { format } from 'date-fns';
import { PLAYER_MATCH } from '../../api/queries';
import DataNotFound  from '../commom/datanotfound';
const empty = '/svgs/Empty.svg';
export default function PlayerMatchTab({ playerID }) {
  let router = useRouter();
  const [types, setTypes] = useState('all');
  const [playerid, setPlayerid] = useState(router.asPath.split('/')[2]);

  useEffect(() => {
    let playerid = router.asPath.split('/')[2];
    setPlayerid(playerid);
  }, []);

  const {
    loading,
    error,
    data: playerMatchData
  } = useQuery(PLAYER_MATCH, {
    variables: { playerID: playerID },
    onCompleted: (data) => {
      // console.log("dat match", data)
    }
  });
  let category = ['all', 'test', 'odi', 't20', 't20Domestic'];
  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return (
      <div>
        <div className='flex w-min  my-2 center p-1 bg-gray items-center justify-between mx-auto rounded-full md:bg-gray-8 lg:bg-gray-8 md:gap-8'>
          {category.map((y, i) => (
            <div
              key={i}
              className={`w-14 h-8 p-1  ${
                types === y ? 'border-2 border-green-6 text-green-6 bg-gray-8' : 'text-white '
              } text-xs font-bold cursor-pointer  text-center  flex items-center justify-center rounded-full cursor-pointer`}
              onClick={() => {
                setTypes(y);
              }}>
              {y === 'test'
                ? 'Test'
                : y === 'odi'
                ? 'ODI'
                : y === 't20'
                ? 'T20I'
                : y === 't20Domestic'
                ? 'T20s'
                : 'ALL'}
            </div>
          ))}
        </div>
        <div className='rounded-md m-2 '>
          {
            <div className='flex w-100 item-center justify-center  bg-gray-4 text-xs py-3 font-bold  rounded-t-md   '>
              <div className={`${types === 'all' ? 'w-1/4' : 'w-1/3'} justify-center  flex items-center `}>
                Opposition
              </div>
              <div className={`${types === 'all' ? 'w-1/5' : 'w-1/3'}  justify-center  flex items-center `}>
                Batting
              </div>
              <div className={`${types === 'all' ? 'w-1/5' : 'w-1/3'} justify-center   flex items-center`}>Bowling</div>
              {types === 'all' && <div className={`w-1/6  justify-center flex items-center  `}>Format</div>}
              {<div className={`w-1/5  flex items-center justify-center `}>Date</div>}
            </div>
          }

          {playerMatchData &&
          playerMatchData.getPlayerMatches &&
          playerMatchData.getPlayerMatches[types] &&
          playerMatchData.getPlayerMatches[types].length > 0 ? (
            playerMatchData.getPlayerMatches[types].map((fantasy, i) => (
              <div
                key={i}
                className={`flex w-100 border-b border-black bg-gray  item-center justify-center py-2 text-xs ${i === playerMatchData.getPlayerMatches[types].length-1 ? 'rounded-b-md':''}`}>
                {fantasy.opposition !== '' ? (
                  <div className={`${types === 'all' ? 'w-1/4' : 'w-1/3'}  flex  items-center justify-center`}>
                    <div className='fw4 f8 f7-l f7-m'>{fantasy.opposition.split('vs')[0]} vs</div>
                    <div className='fw6 pl1'>{fantasy.opposition.split('vs')[1]}</div>
                  </div>
                ) : (
                  <div className={`${types === 'all' ? 'w-1/4' : 'w-1/3'}  flex justify-center items-center `}>--</div>
                )}
                {fantasy.battingStats !== '' ? (
                  <div
                    className={`${types === 'all' ? 'w-1/5' : 'w-1/3'} flex  items-center justify-center `}>
                    <div className='fw4 '>
                      {fantasy.battingStats.split('&')[0]}
                      {fantasy.battingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.battingStats.split('&')[1]}</div>
                  </div>
                ) : (
                  <div className={`${types === 'all' ? 'w-1/5' : 'w-1/3'} justify-center items-center flex  `}>
                    <span className='fw4 '>--</span>
                  </div>
                )}
                {fantasy.bowlingStats !== '' ? (
                  <div
                    className={`${
                      types === 'all' ? 'w-1/5' : 'w-1/3'
                    }  flex flex-col items-center justify-center `}>
                    <div className='fw4 '>
                      {fantasy.bowlingStats.split('&')[0]}
                      {fantasy.bowlingStats.split('&').length > 1 ? '&' : ''}
                    </div>
                    <div>{fantasy.bowlingStats.split('&')[1]}</div>
                  </div>
                ) : (
                  <div className={`${types === 'all' ? 'w-1/5' : 'w-1/3'} justify-center  flex  items-center  `}>--</div>
                )}
                {types === 'all' && (
                  <div className={`w-1/6 flex items-center justify-center   `}>
                    {fantasy.matchType !== '' ? fantasy.matchType : '--'}
                  </div>
                )}
                {fantasy.matchDate !== '' ? (
                  <div className={` w-1/5 justify-center flex items-center  `}>
                    {format(parseInt(fantasy.matchDate), 'dd-MMM-yyyy')}
                  </div>
                ) : (
                  <div className={` w-1/5 flex items-center justify-center   `}>--</div>
                )}
              </div>
            ))
          ) : (
           <DataNotFound/>
          )}
        </div>
      </div>
    );
}
