import React, { useEffect, useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { PREDICTION_CONTEST, PREDICTION_CONTEST_MUTATION } from '../../api/queries';
import Countdown from 'react-countdown-now';
import { useRouter } from 'next/router';
import Image from 'next/image';
import Scores from '../../components/commom/score';
import Slider from 'rc-slider';
import SetUsername from './setUsername';
import { format } from 'date-fns';
import Loading from '../commom/loading'
import PostSuccessModal from "../commom/postSuccessModal";
import Heading from '../commom/heading';
import { useAuth } from '../customHooks/useAuth';
import ImageWithFallback from '../commom/Image';

let downarrow = "/svgs/downArrow.svg";
let uparrow = "/svgs/caret-up.svg";
let clock = "/svgs/clock_timer.svg"
const fantasyDp = '/svgs/mask_man.svg';


export default function ContestDetails({ matchID }) {
    let router = useRouter();
    const [quesList, setQuesList] = useState([]);
    const [payload, setPayload] = useState({});
    const [next, setNext] = useState(false);
    const [bet, setBet] = useState(10);
    const [isSuccess, setIsSucess] = useState(false);
    const [token, userName] = useAuth('tokenData', 'userName');

    const {
        loading,
        error,
        data,
        refetch
    } = useQuery(PREDICTION_CONTEST, {
        variables: { matchID, token },
        onCompleted: data => {
            generatedQues(data);
        },
        // fetchPolicy: 'cache-and-network'
    });




    useEffect(() => {
        generatedQues(data);
    }, [data])

    const [submitBet, response] = useMutation(PREDICTION_CONTEST_MUTATION, {
        onCompleted: (res) => {
            console.log('submitted bet res is: ', res);
            setIsSucess(true);
            refetch && refetch();
            setTimeout(() => {
                setIsSucess(false)
            }, 3000);
        },

    });
    let matchData = data?.predictionContest.matchDetails;

    const handleExpand = (val, from) => {
        setQuesList(quesList.map(e => {
            if (e.question === val.question) {
                return { ...e, expand: from === 'open' ? true : false }
            }
            return e;
        }))
    }

    const handleSelect = (val, opt) => {
        let rem = quesList.filter(ele => ele.submitted);
        let temp = quesList.filter(ele => !ele.submitted).map(e => ({ ...e, optns: e.optns.map(opts => ({ ...opts, select: false })) }));
        setBet(10);
        let ind = temp.findIndex(ele => ele.type === val.type);
        temp[ind] = {
            ...temp[ind], optns: temp[ind].optns.map(opts => {
                if (opts.name === opt.name) {
                    return { ...opts, select: true }
                } return opts;
            })
        }
        setQuesList([...rem, ...temp]);
        setPayload({
            type: [...rem, ...temp].filter(ele => ele.type === val.type)[0].type,
            id: opt.id,
            token,
            matchID
        });
    }

    function generatedQues(data) {
        let ques = [];
        data?.predictionContest.predictionOdds && Object.entries(data?.predictionContest.predictionOdds).map(([key, value]) => {
            let optns = [];
            if (data?.predictionContest.userPredictions[key]) {
                ques.push({
                    question: value['question'],
                    type: key,
                    expand: false,
                    submitted: true,
                    res: {
                        betCoins: data?.predictionContest.userPredictions[key].betCoins,
                        id: data?.predictionContest.userPredictions[key].id,
                        winCoins: data?.predictionContest.userPredictions[key].winCoins,
                        status: data?.predictionContest.userPredictions[key].status,
                        selectedName: data?.predictionContest.userPredictions[key].selectedName
                    },
                    optns: []
                })
                return;
            }
            if (key === 'TotalRunsOdds') {
                optns = [{ name: `more than ${value['runs']}`, odds: value['over'], select: false, id: 'over' }, { name: `less than ${value['runs']}`, odds: value['under'], select: false, id: 'under' }]
            } else if (key === 'WicketTypeOdds') {
                optns = [{ name: 'Bowled', odds: value['bowled'], select: false, id: 'bowled' }, { name: 'Catch', odds: value['caught'], select: false, id: 'caught' }, { name: 'LBW', odds: value['lbw'], select: false, id: 'lbw' }, { name: 'Run Out', odds: value['runout'], select: false, id: 'runout' }, { name: 'Stump', odds: value['stumped'], select: false, id: 'stumped' }, { name: 'Others', odds: value['others'], select: false, id: 'others' }]
            } else {
                if (value['No'] && value['Yes']) {
                    optns = [{ name: 'Yes', odds: value['Yes'], select: false, id: 'Yes' }, { name: 'No', odds: value['No'], select: false, id: 'No' }]
                } else if (value['team']) {
                    value['team'].forEach(ele => {
                        optns.push({
                            name: ele.playerName,
                            id: ele.playerID,
                            odds: ele.Odds,
                            select: false
                        })
                    });
                } else {
                    optns = [{ name: matchData.awayTeamName, id: matchData.awayTeamID, odds: value['awayTeam'], select: false }, { name: matchData.homeTeamName, id: matchData.homeTeamID, odds: value['homeTeam'], select: false }]
                }
            }
            ques.push({
                question: value['question'],
                optns,
                type: key,
                expand: false,
                submitted: false
            })
        })
        console.log('genrated ques list -> ', ques);
        setQuesList(ques);
    }

    const handleSubmit = () => {
        if (data?.predictionContest.userPredictions.remainingCoins < bet) return;
        submitBet({
            variables: {
                ...payload,
                betCoins: bet
            }
        })
    }

    const checkForUsername = () => {
        if (JSON.parse(userName) === null) {
            setNext(true);
        } else {
            handleSubmit();
        }

    }

    if (loading) return <Loading />

    if (isSuccess) return <PostSuccessModal Msg='Submitted successfully' />

    return (
        next && token ? <SetUsername setNext={setNext} handleSubmit={handleSubmit} token={token} />
            : <div className='w-100'>
                <div className='w-full flex justify-between p-3 items-center'>
                    <div className='bg-gray-8 flex gap-2 items-center  rounded-md md:hidden lg:hidden'>
                        <div className='p-2  bg-gray rounded-md'>
                            <ImageWithFallback
                                height={18}
                                width={18}
                                onClick={() => window.history.back()}
                                src='/svgs/backIconWhite.svg'
                                alt='back icon'
                            />
                        </div>
                        <span className='text-base text-white font-bold'>Match</span>

                    </div>
                    <div className='flex w-36 gap-2 bg-gray border-2 border-gray-2 p-2 rounded-md cursor-pointer' onClick={() => router.push(`/play-the-odd/mycontest`)}>
                        <ImageWithFallback width={22} height={22} className='h-5  w-5' src={fantasyDp} alt='leaderboard ' />
                        <span className='text-sm font-semibold text-white'>My Contest</span>
                    </div>
                </div>


                <div className='w-full  overflow-hidden  flex justify-center mt-3 items-center cursor-pointer ' >

                    <div className='-mb-4 w-full  cursor-pointer flex flex-col px-2' >
                        {matchData && matchData.matchID ? <Scores featured={true} data={matchData} playTheOdds={true} /> : <></>}
                    </div>

                </div>
                <div className='flex flex-col p-2 my-2 text-white'>
                    <Heading heading={`Predict & Win`} subHeading='Test Your Cricket Knowledge' />
                </div>


                <div className='flex justify-center gap-3 px-2'>
                    <div className='bg-gray w-3/4 flex flex-col justify-between rounded-md gap-2 p-2'>
                        <div className='flex justify-between items-center h-2/3  '>
                            <span className='text-xs text-white font-semibold'>Answers Predicted</span>
                            <span className='text-base text-semibold text-gray-2'>{data?.predictionContest.userPredictions.totalPredictions || 0}/11</span>
                        </div>
                        <div className='w-full  h-4 flex bg-gray-2 rounded-md mb-2'>
                            <div style={{ width: `${((data?.predictionContest.userPredictions.totalPredictions) * 100) / 11}%` }} className='rounded-md bg-green h-full '></div>
                        </div>
                    </div>
                    <div className='w-1/4 bg-gray rounded-md  flex flex-col items-center justify-center gap-2'>
                        <span className='text-sm text-gray-2 border-b border-b-gray-2 p-1'>Max Bet</span>
                        <div className='flex gap-2 pb-1 items-center'>
                            <ImageWithFallback height={18} width={18} loading='lazy'
              className='w-5 h-5' src='/pngs/coinIcon.png' alt='coins' />
                            <span className='text-base text-white'>{data?.predictionContest.userPredictions.remainingCoins || 0}</span>
                        </div>
                    </div>
                </div>


                {quesList.filter(ele => ele.submitted).length && <> <div className='flex flex-col p-2 my-2 text-white'>
                    <Heading heading={`Your Bets`} />
                </div>


                    <div className='  bg-gray rounded-md p-2 m-2 my-4'>

                        {
                            quesList.filter(ele => ele.submitted && ele.type !== 'TossOdds').map((val, i) => (
                                <div key={val.question} className={`bg-gray-4 rounded-md p-2 my-2 ${val.res.status !== 'NEW' && (val.res.status === 'WIN' ? 'border-2 border-green' : 'border-2 border-red')}`}>
                                    <div className='flex justify-between  items-center '>
                                        <span className={`${val.submitted ? 'text-gray-2' : 'text-white'} text-sm font-semibold`}>{val.question}</span>
                                        {!val.expand && <Image width={18} height={18} className='h-5 w-5 cursor-pointer' src={downarrow} onClick={() => handleExpand(val, 'open')} />}
                                        {val.expand && <Image width={18} height={18} className='h-5 w-5 rotate-180 cursor-pointer' src={downarrow} onClick={() => handleExpand(val, 'close')} />}
                                    </div>
                                    {val.expand &&
                                        <>
                                            <div key={val.res.id} className={`rounded-md bg-gray border-2 cursor-pointer  'border-gray-2'  flex justify-between items-center text-gray-2 text-sm p-3 my-2`} >
                                                <span>{val.res.selectedName}</span>
                                                <div className='flex gap-1 items-center '>
                                                    <Image width={22} height={22} className='h-4 w-4' src='/pngs/coinIcon.png' alt='coins' />
                                                    <span>{val.res.winCoins}</span>
                                                </div>
                                            </div>
                                            {val.res.status === 'NEW' ? <div className='text-gray-2 text-sm text-right'> Result yet to be declared</div> : <div className='text-gray-2 text-sm flex gap-1 justify-end items-center'><span className=''>{val.res.status + ' '}</span><Image width={22} height={22} src='/pngs/coinIcon.png' alt='coins' /><span className=''>{val.res.winCoins}</span></div>}
                                        </>}

                                </div>
                            ))
                        }
                    </div> </>}


                <div className='flex gap-2 items-center mx-2 mt-8'>
                    <Image width={22} height={22} src={clock} alt='timer' />
                    <Countdown
                        date={new Date(Number(matchData?.startDate) - 19800000 - 1000 * 60 * 2)}
                        renderer={(props) => (
                            <span className='text-sm font-semibold text-white'>
                                {`${props.days > 0 ? format(Number(matchData?.startDate), 'd LLLL, hh:mm a ') : (props.formatted.hours + ':' + props.formatted.minutes + ':' + props.formatted.seconds)}`}
                            </span>
                        )}
                    />
                </div>

                <div className='  bg-gray rounded-md p-2 m-2 my-4'>

                    {quesList && quesList.filter(ele => ele.type === 'TossOdds').map(val => <>

                        <div key={val.quesList} className='bg-gray-4 rounded-md p-2 my-2'>
                            <div className='flex justify-between  items-center '>
                                <span className={`${val.submitted ? 'text-gray-2' : 'text-white'} text-sm font-semibold`}>{val.question}</span>
                                {!val.expand && <Image width={22} height={22} className='cursor-pointer' src={downarrow} onClick={() => handleExpand(val, 'open')} />}
                                {val.expand && <Image width={22} height={22} className=' rotate-180 cursor-pointer' src={downarrow} onClick={() => handleExpand(val, 'close')} />}
                            </div>
                            {
                                val.submitted ? val.expand && <> <div key={val.res.id} className={`rounded-md bg-gray border-2 cursor-pointer  'border-gray-2'  flex  justify-between items-center text-gray-2 text-sm p-3 my-2`} >
                                    <span>{val.res.selectedName}</span>
                                    <div className='flex gap-1 items-center'>
                                        <Image width={22} height={22} src='/pngs/coinIcon.png' alt='coins' />
                                        <span>{val.res.winCoins}</span>
                                    </div>

                                </div>
                                    {val.res.status === 'NEW' ? <div className='text-gray-2 text-sm text-right'> Result yet to be declared</div> : <div className='text-gray-2 text-sm text-right flex gap-1'><span className=''>{val.res.status}</span><Image width={22} height={22} src='/pngs/coinIcon.png' alt='coins' /><span className=''>{val.res.winCoins}</span></div>} </>
                                    : val.expand && <div className='p-2 my-2'>  {val.optns && val.optns.map(opt => (
                                        <>
                                            <div key={opt.id} className={`rounded-md bg-gray border-2 cursor-pointer ${opt.select ? 'border-green-3' : 'border-gray-2'}  flex justify-between items-center text-white text-sm p-3 my-2`}
                                                onClick={() => handleSelect(val, opt)} >
                                                <span>{opt.name}</span>
                                                <span>{opt.odds}X</span>
                                            </div>

                                            {
                                                opt.select && <div className='flex flex-col my-6 gap-4 items-center justify-center'>
                                                    <div className='w-full '>
                                                        <Slider
                                                            className='slider-main w-full'
                                                            max={100}
                                                            min={10}
                                                            step={1}
                                                            value={bet}
                                                            onChange={(val) => {
                                                                if (
                                                                    val <= 100
                                                                ) {
                                                                    setBet(val);
                                                                }
                                                            }}
                                                            handle={Slider.handle}
                                                            handleStyle={[
                                                                {
                                                                    backgroundColor: 'white',
                                                                    border: 'white',
                                                                    width: '18px',
                                                                    height: '18px',
                                                                    marginTop: '-8px',
                                                                    marginLeft: '8px',
                                                                    boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
                                                                }
                                                            ]}
                                                            trackStyle={[{ backgroundColor: '#38d925', height: '5px' }]}
                                                            railStyle={{
                                                                backgroundColor: 'gray',
                                                                height: '5px'
                                                            }}
                                                        />
                                                    </div>
                                                    <div className='flex justify-between items-center w-full'>
                                                        <div className='w-32 flex items-center md:gap-2 lg:gap-2'>
                                                            <span className='text-xs text-green font-semibold '>Potential Earning</span>
                                                            <div className='flex gap-1 items-center '>
                                                                <Image width={22} height={22} className={'w-5 h-5'} src='/pngs/coinIcon.png' alt='coins' />
                                                                <span className='text-white text-base font-semibold'>{(bet * opt.odds).toFixed(2)}</span>
                                                            </div>
                                                        </div>
                                                        <div className={`bg-gray-8 px-3 py-1 rounded-md  text-sm font-semibold cursor-pointer border-2 ${data?.predictionContest.userPredictions.remainingCoins < bet ? ' text-gray-2 border-gray-2' : ' border-green text-green'}`}
                                                            onClick={checkForUsername} >
                                                            SUBMIT
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </>

                                    ))}
                                    </div>}

                        </div> </>)}

                </div>
                <div className='flex gap-2 items-center mx-2 mt-8'>
                    <Image width={22} height={22} src={clock} alt='timer' />
                    <Countdown
                        date={new Date(Number(matchData?.startDate) - 19800000 - 1000 * 60 * 2 + 1800000)}
                        renderer={(props) => (
                            <span className='text-sm font-semibold text-white'>
                                {`${props.days > 0 ? format(Number(matchData?.startDate), 'd LLLL, hh:mm a ') : (props.formatted.hours + ':' + props.formatted.minutes + ':' + props.formatted.seconds)}`}
                            </span>
                        )}
                    />
                </div>
                <div className='  bg-gray rounded-md p-2 m-2 my-4'>

                    {
                        quesList.filter(ele => !ele.submitted && ele.type !== 'TossOdds').map((val, i) => (

                            <div key={val.question} className={`bg-gray-4 rounded-md p-2 my-2 ${val.submitted && val.res.status !== 'NEW' && (val.res.status === 'WIN' ? 'border-2 border-green' : 'border-2 border-red')}`}>
                                <div className='flex justify-between  items-center '>
                                    <span className={`${val.submitted ? 'text-gray-2' : 'text-white'} text-sm font-semibold`}>{val.question}</span>
                                    {!val.expand && <Image width={18} height={18} className='cursor-pointer' src={downarrow} onClick={() => handleExpand(val, 'open')} />}
                                    {val.expand && <Image width={18} height={18} className='rotate-180 cursor-pointer' src={downarrow} onClick={() => handleExpand(val, 'close')} />}
                                </div>
                                {val.expand && <div className='p-2 my-2'>  {val.optns && val.optns.map(opt => (

                                    <>
                                        <div key={opt.id} className={`rounded-md bg-gray border-2 cursor-pointer ${opt.select ? 'border-green-3' : 'border-gray-2'}  flex justify-between items-center text-white text-sm p-3 my-2`}
                                            onClick={() => handleSelect(val, opt)} >
                                            <span>{opt.name}</span>
                                            <span>{opt.odds}X</span>
                                        </div>

                                        {
                                            opt.select && <div className='flex flex-col my-6 gap-4 items-center justify-center'>
                                                <div className='w-full '>
                                                    <Slider
                                                        className='slider-main w-full'
                                                        max={100}
                                                        min={10}
                                                        step={1}
                                                        value={bet}
                                                        onChange={(val) => {
                                                            if (
                                                                val <= 100
                                                            ) {
                                                                setBet(val);
                                                            }
                                                        }}
                                                        handle={Slider.handle}
                                                        handleStyle={[
                                                            {
                                                                backgroundColor: 'white',
                                                                border: 'white',
                                                                width: '18px',
                                                                height: '18px',
                                                                marginTop: '-8px',
                                                                marginLeft: '8px',
                                                                boxShadow: '0 2px 7px 0 rgba(162, 167, 177, 0.51)'
                                                            }
                                                        ]}
                                                        trackStyle={[{ backgroundColor: '#38d925', height: '5px' }]}
                                                        railStyle={{
                                                            backgroundColor: 'gray',
                                                            height: '5px'
                                                        }}
                                                    />
                                                </div>
                                                <div className='flex justify-between items-center w-full'>
                                                    <div className='w-32 flex items-center md:gap-2 lg:gap-2'>
                                                        <span className='text-xs text-green font-semibold '>Potential Earning</span>
                                                        <div className='flex gap-1 items-center '>
                                                            <Image width={22} height={22} className={'w-5 h-5'} src='/pngs/coinIcon.png' alt='coins' />
                                                            <span className='text-white text-base font-semibold'>{(bet * opt.odds).toFixed(2)}</span>
                                                        </div>
                                                    </div>
                                                    <div className={`bg-gray-8 px-3 py-1 rounded-md  text-sm font-semibold cursor-pointer border-2 ${data?.predictionContest.userPredictions.remainingCoins < bet ? 'text-gray-2 border-gray-2' : ' border-green text-green'}`}
                                                        onClick={checkForUsername} >
                                                        SUBMIT
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </>

                                ))}
                                </div>}

                            </div>
                        ))
                    }
                </div>

            </div>
    )
}
