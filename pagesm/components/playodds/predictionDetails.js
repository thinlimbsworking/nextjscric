import React from "react";
import { clsx } from "clsx";
import { PREDICTION_PROFILE_MATCH } from "../../api/queries";
import { useQuery } from "@apollo/react-hooks";
import ImageWithFallback from "../commom/Image";
import { useState } from "react";

export default function Predictionbar(props) {
  const token = localStorage.getItem("tokenData");
  const { data } = useQuery(PREDICTION_PROFILE_MATCH, {
    variables: {
      matchID: props?.matchID,
      token: token,
    },
  });
console.log(props,'ioioioioioioio');
  return (
    <div>
      <div className="text-xs lg:text-sm text-gray-2 mt-6">
        <div className="flex justify-between">
          <div className="flex-col justify-start items-start flex">
            <p className="pb-1">Right Prediction</p>
            <span className="flex justify-start text-white lg:text-lg text-xs">
              {data?.predictionProfileMatch?.rightPrediction}/
              {data?.predictionProfileMatch?.totalPredictions}
            </span>
          </div>
          <div className="flex justify-center items-center flex-col">
            <p className="pb-1">Total Bet</p>
            <span className="gap-5 text-white text-xs lg:text-lg">
              {data?.predictionProfileMatch?.TotalBet}
            </span>
          </div>
          <div className="flex-col flex justify-center items-center">
            <p className="text-xs lg:text-sm">
              Earnings
            </p>

            <span className="flex justify-center items-center pt-1 gap-1">
              <ImageWithFallback src="/pngsV2/coin.png" width={18} height={2} />
              <span
                className="text-white text-xs lg:text-lg"
              >
                {Math.abs(+data?.predictionProfileMatch?.totalEarnings)}
              </span>
            </span>
          </div>
          
        </div>
      </div>
    </div>
  );
}
