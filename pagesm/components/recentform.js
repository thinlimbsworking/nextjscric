import React, { useEffect, useState } from 'react';
import { HOME_RECENT_FORM } from '../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import DataNotFound from './commom/datanotfound';

const RightSchevronWhite = '/svgs/RightSchevronWhite.svg';

export default function RecentForm(props) {

  const [dropDownTeam1, setDropDownTeam1] = useState(true);
  const [dropDownTeam2, setDropDownTeam2] = useState(false);
  const { loading, error, data } = useQuery(HOME_RECENT_FORM, {
    variables: { homeTeamID: props.matchData.matchScore[0].teamID, awayTeamID: props.matchData.matchScore[1].teamID ,format: props.matchData.matchType}
  });

  return data && data.homepageRecentForm && data.homepageRecentForm ? (
    <div className=' bg-gray mx-2 rounded px-3 py-3 text-white md:mt-0 mt-3'>
      <div className='flex justify-between items-center'>
        <h3 className='font-semibold text-lg tracking-wider'>Recent Form</h3>
        <div className='flex items-center'>
          <div className=' bg-green-6 w-1 h-2 rounded-r-full blur-[1.5px] '></div>
          <div className='mnr text-gray-2 text-xs pl-1'>Recent Match</div>
        </div>
      </div>
      <div className='bg-gray-4 rounded-lg px-3 py-3 mt-2'>
        <div className='flex justify-between items-center' onClick={() => setDropDownTeam1(!dropDownTeam1)}>
          <div className='flex justify-start items-center w-1/4'>
            <img
              className='w-7 h-5 shadow-md '
              src={`https://images.cricket.com/teams/${data && data.homepageRecentForm.homeTeamID}_flag_safari.png`}
              onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
            />
            <div className=' pl-2 text-md font-semibold'>{data.homepageRecentForm.homeTeamName}</div>
          </div>
          {!dropDownTeam1 && (
            <div className='flex justify-between items-center w-2/4 '>
              {data &&
                data.homepageRecentForm &&
                data.homepageRecentForm.homeTeamData.map((item, y) => (
                  <div className='relative ' key={y}>
                    <div
                      className={`w-6 h-6 mx-1 rounded-full ${
                        item.type == 'W' ? 'bg-green-3' : 'bg-red-5'
                      } text-center flex justify-center items-center text-sm font-semibold`}>
                      {item.type}
                    </div>
                    {y == 0 && (
                      <div className=' bg-green-6 w-2 h-1 rounded-b-full blur-[1.5px] object-center absolute right-3 left-3 mt-1 '></div>
                    )}
                  </div>
                ))}
            </div>
          )}
          <div>
            <img className={`${dropDownTeam1 ? '-rotate-90' : 'rotate-90'}`} src={RightSchevronWhite} />
          </div>
        </div>
        <div className={`${dropDownTeam1 ? 'pt-2' : ''}`}>
          {dropDownTeam1 &&
            data &&
            data.homepageRecentForm &&
            data.homepageRecentForm.homeTeamData.map((item, y) => (
              <div className='flex my-2 items-center justify-start relative truncate' key={y}>
                {y == 0 && (
                  <div className=' bg-green-6 w-1 h-2 rounded-r-full blur-[1.5px] object-center absolute left--1 ml-1'></div>
                )}
                <div
                  className={`w-6 h-6 rounded-full  ${
                    item.type == 'W' ? 'bg-green' : 'bg-red-5'
                  } mr-2 flex justify-center items-center text-sm font-semibold`}>
                  {item.type}
                </div>
                <div className='w-11/12 flex justify-between py-1'>
                  <div className='flex items-center w-2/5'>
                    <div
                      className={`text-sm font-medium px-1 ${
                        item.winningTeamID == data.homepageRecentForm.homeTeamID ? 'text-white' : 'text-gray-2'
                      }`}>
                      {item.team1}
                    </div>
                    <div className='text-sm font-medium text-gray-2 pr-1'>vs</div>
                    <div
                      className={`text-sm font-medium pr-2 ${
                        item.winningTeamID == data.homepageRecentForm.homeTeamID ? 'text-gray-2' : 'text-white'
                      }`}>
                      {item.team2}
                    </div>
                  </div>
                  <div className='text-sm font-normal w-3/5 flex justify-start '>{item.matchResult}</div>
                </div>
              </div>
            ))}
        </div>
      </div>

      {/* h1  2nw team*/}

      <div className='bg-gray-4 rounded-lg px-3 py-3 mt-2'>
        <div className='flex justify-between items-center' onClick={() => setDropDownTeam2(!dropDownTeam2)}>
          <div className='flex justify-start items-center w-1/4'>
            <img
              className='w-7 h-5 shadow-md'
              src={`https://images.cricket.com/teams/${data && data.homepageRecentForm.awayTeamID}_flag_safari.png`}
              onError={(evt) => (evt.target.src = '/svgs/images/flag_empty.svg')}
            />
            <div className=' text-md font-semibold pl-2'>{data.homepageRecentForm.awayTeamName}</div>
          </div>
          {!dropDownTeam2 && (
            <div className='flex justify-between items-center w-2/4'>
              {data &&
                data.homepageRecentForm &&
                data.homepageRecentForm.awayTeamData.map((item, y) => (
                  <div className='relative' key={y}>
                    <div
                      className={`w-6 h-6 mx-1 rounded-full ${
                        item.type == 'W' ? 'bg-green-3' : 'bg-red-5'
                      } text-center flex justify-center items-center text-sm font-semibold`}>
                      {item.type}
                    </div>
                    {y == 0 && (
                      <div className=' bg-green-6 w-2 h-1 rounded-b-full blur-[1.5px] object-center absolute right-3 left-3 mt-1 '></div>
                    )}
                  </div>
                ))}
            </div>
          )}
          <div>
            <img className={`${dropDownTeam2 ? '-rotate-90' : 'rotate-90'}`} src={RightSchevronWhite} />
          </div>
        </div>
        <div className={`${dropDownTeam2 ? 'pt-2' : ''}`}>
          {dropDownTeam2 &&
            data &&
            data.homepageRecentForm &&
            data.homepageRecentForm.awayTeamData.map((item, y) => (
              <div className='flex my-2 items-center justify-start relative truncate text-sm' key={y}>
                {y == 0 && (
                  <div className=' bg-green-6 w-1 h-2 rounded-r-full blur-[1.5px] object-center absolute left--1 ml-1'></div>
                )}

                <div
                  className={`w-6 h-6 rounded-full  ${
                    item.type == 'W' ? 'bg-green' : 'bg-red-5'
                  } mr-2 flex justify-center items-center text-sm font-semibold`}>
                  {item.type}
                </div>
                <div className='w-11/12 flex justify-between py-1'>
                  <div className='flex items-center w-2/5'>
                    <div
                      className={`text-sm font-medium px-1 ${
                        item.winningTeamID == data.homepageRecentForm.awayTeamID ? 'text-white' : 'text-gray-2'
                      }`}>
                      {item.team1}
                    </div>
                    <div className='text-xs font-medium text-gray-2 pr-1'>vs</div>
                    <div
                      className={`text-sm font-medium pr-2 ${
                        item.winningTeamID == data.homepageRecentForm.awayTeamID ? 'text-gray-2' : 'text-white'
                      }`}>
                      {item.team2}
                    </div>
                  </div>
                  <div className='text-sm font-normal w-3/5 flex justify-start '>{item.matchResult}</div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  ) : (
    <div className='flex items-center justify-center'>

      <DataNotFound />
    </div>
  );
}
