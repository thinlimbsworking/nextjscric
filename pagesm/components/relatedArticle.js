import React from 'react';

import { getArticleTabUrl } from '../api/services';
import CleverTap from 'clevertap-react';

import { useRouter } from 'next/router';
import { format } from 'date-fns';

const RelatedArticle = (article,rArticleEvent,setArticleEvent) => {

const handleNav=()=>{

  CleverTap.initialize('Article', {
    Source: 'Related',
    ArticleType:
  article.type === "match_report"
    ? "MatchReport"
    : article.type === "news"
    ? 
    "News"
    : article.type === "fantasy"
    ? "Fantasy"
    : article.type === "interview"
    ? "Interview"
    : "Others",
    Author: article.author,
    ArticleHeading: article.title ,
    ArticleID: article.articleID,
    Platform: localStorage ? localStorage.Platform : ''
  });
 router.push(getArticleTabUrl(article.article.articleID).href, getArticleTabUrl(article.article.articleID).as)
}
  const router = useRouter();
  return (
    <div
      onClick={handleNav}
      className='px-3 flex flex-row items-center'
      style={{ flex: 1 }}>
      <div className='border-2 flex relative cover bg-top' style={{ height: '7rem', flex: 0.4 }}>
        <img className='h-full w-full' src={`${article.article.sm_image_safari}?auto=compress&dpr=2&fit=crop&crop=faces&w=100&h=112`} alt='safari' />
        {article && article.matchName && (
          <div
            className='absolute text-xs bottom-1 left-0 white 
          pl-2 py-1 pr-3 bg-red uppercase border-r-2'
            style={{ clipPath: 'polygon(0 0,100% 0,92% 100%,0 100%)' }}>
            {article.matchName}
          </div>
        )}
        <div
          className=' inblock border-2 text-xs absolute flex  px-2  
        bottom-0 mb-1 cdcgr white br--right p-1'>
          {article.article.type.split('_').splice(-1)[0].toUpperCase()}
        </div>
      </div>
      <div className='pl-2' style={{ flex: 0.6 }}>
        <div className='text-xs grey_10 font-semibold pb-1 pr-1 lh-title text' style={{ WebkitLineClamp: 2, height: '2.1rem' }}>
          {article && (article.article.title || '')}
        </div>
        <div className='font-light lh-title text h-2' style={{ fontSize: 11, WebkitLineClamp: 2 }}>
          {article && (article.article.description || '')}
        </div>
        <div className='mt-1'>
          <img src={'/svgs/user.svg'} alt='user' className='o-50' height='10' />
          <span className='font-light text-xs gray i mt-2 pl-1'>{article && (article.article.author || '')}</span>
        </div>
        <div className='text-xs font-normal mt-1'>{format(+article.article.createdAt, 'dd MMM yyyy')}</div>
      </div>
    </div>
  );
};

export default RelatedArticle;
