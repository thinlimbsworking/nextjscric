import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { MINI_SCORE_CARD_HOME } from '../api/queries';
export default function Scorecard(props) {
  const { loading, error, data } = useQuery(MINI_SCORE_CARD_HOME, {
    variables: { matchID: props.matchID },
    // pollInterval:  8000
  });
 
  // console.log("datasssssssssssssssssssss2s",data,props)
  
  return data && data.miniScoreCard && data.miniScoreCard.batting && data.miniScoreCard.batting.length > 0 ? (
    <div className='bg-gray mx-2 mt-5 rounded px-3 py-3 text-white'>
      <div className='flex items-center justify-between'>
        <div className='font-semibold text-xl tracking-wider  '>Scorecard</div>
        <div className='flex justify-center rounded  bg-basebg p-1  items-center  cursor-pointer '>
          <img className='w-6' src='/pngsV2/arrow.png' alt='' />
        </div>
      </div>
      <div className='mt-2 bg-gray-8 p-2 flex items-center rounded justify-between'>
        <div className='text-md font-semibold'>{`${
          data.miniScoreCard.data[0].matchScore.filter(
            (o) => o.teamID == data.miniScoreCard.data[0].currentInningteamID
          )?.[0]?.teamShortName || ''
        } Score`}</div>
        <div className='text-sm font-medium'>
          {`${
            data.miniScoreCard.data[0].matchScore.filter(
              (o) => o.teamID == data.miniScoreCard.data[0].currentInningteamID
            )?.[0]?.teamScore?.[0]?.runsScored
          }/${
            data.miniScoreCard.data[0].matchScore.filter(
              (o) => o.teamID == data.miniScoreCard.data[0].currentInningteamID
            )?.[0]?.teamScore?.[0]?.wickets || 0
          }`}{' '}
          (
          {data.miniScoreCard.data[0].matchScore.filter(
            (o) => o.teamID == data.miniScoreCard.data[0].currentInningteamID
          )?.[0]?.teamScore?.[0]?.overs || 0}
          )
        </div>
      </div>

<div className='flex w-full flex-col lg:flex-row md:flex-row items-start justify-between '>
      <div className='mt-4 w-full md:w-6/12 lg:w-6/12    bg-gray-8 rounded-lg'>
        <div className='flex items-center  justify-between p-2 py-3'>
          <div className='text-sm font-semibold w-5/12'>BATTER</div>
          <div className='flex items-center justify-between w-7/12 text-sm font-semibold '>
            <div className='w-1/5 text-center'>R</div>
            <div className='w-1/5 text-center'>B</div>
            <div className='w-1/5 text-center'>S/R</div>
            {props.matchType !== 'Test' ? (
              <div className='w-2/5 text-center'>Impact</div>
            ) : (
              <div className='flex justify-around w-2/5'>
                <div className='w-1/2 text-center'>4s</div>
                <div className='w-1/2 text-center'>6s</div>
              </div>
            )}
          </div>
        </div>
        <div className='bg-gray-4 p-2 '>
          {data.miniScoreCard.batting.map((player, i) => (
            <div key={i} className='flex items-center  justify-between py-2.5 relative'>
               {player.playerOnStrike && <div className=' bg-green-6 w-2 h-2 rounded-r-full blur-[1.5px] object-center absolute left-[-8px] '></div>}
              <div
                className={`text-xs  w-5/12 pl-1  ${
                  player.playerOnStrike ? 'text-white font-medium' : 'text-gray-2 font-medium'
                }`}>
                {player.playerName} {player.playerOnStrike ? '*' : ''}
              </div>
              <div className='flex items-center justify-between w-7/12 text-xs font-medium'>
                <div className='w-1/5 text-center'>{player.runs}</div>
                <div className='w-1/5 text-center text-gray-2'>{player.playerMatchBalls}</div>
                <div className='w-1/5 text-center text-gray-2 text-xs'>{player.playerMatchStrikeRate}</div>
                {props.matchType !== 'Test' ? (
                  player.impact ? (
                    <div className={`w-2/5  flex justify-center  text-xs`}>
                      <div
                        className={`rounded flex justify-center items-center pl-1 py-0.5 ${
                          player.arrow === 'upward' ? 'bg-green' : 'bg-red'
                        }`}>
                        <div className={` text-center rounded `}>{player.impact}</div>
                        <img
                          className={`w-4 h-4 ${player.arrow === 'upward' ? '' : 'rotate-90'}`}
                          src='/pngsV2/arrow_tail.png'
                          alt=''
                        />
                      </div>
                    </div>
                  ) : (
                    <div className='w-2/5 flex justify-center '>
                      <div className=' bg-gray px-3 rounded'>--</div>{' '}
                    </div>
                  )
                ) : (
                  <div className='w-2/5 flex justify-around items-center text-gray-2'>
                    <div className='w-1/2 text-center'>{player.fours}</div>
                    <div className='w-1/2 text-center'>{player.sixes}</div>
                  </div>
                )}
              </div>
            </div>
          ))}
        </div>
        <div className='mt-0.5 bg-gray-4 rounded-b-lg text-gray-2 text-sm font-medium flex justify-between items-center py-3 px-2'>
          <div className='text-white'>P’ship: {data.miniScoreCard.partnership}</div>
          <div className=''>CRR: {data.miniScoreCard.runRate}</div>
          <div className=''>RRR: {data.miniScoreCard.rRunRate || '- -'} </div>
        </div>
      </div>

      <div className='mt-4 w-full  md:w-6/12 lg:w-6/12 bg-gray-8 rounded-lg lg:ml-2 md:ml-2'>
        <div className='flex items-center  justify-between p-2 py-3'>
          <div className='text-sm font-semibold w-5/12'>BOWLER</div>
          <div className='flex items-center justify-between w-7/12 text-sm font-semibold '>
            <div className='w-1/5 text-center'>O</div>
            <div className='w-1/5 text-center'>R</div>
            <div className='w-1/5 text-center'>W</div>
            {props.matchType !== 'Test' ? (
              <div className='w-2/5 text-center'>Impact</div>
            ) : (
              <div className='flex justify-around w-2/5'>
                <div className='w-1/2 text-center'>M</div>
                <div className='w-1/2 text-center'>ER</div>
              </div>
            )}
          </div>
        </div>
        <div className='bg-gray-4 p-2 rounded-b-lg'>
          <div className='flex items-center  justify-between py-2.5'>
            <div className='text-sm font-medium w-5/12 tracking-wide '>{data.miniScoreCard.bowling[0].playerName}</div>
            <div className='flex items-center justify-between w-7/12 text-sm font-medium'>
              <div className='w-1/5 text-center text-white'>{data.miniScoreCard.bowling[0].overs}</div>
              <div className='w-1/5 text-center text-gray-2'>{data.miniScoreCard.bowling[0].RunsConceeded}</div>
              <div className='w-1/5 text-center '>{data.miniScoreCard.bowling[0].wickets}</div>
              {props.matchType !== 'Test' ? (
                data.miniScoreCard.bowling[0].impact ? (
                  <div className={`w-2/5 flex justify-center  py-0.5 text-xs`}>
                    <div
                      className={`flex py-0.5 items-center rounded pl-1 ${
                        data.miniScoreCard.bowling[0].arrow === 'upward' ? 'bg-green' : 'bg-red'
                      }`}>
                      <div className={` text-center rounded  `}>{data.miniScoreCard.bowling[0].impact}</div>
                      <img
                        className={`w-4 h-4 ${data.miniScoreCard.bowling[0].arrow === 'upward' ? '' : 'rotate-90'}`}
                        src='/pngsV2/arrow_tail.png'
                        alt=''
                      />
                    </div>
                  </div>
                ) : (
                  <div className='w-2/5 flex justify-center '>
                    <div className=' bg-gray px-3 rounded'>--</div>{' '}
                  </div>
                )
              ) : (
                <div className='w-2/5 flex justify-around items-center text-gray-2'>
                  <div className='w-1/2 text-center'>{data.miniScoreCard.bowling[0].maiden}</div>
                  <div className='w-1/2 text-center'>{data.miniScoreCard.bowling[0].economy}</div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
