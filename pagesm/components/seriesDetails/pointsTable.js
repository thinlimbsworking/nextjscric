import React from 'react';
const flagPlaceHolder = '/pngsV2/flag_dark.png';
import DataNotFound from '../commom/datanotfound';
export default function PointsTable({ browser, ...props }) {
  const { loading, error, data } = props;
  let localStorage = {};
  

  if (loading) return <div></div>;
  if (error)
    return (
      <div className='w-full flex flex-col  justify-center items-center'>
       <DataNotFound/>
      </div>
    );
  else
    return (
      <div>
        {data ? (
          <div className='rounded-md bg-gray-8 mx-2'>
       
            {data.getPointsTable.standings.map((group, j) => (
              <div className='mb-3' key={j}>
                {data.getPointsTable.standings.length > 1 && (
                  <div className='rounded-md flex justify-between white text-sm text-gray-2 p-3'>{group.name}</div>
                )}
                <div className='w-full justify-between rounded-md bg-gray-4' style={{ borderSpacing: 0 }}>
                    <div className='rounded-t-md  flex items-center  w-full'>
                      <div className='py-3 font-semibold text-sm w-6/12  px-3'>TEAMS</div>
                      <div className='font-semibold text-sm tr  w-1/12 text-center'>P</div>
                      <div className='font-semibold text-sm tr  w-1/12 text-center'>W</div>
                      <div className='font-semibold text-sm tr  w-1/12 text-center'>L</div>
                      <div className='font-semibold text-sm tr  w-1/12 text-center'>PTS</div>
                      <div className='font-semibold text-sm tr  w-2/12 text-center'>NRR</div>
                  </div>
                  <div>
                    {group.teams.map((x, i) => (
                      <div key={i} className={`flex items-center  w-full ${i < group.teams.length - 1 && 'border-b border-gray-2'} bg-gray`}>
                        <div className={`p-3 w-6/12 flex gap-2 `}>
                            {x.isQualified && (
                              <div className='white text-xs w-5 font-semibold bg-green flex items-center justify-center' >
                                Q
                              </div>
                            ) }
                            <div className='flex justify-start items-center w-2/3'>
                              <img
                                alt=''
                                src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
                                onError={(evt) => (evt.target.src = flagPlaceHolder)}
                                className='h-5 w-8 '
                              />
                              <span className='font-semibold text-xs pl-2 '>{x.teamShortName}</span>
                            </div>
                          </div>
                        <div className=' f6-l f7 fw4 tr w-1/12 text-center'>{x.all}</div>
                        <div className=' f6-l f7   tr  w-1/12 text-center'>{x.wins}</div>
                        <div className=' f6-l f7   tr w-1/12 text-center'>{x.lost}</div>
                        <div className=' f6- f7  tr w-1/12 text-center'>{x.points}</div>
                        <div className=' f6-l f7 tr w-2/12 text-center'>{x.nrr}</div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div className=' bg-white py-3'>
            <div>
              <img
                className='w-6 h-6'
                style={{ margin: 'auto', display: 'block' }}
                src={ground}
                alt='loading...'
              />
            </div>
            <div className='text-center py-2 text-xl font-bold'>Data Not Available</div>
          </div>
        )}
      </div>
    );
}
