import React from 'react';
import ArticleCard from '../ArticleCard';
// import { navigate } from '@reach/router'
import CleverTap from 'clevertap-react';
import Link from 'next/link';
import { getNewsUrl,getLiveArticleUrl } from '../../api/services';

export default function SeriesArticles(props) {
  const { loading, error, data } = { data: props.data };
 
  const handleCleverTab = (article) => {
    CleverTap.initialize('Article', {
      Source: 'Series',
      // MatchID: String(article.matchIDs),
      // SeriesID: String(article.seriesIDs),
      // PlayerID: String(article.playerIDs),
      ArticleType: article.type,
      Author: article.author,
      ArticleHeading: article.title,
      ArticleID: article.articleID,
      Platform: localStorage ? localStorage.Platform : ''
    });
  };

  if (error)
    return (
      <div className='w-100 vh-100 fw2 f7 gray flex flex-column  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  if (loading) return <div className='w-100 vh-100 fw2 f7 gray flex justify-center items-center'>LOADING...</div>;
  else
    return (
      <div className='mh0-ns cursor-pointer'>
        {data.getArticlesByIdAndType.length ? (
          <div>
            {data.getArticlesByIdAndType.map((article, i) => (
              <>
              {article.type != 'live-blogs' && article.type != 'live-blog' ?<Link {...getNewsUrl(article, 'match-related')} passHref>

                <div
                  onClick={() => {
                    handleCleverTab(article);
                  }}>
                  <ArticleCard article={article} />
                </div>

              </Link>: <Link {...getLiveArticleUrl(article)} passHref>

                <div
                  onClick={() => {
                    handleCleverTab(article);
                  }}>
                  <ArticleCard article={article} />
                </div>

              </Link>}
              </>
            ))}
          </div>
        ) : (
          <div className='flex flex-column justify-center items-center'>
            <div>
              <img className='w5 pl3 h5' src='/svgs/Empty.svg' alt='' />
            </div>
            <div className='gray f6 pb1'>No Articles found</div>
          </div>
        )}
      </div>
    );
}
