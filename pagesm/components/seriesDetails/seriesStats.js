import React, { useState } from 'react';
// import ellipse from "`../../public/svgs/Ellipse.svg";
// import SeriesBattingStats from "../SeriesDetails/SeriesBattingStats";
// import fielding from "`../../public/svgs/fielding.svg";
// import redCap from "`../../public/svgs/rCap.svg";
// import purpleCap from "`../../public/svgs/pCap.svg";
// import { TOP_GUNS } from "../../api/queries";
// const Empty = "/svgs/Empty.svg";
import CleverTap from 'clevertap-react';
import { useRouter } from 'next/router';
import { seriesViewStat, getSeries } from '../../api/services';
import { Background } from 'react-imgix';
const IMAGES = {
  fallback: '/placeHodlers/playerAvatar.png',
  circle: '/svgs/stats-circle.svg'
};

export default function SeriesStats(props) {
  const { seriesID, path, seriesType, slug, seriesName, data, error } = props;
  let matchType;
  if (
    data &&
    data.getStatsResolver &&
    data.getStatsResolver.formatArray &&
    data.getStatsResolver.formatArray.length > 0
  ) {
    matchType = data.getStatsResolver.formatArray[0];
  }

  let [type, setType] = useState(matchType);
  const router = useRouter(data.getStatsResolver.formatArray[0]);

  const handleViewNavigation = (types, view) => {
    CleverTap.initialize('SeriesStats', {
      Source: 'SeriesStatsHome',
      StatsType: view,
      Platform: localStorage.Platform
    });
    let sendData = {
      seriesID,
      seriesType,
      seriesID,
      type,
      types,
      slug
    };
    router.push(seriesViewStat(sendData).href, `${seriesViewStat(sendData).as}/${view}`);
  };
  const handleNavigation = (types) => {
    CleverTap.initialize(`Series${types}Stats`, {
      Source: 'SeriesStatsHome',
      Platform: localStorage.Platform
    });
    let sendData = {
      seriesID,
      seriesType,
      seriesID,
      type,
      types,
      slug
    };

    router.push(seriesViewStat(sendData).href, seriesViewStat(sendData).as);
  };

  if (error)
    return <div className='w-100 vh-100 fw2 f7 gray flex justify-center items-center'>Something went wrong.</div>;
  return (
    <div className=' mh0-ns'>
      {/* <div className="bg-series-stats w-100"> */}
      <div className='relative w-100 z-1'>
        <div className='db-ns dn absolute top-0 bottom-0 left-0 right-0 br2 ' style={{ zIndex: -1 }}></div>
        {data.getStatsResolver && data.getStatsResolver.formatArray && data.getStatsResolver.formatArray.length > 1 ? (
          <div className=' w-100 pb2 pt3 relative flex items-center justify-center'>
            <div className='z-1'>
              {/* { data.getStatsResolver.formatArray.map((tabName, i) => */}
              {data.getStatsResolver.formatArray.map((tabName, i) => (
                <span
                  key={i}
                  className={`black-20 cursor-pointer f8 fw6 pa2 ph3 w-100 tc ba b--black-10 ttu  ${
                    type === tabName ? 'red_Orange cdc' : ''
                  }`}
                  onClick={() => {
                    data && data.getStatsResolver[tabName] && setType(tabName);
                  }}>
                  {tabName}
                </span>
              ))}
            </div>
            <div className='w-100 divider absolute' />
          </div>
        ) : null}
        {data && data.getStatsResolver && data.getStatsResolver[type] && data.getStatsResolver[type].topGuns&& <div className='fw5 f6 bg-gray ph2 pv2'>Top Guns</div>}

        {data && data.getStatsResolver && data.getStatsResolver[type] && data.getStatsResolver[type].topGuns ? (
          <div className='flex  flex-wrap justify-content flex-start pv3-ns ph3-ns'>
            {/* <div className="flex w-100 justify-around"> */}
            {data.getStatsResolver[type].topGuns.Most_Runs && (
              <div className='pa3-ns w-50 w-25-ns'>
                <div
                  className='bg-gray-4 relative   shadow-4  db shadow-4 br2  flex flex-column items-center'
                  onClick={() => {
                    handleViewNavigation('Batting', 'most_runs');
                  }}>
                  <div>
                    <img className='absolute  right-0' src='/svgs/rCap.svg' alt=''></img>
                  </div>
                  <div
                    className=' p-2  w44 overflow-hidden   '
                    style={{
                      backgroundImage: `url(${IMAGES.circle})`,
                      backgroundPosition: 'center top',
                      backgroundRepeat: 'no-repeat',
                      display: 'flex',
                      justifyContent: 'center',
                      overflow: 'hidden',
                      height: '5.5rem',
                      backgroundSize: 'contain'
                    }}>
                    <img
                      className='h4'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type].topGuns.Most_Runs.player_id}_headshot_safari.png`}
                      onError={(evt) => (evt.target.src = '/placeHodlers/playerAvatar.png')}
                      alt=''></img>
                  </div>

                  <div className='darkRed pv1 fw6 f4'>{data.getStatsResolver[type].topGuns.Most_Runs.runs_scored}</div>
                  <div className=' darkRed pv1 f7'>Most Runs</div>
                  <div className='pv1 ph1 fw6 nowrap f6  tc'>
                    {data.getStatsResolver[type].topGuns.Most_Runs.player_name}
                  </div>
                  <div className='text-gray-2 font-semibold f7 pb3-ns pb1 fw4'>
                    {data.getStatsResolver[type].topGuns.Most_Runs.team_short_name}
                  </div>
                </div>
              </div>
            )}
            {data.getStatsResolver[type].topGuns.Most_Wickets && (
              <div className='pa3-ns w-50 w-25-ns'>
                <div
                  className='bg-gray-4 relative   shadow-4  db shadow-4 br2  flex flex-column items-center'
                  onClick={() => {
                    handleViewNavigation('Bowling', 'most_wickets');
                  }}>
                  <div>
                    <img className='absolute  right-0' src='/svgs/pCap.svg' alt=''></img>
                  </div>
                  <div
                    className=' p-2  w44 overflow-hidden   '
                    style={{
                      backgroundImage: `url(${IMAGES.circle})`,
                      backgroundPosition: 'center top',
                      backgroundRepeat: 'no-repeat',
                      display: 'flex',
                      justifyContent: 'center',
                      overflow: 'hidden',
                      height: '5.5rem',
                      backgroundSize: 'contain'
                    }}>
                    <img
                      className='h4'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type].topGuns.Most_Wickets.player_id}_headshot_safari.png`}
                      onError={(evt) => (evt.target.src = IMAGES.fallback)}
                      alt=''></img>
                  </div>

                  <div className='darkRed pv1 fw6 f4'>{data.getStatsResolver[type].topGuns.Most_Wickets.wickets}</div>
                  <div className=' darkRed pv1 f7'>Most Wickets</div>
                  <div className='pv1 ph1 fw6 nowrap f6  tc'>
                    {data.getStatsResolver[type].topGuns.Most_Wickets.player_name}
                  </div>
                  <div className='text-gray-2 font-semibold f7 pb3-ns pb1 fw4'>
                    {data.getStatsResolver[type].topGuns.Most_Wickets.team_short_name}
                  </div>
                </div>
              </div>
            )}
            {data.getStatsResolver[type].topGuns.Highest_Score && (
              <div className='pa3-ns w-50 w-25-ns'>
                <div
                  className='bg-gray-4   shadow-4  mt2 mt0-ns db shadow-4 br2  flex flex-column items-center'
                  onClick={() => {
                    handleViewNavigation('Batting', 'highest_score');
                  }}>
                  <div
                    className=' p-2  w44 overflow-hidden   '
                    style={{
                      backgroundImage: `url(${IMAGES.circle})`,
                      backgroundPosition: 'center top',
                      backgroundRepeat: 'no-repeat',
                      display: 'flex',
                      justifyContent: 'center',
                      overflow: 'hidden',
                      height: '5.5rem',
                      backgroundSize: 'contain'
                    }}>
                    <img
                      className='h4'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type].topGuns.Highest_Score.player_id}_headshot_safari.png`}
                      onError={(evt) => (evt.target.src = IMAGES.fallback)}
                      alt=''></img>
                  </div>

                  <div className='darkRed pv1 fw6 f4'>
                    {data.getStatsResolver[type].topGuns.Highest_Score.highest_score}
                  </div>
                  <div className=' darkRed pv1 f7'>Highest Score</div>
                  <div className='pv1 ph1 fw6 nowrap f6  tc'>
                    {data.getStatsResolver[type].topGuns.Highest_Score.player_name}
                  </div>
                  <div className='text-gray-2 font-semibold f7 pb3-ns pb1 fw4'>
                    {data.getStatsResolver[type].topGuns.Highest_Score.team_short_name}
                  </div>
                </div>
              </div>
            )}
            {data.getStatsResolver[type].topGuns.Best_figures && (
              <div className='pa3-ns w-50 w-25-ns'>
                <div
                  className='bg-gray-4 relative  shadow-4 mt2 mt0-ns db shadow-4 br2  flex flex-column items-center'
                  onClick={() => {
                    handleViewNavigation('Bowling', 'best_figures');
                  }}>
                  <div
                    className=' p-2  w44 overflow-hidden   '
                    style={{
                      backgroundImage: `url(${IMAGES.circle})`,
                      backgroundPosition: 'center top',
                      backgroundRepeat: 'no-repeat',
                      display: 'flex',
                      justifyContent: 'center',
                      overflow: 'hidden',
                      height: '5.5rem',
                      backgroundSize: 'contain'
                    }}>
                    <img
                      className='h4'
                      src={`https://images.cricket.com/players/${data.getStatsResolver[type].topGuns.Best_figures.player_id}_headshot_safari.png`}
                      onError={(evt) => (evt.target.src = IMAGES.fallback)}
                      alt=''></img>
                  </div>

                  <div className='darkRed pv1 fw6 f4'>
                    {data.getStatsResolver[type].topGuns.Best_figures.best_bowling_figures}
                  </div>
                  <div className=' darkRed pv1 f7'>Best Figures</div>
                  <div className='pv1 ph1 fw6 nowrap f6  tc'>
                    {data.getStatsResolver[type].topGuns.Best_figures.player_name}
                  </div>
                  <div className='text-gray-2 font-semibold f7 pb3-ns pb1 fw4'>
                    {data.getStatsResolver[type].topGuns.Best_figures.team_short_name}
                  </div>
                </div>
              </div>
            )}

            {/* </div> */}
          </div>
        ) : (
          <div className='flex  justify-center items-center flex-column mv4 w-100 bg-gray text-white'>
            <img className='w46 h47' src='/svgs/Empty.svg' alt='' />
            <p className='f6 tc fw4'>Series Stats will be available once the series commences</p>
          </div>
        )}
      </div>

      <div className='flex-ns mt3-ns justify-between-ns justify-around-ns flex-row-ns ph0-ns flex-column'>
        {data && data.getStatsResolver  && data.getStatsResolver[type] && data.getStatsResolver[type].Batting && <div
          className='bg-gray-4 mt2 mr2-ns mt0-ns ph1 flex-column-ns rounded border border-red-5 flex justify-between w-100 items-center pv2 pv3-ns '
          style={{ borderColor: '#C63935' }}
          onClick={() => {
            data && data.getStatsResolver && data.getStatsResolver[type] && handleNavigation('Batting');
          }}>
          <div className='flex flex-column-ns items-center'>
            <div className='relative   flex items-center justify-center z-0'>
              <img className='' src='/svgs/Ellipse.svg' alt=''></img>
              <img className='absolute z-1  ' src='/svgs/Bat_Icon.svg' alt=''></img>
            </div>
            <div className='pv1 fw5 f7 f5-ns pl2 pl0-ns pv3-ns'>Batting</div>
          </div>
          <img className='' src='/svgs/RightSchevronBlack.svg' alt=''></img>
        </div>
        }

        {data && data.getStatsResolver && data.getStatsResolver[type] &&  data.getStatsResolver[type].Bowling && <div
          className='bg-gray-4  mv2 mv0-ns ph1 flex-column-ns  w-100  flex justify-between  items-center pv2 pv3-ns rounded border border-red-5 '
          style={{ borderColor: '#C63935' }}
          onClick={() => {
            data && data.getStatsResolver && data.getStatsResolver[type] && handleNavigation('Bowling');
          }}>
          <div className='flex flex-column-ns items-center'>
            <div className='relative   flex items-center justify-center z-0'>
              <img className='' src='/svgs/Ellipse.svg' alt=''></img>
              <img className='absolute z-1  ' src='/svgs/bowling.svg' alt=''></img>
            </div>
            <div className='pv1 pl2 pl0-ns fw5 f7 f5-ns pv3-ns'>Bowling</div>
          </div>
          <img className='' src='/svgs/RightSchevronBlack.svg' alt=''></img>
        </div>}
        {data &&
          data.getStatsResolver &&
          data.getStatsResolver.tourType &&
          data.getStatsResolver.tourType.length > 0 &&
          data.getStatsResolver.tourType === 'Tournament' && data && data.getStatsResolver && data.getStatsResolver[type]&&  data.getStatsResolver[type].fielding  && (
            <div
              className='bg-gray-4 ml2-ns pv3-ns ph1 mb2 mb0-ns  w-100 flex-column-ns rounded border border-red-5 flex justify-between  items-center pv2  '
              style={{ borderColor: '#C63935' }}
              onClick={() => {
                data && data.getStatsResolver && data.getStatsResolver[type] && handleNavigation('Fielding');
              }}>
              <div className='flex flex-column-ns items-center'>
                <div className='relative   flex items-center justify-center z-0'>
                  <img className='' src='/svgs/Ellipse.svg' alt=''></img>
                  <img className='absolute z-1  ' src='/svgs/fielding.svg' alt=''></img>
                </div>
                <div className='pv1 pl2 pl0-ns fw5 f7 f5-ns pv3-ns'>Fielding</div>
              </div>
              <img className='' src='/svgs/RightSchevronBlack.svg' alt=''></img>
            </div>
          )}
      </div>
    </div>
  );
}
