import React, { useState, useEffect } from 'react';

import { TOP_GUNS } from '../../api/queries';

import { useRouter } from 'next/router';

import { seriesViewStatBest } from '../../api/services';

import CleverTap from 'clevertap-react';

const RightSchevronBlack = '/svgs/RightSchevronBlack.svg';
const left = '/svgs/left.svg';
const right = '/svgs/right.svg';
const fallbackProjection = '/pngs/fallbackprojection.png';
const fallback = '/placeHodlers/playerAvatar.png';
const backIconBlack = '/svgs/backIconWhite.svg';

export default function SeriesView(props) {
  var { type, format, slug, seriesID, scoretype, data, seriesType, types } = props;

  // seriesID={props.seriesID}
  //    seriesType={props.type}

  //    slug={props.seriesSlug}

  //     type={props.slugs[5]}
  //      format={props.slugs[4]}
  //      scoretype={props.slugs[6]}

  //      data={props.comp.data}
  // scoretype=scoretype.toLowerCase();

  const router = useRouter();

  let cricketType = type;

  type = type.toLowerCase();

  let caseHandle = {
    most_runs: 'Most_Runs',
    highest_score: 'Highest_Score',
    most_fifties: 'Most_Fifties',
    most_hundreds: 'Most_Hundreds',
    most_4s: 'Most_4s',
    most_6s: 'Most_6s',
    best_batting_average: 'Best_Batting_Average',
    best_strike_rate: 'Best_Strike_Rate',
    most_wickets: 'Most_Wickets',
    best_figures: 'Best_figures',
    five_wicket_haul: 'Five_Wicket_Haul',
    three_wicket_haul: 'Three_Wicket_Haul',
    best_bowling_average: 'Best_Bowling_Average',
    best_economy_rates: 'Best_Economy_Rates',
    best_bowling_strike_rate: 'Best_Bowling_Strike_Rate',
    most_catches: 'Most_Catches',
    most_run_outs: 'Most_Run_Outs',
    most_dismissals: 'Most_Dismissals'
  };

  const category = {
    Most_Runs: ['pos', 'player', 'mat', 'inns', 'runs'],
    Highest_Score: ['pos', 'player', 'HS', 'opponent'],
    Most_Fifties: ['pos', 'player', 'mat', 'runs', '50s'],
    Most_Hundreds: ['pos', 'player', 'mat', 'runs', '100s'],
    Most_4s: ['pos', 'player', 'mat', 'runs', '4s'],
    Most_6s: ['pos', 'player', 'mat', 'runs', '6s'],
    Best_Batting_Average: ['pos', 'player', 'mat', 'runs', 'Avg.'],
    Best_Strike_Rate: ['pos', 'player', 'mat', 'runs', 's/r'],
    Most_Wickets: ['pos', 'player', 'mat', 'Wkt', 'overs'],
    Best_figures: ['pos', 'player', 'best', 'opponent'],
    Five_Wicket_Haul: ['pos', 'player', 'mat', 'Wkt', '5W'],
    Three_Wicket_Haul: ['pos', 'player', 'mat', 'Wkt', '3W'],
    Best_Bowling_Average: ['pos', 'player', 'mat', 'Wkt', 'Avg'],
    Best_Economy_Rates: ['pos', 'player', 'mat', 'Wkt', 'E/R'],
    Best_Bowling_Strike_Rate: ['pos', 'player', 'mat', 'Wkt', 'S/R'],
    Most_Catches: ['pos', 'player', 'mat', 'catches'],
    Most_Run_Outs: ['pos', 'player', 'mat', 'R/O'],
    Most_Dismissals: ['pos', 'player', 'mat', 'dismissals']
  };

  const heading = {
    Most_Wickets: 'Most Wickets',
    Highest_Score: 'Highest Score',
    Most_Runs: 'Most Runs',
    Most_Fifties: 'Most fifties',
    Most_Hundreds: 'Most hundreds',
    Most_4s: 'Most 4s',
    Most_6s: 'Most 6s',
    Best_Batting_Average: 'Batting Average',
    Best_Strike_Rate: 'Batting Strike-Rite',
    Best_figures: 'Best Figures',
    Five_Wicket_Haul: '5W Haul',
    Three_Wicket_Haul: '3W Haul',
    Best_Bowling_Average: 'Bowling Average',
    Best_Economy_Rates: 'Bowling Economy Rates',
    Best_Bowling_Strike_Rate: 'Bowling Strike Rate',
    Most_Catches: ' Catches',
    Most_Run_Outs: 'Run-Outs ',
    Most_Dismissals: 'Dismissals'
  };
  const scores = {
    Most_Runs: 'innings_played',
    Highest_Score: 'vs_team_short_name',
    Most_Fifties: 'fifties',
    Most_Hundreds: 'hundred',
    Most_4s: 'fours',
    Most_6s: 'sixes',
    Best_Batting_Average: 'average',
    Best_Strike_Rate: 'batting_strike_rate',
    Best_figures: 'vs_team_short_name',
    Five_Wicket_Haul: 'five_wickets_haul',
    Three_Wicket_Haul: 'three_wickets_haul',
    Best_Bowling_Average: 'best_bowling_average',
    Best_Economy_Rates: 'economy',
    Best_Bowling_Strike_Rate: 'bowling_strike_rate',
    Most_Catches: 'most_catches',
    Most_Run_Outs: 'most_run_outs',
    Most_Dismissals: 'most_dismissals',
    Most_Wickets: 'overs'
  };
  const colorMap = {
    IND: '#0062A7',
    AUS: '#E8BF00',
    WI: '#890633',
    PAK: '#2A562C',
    ENG: '#6BB9DE',
    NZ: '#0B0F0E',
    SA: '#006F5C',
    SL: '#20447A',
    AFG: '#1049C3',
    BAN: '#00956C',
    MI: '#1473AD',
    KXIP: '#B32229',
    CSK: '#FCC032',
    RCB: '#1D1D1D',
    DC: '#D41C27',
    SRH: '#F7A34F',
    RR: '#F064A1',
    RCB: '#39235C'
  };
  let array = [];

  const uppercase = (str) => {
    var array1 = str.split('_');
    var newarray1 = [];

    for (var x = 0; x < array1.length; x++) {
      newarray1.push(array1[x].charAt(0).toUpperCase() + array1[x].slice(1));
    }
    return newarray1.join('_');
  };

  if (data && data.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType]) {
    let category = Object.keys(data.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType]);
    category.map((nav, i) => {
      if (
        data.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType][nav] &&
        data.getStatsResolver[format][cricketType === 'Fielding' ? 'fielding' : cricketType][nav].length > 0
      ) {
        array.push(nav.toLowerCase());
      }
    });
  }

  let currentIndex = array.indexOf(scoretype.toLowerCase());
  const changeIndexForward = () => {
    let types = uppercase(array[currentIndex === array.length - 1 ? 0 : currentIndex + 1]);

    const name = type;
    const typeCapitalized = name.charAt(0).toUpperCase() + name.slice(1);
    type = typeCapitalized;

    let sendData = { seriesType, seriesID, slug, type, format, types };

    router.push(seriesViewStatBest(sendData).href, seriesViewStatBest(sendData).as);

    CleverTap.initialize('SeriesStats', {
      Source: `Series${cricketType}Stats`,
      StatsType: array[currentIndex === array.length - 1 ? 0 : currentIndex + 1],
      Platform: localStorage.Platform
    });
  };
  const changeIndexBackward = () => {
    let types = uppercase(array[currentIndex === 0 ? array.length - 1 : currentIndex - 1]);

    const name = type;
    const typeCapitalized = name.charAt(0).toUpperCase() + name.slice(1);
    type = typeCapitalized;

    let sendData = { seriesType, seriesID, slug, type, format, types };

    router.push(seriesViewStatBest(sendData).href, seriesViewStatBest(sendData).as);

    CleverTap.initialize('SeriesStats', {
      Source: `Series${cricketType}Stats`,
      StatsType: array[currentIndex === 0 ? array.length - 1 : currentIndex - 1],
      Platform: localStorage.Platform
    });
  };

  return (
    <div className='mh7-ns text-white'>
      <div className=' flex justify-between md:hidden lg:hidden items-center'>
        <div className='w2-5 h2-4 flex justify-center items-center cursor-pointer'>
          <img className=' md:hidden lg:hidden cursor-pointer' onClick={() => window.history.back()} src={backIconBlack} alt='back icon' />
        </div>
        <span className='f6 fw5 mv0 flex-auto  pl-2 truncate'>{`Series ${cricketType} Stats`}</span>
      </div>

      <div className='w-full shadow-4   br2'>
        <div className='py-1 w-full flex flex-between justify-center'>
          <div
            className={`f7   rotate-180  p-2`}
            onClick={() => {
              changeIndexBackward();
            }}>
            &#10148;
          </div>
          <div className='w-full f6 fw5 flex justify-center items-center'>
            {`${heading[caseHandle[scoretype.toLowerCase()]]}`}
          </div>
          <div
            className={`f7   p-2`}
            onClick={() => {
              changeIndexForward();
            }}>
            &#10148;
          </div>
        </div>

        <div className='w-full bg-gray f9 ttu white p-2 flex  justify-between'>
          {category &&
            category[caseHandle[scoretype]] &&
            category[caseHandle[scoretype]].map((text, i) => (
              <div
                className={`  ${
                  i === 1
                    ? 'w-60'
                    : category[caseHandle[scoretype]].length == 5
                    ? 'flex flex-col w-10 items-center '
                    : i == 0
                    ? ' flex flex-col items-center w-10'
                    : ' flex flex-col items-center w-15'
                }  `}
                key={i}>
                {text}
              </div>
            ))}
        </div>

        {type === 'batting' &&
          data.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]].map((scoreType, i) => (
            <div className='bg-gray-4'>
              <div
                className={`flex  px-2 fw6 ${i === 0 ? 'white py-3' : ''} items-center f7 justify-between`}
                style={{
                  background:
                    i !== 0
                      ? ''
                      : colorMap[scoreType.team_short_name]
                      ? colorMap[scoreType.team_short_name]
                      : '#797979'
                }}
                key={i}>
                <div className={` w-10 flex flex-col items-center`}>{i + 1}</div>
                <div className={`w-60 flex py-1 items-center `}>
                  <div
                    className={` h2-4 w2-4  ba b--black br-50 overflow-hidden`}
                    style={{
                      background:
                        i !== 0
                          ? ''
                          : colorMap[scoreType.team_short_name]
                          ? colorMap[scoreType.team_short_name]
                          : '#797979'
                    }}>
                    <img
                      className=' object-cover object-top'
                      src={`https://images.cricket.com/players/${scoreType.player_id}_headshot_safari.png`}
                      alt=''
                      onError={(e) => (e.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className=''>
                    <div className=' flex  items-center pa1 justify-start '>{scoreType.player_name}</div>
                    <div className={`${i === 0 ? 'white' : 'text-gray-2'} pa1 fw4 f7`}>{scoreType.team_short_name}</div>
                  </div>
                </div>

                <div
                  className={`flex flex-col items-center  self-stretch justify-center ${
                    caseHandle[scoretype] === 'Highest_Score' ? 'w-15  ' : ' w-10'
                  } ${i !== 0 && caseHandle[scoretype] === 'Highest_Score' ? 'bg-light-gray w-15' : ''}}`}>
                  <div>
                    {caseHandle[scoretype] === 'Highest_Score' ? scoreType.highest_score : scoreType.matches_played}
                  </div>
                  {caseHandle[scoretype] === 'Highest_Score' && (
                    <div className={`fw3 ${i === 0 ? 'white' : 'gray'}`}> {`(${scoreType.balls_faced})`}</div>
                  )}
                </div>
                <div
                  className={` flex flex-col  items-center justify-center  ${
                    caseHandle[scoretype] === 'Highest_Score' ? 'w-15 ' : 'w-10'
                  }  `}>
                  {caseHandle[scoretype] === 'Highest_Score' && (
                    <div className={`fw4 pr-1 ${i === 0 ? 'white' : 'gray'}`}>vs</div>
                  )}
                  <div>
                    {caseHandle[scoretype] === 'Highest_Score'
                      ? `${
                          data.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]][i][
                            scores[caseHandle[scoretype.toLowerCase()]]
                          ]
                        }`
                      : caseHandle[scoretype.toLowerCase()] === 'Most_Runs'
                      ? scoreType.innings_played
                      : scoreType.runs_scored}
                  </div>
                </div>
                {caseHandle[scoretype.toLowerCase()] !== 'Highest_Score' && (
                  <div
                    className={`${
                      i === 0 ? '' : 'bg-light-gray'
                    } w-10 flex self-stretch flex-col items-center justify-center  `}>
                    {caseHandle[scoretype] === 'Most_Runs'
                      ? scoreType.runs_scored
                      : data.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]][i][
                          scores[caseHandle[scoretype.toLowerCase()]]
                        ]}
                  </div>
                )}
              </div>
              <div className='bg-black h-[1px] ' />
            </div>
          ))}

        {type === 'bowling' &&
          data.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]].map((scoreType, i) => (
            <div>
              <div
                className={`flex px-2  fw6 ${i === 0 ? 'white py-3' : ''} items-center f7 justify-between  `}
                key={i}
                style={{
                  background:
                    i !== 0
                      ? '#ffffff'
                      : colorMap[scoreType.team_short_name]
                      ? colorMap[scoreType.team_short_name]
                      : '#797979'
                }}>
                <div className='w-10 flex flex-col items-center '>{i + 1}</div>
                <div className='w-60 py-1 flex items-center'>
                  <div
                    className={` h2-4 w2-4  ba b--grey_5 br-50 overflow-hidden`}
                    style={{
                      background:
                        i !== 0
                          ? '#ffffff'
                          : colorMap[scoreType.team_short_name]
                          ? colorMap[scoreType.team_short_name]
                          : '#797979'
                    }}>
                    <img
                      className=' object-cover object-top'
                      src={`https://images.cricket.com/players/${scoreType.player_id}_headshot_safari.png`}
                      alt=''
                      onError={(e) => (e.target.src = fallbackProjection)}
                    />
                  </div>
                  <div className=' '>
                    <div className=' flex  items-center pa1 justify-start '>{scoreType.player_name}</div>
                    <div className={`${i === 0 ? 'white' : 'gray'} pa1 fw4 f7`}>{scoreType.team_short_name}</div>
                  </div>
                </div>

                <div
                  className={`flex flex-col items-center self-stretch justify-center ${
                    caseHandle[scoretype] === 'Best_figures' ? 'w-15 ' : 'w-10'
                  } ${i !== 0 && caseHandle[scoretype] === 'Best_figures' ? 'bg-light-gray w-15' : ''}}  `}>
                  <div>
                    {caseHandle[scoretype] === 'Best_figures'
                      ? scoreType.best_bowling_figures
                      : scoreType.matches_played}
                  </div>
                  {caseHandle[scoretype] === 'Best_figures' && (
                    <div className={`fw3 ${i === 0 ? 'white' : 'gray'}`}>{`(${scoreType.overs})`}</div>
                  )}
                </div>
                {caseHandle[scoretype] !== 'Most_Wickets' && (
                  <div
                    className={`flex flex-col items-center justify-center self-stretch ${
                      caseHandle[scoretype] === 'Best_figures' ? 'w-15 ' : 'w-10'
                    }  `}>
                    {caseHandle[scoretype] === 'Best_figures' && (
                      <div className={`fw4 pr-1 ${i === 0 ? 'white' : 'gray'}`}>vs</div>
                    )}
                    <div>
                      {caseHandle[scoretype] === 'Best_figures'
                        ? `${
                            data.getStatsResolver[format][cricketType][caseHandle[scoretype]][i][
                              scores[caseHandle[scoretype]]
                            ]
                          }`
                        : scoreType.wickets}
                    </div>
                  </div>
                )}
                {caseHandle[scoretype] === 'Most_Wickets' && (
                  <div
                    className={`flex flex-col items-center justify-center self-stretch w-10  ${
                      i === 0 ? '' : 'bg-light-gray'
                    } `}>
                    <div>{scoreType.wickets}</div>
                  </div>
                )}

                {caseHandle[scoretype.toLowerCase()] !== 'Best_figures' &&
                  caseHandle[scoretype.toLowerCase()] !== 'Most_Wickets' && (
                    <div
                      className={`${
                        i === 0 ? '' : 'bg-light-gray'
                      } w-10 flex self-stretch flex-col items-center justify-center`}>
                      {
                        data.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]][i][
                          scores[caseHandle[scoretype.toLowerCase()]]
                        ]
                      }
                    </div>
                  )}
                {caseHandle[scoretype.toLowerCase()] === 'Most_Wickets' && (
                  <div className={` w-10 flex self-stretch flex-col items-center justify-center`}>
                    {
                      data.getStatsResolver[format][cricketType][caseHandle[scoretype.toLowerCase()]][i][
                        scores[caseHandle[scoretype.toLowerCase()]]
                      ]
                    }
                  </div>
                )}
              </div>
              <div className='bg-black h-[1px] ' />
            </div>
          ))}

        {type === 'fielding' &&
          data.getStatsResolver[format][cricketType.toLowerCase()][caseHandle[scoretype.toLowerCase()]].map(
            (scoreType, i) => (
              <div>
                <div
                  className={`flex px-2  fw6 ${i === 0 ? 'white py-3' : ''} items-center f7 justify-between `}
                  style={{
                    background:
                      i !== 0
                        ? '#ffffff'
                        : colorMap[scoreType.team_short_name]
                        ? colorMap[scoreType.team_short_name]
                        : '#797979'
                  }}
                  key={i}>
                  <div className='w-10 flex flex-col items-center'>{i + 1}</div>
                  <div className='w-60 py-1 flex items-center'>
                    <div
                      className={` h2-4 w2-4  ba b--grey_5 br-50 overflow-hidden`}
                      style={{
                        background:
                          i !== 0
                            ? '#ffffff'
                            : colorMap[scoreType.team_short_name]
                            ? colorMap[scoreType.team_short_name]
                            : '#797979'
                      }}>
                      <img
                        className=' object-cover object-top'
                        src={`https://images.cricket.com/players/${scoreType.player_id}_headshot_safari.png`}
                        alt=''
                        onError={(e) => (e.target.src = fallbackProjection)}
                      />
                    </div>
                    <div className=' '>
                      <div className=' flex  items-center  pa1 justify-start '>{scoreType.player_name}</div>
                      <div className={`${i === 0 ? 'white' : 'gray'} pa1 fw4 f7`}>{scoreType.team_short_name}</div>
                    </div>
                  </div>

                  <div className={`w-15 flex flex-col items-center self-stretch justify-center  `}>
                    {scoreType.matches_played}
                  </div>
                  <div
                    className={`w-15 flex flex-col items-center self-stretch justify-center  ${
                      i !== 0 ? 'bg-light-gray w-15' : ''
                    } `}>
                    {
                      data.getStatsResolver[format][cricketType.toLowerCase()][caseHandle[scoretype.toLowerCase()]][i][
                        scores[caseHandle[scoretype.toLowerCase()]]
                      ]
                    }
                  </div>
                </div>
                <div className='bg-black h-[1px] ' />
              </div>
            )
          )}
      </div>
    </div>
  );
}
