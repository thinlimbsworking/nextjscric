import React, { useRef, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { EffectCoverflow, Navigation, Pagination, Scrollbar, A11y, Controller } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';
import { format } from 'date-fns';
import Countdown from 'react-countdown-now';
import Image from 'next/image'
import { scheduleMatchView } from '../api/services';


import Score from '../components/commom/score'

const mom = '/pngsV2/MOM2.png';
const LGarrow = '/pngsV2/lgarrow.png';

const RGarrow= '/pngsV2/rgarrow.png';

export default function Index({setPag,pegIndex,...props}) {
  

  const router = useRouter();


  const deviceType = () => {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        return "tablet";
    }
    else if (/Mobile|Android|iP(hone|od)|IEMobile|BlackBerry|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(ua)) {
        return "mobile";
    }
    return "desktop";
};


  // console.log('props-----', props);
  // const navigationPrevRef = React.useRef(null);
  // const navigationNextRef = React.useRef(null);
  // const [swiper, updateSwiper] = useState(null);

  const [swiper, setSwiper] = useState();
  const prevRef = useRef();
  const nextRef = useRef();

  useEffect(() => {
    if (swiper) {
      console.log("Swiper instance:", swiper);
      swiper.params.navigation.prevEl = prevRef.current;
      swiper.params.navigation.nextEl = nextRef.current;
      swiper.navigation.init();
      swiper.navigation.update();
    }
  }, [swiper]);

  const [currentIndex, updateCurrentIndex] = useState(0);
  const [test, setText] = useState(0);
    const [swiperIndex,setSwiperIndex]=useState(0)

  const Bell = './pngsV2/bell.png';
  const location = './pngsV2/location.png';
  const navigate = router.push;

//   useEffect(() => {
//     if (swiper !== null) {
//       try {
//         swiper.on('slideChange', () => {
//           updateCurrentIndex(swiper.realIndex);
//           props.updateCurrentIndex(swiper.realIndex);
//         });
//         swiper.on('click', () => {
         
//         });
//       } catch (e) {
//         console.log(e);
//       }
//     }
//   }, [swiper, test]);

const HandleSwipeCLick=(index)=>{
  props.data&& props.data.length > 0 &&
  props.data[0].displayFeatureMatchScoreCard &&
      handleNavigation(props.data[index]);
}

const handleNavigation = (match) => {
  const event = match.matchStatus === 'upcoming' || match.matchStatus === 'live' ? 'CommentaryTab' : 'SummaryTab';
  CleverTap.initialize(event, {
    Source: 'Homepage',
    MatchID: match.matchID,
    SeriesID: match.seriesID,
    TeamAID: match.matchScore[0].teamID,
    TeamBID: match.matchScore[1].teamID,
    MatchFormat: match.matchType,
    MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
    MatchStatus: match.matchStatus,
    Platform: localStorage.Platform
  });
  let currentTab = match.isAbandoned
    ? 'commentary'
    : match.matchStatus === 'upcoming' || match.matchStatus === 'live'
    ? 'Commentary'
    : 'Summary';
  let { as, href } = scheduleMatchView(match, currentTab);
  navigate(href, as);
}


  return (
    <div className='mt-3    text-white   mx-3 lg:mx-0 md:mx-0  '>
      <div className='flex items-center justify-center'>
       <div className=" mx-2 lg:mx-0 md:mx-0 hidden lg:block md:block" ref={prevRef} >
       <div
          style={{ zIndex: 1000 }}
        
          id='swiper-button-prev'
          className='white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer'>
          <svg width='30' focusable='false' viewBox='0 0 24 24'>
            <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
            <path fill='none' d='M0 0h24v24H0z'></path>
          </svg>
        </div>
     
        </div>
        
   <Swiper
 className="external-buttons "
modules={[EffectCoverflow,Navigation, Pagination, Scrollbar, A11y, Controller]}
                  
                    navigation={{
                      prevEl: prevRef?.current,
                      nextEl: nextRef?.current
                    }}
                    updateOnWindowResize
                    observer
                    observeParents
                    onSwiper={setSwiper}
                    onClick={(swiper)=>HandleSwipeCLick(swiper.realIndex)}
                    
                      onSlideChange={(swiper)=>setPag(swiper.realIndex)}
                      shouldSwiperUpdate={true}
                     
                        effect={"coverflow"}
                        grabCursor={true}
                        centeredSlides={true}
                        slidesPerView={deviceType()=="mobile"?'1':'2'}
                        loop={true}


                        coverflowEffect={{


                            rotate: 0,
                            stretch: 0,
                            depth: 600,

                        }}

                      
                       
                    >




                      
      {/* w-6/12 bg-red */}
        {true && props.data &&
          props.data.map((item, index) => {
            return (
              <SwiperSlide key={index} className="">
                <Score data={item} />
                {/* -----------------------------completed match card starts---------------------- */}
{false &&<div>
                {item.matchStatus == 'completed' && (
                  <div className={`bg-gray rounded-lg px-2 py-2    ${index==props.pegIndex?'opacity-10':'opacity-100'}`} style={{ height: 310 }}>
                    <div className='font-semibold  text-md tracking-wide '>{item.seriesName}</div>
                    <div className='flex justify-between items-center pt-3'>
                      <div className=''>
                        <div className='flex items-center'>
                          <div className='text-xs text-gray-2 font-semibold  pr-3 uppercase'>{item.matchNumber}</div>
                          {item.matchResult === 'Match Cancelled' || item.isAbandoned ? (
                            <div className=' text-red-5 text-xs font-semibold rounded-full bg-red-5/30 border-red-5 border-2 p-1 px-2 flex  uppercase  justify-center items-center'>
                              <div className='pr-1 text-red-5 text-xs'> x</div>{' '}
                              {item.isAbandoned ? 'ABANDONED' : 'Cancelled'}
                            </div>
                          ) : (
                            <div className=' text-gray-7 text-xs font-semibold rounded-full border-gray-7 border-2 px-2 py-0.5 flex  uppercase  justify-center items-center bg-gray-7/20'>
                              <div className='w-1.5 h-1.5 bg-white rounded-full mr-1  '></div> {item.matchStatus}{' '}
                            </div>
                          )}
                        </div>
                        <div className='flex items-center pt-1'>
                          <img src={location} className='w-3' />
                          <div className='text-xs text-gray-2 font-semibold pl-1'>{item.venue}</div>
                        </div>
                      </div>
                      {/* <div className=''>
                        <img src={Bell} alt='' />
                      </div> */}
                    </div>

                    <div className=' bg-gray-4  my-2 rounded p-3 '>
                      <div className='flex justify-between items-center '>
                        <div className='flex items-center  text-white text-md font-semibold'>
                          <img
                            className='w-10 h-6 shadow-2xl  rounded-sm mr-2'
                            src={`https://images.cricket.com/teams/${item.matchScore[0].teamID}_flag_safari.png`}
                            onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                          />{' '}
                          {item.matchScore[0].teamShortName}
                        </div>
                        <div
                          className={`justify-center items-center flex border-2 font-semibold ${
                            item.matchResult === 'Match Cancelled' || item.isAbandoned
                              ? 'border-gray-3 text-gray-3'
                              : 'border-green text-green'
                          }  rounded-full  uppercase text-xs px-3   py-0.5 `}>
                          {item.matchType}
                        </div>
                        <div>
                          <div className='flex items-center  text-white text-md font-semibold'>
                            {' '}
                            {item.matchScore[1].teamShortName}
                            <img
                              className='w-10 h-6 shadow-2xl  rounded-sm ml-2'
                              src={`https://images.cricket.com/teams/${item.matchScore[1].teamID}_flag_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                            />{' '}
                          </div>
                        </div>
                      </div>
                      <div className='flex justify-between items-center pt-2'>
                        <div className=''>
                          {item.matchScore &&
                            item.matchScore[0] &&
                            item.matchScore[0].teamScore &&
                            item.matchScore[0].teamScore.map((team, id) => (
                              <div key={id} className='flex items-center'>
                                <div
                                  className={`${
                                    item.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                  } text-sm font-semibold pb-1`}>
                                  {team.runsScored}/{team.wickets}
                                </div>
                                {team.overs && <div className='text-xs pl-1 font-medium text-white'>({team.overs})</div>}
                                {team.declared && <div className='text-sm text-green  pl-1 font-semibold'>d</div>}
                                {team.folowOn && <div className='text-sm text-green  pl-1 font-semibold'>f</div>}
                              </div>
                            ))}
                        </div>
                        <div className=''>
                          {item.matchScore &&
                            item.matchScore[1] &&
                            item.matchScore[1].teamScore &&
                            item.matchScore[1].teamScore.map((team, id) => (
                              <div key={id} className='flex items-center'>
                                {team.runsScored && <div
                                  className={`${
                                    item.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                  } text-sm font-semibold pb-1`}>
                                  {team.runsScored}/{team.wickets || 0}
                                </div>}
                                {team.overs && <div className='text-xs pl-1 font-medium'>({team.overs})</div>}
                                {team.declared && <div className='text-sm text-green  pl-1 font-semibold'>d</div>}
                                {team.folowOn && <div className='text-sm text-green  pl-1 font-semibold'>f</div>}
                              </div>
                            ))}
                        </div>
                      </div>
                    </div>

                    {item.matchResult === 'Match Cancelled' || item.isAbandoned ? (
                      <div className=' bg-gray-4 text-center my-2 rounded py-2  text-xs font-semibold '>
                        {item.statusMessage}
                      </div>
                    ) : (
                      <div className=' bg-gray-4 text-center my-2 rounded py-2 text-green text-xs font-semibold '>
                        {item.matchResult}
                        {console.log("BBBbbbbbbbb",item)}
                      </div>
                    )}

                    {
                      <div className=''>
                        <div className='flex  justify-start items-center '>
                          {item.matchResult !== 'Match Cancelled' && !item.isAbandoned ? (
                            <div className='flex relative  items-center '>
                              <img
                                className='h-14 w-14 bg-gray-4 object-cover object-top rounded-full p-1'
                                src={`https://images.cricket.com/players/${item && item.playerID}_headshot_safari.png`}
                                 onError={(evt) => (evt.target.src = './pngsV2/MOM2.png')}
                              />

                              <img
                                className=' absolute h-5 w-5   right-0 bottom-0 object-fill rounded shadow-2xl border-2'
                                src={`https://images.cricket.com/teams/${item && item.currentInningteamID
                                }_flag_safari.png`}
                                 onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                              />
                            </div>
                          ) : (
                            <img className='h-16 w-16 bg-gray-4 rounded-full p-2' src={mom} />
                          )}
                          {item.matchResult == 'Match Cancelled' ? (
                            <div className='text-white text-sm font-semibold '>Match Cancelled</div>
                          ) : item.isAbandoned ? (
                            <div className='text-gray-2 text-sm font-semibold ml-3'>Match Abandoned</div>
                          ) : (
                            <div className=' ml-2'>
                              <div className='text-white text-sm font-semibold '>
                                {(item.playerOfTheMatchdDetails && item.playerOfTheMatchdDetails.playerName) || 'TBA'}
                              </div>
                              <div className=' capitalize text-gray-2 text-xs font-medium'>player Of The Match</div>
                            </div>
                          )}
                        </div>
                      </div>
                    }
                  </div>
                )}

                {/* ----------------------completed match block ends-------------------- */}

                {/*------------------------------ upcoming card block starts--------------------------------------------- */}

                {/* {console.log("datadatdata",item)} */}
                {(item.matchStatus === 'upcoming' ||
                  (item.matchStatus == 'live' && !item.isLiveCriclyticsAvailable)) && (
                  <div className={`bg-gray rounded-lg px-2 py-2    ${index==props.pegIndex?'opacity-10':'opacity-100'}`}style={{ height: 310 }}>
                    <div className='font-semibold  text-md tracking-wide '>{item.seriesName}</div>
                    <div className='flex justify-between items-center pt-3'>
                      <div className=''>
                        <div className='flex items-center'>
                          <div className='text-xs text-gray-2 font-semibold  pr-3 uppercase'>{item.matchNumber}</div>
                          <div
                            className={`font-semibold text-xs  rounded-full ${
                              item.matchStatus == 'live' && !item.isLiveCriclyticsAvailable
                                ? 'border-red-5 text-white bg-red-5 '
                                : ' text-blue-9 border-blue-9 bg-blue-9/20'
                            }  border-2 px-2 py-0.5 flex  uppercase  justify-center items-center`}>
                            <div
                              className={`w-1.5 h-1.5 ${
                                item.matchStatus == 'live' && !item.isLiveCriclyticsAvailable
                                  ? 'bg-white'
                                  : '  bg-blue-9'
                              }  rounded-full mr-1  `}></div>{' '}
                            {item.matchStatus}{' '}
                          </div>
                        </div>
                        <div className='flex items-center pt-2'>
                          <img src={location} className='w-3' />
                          <div className='text-xs text-gray-2 font-semibold pl-1'>{item.venue}</div>
                        </div>
                      </div>
                      {/* <div className=''>
                        <img src={Bell} alt='' />
                      </div> */}
                    </div>

                    <div className='flex bg-gray-4 justify-between items-center my-3 rounded px-3 py-5 '>
                      <div className='flex items-center  text-white text-md font-semibold'>
                        <img
                          className='w-10 h-6 shadow-2xl rounded-sm mr-2'
                          src={`https://images.cricket.com/teams/${item.matchScore[0].teamID}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                        />
                        {item.matchScore[0].teamShortName}
                      </div>

                      <div className='justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 '>
                        {item.matchType}
                      </div>
                      <div className='flex items-center  text-white text-md font-semibold'>
                        {item.matchScore[1].teamShortName}
                        <img
                          className='w-10 h-6 shadow-2xl  rounded-sm ml-2'
                          src={`https://images.cricket.com/teams/${item.matchScore[1].teamID}_flag_safari.png`}
                          onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                        />
                      </div>
                    </div>

                    <div className='flex bg-gray-4 justify-center items-center my-3 rounded py-2 text-white'>
                      {item.toss ? (
                        <div className='text-sm font-medium text-center '>{item.toss}</div>
                      ) : (
                        <div className='flex items-center'>
                          <Countdown
                            date={new Date(Number(item.startDate) - 19800000 - 1800000)}
                            renderer={(props) => (
                              <span className='text-sm font-semibold'>
                                {`${props.days !== 0 ? props.days * 24 + Number(props.hours) + ' hrs' :  props.hours == 0 ? '' :props.hours + ' hrs'}  ${
                                  props.minutes
                                } mins to toss`}
                              </span>
                            )}
                          />
                        </div>
                      )}
                    </div>

                    {item.teamsWinProbability && (
                      <div className=''>
                        <div className='text-gray-2 text-xs my-2 font-medium'>WIN PERCENTAGE</div>
                        <div className=' flex bg-gray-3 rounded-full  h-3 overflow-hidden '>
                          <div
                            className={`${
                              
                                'bg-green'
                                
                            }  h-3 rounded-l-lg`}
                            style={{ width: item.teamsWinProbability.homeTeamPercentage + '%' }}
                          />
                          <div
                            className={` bg-gray-3 h-3   `}
                            style={{ width: item.teamsWinProbability.tiePercentage + '%' }}
                          />
                          <div
                            className={`${
                               'bg-white'
                            }  h-3 rounded-r-lg  `}
                            style={{ width: item.teamsWinProbability.awayTeamPercentage + '%' }}
                          />
                        </div>
                      </div>
                    )}
                    {item.teamsWinProbability && (
                      <div className='flex mt-3 justify-between items-center text-gray-2'>
                        <div className=' flex items-center text-xs font-medium'>
                          <div
                            className={`w-2 h-2 ${
                              'bg-green'
                          
                            } rounded-full m-1  `}></div>
                          {item.teamsWinProbability && item.teamsWinProbability.homeTeamShortName} (
                          {item.teamsWinProbability && item.teamsWinProbability.homeTeamPercentage}%)
                        </div>
                        {item.teamsWinProbability && item.teamsWinProbability.tiePercentage && (
                          <div className='flex items-center text-xs font-medium'>
                            <div className='w-2 h-2 bg-gray-3 rounded-full m-1  '></div>
                            {item.matchType !='Test' ? 'Tie':'DRAW' } ({(item.teamsWinProbability && item.teamsWinProbability.tiePercentage) || 0}%)
                          </div>
                        )}
                        <div className='flex items-center text-xs font-medium'>
                          <div
                            className={`w-2 h-2 ${
                             
                                 'bg-white'
                            } rounded-full m-1  `}></div>
                          {item.teamsWinProbability && item.teamsWinProbability.awayTeamShortName} (
                          {item.teamsWinProbability && item.teamsWinProbability.awayTeamPercentage}%)
                        </div>
                      </div>
                    )}
                  </div>
                )}

                {/* -------------------------------------------end of upcoming match card------------------------------------ */}

                {/* --------------------------------------live match card----------------------------------- */}

                {item.matchStatus == 'live' && item.isLiveCriclyticsAvailable && (
                  <div className={`bg-gray rounded-lg px-2 py-2    ${index==props.pegIndex?'opacity-10':'opacity-100'}`} style={{ height: 310 }}>
                    <div className='font-semibold  text-md tracking-wide '>{item.seriesName}</div>
                    <div className='flex justify-between items-center pt-3'>
                      <div className=''>
                        <div className='flex items-center'>
                          <div className='text-xs text-gray-2 font-semibold  pr-3 uppercase'>{item.matchNumber}</div>
                          <div className=' font-semibold text-xs  rounded-full bg-red-5 border-red-5 border-2 px-1 py-0.5 flex  uppercase text-white justify-center items-center'>
                            <div className='w-1.5 h-1.5  bg-white rounded-full mr-1 '></div> {item.matchStatus}{' '}
                          </div>
                        </div>
                        <div className='flex items-center'>
                          <img src={location} className='w-3' />
                          <div className='text-xs text-gray-2 font-semibold pl-1'>{item.venue}</div>
                        </div>
                      </div>
                      {/* <div className=''>
                        <img src={Bell} alt='' />
                      </div> */}
                    </div>

                    <div className=' bg-gray-4  my-3 rounded p-3 '>
                      <div className='flex justify-between items-center'>
                        <div className=''>
                          <div className='flex items-center  text-white text-md font-semibold'>
                            <img
                              className='w-10 h-6 shadow-2xl rounded-sm mr-2'
                              src={`https://images.cricket.com/teams/${item.matchScore[0].teamID}_flag_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                            />{' '}
                            {item.matchScore[0].teamShortName}
                          </div>
                        </div>
                        <div className='justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3   py-0.5 '>
                          {item.matchType}
                        </div>
                        <div>
                          <div className='flex items-center  text-white text-md font-semibold'>
                            {' '}
                            {item.matchScore[1].teamShortName}
                            <img
                              className='w-10 h-6 shadow-2xl  rounded-sm ml-2'
                              src={`https://images.cricket.com/teams/${item.matchScore[1].teamID}_flag_safari.png`}
                              onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                            />{' '}
                          </div>
                        </div>
                      </div>

                      {item.matchScore && item.matchScore[0] && item.matchScore[0].teamScore.length > 0 && (
                        <div className='flex justify-between items-center pt-2'>
                          <div className=''>
                            {item.matchScore &&
                              item.matchScore[0] &&
                              item.matchScore[0].teamScore &&
                              item.matchScore[0].teamScore.map((team, id) => (
                                <div key={id} className='flex items-center'>
                                  <div
                                    className={`${
                                      item.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                    } text-sm font-semibold pb-1`}>
                                    {team.runsScored}/{team.wickets}
                                  </div>
                                  {team.overs && <div className='text-xs pl-1 font-medium text-white'>({team.overs})</div>}
                                  {team.declared && <div className='text-sm text-green  pl-1 font-semibold'>d</div>}
                                  {team.folowOn && <div className='text-sm text-green  pl-1 font-semibold'>f</div>}
                                </div>
                              ))}
                          </div>
                          <div className=''>
                            {item.matchScore && item.matchScore[1] && item.matchScore[1].teamScore.length > 0 ? (
                              item.matchScore[1].teamScore.map((team, id) => (
                                <div key={id} className='flex items-center'>
                                  {team.runsScored && (
                                    <div
                                      className={`${
                                        item.winningTeamID === team.teamID ? 'text-green' : 'text-white'
                                      } text-sm font-semibold pb-1`}>
                                      {team.runsScored}/{team.wickets}
                                    </div>
                                  )}
                                  {team.overs && <div className='text-xs pl-1 font-medium'>({team.overs})</div>}
                                  {team.declared && <div className='text-sm text-green  pl-1 font-semibold'>d</div>}
                                  {team.folowOn && <div className='text-sm text-green  pl-1 font-semibold'>f</div>}
                                </div>
                              ))
                            ) : (
                              <div className='text-gray-2 text-xs font-medium '>Yet to bat</div>
                            )}
                          </div>
                        </div>
                      )}
                    </div>
                    <div className='flex bg-gray-4 justify-center items-center my-3 rounded p-2 text-white text-sm font-medium text-center'>
                      {item.statusMessage}
                    </div>

                    {item.teamsWinProbability && (
                      <div className=''>
                        <div className='text-gray-2 text-xs my-2 font-medium'>WIN PERCENTAGE</div>
                        <div className=' flex bg-gray-3 rounded-full  h-3 '>
                          <div
                            className={`${
                              parseInt(item.teamsWinProbability.homeTeamPercentage) >
                              parseInt(item.teamsWinProbability.awayTeamPercentage)
                                ? 'bg-green'
                                : 'bg-white'
                            }  h-3 rounded-l-lg`}
                            style={{ width: item.teamsWinProbability.homeTeamPercentage + '%' }}
                          />
                          <div
                            className={` bg-gray-3 h-3   `}
                            style={{ width: item.teamsWinProbability.tiePercentage + '%' }}
                          />
                          <div
                            className={`${
                              parseInt(item.teamsWinProbability.homeTeamPercentage) <
                              parseInt(item.teamsWinProbability.awayTeamPercentage)
                                ? 'bg-green'
                                : 'bg-white'
                            }  h-3 rounded-r-lg  `}
                            style={{ width: item.teamsWinProbability.awayTeamPercentage + '%' }}
                          />
                        </div>
                      </div>
                    )}
                    {item.teamsWinProbability && (
                      <div className='flex mt-3 justify-between items-center text-gray-2'>
                        <div className=' flex items-center text-xs font-medium'>
                          <div
                            className={`w-1.5 h-1.5 ${
                              parseInt(item.teamsWinProbability.homeTeamPercentage) >
                              parseInt(item.teamsWinProbability.awayTeamPercentage)
                                ? 'bg-green'
                                : 'bg-white'
                            } rounded-full m-1  `}></div>
                          {item.teamsWinProbability && item.teamsWinProbability.homeTeamShortName} (
                          {item.teamsWinProbability && item.teamsWinProbability.homeTeamPercentage}%)
                        </div>
                        {item.teamsWinProbability && (
                          <div className='flex items-center text-xs font-medium'>
                            <div className='w-1.5 h-1.5 bg-gray-3 rounded-full m-1  '></div>
                            {item.matchType !='Test' ? 'Tie':'DRAW' } ({(item.teamsWinProbability && item.teamsWinProbability.tiePercentage) || 0}%)
                          </div>
                        )}
                        <div className='flex items-center text-xs font-medium'>
                          <div
                            className={`w-1.5 h-1.5 ${
                              parseInt(item.teamsWinProbability.homeTeamPercentage) <
                              parseInt(item.teamsWinProbability.awayTeamPercentage)
                                ? 'bg-green'
                                : 'bg-white'
                            } rounded-full m-1  `}></div>
                          {item.teamsWinProbability && item.teamsWinProbability.awayTeamShortName} (
                          {item.teamsWinProbability && item.teamsWinProbability.awayTeamPercentage}%)
                        </div>
                      </div>
                    )}
                  </div>
                )}
                </div>}
              </SwiperSlide>
            );
          })}
      </Swiper>
      <div className=" text-2xl hidden lg:block md:block" ref={nextRef}>
      <div
          style={{ zIndex: 1000 }}
        
          id='swiper-button-prev'
          className='white cursor-pointer dn db-ns z-999 outline-0 cursor-pointer'>
            <svg width='30' viewBox='0 0 24 24'>
            <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
            <path fill='none' d='M0 0h24v24H0z'></path>
          </svg>
        </div>
        </div>
        </div>
      <div className='flex justify-center mt-2'>
        { props.data &&
          props.data.map((item, key) => (
            <div
            key={key}
              className={`w-2 h-2  ${
                pegIndex == key ? 'bg-blue-8' : 'bg-blue-4 '
              } rounded-full m-1  `}></div>
          ))}
      </div>
    </div>
  );
}
