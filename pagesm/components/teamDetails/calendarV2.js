import React, { useState, useEffect } from 'react';
import Scorecard from "../commom/score";
// import LinearGradient from 'react-native-linear-gradient';
// import HrLine from '../common/HrLine';
// import TachyonsWrapper from '../../../TachyonsWrapper';

// var squares = [];
// const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
const CalenderV2 = ({showScorecard, scorecardData, setShowDate, selectedMatch, ...props }) => {
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const nextMonth =
        new Date().getMonth() === 11 ? 0 : new Date().getMonth() + 1;
    // const [selectedDate, setselectedDate] = useState(null);
    // const [, setnextMonth] = useState(new Date().getMonth()-1);
    const [monthDays, setDate] = useState([]);
    const [currMonth, setcurrMonth] = useState([]);
    const [nxtMonth, setnxtMonth] = useState([]);
    //const tw = useTailwind();
    // console.log(currMonth, nxtMonth, 'mmm');
    useEffect(() => {
        drawCal();
    }, []);
    useEffect(() => {
        drawCal();
    }, [props.matches]);
    var cal = {
        /* [PROPERTIES] */
        mName: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ], // Month Names
        data: null, // Events for the selected period
        sDay: 0, // Current selected day
        sMth: 0, // Current selected month
        sYear: 0, // Current selected year
        sMon: false, // Week start on Monday?
    };
    const drawCal = () => {
        // cal.list() : draw the calendar for the given month
        // BASIC CALCULATIONS
        // Note - Jan is 0 & Dec is 11 in JS.
        // Note - Sun is 0 & Sat is 6
        cal.pMth = currentMonth; // selected month
        cal.pYr = currentYear; // selected year
        // cal.pMth = nextMonth;
        // cal.pYr = nextMonth === 0 ? currentYear+1:currentYear;
        cal.sMth = nextMonth;
        cal.sYear = nextMonth === 0 ? currentYear + 1 : currentYear;
        var daysInMth = new Date(cal.sYear, cal.sMth + 1, 0).getDate(), // number of days in selected month
            startDay = new Date(cal.sYear, cal.sMth, 1).getDay(), // first day of the month
            endDay = new Date(cal.sYear, cal.sMth, daysInMth).getDay(); // last day of the month
        var daysInPrevMth = new Date(cal.pYr, cal.pMth + 1, 0).getDate(), // number of days in selected month
            prevstartDay = new Date(cal.pYr, cal.pMth, 1).getDay(), // first day of the month
            prevendDay = new Date(cal.pYr, cal.pMth, daysInPrevMth).getDay(); // last day of the month
   
        // DRAWING CALCULATIONS
        // Determine the number of blank squares before start of month
        var squares = [];
        var prevSqr = [];
        if (cal.sMon && prevstartDay != 1) {
            var blanks = prevstartDay == 0 ? 7 : prevstartDay;
            for (var i = 1; i < blanks; i++) {
                // squares.push("");
                prevSqr.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        if (!cal.sMon && prevstartDay != 0) {
            for (var i = 0; i < prevstartDay; i++) {
                // prevSqr.push("");
                prevSqr.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        if (cal.sMon && startDay != 1) {
            var blanks = startDay == 0 ? 7 : startDay;
            for (var i = 1; i < blanks; i++) {
                // squares.push("");
                squares.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        if (!cal.sMon && startDay != 0) {
            for (var i = 0; i < startDay; i++) {
                // prevSqr.push("");
                squares.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        // Populate the days of the month
        for (var i = 1; i <= daysInMth; i++) {
            let obj = {};
            for (let match of props.matches || []) {
                if (
                    `${new Date(+match.matchDateTimeIST).getFullYear()}-${new Date(
                        +match.matchDateTimeIST,
                    ).getMonth()}-${new Date(+match.matchDateTimeIST).getDate()}` ==
                    `${cal.sYear}-${cal.sMth}-${i}`
                ) {
                    obj.day = i;
                    obj.matchName = '';
                    obj.matchDate = match.matchDateTimeIST;
                    obj.format = match.compType;
                    obj.matchID = match.matchID;
                    obj.month = cal.sMth;
                }
            }
            // squares.push(i);
            if (obj.day) {
                squares.push(obj);
            } else {
                squares.push({
                    day: i,
                    matchName: '',
                    matchDate: '',
                    format: '',
                    matchID: '',
                    month: cal.sMth,
                });
            }
        }
        // Populate the days of the month
        for (var i = 1; i <= daysInPrevMth; i++) {
            let obj = {};
            for (let match of props.matches || []) {
                if (
                    `${new Date(+match.matchDateTimeIST).getFullYear()}-${new Date(
                        +match.matchDateTimeIST,
                    ).getMonth()}-${new Date(+match.matchDateTimeIST).getDate()}` ==
                    `${cal.pYr}-${cal.pMth}-${i}`
                ) {
                    obj.day = i;
                    obj.matchName = '';
                    obj.matchDate = match.matchDateTimeIST;
                    obj.format = match.compType;
                    obj.matchID = match.matchID;
                    obj.month = cal.pMth;
                }
            }
            // squares.push(i);
            if (obj.day) {
                prevSqr.push(obj);
            } else {
                prevSqr.push({
                    day: i,
                    matchName: '',
                    matchDate: '',
                    format: '',
                    matchID: '',
                    month: cal.pMth,
                });
            }
        }
        // Determine the number of blank squares after end of month
        if (cal.sMon && endDay != 0) {
            var blanks = endDay == 6 ? 1 : 7 - endDay;
            for (var i = 0; i < blanks; i++) {
                // squares.push("");
                squares.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        if (!cal.sMon && endDay != 6) {
            var blanks = endDay == 0 ? 6 : 6 - endDay;
            for (var i = 0; i < blanks; i++) {
                // squares.push("");
                squares.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        if (cal.sMon && prevendDay != 0) {
            var blanks = prevendDay == 6 ? 1 : 7 - prevendDay;
            for (var i = 0; i < blanks; i++) {
                // squares.push("");
                prevSqr.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        if (!cal.sMon && prevendDay != 6) {
            var blanks = prevendDay == 0 ? 6 : 6 - prevendDay;
            for (var i = 0; i < blanks; i++) {
                // prevSqr.push("");
                prevSqr.push({
                    day: '',
                    matchName: '',
                    matchDate: '',
                    format: 'b',
                    matchID: '',
                    month: '',
                });
            }
        }
        setcurrMonth([...prevSqr]);
        setnxtMonth([...squares]);
        setDate([...prevSqr, ...squares]);
        squares = [];


       
    };
    // useEffect(() => {
    //   cal.list();
    // }, [currentMonth]);
    // function forwardCalendar() {
    //   if (currentMonth == 11) {
    //     setNewMonth(0);
    //     setNewYear(currentYear + 1);
    //     setDate([]);
    //     props.getTeamSchedule(currentYear + 1, 0, 1)
    //     // refetch({
    //     //   startDate: `${currentYear + 1}-${0}-${1}`
    //     // })
    //   } else {
    //     setNewMonth(currentMonth + 1);
    //     setDate([]);
    //     props.getTeamSchedule(currentYear, currentMonth + 1, 1)
    //     // refetch({
    //     //   startDate: `${currentYear}-${currentMonth + 1}-${1}`
    //     // })
    //   }
    // }
    // function backwardCalendar() {
    //   if (currentMonth == 0) {
    //     setNewMonth(11);
    //     setNewYear(currentYear - 1);
    //     setDate([]);
    //     props.getTeamSchedule(currentYear - 1, 11, 1);
    //     // refetch({
    //     //   startDate: `${currentYear-1}-${11}-${1}`
    //     // })
    //   } else {
    //     setNewMonth(currentMonth - 1);
    //     setDate([]);
    //     props.getTeamSchedule(currentYear, currentMonth - 1, 1);
    //     // refetch({
    //     //   startDate: `${currentYear}-${currentMonth - 1}-${1}`
    //     // })
    //   }
    //   cal.list();
    // }
    // ====== Calendar Calculation ===
    const RenderCal = ({ data, total, month, isFirst = true }) => {
        // console.log(total/7, "data");
        return (
            <div
                className={`bg-gray rounded-md ,
          
        `}  style={{minWidth:'48%', height:320}} >
                <div
                    className='text-white font-mnb text-sm text-center uppercase py-2 mb-1 '
                    style={{minWidth:'96%'}}
                >
                    {month}
                </div>
                <div
                    className='flex  w-full  h-[279px]' 
                >
                    <div className='h-full ' style={{width: 'calc(100%/7)'}}>
                    {['S', 'M', 'T', 'W', 'T', 'F', 'S'].map((day, idx) => (
                        <div
                            className={`flex items-center justify-center ${idx < 6 ? 'text-xs text-gray-2 border-r border-l border-t' : 'border text-xs text-gray-2'} border-gray-2 items-center justify-center `}
                            style={{height: 'calc(100%/7)'}}
                        >
                           {day}
                        </div>
                    ))}
                    </div>
                    <div className='h-full w-full flex flex-col flex-wrap ' >
                    {data.map((day, index) => (
                        <div
                            disabled={!day.matchID}
                            onClick={() => {
                                day.matchID &&
                                  day.matchID !== "" &&
                                  setShowDate(day.matchDate, day.matchID);
                              }}
                            className={
                                `flex items-center justify-center ${index % 7 === 0
                                    ? 'border-t border-r'
                                    : index % 7 === 6
                                        ? 'border-b border-r border-t'
                                        : 'border-r border-t'
                                } ${day.matchID === selectedMatch
                                    ? 'border-green  border-2'
                                    : 'border-gray-2'
                                } items-center justify-center`
                            }
                            style={{height: 'calc(100%/7)',width: `calc(100%/${props.col})`}}

                        >
                            <div
                                className={
                                    `${day.matchID ? 'text-green' : 'text-gray-2'
                                    } font-mnm text-xs`
                                }
                            >
                                {day.day}
                            </div>
                        </div>
                    ))} </div>
                </div>
            </div>
        );
    };
    // console.log(selectedDate);  // === Calendar calculation closing
    const renderScorecard = match => {
        props.showScorecard(match);
    };
    return (
        <div>
            <div className='flex flex-col md:flex-row lg:flex-row gap-3'>
            <div className='mx-1 flex w-full gap-1 md:w-1/2 lg:w-1/2'>
                <RenderCal
                    data={currMonth}
                    total={currMonth.length + nxtMonth.length}
                    month={cal.mName[currentMonth]}
                    col = {currMonth.length > 42 ? 7 : 6}
                />
                <RenderCal
                    data={nxtMonth}
                    isFirst={false}
                    total={currMonth.length + nxtMonth.length}
                    month={cal.mName[nextMonth]}
                    col = {currMonth.length > 42 ? 7 : 6}
                />
            </div>
            {showScorecard && scorecardData && (
          <div
            className="w-full  px-0 lg:px-2 md:px-2 lg:w-6/12 md:6/12"
            onClick={() => {
              handleNavigation(scorecardData);
            }}
          >
            {/* handleNavigation(scorecardData) */}
            <Scorecard
              data={scorecardData}
              browser={"browser"}
              hideFantasyCriTab={true}
            />
          </div>
        )}
        </div>
            {/* {false && (
        <div
          style={[
            {height: ((screenWidth - 200) / 11) * 7 + 94},
            tw(`justify-center items-center bg-gray`),
          ]}>
          <div
            style={{
              flex: 1,
              flexWrap: 'wrap',
              flexGrow: 1,
              flexShrink: 1,
            }}>
            {['S', 'M', 'T', 'W', 'T', 'F', 'S'].map(day => (
              <div
                style={[
                  {
                    height: (screenWidth - 200) / 11,
                    width: (screenWidth - 200) / 11,
                    marginVertical: 6,
                    marginLeft: 9,
                  },
                  tw(`items-center border border-gray-2 justify-center`),
                ]}>
                <div style={[tw(`font-mnb text-gray-2 text-xs`)]}>{day}</div>
              </div>
            ))}
            {monthDays.map((obj, index) => (
              // obj.matchID === selectedMatch ? (
              //   <div>
              //     <div
              //       cls="aic"
              //       style={{
              //         borderWidth: 1,
              //         borderColor: '#059A2D',
              //         padding: 4,
              //         width: '70%',
              //         marginLeft: 5,
              //         marginTop: 2,
              //       }}>
              //       <div
              //         style={{
              //           width: (screenWidth - 200) / 11,
              //           height: (screenWidth - 200) / 11,
              //           //  marginRight:8,
              //           //  marginLeft:8,
              //           //  marginVertical:4,
              //           //  backgroundColor:"#059A2D"
              //         }}>
              //         <div style={[tw(`font-mnm text-gray-2 text-xs`)]}>
              //           {obj.day}
              //         </div>
              //       </div>
              //     </div>
              //   </div>
              // ) :
              <div
                disabled={!obj.matchID}
                onPress={() => obj.matchID && renderScorecard(obj)}
                key={index}
                style={[
                  {
                    width: (screenWidth - 200) / 11,
                    height: (screenWidth - 200) / 11,
                    // marginRight: 9,
                    // marginLeft: 9,
                    marginVertical: 6,
                    overflow: 'hidden',
                  },
                  tw(`border border-gray-2 items-center justify-center`),
                ]}>
                <div style={[tw(`font-mnm text-gray-2 text-xs`)]}>
                  {obj.day}
                </div>
              </div>
            ))}
          </div>
        </div>
      )} */}
        </div>
    );
};
export default CalenderV2;











