import { scaleDivergingSqrt } from "d3-scale";
import React, { useState, useEffect } from "react";
import { format } from "date-fns";
import { GET_SCORE_CARD } from "../../api/queries";
import { scheduleMatchView } from "../../api/services";
import { useRouter } from "next/router";
import { useQuery, useMutation } from "@apollo/react-hooks";
import Scorecard from "../commom/score";
export default function Calender2({
  scheme,
  selectedMatchDate,
  selectedMatch,
  showScorecard,
  setShowDate,
  scorecardData,
  ...props
}) {
  const router = useRouter();
  const navigate = router.push;
  const handleNavigation = (match) => {
    const event =
      scorecardData.matchStatus === "upcoming" ||
      scorecardData.matchStatus === "live"
        ? "CommentaryTab"
        : "SummaryTab";
    CleverTap.initialize(event, {
      Source: 'Homepage',
      MatchID: scorecardData.matchID,
      SeriesID: scorecardData.seriesID,
      TeamAID: scorecardData.matchScore[0].teamID,
      TeamBID: match.matchScore[1].teamID,
      MatchFormat: match.matchType,
      MatchStartTime: format(Number(match.startDate), 'do MMM yyyy, h:mm a'),
      MatchStatus: match.matchStatus,
      Platform: localStorage.Platform
    });
    let currentTab = scorecardData.isAbandoned
      ? "commentary"
      : scorecardData.matchStatus === "upcoming" ||
        scorecardData.matchStatus === "live"
      ? "Commentary"
      : "Summary";
    let { as, href } = scheduleMatchView(scorecardData, currentTab);
    navigate(href, as);
  };
  // var squares = [];
  // const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth();
  // const [,date,setShowDate]=useState()
  const nextMonth =
    new Date().getMonth() === 11 ? 0 : new Date().getMonth() + 1;
  // const [selectedDate, setselectedDate] = useState(null);
  // const [, setnextMonth] = useState(new Date().getMonth()-1);
  const [monthDays, setDate] = useState([]);
  useEffect(() => {
    drawCal();
  }, []);
  useEffect(() => {
    drawCal();
  }, [props.matches]);

  var cal = {
    /* [PROPERTIES] */
    mName: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ], // Month Names
    data: null, // Events for the selected period
    sDay: 0, // Current selected day
    sMth: 0, // Current selected month
    sYear: 0, // Current selected year
    sMon: false, // Week start on Monday?
  };

  const drawCal = () => {
    // cal.list() : draw the calendar for the given month

    // BASIC CALCULATIONS
    // Note - Jan is 0 & Dec is 11 in JS.
    // Note - Sun is 0 & Sat is 6
    cal.pMth = currentMonth; // selected month
    cal.pYr = currentYear; // selected year

    // cal.pMth = nextMonth;
    // cal.pYr = nextMonth === 0 ? currentYear+1:currentYear;
    cal.sMth = nextMonth;
    cal.sYear = nextMonth === 0 ? currentYear + 1 : currentYear;

    var daysInMth = new Date(cal.sYear, cal.sMth + 1, 0).getDate(), // number of days in selected month
      startDay = new Date(cal.sYear, cal.sMth, 1).getDay(), // first day of the month
      endDay = new Date(cal.sYear, cal.sMth, daysInMth).getDay(); // last day of the month
    var daysInPrevMth = new Date(cal.pYr, cal.pMth + 1, 0).getDate(), // number of days in selected month
      prevstartDay = new Date(cal.pYr, cal.pMth, 1).getDay(), // first day of the month
      prevendDay = new Date(cal.pYr, cal.pMth, daysInPrevMth).getDay(); // last day of the month

    // DRAWING CALCULATIONS
    // Determine the number of blank squares before start of month
    var squares = [];
    var prevSqr = [];
    if (cal.sMon && prevstartDay != 1) {
      var blanks = prevstartDay == 0 ? 7 : prevstartDay;
      for (var i = 1; i < blanks; i++) {
        // squares.push("");
        prevSqr.push({
          day: "",
          matchName: "",
          matchDate: "",
          format: "b",
          matchID: "",
          month: "",
        });
      }
    }
    if (!cal.sMon && prevstartDay != 0) {
      for (var i = 0; i < prevstartDay; i++) {
        // prevSqr.push("");
        prevSqr.push({
          day: "",
          matchName: "",
          matchDate: "",
          format: "b",
          matchID: "",
          month: "",
        });
      }
    }

    // Populate the days of the month
    for (var i = 1; i <= daysInMth; i++) {
      let obj = {};
      for (let match of props.matches || []) {
        if (
          `${new Date(+match.matchDateTimeIST).getFullYear()}-${new Date(
            +match.matchDateTimeIST
          ).getMonth()}-${new Date(+match.matchDateTimeIST).getDate()}` ==
          `${cal.sYear}-${cal.sMth}-${i}`
        ) {
          obj.day = i;
          obj.matchName = "";
          obj.matchDate = match.matchDateTimeIST;
          obj.format = match.compType;
          obj.matchID = match.matchID;
          obj.month = cal.sMth;
        }
      }
      // squares.push(i);
      if (obj.day) {
        squares.push(obj);
      } else {
        squares.push({
          day: i,
          matchName: "",
          matchDate: "",
          format: "",
          matchID: "",
          month: cal.sMth,
        });
      }
    }
    // Populate the days of the month
    for (var i = 1; i <= daysInPrevMth; i++) {
      let obj = {};
      for (let match of props.matches || []) {
        if (
          `${new Date(+match.matchDateTimeIST).getFullYear()}-${new Date(
            +match.matchDateTimeIST
          ).getMonth()}-${new Date(+match.matchDateTimeIST).getDate()}` ==
          `${cal.pYr}-${cal.pMth}-${i}`
        ) {
          obj.day = i;
          obj.matchName = "";
          obj.matchDate = match.matchDateTimeIST;
          obj.format = match.compType;
          obj.matchID = match.matchID;
          obj.month = cal.pMth;
        }
      }
      // squares.push(i);
      if (obj.day) {
        prevSqr.push(obj);
      } else {
        prevSqr.push({
          day: i,
          matchName: "",
          matchDate: "",
          format: "",
          matchID: "",
          month: cal.pMth,
        });
      }
    }

    // Determine the number of blank squares after end of month
    if (cal.sMon && endDay != 0) {
      var blanks = endDay == 6 ? 1 : 7 - endDay;
      for (var i = 0; i < blanks; i++) {
        // squares.push("");
        squares.push({
          day: "",
          matchName: "",
          matchDate: "",
          format: "b",
          matchID: "",
          month: "",
        });
      }
    }
    if (!cal.sMon && endDay != 6) {
      var blanks = endDay == 0 ? 6 : 6 - endDay;
      for (var i = 0; i < blanks; i++) {
        // squares.push("");
        squares.push({
          day: "",
          matchName: "",
          matchDate: "",
          format: "b",
          matchID: "",
          month: "",
        });
      }
    }
    setDate([...prevSqr, ...squares]);
    squares = [];
  };
  // alert(8)
  return (
    <div>
      <div className="flex flex-col lg:flex-row md:flex-row">
        <div className="flex flex-col w-full md:w-6/12 lg:6/12">
          <div className="flex w-full">
            <div className="w-20"></div>
            <div className="w-6/12 py-1 pl-2 gray font-normal text-sm flex items-center justify-center">
              {cal.mName[currentMonth]}
            </div>
            <div className="w-6/12 pl-2 py-1 flex text-sm font-normal items-center justify-center">
              {cal.mName[nextMonth]}
            </div>
          </div>
          <div className="flex items-center justify-center lg:justify-start md:justify-start">
            <div className="flex items-center w-full lg:w-full md:w-full pl-0 lg:pl-4 md:pl-4">
              <div
                className="flex flex-col w-1/12 lg:pl-2"
                style={{ height: "225px" }}
              >
                {["S", "M", "T", "W", "T", "F", "S"].map((day, i) => (
                  <div
                    className="h-2 w-2 flex items-center justify-center"
                    key={i}
                    style={{ height: "1.8rem", width: "1.8rem" }}
                  >
                    <div
                      className="flex items-center justify-center"
                      style={{
                        height: "0.9rem",
                        width: "0.9rem",
                      }}
                    >
                      <div className="gray text-sm">{day}</div>
                    </div>
                  </div>
                ))}
              </div>
              <div
                className="flex flex-col flex-wrap w-11/12"
                style={{ height: "225px" }}
              >
                {monthDays.map((obj, index) =>
                  obj.matchID == selectedMatch ? (
                    <div
                      key={index}
                      className="ba border-green border-2 flex items-center justify-center"
                      style={{ height: "1.8rem", width: "1.8rem" }}
                    >
                      <div
                        className="w-1 h-1"
                        style={{
                          height: "0.9rem",
                          width: "0.9rem",
                          backgroundColor: "#059A2D",
                        }}
                      />
                    </div>
                  ) : (
                    <div
                      key={index}
                      className="flex items-center justify-center  "
                      style={{ height: "1.8rem", width: "1.8rem" }}
                      onClick={() => {
                        obj.matchID &&
                          obj.matchID !== "" &&
                          setShowDate(obj.matchDate, obj.matchID);
                      }}
                    >
                      <div
                        className=""
                        style={{
                          height: "0.9rem",
                          width: "0.9rem",
                          backgroundColor:
                            obj.format == "b"
                              ? "#fff"
                              : obj.matchID
                              ? "#059A2D"
                              : obj.month === currentMonth
                              ? "#CBCBCB"
                              : "#CBCBCB",
                        }}
                      />
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
          <div className="flex items-center justify-end pr-4 -mt-4 lg:pr-6 xl:pr-6 md:pr-6  text-xs">
            {/* <div className="w-30 py-1 pl-2 gray font-normal text-sm flex items-center"><div className="w-20 "><div className="w1  h1" style={{ backgroundColor: "#CBCBCB" }}></div></div><div className=" w-80 "><div >Completed</div></div> </div>
            <div className="w-30 py-1 pl-2 gray font-normal text-sm flex items-center"><div className="w-20  "><div className="w1  h1" style={{ backgroundColor: "#F0F0F0" }}></div></div><div className=" w-80"><div >Upcoming</div></div> </div> */}
            <div className="w-2 h-2 bg-green rounded-full mx-2"></div> 
            Match days
          </div>
          {/* {alert(33)} */}
          {selectedMatchDate !== "" && (
            <div className="w-full lg:w-full flex p-2 lg:px-4 lg:py-2 md:px-4 md:py-2 items-center md:w-full">
              {/* <div className="">
              <div className="flex items-center justify-cente ">
                <div className="w1  h1" style={{
                  height: '0.9rem',
                  width: '0.9rem',
                }} />
              </div>
            </div> */}
              {selectedMatchDate && (
                <div className="w-80">
                  {format(parseInt(selectedMatchDate), "MMM d")}
                </div>
              )}
            </div>
          )}
          <div className="flex h-1 w-10 bg-blue-8 ml-2 -mt-1 mb-2"></div>
        </div>

        {showScorecard && scorecardData && (
          <div
            className="w-full px-0 lg:px-2 md:px-2 lg:w-6/12 md:6/12"
            onClick={() => {
              handleNavigation(scorecardData);
            }}
          >
            {/* handleNavigation(scorecardData) */}
            <Scorecard
              data={scorecardData}
              browser={"browser"}
              hideFantasyCriTab={true}
            />
          </div>
        )}
      </div>
    </div>
  );
}
