import React, { useState } from 'react';
const  Playerfallback = '/pngsV2/playerPH.png';
import { TEAM_PLAYER } from '../../api/queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Loading from '../Loading';
import { getPlayerUrl } from '../../api/services';
import Link from 'next/link';
const empty = '/svgs/Empty.svg';
export default function TeamPlayers({ teamID }) {
  const [ActiveTab, setActiveTab] = useState('test');

  const matchType = {
    label: ['test', 'odi', 't20'],
    value: ['test', 'odi', 't20']
  };
  const { loading, error, data } = useQuery(TEAM_PLAYER, {
    variables: { teamID: teamID },
    onCompleted: (PlayerData) => {
      if (PlayerData && PlayerData.teamPlayersV2) {
        setActiveTab(
          PlayerData.teamPlayersV2.hideTabs
            ? 't20'
            : PlayerData.teamPlayersV2.testSquad && PlayerData.teamPlayersV2.testSquad.length !== 0
            ? 'test'
            : PlayerData.teamPlayersV2.odiSquad && PlayerData.teamPlayersV2.odiSquad.length !== 0
            ? 'odi'
            : 't20'
        );
      }
    }
  });

  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return <>
      {data && data.teamPlayersV2 ? (
        <div className=' py-3'>
          {
            <div className='flex items-center justify-center  p-1 gap-2  mx-auto w-min rounded-full bg-gray md:bg-gray-8 lg:bg-gray-8 md:gap-8 lg:gap-8'>
              {data &&
                data.teamPlayersV2 &&
                !data.teamPlayersV2.hideTabs &&
                matchType.label.map((categ, i) => (
                  <div
                    key={i}
                    className=' flex  items-center   justify-center'
                    onClick={() => {
                      setActiveTab(categ);
                    }}>
                    <div
                      className={` py-1 px-6 text-center text-sm font-semibold uppercase  ${
                        ActiveTab === categ ? 'bg-gray-8 border-2 border-green-3' : ' '
                      }  rounded-full cursor-pointer`}>
                      {matchType.value[i]}
                    </div>
                  </div>
                ))}
            </div>
          }
          {data && data.teamPlayersV2[`${ActiveTab}Squad`].length > 0 ? (
            <div className=' flex flex-wrap  flex-row   p-2'>
              {data.teamPlayersV2[`${ActiveTab}Squad`].map((player, i) => (
                <div className=' w-1/3 flex items-center justify-center md:w-1/5 lg:w-1/5'>
                <div key={i} className=' w-full m-1 bg-gray  h-40  flex flex-col gap-3 items-center justify-center rounded-md md:h-64 lg:h-64'>
                  <div className='bg-gray-8 w-20 h-20 relative overflow-hidden cursor-pointer rounded-full md:h-32 lg:h-32 md:w-32 lg:w-32'>
                    <Link {...getPlayerUrl(player)} passHref legacyBehavior>
                        <img
                          className=' object-cover object-top w-full h-full '
                          src={`https://images.cricket.com/players/${player.playerID}_headshot_safari.png`}
                          onError={(e) => {
                            e.target.src = Playerfallback;
                          }}
                          alt=''
                        />
                    </Link>
                  </div>

                  <div className='text-center flex items-center justify-center w-4/5 h-1/5 text-sm font-semibold'>
                    <div className='py-2'>{player.playerName}</div>
                  </div>
                </div>
                </div>
              ))}
            </div>
          ) : (
            <div className='bg-gray fw5 text-centerf6 flex flex-col items-center justify-center py-4 '>
              <div className='w5 flex items-center justify-center ml3 h5'>
                <img className='h-full w-full' src={empty} alt='' />
              </div>
              <div>Player data not available</div>
            </div>
          )}
        </div>
      ) : (
        <div className='bg-gray fw5 f6 text-centerflex flex-col items-center justify-center py-4 '>
          <div className='w5 flex items-center justify-center ml3 h5'>
            <img className='h-full w-full' src={empty} alt='' />
          </div>
          <div>Player data not available</div>
        </div>
      )}
    </>;
}
