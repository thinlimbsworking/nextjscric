import React, { useState, useRef, useEffect } from "react";
// import flagPlaceHolder from '../../public/svgs/images/flag_empty.svg';
import { scheduleMatchView } from "../../api/services";
import Calender2 from "./calender2";
import CalenderV2 from "./calendarV2";
import Link from "next/link";
import { TEAM_SCHEDULE_V2, MATCH_DATA_FOR_SCORECARD } from "../../api/queries";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";
import Loading from "../Loading";
import { format } from "date-fns";
const empty = "/svgs/empty.svg";
import DataNotFound from "../../components/commom/datanotfound";
export default function TeamSchedule({ teamID }) {
  const flagPlaceHolder = "/pngsV2/emptyflag.svg";

  const [ActiveTab, setActiveTab] = useState("all");
  const [matchID, setmatchID] = useState(null);
  const [matchDate, setmatchDate] = useState("");
  const matchType = {
    label: ["all", "test", "odi", "t20"],
    value: ["all", "test", "odi", "t20"],
  };
  const [showscorecard, setshowScorecard] = useState(false);

  const [getScoreCard, { loading: ScorecardLoading, data: ScorecardData }] =
    useLazyQuery(MATCH_DATA_FOR_SCORECARD);
  const { loading, error, data } = useQuery(TEAM_SCHEDULE_V2, {
    variables: { teamID: teamID },
    onCompleted: (matchData) => {
      if (
        matchData &&
        matchData.teamsScheduleV2 &&
        matchData.teamsScheduleV2.scheduleMatches &&
        matchData.teamsScheduleV2.scheduleMatches.length > 0
      ) {
        // console.log("(matchData.teamsScheduleV2.scheduleMatches[0].matchID",(matchData.teamsScheduleV2.scheduleMatches))
        let firstUpcomingMatch = matchData.teamsScheduleV2.scheduleMatches.find(
          (element) => element.matchStatus === "upcoming"
        );
        //  console.log("firstUpcomingMatch",firstUpcomingMatch)
        setmatchID(firstUpcomingMatch ? firstUpcomingMatch.matchID : null);
        setmatchDate(
          firstUpcomingMatch ? firstUpcomingMatch.matchDateTimeIST : ""
        );
        if (firstUpcomingMatch) {
          getScoreCard({ variables: { matchID: firstUpcomingMatch.matchID } });
          setshowScorecard(true);
        }
        setActiveTab(matchData.teamsScheduleV2.hideTabs ? "t20" : "all");
      }
    },
  });

  const setShowDate = (matchDate, matchID) => {
    setmatchID(matchID);
    setmatchDate(matchDate);
    getScoreCard({ variables: { matchID: matchID } });
    setshowScorecard(true);
  };

  function getSeriesView(match) {
    let currentTab =
      match.matchStatus === "upcoming" &&
      match.playing11Status === false &&
      match.probable11Status === true
        ? "probableplaying11"
        : match.matchStatus === "upcoming" &&
          match.playing11Status === true &&
          match.probable11Status === false
        ? "playing11"
        : match.matchStatus === "upcoming" &&
          match.playing11Status === true &&
          match.probable11Status === true
        ? "playing11"
        : (match.matchStatus === "upcoming" || match.matchStatus === null) &&
          match.playing11Status === false &&
          match.probable11Status === false
        ? "matchInfo"
        : match.matchStatus === "completed"
        ? "scorecard"
        : "live";
    return scheduleMatchView(match);
  }

  const functionOnhange = (categ) => {
    let matchesData =
      categ === "test"
        ? data &&
          data.teamsScheduleV2 &&
          data.teamsScheduleV2.scheduleMatches.filter(
            (mt) => mt.compType === "Test"
          )
        : categ === "odi"
        ? data.teamsScheduleV2.scheduleMatches.filter(
            (mt) => mt.compType === "ODI"
          )
        : categ === "t20"
        ? data.teamsScheduleV2.scheduleMatches.filter(
            (mt) => mt.compType === "T20"
          )
        : data.teamsScheduleV2.scheduleMatches;
    // console.log("matchesData",matchesData)
    const getFirstUpcoming = matchesData.filter(
      (x) => x.matchStatus === "upcoming"
    );
    // console.log("getFirstUpcominggetFirstUpcoming",getFirstUpcoming)
    if (getFirstUpcoming.length > 0) {
      let matchIDs = getFirstUpcoming[0].matchID;
      let date = getFirstUpcoming[0].matchDateTimeIST;
      setmatchID(matchIDs);
      setmatchDate(date);
      getScoreCard({ variables: { matchID: matchIDs } });
      setshowScorecard(true);
    } else {
      let matchIDs = "";
      setmatchID(null);
      setmatchDate("");
      // getScoreCard({ variables: { matchID: "" } })
      setshowScorecard(false);
    }
  };

  if (error) return <div></div>;
  else if (loading) {
    return <Loading />;
  } else
    return <>
      {data && data.teamsScheduleV2 ? (
        <div>
          {data &&
          data.teamsScheduleV2 &&
          data.teamsScheduleV2.upcomingMatches &&
          data.teamsScheduleV2.upcomingMatches.length > 0 ? (
            <div className="bg-gray-8 text-white">
              <div className=" font-medium text-sm py-4 px-4 bg-gray-8">
                Upcoming Matches
              </div>
              <div className="flex h-1 w-10 bg-blue-8 -mt-3 ml-4 mb-4"></div>

              <div>
                <div className="flex items-center overflow-x-scroll cursor-pointer md:flex-row lg:flex-row">
                  {data.teamsScheduleV2.upcomingMatches.map((card, i) => (
                    <div key={i} className={"w-72 lg:w-80 md:w-80 px-1"}>
                      <Link {...getSeriesView(card)} passHref legacyBehavior>
                        <div
                          className={`w-full p-2
                          ml-2 pointer border-gray-8 bg-gray rounded-4xl border ${
                            i === 0 ? "" : ""
                          }  shadow-md `}
                        >
                          <div className="font-medium white justify-items-start mt-2 text-left -mb-2 text-base">
                            {card.matchNumber}
                            {/* {console.log(card,'oooo')} */}
                          </div>

                          <div className="flex items-center justify-between py-2">
                            <div className="flex ml-1 text-xs">
                              <div className="text-white text-sm font-medium text-left -ml-6 pl-5 f7-l f7-m">
                                {format(+card.matchDateTimeIST, "do LLL yyy")}
                              </div>
                            </div>
                            <div className="w-45 flex justify-end">
                              <div className="font-medium f9 f7-l f7-m text-gray-2">
                                {format(+card.matchDateTimeIST, " h:mm a")}
                              </div>
                            </div>
                          </div>

                          {/* <div className='flex items-center justify-between bg-gray-4 rounded-md py-2'>
                            <div className='flex items-center justify-center'>
                              {' '}
                              <img
                                className='h-10 w-10'
                                alt={flagPlaceHolder}
                                src={`https://images.cricket.com/teams/${card.homeTeamID}_flag_safari.png`}
                                onError={(evt) => (evt.target.src = flagPlaceHolder)}
                              />
                              <div className='font-medium fw6 pl1 f7-l f7-m f9'>{card.homeTeamShortName}</div>
                            </div>
                            <div className=' justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-1   py-0.5 '>{data.teamsScheduleV2.scheduleMatches[0].compType}</div>
                            

                            <div className='flex'>
                              {' '}
                              <img
                                className='h-10 w-10'
                                alt={flagPlaceHolder}
                                src={`https://images.cricket.com/teams/${card.awayTeamID}_flag_safari.png`}
                                onError={(evt) => (evt.target.src = flagPlaceHolder)}
                              />{' '}
                              <div className='font-medium f7-l fw6 f7-l pr1 f7-m f9'>{card.awayTeamShortName}</div>
                            </div>
                          </div> */}

                          <div className="flex px-4 md:px-6 lg:px-6 xl:px-6 justify-between rounded bg-gray-8 w-full">
                            <div className="flex items-center justify-center">
                              <div className="flex justify-center items-center py-2 w-10">
                                {" "}
                                <img
                                  className="rounded-md border-2 border-white border-solid"
                                  alt={flagPlaceHolder}
                                  src={`https://images.cricket.com/teams/${card.homeTeamID}_flag_safari.png`}
                                  onError={(evt) =>
                                    (evt.target.src = flagPlaceHolder)
                                  }
                                />{" "}
                              </div>
                              <div className="font-medium pl-1 lg:text-base md:text-base text-base">
                                {card.homeTeamShortName}{" "}
                              </div>
                            </div>

                            <div className="flex justify-center items-center border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-4 md:px-6 lg:px-6 my-2 mx-4 py-0.5 ">
                              {card.matchType}
                            </div>

                            <div className="flex items-center justify-center">
                              <div className="flex justify-center items-center py-2 w-10">
                                <img
                                  // alt={flagPlaceHolder}
                                  src={
                                    (card &&
                                      card.awayTeamID &&
                                      `https://images.cricket.com/teams/${card.awayTeamID}_flag_safari.png`) ||
                                    flagPlaceHolder
                                  }
                                  onError={(evt) =>
                                    (evt.target.src = flagPlaceHolder)
                                  }
                                  className="rounded-md border-2 border-white border-solid"
                                />{" "}
                              </div>
                              <div className="px-1 font-medium text-base">
                                {card.awayTeamShortName}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          ) : (
            <div className="bg-gray-4 font-medium flex text-sm flex-col items-center justify-center text-center py-2">
              <div className="w-5 flex items-center justify-center ml-4 h-5">
                <img className="h-full w-full" src={empty} alt="" />
              </div>
              <div>No upcoming Matches found</div>
            </div>
          )}
          <div className="bg-gray-8 mt-2 pb-2">
            <div className="font-medium text-sm p-3">Events Calender</div>
            <div className="flex h-1 w-10  bg-blue-8 ml-3 -mt-1.5 mb-4"></div>

            {
              <div className="flex items-center justify-center bg-gray-4 sm:bg-gray-4 md:bg-gray-8 py-1 rounded-full mx-10 my-4">
                {data &&
                  data.teamsScheduleV2 &&
                  !data.teamsScheduleV2.hideTabs &&
                  matchType.label.map((categ, i) => (
                    <div
                      key={i}
                      className="flex items-center md:w-28 lg:w-28 xl:w-28 w-5/12 text-center justify-center"
                      onClick={() => {
                        setActiveTab(categ);
                        functionOnhange(categ);
                      }}
                    >
                      <div
                        className={`px-4 pt-2 pb-2 cursor-pointer text-center text-xs font-medium static uppercase lg:px-4 ${
                          ActiveTab === categ
                            ? "bg-basebg rounded-full px-6 lg:px-8 md:px-8 xl:px-8 border-2 border-solid opacity-100 border-green"
                            : "md:bg-gray lg:bg-gray xl:bg-gray px-6 lg:px-8 md:px-8 xl:px-8"
                        }  rounded-full pointer`}
                      >
                        {matchType.value[i]}
                      </div>
                    </div>
                  ))}
              </div>
            }

            {/* <Calender2
              selectedMatch={matchID}
              scorecardData={
                ScorecardData &&
                ScorecardData.miniScoreCard &&
                ScorecardData.miniScoreCard.data &&
                ScorecardData.miniScoreCard.data.length > 0 &&
                ScorecardData.miniScoreCard.data[0]
              }
              selectedMatchDate={matchDate}
              showScorecard={showscorecard}
              setShowDate={setShowDate}
              key={"cal"}
              matches={
                ActiveTab === "test"
                  ? data.teamsScheduleV2.scheduleMatches.filter(
                      (mt) => mt.compType === "Test"
                    )
                  : ActiveTab === "odi"
                  ? data.teamsScheduleV2.scheduleMatches.filter(
                      (mt) => mt.compType === "ODI"
                    )
                  : ActiveTab === "t20"
                  ? data.teamsScheduleV2.scheduleMatches.filter(
                      (mt) => mt.compType === "T20"
                    )
                  : data.teamsScheduleV2.scheduleMatches
              }
            /> */}

          <CalenderV2
                    key={'cal'}
                    scorecardData={
                ScorecardData &&
                ScorecardData.miniScoreCard &&
                ScorecardData.miniScoreCard.data &&
                ScorecardData.miniScoreCard.data.length > 0 &&
                ScorecardData.miniScoreCard.data[0]
              }
              setShowDate={setShowDate}
                    matches={
                      ActiveTab === 'TEST'
                        ? data.teamsScheduleV2.scheduleMatches.filter(
                            mt => mt.compType === 'Test',
                          )
                        : ActiveTab === 'ODI'
                        ? data.teamsScheduleV2.scheduleMatches.filter(
                            mt => mt.compType === 'ODI',
                          )
                        : ActiveTab === 'T20'
                        ? data.teamsScheduleV2.scheduleMatches.filter(
                            mt => mt.compType === 'T20',
                          )
                        : data.teamsScheduleV2.scheduleMatches
                    }
                    selectedMatch={matchID}
                    showScorecard={showscorecard}
                  />






          </div>
        </div>
      ) : (
        <DataNotFound />
      )}
    </>;
}
