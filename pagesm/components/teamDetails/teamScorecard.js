import { format } from "date-fns";
import React from "react";
let downarrow = "/svgs/caret-down.svg";
let uparrow = "/svgs/caret-up.svg";
// import { format } from 'date-fns';
import Score from "../commom/score";
let location = "/pngsV2/location.png";

// ./pngsV2/Location.png
export default function TeamScorecard({
  data,
  RecentMatchIndex,
  changeRecentIndex,
  i,
}) {
  // console.log('111datadatadatadata', data);
  // const { loading, error, data: TeamStatHub } = useQuery(STAT_HUB_TEAM, {
  // variables: { matchID: props.matchID }
  // });

  return (
    <div>
      <div className="">
        {/* <StatHubTeam matchTypeData={data.matchType} matchID={data.matchID} seriesID={seriesID} awayTeamID={data[0].awayTeamID} homeTeamID={data[0].homeTeamID} matchStatus={data[0].matchStatus}
 matchType={data[0].matchType} language={props.language}/> */}
        {/* {data.matchStatus === 'completed' && (
 <div className='pt2'>
 {data.matchScore.map((x, i) => (
 <div key={i} className='flex items-center justify-evenly pv2'>
 <span className='flex w-40'>
 <img
 alt={''}
 src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
 onError={(evt) => (evt.target.src = flagPlaceHolder)}
 className='h1 w15 shadow-4'
 />
 <span className='mh2-l mh1 f6 fw6'> {x.teamShortName} </span>
 </span>
 <div className='w-60 flex items-center tracked-tight '>
 {x.teamScore &&
 x.teamScore.map((score, i) => (
 <div>
 <span key={i} className={`flex justify-center items-start f6 `}>
 {score.runsScored !== '0' ? (
 <span className={``}>
 {score.runsScored}
 {score.wickets && score.wickets !== '10' ? `/${score.wickets}` : null}
 </span>
 ) : null}
 {((data.matchType === 'Test' &&
 data.matchStatus !== 'completed' &&
 score.inning == data.currentinningsNo) ||
 data.matchType !== 'Test') && (
 <div className='ml2'>{score.overs ? `(${score.overs})` : null}</div>
 )}
 {score.declared ? <span className='red font-medium f6 ml1'>d</span> : ''}
 {score.folowOn ? <span className='red font-medium f6 ml1'> f/o</span> : ''}
 {x.teamScore.length > 1 && i === 0 && <div className='mh2 font-medium'>&</div>}
 </span>
 </div>
 ))}
 </div>
 </div>
 ))}
 <div className='truncate pv1'>
 <span className='br4 ph2 pv1 f8 font-medium tl black-80 bg-orange_2'>{data.matchResult}</span>
 </div>
 </div>
 )} */}

        {/* -----------------------------completed match card starts---------------------- */}

        {data.matchStatus == "completed" && (
          <>
            <Score
              hideSeriesName={true}
              featured={true}
              showpadding={false}
              data={data}
              shortName={true}
            />
            <div className="flex items-center relative justify-center w-full ">
              <div className="flex items-center justify-center bottom-10 absolute ">
                <div
                  onClick={() => changeRecentIndex(i)}
                  className="w-full pointer flex items-center justify-center"
                >
                  <img
                    className="w-8 h-5 cursor-pointer"
                    alt=""
                    src={RecentMatchIndex[i] ? uparrow : downarrow}
                  />
                </div>
              </div>
              <div
                className={`flex items-center justify-center white font-semibold absolute bottom-10 right-0 ${
                  data.win === "W"
                    ? "bg-green"
                    : data.win === "L"
                    ? "bg-red"
                    : "bg-gray"
                }`}
              >
                {data.win}
              </div>
            </div>

            {false && (
              <div
                className="bg-gray rounded-lg px-2 pb-2 "
                style={{ height: 335 }}
              >
                <div className="">{data.seriesName}</div>
                <div className="flex justify-between items-center pt-3 w-10/12">
                  <div className="">
                    <div className="flex items-center">
                      <div className="text-xs text-gray-2 font-semibold pr-3 uppercase">
                        {data.matchNumber}
                      </div>

                      {data.matchResult === "Match Cancelled" ||
                      data.isAbandoned ? (
                        <div className=" text-red-5 text-xs font-semibold rounded-full bg-red-5/30 border-red-5 border-2 p-1 px-2 flex uppercase justify-center items-center">
                          <div className="pr-1 text-red-5 text-xs"> x</div>{" "}
                          {data.isAbandoned ? "ABANDONED" : "Cancelled"}
                        </div>
                      ) : (
                        <div className=" text-gray-7 text-xs font-semibold rounded-full border-gray-7 border-2 px-2 py-0.5 flex uppercase justify-center items-center bg-gray-7/20">
                          <div className="w-1.5 h-1.5 bg-white rounded-full mr-1 "></div>{" "}
                          {data.matchStatus}{" "}
                        </div>
                      )}

                      <div className="text-xs flex text-white">
                        {(data.startDate &&
                          format(+data.startDate, "dd MMM yyy")) ||
                          ""}
                      </div>
                    </div>

                    <div className="flex items-center pt-1">
                      <img
                        className="w-4 h-5 shadow-2xl rounded-sm mr-2"
                        src={location}
                        alt=""
                      />
                      <div className="text-xs text-gray-2 font-semibold pl-1">
                        {data.venue}
                      </div>
                    </div>
                  </div>
                  {/* <div className=''>
 <img src={Bell} alt='' />
 </div> */}
                </div>

                <div className="bg-gray-4 my-2 rounded p-3">
                  <div className="flex justify-between items-center">
                    <div className="flex items-center text-white text-md font-semibold">
                      <img
                        className="w-10 h-6 shadow-2xl rounded-sm mr-2"
                        src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                        onError={(evt) =>
                          (evt.target.src = "/pngsV2/flag_dark.png")
                        }
                      />{" "}
                      {data.matchScore[0].teamShortName}
                    </div>
                    <div
                      className={`justify-center items-center flex border font-semibold ${
                        data.matchResult === "Match Cancelled" ||
                        data.isAbandoned
                          ? "border-gray-3 text-gray-3"
                          : "border-green text-green"
                      } rounded-2xl uppercase text-xs px-2 py-1`}
                    >
                      {data.matchType}
                    </div>
                    <div>
                      <div className="flex items-center text-white text-md font-semibold">
                        {" "}
                        {data.matchScore[1].teamShortName}
                        <img
                          className="w-10 h-6 shadow-2xl rounded-sm ml-2"
                          src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                          onError={(evt) =>
                            (evt.target.src = "/pngsV2/flag_dark.png")
                          }
                        />{" "}
                      </div>
                    </div>
                  </div>
                  <div className="flex justify-between items-center pt-2">
                    <div className="">
                      {data.matchScore &&
                        data.matchScore[0] &&
                        data.matchScore[0].teamScore &&
                        data.matchScore[0].teamScore.map((team, id) => (
                          <>
                            <div key={id} className="flex items-center">
                              {/* {console.log('team', team)} */}
                              {/* {0===id&& <TeamStathub matchID={data.matchID} matchStatus={data.matchStatus} matchType={data.matchType} />} */}

                              <p
                                className={`${
                                  data.winningTeamID === team.teamID
                                    ? "text-green"
                                    : "text-white"
                                } text-sm font-semibold pb-1`}
                              >
                                {team.runsScored}/{team.wickets}
                              </p>
                              {team.overs && (
                                <p className="text-xs pl-1 font-medium text-white">
                                  ({team.overs})
                                </p>
                              )}
                              {team.declared && (
                                <p className="text-sm text-green pl-1 font-semibold">
                                  d
                                </p>
                              )}
                              {team.folowOn && (
                                <p className="text-sm text-green pl-1 font-semibold">
                                  f
                                </p>
                              )}
                            </div>
                          </>
                        ))}
                    </div>
                    <div className="">
                      {data.matchScore &&
                        data.matchScore[1] &&
                        data.matchScore[1].teamScore &&
                        data.matchScore[1].teamScore.map((team, id) => (
                          <div key={id} className="flex items-center">
                            {team.runsScored && (
                              <p
                                className={`${
                                  data.winningTeamID === team.teamID
                                    ? "text-green"
                                    : "text-white"
                                } text-sm font-semibold pb-1`}
                              >
                                {team.runsScored}/{team.wickets || 0}
                              </p>
                            )}
                            {team.overs && (
                              <p className="text-xs pl-1 font-medium text-white">
                                ({team.overs})
                              </p>
                            )}
                            {team.declared && (
                              <p className="text-sm text-green pl-1 font-semibold">
                                d
                              </p>
                            )}
                            {team.folowOn && (
                              <p className="text-sm text-green pl-1 font-semibold">
                                f
                              </p>
                            )}
                          </div>
                        ))}
                    </div>
                  </div>
                </div>

                {data.matchResult === "Match Cancelled" || data.isAbandoned ? (
                  <div className=" bg-gray-4 text-center my-2 rounded py-2 text-xs font-semibold ">
                    {data.statusMessage}
                  </div>
                ) : (
                  <div className=" bg-gray-4 text-center my-2 rounded py-2 text-green text-xs font-semibold ">
                    {data.matchResult}
                  </div>
                )}

                {
                  <div className="">
                    <div className="flex justify-start items-center ">
                      {data.matchResult !== "Match Cancelled" &&
                      !data.isAbandoned ? (
                        <div class="flex relative items-center">
                          <img
                            class="h-14 w-14 bg-gray-4 object-cover object-top rounded-full p-1"
                            src={`https://images.cricket.com/players/${
                              data.playerOfTheMatchdDetails &&
                              data.playerOfTheMatchdDetails.playerID
                            }_headshot_safari.png`}
                            onError={(evt) =>
                              (evt.target.src = "/pngsV2/MOM2.png")
                            }
                          />

                          {/* <img
 class="absolute h-5 w-5 right-0 bottom-0 object-fill rounded shadow-2xl border-2"
 src={`https://images.cricket.com/teams/${
 data.playerOfTheMatchdDetails &&
 data.playerOfTheMatchdDetails.playerTeamID
 }_flag_safari.png`}
 onError={(evt) =>
 (evt.target.src = "/pngsV2/flag_dark.png")
 }
 /> */}
                        </div>
                      ) : (
                        <img
                          className="h-16 w-16 bg-gray-4 rounded-full p-2"
                          src={mom}
                        />
                      )}
                      {data.matchResult == "Match Cancelled" ? (
                        <div className="text-white text-sm font-semibold ">
                          Match Cancelled
                        </div>
                      ) : data.isAbandoned ? (
                        <div className="text-gray-2 text-sm font-semibold ml-3">
                          Match Abandoned
                        </div>
                      ) : (
                        <div className=" ml-2">
                          <div className="text-white text-sm font-semibold ">
                            {(data.playerOfTheMatchdDetails &&
                              data.playerOfTheMatchdDetails.playerName) ||
                              "TBA"}
                          </div>
                          <div className=" capitalize text-gray-2 text-xs font-medium">
                            player Of The Match
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                }

                <div>
                  {data.matchStatus === "live" && (
                    <div className="flex items-center mr-1 border-black br-pill py-4 px-1">
                      <span className="bg-green h-4 w-4 br-100" />
                      <span className="f10 font-medium black-40 pl-1">
                        LIVE
                      </span>
                    </div>
                  )}
                </div>
                <div className="flex items-center justify-center mt-8">
                  <div
                    onClick={() => changeRecentIndex(i)}
                    className="w-full pointer flex items-center justify-center"
                  >
                    <img
                      className="w-8 h-5"
                      alt=""
                      src={RecentMatchIndex[i] ? uparrow : downarrow}
                    />
                  </div>
                  <div
                    className={`flex items-center justify-center white font-semibold ${
                      data.win === "W"
                        ? "bg-green"
                        : data.win === "L"
                        ? "bg-red"
                        : "bg-gray"
                    }`}
                  >
                    {data.win}
                  </div>
                </div>
              </div>
            )}
          </>
        )}

        {/* ----------------------completed match block ends-------------------- */}

        {/* 
 {(data.matchStatus === null || data.matchStatus === 'upcoming') && (
 <div>
 <div className='flex mb2'>
 <div className='flex w-40 flex-column justify-center items-evenly'>
 {data.matchScore &&
 data.matchScore.map((x, i) => (
 <div key={i} className='flex pv2'>
 <img
 className='h1 w15 shadow-4'
 src={`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}
 onError={(e) => {
 e.target.src = flagPlaceHolder;
 }}
 alt=''
 />
 <span className=' mh2 f6 fw6'> {x.teamShortName} </span>
 </div>
 ))}
 </div>
 <div className='w-60 flex flex-column justify-center '>
 <span
 className={`pv1 br4 f7 font-medium black-80 tl mt1 truncate ${
 new Date(Number(data.startDate) - 19800000).getTime() - new Date().getTime() > 88200000
 ? ''
 : ' darkRed'
 }`}>
 {new Date(Number(data.startDate) - 19800000).getTime() - new Date().getTime() > 88200000 ? (
 format(new Date(Number(data.startDate) - 19800000), 'do MMMM')
 ) : new Date(Number(data.startDate) - 19800000).getDate() != new Date().getDate() ? (
 <div className={` f7 font-medium black-80 tl truncate`}>
 {format(new Date(Number(data.startDate) - 19800000), 'do MMMM')}
 </div>
 ) : (
 <div className={`dark_red f7 fw6 ff-msb`}>Today</div>
 )}
 </span>

 <div className='f6 pv2'> {format(Number(data.startDate) - 19800000, 'h:mm a')} IST</div>
 </div>
 </div>

 <div className='flex '>
 {true ? (
 data.statusMessage ? (
 <span className='pa1 ph2 br4 f8 font-medium tc black-80 bg-orange_2 truncate'>
 {' '}
 {data.statusMessage}{' '}
 </span>
 ) : new Date(Number(data.startDate) - 19800000) - new Date().getTime() < 84600000 ? (
 <Countdown
 date={new Date(Number(data.startDate) - 19800000 - 1800000)}
 renderer={(props) => (
 <span className='pa2 ph2 br4 f8 font-medium black-80 tl bg-orange_2 truncate'>
 {' '}
 {`${props.days !== 0 ? props.days * 24 + Number(props.hours) : props.hours} hrs ${
 props.minutes
 } mins `}
 {'to toss'}
 </span>
 )}
 />
 ) : (
 <span className='pa1 ph2 br4 f8 font-medium black-80 tl bg-orange_2 mt1 truncate '>
 {new Date(Number(data.startDate) - 19800000).getTime() - new Date().getTime() > 88200000 ? (
 `${formatDistanceToNowStrict(new Date(Number(data.startDate) - 19800000), {
 unit: 'day'
 })} to go`
 ) : new Date(Number(data.startDate) - 19800000).getDate() != new Date().getDate() ? (
 <div className={`f8 font-medium tc black-80 truncate`}>
 {format(new Date(Number(data.startDate) - 19800000), 'dd MMM yyyy')}
 </div>
 ) : (
 <div className={`dark_red f6 ff-msb`}>Today</div>
 )}
 </span>
 )
 ) : (
 <></>
 )}
 </div>
 </div>
 )} */}

        {/*------------------------------ upcoming card block starts--------------------------------------------- */}

        {/* {console.log("datadatdata",item)} */}
        {(data.matchStatus === "upcoming" ||
          (data.matchStatus == "live" && !data.isLiveCriclyticsAvailable)) && (
          <div
            className="bg-gray rounded-lg px-8 py-3 "
            style={{ height: 350 }}
          >
            <div
              className="font-semibold text-md tracking-wide 
 "
            >
              {data.seriesName}
            </div>
            <div className="flex justify-between items-center pt-3">
              <div className="">
                <div className="flex items-center">
                  <div className="text-xs text-gray-2 font-semibold pr-3 uppercase">
                    {data.matchNumber}
                  </div>
                  <div
                    className={`font-semibold text-xs rounded-full ${
                      data.matchStatus == "live" &&
                      !data.isLiveCriclyticsAvailable
                        ? "border-red-5 text-white bg-red-5 "
                        : " text-blue-9 border-blue-9 bg-blue-9/20"
                    } border-2 px-2 py-0.5 flex uppercase justify-center items-center`}
                  >
                    <div
                      className={`w-1.5 h-1.5 ${
                        data.matchStatus == "live" &&
                        !data.isLiveCriclyticsAvailable
                          ? "bg-white"
                          : " bg-blue-9"
                      } rounded-full mr-1 `}
                    ></div>{" "}
                    {data.matchStatus}{" "}
                  </div>
                </div>
                <div className="flex items-center pt-2">
                  <img
                    src={location}
                    className="w-4 h-5 shadow-2xl rounded-sm mr-2"
                  />
                  <div className="text-xs text-gray-2 font-semibold pl-1">
                    {data.venue}
                  </div>
                </div>
              </div>
              {/* <div className=''>
 <img src={Bell} alt='' />
 </div> */}
            </div>

            <div className="flex bg-gray-4 justify-between items-center my-3 rounded px-3 py-5 ">
              <div className="flex items-center text-white text-md font-semibold">
                <img
                  className="w-10 h-6 shadow-2xl rounded-sm mr-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                />
                {data.matchScore[0].teamShortName}
              </div>

              <div className="justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3 py-0.5 ">
                {data.matchType}
              </div>
              <div className="flex items-center text-white text-md font-semibold">
                {data.matchScore[1].teamShortName}
                <img
                  className="w-10 h-6 shadow-2xl rounded-sm ml-2"
                  src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = "/pngsV2/flag_dark.png")}
                />
              </div>
            </div>

            <div className="flex bg-gray-4 justify-center items-center my-3 rounded py-2 text-white">
              {data.toss ? (
                <p className="text-sm font-medium text-center ">{data.toss}</p>
              ) : (
                <div className="flex items-center">
                  <Countdown
                    date={new Date(Number(data.startDate) - 19800000 - 1800000)}
                    renderer={(props) => (
                      <span className="text-sm font-semibold">
                        {`${
                          props.days !== 0
                            ? props.days * 24 + Number(props.hours) + " hrs"
                            : props.hours == 0
                            ? ""
                            : props.hours + " hrs"
                        } ${props.minutes} mins to toss`}
                      </span>
                    )}
                  />
                </div>
              )}
            </div>

            {data.teamsWinProbability && (
              <div className="">
                <div className="text-gray-2 text-xs my-2 font-medium">
                  WIN PERCENTAGE
                </div>
                <div className=" flex bg-gray-3 rounded-full h-3 overflow-hidden ">
                  <div
                    className={`${"bg-green"} h-3 rounded-l-lg`}
                    style={{
                      width: data.teamsWinProbability.homeTeamPercentage + "%",
                    }}
                  />
                  <div
                    className={` bg-gray-3 h-3 `}
                    style={{
                      width: data.teamsWinProbability.tiePercentage + "%",
                    }}
                  />
                  <div
                    className={`${"bg-white"} h-3 rounded-r-lg `}
                    style={{
                      width: data.teamsWinProbability.awayTeamPercentage + "%",
                    }}
                  />
                </div>
              </div>
            )}
            {data.teamsWinProbability && (
              <div className="flex mt-3 justify-between items-center text-gray-2">
                <div className=" flex items-center text-xs font-medium">
                  <p className={`w-2 h-2 ${"bg-green"} rounded-full m-1 `}></p>
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.homeTeamShortName}{" "}
                  (
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.homeTeamPercentage}
                  %)
                </div>
                {data.teamsWinProbability &&
                  data.teamsWinProbability.tiePercentage && (
                    <div className="flex items-center text-xs font-medium">
                      <p className="w-2 h-2 bg-gray-3 rounded-full m-1 "></p>
                      {data.matchType != "Test" ? "Tie" : "DRAW"} (
                      {(data.teamsWinProbability &&
                        data.teamsWinProbability.tiePercentage) ||
                        0}
                      %)
                    </div>
                  )}
                <div className="flex items-center text-xs font-medium">
                  <p className={`w-2 h-2 ${"bg-white"} rounded-full m-1 `}></p>
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.awayTeamShortName}{" "}
                  (
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.awayTeamPercentage}
                  %)
                </div>
              </div>
            )}
          </div>
        )}

        {/* -------------------------------------------end of upcoming match card------------------------------------ */}

        {/* {data.matchStatus === 'live' && (
 <div className=''>
 {data.matchScore &&
 data.matchScore.map((x, i) => (
 <div key={i} className='flex items-center justify-evenly pv2'>
 <span className='flex w-30'>
 <img
 className='h1 w15 shadow-4'
 src={`${`https://images.cricket.com/teams/${x.teamID}_flag_safari.png`}`}
 onError={(e) => {
 e.target.src = flagPlaceHolder;
 }}
 alt=''
 />
 <span className='ml2 f6 fw6'> {x.teamShortName} </span>
 </span>
 <div className='w-70 flex items-center tracked-tight'>
 {x.teamScore &&
 x.teamScore.map((score, i) => (
 <span key={i} className='flex'>
 <span
 className={`f6 ${
 data.currentinningsNo == score.inning && data.matchStatus !== 'completed' ? 'fw6' : ''
 } `}>
 {' '}
 {score.runsScored}
 {score.wickets && score.wickets !== '10' ? `/${score.wickets}` : null}{' '}
 </span>
 {}
 {((data.matchType === 'Test' &&
 data.matchStatus !== 'completed' &&
 score.inning == data.currentinningsNo) ||
 data.matchType !== 'Test') &&
 score.overs !== '' && <div className='ml2'>({score.overs})</div>}
 {score.declared ? <span className='red font-medium f6 ml2'>d</span> : null}
 {score.folowOn ? <span className='red font-medium f6'>f/o</span> : null}
 {x.teamScore.length > 1 && i === 0 && <div className='mh2 font-medium'>&</div>}
 </span>
 ))}
 </div>
 </div>
 ))}
 {}
 {data.statusMessage && (
 <div className='truncate pv1'>
 <span className='br-pill ph2 pv1 f9 fw6 tl black-80 bg-orange_2'>{data.statusMessage}</span>
 </div>
 )}
 </div>
 )} */}

        {/* --------------------------------------live match card----------------------------------- */}
        {/* {console.log(data, "dataaaa")} */}

        {data.matchStatus == "live" && data.isLiveCriclyticsAvailable && (
          <div className="bg-gray rounded-lg px-2 py-3" style={{ height: 350 }}>
            <div className="font-semibold text-md tracking-wide ">
              {data.seriesName}
            </div>
            <div className="flex justify-between datas-center pt-3">
              <div className="">
                <div className="flex items-center">
                  <div className="text-xs text-gray-2 font-semibold pr-3 uppercase">
                    {data.matchNumber}
                  </div>
                  <div className=" font-semibold text-xs rounded-full bg-red-5 border-red-5 border-2 px-1 py-0.5 flex uppercase text-white justify-center items-center">
                    <div className="w-1.5 h-1.5 bg-white rounded-full mr-1 "></div>{" "}
                    {data.matchStatus}{" "}
                  </div>
                </div>
                <div className="flex items-center">
                  <img
                    src={location}
                    className=" w-4 h-5 shadow-2xl rounded-sm mr-2"
                  />
                  <div className="text-xs text-gray-2 font-semibold pl-1">
                    {data.venue}
                  </div>
                </div>
              </div>
              {/* <div className=''>
 <img src={Bell} alt='' />
 </div> */}
            </div>

            <div className=" bg-gray-4 my-3 rounded p-3 ">
              <div className="flex justify-between items-center">
                <div className="">
                  <div className="flex items-center text-white text-md font-semibold">
                    <img
                      className="w-10 h-6 shadow-2xl rounded-sm mr-2"
                      src={`https://images.cricket.com/teams/${data.matchScore[0].teamID}_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = "/pngsV2/flag_dark.png")
                      }
                    />{" "}
                    {data.matchScore[0].teamShortName}
                  </div>
                </div>
                <div className="justify-center items-center flex border-2 font-semibold border-green rounded-full text-green uppercase text-xs px-3 py-0.5 ">
                  {data.matchType}
                </div>
                <div>
                  <div className="flex items-center text-white text-md font-semibold">
                    {" "}
                    {data.matchScore[1].teamShortName}
                    <img
                      className="w-10 h-6 shadow-2xl rounded-sm ml-2"
                      src={`https://images.cricket.com/teams/${data.matchScore[1].teamID}_flag_safari.png`}
                      onError={(evt) =>
                        (evt.target.src = "/pngsV2/flag_dark.png")
                      }
                    />{" "}
                  </div>
                </div>
              </div>

              {data.matchScore &&
                data.matchScore[0] &&
                data.matchScore[0].teamScore.length > 0 && (
                  <div className="flex justify-between items-center pt-2">
                    <div className="">
                      {data.matchScore &&
                        data.matchScore[0] &&
                        data.matchScore[0].teamScore &&
                        data.matchScore[0].teamScore.map((team, id) => (
                          <div key={id} className="flex items-center">
                            <p
                              className={`${
                                data.winningTeamID === team.teamID
                                  ? "text-green"
                                  : "text-white"
                              } text-sm font-semibold pb-1`}
                            >
                              {team.runsScored}/{team.wickets}
                            </p>
                            {team.overs && (
                              <p className="text-xs pl-1 font-medium text-white">
                                ({team.overs})
                              </p>
                            )}
                            {team.declared && (
                              <p className="text-sm text-green pl-1 font-semibold">
                                d
                              </p>
                            )}
                            {team.folowOn && (
                              <p className="text-sm text-green pl-1 font-semibold">
                                f
                              </p>
                            )}
                          </div>
                        ))}
                    </div>
                    <div className="">
                      {data.matchScore &&
                      data.matchScore[1] &&
                      data.matchScore[1].teamScore.length > 0 ? (
                        data.matchScore[1].teamScore.map((team, id) => (
                          <div key={id} className="flex items-center">
                            {team.runsScored && (
                              <p
                                className={`${
                                  data.winningTeamID === team.teamID
                                    ? "text-green"
                                    : "text-white"
                                } text-sm font-semibold pb-1`}
                              >
                                {team.runsScored}/{team.wickets}
                              </p>
                            )}
                            {team.overs && (
                              <p className="text-xs pl-1 font-medium text-white">
                                ({team.overs})
                              </p>
                            )}
                            {team.declared && (
                              <p className="text-sm text-green pl-1 font-semibold">
                                d
                              </p>
                            )}
                            {team.folowOn && (
                              <p className="text-sm text-green pl-1 font-semibold">
                                f
                              </p>
                            )}
                          </div>
                        ))
                      ) : (
                        <div className="text-gray-2 text-xs font-medium ">
                          Yet to bat
                        </div>
                      )}
                    </div>
                  </div>
                )}
            </div>
            <div className="flex bg-gray-4 justify-center items-center my-3 rounded p-2 text-white text-sm font-medium text-center">
              {data.statusMessage}
            </div>

            {data.teamsWinProbability && (
              <div className="">
                <div className="text-gray-2 text-xs my-2 font-medium">
                  WIN PERCENTAGE
                </div>
                <div className=" flex bg-gray-3 rounded-full h-3 ">
                  <div
                    className={`${
                      parseInt(data.teamsWinProbability.homeTeamPercentage) >
                      parseInt(data.teamsWinProbability.awayTeamPercentage)
                        ? "bg-green"
                        : "bg-white"
                    } h-3 rounded-l-lg`}
                    style={{
                      width: data.teamsWinProbability.homeTeamPercentage + "%",
                    }}
                  />
                  <div
                    className={` bg-gray-3 h-3`}
                    style={{
                      width: data.teamsWinProbability.tiePercentage + "%",
                    }}
                  />
                  <div
                    className={`${
                      parseInt(data.teamsWinProbability.homeTeamPercentage) <
                      parseInt(data.teamsWinProbability.awayTeamPercentage)
                        ? "bg-green"
                        : "bg-white"
                    } h-3 rounded-r-lg `}
                    style={{
                      width: data.teamsWinProbability.awayTeamPercentage + "%",
                    }}
                  />
                </div>
              </div>
            )}
            {data.teamsWinProbability && (
              <div className="flex mt-3 justify-between items-center text-gray-2">
                <div className=" flex items-center text-xs font-medium">
                  <p
                    className={`w-1.5 h-1.5 ${
                      parseInt(data.teamsWinProbability.homeTeamPercentage) >
                      parseInt(data.teamsWinProbability.awayTeamPercentage)
                        ? "bg-green"
                        : "bg-white"
                    } rounded-full m-1 `}
                  ></p>
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.homeTeamShortName}{" "}
                  (
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.homeTeamPercentage}
                  %)
                </div>
                {data.teamsWinProbability && (
                  <div className="flex items-center text-xs font-medium">
                    <p className="w-1.5 h-1.5 bg-gray-3 rounded-full m-1 "></p>
                    {data.matchType != "Test" ? "Tie" : "DRAW"} (
                    {(data.teamsWinProbability &&
                      data.teamsWinProbability.tiePercentage) ||
                      0}
                    %)
                  </div>
                )}
                <div className="flex items-center text-xs font-medium">
                  <p
                    className={`w-1.5 h-1.5 ${
                      parseInt(data.teamsWinProbability.homeTeamPercentage) <
                      parseInt(data.teamsWinProbability.awayTeamPercentage)
                        ? "bg-green"
                        : "bg-white"
                    } rounded-full m-1 `}
                  ></p>
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.awayTeamShortName}{" "}
                  (
                  {data.teamsWinProbability &&
                    data.teamsWinProbability.awayTeamPercentage}
                  %)
                </div>
              </div>
            )}
          </div>
        )}
      </div>
      {/* <div className="block relative shadow-3 py-2 px-2 lg:py-4 xl:py-4 md:py-4 xl:px-4 lg:px-4 md:px-4 w-100 bg-gray h-45 text-white">
 <div className='flex justify-between'>
 <div>
 <h3 className='m-0 f6 font-medium truncate'>
 <span className='text-gray-2 font-medium f7 '>{data.matchNumber}</span>
 </h3>
 </div>

 <div className='text-gray-2 font-medium f7'>{(data.startDate && format(+data.startDate, 'dd-MMM-yyy')) || ''}</div>
 </div>

 <div className='flex items-center justify-between pt-2'>
 <div className='flex items-center'>
 <img alt='Location' src='/svgs/location-icon.svg' className='ib w1 h1 f4' />
 <span className='ml-2 text-gray-2 f7'>{data.venue}</span>
 </div>
 </div>
 </div> */}
    </div>
  );
}
