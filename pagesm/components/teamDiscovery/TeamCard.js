import React from 'react';
import RankComp from './ranking';
import IplComp from './ipl';
import Link from 'next/link';
import { getTeamUrl } from '../../api/services';
import ImageComponent from '../commom/Image';

const defaultImg = '/pngs/team-web.jpg';

function TeamCard({ team, tab }) {
  const handleTeamNavigation = (teamName, teamID) => {
    CleverTap.initialize('Teams', {
      Source: 'TeamsDiscovery',
      teamID: teamID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };

  team.name = team.teamName;
  return (
    (<Link
      {...getTeamUrl(team, tab)}
      onClick={() => handleTeamNavigation(team.teamName, team.teamID)}
      passHref
      className='  w-1/2 py-2 px-1 lg:w-3/12 md:w-3/12 flex items-center '>

      <div className='flex flex-col rounded-md -mt-2 pt-2 px-2 bg-gray overflow-hidden w-full '>
        <div className='center flex items-center justify-center'></div>
        <div className=' '>
          <div className='w-full h-24  relative flex items-center justify-center '>
            <ImageComponent
              layout='fill'
              fallbackSrc={defaultImg}
              className='object-cover h-full w-full'
              src={`https://images.cricket.com/teams/${team.teamID}_actionshot_safari.jpg`}
            />
            <img
              className='absolute h-5 w-6 border-2 rounded-md mt-24'
              src={`https://images.cricket.com/teams/${team.teamID}_flag_safari.png`}></img>
          </div>
        </div>

        <p className='Montserrat text-center text-xs font-semibold mt-4 uppercase pb-2'>
          {tab == 'international' && team.teamName || tab !=='international' && team.teamName}
        </p>
        <div className='justify-center text-center'>
          {tab!=='international' && <span className=' text-xs font-thin '>YEAR WON</span>}
        </div>
        {tab == 'international' ? (
          <>
            {team.odiRanking !== '' && team.t20Ranking !== '' && team.testRanking !== '' && (
              <div className=' font-semibold text-xs'>
                <RankComp team={team} />
              </div>
            )}
          </>
        ) : (
          <IplComp years={team.trophy_details} />
        )}
      </div>

    </Link>)
  );
}

const p = React.memo(TeamCard);
export default p;
