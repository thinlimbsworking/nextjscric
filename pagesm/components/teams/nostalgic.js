import React from "react";
import HrLine from "../Common/HrLine";
import DataNotFound from "../../componentV2/common/dataNotFound";
const Nostalgic = props => {
  const nostalgicArray = props.NostalgicArray;
  if (nostalgicArray != null) {
    return (
      <div className="bg-light-yellow">
        <div className="grey_10 pa3 f7 fw5">Nostalgic Matches</div>
        <HrLine />
        <div className="flex overflow-scroll ml2">
          {nostalgicArray.map(data => {
            return <div>{singlePage(data)}</div>;
          })}
        </div>
      </div>
    );
  } else {
    return (
      <div className="flex justify-center items-center">
        <DataNotFound />
      </div>
    );
  }

  function singlePage(data) {
    return (
      <div
        className="pv2 pr3 overflow-scroll"
        id={data.type}
        onClick={() => {}}
      >
        <div className="w5 h4">
          <img className="w5 h4" src={data.path} alt="" />
        </div>
        <div className="w5 f7 mv2 grey_10 lh-copy">{data.longDescription}</div>
      </div>
    );
  }
};
export default Nostalgic;
