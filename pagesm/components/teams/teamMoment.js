import React from "react";
import DataNotFound from "../../componentV2/common/dataNotFound";
import HrLine from "../Common/HrLine";

const PlayerMoments = props => {
  const imageData = props.imageData;
  return (
    <div className="cardShadow br1 pa3 bg-white">
      <div className="f7 fw5">Magical Moments</div>
      <HrLine />
      {imageData && imageData.length > 0 ? (
        <React.Fragment>
          <div
            style={{
              ...pageStyle.bigImg,
              backgroundImage: `linear-gradient(to right, rgba(166, 59, 161, 0.3), rgba(250, 148, 65, 0)), url(${
                imageData[0].path
              })`
            }}
          />
          {imageData.length > 1 && (
            <div className="flex justify-between mt2">
              <div
                style={{
                  ...pageStyle.smallImg,
                  backgroundImage: `linear-gradient(to right, rgba(40, 174, 244, 0.5), rgba(250, 148, 65, 0)), url(${
                    imageData[1].path
                  })`
                }}
              />
              {imageData.length > 2 && (
                <div
                  style={{
                    ...pageStyle.smallImg,
                    backgroundImage: `linear-gradient(to right, rgba(69, 149, 72, 0.5), rgba(250, 148, 65, 0)), url(${
                      imageData[2].path
                    })`
                  }}
                />
              )}
              {imageData.length > 3 && (
                <div
                  className="flex justify-center items-center f3 white"
                  style={{
                    ...pageStyle.smallImg,
                    backgroundImage: `linear-gradient(to right, rgba(20, 27, 47, 0.5), rgba(20, 27, 47, 0.5)), url(${
                      imageData[3].path
                    })`
                  }}
                >
                  {imageData.length - 4 === 0 ? "" : `+${imageData.length - 4}`}
                </div>
              )}
            </div>
          )}
        </React.Fragment>
      ) : (
        <DataNotFound />
      )}
    </div>
  );
};

const pageStyle = {
  smallImg: {
    minHeight: "100px",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    // backgroundBlendMode: "lighten",
    width: "32%"
  },
  bigImg: {
    minHeight: "150px",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top center",
    marginTop: "16px"
  }
};

export default PlayerMoments;
