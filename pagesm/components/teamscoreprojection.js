import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { GET_CRICLYTICS_COMMON_API } from '../api/queries'

export default function TeamScoreProjection(props) {
  const [filterData, setFilterData] = useState({})
  const [maxRun, setMaxRun] = useState()

  const { loading, error, data } = useQuery(GET_CRICLYTICS_COMMON_API, {
    variables: { matchId: props.matchID, innings: '' },
    pollInterval: 8000,
    onCompleted: (data) => {
      setFilterData(data.getcriclyticsCommonApi.TeamProjections[0])
      setMaxRun(
        Math.max(
          data.getcriclyticsCommonApi.TeamProjections[0].predictedScore,
          data.getcriclyticsCommonApi.TeamProjections[0].secondPredictedScore,
          data.getcriclyticsCommonApi.TeamProjections[0].thirdPredictedScore,
          data.getcriclyticsCommonApi.TeamProjections[0].fourthPredictedScore,
        ),
      )
    },
  })
  // console.log('Wswdw', data);
  return data && data.getcriclyticsCommonApi && filterData ? (
    <div className="bg-gray mt-3 mx-2 p-3 rounded-md text-white">
      <div className="font-semibold text-lg tracking-wider">
        Team Score Projection
      </div>
      <div className="flex justify-between items-center mt-4">
        <div className="flex items-center">
          <img
            className="w-8 h-5 rounded-sm"
            src={`https://images.cricket.com/teams/${filterData.team1Id}_flag_safari.png`}
            onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
          />
          <div className="pl-2 text-md font-semibold">
            {filterData.team1ShortName}
          </div>
        </div>
        <div className="w-20 h-0.5 bg-basebg relative object-center flex justify-center">
          <div className="w-6 h-6 bg-basebg -mt-2.5 rounded-full text-xs font-semibold flex items-center justify-center">
            vs
          </div>
        </div>
        <div className="flex items-center">
          <div className="pr-2 text-md font-semibold">
            {filterData.team2ShortName}
          </div>
          <img
            className="w-8 h-5 rounded-sm text-md "
            src={`https://images.cricket.com/teams/${filterData.team2Id}_flag_safari.png`}
            onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
          />
        </div>
      </div>
      {filterData && filterData.inningIds && filterData.inningIds.length == 2 && (
        <div className="mt-4 flex justify-between items-center">
          <div className="flex items-center text-md font-medium">
            <div className="pr-2">
              {filterData.inningNo == 1 &&
              filterData.inningIds[0] == filterData.team1Id
                ? filterData.currentScore + '/' + filterData.currentWickets
                : filterData.predictedScore + '/' + filterData.predictedWicket}
            </div>
            <div className="font-normal text-sm">
              {filterData.inningNo == 1 &&
              filterData.inningIds[0] == filterData.team1Id
                ? '(' + filterData.currentOvers + ')'
                : '(' + filterData.predictedOver + ')'}
            </div>
          </div>
          <div
            className={`${
              filterData.inningNo == 1 &&
              filterData.inningIds[0] == filterData.team1Id
                ? 'text-gray-2 text-sm'
                : 'text-white text-md'
            }  font-medium flex items-center`}
          >
            <div className="pr-2">
              {filterData.inningNo == 1 &&
              filterData.inningIds[0] == filterData.team1Id
                ? 'Yet to Bat'
                : filterData.inningNo == 2 &&
                  filterData.inningIds[1] == filterData.team2Id
                ? filterData.currentScore + '/' + filterData.currentWickets
                : filterData.secondPredictedScore +
                  '/' +
                  filterData.secondPredictedWicket}
            </div>
            <div className="font-normal text-sm">
              {filterData.inningNo == 1 &&
              filterData.inningIds[0] == filterData.team1Id
                ? ''
                : filterData.inningNo == 2 &&
                  filterData.inningIds[1] == filterData.team2Id
                ? '(' + filterData.currentOvers + ')'
                : '(' + filterData.secondPredictedOVer + ')'}
            </div>
          </div>
        </div>
      )}

      {/* --------------------------------------------------For more then 2 innings including test matches and superover--------------------------------- */}

      {filterData &&
        filterData.inningIds &&
        filterData.inningIds.length > 2 &&
        props.matchType == 'Test' && (
          <div className="mt-2 flex justify-between items-center">
            <div className="flex items-center text-md font-medium">
              <div className="pr-2">
                {filterData.inningNo == '1' || filterData.inningNo == '3'
                  ? filterData.currentScore != ''
                    ? filterData.currentScore + '/' + filterData.currentWickets
                    : ''
                  : filterData.inningNo == '2'
                  ? filterData.predictedScore != '--'
                    ? filterData.predictedScore +
                      '/' +
                      filterData.predictedWicket
                    : ''
                  : filterData.thirdPredictedScore != '--'
                  ? filterData.thirdPredictedScore +
                    '/' +
                    filterData.thirdPredictedWicket
                  : ''}
              </div>
              <div className="font-normal text-sm">
                {filterData.inningNo == '1' || filterData.inningNo == '3'
                  ? filterData.currentOvers != ''
                    ? '(' + filterData.currentOvers + ')'
                    : ''
                  : filterData.inningNo == '2'
                  ? '(' + filterData.predictedScore + ')'
                  : '(' + filterData.thirdPredictedOver + ')'}
              </div>
            </div>
            <div className="text-gray-2 text-sm font-medium flex items-center">
              <div className="pr-2">
                {filterData.inningNo == '1' || filterData.inningNo == '3'
                  ? 'Yet to Bat'
                  : filterData.inningNo == '2' || filterData.inningNo == '4'
                  ? filterData.currentScore
                    ? filterData.currentScore + '/' + filterData.currentWickets
                    : ''
                  : ''}
              </div>
              <div className="font-normal text-sm">
                {filterData.inningNo == '1' || filterData.inningNo == '3'
                  ? ''
                  : filterData.inningNo == '2' || filterData.inningNo == '2'
                  ? '(' + filterData.currentOvers + ')'
                  : ''}
              </div>
            </div>
          </div>
        )}

      {filterData.inningNo <= 2 && (
        <div className="bg-gray-4 p-2 py-3 mt-3 rounded-md">
          <div className="text-gray-2 text-xs font-medium">
            PROJECTED SCORES
          </div>
          {filterData.inningNo == '1' &&
            filterData.predictedScore != '--' &&
            filterData.predictedScore != '' && (
              <div className="mt-3 flex items-center justify-between w-100">
                <img
                  className="w-6 h-4 rounded-sm"
                  src={`https://images.cricket.com/teams/${filterData.team1Id}_flag_safari.png`}
                  onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
                />
                <div className="w-9/12">
                  <div
                    className="bg-green rounded h-2"
                    style={{
                      width: `${
                        (filterData.predictedScore / (maxRun + 20)) * 100
                      }%`,
                    }}
                  ></div>
                </div>
                <div className="text-sm font-medium tracking-wider">
                  {filterData.predictedScore + '/' + filterData.predictedWicket}
                </div>
              </div>
            )}
          {filterData.inningNo <= 2 && filterData.secondPredictedScore != '--' && (
            <div className="mt-3 flex items-center justify-between w-100">
              <img
                className="w-6 h-4 rounded-sm"
                src={`https://images.cricket.com/teams/${filterData.team2Id}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="w-9/12 ">
                <div
                  className="bg-gray-2 rounded h-2"
                  style={{
                    width: `${
                      (filterData.secondPredictedScore / (maxRun + 20)) * 100
                    }%`,
                  }}
                ></div>
              </div>
              <div className="text-sm font-medium tracking-wider">
                {filterData.secondPredictedScore +
                  '/' +
                  filterData.secondPredictedWicket}
              </div>
            </div>
          )}
        </div>
      )}

      {filterData && filterData.inningIds && filterData.inningIds.length > 2 && (
        <div className="bg-gray-4 p-2 py-3 mt-3 rounded-md">
          <div className="text-gray-2 text-xs font-medium">
            PROJECTED SCORES
          </div>
          {filterData.inningNo <= 3 && filterData.thirdPredictedScore != '--' && (
            <div className="mt-3 flex items-center justify-between w-100">
              <img
                className="w-6 h-4 rounded-sm mr-1"
                src={`https://images.cricket.com/teams/${filterData.team1Id}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="w-9/12">
                <div
                  className="bg-green rounded h-2"
                  style={{
                    width: `${
                      (filterData.thirdPredictedScore / (maxRun + 20)) * 100
                    }%`,
                  }}
                ></div>
              </div>
              <div
                className="text-sm font-medium tracking-wider text-right"
                style={{ width: '15%' }}
              >
                {filterData.thirdPredictedScore +
                  '/' +
                  filterData.thirdPredictedWicket}
              </div>
            </div>
          )}
          {filterData.inningNo <= 4 && filterData.fourthPredictedScore != '--' && (
            <div className="mt-3 flex items-center justify-between w-100">
              <img
                className="w-6 h-4 rounded-sm mr-1"
                src={`https://images.cricket.com/teams/${filterData.team2Id}_flag_safari.png`}
                onError={(evt) => (evt.target.src = '/pngsV2/flag_dark.png')}
              />
              <div className="w-9/12 ">
                <div
                  className="bg-gray-2 rounded h-2"
                  style={{
                    width: `${
                      (filterData.fourthPredictedScore / (maxRun + 20)) * 100
                    }%`,
                  }}
                ></div>
              </div>
              <div
                className="text-sm font-medium tracking-wider text-right"
                style={{ width: '15%' }}
              >
                {filterData.fourthPredictedScore +
                  '/' +
                  filterData.fourthPredictedWicket}
              </div>
            </div>
          )}
        </div>
      )}

      <div className="bg-gray-4 p-2 py-3 mt-3 rounded-md">
        <div className="text-gray-2 text-xs font-medium">PROJECTED RESULT</div>
        <div className="text-green mt-1 font-semibold text-md">
          {filterData.projected_result}
        </div>
      </div>
    </div>
  ) : (
    <div></div>
  )
}
