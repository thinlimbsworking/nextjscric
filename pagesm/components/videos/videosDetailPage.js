import React, { useState } from 'react';
import { format, formatDistanceStrict } from 'date-fns';
import Link from 'next/link';
import { getVideoUrl } from '../../api/services';
import MetaDescriptor from '../MetaDescriptor';
import { VIDEOS } from '../../constant/MetaDescriptions';

const VideosDetailPage = ({ data, type, ...props }) => {
  const [drop, setDrop] = useState(false);
  if (typeof window !== 'undefined') {
    window.scrollTo(0, 0);
  }
  return <>
    <MetaDescriptor
      section={VIDEOS({
        title: data.getVideoByvideoID.title,
        type: type
      })}
    />
    {console.log(data,'kkkkkk')};
    {data && data.getVideoByvideoID && (
      <div className='text-white'>
        <div className=' flex justify-between items-center p-2 bg-gray-8 md:hidden lg:hidden'>
          <div className='flex items-center gap-2 md:hidden lg:hidden'>
            <div className='p-2 bg-gray rounded-md flex justify-center items-center cursor-pointer'>
              <img
                className=' w-4 h-4'
                onClick={() => window.history.back()}
                src='/svgs/backIconWhite.svg'
                alt='backIconWhite'></img>
            </div>
            <div className='text-white'>Videos</div>
          </div>
        </div>
        {/* <div className='h44-l' /> */}

        <div className=' w-full'>
          <iframe
            className='w-full h-64 '
            src={`https://www.youtube.com/embed/${data.getVideoByvideoID.videoYTId}`}
            title='cricket'
            modestbranding='1'
            rel='0'
            frameBorder='0'
            fs=''
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen='1'></iframe>
          <div className='flex justify-column justify-between items-center py-2 bg-gray'>
            <div>
              <h1 className=' text-sm font-semibold px-2 py-1'> {data.getVideoByvideoID.title} </h1>

              {data.getVideoByvideoID.createdAt && (
                <div className=' px-2 text-sm font-medium text-gray-2'>
                  {data.getVideoByvideoID.createdAt ? new Date().getTime() - Number(data.getVideoByvideoID.createdAt) > 86400000
                    ? format(
                      new Date(Number(data.getVideoByvideoID.createdAt)),
                      'dd MMM, yyyy',
                    )
                    : formatDistanceStrict(
                      new Date(Number(data.getVideoByvideoID.createdAt)),
                      new Date(),
                    ) + ' ago'
                    : ''}
                </div>
              )}
            </div>
            <div className='w-8 h-8 flex justify-center items-center pointer'>
              <img
                className='w-2 pointer '
                src={drop ? '/svgs/up-arrow.png' : '/svgs/down-arrow.png'}
                onClick={() => setDrop(!drop)}
                alt=''
              />
            </div>
          </div>
          {drop && <h2 className='p-2 text-xs text-gray-2 bg-gray'>{data.getVideoByvideoID.description}</h2>}
          {data.getVideoByvideoID.relatedVideos && (
            <div className='flex flex-col gap-3 bg-gray-8 p-3'>
              <h3 className='p-2 text-md font-medium'>Watch Related Videos</h3>
              {data.getVideoByvideoID.relatedVideos.map((video, i) => (
                (<Link
                  key={i}
                  {...getVideoUrl(video, type)}
                  passHref
                  className='flex gap-4 w-full '>

                  <div className='w-1/4 min-w-[35%]  md:min-w-[20%] lg:min-w-[20%] h-full z-0 contain relative'>
                    <img
                      src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                      alt=''
                      className='lazyload w-full h-full mx-auto'
                    />
                    <img className='absolute w-11 h-11 right-0 bottom-0' src='/pngsV2/videos_icon.png' alt='' />
                  </div>
                  <div className='w-3/4 min-w-[60%] md:min-w-[75%] lg:min-w-[75%] h-full'>
                    <div className='text-sm  font-semibold truncate'>{video.title}</div>

                    <div className='text-xs  pt-1 truncate '>{video.description}</div>
                    {video.createdAt && (
                      <div className='text-xs text-gray-2 pt-2 '>
                        {video.createdAt
                          ? new Date().getTime() - Number(video.createdAt) > 86400000
                            ? format(new Date(Number(video.createdAt)), 'dd MMM, yyyy')
                            : formatDistanceStrict(new Date(Number(video.createdAt)), new Date()) + ' ago'
                          : ''}
                      </div>
                    )}
                  </div>

                </Link>)
              ))}
            </div>
          )}
        </div>
      </div>
    )}
  </>;
};

export default VideosDetailPage;
{/* <a className='flex m-2 my-3'>
                      <div className='w44 h35 flex justify-center items-center overflow-hidden z-0 contain ba b--black pointer'>
                        <img
                          src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                          alt=''
                          className='lazyload'
                        />
                        <img className='absolute' src='/svgs/videoIcon.svg' alt='' />
                      </div>
                      <div className='w-50 w-80-lmx-autopv1'>
                        <h4 className='fw5 f7'>{video.title}</h4>
                        {video.createdAt && (
                          <div className='f8 fw4 pt3'> {video.createdAt? new Date().getTime() - Number(video.createdAt) >86400000
                    ? format(
                        new Date(Number(video.createdAt)),
                        'dd MMM, yyyy',
                      )
                    : formatDistanceStrict(
                        new Date(Number(video.createdAt)),
                        new Date(),
                      ) + ' ago'
                  : ''}</div>
                        )}
                      </div>
                    </a> */}