import React from 'react';
import { format, formatDistanceStrict } from 'date-fns';
import Link from 'next/link';
import { getVideoUrl } from '../../api/services';

const VideosHomePage = ({
  error,
  data,
  getPrimaryVideo,
  drop,
  setDrop,
  tabs,
  tab,
  handleVideoNavigation,
  ...props
}) => {
  const { getVideos } = data;
  return (
    <div className=' text-white mx-auto'>
      <div className='flex items-center p-3 md:hidden lg:hidden'>
        <div className='w-8 h-8 rounded-md flex justify-center items-center cursor-pointer bg-gray-4 '>
          <img
            className='cursor-pointer'
            onClick={() => window.history.back()}
            src='/svgs/backIconWhite.svg'
            alt='backIconWhite'></img>
        </div>
        <div className='text-md font-semibold pl-2 white'>Videos</div>
      </div>

      {/* <div className='hidden  md:h-40 lg:h-40 md:block lg:block' /> */}
      <div className=' mx-auto w-full'>
        {getPrimaryVideo && (
          <iframe
            className='w-full h-80  hidden md:block lg:block'
            src={`https://www.youtube.com/embed/${getPrimaryVideo.videoYTId}`}
            title='cricket'
            modestbranding='1'
            rel='0'
            frameBorder='0'
            fs=''
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen='1'></iframe>
        )}

        {getPrimaryVideo && (
          <div className=' justify-between items-center  hidden md:flex lg:flex p-3 bg-gray-4 '>
            <div>
              <h1 className='text-xs font-semibold '> {getPrimaryVideo.title} </h1>
              {getPrimaryVideo.createdAt && (
                <div className='ph-2 text-xs font-medium text-gray-2'>
                  {getPrimaryVideo.createdAt
                    ? new Date().getTime() - Number(getPrimaryVideo.createdAt) > 86400000
                      ? format(new Date(Number(getPrimaryVideo.createdAt)), 'dd MMM, yyyy')
                      : formatDistanceStrict(new Date(Number(getPrimaryVideo.createdAt)), new Date()) + ' ago'
                    : ''}
                </div>
              )}
            </div>
            <div className='w-8 h-6 flex justify-center items-center cursor-pointer'>
              <img
                className='w-3 h-2 cursor-pointer '
                src={drop ? '/svgs/up-arrow.png' : '/svgs/down-arrow.png'}
                onClick={() => setDrop(!drop)}
                alt=''
              />
            </div>
          </div>
        )}
        {drop && <h3 className='p-3 font-medium tracking-wide text-xs bg-gray-4 '>{getPrimaryVideo.description}</h3>}

        <div className='md:py-2 lg:py-2 md:mt-2 lg:mt-2'>
          <div className='flex z-10  justify-between bg-gray-8 ' style={{ position: 'sticky', top: 0 }}>
            {tabs.map((tabName, i) => (
              (<Link
                href='/videos/[...slugs]'
                as={`/videos/${tabName.toLowerCase()}`}
                key={i}
                replace
                passHref
                className={`w-1/4 cursor-pointer  text-xs text-gray-2 px-3 py-2 capitalize  text-center ${tabName.toLowerCase() === tab.toLowerCase() ? ' border-b-2 border-blue-9 text-blue-8' : ''
                  }`}>

                {tabName.toLowerCase()}

              </Link>)
            ))}
          </div>

          {/* video list */}
          {getVideos &&
            getVideos &&
            getVideos[tab].map((video, i) => (
              <Link key={i} {...getVideoUrl(video, tab)} passHref legacyBehavior>
                <div className=' h-64 m-3 mt-3 rounded-md bg-gray-4  md:bg-basebg lg:bg-basebg md:rounded-none lg:rounded-none md:flex lg:flex md:h-32 md:mx-0 lg:h-32 lg:mx-0 2 md:my-3 lg:my-3 md:w-full lg:w-full'>
                  <a className='h-full  md:flex lg:flex  md:w-full lg:w-full' onClick={() => handleVideoNavigation(tab, video.videoID)}>
                    <div className='z-0 h-1/2 contain relative p-2 md:p-0 lg:p-0 md:h-full lg:h-full md:w-1/4 lg:w-1/4'>
                      <img
                        src={`https://img.youtube.com/vi/${video.videoYTId}/mqdefault.jpg`}
                        alt=''
                        className='lazyload w-full h-full mx-auto rounded-md md:rounded-none lg:rounded-none '
                      />
                      <img className='absolute w-11 h-11  top-1/2  left-1/2  transform -translate-x-1/2 -translate-y-1/2  ' src='/pngsV2/videos_icon.png' alt='' />
                    </div>
                    <div className=' p-2 h-1/2 md:p-0 lg:p-0  md:h-full lg:h-full md:w-3/4 lg:w-3/4'>
                      <div className='mx-3  text-sm font-semibold'>{video.title}</div>

                      <div className='mx-3 text-xs  pt-2 truncate'>{video.description}</div>
                      {video.createdAt && (
                        <div className='mx-3 text-xs text-gray-2 pt-2 '>
                          {video.createdAt
                            ? new Date().getTime() - Number(video.createdAt) > 86400000
                              ? format(new Date(Number(video.createdAt)), 'dd MMM, yyyy')
                              : formatDistanceStrict(new Date(Number(video.createdAt)), new Date()) + ' ago'
                            : ''}
                        </div>
                      )}
                    </div>
                  </a>
                </div>
              </Link>
            ))}
        </div>
      </div>
    </div>
  );
};

export default VideosHomePage;

// This gets called on every request
// export async function getServerSideProps(context) {
// 	try {
// 		const getVideoPromise = axios.post(process.env.api, { query: GET_VIDEOS_AXIOS });
// 		const primaryVideoPromise = axios.post(process.env.api, { query: GET_PRIMARY_VIDEOS_AXIOS });

// 		let [{ data }, { data: primaryVideo }] = await Promise.all([
// 			getVideoPromise,
// 			primaryVideoPromise,
// 		]);
// 		return { props: { data, primaryVideo } };
// 	} catch (e) {
// 		return { props: { error: e } };
// 	}
// }
