"use client"
import React, { useState, useEffect } from 'react';
import * as d3 from 'd3';
import axios from 'axios';
import { usePathname } from 'next/navigation';
import StatHubLayout from '../../components/shared/statHubLayout';
import MetaDescriptor from '../../components/MetaDescriptor';
import { STADIUM } from '../../constant/MetaDescriptions';
import { STADIUM_DETAILS } from '../../api/queries';
import Loading from '../../components/Loading';
import Urltracking from '../../components/commom/urltracking';
import DataNotFound from '../../components/commom/datanotfound'
import Tab from '../../components/shared/Tab';
const tabsConfig = {
    TEST: 'testDetails',
    T20: 't20Details',
    ODI: 'odiDetails'
};

/**
 * @description Individual stadium view
 * @route /stadiums/stadiumsID/stadiumsName
 * @param { Object } data
 */

async function getData(context) {
    let [id, name] = context.params.slugs;
    try {
        const { data } = await axios.post(process.env.API, {
            query: STADIUM_DETAILS,
            variables: { venueID: id }
        });

        return {
            data: data.data
        };
    } catch (e) {
        return {
            error: e
        };
    }
}
const StadiumDetails = (context) => {
    const asPath = usePathname();
    const tabs = ['INTERNATIONAL', 'IPL'];
    const formatTabs = [{name:'TEST',value:'TEST'},{name:'ODI',value:'ODI'},{name:'T20',value:'T20'}];
    const [data, setdata] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState(true);
    const [activeTab, setActiveTab] = useState('INTERNATIONAL');
    const [activeFormatTab, setFormatTab] = useState(formatTabs[0]);
    const [screenWidth, setscreenWidth] = useState();

    useEffect(() => {
        if (typeof window !== 'undefined' && window && window.screen && window.screen.width) {
            setscreenWidth(window.screen.width > 975 ? 976 : window.screen.width);
        }
        getData(context).then((res) => {
            setLoading(true)
            const { data, error } = res;
            setdata(data);
            error && setError(error);
            setLoading(false)
        })
    }, []);
    useEffect(() => {
        let chart = radialProgress('.widget');
        let winningPercent =
            (
                (data?.stadium.venueStats.odiDetails !== null ||
                    data?.stadium.venueStats.t20Details !== null ||
                    data?.stadium.venueStats.testDetails !== null) &&
                data?.stadium.venueStats[tabsConfig[activeFormatTab]] &&
                data?.stadium.venueStats[tabsConfig[activeFormatTab]].firstBattingWin.toFixed(1)) ||
            0; // pass value for Team Batting First Winning %
        chart.update(winningPercent);
    }, [formatTabs]);

    function radialProgress(selector) {
        d3.select(`${selector} > *`).remove();
        const parent = d3.select(selector);
        const size = { width: 90, height: 55 };
        const svg = parent && parent.append('svg').attr('width', size.width).attr('height', size.height);
        const outerRadius = Math.min(size.width, size.height) * 0.48;
        const thickness = 4;
        let value = 0;

        const mainArc = d3
            .arc()
            .startAngle(0)
            .endAngle(Math.PI * 2)
            .innerRadius(outerRadius - thickness)
            .outerRadius(outerRadius);

        svg
            .append('path')
            .attr('class', 'progress-bar-bg')
            .attr('transform', `translate(${size.width / 2},${size.height / 2})`)
            .attr('d', mainArc());

        const mainArcPath = svg
            .append('path')
            .attr('class', 'progress-bar')
            .attr('transform', `translate(${size.width / 2},${size.height / 2})`);

        let percentLabel = svg
            .append('text')
            .attr('class', 'progress-label')
            .attr('transform', `translate(${size.width / 2},${size.height / 2})`)
            .text('0');

        return {
            update: function (progressPercent) {
                const startValue = value;
                const startAngle = (Math.PI * startValue) / 50;
                const angleDiff = (Math.PI * progressPercent) / 50 - startAngle;
                const transitionDuration = 1000;

                mainArcPath
                    .transition()
                    .duration(transitionDuration)
                    .attrTween('d', function () {
                        return function (t) {
                            mainArc.endAngle(startAngle + angleDiff * t);
                            return mainArc();
                        };
                    });

                percentLabel.text(progressPercent);
            }
        };
    }

    // ======== function to create Circle closing ========
    if (loading) return <Loading />
    if (error)
        return (
            <div className='w-full flex  justify-center items-center'>
                <DataNotFound />
            </div>
        );
    return (
        <StatHubLayout currIndex={2}>
            {/* {props.urlTrack ? <Urltracking PageName='Stadium' /> : null} */}

            {<MetaDescriptor
                section={STADIUM({
                    title: data.stadium.fullName,
                    description: data.stadium.fullName,
                    url: asPath
                })}>
                <meta itemprop='width' content='595' />
                <meta itemprop='height' content='450' />
                <meta
                    itemprop='url'
                    content={`https://images.cricket.com/stadiums/${data.stadium.venueID}_actionshot_safari.jpg`}
                />
                <meta
                    property='og:image'
                    content={`https://images.cricket.com/stadiums/${data.stadium.venueID}_actionshot_safari.jpg`}
                />
                <meta property='og:width' content='595' />
                <meta property='og:height' content='450' />
                <meta property='og:image:width' content='595' />
                <meta property='og:image:height' content='450' />
                <meta
                    property='og:image:secure_url'
                    content={`https://images.cricket.com/stadiums/${data.stadium.venueID}_actionshot_safari.jpg`}
                />
            </MetaDescriptor>}

            <div className='  dark:text-white '>
                {/* Header and Red Background Section Starts */}
                {/* <div className='bg-gradientRedOrangeLeft h13 dn-l' /> */}
                <div className='p-2 items-center flex gap-2 text-base z-2 md:hidden lg:hidden'>
                    <div className='p-2 rounded-md bg-gray'>
                        <img
                            className=' w-4 h-4'
                            onClick={() => window.history.back()}
                            src='/svgs/backIconWhite.svg'
                            alt='back icon'
                        />
                    </div>
                    <span className='text-base font-bold'>{data.stadium.fullName}</span>
                </div>
                {/* Header and Red Background Section Closing */}

                {/* Venue Image Section Starts */}

                <div className='p-2'>
                    <div className=' p-2  dark:bg-gray rounded-md'>
                        <div className=' relative'>
                            <img
                                className='h-80 w-full object-cover object-position dark:rounded-md'
                                src={
                                    data.stadium.image
                                        ? `${data.stadium.image}?auto=compress&dpr=2&h=192&w=${screenWidth}`
                                        : `https://images.cricket.com/stadiums/${data.stadium.venueID}_actionshot_safari.jpg`
                                }
                                alt='Stadium'
                                onError={(evt) => (evt.target.src = '/pngs/stadium-placeholder.jpg')}
                            />
                            <img
                                className=' hidden md:hidden lg:hidden br1 h45 vh-50-l w-full object-cover object-position '
                                src={
                                    data.stadium.image
                                        ? `${data.stadium.image}?auto=compress&dpr=2&h=400&w=${screenWidth - 100}`
                                        : `https://images.cricket.com/stadiums/${data.stadium.venueID}_actionshot_safari.jpg`
                                }
                                alt='Stadium'
                                onError={(evt) => (evt.target.src = '/pngs/stadium-placeholder.jpg')}
                            />

                            <h1 className='absolute bottom-1 z-50 left-1  text-xl hidden md:block lg:block '>{data.stadium.fullName}</h1>
                        </div>
                    </div>
                        {/* Venue Image Section Closing */}
                    <div className='flex flex-col md:flex-row lg:flex-row'>
                        <div className='flex w-full md:w-1/2 lg:w-1/2 pt-3 md:px-2 lg:px-2'>
                            {/* Stadium Details Section Starts */}
                            <div className='text-sm rounded-lg border dark:border-none flex flex-col gap-2  cardShadow dark:bg-gray w-full h-min'>
                                <h2 className=' p-2 border-b-2'>Stadium Details</h2>
                                

                                <div className='flex  items-center p-2 border-b'>
                                    <div className='w-1/2 text-gray-2'>City</div>
                                    <div className='w-1/2  '>{data.stadium.city || '-'}</div>
                                </div>
                                

                                <div className='flex  items-center p-2 border-b'>
                                    <div className='w-1/2 text-gray-2'>Capacity</div>
                                    <div className='w-1/2'> {data.stadium.capacity || '-'} </div>
                                </div>
                               

                                {/* <div className='divider' /> */}

                                <div className='flex  items-center p-2'>
                                    <div className='w-1/2 text-gray-2'>Bowling Ends</div>
                                    <div className='w-1/2'> {data.stadium.bowlingEnds || '-'} </div>
                                </div>

                            </div>
                        </div>
                       
                    <div className='flex w-full md:w-1/2 lg:w-1/2 pt-3 md:px-2 lg:px-2'>
                        <div className=' dark:bg-none border dark:border-none rounded-md w-full'>

                        <div className=' w-full relative flex items-center justify-center p-2'>
                            {/* <div className='w-full'> */}
                                <Tab data={formatTabs} type='block' selectedTab={activeFormatTab} handleTabChange={(item) => setFormatTab(item)}/>
                                {/* {formatTabs.map((tabName, i) => (
                                    <span
                                        key={i}
                                        className={` w-1/3 text-xs font-bold cursor-pointer p-2 rounded-full  text-center  ${i === 2 ? '' : ''} ${activeFormatTab === tabName ? 'bg-gray-8 border-green border-2' : ''
                                            }`}
                                        onClick={() => setFormatTab(tabName)}>
                                        {tabName}
                                    </span>
                                ))} */}
                            {/* </div> */}
                        </div>
                        


                        <div className='w-full pt-3 '>


                            {data &&
                                data.stadium &&
                                (data.stadium.venueStats.odiDetails !== null ||
                                    data.stadium.venueStats.t20Details !== null ||
                                    data.stadium.venueStats.testDetails !== null) && (
                                    <div className='flex flex-col gap-3'>
                                        <div className='p-2 dark:bg-gray text-base flex flex-col gap-3 rounded-md'>

                                            <div className=' text-gray-2 text-sm'>1st {activeFormatTab.value}</div>
                                            <div className='flex w-full justify-between'>
                                                <div className='w-1/2'>
                                                    {data &&
                                                        data.stadium &&
                                                        (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                            ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].firstMatch.startDate || '-'
                                                            : null)}
                                                </div>
                                                <div className='w-1/2 flex  flex-col'>
                                                    <div className='w-50'>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].firstMatch.matchName || '-'
                                                                : null)}
                                                    </div>
                                                    <div className='w-50 '>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].firstMatch.matchStatusStr || '-'
                                                                : null)}
                                                    </div>
                                                </div>
                                            </div>

                                            {/* <div className='flex items-center f7 fw5 py-1'>
                          
                        </div> */}
                                        </div>
                                        <div className='bg-black/80 h-[1px] w-full hidden md:block lg:block' />

                                        {/* 2 */}

                                        <div className='p-2 dark:bg-gray text-base flex flex-col gap-3 rounded-md '>

                                            <div className=' text-gray-2 text-sm'>Recent {activeFormatTab.value}</div>
                                            <div className='flex w-full justify-between'>
                                                <div className='w-1/2'>
                                                    {data &&
                                                        data.stadium &&
                                                        (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                            ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].lastMatch.startDate || '-'
                                                            : null)}
                                                </div>
                                                <div className='w-1/2 flex  flex-col'>
                                                    <div className='w-50'>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].lastMatch.matchName || '-'
                                                                : null)}
                                                    </div>
                                                    <div className='w-50 '>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].lastMatch.matchStatusStr || '-'
                                                                : null)}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div className='bg-black/80 h-[1px] w-full hidden md:block lg:block' />

                                        {/* 3 */}
                                        <div className='flex dark:bg-gray p-2 rounded-md items-centers'>
                                            <div className='w-1/2 flex flex-col justify-center gap-2 border-r-2 border-gray-2'>
                                                <div className='text-center '>Average 1st Innings Score</div>
                                                <div className=' text-center text-green-3 font-bold'>
                                                    {data &&
                                                        data.stadium &&
                                                        (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                            ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].avgIstInningScore || '0'
                                                            : null)}
                                                </div>
                                            </div>
                                            <div className='w-1/2  flex flex-col justify-center gap-2'>
                                                <div className='text-center'>Team Batting First Winning %</div>
                                                <div className='text-center text-green-3 font-bold'>
                                                    {data &&
                                                        data.stadium &&
                                                        (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                            ? (data.stadium.venueStats[tabsConfig[activeFormatTab.value]].firstBattingWin?.toFixed(2) || '0') + ' %'
                                                            : null)}
                                                </div>
                                            </div>
                                        </div>
                                        <div className='bg-black/80 h-[1px] w-full hidden md:block lg:block' />

                                        {/* 4 */}



                                        <div className='p-2 dark:bg-gray text-base flex flex-col gap-3 rounded-md'>

                                            <div className='text-gray-2 text-sm'>Highest Team Score </div>
                                            <div className='flex w-full justify-between'>
                                                <div className='w-1/2'>
                                                    {data &&
                                                        data.stadium &&
                                                        (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                            ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].highestScoreMatch.startDate || '-'
                                                            : null)}
                                                </div>
                                                <div className='w-1/2 flex  flex-col'>
                                                    <div className='w-50'>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].highestScoreMatch.matchName || '-'
                                                                : null)}
                                                    </div>
                                                    <div className='w-50 '>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].highestScoreMatch.matchStatusStr || '-'
                                                                : null)}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div className='p-2 dark:bg-gray text-base flex flex-col gap-3 rounded-md'>

                                            <div className=' text-gray-2 '>Lowest Team Score </div>
                                            <div className='flex w-full justify-between'>
                                                <div className='w-1/2'>
                                                    {data &&
                                                        data.stadium &&
                                                        (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                            ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].lowestScoreMatch.startDate || '-'
                                                            : null)}
                                                </div>
                                                <div className='w-1/2 flex  flex-col'>
                                                    <div className='w-50'>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].lowestScoreMatch.matchName || '-'
                                                                : null)}
                                                    </div>
                                                    <div className='w-50 '>
                                                        {data &&
                                                            data.stadium &&
                                                            (data.stadium.venueStats[tabsConfig[activeFormatTab.value]]
                                                                ? data.stadium.venueStats[tabsConfig[activeFormatTab.value]].lowestScoreMatch.matchStatusStr || '-'
                                                                : null)}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>




                                        {/* <div className='px-3 py-1'>
                        <div className='flex items-center f7 fw5 py-1'>
                          <div className='w-50 '>Highest Team Score</div>
                          <div className='w-50 '>
                            {data &&
                              data.stadium &&
                              (data.data.stadium.venueStats[tabsConfig[activeFormatTab]]
                                ? data.data.stadium.venueStats[tabsConfig[activeFormatTab]].highestScoreMatch.startDate ||
                                '-'
                                : null)}
                          </div>
                        </div>

                        <div className='flex items-center f7 fw5 py-1'>
                          <div className='w-50'>
                            {data &&
                              data.stadium &&
                              (data.data.stadium.venueStats[tabsConfig[activeFormatTab]]
                                ? data.data.stadium.venueStats[tabsConfig[activeFormatTab]].highestScoreMatch.matchName ||
                                '-'
                                : null)}
                          </div>
                          <div className='w-50 '>
                            {data &&
                              data.stadium &&
                              (data.data.stadium.venueStats[tabsConfig[activeFormatTab]]
                                ? data.data.stadium.venueStats[tabsConfig[activeFormatTab]].highestScoreMatch
                                  .matchStatusStr || '-'
                                : null)}
                          </div>
                        </div>
                      </div> */}
                                        {/* <div className='bg-black/80 h-[1px] w-full hidden md:block lg:block' /> */}

                                        {/* 5 */}
                                        {/* <div className='px-3 py-1'>
                        <div className='flex items-center f7 fw5 py-1'>
                          <div className='w-50 '>Lowest Team Score</div>
                          <div className='w-50 '>
                            {data &&
                              data.stadium &&
                              (data.data.stadium.venueStats[tabsConfig[activeFormatTab]]
                                ? data.data.stadium.venueStats[tabsConfig[activeFormatTab]].lowestScoreMatch.startDate || '-'
                                : null)}
                          </div>
                        </div>

                        <div className='flex items-center f7 fw5 py-1'>
                          <div className='w-50'>
                            {data &&
                              data.stadium &&
                              (data.data.stadium.venueStats[tabsConfig[activeFormatTab]]
                                ? data.data.stadium.venueStats[tabsConfig[activeFormatTab]].lowestScoreMatch.matchName || '-'
                                : null)}
                          </div>
                          <div className='w-50 '>
                            {data &&
                              data.stadium &&
                              (data.data.stadium.venueStats[tabsConfig[activeFormatTab]]
                                ? data.data.stadium.venueStats[tabsConfig[activeFormatTab]].lowestScoreMatch
                                  .matchStatusStr || '-'
                                : null)}
                          </div>
                        </div>
                      </div> */}
                                    </div>
                                )}
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </StatHubLayout>
    );
};





// export async function getServerSideProps({ params: { slugs } }) {
// 	let [id, name] = slugs;

// 	try {
// 		const { data } = await axios.post(process.env.API, {
// 			query: STADIUM_DETAILS,
// 			variables: { venueID: id },
// 		});

// 		return {
// 			props: {
// 				data: data.data,
// 			},
// 		};
// 	} catch (e) {
// 		return {
// 			props: {
// 				error: e,
// 			},
// 		};
// 	}
// }
export default StadiumDetails;