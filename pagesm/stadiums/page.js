
"use client"
import React, { useEffect, useState } from 'react';
import CleverTap from 'clevertap-react';
//import Swiper from 'react-id-swiper';
import axios from 'axios';
import StatHubLayout from '../components/shared/statHubLayout';
import Link from 'next/link';
import { getStadiumUrl } from '../../api/services';
import { STADIUM_LIST_AXIOS } from '../../api/queries';
import { STADIUM } from '../../constant/MetaDescriptions';
import MetaDescriptor from '../../components/MetaDescriptor';
import Loading from '../../components/Loading';
import Urltracking from '../../components/commom/urltracking';
import DataNotFound from '../../components/commom/datanotfound'
import SliderCommon from '../../components/commom/slider';
import Heading from '../components/commom/heading';
const IMAGES = {
  backIconWhite: '/svgs/backIconWhite.svg',
  fallBackPlaceHolder: '/pngs/stadium-placeholder.jpg'
};

/**
 * @description stadium listing
 * @route /stadiums
 * @param { Object } error
 * @param { Object } props
 */

async function getData() {
  try {
    const { data } = await axios.post(process.env.API, { query: STADIUM_LIST_AXIOS });
    return {data: data.data} ;
  } catch (e) {
    return { error: e };
  }
}


export default  function Stadiums(context) {
  // const { loading, data } = { data: props.data.data, loading: false };
  const [data, setdata] = useState();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    getData().then((res) => {
      const {data, error} = res;
      setdata(data);
      error && setError(error);
      setLoading(false)
    })
  },[])

  const handleVenueNavigation = (venueID) => {
    CleverTap.initialize('Stadiums', {
      Source: 'StadiumsDiscovery',
      VenueID: venueID,
      Platform: global.window.localStorage ? global.window.localStorage.Platform : ''
    });
  };
  const params = {
    slidesPerView: 2.5,
    navigation: {
      nextEl: `#nbutton`,
      prevEl: `#pbutton`
    },
    breakpoints: { 640: { slidesPerView: 5, spaceBetween: 30 } }
  };

  if (error)
    return (
      <div className='w-full vh-100 fw2 f7 gray flex flex-col  justify-center items-center'>
        <span>Something isn't right!</span>
        <span>Please refresh and try again...</span>
      </div>
    );
  else
    return (
      <StatHubLayout currIndex={2}>
        {/* {props?.urlTrack ? <Urltracking PageName='Stadium' /> : null} */}

        <MetaDescriptor section={STADIUM()} />
        <div className=' hidescroll  text-white'>
          {/* <div className='bg-gradientRedOrangeLeft h43 dn-l dn-m' /> */}
          <div className='flex items-center p-2 gap-2 white text-base font-bold md:hidden lg:hidden'>
            <div className='p-2 rounded-md bg-gray'>
            <img
              className='w-4 h-4 cursor-pointer'
              onClick={() => window.history.back()}
              src={IMAGES.backIconWhite}
              alt='back icon'
            />
            </div>
            <h1 className=' '> Stadiums </h1>
          </div>

         

          {error ? (
            <div className='w-full flex  justify-center items-center'>
             <DataNotFound />
            </div>
          ) : loading ? (
            <div className='w-full  flex justify-center items-center'>LOADING...</div>
          ) : (
            <div className=' hidescroll p-2'>
              <div className=' relative  shadow-4 dark:bg-gray  rounded-md'>
                {/* <h2 className='text-base  font-bold px-3 pt-3 '>Popular Stadiums</h2> */}
                <div className='p-2'>
                <Heading heading='Popular Stadiums'/>
                </div>
                {/* <div className='bg-black/80 h-[1px] w-full' /> */}
                <div className='p-2 w-full'>

                <SliderCommon data={data?.getStadiumsDiscovery?.popularStadiums || []} handleVenueNavigation={handleVenueNavigation} event='stadiums'/>

                  {/* <Swiper {...params}>
                    {data.getStadiumsDiscovery.popularStadiums.map((stadium, i) => (
                      <div key={i} className='pl2 ' onClick={() => handleVenueNavigation(stadium.id)}>
                        <div className='bg-light-gray relative overflow-hidden h4-ns h35 br2'>
                          <Link {...getStadiumUrl(stadium)} passHref>
                            <a>
                              <img
                                className='object-cover h-100'
                                onLoadStart={() => <Loading />}
                                src={stadium.image ? `${stadium.image}?auto=compress&dpr=2` :`https://images.cricket.com/stadiums/${stadium.id}_actionshot_safari.jpg`}
                                onError={(e) => {
                                  e.target.src = IMAGES.fallBackPlaceHolder;
                                }}
                                alt='stadium'
                              />
                            </a>
                          </Link>
                        </div>
                        <div className='pv2'>
                          <div className=' text-xs font-medium'>{stadium.name}</div>
                        </div>
                      </div>
                    ))}
                  </Swiper> */}
                 
                </div>

                {/* <div className='absolute absolute--fill flex justify-between items-center md:hidden lg:hidden'>
                    <div id={`pbutton`} className='white cursor-pointer flex justify-center items-center'>
                      <svg width='44' focusable='false' viewBox='0 0 24 24'>
                        <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                        <path fill='none' d='M0 0h24v24H0z'></path>
                      </svg>
                    </div>
                    <div id={`nbutton`} className='white cursor-pointer flex justify-center items-center'>
                      <svg width='44' viewBox='0 0 24 24'>
                        <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                        <path fill='none' d='M0 0h24v24H0z'></path>
                      </svg>
                    </div>
                  </div> */}


              </div>

              {data?.getStadiumsDiscovery.stadiumByCategory.map((list, i) => {
                // const params = {
                //   slidesPerView: 2.5,
                //   navigation: {
                //     nextEl: `#nbutton0${i}`,
                //     prevEl: `#pbutton0${i}`
                //   },
                //   breakpoints: { 640: { slidesPerView: 5, spaceBetween: 30 } }
                // };
                return (
                  <div key={i} className='stadium-category shadow-4'>
                    <div className='mt-4 rounded-md relative  dark:bg-gray'>
                      {/* <h2 className='text-base font-bold px-3 pt-3'>{list.type}</h2> */}
                      <div className='p-2'>
                        <Heading heading={list.type}/>
                      </div>
                      {/* <div className='bg-black/80 h-[1px] w-full' /> */}
                      <div className='p-2 '>
                        <SliderCommon data={list?.stadiumList || []} handleVenueNavigation={handleVenueNavigation} event='stadiums'/>
                        {/* <Swiper {...params}>
                          {list.stadiumList.map((stadium, i) => (
                            <div key={i} className='pl2' onClick={() => handleVenueNavigation(stadium.id)}>
                              <div className='bg-light-gray relative overflow-hidden h4-ns h37 br2'>
                                <Link {...getStadiumUrl(stadium)} passHref>
                                  <a>
                                    <img
                                      className='object-cover h-100'
                                      src={stadium.image ? `${stadium.image}?auto=compress&dpr=2&w=90&h=90` : `https://images.cricket.com/stadiums/${stadium.id}_actionshot_safari.jpg`}
                                      onError={(e) => {
                                        e.target.src = IMAGES.fallBackPlaceHolder;
                                      }}
                                      alt='stadium'
                                    />
                                  </a>
                                </Link>
                                <div
                                  className='white absolute  bottom-0 w-full ph2 br2 pb2 pt3'
                                  style={{
                                    backgroundImage: 'linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.5))'
                                  }}>
                                  <div className='text-[11px] font-medium'>{stadium.name}</div>
                                </div>
                              </div>
                            </div>
                          ))}
                        </Swiper> */}
                        
                      </div>


                      {/* <div className='absolute absolute--fill flex justify-between items-center md:hidden lg:hidden'>
                          <div id={`pbutton0${i}`} className='white cursor-pointer flex justify-center items-center'>
                            <svg width='44' focusable='false' viewBox='0 0 24 24'>
                              <path fill='#38d925' d='M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z'></path>
                              <path fill='none' d='M0 0h24v24H0z'></path>
                            </svg>
                          </div>
                          <div id={`nbutton0${i}`} className='white cursor-pointer flex justify-center items-center'>
                            <svg width='44' viewBox='0 0 24 24'>
                              <path fill='#38d925' d='M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z'></path>
                              <path fill='none' d='M0 0h24v24H0z'></path>
                            </svg>
                          </div>
                      </div> */}


                    </div>
                  </div>
                );
              })}
            </div>
          )}
        </div>
      </StatHubLayout>
    );
}


