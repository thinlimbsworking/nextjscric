'use client'
import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import axios from 'axios'
const searchIcon = '/svgs/teamSearch.svg'
const searchIconLarge = '/svgs/search.svg'
import TeamList from '../../components/TeamDiscovery'
import Search from '../../components/Search'
import { TeamDiscovery, TEAM_SEARCH } from '../../api/queries'
import { TEAM_TAB_VIEW, TEAM_VIEW } from '../../constant/Links'
import { TEAMS } from '../../constant/MetaDescriptions'
import MetaDescriptor from '../../components/MetaDescriptor'
import Urltracking from '../../components/commom/urltracking'

async function getServerData(tab) {
  const statusTabs = {
    international: 'international',
    'other-league': 'otherLeagues',
  }
  //   let tab = (context.params.slug
  //     ? context.params.slug
  //     : 'international'
  //   ).toLowerCase()

  let response = { data: {}, tab }
  let { data } = await axios.post(process.env.API, {
    query: TeamDiscovery,
  })
  console.log(data, 'dataaa-->>>>')

  data = data.data.teamDiscoveryV2[statusTabs[tab]]

  if (Array.isArray(data)) {
    response.data[tab] = data
  } else {
    response.data = data
  }

  return { data: response, status: tab, tab }
}

export default function Teams({ params }) {
  console.log(params, '-->>params')
  const [props, setProps] = useState()
  // const { data, tab: status } = props
  const tab = params?.slug?.[0]
  useEffect(() => {
    getServerData(tab).then((res) => console.log(res, 'uiui'), setProps(res))
  }, [])

  const statusTabs = [
    { tabName: 'international', type: 'international' },
    { tabName: 'Other Leagues', type: 'other-league' },
  ]

  const onSearchChange = async (value) => {
    console.log('values: ', value, process, TEAM_SEARCH)
    try {
      if (value.length >= 3) {
        const data = await axios.post(process.env.APIS, {
          query: TEAM_SEARCH,
          variables: { name: value },
        })
        console.log('data value: ', data.data)
        return data &&
          data.data &&
          data.data.data.teamSearch &&
          data.data.data.teamSearch.length > 0
          ? [...data.data.data.teamSearch]
          : []
      } else return null
    } catch (e) {
      console.log('errors: ', e)
    }
  }

  const getTeamUrl = ({ name, teamID }) => {
    let teamName = name.split(' ').join('-').toLowerCase()
    let teamId = teamID
    return {
      as: eval(TEAM_VIEW.as),
      href: TEAM_VIEW.href,
    }
  }

  const getSearchResult = (team, index) => {
    return (
      <Link
        key={index}
        {...getTeamUrl({ name: team.name, teamID: team.teamID })}
        legacyBehavior
      >
        <div className="">
          <li className="text-left text-white pt-0.5 pb-4 px-3 font-normal text-xs border-b-2 border-b-solid border-b-gray bg-gray-8 lg:text-sm md:text-sm cursor-pointer">
            {team.name}
          </li>

          <div className="divider"></div>
        </div>
      </Link>
    )
  }

  const getTabUrl = (tabName) => {
    return {
      as: eval(TEAM_TAB_VIEW.as),
      href: TEAM_TAB_VIEW.href,
    }
  }

  const styleDesktop = {
    backgroundImage: `url(${searchIcon})`,
    backgroundRepeat: 'no-repeat',
    textIndent: '25px',
    backgroundPosition: '5px',
  }

  const styleMobo = {
    backgroundImage: `url(${searchIcon})`,
    backgroundRepeat: 'no-repeat',
    textIndent: '25px',
    backgroundPosition: '5px',
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
  }

  return (
    <div className="lg:max-w-8xl md:ml-4 xl:ml-4 lg:ml-4 xl:mr-12 lg:mr-12 md:mr-12 min-h-screen items-center justify-center text-white">
      {props.urlTrack ? <Urltracking PageName="Teams" /> : null}
      <MetaDescriptor section={TEAMS()} />
      <div className="block lg:hidden md:hidden center">
        <div className="flex items-center p-2.5  white text-base font-semibold lg:hidden">
          {/* <img
            className='mr-2 cursor-pointer'
            onClick={() => window.history.back()}
            src='/svgs/backIconWhite.svg'
            alt='back icon'
          /> */}
          <div
            onClick={() => window.history.back()}
            className="outline-0 cursor-pointer mr-2 lg:hidden bg-gray rounded"
          >
            <svg width="30" focusable="false" viewBox="0 0 24 24">
              <path
                fill="#ffff"
                d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"
              ></path>
              <path fill="none" d="M0 0h24v24H0z"></path>
            </svg>
          </div>
          <h1 className="font-semibold text-lg"> Teams </h1>
        </div>
        <div className=" py-3 px-2 center lg:w-full md:w-full text-center">
          <Search
            placeHolder="Search Team"
            // onSubmitUrl={getTeamUrl}
            getSearchResult={getSearchResult}
            onSearchChange={onSearchChange}
            style={styleMobo}
            className="w-full lg:w-full md:w-full font-nomral text-xs lg:text-sm md:text-sm bg-gray-8 p-2 border-2 opacity-80 border-silver outline-0"
          />
        </div>
        <div className="flex justify-between my-2 rounded-full w-5/11 mx-10  bg-gray ">
          {statusTabs.map(({ tabName, type }, i) => (
            <Link
              key={tabName}
              {...getTabUrl(type)}
              replace
              passHref
              className={`flex justify-center items-center oswald  white-50   p-2 cursor-pointer text-center flex-auto text-xs font-semibold ${
                status.toLowerCase() === type.toLowerCase()
                  ? 'z-1 rounded-full border-2 border-green-6 text-white '
                  : ''
              }`}
            >
              <div> {tabName === 'Live' ? 'LIVE' : tabName.toUpperCase()}</div>
              {tabName === 'Live' ? (
                <div class="ml-2  h-4 w-4 border-xl"></div>
              ) : null}
            </Link>
          ))}
        </div>
      </div>
      <div>
        <div className="hidden lg:block pt-20">
          <div className="py-4 center w-96 lg:w-full md:w-full capitalize">
            <Search
              placeHolder="Team Search"
              getSearchResult={getSearchResult}
              onSearchChange={onSearchChange}
              style={styleDesktop}
              className="w-full lg:w-full md:w-full font-normal text-xs lg:text-sm md:text-sm p-2 border-2 opacity-80 text-white bg-basebg outline-0 "
            />
          </div>
          <div className=" w-full flex flex-grow items-center">
            {statusTabs.map(({ tabName, type }, i) => (
              <Link
                key={i}
                {...getTabUrl(type)}
                passHref
                className={`white-52 w-full cursor-pointer pb-2 text-sm uppercase text-center  ${
                  status.toLowerCase() === type.toLowerCase()
                    ? 'z-1 border-b-2 border-b-green font-semibold white'
                    : 'text-gray-2 font-medium border-b-gray-2 '
                }`}
              >
                <span>{tabName}</span>
              </Link>
            ))}
          </div>
        </div>
        <TeamList teams={data.data} status={status} />
      </div>
    </div>
  )
}
