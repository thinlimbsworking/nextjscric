const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')
module.exports = {
  plugins: [
    require('@tailwindcss/line-clamp'),
    // ...
  ],
  // purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}', './your-other-component-folder/**/*.{js,ts,jsx,tsx}'],
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './app/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './components/**/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'class',
  theme: {
    // extend:{
    extend: {
      colors: {
        transparent: 'transparent',
        white: '#FFFFFF',
        black: '#000000',
        basebg: '#131B2A',
        basered: '#852121',
        light_gray: '#eeeeee',
        dekstop_theme: '#e8e9ef',
        gray: {
          DEFAULT: '#2B323F',
          1: '#2B323F',
          2: '#8C98B0',
          3: '#496780 ',
          4: '#1F2634',
          5: '#C9CED4',
          6: '#242B38',
          7: '#C9CED5',
          8: '#131B2B',
          9: '#3F4B62',
          10: '#F0F1F5',
          11: '#e2e2e2'
        },
        green: {
          DEFAULT: '#38d925',
          2: '#2E5A39',
          3: '#3BA63E',
          4: '#32B529',
          6: '#38D926',
        },
        blue: {
          DEFAULT: '#3587E8',
          2: '#1051F7',
          4: '#415A71',
          6: '#496780',
          7: '#082240',
          8: '#6A91A9',
          9: '#5F92AC',
        },

        red: {
          DEFAULT: '#F1350C',
          2: '#433239',
          4: '#F1350B',
          5: '#9F0C30',
          6: '#852121',
          8: '#541919',
          9: '#F02727',
        },
        yellow: {
          DEFAULT: '#FDD031',
          2: '#534F34',
          3: '#FDD032',
        },
        orange: {
          DEFAULT: '#F07A0D',
          2: '#634E40',
          3: '#F17A0C',
          4: '#3c291d',
          5: '#B1910C',
        },
      },
      // fontFamily: {
      //   mnr: 'Manrope',
      //   mnl: 'Manrope-Light',
      //   mnm: 'Manrope-Medium',
      //   mnsb: 'Manrope-SemiBold',
      //   mnb: 'Manrope-Bold',
      //   ttr: 'TitilliumWeb-Light',
      //   ttsb: 'TitilliumWeb-SemiBold',
      //   ttb: 'TitilliumWeb-Bold',
      // },
    },
    borderWidth: {
      DEFAULT: '1px',
      2: '2px',
      3: '3px',
      4: '4px',
      8: '8px',
    },
    // screens: {
    //  'sm': {'min':  '768px'},
    //   'md':{'min':  '976px'},
    //   'lg': {'min': '1440px'},
    //   'xl': {'min': '1720px'},
    // },
    
  
    plugins: [],
  },
}
